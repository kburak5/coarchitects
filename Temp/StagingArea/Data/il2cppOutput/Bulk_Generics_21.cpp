﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5761_TI;

#include "t637.h"
#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>
extern MethodInfo m30108_MI;
extern MethodInfo m30109_MI;
static PropertyInfo t5761____Item_PropertyInfo = 
{
	&t5761_TI, "Item", &m30108_MI, &m30109_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5761_PIs[] =
{
	&t5761____Item_PropertyInfo,
	NULL
};
extern Il2CppType t637_0_0_0;
extern Il2CppType t637_0_0_0;
static ParameterInfo t5761_m30110_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t637_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30110_GM;
MethodInfo m30110_MI = 
{
	"IndexOf", NULL, &t5761_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5761_m30110_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30110_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t637_0_0_0;
static ParameterInfo t5761_m30111_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t637_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30111_GM;
MethodInfo m30111_MI = 
{
	"Insert", NULL, &t5761_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5761_m30111_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30111_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5761_m30112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30112_GM;
MethodInfo m30112_MI = 
{
	"RemoveAt", NULL, &t5761_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5761_m30112_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30112_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5761_m30108_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t637_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30108_GM;
MethodInfo m30108_MI = 
{
	"get_Item", NULL, &t5761_TI, &t637_0_0_0, RuntimeInvoker_t29_t44, t5761_m30108_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30108_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t637_0_0_0;
static ParameterInfo t5761_m30109_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t637_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30109_GM;
MethodInfo m30109_MI = 
{
	"set_Item", NULL, &t5761_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5761_m30109_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30109_GM};
static MethodInfo* t5761_MIs[] =
{
	&m30110_MI,
	&m30111_MI,
	&m30112_MI,
	&m30108_MI,
	&m30109_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5760_TI;
extern TypeInfo t5762_TI;
static TypeInfo* t5761_ITIs[] = 
{
	&t603_TI,
	&t5760_TI,
	&t5762_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5761_0_0_0;
extern Il2CppType t5761_1_0_0;
struct t5761;
extern Il2CppGenericClass t5761_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5761_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5761_MIs, t5761_PIs, NULL, NULL, NULL, NULL, NULL, &t5761_TI, t5761_ITIs, NULL, &t1908__CustomAttributeCache, &t5761_TI, &t5761_0_0_0, &t5761_1_0_0, NULL, &t5761_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5763_TI;

#include "t40.h"
#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>
extern MethodInfo m30113_MI;
static PropertyInfo t5763____Count_PropertyInfo = 
{
	&t5763_TI, "Count", &m30113_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30114_MI;
static PropertyInfo t5763____IsReadOnly_PropertyInfo = 
{
	&t5763_TI, "IsReadOnly", &m30114_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5763_PIs[] =
{
	&t5763____Count_PropertyInfo,
	&t5763____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30113_GM;
MethodInfo m30113_MI = 
{
	"get_Count", NULL, &t5763_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30113_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30114_GM;
MethodInfo m30114_MI = 
{
	"get_IsReadOnly", NULL, &t5763_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30114_GM};
extern Il2CppType t2037_0_0_0;
extern Il2CppType t2037_0_0_0;
static ParameterInfo t5763_m30115_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2037_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30115_GM;
MethodInfo m30115_MI = 
{
	"Add", NULL, &t5763_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5763_m30115_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30115_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30116_GM;
MethodInfo m30116_MI = 
{
	"Clear", NULL, &t5763_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30116_GM};
extern Il2CppType t2037_0_0_0;
static ParameterInfo t5763_m30117_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2037_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30117_GM;
MethodInfo m30117_MI = 
{
	"Contains", NULL, &t5763_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5763_m30117_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30117_GM};
extern Il2CppType t3547_0_0_0;
extern Il2CppType t3547_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5763_m30118_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3547_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30118_GM;
MethodInfo m30118_MI = 
{
	"CopyTo", NULL, &t5763_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5763_m30118_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30118_GM};
extern Il2CppType t2037_0_0_0;
static ParameterInfo t5763_m30119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2037_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30119_GM;
MethodInfo m30119_MI = 
{
	"Remove", NULL, &t5763_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5763_m30119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30119_GM};
static MethodInfo* t5763_MIs[] =
{
	&m30113_MI,
	&m30114_MI,
	&m30115_MI,
	&m30116_MI,
	&m30117_MI,
	&m30118_MI,
	&m30119_MI,
	NULL
};
extern TypeInfo t5765_TI;
static TypeInfo* t5763_ITIs[] = 
{
	&t603_TI,
	&t5765_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5763_0_0_0;
extern Il2CppType t5763_1_0_0;
struct t5763;
extern Il2CppGenericClass t5763_GC;
TypeInfo t5763_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5763_MIs, t5763_PIs, NULL, NULL, NULL, NULL, NULL, &t5763_TI, t5763_ITIs, NULL, &EmptyCustomAttributesCache, &t5763_TI, &t5763_0_0_0, &t5763_1_0_0, NULL, &t5763_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterInfo>
extern Il2CppType t4489_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30120_GM;
MethodInfo m30120_MI = 
{
	"GetEnumerator", NULL, &t5765_TI, &t4489_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30120_GM};
static MethodInfo* t5765_MIs[] =
{
	&m30120_MI,
	NULL
};
static TypeInfo* t5765_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5765_0_0_0;
extern Il2CppType t5765_1_0_0;
struct t5765;
extern Il2CppGenericClass t5765_GC;
TypeInfo t5765_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5765_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5765_TI, t5765_ITIs, NULL, &EmptyCustomAttributesCache, &t5765_TI, &t5765_0_0_0, &t5765_1_0_0, NULL, &t5765_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4489_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterInfo>
extern MethodInfo m30121_MI;
static PropertyInfo t4489____Current_PropertyInfo = 
{
	&t4489_TI, "Current", &m30121_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4489_PIs[] =
{
	&t4489____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2037_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30121_GM;
MethodInfo m30121_MI = 
{
	"get_Current", NULL, &t4489_TI, &t2037_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30121_GM};
static MethodInfo* t4489_MIs[] =
{
	&m30121_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4489_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4489_0_0_0;
extern Il2CppType t4489_1_0_0;
struct t4489;
extern Il2CppGenericClass t4489_GC;
TypeInfo t4489_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4489_MIs, t4489_PIs, NULL, NULL, NULL, NULL, NULL, &t4489_TI, t4489_ITIs, NULL, &EmptyCustomAttributesCache, &t4489_TI, &t4489_0_0_0, &t4489_1_0_0, NULL, &t4489_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3094.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3094_TI;
#include "t3094MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t2037_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m17094_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m22943_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m22943(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3094_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3094_TI, offsetof(t3094, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3094_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3094_TI, offsetof(t3094, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3094_FIs[] =
{
	&t3094_f0_FieldInfo,
	&t3094_f1_FieldInfo,
	NULL
};
extern MethodInfo m17091_MI;
static PropertyInfo t3094____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3094_TI, "System.Collections.IEnumerator.Current", &m17091_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3094____Current_PropertyInfo = 
{
	&t3094_TI, "Current", &m17094_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3094_PIs[] =
{
	&t3094____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3094____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3094_m17090_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17090_GM;
MethodInfo m17090_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3094_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3094_m17090_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17090_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17091_GM;
MethodInfo m17091_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3094_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17091_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17092_GM;
MethodInfo m17092_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3094_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17092_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17093_GM;
MethodInfo m17093_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3094_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17093_GM};
extern Il2CppType t2037_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17094_GM;
MethodInfo m17094_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3094_TI, &t2037_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17094_GM};
static MethodInfo* t3094_MIs[] =
{
	&m17090_MI,
	&m17091_MI,
	&m17092_MI,
	&m17093_MI,
	&m17094_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m17093_MI;
extern MethodInfo m17092_MI;
static MethodInfo* t3094_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17091_MI,
	&m17093_MI,
	&m17092_MI,
	&m17094_MI,
};
static TypeInfo* t3094_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4489_TI,
};
static Il2CppInterfaceOffsetPair t3094_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4489_TI, 7},
};
extern TypeInfo t2037_TI;
static Il2CppRGCTXData t3094_RGCTXData[3] = 
{
	&m17094_MI/* Method Usage */,
	&t2037_TI/* Class Usage */,
	&m22943_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3094_0_0_0;
extern Il2CppType t3094_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3094_GC;
extern TypeInfo t20_TI;
TypeInfo t3094_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3094_MIs, t3094_PIs, t3094_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3094_TI, t3094_ITIs, t3094_VT, &EmptyCustomAttributesCache, &t3094_TI, &t3094_0_0_0, &t3094_1_0_0, t3094_IOs, &t3094_GC, NULL, NULL, NULL, t3094_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3094)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5764_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>
extern MethodInfo m30122_MI;
extern MethodInfo m30123_MI;
static PropertyInfo t5764____Item_PropertyInfo = 
{
	&t5764_TI, "Item", &m30122_MI, &m30123_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5764_PIs[] =
{
	&t5764____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2037_0_0_0;
static ParameterInfo t5764_m30124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2037_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30124_GM;
MethodInfo m30124_MI = 
{
	"IndexOf", NULL, &t5764_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5764_m30124_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30124_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2037_0_0_0;
static ParameterInfo t5764_m30125_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2037_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30125_GM;
MethodInfo m30125_MI = 
{
	"Insert", NULL, &t5764_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5764_m30125_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30125_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5764_m30126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30126_GM;
MethodInfo m30126_MI = 
{
	"RemoveAt", NULL, &t5764_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5764_m30126_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30126_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5764_m30122_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2037_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30122_GM;
MethodInfo m30122_MI = 
{
	"get_Item", NULL, &t5764_TI, &t2037_0_0_0, RuntimeInvoker_t29_t44, t5764_m30122_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30122_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2037_0_0_0;
static ParameterInfo t5764_m30123_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2037_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30123_GM;
MethodInfo m30123_MI = 
{
	"set_Item", NULL, &t5764_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5764_m30123_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30123_GM};
static MethodInfo* t5764_MIs[] =
{
	&m30124_MI,
	&m30125_MI,
	&m30126_MI,
	&m30122_MI,
	&m30123_MI,
	NULL
};
static TypeInfo* t5764_ITIs[] = 
{
	&t603_TI,
	&t5763_TI,
	&t5765_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5764_0_0_0;
extern Il2CppType t5764_1_0_0;
struct t5764;
extern Il2CppGenericClass t5764_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5764_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5764_MIs, t5764_PIs, NULL, NULL, NULL, NULL, NULL, &t5764_TI, t5764_ITIs, NULL, &t1908__CustomAttributeCache, &t5764_TI, &t5764_0_0_0, &t5764_1_0_0, NULL, &t5764_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4491_TI;

#include "t549.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern MethodInfo m30127_MI;
static PropertyInfo t4491____Current_PropertyInfo = 
{
	&t4491_TI, "Current", &m30127_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4491_PIs[] =
{
	&t4491____Current_PropertyInfo,
	NULL
};
extern Il2CppType t549_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30127_GM;
MethodInfo m30127_MI = 
{
	"get_Current", NULL, &t4491_TI, &t549_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30127_GM};
static MethodInfo* t4491_MIs[] =
{
	&m30127_MI,
	NULL
};
static TypeInfo* t4491_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4491_0_0_0;
extern Il2CppType t4491_1_0_0;
struct t4491;
extern Il2CppGenericClass t4491_GC;
TypeInfo t4491_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4491_MIs, t4491_PIs, NULL, NULL, NULL, NULL, NULL, &t4491_TI, t4491_ITIs, NULL, &EmptyCustomAttributesCache, &t4491_TI, &t4491_0_0_0, &t4491_1_0_0, NULL, &t4491_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3095.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3095_TI;
#include "t3095MD.h"

extern TypeInfo t549_TI;
extern MethodInfo m17099_MI;
extern MethodInfo m22954_MI;
struct t20;
#define m22954(__this, p0, method) (t549 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3095_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3095_TI, offsetof(t3095, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3095_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3095_TI, offsetof(t3095, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3095_FIs[] =
{
	&t3095_f0_FieldInfo,
	&t3095_f1_FieldInfo,
	NULL
};
extern MethodInfo m17096_MI;
static PropertyInfo t3095____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3095_TI, "System.Collections.IEnumerator.Current", &m17096_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3095____Current_PropertyInfo = 
{
	&t3095_TI, "Current", &m17099_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3095_PIs[] =
{
	&t3095____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3095____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3095_m17095_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17095_GM;
MethodInfo m17095_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3095_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3095_m17095_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17095_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17096_GM;
MethodInfo m17096_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3095_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17096_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17097_GM;
MethodInfo m17097_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3095_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17097_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17098_GM;
MethodInfo m17098_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3095_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17098_GM};
extern Il2CppType t549_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17099_GM;
MethodInfo m17099_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3095_TI, &t549_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17099_GM};
static MethodInfo* t3095_MIs[] =
{
	&m17095_MI,
	&m17096_MI,
	&m17097_MI,
	&m17098_MI,
	&m17099_MI,
	NULL
};
extern MethodInfo m17098_MI;
extern MethodInfo m17097_MI;
static MethodInfo* t3095_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17096_MI,
	&m17098_MI,
	&m17097_MI,
	&m17099_MI,
};
static TypeInfo* t3095_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4491_TI,
};
static Il2CppInterfaceOffsetPair t3095_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4491_TI, 7},
};
extern TypeInfo t549_TI;
static Il2CppRGCTXData t3095_RGCTXData[3] = 
{
	&m17099_MI/* Method Usage */,
	&t549_TI/* Class Usage */,
	&m22954_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3095_0_0_0;
extern Il2CppType t3095_1_0_0;
extern Il2CppGenericClass t3095_GC;
TypeInfo t3095_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3095_MIs, t3095_PIs, t3095_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3095_TI, t3095_ITIs, t3095_VT, &EmptyCustomAttributesCache, &t3095_TI, &t3095_0_0_0, &t3095_1_0_0, t3095_IOs, &t3095_GC, NULL, NULL, NULL, t3095_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3095)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5766_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern MethodInfo m30128_MI;
static PropertyInfo t5766____Count_PropertyInfo = 
{
	&t5766_TI, "Count", &m30128_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30129_MI;
static PropertyInfo t5766____IsReadOnly_PropertyInfo = 
{
	&t5766_TI, "IsReadOnly", &m30129_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5766_PIs[] =
{
	&t5766____Count_PropertyInfo,
	&t5766____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30128_GM;
MethodInfo m30128_MI = 
{
	"get_Count", NULL, &t5766_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30128_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30129_GM;
MethodInfo m30129_MI = 
{
	"get_IsReadOnly", NULL, &t5766_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30129_GM};
extern Il2CppType t549_0_0_0;
extern Il2CppType t549_0_0_0;
static ParameterInfo t5766_m30130_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t549_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30130_GM;
MethodInfo m30130_MI = 
{
	"Add", NULL, &t5766_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5766_m30130_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30130_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30131_GM;
MethodInfo m30131_MI = 
{
	"Clear", NULL, &t5766_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30131_GM};
extern Il2CppType t549_0_0_0;
static ParameterInfo t5766_m30132_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t549_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30132_GM;
MethodInfo m30132_MI = 
{
	"Contains", NULL, &t5766_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5766_m30132_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30132_GM};
extern Il2CppType t3787_0_0_0;
extern Il2CppType t3787_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5766_m30133_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3787_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30133_GM;
MethodInfo m30133_MI = 
{
	"CopyTo", NULL, &t5766_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5766_m30133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30133_GM};
extern Il2CppType t549_0_0_0;
static ParameterInfo t5766_m30134_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t549_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30134_GM;
MethodInfo m30134_MI = 
{
	"Remove", NULL, &t5766_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5766_m30134_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30134_GM};
static MethodInfo* t5766_MIs[] =
{
	&m30128_MI,
	&m30129_MI,
	&m30130_MI,
	&m30131_MI,
	&m30132_MI,
	&m30133_MI,
	&m30134_MI,
	NULL
};
extern TypeInfo t5768_TI;
static TypeInfo* t5766_ITIs[] = 
{
	&t603_TI,
	&t5768_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5766_0_0_0;
extern Il2CppType t5766_1_0_0;
struct t5766;
extern Il2CppGenericClass t5766_GC;
TypeInfo t5766_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5766_MIs, t5766_PIs, NULL, NULL, NULL, NULL, NULL, &t5766_TI, t5766_ITIs, NULL, &EmptyCustomAttributesCache, &t5766_TI, &t5766_0_0_0, &t5766_1_0_0, NULL, &t5766_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern Il2CppType t4491_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30135_GM;
MethodInfo m30135_MI = 
{
	"GetEnumerator", NULL, &t5768_TI, &t4491_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30135_GM};
static MethodInfo* t5768_MIs[] =
{
	&m30135_MI,
	NULL
};
static TypeInfo* t5768_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5768_0_0_0;
extern Il2CppType t5768_1_0_0;
struct t5768;
extern Il2CppGenericClass t5768_GC;
TypeInfo t5768_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5768_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5768_TI, t5768_ITIs, NULL, &EmptyCustomAttributesCache, &t5768_TI, &t5768_0_0_0, &t5768_1_0_0, NULL, &t5768_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5767_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern MethodInfo m30136_MI;
extern MethodInfo m30137_MI;
static PropertyInfo t5767____Item_PropertyInfo = 
{
	&t5767_TI, "Item", &m30136_MI, &m30137_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5767_PIs[] =
{
	&t5767____Item_PropertyInfo,
	NULL
};
extern Il2CppType t549_0_0_0;
static ParameterInfo t5767_m30138_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t549_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30138_GM;
MethodInfo m30138_MI = 
{
	"IndexOf", NULL, &t5767_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5767_m30138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30138_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t549_0_0_0;
static ParameterInfo t5767_m30139_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t549_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30139_GM;
MethodInfo m30139_MI = 
{
	"Insert", NULL, &t5767_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5767_m30139_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30139_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5767_m30140_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30140_GM;
MethodInfo m30140_MI = 
{
	"RemoveAt", NULL, &t5767_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5767_m30140_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30140_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5767_m30136_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t549_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30136_GM;
MethodInfo m30136_MI = 
{
	"get_Item", NULL, &t5767_TI, &t549_0_0_0, RuntimeInvoker_t29_t44, t5767_m30136_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30136_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t549_0_0_0;
static ParameterInfo t5767_m30137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t549_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30137_GM;
MethodInfo m30137_MI = 
{
	"set_Item", NULL, &t5767_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5767_m30137_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30137_GM};
static MethodInfo* t5767_MIs[] =
{
	&m30138_MI,
	&m30139_MI,
	&m30140_MI,
	&m30136_MI,
	&m30137_MI,
	NULL
};
static TypeInfo* t5767_ITIs[] = 
{
	&t603_TI,
	&t5766_TI,
	&t5768_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5767_0_0_0;
extern Il2CppType t5767_1_0_0;
struct t5767;
extern Il2CppGenericClass t5767_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5767_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5767_MIs, t5767_PIs, NULL, NULL, NULL, NULL, NULL, &t5767_TI, t5767_ITIs, NULL, &t1908__CustomAttributeCache, &t5767_TI, &t5767_0_0_0, &t5767_1_0_0, NULL, &t5767_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4493_TI;

#include "t550.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.StateMachineBehaviour>
extern MethodInfo m30141_MI;
static PropertyInfo t4493____Current_PropertyInfo = 
{
	&t4493_TI, "Current", &m30141_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4493_PIs[] =
{
	&t4493____Current_PropertyInfo,
	NULL
};
extern Il2CppType t550_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30141_GM;
MethodInfo m30141_MI = 
{
	"get_Current", NULL, &t4493_TI, &t550_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30141_GM};
static MethodInfo* t4493_MIs[] =
{
	&m30141_MI,
	NULL
};
static TypeInfo* t4493_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4493_0_0_0;
extern Il2CppType t4493_1_0_0;
struct t4493;
extern Il2CppGenericClass t4493_GC;
TypeInfo t4493_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4493_MIs, t4493_PIs, NULL, NULL, NULL, NULL, NULL, &t4493_TI, t4493_ITIs, NULL, &EmptyCustomAttributesCache, &t4493_TI, &t4493_0_0_0, &t4493_1_0_0, NULL, &t4493_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3096.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3096_TI;
#include "t3096MD.h"

extern TypeInfo t550_TI;
extern MethodInfo m17104_MI;
extern MethodInfo m22965_MI;
struct t20;
#define m22965(__this, p0, method) (t550 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType t20_0_0_1;
FieldInfo t3096_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3096_TI, offsetof(t3096, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3096_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3096_TI, offsetof(t3096, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3096_FIs[] =
{
	&t3096_f0_FieldInfo,
	&t3096_f1_FieldInfo,
	NULL
};
extern MethodInfo m17101_MI;
static PropertyInfo t3096____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3096_TI, "System.Collections.IEnumerator.Current", &m17101_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3096____Current_PropertyInfo = 
{
	&t3096_TI, "Current", &m17104_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3096_PIs[] =
{
	&t3096____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3096____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3096_m17100_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17100_GM;
MethodInfo m17100_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3096_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3096_m17100_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17100_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17101_GM;
MethodInfo m17101_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3096_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17101_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17102_GM;
MethodInfo m17102_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3096_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17102_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17103_GM;
MethodInfo m17103_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3096_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17103_GM};
extern Il2CppType t550_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17104_GM;
MethodInfo m17104_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3096_TI, &t550_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17104_GM};
static MethodInfo* t3096_MIs[] =
{
	&m17100_MI,
	&m17101_MI,
	&m17102_MI,
	&m17103_MI,
	&m17104_MI,
	NULL
};
extern MethodInfo m17103_MI;
extern MethodInfo m17102_MI;
static MethodInfo* t3096_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17101_MI,
	&m17103_MI,
	&m17102_MI,
	&m17104_MI,
};
static TypeInfo* t3096_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4493_TI,
};
static Il2CppInterfaceOffsetPair t3096_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4493_TI, 7},
};
extern TypeInfo t550_TI;
static Il2CppRGCTXData t3096_RGCTXData[3] = 
{
	&m17104_MI/* Method Usage */,
	&t550_TI/* Class Usage */,
	&m22965_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3096_0_0_0;
extern Il2CppType t3096_1_0_0;
extern Il2CppGenericClass t3096_GC;
TypeInfo t3096_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3096_MIs, t3096_PIs, t3096_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3096_TI, t3096_ITIs, t3096_VT, &EmptyCustomAttributesCache, &t3096_TI, &t3096_0_0_0, &t3096_1_0_0, t3096_IOs, &t3096_GC, NULL, NULL, NULL, t3096_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3096)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5769_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>
extern MethodInfo m30142_MI;
static PropertyInfo t5769____Count_PropertyInfo = 
{
	&t5769_TI, "Count", &m30142_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30143_MI;
static PropertyInfo t5769____IsReadOnly_PropertyInfo = 
{
	&t5769_TI, "IsReadOnly", &m30143_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5769_PIs[] =
{
	&t5769____Count_PropertyInfo,
	&t5769____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30142_GM;
MethodInfo m30142_MI = 
{
	"get_Count", NULL, &t5769_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30142_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30143_GM;
MethodInfo m30143_MI = 
{
	"get_IsReadOnly", NULL, &t5769_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30143_GM};
extern Il2CppType t550_0_0_0;
extern Il2CppType t550_0_0_0;
static ParameterInfo t5769_m30144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30144_GM;
MethodInfo m30144_MI = 
{
	"Add", NULL, &t5769_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5769_m30144_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30144_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30145_GM;
MethodInfo m30145_MI = 
{
	"Clear", NULL, &t5769_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30145_GM};
extern Il2CppType t550_0_0_0;
static ParameterInfo t5769_m30146_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30146_GM;
MethodInfo m30146_MI = 
{
	"Contains", NULL, &t5769_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5769_m30146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30146_GM};
extern Il2CppType t3788_0_0_0;
extern Il2CppType t3788_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5769_m30147_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3788_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30147_GM;
MethodInfo m30147_MI = 
{
	"CopyTo", NULL, &t5769_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5769_m30147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30147_GM};
extern Il2CppType t550_0_0_0;
static ParameterInfo t5769_m30148_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30148_GM;
MethodInfo m30148_MI = 
{
	"Remove", NULL, &t5769_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5769_m30148_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30148_GM};
static MethodInfo* t5769_MIs[] =
{
	&m30142_MI,
	&m30143_MI,
	&m30144_MI,
	&m30145_MI,
	&m30146_MI,
	&m30147_MI,
	&m30148_MI,
	NULL
};
extern TypeInfo t5771_TI;
static TypeInfo* t5769_ITIs[] = 
{
	&t603_TI,
	&t5771_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5769_0_0_0;
extern Il2CppType t5769_1_0_0;
struct t5769;
extern Il2CppGenericClass t5769_GC;
TypeInfo t5769_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5769_MIs, t5769_PIs, NULL, NULL, NULL, NULL, NULL, &t5769_TI, t5769_ITIs, NULL, &EmptyCustomAttributesCache, &t5769_TI, &t5769_0_0_0, &t5769_1_0_0, NULL, &t5769_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType t4493_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30149_GM;
MethodInfo m30149_MI = 
{
	"GetEnumerator", NULL, &t5771_TI, &t4493_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30149_GM};
static MethodInfo* t5771_MIs[] =
{
	&m30149_MI,
	NULL
};
static TypeInfo* t5771_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5771_0_0_0;
extern Il2CppType t5771_1_0_0;
struct t5771;
extern Il2CppGenericClass t5771_GC;
TypeInfo t5771_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5771_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5771_TI, t5771_ITIs, NULL, &EmptyCustomAttributesCache, &t5771_TI, &t5771_0_0_0, &t5771_1_0_0, NULL, &t5771_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5770_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>
extern MethodInfo m30150_MI;
extern MethodInfo m30151_MI;
static PropertyInfo t5770____Item_PropertyInfo = 
{
	&t5770_TI, "Item", &m30150_MI, &m30151_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5770_PIs[] =
{
	&t5770____Item_PropertyInfo,
	NULL
};
extern Il2CppType t550_0_0_0;
static ParameterInfo t5770_m30152_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30152_GM;
MethodInfo m30152_MI = 
{
	"IndexOf", NULL, &t5770_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5770_m30152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30152_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t550_0_0_0;
static ParameterInfo t5770_m30153_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30153_GM;
MethodInfo m30153_MI = 
{
	"Insert", NULL, &t5770_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5770_m30153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30153_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5770_m30154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30154_GM;
MethodInfo m30154_MI = 
{
	"RemoveAt", NULL, &t5770_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5770_m30154_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30154_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5770_m30150_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t550_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30150_GM;
MethodInfo m30150_MI = 
{
	"get_Item", NULL, &t5770_TI, &t550_0_0_0, RuntimeInvoker_t29_t44, t5770_m30150_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30150_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t550_0_0_0;
static ParameterInfo t5770_m30151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30151_GM;
MethodInfo m30151_MI = 
{
	"set_Item", NULL, &t5770_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5770_m30151_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30151_GM};
static MethodInfo* t5770_MIs[] =
{
	&m30152_MI,
	&m30153_MI,
	&m30154_MI,
	&m30150_MI,
	&m30151_MI,
	NULL
};
static TypeInfo* t5770_ITIs[] = 
{
	&t603_TI,
	&t5769_TI,
	&t5771_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5770_0_0_0;
extern Il2CppType t5770_1_0_0;
struct t5770;
extern Il2CppGenericClass t5770_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5770_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5770_MIs, t5770_PIs, NULL, NULL, NULL, NULL, NULL, &t5770_TI, t5770_ITIs, NULL, &t1908__CustomAttributeCache, &t5770_TI, &t5770_0_0_0, &t5770_1_0_0, NULL, &t5770_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3097.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3097_TI;
#include "t3097MD.h"

#include "t41.h"
#include "t557.h"
#include "t3098.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t3098_TI;
extern TypeInfo t21_TI;
#include "t3098MD.h"
extern MethodInfo m17107_MI;
extern MethodInfo m17109_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType t316_0_0_33;
FieldInfo t3097_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3097_TI, offsetof(t3097, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3097_FIs[] =
{
	&t3097_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t550_0_0_0;
static ParameterInfo t3097_m17105_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17105_GM;
MethodInfo m17105_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3097_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3097_m17105_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17105_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t3097_m17106_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17106_GM;
MethodInfo m17106_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3097_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3097_m17106_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17106_GM};
static MethodInfo* t3097_MIs[] =
{
	&m17105_MI,
	&m17106_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m17106_MI;
extern MethodInfo m17110_MI;
static MethodInfo* t3097_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17106_MI,
	&m17110_MI,
};
extern Il2CppType t3099_0_0_0;
extern TypeInfo t3099_TI;
extern MethodInfo m22975_MI;
extern TypeInfo t550_TI;
extern MethodInfo m17112_MI;
extern TypeInfo t550_TI;
static Il2CppRGCTXData t3097_RGCTXData[8] = 
{
	&t3099_0_0_0/* Type Usage */,
	&t3099_TI/* Class Usage */,
	&m22975_MI/* Method Usage */,
	&t550_TI/* Class Usage */,
	&m17112_MI/* Method Usage */,
	&m17107_MI/* Method Usage */,
	&t550_TI/* Class Usage */,
	&m17109_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3097_0_0_0;
extern Il2CppType t3097_1_0_0;
struct t3097;
extern Il2CppGenericClass t3097_GC;
TypeInfo t3097_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3097_MIs, NULL, t3097_FIs, NULL, &t3098_TI, NULL, NULL, &t3097_TI, NULL, t3097_VT, &EmptyCustomAttributesCache, &t3097_TI, &t3097_0_0_0, &t3097_1_0_0, NULL, &t3097_GC, NULL, NULL, NULL, t3097_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3097), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3099.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t3099_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t3099MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m22975(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType t3099_0_0_1;
FieldInfo t3098_f0_FieldInfo = 
{
	"Delegate", &t3099_0_0_1, &t3098_TI, offsetof(t3098, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3098_FIs[] =
{
	&t3098_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3098_m17107_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17107_GM;
MethodInfo m17107_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3098_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3098_m17107_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17107_GM};
extern Il2CppType t3099_0_0_0;
static ParameterInfo t3098_m17108_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17108_GM;
MethodInfo m17108_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3098_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3098_m17108_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17108_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3098_m17109_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17109_GM;
MethodInfo m17109_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3098_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3098_m17109_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17109_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3098_m17110_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17110_GM;
MethodInfo m17110_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3098_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3098_m17110_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17110_GM};
static MethodInfo* t3098_MIs[] =
{
	&m17107_MI,
	&m17108_MI,
	&m17109_MI,
	&m17110_MI,
	NULL
};
static MethodInfo* t3098_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17109_MI,
	&m17110_MI,
};
extern TypeInfo t3099_TI;
extern TypeInfo t550_TI;
static Il2CppRGCTXData t3098_RGCTXData[5] = 
{
	&t3099_0_0_0/* Type Usage */,
	&t3099_TI/* Class Usage */,
	&m22975_MI/* Method Usage */,
	&t550_TI/* Class Usage */,
	&m17112_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3098_0_0_0;
extern Il2CppType t3098_1_0_0;
extern TypeInfo t556_TI;
struct t3098;
extern Il2CppGenericClass t3098_GC;
TypeInfo t3098_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3098_MIs, NULL, t3098_FIs, NULL, &t556_TI, NULL, NULL, &t3098_TI, NULL, t3098_VT, &EmptyCustomAttributesCache, &t3098_TI, &t3098_0_0_0, &t3098_1_0_0, NULL, &t3098_GC, NULL, NULL, NULL, t3098_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3098), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3099_m17111_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17111_GM;
MethodInfo m17111_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3099_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3099_m17111_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17111_GM};
extern Il2CppType t550_0_0_0;
static ParameterInfo t3099_m17112_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17112_GM;
MethodInfo m17112_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3099_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3099_m17112_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17112_GM};
extern Il2CppType t550_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3099_m17113_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t550_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17113_GM;
MethodInfo m17113_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3099_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3099_m17113_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17113_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3099_m17114_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17114_GM;
MethodInfo m17114_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3099_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3099_m17114_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17114_GM};
static MethodInfo* t3099_MIs[] =
{
	&m17111_MI,
	&m17112_MI,
	&m17113_MI,
	&m17114_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m17113_MI;
extern MethodInfo m17114_MI;
static MethodInfo* t3099_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17112_MI,
	&m17113_MI,
	&m17114_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t3099_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3099_1_0_0;
extern TypeInfo t195_TI;
struct t3099;
extern Il2CppGenericClass t3099_GC;
TypeInfo t3099_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3099_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3099_TI, NULL, t3099_VT, &EmptyCustomAttributesCache, &t3099_TI, &t3099_0_0_0, &t3099_1_0_0, t3099_IOs, &t3099_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3099), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t553.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t553_TI;
#include "t553MD.h"

#include "t204.h"
#include "t552.h"
#include "t338.h"
#include "t1248.h"
#include "t1258.h"
#include "t3104.h"
#include "t733.h"
#include "t735.h"
#include "t3106.h"
#include "t725.h"
#include "t3103.h"
#include "t3115.h"
#include "t3108.h"
#include "t3116.h"
#include "t3117.h"
#include "t3119.h"
extern TypeInfo t204_TI;
extern TypeInfo t40_TI;
extern TypeInfo t552_TI;
extern TypeInfo t338_TI;
extern TypeInfo t3102_TI;
extern TypeInfo t44_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t3104_TI;
extern TypeInfo t3105_TI;
extern TypeInfo t3106_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t725_TI;
extern TypeInfo t3103_TI;
extern TypeInfo t3115_TI;
extern TypeInfo t3108_TI;
extern TypeInfo t3116_TI;
extern TypeInfo t915_TI;
extern TypeInfo t3117_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t3100_TI;
extern TypeInfo t3101_TI;
extern TypeInfo t719_TI;
extern TypeInfo t3119_TI;
extern TypeInfo t6742_TI;
extern TypeInfo t7_TI;
#include "t338MD.h"
#include "t1258MD.h"
#include "t3104MD.h"
#include "t29MD.h"
#include "t3106MD.h"
#include "t3103MD.h"
#include "t3115MD.h"
#include "t3108MD.h"
#include "t3116MD.h"
#include "t915MD.h"
#include "t3117MD.h"
#include "t719MD.h"
#include "t3119MD.h"
#include "t733MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t3102_0_0_0;
extern Il2CppType t3105_0_0_0;
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
extern MethodInfo m17146_MI;
extern MethodInfo m17153_MI;
extern MethodInfo m17135_MI;
extern MethodInfo m17154_MI;
extern MethodInfo m17136_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m30155_MI;
extern MethodInfo m30156_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m17143_MI;
extern MethodInfo m17179_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m17137_MI;
extern MethodInfo m17144_MI;
extern MethodInfo m17150_MI;
extern MethodInfo m17159_MI;
extern MethodInfo m17161_MI;
extern MethodInfo m17155_MI;
extern MethodInfo m17142_MI;
extern MethodInfo m17139_MI;
extern MethodInfo m17157_MI;
extern MethodInfo m17214_MI;
extern MethodInfo m23012_MI;
extern MethodInfo m17140_MI;
extern MethodInfo m17218_MI;
extern MethodInfo m23014_MI;
extern MethodInfo m17198_MI;
extern MethodInfo m17222_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m17232_MI;
extern MethodInfo m17138_MI;
extern MethodInfo m17134_MI;
extern MethodInfo m17158_MI;
extern MethodInfo m23015_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m17240_MI;
extern MethodInfo m30157_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m17151_MI;
extern MethodInfo m30158_MI;
extern MethodInfo m3965_MI;
struct t553;
 void m23012 (t553 * __this, t3541* p0, int32_t p1, t3103 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t553;
#include "t295.h"
 void m23014 (t553 * __this, t20 * p0, int32_t p1, t3115 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t553;
 void m23015 (t553 * __this, t3105* p0, int32_t p1, t3115 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17115_MI;
 void m17115 (t553 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m17137(__this, ((int32_t)10), (t29*)NULL, &m17137_MI);
		return;
	}
}
extern MethodInfo m17116_MI;
 void m17116 (t553 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m17137(__this, ((int32_t)10), p0, &m17137_MI);
		return;
	}
}
extern MethodInfo m17117_MI;
 void m17117 (t553 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m17137(__this, p0, (t29*)NULL, &m17137_MI);
		return;
	}
}
extern MethodInfo m17118_MI;
 void m17118 (t553 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m17119_MI;
 t29 * m17119 (t553 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t204 *)IsInst(p0, InitializedTypeInfo(&t204_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t204 * >::Invoke(&m17146_MI, __this, ((t204 *)Castclass(p0, InitializedTypeInfo(&t204_TI))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t204 * L_1 = m17153(__this, p0, &m17153_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t204 * >::Invoke(&m17135_MI, __this, L_1);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t552_TI), &L_3);
		return L_4;
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m17120_MI;
 void m17120 (t553 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t204 * L_0 = m17153(__this, p0, &m17153_MI);
		int32_t L_1 = m17154(__this, p1, &m17154_MI);
		VirtActionInvoker2< t204 *, int32_t >::Invoke(&m17136_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m17121_MI;
 void m17121 (t553 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t204 * L_0 = m17153(__this, p0, &m17153_MI);
		int32_t L_1 = m17154(__this, p1, &m17154_MI);
		VirtActionInvoker2< t204 *, int32_t >::Invoke(&m17144_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m17122_MI;
 void m17122 (t553 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t204 *)IsInst(p0, InitializedTypeInfo(&t204_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t204 * >::Invoke(&m17150_MI, __this, ((t204 *)Castclass(p0, InitializedTypeInfo(&t204_TI))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m17123_MI;
 bool m17123 (t553 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m17124_MI;
 t29 * m17124 (t553 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m17125_MI;
 bool m17125 (t553 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m17126_MI;
 void m17126 (t553 * __this, t3106  p0, MethodInfo* method){
	{
		t204 * L_0 = m17159((&p0), &m17159_MI);
		int32_t L_1 = m17161((&p0), &m17161_MI);
		VirtActionInvoker2< t204 *, int32_t >::Invoke(&m17144_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m17127_MI;
 bool m17127 (t553 * __this, t3106  p0, MethodInfo* method){
	{
		bool L_0 = m17155(__this, p0, &m17155_MI);
		return L_0;
	}
}
extern MethodInfo m17128_MI;
 void m17128 (t553 * __this, t3105* p0, int32_t p1, MethodInfo* method){
	{
		m17142(__this, p0, p1, &m17142_MI);
		return;
	}
}
extern MethodInfo m17129_MI;
 bool m17129 (t553 * __this, t3106  p0, MethodInfo* method){
	{
		bool L_0 = m17155(__this, p0, &m17155_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t204 * L_1 = m17159((&p0), &m17159_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t204 * >::Invoke(&m17150_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m17130_MI;
 void m17130 (t553 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t3105* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t553 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t553 * G_B4_2 = {0};
	{
		V_0 = ((t3105*)IsInst(p0, InitializedTypeInfo(&t3105_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m17142(__this, V_0, p1, &m17142_MI);
		return;
	}

IL_0013:
	{
		m17139(__this, p0, p1, &m17139_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t553 *)(__this));
		if ((((t553_SFs*)InitializedTypeInfo(&t553_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t553 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m17157_MI };
		t3103 * L_1 = (t3103 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3103_TI));
		m17214(L_1, NULL, L_0, &m17214_MI);
		((t553_SFs*)InitializedTypeInfo(&t553_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t553 *)(G_B4_2));
	}

IL_0040:
	{
		m23012(G_B5_2, G_B5_1, G_B5_0, (((t553_SFs*)InitializedTypeInfo(&t553_TI)->static_fields)->f15), &m23012_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m17140_MI };
		t3115 * L_3 = (t3115 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3115_TI));
		m17218(L_3, NULL, L_2, &m17218_MI);
		m23014(__this, p0, p1, L_3, &m23014_MI);
		return;
	}
}
extern MethodInfo m17131_MI;
 t29 * m17131 (t553 * __this, MethodInfo* method){
	{
		t3108  L_0 = {0};
		m17198(&L_0, __this, &m17198_MI);
		t3108  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3108_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m17132_MI;
 t29* m17132 (t553 * __this, MethodInfo* method){
	{
		t3108  L_0 = {0};
		m17198(&L_0, __this, &m17198_MI);
		t3108  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3108_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m17133_MI;
 t29 * m17133 (t553 * __this, MethodInfo* method){
	{
		t3116 * L_0 = (t3116 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3116_TI));
		m17222(L_0, __this, &m17222_MI);
		return L_0;
	}
}
 int32_t m17134 (t553 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 int32_t m17135 (t553 * __this, t204 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t204 * L_0 = p0;
		if (((t204 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t3100* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t204 *, t204 * >::Invoke(&m30156_MI, L_9, (*(t204 **)(t204 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t3101* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m17136 (t553 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t204 * L_0 = p0;
		if (((t204 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t3100* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t204 *, t204 * >::Invoke(&m30156_MI, L_9, (*(t204 **)(t204 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		m17143(__this, &m17143_MI);
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t3100* L_29 = (__this->f6);
		*((t204 **)(t204 **)SZArrayLdElema(L_29, V_2)) = (t204 *)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t3101* L_37 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_2)) = (int32_t)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m17137 (t553 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t553 * G_B4_0 = {0};
	t553 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t553 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t553 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t553 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t553 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3117_TI));
		t3117 * L_1 = m17232(NULL, &m17232_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t553 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m17138(__this, p0, &m17138_MI);
		__this->f14 = 0;
		return;
	}
}
 void m17138 (t553 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t3100*)SZArrayNew(InitializedTypeInfo(&t3100_TI), p0));
		__this->f7 = ((t3101*)SZArrayNew(InitializedTypeInfo(&t3101_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m17139 (t553 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m17134_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t3106  m17140 (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	{
		t3106  L_0 = {0};
		m17158(&L_0, p0, p1, &m17158_MI);
		return L_0;
	}
}
extern MethodInfo m17141_MI;
 int32_t m17141 (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m17142 (t553 * __this, t3105* p0, int32_t p1, MethodInfo* method){
	{
		m17139(__this, (t20 *)(t20 *)p0, p1, &m17139_MI);
		t35 L_0 = { &m17140_MI };
		t3115 * L_1 = (t3115 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3115_TI));
		m17218(L_1, NULL, L_0, &m17218_MI);
		m23015(__this, p0, p1, L_1, &m23015_MI);
		return;
	}
}
 void m17143 (t553 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t3100* V_7 = {0};
	t3101* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t3100* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_4, (*(t204 **)(t204 **)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t3100*)SZArrayNew(InitializedTypeInfo(&t3100_TI), V_0));
		V_8 = ((t3101*)SZArrayNew(InitializedTypeInfo(&t3101_TI), V_0));
		t3100* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t3101* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m17144 (t553 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t204 * L_0 = p0;
		if (((t204 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t3100* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t204 *, t204 * >::Invoke(&m30156_MI, L_9, (*(t204 **)(t204 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		m17143(__this, &m17143_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t3100* L_30 = (__this->f6);
		*((t204 **)(t204 **)SZArrayLdElema(L_30, V_2)) = (t204 *)p0;
		t3101* L_31 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
extern MethodInfo m17145_MI;
 void m17145 (t553 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t3100* L_2 = (__this->f6);
		t3100* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t3101* L_4 = (__this->f7);
		t3101* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m17146 (t553 * __this, t204 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t204 * L_0 = p0;
		if (((t204 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t3100* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t204 *, t204 * >::Invoke(&m30156_MI, L_9, (*(t204 **)(t204 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m17147_MI;
 bool m17147 (t553 * __this, int32_t p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3119_TI));
		t3119 * L_0 = m17240(NULL, &m17240_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t3101* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m30157_MI, V_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m17148_MI;
 void m17148 (t553 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t3105* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t3105*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t3105*)SZArrayNew(InitializedTypeInfo(&t3105_TI), L_4));
		m17142(__this, V_0, 0, &m17142_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m17149_MI;
 void m17149 (t553 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t3105* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t3102_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t3102_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t3105_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t3105*)Castclass(L_10, InitializedTypeInfo(&t3105_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m17138(__this, V_0, &m17138_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t204 * L_11 = m17159(((t3106 *)(t3106 *)SZArrayLdElema(V_1, V_2)), &m17159_MI);
		int32_t L_12 = m17161(((t3106 *)(t3106 *)SZArrayLdElema(V_1, V_2)), &m17161_MI);
		VirtActionInvoker2< t204 *, int32_t >::Invoke(&m17144_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m17150 (t553 * __this, t204 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t204 * V_4 = {0};
	int32_t V_5 = {0};
	{
		t204 * L_0 = p0;
		if (((t204 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t3100* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t204 *, t204 * >::Invoke(&m30156_MI, L_9, (*(t204 **)(t204 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t3100* L_25 = (__this->f6);
		Initobj (&t204_TI, (&V_4));
		*((t204 **)(t204 **)SZArrayLdElema(L_25, V_2)) = (t204 *)V_4;
		t3101* L_26 = (__this->f7);
		Initobj (&t552_TI, (&V_5));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m17151 (t553 * __this, t204 * p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		t204 * L_0 = p0;
		if (((t204 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t204 * >::Invoke(&m30155_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t3100* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t204 *, t204 * >::Invoke(&m30156_MI, L_9, (*(t204 **)(t204 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t3101* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t552_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m17152_MI;
 t3104 * m17152 (t553 * __this, MethodInfo* method){
	{
		t3104 * L_0 = (t3104 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3104_TI));
		m17179(L_0, __this, &m17179_MI);
		return L_0;
	}
}
 t204 * m17153 (t553 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t204 *)IsInst(p0, InitializedTypeInfo(&t204_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t204_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t204 *)Castclass(p0, InitializedTypeInfo(&t204_TI)));
	}
}
 int32_t m17154 (t553 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t552_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t552_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t552_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t552_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t552_TI)))));
	}
}
 bool m17155 (t553 * __this, t3106  p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		t204 * L_0 = m17159((&p0), &m17159_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t204 *, int32_t* >::Invoke(&m17151_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3119_TI));
		t3119 * L_2 = m17240(NULL, &m17240_MI);
		int32_t L_3 = m17161((&p0), &m17161_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m30158_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m17156_MI;
 t3108  m17156 (t553 * __this, MethodInfo* method){
	{
		t3108  L_0 = {0};
		m17198(&L_0, __this, &m17198_MI);
		return L_0;
	}
}
 t725  m17157 (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	{
		t204 * L_0 = p0;
		int32_t L_1 = p1;
		t29 * L_2 = Box(InitializedTypeInfo(&t552_TI), &L_1);
		t725  L_3 = {0};
		m3965(&L_3, ((t204 *)L_0), L_2, &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t44_0_0_32849;
FieldInfo t553_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t553_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t553_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t553_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t553_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t553_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t553_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t553_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t553_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t553_TI, offsetof(t553, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t553_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t553_TI, offsetof(t553, f5), &EmptyCustomAttributesCache};
extern Il2CppType t3100_0_0_1;
FieldInfo t553_f6_FieldInfo = 
{
	"keySlots", &t3100_0_0_1, &t553_TI, offsetof(t553, f6), &EmptyCustomAttributesCache};
extern Il2CppType t3101_0_0_1;
FieldInfo t553_f7_FieldInfo = 
{
	"valueSlots", &t3101_0_0_1, &t553_TI, offsetof(t553, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t553_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t553_TI, offsetof(t553, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t553_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t553_TI, offsetof(t553, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t553_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t553_TI, offsetof(t553, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t553_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t553_TI, offsetof(t553, f11), &EmptyCustomAttributesCache};
extern Il2CppType t3102_0_0_1;
FieldInfo t553_f12_FieldInfo = 
{
	"hcp", &t3102_0_0_1, &t553_TI, offsetof(t553, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t553_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t553_TI, offsetof(t553, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t553_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t553_TI, offsetof(t553, f14), &EmptyCustomAttributesCache};
extern Il2CppType t3103_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t553_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t3103_0_0_17, &t553_TI, offsetof(t553_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t553_FIs[] =
{
	&t553_f0_FieldInfo,
	&t553_f1_FieldInfo,
	&t553_f2_FieldInfo,
	&t553_f3_FieldInfo,
	&t553_f4_FieldInfo,
	&t553_f5_FieldInfo,
	&t553_f6_FieldInfo,
	&t553_f7_FieldInfo,
	&t553_f8_FieldInfo,
	&t553_f9_FieldInfo,
	&t553_f10_FieldInfo,
	&t553_f11_FieldInfo,
	&t553_f12_FieldInfo,
	&t553_f13_FieldInfo,
	&t553_f14_FieldInfo,
	&t553_f15_FieldInfo,
	NULL
};
static const int32_t t553_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t553_f0_DefaultValue = 
{
	&t553_f0_FieldInfo, { (char*)&t553_f0_DefaultValueData, &t44_0_0_0 }};
static const float t553_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t553_f1_DefaultValue = 
{
	&t553_f1_FieldInfo, { (char*)&t553_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t553_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t553_f2_DefaultValue = 
{
	&t553_f2_FieldInfo, { (char*)&t553_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t553_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t553_f3_DefaultValue = 
{
	&t553_f3_FieldInfo, { (char*)&t553_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t553_FDVs[] = 
{
	&t553_f0_DefaultValue,
	&t553_f1_DefaultValue,
	&t553_f2_DefaultValue,
	&t553_f3_DefaultValue,
	NULL
};
static PropertyInfo t553____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t553_TI, "System.Collections.IDictionary.Item", &m17119_MI, &m17120_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t553____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t553_TI, "System.Collections.ICollection.IsSynchronized", &m17123_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t553____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t553_TI, "System.Collections.ICollection.SyncRoot", &m17124_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t553____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t553_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m17125_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t553____Count_PropertyInfo = 
{
	&t553_TI, "Count", &m17134_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t553____Item_PropertyInfo = 
{
	&t553_TI, "Item", &m17135_MI, &m17136_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t553____Values_PropertyInfo = 
{
	&t553_TI, "Values", &m17152_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t553_PIs[] =
{
	&t553____System_Collections_IDictionary_Item_PropertyInfo,
	&t553____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t553____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t553____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t553____Count_PropertyInfo,
	&t553____Item_PropertyInfo,
	&t553____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17115_GM;
MethodInfo m17115_MI = 
{
	".ctor", (methodPointerType)&m17115, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17115_GM};
extern Il2CppType t3102_0_0_0;
static ParameterInfo t553_m17116_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t3102_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17116_GM;
MethodInfo m17116_MI = 
{
	".ctor", (methodPointerType)&m17116, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t553_m17116_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17116_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t553_m17117_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17117_GM;
MethodInfo m17117_MI = 
{
	".ctor", (methodPointerType)&m17117, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t553_m17117_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17117_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t553_m17118_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17118_GM;
MethodInfo m17118_MI = 
{
	".ctor", (methodPointerType)&m17118, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t553_m17118_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17118_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17119_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17119_GM;
MethodInfo m17119_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m17119, &t553_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t553_m17119_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17119_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17120_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17120_GM;
MethodInfo m17120_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m17120, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t553_m17120_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17120_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17121_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17121_GM;
MethodInfo m17121_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m17121, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t553_m17121_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17121_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17122_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17122_GM;
MethodInfo m17122_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m17122, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t553_m17122_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17122_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17123_GM;
MethodInfo m17123_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m17123, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17123_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17124_GM;
MethodInfo m17124_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m17124, &t553_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17124_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17125_GM;
MethodInfo m17125_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m17125, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17125_GM};
extern Il2CppType t3106_0_0_0;
extern Il2CppType t3106_0_0_0;
static ParameterInfo t553_m17126_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17126_GM;
MethodInfo m17126_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m17126, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t3106, t553_m17126_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17126_GM};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t553_m17127_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17127_GM;
MethodInfo m17127_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m17127, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t3106, t553_m17127_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17127_GM};
extern Il2CppType t3105_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t553_m17128_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3105_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17128_GM;
MethodInfo m17128_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m17128, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t553_m17128_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17128_GM};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t553_m17129_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17129_GM;
MethodInfo m17129_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m17129, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t3106, t553_m17129_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17129_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t553_m17130_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17130_GM;
MethodInfo m17130_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m17130, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t553_m17130_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17130_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17131_GM;
MethodInfo m17131_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m17131, &t553_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17131_GM};
extern Il2CppType t3107_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17132_GM;
MethodInfo m17132_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m17132, &t553_TI, &t3107_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17132_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17133_GM;
MethodInfo m17133_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m17133, &t553_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17133_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17134_GM;
MethodInfo m17134_MI = 
{
	"get_Count", (methodPointerType)&m17134, &t553_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17134_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t553_m17135_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17135_GM;
MethodInfo m17135_MI = 
{
	"get_Item", (methodPointerType)&m17135, &t553_TI, &t552_0_0_0, RuntimeInvoker_t552_t29, t553_m17135_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17135_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t553_m17136_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17136_GM;
MethodInfo m17136_MI = 
{
	"set_Item", (methodPointerType)&m17136, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t553_m17136_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17136_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3102_0_0_0;
static ParameterInfo t553_m17137_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t3102_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17137_GM;
MethodInfo m17137_MI = 
{
	"Init", (methodPointerType)&m17137, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t553_m17137_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17137_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t553_m17138_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17138_GM;
MethodInfo m17138_MI = 
{
	"InitArrays", (methodPointerType)&m17138, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t553_m17138_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17138_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t553_m17139_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17139_GM;
MethodInfo m17139_MI = 
{
	"CopyToCheck", (methodPointerType)&m17139, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t553_m17139_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17139_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6743_0_0_0;
extern Il2CppType t6743_0_0_0;
static ParameterInfo t553_m30159_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6743_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m30159_IGC;
extern TypeInfo m30159_gp_TRet_0_TI;
Il2CppGenericParamFull m30159_gp_TRet_0_TI_GenericParamFull = { { &m30159_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m30159_gp_TElem_1_TI;
Il2CppGenericParamFull m30159_gp_TElem_1_TI_GenericParamFull = { { &m30159_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m30159_IGPA[2] = 
{
	&m30159_gp_TRet_0_TI_GenericParamFull,
	&m30159_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m30159_MI;
Il2CppGenericContainer m30159_IGC = { { NULL, NULL }, NULL, &m30159_MI, 2, 1, m30159_IGPA };
extern Il2CppGenericMethod m30160_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m30159_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m30160_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m30159_GM;
MethodInfo m30159_MI = 
{
	"Do_CopyTo", NULL, &t553_TI, &t21_0_0_0, NULL, t553_m30159_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m30159_RGCTXData, (methodPointerType)NULL, &m30159_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t553_m17140_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17140_GM;
MethodInfo m17140_MI = 
{
	"make_pair", (methodPointerType)&m17140, &t553_TI, &t3106_0_0_0, RuntimeInvoker_t3106_t29_t44, t553_m17140_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17140_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t553_m17141_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17141_GM;
MethodInfo m17141_MI = 
{
	"pick_value", (methodPointerType)&m17141, &t553_TI, &t552_0_0_0, RuntimeInvoker_t552_t29_t44, t553_m17141_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17141_GM};
extern Il2CppType t3105_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t553_m17142_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3105_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17142_GM;
MethodInfo m17142_MI = 
{
	"CopyTo", (methodPointerType)&m17142, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t553_m17142_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17142_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6746_0_0_0;
extern Il2CppType t6746_0_0_0;
static ParameterInfo t553_m30161_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m30161_IGC;
extern TypeInfo m30161_gp_TRet_0_TI;
Il2CppGenericParamFull m30161_gp_TRet_0_TI_GenericParamFull = { { &m30161_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m30161_IGPA[1] = 
{
	&m30161_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m30161_MI;
Il2CppGenericContainer m30161_IGC = { { NULL, NULL }, NULL, &m30161_MI, 1, 1, m30161_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m30162_GM;
static Il2CppRGCTXDefinition m30161_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m30162_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m30161_GM;
MethodInfo m30161_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t553_TI, &t21_0_0_0, NULL, t553_m30161_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m30161_RGCTXData, (methodPointerType)NULL, &m30161_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17143_GM;
MethodInfo m17143_MI = 
{
	"Resize", (methodPointerType)&m17143, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17143_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t553_m17144_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17144_GM;
MethodInfo m17144_MI = 
{
	"Add", (methodPointerType)&m17144, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t553_m17144_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17144_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17145_GM;
MethodInfo m17145_MI = 
{
	"Clear", (methodPointerType)&m17145, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17145_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t553_m17146_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17146_GM;
MethodInfo m17146_MI = 
{
	"ContainsKey", (methodPointerType)&m17146, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t553_m17146_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17146_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t553_m17147_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17147_GM;
MethodInfo m17147_MI = 
{
	"ContainsValue", (methodPointerType)&m17147, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t553_m17147_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17147_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t553_m17148_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17148_GM;
MethodInfo m17148_MI = 
{
	"GetObjectData", (methodPointerType)&m17148, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t553_m17148_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17148_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17149_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17149_GM;
MethodInfo m17149_MI = 
{
	"OnDeserialization", (methodPointerType)&m17149, &t553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t553_m17149_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17149_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t553_m17150_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17150_GM;
MethodInfo m17150_MI = 
{
	"Remove", (methodPointerType)&m17150, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t553_m17150_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17150_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_1_0_2;
extern Il2CppType t552_1_0_0;
static ParameterInfo t553_m17151_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t4497 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17151_GM;
MethodInfo m17151_MI = 
{
	"TryGetValue", (methodPointerType)&m17151, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t4497, t553_m17151_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17151_GM};
extern Il2CppType t3104_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17152_GM;
MethodInfo m17152_MI = 
{
	"get_Values", (methodPointerType)&m17152, &t553_TI, &t3104_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17152_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17153_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t204_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17153_GM;
MethodInfo m17153_MI = 
{
	"ToTKey", (methodPointerType)&m17153, &t553_TI, &t204_0_0_0, RuntimeInvoker_t29_t29, t553_m17153_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17153_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t553_m17154_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17154_GM;
MethodInfo m17154_MI = 
{
	"ToTValue", (methodPointerType)&m17154, &t553_TI, &t552_0_0_0, RuntimeInvoker_t552_t29, t553_m17154_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17154_GM};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t553_m17155_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17155_GM;
MethodInfo m17155_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m17155, &t553_TI, &t40_0_0_0, RuntimeInvoker_t40_t3106, t553_m17155_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17155_GM};
extern Il2CppType t3108_0_0_0;
extern void* RuntimeInvoker_t3108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17156_GM;
MethodInfo m17156_MI = 
{
	"GetEnumerator", (methodPointerType)&m17156, &t553_TI, &t3108_0_0_0, RuntimeInvoker_t3108, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17156_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t553_m17157_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m17157_GM;
MethodInfo m17157_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m17157, &t553_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t553_m17157_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17157_GM};
static MethodInfo* t553_MIs[] =
{
	&m17115_MI,
	&m17116_MI,
	&m17117_MI,
	&m17118_MI,
	&m17119_MI,
	&m17120_MI,
	&m17121_MI,
	&m17122_MI,
	&m17123_MI,
	&m17124_MI,
	&m17125_MI,
	&m17126_MI,
	&m17127_MI,
	&m17128_MI,
	&m17129_MI,
	&m17130_MI,
	&m17131_MI,
	&m17132_MI,
	&m17133_MI,
	&m17134_MI,
	&m17135_MI,
	&m17136_MI,
	&m17137_MI,
	&m17138_MI,
	&m17139_MI,
	&m30159_MI,
	&m17140_MI,
	&m17141_MI,
	&m17142_MI,
	&m30161_MI,
	&m17143_MI,
	&m17144_MI,
	&m17145_MI,
	&m17146_MI,
	&m17147_MI,
	&m17148_MI,
	&m17149_MI,
	&m17150_MI,
	&m17151_MI,
	&m17152_MI,
	&m17153_MI,
	&m17154_MI,
	&m17155_MI,
	&m17156_MI,
	&m17157_MI,
	NULL
};
static MethodInfo* t553_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17131_MI,
	&m17148_MI,
	&m17134_MI,
	&m17123_MI,
	&m17124_MI,
	&m17130_MI,
	&m17134_MI,
	&m17125_MI,
	&m17126_MI,
	&m17145_MI,
	&m17127_MI,
	&m17128_MI,
	&m17129_MI,
	&m17132_MI,
	&m17150_MI,
	&m17119_MI,
	&m17120_MI,
	&m17121_MI,
	&m17133_MI,
	&m17122_MI,
	&m17149_MI,
	&m17135_MI,
	&m17136_MI,
	&m17144_MI,
	&m17146_MI,
	&m17148_MI,
	&m17149_MI,
	&m17151_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t5772_TI;
extern TypeInfo t5774_TI;
extern TypeInfo t6748_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t553_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5772_TI,
	&t5774_TI,
	&t6748_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t553_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5772_TI, 10},
	{ &t5774_TI, 17},
	{ &t6748_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t553_0_0_0;
extern Il2CppType t553_1_0_0;
struct t553;
extern Il2CppGenericClass t553_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t553_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t553_MIs, t553_PIs, t553_FIs, NULL, &t29_TI, NULL, NULL, &t553_TI, t553_ITIs, t553_VT, &t1254__CustomAttributeCache, &t553_TI, &t553_0_0_0, &t553_1_0_0, t553_IOs, &t553_GC, NULL, t553_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t553), 0, -1, sizeof(t553_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
extern MethodInfo m30163_MI;
static PropertyInfo t5772____Count_PropertyInfo = 
{
	&t5772_TI, "Count", &m30163_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30164_MI;
static PropertyInfo t5772____IsReadOnly_PropertyInfo = 
{
	&t5772_TI, "IsReadOnly", &m30164_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5772_PIs[] =
{
	&t5772____Count_PropertyInfo,
	&t5772____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30163_GM;
MethodInfo m30163_MI = 
{
	"get_Count", NULL, &t5772_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30163_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30164_GM;
MethodInfo m30164_MI = 
{
	"get_IsReadOnly", NULL, &t5772_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30164_GM};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t5772_m30165_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30165_GM;
MethodInfo m30165_MI = 
{
	"Add", NULL, &t5772_TI, &t21_0_0_0, RuntimeInvoker_t21_t3106, t5772_m30165_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30165_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30166_GM;
MethodInfo m30166_MI = 
{
	"Clear", NULL, &t5772_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30166_GM};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t5772_m30167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30167_GM;
MethodInfo m30167_MI = 
{
	"Contains", NULL, &t5772_TI, &t40_0_0_0, RuntimeInvoker_t40_t3106, t5772_m30167_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30167_GM};
extern Il2CppType t3105_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5772_m30168_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3105_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30168_GM;
MethodInfo m30168_MI = 
{
	"CopyTo", NULL, &t5772_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5772_m30168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30168_GM};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t5772_m30169_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30169_GM;
MethodInfo m30169_MI = 
{
	"Remove", NULL, &t5772_TI, &t40_0_0_0, RuntimeInvoker_t40_t3106, t5772_m30169_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30169_GM};
static MethodInfo* t5772_MIs[] =
{
	&m30163_MI,
	&m30164_MI,
	&m30165_MI,
	&m30166_MI,
	&m30167_MI,
	&m30168_MI,
	&m30169_MI,
	NULL
};
static TypeInfo* t5772_ITIs[] = 
{
	&t603_TI,
	&t5774_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5772_0_0_0;
extern Il2CppType t5772_1_0_0;
struct t5772;
extern Il2CppGenericClass t5772_GC;
TypeInfo t5772_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5772_MIs, t5772_PIs, NULL, NULL, NULL, NULL, NULL, &t5772_TI, t5772_ITIs, NULL, &EmptyCustomAttributesCache, &t5772_TI, &t5772_0_0_0, &t5772_1_0_0, NULL, &t5772_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
extern Il2CppType t3107_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30170_GM;
MethodInfo m30170_MI = 
{
	"GetEnumerator", NULL, &t5774_TI, &t3107_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30170_GM};
static MethodInfo* t5774_MIs[] =
{
	&m30170_MI,
	NULL
};
static TypeInfo* t5774_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5774_0_0_0;
extern Il2CppType t5774_1_0_0;
struct t5774;
extern Il2CppGenericClass t5774_GC;
TypeInfo t5774_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5774_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5774_TI, t5774_ITIs, NULL, &EmptyCustomAttributesCache, &t5774_TI, &t5774_0_0_0, &t5774_1_0_0, NULL, &t5774_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3107_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
extern MethodInfo m30171_MI;
static PropertyInfo t3107____Current_PropertyInfo = 
{
	&t3107_TI, "Current", &m30171_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3107_PIs[] =
{
	&t3107____Current_PropertyInfo,
	NULL
};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30171_GM;
MethodInfo m30171_MI = 
{
	"get_Current", NULL, &t3107_TI, &t3106_0_0_0, RuntimeInvoker_t3106, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30171_GM};
static MethodInfo* t3107_MIs[] =
{
	&m30171_MI,
	NULL
};
static TypeInfo* t3107_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3107_0_0_0;
extern Il2CppType t3107_1_0_0;
struct t3107;
extern Il2CppGenericClass t3107_GC;
TypeInfo t3107_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3107_MIs, t3107_PIs, NULL, NULL, NULL, NULL, NULL, &t3107_TI, t3107_ITIs, NULL, &EmptyCustomAttributesCache, &t3107_TI, &t3107_0_0_0, &t3107_1_0_0, NULL, &t3107_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t446_TI;
extern MethodInfo m17160_MI;
extern MethodInfo m17162_MI;
extern MethodInfo m4008_MI;


 void m17158 (t3106 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	{
		m17160(__this, p0, &m17160_MI);
		m17162(__this, p1, &m17162_MI);
		return;
	}
}
 t204 * m17159 (t3106 * __this, MethodInfo* method){
	{
		t204 * L_0 = (__this->f0);
		return L_0;
	}
}
 void m17160 (t3106 * __this, t204 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 int32_t m17161 (t3106 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 void m17162 (t3106 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m17163_MI;
 t7* m17163 (t3106 * __this, MethodInfo* method){
	t204 * V_0 = {0};
	int32_t V_1 = {0};
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t204 * L_2 = m17159(__this, &m17159_MI);
		t204 * L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t204 *)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t204 * L_4 = m17159(__this, &m17159_MI);
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		int32_t L_8 = m17161(__this, &m17161_MI);
		int32_t L_9 = L_8;
		t29 * L_10 = Box(InitializedTypeInfo(&t552_TI), &L_9);
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_10)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		int32_t L_11 = m17161(__this, &m17161_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t552_TI), &(*(&V_1))));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t204_0_0_1;
FieldInfo t3106_f0_FieldInfo = 
{
	"key", &t204_0_0_1, &t3106_TI, offsetof(t3106, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_1;
FieldInfo t3106_f1_FieldInfo = 
{
	"value", &t552_0_0_1, &t3106_TI, offsetof(t3106, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3106_FIs[] =
{
	&t3106_f0_FieldInfo,
	&t3106_f1_FieldInfo,
	NULL
};
static PropertyInfo t3106____Key_PropertyInfo = 
{
	&t3106_TI, "Key", &m17159_MI, &m17160_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3106____Value_PropertyInfo = 
{
	&t3106_TI, "Value", &m17161_MI, &m17162_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3106_PIs[] =
{
	&t3106____Key_PropertyInfo,
	&t3106____Value_PropertyInfo,
	NULL
};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t3106_m17158_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17158_GM;
MethodInfo m17158_MI = 
{
	".ctor", (methodPointerType)&m17158, &t3106_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3106_m17158_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17158_GM};
extern Il2CppType t204_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17159_GM;
MethodInfo m17159_MI = 
{
	"get_Key", (methodPointerType)&m17159, &t3106_TI, &t204_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17159_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t3106_m17160_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17160_GM;
MethodInfo m17160_MI = 
{
	"set_Key", (methodPointerType)&m17160, &t3106_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3106_m17160_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17160_GM};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17161_GM;
MethodInfo m17161_MI = 
{
	"get_Value", (methodPointerType)&m17161, &t3106_TI, &t552_0_0_0, RuntimeInvoker_t552, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17161_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t3106_m17162_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17162_GM;
MethodInfo m17162_MI = 
{
	"set_Value", (methodPointerType)&m17162, &t3106_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3106_m17162_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17162_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17163_GM;
MethodInfo m17163_MI = 
{
	"ToString", (methodPointerType)&m17163, &t3106_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17163_GM};
static MethodInfo* t3106_MIs[] =
{
	&m17158_MI,
	&m17159_MI,
	&m17160_MI,
	&m17161_MI,
	&m17162_MI,
	&m17163_MI,
	NULL
};
static MethodInfo* t3106_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m17163_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3106_1_0_0;
extern Il2CppGenericClass t3106_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t3106_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t3106_MIs, t3106_PIs, t3106_FIs, NULL, &t110_TI, NULL, NULL, &t3106_TI, NULL, t3106_VT, &t1259__CustomAttributeCache, &t3106_TI, &t3106_0_0_0, &t3106_1_0_0, NULL, &t3106_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3106)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t3109.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3109_TI;
#include "t3109MD.h"

extern MethodInfo m17168_MI;
extern MethodInfo m22977_MI;
struct t20;
 t3106  m22977 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17164_MI;
 void m17164 (t3109 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17165_MI;
 t29 * m17165 (t3109 * __this, MethodInfo* method){
	{
		t3106  L_0 = m17168(__this, &m17168_MI);
		t3106  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3106_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17166_MI;
 void m17166 (t3109 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17167_MI;
 bool m17167 (t3109 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t3106  m17168 (t3109 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t3106  L_8 = m22977(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22977_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
extern Il2CppType t20_0_0_1;
FieldInfo t3109_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3109_TI, offsetof(t3109, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3109_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3109_TI, offsetof(t3109, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3109_FIs[] =
{
	&t3109_f0_FieldInfo,
	&t3109_f1_FieldInfo,
	NULL
};
static PropertyInfo t3109____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3109_TI, "System.Collections.IEnumerator.Current", &m17165_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3109____Current_PropertyInfo = 
{
	&t3109_TI, "Current", &m17168_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3109_PIs[] =
{
	&t3109____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3109____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3109_m17164_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17164_GM;
MethodInfo m17164_MI = 
{
	".ctor", (methodPointerType)&m17164, &t3109_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3109_m17164_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17164_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17165_GM;
MethodInfo m17165_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17165, &t3109_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17165_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17166_GM;
MethodInfo m17166_MI = 
{
	"Dispose", (methodPointerType)&m17166, &t3109_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17166_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17167_GM;
MethodInfo m17167_MI = 
{
	"MoveNext", (methodPointerType)&m17167, &t3109_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17167_GM};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17168_GM;
MethodInfo m17168_MI = 
{
	"get_Current", (methodPointerType)&m17168, &t3109_TI, &t3106_0_0_0, RuntimeInvoker_t3106, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17168_GM};
static MethodInfo* t3109_MIs[] =
{
	&m17164_MI,
	&m17165_MI,
	&m17166_MI,
	&m17167_MI,
	&m17168_MI,
	NULL
};
static MethodInfo* t3109_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17165_MI,
	&m17167_MI,
	&m17166_MI,
	&m17168_MI,
};
static TypeInfo* t3109_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3107_TI,
};
static Il2CppInterfaceOffsetPair t3109_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3107_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3109_0_0_0;
extern Il2CppType t3109_1_0_0;
extern Il2CppGenericClass t3109_GC;
TypeInfo t3109_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3109_MIs, t3109_PIs, t3109_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3109_TI, t3109_ITIs, t3109_VT, &EmptyCustomAttributesCache, &t3109_TI, &t3109_0_0_0, &t3109_1_0_0, t3109_IOs, &t3109_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3109)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5773_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
extern MethodInfo m30172_MI;
extern MethodInfo m30173_MI;
static PropertyInfo t5773____Item_PropertyInfo = 
{
	&t5773_TI, "Item", &m30172_MI, &m30173_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5773_PIs[] =
{
	&t5773____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3106_0_0_0;
static ParameterInfo t5773_m30174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30174_GM;
MethodInfo m30174_MI = 
{
	"IndexOf", NULL, &t5773_TI, &t44_0_0_0, RuntimeInvoker_t44_t3106, t5773_m30174_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30174_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3106_0_0_0;
static ParameterInfo t5773_m30175_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30175_GM;
MethodInfo m30175_MI = 
{
	"Insert", NULL, &t5773_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t3106, t5773_m30175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30175_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5773_m30176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30176_GM;
MethodInfo m30176_MI = 
{
	"RemoveAt", NULL, &t5773_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5773_m30176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30176_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5773_m30172_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30172_GM;
MethodInfo m30172_MI = 
{
	"get_Item", NULL, &t5773_TI, &t3106_0_0_0, RuntimeInvoker_t3106_t44, t5773_m30172_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30172_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3106_0_0_0;
static ParameterInfo t5773_m30173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t3106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30173_GM;
MethodInfo m30173_MI = 
{
	"set_Item", NULL, &t5773_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t3106, t5773_m30173_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30173_GM};
static MethodInfo* t5773_MIs[] =
{
	&m30174_MI,
	&m30175_MI,
	&m30176_MI,
	&m30172_MI,
	&m30173_MI,
	NULL
};
static TypeInfo* t5773_ITIs[] = 
{
	&t603_TI,
	&t5772_TI,
	&t5774_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5773_0_0_0;
extern Il2CppType t5773_1_0_0;
struct t5773;
extern Il2CppGenericClass t5773_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5773_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5773_MIs, t5773_PIs, NULL, NULL, NULL, NULL, NULL, &t5773_TI, t5773_ITIs, NULL, &t1908__CustomAttributeCache, &t5773_TI, &t5773_0_0_0, &t5773_1_0_0, NULL, &t5773_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t204_0_0_0;
static ParameterInfo t6748_m30177_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30177_GM;
MethodInfo m30177_MI = 
{
	"Remove", NULL, &t6748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6748_m30177_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30177_GM};
static MethodInfo* t6748_MIs[] =
{
	&m30177_MI,
	NULL
};
static TypeInfo* t6748_ITIs[] = 
{
	&t603_TI,
	&t5772_TI,
	&t5774_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6748_0_0_0;
extern Il2CppType t6748_1_0_0;
struct t6748;
extern Il2CppGenericClass t6748_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6748_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6748_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6748_TI, t6748_ITIs, NULL, &t1975__CustomAttributeCache, &t6748_TI, &t6748_0_0_0, &t6748_1_0_0, NULL, &t6748_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Event>
extern Il2CppType t204_0_0_0;
extern Il2CppType t204_0_0_0;
static ParameterInfo t3102_m30156_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30156_GM;
MethodInfo m30156_MI = 
{
	"Equals", NULL, &t3102_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3102_m30156_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30156_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t3102_m30155_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30155_GM;
MethodInfo m30155_MI = 
{
	"GetHashCode", NULL, &t3102_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3102_m30155_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30155_GM};
static MethodInfo* t3102_MIs[] =
{
	&m30156_MI,
	&m30155_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3102_1_0_0;
struct t3102;
extern Il2CppGenericClass t3102_GC;
TypeInfo t3102_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t3102_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3102_TI, NULL, NULL, &EmptyCustomAttributesCache, &t3102_TI, &t3102_0_0_0, &t3102_1_0_0, NULL, &t3102_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4496_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Event>
extern MethodInfo m30178_MI;
static PropertyInfo t4496____Current_PropertyInfo = 
{
	&t4496_TI, "Current", &m30178_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4496_PIs[] =
{
	&t4496____Current_PropertyInfo,
	NULL
};
extern Il2CppType t204_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30178_GM;
MethodInfo m30178_MI = 
{
	"get_Current", NULL, &t4496_TI, &t204_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30178_GM};
static MethodInfo* t4496_MIs[] =
{
	&m30178_MI,
	NULL
};
static TypeInfo* t4496_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4496_0_0_0;
extern Il2CppType t4496_1_0_0;
struct t4496;
extern Il2CppGenericClass t4496_GC;
TypeInfo t4496_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4496_MIs, t4496_PIs, NULL, NULL, NULL, NULL, NULL, &t4496_TI, t4496_ITIs, NULL, &EmptyCustomAttributesCache, &t4496_TI, &t4496_0_0_0, &t4496_1_0_0, NULL, &t4496_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3110.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3110_TI;
#include "t3110MD.h"

extern MethodInfo m17173_MI;
extern MethodInfo m22988_MI;
struct t20;
#define m22988(__this, p0, method) (t204 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Event>
extern Il2CppType t20_0_0_1;
FieldInfo t3110_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3110_TI, offsetof(t3110, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3110_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3110_TI, offsetof(t3110, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3110_FIs[] =
{
	&t3110_f0_FieldInfo,
	&t3110_f1_FieldInfo,
	NULL
};
extern MethodInfo m17170_MI;
static PropertyInfo t3110____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3110_TI, "System.Collections.IEnumerator.Current", &m17170_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3110____Current_PropertyInfo = 
{
	&t3110_TI, "Current", &m17173_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3110_PIs[] =
{
	&t3110____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3110____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3110_m17169_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17169_GM;
MethodInfo m17169_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3110_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3110_m17169_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17169_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17170_GM;
MethodInfo m17170_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3110_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17170_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17171_GM;
MethodInfo m17171_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3110_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17171_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17172_GM;
MethodInfo m17172_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3110_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17172_GM};
extern Il2CppType t204_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17173_GM;
MethodInfo m17173_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3110_TI, &t204_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17173_GM};
static MethodInfo* t3110_MIs[] =
{
	&m17169_MI,
	&m17170_MI,
	&m17171_MI,
	&m17172_MI,
	&m17173_MI,
	NULL
};
extern MethodInfo m17172_MI;
extern MethodInfo m17171_MI;
static MethodInfo* t3110_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17170_MI,
	&m17172_MI,
	&m17171_MI,
	&m17173_MI,
};
static TypeInfo* t3110_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4496_TI,
};
static Il2CppInterfaceOffsetPair t3110_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4496_TI, 7},
};
extern TypeInfo t204_TI;
static Il2CppRGCTXData t3110_RGCTXData[3] = 
{
	&m17173_MI/* Method Usage */,
	&t204_TI/* Class Usage */,
	&m22988_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3110_0_0_0;
extern Il2CppType t3110_1_0_0;
extern Il2CppGenericClass t3110_GC;
TypeInfo t3110_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3110_MIs, t3110_PIs, t3110_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3110_TI, t3110_ITIs, t3110_VT, &EmptyCustomAttributesCache, &t3110_TI, &t3110_0_0_0, &t3110_1_0_0, t3110_IOs, &t3110_GC, NULL, NULL, NULL, t3110_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3110)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5775_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Event>
extern MethodInfo m30179_MI;
static PropertyInfo t5775____Count_PropertyInfo = 
{
	&t5775_TI, "Count", &m30179_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30180_MI;
static PropertyInfo t5775____IsReadOnly_PropertyInfo = 
{
	&t5775_TI, "IsReadOnly", &m30180_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5775_PIs[] =
{
	&t5775____Count_PropertyInfo,
	&t5775____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30179_GM;
MethodInfo m30179_MI = 
{
	"get_Count", NULL, &t5775_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30179_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30180_GM;
MethodInfo m30180_MI = 
{
	"get_IsReadOnly", NULL, &t5775_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30180_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t5775_m30181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30181_GM;
MethodInfo m30181_MI = 
{
	"Add", NULL, &t5775_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5775_m30181_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30181_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30182_GM;
MethodInfo m30182_MI = 
{
	"Clear", NULL, &t5775_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30182_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t5775_m30183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30183_GM;
MethodInfo m30183_MI = 
{
	"Contains", NULL, &t5775_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5775_m30183_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30183_GM};
extern Il2CppType t3100_0_0_0;
extern Il2CppType t3100_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5775_m30184_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3100_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30184_GM;
MethodInfo m30184_MI = 
{
	"CopyTo", NULL, &t5775_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5775_m30184_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30184_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t5775_m30185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30185_GM;
MethodInfo m30185_MI = 
{
	"Remove", NULL, &t5775_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5775_m30185_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30185_GM};
static MethodInfo* t5775_MIs[] =
{
	&m30179_MI,
	&m30180_MI,
	&m30181_MI,
	&m30182_MI,
	&m30183_MI,
	&m30184_MI,
	&m30185_MI,
	NULL
};
extern TypeInfo t5777_TI;
static TypeInfo* t5775_ITIs[] = 
{
	&t603_TI,
	&t5777_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5775_0_0_0;
extern Il2CppType t5775_1_0_0;
struct t5775;
extern Il2CppGenericClass t5775_GC;
TypeInfo t5775_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5775_MIs, t5775_PIs, NULL, NULL, NULL, NULL, NULL, &t5775_TI, t5775_ITIs, NULL, &EmptyCustomAttributesCache, &t5775_TI, &t5775_0_0_0, &t5775_1_0_0, NULL, &t5775_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Event>
extern Il2CppType t4496_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30186_GM;
MethodInfo m30186_MI = 
{
	"GetEnumerator", NULL, &t5777_TI, &t4496_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30186_GM};
static MethodInfo* t5777_MIs[] =
{
	&m30186_MI,
	NULL
};
static TypeInfo* t5777_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5777_0_0_0;
extern Il2CppType t5777_1_0_0;
struct t5777;
extern Il2CppGenericClass t5777_GC;
TypeInfo t5777_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5777_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5777_TI, t5777_ITIs, NULL, &EmptyCustomAttributesCache, &t5777_TI, &t5777_0_0_0, &t5777_1_0_0, NULL, &t5777_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5776_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Event>
extern MethodInfo m30187_MI;
extern MethodInfo m30188_MI;
static PropertyInfo t5776____Item_PropertyInfo = 
{
	&t5776_TI, "Item", &m30187_MI, &m30188_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5776_PIs[] =
{
	&t5776____Item_PropertyInfo,
	NULL
};
extern Il2CppType t204_0_0_0;
static ParameterInfo t5776_m30189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30189_GM;
MethodInfo m30189_MI = 
{
	"IndexOf", NULL, &t5776_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5776_m30189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30189_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t204_0_0_0;
static ParameterInfo t5776_m30190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30190_GM;
MethodInfo m30190_MI = 
{
	"Insert", NULL, &t5776_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5776_m30190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30190_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5776_m30191_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30191_GM;
MethodInfo m30191_MI = 
{
	"RemoveAt", NULL, &t5776_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5776_m30191_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30191_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5776_m30187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t204_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30187_GM;
MethodInfo m30187_MI = 
{
	"get_Item", NULL, &t5776_TI, &t204_0_0_0, RuntimeInvoker_t29_t44, t5776_m30187_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30187_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t204_0_0_0;
static ParameterInfo t5776_m30188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30188_GM;
MethodInfo m30188_MI = 
{
	"set_Item", NULL, &t5776_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5776_m30188_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30188_GM};
static MethodInfo* t5776_MIs[] =
{
	&m30189_MI,
	&m30190_MI,
	&m30191_MI,
	&m30187_MI,
	&m30188_MI,
	NULL
};
static TypeInfo* t5776_ITIs[] = 
{
	&t603_TI,
	&t5775_TI,
	&t5777_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5776_0_0_0;
extern Il2CppType t5776_1_0_0;
struct t5776;
extern Il2CppGenericClass t5776_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5776_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5776_MIs, t5776_PIs, NULL, NULL, NULL, NULL, NULL, &t5776_TI, t5776_ITIs, NULL, &t1908__CustomAttributeCache, &t5776_TI, &t5776_0_0_0, &t5776_1_0_0, NULL, &t5776_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3112_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextEditor/TextEditOp>
extern MethodInfo m30192_MI;
static PropertyInfo t3112____Current_PropertyInfo = 
{
	&t3112_TI, "Current", &m30192_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3112_PIs[] =
{
	&t3112____Current_PropertyInfo,
	NULL
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30192_GM;
MethodInfo m30192_MI = 
{
	"get_Current", NULL, &t3112_TI, &t552_0_0_0, RuntimeInvoker_t552, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30192_GM};
static MethodInfo* t3112_MIs[] =
{
	&m30192_MI,
	NULL
};
static TypeInfo* t3112_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3112_0_0_0;
extern Il2CppType t3112_1_0_0;
struct t3112;
extern Il2CppGenericClass t3112_GC;
TypeInfo t3112_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3112_MIs, t3112_PIs, NULL, NULL, NULL, NULL, NULL, &t3112_TI, t3112_ITIs, NULL, &EmptyCustomAttributesCache, &t3112_TI, &t3112_0_0_0, &t3112_1_0_0, NULL, &t3112_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3111.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3111_TI;
#include "t3111MD.h"

extern MethodInfo m17178_MI;
extern MethodInfo m22999_MI;
struct t20;
 int32_t m22999 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17174_MI;
 void m17174 (t3111 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17175_MI;
 t29 * m17175 (t3111 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17178(__this, &m17178_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t552_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17176_MI;
 void m17176 (t3111 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17177_MI;
 bool m17177 (t3111 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17178 (t3111 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22999(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22999_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t20_0_0_1;
FieldInfo t3111_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3111_TI, offsetof(t3111, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3111_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3111_TI, offsetof(t3111, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3111_FIs[] =
{
	&t3111_f0_FieldInfo,
	&t3111_f1_FieldInfo,
	NULL
};
static PropertyInfo t3111____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3111_TI, "System.Collections.IEnumerator.Current", &m17175_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3111____Current_PropertyInfo = 
{
	&t3111_TI, "Current", &m17178_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3111_PIs[] =
{
	&t3111____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3111____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3111_m17174_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17174_GM;
MethodInfo m17174_MI = 
{
	".ctor", (methodPointerType)&m17174, &t3111_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3111_m17174_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17174_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17175_GM;
MethodInfo m17175_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17175, &t3111_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17175_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17176_GM;
MethodInfo m17176_MI = 
{
	"Dispose", (methodPointerType)&m17176, &t3111_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17176_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17177_GM;
MethodInfo m17177_MI = 
{
	"MoveNext", (methodPointerType)&m17177, &t3111_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17177_GM};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17178_GM;
MethodInfo m17178_MI = 
{
	"get_Current", (methodPointerType)&m17178, &t3111_TI, &t552_0_0_0, RuntimeInvoker_t552, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17178_GM};
static MethodInfo* t3111_MIs[] =
{
	&m17174_MI,
	&m17175_MI,
	&m17176_MI,
	&m17177_MI,
	&m17178_MI,
	NULL
};
static MethodInfo* t3111_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17175_MI,
	&m17177_MI,
	&m17176_MI,
	&m17178_MI,
};
static TypeInfo* t3111_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3112_TI,
};
static Il2CppInterfaceOffsetPair t3111_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3112_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3111_0_0_0;
extern Il2CppType t3111_1_0_0;
extern Il2CppGenericClass t3111_GC;
TypeInfo t3111_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3111_MIs, t3111_PIs, t3111_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3111_TI, t3111_ITIs, t3111_VT, &EmptyCustomAttributesCache, &t3111_TI, &t3111_0_0_0, &t3111_1_0_0, t3111_IOs, &t3111_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3111)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5778_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
extern MethodInfo m30193_MI;
static PropertyInfo t5778____Count_PropertyInfo = 
{
	&t5778_TI, "Count", &m30193_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30194_MI;
static PropertyInfo t5778____IsReadOnly_PropertyInfo = 
{
	&t5778_TI, "IsReadOnly", &m30194_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5778_PIs[] =
{
	&t5778____Count_PropertyInfo,
	&t5778____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30193_GM;
MethodInfo m30193_MI = 
{
	"get_Count", NULL, &t5778_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30193_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30194_GM;
MethodInfo m30194_MI = 
{
	"get_IsReadOnly", NULL, &t5778_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30194_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t5778_m30195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30195_GM;
MethodInfo m30195_MI = 
{
	"Add", NULL, &t5778_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5778_m30195_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30195_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30196_GM;
MethodInfo m30196_MI = 
{
	"Clear", NULL, &t5778_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30196_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t5778_m30197_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30197_GM;
MethodInfo m30197_MI = 
{
	"Contains", NULL, &t5778_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5778_m30197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30197_GM};
extern Il2CppType t3101_0_0_0;
extern Il2CppType t3101_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5778_m30198_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3101_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30198_GM;
MethodInfo m30198_MI = 
{
	"CopyTo", NULL, &t5778_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5778_m30198_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30198_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t5778_m30199_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30199_GM;
MethodInfo m30199_MI = 
{
	"Remove", NULL, &t5778_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5778_m30199_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30199_GM};
static MethodInfo* t5778_MIs[] =
{
	&m30193_MI,
	&m30194_MI,
	&m30195_MI,
	&m30196_MI,
	&m30197_MI,
	&m30198_MI,
	&m30199_MI,
	NULL
};
extern TypeInfo t5780_TI;
static TypeInfo* t5778_ITIs[] = 
{
	&t603_TI,
	&t5780_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5778_0_0_0;
extern Il2CppType t5778_1_0_0;
struct t5778;
extern Il2CppGenericClass t5778_GC;
TypeInfo t5778_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5778_MIs, t5778_PIs, NULL, NULL, NULL, NULL, NULL, &t5778_TI, t5778_ITIs, NULL, &EmptyCustomAttributesCache, &t5778_TI, &t5778_0_0_0, &t5778_1_0_0, NULL, &t5778_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t3112_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30200_GM;
MethodInfo m30200_MI = 
{
	"GetEnumerator", NULL, &t5780_TI, &t3112_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30200_GM};
static MethodInfo* t5780_MIs[] =
{
	&m30200_MI,
	NULL
};
static TypeInfo* t5780_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5780_0_0_0;
extern Il2CppType t5780_1_0_0;
struct t5780;
extern Il2CppGenericClass t5780_GC;
TypeInfo t5780_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5780_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5780_TI, t5780_ITIs, NULL, &EmptyCustomAttributesCache, &t5780_TI, &t5780_0_0_0, &t5780_1_0_0, NULL, &t5780_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5779_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextEditor/TextEditOp>
extern MethodInfo m30201_MI;
extern MethodInfo m30202_MI;
static PropertyInfo t5779____Item_PropertyInfo = 
{
	&t5779_TI, "Item", &m30201_MI, &m30202_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5779_PIs[] =
{
	&t5779____Item_PropertyInfo,
	NULL
};
extern Il2CppType t552_0_0_0;
static ParameterInfo t5779_m30203_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30203_GM;
MethodInfo m30203_MI = 
{
	"IndexOf", NULL, &t5779_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5779_m30203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30203_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t5779_m30204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30204_GM;
MethodInfo m30204_MI = 
{
	"Insert", NULL, &t5779_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5779_m30204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30204_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5779_m30205_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30205_GM;
MethodInfo m30205_MI = 
{
	"RemoveAt", NULL, &t5779_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5779_m30205_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30205_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5779_m30201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30201_GM;
MethodInfo m30201_MI = 
{
	"get_Item", NULL, &t5779_TI, &t552_0_0_0, RuntimeInvoker_t552_t44, t5779_m30201_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30201_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t5779_m30202_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30202_GM;
MethodInfo m30202_MI = 
{
	"set_Item", NULL, &t5779_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5779_m30202_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30202_GM};
static MethodInfo* t5779_MIs[] =
{
	&m30203_MI,
	&m30204_MI,
	&m30205_MI,
	&m30201_MI,
	&m30202_MI,
	NULL
};
static TypeInfo* t5779_ITIs[] = 
{
	&t603_TI,
	&t5778_TI,
	&t5780_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5779_0_0_0;
extern Il2CppType t5779_1_0_0;
struct t5779;
extern Il2CppGenericClass t5779_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5779_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5779_MIs, t5779_PIs, NULL, NULL, NULL, NULL, NULL, &t5779_TI, t5779_ITIs, NULL, &t1908__CustomAttributeCache, &t5779_TI, &t5779_0_0_0, &t5779_1_0_0, NULL, &t5779_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
#include "t3113.h"
#include "t3114.h"
extern TypeInfo t345_TI;
extern TypeInfo t3113_TI;
extern TypeInfo t3114_TI;
#include "t345MD.h"
#include "t3114MD.h"
#include "t3113MD.h"
extern MethodInfo m9816_MI;
extern MethodInfo m3988_MI;
extern MethodInfo m17191_MI;
extern MethodInfo m17190_MI;
extern MethodInfo m17210_MI;
extern MethodInfo m23010_MI;
extern MethodInfo m23011_MI;
extern MethodInfo m17193_MI;
struct t553;
 void m23010 (t553 * __this, t20 * p0, int32_t p1, t3114 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t553;
 void m23011 (t553 * __this, t3101* p0, int32_t p1, t3114 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m17179 (t3104 * __this, t553 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m17180_MI;
 void m17180 (t3104 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m17181_MI;
 void m17181 (t3104 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m17182_MI;
 bool m17182 (t3104 * __this, int32_t p0, MethodInfo* method){
	{
		t553 * L_0 = (__this->f0);
		bool L_1 = m17147(L_0, p0, &m17147_MI);
		return L_1;
	}
}
extern MethodInfo m17183_MI;
 bool m17183 (t3104 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m17184_MI;
 t29* m17184 (t3104 * __this, MethodInfo* method){
	{
		t3113  L_0 = m17191(__this, &m17191_MI);
		t3113  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3113_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m17185_MI;
 void m17185 (t3104 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t3101* V_0 = {0};
	{
		V_0 = ((t3101*)IsInst(p0, InitializedTypeInfo(&t3101_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t3101*, int32_t >::Invoke(&m17190_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t553 * L_0 = (__this->f0);
		m17139(L_0, p0, p1, &m17139_MI);
		t553 * L_1 = (__this->f0);
		t35 L_2 = { &m17141_MI };
		t3114 * L_3 = (t3114 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3114_TI));
		m17210(L_3, NULL, L_2, &m17210_MI);
		m23010(L_1, p0, p1, L_3, &m23010_MI);
		return;
	}
}
extern MethodInfo m17186_MI;
 t29 * m17186 (t3104 * __this, MethodInfo* method){
	{
		t3113  L_0 = m17191(__this, &m17191_MI);
		t3113  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3113_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m17187_MI;
 bool m17187 (t3104 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m17188_MI;
 bool m17188 (t3104 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m17189_MI;
 t29 * m17189 (t3104 * __this, MethodInfo* method){
	{
		t553 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m17190 (t3104 * __this, t3101* p0, int32_t p1, MethodInfo* method){
	{
		t553 * L_0 = (__this->f0);
		m17139(L_0, (t20 *)(t20 *)p0, p1, &m17139_MI);
		t553 * L_1 = (__this->f0);
		t35 L_2 = { &m17141_MI };
		t3114 * L_3 = (t3114 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3114_TI));
		m17210(L_3, NULL, L_2, &m17210_MI);
		m23011(L_1, p0, p1, L_3, &m23011_MI);
		return;
	}
}
 t3113  m17191 (t3104 * __this, MethodInfo* method){
	{
		t553 * L_0 = (__this->f0);
		t3113  L_1 = {0};
		m17193(&L_1, L_0, &m17193_MI);
		return L_1;
	}
}
extern MethodInfo m17192_MI;
 int32_t m17192 (t3104 * __this, MethodInfo* method){
	{
		t553 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m17134_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t553_0_0_1;
FieldInfo t3104_f0_FieldInfo = 
{
	"dictionary", &t553_0_0_1, &t3104_TI, offsetof(t3104, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3104_FIs[] =
{
	&t3104_f0_FieldInfo,
	NULL
};
static PropertyInfo t3104____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t3104_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m17187_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3104____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3104_TI, "System.Collections.ICollection.IsSynchronized", &m17188_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3104____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3104_TI, "System.Collections.ICollection.SyncRoot", &m17189_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3104____Count_PropertyInfo = 
{
	&t3104_TI, "Count", &m17192_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3104_PIs[] =
{
	&t3104____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t3104____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3104____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3104____Count_PropertyInfo,
	NULL
};
extern Il2CppType t553_0_0_0;
static ParameterInfo t3104_m17179_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t553_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17179_GM;
MethodInfo m17179_MI = 
{
	".ctor", (methodPointerType)&m17179, &t3104_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3104_m17179_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17179_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t3104_m17180_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17180_GM;
MethodInfo m17180_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m17180, &t3104_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3104_m17180_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17180_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17181_GM;
MethodInfo m17181_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m17181, &t3104_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17181_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t3104_m17182_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17182_GM;
MethodInfo m17182_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m17182, &t3104_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t3104_m17182_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17182_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t3104_m17183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17183_GM;
MethodInfo m17183_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m17183, &t3104_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t3104_m17183_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17183_GM};
extern Il2CppType t3112_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17184_GM;
MethodInfo m17184_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m17184, &t3104_TI, &t3112_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17184_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3104_m17185_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17185_GM;
MethodInfo m17185_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m17185, &t3104_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3104_m17185_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17185_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17186_GM;
MethodInfo m17186_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m17186, &t3104_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17186_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17187_GM;
MethodInfo m17187_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m17187, &t3104_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17187_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17188_GM;
MethodInfo m17188_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m17188, &t3104_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17188_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17189_GM;
MethodInfo m17189_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m17189, &t3104_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17189_GM};
extern Il2CppType t3101_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3104_m17190_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3101_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17190_GM;
MethodInfo m17190_MI = 
{
	"CopyTo", (methodPointerType)&m17190, &t3104_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3104_m17190_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17190_GM};
extern Il2CppType t3113_0_0_0;
extern void* RuntimeInvoker_t3113 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17191_GM;
MethodInfo m17191_MI = 
{
	"GetEnumerator", (methodPointerType)&m17191, &t3104_TI, &t3113_0_0_0, RuntimeInvoker_t3113, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17191_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17192_GM;
MethodInfo m17192_MI = 
{
	"get_Count", (methodPointerType)&m17192, &t3104_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17192_GM};
static MethodInfo* t3104_MIs[] =
{
	&m17179_MI,
	&m17180_MI,
	&m17181_MI,
	&m17182_MI,
	&m17183_MI,
	&m17184_MI,
	&m17185_MI,
	&m17186_MI,
	&m17187_MI,
	&m17188_MI,
	&m17189_MI,
	&m17190_MI,
	&m17191_MI,
	&m17192_MI,
	NULL
};
static MethodInfo* t3104_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17186_MI,
	&m17192_MI,
	&m17188_MI,
	&m17189_MI,
	&m17185_MI,
	&m17192_MI,
	&m17187_MI,
	&m17180_MI,
	&m17181_MI,
	&m17182_MI,
	&m17190_MI,
	&m17183_MI,
	&m17184_MI,
};
static TypeInfo* t3104_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5778_TI,
	&t5780_TI,
};
static Il2CppInterfaceOffsetPair t3104_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5778_TI, 9},
	{ &t5780_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3104_0_0_0;
extern Il2CppType t3104_1_0_0;
struct t3104;
extern Il2CppGenericClass t3104_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t3104_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t3104_MIs, t3104_PIs, t3104_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t3104_TI, t3104_ITIs, t3104_VT, &t1252__CustomAttributeCache, &t3104_TI, &t3104_0_0_0, &t3104_1_0_0, t3104_IOs, &t3104_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3104), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17206_MI;
extern MethodInfo m17209_MI;
extern MethodInfo m17203_MI;


 void m17193 (t3113 * __this, t553 * p0, MethodInfo* method){
	{
		t3108  L_0 = m17156(p0, &m17156_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m17194_MI;
 t29 * m17194 (t3113 * __this, MethodInfo* method){
	{
		t3108 * L_0 = &(__this->f0);
		int32_t L_1 = m17206(L_0, &m17206_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t552_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m17195_MI;
 void m17195 (t3113 * __this, MethodInfo* method){
	{
		t3108 * L_0 = &(__this->f0);
		m17209(L_0, &m17209_MI);
		return;
	}
}
extern MethodInfo m17196_MI;
 bool m17196 (t3113 * __this, MethodInfo* method){
	{
		t3108 * L_0 = &(__this->f0);
		bool L_1 = m17203(L_0, &m17203_MI);
		return L_1;
	}
}
extern MethodInfo m17197_MI;
 int32_t m17197 (t3113 * __this, MethodInfo* method){
	{
		t3108 * L_0 = &(__this->f0);
		t3106 * L_1 = &(L_0->f3);
		int32_t L_2 = m17161(L_1, &m17161_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t3108_0_0_1;
FieldInfo t3113_f0_FieldInfo = 
{
	"host_enumerator", &t3108_0_0_1, &t3113_TI, offsetof(t3113, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3113_FIs[] =
{
	&t3113_f0_FieldInfo,
	NULL
};
static PropertyInfo t3113____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3113_TI, "System.Collections.IEnumerator.Current", &m17194_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3113____Current_PropertyInfo = 
{
	&t3113_TI, "Current", &m17197_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3113_PIs[] =
{
	&t3113____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3113____Current_PropertyInfo,
	NULL
};
extern Il2CppType t553_0_0_0;
static ParameterInfo t3113_m17193_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t553_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17193_GM;
MethodInfo m17193_MI = 
{
	".ctor", (methodPointerType)&m17193, &t3113_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3113_m17193_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17193_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17194_GM;
MethodInfo m17194_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17194, &t3113_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17194_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17195_GM;
MethodInfo m17195_MI = 
{
	"Dispose", (methodPointerType)&m17195, &t3113_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17195_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17196_GM;
MethodInfo m17196_MI = 
{
	"MoveNext", (methodPointerType)&m17196, &t3113_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17196_GM};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17197_GM;
MethodInfo m17197_MI = 
{
	"get_Current", (methodPointerType)&m17197, &t3113_TI, &t552_0_0_0, RuntimeInvoker_t552, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17197_GM};
static MethodInfo* t3113_MIs[] =
{
	&m17193_MI,
	&m17194_MI,
	&m17195_MI,
	&m17196_MI,
	&m17197_MI,
	NULL
};
static MethodInfo* t3113_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17194_MI,
	&m17196_MI,
	&m17195_MI,
	&m17197_MI,
};
static TypeInfo* t3113_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3112_TI,
};
static Il2CppInterfaceOffsetPair t3113_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3112_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3113_0_0_0;
extern Il2CppType t3113_1_0_0;
extern Il2CppGenericClass t3113_GC;
extern TypeInfo t1252_TI;
TypeInfo t3113_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3113_MIs, t3113_PIs, t3113_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t3113_TI, t3113_ITIs, t3113_VT, &EmptyCustomAttributesCache, &t3113_TI, &t3113_0_0_0, &t3113_1_0_0, t3113_IOs, &t3113_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3113)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m17208_MI;
extern MethodInfo m17205_MI;
extern MethodInfo m17207_MI;
extern MethodInfo m5150_MI;


 void m17198 (t3108 * __this, t553 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m17199_MI;
 t29 * m17199 (t3108 * __this, MethodInfo* method){
	{
		m17208(__this, &m17208_MI);
		t3106  L_0 = (__this->f3);
		t3106  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3106_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17200_MI;
 t725  m17200 (t3108 * __this, MethodInfo* method){
	{
		m17208(__this, &m17208_MI);
		t3106 * L_0 = &(__this->f3);
		t204 * L_1 = m17159(L_0, &m17159_MI);
		t204 * L_2 = L_1;
		t3106 * L_3 = &(__this->f3);
		int32_t L_4 = m17161(L_3, &m17161_MI);
		int32_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t552_TI), &L_5);
		t725  L_7 = {0};
		m3965(&L_7, ((t204 *)L_2), L_6, &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m17201_MI;
 t29 * m17201 (t3108 * __this, MethodInfo* method){
	{
		t204 * L_0 = m17205(__this, &m17205_MI);
		t204 * L_1 = L_0;
		return ((t204 *)L_1);
	}
}
extern MethodInfo m17202_MI;
 t29 * m17202 (t3108 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17206(__this, &m17206_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t552_TI), &L_1);
		return L_2;
	}
}
 bool m17203 (t3108 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m17207(__this, &m17207_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t553 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t553 * L_6 = (__this->f0);
		t3100* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t553 * L_9 = (__this->f0);
		t3101* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t3106  L_12 = {0};
		m17158(&L_12, (*(t204 **)(t204 **)SZArrayLdElema(L_7, L_8)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_11)), &m17158_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t553 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m17204_MI;
 t3106  m17204 (t3108 * __this, MethodInfo* method){
	{
		t3106  L_0 = (__this->f3);
		return L_0;
	}
}
 t204 * m17205 (t3108 * __this, MethodInfo* method){
	{
		m17208(__this, &m17208_MI);
		t3106 * L_0 = &(__this->f3);
		t204 * L_1 = m17159(L_0, &m17159_MI);
		return L_1;
	}
}
 int32_t m17206 (t3108 * __this, MethodInfo* method){
	{
		m17208(__this, &m17208_MI);
		t3106 * L_0 = &(__this->f3);
		int32_t L_1 = m17161(L_0, &m17161_MI);
		return L_1;
	}
}
 void m17207 (t3108 * __this, MethodInfo* method){
	{
		t553 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t553 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m17208 (t3108 * __this, MethodInfo* method){
	{
		m17207(__this, &m17207_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m17209 (t3108 * __this, MethodInfo* method){
	{
		__this->f0 = (t553 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t553_0_0_1;
FieldInfo t3108_f0_FieldInfo = 
{
	"dictionary", &t553_0_0_1, &t3108_TI, offsetof(t3108, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3108_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3108_TI, offsetof(t3108, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3108_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t3108_TI, offsetof(t3108, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t3106_0_0_3;
FieldInfo t3108_f3_FieldInfo = 
{
	"current", &t3106_0_0_3, &t3108_TI, offsetof(t3108, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3108_FIs[] =
{
	&t3108_f0_FieldInfo,
	&t3108_f1_FieldInfo,
	&t3108_f2_FieldInfo,
	&t3108_f3_FieldInfo,
	NULL
};
static PropertyInfo t3108____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3108_TI, "System.Collections.IEnumerator.Current", &m17199_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3108____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t3108_TI, "System.Collections.IDictionaryEnumerator.Entry", &m17200_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3108____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t3108_TI, "System.Collections.IDictionaryEnumerator.Key", &m17201_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3108____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t3108_TI, "System.Collections.IDictionaryEnumerator.Value", &m17202_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3108____Current_PropertyInfo = 
{
	&t3108_TI, "Current", &m17204_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3108____CurrentKey_PropertyInfo = 
{
	&t3108_TI, "CurrentKey", &m17205_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3108____CurrentValue_PropertyInfo = 
{
	&t3108_TI, "CurrentValue", &m17206_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3108_PIs[] =
{
	&t3108____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3108____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t3108____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t3108____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t3108____Current_PropertyInfo,
	&t3108____CurrentKey_PropertyInfo,
	&t3108____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t553_0_0_0;
static ParameterInfo t3108_m17198_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t553_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17198_GM;
MethodInfo m17198_MI = 
{
	".ctor", (methodPointerType)&m17198, &t3108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3108_m17198_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17198_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17199_GM;
MethodInfo m17199_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17199, &t3108_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17199_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17200_GM;
MethodInfo m17200_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m17200, &t3108_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17200_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17201_GM;
MethodInfo m17201_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m17201, &t3108_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17201_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17202_GM;
MethodInfo m17202_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m17202, &t3108_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17202_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17203_GM;
MethodInfo m17203_MI = 
{
	"MoveNext", (methodPointerType)&m17203, &t3108_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17203_GM};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17204_GM;
MethodInfo m17204_MI = 
{
	"get_Current", (methodPointerType)&m17204, &t3108_TI, &t3106_0_0_0, RuntimeInvoker_t3106, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17204_GM};
extern Il2CppType t204_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17205_GM;
MethodInfo m17205_MI = 
{
	"get_CurrentKey", (methodPointerType)&m17205, &t3108_TI, &t204_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17205_GM};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17206_GM;
MethodInfo m17206_MI = 
{
	"get_CurrentValue", (methodPointerType)&m17206, &t3108_TI, &t552_0_0_0, RuntimeInvoker_t552, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17206_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17207_GM;
MethodInfo m17207_MI = 
{
	"VerifyState", (methodPointerType)&m17207, &t3108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17207_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17208_GM;
MethodInfo m17208_MI = 
{
	"VerifyCurrent", (methodPointerType)&m17208, &t3108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17208_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17209_GM;
MethodInfo m17209_MI = 
{
	"Dispose", (methodPointerType)&m17209, &t3108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17209_GM};
static MethodInfo* t3108_MIs[] =
{
	&m17198_MI,
	&m17199_MI,
	&m17200_MI,
	&m17201_MI,
	&m17202_MI,
	&m17203_MI,
	&m17204_MI,
	&m17205_MI,
	&m17206_MI,
	&m17207_MI,
	&m17208_MI,
	&m17209_MI,
	NULL
};
static MethodInfo* t3108_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17199_MI,
	&m17203_MI,
	&m17209_MI,
	&m17204_MI,
	&m17200_MI,
	&m17201_MI,
	&m17202_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t3108_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3107_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t3108_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3107_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3108_0_0_0;
extern Il2CppType t3108_1_0_0;
extern Il2CppGenericClass t3108_GC;
TypeInfo t3108_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3108_MIs, t3108_PIs, t3108_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t3108_TI, t3108_ITIs, t3108_VT, &EmptyCustomAttributesCache, &t3108_TI, &t3108_0_0_0, &t3108_1_0_0, t3108_IOs, &t3108_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3108)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m17210 (t3114 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17211_MI;
 int32_t m17211 (t3114 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17211((t3114 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t204 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17212_MI;
 t29 * m17212 (t3114 * __this, t204 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t552_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17213_MI;
 int32_t m17213 (t3114 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3114_m17210_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17210_GM;
MethodInfo m17210_MI = 
{
	".ctor", (methodPointerType)&m17210, &t3114_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3114_m17210_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17210_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t3114_m17211_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17211_GM;
MethodInfo m17211_MI = 
{
	"Invoke", (methodPointerType)&m17211, &t3114_TI, &t552_0_0_0, RuntimeInvoker_t552_t29_t44, t3114_m17211_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17211_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3114_m17212_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17212_GM;
MethodInfo m17212_MI = 
{
	"BeginInvoke", (methodPointerType)&m17212, &t3114_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t3114_m17212_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17212_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3114_m17213_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t552_0_0_0;
extern void* RuntimeInvoker_t552_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17213_GM;
MethodInfo m17213_MI = 
{
	"EndInvoke", (methodPointerType)&m17213, &t3114_TI, &t552_0_0_0, RuntimeInvoker_t552_t29, t3114_m17213_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17213_GM};
static MethodInfo* t3114_MIs[] =
{
	&m17210_MI,
	&m17211_MI,
	&m17212_MI,
	&m17213_MI,
	NULL
};
static MethodInfo* t3114_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17211_MI,
	&m17212_MI,
	&m17213_MI,
};
static Il2CppInterfaceOffsetPair t3114_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3114_0_0_0;
extern Il2CppType t3114_1_0_0;
struct t3114;
extern Il2CppGenericClass t3114_GC;
TypeInfo t3114_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t3114_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t3114_TI, NULL, t3114_VT, &EmptyCustomAttributesCache, &t3114_TI, &t3114_0_0_0, &t3114_1_0_0, t3114_IOs, &t3114_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3114), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m17214 (t3103 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17215_MI;
 t725  m17215 (t3103 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17215((t3103 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t204 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17216_MI;
 t29 * m17216 (t3103 * __this, t204 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t552_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17217_MI;
 t725  m17217 (t3103 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3103_m17214_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17214_GM;
MethodInfo m17214_MI = 
{
	".ctor", (methodPointerType)&m17214, &t3103_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3103_m17214_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17214_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t3103_m17215_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17215_GM;
MethodInfo m17215_MI = 
{
	"Invoke", (methodPointerType)&m17215, &t3103_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t3103_m17215_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17215_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3103_m17216_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17216_GM;
MethodInfo m17216_MI = 
{
	"BeginInvoke", (methodPointerType)&m17216, &t3103_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t3103_m17216_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17216_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3103_m17217_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17217_GM;
MethodInfo m17217_MI = 
{
	"EndInvoke", (methodPointerType)&m17217, &t3103_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t3103_m17217_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17217_GM};
static MethodInfo* t3103_MIs[] =
{
	&m17214_MI,
	&m17215_MI,
	&m17216_MI,
	&m17217_MI,
	NULL
};
static MethodInfo* t3103_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17215_MI,
	&m17216_MI,
	&m17217_MI,
};
static Il2CppInterfaceOffsetPair t3103_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3103_0_0_0;
extern Il2CppType t3103_1_0_0;
struct t3103;
extern Il2CppGenericClass t3103_GC;
TypeInfo t3103_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t3103_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t3103_TI, NULL, t3103_VT, &EmptyCustomAttributesCache, &t3103_TI, &t3103_0_0_0, &t3103_1_0_0, t3103_IOs, &t3103_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3103), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m17218 (t3115 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17219_MI;
 t3106  m17219 (t3115 * __this, t204 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17219((t3115 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t3106  (*FunctionPointerType) (t29 *, t29 * __this, t204 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t3106  (*FunctionPointerType) (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t3106  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17220_MI;
 t29 * m17220 (t3115 * __this, t204 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t552_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17221_MI;
 t3106  m17221 (t3115 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t3106 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3115_m17218_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17218_GM;
MethodInfo m17218_MI = 
{
	".ctor", (methodPointerType)&m17218, &t3115_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3115_m17218_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17218_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t3115_m17219_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17219_GM;
MethodInfo m17219_MI = 
{
	"Invoke", (methodPointerType)&m17219, &t3115_TI, &t3106_0_0_0, RuntimeInvoker_t3106_t29_t44, t3115_m17219_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17219_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t552_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3115_m17220_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17220_GM;
MethodInfo m17220_MI = 
{
	"BeginInvoke", (methodPointerType)&m17220, &t3115_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t3115_m17220_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17220_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3115_m17221_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t3106_0_0_0;
extern void* RuntimeInvoker_t3106_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17221_GM;
MethodInfo m17221_MI = 
{
	"EndInvoke", (methodPointerType)&m17221, &t3115_TI, &t3106_0_0_0, RuntimeInvoker_t3106_t29, t3115_m17221_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17221_GM};
static MethodInfo* t3115_MIs[] =
{
	&m17218_MI,
	&m17219_MI,
	&m17220_MI,
	&m17221_MI,
	NULL
};
static MethodInfo* t3115_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17219_MI,
	&m17220_MI,
	&m17221_MI,
};
static Il2CppInterfaceOffsetPair t3115_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3115_0_0_0;
extern Il2CppType t3115_1_0_0;
struct t3115;
extern Il2CppGenericClass t3115_GC;
TypeInfo t3115_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t3115_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t3115_TI, NULL, t3115_VT, &EmptyCustomAttributesCache, &t3115_TI, &t3115_0_0_0, &t3115_1_0_0, t3115_IOs, &t3115_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3115), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10122_MI;
extern MethodInfo m17224_MI;


 void m17222 (t3116 * __this, t553 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t3108  L_0 = m17156(p0, &m17156_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m17223_MI;
 bool m17223 (t3116 * __this, MethodInfo* method){
	{
		t3108 * L_0 = &(__this->f0);
		bool L_1 = m17203(L_0, &m17203_MI);
		return L_1;
	}
}
 t725  m17224 (t3116 * __this, MethodInfo* method){
	{
		t3108  L_0 = (__this->f0);
		t3108  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3108_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m17225_MI;
 t29 * m17225 (t3116 * __this, MethodInfo* method){
	t3106  V_0 = {0};
	{
		t3108 * L_0 = &(__this->f0);
		t3106  L_1 = m17204(L_0, &m17204_MI);
		V_0 = L_1;
		t204 * L_2 = m17159((&V_0), &m17159_MI);
		t204 * L_3 = L_2;
		return ((t204 *)L_3);
	}
}
extern MethodInfo m17226_MI;
 t29 * m17226 (t3116 * __this, MethodInfo* method){
	t3106  V_0 = {0};
	{
		t3108 * L_0 = &(__this->f0);
		t3106  L_1 = m17204(L_0, &m17204_MI);
		V_0 = L_1;
		int32_t L_2 = m17161((&V_0), &m17161_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t552_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m17227_MI;
 t29 * m17227 (t3116 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m17224_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t3108_0_0_1;
FieldInfo t3116_f0_FieldInfo = 
{
	"host_enumerator", &t3108_0_0_1, &t3116_TI, offsetof(t3116, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3116_FIs[] =
{
	&t3116_f0_FieldInfo,
	NULL
};
static PropertyInfo t3116____Entry_PropertyInfo = 
{
	&t3116_TI, "Entry", &m17224_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3116____Key_PropertyInfo = 
{
	&t3116_TI, "Key", &m17225_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3116____Value_PropertyInfo = 
{
	&t3116_TI, "Value", &m17226_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3116____Current_PropertyInfo = 
{
	&t3116_TI, "Current", &m17227_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3116_PIs[] =
{
	&t3116____Entry_PropertyInfo,
	&t3116____Key_PropertyInfo,
	&t3116____Value_PropertyInfo,
	&t3116____Current_PropertyInfo,
	NULL
};
extern Il2CppType t553_0_0_0;
static ParameterInfo t3116_m17222_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t553_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17222_GM;
MethodInfo m17222_MI = 
{
	".ctor", (methodPointerType)&m17222, &t3116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3116_m17222_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17222_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17223_GM;
MethodInfo m17223_MI = 
{
	"MoveNext", (methodPointerType)&m17223, &t3116_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17223_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17224_GM;
MethodInfo m17224_MI = 
{
	"get_Entry", (methodPointerType)&m17224, &t3116_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17224_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17225_GM;
MethodInfo m17225_MI = 
{
	"get_Key", (methodPointerType)&m17225, &t3116_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17225_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17226_GM;
MethodInfo m17226_MI = 
{
	"get_Value", (methodPointerType)&m17226, &t3116_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17226_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17227_GM;
MethodInfo m17227_MI = 
{
	"get_Current", (methodPointerType)&m17227, &t3116_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17227_GM};
static MethodInfo* t3116_MIs[] =
{
	&m17222_MI,
	&m17223_MI,
	&m17224_MI,
	&m17225_MI,
	&m17226_MI,
	&m17227_MI,
	NULL
};
static MethodInfo* t3116_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17227_MI,
	&m17223_MI,
	&m17224_MI,
	&m17225_MI,
	&m17226_MI,
};
static TypeInfo* t3116_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t3116_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3116_0_0_0;
extern Il2CppType t3116_1_0_0;
struct t3116;
extern Il2CppGenericClass t3116_GC;
TypeInfo t3116_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t3116_MIs, t3116_PIs, t3116_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t3116_TI, t3116_ITIs, t3116_VT, &EmptyCustomAttributesCache, &t3116_TI, &t3116_0_0_0, &t3116_1_0_0, t3116_IOs, &t3116_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3116), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1257.h"
#include "t3118.h"
extern TypeInfo t6749_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3118_TI;
#include "t931MD.h"
#include "t3118MD.h"
extern Il2CppType t6749_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m17233_MI;
extern MethodInfo m30206_MI;
extern MethodInfo m30207_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Event>
extern Il2CppType t3117_0_0_49;
FieldInfo t3117_f0_FieldInfo = 
{
	"_default", &t3117_0_0_49, &t3117_TI, offsetof(t3117_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3117_FIs[] =
{
	&t3117_f0_FieldInfo,
	NULL
};
static PropertyInfo t3117____Default_PropertyInfo = 
{
	&t3117_TI, "Default", &m17232_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3117_PIs[] =
{
	&t3117____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17228_GM;
MethodInfo m17228_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t3117_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17228_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17229_GM;
MethodInfo m17229_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t3117_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17229_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3117_m17230_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17230_GM;
MethodInfo m17230_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t3117_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3117_m17230_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17230_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3117_m17231_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17231_GM;
MethodInfo m17231_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t3117_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3117_m17231_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17231_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t3117_m30206_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30206_GM;
MethodInfo m30206_MI = 
{
	"GetHashCode", NULL, &t3117_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3117_m30206_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30206_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t204_0_0_0;
static ParameterInfo t3117_m30207_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30207_GM;
MethodInfo m30207_MI = 
{
	"Equals", NULL, &t3117_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3117_m30207_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30207_GM};
extern Il2CppType t3117_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17232_GM;
MethodInfo m17232_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t3117_TI, &t3117_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17232_GM};
static MethodInfo* t3117_MIs[] =
{
	&m17228_MI,
	&m17229_MI,
	&m17230_MI,
	&m17231_MI,
	&m30206_MI,
	&m30207_MI,
	&m17232_MI,
	NULL
};
extern MethodInfo m17231_MI;
extern MethodInfo m17230_MI;
static MethodInfo* t3117_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m30207_MI,
	&m30206_MI,
	&m17231_MI,
	&m17230_MI,
	NULL,
	NULL,
};
extern TypeInfo t734_TI;
static TypeInfo* t3117_ITIs[] = 
{
	&t3102_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3117_IOs[] = 
{
	{ &t3102_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3117_TI;
extern TypeInfo t3117_TI;
extern TypeInfo t3118_TI;
extern TypeInfo t204_TI;
static Il2CppRGCTXData t3117_RGCTXData[9] = 
{
	&t6749_0_0_0/* Type Usage */,
	&t204_0_0_0/* Type Usage */,
	&t3117_TI/* Class Usage */,
	&t3117_TI/* Static Usage */,
	&t3118_TI/* Class Usage */,
	&m17233_MI/* Method Usage */,
	&t204_TI/* Class Usage */,
	&m30206_MI/* Method Usage */,
	&m30207_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3117_0_0_0;
extern Il2CppType t3117_1_0_0;
struct t3117;
extern Il2CppGenericClass t3117_GC;
TypeInfo t3117_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3117_MIs, t3117_PIs, t3117_FIs, NULL, &t29_TI, NULL, NULL, &t3117_TI, t3117_ITIs, t3117_VT, &EmptyCustomAttributesCache, &t3117_TI, &t3117_0_0_0, &t3117_1_0_0, t3117_IOs, &t3117_GC, NULL, NULL, NULL, t3117_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3117), 0, -1, sizeof(t3117_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Event>
extern Il2CppType t204_0_0_0;
static ParameterInfo t6749_m30208_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30208_GM;
MethodInfo m30208_MI = 
{
	"Equals", NULL, &t6749_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6749_m30208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30208_GM};
static MethodInfo* t6749_MIs[] =
{
	&m30208_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6749_1_0_0;
struct t6749;
extern Il2CppGenericClass t6749_GC;
TypeInfo t6749_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6749_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6749_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6749_TI, &t6749_0_0_0, &t6749_1_0_0, NULL, &t6749_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17228_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Event>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17233_GM;
MethodInfo m17233_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t3118_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17233_GM};
extern Il2CppType t204_0_0_0;
static ParameterInfo t3118_m17234_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17234_GM;
MethodInfo m17234_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t3118_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3118_m17234_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17234_GM};
extern Il2CppType t204_0_0_0;
extern Il2CppType t204_0_0_0;
static ParameterInfo t3118_m17235_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t204_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17235_GM;
MethodInfo m17235_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t3118_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3118_m17235_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17235_GM};
static MethodInfo* t3118_MIs[] =
{
	&m17233_MI,
	&m17234_MI,
	&m17235_MI,
	NULL
};
extern MethodInfo m17235_MI;
extern MethodInfo m17234_MI;
static MethodInfo* t3118_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17235_MI,
	&m17234_MI,
	&m17231_MI,
	&m17230_MI,
	&m17234_MI,
	&m17235_MI,
};
static Il2CppInterfaceOffsetPair t3118_IOs[] = 
{
	{ &t3102_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3117_TI;
extern TypeInfo t3117_TI;
extern TypeInfo t3118_TI;
extern TypeInfo t204_TI;
extern TypeInfo t204_TI;
static Il2CppRGCTXData t3118_RGCTXData[11] = 
{
	&t6749_0_0_0/* Type Usage */,
	&t204_0_0_0/* Type Usage */,
	&t3117_TI/* Class Usage */,
	&t3117_TI/* Static Usage */,
	&t3118_TI/* Class Usage */,
	&m17233_MI/* Method Usage */,
	&t204_TI/* Class Usage */,
	&m30206_MI/* Method Usage */,
	&m30207_MI/* Method Usage */,
	&m17228_MI/* Method Usage */,
	&t204_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3118_0_0_0;
extern Il2CppType t3118_1_0_0;
struct t3118;
extern Il2CppGenericClass t3118_GC;
extern TypeInfo t1256_TI;
TypeInfo t3118_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3118_MIs, NULL, NULL, NULL, &t3117_TI, NULL, &t1256_TI, &t3118_TI, NULL, t3118_VT, &EmptyCustomAttributesCache, &t3118_TI, &t3118_0_0_0, &t3118_1_0_0, t3118_IOs, &t3118_GC, NULL, NULL, NULL, t3118_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3118), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t552_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t6742_m30157_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30157_GM;
MethodInfo m30157_MI = 
{
	"Equals", NULL, &t6742_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t6742_m30157_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30157_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t6742_m30209_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30209_GM;
MethodInfo m30209_MI = 
{
	"GetHashCode", NULL, &t6742_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6742_m30209_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30209_GM};
static MethodInfo* t6742_MIs[] =
{
	&m30157_MI,
	&m30209_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6742_0_0_0;
extern Il2CppType t6742_1_0_0;
struct t6742;
extern Il2CppGenericClass t6742_GC;
TypeInfo t6742_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6742_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6742_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6742_TI, &t6742_0_0_0, &t6742_1_0_0, NULL, &t6742_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3120.h"
extern TypeInfo t6750_TI;
extern TypeInfo t3120_TI;
#include "t3120MD.h"
extern Il2CppType t6750_0_0_0;
extern MethodInfo m17241_MI;
extern MethodInfo m30210_MI;


extern MethodInfo m17236_MI;
 void m17236 (t3119 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m17237_MI;
 void m17237 (t29 * __this, MethodInfo* method){
	t3120 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3120 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3120_TI));
	m17241(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m17241_MI);
	((t3119_SFs*)InitializedTypeInfo(&t3119_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m17238_MI;
 int32_t m17238 (t3119 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(&m30210_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t552_TI))))));
		return L_0;
	}
}
extern MethodInfo m17239_MI;
 bool m17239 (t3119 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m30158_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t552_TI))))), ((*(int32_t*)((int32_t*)UnBox (p1, InitializedTypeInfo(&t552_TI))))));
		return L_0;
	}
}
 t3119 * m17240 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3119_TI));
		return (((t3119_SFs*)InitializedTypeInfo(&t3119_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t3119_0_0_49;
FieldInfo t3119_f0_FieldInfo = 
{
	"_default", &t3119_0_0_49, &t3119_TI, offsetof(t3119_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3119_FIs[] =
{
	&t3119_f0_FieldInfo,
	NULL
};
static PropertyInfo t3119____Default_PropertyInfo = 
{
	&t3119_TI, "Default", &m17240_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3119_PIs[] =
{
	&t3119____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17236_GM;
MethodInfo m17236_MI = 
{
	".ctor", (methodPointerType)&m17236, &t3119_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17236_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17237_GM;
MethodInfo m17237_MI = 
{
	".cctor", (methodPointerType)&m17237, &t3119_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17237_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3119_m17238_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17238_GM;
MethodInfo m17238_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m17238, &t3119_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3119_m17238_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17238_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3119_m17239_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17239_GM;
MethodInfo m17239_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m17239, &t3119_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3119_m17239_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17239_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t3119_m30210_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30210_GM;
MethodInfo m30210_MI = 
{
	"GetHashCode", NULL, &t3119_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t3119_m30210_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30210_GM};
extern Il2CppType t552_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t3119_m30158_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30158_GM;
MethodInfo m30158_MI = 
{
	"Equals", NULL, &t3119_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t3119_m30158_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30158_GM};
extern Il2CppType t3119_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17240_GM;
MethodInfo m17240_MI = 
{
	"get_Default", (methodPointerType)&m17240, &t3119_TI, &t3119_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17240_GM};
static MethodInfo* t3119_MIs[] =
{
	&m17236_MI,
	&m17237_MI,
	&m17238_MI,
	&m17239_MI,
	&m30210_MI,
	&m30158_MI,
	&m17240_MI,
	NULL
};
static MethodInfo* t3119_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m30158_MI,
	&m30210_MI,
	&m17239_MI,
	&m17238_MI,
	NULL,
	NULL,
};
static TypeInfo* t3119_ITIs[] = 
{
	&t6742_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3119_IOs[] = 
{
	{ &t6742_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3119_0_0_0;
extern Il2CppType t3119_1_0_0;
struct t3119;
extern Il2CppGenericClass t3119_GC;
TypeInfo t3119_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3119_MIs, t3119_PIs, t3119_FIs, NULL, &t29_TI, NULL, NULL, &t3119_TI, t3119_ITIs, t3119_VT, &EmptyCustomAttributesCache, &t3119_TI, &t3119_0_0_0, &t3119_1_0_0, t3119_IOs, &t3119_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3119), 0, -1, sizeof(t3119_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t552_0_0_0;
static ParameterInfo t6750_m30211_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30211_GM;
MethodInfo m30211_MI = 
{
	"Equals", NULL, &t6750_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6750_m30211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30211_GM};
static MethodInfo* t6750_MIs[] =
{
	&m30211_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6750_1_0_0;
struct t6750;
extern Il2CppGenericClass t6750_GC;
TypeInfo t6750_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6750_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6750_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6750_TI, &t6750_0_0_0, &t6750_1_0_0, NULL, &t6750_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m17241 (t3120 * __this, MethodInfo* method){
	{
		m17236(__this, &m17236_MI);
		return;
	}
}
extern MethodInfo m17242_MI;
 int32_t m17242 (t3120 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t552_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t552_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m17243_MI;
 bool m17243 (t3120 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t552_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t552_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		int32_t L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t552_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t552_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17241_GM;
MethodInfo m17241_MI = 
{
	".ctor", (methodPointerType)&m17241, &t3120_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17241_GM};
extern Il2CppType t552_0_0_0;
static ParameterInfo t3120_m17242_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17242_GM;
MethodInfo m17242_MI = 
{
	"GetHashCode", (methodPointerType)&m17242, &t3120_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t3120_m17242_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17242_GM};
extern Il2CppType t552_0_0_0;
extern Il2CppType t552_0_0_0;
static ParameterInfo t3120_m17243_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t552_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17243_GM;
MethodInfo m17243_MI = 
{
	"Equals", (methodPointerType)&m17243, &t3120_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t3120_m17243_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17243_GM};
static MethodInfo* t3120_MIs[] =
{
	&m17241_MI,
	&m17242_MI,
	&m17243_MI,
	NULL
};
static MethodInfo* t3120_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17243_MI,
	&m17242_MI,
	&m17239_MI,
	&m17238_MI,
	&m17242_MI,
	&m17243_MI,
};
static Il2CppInterfaceOffsetPair t3120_IOs[] = 
{
	{ &t6742_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3120_0_0_0;
extern Il2CppType t3120_1_0_0;
struct t3120;
extern Il2CppGenericClass t3120_GC;
TypeInfo t3120_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3120_MIs, NULL, NULL, NULL, &t3119_TI, NULL, &t1256_TI, &t3120_TI, NULL, t3120_VT, &EmptyCustomAttributesCache, &t3120_TI, &t3120_0_0_0, &t3120_1_0_0, t3120_IOs, &t3120_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3120), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4499_TI;

#include "t551.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextEditor/DblClickSnapping>
extern MethodInfo m30212_MI;
static PropertyInfo t4499____Current_PropertyInfo = 
{
	&t4499_TI, "Current", &m30212_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4499_PIs[] =
{
	&t4499____Current_PropertyInfo,
	NULL
};
extern Il2CppType t551_0_0_0;
extern void* RuntimeInvoker_t551 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30212_GM;
MethodInfo m30212_MI = 
{
	"get_Current", NULL, &t4499_TI, &t551_0_0_0, RuntimeInvoker_t551, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30212_GM};
static MethodInfo* t4499_MIs[] =
{
	&m30212_MI,
	NULL
};
static TypeInfo* t4499_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4499_0_0_0;
extern Il2CppType t4499_1_0_0;
struct t4499;
extern Il2CppGenericClass t4499_GC;
TypeInfo t4499_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4499_MIs, t4499_PIs, NULL, NULL, NULL, NULL, NULL, &t4499_TI, t4499_ITIs, NULL, &EmptyCustomAttributesCache, &t4499_TI, &t4499_0_0_0, &t4499_1_0_0, NULL, &t4499_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3121.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3121_TI;
#include "t3121MD.h"

extern TypeInfo t551_TI;
extern MethodInfo m17248_MI;
extern MethodInfo m23017_MI;
struct t20;
 uint8_t m23017 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17244_MI;
 void m17244 (t3121 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17245_MI;
 t29 * m17245 (t3121 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m17248(__this, &m17248_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t551_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17246_MI;
 void m17246 (t3121 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17247_MI;
 bool m17247 (t3121 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m17248 (t3121 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m23017(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23017_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextEditor/DblClickSnapping>
extern Il2CppType t20_0_0_1;
FieldInfo t3121_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3121_TI, offsetof(t3121, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3121_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3121_TI, offsetof(t3121, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3121_FIs[] =
{
	&t3121_f0_FieldInfo,
	&t3121_f1_FieldInfo,
	NULL
};
static PropertyInfo t3121____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3121_TI, "System.Collections.IEnumerator.Current", &m17245_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3121____Current_PropertyInfo = 
{
	&t3121_TI, "Current", &m17248_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3121_PIs[] =
{
	&t3121____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3121____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3121_m17244_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17244_GM;
MethodInfo m17244_MI = 
{
	".ctor", (methodPointerType)&m17244, &t3121_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3121_m17244_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17244_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17245_GM;
MethodInfo m17245_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17245, &t3121_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17245_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17246_GM;
MethodInfo m17246_MI = 
{
	"Dispose", (methodPointerType)&m17246, &t3121_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17246_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17247_GM;
MethodInfo m17247_MI = 
{
	"MoveNext", (methodPointerType)&m17247, &t3121_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17247_GM};
extern Il2CppType t551_0_0_0;
extern void* RuntimeInvoker_t551 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17248_GM;
MethodInfo m17248_MI = 
{
	"get_Current", (methodPointerType)&m17248, &t3121_TI, &t551_0_0_0, RuntimeInvoker_t551, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17248_GM};
static MethodInfo* t3121_MIs[] =
{
	&m17244_MI,
	&m17245_MI,
	&m17246_MI,
	&m17247_MI,
	&m17248_MI,
	NULL
};
static MethodInfo* t3121_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17245_MI,
	&m17247_MI,
	&m17246_MI,
	&m17248_MI,
};
static TypeInfo* t3121_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4499_TI,
};
static Il2CppInterfaceOffsetPair t3121_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4499_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3121_0_0_0;
extern Il2CppType t3121_1_0_0;
extern Il2CppGenericClass t3121_GC;
TypeInfo t3121_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3121_MIs, t3121_PIs, t3121_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3121_TI, t3121_ITIs, t3121_VT, &EmptyCustomAttributesCache, &t3121_TI, &t3121_0_0_0, &t3121_1_0_0, t3121_IOs, &t3121_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3121)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5781_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/DblClickSnapping>
extern MethodInfo m30213_MI;
static PropertyInfo t5781____Count_PropertyInfo = 
{
	&t5781_TI, "Count", &m30213_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30214_MI;
static PropertyInfo t5781____IsReadOnly_PropertyInfo = 
{
	&t5781_TI, "IsReadOnly", &m30214_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5781_PIs[] =
{
	&t5781____Count_PropertyInfo,
	&t5781____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30213_GM;
MethodInfo m30213_MI = 
{
	"get_Count", NULL, &t5781_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30213_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30214_GM;
MethodInfo m30214_MI = 
{
	"get_IsReadOnly", NULL, &t5781_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30214_GM};
extern Il2CppType t551_0_0_0;
extern Il2CppType t551_0_0_0;
static ParameterInfo t5781_m30215_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t551_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30215_GM;
MethodInfo m30215_MI = 
{
	"Add", NULL, &t5781_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t5781_m30215_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30215_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30216_GM;
MethodInfo m30216_MI = 
{
	"Clear", NULL, &t5781_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30216_GM};
extern Il2CppType t551_0_0_0;
static ParameterInfo t5781_m30217_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t551_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30217_GM;
MethodInfo m30217_MI = 
{
	"Contains", NULL, &t5781_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5781_m30217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30217_GM};
extern Il2CppType t3789_0_0_0;
extern Il2CppType t3789_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5781_m30218_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3789_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30218_GM;
MethodInfo m30218_MI = 
{
	"CopyTo", NULL, &t5781_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5781_m30218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30218_GM};
extern Il2CppType t551_0_0_0;
static ParameterInfo t5781_m30219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t551_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30219_GM;
MethodInfo m30219_MI = 
{
	"Remove", NULL, &t5781_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5781_m30219_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30219_GM};
static MethodInfo* t5781_MIs[] =
{
	&m30213_MI,
	&m30214_MI,
	&m30215_MI,
	&m30216_MI,
	&m30217_MI,
	&m30218_MI,
	&m30219_MI,
	NULL
};
extern TypeInfo t5783_TI;
static TypeInfo* t5781_ITIs[] = 
{
	&t603_TI,
	&t5783_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5781_0_0_0;
extern Il2CppType t5781_1_0_0;
struct t5781;
extern Il2CppGenericClass t5781_GC;
TypeInfo t5781_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5781_MIs, t5781_PIs, NULL, NULL, NULL, NULL, NULL, &t5781_TI, t5781_ITIs, NULL, &EmptyCustomAttributesCache, &t5781_TI, &t5781_0_0_0, &t5781_1_0_0, NULL, &t5781_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextEditor/DblClickSnapping>
extern Il2CppType t4499_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30220_GM;
MethodInfo m30220_MI = 
{
	"GetEnumerator", NULL, &t5783_TI, &t4499_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30220_GM};
static MethodInfo* t5783_MIs[] =
{
	&m30220_MI,
	NULL
};
static TypeInfo* t5783_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5783_0_0_0;
extern Il2CppType t5783_1_0_0;
struct t5783;
extern Il2CppGenericClass t5783_GC;
TypeInfo t5783_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5783_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5783_TI, t5783_ITIs, NULL, &EmptyCustomAttributesCache, &t5783_TI, &t5783_0_0_0, &t5783_1_0_0, NULL, &t5783_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5782_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextEditor/DblClickSnapping>
extern MethodInfo m30221_MI;
extern MethodInfo m30222_MI;
static PropertyInfo t5782____Item_PropertyInfo = 
{
	&t5782_TI, "Item", &m30221_MI, &m30222_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5782_PIs[] =
{
	&t5782____Item_PropertyInfo,
	NULL
};
extern Il2CppType t551_0_0_0;
static ParameterInfo t5782_m30223_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t551_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30223_GM;
MethodInfo m30223_MI = 
{
	"IndexOf", NULL, &t5782_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t5782_m30223_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30223_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t551_0_0_0;
static ParameterInfo t5782_m30224_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t551_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30224_GM;
MethodInfo m30224_MI = 
{
	"Insert", NULL, &t5782_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5782_m30224_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30224_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5782_m30225_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30225_GM;
MethodInfo m30225_MI = 
{
	"RemoveAt", NULL, &t5782_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5782_m30225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30225_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5782_m30221_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t551_0_0_0;
extern void* RuntimeInvoker_t551_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30221_GM;
MethodInfo m30221_MI = 
{
	"get_Item", NULL, &t5782_TI, &t551_0_0_0, RuntimeInvoker_t551_t44, t5782_m30221_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30221_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t551_0_0_0;
static ParameterInfo t5782_m30222_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t551_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30222_GM;
MethodInfo m30222_MI = 
{
	"set_Item", NULL, &t5782_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5782_m30222_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30222_GM};
static MethodInfo* t5782_MIs[] =
{
	&m30223_MI,
	&m30224_MI,
	&m30225_MI,
	&m30221_MI,
	&m30222_MI,
	NULL
};
static TypeInfo* t5782_ITIs[] = 
{
	&t603_TI,
	&t5781_TI,
	&t5783_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5782_0_0_0;
extern Il2CppType t5782_1_0_0;
struct t5782;
extern Il2CppGenericClass t5782_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5782_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5782_MIs, t5782_PIs, NULL, NULL, NULL, NULL, NULL, &t5782_TI, t5782_ITIs, NULL, &t1908__CustomAttributeCache, &t5782_TI, &t5782_0_0_0, &t5782_1_0_0, NULL, &t5782_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4501_TI;

#include "t554.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Events.PersistentListenerMode>
extern MethodInfo m30226_MI;
static PropertyInfo t4501____Current_PropertyInfo = 
{
	&t4501_TI, "Current", &m30226_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4501_PIs[] =
{
	&t4501____Current_PropertyInfo,
	NULL
};
extern Il2CppType t554_0_0_0;
extern void* RuntimeInvoker_t554 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30226_GM;
MethodInfo m30226_MI = 
{
	"get_Current", NULL, &t4501_TI, &t554_0_0_0, RuntimeInvoker_t554, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30226_GM};
static MethodInfo* t4501_MIs[] =
{
	&m30226_MI,
	NULL
};
static TypeInfo* t4501_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4501_0_0_0;
extern Il2CppType t4501_1_0_0;
struct t4501;
extern Il2CppGenericClass t4501_GC;
TypeInfo t4501_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4501_MIs, t4501_PIs, NULL, NULL, NULL, NULL, NULL, &t4501_TI, t4501_ITIs, NULL, &EmptyCustomAttributesCache, &t4501_TI, &t4501_0_0_0, &t4501_1_0_0, NULL, &t4501_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3122.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3122_TI;
#include "t3122MD.h"

extern TypeInfo t554_TI;
extern MethodInfo m17253_MI;
extern MethodInfo m23028_MI;
struct t20;
 int32_t m23028 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17249_MI;
 void m17249 (t3122 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17250_MI;
 t29 * m17250 (t3122 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17253(__this, &m17253_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t554_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17251_MI;
 void m17251 (t3122 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17252_MI;
 bool m17252 (t3122 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17253 (t3122 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23028(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23028_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3122_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3122_TI, offsetof(t3122, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3122_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3122_TI, offsetof(t3122, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3122_FIs[] =
{
	&t3122_f0_FieldInfo,
	&t3122_f1_FieldInfo,
	NULL
};
static PropertyInfo t3122____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3122_TI, "System.Collections.IEnumerator.Current", &m17250_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3122____Current_PropertyInfo = 
{
	&t3122_TI, "Current", &m17253_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3122_PIs[] =
{
	&t3122____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3122____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3122_m17249_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17249_GM;
MethodInfo m17249_MI = 
{
	".ctor", (methodPointerType)&m17249, &t3122_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3122_m17249_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17249_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17250_GM;
MethodInfo m17250_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17250, &t3122_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17250_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17251_GM;
MethodInfo m17251_MI = 
{
	"Dispose", (methodPointerType)&m17251, &t3122_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17251_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17252_GM;
MethodInfo m17252_MI = 
{
	"MoveNext", (methodPointerType)&m17252, &t3122_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17252_GM};
extern Il2CppType t554_0_0_0;
extern void* RuntimeInvoker_t554 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17253_GM;
MethodInfo m17253_MI = 
{
	"get_Current", (methodPointerType)&m17253, &t3122_TI, &t554_0_0_0, RuntimeInvoker_t554, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17253_GM};
static MethodInfo* t3122_MIs[] =
{
	&m17249_MI,
	&m17250_MI,
	&m17251_MI,
	&m17252_MI,
	&m17253_MI,
	NULL
};
static MethodInfo* t3122_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17250_MI,
	&m17252_MI,
	&m17251_MI,
	&m17253_MI,
};
static TypeInfo* t3122_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4501_TI,
};
static Il2CppInterfaceOffsetPair t3122_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4501_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3122_0_0_0;
extern Il2CppType t3122_1_0_0;
extern Il2CppGenericClass t3122_GC;
TypeInfo t3122_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3122_MIs, t3122_PIs, t3122_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3122_TI, t3122_ITIs, t3122_VT, &EmptyCustomAttributesCache, &t3122_TI, &t3122_0_0_0, &t3122_1_0_0, t3122_IOs, &t3122_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3122)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5784_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Events.PersistentListenerMode>
extern MethodInfo m30227_MI;
static PropertyInfo t5784____Count_PropertyInfo = 
{
	&t5784_TI, "Count", &m30227_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30228_MI;
static PropertyInfo t5784____IsReadOnly_PropertyInfo = 
{
	&t5784_TI, "IsReadOnly", &m30228_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5784_PIs[] =
{
	&t5784____Count_PropertyInfo,
	&t5784____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30227_GM;
MethodInfo m30227_MI = 
{
	"get_Count", NULL, &t5784_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30227_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30228_GM;
MethodInfo m30228_MI = 
{
	"get_IsReadOnly", NULL, &t5784_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30228_GM};
extern Il2CppType t554_0_0_0;
extern Il2CppType t554_0_0_0;
static ParameterInfo t5784_m30229_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t554_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30229_GM;
MethodInfo m30229_MI = 
{
	"Add", NULL, &t5784_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5784_m30229_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30229_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30230_GM;
MethodInfo m30230_MI = 
{
	"Clear", NULL, &t5784_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30230_GM};
extern Il2CppType t554_0_0_0;
static ParameterInfo t5784_m30231_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t554_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30231_GM;
MethodInfo m30231_MI = 
{
	"Contains", NULL, &t5784_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5784_m30231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30231_GM};
extern Il2CppType t3790_0_0_0;
extern Il2CppType t3790_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5784_m30232_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3790_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30232_GM;
MethodInfo m30232_MI = 
{
	"CopyTo", NULL, &t5784_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5784_m30232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30232_GM};
extern Il2CppType t554_0_0_0;
static ParameterInfo t5784_m30233_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t554_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30233_GM;
MethodInfo m30233_MI = 
{
	"Remove", NULL, &t5784_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5784_m30233_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30233_GM};
static MethodInfo* t5784_MIs[] =
{
	&m30227_MI,
	&m30228_MI,
	&m30229_MI,
	&m30230_MI,
	&m30231_MI,
	&m30232_MI,
	&m30233_MI,
	NULL
};
extern TypeInfo t5786_TI;
static TypeInfo* t5784_ITIs[] = 
{
	&t603_TI,
	&t5786_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5784_0_0_0;
extern Il2CppType t5784_1_0_0;
struct t5784;
extern Il2CppGenericClass t5784_GC;
TypeInfo t5784_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5784_MIs, t5784_PIs, NULL, NULL, NULL, NULL, NULL, &t5784_TI, t5784_ITIs, NULL, &EmptyCustomAttributesCache, &t5784_TI, &t5784_0_0_0, &t5784_1_0_0, NULL, &t5784_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Events.PersistentListenerMode>
extern Il2CppType t4501_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30234_GM;
MethodInfo m30234_MI = 
{
	"GetEnumerator", NULL, &t5786_TI, &t4501_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30234_GM};
static MethodInfo* t5786_MIs[] =
{
	&m30234_MI,
	NULL
};
static TypeInfo* t5786_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5786_0_0_0;
extern Il2CppType t5786_1_0_0;
struct t5786;
extern Il2CppGenericClass t5786_GC;
TypeInfo t5786_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5786_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5786_TI, t5786_ITIs, NULL, &EmptyCustomAttributesCache, &t5786_TI, &t5786_0_0_0, &t5786_1_0_0, NULL, &t5786_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5785_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Events.PersistentListenerMode>
extern MethodInfo m30235_MI;
extern MethodInfo m30236_MI;
static PropertyInfo t5785____Item_PropertyInfo = 
{
	&t5785_TI, "Item", &m30235_MI, &m30236_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5785_PIs[] =
{
	&t5785____Item_PropertyInfo,
	NULL
};
extern Il2CppType t554_0_0_0;
static ParameterInfo t5785_m30237_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t554_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30237_GM;
MethodInfo m30237_MI = 
{
	"IndexOf", NULL, &t5785_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5785_m30237_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30237_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t554_0_0_0;
static ParameterInfo t5785_m30238_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t554_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30238_GM;
MethodInfo m30238_MI = 
{
	"Insert", NULL, &t5785_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5785_m30238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30238_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5785_m30239_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30239_GM;
MethodInfo m30239_MI = 
{
	"RemoveAt", NULL, &t5785_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5785_m30239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30239_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5785_m30235_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t554_0_0_0;
extern void* RuntimeInvoker_t554_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30235_GM;
MethodInfo m30235_MI = 
{
	"get_Item", NULL, &t5785_TI, &t554_0_0_0, RuntimeInvoker_t554_t44, t5785_m30235_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30235_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t554_0_0_0;
static ParameterInfo t5785_m30236_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t554_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30236_GM;
MethodInfo m30236_MI = 
{
	"set_Item", NULL, &t5785_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5785_m30236_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30236_GM};
static MethodInfo* t5785_MIs[] =
{
	&m30237_MI,
	&m30238_MI,
	&m30239_MI,
	&m30235_MI,
	&m30236_MI,
	NULL
};
static TypeInfo* t5785_ITIs[] = 
{
	&t603_TI,
	&t5784_TI,
	&t5786_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5785_0_0_0;
extern Il2CppType t5785_1_0_0;
struct t5785;
extern Il2CppGenericClass t5785_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5785_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5785_MIs, t5785_PIs, NULL, NULL, NULL, NULL, NULL, &t5785_TI, t5785_ITIs, NULL, &t1908__CustomAttributeCache, &t5785_TI, &t5785_0_0_0, &t5785_1_0_0, NULL, &t5785_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3123.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3123_TI;
#include "t3123MD.h"

#include "t3124.h"
extern TypeInfo t3124_TI;
#include "t3124MD.h"
extern Il2CppType t3124_0_0_0;
extern MethodInfo m19555_MI;
extern MethodInfo m17258_MI;


extern MethodInfo m17254_MI;
 void m17254_gshared (t3123 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		m2790(__this, p0, p1, &m2790_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		t353 * L_1 = m2957(NULL, L_0, p0, p1, &m2957_MI);
		__this->f0 = ((t3124 *)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
extern MethodInfo m17255_MI;
 void m17255_gshared (t3123 * __this, t316* p0, MethodInfo* method)
{
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_2 = 1;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_2)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		t3124 * L_3 = (__this->f0);
		bool L_4 = m2791(NULL, L_3, &m2791_MI);
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		t3124 * L_5 = (__this->f0);
		int32_t L_6 = 0;
		int32_t L_7 = 1;
		VirtActionInvoker2< t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), L_5, ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_6)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))), ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_7)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5))));
	}

IL_004f:
	{
		return;
	}
}
extern MethodInfo m17256_MI;
 bool m17256_gshared (t3123 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		t3124 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t3124 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
extern Il2CppType t3124_0_0_1;
FieldInfo t3123_f0_FieldInfo = 
{
	"Delegate", &t3124_0_0_1, &t3123_TI, offsetof(t3123, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3123_FIs[] =
{
	&t3123_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3123_m17254_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17254_GM;
MethodInfo m17254_MI = 
{
	".ctor", (methodPointerType)&m17254_gshared, &t3123_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3123_m17254_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17254_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3123_m17255_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17255_GM;
MethodInfo m17255_MI = 
{
	"Invoke", (methodPointerType)&m17255_gshared, &t3123_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3123_m17255_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17255_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3123_m17256_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17256_GM;
MethodInfo m17256_MI = 
{
	"Find", (methodPointerType)&m17256_gshared, &t3123_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3123_m17256_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17256_GM};
static MethodInfo* t3123_MIs[] =
{
	&m17254_MI,
	&m17255_MI,
	&m17256_MI,
	NULL
};
static MethodInfo* t3123_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17255_MI,
	&m17256_MI,
};
extern TypeInfo t3124_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t3123_RGCTXData[7] = 
{
	&t3124_0_0_0/* Type Usage */,
	&t3124_TI/* Class Usage */,
	&m19555_MI/* Method Usage */,
	&m19555_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&m17258_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3123_0_0_0;
extern Il2CppType t3123_1_0_0;
struct t3123;
extern Il2CppGenericClass t3123_GC;
TypeInfo t3123_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`2", "UnityEngine.Events", t3123_MIs, NULL, t3123_FIs, NULL, &t556_TI, NULL, NULL, &t3123_TI, NULL, t3123_VT, &EmptyCustomAttributesCache, &t3123_TI, &t3123_0_0_0, &t3123_1_0_0, NULL, &t3123_GC, NULL, NULL, NULL, t3123_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3123), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m17257_MI;
 void m17257_gshared (t3124 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 void m17258_gshared (t3124 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m17258((t3124 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17259_MI;
 t29 * m17259_gshared (t3124 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17260_MI;
 void m17260_gshared (t3124 * __this, t29 * p0, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`2<System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3124_m17257_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17257_GM;
MethodInfo m17257_MI = 
{
	".ctor", (methodPointerType)&m17257_gshared, &t3124_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3124_m17257_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17257_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3124_m17258_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg1", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17258_GM;
MethodInfo m17258_MI = 
{
	"Invoke", (methodPointerType)&m17258_gshared, &t3124_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3124_m17258_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17258_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3124_m17259_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg1", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17259_GM;
MethodInfo m17259_MI = 
{
	"BeginInvoke", (methodPointerType)&m17259_gshared, &t3124_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t3124_m17259_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17259_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3124_m17260_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17260_GM;
MethodInfo m17260_MI = 
{
	"EndInvoke", (methodPointerType)&m17260_gshared, &t3124_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3124_m17260_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17260_GM};
static MethodInfo* t3124_MIs[] =
{
	&m17257_MI,
	&m17258_MI,
	&m17259_MI,
	&m17260_MI,
	NULL
};
static MethodInfo* t3124_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17258_MI,
	&m17259_MI,
	&m17260_MI,
};
static Il2CppInterfaceOffsetPair t3124_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3124_1_0_0;
struct t3124;
extern Il2CppGenericClass t3124_GC;
TypeInfo t3124_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`2", "UnityEngine.Events", t3124_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3124_TI, NULL, t3124_VT, &EmptyCustomAttributesCache, &t3124_TI, &t3124_0_0_0, &t3124_1_0_0, t3124_IOs, &t3124_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3124), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3125.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3125_TI;
#include "t3125MD.h"

#include "t3126.h"
extern TypeInfo t3126_TI;
#include "t3126MD.h"
extern Il2CppType t3126_0_0_0;
extern MethodInfo m17265_MI;


extern MethodInfo m17261_MI;
 void m17261_gshared (t3125 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		m2790(__this, p0, p1, &m2790_MI);
		t3126 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t3126 *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), &m1597_MI);
		__this->f0 = ((t3126 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
extern MethodInfo m17262_MI;
 void m17262_gshared (t3125 * __this, t316* p0, MethodInfo* method)
{
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)3)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_2 = 1;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_2)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_3 = 2;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_3)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		t3126 * L_4 = (__this->f0);
		bool L_5 = m2791(NULL, L_4, &m2791_MI);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		t3126 * L_6 = (__this->f0);
		int32_t L_7 = 0;
		int32_t L_8 = 1;
		int32_t L_9 = 2;
		VirtActionInvoker3< t29 *, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), L_6, ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_7)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5))), ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_8)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))), ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_9)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7))));
	}

IL_005f:
	{
		return;
	}
}
extern MethodInfo m17263_MI;
 bool m17263_gshared (t3125 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		t3126 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t3126 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
extern Il2CppType t3126_0_0_1;
FieldInfo t3125_f0_FieldInfo = 
{
	"Delegate", &t3126_0_0_1, &t3125_TI, offsetof(t3125, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3125_FIs[] =
{
	&t3125_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3125_m17261_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17261_GM;
MethodInfo m17261_MI = 
{
	".ctor", (methodPointerType)&m17261_gshared, &t3125_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3125_m17261_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17261_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3125_m17262_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17262_GM;
MethodInfo m17262_MI = 
{
	"Invoke", (methodPointerType)&m17262_gshared, &t3125_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3125_m17262_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17262_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3125_m17263_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17263_GM;
MethodInfo m17263_MI = 
{
	"Find", (methodPointerType)&m17263_gshared, &t3125_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3125_m17263_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17263_GM};
static MethodInfo* t3125_MIs[] =
{
	&m17261_MI,
	&m17262_MI,
	&m17263_MI,
	NULL
};
static MethodInfo* t3125_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17262_MI,
	&m17263_MI,
};
extern TypeInfo t3126_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t3125_RGCTXData[9] = 
{
	&t3126_0_0_0/* Type Usage */,
	&t3126_TI/* Class Usage */,
	&m19555_MI/* Method Usage */,
	&m19555_MI/* Method Usage */,
	&m19555_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&m17265_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3125_0_0_0;
extern Il2CppType t3125_1_0_0;
struct t3125;
extern Il2CppGenericClass t3125_GC;
TypeInfo t3125_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`3", "UnityEngine.Events", t3125_MIs, NULL, t3125_FIs, NULL, &t556_TI, NULL, NULL, &t3125_TI, NULL, t3125_VT, &EmptyCustomAttributesCache, &t3125_TI, &t3125_0_0_0, &t3125_1_0_0, NULL, &t3125_GC, NULL, NULL, NULL, t3125_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3125), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m17264_MI;
 void m17264_gshared (t3126 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 void m17265_gshared (t3126 * __this, t29 * p0, t29 * p1, t29 * p2, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m17265((t3126 *)__this->f9,p0, p1, p2, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, t29 * p2, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1, p2,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, t29 * p2, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1, p2,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p1, t29 * p2, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1, p2,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17266_MI;
 t29 * m17266_gshared (t3126 * __this, t29 * p0, t29 * p1, t29 * p2, t67 * p3, t29 * p4, MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	__d_args[2] = p2;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p3, (Il2CppObject*)p4);
}
extern MethodInfo m17267_MI;
 void m17267_gshared (t3126 * __this, t29 * p0, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3126_m17264_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17264_GM;
MethodInfo m17264_MI = 
{
	".ctor", (methodPointerType)&m17264_gshared, &t3126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3126_m17264_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17264_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3126_m17265_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg1", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg2", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17265_GM;
MethodInfo m17265_MI = 
{
	"Invoke", (methodPointerType)&m17265_gshared, &t3126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3126_m17265_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17265_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3126_m17266_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg1", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg2", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 3, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 4, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17266_GM;
MethodInfo m17266_MI = 
{
	"BeginInvoke", (methodPointerType)&m17266_gshared, &t3126_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29_t29, t3126_m17266_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 5, false, true, 0, NULL, (methodPointerType)NULL, &m17266_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3126_m17267_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17267_GM;
MethodInfo m17267_MI = 
{
	"EndInvoke", (methodPointerType)&m17267_gshared, &t3126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3126_m17267_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17267_GM};
static MethodInfo* t3126_MIs[] =
{
	&m17264_MI,
	&m17265_MI,
	&m17266_MI,
	&m17267_MI,
	NULL
};
static MethodInfo* t3126_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17265_MI,
	&m17266_MI,
	&m17267_MI,
};
static Il2CppInterfaceOffsetPair t3126_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3126_1_0_0;
struct t3126;
extern Il2CppGenericClass t3126_GC;
TypeInfo t3126_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`3", "UnityEngine.Events", t3126_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3126_TI, NULL, t3126_VT, &EmptyCustomAttributesCache, &t3126_TI, &t3126_0_0_0, &t3126_1_0_0, t3126_IOs, &t3126_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3126), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3127.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3127_TI;
#include "t3127MD.h"

#include "t3128.h"
extern TypeInfo t3128_TI;
#include "t3128MD.h"
extern Il2CppType t3128_0_0_0;
extern MethodInfo m17272_MI;


extern MethodInfo m17268_MI;
 void m17268_gshared (t3127 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		m2790(__this, p0, p1, &m2790_MI);
		t3128 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t3128 *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), &m1597_MI);
		__this->f0 = ((t3128 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
extern MethodInfo m17269_MI;
 void m17269_gshared (t3127 * __this, t316* p0, MethodInfo* method)
{
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_2 = 1;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_2)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_3 = 2;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_3)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_4 = 3;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_4)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		t3128 * L_5 = (__this->f0);
		bool L_6 = m2791(NULL, L_5, &m2791_MI);
		if (!L_6)
		{
			goto IL_006f;
		}
	}
	{
		t3128 * L_7 = (__this->f0);
		int32_t L_8 = 0;
		int32_t L_9 = 1;
		int32_t L_10 = 2;
		int32_t L_11 = 3;
		VirtActionInvoker4< t29 *, t29 *, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), L_7, ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_8)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))), ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_9)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7))), ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_10)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))), ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_11)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9))));
	}

IL_006f:
	{
		return;
	}
}
extern MethodInfo m17270_MI;
 bool m17270_gshared (t3127 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		t3128 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t3128 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
extern Il2CppType t3128_0_0_1;
FieldInfo t3127_f0_FieldInfo = 
{
	"Delegate", &t3128_0_0_1, &t3127_TI, offsetof(t3127, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3127_FIs[] =
{
	&t3127_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3127_m17268_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17268_GM;
MethodInfo m17268_MI = 
{
	".ctor", (methodPointerType)&m17268_gshared, &t3127_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3127_m17268_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17268_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3127_m17269_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17269_GM;
MethodInfo m17269_MI = 
{
	"Invoke", (methodPointerType)&m17269_gshared, &t3127_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3127_m17269_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17269_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3127_m17270_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17270_GM;
MethodInfo m17270_MI = 
{
	"Find", (methodPointerType)&m17270_gshared, &t3127_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3127_m17270_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17270_GM};
static MethodInfo* t3127_MIs[] =
{
	&m17268_MI,
	&m17269_MI,
	&m17270_MI,
	NULL
};
static MethodInfo* t3127_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17269_MI,
	&m17270_MI,
};
extern TypeInfo t3128_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t3127_RGCTXData[11] = 
{
	&t3128_0_0_0/* Type Usage */,
	&t3128_TI/* Class Usage */,
	&m19555_MI/* Method Usage */,
	&m19555_MI/* Method Usage */,
	&m19555_MI/* Method Usage */,
	&m19555_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&m17272_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3127_0_0_0;
extern Il2CppType t3127_1_0_0;
struct t3127;
extern Il2CppGenericClass t3127_GC;
TypeInfo t3127_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`4", "UnityEngine.Events", t3127_MIs, NULL, t3127_FIs, NULL, &t556_TI, NULL, NULL, &t3127_TI, NULL, t3127_VT, &EmptyCustomAttributesCache, &t3127_TI, &t3127_0_0_0, &t3127_1_0_0, NULL, &t3127_GC, NULL, NULL, NULL, t3127_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3127), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m17271_MI;
 void m17271_gshared (t3128 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 void m17272_gshared (t3128 * __this, t29 * p0, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m17272((t3128 *)__this->f9,p0, p1, p2, p3, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17273_MI;
 t29 * m17273_gshared (t3128 * __this, t29 * p0, t29 * p1, t29 * p2, t29 * p3, t67 * p4, t29 * p5, MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	__d_args[2] = p2;
	__d_args[3] = p3;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p4, (Il2CppObject*)p5);
}
extern MethodInfo m17274_MI;
 void m17274_gshared (t3128 * __this, t29 * p0, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3128_m17271_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17271_GM;
MethodInfo m17271_MI = 
{
	".ctor", (methodPointerType)&m17271_gshared, &t3128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3128_m17271_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17271_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3128_m17272_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg1", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg2", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg3", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17272_GM;
MethodInfo m17272_MI = 
{
	"Invoke", (methodPointerType)&m17272_gshared, &t3128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29_t29, t3128_m17272_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17272_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3128_m17273_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg1", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg2", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"arg3", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 4, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 5, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17273_GM;
MethodInfo m17273_MI = 
{
	"BeginInvoke", (methodPointerType)&m17273_gshared, &t3128_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29_t29_t29, t3128_m17273_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 6, false, true, 0, NULL, (methodPointerType)NULL, &m17273_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3128_m17274_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17274_GM;
MethodInfo m17274_MI = 
{
	"EndInvoke", (methodPointerType)&m17274_gshared, &t3128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3128_m17274_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17274_GM};
static MethodInfo* t3128_MIs[] =
{
	&m17271_MI,
	&m17272_MI,
	&m17273_MI,
	&m17274_MI,
	NULL
};
static MethodInfo* t3128_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17272_MI,
	&m17273_MI,
	&m17274_MI,
};
static Il2CppInterfaceOffsetPair t3128_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3128_1_0_0;
struct t3128;
extern Il2CppGenericClass t3128_GC;
TypeInfo t3128_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`4", "UnityEngine.Events", t3128_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3128_TI, NULL, t3128_VT, &EmptyCustomAttributesCache, &t3128_TI, &t3128_0_0_0, &t3128_1_0_0, t3128_IOs, &t3128_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3128), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4503_TI;

#include "t564.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Events.UnityEventCallState>
extern MethodInfo m30240_MI;
static PropertyInfo t4503____Current_PropertyInfo = 
{
	&t4503_TI, "Current", &m30240_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4503_PIs[] =
{
	&t4503____Current_PropertyInfo,
	NULL
};
extern Il2CppType t564_0_0_0;
extern void* RuntimeInvoker_t564 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30240_GM;
MethodInfo m30240_MI = 
{
	"get_Current", NULL, &t4503_TI, &t564_0_0_0, RuntimeInvoker_t564, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30240_GM};
static MethodInfo* t4503_MIs[] =
{
	&m30240_MI,
	NULL
};
static TypeInfo* t4503_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4503_0_0_0;
extern Il2CppType t4503_1_0_0;
struct t4503;
extern Il2CppGenericClass t4503_GC;
TypeInfo t4503_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4503_MIs, t4503_PIs, NULL, NULL, NULL, NULL, NULL, &t4503_TI, t4503_ITIs, NULL, &EmptyCustomAttributesCache, &t4503_TI, &t4503_0_0_0, &t4503_1_0_0, NULL, &t4503_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3129.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3129_TI;
#include "t3129MD.h"

extern TypeInfo t564_TI;
extern MethodInfo m17279_MI;
extern MethodInfo m23039_MI;
struct t20;
 int32_t m23039 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17275_MI;
 void m17275 (t3129 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17276_MI;
 t29 * m17276 (t3129 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17279(__this, &m17279_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t564_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17277_MI;
 void m17277 (t3129 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17278_MI;
 bool m17278 (t3129 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17279 (t3129 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23039(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23039_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Events.UnityEventCallState>
extern Il2CppType t20_0_0_1;
FieldInfo t3129_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3129_TI, offsetof(t3129, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3129_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3129_TI, offsetof(t3129, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3129_FIs[] =
{
	&t3129_f0_FieldInfo,
	&t3129_f1_FieldInfo,
	NULL
};
static PropertyInfo t3129____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3129_TI, "System.Collections.IEnumerator.Current", &m17276_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3129____Current_PropertyInfo = 
{
	&t3129_TI, "Current", &m17279_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3129_PIs[] =
{
	&t3129____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3129____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3129_m17275_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17275_GM;
MethodInfo m17275_MI = 
{
	".ctor", (methodPointerType)&m17275, &t3129_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3129_m17275_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17275_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17276_GM;
MethodInfo m17276_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17276, &t3129_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17276_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17277_GM;
MethodInfo m17277_MI = 
{
	"Dispose", (methodPointerType)&m17277, &t3129_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17277_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17278_GM;
MethodInfo m17278_MI = 
{
	"MoveNext", (methodPointerType)&m17278, &t3129_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17278_GM};
extern Il2CppType t564_0_0_0;
extern void* RuntimeInvoker_t564 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17279_GM;
MethodInfo m17279_MI = 
{
	"get_Current", (methodPointerType)&m17279, &t3129_TI, &t564_0_0_0, RuntimeInvoker_t564, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17279_GM};
static MethodInfo* t3129_MIs[] =
{
	&m17275_MI,
	&m17276_MI,
	&m17277_MI,
	&m17278_MI,
	&m17279_MI,
	NULL
};
static MethodInfo* t3129_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17276_MI,
	&m17278_MI,
	&m17277_MI,
	&m17279_MI,
};
static TypeInfo* t3129_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4503_TI,
};
static Il2CppInterfaceOffsetPair t3129_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4503_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3129_0_0_0;
extern Il2CppType t3129_1_0_0;
extern Il2CppGenericClass t3129_GC;
TypeInfo t3129_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3129_MIs, t3129_PIs, t3129_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3129_TI, t3129_ITIs, t3129_VT, &EmptyCustomAttributesCache, &t3129_TI, &t3129_0_0_0, &t3129_1_0_0, t3129_IOs, &t3129_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3129)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5787_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Events.UnityEventCallState>
extern MethodInfo m30241_MI;
static PropertyInfo t5787____Count_PropertyInfo = 
{
	&t5787_TI, "Count", &m30241_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30242_MI;
static PropertyInfo t5787____IsReadOnly_PropertyInfo = 
{
	&t5787_TI, "IsReadOnly", &m30242_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5787_PIs[] =
{
	&t5787____Count_PropertyInfo,
	&t5787____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30241_GM;
MethodInfo m30241_MI = 
{
	"get_Count", NULL, &t5787_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30241_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30242_GM;
MethodInfo m30242_MI = 
{
	"get_IsReadOnly", NULL, &t5787_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30242_GM};
extern Il2CppType t564_0_0_0;
extern Il2CppType t564_0_0_0;
static ParameterInfo t5787_m30243_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t564_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30243_GM;
MethodInfo m30243_MI = 
{
	"Add", NULL, &t5787_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5787_m30243_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30243_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30244_GM;
MethodInfo m30244_MI = 
{
	"Clear", NULL, &t5787_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30244_GM};
extern Il2CppType t564_0_0_0;
static ParameterInfo t5787_m30245_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t564_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30245_GM;
MethodInfo m30245_MI = 
{
	"Contains", NULL, &t5787_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5787_m30245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30245_GM};
extern Il2CppType t3791_0_0_0;
extern Il2CppType t3791_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5787_m30246_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3791_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30246_GM;
MethodInfo m30246_MI = 
{
	"CopyTo", NULL, &t5787_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5787_m30246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30246_GM};
extern Il2CppType t564_0_0_0;
static ParameterInfo t5787_m30247_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t564_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30247_GM;
MethodInfo m30247_MI = 
{
	"Remove", NULL, &t5787_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5787_m30247_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30247_GM};
static MethodInfo* t5787_MIs[] =
{
	&m30241_MI,
	&m30242_MI,
	&m30243_MI,
	&m30244_MI,
	&m30245_MI,
	&m30246_MI,
	&m30247_MI,
	NULL
};
extern TypeInfo t5789_TI;
static TypeInfo* t5787_ITIs[] = 
{
	&t603_TI,
	&t5789_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5787_0_0_0;
extern Il2CppType t5787_1_0_0;
struct t5787;
extern Il2CppGenericClass t5787_GC;
TypeInfo t5787_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5787_MIs, t5787_PIs, NULL, NULL, NULL, NULL, NULL, &t5787_TI, t5787_ITIs, NULL, &EmptyCustomAttributesCache, &t5787_TI, &t5787_0_0_0, &t5787_1_0_0, NULL, &t5787_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Events.UnityEventCallState>
extern Il2CppType t4503_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30248_GM;
MethodInfo m30248_MI = 
{
	"GetEnumerator", NULL, &t5789_TI, &t4503_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30248_GM};
static MethodInfo* t5789_MIs[] =
{
	&m30248_MI,
	NULL
};
static TypeInfo* t5789_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5789_0_0_0;
extern Il2CppType t5789_1_0_0;
struct t5789;
extern Il2CppGenericClass t5789_GC;
TypeInfo t5789_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5789_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5789_TI, t5789_ITIs, NULL, &EmptyCustomAttributesCache, &t5789_TI, &t5789_0_0_0, &t5789_1_0_0, NULL, &t5789_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5788_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Events.UnityEventCallState>
extern MethodInfo m30249_MI;
extern MethodInfo m30250_MI;
static PropertyInfo t5788____Item_PropertyInfo = 
{
	&t5788_TI, "Item", &m30249_MI, &m30250_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5788_PIs[] =
{
	&t5788____Item_PropertyInfo,
	NULL
};
extern Il2CppType t564_0_0_0;
static ParameterInfo t5788_m30251_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t564_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30251_GM;
MethodInfo m30251_MI = 
{
	"IndexOf", NULL, &t5788_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5788_m30251_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30251_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t564_0_0_0;
static ParameterInfo t5788_m30252_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t564_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30252_GM;
MethodInfo m30252_MI = 
{
	"Insert", NULL, &t5788_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5788_m30252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30252_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5788_m30253_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30253_GM;
MethodInfo m30253_MI = 
{
	"RemoveAt", NULL, &t5788_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5788_m30253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30253_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5788_m30249_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t564_0_0_0;
extern void* RuntimeInvoker_t564_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30249_GM;
MethodInfo m30249_MI = 
{
	"get_Item", NULL, &t5788_TI, &t564_0_0_0, RuntimeInvoker_t564_t44, t5788_m30249_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30249_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t564_0_0_0;
static ParameterInfo t5788_m30250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t564_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30250_GM;
MethodInfo m30250_MI = 
{
	"set_Item", NULL, &t5788_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5788_m30250_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30250_GM};
static MethodInfo* t5788_MIs[] =
{
	&m30251_MI,
	&m30252_MI,
	&m30253_MI,
	&m30249_MI,
	&m30250_MI,
	NULL
};
static TypeInfo* t5788_ITIs[] = 
{
	&t603_TI,
	&t5787_TI,
	&t5789_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5788_0_0_0;
extern Il2CppType t5788_1_0_0;
struct t5788;
extern Il2CppGenericClass t5788_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5788_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5788_MIs, t5788_PIs, NULL, NULL, NULL, NULL, NULL, &t5788_TI, t5788_ITIs, NULL, &t1908__CustomAttributeCache, &t5788_TI, &t5788_0_0_0, &t5788_1_0_0, NULL, &t5788_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t656.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t656_TI;
#include "t656MD.h"

#include "t22.h"
#include "t2624.h"
extern TypeInfo t22_TI;
extern TypeInfo t2624_TI;
#include "t2624MD.h"
extern MethodInfo m14118_MI;
extern MethodInfo m14120_MI;


extern MethodInfo m2974_MI;
 void m2974 (t656 * __this, t41 * p0, t557 * p1, float p2, MethodInfo* method){
	{
		__this->f1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m14118(__this, p0, p1, &m14118_MI);
		t316* L_0 = (__this->f1);
		float L_1 = p2;
		t29 * L_2 = Box(InitializedTypeInfo(&t22_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		return;
	}
}
extern MethodInfo m17280_MI;
 void m17280 (t656 * __this, t316* p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f1);
		m14120(__this, L_0, &m14120_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.Single>
extern Il2CppType t316_0_0_33;
FieldInfo t656_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t656_TI, offsetof(t656, f1), &EmptyCustomAttributesCache};
static FieldInfo* t656_FIs[] =
{
	&t656_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t656_m2974_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2974_GM;
MethodInfo m2974_MI = 
{
	".ctor", (methodPointerType)&m2974, &t656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t22, t656_m2974_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m2974_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t656_m17280_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17280_GM;
MethodInfo m17280_MI = 
{
	"Invoke", (methodPointerType)&m17280, &t656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t656_m17280_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17280_GM};
static MethodInfo* t656_MIs[] =
{
	&m2974_MI,
	&m17280_MI,
	NULL
};
extern MethodInfo m14121_MI;
static MethodInfo* t656_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17280_MI,
	&m14121_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t656_0_0_0;
extern Il2CppType t656_1_0_0;
struct t656;
extern Il2CppGenericClass t656_GC;
TypeInfo t656_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t656_MIs, NULL, t656_FIs, NULL, &t2624_TI, NULL, NULL, &t656_TI, NULL, t656_VT, &EmptyCustomAttributesCache, &t656_TI, &t656_0_0_0, &t656_1_0_0, NULL, &t656_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t656), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#include "t657.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t657_TI;
#include "t657MD.h"

#include "t3130.h"
extern TypeInfo t3130_TI;
#include "t3130MD.h"
extern MethodInfo m17282_MI;
extern MethodInfo m17284_MI;


extern MethodInfo m2975_MI;
 void m2975 (t657 * __this, t41 * p0, t557 * p1, int32_t p2, MethodInfo* method){
	{
		__this->f1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m17282(__this, p0, p1, &m17282_MI);
		t316* L_0 = (__this->f1);
		int32_t L_1 = p2;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		return;
	}
}
extern MethodInfo m17281_MI;
 void m17281 (t657 * __this, t316* p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f1);
		m17284(__this, L_0, &m17284_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.Int32>
extern Il2CppType t316_0_0_33;
FieldInfo t657_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t657_TI, offsetof(t657, f1), &EmptyCustomAttributesCache};
static FieldInfo* t657_FIs[] =
{
	&t657_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t657_m2975_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2975_GM;
MethodInfo m2975_MI = 
{
	".ctor", (methodPointerType)&m2975, &t657_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44, t657_m2975_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m2975_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t657_m17281_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17281_GM;
MethodInfo m17281_MI = 
{
	"Invoke", (methodPointerType)&m17281, &t657_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t657_m17281_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17281_GM};
static MethodInfo* t657_MIs[] =
{
	&m2975_MI,
	&m17281_MI,
	NULL
};
extern MethodInfo m17285_MI;
static MethodInfo* t657_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17281_MI,
	&m17285_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t657_0_0_0;
extern Il2CppType t657_1_0_0;
struct t657;
extern Il2CppGenericClass t657_GC;
TypeInfo t657_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t657_MIs, NULL, t657_FIs, NULL, &t3130_TI, NULL, NULL, &t657_TI, NULL, t657_VT, &EmptyCustomAttributesCache, &t657_TI, &t657_0_0_0, &t657_1_0_0, NULL, &t657_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t657), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3131.h"
extern TypeInfo t3131_TI;
#include "t3131MD.h"
extern Il2CppType t3131_0_0_0;
extern MethodInfo m23049_MI;
extern MethodInfo m17287_MI;
struct t556;
 void m23049 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m17282 (t3130 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m2790(__this, p0, p1, &m2790_MI);
		t3131 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t3131_0_0_0), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t3131 *)IsInst(L_2, InitializedTypeInfo(&t3131_TI))), &m1597_MI);
		__this->f0 = ((t3131 *)Castclass(L_3, InitializedTypeInfo(&t3131_TI)));
		return;
	}
}
extern MethodInfo m17283_MI;
 void m17283 (t3130 * __this, t3131 * p0, MethodInfo* method){
	{
		m2789(__this, &m2789_MI);
		t3131 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t3131 *)Castclass(L_1, InitializedTypeInfo(&t3131_TI)));
		return;
	}
}
 void m17284 (t3130 * __this, t316* p0, MethodInfo* method){
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		m23049(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), &m23049_MI);
		t3131 * L_2 = (__this->f0);
		bool L_3 = m2791(NULL, L_2, &m2791_MI);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		t3131 * L_4 = (__this->f0);
		int32_t L_5 = 0;
		VirtActionInvoker1< int32_t >::Invoke(&m17287_MI, L_4, ((*(int32_t*)((int32_t*)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(p0, L_5)), InitializedTypeInfo(&t44_TI))))));
	}

IL_003f:
	{
		return;
	}
}
 bool m17285 (t3130 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t3131 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t3131 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.Int32>
extern Il2CppType t3131_0_0_1;
FieldInfo t3130_f0_FieldInfo = 
{
	"Delegate", &t3131_0_0_1, &t3130_TI, offsetof(t3130, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3130_FIs[] =
{
	&t3130_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3130_m17282_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17282_GM;
MethodInfo m17282_MI = 
{
	".ctor", (methodPointerType)&m17282, &t3130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3130_m17282_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17282_GM};
extern Il2CppType t3131_0_0_0;
static ParameterInfo t3130_m17283_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3131_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17283_GM;
MethodInfo m17283_MI = 
{
	".ctor", (methodPointerType)&m17283, &t3130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3130_m17283_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17283_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3130_m17284_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17284_GM;
MethodInfo m17284_MI = 
{
	"Invoke", (methodPointerType)&m17284, &t3130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3130_m17284_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17284_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3130_m17285_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17285_GM;
MethodInfo m17285_MI = 
{
	"Find", (methodPointerType)&m17285, &t3130_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3130_m17285_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17285_GM};
static MethodInfo* t3130_MIs[] =
{
	&m17282_MI,
	&m17283_MI,
	&m17284_MI,
	&m17285_MI,
	NULL
};
static MethodInfo* t3130_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17284_MI,
	&m17285_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3130_0_0_0;
extern Il2CppType t3130_1_0_0;
struct t3130;
extern Il2CppGenericClass t3130_GC;
TypeInfo t3130_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3130_MIs, NULL, t3130_FIs, NULL, &t556_TI, NULL, NULL, &t3130_TI, NULL, t3130_VT, &EmptyCustomAttributesCache, &t3130_TI, &t3130_0_0_0, &t3130_1_0_0, NULL, &t3130_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3130), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m17286_MI;
 void m17286 (t3131 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 void m17287 (t3131 * __this, int32_t p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17287((t3131 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, int32_t p0, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m17288_MI;
 t29 * m17288 (t3131 * __this, int32_t p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m17289_MI;
 void m17289 (t3131 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Int32>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3131_m17286_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17286_GM;
MethodInfo m17286_MI = 
{
	".ctor", (methodPointerType)&m17286, &t3131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3131_m17286_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17286_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3131_m17287_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17287_GM;
MethodInfo m17287_MI = 
{
	"Invoke", (methodPointerType)&m17287, &t3131_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3131_m17287_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17287_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3131_m17288_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17288_GM;
MethodInfo m17288_MI = 
{
	"BeginInvoke", (methodPointerType)&m17288, &t3131_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29, t3131_m17288_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17288_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3131_m17289_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17289_GM;
MethodInfo m17289_MI = 
{
	"EndInvoke", (methodPointerType)&m17289, &t3131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3131_m17289_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17289_GM};
static MethodInfo* t3131_MIs[] =
{
	&m17286_MI,
	&m17287_MI,
	&m17288_MI,
	&m17289_MI,
	NULL
};
static MethodInfo* t3131_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17287_MI,
	&m17288_MI,
	&m17289_MI,
};
static Il2CppInterfaceOffsetPair t3131_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3131_1_0_0;
struct t3131;
extern Il2CppGenericClass t3131_GC;
TypeInfo t3131_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3131_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3131_TI, NULL, t3131_VT, &EmptyCustomAttributesCache, &t3131_TI, &t3131_0_0_0, &t3131_1_0_0, t3131_IOs, &t3131_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3131), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t658.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t658_TI;
#include "t658MD.h"

#include "t371.h"
extern TypeInfo t371_TI;
#include "t371MD.h"
extern MethodInfo m1697_MI;
extern MethodInfo m14047_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.String>
extern Il2CppType t316_0_0_33;
FieldInfo t658_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t658_TI, offsetof(t658, f1), &EmptyCustomAttributesCache};
static FieldInfo* t658_FIs[] =
{
	&t658_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t658_m2976_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2976_GM;
MethodInfo m2976_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t658_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t658_m2976_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m2976_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t658_m17290_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17290_GM;
MethodInfo m17290_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t658_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t658_m17290_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17290_GM};
static MethodInfo* t658_MIs[] =
{
	&m2976_MI,
	&m17290_MI,
	NULL
};
extern MethodInfo m17290_MI;
extern MethodInfo m14048_MI;
static MethodInfo* t658_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17290_MI,
	&m14048_MI,
};
extern Il2CppType t2607_0_0_0;
extern TypeInfo t2607_TI;
extern MethodInfo m21225_MI;
extern TypeInfo t7_TI;
extern MethodInfo m14044_MI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t658_RGCTXData[8] = 
{
	&t2607_0_0_0/* Type Usage */,
	&t2607_TI/* Class Usage */,
	&m21225_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m14044_MI/* Method Usage */,
	&m1697_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m14047_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t658_0_0_0;
extern Il2CppType t658_1_0_0;
struct t658;
extern Il2CppGenericClass t658_GC;
TypeInfo t658_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t658_MIs, NULL, t658_FIs, NULL, &t371_TI, NULL, NULL, &t658_TI, NULL, t658_VT, &EmptyCustomAttributesCache, &t658_TI, &t658_0_0_0, &t658_1_0_0, NULL, &t658_GC, NULL, NULL, NULL, t658_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t658), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#include "t659.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t659_TI;
#include "t659MD.h"

#include "t2698.h"
extern TypeInfo t2698_TI;
#include "t2698MD.h"
extern MethodInfo m14671_MI;
extern MethodInfo m14673_MI;


extern MethodInfo m2977_MI;
 void m2977 (t659 * __this, t41 * p0, t557 * p1, bool p2, MethodInfo* method){
	{
		__this->f1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m14671(__this, p0, p1, &m14671_MI);
		t316* L_0 = (__this->f1);
		bool L_1 = p2;
		t29 * L_2 = Box(InitializedTypeInfo(&t40_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		return;
	}
}
extern MethodInfo m17291_MI;
 void m17291 (t659 * __this, t316* p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f1);
		m14673(__this, L_0, &m14673_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
extern Il2CppType t316_0_0_33;
FieldInfo t659_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t659_TI, offsetof(t659, f1), &EmptyCustomAttributesCache};
static FieldInfo* t659_FIs[] =
{
	&t659_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t659_m2977_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2977_GM;
MethodInfo m2977_MI = 
{
	".ctor", (methodPointerType)&m2977, &t659_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t297, t659_m2977_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m2977_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t659_m17291_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17291_GM;
MethodInfo m17291_MI = 
{
	"Invoke", (methodPointerType)&m17291, &t659_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t659_m17291_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17291_GM};
static MethodInfo* t659_MIs[] =
{
	&m2977_MI,
	&m17291_MI,
	NULL
};
extern MethodInfo m14674_MI;
static MethodInfo* t659_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17291_MI,
	&m14674_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t659_0_0_0;
extern Il2CppType t659_1_0_0;
struct t659;
extern Il2CppGenericClass t659_GC;
TypeInfo t659_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t659_MIs, NULL, t659_FIs, NULL, &t2698_TI, NULL, NULL, &t659_TI, NULL, t659_VT, &EmptyCustomAttributesCache, &t659_TI, &t659_0_0_0, &t659_1_0_0, NULL, &t659_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t659), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#include "t568.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t568_TI;
#include "t568MD.h"

#include "t565.h"
#include "t661.h"
#include "t3136.h"
#include "t3137.h"
#include "t3144.h"
#include "t3138.h"
extern TypeInfo t565_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t3132_TI;
extern TypeInfo t661_TI;
extern TypeInfo t3134_TI;
extern TypeInfo t3135_TI;
extern TypeInfo t3133_TI;
extern TypeInfo t3136_TI;
extern TypeInfo t3137_TI;
extern TypeInfo t3144_TI;
#include "t602MD.h"
#include "t3136MD.h"
#include "t3137MD.h"
#include "t661MD.h"
#include "t3144MD.h"
extern MethodInfo m17337_MI;
extern MethodInfo m17338_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m23062_MI;
extern MethodInfo m17323_MI;
extern MethodInfo m2984_MI;
extern MethodInfo m17309_MI;
extern MethodInfo m17316_MI;
extern MethodInfo m17321_MI;
extern MethodInfo m17324_MI;
extern MethodInfo m17326_MI;
extern MethodInfo m17310_MI;
extern MethodInfo m17334_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m17335_MI;
extern MethodInfo m30254_MI;
extern MethodInfo m30255_MI;
extern MethodInfo m30256_MI;
extern MethodInfo m30257_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m17325_MI;
extern MethodInfo m17311_MI;
extern MethodInfo m17312_MI;
extern MethodInfo m17348_MI;
extern MethodInfo m23064_MI;
extern MethodInfo m17319_MI;
extern MethodInfo m17320_MI;
extern MethodInfo m17423_MI;
extern MethodInfo m17344_MI;
extern MethodInfo m17322_MI;
extern MethodInfo m17328_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m17429_MI;
extern MethodInfo m23066_MI;
extern MethodInfo m23074_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m23062(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t3142.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m23064(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m23066(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m23074(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t661  m2984 (t568 * __this, MethodInfo* method){
	{
		t661  L_0 = {0};
		m17344(&L_0, __this, &m17344_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t44_0_0_32849;
FieldInfo t568_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t568_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t3132_0_0_1;
FieldInfo t568_f1_FieldInfo = 
{
	"_items", &t3132_0_0_1, &t568_TI, offsetof(t568, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t568_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t568_TI, offsetof(t568, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t568_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t568_TI, offsetof(t568, f3), &EmptyCustomAttributesCache};
extern Il2CppType t3132_0_0_49;
FieldInfo t568_f4_FieldInfo = 
{
	"EmptyArray", &t3132_0_0_49, &t568_TI, offsetof(t568_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t568_FIs[] =
{
	&t568_f0_FieldInfo,
	&t568_f1_FieldInfo,
	&t568_f2_FieldInfo,
	&t568_f3_FieldInfo,
	&t568_f4_FieldInfo,
	NULL
};
static const int32_t t568_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t568_f0_DefaultValue = 
{
	&t568_f0_FieldInfo, { (char*)&t568_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t568_FDVs[] = 
{
	&t568_f0_DefaultValue,
	NULL
};
extern MethodInfo m17302_MI;
static PropertyInfo t568____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t568_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m17302_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17303_MI;
static PropertyInfo t568____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t568_TI, "System.Collections.ICollection.IsSynchronized", &m17303_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17304_MI;
static PropertyInfo t568____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t568_TI, "System.Collections.ICollection.SyncRoot", &m17304_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17305_MI;
static PropertyInfo t568____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t568_TI, "System.Collections.IList.IsFixedSize", &m17305_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17306_MI;
static PropertyInfo t568____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t568_TI, "System.Collections.IList.IsReadOnly", &m17306_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17307_MI;
extern MethodInfo m17308_MI;
static PropertyInfo t568____System_Collections_IList_Item_PropertyInfo = 
{
	&t568_TI, "System.Collections.IList.Item", &m17307_MI, &m17308_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t568____Capacity_PropertyInfo = 
{
	&t568_TI, "Capacity", &m17334_MI, &m17335_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17336_MI;
static PropertyInfo t568____Count_PropertyInfo = 
{
	&t568_TI, "Count", &m17336_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t568____Item_PropertyInfo = 
{
	&t568_TI, "Item", &m17337_MI, &m17338_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t568_PIs[] =
{
	&t568____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t568____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t568____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t568____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t568____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t568____System_Collections_IList_Item_PropertyInfo,
	&t568____Capacity_PropertyInfo,
	&t568____Count_PropertyInfo,
	&t568____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2983_GM;
MethodInfo m2983_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2983_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17292_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17292_GM;
MethodInfo m17292_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t568_m17292_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17292_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17293_GM;
MethodInfo m17293_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17293_GM};
extern Il2CppType t3133_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17294_GM;
MethodInfo m17294_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t568_TI, &t3133_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17294_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17295_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17295_GM;
MethodInfo m17295_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t568_m17295_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17295_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17296_GM;
MethodInfo m17296_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t568_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17296_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t568_m17297_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17297_GM;
MethodInfo m17297_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t568_m17297_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17297_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t568_m17298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17298_GM;
MethodInfo m17298_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t568_m17298_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17298_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t568_m17299_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17299_GM;
MethodInfo m17299_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t568_m17299_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17299_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t568_m17300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17300_GM;
MethodInfo m17300_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t568_m17300_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17300_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t568_m17301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17301_GM;
MethodInfo m17301_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17301_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17301_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17302_GM;
MethodInfo m17302_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17302_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17303_GM;
MethodInfo m17303_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17303_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17304_GM;
MethodInfo m17304_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t568_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17304_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17305_GM;
MethodInfo m17305_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17305_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17306_GM;
MethodInfo m17306_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17306_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17307_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17307_GM;
MethodInfo m17307_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t568_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t568_m17307_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17307_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t568_m17308_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17308_GM;
MethodInfo m17308_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t568_m17308_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17308_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t568_m17309_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17309_GM;
MethodInfo m17309_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17309_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17309_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17310_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17310_GM;
MethodInfo m17310_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t568_m17310_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17310_GM};
extern Il2CppType t3134_0_0_0;
extern Il2CppType t3134_0_0_0;
static ParameterInfo t568_m17311_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3134_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17311_GM;
MethodInfo m17311_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17311_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17311_GM};
extern Il2CppType t3135_0_0_0;
extern Il2CppType t3135_0_0_0;
static ParameterInfo t568_m17312_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3135_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17312_GM;
MethodInfo m17312_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17312_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17312_GM};
extern Il2CppType t3135_0_0_0;
static ParameterInfo t568_m17313_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3135_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17313_GM;
MethodInfo m17313_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17313_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17313_GM};
extern Il2CppType t3136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17314_GM;
MethodInfo m17314_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t568_TI, &t3136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17314_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17315_GM;
MethodInfo m17315_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17315_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t568_m17316_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17316_GM;
MethodInfo m17316_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t568_m17316_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17316_GM};
extern Il2CppType t3132_0_0_0;
extern Il2CppType t3132_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17317_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3132_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17317_GM;
MethodInfo m17317_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t568_m17317_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17317_GM};
extern Il2CppType t3137_0_0_0;
extern Il2CppType t3137_0_0_0;
static ParameterInfo t568_m17318_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3137_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17318_GM;
MethodInfo m17318_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t568_TI, &t565_0_0_0, RuntimeInvoker_t29_t29, t568_m17318_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17318_GM};
extern Il2CppType t3137_0_0_0;
static ParameterInfo t568_m17319_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3137_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17319_GM;
MethodInfo m17319_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17319_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17319_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3137_0_0_0;
static ParameterInfo t568_m17320_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t3137_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17320_GM;
MethodInfo m17320_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t568_m17320_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17320_GM};
extern Il2CppType t661_0_0_0;
extern void* RuntimeInvoker_t661 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2984_GM;
MethodInfo m2984_MI = 
{
	"GetEnumerator", (methodPointerType)&m2984, &t568_TI, &t661_0_0_0, RuntimeInvoker_t661, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2984_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t568_m17321_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17321_GM;
MethodInfo m17321_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t568_m17321_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17321_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17322_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17322_GM;
MethodInfo m17322_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t568_m17322_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17322_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17323_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17323_GM;
MethodInfo m17323_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t568_m17323_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17323_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t568_m17324_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17324_GM;
MethodInfo m17324_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t568_m17324_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17324_GM};
extern Il2CppType t3135_0_0_0;
static ParameterInfo t568_m17325_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3135_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17325_GM;
MethodInfo m17325_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17325_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17325_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t568_m17326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17326_GM;
MethodInfo m17326_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t568_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t568_m17326_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17326_GM};
extern Il2CppType t3137_0_0_0;
static ParameterInfo t568_m17327_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3137_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17327_GM;
MethodInfo m17327_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t568_m17327_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17327_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17328_GM;
MethodInfo m17328_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t568_m17328_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17328_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17329_GM;
MethodInfo m17329_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17329_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17330_GM;
MethodInfo m17330_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17330_GM};
extern Il2CppType t3138_0_0_0;
extern Il2CppType t3138_0_0_0;
static ParameterInfo t568_m17331_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3138_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17331_GM;
MethodInfo m17331_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t568_m17331_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17331_GM};
extern Il2CppType t3132_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17332_GM;
MethodInfo m17332_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t568_TI, &t3132_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17332_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17333_GM;
MethodInfo m17333_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17333_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17334_GM;
MethodInfo m17334_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17334_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17335_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17335_GM;
MethodInfo m17335_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t568_m17335_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17335_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17336_GM;
MethodInfo m17336_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t568_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17336_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t568_m17337_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17337_GM;
MethodInfo m17337_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t568_TI, &t565_0_0_0, RuntimeInvoker_t29_t44, t568_m17337_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17337_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t568_m17338_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17338_GM;
MethodInfo m17338_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t568_m17338_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17338_GM};
static MethodInfo* t568_MIs[] =
{
	&m2983_MI,
	&m17292_MI,
	&m17293_MI,
	&m17294_MI,
	&m17295_MI,
	&m17296_MI,
	&m17297_MI,
	&m17298_MI,
	&m17299_MI,
	&m17300_MI,
	&m17301_MI,
	&m17302_MI,
	&m17303_MI,
	&m17304_MI,
	&m17305_MI,
	&m17306_MI,
	&m17307_MI,
	&m17308_MI,
	&m17309_MI,
	&m17310_MI,
	&m17311_MI,
	&m17312_MI,
	&m17313_MI,
	&m17314_MI,
	&m17315_MI,
	&m17316_MI,
	&m17317_MI,
	&m17318_MI,
	&m17319_MI,
	&m17320_MI,
	&m2984_MI,
	&m17321_MI,
	&m17322_MI,
	&m17323_MI,
	&m17324_MI,
	&m17325_MI,
	&m17326_MI,
	&m17327_MI,
	&m17328_MI,
	&m17329_MI,
	&m17330_MI,
	&m17331_MI,
	&m17332_MI,
	&m17333_MI,
	&m17334_MI,
	&m17335_MI,
	&m17336_MI,
	&m17337_MI,
	&m17338_MI,
	NULL
};
extern MethodInfo m17296_MI;
extern MethodInfo m17295_MI;
extern MethodInfo m17297_MI;
extern MethodInfo m17315_MI;
extern MethodInfo m17298_MI;
extern MethodInfo m17299_MI;
extern MethodInfo m17300_MI;
extern MethodInfo m17301_MI;
extern MethodInfo m17317_MI;
extern MethodInfo m17294_MI;
static MethodInfo* t568_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17296_MI,
	&m17336_MI,
	&m17303_MI,
	&m17304_MI,
	&m17295_MI,
	&m17305_MI,
	&m17306_MI,
	&m17307_MI,
	&m17308_MI,
	&m17297_MI,
	&m17315_MI,
	&m17298_MI,
	&m17299_MI,
	&m17300_MI,
	&m17301_MI,
	&m17328_MI,
	&m17336_MI,
	&m17302_MI,
	&m17309_MI,
	&m17315_MI,
	&m17316_MI,
	&m17317_MI,
	&m17326_MI,
	&m17294_MI,
	&m17321_MI,
	&m17324_MI,
	&m17328_MI,
	&m17337_MI,
	&m17338_MI,
};
extern TypeInfo t868_TI;
extern TypeInfo t3140_TI;
static TypeInfo* t568_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3134_TI,
	&t3135_TI,
	&t3140_TI,
};
static Il2CppInterfaceOffsetPair t568_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3134_TI, 20},
	{ &t3135_TI, 27},
	{ &t3140_TI, 28},
};
extern TypeInfo t568_TI;
extern TypeInfo t3132_TI;
extern TypeInfo t661_TI;
extern TypeInfo t565_TI;
extern TypeInfo t3134_TI;
extern TypeInfo t3136_TI;
static Il2CppRGCTXData t568_RGCTXData[37] = 
{
	&t568_TI/* Static Usage */,
	&t3132_TI/* Array Usage */,
	&m2984_MI/* Method Usage */,
	&t661_TI/* Class Usage */,
	&t565_TI/* Class Usage */,
	&m17309_MI/* Method Usage */,
	&m17316_MI/* Method Usage */,
	&m17321_MI/* Method Usage */,
	&m17323_MI/* Method Usage */,
	&m17324_MI/* Method Usage */,
	&m17326_MI/* Method Usage */,
	&m17337_MI/* Method Usage */,
	&m17338_MI/* Method Usage */,
	&m17310_MI/* Method Usage */,
	&m17334_MI/* Method Usage */,
	&m17335_MI/* Method Usage */,
	&m30254_MI/* Method Usage */,
	&m30255_MI/* Method Usage */,
	&m30256_MI/* Method Usage */,
	&m30257_MI/* Method Usage */,
	&m17325_MI/* Method Usage */,
	&t3134_TI/* Class Usage */,
	&m17311_MI/* Method Usage */,
	&m17312_MI/* Method Usage */,
	&t3136_TI/* Class Usage */,
	&m17348_MI/* Method Usage */,
	&m23064_MI/* Method Usage */,
	&m17319_MI/* Method Usage */,
	&m17320_MI/* Method Usage */,
	&m17423_MI/* Method Usage */,
	&m17344_MI/* Method Usage */,
	&m17322_MI/* Method Usage */,
	&m17328_MI/* Method Usage */,
	&m17429_MI/* Method Usage */,
	&m23066_MI/* Method Usage */,
	&m23074_MI/* Method Usage */,
	&m23062_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t568_0_0_0;
extern Il2CppType t568_1_0_0;
struct t568;
extern Il2CppGenericClass t568_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t568_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t568_MIs, t568_PIs, t568_FIs, NULL, &t29_TI, NULL, NULL, &t568_TI, t568_ITIs, t568_VT, &t1261__CustomAttributeCache, &t568_TI, &t568_0_0_0, &t568_1_0_0, t568_IOs, &t568_GC, NULL, t568_FDVs, NULL, t568_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t568), 0, -1, sizeof(t568_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Events.PersistentCall>
static PropertyInfo t3134____Count_PropertyInfo = 
{
	&t3134_TI, "Count", &m30254_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30258_MI;
static PropertyInfo t3134____IsReadOnly_PropertyInfo = 
{
	&t3134_TI, "IsReadOnly", &m30258_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3134_PIs[] =
{
	&t3134____Count_PropertyInfo,
	&t3134____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30254_GM;
MethodInfo m30254_MI = 
{
	"get_Count", NULL, &t3134_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30254_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30258_GM;
MethodInfo m30258_MI = 
{
	"get_IsReadOnly", NULL, &t3134_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30258_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3134_m30259_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30259_GM;
MethodInfo m30259_MI = 
{
	"Add", NULL, &t3134_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3134_m30259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30259_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30260_GM;
MethodInfo m30260_MI = 
{
	"Clear", NULL, &t3134_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30260_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3134_m30261_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30261_GM;
MethodInfo m30261_MI = 
{
	"Contains", NULL, &t3134_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3134_m30261_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30261_GM};
extern Il2CppType t3132_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3134_m30255_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3132_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30255_GM;
MethodInfo m30255_MI = 
{
	"CopyTo", NULL, &t3134_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3134_m30255_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30255_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3134_m30262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30262_GM;
MethodInfo m30262_MI = 
{
	"Remove", NULL, &t3134_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3134_m30262_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30262_GM};
static MethodInfo* t3134_MIs[] =
{
	&m30254_MI,
	&m30258_MI,
	&m30259_MI,
	&m30260_MI,
	&m30261_MI,
	&m30255_MI,
	&m30262_MI,
	NULL
};
static TypeInfo* t3134_ITIs[] = 
{
	&t603_TI,
	&t3135_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3134_1_0_0;
struct t3134;
extern Il2CppGenericClass t3134_GC;
TypeInfo t3134_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t3134_MIs, t3134_PIs, NULL, NULL, NULL, NULL, NULL, &t3134_TI, t3134_ITIs, NULL, &EmptyCustomAttributesCache, &t3134_TI, &t3134_0_0_0, &t3134_1_0_0, NULL, &t3134_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t3133_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30256_GM;
MethodInfo m30256_MI = 
{
	"GetEnumerator", NULL, &t3135_TI, &t3133_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30256_GM};
static MethodInfo* t3135_MIs[] =
{
	&m30256_MI,
	NULL
};
static TypeInfo* t3135_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3135_1_0_0;
struct t3135;
extern Il2CppGenericClass t3135_GC;
TypeInfo t3135_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3135_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3135_TI, t3135_ITIs, NULL, &EmptyCustomAttributesCache, &t3135_TI, &t3135_0_0_0, &t3135_1_0_0, NULL, &t3135_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Events.PersistentCall>
static PropertyInfo t3133____Current_PropertyInfo = 
{
	&t3133_TI, "Current", &m30257_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3133_PIs[] =
{
	&t3133____Current_PropertyInfo,
	NULL
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30257_GM;
MethodInfo m30257_MI = 
{
	"get_Current", NULL, &t3133_TI, &t565_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30257_GM};
static MethodInfo* t3133_MIs[] =
{
	&m30257_MI,
	NULL
};
static TypeInfo* t3133_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3133_0_0_0;
extern Il2CppType t3133_1_0_0;
struct t3133;
extern Il2CppGenericClass t3133_GC;
TypeInfo t3133_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3133_MIs, t3133_PIs, NULL, NULL, NULL, NULL, NULL, &t3133_TI, t3133_ITIs, NULL, &EmptyCustomAttributesCache, &t3133_TI, &t3133_0_0_0, &t3133_1_0_0, NULL, &t3133_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3139.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3139_TI;
#include "t3139MD.h"

extern MethodInfo m17343_MI;
extern MethodInfo m23051_MI;
struct t20;
#define m23051(__this, p0, method) (t565 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t20_0_0_1;
FieldInfo t3139_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3139_TI, offsetof(t3139, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3139_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3139_TI, offsetof(t3139, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3139_FIs[] =
{
	&t3139_f0_FieldInfo,
	&t3139_f1_FieldInfo,
	NULL
};
extern MethodInfo m17340_MI;
static PropertyInfo t3139____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3139_TI, "System.Collections.IEnumerator.Current", &m17340_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3139____Current_PropertyInfo = 
{
	&t3139_TI, "Current", &m17343_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3139_PIs[] =
{
	&t3139____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3139____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3139_m17339_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17339_GM;
MethodInfo m17339_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3139_m17339_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17339_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17340_GM;
MethodInfo m17340_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3139_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17340_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17341_GM;
MethodInfo m17341_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17341_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17342_GM;
MethodInfo m17342_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17342_GM};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17343_GM;
MethodInfo m17343_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3139_TI, &t565_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17343_GM};
static MethodInfo* t3139_MIs[] =
{
	&m17339_MI,
	&m17340_MI,
	&m17341_MI,
	&m17342_MI,
	&m17343_MI,
	NULL
};
extern MethodInfo m17342_MI;
extern MethodInfo m17341_MI;
static MethodInfo* t3139_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17340_MI,
	&m17342_MI,
	&m17341_MI,
	&m17343_MI,
};
static TypeInfo* t3139_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3133_TI,
};
static Il2CppInterfaceOffsetPair t3139_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3133_TI, 7},
};
extern TypeInfo t565_TI;
static Il2CppRGCTXData t3139_RGCTXData[3] = 
{
	&m17343_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m23051_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3139_0_0_0;
extern Il2CppType t3139_1_0_0;
extern Il2CppGenericClass t3139_GC;
TypeInfo t3139_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3139_MIs, t3139_PIs, t3139_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3139_TI, t3139_ITIs, t3139_VT, &EmptyCustomAttributesCache, &t3139_TI, &t3139_0_0_0, &t3139_1_0_0, t3139_IOs, &t3139_GC, NULL, NULL, NULL, t3139_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3139)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Events.PersistentCall>
extern MethodInfo m30263_MI;
extern MethodInfo m30264_MI;
static PropertyInfo t3140____Item_PropertyInfo = 
{
	&t3140_TI, "Item", &m30263_MI, &m30264_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3140_PIs[] =
{
	&t3140____Item_PropertyInfo,
	NULL
};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3140_m30265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30265_GM;
MethodInfo m30265_MI = 
{
	"IndexOf", NULL, &t3140_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3140_m30265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30265_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3140_m30266_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30266_GM;
MethodInfo m30266_MI = 
{
	"Insert", NULL, &t3140_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3140_m30266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30266_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3140_m30267_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30267_GM;
MethodInfo m30267_MI = 
{
	"RemoveAt", NULL, &t3140_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3140_m30267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30267_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3140_m30263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30263_GM;
MethodInfo m30263_MI = 
{
	"get_Item", NULL, &t3140_TI, &t565_0_0_0, RuntimeInvoker_t29_t44, t3140_m30263_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30263_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3140_m30264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30264_GM;
MethodInfo m30264_MI = 
{
	"set_Item", NULL, &t3140_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3140_m30264_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30264_GM};
static MethodInfo* t3140_MIs[] =
{
	&m30265_MI,
	&m30266_MI,
	&m30267_MI,
	&m30263_MI,
	&m30264_MI,
	NULL
};
static TypeInfo* t3140_ITIs[] = 
{
	&t603_TI,
	&t3134_TI,
	&t3135_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3140_0_0_0;
extern Il2CppType t3140_1_0_0;
struct t3140;
extern Il2CppGenericClass t3140_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t3140_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t3140_MIs, t3140_PIs, NULL, NULL, NULL, NULL, NULL, &t3140_TI, t3140_ITIs, NULL, &t1908__CustomAttributeCache, &t3140_TI, &t3140_0_0_0, &t3140_1_0_0, NULL, &t3140_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17347_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
extern Il2CppType t568_0_0_1;
FieldInfo t661_f0_FieldInfo = 
{
	"l", &t568_0_0_1, &t661_TI, offsetof(t661, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t661_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t661_TI, offsetof(t661, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t661_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t661_TI, offsetof(t661, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t565_0_0_1;
FieldInfo t661_f3_FieldInfo = 
{
	"current", &t565_0_0_1, &t661_TI, offsetof(t661, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t661_FIs[] =
{
	&t661_f0_FieldInfo,
	&t661_f1_FieldInfo,
	&t661_f2_FieldInfo,
	&t661_f3_FieldInfo,
	NULL
};
extern MethodInfo m17345_MI;
static PropertyInfo t661____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t661_TI, "System.Collections.IEnumerator.Current", &m17345_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2985_MI;
static PropertyInfo t661____Current_PropertyInfo = 
{
	&t661_TI, "Current", &m2985_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t661_PIs[] =
{
	&t661____System_Collections_IEnumerator_Current_PropertyInfo,
	&t661____Current_PropertyInfo,
	NULL
};
extern Il2CppType t568_0_0_0;
static ParameterInfo t661_m17344_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17344_GM;
MethodInfo m17344_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t661_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t661_m17344_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17344_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17345_GM;
MethodInfo m17345_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t661_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17345_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17346_GM;
MethodInfo m17346_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t661_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17346_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17347_GM;
MethodInfo m17347_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t661_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17347_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2986_GM;
MethodInfo m2986_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t661_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2986_GM};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2985_GM;
MethodInfo m2985_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t661_TI, &t565_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2985_GM};
static MethodInfo* t661_MIs[] =
{
	&m17344_MI,
	&m17345_MI,
	&m17346_MI,
	&m17347_MI,
	&m2986_MI,
	&m2985_MI,
	NULL
};
extern MethodInfo m2986_MI;
extern MethodInfo m17346_MI;
static MethodInfo* t661_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17345_MI,
	&m2986_MI,
	&m17346_MI,
	&m2985_MI,
};
static TypeInfo* t661_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3133_TI,
};
static Il2CppInterfaceOffsetPair t661_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3133_TI, 7},
};
extern TypeInfo t565_TI;
extern TypeInfo t661_TI;
static Il2CppRGCTXData t661_RGCTXData[3] = 
{
	&m17347_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&t661_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t661_0_0_0;
extern Il2CppType t661_1_0_0;
extern Il2CppGenericClass t661_GC;
extern TypeInfo t1261_TI;
TypeInfo t661_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t661_MIs, t661_PIs, t661_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t661_TI, t661_ITIs, t661_VT, &EmptyCustomAttributesCache, &t661_TI, &t661_0_0_0, &t661_1_0_0, t661_IOs, &t661_GC, NULL, NULL, NULL, t661_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t661)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t3141MD.h"
extern MethodInfo m17377_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m17409_MI;
extern MethodInfo m30261_MI;
extern MethodInfo m30265_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t3140_0_0_1;
FieldInfo t3136_f0_FieldInfo = 
{
	"list", &t3140_0_0_1, &t3136_TI, offsetof(t3136, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3136_FIs[] =
{
	&t3136_f0_FieldInfo,
	NULL
};
extern MethodInfo m17354_MI;
extern MethodInfo m17355_MI;
static PropertyInfo t3136____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3136_TI, "System.Collections.Generic.IList<T>.Item", &m17354_MI, &m17355_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17356_MI;
static PropertyInfo t3136____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3136_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m17356_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17366_MI;
static PropertyInfo t3136____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3136_TI, "System.Collections.ICollection.IsSynchronized", &m17366_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17367_MI;
static PropertyInfo t3136____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3136_TI, "System.Collections.ICollection.SyncRoot", &m17367_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17368_MI;
static PropertyInfo t3136____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3136_TI, "System.Collections.IList.IsFixedSize", &m17368_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17369_MI;
static PropertyInfo t3136____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3136_TI, "System.Collections.IList.IsReadOnly", &m17369_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17370_MI;
extern MethodInfo m17371_MI;
static PropertyInfo t3136____System_Collections_IList_Item_PropertyInfo = 
{
	&t3136_TI, "System.Collections.IList.Item", &m17370_MI, &m17371_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17376_MI;
static PropertyInfo t3136____Count_PropertyInfo = 
{
	&t3136_TI, "Count", &m17376_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3136____Item_PropertyInfo = 
{
	&t3136_TI, "Item", &m17377_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3136_PIs[] =
{
	&t3136____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3136____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3136____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3136____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3136____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3136____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3136____System_Collections_IList_Item_PropertyInfo,
	&t3136____Count_PropertyInfo,
	&t3136____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3140_0_0_0;
static ParameterInfo t3136_m17348_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17348_GM;
MethodInfo m17348_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3136_m17348_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17348_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3136_m17349_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17349_GM;
MethodInfo m17349_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3136_m17349_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17349_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17350_GM;
MethodInfo m17350_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17350_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3136_m17351_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17351_GM;
MethodInfo m17351_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3136_m17351_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17351_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3136_m17352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17352_GM;
MethodInfo m17352_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3136_m17352_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17352_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17353_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17353_GM;
MethodInfo m17353_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3136_m17353_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17353_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17354_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17354_GM;
MethodInfo m17354_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t3136_TI, &t565_0_0_0, RuntimeInvoker_t29_t44, t3136_m17354_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17354_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3136_m17355_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17355_GM;
MethodInfo m17355_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3136_m17355_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17355_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17356_GM;
MethodInfo m17356_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17356_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17357_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17357_GM;
MethodInfo m17357_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3136_m17357_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17357_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17358_GM;
MethodInfo m17358_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t3136_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17358_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3136_m17359_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17359_GM;
MethodInfo m17359_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t3136_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3136_m17359_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17359_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17360_GM;
MethodInfo m17360_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17360_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3136_m17361_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17361_GM;
MethodInfo m17361_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3136_m17361_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17361_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3136_m17362_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17362_GM;
MethodInfo m17362_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t3136_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3136_m17362_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17362_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3136_m17363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17363_GM;
MethodInfo m17363_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3136_m17363_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17363_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3136_m17364_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17364_GM;
MethodInfo m17364_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3136_m17364_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17364_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17365_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17365_GM;
MethodInfo m17365_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3136_m17365_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17365_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17366_GM;
MethodInfo m17366_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17366_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17367_GM;
MethodInfo m17367_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t3136_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17367_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17368_GM;
MethodInfo m17368_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17368_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17369_GM;
MethodInfo m17369_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17369_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17370_GM;
MethodInfo m17370_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t3136_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3136_m17370_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17370_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3136_m17371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17371_GM;
MethodInfo m17371_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3136_m17371_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17371_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3136_m17372_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17372_GM;
MethodInfo m17372_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t3136_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3136_m17372_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17372_GM};
extern Il2CppType t3132_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17373_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3132_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17373_GM;
MethodInfo m17373_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t3136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3136_m17373_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17373_GM};
extern Il2CppType t3133_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17374_GM;
MethodInfo m17374_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t3136_TI, &t3133_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17374_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3136_m17375_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17375_GM;
MethodInfo m17375_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t3136_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3136_m17375_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17375_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17376_GM;
MethodInfo m17376_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t3136_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17376_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3136_m17377_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17377_GM;
MethodInfo m17377_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t3136_TI, &t565_0_0_0, RuntimeInvoker_t29_t44, t3136_m17377_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17377_GM};
static MethodInfo* t3136_MIs[] =
{
	&m17348_MI,
	&m17349_MI,
	&m17350_MI,
	&m17351_MI,
	&m17352_MI,
	&m17353_MI,
	&m17354_MI,
	&m17355_MI,
	&m17356_MI,
	&m17357_MI,
	&m17358_MI,
	&m17359_MI,
	&m17360_MI,
	&m17361_MI,
	&m17362_MI,
	&m17363_MI,
	&m17364_MI,
	&m17365_MI,
	&m17366_MI,
	&m17367_MI,
	&m17368_MI,
	&m17369_MI,
	&m17370_MI,
	&m17371_MI,
	&m17372_MI,
	&m17373_MI,
	&m17374_MI,
	&m17375_MI,
	&m17376_MI,
	&m17377_MI,
	NULL
};
extern MethodInfo m17358_MI;
extern MethodInfo m17357_MI;
extern MethodInfo m17359_MI;
extern MethodInfo m17360_MI;
extern MethodInfo m17361_MI;
extern MethodInfo m17362_MI;
extern MethodInfo m17363_MI;
extern MethodInfo m17364_MI;
extern MethodInfo m17365_MI;
extern MethodInfo m17349_MI;
extern MethodInfo m17350_MI;
extern MethodInfo m17372_MI;
extern MethodInfo m17373_MI;
extern MethodInfo m17352_MI;
extern MethodInfo m17375_MI;
extern MethodInfo m17351_MI;
extern MethodInfo m17353_MI;
extern MethodInfo m17374_MI;
static MethodInfo* t3136_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17358_MI,
	&m17376_MI,
	&m17366_MI,
	&m17367_MI,
	&m17357_MI,
	&m17368_MI,
	&m17369_MI,
	&m17370_MI,
	&m17371_MI,
	&m17359_MI,
	&m17360_MI,
	&m17361_MI,
	&m17362_MI,
	&m17363_MI,
	&m17364_MI,
	&m17365_MI,
	&m17376_MI,
	&m17356_MI,
	&m17349_MI,
	&m17350_MI,
	&m17372_MI,
	&m17373_MI,
	&m17352_MI,
	&m17375_MI,
	&m17351_MI,
	&m17353_MI,
	&m17354_MI,
	&m17355_MI,
	&m17374_MI,
	&m17377_MI,
};
static TypeInfo* t3136_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3134_TI,
	&t3140_TI,
	&t3135_TI,
};
static Il2CppInterfaceOffsetPair t3136_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3134_TI, 20},
	{ &t3140_TI, 27},
	{ &t3135_TI, 32},
};
extern TypeInfo t565_TI;
static Il2CppRGCTXData t3136_RGCTXData[9] = 
{
	&m17377_MI/* Method Usage */,
	&m17409_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m30261_MI/* Method Usage */,
	&m30265_MI/* Method Usage */,
	&m30263_MI/* Method Usage */,
	&m30255_MI/* Method Usage */,
	&m30256_MI/* Method Usage */,
	&m30254_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3136_0_0_0;
extern Il2CppType t3136_1_0_0;
struct t3136;
extern Il2CppGenericClass t3136_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3136_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3136_MIs, t3136_PIs, t3136_FIs, NULL, &t29_TI, NULL, NULL, &t3136_TI, t3136_ITIs, t3136_VT, &t1263__CustomAttributeCache, &t3136_TI, &t3136_0_0_0, &t3136_1_0_0, t3136_IOs, &t3136_GC, NULL, NULL, NULL, t3136_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3136), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3141.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3141_TI;

extern MethodInfo m17412_MI;
extern MethodInfo m17413_MI;
extern MethodInfo m17410_MI;
extern MethodInfo m17408_MI;
extern MethodInfo m2983_MI;
extern MethodInfo m17401_MI;
extern MethodInfo m17411_MI;
extern MethodInfo m17399_MI;
extern MethodInfo m17404_MI;
extern MethodInfo m17395_MI;
extern MethodInfo m30260_MI;
extern MethodInfo m30266_MI;
extern MethodInfo m30267_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t3140_0_0_1;
FieldInfo t3141_f0_FieldInfo = 
{
	"list", &t3140_0_0_1, &t3141_TI, offsetof(t3141, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3141_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3141_TI, offsetof(t3141, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3141_FIs[] =
{
	&t3141_f0_FieldInfo,
	&t3141_f1_FieldInfo,
	NULL
};
extern MethodInfo m17379_MI;
static PropertyInfo t3141____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3141_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m17379_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17387_MI;
static PropertyInfo t3141____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3141_TI, "System.Collections.ICollection.IsSynchronized", &m17387_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17388_MI;
static PropertyInfo t3141____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3141_TI, "System.Collections.ICollection.SyncRoot", &m17388_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17389_MI;
static PropertyInfo t3141____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3141_TI, "System.Collections.IList.IsFixedSize", &m17389_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17390_MI;
static PropertyInfo t3141____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3141_TI, "System.Collections.IList.IsReadOnly", &m17390_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17391_MI;
extern MethodInfo m17392_MI;
static PropertyInfo t3141____System_Collections_IList_Item_PropertyInfo = 
{
	&t3141_TI, "System.Collections.IList.Item", &m17391_MI, &m17392_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17405_MI;
static PropertyInfo t3141____Count_PropertyInfo = 
{
	&t3141_TI, "Count", &m17405_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17406_MI;
extern MethodInfo m17407_MI;
static PropertyInfo t3141____Item_PropertyInfo = 
{
	&t3141_TI, "Item", &m17406_MI, &m17407_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3141_PIs[] =
{
	&t3141____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3141____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3141____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3141____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3141____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3141____System_Collections_IList_Item_PropertyInfo,
	&t3141____Count_PropertyInfo,
	&t3141____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17378_GM;
MethodInfo m17378_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17378_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17379_GM;
MethodInfo m17379_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17379_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3141_m17380_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17380_GM;
MethodInfo m17380_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3141_m17380_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17380_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17381_GM;
MethodInfo m17381_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t3141_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17381_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17382_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17382_GM;
MethodInfo m17382_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t3141_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3141_m17382_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17382_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17383_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17383_GM;
MethodInfo m17383_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3141_m17383_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17383_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17384_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17384_GM;
MethodInfo m17384_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t3141_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3141_m17384_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17384_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17385_GM;
MethodInfo m17385_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3141_m17385_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17385_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17386_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17386_GM;
MethodInfo m17386_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3141_m17386_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17386_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17387_GM;
MethodInfo m17387_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17387_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17388_GM;
MethodInfo m17388_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t3141_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17388_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17389_GM;
MethodInfo m17389_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17389_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17390_GM;
MethodInfo m17390_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17390_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3141_m17391_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17391_GM;
MethodInfo m17391_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t3141_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3141_m17391_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17391_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17392_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17392_GM;
MethodInfo m17392_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3141_m17392_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17392_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17393_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17393_GM;
MethodInfo m17393_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3141_m17393_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17393_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17394_GM;
MethodInfo m17394_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17394_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17395_GM;
MethodInfo m17395_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17395_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17396_GM;
MethodInfo m17396_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3141_m17396_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17396_GM};
extern Il2CppType t3132_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3141_m17397_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3132_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17397_GM;
MethodInfo m17397_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3141_m17397_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17397_GM};
extern Il2CppType t3133_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17398_GM;
MethodInfo m17398_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t3141_TI, &t3133_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17398_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17399_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17399_GM;
MethodInfo m17399_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t3141_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3141_m17399_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17399_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17400_GM;
MethodInfo m17400_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3141_m17400_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17400_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17401_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17401_GM;
MethodInfo m17401_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3141_m17401_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17401_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17402_GM;
MethodInfo m17402_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3141_m17402_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17402_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3141_m17403_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17403_GM;
MethodInfo m17403_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3141_m17403_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17403_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3141_m17404_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17404_GM;
MethodInfo m17404_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3141_m17404_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17404_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17405_GM;
MethodInfo m17405_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t3141_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17405_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3141_m17406_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17406_GM;
MethodInfo m17406_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t3141_TI, &t565_0_0_0, RuntimeInvoker_t29_t44, t3141_m17406_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17406_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17407_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17407_GM;
MethodInfo m17407_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3141_m17407_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17407_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3141_m17408_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17408_GM;
MethodInfo m17408_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3141_m17408_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17408_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17409_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17409_GM;
MethodInfo m17409_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3141_m17409_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17409_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3141_m17410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t565_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17410_GM;
MethodInfo m17410_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t3141_TI, &t565_0_0_0, RuntimeInvoker_t29_t29, t3141_m17410_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17410_GM};
extern Il2CppType t3140_0_0_0;
static ParameterInfo t3141_m17411_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17411_GM;
MethodInfo m17411_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t3141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3141_m17411_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17411_GM};
extern Il2CppType t3140_0_0_0;
static ParameterInfo t3141_m17412_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3140_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17412_GM;
MethodInfo m17412_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3141_m17412_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17412_GM};
extern Il2CppType t3140_0_0_0;
static ParameterInfo t3141_m17413_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3140_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17413_GM;
MethodInfo m17413_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t3141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3141_m17413_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17413_GM};
static MethodInfo* t3141_MIs[] =
{
	&m17378_MI,
	&m17379_MI,
	&m17380_MI,
	&m17381_MI,
	&m17382_MI,
	&m17383_MI,
	&m17384_MI,
	&m17385_MI,
	&m17386_MI,
	&m17387_MI,
	&m17388_MI,
	&m17389_MI,
	&m17390_MI,
	&m17391_MI,
	&m17392_MI,
	&m17393_MI,
	&m17394_MI,
	&m17395_MI,
	&m17396_MI,
	&m17397_MI,
	&m17398_MI,
	&m17399_MI,
	&m17400_MI,
	&m17401_MI,
	&m17402_MI,
	&m17403_MI,
	&m17404_MI,
	&m17405_MI,
	&m17406_MI,
	&m17407_MI,
	&m17408_MI,
	&m17409_MI,
	&m17410_MI,
	&m17411_MI,
	&m17412_MI,
	&m17413_MI,
	NULL
};
extern MethodInfo m17381_MI;
extern MethodInfo m17380_MI;
extern MethodInfo m17382_MI;
extern MethodInfo m17394_MI;
extern MethodInfo m17383_MI;
extern MethodInfo m17384_MI;
extern MethodInfo m17385_MI;
extern MethodInfo m17386_MI;
extern MethodInfo m17403_MI;
extern MethodInfo m17393_MI;
extern MethodInfo m17396_MI;
extern MethodInfo m17397_MI;
extern MethodInfo m17402_MI;
extern MethodInfo m17400_MI;
extern MethodInfo m17398_MI;
static MethodInfo* t3141_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17381_MI,
	&m17405_MI,
	&m17387_MI,
	&m17388_MI,
	&m17380_MI,
	&m17389_MI,
	&m17390_MI,
	&m17391_MI,
	&m17392_MI,
	&m17382_MI,
	&m17394_MI,
	&m17383_MI,
	&m17384_MI,
	&m17385_MI,
	&m17386_MI,
	&m17403_MI,
	&m17405_MI,
	&m17379_MI,
	&m17393_MI,
	&m17394_MI,
	&m17396_MI,
	&m17397_MI,
	&m17402_MI,
	&m17399_MI,
	&m17400_MI,
	&m17403_MI,
	&m17406_MI,
	&m17407_MI,
	&m17398_MI,
	&m17395_MI,
	&m17401_MI,
	&m17404_MI,
	&m17408_MI,
};
static TypeInfo* t3141_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3134_TI,
	&t3140_TI,
	&t3135_TI,
};
static Il2CppInterfaceOffsetPair t3141_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3134_TI, 20},
	{ &t3140_TI, 27},
	{ &t3135_TI, 32},
};
extern TypeInfo t568_TI;
extern TypeInfo t565_TI;
static Il2CppRGCTXData t3141_RGCTXData[25] = 
{
	&t568_TI/* Class Usage */,
	&m2983_MI/* Method Usage */,
	&m30258_MI/* Method Usage */,
	&m30256_MI/* Method Usage */,
	&m30254_MI/* Method Usage */,
	&m17410_MI/* Method Usage */,
	&m17401_MI/* Method Usage */,
	&m17409_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m30261_MI/* Method Usage */,
	&m30265_MI/* Method Usage */,
	&m17411_MI/* Method Usage */,
	&m17399_MI/* Method Usage */,
	&m17404_MI/* Method Usage */,
	&m17412_MI/* Method Usage */,
	&m17413_MI/* Method Usage */,
	&m30263_MI/* Method Usage */,
	&m17408_MI/* Method Usage */,
	&m17395_MI/* Method Usage */,
	&m30260_MI/* Method Usage */,
	&m30255_MI/* Method Usage */,
	&m30266_MI/* Method Usage */,
	&m30267_MI/* Method Usage */,
	&m30264_MI/* Method Usage */,
	&t565_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3141_0_0_0;
extern Il2CppType t3141_1_0_0;
struct t3141;
extern Il2CppGenericClass t3141_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3141_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3141_MIs, t3141_PIs, t3141_FIs, NULL, &t29_TI, NULL, NULL, &t3141_TI, t3141_ITIs, t3141_VT, &t1262__CustomAttributeCache, &t3141_TI, &t3141_0_0_0, &t3141_1_0_0, t3141_IOs, &t3141_GC, NULL, NULL, NULL, t3141_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3141), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3142_TI;
#include "t3142MD.h"

#include "t3143.h"
extern TypeInfo t6751_TI;
extern TypeInfo t3143_TI;
#include "t3143MD.h"
extern Il2CppType t6751_0_0_0;
extern MethodInfo m17419_MI;
extern MethodInfo m30268_MI;
extern MethodInfo m23063_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t3142_0_0_49;
FieldInfo t3142_f0_FieldInfo = 
{
	"_default", &t3142_0_0_49, &t3142_TI, offsetof(t3142_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3142_FIs[] =
{
	&t3142_f0_FieldInfo,
	NULL
};
extern MethodInfo m17418_MI;
static PropertyInfo t3142____Default_PropertyInfo = 
{
	&t3142_TI, "Default", &m17418_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3142_PIs[] =
{
	&t3142____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17414_GM;
MethodInfo m17414_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t3142_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17414_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17415_GM;
MethodInfo m17415_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t3142_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17415_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3142_m17416_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17416_GM;
MethodInfo m17416_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t3142_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3142_m17416_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17416_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3142_m17417_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17417_GM;
MethodInfo m17417_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t3142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3142_m17417_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17417_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3142_m30268_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30268_GM;
MethodInfo m30268_MI = 
{
	"GetHashCode", NULL, &t3142_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3142_m30268_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30268_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3142_m23063_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23063_GM;
MethodInfo m23063_MI = 
{
	"Equals", NULL, &t3142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3142_m23063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m23063_GM};
extern Il2CppType t3142_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17418_GM;
MethodInfo m17418_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t3142_TI, &t3142_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17418_GM};
static MethodInfo* t3142_MIs[] =
{
	&m17414_MI,
	&m17415_MI,
	&m17416_MI,
	&m17417_MI,
	&m30268_MI,
	&m23063_MI,
	&m17418_MI,
	NULL
};
extern MethodInfo m17417_MI;
extern MethodInfo m17416_MI;
static MethodInfo* t3142_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m23063_MI,
	&m30268_MI,
	&m17417_MI,
	&m17416_MI,
	NULL,
	NULL,
};
extern TypeInfo t6752_TI;
static TypeInfo* t3142_ITIs[] = 
{
	&t6752_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3142_IOs[] = 
{
	{ &t6752_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3142_TI;
extern TypeInfo t3142_TI;
extern TypeInfo t3143_TI;
extern TypeInfo t565_TI;
static Il2CppRGCTXData t3142_RGCTXData[9] = 
{
	&t6751_0_0_0/* Type Usage */,
	&t565_0_0_0/* Type Usage */,
	&t3142_TI/* Class Usage */,
	&t3142_TI/* Static Usage */,
	&t3143_TI/* Class Usage */,
	&m17419_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m30268_MI/* Method Usage */,
	&m23063_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3142_0_0_0;
extern Il2CppType t3142_1_0_0;
struct t3142;
extern Il2CppGenericClass t3142_GC;
TypeInfo t3142_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3142_MIs, t3142_PIs, t3142_FIs, NULL, &t29_TI, NULL, NULL, &t3142_TI, t3142_ITIs, t3142_VT, &EmptyCustomAttributesCache, &t3142_TI, &t3142_0_0_0, &t3142_1_0_0, t3142_IOs, &t3142_GC, NULL, NULL, NULL, t3142_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3142), 0, -1, sizeof(t3142_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t6752_m30269_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30269_GM;
MethodInfo m30269_MI = 
{
	"Equals", NULL, &t6752_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6752_m30269_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30269_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t6752_m30270_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30270_GM;
MethodInfo m30270_MI = 
{
	"GetHashCode", NULL, &t6752_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6752_m30270_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30270_GM};
static MethodInfo* t6752_MIs[] =
{
	&m30269_MI,
	&m30270_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6752_0_0_0;
extern Il2CppType t6752_1_0_0;
struct t6752;
extern Il2CppGenericClass t6752_GC;
TypeInfo t6752_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6752_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6752_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6752_TI, &t6752_0_0_0, &t6752_1_0_0, NULL, &t6752_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t565_0_0_0;
static ParameterInfo t6751_m30271_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30271_GM;
MethodInfo m30271_MI = 
{
	"Equals", NULL, &t6751_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6751_m30271_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30271_GM};
static MethodInfo* t6751_MIs[] =
{
	&m30271_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6751_1_0_0;
struct t6751;
extern Il2CppGenericClass t6751_GC;
TypeInfo t6751_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6751_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6751_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6751_TI, &t6751_0_0_0, &t6751_1_0_0, NULL, &t6751_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17414_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Events.PersistentCall>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17419_GM;
MethodInfo m17419_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t3143_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17419_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3143_m17420_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17420_GM;
MethodInfo m17420_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t3143_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3143_m17420_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17420_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3143_m17421_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17421_GM;
MethodInfo m17421_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t3143_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3143_m17421_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17421_GM};
static MethodInfo* t3143_MIs[] =
{
	&m17419_MI,
	&m17420_MI,
	&m17421_MI,
	NULL
};
extern MethodInfo m17421_MI;
extern MethodInfo m17420_MI;
static MethodInfo* t3143_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17421_MI,
	&m17420_MI,
	&m17417_MI,
	&m17416_MI,
	&m17420_MI,
	&m17421_MI,
};
static Il2CppInterfaceOffsetPair t3143_IOs[] = 
{
	{ &t6752_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3142_TI;
extern TypeInfo t3142_TI;
extern TypeInfo t3143_TI;
extern TypeInfo t565_TI;
extern TypeInfo t565_TI;
static Il2CppRGCTXData t3143_RGCTXData[11] = 
{
	&t6751_0_0_0/* Type Usage */,
	&t565_0_0_0/* Type Usage */,
	&t3142_TI/* Class Usage */,
	&t3142_TI/* Static Usage */,
	&t3143_TI/* Class Usage */,
	&m17419_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m30268_MI/* Method Usage */,
	&m23063_MI/* Method Usage */,
	&m17414_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3143_0_0_0;
extern Il2CppType t3143_1_0_0;
struct t3143;
extern Il2CppGenericClass t3143_GC;
TypeInfo t3143_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3143_MIs, NULL, NULL, NULL, &t3142_TI, NULL, &t1256_TI, &t3143_TI, NULL, t3143_VT, &EmptyCustomAttributesCache, &t3143_TI, &t3143_0_0_0, &t3143_1_0_0, t3143_IOs, &t3143_GC, NULL, NULL, NULL, t3143_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3143), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3137_m17422_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17422_GM;
MethodInfo m17422_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t3137_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3137_m17422_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17422_GM};
extern Il2CppType t565_0_0_0;
static ParameterInfo t3137_m17423_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17423_GM;
MethodInfo m17423_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t3137_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3137_m17423_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17423_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3137_m17424_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17424_GM;
MethodInfo m17424_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t3137_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3137_m17424_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17424_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3137_m17425_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17425_GM;
MethodInfo m17425_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t3137_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3137_m17425_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17425_GM};
static MethodInfo* t3137_MIs[] =
{
	&m17422_MI,
	&m17423_MI,
	&m17424_MI,
	&m17425_MI,
	NULL
};
extern MethodInfo m17424_MI;
extern MethodInfo m17425_MI;
static MethodInfo* t3137_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17423_MI,
	&m17424_MI,
	&m17425_MI,
};
static Il2CppInterfaceOffsetPair t3137_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3137_1_0_0;
struct t3137;
extern Il2CppGenericClass t3137_GC;
TypeInfo t3137_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t3137_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3137_TI, NULL, t3137_VT, &EmptyCustomAttributesCache, &t3137_TI, &t3137_0_0_0, &t3137_1_0_0, t3137_IOs, &t3137_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3137), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t3145.h"
extern TypeInfo t4507_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t3145_TI;
#include "t3145MD.h"
extern Il2CppType t4507_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m17430_MI;
extern MethodInfo m30272_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t3144_0_0_49;
FieldInfo t3144_f0_FieldInfo = 
{
	"_default", &t3144_0_0_49, &t3144_TI, offsetof(t3144_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3144_FIs[] =
{
	&t3144_f0_FieldInfo,
	NULL
};
static PropertyInfo t3144____Default_PropertyInfo = 
{
	&t3144_TI, "Default", &m17429_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3144_PIs[] =
{
	&t3144____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17426_GM;
MethodInfo m17426_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t3144_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17426_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17427_GM;
MethodInfo m17427_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t3144_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17427_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3144_m17428_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17428_GM;
MethodInfo m17428_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t3144_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3144_m17428_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17428_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3144_m30272_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30272_GM;
MethodInfo m30272_MI = 
{
	"Compare", NULL, &t3144_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3144_m30272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30272_GM};
extern Il2CppType t3144_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17429_GM;
MethodInfo m17429_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t3144_TI, &t3144_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17429_GM};
static MethodInfo* t3144_MIs[] =
{
	&m17426_MI,
	&m17427_MI,
	&m17428_MI,
	&m30272_MI,
	&m17429_MI,
	NULL
};
extern MethodInfo m17428_MI;
static MethodInfo* t3144_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m30272_MI,
	&m17428_MI,
	NULL,
};
extern TypeInfo t4506_TI;
extern TypeInfo t726_TI;
static TypeInfo* t3144_ITIs[] = 
{
	&t4506_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3144_IOs[] = 
{
	{ &t4506_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3144_TI;
extern TypeInfo t3144_TI;
extern TypeInfo t3145_TI;
extern TypeInfo t565_TI;
static Il2CppRGCTXData t3144_RGCTXData[8] = 
{
	&t4507_0_0_0/* Type Usage */,
	&t565_0_0_0/* Type Usage */,
	&t3144_TI/* Class Usage */,
	&t3144_TI/* Static Usage */,
	&t3145_TI/* Class Usage */,
	&m17430_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m30272_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3144_0_0_0;
extern Il2CppType t3144_1_0_0;
struct t3144;
extern Il2CppGenericClass t3144_GC;
TypeInfo t3144_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3144_MIs, t3144_PIs, t3144_FIs, NULL, &t29_TI, NULL, NULL, &t3144_TI, t3144_ITIs, t3144_VT, &EmptyCustomAttributesCache, &t3144_TI, &t3144_0_0_0, &t3144_1_0_0, t3144_IOs, &t3144_GC, NULL, NULL, NULL, t3144_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3144), 0, -1, sizeof(t3144_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t4506_m23071_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23071_GM;
MethodInfo m23071_MI = 
{
	"Compare", NULL, &t4506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4506_m23071_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m23071_GM};
static MethodInfo* t4506_MIs[] =
{
	&m23071_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4506_0_0_0;
extern Il2CppType t4506_1_0_0;
struct t4506;
extern Il2CppGenericClass t4506_GC;
TypeInfo t4506_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4506_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4506_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4506_TI, &t4506_0_0_0, &t4506_1_0_0, NULL, &t4506_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t565_0_0_0;
static ParameterInfo t4507_m23072_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23072_GM;
MethodInfo m23072_MI = 
{
	"CompareTo", NULL, &t4507_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4507_m23072_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m23072_GM};
static MethodInfo* t4507_MIs[] =
{
	&m23072_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4507_1_0_0;
struct t4507;
extern Il2CppGenericClass t4507_GC;
TypeInfo t4507_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4507_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4507_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4507_TI, &t4507_0_0_0, &t4507_1_0_0, NULL, &t4507_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m17426_MI;
extern MethodInfo m23072_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.PersistentCall>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17430_GM;
MethodInfo m17430_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t3145_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17430_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3145_m17431_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17431_GM;
MethodInfo m17431_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t3145_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3145_m17431_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17431_GM};
static MethodInfo* t3145_MIs[] =
{
	&m17430_MI,
	&m17431_MI,
	NULL
};
extern MethodInfo m17431_MI;
static MethodInfo* t3145_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17431_MI,
	&m17428_MI,
	&m17431_MI,
};
static Il2CppInterfaceOffsetPair t3145_IOs[] = 
{
	{ &t4506_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3144_TI;
extern TypeInfo t3144_TI;
extern TypeInfo t3145_TI;
extern TypeInfo t565_TI;
extern TypeInfo t565_TI;
extern TypeInfo t4507_TI;
static Il2CppRGCTXData t3145_RGCTXData[12] = 
{
	&t4507_0_0_0/* Type Usage */,
	&t565_0_0_0/* Type Usage */,
	&t3144_TI/* Class Usage */,
	&t3144_TI/* Static Usage */,
	&t3145_TI/* Class Usage */,
	&m17430_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&m30272_MI/* Method Usage */,
	&m17426_MI/* Method Usage */,
	&t565_TI/* Class Usage */,
	&t4507_TI/* Class Usage */,
	&m23072_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3145_0_0_0;
extern Il2CppType t3145_1_0_0;
struct t3145;
extern Il2CppGenericClass t3145_GC;
extern TypeInfo t1246_TI;
TypeInfo t3145_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3145_MIs, NULL, NULL, NULL, &t3144_TI, NULL, &t1246_TI, &t3145_TI, NULL, t3145_VT, &EmptyCustomAttributesCache, &t3145_TI, &t3145_0_0_0, &t3145_1_0_0, t3145_IOs, &t3145_GC, NULL, NULL, NULL, t3145_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3145), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3138_TI;
#include "t3138MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.Events.PersistentCall>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3138_m17432_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17432_GM;
MethodInfo m17432_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t3138_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3138_m17432_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17432_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
static ParameterInfo t3138_m17433_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17433_GM;
MethodInfo m17433_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t3138_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3138_m17433_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17433_GM};
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3138_m17434_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t565_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17434_GM;
MethodInfo m17434_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t3138_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t3138_m17434_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17434_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3138_m17435_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17435_GM;
MethodInfo m17435_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t3138_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3138_m17435_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17435_GM};
static MethodInfo* t3138_MIs[] =
{
	&m17432_MI,
	&m17433_MI,
	&m17434_MI,
	&m17435_MI,
	NULL
};
extern MethodInfo m17433_MI;
extern MethodInfo m17434_MI;
extern MethodInfo m17435_MI;
static MethodInfo* t3138_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17433_MI,
	&m17434_MI,
	&m17435_MI,
};
static Il2CppInterfaceOffsetPair t3138_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3138_1_0_0;
struct t3138;
extern Il2CppGenericClass t3138_GC;
TypeInfo t3138_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3138_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3138_TI, NULL, t3138_VT, &EmptyCustomAttributesCache, &t3138_TI, &t3138_0_0_0, &t3138_1_0_0, t3138_IOs, &t3138_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3138), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t570.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t570_TI;
#include "t570MD.h"

#include "t3152.h"
#include "t3150.h"
#include "t662.h"
#include "t3158.h"
#include "t3151.h"
extern TypeInfo t3146_TI;
extern TypeInfo t3152_TI;
extern TypeInfo t3148_TI;
extern TypeInfo t3149_TI;
extern TypeInfo t3147_TI;
extern TypeInfo t3150_TI;
extern TypeInfo t662_TI;
extern TypeInfo t3158_TI;
#include "t3150MD.h"
#include "t662MD.h"
#include "t3152MD.h"
#include "t3158MD.h"
extern MethodInfo m2989_MI;
extern MethodInfo m17476_MI;
extern MethodInfo m23088_MI;
extern MethodInfo m17464_MI;
extern MethodInfo m17461_MI;
extern MethodInfo m2988_MI;
extern MethodInfo m2991_MI;
extern MethodInfo m17462_MI;
extern MethodInfo m17465_MI;
extern MethodInfo m17467_MI;
extern MethodInfo m17453_MI;
extern MethodInfo m17474_MI;
extern MethodInfo m17475_MI;
extern MethodInfo m30273_MI;
extern MethodInfo m30274_MI;
extern MethodInfo m30275_MI;
extern MethodInfo m30276_MI;
extern MethodInfo m17466_MI;
extern MethodInfo m17454_MI;
extern MethodInfo m17455_MI;
extern MethodInfo m17488_MI;
extern MethodInfo m23090_MI;
extern MethodInfo m17459_MI;
extern MethodInfo m17460_MI;
extern MethodInfo m17562_MI;
extern MethodInfo m17482_MI;
extern MethodInfo m17463_MI;
extern MethodInfo m17468_MI;
extern MethodInfo m17568_MI;
extern MethodInfo m23092_MI;
extern MethodInfo m23100_MI;
struct t20;
#define m23088(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t3156.h"
#define m23090(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
#define m23092(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#define m23100(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t3152  m17461 (t570 * __this, MethodInfo* method){
	{
		t3152  L_0 = {0};
		m17482(&L_0, __this, &m17482_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t44_0_0_32849;
FieldInfo t570_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t570_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t3146_0_0_1;
FieldInfo t570_f1_FieldInfo = 
{
	"_items", &t3146_0_0_1, &t570_TI, offsetof(t570, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t570_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t570_TI, offsetof(t570, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t570_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t570_TI, offsetof(t570, f3), &EmptyCustomAttributesCache};
extern Il2CppType t3146_0_0_49;
FieldInfo t570_f4_FieldInfo = 
{
	"EmptyArray", &t3146_0_0_49, &t570_TI, offsetof(t570_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t570_FIs[] =
{
	&t570_f0_FieldInfo,
	&t570_f1_FieldInfo,
	&t570_f2_FieldInfo,
	&t570_f3_FieldInfo,
	&t570_f4_FieldInfo,
	NULL
};
static const int32_t t570_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t570_f0_DefaultValue = 
{
	&t570_f0_FieldInfo, { (char*)&t570_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t570_FDVs[] = 
{
	&t570_f0_DefaultValue,
	NULL
};
extern MethodInfo m17446_MI;
static PropertyInfo t570____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t570_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m17446_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17447_MI;
static PropertyInfo t570____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t570_TI, "System.Collections.ICollection.IsSynchronized", &m17447_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17448_MI;
static PropertyInfo t570____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t570_TI, "System.Collections.ICollection.SyncRoot", &m17448_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17449_MI;
static PropertyInfo t570____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t570_TI, "System.Collections.IList.IsFixedSize", &m17449_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17450_MI;
static PropertyInfo t570____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t570_TI, "System.Collections.IList.IsReadOnly", &m17450_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17451_MI;
extern MethodInfo m17452_MI;
static PropertyInfo t570____System_Collections_IList_Item_PropertyInfo = 
{
	&t570_TI, "System.Collections.IList.Item", &m17451_MI, &m17452_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t570____Capacity_PropertyInfo = 
{
	&t570_TI, "Capacity", &m17474_MI, &m17475_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2990_MI;
static PropertyInfo t570____Count_PropertyInfo = 
{
	&t570_TI, "Count", &m2990_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t570____Item_PropertyInfo = 
{
	&t570_TI, "Item", &m2989_MI, &m17476_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t570_PIs[] =
{
	&t570____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t570____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t570____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t570____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t570____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t570____System_Collections_IList_Item_PropertyInfo,
	&t570____Capacity_PropertyInfo,
	&t570____Count_PropertyInfo,
	&t570____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2987_GM;
MethodInfo m2987_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2987_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17436_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17436_GM;
MethodInfo m17436_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t570_m17436_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17436_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17437_GM;
MethodInfo m17437_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17437_GM};
extern Il2CppType t3147_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17438_GM;
MethodInfo m17438_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t570_TI, &t3147_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17438_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17439_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17439_GM;
MethodInfo m17439_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t570_m17439_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17439_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17440_GM;
MethodInfo m17440_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t570_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17440_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t570_m17441_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17441_GM;
MethodInfo m17441_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t570_m17441_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17441_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t570_m17442_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17442_GM;
MethodInfo m17442_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t570_m17442_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17442_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t570_m17443_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17443_GM;
MethodInfo m17443_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t570_m17443_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17443_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t570_m17444_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17444_GM;
MethodInfo m17444_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t570_m17444_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17444_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t570_m17445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17445_GM;
MethodInfo m17445_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m17445_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17445_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17446_GM;
MethodInfo m17446_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17446_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17447_GM;
MethodInfo m17447_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17447_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17448_GM;
MethodInfo m17448_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t570_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17448_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17449_GM;
MethodInfo m17449_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17449_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17450_GM;
MethodInfo m17450_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17450_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17451_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17451_GM;
MethodInfo m17451_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t570_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t570_m17451_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17451_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t570_m17452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17452_GM;
MethodInfo m17452_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t570_m17452_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17452_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t570_m2988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2988_GM;
MethodInfo m2988_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m2988_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2988_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17453_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17453_GM;
MethodInfo m17453_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t570_m17453_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17453_GM};
extern Il2CppType t3148_0_0_0;
extern Il2CppType t3148_0_0_0;
static ParameterInfo t570_m17454_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17454_GM;
MethodInfo m17454_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m17454_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17454_GM};
extern Il2CppType t3149_0_0_0;
extern Il2CppType t3149_0_0_0;
static ParameterInfo t570_m17455_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17455_GM;
MethodInfo m17455_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m17455_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17455_GM};
extern Il2CppType t3149_0_0_0;
static ParameterInfo t570_m2995_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2995_GM;
MethodInfo m2995_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m2995_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2995_GM};
extern Il2CppType t3150_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17456_GM;
MethodInfo m17456_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t570_TI, &t3150_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17456_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2994_GM;
MethodInfo m2994_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2994_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t570_m2991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2991_GM;
MethodInfo m2991_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t570_m2991_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2991_GM};
extern Il2CppType t3146_0_0_0;
extern Il2CppType t3146_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17457_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3146_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17457_GM;
MethodInfo m17457_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t570_m17457_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17457_GM};
extern Il2CppType t662_0_0_0;
extern Il2CppType t662_0_0_0;
static ParameterInfo t570_m17458_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t662_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17458_GM;
MethodInfo m17458_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t570_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t570_m17458_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17458_GM};
extern Il2CppType t662_0_0_0;
static ParameterInfo t570_m17459_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17459_GM;
MethodInfo m17459_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m17459_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17459_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t662_0_0_0;
static ParameterInfo t570_m17460_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t662_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17460_GM;
MethodInfo m17460_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t570_m17460_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17460_GM};
extern Il2CppType t3152_0_0_0;
extern void* RuntimeInvoker_t3152 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17461_GM;
MethodInfo m17461_MI = 
{
	"GetEnumerator", (methodPointerType)&m17461, &t570_TI, &t3152_0_0_0, RuntimeInvoker_t3152, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17461_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t570_m17462_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17462_GM;
MethodInfo m17462_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t570_m17462_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17462_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17463_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17463_GM;
MethodInfo m17463_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t570_m17463_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17463_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17464_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17464_GM;
MethodInfo m17464_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t570_m17464_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17464_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t570_m17465_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17465_GM;
MethodInfo m17465_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t570_m17465_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17465_GM};
extern Il2CppType t3149_0_0_0;
static ParameterInfo t570_m17466_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17466_GM;
MethodInfo m17466_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m17466_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17466_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t570_m17467_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17467_GM;
MethodInfo m17467_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t570_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t570_m17467_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17467_GM};
extern Il2CppType t662_0_0_0;
static ParameterInfo t570_m2993_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t662_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2993_GM;
MethodInfo m2993_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t570_m2993_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2993_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17468_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17468_GM;
MethodInfo m17468_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t570_m17468_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17468_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17469_GM;
MethodInfo m17469_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17469_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17470_GM;
MethodInfo m17470_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17470_GM};
extern Il2CppType t3151_0_0_0;
extern Il2CppType t3151_0_0_0;
static ParameterInfo t570_m17471_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17471_GM;
MethodInfo m17471_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t570_m17471_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17471_GM};
extern Il2CppType t3146_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17472_GM;
MethodInfo m17472_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t570_TI, &t3146_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17472_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17473_GM;
MethodInfo m17473_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17473_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17474_GM;
MethodInfo m17474_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17474_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m17475_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17475_GM;
MethodInfo m17475_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t570_m17475_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17475_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2990_GM;
MethodInfo m2990_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t570_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2990_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t570_m2989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2989_GM;
MethodInfo m2989_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t570_TI, &t556_0_0_0, RuntimeInvoker_t29_t44, t570_m2989_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2989_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t570_m17476_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17476_GM;
MethodInfo m17476_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t570_m17476_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17476_GM};
static MethodInfo* t570_MIs[] =
{
	&m2987_MI,
	&m17436_MI,
	&m17437_MI,
	&m17438_MI,
	&m17439_MI,
	&m17440_MI,
	&m17441_MI,
	&m17442_MI,
	&m17443_MI,
	&m17444_MI,
	&m17445_MI,
	&m17446_MI,
	&m17447_MI,
	&m17448_MI,
	&m17449_MI,
	&m17450_MI,
	&m17451_MI,
	&m17452_MI,
	&m2988_MI,
	&m17453_MI,
	&m17454_MI,
	&m17455_MI,
	&m2995_MI,
	&m17456_MI,
	&m2994_MI,
	&m2991_MI,
	&m17457_MI,
	&m17458_MI,
	&m17459_MI,
	&m17460_MI,
	&m17461_MI,
	&m17462_MI,
	&m17463_MI,
	&m17464_MI,
	&m17465_MI,
	&m17466_MI,
	&m17467_MI,
	&m2993_MI,
	&m17468_MI,
	&m17469_MI,
	&m17470_MI,
	&m17471_MI,
	&m17472_MI,
	&m17473_MI,
	&m17474_MI,
	&m17475_MI,
	&m2990_MI,
	&m2989_MI,
	&m17476_MI,
	NULL
};
extern MethodInfo m17440_MI;
extern MethodInfo m17439_MI;
extern MethodInfo m17441_MI;
extern MethodInfo m2994_MI;
extern MethodInfo m17442_MI;
extern MethodInfo m17443_MI;
extern MethodInfo m17444_MI;
extern MethodInfo m17445_MI;
extern MethodInfo m17457_MI;
extern MethodInfo m17438_MI;
static MethodInfo* t570_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17440_MI,
	&m2990_MI,
	&m17447_MI,
	&m17448_MI,
	&m17439_MI,
	&m17449_MI,
	&m17450_MI,
	&m17451_MI,
	&m17452_MI,
	&m17441_MI,
	&m2994_MI,
	&m17442_MI,
	&m17443_MI,
	&m17444_MI,
	&m17445_MI,
	&m17468_MI,
	&m2990_MI,
	&m17446_MI,
	&m2988_MI,
	&m2994_MI,
	&m2991_MI,
	&m17457_MI,
	&m17467_MI,
	&m17438_MI,
	&m17462_MI,
	&m17465_MI,
	&m17468_MI,
	&m2989_MI,
	&m17476_MI,
};
extern TypeInfo t3154_TI;
static TypeInfo* t570_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3148_TI,
	&t3149_TI,
	&t3154_TI,
};
static Il2CppInterfaceOffsetPair t570_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3148_TI, 20},
	{ &t3149_TI, 27},
	{ &t3154_TI, 28},
};
extern TypeInfo t570_TI;
extern TypeInfo t3146_TI;
extern TypeInfo t3152_TI;
extern TypeInfo t556_TI;
extern TypeInfo t3148_TI;
extern TypeInfo t3150_TI;
static Il2CppRGCTXData t570_RGCTXData[37] = 
{
	&t570_TI/* Static Usage */,
	&t3146_TI/* Array Usage */,
	&m17461_MI/* Method Usage */,
	&t3152_TI/* Class Usage */,
	&t556_TI/* Class Usage */,
	&m2988_MI/* Method Usage */,
	&m2991_MI/* Method Usage */,
	&m17462_MI/* Method Usage */,
	&m17464_MI/* Method Usage */,
	&m17465_MI/* Method Usage */,
	&m17467_MI/* Method Usage */,
	&m2989_MI/* Method Usage */,
	&m17476_MI/* Method Usage */,
	&m17453_MI/* Method Usage */,
	&m17474_MI/* Method Usage */,
	&m17475_MI/* Method Usage */,
	&m30273_MI/* Method Usage */,
	&m30274_MI/* Method Usage */,
	&m30275_MI/* Method Usage */,
	&m30276_MI/* Method Usage */,
	&m17466_MI/* Method Usage */,
	&t3148_TI/* Class Usage */,
	&m17454_MI/* Method Usage */,
	&m17455_MI/* Method Usage */,
	&t3150_TI/* Class Usage */,
	&m17488_MI/* Method Usage */,
	&m23090_MI/* Method Usage */,
	&m17459_MI/* Method Usage */,
	&m17460_MI/* Method Usage */,
	&m17562_MI/* Method Usage */,
	&m17482_MI/* Method Usage */,
	&m17463_MI/* Method Usage */,
	&m17468_MI/* Method Usage */,
	&m17568_MI/* Method Usage */,
	&m23092_MI/* Method Usage */,
	&m23100_MI/* Method Usage */,
	&m23088_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t570_0_0_0;
extern Il2CppType t570_1_0_0;
struct t570;
extern Il2CppGenericClass t570_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t570_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t570_MIs, t570_PIs, t570_FIs, NULL, &t29_TI, NULL, NULL, &t570_TI, t570_ITIs, t570_VT, &t1261__CustomAttributeCache, &t570_TI, &t570_0_0_0, &t570_1_0_0, t570_IOs, &t570_GC, NULL, t570_FDVs, NULL, t570_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t570), 0, -1, sizeof(t570_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Events.BaseInvokableCall>
static PropertyInfo t3148____Count_PropertyInfo = 
{
	&t3148_TI, "Count", &m30273_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30277_MI;
static PropertyInfo t3148____IsReadOnly_PropertyInfo = 
{
	&t3148_TI, "IsReadOnly", &m30277_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3148_PIs[] =
{
	&t3148____Count_PropertyInfo,
	&t3148____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30273_GM;
MethodInfo m30273_MI = 
{
	"get_Count", NULL, &t3148_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30273_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30277_GM;
MethodInfo m30277_MI = 
{
	"get_IsReadOnly", NULL, &t3148_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30277_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3148_m30278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30278_GM;
MethodInfo m30278_MI = 
{
	"Add", NULL, &t3148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3148_m30278_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30278_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30279_GM;
MethodInfo m30279_MI = 
{
	"Clear", NULL, &t3148_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30279_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3148_m30280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30280_GM;
MethodInfo m30280_MI = 
{
	"Contains", NULL, &t3148_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3148_m30280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30280_GM};
extern Il2CppType t3146_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3148_m30274_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3146_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30274_GM;
MethodInfo m30274_MI = 
{
	"CopyTo", NULL, &t3148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3148_m30274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30274_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3148_m30281_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30281_GM;
MethodInfo m30281_MI = 
{
	"Remove", NULL, &t3148_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3148_m30281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30281_GM};
static MethodInfo* t3148_MIs[] =
{
	&m30273_MI,
	&m30277_MI,
	&m30278_MI,
	&m30279_MI,
	&m30280_MI,
	&m30274_MI,
	&m30281_MI,
	NULL
};
static TypeInfo* t3148_ITIs[] = 
{
	&t603_TI,
	&t3149_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3148_1_0_0;
struct t3148;
extern Il2CppGenericClass t3148_GC;
TypeInfo t3148_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t3148_MIs, t3148_PIs, NULL, NULL, NULL, NULL, NULL, &t3148_TI, t3148_ITIs, NULL, &EmptyCustomAttributesCache, &t3148_TI, &t3148_0_0_0, &t3148_1_0_0, NULL, &t3148_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
