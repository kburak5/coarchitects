﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t3942_TI;

#include "t1.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<BackgroundMove>
extern MethodInfo m26252_MI;
static PropertyInfo t3942____Current_PropertyInfo = 
{
	&t3942_TI, "Current", &m26252_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3942_PIs[] =
{
	&t3942____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26252_GM;
MethodInfo m26252_MI = 
{
	"get_Current", NULL, &t3942_TI, &t1_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26252_GM};
static MethodInfo* t3942_MIs[] =
{
	&m26252_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t3942_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3942_0_0_0;
extern Il2CppType t3942_1_0_0;
struct t3942;
extern Il2CppGenericClass t3942_GC;
TypeInfo t3942_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3942_MIs, t3942_PIs, NULL, NULL, NULL, NULL, NULL, &t3942_TI, t3942_ITIs, NULL, &EmptyCustomAttributesCache, &t3942_TI, &t3942_0_0_0, &t3942_1_0_0, NULL, &t3942_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t346_TI;

#include "t29.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Object>
extern MethodInfo m26253_MI;
static PropertyInfo t346____Current_PropertyInfo = 
{
	&t346_TI, "Current", &m26253_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t346_PIs[] =
{
	&t346____Current_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26253_GM;
MethodInfo m26253_MI = 
{
	"get_Current", NULL, &t346_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26253_GM};
static MethodInfo* t346_MIs[] =
{
	&m26253_MI,
	NULL
};
static TypeInfo* t346_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t346_0_0_0;
extern Il2CppType t346_1_0_0;
struct t346;
extern Il2CppGenericClass t346_GC;
TypeInfo t346_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t346_MIs, t346_PIs, NULL, NULL, NULL, NULL, NULL, &t346_TI, t346_ITIs, NULL, &EmptyCustomAttributesCache, &t346_TI, &t346_0_0_0, &t346_1_0_0, NULL, &t346_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2110.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2110_TI;
#include "t2110MD.h"

#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t1_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m10295_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m19492_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m19492(__this, p0, method) (t1 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<BackgroundMove>
extern Il2CppType t20_0_0_1;
FieldInfo t2110_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2110_TI, offsetof(t2110, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2110_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2110_TI, offsetof(t2110, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2110_FIs[] =
{
	&t2110_f0_FieldInfo,
	&t2110_f1_FieldInfo,
	NULL
};
extern MethodInfo m10289_MI;
static PropertyInfo t2110____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2110_TI, "System.Collections.IEnumerator.Current", &m10289_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2110____Current_PropertyInfo = 
{
	&t2110_TI, "Current", &m10295_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2110_PIs[] =
{
	&t2110____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2110____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2110_m10287_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10287_GM;
MethodInfo m10287_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2110_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2110_m10287_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10287_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10289_GM;
MethodInfo m10289_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2110_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10289_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10291_GM;
MethodInfo m10291_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2110_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10291_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10293_GM;
MethodInfo m10293_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2110_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10293_GM};
extern Il2CppType t1_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10295_GM;
MethodInfo m10295_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2110_TI, &t1_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10295_GM};
static MethodInfo* t2110_MIs[] =
{
	&m10287_MI,
	&m10289_MI,
	&m10291_MI,
	&m10293_MI,
	&m10295_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m10293_MI;
extern MethodInfo m10291_MI;
static MethodInfo* t2110_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10289_MI,
	&m10293_MI,
	&m10291_MI,
	&m10295_MI,
};
static TypeInfo* t2110_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3942_TI,
};
static Il2CppInterfaceOffsetPair t2110_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3942_TI, 7},
};
extern TypeInfo t1_TI;
static Il2CppRGCTXData t2110_RGCTXData[3] = 
{
	&m10295_MI/* Method Usage */,
	&t1_TI/* Class Usage */,
	&m19492_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2110_0_0_0;
extern Il2CppType t2110_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2110_GC;
extern TypeInfo t20_TI;
TypeInfo t2110_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2110_MIs, t2110_PIs, t2110_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2110_TI, t2110_ITIs, t2110_VT, &EmptyCustomAttributesCache, &t2110_TI, &t2110_0_0_0, &t2110_1_0_0, t2110_IOs, &t2110_GC, NULL, NULL, NULL, t2110_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2110)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#include "t2111.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2111_TI;
#include "t2111MD.h"

extern TypeInfo t29_TI;
extern MethodInfo m10296_MI;
extern MethodInfo m19490_MI;


extern MethodInfo m10288_MI;
 void m10288_gshared (t2111 * __this, t20 * p0, MethodInfo* method)
{
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10290_MI;
 t29 * m10290_gshared (t2111 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t2111 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m10292_MI;
 void m10292_gshared (t2111 * __this, MethodInfo* method)
{
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10294_MI;
 bool m10294_gshared (t2111 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t29 * m10296_gshared (t2111 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t29 * L_8 = (( t29 * (*) (t20 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Object>
extern Il2CppType t20_0_0_1;
FieldInfo t2111_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2111_TI, offsetof(t2111, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2111_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2111_TI, offsetof(t2111, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2111_FIs[] =
{
	&t2111_f0_FieldInfo,
	&t2111_f1_FieldInfo,
	NULL
};
static PropertyInfo t2111____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2111_TI, "System.Collections.IEnumerator.Current", &m10290_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2111____Current_PropertyInfo = 
{
	&t2111_TI, "Current", &m10296_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2111_PIs[] =
{
	&t2111____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2111____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2111_m10288_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10288_GM;
MethodInfo m10288_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2111_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2111_m10288_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10288_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10290_GM;
MethodInfo m10290_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2111_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10290_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10292_GM;
MethodInfo m10292_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2111_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10292_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10294_GM;
MethodInfo m10294_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2111_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10294_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10296_GM;
MethodInfo m10296_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2111_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10296_GM};
static MethodInfo* t2111_MIs[] =
{
	&m10288_MI,
	&m10290_MI,
	&m10292_MI,
	&m10294_MI,
	&m10296_MI,
	NULL
};
static MethodInfo* t2111_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10290_MI,
	&m10294_MI,
	&m10292_MI,
	&m10296_MI,
};
static TypeInfo* t2111_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t2111_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t346_TI, 7},
};
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2111_RGCTXData[3] = 
{
	&m10296_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m19490_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2111_0_0_0;
extern Il2CppType t2111_1_0_0;
extern Il2CppGenericClass t2111_GC;
TypeInfo t2111_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2111_MIs, t2111_PIs, t2111_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2111_TI, t2111_ITIs, t2111_VT, &EmptyCustomAttributesCache, &t2111_TI, &t2111_0_0_0, &t2111_1_0_0, t2111_IOs, &t2111_GC, NULL, NULL, NULL, t2111_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2111)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2182_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Object>
extern MethodInfo m26254_MI;
static PropertyInfo t2182____Count_PropertyInfo = 
{
	&t2182_TI, "Count", &m26254_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26255_MI;
static PropertyInfo t2182____IsReadOnly_PropertyInfo = 
{
	&t2182_TI, "IsReadOnly", &m26255_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2182_PIs[] =
{
	&t2182____Count_PropertyInfo,
	&t2182____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26254_GM;
MethodInfo m26254_MI = 
{
	"get_Count", NULL, &t2182_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26254_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26255_GM;
MethodInfo m26255_MI = 
{
	"get_IsReadOnly", NULL, &t2182_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26255_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2182_m26256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26256_GM;
MethodInfo m26256_MI = 
{
	"Add", NULL, &t2182_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2182_m26256_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26256_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26257_GM;
MethodInfo m26257_MI = 
{
	"Clear", NULL, &t2182_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26257_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2182_m26258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26258_GM;
MethodInfo m26258_MI = 
{
	"Contains", NULL, &t2182_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2182_m26258_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26258_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2182_m26259_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26259_GM;
MethodInfo m26259_MI = 
{
	"CopyTo", NULL, &t2182_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2182_m26259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26259_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2182_m26260_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26260_GM;
MethodInfo m26260_MI = 
{
	"Remove", NULL, &t2182_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2182_m26260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26260_GM};
static MethodInfo* t2182_MIs[] =
{
	&m26254_MI,
	&m26255_MI,
	&m26256_MI,
	&m26257_MI,
	&m26258_MI,
	&m26259_MI,
	&m26260_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t2182_ITIs[] = 
{
	&t603_TI,
	&t2183_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2182_0_0_0;
extern Il2CppType t2182_1_0_0;
struct t2182;
extern Il2CppGenericClass t2182_GC;
TypeInfo t2182_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2182_MIs, t2182_PIs, NULL, NULL, NULL, NULL, NULL, &t2182_TI, t2182_ITIs, NULL, &EmptyCustomAttributesCache, &t2182_TI, &t2182_0_0_0, &t2182_1_0_0, NULL, &t2182_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Object>
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26261_GM;
MethodInfo m26261_MI = 
{
	"GetEnumerator", NULL, &t2183_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26261_GM};
static MethodInfo* t2183_MIs[] =
{
	&m26261_MI,
	NULL
};
static TypeInfo* t2183_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2183_0_0_0;
extern Il2CppType t2183_1_0_0;
struct t2183;
extern Il2CppGenericClass t2183_GC;
TypeInfo t2183_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2183_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2183_TI, t2183_ITIs, NULL, &EmptyCustomAttributesCache, &t2183_TI, &t2183_0_0_0, &t2183_1_0_0, NULL, &t2183_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2186_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Object>
extern MethodInfo m26262_MI;
extern MethodInfo m26263_MI;
static PropertyInfo t2186____Item_PropertyInfo = 
{
	&t2186_TI, "Item", &m26262_MI, &m26263_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2186_PIs[] =
{
	&t2186____Item_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2186_m26264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26264_GM;
MethodInfo m26264_MI = 
{
	"IndexOf", NULL, &t2186_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2186_m26264_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26264_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2186_m26265_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26265_GM;
MethodInfo m26265_MI = 
{
	"Insert", NULL, &t2186_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2186_m26265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26265_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2186_m26266_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26266_GM;
MethodInfo m26266_MI = 
{
	"RemoveAt", NULL, &t2186_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2186_m26266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26266_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2186_m26262_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26262_GM;
MethodInfo m26262_MI = 
{
	"get_Item", NULL, &t2186_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2186_m26262_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26262_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2186_m26263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26263_GM;
MethodInfo m26263_MI = 
{
	"set_Item", NULL, &t2186_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2186_m26263_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26263_GM};
static MethodInfo* t2186_MIs[] =
{
	&m26264_MI,
	&m26265_MI,
	&m26266_MI,
	&m26262_MI,
	&m26263_MI,
	NULL
};
static TypeInfo* t2186_ITIs[] = 
{
	&t603_TI,
	&t2182_TI,
	&t2183_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2186_0_0_0;
extern Il2CppType t2186_1_0_0;
struct t2186;
extern Il2CppGenericClass t2186_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2186_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2186_MIs, t2186_PIs, NULL, NULL, NULL, NULL, NULL, &t2186_TI, t2186_ITIs, NULL, &t1908__CustomAttributeCache, &t2186_TI, &t2186_0_0_0, &t2186_1_0_0, NULL, &t2186_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5056_TI;

#include "Assembly-CSharp_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<BackgroundMove>
extern MethodInfo m26267_MI;
static PropertyInfo t5056____Count_PropertyInfo = 
{
	&t5056_TI, "Count", &m26267_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26268_MI;
static PropertyInfo t5056____IsReadOnly_PropertyInfo = 
{
	&t5056_TI, "IsReadOnly", &m26268_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5056_PIs[] =
{
	&t5056____Count_PropertyInfo,
	&t5056____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26267_GM;
MethodInfo m26267_MI = 
{
	"get_Count", NULL, &t5056_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26267_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26268_GM;
MethodInfo m26268_MI = 
{
	"get_IsReadOnly", NULL, &t5056_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26268_GM};
extern Il2CppType t1_0_0_0;
extern Il2CppType t1_0_0_0;
static ParameterInfo t5056_m26269_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26269_GM;
MethodInfo m26269_MI = 
{
	"Add", NULL, &t5056_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5056_m26269_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26269_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26270_GM;
MethodInfo m26270_MI = 
{
	"Clear", NULL, &t5056_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26270_GM};
extern Il2CppType t1_0_0_0;
static ParameterInfo t5056_m26271_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26271_GM;
MethodInfo m26271_MI = 
{
	"Contains", NULL, &t5056_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5056_m26271_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26271_GM};
extern Il2CppType t3510_0_0_0;
extern Il2CppType t3510_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5056_m26272_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3510_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26272_GM;
MethodInfo m26272_MI = 
{
	"CopyTo", NULL, &t5056_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5056_m26272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26272_GM};
extern Il2CppType t1_0_0_0;
static ParameterInfo t5056_m26273_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26273_GM;
MethodInfo m26273_MI = 
{
	"Remove", NULL, &t5056_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5056_m26273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26273_GM};
static MethodInfo* t5056_MIs[] =
{
	&m26267_MI,
	&m26268_MI,
	&m26269_MI,
	&m26270_MI,
	&m26271_MI,
	&m26272_MI,
	&m26273_MI,
	NULL
};
extern TypeInfo t5058_TI;
static TypeInfo* t5056_ITIs[] = 
{
	&t603_TI,
	&t5058_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5056_0_0_0;
extern Il2CppType t5056_1_0_0;
struct t5056;
extern Il2CppGenericClass t5056_GC;
TypeInfo t5056_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5056_MIs, t5056_PIs, NULL, NULL, NULL, NULL, NULL, &t5056_TI, t5056_ITIs, NULL, &EmptyCustomAttributesCache, &t5056_TI, &t5056_0_0_0, &t5056_1_0_0, NULL, &t5056_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<BackgroundMove>
extern Il2CppType t3942_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26274_GM;
MethodInfo m26274_MI = 
{
	"GetEnumerator", NULL, &t5058_TI, &t3942_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26274_GM};
static MethodInfo* t5058_MIs[] =
{
	&m26274_MI,
	NULL
};
static TypeInfo* t5058_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5058_0_0_0;
extern Il2CppType t5058_1_0_0;
struct t5058;
extern Il2CppGenericClass t5058_GC;
TypeInfo t5058_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5058_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5058_TI, t5058_ITIs, NULL, &EmptyCustomAttributesCache, &t5058_TI, &t5058_0_0_0, &t5058_1_0_0, NULL, &t5058_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5057_TI;



// Metadata Definition System.Collections.Generic.IList`1<BackgroundMove>
extern MethodInfo m26275_MI;
extern MethodInfo m26276_MI;
static PropertyInfo t5057____Item_PropertyInfo = 
{
	&t5057_TI, "Item", &m26275_MI, &m26276_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5057_PIs[] =
{
	&t5057____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1_0_0_0;
static ParameterInfo t5057_m26277_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26277_GM;
MethodInfo m26277_MI = 
{
	"IndexOf", NULL, &t5057_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5057_m26277_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26277_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1_0_0_0;
static ParameterInfo t5057_m26278_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26278_GM;
MethodInfo m26278_MI = 
{
	"Insert", NULL, &t5057_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5057_m26278_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26278_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5057_m26279_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26279_GM;
MethodInfo m26279_MI = 
{
	"RemoveAt", NULL, &t5057_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5057_m26279_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26279_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5057_m26275_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26275_GM;
MethodInfo m26275_MI = 
{
	"get_Item", NULL, &t5057_TI, &t1_0_0_0, RuntimeInvoker_t29_t44, t5057_m26275_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26275_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1_0_0_0;
static ParameterInfo t5057_m26276_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26276_GM;
MethodInfo m26276_MI = 
{
	"set_Item", NULL, &t5057_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5057_m26276_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26276_GM};
static MethodInfo* t5057_MIs[] =
{
	&m26277_MI,
	&m26278_MI,
	&m26279_MI,
	&m26275_MI,
	&m26276_MI,
	NULL
};
static TypeInfo* t5057_ITIs[] = 
{
	&t603_TI,
	&t5056_TI,
	&t5058_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5057_0_0_0;
extern Il2CppType t5057_1_0_0;
struct t5057;
extern Il2CppGenericClass t5057_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5057_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5057_MIs, t5057_PIs, NULL, NULL, NULL, NULL, NULL, &t5057_TI, t5057_ITIs, NULL, &t1908__CustomAttributeCache, &t5057_TI, &t5057_0_0_0, &t5057_1_0_0, NULL, &t5057_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5059_TI;

#include "t4.h"
#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>
extern MethodInfo m26280_MI;
static PropertyInfo t5059____Count_PropertyInfo = 
{
	&t5059_TI, "Count", &m26280_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26281_MI;
static PropertyInfo t5059____IsReadOnly_PropertyInfo = 
{
	&t5059_TI, "IsReadOnly", &m26281_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5059_PIs[] =
{
	&t5059____Count_PropertyInfo,
	&t5059____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26280_GM;
MethodInfo m26280_MI = 
{
	"get_Count", NULL, &t5059_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26280_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26281_GM;
MethodInfo m26281_MI = 
{
	"get_IsReadOnly", NULL, &t5059_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26281_GM};
extern Il2CppType t4_0_0_0;
extern Il2CppType t4_0_0_0;
static ParameterInfo t5059_m26282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26282_GM;
MethodInfo m26282_MI = 
{
	"Add", NULL, &t5059_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5059_m26282_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26282_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26283_GM;
MethodInfo m26283_MI = 
{
	"Clear", NULL, &t5059_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26283_GM};
extern Il2CppType t4_0_0_0;
static ParameterInfo t5059_m26284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26284_GM;
MethodInfo m26284_MI = 
{
	"Contains", NULL, &t5059_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5059_m26284_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26284_GM};
extern Il2CppType t3728_0_0_0;
extern Il2CppType t3728_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5059_m26285_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3728_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26285_GM;
MethodInfo m26285_MI = 
{
	"CopyTo", NULL, &t5059_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5059_m26285_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26285_GM};
extern Il2CppType t4_0_0_0;
static ParameterInfo t5059_m26286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26286_GM;
MethodInfo m26286_MI = 
{
	"Remove", NULL, &t5059_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5059_m26286_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26286_GM};
static MethodInfo* t5059_MIs[] =
{
	&m26280_MI,
	&m26281_MI,
	&m26282_MI,
	&m26283_MI,
	&m26284_MI,
	&m26285_MI,
	&m26286_MI,
	NULL
};
extern TypeInfo t5061_TI;
static TypeInfo* t5059_ITIs[] = 
{
	&t603_TI,
	&t5061_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5059_0_0_0;
extern Il2CppType t5059_1_0_0;
struct t5059;
extern Il2CppGenericClass t5059_GC;
TypeInfo t5059_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5059_MIs, t5059_PIs, NULL, NULL, NULL, NULL, NULL, &t5059_TI, t5059_ITIs, NULL, &EmptyCustomAttributesCache, &t5059_TI, &t5059_0_0_0, &t5059_1_0_0, NULL, &t5059_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>
extern Il2CppType t3944_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26287_GM;
MethodInfo m26287_MI = 
{
	"GetEnumerator", NULL, &t5061_TI, &t3944_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26287_GM};
static MethodInfo* t5061_MIs[] =
{
	&m26287_MI,
	NULL
};
static TypeInfo* t5061_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5061_0_0_0;
extern Il2CppType t5061_1_0_0;
struct t5061;
extern Il2CppGenericClass t5061_GC;
TypeInfo t5061_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5061_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5061_TI, t5061_ITIs, NULL, &EmptyCustomAttributesCache, &t5061_TI, &t5061_0_0_0, &t5061_1_0_0, NULL, &t5061_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3944_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>
extern MethodInfo m26288_MI;
static PropertyInfo t3944____Current_PropertyInfo = 
{
	&t3944_TI, "Current", &m26288_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3944_PIs[] =
{
	&t3944____Current_PropertyInfo,
	NULL
};
extern Il2CppType t4_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26288_GM;
MethodInfo m26288_MI = 
{
	"get_Current", NULL, &t3944_TI, &t4_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26288_GM};
static MethodInfo* t3944_MIs[] =
{
	&m26288_MI,
	NULL
};
static TypeInfo* t3944_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3944_0_0_0;
extern Il2CppType t3944_1_0_0;
struct t3944;
extern Il2CppGenericClass t3944_GC;
TypeInfo t3944_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3944_MIs, t3944_PIs, NULL, NULL, NULL, NULL, NULL, &t3944_TI, t3944_ITIs, NULL, &EmptyCustomAttributesCache, &t3944_TI, &t3944_0_0_0, &t3944_1_0_0, NULL, &t3944_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2112.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2112_TI;
#include "t2112MD.h"

extern TypeInfo t4_TI;
extern MethodInfo m10301_MI;
extern MethodInfo m19512_MI;
struct t20;
#define m19512(__this, p0, method) (t4 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
extern Il2CppType t20_0_0_1;
FieldInfo t2112_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2112_TI, offsetof(t2112, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2112_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2112_TI, offsetof(t2112, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2112_FIs[] =
{
	&t2112_f0_FieldInfo,
	&t2112_f1_FieldInfo,
	NULL
};
extern MethodInfo m10298_MI;
static PropertyInfo t2112____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2112_TI, "System.Collections.IEnumerator.Current", &m10298_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2112____Current_PropertyInfo = 
{
	&t2112_TI, "Current", &m10301_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2112_PIs[] =
{
	&t2112____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2112____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2112_m10297_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10297_GM;
MethodInfo m10297_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2112_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2112_m10297_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10297_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10298_GM;
MethodInfo m10298_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2112_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10298_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10299_GM;
MethodInfo m10299_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2112_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10299_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10300_GM;
MethodInfo m10300_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2112_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10300_GM};
extern Il2CppType t4_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10301_GM;
MethodInfo m10301_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2112_TI, &t4_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10301_GM};
static MethodInfo* t2112_MIs[] =
{
	&m10297_MI,
	&m10298_MI,
	&m10299_MI,
	&m10300_MI,
	&m10301_MI,
	NULL
};
extern MethodInfo m10300_MI;
extern MethodInfo m10299_MI;
static MethodInfo* t2112_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10298_MI,
	&m10300_MI,
	&m10299_MI,
	&m10301_MI,
};
static TypeInfo* t2112_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3944_TI,
};
static Il2CppInterfaceOffsetPair t2112_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3944_TI, 7},
};
extern TypeInfo t4_TI;
static Il2CppRGCTXData t2112_RGCTXData[3] = 
{
	&m10301_MI/* Method Usage */,
	&t4_TI/* Class Usage */,
	&m19512_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2112_0_0_0;
extern Il2CppType t2112_1_0_0;
extern Il2CppGenericClass t2112_GC;
TypeInfo t2112_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2112_MIs, t2112_PIs, t2112_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2112_TI, t2112_ITIs, t2112_VT, &EmptyCustomAttributesCache, &t2112_TI, &t2112_0_0_0, &t2112_1_0_0, t2112_IOs, &t2112_GC, NULL, NULL, NULL, t2112_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2112)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5060_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>
extern MethodInfo m26289_MI;
extern MethodInfo m26290_MI;
static PropertyInfo t5060____Item_PropertyInfo = 
{
	&t5060_TI, "Item", &m26289_MI, &m26290_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5060_PIs[] =
{
	&t5060____Item_PropertyInfo,
	NULL
};
extern Il2CppType t4_0_0_0;
static ParameterInfo t5060_m26291_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26291_GM;
MethodInfo m26291_MI = 
{
	"IndexOf", NULL, &t5060_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5060_m26291_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26291_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t4_0_0_0;
static ParameterInfo t5060_m26292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26292_GM;
MethodInfo m26292_MI = 
{
	"Insert", NULL, &t5060_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5060_m26292_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26292_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5060_m26293_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26293_GM;
MethodInfo m26293_MI = 
{
	"RemoveAt", NULL, &t5060_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5060_m26293_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26293_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5060_m26289_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t4_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26289_GM;
MethodInfo m26289_MI = 
{
	"get_Item", NULL, &t5060_TI, &t4_0_0_0, RuntimeInvoker_t29_t44, t5060_m26289_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26289_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t4_0_0_0;
static ParameterInfo t5060_m26290_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26290_GM;
MethodInfo m26290_MI = 
{
	"set_Item", NULL, &t5060_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5060_m26290_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26290_GM};
static MethodInfo* t5060_MIs[] =
{
	&m26291_MI,
	&m26292_MI,
	&m26293_MI,
	&m26289_MI,
	&m26290_MI,
	NULL
};
static TypeInfo* t5060_ITIs[] = 
{
	&t603_TI,
	&t5059_TI,
	&t5061_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5060_0_0_0;
extern Il2CppType t5060_1_0_0;
struct t5060;
extern Il2CppGenericClass t5060_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5060_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5060_MIs, t5060_PIs, NULL, NULL, NULL, NULL, NULL, &t5060_TI, t5060_ITIs, NULL, &t1908__CustomAttributeCache, &t5060_TI, &t5060_0_0_0, &t5060_1_0_0, NULL, &t5060_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5062_TI;

#include "t317.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>
extern MethodInfo m26294_MI;
static PropertyInfo t5062____Count_PropertyInfo = 
{
	&t5062_TI, "Count", &m26294_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26295_MI;
static PropertyInfo t5062____IsReadOnly_PropertyInfo = 
{
	&t5062_TI, "IsReadOnly", &m26295_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5062_PIs[] =
{
	&t5062____Count_PropertyInfo,
	&t5062____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26294_GM;
MethodInfo m26294_MI = 
{
	"get_Count", NULL, &t5062_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26294_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26295_GM;
MethodInfo m26295_MI = 
{
	"get_IsReadOnly", NULL, &t5062_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26295_GM};
extern Il2CppType t317_0_0_0;
extern Il2CppType t317_0_0_0;
static ParameterInfo t5062_m26296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26296_GM;
MethodInfo m26296_MI = 
{
	"Add", NULL, &t5062_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5062_m26296_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26296_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26297_GM;
MethodInfo m26297_MI = 
{
	"Clear", NULL, &t5062_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26297_GM};
extern Il2CppType t317_0_0_0;
static ParameterInfo t5062_m26298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26298_GM;
MethodInfo m26298_MI = 
{
	"Contains", NULL, &t5062_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5062_m26298_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26298_GM};
extern Il2CppType t3729_0_0_0;
extern Il2CppType t3729_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5062_m26299_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3729_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26299_GM;
MethodInfo m26299_MI = 
{
	"CopyTo", NULL, &t5062_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5062_m26299_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26299_GM};
extern Il2CppType t317_0_0_0;
static ParameterInfo t5062_m26300_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26300_GM;
MethodInfo m26300_MI = 
{
	"Remove", NULL, &t5062_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5062_m26300_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26300_GM};
static MethodInfo* t5062_MIs[] =
{
	&m26294_MI,
	&m26295_MI,
	&m26296_MI,
	&m26297_MI,
	&m26298_MI,
	&m26299_MI,
	&m26300_MI,
	NULL
};
extern TypeInfo t5064_TI;
static TypeInfo* t5062_ITIs[] = 
{
	&t603_TI,
	&t5064_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5062_0_0_0;
extern Il2CppType t5062_1_0_0;
struct t5062;
extern Il2CppGenericClass t5062_GC;
TypeInfo t5062_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5062_MIs, t5062_PIs, NULL, NULL, NULL, NULL, NULL, &t5062_TI, t5062_ITIs, NULL, &EmptyCustomAttributesCache, &t5062_TI, &t5062_0_0_0, &t5062_1_0_0, NULL, &t5062_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>
extern Il2CppType t3946_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26301_GM;
MethodInfo m26301_MI = 
{
	"GetEnumerator", NULL, &t5064_TI, &t3946_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26301_GM};
static MethodInfo* t5064_MIs[] =
{
	&m26301_MI,
	NULL
};
static TypeInfo* t5064_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5064_0_0_0;
extern Il2CppType t5064_1_0_0;
struct t5064;
extern Il2CppGenericClass t5064_GC;
TypeInfo t5064_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5064_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5064_TI, t5064_ITIs, NULL, &EmptyCustomAttributesCache, &t5064_TI, &t5064_0_0_0, &t5064_1_0_0, NULL, &t5064_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3946_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>
extern MethodInfo m26302_MI;
static PropertyInfo t3946____Current_PropertyInfo = 
{
	&t3946_TI, "Current", &m26302_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3946_PIs[] =
{
	&t3946____Current_PropertyInfo,
	NULL
};
extern Il2CppType t317_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26302_GM;
MethodInfo m26302_MI = 
{
	"get_Current", NULL, &t3946_TI, &t317_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26302_GM};
static MethodInfo* t3946_MIs[] =
{
	&m26302_MI,
	NULL
};
static TypeInfo* t3946_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3946_0_0_0;
extern Il2CppType t3946_1_0_0;
struct t3946;
extern Il2CppGenericClass t3946_GC;
TypeInfo t3946_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3946_MIs, t3946_PIs, NULL, NULL, NULL, NULL, NULL, &t3946_TI, t3946_ITIs, NULL, &EmptyCustomAttributesCache, &t3946_TI, &t3946_0_0_0, &t3946_1_0_0, NULL, &t3946_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2113.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2113_TI;
#include "t2113MD.h"

extern TypeInfo t317_TI;
extern MethodInfo m10306_MI;
extern MethodInfo m19523_MI;
struct t20;
#define m19523(__this, p0, method) (t317 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
extern Il2CppType t20_0_0_1;
FieldInfo t2113_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2113_TI, offsetof(t2113, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2113_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2113_TI, offsetof(t2113, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2113_FIs[] =
{
	&t2113_f0_FieldInfo,
	&t2113_f1_FieldInfo,
	NULL
};
extern MethodInfo m10303_MI;
static PropertyInfo t2113____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2113_TI, "System.Collections.IEnumerator.Current", &m10303_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2113____Current_PropertyInfo = 
{
	&t2113_TI, "Current", &m10306_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2113_PIs[] =
{
	&t2113____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2113____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2113_m10302_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10302_GM;
MethodInfo m10302_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2113_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2113_m10302_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10302_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10303_GM;
MethodInfo m10303_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2113_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10303_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10304_GM;
MethodInfo m10304_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2113_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10304_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10305_GM;
MethodInfo m10305_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2113_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10305_GM};
extern Il2CppType t317_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10306_GM;
MethodInfo m10306_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2113_TI, &t317_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10306_GM};
static MethodInfo* t2113_MIs[] =
{
	&m10302_MI,
	&m10303_MI,
	&m10304_MI,
	&m10305_MI,
	&m10306_MI,
	NULL
};
extern MethodInfo m10305_MI;
extern MethodInfo m10304_MI;
static MethodInfo* t2113_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10303_MI,
	&m10305_MI,
	&m10304_MI,
	&m10306_MI,
};
static TypeInfo* t2113_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3946_TI,
};
static Il2CppInterfaceOffsetPair t2113_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3946_TI, 7},
};
extern TypeInfo t317_TI;
static Il2CppRGCTXData t2113_RGCTXData[3] = 
{
	&m10306_MI/* Method Usage */,
	&t317_TI/* Class Usage */,
	&m19523_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2113_0_0_0;
extern Il2CppType t2113_1_0_0;
extern Il2CppGenericClass t2113_GC;
TypeInfo t2113_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2113_MIs, t2113_PIs, t2113_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2113_TI, t2113_ITIs, t2113_VT, &EmptyCustomAttributesCache, &t2113_TI, &t2113_0_0_0, &t2113_1_0_0, t2113_IOs, &t2113_GC, NULL, NULL, NULL, t2113_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2113)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5063_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Behaviour>
extern MethodInfo m26303_MI;
extern MethodInfo m26304_MI;
static PropertyInfo t5063____Item_PropertyInfo = 
{
	&t5063_TI, "Item", &m26303_MI, &m26304_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5063_PIs[] =
{
	&t5063____Item_PropertyInfo,
	NULL
};
extern Il2CppType t317_0_0_0;
static ParameterInfo t5063_m26305_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26305_GM;
MethodInfo m26305_MI = 
{
	"IndexOf", NULL, &t5063_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5063_m26305_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26305_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t317_0_0_0;
static ParameterInfo t5063_m26306_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26306_GM;
MethodInfo m26306_MI = 
{
	"Insert", NULL, &t5063_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5063_m26306_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26306_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5063_m26307_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26307_GM;
MethodInfo m26307_MI = 
{
	"RemoveAt", NULL, &t5063_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5063_m26307_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26307_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5063_m26303_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t317_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26303_GM;
MethodInfo m26303_MI = 
{
	"get_Item", NULL, &t5063_TI, &t317_0_0_0, RuntimeInvoker_t29_t44, t5063_m26303_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26303_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t317_0_0_0;
static ParameterInfo t5063_m26304_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26304_GM;
MethodInfo m26304_MI = 
{
	"set_Item", NULL, &t5063_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5063_m26304_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26304_GM};
static MethodInfo* t5063_MIs[] =
{
	&m26305_MI,
	&m26306_MI,
	&m26307_MI,
	&m26303_MI,
	&m26304_MI,
	NULL
};
static TypeInfo* t5063_ITIs[] = 
{
	&t603_TI,
	&t5062_TI,
	&t5064_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5063_0_0_0;
extern Il2CppType t5063_1_0_0;
struct t5063;
extern Il2CppGenericClass t5063_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5063_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5063_MIs, t5063_PIs, NULL, NULL, NULL, NULL, NULL, &t5063_TI, t5063_ITIs, NULL, &t1908__CustomAttributeCache, &t5063_TI, &t5063_0_0_0, &t5063_1_0_0, NULL, &t5063_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2240_TI;

#include "t28.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Component>
extern MethodInfo m26308_MI;
static PropertyInfo t2240____Count_PropertyInfo = 
{
	&t2240_TI, "Count", &m26308_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26309_MI;
static PropertyInfo t2240____IsReadOnly_PropertyInfo = 
{
	&t2240_TI, "IsReadOnly", &m26309_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2240_PIs[] =
{
	&t2240____Count_PropertyInfo,
	&t2240____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26308_GM;
MethodInfo m26308_MI = 
{
	"get_Count", NULL, &t2240_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26308_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26309_GM;
MethodInfo m26309_MI = 
{
	"get_IsReadOnly", NULL, &t2240_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26309_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2240_m26310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26310_GM;
MethodInfo m26310_MI = 
{
	"Add", NULL, &t2240_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2240_m26310_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26310_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26311_GM;
MethodInfo m26311_MI = 
{
	"Clear", NULL, &t2240_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26311_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2240_m26312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26312_GM;
MethodInfo m26312_MI = 
{
	"Contains", NULL, &t2240_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2240_m26312_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26312_GM};
extern Il2CppType t2238_0_0_0;
extern Il2CppType t2238_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2240_m26313_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2238_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26313_GM;
MethodInfo m26313_MI = 
{
	"CopyTo", NULL, &t2240_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2240_m26313_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26313_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2240_m26314_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26314_GM;
MethodInfo m26314_MI = 
{
	"Remove", NULL, &t2240_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2240_m26314_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26314_GM};
static MethodInfo* t2240_MIs[] =
{
	&m26308_MI,
	&m26309_MI,
	&m26310_MI,
	&m26311_MI,
	&m26312_MI,
	&m26313_MI,
	&m26314_MI,
	NULL
};
extern TypeInfo t2241_TI;
static TypeInfo* t2240_ITIs[] = 
{
	&t603_TI,
	&t2241_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2240_0_0_0;
extern Il2CppType t2240_1_0_0;
struct t2240;
extern Il2CppGenericClass t2240_GC;
TypeInfo t2240_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2240_MIs, t2240_PIs, NULL, NULL, NULL, NULL, NULL, &t2240_TI, t2240_ITIs, NULL, &EmptyCustomAttributesCache, &t2240_TI, &t2240_0_0_0, &t2240_1_0_0, NULL, &t2240_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Component>
extern Il2CppType t2239_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26315_GM;
MethodInfo m26315_MI = 
{
	"GetEnumerator", NULL, &t2241_TI, &t2239_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26315_GM};
static MethodInfo* t2241_MIs[] =
{
	&m26315_MI,
	NULL
};
static TypeInfo* t2241_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2241_0_0_0;
extern Il2CppType t2241_1_0_0;
struct t2241;
extern Il2CppGenericClass t2241_GC;
TypeInfo t2241_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2241_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2241_TI, t2241_ITIs, NULL, &EmptyCustomAttributesCache, &t2241_TI, &t2241_0_0_0, &t2241_1_0_0, NULL, &t2241_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2239_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Component>
extern MethodInfo m26316_MI;
static PropertyInfo t2239____Current_PropertyInfo = 
{
	&t2239_TI, "Current", &m26316_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2239_PIs[] =
{
	&t2239____Current_PropertyInfo,
	NULL
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26316_GM;
MethodInfo m26316_MI = 
{
	"get_Current", NULL, &t2239_TI, &t28_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26316_GM};
static MethodInfo* t2239_MIs[] =
{
	&m26316_MI,
	NULL
};
static TypeInfo* t2239_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2239_0_0_0;
extern Il2CppType t2239_1_0_0;
struct t2239;
extern Il2CppGenericClass t2239_GC;
TypeInfo t2239_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2239_MIs, t2239_PIs, NULL, NULL, NULL, NULL, NULL, &t2239_TI, t2239_ITIs, NULL, &EmptyCustomAttributesCache, &t2239_TI, &t2239_0_0_0, &t2239_1_0_0, NULL, &t2239_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2114.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2114_TI;
#include "t2114MD.h"

extern TypeInfo t28_TI;
extern MethodInfo m10311_MI;
extern MethodInfo m19534_MI;
struct t20;
#define m19534(__this, p0, method) (t28 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Component>
extern Il2CppType t20_0_0_1;
FieldInfo t2114_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2114_TI, offsetof(t2114, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2114_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2114_TI, offsetof(t2114, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2114_FIs[] =
{
	&t2114_f0_FieldInfo,
	&t2114_f1_FieldInfo,
	NULL
};
extern MethodInfo m10308_MI;
static PropertyInfo t2114____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2114_TI, "System.Collections.IEnumerator.Current", &m10308_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2114____Current_PropertyInfo = 
{
	&t2114_TI, "Current", &m10311_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2114_PIs[] =
{
	&t2114____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2114____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2114_m10307_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10307_GM;
MethodInfo m10307_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2114_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2114_m10307_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10307_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10308_GM;
MethodInfo m10308_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2114_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10308_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10309_GM;
MethodInfo m10309_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2114_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10309_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10310_GM;
MethodInfo m10310_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2114_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10310_GM};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10311_GM;
MethodInfo m10311_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2114_TI, &t28_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10311_GM};
static MethodInfo* t2114_MIs[] =
{
	&m10307_MI,
	&m10308_MI,
	&m10309_MI,
	&m10310_MI,
	&m10311_MI,
	NULL
};
extern MethodInfo m10310_MI;
extern MethodInfo m10309_MI;
static MethodInfo* t2114_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10308_MI,
	&m10310_MI,
	&m10309_MI,
	&m10311_MI,
};
static TypeInfo* t2114_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2239_TI,
};
static Il2CppInterfaceOffsetPair t2114_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2239_TI, 7},
};
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2114_RGCTXData[3] = 
{
	&m10311_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m19534_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2114_0_0_0;
extern Il2CppType t2114_1_0_0;
extern Il2CppGenericClass t2114_GC;
TypeInfo t2114_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2114_MIs, t2114_PIs, t2114_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2114_TI, t2114_ITIs, t2114_VT, &EmptyCustomAttributesCache, &t2114_TI, &t2114_0_0_0, &t2114_1_0_0, t2114_IOs, &t2114_GC, NULL, NULL, NULL, t2114_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2114)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2245_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Component>
extern MethodInfo m26317_MI;
extern MethodInfo m26318_MI;
static PropertyInfo t2245____Item_PropertyInfo = 
{
	&t2245_TI, "Item", &m26317_MI, &m26318_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2245_PIs[] =
{
	&t2245____Item_PropertyInfo,
	NULL
};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2245_m26319_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26319_GM;
MethodInfo m26319_MI = 
{
	"IndexOf", NULL, &t2245_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2245_m26319_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26319_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2245_m26320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26320_GM;
MethodInfo m26320_MI = 
{
	"Insert", NULL, &t2245_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2245_m26320_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26320_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2245_m26321_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26321_GM;
MethodInfo m26321_MI = 
{
	"RemoveAt", NULL, &t2245_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2245_m26321_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26321_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2245_m26317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26317_GM;
MethodInfo m26317_MI = 
{
	"get_Item", NULL, &t2245_TI, &t28_0_0_0, RuntimeInvoker_t29_t44, t2245_m26317_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26317_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2245_m26318_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26318_GM;
MethodInfo m26318_MI = 
{
	"set_Item", NULL, &t2245_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2245_m26318_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26318_GM};
static MethodInfo* t2245_MIs[] =
{
	&m26319_MI,
	&m26320_MI,
	&m26321_MI,
	&m26317_MI,
	&m26318_MI,
	NULL
};
static TypeInfo* t2245_ITIs[] = 
{
	&t603_TI,
	&t2240_TI,
	&t2241_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2245_0_0_0;
extern Il2CppType t2245_1_0_0;
struct t2245;
extern Il2CppGenericClass t2245_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2245_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2245_MIs, t2245_PIs, NULL, NULL, NULL, NULL, NULL, &t2245_TI, t2245_ITIs, NULL, &t1908__CustomAttributeCache, &t2245_TI, &t2245_0_0_0, &t2245_1_0_0, NULL, &t2245_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5065_TI;

#include "t41.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Object>
extern MethodInfo m26322_MI;
static PropertyInfo t5065____Count_PropertyInfo = 
{
	&t5065_TI, "Count", &m26322_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26323_MI;
static PropertyInfo t5065____IsReadOnly_PropertyInfo = 
{
	&t5065_TI, "IsReadOnly", &m26323_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5065_PIs[] =
{
	&t5065____Count_PropertyInfo,
	&t5065____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26322_GM;
MethodInfo m26322_MI = 
{
	"get_Count", NULL, &t5065_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26322_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26323_GM;
MethodInfo m26323_MI = 
{
	"get_IsReadOnly", NULL, &t5065_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26323_GM};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t5065_m26324_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26324_GM;
MethodInfo m26324_MI = 
{
	"Add", NULL, &t5065_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5065_m26324_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26324_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26325_GM;
MethodInfo m26325_MI = 
{
	"Clear", NULL, &t5065_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26325_GM};
extern Il2CppType t41_0_0_0;
static ParameterInfo t5065_m26326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26326_GM;
MethodInfo m26326_MI = 
{
	"Contains", NULL, &t5065_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5065_m26326_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26326_GM};
extern Il2CppType t444_0_0_0;
extern Il2CppType t444_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5065_m26327_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t444_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26327_GM;
MethodInfo m26327_MI = 
{
	"CopyTo", NULL, &t5065_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5065_m26327_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26327_GM};
extern Il2CppType t41_0_0_0;
static ParameterInfo t5065_m26328_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26328_GM;
MethodInfo m26328_MI = 
{
	"Remove", NULL, &t5065_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5065_m26328_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26328_GM};
static MethodInfo* t5065_MIs[] =
{
	&m26322_MI,
	&m26323_MI,
	&m26324_MI,
	&m26325_MI,
	&m26326_MI,
	&m26327_MI,
	&m26328_MI,
	NULL
};
extern TypeInfo t5067_TI;
static TypeInfo* t5065_ITIs[] = 
{
	&t603_TI,
	&t5067_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5065_0_0_0;
extern Il2CppType t5065_1_0_0;
struct t5065;
extern Il2CppGenericClass t5065_GC;
TypeInfo t5065_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5065_MIs, t5065_PIs, NULL, NULL, NULL, NULL, NULL, &t5065_TI, t5065_ITIs, NULL, &EmptyCustomAttributesCache, &t5065_TI, &t5065_0_0_0, &t5065_1_0_0, NULL, &t5065_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Object>
extern Il2CppType t3949_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26329_GM;
MethodInfo m26329_MI = 
{
	"GetEnumerator", NULL, &t5067_TI, &t3949_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26329_GM};
static MethodInfo* t5067_MIs[] =
{
	&m26329_MI,
	NULL
};
static TypeInfo* t5067_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5067_0_0_0;
extern Il2CppType t5067_1_0_0;
struct t5067;
extern Il2CppGenericClass t5067_GC;
TypeInfo t5067_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5067_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5067_TI, t5067_ITIs, NULL, &EmptyCustomAttributesCache, &t5067_TI, &t5067_0_0_0, &t5067_1_0_0, NULL, &t5067_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3949_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Object>
extern MethodInfo m26330_MI;
static PropertyInfo t3949____Current_PropertyInfo = 
{
	&t3949_TI, "Current", &m26330_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3949_PIs[] =
{
	&t3949____Current_PropertyInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26330_GM;
MethodInfo m26330_MI = 
{
	"get_Current", NULL, &t3949_TI, &t41_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26330_GM};
static MethodInfo* t3949_MIs[] =
{
	&m26330_MI,
	NULL
};
static TypeInfo* t3949_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3949_0_0_0;
extern Il2CppType t3949_1_0_0;
struct t3949;
extern Il2CppGenericClass t3949_GC;
TypeInfo t3949_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3949_MIs, t3949_PIs, NULL, NULL, NULL, NULL, NULL, &t3949_TI, t3949_ITIs, NULL, &EmptyCustomAttributesCache, &t3949_TI, &t3949_0_0_0, &t3949_1_0_0, NULL, &t3949_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2115.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2115_TI;
#include "t2115MD.h"

extern TypeInfo t41_TI;
extern MethodInfo m10316_MI;
extern MethodInfo m19545_MI;
struct t20;
#define m19545(__this, p0, method) (t41 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Object>
extern Il2CppType t20_0_0_1;
FieldInfo t2115_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2115_TI, offsetof(t2115, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2115_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2115_TI, offsetof(t2115, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2115_FIs[] =
{
	&t2115_f0_FieldInfo,
	&t2115_f1_FieldInfo,
	NULL
};
extern MethodInfo m10313_MI;
static PropertyInfo t2115____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2115_TI, "System.Collections.IEnumerator.Current", &m10313_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2115____Current_PropertyInfo = 
{
	&t2115_TI, "Current", &m10316_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2115_PIs[] =
{
	&t2115____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2115____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2115_m10312_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10312_GM;
MethodInfo m10312_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2115_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2115_m10312_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10312_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10313_GM;
MethodInfo m10313_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2115_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10313_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10314_GM;
MethodInfo m10314_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2115_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10314_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10315_GM;
MethodInfo m10315_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2115_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10315_GM};
extern Il2CppType t41_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10316_GM;
MethodInfo m10316_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2115_TI, &t41_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10316_GM};
static MethodInfo* t2115_MIs[] =
{
	&m10312_MI,
	&m10313_MI,
	&m10314_MI,
	&m10315_MI,
	&m10316_MI,
	NULL
};
extern MethodInfo m10315_MI;
extern MethodInfo m10314_MI;
static MethodInfo* t2115_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10313_MI,
	&m10315_MI,
	&m10314_MI,
	&m10316_MI,
};
static TypeInfo* t2115_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3949_TI,
};
static Il2CppInterfaceOffsetPair t2115_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3949_TI, 7},
};
extern TypeInfo t41_TI;
static Il2CppRGCTXData t2115_RGCTXData[3] = 
{
	&m10316_MI/* Method Usage */,
	&t41_TI/* Class Usage */,
	&m19545_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2115_0_0_0;
extern Il2CppType t2115_1_0_0;
extern Il2CppGenericClass t2115_GC;
TypeInfo t2115_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2115_MIs, t2115_PIs, t2115_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2115_TI, t2115_ITIs, t2115_VT, &EmptyCustomAttributesCache, &t2115_TI, &t2115_0_0_0, &t2115_1_0_0, t2115_IOs, &t2115_GC, NULL, NULL, NULL, t2115_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2115)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5066_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Object>
extern MethodInfo m26331_MI;
extern MethodInfo m26332_MI;
static PropertyInfo t5066____Item_PropertyInfo = 
{
	&t5066_TI, "Item", &m26331_MI, &m26332_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5066_PIs[] =
{
	&t5066____Item_PropertyInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
static ParameterInfo t5066_m26333_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26333_GM;
MethodInfo m26333_MI = 
{
	"IndexOf", NULL, &t5066_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5066_m26333_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26333_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t5066_m26334_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26334_GM;
MethodInfo m26334_MI = 
{
	"Insert", NULL, &t5066_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5066_m26334_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26334_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5066_m26335_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26335_GM;
MethodInfo m26335_MI = 
{
	"RemoveAt", NULL, &t5066_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5066_m26335_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26335_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5066_m26331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t41_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26331_GM;
MethodInfo m26331_MI = 
{
	"get_Item", NULL, &t5066_TI, &t41_0_0_0, RuntimeInvoker_t29_t44, t5066_m26331_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26331_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t5066_m26332_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26332_GM;
MethodInfo m26332_MI = 
{
	"set_Item", NULL, &t5066_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5066_m26332_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26332_GM};
static MethodInfo* t5066_MIs[] =
{
	&m26333_MI,
	&m26334_MI,
	&m26335_MI,
	&m26331_MI,
	&m26332_MI,
	NULL
};
static TypeInfo* t5066_ITIs[] = 
{
	&t603_TI,
	&t5065_TI,
	&t5067_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5066_0_0_0;
extern Il2CppType t5066_1_0_0;
struct t5066;
extern Il2CppGenericClass t5066_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5066_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5066_MIs, t5066_PIs, NULL, NULL, NULL, NULL, NULL, &t5066_TI, t5066_ITIs, NULL, &t1908__CustomAttributeCache, &t5066_TI, &t5066_0_0_0, &t5066_1_0_0, NULL, &t5066_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2116.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2116_TI;
#include "t2116MD.h"

#include "t557.h"
#include "t2117.h"
extern TypeInfo t316_TI;
extern TypeInfo t2117_TI;
extern TypeInfo t21_TI;
#include "t2117MD.h"
extern MethodInfo m10329_MI;
extern MethodInfo m10331_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<BackgroundMove>
extern Il2CppType t316_0_0_33;
FieldInfo t2116_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2116_TI, offsetof(t2116, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2116_FIs[] =
{
	&t2116_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t1_0_0_0;
static ParameterInfo t2116_m10317_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10317_GM;
MethodInfo m10317_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2116_m10317_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10317_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2116_m10319_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10319_GM;
MethodInfo m10319_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2116_m10319_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10319_GM};
static MethodInfo* t2116_MIs[] =
{
	&m10317_MI,
	&m10319_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m10319_MI;
extern MethodInfo m10332_MI;
static MethodInfo* t2116_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10319_MI,
	&m10332_MI,
};
extern Il2CppType t2121_0_0_0;
extern TypeInfo t2121_TI;
extern MethodInfo m19556_MI;
extern TypeInfo t1_TI;
extern MethodInfo m10334_MI;
extern TypeInfo t1_TI;
static Il2CppRGCTXData t2116_RGCTXData[8] = 
{
	&t2121_0_0_0/* Type Usage */,
	&t2121_TI/* Class Usage */,
	&m19556_MI/* Method Usage */,
	&t1_TI/* Class Usage */,
	&m10334_MI/* Method Usage */,
	&m10329_MI/* Method Usage */,
	&t1_TI/* Class Usage */,
	&m10331_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2116_0_0_0;
extern Il2CppType t2116_1_0_0;
struct t2116;
extern Il2CppGenericClass t2116_GC;
TypeInfo t2116_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2116_MIs, NULL, t2116_FIs, NULL, &t2117_TI, NULL, NULL, &t2116_TI, NULL, t2116_VT, &EmptyCustomAttributesCache, &t2116_TI, &t2116_0_0_0, &t2116_1_0_0, NULL, &t2116_GC, NULL, NULL, NULL, t2116_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2116), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#include "t2118.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2118_TI;
#include "t2118MD.h"

#include "t2119.h"
extern TypeInfo t2119_TI;
#include "t2119MD.h"
extern MethodInfo m10321_MI;
extern MethodInfo m10323_MI;


extern MethodInfo m10318_MI;
 void m10318_gshared (t2118 * __this, t41 * p0, t557 * p1, t29 * p2, MethodInfo* method)
{
	{
		__this->f1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		(( void (*) (t2119 * __this, t29 * p0, t557 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(__this, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		t316* L_0 = (__this->f1);
		t29 * L_1 = p2;
		ArrayElementTypeCheck (L_0, ((t29 *)L_1));
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)((t29 *)L_1);
		return;
	}
}
extern MethodInfo m10320_MI;
 void m10320_gshared (t2118 * __this, t316* p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		(( void (*) (t2119 * __this, t316* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(__this, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.Object>
extern Il2CppType t316_0_0_33;
FieldInfo t2118_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2118_TI, offsetof(t2118, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2118_FIs[] =
{
	&t2118_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2118_m10318_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10318_GM;
MethodInfo m10318_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2118_m10318_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10318_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2118_m10320_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10320_GM;
MethodInfo m10320_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2118_m10320_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10320_GM};
static MethodInfo* t2118_MIs[] =
{
	&m10318_MI,
	&m10320_MI,
	NULL
};
extern MethodInfo m10324_MI;
static MethodInfo* t2118_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10320_MI,
	&m10324_MI,
};
extern Il2CppType t2120_0_0_0;
extern TypeInfo t2120_TI;
extern MethodInfo m19555_MI;
extern TypeInfo t29_TI;
extern MethodInfo m10326_MI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2118_RGCTXData[8] = 
{
	&t2120_0_0_0/* Type Usage */,
	&t2120_TI/* Class Usage */,
	&m19555_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m10326_MI/* Method Usage */,
	&m10321_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m10323_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2118_0_0_0;
extern Il2CppType t2118_1_0_0;
struct t2118;
extern Il2CppGenericClass t2118_GC;
TypeInfo t2118_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2118_MIs, NULL, t2118_FIs, NULL, &t2119_TI, NULL, NULL, &t2118_TI, NULL, t2118_VT, &EmptyCustomAttributesCache, &t2118_TI, &t2118_0_0_0, &t2118_1_0_0, NULL, &t2118_GC, NULL, NULL, NULL, t2118_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2118), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2120.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2120_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2120MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


 void m10321_gshared (t2119 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		m2790(__this, p0, p1, &m2790_MI);
		t2120 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t2120 *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), &m1597_MI);
		__this->f0 = ((t2120 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
extern MethodInfo m10322_MI;
 void m10322_gshared (t2119 * __this, t2120 * p0, MethodInfo* method)
{
	{
		m2789(__this, &m2789_MI);
		t2120 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t2120 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
 void m10323_gshared (t2119 * __this, t316* p0, MethodInfo* method)
{
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		(( void (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		t2120 * L_2 = (__this->f0);
		bool L_3 = m2791(NULL, L_2, &m2791_MI);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		t2120 * L_4 = (__this->f0);
		int32_t L_5 = 0;
		VirtActionInvoker1< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_4, ((t29 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(p0, L_5)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
	}

IL_003f:
	{
		return;
	}
}
 bool m10324_gshared (t2119 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		t2120 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t2120 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.Object>
extern Il2CppType t2120_0_0_1;
FieldInfo t2119_f0_FieldInfo = 
{
	"Delegate", &t2120_0_0_1, &t2119_TI, offsetof(t2119, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2119_FIs[] =
{
	&t2119_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2119_m10321_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10321_GM;
MethodInfo m10321_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2119_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2119_m10321_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10321_GM};
extern Il2CppType t2120_0_0_0;
static ParameterInfo t2119_m10322_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10322_GM;
MethodInfo m10322_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2119_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2119_m10322_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10322_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2119_m10323_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10323_GM;
MethodInfo m10323_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2119_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2119_m10323_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10323_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2119_m10324_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10324_GM;
MethodInfo m10324_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2119_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2119_m10324_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10324_GM};
static MethodInfo* t2119_MIs[] =
{
	&m10321_MI,
	&m10322_MI,
	&m10323_MI,
	&m10324_MI,
	NULL
};
static MethodInfo* t2119_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10323_MI,
	&m10324_MI,
};
extern TypeInfo t2120_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2119_RGCTXData[5] = 
{
	&t2120_0_0_0/* Type Usage */,
	&t2120_TI/* Class Usage */,
	&m19555_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m10326_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2119_0_0_0;
extern Il2CppType t2119_1_0_0;
extern TypeInfo t556_TI;
struct t2119;
extern Il2CppGenericClass t2119_GC;
TypeInfo t2119_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2119_MIs, NULL, t2119_FIs, NULL, &t556_TI, NULL, NULL, &t2119_TI, NULL, t2119_VT, &EmptyCustomAttributesCache, &t2119_TI, &t2119_0_0_0, &t2119_1_0_0, NULL, &t2119_GC, NULL, NULL, NULL, t2119_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2119), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


extern MethodInfo m10325_MI;
 void m10325_gshared (t2120 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 void m10326_gshared (t2120 * __this, t29 * p0, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m10326((t2120 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m10327_MI;
 t29 * m10327_gshared (t2120 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m10328_MI;
 void m10328_gshared (t2120 * __this, t29 * p0, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2120_m10325_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10325_GM;
MethodInfo m10325_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2120_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2120_m10325_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10325_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2120_m10326_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10326_GM;
MethodInfo m10326_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2120_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2120_m10326_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10326_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2120_m10327_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10327_GM;
MethodInfo m10327_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2120_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2120_m10327_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10327_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2120_m10328_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10328_GM;
MethodInfo m10328_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2120_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2120_m10328_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10328_GM};
static MethodInfo* t2120_MIs[] =
{
	&m10325_MI,
	&m10326_MI,
	&m10327_MI,
	&m10328_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t2120_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10326_MI,
	&m10327_MI,
	&m10328_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2120_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2120_1_0_0;
extern TypeInfo t195_TI;
struct t2120;
extern Il2CppGenericClass t2120_GC;
TypeInfo t2120_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2120_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2120_TI, NULL, t2120_VT, &EmptyCustomAttributesCache, &t2120_TI, &t2120_0_0_0, &t2120_1_0_0, t2120_IOs, &t2120_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2120), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2121.h"
extern TypeInfo t2121_TI;
#include "t2121MD.h"
struct t556;
#define m19556(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<BackgroundMove>
extern Il2CppType t2121_0_0_1;
FieldInfo t2117_f0_FieldInfo = 
{
	"Delegate", &t2121_0_0_1, &t2117_TI, offsetof(t2117, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2117_FIs[] =
{
	&t2117_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2117_m10329_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10329_GM;
MethodInfo m10329_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2117_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2117_m10329_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10329_GM};
extern Il2CppType t2121_0_0_0;
static ParameterInfo t2117_m10330_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2121_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10330_GM;
MethodInfo m10330_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2117_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2117_m10330_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10330_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2117_m10331_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10331_GM;
MethodInfo m10331_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2117_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2117_m10331_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10331_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2117_m10332_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10332_GM;
MethodInfo m10332_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2117_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2117_m10332_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10332_GM};
static MethodInfo* t2117_MIs[] =
{
	&m10329_MI,
	&m10330_MI,
	&m10331_MI,
	&m10332_MI,
	NULL
};
static MethodInfo* t2117_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10331_MI,
	&m10332_MI,
};
extern TypeInfo t2121_TI;
extern TypeInfo t1_TI;
static Il2CppRGCTXData t2117_RGCTXData[5] = 
{
	&t2121_0_0_0/* Type Usage */,
	&t2121_TI/* Class Usage */,
	&m19556_MI/* Method Usage */,
	&t1_TI/* Class Usage */,
	&m10334_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2117_0_0_0;
extern Il2CppType t2117_1_0_0;
struct t2117;
extern Il2CppGenericClass t2117_GC;
TypeInfo t2117_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2117_MIs, NULL, t2117_FIs, NULL, &t556_TI, NULL, NULL, &t2117_TI, NULL, t2117_VT, &EmptyCustomAttributesCache, &t2117_TI, &t2117_0_0_0, &t2117_1_0_0, NULL, &t2117_GC, NULL, NULL, NULL, t2117_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2117), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<BackgroundMove>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2121_m10333_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10333_GM;
MethodInfo m10333_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2121_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2121_m10333_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10333_GM};
extern Il2CppType t1_0_0_0;
static ParameterInfo t2121_m10334_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10334_GM;
MethodInfo m10334_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2121_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2121_m10334_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10334_GM};
extern Il2CppType t1_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2121_m10335_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t1_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10335_GM;
MethodInfo m10335_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2121_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2121_m10335_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10335_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2121_m10336_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10336_GM;
MethodInfo m10336_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2121_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2121_m10336_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10336_GM};
static MethodInfo* t2121_MIs[] =
{
	&m10333_MI,
	&m10334_MI,
	&m10335_MI,
	&m10336_MI,
	NULL
};
extern MethodInfo m10335_MI;
extern MethodInfo m10336_MI;
static MethodInfo* t2121_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10334_MI,
	&m10335_MI,
	&m10336_MI,
};
static Il2CppInterfaceOffsetPair t2121_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2121_1_0_0;
struct t2121;
extern Il2CppGenericClass t2121_GC;
TypeInfo t2121_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2121_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2121_TI, NULL, t2121_VT, &EmptyCustomAttributesCache, &t2121_TI, &t2121_0_0_0, &t2121_1_0_0, t2121_IOs, &t2121_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2121), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3951_TI;

#include "t5.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<ButtonScale>
extern MethodInfo m26336_MI;
static PropertyInfo t3951____Current_PropertyInfo = 
{
	&t3951_TI, "Current", &m26336_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3951_PIs[] =
{
	&t3951____Current_PropertyInfo,
	NULL
};
extern Il2CppType t5_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26336_GM;
MethodInfo m26336_MI = 
{
	"get_Current", NULL, &t3951_TI, &t5_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26336_GM};
static MethodInfo* t3951_MIs[] =
{
	&m26336_MI,
	NULL
};
static TypeInfo* t3951_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3951_0_0_0;
extern Il2CppType t3951_1_0_0;
struct t3951;
extern Il2CppGenericClass t3951_GC;
TypeInfo t3951_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3951_MIs, t3951_PIs, NULL, NULL, NULL, NULL, NULL, &t3951_TI, t3951_ITIs, NULL, &EmptyCustomAttributesCache, &t3951_TI, &t3951_0_0_0, &t3951_1_0_0, NULL, &t3951_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2122.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2122_TI;
#include "t2122MD.h"

extern TypeInfo t5_TI;
extern MethodInfo m10341_MI;
extern MethodInfo m19558_MI;
struct t20;
#define m19558(__this, p0, method) (t5 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<ButtonScale>
extern Il2CppType t20_0_0_1;
FieldInfo t2122_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2122_TI, offsetof(t2122, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2122_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2122_TI, offsetof(t2122, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2122_FIs[] =
{
	&t2122_f0_FieldInfo,
	&t2122_f1_FieldInfo,
	NULL
};
extern MethodInfo m10338_MI;
static PropertyInfo t2122____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2122_TI, "System.Collections.IEnumerator.Current", &m10338_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2122____Current_PropertyInfo = 
{
	&t2122_TI, "Current", &m10341_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2122_PIs[] =
{
	&t2122____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2122____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2122_m10337_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10337_GM;
MethodInfo m10337_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2122_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2122_m10337_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10337_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10338_GM;
MethodInfo m10338_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2122_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10338_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10339_GM;
MethodInfo m10339_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2122_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10339_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10340_GM;
MethodInfo m10340_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2122_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10340_GM};
extern Il2CppType t5_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10341_GM;
MethodInfo m10341_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2122_TI, &t5_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10341_GM};
static MethodInfo* t2122_MIs[] =
{
	&m10337_MI,
	&m10338_MI,
	&m10339_MI,
	&m10340_MI,
	&m10341_MI,
	NULL
};
extern MethodInfo m10340_MI;
extern MethodInfo m10339_MI;
static MethodInfo* t2122_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10338_MI,
	&m10340_MI,
	&m10339_MI,
	&m10341_MI,
};
static TypeInfo* t2122_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3951_TI,
};
static Il2CppInterfaceOffsetPair t2122_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3951_TI, 7},
};
extern TypeInfo t5_TI;
static Il2CppRGCTXData t2122_RGCTXData[3] = 
{
	&m10341_MI/* Method Usage */,
	&t5_TI/* Class Usage */,
	&m19558_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2122_0_0_0;
extern Il2CppType t2122_1_0_0;
extern Il2CppGenericClass t2122_GC;
TypeInfo t2122_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2122_MIs, t2122_PIs, t2122_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2122_TI, t2122_ITIs, t2122_VT, &EmptyCustomAttributesCache, &t2122_TI, &t2122_0_0_0, &t2122_1_0_0, t2122_IOs, &t2122_GC, NULL, NULL, NULL, t2122_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2122)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5068_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<ButtonScale>
extern MethodInfo m26337_MI;
static PropertyInfo t5068____Count_PropertyInfo = 
{
	&t5068_TI, "Count", &m26337_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26338_MI;
static PropertyInfo t5068____IsReadOnly_PropertyInfo = 
{
	&t5068_TI, "IsReadOnly", &m26338_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5068_PIs[] =
{
	&t5068____Count_PropertyInfo,
	&t5068____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26337_GM;
MethodInfo m26337_MI = 
{
	"get_Count", NULL, &t5068_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26337_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26338_GM;
MethodInfo m26338_MI = 
{
	"get_IsReadOnly", NULL, &t5068_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26338_GM};
extern Il2CppType t5_0_0_0;
extern Il2CppType t5_0_0_0;
static ParameterInfo t5068_m26339_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26339_GM;
MethodInfo m26339_MI = 
{
	"Add", NULL, &t5068_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5068_m26339_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26339_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26340_GM;
MethodInfo m26340_MI = 
{
	"Clear", NULL, &t5068_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26340_GM};
extern Il2CppType t5_0_0_0;
static ParameterInfo t5068_m26341_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26341_GM;
MethodInfo m26341_MI = 
{
	"Contains", NULL, &t5068_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5068_m26341_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26341_GM};
extern Il2CppType t3511_0_0_0;
extern Il2CppType t3511_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5068_m26342_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3511_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26342_GM;
MethodInfo m26342_MI = 
{
	"CopyTo", NULL, &t5068_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5068_m26342_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26342_GM};
extern Il2CppType t5_0_0_0;
static ParameterInfo t5068_m26343_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26343_GM;
MethodInfo m26343_MI = 
{
	"Remove", NULL, &t5068_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5068_m26343_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26343_GM};
static MethodInfo* t5068_MIs[] =
{
	&m26337_MI,
	&m26338_MI,
	&m26339_MI,
	&m26340_MI,
	&m26341_MI,
	&m26342_MI,
	&m26343_MI,
	NULL
};
extern TypeInfo t5070_TI;
static TypeInfo* t5068_ITIs[] = 
{
	&t603_TI,
	&t5070_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5068_0_0_0;
extern Il2CppType t5068_1_0_0;
struct t5068;
extern Il2CppGenericClass t5068_GC;
TypeInfo t5068_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5068_MIs, t5068_PIs, NULL, NULL, NULL, NULL, NULL, &t5068_TI, t5068_ITIs, NULL, &EmptyCustomAttributesCache, &t5068_TI, &t5068_0_0_0, &t5068_1_0_0, NULL, &t5068_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<ButtonScale>
extern Il2CppType t3951_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26344_GM;
MethodInfo m26344_MI = 
{
	"GetEnumerator", NULL, &t5070_TI, &t3951_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26344_GM};
static MethodInfo* t5070_MIs[] =
{
	&m26344_MI,
	NULL
};
static TypeInfo* t5070_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5070_0_0_0;
extern Il2CppType t5070_1_0_0;
struct t5070;
extern Il2CppGenericClass t5070_GC;
TypeInfo t5070_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5070_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5070_TI, t5070_ITIs, NULL, &EmptyCustomAttributesCache, &t5070_TI, &t5070_0_0_0, &t5070_1_0_0, NULL, &t5070_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5069_TI;



// Metadata Definition System.Collections.Generic.IList`1<ButtonScale>
extern MethodInfo m26345_MI;
extern MethodInfo m26346_MI;
static PropertyInfo t5069____Item_PropertyInfo = 
{
	&t5069_TI, "Item", &m26345_MI, &m26346_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5069_PIs[] =
{
	&t5069____Item_PropertyInfo,
	NULL
};
extern Il2CppType t5_0_0_0;
static ParameterInfo t5069_m26347_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26347_GM;
MethodInfo m26347_MI = 
{
	"IndexOf", NULL, &t5069_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5069_m26347_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26347_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t5_0_0_0;
static ParameterInfo t5069_m26348_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26348_GM;
MethodInfo m26348_MI = 
{
	"Insert", NULL, &t5069_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5069_m26348_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26348_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5069_m26349_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26349_GM;
MethodInfo m26349_MI = 
{
	"RemoveAt", NULL, &t5069_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5069_m26349_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26349_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5069_m26345_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t5_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26345_GM;
MethodInfo m26345_MI = 
{
	"get_Item", NULL, &t5069_TI, &t5_0_0_0, RuntimeInvoker_t29_t44, t5069_m26345_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26345_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t5_0_0_0;
static ParameterInfo t5069_m26346_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26346_GM;
MethodInfo m26346_MI = 
{
	"set_Item", NULL, &t5069_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5069_m26346_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26346_GM};
static MethodInfo* t5069_MIs[] =
{
	&m26347_MI,
	&m26348_MI,
	&m26349_MI,
	&m26345_MI,
	&m26346_MI,
	NULL
};
static TypeInfo* t5069_ITIs[] = 
{
	&t603_TI,
	&t5068_TI,
	&t5070_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5069_0_0_0;
extern Il2CppType t5069_1_0_0;
struct t5069;
extern Il2CppGenericClass t5069_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5069_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5069_MIs, t5069_PIs, NULL, NULL, NULL, NULL, NULL, &t5069_TI, t5069_ITIs, NULL, &t1908__CustomAttributeCache, &t5069_TI, &t5069_0_0_0, &t5069_1_0_0, NULL, &t5069_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5071_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern MethodInfo m26350_MI;
static PropertyInfo t5071____Count_PropertyInfo = 
{
	&t5071_TI, "Count", &m26350_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26351_MI;
static PropertyInfo t5071____IsReadOnly_PropertyInfo = 
{
	&t5071_TI, "IsReadOnly", &m26351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5071_PIs[] =
{
	&t5071____Count_PropertyInfo,
	&t5071____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26350_GM;
MethodInfo m26350_MI = 
{
	"get_Count", NULL, &t5071_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26350_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26351_GM;
MethodInfo m26351_MI = 
{
	"get_IsReadOnly", NULL, &t5071_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26351_GM};
extern Il2CppType t30_0_0_0;
extern Il2CppType t30_0_0_0;
static ParameterInfo t5071_m26352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26352_GM;
MethodInfo m26352_MI = 
{
	"Add", NULL, &t5071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5071_m26352_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26352_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26353_GM;
MethodInfo m26353_MI = 
{
	"Clear", NULL, &t5071_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26353_GM};
extern Il2CppType t30_0_0_0;
static ParameterInfo t5071_m26354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26354_GM;
MethodInfo m26354_MI = 
{
	"Contains", NULL, &t5071_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5071_m26354_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26354_GM};
extern Il2CppType t3798_0_0_0;
extern Il2CppType t3798_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5071_m26355_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3798_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26355_GM;
MethodInfo m26355_MI = 
{
	"CopyTo", NULL, &t5071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5071_m26355_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26355_GM};
extern Il2CppType t30_0_0_0;
static ParameterInfo t5071_m26356_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26356_GM;
MethodInfo m26356_MI = 
{
	"Remove", NULL, &t5071_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5071_m26356_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26356_GM};
static MethodInfo* t5071_MIs[] =
{
	&m26350_MI,
	&m26351_MI,
	&m26352_MI,
	&m26353_MI,
	&m26354_MI,
	&m26355_MI,
	&m26356_MI,
	NULL
};
extern TypeInfo t5073_TI;
static TypeInfo* t5071_ITIs[] = 
{
	&t603_TI,
	&t5073_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5071_0_0_0;
extern Il2CppType t5071_1_0_0;
struct t5071;
extern Il2CppGenericClass t5071_GC;
TypeInfo t5071_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5071_MIs, t5071_PIs, NULL, NULL, NULL, NULL, NULL, &t5071_TI, t5071_ITIs, NULL, &EmptyCustomAttributesCache, &t5071_TI, &t5071_0_0_0, &t5071_1_0_0, NULL, &t5071_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern Il2CppType t3953_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26357_GM;
MethodInfo m26357_MI = 
{
	"GetEnumerator", NULL, &t5073_TI, &t3953_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26357_GM};
static MethodInfo* t5073_MIs[] =
{
	&m26357_MI,
	NULL
};
static TypeInfo* t5073_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5073_0_0_0;
extern Il2CppType t5073_1_0_0;
struct t5073;
extern Il2CppGenericClass t5073_GC;
TypeInfo t5073_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5073_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5073_TI, t5073_ITIs, NULL, &EmptyCustomAttributesCache, &t5073_TI, &t5073_0_0_0, &t5073_1_0_0, NULL, &t5073_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3953_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern MethodInfo m26358_MI;
static PropertyInfo t3953____Current_PropertyInfo = 
{
	&t3953_TI, "Current", &m26358_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3953_PIs[] =
{
	&t3953____Current_PropertyInfo,
	NULL
};
extern Il2CppType t30_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26358_GM;
MethodInfo m26358_MI = 
{
	"get_Current", NULL, &t3953_TI, &t30_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26358_GM};
static MethodInfo* t3953_MIs[] =
{
	&m26358_MI,
	NULL
};
static TypeInfo* t3953_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3953_0_0_0;
extern Il2CppType t3953_1_0_0;
struct t3953;
extern Il2CppGenericClass t3953_GC;
TypeInfo t3953_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3953_MIs, t3953_PIs, NULL, NULL, NULL, NULL, NULL, &t3953_TI, t3953_ITIs, NULL, &EmptyCustomAttributesCache, &t3953_TI, &t3953_0_0_0, &t3953_1_0_0, NULL, &t3953_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2123.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2123_TI;
#include "t2123MD.h"

extern TypeInfo t30_TI;
extern MethodInfo m10346_MI;
extern MethodInfo m19569_MI;
struct t20;
#define m19569(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2123_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2123_TI, offsetof(t2123, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2123_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2123_TI, offsetof(t2123, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2123_FIs[] =
{
	&t2123_f0_FieldInfo,
	&t2123_f1_FieldInfo,
	NULL
};
extern MethodInfo m10343_MI;
static PropertyInfo t2123____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2123_TI, "System.Collections.IEnumerator.Current", &m10343_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2123____Current_PropertyInfo = 
{
	&t2123_TI, "Current", &m10346_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2123_PIs[] =
{
	&t2123____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2123____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2123_m10342_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10342_GM;
MethodInfo m10342_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2123_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2123_m10342_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10342_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10343_GM;
MethodInfo m10343_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2123_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10343_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10344_GM;
MethodInfo m10344_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2123_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10344_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10345_GM;
MethodInfo m10345_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2123_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10345_GM};
extern Il2CppType t30_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10346_GM;
MethodInfo m10346_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2123_TI, &t30_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10346_GM};
static MethodInfo* t2123_MIs[] =
{
	&m10342_MI,
	&m10343_MI,
	&m10344_MI,
	&m10345_MI,
	&m10346_MI,
	NULL
};
extern MethodInfo m10345_MI;
extern MethodInfo m10344_MI;
static MethodInfo* t2123_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10343_MI,
	&m10345_MI,
	&m10344_MI,
	&m10346_MI,
};
static TypeInfo* t2123_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3953_TI,
};
static Il2CppInterfaceOffsetPair t2123_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3953_TI, 7},
};
extern TypeInfo t30_TI;
static Il2CppRGCTXData t2123_RGCTXData[3] = 
{
	&m10346_MI/* Method Usage */,
	&t30_TI/* Class Usage */,
	&m19569_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2123_0_0_0;
extern Il2CppType t2123_1_0_0;
extern Il2CppGenericClass t2123_GC;
TypeInfo t2123_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2123_MIs, t2123_PIs, t2123_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2123_TI, t2123_ITIs, t2123_VT, &EmptyCustomAttributesCache, &t2123_TI, &t2123_0_0_0, &t2123_1_0_0, t2123_IOs, &t2123_GC, NULL, NULL, NULL, t2123_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2123)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5072_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern MethodInfo m26359_MI;
extern MethodInfo m26360_MI;
static PropertyInfo t5072____Item_PropertyInfo = 
{
	&t5072_TI, "Item", &m26359_MI, &m26360_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5072_PIs[] =
{
	&t5072____Item_PropertyInfo,
	NULL
};
extern Il2CppType t30_0_0_0;
static ParameterInfo t5072_m26361_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26361_GM;
MethodInfo m26361_MI = 
{
	"IndexOf", NULL, &t5072_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5072_m26361_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26361_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t30_0_0_0;
static ParameterInfo t5072_m26362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26362_GM;
MethodInfo m26362_MI = 
{
	"Insert", NULL, &t5072_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5072_m26362_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26362_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5072_m26363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26363_GM;
MethodInfo m26363_MI = 
{
	"RemoveAt", NULL, &t5072_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5072_m26363_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26363_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5072_m26359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t30_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26359_GM;
MethodInfo m26359_MI = 
{
	"get_Item", NULL, &t5072_TI, &t30_0_0_0, RuntimeInvoker_t29_t44, t5072_m26359_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26359_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t30_0_0_0;
static ParameterInfo t5072_m26360_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26360_GM;
MethodInfo m26360_MI = 
{
	"set_Item", NULL, &t5072_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5072_m26360_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26360_GM};
static MethodInfo* t5072_MIs[] =
{
	&m26361_MI,
	&m26362_MI,
	&m26363_MI,
	&m26359_MI,
	&m26360_MI,
	NULL
};
static TypeInfo* t5072_ITIs[] = 
{
	&t603_TI,
	&t5071_TI,
	&t5073_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5072_0_0_0;
extern Il2CppType t5072_1_0_0;
struct t5072;
extern Il2CppGenericClass t5072_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5072_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5072_MIs, t5072_PIs, NULL, NULL, NULL, NULL, NULL, &t5072_TI, t5072_ITIs, NULL, &t1908__CustomAttributeCache, &t5072_TI, &t5072_0_0_0, &t5072_1_0_0, NULL, &t5072_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2216_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEventSystemHandler>
extern MethodInfo m26364_MI;
static PropertyInfo t2216____Count_PropertyInfo = 
{
	&t2216_TI, "Count", &m26364_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26365_MI;
static PropertyInfo t2216____IsReadOnly_PropertyInfo = 
{
	&t2216_TI, "IsReadOnly", &m26365_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2216_PIs[] =
{
	&t2216____Count_PropertyInfo,
	&t2216____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26364_GM;
MethodInfo m26364_MI = 
{
	"get_Count", NULL, &t2216_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26364_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26365_GM;
MethodInfo m26365_MI = 
{
	"get_IsReadOnly", NULL, &t2216_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26365_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2216_m20058_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20058_GM;
MethodInfo m20058_MI = 
{
	"Add", NULL, &t2216_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2216_m20058_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20058_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26366_GM;
MethodInfo m26366_MI = 
{
	"Clear", NULL, &t2216_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26366_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2216_m26367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26367_GM;
MethodInfo m26367_MI = 
{
	"Contains", NULL, &t2216_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2216_m26367_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26367_GM};
extern Il2CppType t2214_0_0_0;
extern Il2CppType t2214_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2216_m26368_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2214_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26368_GM;
MethodInfo m26368_MI = 
{
	"CopyTo", NULL, &t2216_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2216_m26368_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26368_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2216_m26369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26369_GM;
MethodInfo m26369_MI = 
{
	"Remove", NULL, &t2216_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2216_m26369_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26369_GM};
static MethodInfo* t2216_MIs[] =
{
	&m26364_MI,
	&m26365_MI,
	&m20058_MI,
	&m26366_MI,
	&m26367_MI,
	&m26368_MI,
	&m26369_MI,
	NULL
};
extern TypeInfo t2217_TI;
static TypeInfo* t2216_ITIs[] = 
{
	&t603_TI,
	&t2217_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2216_0_0_0;
extern Il2CppType t2216_1_0_0;
struct t2216;
extern Il2CppGenericClass t2216_GC;
TypeInfo t2216_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2216_MIs, t2216_PIs, NULL, NULL, NULL, NULL, NULL, &t2216_TI, t2216_ITIs, NULL, &EmptyCustomAttributesCache, &t2216_TI, &t2216_0_0_0, &t2216_1_0_0, NULL, &t2216_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t2215_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26370_GM;
MethodInfo m26370_MI = 
{
	"GetEnumerator", NULL, &t2217_TI, &t2215_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26370_GM};
static MethodInfo* t2217_MIs[] =
{
	&m26370_MI,
	NULL
};
static TypeInfo* t2217_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2217_0_0_0;
extern Il2CppType t2217_1_0_0;
struct t2217;
extern Il2CppGenericClass t2217_GC;
TypeInfo t2217_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2217_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2217_TI, t2217_ITIs, NULL, &EmptyCustomAttributesCache, &t2217_TI, &t2217_0_0_0, &t2217_1_0_0, NULL, &t2217_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2215_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IEventSystemHandler>
extern MethodInfo m26371_MI;
static PropertyInfo t2215____Current_PropertyInfo = 
{
	&t2215_TI, "Current", &m26371_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2215_PIs[] =
{
	&t2215____Current_PropertyInfo,
	NULL
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26371_GM;
MethodInfo m26371_MI = 
{
	"get_Current", NULL, &t2215_TI, &t31_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26371_GM};
static MethodInfo* t2215_MIs[] =
{
	&m26371_MI,
	NULL
};
static TypeInfo* t2215_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2215_0_0_0;
extern Il2CppType t2215_1_0_0;
struct t2215;
extern Il2CppGenericClass t2215_GC;
TypeInfo t2215_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2215_MIs, t2215_PIs, NULL, NULL, NULL, NULL, NULL, &t2215_TI, t2215_ITIs, NULL, &EmptyCustomAttributesCache, &t2215_TI, &t2215_0_0_0, &t2215_1_0_0, NULL, &t2215_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2124.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2124_TI;
#include "t2124MD.h"

extern TypeInfo t31_TI;
extern MethodInfo m10351_MI;
extern MethodInfo m19580_MI;
struct t20;
#define m19580(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2124_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2124_TI, offsetof(t2124, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2124_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2124_TI, offsetof(t2124, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2124_FIs[] =
{
	&t2124_f0_FieldInfo,
	&t2124_f1_FieldInfo,
	NULL
};
extern MethodInfo m10348_MI;
static PropertyInfo t2124____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2124_TI, "System.Collections.IEnumerator.Current", &m10348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2124____Current_PropertyInfo = 
{
	&t2124_TI, "Current", &m10351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2124_PIs[] =
{
	&t2124____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2124____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2124_m10347_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10347_GM;
MethodInfo m10347_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2124_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2124_m10347_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10347_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10348_GM;
MethodInfo m10348_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2124_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10348_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10349_GM;
MethodInfo m10349_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2124_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10349_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10350_GM;
MethodInfo m10350_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2124_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10350_GM};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10351_GM;
MethodInfo m10351_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2124_TI, &t31_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10351_GM};
static MethodInfo* t2124_MIs[] =
{
	&m10347_MI,
	&m10348_MI,
	&m10349_MI,
	&m10350_MI,
	&m10351_MI,
	NULL
};
extern MethodInfo m10350_MI;
extern MethodInfo m10349_MI;
static MethodInfo* t2124_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10348_MI,
	&m10350_MI,
	&m10349_MI,
	&m10351_MI,
};
static TypeInfo* t2124_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2215_TI,
};
static Il2CppInterfaceOffsetPair t2124_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2215_TI, 7},
};
extern TypeInfo t31_TI;
static Il2CppRGCTXData t2124_RGCTXData[3] = 
{
	&m10351_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m19580_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2124_0_0_0;
extern Il2CppType t2124_1_0_0;
extern Il2CppGenericClass t2124_GC;
TypeInfo t2124_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2124_MIs, t2124_PIs, t2124_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2124_TI, t2124_ITIs, t2124_VT, &EmptyCustomAttributesCache, &t2124_TI, &t2124_0_0_0, &t2124_1_0_0, t2124_IOs, &t2124_GC, NULL, NULL, NULL, t2124_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2124)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t312_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEventSystemHandler>
extern MethodInfo m26372_MI;
extern MethodInfo m26373_MI;
static PropertyInfo t312____Item_PropertyInfo = 
{
	&t312_TI, "Item", &m26372_MI, &m26373_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t312_PIs[] =
{
	&t312____Item_PropertyInfo,
	NULL
};
extern Il2CppType t31_0_0_0;
static ParameterInfo t312_m26374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26374_GM;
MethodInfo m26374_MI = 
{
	"IndexOf", NULL, &t312_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t312_m26374_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26374_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t312_m26375_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26375_GM;
MethodInfo m26375_MI = 
{
	"Insert", NULL, &t312_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t312_m26375_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26375_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t312_m26376_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26376_GM;
MethodInfo m26376_MI = 
{
	"RemoveAt", NULL, &t312_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t312_m26376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26376_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t312_m26372_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26372_GM;
MethodInfo m26372_MI = 
{
	"get_Item", NULL, &t312_TI, &t31_0_0_0, RuntimeInvoker_t29_t44, t312_m26372_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26372_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t312_m26373_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26373_GM;
MethodInfo m26373_MI = 
{
	"set_Item", NULL, &t312_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t312_m26373_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26373_GM};
static MethodInfo* t312_MIs[] =
{
	&m26374_MI,
	&m26375_MI,
	&m26376_MI,
	&m26372_MI,
	&m26373_MI,
	NULL
};
static TypeInfo* t312_ITIs[] = 
{
	&t603_TI,
	&t2216_TI,
	&t2217_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t312_0_0_0;
extern Il2CppType t312_1_0_0;
struct t312;
extern Il2CppGenericClass t312_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t312_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t312_MIs, t312_PIs, NULL, NULL, NULL, NULL, NULL, &t312_TI, t312_ITIs, NULL, &t1908__CustomAttributeCache, &t312_TI, &t312_0_0_0, &t312_1_0_0, NULL, &t312_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5074_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>
extern MethodInfo m26377_MI;
static PropertyInfo t5074____Count_PropertyInfo = 
{
	&t5074_TI, "Count", &m26377_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26378_MI;
static PropertyInfo t5074____IsReadOnly_PropertyInfo = 
{
	&t5074_TI, "IsReadOnly", &m26378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5074_PIs[] =
{
	&t5074____Count_PropertyInfo,
	&t5074____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26377_GM;
MethodInfo m26377_MI = 
{
	"get_Count", NULL, &t5074_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26377_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26378_GM;
MethodInfo m26378_MI = 
{
	"get_IsReadOnly", NULL, &t5074_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26378_GM};
extern Il2CppType t32_0_0_0;
extern Il2CppType t32_0_0_0;
static ParameterInfo t5074_m26379_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26379_GM;
MethodInfo m26379_MI = 
{
	"Add", NULL, &t5074_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5074_m26379_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26379_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26380_GM;
MethodInfo m26380_MI = 
{
	"Clear", NULL, &t5074_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26380_GM};
extern Il2CppType t32_0_0_0;
static ParameterInfo t5074_m26381_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26381_GM;
MethodInfo m26381_MI = 
{
	"Contains", NULL, &t5074_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5074_m26381_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26381_GM};
extern Il2CppType t3799_0_0_0;
extern Il2CppType t3799_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5074_m26382_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3799_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26382_GM;
MethodInfo m26382_MI = 
{
	"CopyTo", NULL, &t5074_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5074_m26382_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26382_GM};
extern Il2CppType t32_0_0_0;
static ParameterInfo t5074_m26383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26383_GM;
MethodInfo m26383_MI = 
{
	"Remove", NULL, &t5074_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5074_m26383_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26383_GM};
static MethodInfo* t5074_MIs[] =
{
	&m26377_MI,
	&m26378_MI,
	&m26379_MI,
	&m26380_MI,
	&m26381_MI,
	&m26382_MI,
	&m26383_MI,
	NULL
};
extern TypeInfo t5076_TI;
static TypeInfo* t5074_ITIs[] = 
{
	&t603_TI,
	&t5076_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5074_0_0_0;
extern Il2CppType t5074_1_0_0;
struct t5074;
extern Il2CppGenericClass t5074_GC;
TypeInfo t5074_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5074_MIs, t5074_PIs, NULL, NULL, NULL, NULL, NULL, &t5074_TI, t5074_ITIs, NULL, &EmptyCustomAttributesCache, &t5074_TI, &t5074_0_0_0, &t5074_1_0_0, NULL, &t5074_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerExitHandler>
extern Il2CppType t3956_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26384_GM;
MethodInfo m26384_MI = 
{
	"GetEnumerator", NULL, &t5076_TI, &t3956_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26384_GM};
static MethodInfo* t5076_MIs[] =
{
	&m26384_MI,
	NULL
};
static TypeInfo* t5076_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5076_0_0_0;
extern Il2CppType t5076_1_0_0;
struct t5076;
extern Il2CppGenericClass t5076_GC;
TypeInfo t5076_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5076_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5076_TI, t5076_ITIs, NULL, &EmptyCustomAttributesCache, &t5076_TI, &t5076_0_0_0, &t5076_1_0_0, NULL, &t5076_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3956_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>
extern MethodInfo m26385_MI;
static PropertyInfo t3956____Current_PropertyInfo = 
{
	&t3956_TI, "Current", &m26385_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3956_PIs[] =
{
	&t3956____Current_PropertyInfo,
	NULL
};
extern Il2CppType t32_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26385_GM;
MethodInfo m26385_MI = 
{
	"get_Current", NULL, &t3956_TI, &t32_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26385_GM};
static MethodInfo* t3956_MIs[] =
{
	&m26385_MI,
	NULL
};
static TypeInfo* t3956_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3956_0_0_0;
extern Il2CppType t3956_1_0_0;
struct t3956;
extern Il2CppGenericClass t3956_GC;
TypeInfo t3956_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3956_MIs, t3956_PIs, NULL, NULL, NULL, NULL, NULL, &t3956_TI, t3956_ITIs, NULL, &EmptyCustomAttributesCache, &t3956_TI, &t3956_0_0_0, &t3956_1_0_0, NULL, &t3956_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2125.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2125_TI;
#include "t2125MD.h"

extern TypeInfo t32_TI;
extern MethodInfo m10356_MI;
extern MethodInfo m19591_MI;
struct t20;
#define m19591(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2125_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2125_TI, offsetof(t2125, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2125_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2125_TI, offsetof(t2125, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2125_FIs[] =
{
	&t2125_f0_FieldInfo,
	&t2125_f1_FieldInfo,
	NULL
};
extern MethodInfo m10353_MI;
static PropertyInfo t2125____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2125_TI, "System.Collections.IEnumerator.Current", &m10353_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2125____Current_PropertyInfo = 
{
	&t2125_TI, "Current", &m10356_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2125_PIs[] =
{
	&t2125____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2125____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2125_m10352_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10352_GM;
MethodInfo m10352_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2125_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2125_m10352_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10352_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10353_GM;
MethodInfo m10353_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2125_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10353_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10354_GM;
MethodInfo m10354_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2125_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10354_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10355_GM;
MethodInfo m10355_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2125_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10355_GM};
extern Il2CppType t32_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10356_GM;
MethodInfo m10356_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2125_TI, &t32_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10356_GM};
static MethodInfo* t2125_MIs[] =
{
	&m10352_MI,
	&m10353_MI,
	&m10354_MI,
	&m10355_MI,
	&m10356_MI,
	NULL
};
extern MethodInfo m10355_MI;
extern MethodInfo m10354_MI;
static MethodInfo* t2125_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10353_MI,
	&m10355_MI,
	&m10354_MI,
	&m10356_MI,
};
static TypeInfo* t2125_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3956_TI,
};
static Il2CppInterfaceOffsetPair t2125_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3956_TI, 7},
};
extern TypeInfo t32_TI;
static Il2CppRGCTXData t2125_RGCTXData[3] = 
{
	&m10356_MI/* Method Usage */,
	&t32_TI/* Class Usage */,
	&m19591_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2125_0_0_0;
extern Il2CppType t2125_1_0_0;
extern Il2CppGenericClass t2125_GC;
TypeInfo t2125_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2125_MIs, t2125_PIs, t2125_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2125_TI, t2125_ITIs, t2125_VT, &EmptyCustomAttributesCache, &t2125_TI, &t2125_0_0_0, &t2125_1_0_0, t2125_IOs, &t2125_GC, NULL, NULL, NULL, t2125_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2125)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5075_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>
extern MethodInfo m26386_MI;
extern MethodInfo m26387_MI;
static PropertyInfo t5075____Item_PropertyInfo = 
{
	&t5075_TI, "Item", &m26386_MI, &m26387_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5075_PIs[] =
{
	&t5075____Item_PropertyInfo,
	NULL
};
extern Il2CppType t32_0_0_0;
static ParameterInfo t5075_m26388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26388_GM;
MethodInfo m26388_MI = 
{
	"IndexOf", NULL, &t5075_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5075_m26388_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26388_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t32_0_0_0;
static ParameterInfo t5075_m26389_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26389_GM;
MethodInfo m26389_MI = 
{
	"Insert", NULL, &t5075_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5075_m26389_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26389_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5075_m26390_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26390_GM;
MethodInfo m26390_MI = 
{
	"RemoveAt", NULL, &t5075_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5075_m26390_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26390_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5075_m26386_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t32_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26386_GM;
MethodInfo m26386_MI = 
{
	"get_Item", NULL, &t5075_TI, &t32_0_0_0, RuntimeInvoker_t29_t44, t5075_m26386_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26386_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t32_0_0_0;
static ParameterInfo t5075_m26387_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26387_GM;
MethodInfo m26387_MI = 
{
	"set_Item", NULL, &t5075_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5075_m26387_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26387_GM};
static MethodInfo* t5075_MIs[] =
{
	&m26388_MI,
	&m26389_MI,
	&m26390_MI,
	&m26386_MI,
	&m26387_MI,
	NULL
};
static TypeInfo* t5075_ITIs[] = 
{
	&t603_TI,
	&t5074_TI,
	&t5076_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5075_0_0_0;
extern Il2CppType t5075_1_0_0;
struct t5075;
extern Il2CppGenericClass t5075_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5075_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5075_MIs, t5075_PIs, NULL, NULL, NULL, NULL, NULL, &t5075_TI, t5075_ITIs, NULL, &t1908__CustomAttributeCache, &t5075_TI, &t5075_0_0_0, &t5075_1_0_0, NULL, &t5075_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2126.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2126_TI;
#include "t2126MD.h"

#include "t2127.h"
extern TypeInfo t2127_TI;
#include "t2127MD.h"
extern MethodInfo m10359_MI;
extern MethodInfo m10361_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ButtonScale>
extern Il2CppType t316_0_0_33;
FieldInfo t2126_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2126_TI, offsetof(t2126, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2126_FIs[] =
{
	&t2126_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t5_0_0_0;
static ParameterInfo t2126_m10357_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10357_GM;
MethodInfo m10357_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2126_m10357_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10357_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2126_m10358_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10358_GM;
MethodInfo m10358_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2126_m10358_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10358_GM};
static MethodInfo* t2126_MIs[] =
{
	&m10357_MI,
	&m10358_MI,
	NULL
};
extern MethodInfo m10358_MI;
extern MethodInfo m10362_MI;
static MethodInfo* t2126_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10358_MI,
	&m10362_MI,
};
extern Il2CppType t2128_0_0_0;
extern TypeInfo t2128_TI;
extern MethodInfo m19601_MI;
extern TypeInfo t5_TI;
extern MethodInfo m10364_MI;
extern TypeInfo t5_TI;
static Il2CppRGCTXData t2126_RGCTXData[8] = 
{
	&t2128_0_0_0/* Type Usage */,
	&t2128_TI/* Class Usage */,
	&m19601_MI/* Method Usage */,
	&t5_TI/* Class Usage */,
	&m10364_MI/* Method Usage */,
	&m10359_MI/* Method Usage */,
	&t5_TI/* Class Usage */,
	&m10361_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2126_0_0_0;
extern Il2CppType t2126_1_0_0;
struct t2126;
extern Il2CppGenericClass t2126_GC;
TypeInfo t2126_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2126_MIs, NULL, t2126_FIs, NULL, &t2127_TI, NULL, NULL, &t2126_TI, NULL, t2126_VT, &EmptyCustomAttributesCache, &t2126_TI, &t2126_0_0_0, &t2126_1_0_0, NULL, &t2126_GC, NULL, NULL, NULL, t2126_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2126), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2128.h"
extern TypeInfo t2128_TI;
#include "t2128MD.h"
struct t556;
#define m19601(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<ButtonScale>
extern Il2CppType t2128_0_0_1;
FieldInfo t2127_f0_FieldInfo = 
{
	"Delegate", &t2128_0_0_1, &t2127_TI, offsetof(t2127, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2127_FIs[] =
{
	&t2127_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2127_m10359_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10359_GM;
MethodInfo m10359_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2127_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2127_m10359_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10359_GM};
extern Il2CppType t2128_0_0_0;
static ParameterInfo t2127_m10360_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10360_GM;
MethodInfo m10360_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2127_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2127_m10360_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10360_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2127_m10361_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10361_GM;
MethodInfo m10361_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2127_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2127_m10361_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10361_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2127_m10362_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10362_GM;
MethodInfo m10362_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2127_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2127_m10362_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10362_GM};
static MethodInfo* t2127_MIs[] =
{
	&m10359_MI,
	&m10360_MI,
	&m10361_MI,
	&m10362_MI,
	NULL
};
static MethodInfo* t2127_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10361_MI,
	&m10362_MI,
};
extern TypeInfo t2128_TI;
extern TypeInfo t5_TI;
static Il2CppRGCTXData t2127_RGCTXData[5] = 
{
	&t2128_0_0_0/* Type Usage */,
	&t2128_TI/* Class Usage */,
	&m19601_MI/* Method Usage */,
	&t5_TI/* Class Usage */,
	&m10364_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2127_0_0_0;
extern Il2CppType t2127_1_0_0;
struct t2127;
extern Il2CppGenericClass t2127_GC;
TypeInfo t2127_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2127_MIs, NULL, t2127_FIs, NULL, &t556_TI, NULL, NULL, &t2127_TI, NULL, t2127_VT, &EmptyCustomAttributesCache, &t2127_TI, &t2127_0_0_0, &t2127_1_0_0, NULL, &t2127_GC, NULL, NULL, NULL, t2127_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2127), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<ButtonScale>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2128_m10363_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10363_GM;
MethodInfo m10363_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2128_m10363_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10363_GM};
extern Il2CppType t5_0_0_0;
static ParameterInfo t2128_m10364_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10364_GM;
MethodInfo m10364_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2128_m10364_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10364_GM};
extern Il2CppType t5_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2128_m10365_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t5_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10365_GM;
MethodInfo m10365_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2128_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2128_m10365_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10365_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2128_m10366_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10366_GM;
MethodInfo m10366_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2128_m10366_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10366_GM};
static MethodInfo* t2128_MIs[] =
{
	&m10363_MI,
	&m10364_MI,
	&m10365_MI,
	&m10366_MI,
	NULL
};
extern MethodInfo m10365_MI;
extern MethodInfo m10366_MI;
static MethodInfo* t2128_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10364_MI,
	&m10365_MI,
	&m10366_MI,
};
static Il2CppInterfaceOffsetPair t2128_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2128_1_0_0;
struct t2128;
extern Il2CppGenericClass t2128_GC;
TypeInfo t2128_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2128_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2128_TI, NULL, t2128_VT, &EmptyCustomAttributesCache, &t2128_TI, &t2128_0_0_0, &t2128_1_0_0, t2128_IOs, &t2128_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2128), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3958_TI;

#include "t8.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<LoadScene_0>
extern MethodInfo m26391_MI;
static PropertyInfo t3958____Current_PropertyInfo = 
{
	&t3958_TI, "Current", &m26391_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3958_PIs[] =
{
	&t3958____Current_PropertyInfo,
	NULL
};
extern Il2CppType t8_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26391_GM;
MethodInfo m26391_MI = 
{
	"get_Current", NULL, &t3958_TI, &t8_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26391_GM};
static MethodInfo* t3958_MIs[] =
{
	&m26391_MI,
	NULL
};
static TypeInfo* t3958_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3958_0_0_0;
extern Il2CppType t3958_1_0_0;
struct t3958;
extern Il2CppGenericClass t3958_GC;
TypeInfo t3958_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3958_MIs, t3958_PIs, NULL, NULL, NULL, NULL, NULL, &t3958_TI, t3958_ITIs, NULL, &EmptyCustomAttributesCache, &t3958_TI, &t3958_0_0_0, &t3958_1_0_0, NULL, &t3958_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2129.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2129_TI;
#include "t2129MD.h"

extern TypeInfo t8_TI;
extern MethodInfo m10371_MI;
extern MethodInfo m19603_MI;
struct t20;
#define m19603(__this, p0, method) (t8 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<LoadScene_0>
extern Il2CppType t20_0_0_1;
FieldInfo t2129_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2129_TI, offsetof(t2129, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2129_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2129_TI, offsetof(t2129, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2129_FIs[] =
{
	&t2129_f0_FieldInfo,
	&t2129_f1_FieldInfo,
	NULL
};
extern MethodInfo m10368_MI;
static PropertyInfo t2129____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2129_TI, "System.Collections.IEnumerator.Current", &m10368_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2129____Current_PropertyInfo = 
{
	&t2129_TI, "Current", &m10371_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2129_PIs[] =
{
	&t2129____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2129____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2129_m10367_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10367_GM;
MethodInfo m10367_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2129_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2129_m10367_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10367_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10368_GM;
MethodInfo m10368_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2129_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10368_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10369_GM;
MethodInfo m10369_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2129_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10369_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10370_GM;
MethodInfo m10370_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2129_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10370_GM};
extern Il2CppType t8_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10371_GM;
MethodInfo m10371_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2129_TI, &t8_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10371_GM};
static MethodInfo* t2129_MIs[] =
{
	&m10367_MI,
	&m10368_MI,
	&m10369_MI,
	&m10370_MI,
	&m10371_MI,
	NULL
};
extern MethodInfo m10370_MI;
extern MethodInfo m10369_MI;
static MethodInfo* t2129_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10368_MI,
	&m10370_MI,
	&m10369_MI,
	&m10371_MI,
};
static TypeInfo* t2129_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3958_TI,
};
static Il2CppInterfaceOffsetPair t2129_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3958_TI, 7},
};
extern TypeInfo t8_TI;
static Il2CppRGCTXData t2129_RGCTXData[3] = 
{
	&m10371_MI/* Method Usage */,
	&t8_TI/* Class Usage */,
	&m19603_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2129_0_0_0;
extern Il2CppType t2129_1_0_0;
extern Il2CppGenericClass t2129_GC;
TypeInfo t2129_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2129_MIs, t2129_PIs, t2129_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2129_TI, t2129_ITIs, t2129_VT, &EmptyCustomAttributesCache, &t2129_TI, &t2129_0_0_0, &t2129_1_0_0, t2129_IOs, &t2129_GC, NULL, NULL, NULL, t2129_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2129)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5077_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<LoadScene_0>
extern MethodInfo m26392_MI;
static PropertyInfo t5077____Count_PropertyInfo = 
{
	&t5077_TI, "Count", &m26392_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26393_MI;
static PropertyInfo t5077____IsReadOnly_PropertyInfo = 
{
	&t5077_TI, "IsReadOnly", &m26393_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5077_PIs[] =
{
	&t5077____Count_PropertyInfo,
	&t5077____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26392_GM;
MethodInfo m26392_MI = 
{
	"get_Count", NULL, &t5077_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26392_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26393_GM;
MethodInfo m26393_MI = 
{
	"get_IsReadOnly", NULL, &t5077_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26393_GM};
extern Il2CppType t8_0_0_0;
extern Il2CppType t8_0_0_0;
static ParameterInfo t5077_m26394_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26394_GM;
MethodInfo m26394_MI = 
{
	"Add", NULL, &t5077_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5077_m26394_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26394_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26395_GM;
MethodInfo m26395_MI = 
{
	"Clear", NULL, &t5077_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26395_GM};
extern Il2CppType t8_0_0_0;
static ParameterInfo t5077_m26396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26396_GM;
MethodInfo m26396_MI = 
{
	"Contains", NULL, &t5077_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5077_m26396_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26396_GM};
extern Il2CppType t3512_0_0_0;
extern Il2CppType t3512_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5077_m26397_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3512_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26397_GM;
MethodInfo m26397_MI = 
{
	"CopyTo", NULL, &t5077_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5077_m26397_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26397_GM};
extern Il2CppType t8_0_0_0;
static ParameterInfo t5077_m26398_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26398_GM;
MethodInfo m26398_MI = 
{
	"Remove", NULL, &t5077_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5077_m26398_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26398_GM};
static MethodInfo* t5077_MIs[] =
{
	&m26392_MI,
	&m26393_MI,
	&m26394_MI,
	&m26395_MI,
	&m26396_MI,
	&m26397_MI,
	&m26398_MI,
	NULL
};
extern TypeInfo t5079_TI;
static TypeInfo* t5077_ITIs[] = 
{
	&t603_TI,
	&t5079_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5077_0_0_0;
extern Il2CppType t5077_1_0_0;
struct t5077;
extern Il2CppGenericClass t5077_GC;
TypeInfo t5077_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5077_MIs, t5077_PIs, NULL, NULL, NULL, NULL, NULL, &t5077_TI, t5077_ITIs, NULL, &EmptyCustomAttributesCache, &t5077_TI, &t5077_0_0_0, &t5077_1_0_0, NULL, &t5077_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<LoadScene_0>
extern Il2CppType t3958_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26399_GM;
MethodInfo m26399_MI = 
{
	"GetEnumerator", NULL, &t5079_TI, &t3958_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26399_GM};
static MethodInfo* t5079_MIs[] =
{
	&m26399_MI,
	NULL
};
static TypeInfo* t5079_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5079_0_0_0;
extern Il2CppType t5079_1_0_0;
struct t5079;
extern Il2CppGenericClass t5079_GC;
TypeInfo t5079_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5079_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5079_TI, t5079_ITIs, NULL, &EmptyCustomAttributesCache, &t5079_TI, &t5079_0_0_0, &t5079_1_0_0, NULL, &t5079_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5078_TI;



// Metadata Definition System.Collections.Generic.IList`1<LoadScene_0>
extern MethodInfo m26400_MI;
extern MethodInfo m26401_MI;
static PropertyInfo t5078____Item_PropertyInfo = 
{
	&t5078_TI, "Item", &m26400_MI, &m26401_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5078_PIs[] =
{
	&t5078____Item_PropertyInfo,
	NULL
};
extern Il2CppType t8_0_0_0;
static ParameterInfo t5078_m26402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26402_GM;
MethodInfo m26402_MI = 
{
	"IndexOf", NULL, &t5078_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5078_m26402_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26402_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t8_0_0_0;
static ParameterInfo t5078_m26403_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26403_GM;
MethodInfo m26403_MI = 
{
	"Insert", NULL, &t5078_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5078_m26403_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26403_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5078_m26404_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26404_GM;
MethodInfo m26404_MI = 
{
	"RemoveAt", NULL, &t5078_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5078_m26404_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26404_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5078_m26400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t8_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26400_GM;
MethodInfo m26400_MI = 
{
	"get_Item", NULL, &t5078_TI, &t8_0_0_0, RuntimeInvoker_t29_t44, t5078_m26400_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26400_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t8_0_0_0;
static ParameterInfo t5078_m26401_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26401_GM;
MethodInfo m26401_MI = 
{
	"set_Item", NULL, &t5078_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5078_m26401_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26401_GM};
static MethodInfo* t5078_MIs[] =
{
	&m26402_MI,
	&m26403_MI,
	&m26404_MI,
	&m26400_MI,
	&m26401_MI,
	NULL
};
static TypeInfo* t5078_ITIs[] = 
{
	&t603_TI,
	&t5077_TI,
	&t5079_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5078_0_0_0;
extern Il2CppType t5078_1_0_0;
struct t5078;
extern Il2CppGenericClass t5078_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5078_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5078_MIs, t5078_PIs, NULL, NULL, NULL, NULL, NULL, &t5078_TI, t5078_ITIs, NULL, &t1908__CustomAttributeCache, &t5078_TI, &t5078_0_0_0, &t5078_1_0_0, NULL, &t5078_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2130.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2130_TI;
#include "t2130MD.h"

#include "t2131.h"
extern TypeInfo t2131_TI;
#include "t2131MD.h"
extern MethodInfo m10374_MI;
extern MethodInfo m10376_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<LoadScene_0>
extern Il2CppType t316_0_0_33;
FieldInfo t2130_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2130_TI, offsetof(t2130, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2130_FIs[] =
{
	&t2130_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t8_0_0_0;
static ParameterInfo t2130_m10372_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10372_GM;
MethodInfo m10372_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2130_m10372_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10372_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2130_m10373_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10373_GM;
MethodInfo m10373_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2130_m10373_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10373_GM};
static MethodInfo* t2130_MIs[] =
{
	&m10372_MI,
	&m10373_MI,
	NULL
};
extern MethodInfo m10373_MI;
extern MethodInfo m10377_MI;
static MethodInfo* t2130_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10373_MI,
	&m10377_MI,
};
extern Il2CppType t2132_0_0_0;
extern TypeInfo t2132_TI;
extern MethodInfo m19613_MI;
extern TypeInfo t8_TI;
extern MethodInfo m10379_MI;
extern TypeInfo t8_TI;
static Il2CppRGCTXData t2130_RGCTXData[8] = 
{
	&t2132_0_0_0/* Type Usage */,
	&t2132_TI/* Class Usage */,
	&m19613_MI/* Method Usage */,
	&t8_TI/* Class Usage */,
	&m10379_MI/* Method Usage */,
	&m10374_MI/* Method Usage */,
	&t8_TI/* Class Usage */,
	&m10376_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2130_0_0_0;
extern Il2CppType t2130_1_0_0;
struct t2130;
extern Il2CppGenericClass t2130_GC;
TypeInfo t2130_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2130_MIs, NULL, t2130_FIs, NULL, &t2131_TI, NULL, NULL, &t2130_TI, NULL, t2130_VT, &EmptyCustomAttributesCache, &t2130_TI, &t2130_0_0_0, &t2130_1_0_0, NULL, &t2130_GC, NULL, NULL, NULL, t2130_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2130), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2132.h"
extern TypeInfo t2132_TI;
#include "t2132MD.h"
struct t556;
#define m19613(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<LoadScene_0>
extern Il2CppType t2132_0_0_1;
FieldInfo t2131_f0_FieldInfo = 
{
	"Delegate", &t2132_0_0_1, &t2131_TI, offsetof(t2131, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2131_FIs[] =
{
	&t2131_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2131_m10374_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10374_GM;
MethodInfo m10374_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2131_m10374_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10374_GM};
extern Il2CppType t2132_0_0_0;
static ParameterInfo t2131_m10375_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2132_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10375_GM;
MethodInfo m10375_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2131_m10375_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10375_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2131_m10376_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10376_GM;
MethodInfo m10376_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2131_m10376_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10376_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2131_m10377_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10377_GM;
MethodInfo m10377_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2131_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2131_m10377_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10377_GM};
static MethodInfo* t2131_MIs[] =
{
	&m10374_MI,
	&m10375_MI,
	&m10376_MI,
	&m10377_MI,
	NULL
};
static MethodInfo* t2131_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10376_MI,
	&m10377_MI,
};
extern TypeInfo t2132_TI;
extern TypeInfo t8_TI;
static Il2CppRGCTXData t2131_RGCTXData[5] = 
{
	&t2132_0_0_0/* Type Usage */,
	&t2132_TI/* Class Usage */,
	&m19613_MI/* Method Usage */,
	&t8_TI/* Class Usage */,
	&m10379_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2131_0_0_0;
extern Il2CppType t2131_1_0_0;
struct t2131;
extern Il2CppGenericClass t2131_GC;
TypeInfo t2131_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2131_MIs, NULL, t2131_FIs, NULL, &t556_TI, NULL, NULL, &t2131_TI, NULL, t2131_VT, &EmptyCustomAttributesCache, &t2131_TI, &t2131_0_0_0, &t2131_1_0_0, NULL, &t2131_GC, NULL, NULL, NULL, t2131_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2131), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<LoadScene_0>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2132_m10378_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10378_GM;
MethodInfo m10378_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2132_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2132_m10378_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10378_GM};
extern Il2CppType t8_0_0_0;
static ParameterInfo t2132_m10379_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10379_GM;
MethodInfo m10379_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2132_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2132_m10379_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10379_GM};
extern Il2CppType t8_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2132_m10380_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t8_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10380_GM;
MethodInfo m10380_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2132_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2132_m10380_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10380_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2132_m10381_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10381_GM;
MethodInfo m10381_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2132_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2132_m10381_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10381_GM};
static MethodInfo* t2132_MIs[] =
{
	&m10378_MI,
	&m10379_MI,
	&m10380_MI,
	&m10381_MI,
	NULL
};
extern MethodInfo m10380_MI;
extern MethodInfo m10381_MI;
static MethodInfo* t2132_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10379_MI,
	&m10380_MI,
	&m10381_MI,
};
static Il2CppInterfaceOffsetPair t2132_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2132_1_0_0;
struct t2132;
extern Il2CppGenericClass t2132_GC;
TypeInfo t2132_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2132_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2132_TI, NULL, t2132_VT, &EmptyCustomAttributesCache, &t2132_TI, &t2132_0_0_0, &t2132_1_0_0, t2132_IOs, &t2132_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2132), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3960_TI;

#include "t10.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<LoadScene_1>
extern MethodInfo m26405_MI;
static PropertyInfo t3960____Current_PropertyInfo = 
{
	&t3960_TI, "Current", &m26405_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3960_PIs[] =
{
	&t3960____Current_PropertyInfo,
	NULL
};
extern Il2CppType t10_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26405_GM;
MethodInfo m26405_MI = 
{
	"get_Current", NULL, &t3960_TI, &t10_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26405_GM};
static MethodInfo* t3960_MIs[] =
{
	&m26405_MI,
	NULL
};
static TypeInfo* t3960_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3960_0_0_0;
extern Il2CppType t3960_1_0_0;
struct t3960;
extern Il2CppGenericClass t3960_GC;
TypeInfo t3960_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3960_MIs, t3960_PIs, NULL, NULL, NULL, NULL, NULL, &t3960_TI, t3960_ITIs, NULL, &EmptyCustomAttributesCache, &t3960_TI, &t3960_0_0_0, &t3960_1_0_0, NULL, &t3960_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2133.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2133_TI;
#include "t2133MD.h"

extern TypeInfo t10_TI;
extern MethodInfo m10386_MI;
extern MethodInfo m19615_MI;
struct t20;
#define m19615(__this, p0, method) (t10 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<LoadScene_1>
extern Il2CppType t20_0_0_1;
FieldInfo t2133_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2133_TI, offsetof(t2133, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2133_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2133_TI, offsetof(t2133, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2133_FIs[] =
{
	&t2133_f0_FieldInfo,
	&t2133_f1_FieldInfo,
	NULL
};
extern MethodInfo m10383_MI;
static PropertyInfo t2133____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2133_TI, "System.Collections.IEnumerator.Current", &m10383_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2133____Current_PropertyInfo = 
{
	&t2133_TI, "Current", &m10386_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2133_PIs[] =
{
	&t2133____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2133____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2133_m10382_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10382_GM;
MethodInfo m10382_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2133_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2133_m10382_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10382_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10383_GM;
MethodInfo m10383_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2133_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10383_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10384_GM;
MethodInfo m10384_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2133_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10384_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10385_GM;
MethodInfo m10385_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2133_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10385_GM};
extern Il2CppType t10_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10386_GM;
MethodInfo m10386_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2133_TI, &t10_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10386_GM};
static MethodInfo* t2133_MIs[] =
{
	&m10382_MI,
	&m10383_MI,
	&m10384_MI,
	&m10385_MI,
	&m10386_MI,
	NULL
};
extern MethodInfo m10385_MI;
extern MethodInfo m10384_MI;
static MethodInfo* t2133_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10383_MI,
	&m10385_MI,
	&m10384_MI,
	&m10386_MI,
};
static TypeInfo* t2133_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3960_TI,
};
static Il2CppInterfaceOffsetPair t2133_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3960_TI, 7},
};
extern TypeInfo t10_TI;
static Il2CppRGCTXData t2133_RGCTXData[3] = 
{
	&m10386_MI/* Method Usage */,
	&t10_TI/* Class Usage */,
	&m19615_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2133_0_0_0;
extern Il2CppType t2133_1_0_0;
extern Il2CppGenericClass t2133_GC;
TypeInfo t2133_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2133_MIs, t2133_PIs, t2133_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2133_TI, t2133_ITIs, t2133_VT, &EmptyCustomAttributesCache, &t2133_TI, &t2133_0_0_0, &t2133_1_0_0, t2133_IOs, &t2133_GC, NULL, NULL, NULL, t2133_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2133)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5080_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<LoadScene_1>
extern MethodInfo m26406_MI;
static PropertyInfo t5080____Count_PropertyInfo = 
{
	&t5080_TI, "Count", &m26406_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26407_MI;
static PropertyInfo t5080____IsReadOnly_PropertyInfo = 
{
	&t5080_TI, "IsReadOnly", &m26407_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5080_PIs[] =
{
	&t5080____Count_PropertyInfo,
	&t5080____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26406_GM;
MethodInfo m26406_MI = 
{
	"get_Count", NULL, &t5080_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26406_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26407_GM;
MethodInfo m26407_MI = 
{
	"get_IsReadOnly", NULL, &t5080_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26407_GM};
extern Il2CppType t10_0_0_0;
extern Il2CppType t10_0_0_0;
static ParameterInfo t5080_m26408_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26408_GM;
MethodInfo m26408_MI = 
{
	"Add", NULL, &t5080_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5080_m26408_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26408_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26409_GM;
MethodInfo m26409_MI = 
{
	"Clear", NULL, &t5080_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26409_GM};
extern Il2CppType t10_0_0_0;
static ParameterInfo t5080_m26410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26410_GM;
MethodInfo m26410_MI = 
{
	"Contains", NULL, &t5080_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5080_m26410_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26410_GM};
extern Il2CppType t3513_0_0_0;
extern Il2CppType t3513_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5080_m26411_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3513_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26411_GM;
MethodInfo m26411_MI = 
{
	"CopyTo", NULL, &t5080_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5080_m26411_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26411_GM};
extern Il2CppType t10_0_0_0;
static ParameterInfo t5080_m26412_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26412_GM;
MethodInfo m26412_MI = 
{
	"Remove", NULL, &t5080_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5080_m26412_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26412_GM};
static MethodInfo* t5080_MIs[] =
{
	&m26406_MI,
	&m26407_MI,
	&m26408_MI,
	&m26409_MI,
	&m26410_MI,
	&m26411_MI,
	&m26412_MI,
	NULL
};
extern TypeInfo t5082_TI;
static TypeInfo* t5080_ITIs[] = 
{
	&t603_TI,
	&t5082_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5080_0_0_0;
extern Il2CppType t5080_1_0_0;
struct t5080;
extern Il2CppGenericClass t5080_GC;
TypeInfo t5080_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5080_MIs, t5080_PIs, NULL, NULL, NULL, NULL, NULL, &t5080_TI, t5080_ITIs, NULL, &EmptyCustomAttributesCache, &t5080_TI, &t5080_0_0_0, &t5080_1_0_0, NULL, &t5080_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<LoadScene_1>
extern Il2CppType t3960_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26413_GM;
MethodInfo m26413_MI = 
{
	"GetEnumerator", NULL, &t5082_TI, &t3960_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26413_GM};
static MethodInfo* t5082_MIs[] =
{
	&m26413_MI,
	NULL
};
static TypeInfo* t5082_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5082_0_0_0;
extern Il2CppType t5082_1_0_0;
struct t5082;
extern Il2CppGenericClass t5082_GC;
TypeInfo t5082_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5082_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5082_TI, t5082_ITIs, NULL, &EmptyCustomAttributesCache, &t5082_TI, &t5082_0_0_0, &t5082_1_0_0, NULL, &t5082_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5081_TI;



// Metadata Definition System.Collections.Generic.IList`1<LoadScene_1>
extern MethodInfo m26414_MI;
extern MethodInfo m26415_MI;
static PropertyInfo t5081____Item_PropertyInfo = 
{
	&t5081_TI, "Item", &m26414_MI, &m26415_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5081_PIs[] =
{
	&t5081____Item_PropertyInfo,
	NULL
};
extern Il2CppType t10_0_0_0;
static ParameterInfo t5081_m26416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26416_GM;
MethodInfo m26416_MI = 
{
	"IndexOf", NULL, &t5081_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5081_m26416_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26416_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t10_0_0_0;
static ParameterInfo t5081_m26417_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26417_GM;
MethodInfo m26417_MI = 
{
	"Insert", NULL, &t5081_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5081_m26417_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26417_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5081_m26418_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26418_GM;
MethodInfo m26418_MI = 
{
	"RemoveAt", NULL, &t5081_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5081_m26418_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26418_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5081_m26414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t10_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26414_GM;
MethodInfo m26414_MI = 
{
	"get_Item", NULL, &t5081_TI, &t10_0_0_0, RuntimeInvoker_t29_t44, t5081_m26414_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26414_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t10_0_0_0;
static ParameterInfo t5081_m26415_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26415_GM;
MethodInfo m26415_MI = 
{
	"set_Item", NULL, &t5081_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5081_m26415_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26415_GM};
static MethodInfo* t5081_MIs[] =
{
	&m26416_MI,
	&m26417_MI,
	&m26418_MI,
	&m26414_MI,
	&m26415_MI,
	NULL
};
static TypeInfo* t5081_ITIs[] = 
{
	&t603_TI,
	&t5080_TI,
	&t5082_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5081_0_0_0;
extern Il2CppType t5081_1_0_0;
struct t5081;
extern Il2CppGenericClass t5081_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5081_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5081_MIs, t5081_PIs, NULL, NULL, NULL, NULL, NULL, &t5081_TI, t5081_ITIs, NULL, &t1908__CustomAttributeCache, &t5081_TI, &t5081_0_0_0, &t5081_1_0_0, NULL, &t5081_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2134.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2134_TI;
#include "t2134MD.h"

#include "t2135.h"
extern TypeInfo t2135_TI;
#include "t2135MD.h"
extern MethodInfo m10389_MI;
extern MethodInfo m10391_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<LoadScene_1>
extern Il2CppType t316_0_0_33;
FieldInfo t2134_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2134_TI, offsetof(t2134, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2134_FIs[] =
{
	&t2134_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t10_0_0_0;
static ParameterInfo t2134_m10387_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10387_GM;
MethodInfo m10387_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2134_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2134_m10387_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10387_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2134_m10388_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10388_GM;
MethodInfo m10388_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2134_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2134_m10388_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10388_GM};
static MethodInfo* t2134_MIs[] =
{
	&m10387_MI,
	&m10388_MI,
	NULL
};
extern MethodInfo m10388_MI;
extern MethodInfo m10392_MI;
static MethodInfo* t2134_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10388_MI,
	&m10392_MI,
};
extern Il2CppType t2136_0_0_0;
extern TypeInfo t2136_TI;
extern MethodInfo m19625_MI;
extern TypeInfo t10_TI;
extern MethodInfo m10394_MI;
extern TypeInfo t10_TI;
static Il2CppRGCTXData t2134_RGCTXData[8] = 
{
	&t2136_0_0_0/* Type Usage */,
	&t2136_TI/* Class Usage */,
	&m19625_MI/* Method Usage */,
	&t10_TI/* Class Usage */,
	&m10394_MI/* Method Usage */,
	&m10389_MI/* Method Usage */,
	&t10_TI/* Class Usage */,
	&m10391_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2134_0_0_0;
extern Il2CppType t2134_1_0_0;
struct t2134;
extern Il2CppGenericClass t2134_GC;
TypeInfo t2134_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2134_MIs, NULL, t2134_FIs, NULL, &t2135_TI, NULL, NULL, &t2134_TI, NULL, t2134_VT, &EmptyCustomAttributesCache, &t2134_TI, &t2134_0_0_0, &t2134_1_0_0, NULL, &t2134_GC, NULL, NULL, NULL, t2134_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2134), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2136.h"
extern TypeInfo t2136_TI;
#include "t2136MD.h"
struct t556;
#define m19625(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<LoadScene_1>
extern Il2CppType t2136_0_0_1;
FieldInfo t2135_f0_FieldInfo = 
{
	"Delegate", &t2136_0_0_1, &t2135_TI, offsetof(t2135, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2135_FIs[] =
{
	&t2135_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2135_m10389_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10389_GM;
MethodInfo m10389_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2135_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2135_m10389_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10389_GM};
extern Il2CppType t2136_0_0_0;
static ParameterInfo t2135_m10390_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2136_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10390_GM;
MethodInfo m10390_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2135_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2135_m10390_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10390_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2135_m10391_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10391_GM;
MethodInfo m10391_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2135_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2135_m10391_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10391_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2135_m10392_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10392_GM;
MethodInfo m10392_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2135_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2135_m10392_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10392_GM};
static MethodInfo* t2135_MIs[] =
{
	&m10389_MI,
	&m10390_MI,
	&m10391_MI,
	&m10392_MI,
	NULL
};
static MethodInfo* t2135_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10391_MI,
	&m10392_MI,
};
extern TypeInfo t2136_TI;
extern TypeInfo t10_TI;
static Il2CppRGCTXData t2135_RGCTXData[5] = 
{
	&t2136_0_0_0/* Type Usage */,
	&t2136_TI/* Class Usage */,
	&m19625_MI/* Method Usage */,
	&t10_TI/* Class Usage */,
	&m10394_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2135_0_0_0;
extern Il2CppType t2135_1_0_0;
struct t2135;
extern Il2CppGenericClass t2135_GC;
TypeInfo t2135_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2135_MIs, NULL, t2135_FIs, NULL, &t556_TI, NULL, NULL, &t2135_TI, NULL, t2135_VT, &EmptyCustomAttributesCache, &t2135_TI, &t2135_0_0_0, &t2135_1_0_0, NULL, &t2135_GC, NULL, NULL, NULL, t2135_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2135), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<LoadScene_1>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2136_m10393_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10393_GM;
MethodInfo m10393_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2136_m10393_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10393_GM};
extern Il2CppType t10_0_0_0;
static ParameterInfo t2136_m10394_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10394_GM;
MethodInfo m10394_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2136_m10394_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10394_GM};
extern Il2CppType t10_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2136_m10395_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t10_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10395_GM;
MethodInfo m10395_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2136_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2136_m10395_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10395_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2136_m10396_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10396_GM;
MethodInfo m10396_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2136_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2136_m10396_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10396_GM};
static MethodInfo* t2136_MIs[] =
{
	&m10393_MI,
	&m10394_MI,
	&m10395_MI,
	&m10396_MI,
	NULL
};
extern MethodInfo m10395_MI;
extern MethodInfo m10396_MI;
static MethodInfo* t2136_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10394_MI,
	&m10395_MI,
	&m10396_MI,
};
static Il2CppInterfaceOffsetPair t2136_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2136_1_0_0;
struct t2136;
extern Il2CppGenericClass t2136_GC;
TypeInfo t2136_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2136_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2136_TI, NULL, t2136_VT, &EmptyCustomAttributesCache, &t2136_TI, &t2136_0_0_0, &t2136_1_0_0, t2136_IOs, &t2136_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2136), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3962_TI;

#include "t11.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<LoadScene_2>
extern MethodInfo m26419_MI;
static PropertyInfo t3962____Current_PropertyInfo = 
{
	&t3962_TI, "Current", &m26419_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3962_PIs[] =
{
	&t3962____Current_PropertyInfo,
	NULL
};
extern Il2CppType t11_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26419_GM;
MethodInfo m26419_MI = 
{
	"get_Current", NULL, &t3962_TI, &t11_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26419_GM};
static MethodInfo* t3962_MIs[] =
{
	&m26419_MI,
	NULL
};
static TypeInfo* t3962_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3962_0_0_0;
extern Il2CppType t3962_1_0_0;
struct t3962;
extern Il2CppGenericClass t3962_GC;
TypeInfo t3962_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3962_MIs, t3962_PIs, NULL, NULL, NULL, NULL, NULL, &t3962_TI, t3962_ITIs, NULL, &EmptyCustomAttributesCache, &t3962_TI, &t3962_0_0_0, &t3962_1_0_0, NULL, &t3962_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2137.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2137_TI;
#include "t2137MD.h"

extern TypeInfo t11_TI;
extern MethodInfo m10401_MI;
extern MethodInfo m19627_MI;
struct t20;
#define m19627(__this, p0, method) (t11 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<LoadScene_2>
extern Il2CppType t20_0_0_1;
FieldInfo t2137_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2137_TI, offsetof(t2137, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2137_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2137_TI, offsetof(t2137, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2137_FIs[] =
{
	&t2137_f0_FieldInfo,
	&t2137_f1_FieldInfo,
	NULL
};
extern MethodInfo m10398_MI;
static PropertyInfo t2137____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2137_TI, "System.Collections.IEnumerator.Current", &m10398_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2137____Current_PropertyInfo = 
{
	&t2137_TI, "Current", &m10401_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2137_PIs[] =
{
	&t2137____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2137____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2137_m10397_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10397_GM;
MethodInfo m10397_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2137_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2137_m10397_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10397_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10398_GM;
MethodInfo m10398_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2137_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10398_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10399_GM;
MethodInfo m10399_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2137_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10399_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10400_GM;
MethodInfo m10400_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2137_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10400_GM};
extern Il2CppType t11_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10401_GM;
MethodInfo m10401_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2137_TI, &t11_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10401_GM};
static MethodInfo* t2137_MIs[] =
{
	&m10397_MI,
	&m10398_MI,
	&m10399_MI,
	&m10400_MI,
	&m10401_MI,
	NULL
};
extern MethodInfo m10400_MI;
extern MethodInfo m10399_MI;
static MethodInfo* t2137_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10398_MI,
	&m10400_MI,
	&m10399_MI,
	&m10401_MI,
};
static TypeInfo* t2137_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3962_TI,
};
static Il2CppInterfaceOffsetPair t2137_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3962_TI, 7},
};
extern TypeInfo t11_TI;
static Il2CppRGCTXData t2137_RGCTXData[3] = 
{
	&m10401_MI/* Method Usage */,
	&t11_TI/* Class Usage */,
	&m19627_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2137_0_0_0;
extern Il2CppType t2137_1_0_0;
extern Il2CppGenericClass t2137_GC;
TypeInfo t2137_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2137_MIs, t2137_PIs, t2137_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2137_TI, t2137_ITIs, t2137_VT, &EmptyCustomAttributesCache, &t2137_TI, &t2137_0_0_0, &t2137_1_0_0, t2137_IOs, &t2137_GC, NULL, NULL, NULL, t2137_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2137)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5083_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<LoadScene_2>
extern MethodInfo m26420_MI;
static PropertyInfo t5083____Count_PropertyInfo = 
{
	&t5083_TI, "Count", &m26420_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26421_MI;
static PropertyInfo t5083____IsReadOnly_PropertyInfo = 
{
	&t5083_TI, "IsReadOnly", &m26421_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5083_PIs[] =
{
	&t5083____Count_PropertyInfo,
	&t5083____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26420_GM;
MethodInfo m26420_MI = 
{
	"get_Count", NULL, &t5083_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26420_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26421_GM;
MethodInfo m26421_MI = 
{
	"get_IsReadOnly", NULL, &t5083_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26421_GM};
extern Il2CppType t11_0_0_0;
extern Il2CppType t11_0_0_0;
static ParameterInfo t5083_m26422_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26422_GM;
MethodInfo m26422_MI = 
{
	"Add", NULL, &t5083_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5083_m26422_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26422_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26423_GM;
MethodInfo m26423_MI = 
{
	"Clear", NULL, &t5083_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26423_GM};
extern Il2CppType t11_0_0_0;
static ParameterInfo t5083_m26424_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26424_GM;
MethodInfo m26424_MI = 
{
	"Contains", NULL, &t5083_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5083_m26424_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26424_GM};
extern Il2CppType t3514_0_0_0;
extern Il2CppType t3514_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5083_m26425_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3514_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26425_GM;
MethodInfo m26425_MI = 
{
	"CopyTo", NULL, &t5083_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5083_m26425_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26425_GM};
extern Il2CppType t11_0_0_0;
static ParameterInfo t5083_m26426_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26426_GM;
MethodInfo m26426_MI = 
{
	"Remove", NULL, &t5083_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5083_m26426_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26426_GM};
static MethodInfo* t5083_MIs[] =
{
	&m26420_MI,
	&m26421_MI,
	&m26422_MI,
	&m26423_MI,
	&m26424_MI,
	&m26425_MI,
	&m26426_MI,
	NULL
};
extern TypeInfo t5085_TI;
static TypeInfo* t5083_ITIs[] = 
{
	&t603_TI,
	&t5085_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5083_0_0_0;
extern Il2CppType t5083_1_0_0;
struct t5083;
extern Il2CppGenericClass t5083_GC;
TypeInfo t5083_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5083_MIs, t5083_PIs, NULL, NULL, NULL, NULL, NULL, &t5083_TI, t5083_ITIs, NULL, &EmptyCustomAttributesCache, &t5083_TI, &t5083_0_0_0, &t5083_1_0_0, NULL, &t5083_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<LoadScene_2>
extern Il2CppType t3962_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26427_GM;
MethodInfo m26427_MI = 
{
	"GetEnumerator", NULL, &t5085_TI, &t3962_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26427_GM};
static MethodInfo* t5085_MIs[] =
{
	&m26427_MI,
	NULL
};
static TypeInfo* t5085_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5085_0_0_0;
extern Il2CppType t5085_1_0_0;
struct t5085;
extern Il2CppGenericClass t5085_GC;
TypeInfo t5085_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5085_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5085_TI, t5085_ITIs, NULL, &EmptyCustomAttributesCache, &t5085_TI, &t5085_0_0_0, &t5085_1_0_0, NULL, &t5085_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5084_TI;



// Metadata Definition System.Collections.Generic.IList`1<LoadScene_2>
extern MethodInfo m26428_MI;
extern MethodInfo m26429_MI;
static PropertyInfo t5084____Item_PropertyInfo = 
{
	&t5084_TI, "Item", &m26428_MI, &m26429_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5084_PIs[] =
{
	&t5084____Item_PropertyInfo,
	NULL
};
extern Il2CppType t11_0_0_0;
static ParameterInfo t5084_m26430_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26430_GM;
MethodInfo m26430_MI = 
{
	"IndexOf", NULL, &t5084_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5084_m26430_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26430_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t11_0_0_0;
static ParameterInfo t5084_m26431_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26431_GM;
MethodInfo m26431_MI = 
{
	"Insert", NULL, &t5084_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5084_m26431_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26431_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5084_m26432_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26432_GM;
MethodInfo m26432_MI = 
{
	"RemoveAt", NULL, &t5084_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5084_m26432_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26432_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5084_m26428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t11_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26428_GM;
MethodInfo m26428_MI = 
{
	"get_Item", NULL, &t5084_TI, &t11_0_0_0, RuntimeInvoker_t29_t44, t5084_m26428_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26428_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t11_0_0_0;
static ParameterInfo t5084_m26429_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26429_GM;
MethodInfo m26429_MI = 
{
	"set_Item", NULL, &t5084_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5084_m26429_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26429_GM};
static MethodInfo* t5084_MIs[] =
{
	&m26430_MI,
	&m26431_MI,
	&m26432_MI,
	&m26428_MI,
	&m26429_MI,
	NULL
};
static TypeInfo* t5084_ITIs[] = 
{
	&t603_TI,
	&t5083_TI,
	&t5085_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5084_0_0_0;
extern Il2CppType t5084_1_0_0;
struct t5084;
extern Il2CppGenericClass t5084_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5084_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5084_MIs, t5084_PIs, NULL, NULL, NULL, NULL, NULL, &t5084_TI, t5084_ITIs, NULL, &t1908__CustomAttributeCache, &t5084_TI, &t5084_0_0_0, &t5084_1_0_0, NULL, &t5084_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2138.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2138_TI;
#include "t2138MD.h"

#include "t2139.h"
extern TypeInfo t2139_TI;
#include "t2139MD.h"
extern MethodInfo m10404_MI;
extern MethodInfo m10406_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<LoadScene_2>
extern Il2CppType t316_0_0_33;
FieldInfo t2138_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2138_TI, offsetof(t2138, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2138_FIs[] =
{
	&t2138_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t11_0_0_0;
static ParameterInfo t2138_m10402_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10402_GM;
MethodInfo m10402_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2138_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2138_m10402_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10402_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2138_m10403_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10403_GM;
MethodInfo m10403_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2138_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2138_m10403_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10403_GM};
static MethodInfo* t2138_MIs[] =
{
	&m10402_MI,
	&m10403_MI,
	NULL
};
extern MethodInfo m10403_MI;
extern MethodInfo m10407_MI;
static MethodInfo* t2138_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10403_MI,
	&m10407_MI,
};
extern Il2CppType t2140_0_0_0;
extern TypeInfo t2140_TI;
extern MethodInfo m19637_MI;
extern TypeInfo t11_TI;
extern MethodInfo m10409_MI;
extern TypeInfo t11_TI;
static Il2CppRGCTXData t2138_RGCTXData[8] = 
{
	&t2140_0_0_0/* Type Usage */,
	&t2140_TI/* Class Usage */,
	&m19637_MI/* Method Usage */,
	&t11_TI/* Class Usage */,
	&m10409_MI/* Method Usage */,
	&m10404_MI/* Method Usage */,
	&t11_TI/* Class Usage */,
	&m10406_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2138_0_0_0;
extern Il2CppType t2138_1_0_0;
struct t2138;
extern Il2CppGenericClass t2138_GC;
TypeInfo t2138_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2138_MIs, NULL, t2138_FIs, NULL, &t2139_TI, NULL, NULL, &t2138_TI, NULL, t2138_VT, &EmptyCustomAttributesCache, &t2138_TI, &t2138_0_0_0, &t2138_1_0_0, NULL, &t2138_GC, NULL, NULL, NULL, t2138_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2138), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2140.h"
extern TypeInfo t2140_TI;
#include "t2140MD.h"
struct t556;
#define m19637(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<LoadScene_2>
extern Il2CppType t2140_0_0_1;
FieldInfo t2139_f0_FieldInfo = 
{
	"Delegate", &t2140_0_0_1, &t2139_TI, offsetof(t2139, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2139_FIs[] =
{
	&t2139_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2139_m10404_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10404_GM;
MethodInfo m10404_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2139_m10404_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10404_GM};
extern Il2CppType t2140_0_0_0;
static ParameterInfo t2139_m10405_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10405_GM;
MethodInfo m10405_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2139_m10405_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10405_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2139_m10406_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10406_GM;
MethodInfo m10406_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2139_m10406_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10406_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2139_m10407_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10407_GM;
MethodInfo m10407_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2139_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2139_m10407_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10407_GM};
static MethodInfo* t2139_MIs[] =
{
	&m10404_MI,
	&m10405_MI,
	&m10406_MI,
	&m10407_MI,
	NULL
};
static MethodInfo* t2139_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10406_MI,
	&m10407_MI,
};
extern TypeInfo t2140_TI;
extern TypeInfo t11_TI;
static Il2CppRGCTXData t2139_RGCTXData[5] = 
{
	&t2140_0_0_0/* Type Usage */,
	&t2140_TI/* Class Usage */,
	&m19637_MI/* Method Usage */,
	&t11_TI/* Class Usage */,
	&m10409_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2139_0_0_0;
extern Il2CppType t2139_1_0_0;
struct t2139;
extern Il2CppGenericClass t2139_GC;
TypeInfo t2139_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2139_MIs, NULL, t2139_FIs, NULL, &t556_TI, NULL, NULL, &t2139_TI, NULL, t2139_VT, &EmptyCustomAttributesCache, &t2139_TI, &t2139_0_0_0, &t2139_1_0_0, NULL, &t2139_GC, NULL, NULL, NULL, t2139_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2139), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<LoadScene_2>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2140_m10408_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10408_GM;
MethodInfo m10408_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2140_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2140_m10408_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10408_GM};
extern Il2CppType t11_0_0_0;
static ParameterInfo t2140_m10409_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10409_GM;
MethodInfo m10409_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2140_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2140_m10409_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10409_GM};
extern Il2CppType t11_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2140_m10410_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t11_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10410_GM;
MethodInfo m10410_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2140_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2140_m10410_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10410_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2140_m10411_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10411_GM;
MethodInfo m10411_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2140_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2140_m10411_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10411_GM};
static MethodInfo* t2140_MIs[] =
{
	&m10408_MI,
	&m10409_MI,
	&m10410_MI,
	&m10411_MI,
	NULL
};
extern MethodInfo m10410_MI;
extern MethodInfo m10411_MI;
static MethodInfo* t2140_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10409_MI,
	&m10410_MI,
	&m10411_MI,
};
static Il2CppInterfaceOffsetPair t2140_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2140_1_0_0;
struct t2140;
extern Il2CppGenericClass t2140_GC;
TypeInfo t2140_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2140_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2140_TI, NULL, t2140_VT, &EmptyCustomAttributesCache, &t2140_TI, &t2140_0_0_0, &t2140_1_0_0, t2140_IOs, &t2140_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2140), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3964_TI;

#include "t12.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<LoadScene_3>
extern MethodInfo m26433_MI;
static PropertyInfo t3964____Current_PropertyInfo = 
{
	&t3964_TI, "Current", &m26433_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3964_PIs[] =
{
	&t3964____Current_PropertyInfo,
	NULL
};
extern Il2CppType t12_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26433_GM;
MethodInfo m26433_MI = 
{
	"get_Current", NULL, &t3964_TI, &t12_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26433_GM};
static MethodInfo* t3964_MIs[] =
{
	&m26433_MI,
	NULL
};
static TypeInfo* t3964_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3964_0_0_0;
extern Il2CppType t3964_1_0_0;
struct t3964;
extern Il2CppGenericClass t3964_GC;
TypeInfo t3964_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3964_MIs, t3964_PIs, NULL, NULL, NULL, NULL, NULL, &t3964_TI, t3964_ITIs, NULL, &EmptyCustomAttributesCache, &t3964_TI, &t3964_0_0_0, &t3964_1_0_0, NULL, &t3964_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2141.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2141_TI;
#include "t2141MD.h"

extern TypeInfo t12_TI;
extern MethodInfo m10416_MI;
extern MethodInfo m19639_MI;
struct t20;
#define m19639(__this, p0, method) (t12 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<LoadScene_3>
extern Il2CppType t20_0_0_1;
FieldInfo t2141_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2141_TI, offsetof(t2141, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2141_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2141_TI, offsetof(t2141, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2141_FIs[] =
{
	&t2141_f0_FieldInfo,
	&t2141_f1_FieldInfo,
	NULL
};
extern MethodInfo m10413_MI;
static PropertyInfo t2141____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2141_TI, "System.Collections.IEnumerator.Current", &m10413_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2141____Current_PropertyInfo = 
{
	&t2141_TI, "Current", &m10416_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2141_PIs[] =
{
	&t2141____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2141____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2141_m10412_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10412_GM;
MethodInfo m10412_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2141_m10412_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10412_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10413_GM;
MethodInfo m10413_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2141_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10413_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10414_GM;
MethodInfo m10414_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2141_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10414_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10415_GM;
MethodInfo m10415_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2141_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10415_GM};
extern Il2CppType t12_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10416_GM;
MethodInfo m10416_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2141_TI, &t12_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10416_GM};
static MethodInfo* t2141_MIs[] =
{
	&m10412_MI,
	&m10413_MI,
	&m10414_MI,
	&m10415_MI,
	&m10416_MI,
	NULL
};
extern MethodInfo m10415_MI;
extern MethodInfo m10414_MI;
static MethodInfo* t2141_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10413_MI,
	&m10415_MI,
	&m10414_MI,
	&m10416_MI,
};
static TypeInfo* t2141_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3964_TI,
};
static Il2CppInterfaceOffsetPair t2141_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3964_TI, 7},
};
extern TypeInfo t12_TI;
static Il2CppRGCTXData t2141_RGCTXData[3] = 
{
	&m10416_MI/* Method Usage */,
	&t12_TI/* Class Usage */,
	&m19639_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2141_0_0_0;
extern Il2CppType t2141_1_0_0;
extern Il2CppGenericClass t2141_GC;
TypeInfo t2141_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2141_MIs, t2141_PIs, t2141_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2141_TI, t2141_ITIs, t2141_VT, &EmptyCustomAttributesCache, &t2141_TI, &t2141_0_0_0, &t2141_1_0_0, t2141_IOs, &t2141_GC, NULL, NULL, NULL, t2141_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2141)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5086_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<LoadScene_3>
extern MethodInfo m26434_MI;
static PropertyInfo t5086____Count_PropertyInfo = 
{
	&t5086_TI, "Count", &m26434_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26435_MI;
static PropertyInfo t5086____IsReadOnly_PropertyInfo = 
{
	&t5086_TI, "IsReadOnly", &m26435_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5086_PIs[] =
{
	&t5086____Count_PropertyInfo,
	&t5086____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26434_GM;
MethodInfo m26434_MI = 
{
	"get_Count", NULL, &t5086_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26434_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26435_GM;
MethodInfo m26435_MI = 
{
	"get_IsReadOnly", NULL, &t5086_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26435_GM};
extern Il2CppType t12_0_0_0;
extern Il2CppType t12_0_0_0;
static ParameterInfo t5086_m26436_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26436_GM;
MethodInfo m26436_MI = 
{
	"Add", NULL, &t5086_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5086_m26436_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26436_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26437_GM;
MethodInfo m26437_MI = 
{
	"Clear", NULL, &t5086_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26437_GM};
extern Il2CppType t12_0_0_0;
static ParameterInfo t5086_m26438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26438_GM;
MethodInfo m26438_MI = 
{
	"Contains", NULL, &t5086_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5086_m26438_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26438_GM};
extern Il2CppType t3515_0_0_0;
extern Il2CppType t3515_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5086_m26439_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3515_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26439_GM;
MethodInfo m26439_MI = 
{
	"CopyTo", NULL, &t5086_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5086_m26439_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26439_GM};
extern Il2CppType t12_0_0_0;
static ParameterInfo t5086_m26440_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26440_GM;
MethodInfo m26440_MI = 
{
	"Remove", NULL, &t5086_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5086_m26440_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26440_GM};
static MethodInfo* t5086_MIs[] =
{
	&m26434_MI,
	&m26435_MI,
	&m26436_MI,
	&m26437_MI,
	&m26438_MI,
	&m26439_MI,
	&m26440_MI,
	NULL
};
extern TypeInfo t5088_TI;
static TypeInfo* t5086_ITIs[] = 
{
	&t603_TI,
	&t5088_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5086_0_0_0;
extern Il2CppType t5086_1_0_0;
struct t5086;
extern Il2CppGenericClass t5086_GC;
TypeInfo t5086_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5086_MIs, t5086_PIs, NULL, NULL, NULL, NULL, NULL, &t5086_TI, t5086_ITIs, NULL, &EmptyCustomAttributesCache, &t5086_TI, &t5086_0_0_0, &t5086_1_0_0, NULL, &t5086_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<LoadScene_3>
extern Il2CppType t3964_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26441_GM;
MethodInfo m26441_MI = 
{
	"GetEnumerator", NULL, &t5088_TI, &t3964_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26441_GM};
static MethodInfo* t5088_MIs[] =
{
	&m26441_MI,
	NULL
};
static TypeInfo* t5088_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5088_0_0_0;
extern Il2CppType t5088_1_0_0;
struct t5088;
extern Il2CppGenericClass t5088_GC;
TypeInfo t5088_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5088_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5088_TI, t5088_ITIs, NULL, &EmptyCustomAttributesCache, &t5088_TI, &t5088_0_0_0, &t5088_1_0_0, NULL, &t5088_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5087_TI;



// Metadata Definition System.Collections.Generic.IList`1<LoadScene_3>
extern MethodInfo m26442_MI;
extern MethodInfo m26443_MI;
static PropertyInfo t5087____Item_PropertyInfo = 
{
	&t5087_TI, "Item", &m26442_MI, &m26443_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5087_PIs[] =
{
	&t5087____Item_PropertyInfo,
	NULL
};
extern Il2CppType t12_0_0_0;
static ParameterInfo t5087_m26444_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26444_GM;
MethodInfo m26444_MI = 
{
	"IndexOf", NULL, &t5087_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5087_m26444_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26444_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t12_0_0_0;
static ParameterInfo t5087_m26445_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26445_GM;
MethodInfo m26445_MI = 
{
	"Insert", NULL, &t5087_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5087_m26445_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26445_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5087_m26446_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26446_GM;
MethodInfo m26446_MI = 
{
	"RemoveAt", NULL, &t5087_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5087_m26446_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26446_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5087_m26442_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t12_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26442_GM;
MethodInfo m26442_MI = 
{
	"get_Item", NULL, &t5087_TI, &t12_0_0_0, RuntimeInvoker_t29_t44, t5087_m26442_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26442_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t12_0_0_0;
static ParameterInfo t5087_m26443_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26443_GM;
MethodInfo m26443_MI = 
{
	"set_Item", NULL, &t5087_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5087_m26443_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26443_GM};
static MethodInfo* t5087_MIs[] =
{
	&m26444_MI,
	&m26445_MI,
	&m26446_MI,
	&m26442_MI,
	&m26443_MI,
	NULL
};
static TypeInfo* t5087_ITIs[] = 
{
	&t603_TI,
	&t5086_TI,
	&t5088_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5087_0_0_0;
extern Il2CppType t5087_1_0_0;
struct t5087;
extern Il2CppGenericClass t5087_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5087_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5087_MIs, t5087_PIs, NULL, NULL, NULL, NULL, NULL, &t5087_TI, t5087_ITIs, NULL, &t1908__CustomAttributeCache, &t5087_TI, &t5087_0_0_0, &t5087_1_0_0, NULL, &t5087_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2142.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2142_TI;
#include "t2142MD.h"

#include "t2143.h"
extern TypeInfo t2143_TI;
#include "t2143MD.h"
extern MethodInfo m10419_MI;
extern MethodInfo m10421_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<LoadScene_3>
extern Il2CppType t316_0_0_33;
FieldInfo t2142_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2142_TI, offsetof(t2142, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2142_FIs[] =
{
	&t2142_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t12_0_0_0;
static ParameterInfo t2142_m10417_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10417_GM;
MethodInfo m10417_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2142_m10417_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10417_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2142_m10418_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10418_GM;
MethodInfo m10418_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2142_m10418_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10418_GM};
static MethodInfo* t2142_MIs[] =
{
	&m10417_MI,
	&m10418_MI,
	NULL
};
extern MethodInfo m10418_MI;
extern MethodInfo m10422_MI;
static MethodInfo* t2142_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10418_MI,
	&m10422_MI,
};
extern Il2CppType t2144_0_0_0;
extern TypeInfo t2144_TI;
extern MethodInfo m19649_MI;
extern TypeInfo t12_TI;
extern MethodInfo m10424_MI;
extern TypeInfo t12_TI;
static Il2CppRGCTXData t2142_RGCTXData[8] = 
{
	&t2144_0_0_0/* Type Usage */,
	&t2144_TI/* Class Usage */,
	&m19649_MI/* Method Usage */,
	&t12_TI/* Class Usage */,
	&m10424_MI/* Method Usage */,
	&m10419_MI/* Method Usage */,
	&t12_TI/* Class Usage */,
	&m10421_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2142_0_0_0;
extern Il2CppType t2142_1_0_0;
struct t2142;
extern Il2CppGenericClass t2142_GC;
TypeInfo t2142_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2142_MIs, NULL, t2142_FIs, NULL, &t2143_TI, NULL, NULL, &t2142_TI, NULL, t2142_VT, &EmptyCustomAttributesCache, &t2142_TI, &t2142_0_0_0, &t2142_1_0_0, NULL, &t2142_GC, NULL, NULL, NULL, t2142_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2142), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2144.h"
extern TypeInfo t2144_TI;
#include "t2144MD.h"
struct t556;
#define m19649(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<LoadScene_3>
extern Il2CppType t2144_0_0_1;
FieldInfo t2143_f0_FieldInfo = 
{
	"Delegate", &t2144_0_0_1, &t2143_TI, offsetof(t2143, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2143_FIs[] =
{
	&t2143_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2143_m10419_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10419_GM;
MethodInfo m10419_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2143_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2143_m10419_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10419_GM};
extern Il2CppType t2144_0_0_0;
static ParameterInfo t2143_m10420_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2144_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10420_GM;
MethodInfo m10420_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2143_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2143_m10420_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10420_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2143_m10421_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10421_GM;
MethodInfo m10421_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2143_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2143_m10421_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10421_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2143_m10422_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10422_GM;
MethodInfo m10422_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2143_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2143_m10422_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10422_GM};
static MethodInfo* t2143_MIs[] =
{
	&m10419_MI,
	&m10420_MI,
	&m10421_MI,
	&m10422_MI,
	NULL
};
static MethodInfo* t2143_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10421_MI,
	&m10422_MI,
};
extern TypeInfo t2144_TI;
extern TypeInfo t12_TI;
static Il2CppRGCTXData t2143_RGCTXData[5] = 
{
	&t2144_0_0_0/* Type Usage */,
	&t2144_TI/* Class Usage */,
	&m19649_MI/* Method Usage */,
	&t12_TI/* Class Usage */,
	&m10424_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2143_0_0_0;
extern Il2CppType t2143_1_0_0;
struct t2143;
extern Il2CppGenericClass t2143_GC;
TypeInfo t2143_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2143_MIs, NULL, t2143_FIs, NULL, &t556_TI, NULL, NULL, &t2143_TI, NULL, t2143_VT, &EmptyCustomAttributesCache, &t2143_TI, &t2143_0_0_0, &t2143_1_0_0, NULL, &t2143_GC, NULL, NULL, NULL, t2143_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2143), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<LoadScene_3>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2144_m10423_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10423_GM;
MethodInfo m10423_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2144_m10423_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10423_GM};
extern Il2CppType t12_0_0_0;
static ParameterInfo t2144_m10424_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10424_GM;
MethodInfo m10424_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2144_m10424_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10424_GM};
extern Il2CppType t12_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2144_m10425_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t12_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10425_GM;
MethodInfo m10425_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2144_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2144_m10425_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10425_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2144_m10426_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10426_GM;
MethodInfo m10426_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2144_m10426_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10426_GM};
static MethodInfo* t2144_MIs[] =
{
	&m10423_MI,
	&m10424_MI,
	&m10425_MI,
	&m10426_MI,
	NULL
};
extern MethodInfo m10425_MI;
extern MethodInfo m10426_MI;
static MethodInfo* t2144_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10424_MI,
	&m10425_MI,
	&m10426_MI,
};
static Il2CppInterfaceOffsetPair t2144_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2144_1_0_0;
struct t2144;
extern Il2CppGenericClass t2144_GC;
TypeInfo t2144_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2144_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2144_TI, NULL, t2144_VT, &EmptyCustomAttributesCache, &t2144_TI, &t2144_0_0_0, &t2144_1_0_0, t2144_IOs, &t2144_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2144), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3966_TI;

#include "t13.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<LoadScene_4>
extern MethodInfo m26447_MI;
static PropertyInfo t3966____Current_PropertyInfo = 
{
	&t3966_TI, "Current", &m26447_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3966_PIs[] =
{
	&t3966____Current_PropertyInfo,
	NULL
};
extern Il2CppType t13_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26447_GM;
MethodInfo m26447_MI = 
{
	"get_Current", NULL, &t3966_TI, &t13_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26447_GM};
static MethodInfo* t3966_MIs[] =
{
	&m26447_MI,
	NULL
};
static TypeInfo* t3966_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3966_0_0_0;
extern Il2CppType t3966_1_0_0;
struct t3966;
extern Il2CppGenericClass t3966_GC;
TypeInfo t3966_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3966_MIs, t3966_PIs, NULL, NULL, NULL, NULL, NULL, &t3966_TI, t3966_ITIs, NULL, &EmptyCustomAttributesCache, &t3966_TI, &t3966_0_0_0, &t3966_1_0_0, NULL, &t3966_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2145.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2145_TI;
#include "t2145MD.h"

extern TypeInfo t13_TI;
extern MethodInfo m10431_MI;
extern MethodInfo m19651_MI;
struct t20;
#define m19651(__this, p0, method) (t13 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<LoadScene_4>
extern Il2CppType t20_0_0_1;
FieldInfo t2145_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2145_TI, offsetof(t2145, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2145_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2145_TI, offsetof(t2145, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2145_FIs[] =
{
	&t2145_f0_FieldInfo,
	&t2145_f1_FieldInfo,
	NULL
};
extern MethodInfo m10428_MI;
static PropertyInfo t2145____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2145_TI, "System.Collections.IEnumerator.Current", &m10428_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2145____Current_PropertyInfo = 
{
	&t2145_TI, "Current", &m10431_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2145_PIs[] =
{
	&t2145____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2145____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2145_m10427_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10427_GM;
MethodInfo m10427_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2145_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2145_m10427_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10427_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10428_GM;
MethodInfo m10428_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2145_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10428_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10429_GM;
MethodInfo m10429_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2145_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10429_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10430_GM;
MethodInfo m10430_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2145_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10430_GM};
extern Il2CppType t13_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10431_GM;
MethodInfo m10431_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2145_TI, &t13_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10431_GM};
static MethodInfo* t2145_MIs[] =
{
	&m10427_MI,
	&m10428_MI,
	&m10429_MI,
	&m10430_MI,
	&m10431_MI,
	NULL
};
extern MethodInfo m10430_MI;
extern MethodInfo m10429_MI;
static MethodInfo* t2145_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10428_MI,
	&m10430_MI,
	&m10429_MI,
	&m10431_MI,
};
static TypeInfo* t2145_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3966_TI,
};
static Il2CppInterfaceOffsetPair t2145_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3966_TI, 7},
};
extern TypeInfo t13_TI;
static Il2CppRGCTXData t2145_RGCTXData[3] = 
{
	&m10431_MI/* Method Usage */,
	&t13_TI/* Class Usage */,
	&m19651_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2145_0_0_0;
extern Il2CppType t2145_1_0_0;
extern Il2CppGenericClass t2145_GC;
TypeInfo t2145_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2145_MIs, t2145_PIs, t2145_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2145_TI, t2145_ITIs, t2145_VT, &EmptyCustomAttributesCache, &t2145_TI, &t2145_0_0_0, &t2145_1_0_0, t2145_IOs, &t2145_GC, NULL, NULL, NULL, t2145_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2145)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5089_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<LoadScene_4>
extern MethodInfo m26448_MI;
static PropertyInfo t5089____Count_PropertyInfo = 
{
	&t5089_TI, "Count", &m26448_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26449_MI;
static PropertyInfo t5089____IsReadOnly_PropertyInfo = 
{
	&t5089_TI, "IsReadOnly", &m26449_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5089_PIs[] =
{
	&t5089____Count_PropertyInfo,
	&t5089____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26448_GM;
MethodInfo m26448_MI = 
{
	"get_Count", NULL, &t5089_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26448_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26449_GM;
MethodInfo m26449_MI = 
{
	"get_IsReadOnly", NULL, &t5089_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26449_GM};
extern Il2CppType t13_0_0_0;
extern Il2CppType t13_0_0_0;
static ParameterInfo t5089_m26450_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26450_GM;
MethodInfo m26450_MI = 
{
	"Add", NULL, &t5089_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5089_m26450_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26450_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26451_GM;
MethodInfo m26451_MI = 
{
	"Clear", NULL, &t5089_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26451_GM};
extern Il2CppType t13_0_0_0;
static ParameterInfo t5089_m26452_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26452_GM;
MethodInfo m26452_MI = 
{
	"Contains", NULL, &t5089_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5089_m26452_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26452_GM};
extern Il2CppType t3516_0_0_0;
extern Il2CppType t3516_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5089_m26453_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3516_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26453_GM;
MethodInfo m26453_MI = 
{
	"CopyTo", NULL, &t5089_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5089_m26453_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26453_GM};
extern Il2CppType t13_0_0_0;
static ParameterInfo t5089_m26454_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26454_GM;
MethodInfo m26454_MI = 
{
	"Remove", NULL, &t5089_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5089_m26454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26454_GM};
static MethodInfo* t5089_MIs[] =
{
	&m26448_MI,
	&m26449_MI,
	&m26450_MI,
	&m26451_MI,
	&m26452_MI,
	&m26453_MI,
	&m26454_MI,
	NULL
};
extern TypeInfo t5091_TI;
static TypeInfo* t5089_ITIs[] = 
{
	&t603_TI,
	&t5091_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5089_0_0_0;
extern Il2CppType t5089_1_0_0;
struct t5089;
extern Il2CppGenericClass t5089_GC;
TypeInfo t5089_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5089_MIs, t5089_PIs, NULL, NULL, NULL, NULL, NULL, &t5089_TI, t5089_ITIs, NULL, &EmptyCustomAttributesCache, &t5089_TI, &t5089_0_0_0, &t5089_1_0_0, NULL, &t5089_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<LoadScene_4>
extern Il2CppType t3966_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26455_GM;
MethodInfo m26455_MI = 
{
	"GetEnumerator", NULL, &t5091_TI, &t3966_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26455_GM};
static MethodInfo* t5091_MIs[] =
{
	&m26455_MI,
	NULL
};
static TypeInfo* t5091_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5091_0_0_0;
extern Il2CppType t5091_1_0_0;
struct t5091;
extern Il2CppGenericClass t5091_GC;
TypeInfo t5091_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5091_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5091_TI, t5091_ITIs, NULL, &EmptyCustomAttributesCache, &t5091_TI, &t5091_0_0_0, &t5091_1_0_0, NULL, &t5091_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5090_TI;



// Metadata Definition System.Collections.Generic.IList`1<LoadScene_4>
extern MethodInfo m26456_MI;
extern MethodInfo m26457_MI;
static PropertyInfo t5090____Item_PropertyInfo = 
{
	&t5090_TI, "Item", &m26456_MI, &m26457_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5090_PIs[] =
{
	&t5090____Item_PropertyInfo,
	NULL
};
extern Il2CppType t13_0_0_0;
static ParameterInfo t5090_m26458_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26458_GM;
MethodInfo m26458_MI = 
{
	"IndexOf", NULL, &t5090_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5090_m26458_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26458_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t13_0_0_0;
static ParameterInfo t5090_m26459_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26459_GM;
MethodInfo m26459_MI = 
{
	"Insert", NULL, &t5090_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5090_m26459_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26459_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5090_m26460_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26460_GM;
MethodInfo m26460_MI = 
{
	"RemoveAt", NULL, &t5090_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5090_m26460_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26460_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5090_m26456_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t13_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26456_GM;
MethodInfo m26456_MI = 
{
	"get_Item", NULL, &t5090_TI, &t13_0_0_0, RuntimeInvoker_t29_t44, t5090_m26456_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26456_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t13_0_0_0;
static ParameterInfo t5090_m26457_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26457_GM;
MethodInfo m26457_MI = 
{
	"set_Item", NULL, &t5090_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5090_m26457_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26457_GM};
static MethodInfo* t5090_MIs[] =
{
	&m26458_MI,
	&m26459_MI,
	&m26460_MI,
	&m26456_MI,
	&m26457_MI,
	NULL
};
static TypeInfo* t5090_ITIs[] = 
{
	&t603_TI,
	&t5089_TI,
	&t5091_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5090_0_0_0;
extern Il2CppType t5090_1_0_0;
struct t5090;
extern Il2CppGenericClass t5090_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5090_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5090_MIs, t5090_PIs, NULL, NULL, NULL, NULL, NULL, &t5090_TI, t5090_ITIs, NULL, &t1908__CustomAttributeCache, &t5090_TI, &t5090_0_0_0, &t5090_1_0_0, NULL, &t5090_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2146.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2146_TI;
#include "t2146MD.h"

#include "t2147.h"
extern TypeInfo t2147_TI;
#include "t2147MD.h"
extern MethodInfo m10434_MI;
extern MethodInfo m10436_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<LoadScene_4>
extern Il2CppType t316_0_0_33;
FieldInfo t2146_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2146_TI, offsetof(t2146, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2146_FIs[] =
{
	&t2146_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t13_0_0_0;
static ParameterInfo t2146_m10432_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10432_GM;
MethodInfo m10432_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2146_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2146_m10432_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10432_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2146_m10433_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10433_GM;
MethodInfo m10433_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2146_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2146_m10433_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10433_GM};
static MethodInfo* t2146_MIs[] =
{
	&m10432_MI,
	&m10433_MI,
	NULL
};
extern MethodInfo m10433_MI;
extern MethodInfo m10437_MI;
static MethodInfo* t2146_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10433_MI,
	&m10437_MI,
};
extern Il2CppType t2148_0_0_0;
extern TypeInfo t2148_TI;
extern MethodInfo m19661_MI;
extern TypeInfo t13_TI;
extern MethodInfo m10439_MI;
extern TypeInfo t13_TI;
static Il2CppRGCTXData t2146_RGCTXData[8] = 
{
	&t2148_0_0_0/* Type Usage */,
	&t2148_TI/* Class Usage */,
	&m19661_MI/* Method Usage */,
	&t13_TI/* Class Usage */,
	&m10439_MI/* Method Usage */,
	&m10434_MI/* Method Usage */,
	&t13_TI/* Class Usage */,
	&m10436_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2146_0_0_0;
extern Il2CppType t2146_1_0_0;
struct t2146;
extern Il2CppGenericClass t2146_GC;
TypeInfo t2146_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2146_MIs, NULL, t2146_FIs, NULL, &t2147_TI, NULL, NULL, &t2146_TI, NULL, t2146_VT, &EmptyCustomAttributesCache, &t2146_TI, &t2146_0_0_0, &t2146_1_0_0, NULL, &t2146_GC, NULL, NULL, NULL, t2146_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2146), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2148.h"
extern TypeInfo t2148_TI;
#include "t2148MD.h"
struct t556;
#define m19661(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<LoadScene_4>
extern Il2CppType t2148_0_0_1;
FieldInfo t2147_f0_FieldInfo = 
{
	"Delegate", &t2148_0_0_1, &t2147_TI, offsetof(t2147, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2147_FIs[] =
{
	&t2147_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2147_m10434_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10434_GM;
MethodInfo m10434_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2147_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2147_m10434_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10434_GM};
extern Il2CppType t2148_0_0_0;
static ParameterInfo t2147_m10435_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10435_GM;
MethodInfo m10435_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2147_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2147_m10435_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10435_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2147_m10436_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10436_GM;
MethodInfo m10436_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2147_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2147_m10436_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10436_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2147_m10437_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10437_GM;
MethodInfo m10437_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2147_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2147_m10437_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10437_GM};
static MethodInfo* t2147_MIs[] =
{
	&m10434_MI,
	&m10435_MI,
	&m10436_MI,
	&m10437_MI,
	NULL
};
static MethodInfo* t2147_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10436_MI,
	&m10437_MI,
};
extern TypeInfo t2148_TI;
extern TypeInfo t13_TI;
static Il2CppRGCTXData t2147_RGCTXData[5] = 
{
	&t2148_0_0_0/* Type Usage */,
	&t2148_TI/* Class Usage */,
	&m19661_MI/* Method Usage */,
	&t13_TI/* Class Usage */,
	&m10439_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2147_0_0_0;
extern Il2CppType t2147_1_0_0;
struct t2147;
extern Il2CppGenericClass t2147_GC;
TypeInfo t2147_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2147_MIs, NULL, t2147_FIs, NULL, &t556_TI, NULL, NULL, &t2147_TI, NULL, t2147_VT, &EmptyCustomAttributesCache, &t2147_TI, &t2147_0_0_0, &t2147_1_0_0, NULL, &t2147_GC, NULL, NULL, NULL, t2147_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2147), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<LoadScene_4>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2148_m10438_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10438_GM;
MethodInfo m10438_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2148_m10438_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10438_GM};
extern Il2CppType t13_0_0_0;
static ParameterInfo t2148_m10439_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10439_GM;
MethodInfo m10439_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2148_m10439_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10439_GM};
extern Il2CppType t13_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2148_m10440_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t13_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10440_GM;
MethodInfo m10440_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2148_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2148_m10440_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10440_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2148_m10441_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10441_GM;
MethodInfo m10441_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2148_m10441_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10441_GM};
static MethodInfo* t2148_MIs[] =
{
	&m10438_MI,
	&m10439_MI,
	&m10440_MI,
	&m10441_MI,
	NULL
};
extern MethodInfo m10440_MI;
extern MethodInfo m10441_MI;
static MethodInfo* t2148_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10439_MI,
	&m10440_MI,
	&m10441_MI,
};
static Il2CppInterfaceOffsetPair t2148_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2148_1_0_0;
struct t2148;
extern Il2CppGenericClass t2148_GC;
TypeInfo t2148_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2148_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2148_TI, NULL, t2148_VT, &EmptyCustomAttributesCache, &t2148_TI, &t2148_0_0_0, &t2148_1_0_0, t2148_IOs, &t2148_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2148), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3968_TI;

#include "t14.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<ModifyTransform>
extern MethodInfo m26461_MI;
static PropertyInfo t3968____Current_PropertyInfo = 
{
	&t3968_TI, "Current", &m26461_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3968_PIs[] =
{
	&t3968____Current_PropertyInfo,
	NULL
};
extern Il2CppType t14_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26461_GM;
MethodInfo m26461_MI = 
{
	"get_Current", NULL, &t3968_TI, &t14_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26461_GM};
static MethodInfo* t3968_MIs[] =
{
	&m26461_MI,
	NULL
};
static TypeInfo* t3968_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3968_0_0_0;
extern Il2CppType t3968_1_0_0;
struct t3968;
extern Il2CppGenericClass t3968_GC;
TypeInfo t3968_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3968_MIs, t3968_PIs, NULL, NULL, NULL, NULL, NULL, &t3968_TI, t3968_ITIs, NULL, &EmptyCustomAttributesCache, &t3968_TI, &t3968_0_0_0, &t3968_1_0_0, NULL, &t3968_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
