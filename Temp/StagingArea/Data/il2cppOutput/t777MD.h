﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t777;
struct t7;
struct t781;

 void m8166 (t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5152 (t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4109 (t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5149 (t777 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5281 (t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8167 (t29 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
