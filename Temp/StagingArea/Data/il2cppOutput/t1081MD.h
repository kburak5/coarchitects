﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1081;
struct t1081_marshaled;

void t1081_marshal(const t1081& unmarshaled, t1081_marshaled& marshaled);
void t1081_marshal_back(const t1081_marshaled& marshaled, t1081& unmarshaled);
void t1081_marshal_cleanup(t1081_marshaled& marshaled);
