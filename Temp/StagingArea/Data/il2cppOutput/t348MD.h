﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t348;
struct t29;
struct t42;
struct t1094;
struct t7;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 t29 * m5416 (uint8_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5417 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5418 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5419 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5420 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5421 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5422 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5423 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5424 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5425 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5426 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5427 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5428 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5429 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5430 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5431 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5432 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5433 (uint8_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5434 (uint8_t* __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5435 (uint8_t* __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5436 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5437 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5438 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5439 (t29 * __this, t7* p0, uint8_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5440 (t29 * __this, t7* p0, int32_t p1, t29 * p2, uint8_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5191 (uint8_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4083 (uint8_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5107 (uint8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5127 (uint8_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
