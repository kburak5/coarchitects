﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3044;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t380.h"

 void m16799 (t3044 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16800 (t3044 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16801 (t3044 * __this, t380  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16802 (t3044 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
