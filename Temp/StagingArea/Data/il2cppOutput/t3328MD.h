﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3328;
struct t29;
struct t20;
#include "t1272.h"

 void m18512 (t3328 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18513 (t3328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18514 (t3328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18515 (t3328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1272  m18516 (t3328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
