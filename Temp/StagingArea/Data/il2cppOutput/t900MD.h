﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t900;
struct t7;
struct t733;
#include "t735.h"

 void m3919 (t900 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3920 (t900 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3921 (t900 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3922 (t900 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
