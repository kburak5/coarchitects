﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "mscorlib_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t1203_TI;


#include "t20.h"

// Metadata Definition Mono.Math.BigInteger[]
static MethodInfo* t1203_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m4200_MI;
extern MethodInfo m5904_MI;
extern MethodInfo m5920_MI;
extern MethodInfo m5921_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5922_MI;
extern MethodInfo m5923_MI;
extern MethodInfo m5895_MI;
extern MethodInfo m5896_MI;
extern MethodInfo m5897_MI;
extern MethodInfo m5898_MI;
extern MethodInfo m5899_MI;
extern MethodInfo m5900_MI;
extern MethodInfo m5901_MI;
extern MethodInfo m5902_MI;
extern MethodInfo m5903_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m5905_MI;
extern MethodInfo m5906_MI;
extern MethodInfo m24554_MI;
extern MethodInfo m5907_MI;
extern MethodInfo m24555_MI;
extern MethodInfo m24556_MI;
extern MethodInfo m24557_MI;
extern MethodInfo m24558_MI;
extern MethodInfo m24559_MI;
extern MethodInfo m5908_MI;
extern MethodInfo m24553_MI;
extern MethodInfo m24561_MI;
extern MethodInfo m24562_MI;
extern MethodInfo m19494_MI;
extern MethodInfo m19496_MI;
extern MethodInfo m19498_MI;
extern MethodInfo m19499_MI;
extern MethodInfo m19500_MI;
extern MethodInfo m19501_MI;
extern MethodInfo m19490_MI;
extern MethodInfo m19503_MI;
extern MethodInfo m19504_MI;
static MethodInfo* t1203_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24554_MI,
	&m5907_MI,
	&m24555_MI,
	&m24556_MI,
	&m24557_MI,
	&m24558_MI,
	&m24559_MI,
	&m5908_MI,
	&m24553_MI,
	&m24561_MI,
	&m24562_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6171_TI;
extern TypeInfo t6172_TI;
extern TypeInfo t6173_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2186_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t1203_ITIs[] = 
{
	&t6171_TI,
	&t6172_TI,
	&t6173_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static Il2CppInterfaceOffsetPair t1203_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6171_TI, 21},
	{ &t6172_TI, 28},
	{ &t6173_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1203_0_0_0;
extern Il2CppType t1203_1_0_0;
extern TypeInfo t20_TI;
struct t1196;
extern TypeInfo t1196_TI;
TypeInfo t1203_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BigInteger[]", "Mono.Math", t1203_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1196_TI, t1203_ITIs, t1203_VT, &EmptyCustomAttributesCache, &t1203_TI, &t1203_0_0_0, &t1203_1_0_0, t1203_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1196 *), -1, sizeof(t1203_SFs), 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3596_TI;



// Metadata Definition Mono.Math.BigInteger/Sign[]
static MethodInfo* t3596_MIs[] =
{
	NULL
};
extern MethodInfo m24565_MI;
extern MethodInfo m24566_MI;
extern MethodInfo m24567_MI;
extern MethodInfo m24568_MI;
extern MethodInfo m24569_MI;
extern MethodInfo m24570_MI;
extern MethodInfo m24564_MI;
extern MethodInfo m24572_MI;
extern MethodInfo m24573_MI;
extern MethodInfo m19711_MI;
extern MethodInfo m19712_MI;
extern MethodInfo m19713_MI;
extern MethodInfo m19714_MI;
extern MethodInfo m19715_MI;
extern MethodInfo m19716_MI;
extern MethodInfo m19710_MI;
extern MethodInfo m19718_MI;
extern MethodInfo m19719_MI;
extern MethodInfo m19722_MI;
extern MethodInfo m19723_MI;
extern MethodInfo m19724_MI;
extern MethodInfo m19725_MI;
extern MethodInfo m19726_MI;
extern MethodInfo m19727_MI;
extern MethodInfo m19721_MI;
extern MethodInfo m19729_MI;
extern MethodInfo m19730_MI;
extern MethodInfo m19733_MI;
extern MethodInfo m19734_MI;
extern MethodInfo m19735_MI;
extern MethodInfo m19736_MI;
extern MethodInfo m19737_MI;
extern MethodInfo m19738_MI;
extern MethodInfo m19732_MI;
extern MethodInfo m19740_MI;
extern MethodInfo m19741_MI;
extern MethodInfo m19744_MI;
extern MethodInfo m19745_MI;
extern MethodInfo m19746_MI;
extern MethodInfo m19747_MI;
extern MethodInfo m19748_MI;
extern MethodInfo m19749_MI;
extern MethodInfo m19743_MI;
extern MethodInfo m19751_MI;
extern MethodInfo m19752_MI;
extern MethodInfo m19755_MI;
extern MethodInfo m19756_MI;
extern MethodInfo m19757_MI;
extern MethodInfo m19758_MI;
extern MethodInfo m19759_MI;
extern MethodInfo m19760_MI;
extern MethodInfo m19754_MI;
extern MethodInfo m19762_MI;
extern MethodInfo m19763_MI;
static MethodInfo* t3596_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24565_MI,
	&m5907_MI,
	&m24566_MI,
	&m24567_MI,
	&m24568_MI,
	&m24569_MI,
	&m24570_MI,
	&m5908_MI,
	&m24564_MI,
	&m24572_MI,
	&m24573_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6174_TI;
extern TypeInfo t6175_TI;
extern TypeInfo t6176_TI;
extern TypeInfo t5104_TI;
extern TypeInfo t5105_TI;
extern TypeInfo t5106_TI;
extern TypeInfo t5107_TI;
extern TypeInfo t5108_TI;
extern TypeInfo t5109_TI;
extern TypeInfo t5110_TI;
extern TypeInfo t5111_TI;
extern TypeInfo t5112_TI;
extern TypeInfo t5113_TI;
extern TypeInfo t5114_TI;
extern TypeInfo t5115_TI;
extern TypeInfo t5116_TI;
extern TypeInfo t5117_TI;
extern TypeInfo t5118_TI;
static TypeInfo* t3596_ITIs[] = 
{
	&t6174_TI,
	&t6175_TI,
	&t6176_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3596_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6174_TI, 21},
	{ &t6175_TI, 28},
	{ &t6176_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3596_0_0_0;
extern Il2CppType t3596_1_0_0;
#include "t1200.h"
extern TypeInfo t1200_TI;
extern TypeInfo t44_TI;
TypeInfo t3596_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Sign[]", "", t3596_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1200_TI, t3596_ITIs, t3596_VT, &EmptyCustomAttributesCache, &t44_TI, &t3596_0_0_0, &t3596_1_0_0, t3596_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1278_TI;



// Metadata Definition System.Collections.Hashtable/Slot[]
static MethodInfo* t1278_MIs[] =
{
	NULL
};
extern MethodInfo m24576_MI;
extern MethodInfo m24577_MI;
extern MethodInfo m24578_MI;
extern MethodInfo m24579_MI;
extern MethodInfo m24580_MI;
extern MethodInfo m24581_MI;
extern MethodInfo m24575_MI;
extern MethodInfo m24583_MI;
extern MethodInfo m24584_MI;
static MethodInfo* t1278_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24576_MI,
	&m5907_MI,
	&m24577_MI,
	&m24578_MI,
	&m24579_MI,
	&m24580_MI,
	&m24581_MI,
	&m5908_MI,
	&m24575_MI,
	&m24583_MI,
	&m24584_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6177_TI;
extern TypeInfo t6178_TI;
extern TypeInfo t6179_TI;
static TypeInfo* t1278_ITIs[] = 
{
	&t6177_TI,
	&t6178_TI,
	&t6179_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1278_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6177_TI, 21},
	{ &t6178_TI, 28},
	{ &t6179_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1278_0_0_0;
extern Il2CppType t1278_1_0_0;
#include "t1272.h"
extern TypeInfo t1272_TI;
TypeInfo t1278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Slot[]", "", t1278_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1272_TI, t1278_ITIs, t1278_VT, &EmptyCustomAttributesCache, &t1278_TI, &t1278_0_0_0, &t1278_1_0_0, t1278_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1272 ), -1, 0, 0, -1, 1057037, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3597_TI;



// Metadata Definition System.Collections.Hashtable/EnumeratorMode[]
static MethodInfo* t3597_MIs[] =
{
	NULL
};
extern MethodInfo m24587_MI;
extern MethodInfo m24588_MI;
extern MethodInfo m24589_MI;
extern MethodInfo m24590_MI;
extern MethodInfo m24591_MI;
extern MethodInfo m24592_MI;
extern MethodInfo m24586_MI;
extern MethodInfo m24594_MI;
extern MethodInfo m24595_MI;
static MethodInfo* t3597_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24587_MI,
	&m5907_MI,
	&m24588_MI,
	&m24589_MI,
	&m24590_MI,
	&m24591_MI,
	&m24592_MI,
	&m5908_MI,
	&m24586_MI,
	&m24594_MI,
	&m24595_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6180_TI;
extern TypeInfo t6181_TI;
extern TypeInfo t6182_TI;
static TypeInfo* t3597_ITIs[] = 
{
	&t6180_TI,
	&t6181_TI,
	&t6182_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3597_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6180_TI, 21},
	{ &t6181_TI, 28},
	{ &t6182_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3597_0_0_0;
extern Il2CppType t3597_1_0_0;
#include "t1274.h"
extern TypeInfo t1274_TI;
TypeInfo t3597_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EnumeratorMode[]", "", t3597_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1274_TI, t3597_ITIs, t3597_VT, &EmptyCustomAttributesCache, &t44_TI, &t3597_0_0_0, &t3597_1_0_0, t3597_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1282_TI;



// Metadata Definition System.Collections.SortedList/Slot[]
static MethodInfo* t1282_MIs[] =
{
	NULL
};
extern MethodInfo m24598_MI;
extern MethodInfo m24599_MI;
extern MethodInfo m24600_MI;
extern MethodInfo m24601_MI;
extern MethodInfo m24602_MI;
extern MethodInfo m24603_MI;
extern MethodInfo m24597_MI;
extern MethodInfo m24605_MI;
extern MethodInfo m24606_MI;
static MethodInfo* t1282_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24598_MI,
	&m5907_MI,
	&m24599_MI,
	&m24600_MI,
	&m24601_MI,
	&m24602_MI,
	&m24603_MI,
	&m5908_MI,
	&m24597_MI,
	&m24605_MI,
	&m24606_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6183_TI;
extern TypeInfo t6184_TI;
extern TypeInfo t6185_TI;
static TypeInfo* t1282_ITIs[] = 
{
	&t6183_TI,
	&t6184_TI,
	&t6185_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1282_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6183_TI, 21},
	{ &t6184_TI, 28},
	{ &t6185_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1282_0_0_0;
extern Il2CppType t1282_1_0_0;
#include "t1279.h"
extern TypeInfo t1279_TI;
TypeInfo t1282_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Slot[]", "", t1282_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1279_TI, t1282_ITIs, t1282_VT, &EmptyCustomAttributesCache, &t1282_TI, &t1282_0_0_0, &t1282_1_0_0, t1282_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1279 ), -1, 0, 0, -1, 1057037, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3598_TI;



// Metadata Definition System.Collections.SortedList/EnumeratorMode[]
static MethodInfo* t3598_MIs[] =
{
	NULL
};
extern MethodInfo m24609_MI;
extern MethodInfo m24610_MI;
extern MethodInfo m24611_MI;
extern MethodInfo m24612_MI;
extern MethodInfo m24613_MI;
extern MethodInfo m24614_MI;
extern MethodInfo m24608_MI;
extern MethodInfo m24616_MI;
extern MethodInfo m24617_MI;
static MethodInfo* t3598_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24609_MI,
	&m5907_MI,
	&m24610_MI,
	&m24611_MI,
	&m24612_MI,
	&m24613_MI,
	&m24614_MI,
	&m5908_MI,
	&m24608_MI,
	&m24616_MI,
	&m24617_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6186_TI;
extern TypeInfo t6187_TI;
extern TypeInfo t6188_TI;
static TypeInfo* t3598_ITIs[] = 
{
	&t6186_TI,
	&t6187_TI,
	&t6188_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3598_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6186_TI, 21},
	{ &t6187_TI, 28},
	{ &t6188_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3598_0_0_0;
extern Il2CppType t3598_1_0_0;
#include "t1280.h"
extern TypeInfo t1280_TI;
TypeInfo t3598_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EnumeratorMode[]", "", t3598_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1280_TI, t3598_ITIs, t3598_VT, &EmptyCustomAttributesCache, &t44_TI, &t3598_0_0_0, &t3598_1_0_0, t3598_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3599_TI;



// Metadata Definition System.Configuration.Assemblies.AssemblyHashAlgorithm[]
static MethodInfo* t3599_MIs[] =
{
	NULL
};
extern MethodInfo m24620_MI;
extern MethodInfo m24621_MI;
extern MethodInfo m24622_MI;
extern MethodInfo m24623_MI;
extern MethodInfo m24624_MI;
extern MethodInfo m24625_MI;
extern MethodInfo m24619_MI;
extern MethodInfo m24627_MI;
extern MethodInfo m24628_MI;
static MethodInfo* t3599_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24620_MI,
	&m5907_MI,
	&m24621_MI,
	&m24622_MI,
	&m24623_MI,
	&m24624_MI,
	&m24625_MI,
	&m5908_MI,
	&m24619_MI,
	&m24627_MI,
	&m24628_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6189_TI;
extern TypeInfo t6190_TI;
extern TypeInfo t6191_TI;
static TypeInfo* t3599_ITIs[] = 
{
	&t6189_TI,
	&t6190_TI,
	&t6191_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3599_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6189_TI, 21},
	{ &t6190_TI, 28},
	{ &t6191_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3599_0_0_0;
extern Il2CppType t3599_1_0_0;
#include "t1284.h"
extern TypeInfo t1284_TI;
extern CustomAttributesCache t1284__CustomAttributeCache;
TypeInfo t3599_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyHashAlgorithm[]", "System.Configuration.Assemblies", t3599_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1284_TI, t3599_ITIs, t3599_VT, &EmptyCustomAttributesCache, &t44_TI, &t3599_0_0_0, &t3599_1_0_0, t3599_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3600_TI;



// Metadata Definition System.Configuration.Assemblies.AssemblyVersionCompatibility[]
static MethodInfo* t3600_MIs[] =
{
	NULL
};
extern MethodInfo m24631_MI;
extern MethodInfo m24632_MI;
extern MethodInfo m24633_MI;
extern MethodInfo m24634_MI;
extern MethodInfo m24635_MI;
extern MethodInfo m24636_MI;
extern MethodInfo m24630_MI;
extern MethodInfo m24638_MI;
extern MethodInfo m24639_MI;
static MethodInfo* t3600_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24631_MI,
	&m5907_MI,
	&m24632_MI,
	&m24633_MI,
	&m24634_MI,
	&m24635_MI,
	&m24636_MI,
	&m5908_MI,
	&m24630_MI,
	&m24638_MI,
	&m24639_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6192_TI;
extern TypeInfo t6193_TI;
extern TypeInfo t6194_TI;
static TypeInfo* t3600_ITIs[] = 
{
	&t6192_TI,
	&t6193_TI,
	&t6194_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3600_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6192_TI, 21},
	{ &t6193_TI, 28},
	{ &t6194_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3600_0_0_0;
extern Il2CppType t3600_1_0_0;
#include "t1285.h"
extern TypeInfo t1285_TI;
extern CustomAttributesCache t1285__CustomAttributeCache;
TypeInfo t3600_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyVersionCompatibility[]", "System.Configuration.Assemblies", t3600_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1285_TI, t3600_ITIs, t3600_VT, &EmptyCustomAttributesCache, &t44_TI, &t3600_0_0_0, &t3600_1_0_0, t3600_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3601_TI;



// Metadata Definition System.Diagnostics.DebuggableAttribute[]
static MethodInfo* t3601_MIs[] =
{
	NULL
};
extern MethodInfo m24642_MI;
extern MethodInfo m24643_MI;
extern MethodInfo m24644_MI;
extern MethodInfo m24645_MI;
extern MethodInfo m24646_MI;
extern MethodInfo m24647_MI;
extern MethodInfo m24641_MI;
extern MethodInfo m24649_MI;
extern MethodInfo m24650_MI;
extern MethodInfo m22323_MI;
extern MethodInfo m22324_MI;
extern MethodInfo m22325_MI;
extern MethodInfo m22326_MI;
extern MethodInfo m22327_MI;
extern MethodInfo m22328_MI;
extern MethodInfo m22322_MI;
extern MethodInfo m22330_MI;
extern MethodInfo m22331_MI;
extern MethodInfo m22334_MI;
extern MethodInfo m22335_MI;
extern MethodInfo m22336_MI;
extern MethodInfo m22337_MI;
extern MethodInfo m22338_MI;
extern MethodInfo m22339_MI;
extern MethodInfo m22333_MI;
extern MethodInfo m22341_MI;
extern MethodInfo m22342_MI;
static MethodInfo* t3601_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24642_MI,
	&m5907_MI,
	&m24643_MI,
	&m24644_MI,
	&m24645_MI,
	&m24646_MI,
	&m24647_MI,
	&m5908_MI,
	&m24641_MI,
	&m24649_MI,
	&m24650_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6195_TI;
extern TypeInfo t6196_TI;
extern TypeInfo t6197_TI;
extern TypeInfo t5619_TI;
extern TypeInfo t5620_TI;
extern TypeInfo t5621_TI;
extern TypeInfo t5622_TI;
extern TypeInfo t5623_TI;
extern TypeInfo t5624_TI;
static TypeInfo* t3601_ITIs[] = 
{
	&t6195_TI,
	&t6196_TI,
	&t6197_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3601_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6195_TI, 21},
	{ &t6196_TI, 28},
	{ &t6197_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3601_0_0_0;
extern Il2CppType t3601_1_0_0;
struct t708;
extern TypeInfo t708_TI;
extern CustomAttributesCache t708__CustomAttributeCache;
TypeInfo t3601_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DebuggableAttribute[]", "System.Diagnostics", t3601_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t708_TI, t3601_ITIs, t3601_VT, &EmptyCustomAttributesCache, &t3601_TI, &t3601_0_0_0, &t3601_1_0_0, t3601_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t708 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3602_TI;



// Metadata Definition System.Diagnostics.DebuggableAttribute/DebuggingModes[]
static MethodInfo* t3602_MIs[] =
{
	NULL
};
extern MethodInfo m24653_MI;
extern MethodInfo m24654_MI;
extern MethodInfo m24655_MI;
extern MethodInfo m24656_MI;
extern MethodInfo m24657_MI;
extern MethodInfo m24658_MI;
extern MethodInfo m24652_MI;
extern MethodInfo m24660_MI;
extern MethodInfo m24661_MI;
static MethodInfo* t3602_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24653_MI,
	&m5907_MI,
	&m24654_MI,
	&m24655_MI,
	&m24656_MI,
	&m24657_MI,
	&m24658_MI,
	&m5908_MI,
	&m24652_MI,
	&m24660_MI,
	&m24661_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6198_TI;
extern TypeInfo t6199_TI;
extern TypeInfo t6200_TI;
static TypeInfo* t3602_ITIs[] = 
{
	&t6198_TI,
	&t6199_TI,
	&t6200_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3602_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6198_TI, 21},
	{ &t6199_TI, 28},
	{ &t6200_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3602_0_0_0;
extern Il2CppType t3602_1_0_0;
#include "t1286.h"
extern TypeInfo t1286_TI;
extern CustomAttributesCache t1286__CustomAttributeCache;
TypeInfo t3602_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DebuggingModes[]", "", t3602_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1286_TI, t3602_ITIs, t3602_VT, &EmptyCustomAttributesCache, &t44_TI, &t3602_0_0_0, &t3602_1_0_0, t3602_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3603_TI;



// Metadata Definition System.Diagnostics.DebuggerDisplayAttribute[]
static MethodInfo* t3603_MIs[] =
{
	NULL
};
extern MethodInfo m24664_MI;
extern MethodInfo m24665_MI;
extern MethodInfo m24666_MI;
extern MethodInfo m24667_MI;
extern MethodInfo m24668_MI;
extern MethodInfo m24669_MI;
extern MethodInfo m24663_MI;
extern MethodInfo m24671_MI;
extern MethodInfo m24672_MI;
static MethodInfo* t3603_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24664_MI,
	&m5907_MI,
	&m24665_MI,
	&m24666_MI,
	&m24667_MI,
	&m24668_MI,
	&m24669_MI,
	&m5908_MI,
	&m24663_MI,
	&m24671_MI,
	&m24672_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6201_TI;
extern TypeInfo t6202_TI;
extern TypeInfo t6203_TI;
static TypeInfo* t3603_ITIs[] = 
{
	&t6201_TI,
	&t6202_TI,
	&t6203_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3603_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6201_TI, 21},
	{ &t6202_TI, 28},
	{ &t6203_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3603_0_0_0;
extern Il2CppType t3603_1_0_0;
struct t1287;
extern TypeInfo t1287_TI;
extern CustomAttributesCache t1287__CustomAttributeCache;
TypeInfo t3603_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DebuggerDisplayAttribute[]", "System.Diagnostics", t3603_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1287_TI, t3603_ITIs, t3603_VT, &EmptyCustomAttributesCache, &t3603_TI, &t3603_0_0_0, &t3603_1_0_0, t3603_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1287 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3604_TI;



// Metadata Definition System.Diagnostics.DebuggerStepThroughAttribute[]
static MethodInfo* t3604_MIs[] =
{
	NULL
};
extern MethodInfo m24675_MI;
extern MethodInfo m24676_MI;
extern MethodInfo m24677_MI;
extern MethodInfo m24678_MI;
extern MethodInfo m24679_MI;
extern MethodInfo m24680_MI;
extern MethodInfo m24674_MI;
extern MethodInfo m24682_MI;
extern MethodInfo m24683_MI;
static MethodInfo* t3604_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24675_MI,
	&m5907_MI,
	&m24676_MI,
	&m24677_MI,
	&m24678_MI,
	&m24679_MI,
	&m24680_MI,
	&m5908_MI,
	&m24674_MI,
	&m24682_MI,
	&m24683_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6204_TI;
extern TypeInfo t6205_TI;
extern TypeInfo t6206_TI;
static TypeInfo* t3604_ITIs[] = 
{
	&t6204_TI,
	&t6205_TI,
	&t6206_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3604_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6204_TI, 21},
	{ &t6205_TI, 28},
	{ &t6206_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3604_0_0_0;
extern Il2CppType t3604_1_0_0;
struct t1288;
extern TypeInfo t1288_TI;
extern CustomAttributesCache t1288__CustomAttributeCache;
TypeInfo t3604_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DebuggerStepThroughAttribute[]", "System.Diagnostics", t3604_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1288_TI, t3604_ITIs, t3604_VT, &EmptyCustomAttributesCache, &t3604_TI, &t3604_0_0_0, &t3604_1_0_0, t3604_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1288 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3605_TI;



// Metadata Definition System.Diagnostics.DebuggerTypeProxyAttribute[]
static MethodInfo* t3605_MIs[] =
{
	NULL
};
extern MethodInfo m24686_MI;
extern MethodInfo m24687_MI;
extern MethodInfo m24688_MI;
extern MethodInfo m24689_MI;
extern MethodInfo m24690_MI;
extern MethodInfo m24691_MI;
extern MethodInfo m24685_MI;
extern MethodInfo m24693_MI;
extern MethodInfo m24694_MI;
static MethodInfo* t3605_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24686_MI,
	&m5907_MI,
	&m24687_MI,
	&m24688_MI,
	&m24689_MI,
	&m24690_MI,
	&m24691_MI,
	&m5908_MI,
	&m24685_MI,
	&m24693_MI,
	&m24694_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6207_TI;
extern TypeInfo t6208_TI;
extern TypeInfo t6209_TI;
static TypeInfo* t3605_ITIs[] = 
{
	&t6207_TI,
	&t6208_TI,
	&t6209_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3605_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6207_TI, 21},
	{ &t6208_TI, 28},
	{ &t6209_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3605_0_0_0;
extern Il2CppType t3605_1_0_0;
struct t1289;
extern TypeInfo t1289_TI;
extern CustomAttributesCache t1289__CustomAttributeCache;
TypeInfo t3605_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DebuggerTypeProxyAttribute[]", "System.Diagnostics", t3605_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1289_TI, t3605_ITIs, t3605_VT, &EmptyCustomAttributesCache, &t3605_TI, &t3605_0_0_0, &t3605_1_0_0, t3605_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1289 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1290_TI;



// Metadata Definition System.Diagnostics.StackFrame[]
static MethodInfo* t1290_MIs[] =
{
	NULL
};
extern MethodInfo m24697_MI;
extern MethodInfo m24698_MI;
extern MethodInfo m24699_MI;
extern MethodInfo m24700_MI;
extern MethodInfo m24701_MI;
extern MethodInfo m24702_MI;
extern MethodInfo m24696_MI;
extern MethodInfo m24704_MI;
extern MethodInfo m24705_MI;
static MethodInfo* t1290_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24697_MI,
	&m5907_MI,
	&m24698_MI,
	&m24699_MI,
	&m24700_MI,
	&m24701_MI,
	&m24702_MI,
	&m5908_MI,
	&m24696_MI,
	&m24704_MI,
	&m24705_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6210_TI;
extern TypeInfo t6211_TI;
extern TypeInfo t6212_TI;
static TypeInfo* t1290_ITIs[] = 
{
	&t6210_TI,
	&t6211_TI,
	&t6212_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1290_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6210_TI, 21},
	{ &t6211_TI, 28},
	{ &t6212_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1290_0_0_0;
extern Il2CppType t1290_1_0_0;
struct t635;
extern TypeInfo t635_TI;
extern CustomAttributesCache t635__CustomAttributeCache;
TypeInfo t1290_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StackFrame[]", "System.Diagnostics", t1290_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t635_TI, t1290_ITIs, t1290_VT, &EmptyCustomAttributesCache, &t1290_TI, &t1290_0_0_0, &t1290_1_0_0, t1290_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t635 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3606_TI;



// Metadata Definition System.Globalization.CompareOptions[]
static MethodInfo* t3606_MIs[] =
{
	NULL
};
extern MethodInfo m24708_MI;
extern MethodInfo m24709_MI;
extern MethodInfo m24710_MI;
extern MethodInfo m24711_MI;
extern MethodInfo m24712_MI;
extern MethodInfo m24713_MI;
extern MethodInfo m24707_MI;
extern MethodInfo m24715_MI;
extern MethodInfo m24716_MI;
static MethodInfo* t3606_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24708_MI,
	&m5907_MI,
	&m24709_MI,
	&m24710_MI,
	&m24711_MI,
	&m24712_MI,
	&m24713_MI,
	&m5908_MI,
	&m24707_MI,
	&m24715_MI,
	&m24716_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6213_TI;
extern TypeInfo t6214_TI;
extern TypeInfo t6215_TI;
static TypeInfo* t3606_ITIs[] = 
{
	&t6213_TI,
	&t6214_TI,
	&t6215_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3606_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6213_TI, 21},
	{ &t6214_TI, 28},
	{ &t6215_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3606_0_0_0;
extern Il2CppType t3606_1_0_0;
#include "t1120.h"
extern TypeInfo t1120_TI;
extern CustomAttributesCache t1120__CustomAttributeCache;
TypeInfo t3606_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CompareOptions[]", "System.Globalization", t3606_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1120_TI, t3606_ITIs, t3606_VT, &EmptyCustomAttributesCache, &t44_TI, &t3606_0_0_0, &t3606_1_0_0, t3606_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1297_TI;



// Metadata Definition System.Globalization.Calendar[]
static MethodInfo* t1297_MIs[] =
{
	NULL
};
extern MethodInfo m24719_MI;
extern MethodInfo m24720_MI;
extern MethodInfo m24721_MI;
extern MethodInfo m24722_MI;
extern MethodInfo m24723_MI;
extern MethodInfo m24724_MI;
extern MethodInfo m24718_MI;
extern MethodInfo m24726_MI;
extern MethodInfo m24727_MI;
extern MethodInfo m20434_MI;
extern MethodInfo m20435_MI;
extern MethodInfo m20436_MI;
extern MethodInfo m20437_MI;
extern MethodInfo m20438_MI;
extern MethodInfo m20439_MI;
extern MethodInfo m20433_MI;
extern MethodInfo m20441_MI;
extern MethodInfo m20442_MI;
static MethodInfo* t1297_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24719_MI,
	&m5907_MI,
	&m24720_MI,
	&m24721_MI,
	&m24722_MI,
	&m24723_MI,
	&m24724_MI,
	&m5908_MI,
	&m24718_MI,
	&m24726_MI,
	&m24727_MI,
	&m5905_MI,
	&m5906_MI,
	&m20434_MI,
	&m5907_MI,
	&m20435_MI,
	&m20436_MI,
	&m20437_MI,
	&m20438_MI,
	&m20439_MI,
	&m5908_MI,
	&m20433_MI,
	&m20441_MI,
	&m20442_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6216_TI;
extern TypeInfo t6217_TI;
extern TypeInfo t6218_TI;
extern TypeInfo t5241_TI;
extern TypeInfo t5242_TI;
extern TypeInfo t5243_TI;
static TypeInfo* t1297_ITIs[] = 
{
	&t6216_TI,
	&t6217_TI,
	&t6218_TI,
	&t5241_TI,
	&t5242_TI,
	&t5243_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1297_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6216_TI, 21},
	{ &t6217_TI, 28},
	{ &t6218_TI, 33},
	{ &t5241_TI, 34},
	{ &t5242_TI, 41},
	{ &t5243_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1297_0_0_0;
extern Il2CppType t1297_1_0_0;
struct t1291;
extern TypeInfo t1291_TI;
extern CustomAttributesCache t1291__CustomAttributeCache;
TypeInfo t1297_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Calendar[]", "System.Globalization", t1297_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1291_TI, t1297_ITIs, t1297_VT, &EmptyCustomAttributesCache, &t1297_TI, &t1297_0_0_0, &t1297_1_0_0, t1297_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1291 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3607_TI;



// Metadata Definition System.Globalization.DateTimeFormatFlags[]
static MethodInfo* t3607_MIs[] =
{
	NULL
};
extern MethodInfo m24730_MI;
extern MethodInfo m24731_MI;
extern MethodInfo m24732_MI;
extern MethodInfo m24733_MI;
extern MethodInfo m24734_MI;
extern MethodInfo m24735_MI;
extern MethodInfo m24729_MI;
extern MethodInfo m24737_MI;
extern MethodInfo m24738_MI;
static MethodInfo* t3607_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24730_MI,
	&m5907_MI,
	&m24731_MI,
	&m24732_MI,
	&m24733_MI,
	&m24734_MI,
	&m24735_MI,
	&m5908_MI,
	&m24729_MI,
	&m24737_MI,
	&m24738_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6219_TI;
extern TypeInfo t6220_TI;
extern TypeInfo t6221_TI;
static TypeInfo* t3607_ITIs[] = 
{
	&t6219_TI,
	&t6220_TI,
	&t6221_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3607_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6219_TI, 21},
	{ &t6220_TI, 28},
	{ &t6221_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3607_0_0_0;
extern Il2CppType t3607_1_0_0;
#include "t1298.h"
extern TypeInfo t1298_TI;
extern CustomAttributesCache t1298__CustomAttributeCache;
TypeInfo t3607_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DateTimeFormatFlags[]", "System.Globalization", t3607_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1298_TI, t3607_ITIs, t3607_VT, &EmptyCustomAttributesCache, &t44_TI, &t3607_0_0_0, &t3607_1_0_0, t3607_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3608_TI;



// Metadata Definition System.Globalization.DateTimeStyles[]
static MethodInfo* t3608_MIs[] =
{
	NULL
};
extern MethodInfo m24741_MI;
extern MethodInfo m24742_MI;
extern MethodInfo m24743_MI;
extern MethodInfo m24744_MI;
extern MethodInfo m24745_MI;
extern MethodInfo m24746_MI;
extern MethodInfo m24740_MI;
extern MethodInfo m24748_MI;
extern MethodInfo m24749_MI;
static MethodInfo* t3608_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24741_MI,
	&m5907_MI,
	&m24742_MI,
	&m24743_MI,
	&m24744_MI,
	&m24745_MI,
	&m24746_MI,
	&m5908_MI,
	&m24740_MI,
	&m24748_MI,
	&m24749_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6222_TI;
extern TypeInfo t6223_TI;
extern TypeInfo t6224_TI;
static TypeInfo* t3608_ITIs[] = 
{
	&t6222_TI,
	&t6223_TI,
	&t6224_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3608_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6222_TI, 21},
	{ &t6223_TI, 28},
	{ &t6224_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3608_0_0_0;
extern Il2CppType t3608_1_0_0;
#include "t1093.h"
extern TypeInfo t1093_TI;
extern CustomAttributesCache t1093__CustomAttributeCache;
TypeInfo t3608_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DateTimeStyles[]", "System.Globalization", t3608_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1093_TI, t3608_ITIs, t3608_VT, &EmptyCustomAttributesCache, &t44_TI, &t3608_0_0_0, &t3608_1_0_0, t3608_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3609_TI;



// Metadata Definition System.Globalization.GregorianCalendarTypes[]
static MethodInfo* t3609_MIs[] =
{
	NULL
};
extern MethodInfo m24752_MI;
extern MethodInfo m24753_MI;
extern MethodInfo m24754_MI;
extern MethodInfo m24755_MI;
extern MethodInfo m24756_MI;
extern MethodInfo m24757_MI;
extern MethodInfo m24751_MI;
extern MethodInfo m24759_MI;
extern MethodInfo m24760_MI;
static MethodInfo* t3609_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24752_MI,
	&m5907_MI,
	&m24753_MI,
	&m24754_MI,
	&m24755_MI,
	&m24756_MI,
	&m24757_MI,
	&m5908_MI,
	&m24751_MI,
	&m24759_MI,
	&m24760_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6225_TI;
extern TypeInfo t6226_TI;
extern TypeInfo t6227_TI;
static TypeInfo* t3609_ITIs[] = 
{
	&t6225_TI,
	&t6226_TI,
	&t6227_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3609_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6225_TI, 21},
	{ &t6226_TI, 28},
	{ &t6227_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3609_0_0_0;
extern Il2CppType t3609_1_0_0;
#include "t1301.h"
extern TypeInfo t1301_TI;
extern CustomAttributesCache t1301__CustomAttributeCache;
TypeInfo t3609_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GregorianCalendarTypes[]", "System.Globalization", t3609_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1301_TI, t3609_ITIs, t3609_VT, &EmptyCustomAttributesCache, &t44_TI, &t3609_0_0_0, &t3609_1_0_0, t3609_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3610_TI;



// Metadata Definition System.Globalization.NumberStyles[]
static MethodInfo* t3610_MIs[] =
{
	NULL
};
extern MethodInfo m24763_MI;
extern MethodInfo m24764_MI;
extern MethodInfo m24765_MI;
extern MethodInfo m24766_MI;
extern MethodInfo m24767_MI;
extern MethodInfo m24768_MI;
extern MethodInfo m24762_MI;
extern MethodInfo m24770_MI;
extern MethodInfo m24771_MI;
static MethodInfo* t3610_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24763_MI,
	&m5907_MI,
	&m24764_MI,
	&m24765_MI,
	&m24766_MI,
	&m24767_MI,
	&m24768_MI,
	&m5908_MI,
	&m24762_MI,
	&m24770_MI,
	&m24771_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6228_TI;
extern TypeInfo t6229_TI;
extern TypeInfo t6230_TI;
static TypeInfo* t3610_ITIs[] = 
{
	&t6228_TI,
	&t6229_TI,
	&t6230_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3610_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6228_TI, 21},
	{ &t6229_TI, 28},
	{ &t6230_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3610_0_0_0;
extern Il2CppType t3610_1_0_0;
#include "t923.h"
extern TypeInfo t923_TI;
extern CustomAttributesCache t923__CustomAttributeCache;
TypeInfo t3610_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NumberStyles[]", "System.Globalization", t3610_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t923_TI, t3610_ITIs, t3610_VT, &EmptyCustomAttributesCache, &t44_TI, &t3610_0_0_0, &t3610_1_0_0, t3610_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3611_TI;



// Metadata Definition System.Globalization.UnicodeCategory[]
static MethodInfo* t3611_MIs[] =
{
	NULL
};
extern MethodInfo m24774_MI;
extern MethodInfo m24775_MI;
extern MethodInfo m24776_MI;
extern MethodInfo m24777_MI;
extern MethodInfo m24778_MI;
extern MethodInfo m24779_MI;
extern MethodInfo m24773_MI;
extern MethodInfo m24781_MI;
extern MethodInfo m24782_MI;
static MethodInfo* t3611_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24774_MI,
	&m5907_MI,
	&m24775_MI,
	&m24776_MI,
	&m24777_MI,
	&m24778_MI,
	&m24779_MI,
	&m5908_MI,
	&m24773_MI,
	&m24781_MI,
	&m24782_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6231_TI;
extern TypeInfo t6232_TI;
extern TypeInfo t6233_TI;
static TypeInfo* t3611_ITIs[] = 
{
	&t6231_TI,
	&t6232_TI,
	&t6233_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3611_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6231_TI, 21},
	{ &t6232_TI, 28},
	{ &t6233_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3611_0_0_0;
extern Il2CppType t3611_1_0_0;
#include "t851.h"
extern TypeInfo t851_TI;
extern CustomAttributesCache t851__CustomAttributeCache;
TypeInfo t3611_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnicodeCategory[]", "System.Globalization", t3611_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t851_TI, t3611_ITIs, t3611_VT, &EmptyCustomAttributesCache, &t44_TI, &t3611_0_0_0, &t3611_1_0_0, t3611_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3612_TI;



// Metadata Definition System.IO.FileAttributes[]
static MethodInfo* t3612_MIs[] =
{
	NULL
};
extern MethodInfo m24785_MI;
extern MethodInfo m24786_MI;
extern MethodInfo m24787_MI;
extern MethodInfo m24788_MI;
extern MethodInfo m24789_MI;
extern MethodInfo m24790_MI;
extern MethodInfo m24784_MI;
extern MethodInfo m24792_MI;
extern MethodInfo m24793_MI;
static MethodInfo* t3612_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24785_MI,
	&m5907_MI,
	&m24786_MI,
	&m24787_MI,
	&m24788_MI,
	&m24789_MI,
	&m24790_MI,
	&m5908_MI,
	&m24784_MI,
	&m24792_MI,
	&m24793_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6234_TI;
extern TypeInfo t6235_TI;
extern TypeInfo t6236_TI;
static TypeInfo* t3612_ITIs[] = 
{
	&t6234_TI,
	&t6235_TI,
	&t6236_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3612_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6234_TI, 21},
	{ &t6235_TI, 28},
	{ &t6236_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3612_0_0_0;
extern Il2CppType t3612_1_0_0;
#include "t1306.h"
extern TypeInfo t1306_TI;
extern CustomAttributesCache t1306__CustomAttributeCache;
TypeInfo t3612_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FileAttributes[]", "System.IO", t3612_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1306_TI, t3612_ITIs, t3612_VT, &EmptyCustomAttributesCache, &t44_TI, &t3612_0_0_0, &t3612_1_0_0, t3612_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3613_TI;



// Metadata Definition System.IO.FileMode[]
static MethodInfo* t3613_MIs[] =
{
	NULL
};
extern MethodInfo m24796_MI;
extern MethodInfo m24797_MI;
extern MethodInfo m24798_MI;
extern MethodInfo m24799_MI;
extern MethodInfo m24800_MI;
extern MethodInfo m24801_MI;
extern MethodInfo m24795_MI;
extern MethodInfo m24803_MI;
extern MethodInfo m24804_MI;
static MethodInfo* t3613_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24796_MI,
	&m5907_MI,
	&m24797_MI,
	&m24798_MI,
	&m24799_MI,
	&m24800_MI,
	&m24801_MI,
	&m5908_MI,
	&m24795_MI,
	&m24803_MI,
	&m24804_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6237_TI;
extern TypeInfo t6238_TI;
extern TypeInfo t6239_TI;
static TypeInfo* t3613_ITIs[] = 
{
	&t6237_TI,
	&t6238_TI,
	&t6239_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3613_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6237_TI, 21},
	{ &t6238_TI, 28},
	{ &t6239_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3613_0_0_0;
extern Il2CppType t3613_1_0_0;
#include "t1311.h"
extern TypeInfo t1311_TI;
extern CustomAttributesCache t1311__CustomAttributeCache;
TypeInfo t3613_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FileMode[]", "System.IO", t3613_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1311_TI, t3613_ITIs, t3613_VT, &EmptyCustomAttributesCache, &t44_TI, &t3613_0_0_0, &t3613_1_0_0, t3613_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3614_TI;



// Metadata Definition System.IO.FileOptions[]
static MethodInfo* t3614_MIs[] =
{
	NULL
};
extern MethodInfo m24807_MI;
extern MethodInfo m24808_MI;
extern MethodInfo m24809_MI;
extern MethodInfo m24810_MI;
extern MethodInfo m24811_MI;
extern MethodInfo m24812_MI;
extern MethodInfo m24806_MI;
extern MethodInfo m24814_MI;
extern MethodInfo m24815_MI;
static MethodInfo* t3614_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24807_MI,
	&m5907_MI,
	&m24808_MI,
	&m24809_MI,
	&m24810_MI,
	&m24811_MI,
	&m24812_MI,
	&m5908_MI,
	&m24806_MI,
	&m24814_MI,
	&m24815_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6240_TI;
extern TypeInfo t6241_TI;
extern TypeInfo t6242_TI;
static TypeInfo* t3614_ITIs[] = 
{
	&t6240_TI,
	&t6241_TI,
	&t6242_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3614_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6240_TI, 21},
	{ &t6241_TI, 28},
	{ &t6242_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3614_0_0_0;
extern Il2CppType t3614_1_0_0;
#include "t1313.h"
extern TypeInfo t1313_TI;
extern CustomAttributesCache t1313__CustomAttributeCache;
TypeInfo t3614_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FileOptions[]", "System.IO", t3614_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1313_TI, t3614_ITIs, t3614_VT, &EmptyCustomAttributesCache, &t44_TI, &t3614_0_0_0, &t3614_1_0_0, t3614_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3615_TI;



// Metadata Definition System.IO.FileShare[]
static MethodInfo* t3615_MIs[] =
{
	NULL
};
extern MethodInfo m24818_MI;
extern MethodInfo m24819_MI;
extern MethodInfo m24820_MI;
extern MethodInfo m24821_MI;
extern MethodInfo m24822_MI;
extern MethodInfo m24823_MI;
extern MethodInfo m24817_MI;
extern MethodInfo m24825_MI;
extern MethodInfo m24826_MI;
static MethodInfo* t3615_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24818_MI,
	&m5907_MI,
	&m24819_MI,
	&m24820_MI,
	&m24821_MI,
	&m24822_MI,
	&m24823_MI,
	&m5908_MI,
	&m24817_MI,
	&m24825_MI,
	&m24826_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6243_TI;
extern TypeInfo t6244_TI;
extern TypeInfo t6245_TI;
static TypeInfo* t3615_ITIs[] = 
{
	&t6243_TI,
	&t6244_TI,
	&t6245_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3615_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6243_TI, 21},
	{ &t6244_TI, 28},
	{ &t6245_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3615_0_0_0;
extern Il2CppType t3615_1_0_0;
#include "t1314.h"
extern TypeInfo t1314_TI;
extern CustomAttributesCache t1314__CustomAttributeCache;
TypeInfo t3615_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FileShare[]", "System.IO", t3615_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1314_TI, t3615_ITIs, t3615_VT, &EmptyCustomAttributesCache, &t44_TI, &t3615_0_0_0, &t3615_1_0_0, t3615_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3616_TI;



// Metadata Definition System.IO.MonoFileType[]
static MethodInfo* t3616_MIs[] =
{
	NULL
};
extern MethodInfo m24829_MI;
extern MethodInfo m24830_MI;
extern MethodInfo m24831_MI;
extern MethodInfo m24832_MI;
extern MethodInfo m24833_MI;
extern MethodInfo m24834_MI;
extern MethodInfo m24828_MI;
extern MethodInfo m24836_MI;
extern MethodInfo m24837_MI;
static MethodInfo* t3616_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24829_MI,
	&m5907_MI,
	&m24830_MI,
	&m24831_MI,
	&m24832_MI,
	&m24833_MI,
	&m24834_MI,
	&m5908_MI,
	&m24828_MI,
	&m24836_MI,
	&m24837_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6246_TI;
extern TypeInfo t6247_TI;
extern TypeInfo t6248_TI;
static TypeInfo* t3616_ITIs[] = 
{
	&t6246_TI,
	&t6247_TI,
	&t6248_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3616_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6246_TI, 21},
	{ &t6247_TI, 28},
	{ &t6248_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3616_0_0_0;
extern Il2CppType t3616_1_0_0;
#include "t1319.h"
extern TypeInfo t1319_TI;
TypeInfo t3616_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoFileType[]", "System.IO", t3616_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1319_TI, t3616_ITIs, t3616_VT, &EmptyCustomAttributesCache, &t44_TI, &t3616_0_0_0, &t3616_1_0_0, t3616_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3617_TI;



// Metadata Definition System.IO.MonoIOError[]
static MethodInfo* t3617_MIs[] =
{
	NULL
};
extern MethodInfo m24840_MI;
extern MethodInfo m24841_MI;
extern MethodInfo m24842_MI;
extern MethodInfo m24843_MI;
extern MethodInfo m24844_MI;
extern MethodInfo m24845_MI;
extern MethodInfo m24839_MI;
extern MethodInfo m24847_MI;
extern MethodInfo m24848_MI;
static MethodInfo* t3617_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24840_MI,
	&m5907_MI,
	&m24841_MI,
	&m24842_MI,
	&m24843_MI,
	&m24844_MI,
	&m24845_MI,
	&m5908_MI,
	&m24839_MI,
	&m24847_MI,
	&m24848_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6249_TI;
extern TypeInfo t6250_TI;
extern TypeInfo t6251_TI;
static TypeInfo* t3617_ITIs[] = 
{
	&t6249_TI,
	&t6250_TI,
	&t6251_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3617_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6249_TI, 21},
	{ &t6250_TI, 28},
	{ &t6251_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3617_0_0_0;
extern Il2CppType t3617_1_0_0;
#include "t1321.h"
extern TypeInfo t1321_TI;
TypeInfo t3617_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoIOError[]", "System.IO", t3617_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1321_TI, t3617_ITIs, t3617_VT, &EmptyCustomAttributesCache, &t44_TI, &t3617_0_0_0, &t3617_1_0_0, t3617_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3618_TI;



// Metadata Definition System.IO.SeekOrigin[]
static MethodInfo* t3618_MIs[] =
{
	NULL
};
extern MethodInfo m24851_MI;
extern MethodInfo m24852_MI;
extern MethodInfo m24853_MI;
extern MethodInfo m24854_MI;
extern MethodInfo m24855_MI;
extern MethodInfo m24856_MI;
extern MethodInfo m24850_MI;
extern MethodInfo m24858_MI;
extern MethodInfo m24859_MI;
static MethodInfo* t3618_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24851_MI,
	&m5907_MI,
	&m24852_MI,
	&m24853_MI,
	&m24854_MI,
	&m24855_MI,
	&m24856_MI,
	&m5908_MI,
	&m24850_MI,
	&m24858_MI,
	&m24859_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6252_TI;
extern TypeInfo t6253_TI;
extern TypeInfo t6254_TI;
static TypeInfo* t3618_ITIs[] = 
{
	&t6252_TI,
	&t6253_TI,
	&t6254_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3618_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6252_TI, 21},
	{ &t6253_TI, 28},
	{ &t6254_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3618_0_0_0;
extern Il2CppType t3618_1_0_0;
#include "t1064.h"
extern TypeInfo t1064_TI;
extern CustomAttributesCache t1064__CustomAttributeCache;
TypeInfo t3618_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SeekOrigin[]", "System.IO", t3618_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1064_TI, t3618_ITIs, t3618_VT, &EmptyCustomAttributesCache, &t44_TI, &t3618_0_0_0, &t3618_1_0_0, t3618_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1336_TI;



// Metadata Definition System.Reflection.Emit.ModuleBuilder[]
static MethodInfo* t1336_MIs[] =
{
	NULL
};
extern MethodInfo m24862_MI;
extern MethodInfo m24863_MI;
extern MethodInfo m24864_MI;
extern MethodInfo m24865_MI;
extern MethodInfo m24866_MI;
extern MethodInfo m24867_MI;
extern MethodInfo m24861_MI;
extern MethodInfo m24869_MI;
extern MethodInfo m24870_MI;
extern MethodInfo m24873_MI;
extern MethodInfo m24874_MI;
extern MethodInfo m24875_MI;
extern MethodInfo m24876_MI;
extern MethodInfo m24877_MI;
extern MethodInfo m24878_MI;
extern MethodInfo m24872_MI;
extern MethodInfo m24880_MI;
extern MethodInfo m24881_MI;
extern MethodInfo m24884_MI;
extern MethodInfo m24885_MI;
extern MethodInfo m24886_MI;
extern MethodInfo m24887_MI;
extern MethodInfo m24888_MI;
extern MethodInfo m24889_MI;
extern MethodInfo m24883_MI;
extern MethodInfo m24891_MI;
extern MethodInfo m24892_MI;
extern MethodInfo m22439_MI;
extern MethodInfo m22440_MI;
extern MethodInfo m22441_MI;
extern MethodInfo m22442_MI;
extern MethodInfo m22443_MI;
extern MethodInfo m22444_MI;
extern MethodInfo m22438_MI;
extern MethodInfo m22446_MI;
extern MethodInfo m22447_MI;
extern MethodInfo m19836_MI;
extern MethodInfo m19837_MI;
extern MethodInfo m19838_MI;
extern MethodInfo m19839_MI;
extern MethodInfo m19840_MI;
extern MethodInfo m19841_MI;
extern MethodInfo m19835_MI;
extern MethodInfo m19843_MI;
extern MethodInfo m19844_MI;
extern MethodInfo m24895_MI;
extern MethodInfo m24896_MI;
extern MethodInfo m24897_MI;
extern MethodInfo m24898_MI;
extern MethodInfo m24899_MI;
extern MethodInfo m24900_MI;
extern MethodInfo m24894_MI;
extern MethodInfo m24902_MI;
extern MethodInfo m24903_MI;
static MethodInfo* t1336_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24862_MI,
	&m5907_MI,
	&m24863_MI,
	&m24864_MI,
	&m24865_MI,
	&m24866_MI,
	&m24867_MI,
	&m5908_MI,
	&m24861_MI,
	&m24869_MI,
	&m24870_MI,
	&m5905_MI,
	&m5906_MI,
	&m24873_MI,
	&m5907_MI,
	&m24874_MI,
	&m24875_MI,
	&m24876_MI,
	&m24877_MI,
	&m24878_MI,
	&m5908_MI,
	&m24872_MI,
	&m24880_MI,
	&m24881_MI,
	&m5905_MI,
	&m5906_MI,
	&m24884_MI,
	&m5907_MI,
	&m24885_MI,
	&m24886_MI,
	&m24887_MI,
	&m24888_MI,
	&m24889_MI,
	&m5908_MI,
	&m24883_MI,
	&m24891_MI,
	&m24892_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m24895_MI,
	&m5907_MI,
	&m24896_MI,
	&m24897_MI,
	&m24898_MI,
	&m24899_MI,
	&m24900_MI,
	&m5908_MI,
	&m24894_MI,
	&m24902_MI,
	&m24903_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6255_TI;
extern TypeInfo t6256_TI;
extern TypeInfo t6257_TI;
extern TypeInfo t6258_TI;
extern TypeInfo t6259_TI;
extern TypeInfo t6260_TI;
extern TypeInfo t6261_TI;
extern TypeInfo t6262_TI;
extern TypeInfo t6263_TI;
extern TypeInfo t5649_TI;
extern TypeInfo t5650_TI;
extern TypeInfo t5651_TI;
extern TypeInfo t5134_TI;
extern TypeInfo t5135_TI;
extern TypeInfo t5136_TI;
extern TypeInfo t6264_TI;
extern TypeInfo t6265_TI;
extern TypeInfo t6266_TI;
static TypeInfo* t1336_ITIs[] = 
{
	&t6255_TI,
	&t6256_TI,
	&t6257_TI,
	&t6258_TI,
	&t6259_TI,
	&t6260_TI,
	&t6261_TI,
	&t6262_TI,
	&t6263_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t6264_TI,
	&t6265_TI,
	&t6266_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1336_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6255_TI, 21},
	{ &t6256_TI, 28},
	{ &t6257_TI, 33},
	{ &t6258_TI, 34},
	{ &t6259_TI, 41},
	{ &t6260_TI, 46},
	{ &t6261_TI, 47},
	{ &t6262_TI, 54},
	{ &t6263_TI, 59},
	{ &t5649_TI, 60},
	{ &t5650_TI, 67},
	{ &t5651_TI, 72},
	{ &t5134_TI, 73},
	{ &t5135_TI, 80},
	{ &t5136_TI, 85},
	{ &t6264_TI, 86},
	{ &t6265_TI, 93},
	{ &t6266_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1336_0_0_0;
extern Il2CppType t1336_1_0_0;
struct t1351;
extern TypeInfo t1351_TI;
extern CustomAttributesCache t1351__CustomAttributeCache;
TypeInfo t1336_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ModuleBuilder[]", "System.Reflection.Emit", t1336_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1351_TI, t1336_ITIs, t1336_VT, &EmptyCustomAttributesCache, &t1336_TI, &t1336_0_0_0, &t1336_1_0_0, t1336_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1351 *), -1, sizeof(t1336_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3619_TI;



// Metadata Definition System.Runtime.InteropServices._ModuleBuilder[]
static MethodInfo* t3619_MIs[] =
{
	NULL
};
static MethodInfo* t3619_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24873_MI,
	&m5907_MI,
	&m24874_MI,
	&m24875_MI,
	&m24876_MI,
	&m24877_MI,
	&m24878_MI,
	&m5908_MI,
	&m24872_MI,
	&m24880_MI,
	&m24881_MI,
};
static TypeInfo* t3619_ITIs[] = 
{
	&t6258_TI,
	&t6259_TI,
	&t6260_TI,
};
static Il2CppInterfaceOffsetPair t3619_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6258_TI, 21},
	{ &t6259_TI, 28},
	{ &t6260_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3619_0_0_0;
extern Il2CppType t3619_1_0_0;
struct t2016;
extern TypeInfo t2016_TI;
extern CustomAttributesCache t2016__CustomAttributeCache;
TypeInfo t3619_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ModuleBuilder[]", "System.Runtime.InteropServices", t3619_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2016_TI, t3619_ITIs, t3619_VT, &EmptyCustomAttributesCache, &t3619_TI, &t3619_0_0_0, &t3619_1_0_0, t3619_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1337_TI;



// Metadata Definition System.Reflection.Module[]
static MethodInfo* t1337_MIs[] =
{
	NULL
};
static MethodInfo* t1337_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24884_MI,
	&m5907_MI,
	&m24885_MI,
	&m24886_MI,
	&m24887_MI,
	&m24888_MI,
	&m24889_MI,
	&m5908_MI,
	&m24883_MI,
	&m24891_MI,
	&m24892_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m24895_MI,
	&m5907_MI,
	&m24896_MI,
	&m24897_MI,
	&m24898_MI,
	&m24899_MI,
	&m24900_MI,
	&m5908_MI,
	&m24894_MI,
	&m24902_MI,
	&m24903_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t1337_ITIs[] = 
{
	&t6261_TI,
	&t6262_TI,
	&t6263_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t6264_TI,
	&t6265_TI,
	&t6266_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1337_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6261_TI, 21},
	{ &t6262_TI, 28},
	{ &t6263_TI, 33},
	{ &t5649_TI, 34},
	{ &t5650_TI, 41},
	{ &t5651_TI, 46},
	{ &t5134_TI, 47},
	{ &t5135_TI, 54},
	{ &t5136_TI, 59},
	{ &t6264_TI, 60},
	{ &t6265_TI, 67},
	{ &t6266_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1337_0_0_0;
extern Il2CppType t1337_1_0_0;
struct t1142;
extern TypeInfo t1142_TI;
extern CustomAttributesCache t1142__CustomAttributeCache;
TypeInfo t1337_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Module[]", "System.Reflection", t1337_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1142_TI, t1337_ITIs, t1337_VT, &EmptyCustomAttributesCache, &t1337_TI, &t1337_0_0_0, &t1337_1_0_0, t1337_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1142 *), -1, sizeof(t1337_SFs), 0, -1, 8193, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3620_TI;



// Metadata Definition System.Runtime.InteropServices._Module[]
static MethodInfo* t3620_MIs[] =
{
	NULL
};
static MethodInfo* t3620_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24895_MI,
	&m5907_MI,
	&m24896_MI,
	&m24897_MI,
	&m24898_MI,
	&m24899_MI,
	&m24900_MI,
	&m5908_MI,
	&m24894_MI,
	&m24902_MI,
	&m24903_MI,
};
static TypeInfo* t3620_ITIs[] = 
{
	&t6264_TI,
	&t6265_TI,
	&t6266_TI,
};
static Il2CppInterfaceOffsetPair t3620_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6264_TI, 21},
	{ &t6265_TI, 28},
	{ &t6266_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3620_0_0_0;
extern Il2CppType t3620_1_0_0;
struct t2017;
extern TypeInfo t2017_TI;
extern CustomAttributesCache t2017__CustomAttributeCache;
TypeInfo t3620_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Module[]", "System.Runtime.InteropServices", t3620_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2017_TI, t3620_ITIs, t3620_VT, &EmptyCustomAttributesCache, &t3620_TI, &t3620_0_0_0, &t3620_1_0_0, t3620_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1341_TI;



// Metadata Definition System.Reflection.Emit.ParameterBuilder[]
static MethodInfo* t1341_MIs[] =
{
	NULL
};
extern MethodInfo m24906_MI;
extern MethodInfo m24907_MI;
extern MethodInfo m24908_MI;
extern MethodInfo m24909_MI;
extern MethodInfo m24910_MI;
extern MethodInfo m24911_MI;
extern MethodInfo m24905_MI;
extern MethodInfo m24913_MI;
extern MethodInfo m24914_MI;
extern MethodInfo m24917_MI;
extern MethodInfo m24918_MI;
extern MethodInfo m24919_MI;
extern MethodInfo m24920_MI;
extern MethodInfo m24921_MI;
extern MethodInfo m24922_MI;
extern MethodInfo m24916_MI;
extern MethodInfo m24924_MI;
extern MethodInfo m24925_MI;
static MethodInfo* t1341_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24906_MI,
	&m5907_MI,
	&m24907_MI,
	&m24908_MI,
	&m24909_MI,
	&m24910_MI,
	&m24911_MI,
	&m5908_MI,
	&m24905_MI,
	&m24913_MI,
	&m24914_MI,
	&m5905_MI,
	&m5906_MI,
	&m24917_MI,
	&m5907_MI,
	&m24918_MI,
	&m24919_MI,
	&m24920_MI,
	&m24921_MI,
	&m24922_MI,
	&m5908_MI,
	&m24916_MI,
	&m24924_MI,
	&m24925_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6267_TI;
extern TypeInfo t6268_TI;
extern TypeInfo t6269_TI;
extern TypeInfo t6270_TI;
extern TypeInfo t6271_TI;
extern TypeInfo t6272_TI;
static TypeInfo* t1341_ITIs[] = 
{
	&t6267_TI,
	&t6268_TI,
	&t6269_TI,
	&t6270_TI,
	&t6271_TI,
	&t6272_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1341_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6267_TI, 21},
	{ &t6268_TI, 28},
	{ &t6269_TI, 33},
	{ &t6270_TI, 34},
	{ &t6271_TI, 41},
	{ &t6272_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1341_0_0_0;
extern Il2CppType t1341_1_0_0;
struct t1352;
extern TypeInfo t1352_TI;
extern CustomAttributesCache t1352__CustomAttributeCache;
TypeInfo t1341_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterBuilder[]", "System.Reflection.Emit", t1341_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1352_TI, t1341_ITIs, t1341_VT, &EmptyCustomAttributesCache, &t1341_TI, &t1341_0_0_0, &t1341_1_0_0, t1341_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1352 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3621_TI;



// Metadata Definition System.Runtime.InteropServices._ParameterBuilder[]
static MethodInfo* t3621_MIs[] =
{
	NULL
};
static MethodInfo* t3621_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24917_MI,
	&m5907_MI,
	&m24918_MI,
	&m24919_MI,
	&m24920_MI,
	&m24921_MI,
	&m24922_MI,
	&m5908_MI,
	&m24916_MI,
	&m24924_MI,
	&m24925_MI,
};
static TypeInfo* t3621_ITIs[] = 
{
	&t6270_TI,
	&t6271_TI,
	&t6272_TI,
};
static Il2CppInterfaceOffsetPair t3621_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6270_TI, 21},
	{ &t6271_TI, 28},
	{ &t6272_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3621_0_0_0;
extern Il2CppType t3621_1_0_0;
struct t2018;
extern TypeInfo t2018_TI;
extern CustomAttributesCache t2018__CustomAttributeCache;
TypeInfo t3621_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ParameterBuilder[]", "System.Runtime.InteropServices", t3621_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2018_TI, t3621_ITIs, t3621_VT, &EmptyCustomAttributesCache, &t3621_TI, &t3621_0_0_0, &t3621_1_0_0, t3621_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1350_TI;



// Metadata Definition System.Reflection.Emit.GenericTypeParameterBuilder[]
static MethodInfo* t1350_MIs[] =
{
	NULL
};
extern MethodInfo m24928_MI;
extern MethodInfo m24929_MI;
extern MethodInfo m24930_MI;
extern MethodInfo m24931_MI;
extern MethodInfo m24932_MI;
extern MethodInfo m24933_MI;
extern MethodInfo m24927_MI;
extern MethodInfo m24935_MI;
extern MethodInfo m24936_MI;
extern MethodInfo m19792_MI;
extern MethodInfo m19793_MI;
extern MethodInfo m19794_MI;
extern MethodInfo m19795_MI;
extern MethodInfo m19796_MI;
extern MethodInfo m19797_MI;
extern MethodInfo m19791_MI;
extern MethodInfo m19799_MI;
extern MethodInfo m19800_MI;
extern MethodInfo m19803_MI;
extern MethodInfo m19804_MI;
extern MethodInfo m19805_MI;
extern MethodInfo m19806_MI;
extern MethodInfo m19807_MI;
extern MethodInfo m19808_MI;
extern MethodInfo m19802_MI;
extern MethodInfo m19810_MI;
extern MethodInfo m19811_MI;
extern MethodInfo m19814_MI;
extern MethodInfo m19815_MI;
extern MethodInfo m19816_MI;
extern MethodInfo m19817_MI;
extern MethodInfo m19818_MI;
extern MethodInfo m19819_MI;
extern MethodInfo m19813_MI;
extern MethodInfo m19821_MI;
extern MethodInfo m19822_MI;
extern MethodInfo m19825_MI;
extern MethodInfo m19826_MI;
extern MethodInfo m19827_MI;
extern MethodInfo m19828_MI;
extern MethodInfo m19829_MI;
extern MethodInfo m19830_MI;
extern MethodInfo m19824_MI;
extern MethodInfo m19832_MI;
extern MethodInfo m19833_MI;
extern MethodInfo m19847_MI;
extern MethodInfo m19848_MI;
extern MethodInfo m19849_MI;
extern MethodInfo m19850_MI;
extern MethodInfo m19851_MI;
extern MethodInfo m19852_MI;
extern MethodInfo m19846_MI;
extern MethodInfo m19854_MI;
extern MethodInfo m19855_MI;
static MethodInfo* t1350_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24928_MI,
	&m5907_MI,
	&m24929_MI,
	&m24930_MI,
	&m24931_MI,
	&m24932_MI,
	&m24933_MI,
	&m5908_MI,
	&m24927_MI,
	&m24935_MI,
	&m24936_MI,
	&m5905_MI,
	&m5906_MI,
	&m19792_MI,
	&m5907_MI,
	&m19793_MI,
	&m19794_MI,
	&m19795_MI,
	&m19796_MI,
	&m19797_MI,
	&m5908_MI,
	&m19791_MI,
	&m19799_MI,
	&m19800_MI,
	&m5905_MI,
	&m5906_MI,
	&m19803_MI,
	&m5907_MI,
	&m19804_MI,
	&m19805_MI,
	&m19806_MI,
	&m19807_MI,
	&m19808_MI,
	&m5908_MI,
	&m19802_MI,
	&m19810_MI,
	&m19811_MI,
	&m5905_MI,
	&m5906_MI,
	&m19814_MI,
	&m5907_MI,
	&m19815_MI,
	&m19816_MI,
	&m19817_MI,
	&m19818_MI,
	&m19819_MI,
	&m5908_MI,
	&m19813_MI,
	&m19821_MI,
	&m19822_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6273_TI;
extern TypeInfo t6274_TI;
extern TypeInfo t6275_TI;
extern TypeInfo t3069_TI;
extern TypeInfo t3075_TI;
extern TypeInfo t3070_TI;
extern TypeInfo t5125_TI;
extern TypeInfo t5126_TI;
extern TypeInfo t5127_TI;
extern TypeInfo t5128_TI;
extern TypeInfo t5129_TI;
extern TypeInfo t5130_TI;
extern TypeInfo t5131_TI;
extern TypeInfo t5132_TI;
extern TypeInfo t5133_TI;
extern TypeInfo t5137_TI;
extern TypeInfo t5138_TI;
extern TypeInfo t5139_TI;
static TypeInfo* t1350_ITIs[] = 
{
	&t6273_TI,
	&t6274_TI,
	&t6275_TI,
	&t3069_TI,
	&t3075_TI,
	&t3070_TI,
	&t5125_TI,
	&t5126_TI,
	&t5127_TI,
	&t5128_TI,
	&t5129_TI,
	&t5130_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1350_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6273_TI, 21},
	{ &t6274_TI, 28},
	{ &t6275_TI, 33},
	{ &t3069_TI, 34},
	{ &t3075_TI, 41},
	{ &t3070_TI, 46},
	{ &t5125_TI, 47},
	{ &t5126_TI, 54},
	{ &t5127_TI, 59},
	{ &t5128_TI, 60},
	{ &t5129_TI, 67},
	{ &t5130_TI, 72},
	{ &t5131_TI, 73},
	{ &t5132_TI, 80},
	{ &t5133_TI, 85},
	{ &t5134_TI, 86},
	{ &t5135_TI, 93},
	{ &t5136_TI, 98},
	{ &t5137_TI, 99},
	{ &t5138_TI, 106},
	{ &t5139_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1350_0_0_0;
extern Il2CppType t1350_1_0_0;
struct t1348;
extern TypeInfo t1348_TI;
extern CustomAttributesCache t1348__CustomAttributeCache;
extern CustomAttributesCache t1348__CustomAttributeCache_m7334;
extern CustomAttributesCache t1348__CustomAttributeCache_m7337;
extern CustomAttributesCache t1348__CustomAttributeCache_m7376;
extern CustomAttributesCache t1348__CustomAttributeCache_m7377;
extern CustomAttributesCache t1348__CustomAttributeCache_m7378;
TypeInfo t1350_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericTypeParameterBuilder[]", "System.Reflection.Emit", t1350_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1348_TI, t1350_ITIs, t1350_VT, &EmptyCustomAttributesCache, &t1350_TI, &t1350_0_0_0, &t1350_1_0_0, t1350_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1348 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1354_TI;



// Metadata Definition System.Reflection.Emit.MethodBuilder[]
static MethodInfo* t1354_MIs[] =
{
	NULL
};
extern MethodInfo m24939_MI;
extern MethodInfo m24940_MI;
extern MethodInfo m24941_MI;
extern MethodInfo m24942_MI;
extern MethodInfo m24943_MI;
extern MethodInfo m24944_MI;
extern MethodInfo m24938_MI;
extern MethodInfo m24946_MI;
extern MethodInfo m24947_MI;
extern MethodInfo m24950_MI;
extern MethodInfo m24951_MI;
extern MethodInfo m24952_MI;
extern MethodInfo m24953_MI;
extern MethodInfo m24954_MI;
extern MethodInfo m24955_MI;
extern MethodInfo m24949_MI;
extern MethodInfo m24957_MI;
extern MethodInfo m24958_MI;
extern MethodInfo m24213_MI;
extern MethodInfo m24214_MI;
extern MethodInfo m24215_MI;
extern MethodInfo m24216_MI;
extern MethodInfo m24217_MI;
extern MethodInfo m24218_MI;
extern MethodInfo m24212_MI;
extern MethodInfo m24220_MI;
extern MethodInfo m24221_MI;
extern MethodInfo m24224_MI;
extern MethodInfo m24225_MI;
extern MethodInfo m24226_MI;
extern MethodInfo m24227_MI;
extern MethodInfo m24228_MI;
extern MethodInfo m24229_MI;
extern MethodInfo m24223_MI;
extern MethodInfo m24231_MI;
extern MethodInfo m24232_MI;
extern MethodInfo m24235_MI;
extern MethodInfo m24236_MI;
extern MethodInfo m24237_MI;
extern MethodInfo m24238_MI;
extern MethodInfo m24239_MI;
extern MethodInfo m24240_MI;
extern MethodInfo m24234_MI;
extern MethodInfo m24242_MI;
extern MethodInfo m24243_MI;
extern MethodInfo m24246_MI;
extern MethodInfo m24247_MI;
extern MethodInfo m24248_MI;
extern MethodInfo m24249_MI;
extern MethodInfo m24250_MI;
extern MethodInfo m24251_MI;
extern MethodInfo m24245_MI;
extern MethodInfo m24253_MI;
extern MethodInfo m24254_MI;
static MethodInfo* t1354_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24939_MI,
	&m5907_MI,
	&m24940_MI,
	&m24941_MI,
	&m24942_MI,
	&m24943_MI,
	&m24944_MI,
	&m5908_MI,
	&m24938_MI,
	&m24946_MI,
	&m24947_MI,
	&m5905_MI,
	&m5906_MI,
	&m24950_MI,
	&m5907_MI,
	&m24951_MI,
	&m24952_MI,
	&m24953_MI,
	&m24954_MI,
	&m24955_MI,
	&m5908_MI,
	&m24949_MI,
	&m24957_MI,
	&m24958_MI,
	&m5905_MI,
	&m5906_MI,
	&m24213_MI,
	&m5907_MI,
	&m24214_MI,
	&m24215_MI,
	&m24216_MI,
	&m24217_MI,
	&m24218_MI,
	&m5908_MI,
	&m24212_MI,
	&m24220_MI,
	&m24221_MI,
	&m5905_MI,
	&m5906_MI,
	&m24224_MI,
	&m5907_MI,
	&m24225_MI,
	&m24226_MI,
	&m24227_MI,
	&m24228_MI,
	&m24229_MI,
	&m5908_MI,
	&m24223_MI,
	&m24231_MI,
	&m24232_MI,
	&m5905_MI,
	&m5906_MI,
	&m24235_MI,
	&m5907_MI,
	&m24236_MI,
	&m24237_MI,
	&m24238_MI,
	&m24239_MI,
	&m24240_MI,
	&m5908_MI,
	&m24234_MI,
	&m24242_MI,
	&m24243_MI,
	&m5905_MI,
	&m5906_MI,
	&m24246_MI,
	&m5907_MI,
	&m24247_MI,
	&m24248_MI,
	&m24249_MI,
	&m24250_MI,
	&m24251_MI,
	&m5908_MI,
	&m24245_MI,
	&m24253_MI,
	&m24254_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6276_TI;
extern TypeInfo t6277_TI;
extern TypeInfo t6278_TI;
extern TypeInfo t6279_TI;
extern TypeInfo t6280_TI;
extern TypeInfo t6281_TI;
extern TypeInfo t6078_TI;
extern TypeInfo t6079_TI;
extern TypeInfo t6080_TI;
extern TypeInfo t6081_TI;
extern TypeInfo t6082_TI;
extern TypeInfo t6083_TI;
extern TypeInfo t6084_TI;
extern TypeInfo t6085_TI;
extern TypeInfo t6086_TI;
extern TypeInfo t6087_TI;
extern TypeInfo t6088_TI;
extern TypeInfo t6089_TI;
static TypeInfo* t1354_ITIs[] = 
{
	&t6276_TI,
	&t6277_TI,
	&t6278_TI,
	&t6279_TI,
	&t6280_TI,
	&t6281_TI,
	&t6078_TI,
	&t6079_TI,
	&t6080_TI,
	&t6081_TI,
	&t6082_TI,
	&t6083_TI,
	&t6084_TI,
	&t6085_TI,
	&t6086_TI,
	&t6087_TI,
	&t6088_TI,
	&t6089_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1354_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6276_TI, 21},
	{ &t6277_TI, 28},
	{ &t6278_TI, 33},
	{ &t6279_TI, 34},
	{ &t6280_TI, 41},
	{ &t6281_TI, 46},
	{ &t6078_TI, 47},
	{ &t6079_TI, 54},
	{ &t6080_TI, 59},
	{ &t6081_TI, 60},
	{ &t6082_TI, 67},
	{ &t6083_TI, 72},
	{ &t6084_TI, 73},
	{ &t6085_TI, 80},
	{ &t6086_TI, 85},
	{ &t6087_TI, 86},
	{ &t6088_TI, 93},
	{ &t6089_TI, 98},
	{ &t5131_TI, 99},
	{ &t5132_TI, 106},
	{ &t5133_TI, 111},
	{ &t5134_TI, 112},
	{ &t5135_TI, 119},
	{ &t5136_TI, 124},
	{ &t5137_TI, 125},
	{ &t5138_TI, 132},
	{ &t5139_TI, 137},
	{ &t2182_TI, 138},
	{ &t2186_TI, 145},
	{ &t2183_TI, 150},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1354_0_0_0;
extern Il2CppType t1354_1_0_0;
struct t1349;
extern TypeInfo t1349_TI;
extern CustomAttributesCache t1349__CustomAttributeCache;
extern CustomAttributesCache t1349__CustomAttributeCache_m7394;
TypeInfo t1354_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodBuilder[]", "System.Reflection.Emit", t1354_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1349_TI, t1354_ITIs, t1354_VT, &EmptyCustomAttributesCache, &t1354_TI, &t1354_0_0_0, &t1354_1_0_0, t1354_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1349 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 151, 30, 34};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3622_TI;



// Metadata Definition System.Runtime.InteropServices._MethodBuilder[]
static MethodInfo* t3622_MIs[] =
{
	NULL
};
static MethodInfo* t3622_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24950_MI,
	&m5907_MI,
	&m24951_MI,
	&m24952_MI,
	&m24953_MI,
	&m24954_MI,
	&m24955_MI,
	&m5908_MI,
	&m24949_MI,
	&m24957_MI,
	&m24958_MI,
};
static TypeInfo* t3622_ITIs[] = 
{
	&t6279_TI,
	&t6280_TI,
	&t6281_TI,
};
static Il2CppInterfaceOffsetPair t3622_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6279_TI, 21},
	{ &t6280_TI, 28},
	{ &t6281_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3622_0_0_0;
extern Il2CppType t3622_1_0_0;
struct t2014;
extern TypeInfo t2014_TI;
extern CustomAttributesCache t2014__CustomAttributeCache;
TypeInfo t3622_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MethodBuilder[]", "System.Runtime.InteropServices", t3622_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2014_TI, t3622_ITIs, t3622_VT, &EmptyCustomAttributesCache, &t3622_TI, &t3622_0_0_0, &t3622_1_0_0, t3622_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1355_TI;



// Metadata Definition System.Reflection.Emit.ConstructorBuilder[]
static MethodInfo* t1355_MIs[] =
{
	NULL
};
extern MethodInfo m24961_MI;
extern MethodInfo m24962_MI;
extern MethodInfo m24963_MI;
extern MethodInfo m24964_MI;
extern MethodInfo m24965_MI;
extern MethodInfo m24966_MI;
extern MethodInfo m24960_MI;
extern MethodInfo m24968_MI;
extern MethodInfo m24969_MI;
extern MethodInfo m24972_MI;
extern MethodInfo m24973_MI;
extern MethodInfo m24974_MI;
extern MethodInfo m24975_MI;
extern MethodInfo m24976_MI;
extern MethodInfo m24977_MI;
extern MethodInfo m24971_MI;
extern MethodInfo m24979_MI;
extern MethodInfo m24980_MI;
extern MethodInfo m24257_MI;
extern MethodInfo m24258_MI;
extern MethodInfo m24259_MI;
extern MethodInfo m24260_MI;
extern MethodInfo m24261_MI;
extern MethodInfo m24262_MI;
extern MethodInfo m24256_MI;
extern MethodInfo m24264_MI;
extern MethodInfo m24265_MI;
extern MethodInfo m24268_MI;
extern MethodInfo m24269_MI;
extern MethodInfo m24270_MI;
extern MethodInfo m24271_MI;
extern MethodInfo m24272_MI;
extern MethodInfo m24273_MI;
extern MethodInfo m24267_MI;
extern MethodInfo m24275_MI;
extern MethodInfo m24276_MI;
static MethodInfo* t1355_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24961_MI,
	&m5907_MI,
	&m24962_MI,
	&m24963_MI,
	&m24964_MI,
	&m24965_MI,
	&m24966_MI,
	&m5908_MI,
	&m24960_MI,
	&m24968_MI,
	&m24969_MI,
	&m5905_MI,
	&m5906_MI,
	&m24972_MI,
	&m5907_MI,
	&m24973_MI,
	&m24974_MI,
	&m24975_MI,
	&m24976_MI,
	&m24977_MI,
	&m5908_MI,
	&m24971_MI,
	&m24979_MI,
	&m24980_MI,
	&m5905_MI,
	&m5906_MI,
	&m24257_MI,
	&m5907_MI,
	&m24258_MI,
	&m24259_MI,
	&m24260_MI,
	&m24261_MI,
	&m24262_MI,
	&m5908_MI,
	&m24256_MI,
	&m24264_MI,
	&m24265_MI,
	&m5905_MI,
	&m5906_MI,
	&m24268_MI,
	&m5907_MI,
	&m24269_MI,
	&m24270_MI,
	&m24271_MI,
	&m24272_MI,
	&m24273_MI,
	&m5908_MI,
	&m24267_MI,
	&m24275_MI,
	&m24276_MI,
	&m5905_MI,
	&m5906_MI,
	&m24235_MI,
	&m5907_MI,
	&m24236_MI,
	&m24237_MI,
	&m24238_MI,
	&m24239_MI,
	&m24240_MI,
	&m5908_MI,
	&m24234_MI,
	&m24242_MI,
	&m24243_MI,
	&m5905_MI,
	&m5906_MI,
	&m24246_MI,
	&m5907_MI,
	&m24247_MI,
	&m24248_MI,
	&m24249_MI,
	&m24250_MI,
	&m24251_MI,
	&m5908_MI,
	&m24245_MI,
	&m24253_MI,
	&m24254_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6282_TI;
extern TypeInfo t6283_TI;
extern TypeInfo t6284_TI;
extern TypeInfo t6285_TI;
extern TypeInfo t6286_TI;
extern TypeInfo t6287_TI;
extern TypeInfo t6090_TI;
extern TypeInfo t6091_TI;
extern TypeInfo t6092_TI;
extern TypeInfo t6093_TI;
extern TypeInfo t6094_TI;
extern TypeInfo t6095_TI;
static TypeInfo* t1355_ITIs[] = 
{
	&t6282_TI,
	&t6283_TI,
	&t6284_TI,
	&t6285_TI,
	&t6286_TI,
	&t6287_TI,
	&t6090_TI,
	&t6091_TI,
	&t6092_TI,
	&t6093_TI,
	&t6094_TI,
	&t6095_TI,
	&t6084_TI,
	&t6085_TI,
	&t6086_TI,
	&t6087_TI,
	&t6088_TI,
	&t6089_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1355_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6282_TI, 21},
	{ &t6283_TI, 28},
	{ &t6284_TI, 33},
	{ &t6285_TI, 34},
	{ &t6286_TI, 41},
	{ &t6287_TI, 46},
	{ &t6090_TI, 47},
	{ &t6091_TI, 54},
	{ &t6092_TI, 59},
	{ &t6093_TI, 60},
	{ &t6094_TI, 67},
	{ &t6095_TI, 72},
	{ &t6084_TI, 73},
	{ &t6085_TI, 80},
	{ &t6086_TI, 85},
	{ &t6087_TI, 86},
	{ &t6088_TI, 93},
	{ &t6089_TI, 98},
	{ &t5131_TI, 99},
	{ &t5132_TI, 106},
	{ &t5133_TI, 111},
	{ &t5134_TI, 112},
	{ &t5135_TI, 119},
	{ &t5136_TI, 124},
	{ &t5137_TI, 125},
	{ &t5138_TI, 132},
	{ &t5139_TI, 137},
	{ &t2182_TI, 138},
	{ &t2186_TI, 145},
	{ &t2183_TI, 150},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1355_0_0_0;
extern Il2CppType t1355_1_0_0;
struct t1339;
extern TypeInfo t1339_TI;
extern CustomAttributesCache t1339__CustomAttributeCache;
extern CustomAttributesCache t1339__CustomAttributeCache_t1339____CallingConvention_PropertyInfo;
TypeInfo t1355_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConstructorBuilder[]", "System.Reflection.Emit", t1355_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1339_TI, t1355_ITIs, t1355_VT, &EmptyCustomAttributesCache, &t1355_TI, &t1355_0_0_0, &t1355_1_0_0, t1355_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1339 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 151, 30, 34};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3623_TI;



// Metadata Definition System.Runtime.InteropServices._ConstructorBuilder[]
static MethodInfo* t3623_MIs[] =
{
	NULL
};
static MethodInfo* t3623_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24972_MI,
	&m5907_MI,
	&m24973_MI,
	&m24974_MI,
	&m24975_MI,
	&m24976_MI,
	&m24977_MI,
	&m5908_MI,
	&m24971_MI,
	&m24979_MI,
	&m24980_MI,
};
static TypeInfo* t3623_ITIs[] = 
{
	&t6285_TI,
	&t6286_TI,
	&t6287_TI,
};
static Il2CppInterfaceOffsetPair t3623_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6285_TI, 21},
	{ &t6286_TI, 28},
	{ &t6287_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3623_0_0_0;
extern Il2CppType t3623_1_0_0;
struct t2008;
extern TypeInfo t2008_TI;
extern CustomAttributesCache t2008__CustomAttributeCache;
TypeInfo t3623_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ConstructorBuilder[]", "System.Runtime.InteropServices", t3623_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2008_TI, t3623_ITIs, t3623_VT, &EmptyCustomAttributesCache, &t3623_TI, &t3623_0_0_0, &t3623_1_0_0, t3623_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1356_TI;



// Metadata Definition System.Reflection.Emit.FieldBuilder[]
static MethodInfo* t1356_MIs[] =
{
	NULL
};
extern MethodInfo m24983_MI;
extern MethodInfo m24984_MI;
extern MethodInfo m24985_MI;
extern MethodInfo m24986_MI;
extern MethodInfo m24987_MI;
extern MethodInfo m24988_MI;
extern MethodInfo m24982_MI;
extern MethodInfo m24990_MI;
extern MethodInfo m24991_MI;
extern MethodInfo m24994_MI;
extern MethodInfo m24995_MI;
extern MethodInfo m24996_MI;
extern MethodInfo m24997_MI;
extern MethodInfo m24998_MI;
extern MethodInfo m24999_MI;
extern MethodInfo m24993_MI;
extern MethodInfo m25001_MI;
extern MethodInfo m25002_MI;
extern MethodInfo m25005_MI;
extern MethodInfo m25006_MI;
extern MethodInfo m25007_MI;
extern MethodInfo m25008_MI;
extern MethodInfo m25009_MI;
extern MethodInfo m25010_MI;
extern MethodInfo m25004_MI;
extern MethodInfo m25012_MI;
extern MethodInfo m25013_MI;
extern MethodInfo m25016_MI;
extern MethodInfo m25017_MI;
extern MethodInfo m25018_MI;
extern MethodInfo m25019_MI;
extern MethodInfo m25020_MI;
extern MethodInfo m25021_MI;
extern MethodInfo m25015_MI;
extern MethodInfo m25023_MI;
extern MethodInfo m25024_MI;
static MethodInfo* t1356_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24983_MI,
	&m5907_MI,
	&m24984_MI,
	&m24985_MI,
	&m24986_MI,
	&m24987_MI,
	&m24988_MI,
	&m5908_MI,
	&m24982_MI,
	&m24990_MI,
	&m24991_MI,
	&m5905_MI,
	&m5906_MI,
	&m24994_MI,
	&m5907_MI,
	&m24995_MI,
	&m24996_MI,
	&m24997_MI,
	&m24998_MI,
	&m24999_MI,
	&m5908_MI,
	&m24993_MI,
	&m25001_MI,
	&m25002_MI,
	&m5905_MI,
	&m5906_MI,
	&m25005_MI,
	&m5907_MI,
	&m25006_MI,
	&m25007_MI,
	&m25008_MI,
	&m25009_MI,
	&m25010_MI,
	&m5908_MI,
	&m25004_MI,
	&m25012_MI,
	&m25013_MI,
	&m5905_MI,
	&m5906_MI,
	&m25016_MI,
	&m5907_MI,
	&m25017_MI,
	&m25018_MI,
	&m25019_MI,
	&m25020_MI,
	&m25021_MI,
	&m5908_MI,
	&m25015_MI,
	&m25023_MI,
	&m25024_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6288_TI;
extern TypeInfo t6289_TI;
extern TypeInfo t6290_TI;
extern TypeInfo t6291_TI;
extern TypeInfo t6292_TI;
extern TypeInfo t6293_TI;
extern TypeInfo t6294_TI;
extern TypeInfo t6295_TI;
extern TypeInfo t6296_TI;
extern TypeInfo t6297_TI;
extern TypeInfo t6298_TI;
extern TypeInfo t6299_TI;
static TypeInfo* t1356_ITIs[] = 
{
	&t6288_TI,
	&t6289_TI,
	&t6290_TI,
	&t6291_TI,
	&t6292_TI,
	&t6293_TI,
	&t6294_TI,
	&t6295_TI,
	&t6296_TI,
	&t6297_TI,
	&t6298_TI,
	&t6299_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1356_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6288_TI, 21},
	{ &t6289_TI, 28},
	{ &t6290_TI, 33},
	{ &t6291_TI, 34},
	{ &t6292_TI, 41},
	{ &t6293_TI, 46},
	{ &t6294_TI, 47},
	{ &t6295_TI, 54},
	{ &t6296_TI, 59},
	{ &t6297_TI, 60},
	{ &t6298_TI, 67},
	{ &t6299_TI, 72},
	{ &t5131_TI, 73},
	{ &t5132_TI, 80},
	{ &t5133_TI, 85},
	{ &t5134_TI, 86},
	{ &t5135_TI, 93},
	{ &t5136_TI, 98},
	{ &t5137_TI, 99},
	{ &t5138_TI, 106},
	{ &t5139_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1356_0_0_0;
extern Il2CppType t1356_1_0_0;
struct t1345;
extern TypeInfo t1345_TI;
extern CustomAttributesCache t1345__CustomAttributeCache;
TypeInfo t1356_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FieldBuilder[]", "System.Reflection.Emit", t1356_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1345_TI, t1356_ITIs, t1356_VT, &EmptyCustomAttributesCache, &t1356_TI, &t1356_0_0_0, &t1356_1_0_0, t1356_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1345 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3624_TI;



// Metadata Definition System.Runtime.InteropServices._FieldBuilder[]
static MethodInfo* t3624_MIs[] =
{
	NULL
};
static MethodInfo* t3624_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24994_MI,
	&m5907_MI,
	&m24995_MI,
	&m24996_MI,
	&m24997_MI,
	&m24998_MI,
	&m24999_MI,
	&m5908_MI,
	&m24993_MI,
	&m25001_MI,
	&m25002_MI,
};
static TypeInfo* t3624_ITIs[] = 
{
	&t6291_TI,
	&t6292_TI,
	&t6293_TI,
};
static Il2CppInterfaceOffsetPair t3624_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6291_TI, 21},
	{ &t6292_TI, 28},
	{ &t6293_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3624_0_0_0;
extern Il2CppType t3624_1_0_0;
struct t2012;
extern TypeInfo t2012_TI;
extern CustomAttributesCache t2012__CustomAttributeCache;
TypeInfo t3624_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_FieldBuilder[]", "System.Runtime.InteropServices", t3624_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2012_TI, t3624_ITIs, t3624_VT, &EmptyCustomAttributesCache, &t3624_TI, &t3624_0_0_0, &t3624_1_0_0, t3624_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3625_TI;



// Metadata Definition System.Reflection.FieldInfo[]
static MethodInfo* t3625_MIs[] =
{
	NULL
};
static MethodInfo* t3625_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25005_MI,
	&m5907_MI,
	&m25006_MI,
	&m25007_MI,
	&m25008_MI,
	&m25009_MI,
	&m25010_MI,
	&m5908_MI,
	&m25004_MI,
	&m25012_MI,
	&m25013_MI,
	&m5905_MI,
	&m5906_MI,
	&m25016_MI,
	&m5907_MI,
	&m25017_MI,
	&m25018_MI,
	&m25019_MI,
	&m25020_MI,
	&m25021_MI,
	&m5908_MI,
	&m25015_MI,
	&m25023_MI,
	&m25024_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3625_ITIs[] = 
{
	&t6294_TI,
	&t6295_TI,
	&t6296_TI,
	&t6297_TI,
	&t6298_TI,
	&t6299_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3625_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6294_TI, 21},
	{ &t6295_TI, 28},
	{ &t6296_TI, 33},
	{ &t6297_TI, 34},
	{ &t6298_TI, 41},
	{ &t6299_TI, 46},
	{ &t5131_TI, 47},
	{ &t5132_TI, 54},
	{ &t5133_TI, 59},
	{ &t5134_TI, 60},
	{ &t5135_TI, 67},
	{ &t5136_TI, 72},
	{ &t5137_TI, 73},
	{ &t5138_TI, 80},
	{ &t5139_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3625_0_0_0;
extern Il2CppType t3625_1_0_0;
struct t1144;
extern TypeInfo t1144_TI;
extern CustomAttributesCache t1144__CustomAttributeCache;
extern CustomAttributesCache t1144__CustomAttributeCache_m7529;
TypeInfo t3625_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FieldInfo[]", "System.Reflection", t3625_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1144_TI, t3625_ITIs, t3625_VT, &EmptyCustomAttributesCache, &t3625_TI, &t3625_0_0_0, &t3625_1_0_0, t3625_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1144 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3626_TI;



// Metadata Definition System.Runtime.InteropServices._FieldInfo[]
static MethodInfo* t3626_MIs[] =
{
	NULL
};
static MethodInfo* t3626_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25016_MI,
	&m5907_MI,
	&m25017_MI,
	&m25018_MI,
	&m25019_MI,
	&m25020_MI,
	&m25021_MI,
	&m5908_MI,
	&m25015_MI,
	&m25023_MI,
	&m25024_MI,
};
static TypeInfo* t3626_ITIs[] = 
{
	&t6297_TI,
	&t6298_TI,
	&t6299_TI,
};
static Il2CppInterfaceOffsetPair t3626_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6297_TI, 21},
	{ &t6298_TI, 28},
	{ &t6299_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3626_0_0_0;
extern Il2CppType t3626_1_0_0;
struct t2013;
extern TypeInfo t2013_TI;
extern CustomAttributesCache t2013__CustomAttributeCache;
TypeInfo t3626_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_FieldInfo[]", "System.Runtime.InteropServices", t3626_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2013_TI, t3626_ITIs, t3626_VT, &EmptyCustomAttributesCache, &t3626_TI, &t3626_0_0_0, &t3626_1_0_0, t3626_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3627_TI;



// Metadata Definition System.Reflection.AssemblyCompanyAttribute[]
static MethodInfo* t3627_MIs[] =
{
	NULL
};
extern MethodInfo m25027_MI;
extern MethodInfo m25028_MI;
extern MethodInfo m25029_MI;
extern MethodInfo m25030_MI;
extern MethodInfo m25031_MI;
extern MethodInfo m25032_MI;
extern MethodInfo m25026_MI;
extern MethodInfo m25034_MI;
extern MethodInfo m25035_MI;
static MethodInfo* t3627_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25027_MI,
	&m5907_MI,
	&m25028_MI,
	&m25029_MI,
	&m25030_MI,
	&m25031_MI,
	&m25032_MI,
	&m5908_MI,
	&m25026_MI,
	&m25034_MI,
	&m25035_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6300_TI;
extern TypeInfo t6301_TI;
extern TypeInfo t6302_TI;
static TypeInfo* t3627_ITIs[] = 
{
	&t6300_TI,
	&t6301_TI,
	&t6302_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3627_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6300_TI, 21},
	{ &t6301_TI, 28},
	{ &t6302_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3627_0_0_0;
extern Il2CppType t3627_1_0_0;
struct t432;
extern TypeInfo t432_TI;
extern CustomAttributesCache t432__CustomAttributeCache;
TypeInfo t3627_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyCompanyAttribute[]", "System.Reflection", t3627_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t432_TI, t3627_ITIs, t3627_VT, &EmptyCustomAttributesCache, &t3627_TI, &t3627_0_0_0, &t3627_1_0_0, t3627_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t432 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3628_TI;



// Metadata Definition System.Reflection.AssemblyConfigurationAttribute[]
static MethodInfo* t3628_MIs[] =
{
	NULL
};
extern MethodInfo m25038_MI;
extern MethodInfo m25039_MI;
extern MethodInfo m25040_MI;
extern MethodInfo m25041_MI;
extern MethodInfo m25042_MI;
extern MethodInfo m25043_MI;
extern MethodInfo m25037_MI;
extern MethodInfo m25045_MI;
extern MethodInfo m25046_MI;
static MethodInfo* t3628_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25038_MI,
	&m5907_MI,
	&m25039_MI,
	&m25040_MI,
	&m25041_MI,
	&m25042_MI,
	&m25043_MI,
	&m5908_MI,
	&m25037_MI,
	&m25045_MI,
	&m25046_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6303_TI;
extern TypeInfo t6304_TI;
extern TypeInfo t6305_TI;
static TypeInfo* t3628_ITIs[] = 
{
	&t6303_TI,
	&t6304_TI,
	&t6305_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3628_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6303_TI, 21},
	{ &t6304_TI, 28},
	{ &t6305_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3628_0_0_0;
extern Il2CppType t3628_1_0_0;
struct t431;
extern TypeInfo t431_TI;
extern CustomAttributesCache t431__CustomAttributeCache;
TypeInfo t3628_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyConfigurationAttribute[]", "System.Reflection", t3628_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t431_TI, t3628_ITIs, t3628_VT, &EmptyCustomAttributesCache, &t3628_TI, &t3628_0_0_0, &t3628_1_0_0, t3628_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t431 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3629_TI;



// Metadata Definition System.Reflection.AssemblyCopyrightAttribute[]
static MethodInfo* t3629_MIs[] =
{
	NULL
};
extern MethodInfo m25049_MI;
extern MethodInfo m25050_MI;
extern MethodInfo m25051_MI;
extern MethodInfo m25052_MI;
extern MethodInfo m25053_MI;
extern MethodInfo m25054_MI;
extern MethodInfo m25048_MI;
extern MethodInfo m25056_MI;
extern MethodInfo m25057_MI;
static MethodInfo* t3629_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25049_MI,
	&m5907_MI,
	&m25050_MI,
	&m25051_MI,
	&m25052_MI,
	&m25053_MI,
	&m25054_MI,
	&m5908_MI,
	&m25048_MI,
	&m25056_MI,
	&m25057_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6306_TI;
extern TypeInfo t6307_TI;
extern TypeInfo t6308_TI;
static TypeInfo* t3629_ITIs[] = 
{
	&t6306_TI,
	&t6307_TI,
	&t6308_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3629_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6306_TI, 21},
	{ &t6307_TI, 28},
	{ &t6308_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3629_0_0_0;
extern Il2CppType t3629_1_0_0;
struct t434;
extern TypeInfo t434_TI;
extern CustomAttributesCache t434__CustomAttributeCache;
TypeInfo t3629_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyCopyrightAttribute[]", "System.Reflection", t3629_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t434_TI, t3629_ITIs, t3629_VT, &EmptyCustomAttributesCache, &t3629_TI, &t3629_0_0_0, &t3629_1_0_0, t3629_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t434 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3630_TI;



// Metadata Definition System.Reflection.AssemblyDefaultAliasAttribute[]
static MethodInfo* t3630_MIs[] =
{
	NULL
};
extern MethodInfo m25060_MI;
extern MethodInfo m25061_MI;
extern MethodInfo m25062_MI;
extern MethodInfo m25063_MI;
extern MethodInfo m25064_MI;
extern MethodInfo m25065_MI;
extern MethodInfo m25059_MI;
extern MethodInfo m25067_MI;
extern MethodInfo m25068_MI;
static MethodInfo* t3630_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25060_MI,
	&m5907_MI,
	&m25061_MI,
	&m25062_MI,
	&m25063_MI,
	&m25064_MI,
	&m25065_MI,
	&m5908_MI,
	&m25059_MI,
	&m25067_MI,
	&m25068_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6309_TI;
extern TypeInfo t6310_TI;
extern TypeInfo t6311_TI;
static TypeInfo* t3630_ITIs[] = 
{
	&t6309_TI,
	&t6310_TI,
	&t6311_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3630_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6309_TI, 21},
	{ &t6310_TI, 28},
	{ &t6311_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3630_0_0_0;
extern Il2CppType t3630_1_0_0;
struct t712;
extern TypeInfo t712_TI;
extern CustomAttributesCache t712__CustomAttributeCache;
TypeInfo t3630_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyDefaultAliasAttribute[]", "System.Reflection", t3630_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t712_TI, t3630_ITIs, t3630_VT, &EmptyCustomAttributesCache, &t3630_TI, &t3630_0_0_0, &t3630_1_0_0, t3630_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t712 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3631_TI;



// Metadata Definition System.Reflection.AssemblyDelaySignAttribute[]
static MethodInfo* t3631_MIs[] =
{
	NULL
};
extern MethodInfo m25071_MI;
extern MethodInfo m25072_MI;
extern MethodInfo m25073_MI;
extern MethodInfo m25074_MI;
extern MethodInfo m25075_MI;
extern MethodInfo m25076_MI;
extern MethodInfo m25070_MI;
extern MethodInfo m25078_MI;
extern MethodInfo m25079_MI;
static MethodInfo* t3631_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25071_MI,
	&m5907_MI,
	&m25072_MI,
	&m25073_MI,
	&m25074_MI,
	&m25075_MI,
	&m25076_MI,
	&m5908_MI,
	&m25070_MI,
	&m25078_MI,
	&m25079_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6312_TI;
extern TypeInfo t6313_TI;
extern TypeInfo t6314_TI;
static TypeInfo* t3631_ITIs[] = 
{
	&t6312_TI,
	&t6313_TI,
	&t6314_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3631_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6312_TI, 21},
	{ &t6313_TI, 28},
	{ &t6314_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3631_0_0_0;
extern Il2CppType t3631_1_0_0;
struct t711;
extern TypeInfo t711_TI;
extern CustomAttributesCache t711__CustomAttributeCache;
TypeInfo t3631_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyDelaySignAttribute[]", "System.Reflection", t3631_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t711_TI, t3631_ITIs, t3631_VT, &EmptyCustomAttributesCache, &t3631_TI, &t3631_0_0_0, &t3631_1_0_0, t3631_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t711 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3632_TI;



// Metadata Definition System.Reflection.AssemblyDescriptionAttribute[]
static MethodInfo* t3632_MIs[] =
{
	NULL
};
extern MethodInfo m25082_MI;
extern MethodInfo m25083_MI;
extern MethodInfo m25084_MI;
extern MethodInfo m25085_MI;
extern MethodInfo m25086_MI;
extern MethodInfo m25087_MI;
extern MethodInfo m25081_MI;
extern MethodInfo m25089_MI;
extern MethodInfo m25090_MI;
static MethodInfo* t3632_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25082_MI,
	&m5907_MI,
	&m25083_MI,
	&m25084_MI,
	&m25085_MI,
	&m25086_MI,
	&m25087_MI,
	&m5908_MI,
	&m25081_MI,
	&m25089_MI,
	&m25090_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6315_TI;
extern TypeInfo t6316_TI;
extern TypeInfo t6317_TI;
static TypeInfo* t3632_ITIs[] = 
{
	&t6315_TI,
	&t6316_TI,
	&t6317_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3632_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6315_TI, 21},
	{ &t6316_TI, 28},
	{ &t6317_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3632_0_0_0;
extern Il2CppType t3632_1_0_0;
struct t430;
extern TypeInfo t430_TI;
extern CustomAttributesCache t430__CustomAttributeCache;
TypeInfo t3632_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyDescriptionAttribute[]", "System.Reflection", t3632_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t430_TI, t3632_ITIs, t3632_VT, &EmptyCustomAttributesCache, &t3632_TI, &t3632_0_0_0, &t3632_1_0_0, t3632_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t430 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3633_TI;



// Metadata Definition System.Reflection.AssemblyFileVersionAttribute[]
static MethodInfo* t3633_MIs[] =
{
	NULL
};
extern MethodInfo m25093_MI;
extern MethodInfo m25094_MI;
extern MethodInfo m25095_MI;
extern MethodInfo m25096_MI;
extern MethodInfo m25097_MI;
extern MethodInfo m25098_MI;
extern MethodInfo m25092_MI;
extern MethodInfo m25100_MI;
extern MethodInfo m25101_MI;
static MethodInfo* t3633_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25093_MI,
	&m5907_MI,
	&m25094_MI,
	&m25095_MI,
	&m25096_MI,
	&m25097_MI,
	&m25098_MI,
	&m5908_MI,
	&m25092_MI,
	&m25100_MI,
	&m25101_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6318_TI;
extern TypeInfo t6319_TI;
extern TypeInfo t6320_TI;
static TypeInfo* t3633_ITIs[] = 
{
	&t6318_TI,
	&t6319_TI,
	&t6320_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3633_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6318_TI, 21},
	{ &t6319_TI, 28},
	{ &t6320_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3633_0_0_0;
extern Il2CppType t3633_1_0_0;
struct t435;
extern TypeInfo t435_TI;
extern CustomAttributesCache t435__CustomAttributeCache;
TypeInfo t3633_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyFileVersionAttribute[]", "System.Reflection", t3633_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t435_TI, t3633_ITIs, t3633_VT, &EmptyCustomAttributesCache, &t3633_TI, &t3633_0_0_0, &t3633_1_0_0, t3633_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t435 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3634_TI;



// Metadata Definition System.Reflection.AssemblyInformationalVersionAttribute[]
static MethodInfo* t3634_MIs[] =
{
	NULL
};
extern MethodInfo m25104_MI;
extern MethodInfo m25105_MI;
extern MethodInfo m25106_MI;
extern MethodInfo m25107_MI;
extern MethodInfo m25108_MI;
extern MethodInfo m25109_MI;
extern MethodInfo m25103_MI;
extern MethodInfo m25111_MI;
extern MethodInfo m25112_MI;
static MethodInfo* t3634_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25104_MI,
	&m5907_MI,
	&m25105_MI,
	&m25106_MI,
	&m25107_MI,
	&m25108_MI,
	&m25109_MI,
	&m5908_MI,
	&m25103_MI,
	&m25111_MI,
	&m25112_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6321_TI;
extern TypeInfo t6322_TI;
extern TypeInfo t6323_TI;
static TypeInfo* t3634_ITIs[] = 
{
	&t6321_TI,
	&t6322_TI,
	&t6323_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3634_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6321_TI, 21},
	{ &t6322_TI, 28},
	{ &t6323_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3634_0_0_0;
extern Il2CppType t3634_1_0_0;
struct t705;
extern TypeInfo t705_TI;
extern CustomAttributesCache t705__CustomAttributeCache;
TypeInfo t3634_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyInformationalVersionAttribute[]", "System.Reflection", t3634_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t705_TI, t3634_ITIs, t3634_VT, &EmptyCustomAttributesCache, &t3634_TI, &t3634_0_0_0, &t3634_1_0_0, t3634_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t705 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3635_TI;



// Metadata Definition System.Reflection.AssemblyKeyFileAttribute[]
static MethodInfo* t3635_MIs[] =
{
	NULL
};
extern MethodInfo m25115_MI;
extern MethodInfo m25116_MI;
extern MethodInfo m25117_MI;
extern MethodInfo m25118_MI;
extern MethodInfo m25119_MI;
extern MethodInfo m25120_MI;
extern MethodInfo m25114_MI;
extern MethodInfo m25122_MI;
extern MethodInfo m25123_MI;
static MethodInfo* t3635_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25115_MI,
	&m5907_MI,
	&m25116_MI,
	&m25117_MI,
	&m25118_MI,
	&m25119_MI,
	&m25120_MI,
	&m5908_MI,
	&m25114_MI,
	&m25122_MI,
	&m25123_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6324_TI;
extern TypeInfo t6325_TI;
extern TypeInfo t6326_TI;
static TypeInfo* t3635_ITIs[] = 
{
	&t6324_TI,
	&t6325_TI,
	&t6326_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3635_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6324_TI, 21},
	{ &t6325_TI, 28},
	{ &t6326_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3635_0_0_0;
extern Il2CppType t3635_1_0_0;
struct t710;
extern TypeInfo t710_TI;
extern CustomAttributesCache t710__CustomAttributeCache;
TypeInfo t3635_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyKeyFileAttribute[]", "System.Reflection", t3635_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t710_TI, t3635_ITIs, t3635_VT, &EmptyCustomAttributesCache, &t3635_TI, &t3635_0_0_0, &t3635_1_0_0, t3635_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t710 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3636_TI;



// Metadata Definition System.Reflection.AssemblyNameFlags[]
static MethodInfo* t3636_MIs[] =
{
	NULL
};
extern MethodInfo m25126_MI;
extern MethodInfo m25127_MI;
extern MethodInfo m25128_MI;
extern MethodInfo m25129_MI;
extern MethodInfo m25130_MI;
extern MethodInfo m25131_MI;
extern MethodInfo m25125_MI;
extern MethodInfo m25133_MI;
extern MethodInfo m25134_MI;
static MethodInfo* t3636_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25126_MI,
	&m5907_MI,
	&m25127_MI,
	&m25128_MI,
	&m25129_MI,
	&m25130_MI,
	&m25131_MI,
	&m5908_MI,
	&m25125_MI,
	&m25133_MI,
	&m25134_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6327_TI;
extern TypeInfo t6328_TI;
extern TypeInfo t6329_TI;
static TypeInfo* t3636_ITIs[] = 
{
	&t6327_TI,
	&t6328_TI,
	&t6329_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3636_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6327_TI, 21},
	{ &t6328_TI, 28},
	{ &t6329_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3636_0_0_0;
extern Il2CppType t3636_1_0_0;
#include "t1362.h"
extern TypeInfo t1362_TI;
extern CustomAttributesCache t1362__CustomAttributeCache;
TypeInfo t3636_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyNameFlags[]", "System.Reflection", t3636_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1362_TI, t3636_ITIs, t3636_VT, &EmptyCustomAttributesCache, &t44_TI, &t3636_0_0_0, &t3636_1_0_0, t3636_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3637_TI;



// Metadata Definition System.Reflection.AssemblyProductAttribute[]
static MethodInfo* t3637_MIs[] =
{
	NULL
};
extern MethodInfo m25137_MI;
extern MethodInfo m25138_MI;
extern MethodInfo m25139_MI;
extern MethodInfo m25140_MI;
extern MethodInfo m25141_MI;
extern MethodInfo m25142_MI;
extern MethodInfo m25136_MI;
extern MethodInfo m25144_MI;
extern MethodInfo m25145_MI;
static MethodInfo* t3637_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25137_MI,
	&m5907_MI,
	&m25138_MI,
	&m25139_MI,
	&m25140_MI,
	&m25141_MI,
	&m25142_MI,
	&m5908_MI,
	&m25136_MI,
	&m25144_MI,
	&m25145_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6330_TI;
extern TypeInfo t6331_TI;
extern TypeInfo t6332_TI;
static TypeInfo* t3637_ITIs[] = 
{
	&t6330_TI,
	&t6331_TI,
	&t6332_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3637_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6330_TI, 21},
	{ &t6331_TI, 28},
	{ &t6332_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3637_0_0_0;
extern Il2CppType t3637_1_0_0;
struct t433;
extern TypeInfo t433_TI;
extern CustomAttributesCache t433__CustomAttributeCache;
TypeInfo t3637_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyProductAttribute[]", "System.Reflection", t3637_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t433_TI, t3637_ITIs, t3637_VT, &EmptyCustomAttributesCache, &t3637_TI, &t3637_0_0_0, &t3637_1_0_0, t3637_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t433 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3638_TI;



// Metadata Definition System.Reflection.AssemblyTitleAttribute[]
static MethodInfo* t3638_MIs[] =
{
	NULL
};
extern MethodInfo m25148_MI;
extern MethodInfo m25149_MI;
extern MethodInfo m25150_MI;
extern MethodInfo m25151_MI;
extern MethodInfo m25152_MI;
extern MethodInfo m25153_MI;
extern MethodInfo m25147_MI;
extern MethodInfo m25155_MI;
extern MethodInfo m25156_MI;
static MethodInfo* t3638_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25148_MI,
	&m5907_MI,
	&m25149_MI,
	&m25150_MI,
	&m25151_MI,
	&m25152_MI,
	&m25153_MI,
	&m5908_MI,
	&m25147_MI,
	&m25155_MI,
	&m25156_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6333_TI;
extern TypeInfo t6334_TI;
extern TypeInfo t6335_TI;
static TypeInfo* t3638_ITIs[] = 
{
	&t6333_TI,
	&t6334_TI,
	&t6335_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3638_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6333_TI, 21},
	{ &t6334_TI, 28},
	{ &t6335_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3638_0_0_0;
extern Il2CppType t3638_1_0_0;
struct t429;
extern TypeInfo t429_TI;
extern CustomAttributesCache t429__CustomAttributeCache;
TypeInfo t3638_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyTitleAttribute[]", "System.Reflection", t3638_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t429_TI, t3638_ITIs, t3638_VT, &EmptyCustomAttributesCache, &t3638_TI, &t3638_0_0_0, &t3638_1_0_0, t3638_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t429 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3639_TI;



// Metadata Definition System.Reflection.AssemblyTrademarkAttribute[]
static MethodInfo* t3639_MIs[] =
{
	NULL
};
extern MethodInfo m25159_MI;
extern MethodInfo m25160_MI;
extern MethodInfo m25161_MI;
extern MethodInfo m25162_MI;
extern MethodInfo m25163_MI;
extern MethodInfo m25164_MI;
extern MethodInfo m25158_MI;
extern MethodInfo m25166_MI;
extern MethodInfo m25167_MI;
static MethodInfo* t3639_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25159_MI,
	&m5907_MI,
	&m25160_MI,
	&m25161_MI,
	&m25162_MI,
	&m25163_MI,
	&m25164_MI,
	&m5908_MI,
	&m25158_MI,
	&m25166_MI,
	&m25167_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6336_TI;
extern TypeInfo t6337_TI;
extern TypeInfo t6338_TI;
static TypeInfo* t3639_ITIs[] = 
{
	&t6336_TI,
	&t6337_TI,
	&t6338_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3639_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6336_TI, 21},
	{ &t6337_TI, 28},
	{ &t6338_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3639_0_0_0;
extern Il2CppType t3639_1_0_0;
struct t438;
extern TypeInfo t438_TI;
extern CustomAttributesCache t438__CustomAttributeCache;
TypeInfo t3639_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyTrademarkAttribute[]", "System.Reflection", t3639_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t438_TI, t3639_ITIs, t3639_VT, &EmptyCustomAttributesCache, &t3639_TI, &t3639_0_0_0, &t3639_1_0_0, t3639_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t438 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1366_TI;



// Metadata Definition System.Reflection.PropertyInfo[]
static MethodInfo* t1366_MIs[] =
{
	NULL
};
extern MethodInfo m25170_MI;
extern MethodInfo m25171_MI;
extern MethodInfo m25172_MI;
extern MethodInfo m25173_MI;
extern MethodInfo m25174_MI;
extern MethodInfo m25175_MI;
extern MethodInfo m25169_MI;
extern MethodInfo m25177_MI;
extern MethodInfo m25178_MI;
extern MethodInfo m25181_MI;
extern MethodInfo m25182_MI;
extern MethodInfo m25183_MI;
extern MethodInfo m25184_MI;
extern MethodInfo m25185_MI;
extern MethodInfo m25186_MI;
extern MethodInfo m25180_MI;
extern MethodInfo m25188_MI;
extern MethodInfo m25189_MI;
static MethodInfo* t1366_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25170_MI,
	&m5907_MI,
	&m25171_MI,
	&m25172_MI,
	&m25173_MI,
	&m25174_MI,
	&m25175_MI,
	&m5908_MI,
	&m25169_MI,
	&m25177_MI,
	&m25178_MI,
	&m5905_MI,
	&m5906_MI,
	&m25181_MI,
	&m5907_MI,
	&m25182_MI,
	&m25183_MI,
	&m25184_MI,
	&m25185_MI,
	&m25186_MI,
	&m5908_MI,
	&m25180_MI,
	&m25188_MI,
	&m25189_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6339_TI;
extern TypeInfo t6340_TI;
extern TypeInfo t6341_TI;
extern TypeInfo t6342_TI;
extern TypeInfo t6343_TI;
extern TypeInfo t6344_TI;
static TypeInfo* t1366_ITIs[] = 
{
	&t6339_TI,
	&t6340_TI,
	&t6341_TI,
	&t6342_TI,
	&t6343_TI,
	&t6344_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1366_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6339_TI, 21},
	{ &t6340_TI, 28},
	{ &t6341_TI, 33},
	{ &t6342_TI, 34},
	{ &t6343_TI, 41},
	{ &t6344_TI, 46},
	{ &t5131_TI, 47},
	{ &t5132_TI, 54},
	{ &t5133_TI, 59},
	{ &t5134_TI, 60},
	{ &t5135_TI, 67},
	{ &t5136_TI, 72},
	{ &t5137_TI, 73},
	{ &t5138_TI, 80},
	{ &t5139_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1366_0_0_0;
extern Il2CppType t1366_1_0_0;
struct t1146;
extern TypeInfo t1146_TI;
extern CustomAttributesCache t1146__CustomAttributeCache;
extern CustomAttributesCache t1146__CustomAttributeCache_m7713;
extern CustomAttributesCache t1146__CustomAttributeCache_m7714;
TypeInfo t1366_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PropertyInfo[]", "System.Reflection", t1366_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1146_TI, t1366_ITIs, t1366_VT, &EmptyCustomAttributesCache, &t1366_TI, &t1366_0_0_0, &t1366_1_0_0, t1366_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1146 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3640_TI;



// Metadata Definition System.Runtime.InteropServices._PropertyInfo[]
static MethodInfo* t3640_MIs[] =
{
	NULL
};
static MethodInfo* t3640_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25181_MI,
	&m5907_MI,
	&m25182_MI,
	&m25183_MI,
	&m25184_MI,
	&m25185_MI,
	&m25186_MI,
	&m5908_MI,
	&m25180_MI,
	&m25188_MI,
	&m25189_MI,
};
static TypeInfo* t3640_ITIs[] = 
{
	&t6342_TI,
	&t6343_TI,
	&t6344_TI,
};
static Il2CppInterfaceOffsetPair t3640_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6342_TI, 21},
	{ &t6343_TI, 28},
	{ &t6344_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3640_0_0_0;
extern Il2CppType t3640_1_0_0;
struct t2036;
extern TypeInfo t2036_TI;
extern CustomAttributesCache t2036__CustomAttributeCache;
TypeInfo t3640_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_PropertyInfo[]", "System.Runtime.InteropServices", t3640_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2036_TI, t3640_ITIs, t3640_VT, &EmptyCustomAttributesCache, &t3640_TI, &t3640_0_0_0, &t3640_1_0_0, t3640_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3641_TI;



// Metadata Definition System.Reflection.BindingFlags[]
static MethodInfo* t3641_MIs[] =
{
	NULL
};
extern MethodInfo m25192_MI;
extern MethodInfo m25193_MI;
extern MethodInfo m25194_MI;
extern MethodInfo m25195_MI;
extern MethodInfo m25196_MI;
extern MethodInfo m25197_MI;
extern MethodInfo m25191_MI;
extern MethodInfo m25199_MI;
extern MethodInfo m25200_MI;
static MethodInfo* t3641_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25192_MI,
	&m5907_MI,
	&m25193_MI,
	&m25194_MI,
	&m25195_MI,
	&m25196_MI,
	&m25197_MI,
	&m5908_MI,
	&m25191_MI,
	&m25199_MI,
	&m25200_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6345_TI;
extern TypeInfo t6346_TI;
extern TypeInfo t6347_TI;
static TypeInfo* t3641_ITIs[] = 
{
	&t6345_TI,
	&t6346_TI,
	&t6347_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3641_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6345_TI, 21},
	{ &t6346_TI, 28},
	{ &t6347_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3641_0_0_0;
extern Il2CppType t3641_1_0_0;
#include "t630.h"
extern TypeInfo t630_TI;
extern CustomAttributesCache t630__CustomAttributeCache;
TypeInfo t3641_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BindingFlags[]", "System.Reflection", t3641_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t630_TI, t3641_ITIs, t3641_VT, &EmptyCustomAttributesCache, &t44_TI, &t3641_0_0_0, &t3641_1_0_0, t3641_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3642_TI;



// Metadata Definition System.Reflection.CallingConventions[]
static MethodInfo* t3642_MIs[] =
{
	NULL
};
extern MethodInfo m25203_MI;
extern MethodInfo m25204_MI;
extern MethodInfo m25205_MI;
extern MethodInfo m25206_MI;
extern MethodInfo m25207_MI;
extern MethodInfo m25208_MI;
extern MethodInfo m25202_MI;
extern MethodInfo m25210_MI;
extern MethodInfo m25211_MI;
static MethodInfo* t3642_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25203_MI,
	&m5907_MI,
	&m25204_MI,
	&m25205_MI,
	&m25206_MI,
	&m25207_MI,
	&m25208_MI,
	&m5908_MI,
	&m25202_MI,
	&m25210_MI,
	&m25211_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6348_TI;
extern TypeInfo t6349_TI;
extern TypeInfo t6350_TI;
static TypeInfo* t3642_ITIs[] = 
{
	&t6348_TI,
	&t6349_TI,
	&t6350_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3642_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6348_TI, 21},
	{ &t6349_TI, 28},
	{ &t6350_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3642_0_0_0;
extern Il2CppType t3642_1_0_0;
#include "t1150.h"
extern TypeInfo t1150_TI;
extern CustomAttributesCache t1150__CustomAttributeCache;
TypeInfo t3642_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CallingConventions[]", "System.Reflection", t3642_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1150_TI, t3642_ITIs, t3642_VT, &EmptyCustomAttributesCache, &t44_TI, &t3642_0_0_0, &t3642_1_0_0, t3642_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3643_TI;



// Metadata Definition System.Reflection.EventAttributes[]
static MethodInfo* t3643_MIs[] =
{
	NULL
};
extern MethodInfo m25214_MI;
extern MethodInfo m25215_MI;
extern MethodInfo m25216_MI;
extern MethodInfo m25217_MI;
extern MethodInfo m25218_MI;
extern MethodInfo m25219_MI;
extern MethodInfo m25213_MI;
extern MethodInfo m25221_MI;
extern MethodInfo m25222_MI;
static MethodInfo* t3643_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25214_MI,
	&m5907_MI,
	&m25215_MI,
	&m25216_MI,
	&m25217_MI,
	&m25218_MI,
	&m25219_MI,
	&m5908_MI,
	&m25213_MI,
	&m25221_MI,
	&m25222_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6351_TI;
extern TypeInfo t6352_TI;
extern TypeInfo t6353_TI;
static TypeInfo* t3643_ITIs[] = 
{
	&t6351_TI,
	&t6352_TI,
	&t6353_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3643_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6351_TI, 21},
	{ &t6352_TI, 28},
	{ &t6353_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3643_0_0_0;
extern Il2CppType t3643_1_0_0;
#include "t1367.h"
extern TypeInfo t1367_TI;
extern CustomAttributesCache t1367__CustomAttributeCache;
TypeInfo t3643_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EventAttributes[]", "System.Reflection", t3643_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1367_TI, t3643_ITIs, t3643_VT, &EmptyCustomAttributesCache, &t44_TI, &t3643_0_0_0, &t3643_1_0_0, t3643_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3644_TI;



// Metadata Definition System.Reflection.FieldAttributes[]
static MethodInfo* t3644_MIs[] =
{
	NULL
};
extern MethodInfo m25225_MI;
extern MethodInfo m25226_MI;
extern MethodInfo m25227_MI;
extern MethodInfo m25228_MI;
extern MethodInfo m25229_MI;
extern MethodInfo m25230_MI;
extern MethodInfo m25224_MI;
extern MethodInfo m25232_MI;
extern MethodInfo m25233_MI;
static MethodInfo* t3644_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25225_MI,
	&m5907_MI,
	&m25226_MI,
	&m25227_MI,
	&m25228_MI,
	&m25229_MI,
	&m25230_MI,
	&m5908_MI,
	&m25224_MI,
	&m25232_MI,
	&m25233_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6354_TI;
extern TypeInfo t6355_TI;
extern TypeInfo t6356_TI;
static TypeInfo* t3644_ITIs[] = 
{
	&t6354_TI,
	&t6355_TI,
	&t6356_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3644_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6354_TI, 21},
	{ &t6355_TI, 28},
	{ &t6356_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3644_0_0_0;
extern Il2CppType t3644_1_0_0;
#include "t1347.h"
extern TypeInfo t1347_TI;
extern CustomAttributesCache t1347__CustomAttributeCache;
TypeInfo t3644_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FieldAttributes[]", "System.Reflection", t3644_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1347_TI, t3644_ITIs, t3644_VT, &EmptyCustomAttributesCache, &t44_TI, &t3644_0_0_0, &t3644_1_0_0, t3644_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3645_TI;



// Metadata Definition System.Reflection.MemberTypes[]
static MethodInfo* t3645_MIs[] =
{
	NULL
};
extern MethodInfo m25236_MI;
extern MethodInfo m25237_MI;
extern MethodInfo m25238_MI;
extern MethodInfo m25239_MI;
extern MethodInfo m25240_MI;
extern MethodInfo m25241_MI;
extern MethodInfo m25235_MI;
extern MethodInfo m25243_MI;
extern MethodInfo m25244_MI;
static MethodInfo* t3645_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25236_MI,
	&m5907_MI,
	&m25237_MI,
	&m25238_MI,
	&m25239_MI,
	&m25240_MI,
	&m25241_MI,
	&m5908_MI,
	&m25235_MI,
	&m25243_MI,
	&m25244_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6357_TI;
extern TypeInfo t6358_TI;
extern TypeInfo t6359_TI;
static TypeInfo* t3645_ITIs[] = 
{
	&t6357_TI,
	&t6358_TI,
	&t6359_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3645_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6357_TI, 21},
	{ &t6358_TI, 28},
	{ &t6359_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3645_0_0_0;
extern Il2CppType t3645_1_0_0;
#include "t1149.h"
extern TypeInfo t1149_TI;
extern CustomAttributesCache t1149__CustomAttributeCache;
TypeInfo t3645_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MemberTypes[]", "System.Reflection", t3645_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1149_TI, t3645_ITIs, t3645_VT, &EmptyCustomAttributesCache, &t44_TI, &t3645_0_0_0, &t3645_1_0_0, t3645_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3646_TI;



// Metadata Definition System.Reflection.MethodAttributes[]
static MethodInfo* t3646_MIs[] =
{
	NULL
};
extern MethodInfo m25247_MI;
extern MethodInfo m25248_MI;
extern MethodInfo m25249_MI;
extern MethodInfo m25250_MI;
extern MethodInfo m25251_MI;
extern MethodInfo m25252_MI;
extern MethodInfo m25246_MI;
extern MethodInfo m25254_MI;
extern MethodInfo m25255_MI;
static MethodInfo* t3646_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25247_MI,
	&m5907_MI,
	&m25248_MI,
	&m25249_MI,
	&m25250_MI,
	&m25251_MI,
	&m25252_MI,
	&m5908_MI,
	&m25246_MI,
	&m25254_MI,
	&m25255_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6360_TI;
extern TypeInfo t6361_TI;
extern TypeInfo t6362_TI;
static TypeInfo* t3646_ITIs[] = 
{
	&t6360_TI,
	&t6361_TI,
	&t6362_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3646_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6360_TI, 21},
	{ &t6361_TI, 28},
	{ &t6362_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3646_0_0_0;
extern Il2CppType t3646_1_0_0;
#include "t1342.h"
extern TypeInfo t1342_TI;
extern CustomAttributesCache t1342__CustomAttributeCache;
TypeInfo t3646_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodAttributes[]", "System.Reflection", t3646_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1342_TI, t3646_ITIs, t3646_VT, &EmptyCustomAttributesCache, &t44_TI, &t3646_0_0_0, &t3646_1_0_0, t3646_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3647_TI;



// Metadata Definition System.Reflection.MethodImplAttributes[]
static MethodInfo* t3647_MIs[] =
{
	NULL
};
extern MethodInfo m25258_MI;
extern MethodInfo m25259_MI;
extern MethodInfo m25260_MI;
extern MethodInfo m25261_MI;
extern MethodInfo m25262_MI;
extern MethodInfo m25263_MI;
extern MethodInfo m25257_MI;
extern MethodInfo m25265_MI;
extern MethodInfo m25266_MI;
static MethodInfo* t3647_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25258_MI,
	&m5907_MI,
	&m25259_MI,
	&m25260_MI,
	&m25261_MI,
	&m25262_MI,
	&m25263_MI,
	&m5908_MI,
	&m25257_MI,
	&m25265_MI,
	&m25266_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6363_TI;
extern TypeInfo t6364_TI;
extern TypeInfo t6365_TI;
static TypeInfo* t3647_ITIs[] = 
{
	&t6363_TI,
	&t6364_TI,
	&t6365_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3647_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6363_TI, 21},
	{ &t6364_TI, 28},
	{ &t6365_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3647_0_0_0;
extern Il2CppType t3647_1_0_0;
#include "t1370.h"
extern TypeInfo t1370_TI;
extern CustomAttributesCache t1370__CustomAttributeCache;
TypeInfo t3647_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodImplAttributes[]", "System.Reflection", t3647_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1370_TI, t3647_ITIs, t3647_VT, &EmptyCustomAttributesCache, &t44_TI, &t3647_0_0_0, &t3647_1_0_0, t3647_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3648_TI;



// Metadata Definition System.Reflection.PInfo[]
static MethodInfo* t3648_MIs[] =
{
	NULL
};
extern MethodInfo m25269_MI;
extern MethodInfo m25270_MI;
extern MethodInfo m25271_MI;
extern MethodInfo m25272_MI;
extern MethodInfo m25273_MI;
extern MethodInfo m25274_MI;
extern MethodInfo m25268_MI;
extern MethodInfo m25276_MI;
extern MethodInfo m25277_MI;
static MethodInfo* t3648_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25269_MI,
	&m5907_MI,
	&m25270_MI,
	&m25271_MI,
	&m25272_MI,
	&m25273_MI,
	&m25274_MI,
	&m5908_MI,
	&m25268_MI,
	&m25276_MI,
	&m25277_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6366_TI;
extern TypeInfo t6367_TI;
extern TypeInfo t6368_TI;
static TypeInfo* t3648_ITIs[] = 
{
	&t6366_TI,
	&t6367_TI,
	&t6368_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3648_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6366_TI, 21},
	{ &t6367_TI, 28},
	{ &t6368_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3648_0_0_0;
extern Il2CppType t3648_1_0_0;
#include "t1384.h"
extern TypeInfo t1384_TI;
extern CustomAttributesCache t1384__CustomAttributeCache;
TypeInfo t3648_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PInfo[]", "System.Reflection", t3648_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1384_TI, t3648_ITIs, t3648_VT, &EmptyCustomAttributesCache, &t44_TI, &t3648_0_0_0, &t3648_1_0_0, t3648_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3649_TI;



// Metadata Definition System.Reflection.ParameterAttributes[]
static MethodInfo* t3649_MIs[] =
{
	NULL
};
extern MethodInfo m25282_MI;
extern MethodInfo m25283_MI;
extern MethodInfo m25284_MI;
extern MethodInfo m25285_MI;
extern MethodInfo m25286_MI;
extern MethodInfo m25287_MI;
extern MethodInfo m25281_MI;
extern MethodInfo m25289_MI;
extern MethodInfo m25290_MI;
static MethodInfo* t3649_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25282_MI,
	&m5907_MI,
	&m25283_MI,
	&m25284_MI,
	&m25285_MI,
	&m25286_MI,
	&m25287_MI,
	&m5908_MI,
	&m25281_MI,
	&m25289_MI,
	&m25290_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6369_TI;
extern TypeInfo t6370_TI;
extern TypeInfo t6371_TI;
static TypeInfo* t3649_ITIs[] = 
{
	&t6369_TI,
	&t6370_TI,
	&t6371_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3649_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6369_TI, 21},
	{ &t6370_TI, 28},
	{ &t6371_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3649_0_0_0;
extern Il2CppType t3649_1_0_0;
#include "t1353.h"
extern TypeInfo t1353_TI;
extern CustomAttributesCache t1353__CustomAttributeCache;
TypeInfo t3649_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterAttributes[]", "System.Reflection", t3649_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1353_TI, t3649_ITIs, t3649_VT, &EmptyCustomAttributesCache, &t44_TI, &t3649_0_0_0, &t3649_1_0_0, t3649_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3650_TI;



// Metadata Definition System.Reflection.ProcessorArchitecture[]
static MethodInfo* t3650_MIs[] =
{
	NULL
};
extern MethodInfo m25293_MI;
extern MethodInfo m25294_MI;
extern MethodInfo m25295_MI;
extern MethodInfo m25296_MI;
extern MethodInfo m25297_MI;
extern MethodInfo m25298_MI;
extern MethodInfo m25292_MI;
extern MethodInfo m25300_MI;
extern MethodInfo m25301_MI;
static MethodInfo* t3650_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25293_MI,
	&m5907_MI,
	&m25294_MI,
	&m25295_MI,
	&m25296_MI,
	&m25297_MI,
	&m25298_MI,
	&m5908_MI,
	&m25292_MI,
	&m25300_MI,
	&m25301_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6372_TI;
extern TypeInfo t6373_TI;
extern TypeInfo t6374_TI;
static TypeInfo* t3650_ITIs[] = 
{
	&t6372_TI,
	&t6373_TI,
	&t6374_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3650_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6372_TI, 21},
	{ &t6373_TI, 28},
	{ &t6374_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3650_0_0_0;
extern Il2CppType t3650_1_0_0;
#include "t1363.h"
extern TypeInfo t1363_TI;
extern CustomAttributesCache t1363__CustomAttributeCache;
TypeInfo t3650_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ProcessorArchitecture[]", "System.Reflection", t3650_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1363_TI, t3650_ITIs, t3650_VT, &EmptyCustomAttributesCache, &t44_TI, &t3650_0_0_0, &t3650_1_0_0, t3650_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3651_TI;



// Metadata Definition System.Reflection.PropertyAttributes[]
static MethodInfo* t3651_MIs[] =
{
	NULL
};
extern MethodInfo m25304_MI;
extern MethodInfo m25305_MI;
extern MethodInfo m25306_MI;
extern MethodInfo m25307_MI;
extern MethodInfo m25308_MI;
extern MethodInfo m25309_MI;
extern MethodInfo m25303_MI;
extern MethodInfo m25311_MI;
extern MethodInfo m25312_MI;
static MethodInfo* t3651_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25304_MI,
	&m5907_MI,
	&m25305_MI,
	&m25306_MI,
	&m25307_MI,
	&m25308_MI,
	&m25309_MI,
	&m5908_MI,
	&m25303_MI,
	&m25311_MI,
	&m25312_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6375_TI;
extern TypeInfo t6376_TI;
extern TypeInfo t6377_TI;
static TypeInfo* t3651_ITIs[] = 
{
	&t6375_TI,
	&t6376_TI,
	&t6377_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3651_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6375_TI, 21},
	{ &t6376_TI, 28},
	{ &t6377_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3651_0_0_0;
extern Il2CppType t3651_1_0_0;
#include "t1382.h"
extern TypeInfo t1382_TI;
extern CustomAttributesCache t1382__CustomAttributeCache;
TypeInfo t3651_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PropertyAttributes[]", "System.Reflection", t3651_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1382_TI, t3651_ITIs, t3651_VT, &EmptyCustomAttributesCache, &t44_TI, &t3651_0_0_0, &t3651_1_0_0, t3651_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3652_TI;



// Metadata Definition System.Reflection.TypeAttributes[]
static MethodInfo* t3652_MIs[] =
{
	NULL
};
extern MethodInfo m25315_MI;
extern MethodInfo m25316_MI;
extern MethodInfo m25317_MI;
extern MethodInfo m25318_MI;
extern MethodInfo m25319_MI;
extern MethodInfo m25320_MI;
extern MethodInfo m25314_MI;
extern MethodInfo m25322_MI;
extern MethodInfo m25323_MI;
static MethodInfo* t3652_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25315_MI,
	&m5907_MI,
	&m25316_MI,
	&m25317_MI,
	&m25318_MI,
	&m25319_MI,
	&m25320_MI,
	&m5908_MI,
	&m25314_MI,
	&m25322_MI,
	&m25323_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6378_TI;
extern TypeInfo t6379_TI;
extern TypeInfo t6380_TI;
static TypeInfo* t3652_ITIs[] = 
{
	&t6378_TI,
	&t6379_TI,
	&t6380_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3652_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6378_TI, 21},
	{ &t6379_TI, 28},
	{ &t6380_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3652_0_0_0;
extern Il2CppType t3652_1_0_0;
#include "t1148.h"
extern TypeInfo t1148_TI;
extern CustomAttributesCache t1148__CustomAttributeCache;
TypeInfo t3652_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeAttributes[]", "System.Reflection", t3652_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1148_TI, t3652_ITIs, t3652_VT, &EmptyCustomAttributesCache, &t44_TI, &t3652_0_0_0, &t3652_1_0_0, t3652_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3653_TI;



// Metadata Definition System.Resources.NeutralResourcesLanguageAttribute[]
static MethodInfo* t3653_MIs[] =
{
	NULL
};
extern MethodInfo m25326_MI;
extern MethodInfo m25327_MI;
extern MethodInfo m25328_MI;
extern MethodInfo m25329_MI;
extern MethodInfo m25330_MI;
extern MethodInfo m25331_MI;
extern MethodInfo m25325_MI;
extern MethodInfo m25333_MI;
extern MethodInfo m25334_MI;
static MethodInfo* t3653_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25326_MI,
	&m5907_MI,
	&m25327_MI,
	&m25328_MI,
	&m25329_MI,
	&m25330_MI,
	&m25331_MI,
	&m5908_MI,
	&m25325_MI,
	&m25333_MI,
	&m25334_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6381_TI;
extern TypeInfo t6382_TI;
extern TypeInfo t6383_TI;
static TypeInfo* t3653_ITIs[] = 
{
	&t6381_TI,
	&t6382_TI,
	&t6383_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3653_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6381_TI, 21},
	{ &t6382_TI, 28},
	{ &t6383_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3653_0_0_0;
extern Il2CppType t3653_1_0_0;
struct t706;
extern TypeInfo t706_TI;
extern CustomAttributesCache t706__CustomAttributeCache;
TypeInfo t3653_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NeutralResourcesLanguageAttribute[]", "System.Resources", t3653_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t706_TI, t3653_ITIs, t3653_VT, &EmptyCustomAttributesCache, &t3653_TI, &t3653_0_0_0, &t3653_1_0_0, t3653_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t706 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3654_TI;



// Metadata Definition System.Resources.SatelliteContractVersionAttribute[]
static MethodInfo* t3654_MIs[] =
{
	NULL
};
extern MethodInfo m25337_MI;
extern MethodInfo m25338_MI;
extern MethodInfo m25339_MI;
extern MethodInfo m25340_MI;
extern MethodInfo m25341_MI;
extern MethodInfo m25342_MI;
extern MethodInfo m25336_MI;
extern MethodInfo m25344_MI;
extern MethodInfo m25345_MI;
static MethodInfo* t3654_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25337_MI,
	&m5907_MI,
	&m25338_MI,
	&m25339_MI,
	&m25340_MI,
	&m25341_MI,
	&m25342_MI,
	&m5908_MI,
	&m25336_MI,
	&m25344_MI,
	&m25345_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6384_TI;
extern TypeInfo t6385_TI;
extern TypeInfo t6386_TI;
static TypeInfo* t3654_ITIs[] = 
{
	&t6384_TI,
	&t6385_TI,
	&t6386_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3654_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6384_TI, 21},
	{ &t6385_TI, 28},
	{ &t6386_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3654_0_0_0;
extern Il2CppType t3654_1_0_0;
struct t704;
extern TypeInfo t704_TI;
extern CustomAttributesCache t704__CustomAttributeCache;
TypeInfo t3654_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SatelliteContractVersionAttribute[]", "System.Resources", t3654_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t704_TI, t3654_ITIs, t3654_VT, &EmptyCustomAttributesCache, &t3654_TI, &t3654_0_0_0, &t3654_1_0_0, t3654_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t704 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3655_TI;



// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxations[]
static MethodInfo* t3655_MIs[] =
{
	NULL
};
extern MethodInfo m25348_MI;
extern MethodInfo m25349_MI;
extern MethodInfo m25350_MI;
extern MethodInfo m25351_MI;
extern MethodInfo m25352_MI;
extern MethodInfo m25353_MI;
extern MethodInfo m25347_MI;
extern MethodInfo m25355_MI;
extern MethodInfo m25356_MI;
static MethodInfo* t3655_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25348_MI,
	&m5907_MI,
	&m25349_MI,
	&m25350_MI,
	&m25351_MI,
	&m25352_MI,
	&m25353_MI,
	&m5908_MI,
	&m25347_MI,
	&m25355_MI,
	&m25356_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6387_TI;
extern TypeInfo t6388_TI;
extern TypeInfo t6389_TI;
static TypeInfo* t3655_ITIs[] = 
{
	&t6387_TI,
	&t6388_TI,
	&t6389_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3655_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6387_TI, 21},
	{ &t6388_TI, 28},
	{ &t6389_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3655_0_0_0;
extern Il2CppType t3655_1_0_0;
#include "t1392.h"
extern TypeInfo t1392_TI;
extern CustomAttributesCache t1392__CustomAttributeCache;
TypeInfo t3655_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CompilationRelaxations[]", "System.Runtime.CompilerServices", t3655_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1392_TI, t3655_ITIs, t3655_VT, &EmptyCustomAttributesCache, &t44_TI, &t3655_0_0_0, &t3655_1_0_0, t3655_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3656_TI;



// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxationsAttribute[]
static MethodInfo* t3656_MIs[] =
{
	NULL
};
extern MethodInfo m25359_MI;
extern MethodInfo m25360_MI;
extern MethodInfo m25361_MI;
extern MethodInfo m25362_MI;
extern MethodInfo m25363_MI;
extern MethodInfo m25364_MI;
extern MethodInfo m25358_MI;
extern MethodInfo m25366_MI;
extern MethodInfo m25367_MI;
static MethodInfo* t3656_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25359_MI,
	&m5907_MI,
	&m25360_MI,
	&m25361_MI,
	&m25362_MI,
	&m25363_MI,
	&m25364_MI,
	&m5908_MI,
	&m25358_MI,
	&m25366_MI,
	&m25367_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6390_TI;
extern TypeInfo t6391_TI;
extern TypeInfo t6392_TI;
static TypeInfo* t3656_ITIs[] = 
{
	&t6390_TI,
	&t6391_TI,
	&t6392_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3656_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6390_TI, 21},
	{ &t6391_TI, 28},
	{ &t6392_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3656_0_0_0;
extern Il2CppType t3656_1_0_0;
struct t709;
extern TypeInfo t709_TI;
extern CustomAttributesCache t709__CustomAttributeCache;
TypeInfo t3656_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CompilationRelaxationsAttribute[]", "System.Runtime.CompilerServices", t3656_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t709_TI, t3656_ITIs, t3656_VT, &EmptyCustomAttributesCache, &t3656_TI, &t3656_0_0_0, &t3656_1_0_0, t3656_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t709 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3657_TI;



// Metadata Definition System.Runtime.CompilerServices.DefaultDependencyAttribute[]
static MethodInfo* t3657_MIs[] =
{
	NULL
};
extern MethodInfo m25370_MI;
extern MethodInfo m25371_MI;
extern MethodInfo m25372_MI;
extern MethodInfo m25373_MI;
extern MethodInfo m25374_MI;
extern MethodInfo m25375_MI;
extern MethodInfo m25369_MI;
extern MethodInfo m25377_MI;
extern MethodInfo m25378_MI;
static MethodInfo* t3657_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25370_MI,
	&m5907_MI,
	&m25371_MI,
	&m25372_MI,
	&m25373_MI,
	&m25374_MI,
	&m25375_MI,
	&m5908_MI,
	&m25369_MI,
	&m25377_MI,
	&m25378_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6393_TI;
extern TypeInfo t6394_TI;
extern TypeInfo t6395_TI;
static TypeInfo* t3657_ITIs[] = 
{
	&t6393_TI,
	&t6394_TI,
	&t6395_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3657_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6393_TI, 21},
	{ &t6394_TI, 28},
	{ &t6395_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3657_0_0_0;
extern Il2CppType t3657_1_0_0;
struct t1393;
extern TypeInfo t1393_TI;
extern CustomAttributesCache t1393__CustomAttributeCache;
TypeInfo t3657_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultDependencyAttribute[]", "System.Runtime.CompilerServices", t3657_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1393_TI, t3657_ITIs, t3657_VT, &EmptyCustomAttributesCache, &t3657_TI, &t3657_0_0_0, &t3657_1_0_0, t3657_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1393 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3658_TI;



// Metadata Definition System.Runtime.CompilerServices.LoadHint[]
static MethodInfo* t3658_MIs[] =
{
	NULL
};
extern MethodInfo m25381_MI;
extern MethodInfo m25382_MI;
extern MethodInfo m25383_MI;
extern MethodInfo m25384_MI;
extern MethodInfo m25385_MI;
extern MethodInfo m25386_MI;
extern MethodInfo m25380_MI;
extern MethodInfo m25388_MI;
extern MethodInfo m25389_MI;
static MethodInfo* t3658_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25381_MI,
	&m5907_MI,
	&m25382_MI,
	&m25383_MI,
	&m25384_MI,
	&m25385_MI,
	&m25386_MI,
	&m5908_MI,
	&m25380_MI,
	&m25388_MI,
	&m25389_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6396_TI;
extern TypeInfo t6397_TI;
extern TypeInfo t6398_TI;
static TypeInfo* t3658_ITIs[] = 
{
	&t6396_TI,
	&t6397_TI,
	&t6398_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3658_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6396_TI, 21},
	{ &t6397_TI, 28},
	{ &t6398_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3658_0_0_0;
extern Il2CppType t3658_1_0_0;
#include "t1394.h"
extern TypeInfo t1394_TI;
TypeInfo t3658_TI = 
{
	&g_mscorlib_dll_Image, NULL, "LoadHint[]", "System.Runtime.CompilerServices", t3658_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1394_TI, t3658_ITIs, t3658_VT, &EmptyCustomAttributesCache, &t44_TI, &t3658_0_0_0, &t3658_1_0_0, t3658_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3659_TI;



// Metadata Definition System.Runtime.CompilerServices.StringFreezingAttribute[]
static MethodInfo* t3659_MIs[] =
{
	NULL
};
extern MethodInfo m25392_MI;
extern MethodInfo m25393_MI;
extern MethodInfo m25394_MI;
extern MethodInfo m25395_MI;
extern MethodInfo m25396_MI;
extern MethodInfo m25397_MI;
extern MethodInfo m25391_MI;
extern MethodInfo m25399_MI;
extern MethodInfo m25400_MI;
static MethodInfo* t3659_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25392_MI,
	&m5907_MI,
	&m25393_MI,
	&m25394_MI,
	&m25395_MI,
	&m25396_MI,
	&m25397_MI,
	&m5908_MI,
	&m25391_MI,
	&m25399_MI,
	&m25400_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6399_TI;
extern TypeInfo t6400_TI;
extern TypeInfo t6401_TI;
static TypeInfo* t3659_ITIs[] = 
{
	&t6399_TI,
	&t6400_TI,
	&t6401_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3659_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6399_TI, 21},
	{ &t6400_TI, 28},
	{ &t6401_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3659_0_0_0;
extern Il2CppType t3659_1_0_0;
struct t1396;
extern TypeInfo t1396_TI;
extern CustomAttributesCache t1396__CustomAttributeCache;
TypeInfo t3659_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringFreezingAttribute[]", "System.Runtime.CompilerServices", t3659_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1396_TI, t3659_ITIs, t3659_VT, &EmptyCustomAttributesCache, &t3659_TI, &t3659_0_0_0, &t3659_1_0_0, t3659_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1396 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3660_TI;



// Metadata Definition System.Runtime.ConstrainedExecution.Cer[]
static MethodInfo* t3660_MIs[] =
{
	NULL
};
extern MethodInfo m25403_MI;
extern MethodInfo m25404_MI;
extern MethodInfo m25405_MI;
extern MethodInfo m25406_MI;
extern MethodInfo m25407_MI;
extern MethodInfo m25408_MI;
extern MethodInfo m25402_MI;
extern MethodInfo m25410_MI;
extern MethodInfo m25411_MI;
static MethodInfo* t3660_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25403_MI,
	&m5907_MI,
	&m25404_MI,
	&m25405_MI,
	&m25406_MI,
	&m25407_MI,
	&m25408_MI,
	&m5908_MI,
	&m25402_MI,
	&m25410_MI,
	&m25411_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6402_TI;
extern TypeInfo t6403_TI;
extern TypeInfo t6404_TI;
static TypeInfo* t3660_ITIs[] = 
{
	&t6402_TI,
	&t6403_TI,
	&t6404_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3660_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6402_TI, 21},
	{ &t6403_TI, 28},
	{ &t6404_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3660_0_0_0;
extern Il2CppType t3660_1_0_0;
#include "t1397.h"
extern TypeInfo t1397_TI;
TypeInfo t3660_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Cer[]", "System.Runtime.ConstrainedExecution", t3660_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1397_TI, t3660_ITIs, t3660_VT, &EmptyCustomAttributesCache, &t44_TI, &t3660_0_0_0, &t3660_1_0_0, t3660_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3661_TI;



// Metadata Definition System.Runtime.ConstrainedExecution.Consistency[]
static MethodInfo* t3661_MIs[] =
{
	NULL
};
extern MethodInfo m25414_MI;
extern MethodInfo m25415_MI;
extern MethodInfo m25416_MI;
extern MethodInfo m25417_MI;
extern MethodInfo m25418_MI;
extern MethodInfo m25419_MI;
extern MethodInfo m25413_MI;
extern MethodInfo m25421_MI;
extern MethodInfo m25422_MI;
static MethodInfo* t3661_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25414_MI,
	&m5907_MI,
	&m25415_MI,
	&m25416_MI,
	&m25417_MI,
	&m25418_MI,
	&m25419_MI,
	&m5908_MI,
	&m25413_MI,
	&m25421_MI,
	&m25422_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6405_TI;
extern TypeInfo t6406_TI;
extern TypeInfo t6407_TI;
static TypeInfo* t3661_ITIs[] = 
{
	&t6405_TI,
	&t6406_TI,
	&t6407_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3661_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6405_TI, 21},
	{ &t6406_TI, 28},
	{ &t6407_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3661_0_0_0;
extern Il2CppType t3661_1_0_0;
#include "t1398.h"
extern TypeInfo t1398_TI;
TypeInfo t3661_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Consistency[]", "System.Runtime.ConstrainedExecution", t3661_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1398_TI, t3661_ITIs, t3661_VT, &EmptyCustomAttributesCache, &t44_TI, &t3661_0_0_0, &t3661_1_0_0, t3661_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3662_TI;



// Metadata Definition System.Runtime.ConstrainedExecution.ReliabilityContractAttribute[]
static MethodInfo* t3662_MIs[] =
{
	NULL
};
extern MethodInfo m25425_MI;
extern MethodInfo m25426_MI;
extern MethodInfo m25427_MI;
extern MethodInfo m25428_MI;
extern MethodInfo m25429_MI;
extern MethodInfo m25430_MI;
extern MethodInfo m25424_MI;
extern MethodInfo m25432_MI;
extern MethodInfo m25433_MI;
static MethodInfo* t3662_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25425_MI,
	&m5907_MI,
	&m25426_MI,
	&m25427_MI,
	&m25428_MI,
	&m25429_MI,
	&m25430_MI,
	&m5908_MI,
	&m25424_MI,
	&m25432_MI,
	&m25433_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6408_TI;
extern TypeInfo t6409_TI;
extern TypeInfo t6410_TI;
static TypeInfo* t3662_ITIs[] = 
{
	&t6408_TI,
	&t6409_TI,
	&t6410_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3662_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6408_TI, 21},
	{ &t6409_TI, 28},
	{ &t6410_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3662_0_0_0;
extern Il2CppType t3662_1_0_0;
struct t1400;
extern TypeInfo t1400_TI;
extern CustomAttributesCache t1400__CustomAttributeCache;
TypeInfo t3662_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReliabilityContractAttribute[]", "System.Runtime.ConstrainedExecution", t3662_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1400_TI, t3662_ITIs, t3662_VT, &EmptyCustomAttributesCache, &t3662_TI, &t3662_0_0_0, &t3662_1_0_0, t3662_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1400 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3663_TI;



// Metadata Definition System.Runtime.InteropServices.CallingConvention[]
static MethodInfo* t3663_MIs[] =
{
	NULL
};
extern MethodInfo m25436_MI;
extern MethodInfo m25437_MI;
extern MethodInfo m25438_MI;
extern MethodInfo m25439_MI;
extern MethodInfo m25440_MI;
extern MethodInfo m25441_MI;
extern MethodInfo m25435_MI;
extern MethodInfo m25443_MI;
extern MethodInfo m25444_MI;
static MethodInfo* t3663_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25436_MI,
	&m5907_MI,
	&m25437_MI,
	&m25438_MI,
	&m25439_MI,
	&m25440_MI,
	&m25441_MI,
	&m5908_MI,
	&m25435_MI,
	&m25443_MI,
	&m25444_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6411_TI;
extern TypeInfo t6412_TI;
extern TypeInfo t6413_TI;
static TypeInfo* t3663_ITIs[] = 
{
	&t6411_TI,
	&t6412_TI,
	&t6413_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3663_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6411_TI, 21},
	{ &t6412_TI, 28},
	{ &t6413_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3663_0_0_0;
extern Il2CppType t3663_1_0_0;
#include "t1153.h"
extern TypeInfo t1153_TI;
extern CustomAttributesCache t1153__CustomAttributeCache;
TypeInfo t3663_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CallingConvention[]", "System.Runtime.InteropServices", t3663_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1153_TI, t3663_ITIs, t3663_VT, &EmptyCustomAttributesCache, &t44_TI, &t3663_0_0_0, &t3663_1_0_0, t3663_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3664_TI;



// Metadata Definition System.Runtime.InteropServices.CharSet[]
static MethodInfo* t3664_MIs[] =
{
	NULL
};
extern MethodInfo m25447_MI;
extern MethodInfo m25448_MI;
extern MethodInfo m25449_MI;
extern MethodInfo m25450_MI;
extern MethodInfo m25451_MI;
extern MethodInfo m25452_MI;
extern MethodInfo m25446_MI;
extern MethodInfo m25454_MI;
extern MethodInfo m25455_MI;
static MethodInfo* t3664_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25447_MI,
	&m5907_MI,
	&m25448_MI,
	&m25449_MI,
	&m25450_MI,
	&m25451_MI,
	&m25452_MI,
	&m5908_MI,
	&m25446_MI,
	&m25454_MI,
	&m25455_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6414_TI;
extern TypeInfo t6415_TI;
extern TypeInfo t6416_TI;
static TypeInfo* t3664_ITIs[] = 
{
	&t6414_TI,
	&t6415_TI,
	&t6416_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3664_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6414_TI, 21},
	{ &t6415_TI, 28},
	{ &t6416_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3664_0_0_0;
extern Il2CppType t3664_1_0_0;
#include "t1154.h"
extern TypeInfo t1154_TI;
extern CustomAttributesCache t1154__CustomAttributeCache;
TypeInfo t3664_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CharSet[]", "System.Runtime.InteropServices", t3664_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1154_TI, t3664_ITIs, t3664_VT, &EmptyCustomAttributesCache, &t44_TI, &t3664_0_0_0, &t3664_1_0_0, t3664_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3665_TI;



// Metadata Definition System.Runtime.InteropServices.ClassInterfaceAttribute[]
static MethodInfo* t3665_MIs[] =
{
	NULL
};
extern MethodInfo m25458_MI;
extern MethodInfo m25459_MI;
extern MethodInfo m25460_MI;
extern MethodInfo m25461_MI;
extern MethodInfo m25462_MI;
extern MethodInfo m25463_MI;
extern MethodInfo m25457_MI;
extern MethodInfo m25465_MI;
extern MethodInfo m25466_MI;
static MethodInfo* t3665_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25458_MI,
	&m5907_MI,
	&m25459_MI,
	&m25460_MI,
	&m25461_MI,
	&m25462_MI,
	&m25463_MI,
	&m5908_MI,
	&m25457_MI,
	&m25465_MI,
	&m25466_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6417_TI;
extern TypeInfo t6418_TI;
extern TypeInfo t6419_TI;
static TypeInfo* t3665_ITIs[] = 
{
	&t6417_TI,
	&t6418_TI,
	&t6419_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3665_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6417_TI, 21},
	{ &t6418_TI, 28},
	{ &t6419_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3665_0_0_0;
extern Il2CppType t3665_1_0_0;
struct t1402;
extern TypeInfo t1402_TI;
extern CustomAttributesCache t1402__CustomAttributeCache;
TypeInfo t3665_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ClassInterfaceAttribute[]", "System.Runtime.InteropServices", t3665_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1402_TI, t3665_ITIs, t3665_VT, &EmptyCustomAttributesCache, &t3665_TI, &t3665_0_0_0, &t3665_1_0_0, t3665_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1402 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3666_TI;



// Metadata Definition System.Runtime.InteropServices.ClassInterfaceType[]
static MethodInfo* t3666_MIs[] =
{
	NULL
};
extern MethodInfo m25469_MI;
extern MethodInfo m25470_MI;
extern MethodInfo m25471_MI;
extern MethodInfo m25472_MI;
extern MethodInfo m25473_MI;
extern MethodInfo m25474_MI;
extern MethodInfo m25468_MI;
extern MethodInfo m25476_MI;
extern MethodInfo m25477_MI;
static MethodInfo* t3666_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25469_MI,
	&m5907_MI,
	&m25470_MI,
	&m25471_MI,
	&m25472_MI,
	&m25473_MI,
	&m25474_MI,
	&m5908_MI,
	&m25468_MI,
	&m25476_MI,
	&m25477_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6420_TI;
extern TypeInfo t6421_TI;
extern TypeInfo t6422_TI;
static TypeInfo* t3666_ITIs[] = 
{
	&t6420_TI,
	&t6421_TI,
	&t6422_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3666_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6420_TI, 21},
	{ &t6421_TI, 28},
	{ &t6422_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3666_0_0_0;
extern Il2CppType t3666_1_0_0;
#include "t1403.h"
extern TypeInfo t1403_TI;
extern CustomAttributesCache t1403__CustomAttributeCache;
TypeInfo t3666_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ClassInterfaceType[]", "System.Runtime.InteropServices", t3666_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1403_TI, t3666_ITIs, t3666_VT, &EmptyCustomAttributesCache, &t44_TI, &t3666_0_0_0, &t3666_1_0_0, t3666_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3667_TI;



// Metadata Definition System.Runtime.InteropServices.ComDefaultInterfaceAttribute[]
static MethodInfo* t3667_MIs[] =
{
	NULL
};
extern MethodInfo m25480_MI;
extern MethodInfo m25481_MI;
extern MethodInfo m25482_MI;
extern MethodInfo m25483_MI;
extern MethodInfo m25484_MI;
extern MethodInfo m25485_MI;
extern MethodInfo m25479_MI;
extern MethodInfo m25487_MI;
extern MethodInfo m25488_MI;
static MethodInfo* t3667_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25480_MI,
	&m5907_MI,
	&m25481_MI,
	&m25482_MI,
	&m25483_MI,
	&m25484_MI,
	&m25485_MI,
	&m5908_MI,
	&m25479_MI,
	&m25487_MI,
	&m25488_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6423_TI;
extern TypeInfo t6424_TI;
extern TypeInfo t6425_TI;
static TypeInfo* t3667_ITIs[] = 
{
	&t6423_TI,
	&t6424_TI,
	&t6425_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3667_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6423_TI, 21},
	{ &t6424_TI, 28},
	{ &t6425_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3667_0_0_0;
extern Il2CppType t3667_1_0_0;
struct t1404;
extern TypeInfo t1404_TI;
extern CustomAttributesCache t1404__CustomAttributeCache;
TypeInfo t3667_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ComDefaultInterfaceAttribute[]", "System.Runtime.InteropServices", t3667_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1404_TI, t3667_ITIs, t3667_VT, &EmptyCustomAttributesCache, &t3667_TI, &t3667_0_0_0, &t3667_1_0_0, t3667_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1404 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3668_TI;



// Metadata Definition System.Runtime.InteropServices.ComInterfaceType[]
static MethodInfo* t3668_MIs[] =
{
	NULL
};
extern MethodInfo m25491_MI;
extern MethodInfo m25492_MI;
extern MethodInfo m25493_MI;
extern MethodInfo m25494_MI;
extern MethodInfo m25495_MI;
extern MethodInfo m25496_MI;
extern MethodInfo m25490_MI;
extern MethodInfo m25498_MI;
extern MethodInfo m25499_MI;
static MethodInfo* t3668_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25491_MI,
	&m5907_MI,
	&m25492_MI,
	&m25493_MI,
	&m25494_MI,
	&m25495_MI,
	&m25496_MI,
	&m5908_MI,
	&m25490_MI,
	&m25498_MI,
	&m25499_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6426_TI;
extern TypeInfo t6427_TI;
extern TypeInfo t6428_TI;
static TypeInfo* t3668_ITIs[] = 
{
	&t6426_TI,
	&t6427_TI,
	&t6428_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3668_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6426_TI, 21},
	{ &t6427_TI, 28},
	{ &t6428_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3668_0_0_0;
extern Il2CppType t3668_1_0_0;
#include "t1405.h"
extern TypeInfo t1405_TI;
extern CustomAttributesCache t1405__CustomAttributeCache;
TypeInfo t3668_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ComInterfaceType[]", "System.Runtime.InteropServices", t3668_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1405_TI, t3668_ITIs, t3668_VT, &EmptyCustomAttributesCache, &t44_TI, &t3668_0_0_0, &t3668_1_0_0, t3668_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3669_TI;



// Metadata Definition System.Runtime.InteropServices.DispIdAttribute[]
static MethodInfo* t3669_MIs[] =
{
	NULL
};
extern MethodInfo m25502_MI;
extern MethodInfo m25503_MI;
extern MethodInfo m25504_MI;
extern MethodInfo m25505_MI;
extern MethodInfo m25506_MI;
extern MethodInfo m25507_MI;
extern MethodInfo m25501_MI;
extern MethodInfo m25509_MI;
extern MethodInfo m25510_MI;
static MethodInfo* t3669_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25502_MI,
	&m5907_MI,
	&m25503_MI,
	&m25504_MI,
	&m25505_MI,
	&m25506_MI,
	&m25507_MI,
	&m5908_MI,
	&m25501_MI,
	&m25509_MI,
	&m25510_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6429_TI;
extern TypeInfo t6430_TI;
extern TypeInfo t6431_TI;
static TypeInfo* t3669_ITIs[] = 
{
	&t6429_TI,
	&t6430_TI,
	&t6431_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3669_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6429_TI, 21},
	{ &t6430_TI, 28},
	{ &t6431_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3669_0_0_0;
extern Il2CppType t3669_1_0_0;
struct t1406;
extern TypeInfo t1406_TI;
extern CustomAttributesCache t1406__CustomAttributeCache;
TypeInfo t3669_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DispIdAttribute[]", "System.Runtime.InteropServices", t3669_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1406_TI, t3669_ITIs, t3669_VT, &EmptyCustomAttributesCache, &t3669_TI, &t3669_0_0_0, &t3669_1_0_0, t3669_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1406 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3670_TI;



// Metadata Definition System.Runtime.InteropServices.GCHandleType[]
static MethodInfo* t3670_MIs[] =
{
	NULL
};
extern MethodInfo m25513_MI;
extern MethodInfo m25514_MI;
extern MethodInfo m25515_MI;
extern MethodInfo m25516_MI;
extern MethodInfo m25517_MI;
extern MethodInfo m25518_MI;
extern MethodInfo m25512_MI;
extern MethodInfo m25520_MI;
extern MethodInfo m25521_MI;
static MethodInfo* t3670_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25513_MI,
	&m5907_MI,
	&m25514_MI,
	&m25515_MI,
	&m25516_MI,
	&m25517_MI,
	&m25518_MI,
	&m5908_MI,
	&m25512_MI,
	&m25520_MI,
	&m25521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6432_TI;
extern TypeInfo t6433_TI;
extern TypeInfo t6434_TI;
static TypeInfo* t3670_ITIs[] = 
{
	&t6432_TI,
	&t6433_TI,
	&t6434_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3670_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6432_TI, 21},
	{ &t6433_TI, 28},
	{ &t6434_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3670_0_0_0;
extern Il2CppType t3670_1_0_0;
#include "t1408.h"
extern TypeInfo t1408_TI;
extern CustomAttributesCache t1408__CustomAttributeCache;
TypeInfo t3670_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GCHandleType[]", "System.Runtime.InteropServices", t3670_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1408_TI, t3670_ITIs, t3670_VT, &EmptyCustomAttributesCache, &t44_TI, &t3670_0_0_0, &t3670_1_0_0, t3670_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3671_TI;



// Metadata Definition System.Runtime.InteropServices.InterfaceTypeAttribute[]
static MethodInfo* t3671_MIs[] =
{
	NULL
};
extern MethodInfo m25524_MI;
extern MethodInfo m25525_MI;
extern MethodInfo m25526_MI;
extern MethodInfo m25527_MI;
extern MethodInfo m25528_MI;
extern MethodInfo m25529_MI;
extern MethodInfo m25523_MI;
extern MethodInfo m25531_MI;
extern MethodInfo m25532_MI;
static MethodInfo* t3671_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25524_MI,
	&m5907_MI,
	&m25525_MI,
	&m25526_MI,
	&m25527_MI,
	&m25528_MI,
	&m25529_MI,
	&m5908_MI,
	&m25523_MI,
	&m25531_MI,
	&m25532_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6435_TI;
extern TypeInfo t6436_TI;
extern TypeInfo t6437_TI;
static TypeInfo* t3671_ITIs[] = 
{
	&t6435_TI,
	&t6436_TI,
	&t6437_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3671_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6435_TI, 21},
	{ &t6436_TI, 28},
	{ &t6437_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3671_0_0_0;
extern Il2CppType t3671_1_0_0;
struct t1409;
extern TypeInfo t1409_TI;
extern CustomAttributesCache t1409__CustomAttributeCache;
TypeInfo t3671_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InterfaceTypeAttribute[]", "System.Runtime.InteropServices", t3671_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1409_TI, t3671_ITIs, t3671_VT, &EmptyCustomAttributesCache, &t3671_TI, &t3671_0_0_0, &t3671_1_0_0, t3671_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1409 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3672_TI;



// Metadata Definition System.Runtime.InteropServices.PreserveSigAttribute[]
static MethodInfo* t3672_MIs[] =
{
	NULL
};
extern MethodInfo m25535_MI;
extern MethodInfo m25536_MI;
extern MethodInfo m25537_MI;
extern MethodInfo m25538_MI;
extern MethodInfo m25539_MI;
extern MethodInfo m25540_MI;
extern MethodInfo m25534_MI;
extern MethodInfo m25542_MI;
extern MethodInfo m25543_MI;
static MethodInfo* t3672_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25535_MI,
	&m5907_MI,
	&m25536_MI,
	&m25537_MI,
	&m25538_MI,
	&m25539_MI,
	&m25540_MI,
	&m5908_MI,
	&m25534_MI,
	&m25542_MI,
	&m25543_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6438_TI;
extern TypeInfo t6439_TI;
extern TypeInfo t6440_TI;
static TypeInfo* t3672_ITIs[] = 
{
	&t6438_TI,
	&t6439_TI,
	&t6440_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3672_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6438_TI, 21},
	{ &t6439_TI, 28},
	{ &t6440_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3672_0_0_0;
extern Il2CppType t3672_1_0_0;
struct t1412;
extern TypeInfo t1412_TI;
extern CustomAttributesCache t1412__CustomAttributeCache;
TypeInfo t3672_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PreserveSigAttribute[]", "System.Runtime.InteropServices", t3672_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1412_TI, t3672_ITIs, t3672_VT, &EmptyCustomAttributesCache, &t3672_TI, &t3672_0_0_0, &t3672_1_0_0, t3672_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1412 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3673_TI;



// Metadata Definition System.Runtime.InteropServices.TypeLibImportClassAttribute[]
static MethodInfo* t3673_MIs[] =
{
	NULL
};
extern MethodInfo m25546_MI;
extern MethodInfo m25547_MI;
extern MethodInfo m25548_MI;
extern MethodInfo m25549_MI;
extern MethodInfo m25550_MI;
extern MethodInfo m25551_MI;
extern MethodInfo m25545_MI;
extern MethodInfo m25553_MI;
extern MethodInfo m25554_MI;
static MethodInfo* t3673_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25546_MI,
	&m5907_MI,
	&m25547_MI,
	&m25548_MI,
	&m25549_MI,
	&m25550_MI,
	&m25551_MI,
	&m5908_MI,
	&m25545_MI,
	&m25553_MI,
	&m25554_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6441_TI;
extern TypeInfo t6442_TI;
extern TypeInfo t6443_TI;
static TypeInfo* t3673_ITIs[] = 
{
	&t6441_TI,
	&t6442_TI,
	&t6443_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3673_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6441_TI, 21},
	{ &t6442_TI, 28},
	{ &t6443_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3673_0_0_0;
extern Il2CppType t3673_1_0_0;
struct t1413;
extern TypeInfo t1413_TI;
extern CustomAttributesCache t1413__CustomAttributeCache;
TypeInfo t3673_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeLibImportClassAttribute[]", "System.Runtime.InteropServices", t3673_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1413_TI, t3673_ITIs, t3673_VT, &EmptyCustomAttributesCache, &t3673_TI, &t3673_0_0_0, &t3673_1_0_0, t3673_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1413 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3674_TI;



// Metadata Definition System.Runtime.InteropServices.TypeLibVersionAttribute[]
static MethodInfo* t3674_MIs[] =
{
	NULL
};
extern MethodInfo m25557_MI;
extern MethodInfo m25558_MI;
extern MethodInfo m25559_MI;
extern MethodInfo m25560_MI;
extern MethodInfo m25561_MI;
extern MethodInfo m25562_MI;
extern MethodInfo m25556_MI;
extern MethodInfo m25564_MI;
extern MethodInfo m25565_MI;
static MethodInfo* t3674_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25557_MI,
	&m5907_MI,
	&m25558_MI,
	&m25559_MI,
	&m25560_MI,
	&m25561_MI,
	&m25562_MI,
	&m5908_MI,
	&m25556_MI,
	&m25564_MI,
	&m25565_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6444_TI;
extern TypeInfo t6445_TI;
extern TypeInfo t6446_TI;
static TypeInfo* t3674_ITIs[] = 
{
	&t6444_TI,
	&t6445_TI,
	&t6446_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3674_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6444_TI, 21},
	{ &t6445_TI, 28},
	{ &t6446_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3674_0_0_0;
extern Il2CppType t3674_1_0_0;
struct t1414;
extern TypeInfo t1414_TI;
extern CustomAttributesCache t1414__CustomAttributeCache;
TypeInfo t3674_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeLibVersionAttribute[]", "System.Runtime.InteropServices", t3674_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1414_TI, t3674_ITIs, t3674_VT, &EmptyCustomAttributesCache, &t3674_TI, &t3674_0_0_0, &t3674_1_0_0, t3674_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1414 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3675_TI;



// Metadata Definition System.Runtime.InteropServices.UnmanagedType[]
static MethodInfo* t3675_MIs[] =
{
	NULL
};
extern MethodInfo m25568_MI;
extern MethodInfo m25569_MI;
extern MethodInfo m25570_MI;
extern MethodInfo m25571_MI;
extern MethodInfo m25572_MI;
extern MethodInfo m25573_MI;
extern MethodInfo m25567_MI;
extern MethodInfo m25575_MI;
extern MethodInfo m25576_MI;
static MethodInfo* t3675_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25568_MI,
	&m5907_MI,
	&m25569_MI,
	&m25570_MI,
	&m25571_MI,
	&m25572_MI,
	&m25573_MI,
	&m5908_MI,
	&m25567_MI,
	&m25575_MI,
	&m25576_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6447_TI;
extern TypeInfo t6448_TI;
extern TypeInfo t6449_TI;
static TypeInfo* t3675_ITIs[] = 
{
	&t6447_TI,
	&t6448_TI,
	&t6449_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3675_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6447_TI, 21},
	{ &t6448_TI, 28},
	{ &t6449_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3675_0_0_0;
extern Il2CppType t3675_1_0_0;
#include "t1156.h"
extern TypeInfo t1156_TI;
extern CustomAttributesCache t1156__CustomAttributeCache;
TypeInfo t3675_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnmanagedType[]", "System.Runtime.InteropServices", t3675_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1156_TI, t3675_ITIs, t3675_VT, &EmptyCustomAttributesCache, &t44_TI, &t3675_0_0_0, &t3675_1_0_0, t3675_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3676_TI;



// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute[]
static MethodInfo* t3676_MIs[] =
{
	NULL
};
extern MethodInfo m25579_MI;
extern MethodInfo m25580_MI;
extern MethodInfo m25581_MI;
extern MethodInfo m25582_MI;
extern MethodInfo m25583_MI;
extern MethodInfo m25584_MI;
extern MethodInfo m25578_MI;
extern MethodInfo m25586_MI;
extern MethodInfo m25587_MI;
extern MethodInfo m25590_MI;
extern MethodInfo m25591_MI;
extern MethodInfo m25592_MI;
extern MethodInfo m25593_MI;
extern MethodInfo m25594_MI;
extern MethodInfo m25595_MI;
extern MethodInfo m25589_MI;
extern MethodInfo m25597_MI;
extern MethodInfo m25598_MI;
extern MethodInfo m25601_MI;
extern MethodInfo m25602_MI;
extern MethodInfo m25603_MI;
extern MethodInfo m25604_MI;
extern MethodInfo m25605_MI;
extern MethodInfo m25606_MI;
extern MethodInfo m25600_MI;
extern MethodInfo m25608_MI;
extern MethodInfo m25609_MI;
extern MethodInfo m25612_MI;
extern MethodInfo m25613_MI;
extern MethodInfo m25614_MI;
extern MethodInfo m25615_MI;
extern MethodInfo m25616_MI;
extern MethodInfo m25617_MI;
extern MethodInfo m25611_MI;
extern MethodInfo m25619_MI;
extern MethodInfo m25620_MI;
static MethodInfo* t3676_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25579_MI,
	&m5907_MI,
	&m25580_MI,
	&m25581_MI,
	&m25582_MI,
	&m25583_MI,
	&m25584_MI,
	&m5908_MI,
	&m25578_MI,
	&m25586_MI,
	&m25587_MI,
	&m5905_MI,
	&m5906_MI,
	&m25590_MI,
	&m5907_MI,
	&m25591_MI,
	&m25592_MI,
	&m25593_MI,
	&m25594_MI,
	&m25595_MI,
	&m5908_MI,
	&m25589_MI,
	&m25597_MI,
	&m25598_MI,
	&m5905_MI,
	&m5906_MI,
	&m25601_MI,
	&m5907_MI,
	&m25602_MI,
	&m25603_MI,
	&m25604_MI,
	&m25605_MI,
	&m25606_MI,
	&m5908_MI,
	&m25600_MI,
	&m25608_MI,
	&m25609_MI,
	&m5905_MI,
	&m5906_MI,
	&m25612_MI,
	&m5907_MI,
	&m25613_MI,
	&m25614_MI,
	&m25615_MI,
	&m25616_MI,
	&m25617_MI,
	&m5908_MI,
	&m25611_MI,
	&m25619_MI,
	&m25620_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6450_TI;
extern TypeInfo t6451_TI;
extern TypeInfo t6452_TI;
extern TypeInfo t6453_TI;
extern TypeInfo t6454_TI;
extern TypeInfo t6455_TI;
extern TypeInfo t6456_TI;
extern TypeInfo t6457_TI;
extern TypeInfo t6458_TI;
extern TypeInfo t6459_TI;
extern TypeInfo t6460_TI;
extern TypeInfo t6461_TI;
static TypeInfo* t3676_ITIs[] = 
{
	&t6450_TI,
	&t6451_TI,
	&t6452_TI,
	&t6453_TI,
	&t6454_TI,
	&t6455_TI,
	&t6456_TI,
	&t6457_TI,
	&t6458_TI,
	&t6459_TI,
	&t6460_TI,
	&t6461_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3676_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6450_TI, 21},
	{ &t6451_TI, 28},
	{ &t6452_TI, 33},
	{ &t6453_TI, 34},
	{ &t6454_TI, 41},
	{ &t6455_TI, 46},
	{ &t6456_TI, 47},
	{ &t6457_TI, 54},
	{ &t6458_TI, 59},
	{ &t6459_TI, 60},
	{ &t6460_TI, 67},
	{ &t6461_TI, 72},
	{ &t5619_TI, 73},
	{ &t5620_TI, 80},
	{ &t5621_TI, 85},
	{ &t5622_TI, 86},
	{ &t5623_TI, 93},
	{ &t5624_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3676_0_0_0;
extern Il2CppType t3676_1_0_0;
struct t1422;
extern TypeInfo t1422_TI;
extern CustomAttributesCache t1422__CustomAttributeCache;
extern CustomAttributesCache t1422__CustomAttributeCache_m7774;
extern CustomAttributesCache t1422__CustomAttributeCache_m7775;
TypeInfo t3676_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UrlAttribute[]", "System.Runtime.Remoting.Activation", t3676_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1422_TI, t3676_ITIs, t3676_VT, &EmptyCustomAttributesCache, &t3676_TI, &t3676_0_0_0, &t3676_1_0_0, t3676_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1422 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3677_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute[]
static MethodInfo* t3677_MIs[] =
{
	NULL
};
static MethodInfo* t3677_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25590_MI,
	&m5907_MI,
	&m25591_MI,
	&m25592_MI,
	&m25593_MI,
	&m25594_MI,
	&m25595_MI,
	&m5908_MI,
	&m25589_MI,
	&m25597_MI,
	&m25598_MI,
	&m5905_MI,
	&m5906_MI,
	&m25601_MI,
	&m5907_MI,
	&m25602_MI,
	&m25603_MI,
	&m25604_MI,
	&m25605_MI,
	&m25606_MI,
	&m5908_MI,
	&m25600_MI,
	&m25608_MI,
	&m25609_MI,
	&m5905_MI,
	&m5906_MI,
	&m25612_MI,
	&m5907_MI,
	&m25613_MI,
	&m25614_MI,
	&m25615_MI,
	&m25616_MI,
	&m25617_MI,
	&m5908_MI,
	&m25611_MI,
	&m25619_MI,
	&m25620_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3677_ITIs[] = 
{
	&t6453_TI,
	&t6454_TI,
	&t6455_TI,
	&t6456_TI,
	&t6457_TI,
	&t6458_TI,
	&t6459_TI,
	&t6460_TI,
	&t6461_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3677_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6453_TI, 21},
	{ &t6454_TI, 28},
	{ &t6455_TI, 33},
	{ &t6456_TI, 34},
	{ &t6457_TI, 41},
	{ &t6458_TI, 46},
	{ &t6459_TI, 47},
	{ &t6460_TI, 54},
	{ &t6461_TI, 59},
	{ &t5619_TI, 60},
	{ &t5620_TI, 67},
	{ &t5621_TI, 72},
	{ &t5622_TI, 73},
	{ &t5623_TI, 80},
	{ &t5624_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3677_0_0_0;
extern Il2CppType t3677_1_0_0;
struct t1423;
extern TypeInfo t1423_TI;
extern CustomAttributesCache t1423__CustomAttributeCache;
TypeInfo t3677_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ContextAttribute[]", "System.Runtime.Remoting.Contexts", t3677_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1423_TI, t3677_ITIs, t3677_VT, &EmptyCustomAttributesCache, &t3677_TI, &t3677_0_0_0, &t3677_1_0_0, t3677_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1423 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1474_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute[]
static MethodInfo* t1474_MIs[] =
{
	NULL
};
static MethodInfo* t1474_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25601_MI,
	&m5907_MI,
	&m25602_MI,
	&m25603_MI,
	&m25604_MI,
	&m25605_MI,
	&m25606_MI,
	&m5908_MI,
	&m25600_MI,
	&m25608_MI,
	&m25609_MI,
};
static TypeInfo* t1474_ITIs[] = 
{
	&t6456_TI,
	&t6457_TI,
	&t6458_TI,
};
static Il2CppInterfaceOffsetPair t1474_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6456_TI, 21},
	{ &t6457_TI, 28},
	{ &t6458_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1474_0_0_0;
extern Il2CppType t1474_1_0_0;
struct t2040;
extern TypeInfo t2040_TI;
extern CustomAttributesCache t2040__CustomAttributeCache;
TypeInfo t1474_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContextAttribute[]", "System.Runtime.Remoting.Contexts", t1474_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2040_TI, t1474_ITIs, t1474_VT, &EmptyCustomAttributesCache, &t1474_TI, &t1474_0_0_0, &t1474_1_0_0, t1474_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3678_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty[]
static MethodInfo* t3678_MIs[] =
{
	NULL
};
static MethodInfo* t3678_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25612_MI,
	&m5907_MI,
	&m25613_MI,
	&m25614_MI,
	&m25615_MI,
	&m25616_MI,
	&m25617_MI,
	&m5908_MI,
	&m25611_MI,
	&m25619_MI,
	&m25620_MI,
};
static TypeInfo* t3678_ITIs[] = 
{
	&t6459_TI,
	&t6460_TI,
	&t6461_TI,
};
static Il2CppInterfaceOffsetPair t3678_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6459_TI, 21},
	{ &t6460_TI, 28},
	{ &t6461_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3678_0_0_0;
extern Il2CppType t3678_1_0_0;
struct t1433;
extern TypeInfo t1433_TI;
extern CustomAttributesCache t1433__CustomAttributeCache;
TypeInfo t3678_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContextProperty[]", "System.Runtime.Remoting.Contexts", t3678_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1433_TI, t3678_ITIs, t3678_VT, &EmptyCustomAttributesCache, &t3678_TI, &t3678_0_0_0, &t3678_1_0_0, t3678_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3679_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute[]
static MethodInfo* t3679_MIs[] =
{
	NULL
};
extern MethodInfo m25623_MI;
extern MethodInfo m25624_MI;
extern MethodInfo m25625_MI;
extern MethodInfo m25626_MI;
extern MethodInfo m25627_MI;
extern MethodInfo m25628_MI;
extern MethodInfo m25622_MI;
extern MethodInfo m25630_MI;
extern MethodInfo m25631_MI;
extern MethodInfo m25634_MI;
extern MethodInfo m25635_MI;
extern MethodInfo m25636_MI;
extern MethodInfo m25637_MI;
extern MethodInfo m25638_MI;
extern MethodInfo m25639_MI;
extern MethodInfo m25633_MI;
extern MethodInfo m25641_MI;
extern MethodInfo m25642_MI;
extern MethodInfo m25645_MI;
extern MethodInfo m25646_MI;
extern MethodInfo m25647_MI;
extern MethodInfo m25648_MI;
extern MethodInfo m25649_MI;
extern MethodInfo m25650_MI;
extern MethodInfo m25644_MI;
extern MethodInfo m25652_MI;
extern MethodInfo m25653_MI;
static MethodInfo* t3679_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25623_MI,
	&m5907_MI,
	&m25624_MI,
	&m25625_MI,
	&m25626_MI,
	&m25627_MI,
	&m25628_MI,
	&m5908_MI,
	&m25622_MI,
	&m25630_MI,
	&m25631_MI,
	&m5905_MI,
	&m5906_MI,
	&m25634_MI,
	&m5907_MI,
	&m25635_MI,
	&m25636_MI,
	&m25637_MI,
	&m25638_MI,
	&m25639_MI,
	&m5908_MI,
	&m25633_MI,
	&m25641_MI,
	&m25642_MI,
	&m5905_MI,
	&m5906_MI,
	&m25645_MI,
	&m5907_MI,
	&m25646_MI,
	&m25647_MI,
	&m25648_MI,
	&m25649_MI,
	&m25650_MI,
	&m5908_MI,
	&m25644_MI,
	&m25652_MI,
	&m25653_MI,
	&m5905_MI,
	&m5906_MI,
	&m25590_MI,
	&m5907_MI,
	&m25591_MI,
	&m25592_MI,
	&m25593_MI,
	&m25594_MI,
	&m25595_MI,
	&m5908_MI,
	&m25589_MI,
	&m25597_MI,
	&m25598_MI,
	&m5905_MI,
	&m5906_MI,
	&m25601_MI,
	&m5907_MI,
	&m25602_MI,
	&m25603_MI,
	&m25604_MI,
	&m25605_MI,
	&m25606_MI,
	&m5908_MI,
	&m25600_MI,
	&m25608_MI,
	&m25609_MI,
	&m5905_MI,
	&m5906_MI,
	&m25612_MI,
	&m5907_MI,
	&m25613_MI,
	&m25614_MI,
	&m25615_MI,
	&m25616_MI,
	&m25617_MI,
	&m5908_MI,
	&m25611_MI,
	&m25619_MI,
	&m25620_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6462_TI;
extern TypeInfo t6463_TI;
extern TypeInfo t6464_TI;
extern TypeInfo t6465_TI;
extern TypeInfo t6466_TI;
extern TypeInfo t6467_TI;
extern TypeInfo t6468_TI;
extern TypeInfo t6469_TI;
extern TypeInfo t6470_TI;
static TypeInfo* t3679_ITIs[] = 
{
	&t6462_TI,
	&t6463_TI,
	&t6464_TI,
	&t6465_TI,
	&t6466_TI,
	&t6467_TI,
	&t6468_TI,
	&t6469_TI,
	&t6470_TI,
	&t6453_TI,
	&t6454_TI,
	&t6455_TI,
	&t6456_TI,
	&t6457_TI,
	&t6458_TI,
	&t6459_TI,
	&t6460_TI,
	&t6461_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3679_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6462_TI, 21},
	{ &t6463_TI, 28},
	{ &t6464_TI, 33},
	{ &t6465_TI, 34},
	{ &t6466_TI, 41},
	{ &t6467_TI, 46},
	{ &t6468_TI, 47},
	{ &t6469_TI, 54},
	{ &t6470_TI, 59},
	{ &t6453_TI, 60},
	{ &t6454_TI, 67},
	{ &t6455_TI, 72},
	{ &t6456_TI, 73},
	{ &t6457_TI, 80},
	{ &t6458_TI, 85},
	{ &t6459_TI, 86},
	{ &t6460_TI, 93},
	{ &t6461_TI, 98},
	{ &t5619_TI, 99},
	{ &t5620_TI, 106},
	{ &t5621_TI, 111},
	{ &t5622_TI, 112},
	{ &t5623_TI, 119},
	{ &t5624_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3679_0_0_0;
extern Il2CppType t3679_1_0_0;
struct t1434;
extern TypeInfo t1434_TI;
extern CustomAttributesCache t1434__CustomAttributeCache;
extern CustomAttributesCache t1434__CustomAttributeCache_m7809;
extern CustomAttributesCache t1434__CustomAttributeCache_m7810;
TypeInfo t3679_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SynchronizationAttribute[]", "System.Runtime.Remoting.Contexts", t3679_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1434_TI, t3679_ITIs, t3679_VT, &EmptyCustomAttributesCache, &t3679_TI, &t3679_0_0_0, &t3679_1_0_0, t3679_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1434 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3680_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink[]
static MethodInfo* t3680_MIs[] =
{
	NULL
};
static MethodInfo* t3680_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25634_MI,
	&m5907_MI,
	&m25635_MI,
	&m25636_MI,
	&m25637_MI,
	&m25638_MI,
	&m25639_MI,
	&m5908_MI,
	&m25633_MI,
	&m25641_MI,
	&m25642_MI,
};
static TypeInfo* t3680_ITIs[] = 
{
	&t6465_TI,
	&t6466_TI,
	&t6467_TI,
};
static Il2CppInterfaceOffsetPair t3680_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6465_TI, 21},
	{ &t6466_TI, 28},
	{ &t6467_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3680_0_0_0;
extern Il2CppType t3680_1_0_0;
struct t2044;
extern TypeInfo t2044_TI;
extern CustomAttributesCache t2044__CustomAttributeCache;
TypeInfo t3680_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContributeClientContextSink[]", "System.Runtime.Remoting.Contexts", t3680_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2044_TI, t3680_ITIs, t3680_VT, &EmptyCustomAttributesCache, &t3680_TI, &t3680_0_0_0, &t3680_1_0_0, t3680_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3681_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink[]
static MethodInfo* t3681_MIs[] =
{
	NULL
};
static MethodInfo* t3681_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25645_MI,
	&m5907_MI,
	&m25646_MI,
	&m25647_MI,
	&m25648_MI,
	&m25649_MI,
	&m25650_MI,
	&m5908_MI,
	&m25644_MI,
	&m25652_MI,
	&m25653_MI,
};
static TypeInfo* t3681_ITIs[] = 
{
	&t6468_TI,
	&t6469_TI,
	&t6470_TI,
};
static Il2CppInterfaceOffsetPair t3681_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6468_TI, 21},
	{ &t6469_TI, 28},
	{ &t6470_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3681_0_0_0;
extern Il2CppType t3681_1_0_0;
struct t2045;
extern TypeInfo t2045_TI;
extern CustomAttributesCache t2045__CustomAttributeCache;
TypeInfo t3681_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContributeServerContextSink[]", "System.Runtime.Remoting.Contexts", t3681_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2045_TI, t3681_ITIs, t3681_VT, &EmptyCustomAttributesCache, &t3681_TI, &t3681_0_0_0, &t3681_1_0_0, t3681_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
