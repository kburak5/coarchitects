﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1224;
struct t7;
struct t1219;
struct t292;

 void m6444 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6445 (t29 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6446 (t29 * __this, t1219 * p0, bool p1, t7* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6447 (t29 * __this, t292 * p0, t1219 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
