﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1186;
struct t1186_marshaled;
#include "t1120.h"

 void m6121 (t1186 * __this, int32_t p0, uint8_t* p1, uint8_t* p2, uint8_t* p3, uint8_t* p4, uint8_t* p5, bool p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t1186_marshal(const t1186& unmarshaled, t1186_marshaled& marshaled);
void t1186_marshal_back(const t1186_marshaled& marshaled, t1186& unmarshaled);
void t1186_marshal_cleanup(t1186_marshaled& marshaled);
