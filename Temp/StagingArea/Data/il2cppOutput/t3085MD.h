﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3085;
struct t29;
struct t20;
#include "t498.h"

 void m17045 (t3085 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17046 (t3085 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17047 (t3085 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17048 (t3085 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17049 (t3085 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
