﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3496;
struct t29;
#include "t1648.h"

 void m19424 (t3496 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19425 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19426 (t3496 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19427 (t3496 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3496 * m19428 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
