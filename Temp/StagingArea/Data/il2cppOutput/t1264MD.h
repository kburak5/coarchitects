﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1264;
struct t29;
struct t731;

 void m6571 (t1264 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6572 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6573 (t1264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6574 (t1264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
