﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t528;
struct t29;
struct t3029;
struct t20;
struct t136;
struct t3030;
struct t3031;
struct t3032;
struct t530;
struct t3033;
struct t3034;
#include "t381.h"
#include "t3035.h"

 void m16529 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2904 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16530 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m16531 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16532 (t528 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16533 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16534 (t528 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16535 (t528 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16536 (t528 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16537 (t528 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16538 (t528 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16539 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16540 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16541 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16542 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16543 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16544 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16545 (t528 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16546 (t528 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16547 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16548 (t528 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16549 (t528 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16550 (t528 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3032 * m16551 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16552 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16553 (t528 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16554 (t528 * __this, t530* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16555 (t528 * __this, t3033 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16556 (t29 * __this, t3033 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16557 (t528 * __this, int32_t p0, int32_t p1, t3033 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3035  m16558 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16559 (t528 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16560 (t528 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16561 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16562 (t528 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16563 (t528 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16564 (t528 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16565 (t528 * __this, t3033 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16566 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16567 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16568 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16569 (t528 * __this, t3034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t530* m16570 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16571 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16572 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16573 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16574 (t528 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16575 (t528 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16576 (t528 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
