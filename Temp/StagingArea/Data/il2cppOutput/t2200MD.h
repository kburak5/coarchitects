﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2200;
struct t29;
struct t20;

 void m10766 (t2200 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m10767 (t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m10768 (t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m10769 (t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m10770 (t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
