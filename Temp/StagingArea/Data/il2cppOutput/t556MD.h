﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t556;
struct t29;
struct t557;
struct t316;
struct t353;

 void m2789 (t556 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2790 (t556 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2791 (t29 * __this, t353 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
