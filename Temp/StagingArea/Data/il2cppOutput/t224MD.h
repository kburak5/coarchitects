﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t224;
struct t29;
struct t7;
#include "t23.h"
#include "t224.h"
#include "t329.h"

 void m1855 (t224 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2381 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2382 (t224 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1856 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1858 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1844 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1857 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2383 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2384 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1845 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2385 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1865 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2386 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2387 (t224 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1864 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2388 (t224 * __this, t224  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2389 (t224 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2390 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2391 (t224 * __this, t224  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2392 (t29 * __this, t224  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2393 (t29 * __this, t224 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2394 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2395 (t29 * __this, t224  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2396 (t29 * __this, t224 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2397 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2398 (t29 * __this, t329 * p0, t224 * p1, float* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2399 (t29 * __this, t329 * p0, t224 * p1, float* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2400 (t224 * __this, t329  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2401 (t224 * __this, t329  p0, float* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2402 (t29 * __this, t224 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2403 (t29 * __this, t224 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2404 (t224 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2405 (t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2406 (t224 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2407 (t29 * __this, t224  p0, t224  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1850 (t29 * __this, t224  p0, t224  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
