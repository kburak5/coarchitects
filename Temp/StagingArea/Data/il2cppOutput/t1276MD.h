﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1276;
struct t29;
struct t719;
struct t20;
struct t136;

 void m6697 (t1276 * __this, t719 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6698 (t1276 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6699 (t1276 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6700 (t1276 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6701 (t1276 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6702 (t1276 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
