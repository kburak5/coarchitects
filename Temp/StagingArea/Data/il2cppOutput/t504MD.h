﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t504;
struct t29;
struct t25;

 void m2559 (t504 * __this, t25 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2560 (t504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2561 (t504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
