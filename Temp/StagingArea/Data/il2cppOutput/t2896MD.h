﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2896;
struct t29;
struct t20;
#include "t2893.h"

 void m15824 (t2896 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15825 (t2896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15826 (t2896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15827 (t2896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2893  m15828 (t2896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
