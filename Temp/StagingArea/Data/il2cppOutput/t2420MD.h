﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2420;
struct t29;
struct t7;

 void m12525 (t2420 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12526 (t2420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12527 (t2420 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12528 (t2420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12529 (t2420 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m12530 (t2420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
