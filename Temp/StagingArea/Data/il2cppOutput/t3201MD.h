﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3201;
struct t29;

 void m17809 (t3201 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17810 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17811 (t3201 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17812 (t3201 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3201 * m17813 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
