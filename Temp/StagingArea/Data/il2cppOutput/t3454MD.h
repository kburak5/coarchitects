﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3454;
struct t29;
struct t20;
#include "t1523.h"

 void m19140 (t3454 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19141 (t3454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19142 (t3454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19143 (t3454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19144 (t3454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
