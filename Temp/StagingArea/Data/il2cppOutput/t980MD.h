﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t980;
struct t29;
struct t972;
struct t66;
struct t67;
#include "t35.h"
#include "t977.h"

 void m5080 (t980 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5081 (t980 * __this, t972 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5082 (t980 * __this, t972 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5083 (t980 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
