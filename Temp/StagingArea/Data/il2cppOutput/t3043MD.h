﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3043;
struct t29;
struct t387;
struct t20;
struct t136;
struct t531;
struct t3041;
#include "t380.h"

 void m16725 (t3043 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16726 (t3043 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16727 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16728 (t3043 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16729 (t3043 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16730 (t3043 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16731 (t3043 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16732 (t3043 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16733 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16734 (t3043 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16735 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16736 (t3043 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16737 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16738 (t3043 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16739 (t3043 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16740 (t3043 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16741 (t3043 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16742 (t3043 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16743 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16744 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16745 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16746 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16747 (t3043 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16748 (t3043 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16749 (t3043 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16750 (t3043 * __this, t531* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m16751 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16752 (t3043 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16753 (t3043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16754 (t3043 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
