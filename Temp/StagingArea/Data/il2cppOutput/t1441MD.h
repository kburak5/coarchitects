﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1441;
struct t316;
struct t1449;
struct t636;
struct t7;
struct t29;
struct t295;

 t316* m7909 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1449 * m7910 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7911 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7912 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7913 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7914 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7915 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7916 (t1441 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7917 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7918 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7919 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7920 (t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
