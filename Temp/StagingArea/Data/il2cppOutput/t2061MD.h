﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2061;
struct t29;
struct t1567;
struct t3463;
struct t20;
struct t136;
struct t3464;
struct t3465;
struct t3466;
struct t3462;
struct t3467;
struct t3468;
#include "t3469.h"

#include "t294MD.h"
#define m19180(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m10242(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m19181(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m19182(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m19183(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m19184(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m19185(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m19186(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m19187(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m19188(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m19189(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m19190(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m19191(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m19192(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m19193(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m19194(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m19195(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m19196(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m19197(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m19198(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m19199(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m19200(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m19201(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m19202(__this, method) (t3466 *)m10583_gshared((t294 *)__this, method)
#define m19203(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m19204(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m19205(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m19206(__this, p0, method) (t1567 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m19207(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m19208(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t3469  m19209 (t2061 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m19210(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m19211(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m19212(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m19213(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m19214(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m19215(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m19216(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m19217(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m19218(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m19219(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m19220(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m19221(__this, method) (t3462*)m10619_gshared((t294 *)__this, method)
#define m19222(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m19223(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m19224(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m19225(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m19226(__this, p0, method) (t1567 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m19227(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
