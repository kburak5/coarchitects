﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t637;
struct t42;
struct t296;
struct t7;
struct t1352;
struct t316;
#include "t1353.h"

 void m7694 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7695 (t637 * __this, t1352 * p0, t42 * p1, t296 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7696 (t637 * __this, t637 * p0, t296 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7697 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m2940 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7698 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7699 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7700 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7701 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7702 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t296 * m7703 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7704 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7705 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7706 (t637 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7707 (t637 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7708 (t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
