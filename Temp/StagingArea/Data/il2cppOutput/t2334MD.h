﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2334;
struct t29;
struct t20;
#include "t111.h"

 void m12003 (t2334 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12004 (t2334 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12005 (t2334 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12006 (t2334 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12007 (t2334 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
