﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1695;
struct t1695_marshaled;

void t1695_marshal(const t1695& unmarshaled, t1695_marshaled& marshaled);
void t1695_marshal_back(const t1695_marshaled& marshaled, t1695& unmarshaled);
void t1695_marshal_cleanup(t1695_marshaled& marshaled);
