﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t490;
struct t29;
struct t42;
struct t296;
struct t637;

 void m2881 (t490 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5289 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t490 * m5290 (t29 * __this, t296 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t490 * m5291 (t29 * __this, t296 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2883 (t490 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5292 (t29 * __this, t637 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5293 (t29 * __this, t296 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5294 (t29 * __this, t296 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5295 (t29 * __this, t637 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2882 (t490 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
