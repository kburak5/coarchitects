﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1085;
struct t1085_marshaled;

void t1085_marshal(const t1085& unmarshaled, t1085_marshaled& marshaled);
void t1085_marshal_back(const t1085_marshaled& marshaled, t1085& unmarshaled);
void t1085_marshal_cleanup(t1085_marshaled& marshaled);
