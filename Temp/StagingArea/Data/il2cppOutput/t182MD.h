﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t182;
struct t156;

 void m693 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m694 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m695 (t182 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m696 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m697 (t182 * __this, t156 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m698 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m699 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m700 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m701 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m702 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m703 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m704 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m705 (t182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
