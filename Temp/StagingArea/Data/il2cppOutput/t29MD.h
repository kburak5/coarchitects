﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t29;
struct t42;
struct t7;

 void m1331 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1321 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4286 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m46 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1322 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m1430 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5283 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1332 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2868 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5284 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
