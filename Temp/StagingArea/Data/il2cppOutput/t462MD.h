﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t462;
struct t463;
struct t461;
struct t466;
#include "t465.h"

 void m2126 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2127 (t29 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2128 (t29 * __this, t463 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t463 * m2129 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2130 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2131 (t29 * __this, t461 * p0, int32_t p1, t463 * p2, int32_t p3, float p4, float p5, t466 * p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
