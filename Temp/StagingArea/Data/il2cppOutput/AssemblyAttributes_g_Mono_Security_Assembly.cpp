﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo t434_TI;
#include "t434.h"
#include "t434MD.h"
extern MethodInfo m2052_MI;
extern TypeInfo t430_TI;
#include "t430.h"
#include "t430MD.h"
extern MethodInfo m2048_MI;
extern TypeInfo t433_TI;
#include "t433.h"
#include "t433MD.h"
extern MethodInfo m2051_MI;
extern TypeInfo t429_TI;
#include "t429.h"
#include "t429MD.h"
extern MethodInfo m2047_MI;
extern TypeInfo t707_TI;
#include "t707.h"
#include "t707MD.h"
extern MethodInfo m3062_MI;
extern TypeInfo t432_TI;
#include "t432.h"
#include "t432MD.h"
extern MethodInfo m2050_MI;
extern TypeInfo t437_TI;
#include "t437.h"
#include "t437MD.h"
extern MethodInfo m2055_MI;
extern TypeInfo t46_TI;
#include "t46.h"
#include "t46MD.h"
extern MethodInfo m71_MI;
extern MethodInfo m72_MI;
extern TypeInfo t685_TI;
#include "t685.h"
#include "t685MD.h"
extern MethodInfo m3041_MI;
extern TypeInfo t710_TI;
#include "t710.h"
#include "t710MD.h"
extern MethodInfo m3065_MI;
extern TypeInfo t711_TI;
#include "t711.h"
#include "t711MD.h"
extern MethodInfo m3066_MI;
extern TypeInfo t706_TI;
#include "t706.h"
#include "t706MD.h"
extern MethodInfo m3061_MI;
void g_Mono_Security_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t434 * tmp;
		tmp = (t434 *)il2cpp_codegen_object_new (&t434_TI);
		m2052(tmp, il2cpp_codegen_string_new_wrapper("(c) 2003-2004 Various Authors"), &m2052_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t430 * tmp;
		tmp = (t430 *)il2cpp_codegen_object_new (&t430_TI);
		m2048(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), &m2048_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t433 * tmp;
		tmp = (t433 *)il2cpp_codegen_object_new (&t433_TI);
		m2051(tmp, il2cpp_codegen_string_new_wrapper("MONO CLI"), &m2051_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t429 * tmp;
		tmp = (t429 *)il2cpp_codegen_object_new (&t429_TI);
		m2047(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), &m2047_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, true, &m3062_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		t432 * tmp;
		tmp = (t432 *)il2cpp_codegen_object_new (&t432_TI);
		m2050(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), &m2050_MI);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, false, &m2055_MI);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		t46 * tmp;
		tmp = (t46 *)il2cpp_codegen_object_new (&t46_TI);
		m71(tmp, &m71_MI);
		m72(tmp, true, &m72_MI);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("System, PublicKey=00240000048000009400000006020000002400005253413100040000010001008D56C76F9E8649383049F383C44BE0EC204181822A6C31CF5EB7EF486944D032188EA1D3920763712CCB12D75FB77E9811149E6148E5D32FBAAB37611C1878DDC19E20EF135D0CB2CFF2BFEC3D115810C3D9069638FE4BE215DBF795861920E5AB6F7DB2E2CEEF136AC23D5DD2BF031700AEC232F6C6B1C785B4305C123B37AB"), &m3041_MI);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		t710 * tmp;
		tmp = (t710 *)il2cpp_codegen_object_new (&t710_TI);
		m3065(tmp, il2cpp_codegen_string_new_wrapper("../mono.pub"), &m3065_MI);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		t711 * tmp;
		tmp = (t711 *)il2cpp_codegen_object_new (&t711_TI);
		m3066(tmp, true, &m3066_MI);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		t706 * tmp;
		tmp = (t706 *)il2cpp_codegen_object_new (&t706_TI);
		m3061(tmp, il2cpp_codegen_string_new_wrapper("en-US"), &m3061_MI);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_Mono_Security_Assembly__CustomAttributeCache = {
12,
NULL,
&g_Mono_Security_Assembly_CustomAttributesCacheGenerator
};
