﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t155;
struct t156;
struct t2;
struct t3;
struct t159;
struct t162;
struct t163;
struct t24;
struct t34;
struct t25;
#include "t132.h"
#include "t140.h"
#include "t17.h"
#include "t164.h"

 void m440 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m441 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m442 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m443 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m444 (t155 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m445 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m446 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m447 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m448 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m449 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m450 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m451 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m452 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m453 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3 * m454 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m455 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t159 * m456 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m457 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m458 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m459 (t155 * __this, t156 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m460 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m461 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m462 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m463 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m464 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m465 (t155 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m466 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m467 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m468 (t155 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m469 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m470 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m471 (t155 * __this, t17  p0, t24 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m472 (t155 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m473 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m474 (t155 * __this, t132  p0, float p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m475 (t155 * __this, t132  p0, float p1, bool p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m476 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m477 (t155 * __this, float p0, float p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m478 (t155 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m479 (t155 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m480 (t155 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m481 (t155 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m482 (t155 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m483 (t155 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m484 (t29 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m485 (t29 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m486 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m487 (t155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
