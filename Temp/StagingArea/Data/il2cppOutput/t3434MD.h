﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3434;
struct t29;
struct t20;
#include "t1491.h"

 void m19040 (t3434 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19041 (t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19042 (t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19043 (t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m19044 (t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
