﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2117;
struct t29;
struct t557;
struct t2121;
struct t316;

#include "t2119MD.h"
#define m10329(__this, p0, p1, method) (void)m10321_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
#define m10330(__this, p0, method) (void)m10322_gshared((t2119 *)__this, (t2120 *)p0, method)
#define m10331(__this, p0, method) (void)m10323_gshared((t2119 *)__this, (t316*)p0, method)
#define m10332(__this, p0, p1, method) (bool)m10324_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
