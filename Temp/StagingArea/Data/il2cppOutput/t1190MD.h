﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1190;
struct t633;
struct t1174;
struct t1177;
struct t7;
struct t1183;
struct t1192;
struct t1193;
#include "t1189.h"
#include "t1120.h"
#include "t1186.h"

 void m6123 (t1190 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6124 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6125 (t1190 * __this, t633 * p0, t1174 ** p1, uint8_t** p2, uint8_t** p3, t1174 ** p4, uint8_t** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m6126 (t29 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6127 (t1190 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6128 (t1190 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6129 (t1190 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6130 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1177 * m6131 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1177 * m6132 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t1183* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1177 * m6133 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1177 * m6134 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t1183* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6135 (t1190 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6136 (t1190 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6137 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6138 (t1190 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6139 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6140 (t1190 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1192 * m6141 (t1190 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1192 * m6142 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6143 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t1193 * p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6144 (t1190 * __this, int32_t p0, int32_t p1, t1193 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6145 (t1190 * __this, int32_t p0, t1193 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6146 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6147 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, bool* p6, bool* p7, bool p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6148 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6149 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6150 (t1190 * __this, uint8_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6151 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6152 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, bool* p6, bool* p7, bool p8, bool p9, t1186 * p10, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6153 (t1190 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6154 (t1190 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6155 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6156 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, bool p4, t1186 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6157 (t1190 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6158 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6159 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, bool* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6160 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6161 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6162 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6163 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, uint8_t* p3, uint16_t p4, int32_t p5, bool p6, t1186 * p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6164 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, uint8_t* p4, t1186 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6165 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6166 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6167 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6168 (t1190 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, uint8_t* p4, int32_t p5, bool p6, t1186 * p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6169 (t1190 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, uint8_t* p4, t1186 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6170 (t1190 * __this, t7* p0, int32_t* p1, int32_t p2, int32_t p3, uint8_t* p4, bool p5, t1186 * p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6171 (t1190 * __this, t7* p0, int32_t* p1, int32_t p2, int32_t p3, uint8_t* p4, bool p5, int32_t p6, t1177 ** p7, t1186 * p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6172 (t1190 * __this, int32_t p0, uint8_t* p1, int32_t p2, int32_t p3, uint8_t* p4, int32_t p5, bool p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6173 (t1190 * __this, t7* p0, int32_t* p1, int32_t p2, int32_t p3, int32_t p4, uint8_t* p5, bool p6, t1186 * p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6174 (t1190 * __this, t7* p0, int32_t* p1, int32_t p2, int32_t p3, int32_t p4, uint8_t* p5, bool p6, int32_t p7, t1177 ** p8, t1186 * p9, MethodInfo* method) IL2CPP_METHOD_ATTR;
