﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1541;
struct t781;
#include "t35.h"

 void m8319 (t1541 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8320 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8321 (t1541 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8322 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m8323 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m8324 (t29 * __this, t35 p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8325 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8326 (t1541 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8327 (t1541 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8328 (t1541 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
