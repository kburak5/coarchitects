﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1570;
struct t29;
struct t1571;
struct t7;
struct t42;
struct t733;
#include "t735.h"

 void m8518 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8519 (t1570 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8520 (t1570 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8521 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8522 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8523 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m8524 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8525 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8526 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8527 (t1570 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8528 (t1570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
