﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1587;
struct t7;
struct t1575;
struct t29;

 void m8598 (t1587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8599 (t1587 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8600 (t1587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1575 * m8601 (t1587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8602 (t1587 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8603 (t1587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
