﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2639_TI;

#include "t139.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Selectable>
extern MethodInfo m27583_MI;
static PropertyInfo t2639____Current_PropertyInfo = 
{
	&t2639_TI, "Current", &m27583_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2639_PIs[] =
{
	&t2639____Current_PropertyInfo,
	NULL
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27583_GM;
MethodInfo m27583_MI = 
{
	"get_Current", NULL, &t2639_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27583_GM};
static MethodInfo* t2639_MIs[] =
{
	&m27583_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t2639_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2639_0_0_0;
extern Il2CppType t2639_1_0_0;
struct t2639;
extern Il2CppGenericClass t2639_GC;
TypeInfo t2639_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2639_MIs, t2639_PIs, NULL, NULL, NULL, NULL, NULL, &t2639_TI, t2639_ITIs, NULL, &EmptyCustomAttributesCache, &t2639_TI, &t2639_0_0_0, &t2639_1_0_0, NULL, &t2639_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2406.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2406_TI;
#include "t2406MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t139_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m12440_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20695_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20695(__this, p0, method) (t139 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable>
extern Il2CppType t20_0_0_1;
FieldInfo t2406_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2406_TI, offsetof(t2406, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2406_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2406_TI, offsetof(t2406, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2406_FIs[] =
{
	&t2406_f0_FieldInfo,
	&t2406_f1_FieldInfo,
	NULL
};
extern MethodInfo m12437_MI;
static PropertyInfo t2406____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2406_TI, "System.Collections.IEnumerator.Current", &m12437_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2406____Current_PropertyInfo = 
{
	&t2406_TI, "Current", &m12440_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2406_PIs[] =
{
	&t2406____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2406____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2406_m12436_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12436_GM;
MethodInfo m12436_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2406_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2406_m12436_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12436_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12437_GM;
MethodInfo m12437_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2406_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12437_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12438_GM;
MethodInfo m12438_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2406_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12438_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12439_GM;
MethodInfo m12439_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2406_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12439_GM};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12440_GM;
MethodInfo m12440_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2406_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12440_GM};
static MethodInfo* t2406_MIs[] =
{
	&m12436_MI,
	&m12437_MI,
	&m12438_MI,
	&m12439_MI,
	&m12440_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m12439_MI;
extern MethodInfo m12438_MI;
static MethodInfo* t2406_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12437_MI,
	&m12439_MI,
	&m12438_MI,
	&m12440_MI,
};
static TypeInfo* t2406_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2639_TI,
};
static Il2CppInterfaceOffsetPair t2406_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2639_TI, 7},
};
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2406_RGCTXData[3] = 
{
	&m12440_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m20695_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2406_0_0_0;
extern Il2CppType t2406_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2406_GC;
extern TypeInfo t20_TI;
TypeInfo t2406_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2406_MIs, t2406_PIs, t2406_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2406_TI, t2406_ITIs, t2406_VT, &EmptyCustomAttributesCache, &t2406_TI, &t2406_0_0_0, &t2406_1_0_0, t2406_IOs, &t2406_GC, NULL, NULL, NULL, t2406_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2406)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2646_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Selectable>
extern MethodInfo m27584_MI;
extern MethodInfo m27585_MI;
static PropertyInfo t2646____Item_PropertyInfo = 
{
	&t2646_TI, "Item", &m27584_MI, &m27585_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2646_PIs[] =
{
	&t2646____Item_PropertyInfo,
	NULL
};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2646_m27586_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27586_GM;
MethodInfo m27586_MI = 
{
	"IndexOf", NULL, &t2646_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2646_m27586_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27586_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2646_m27587_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27587_GM;
MethodInfo m27587_MI = 
{
	"Insert", NULL, &t2646_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2646_m27587_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27587_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2646_m27588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27588_GM;
MethodInfo m27588_MI = 
{
	"RemoveAt", NULL, &t2646_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2646_m27588_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27588_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2646_m27584_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27584_GM;
MethodInfo m27584_MI = 
{
	"get_Item", NULL, &t2646_TI, &t139_0_0_0, RuntimeInvoker_t29_t44, t2646_m27584_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27584_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2646_m27585_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27585_GM;
MethodInfo m27585_MI = 
{
	"set_Item", NULL, &t2646_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2646_m27585_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27585_GM};
static MethodInfo* t2646_MIs[] =
{
	&m27586_MI,
	&m27587_MI,
	&m27588_MI,
	&m27584_MI,
	&m27585_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t2640_TI;
extern TypeInfo t2641_TI;
static TypeInfo* t2646_ITIs[] = 
{
	&t603_TI,
	&t2640_TI,
	&t2641_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2646_0_0_0;
extern Il2CppType t2646_1_0_0;
struct t2646;
extern Il2CppGenericClass t2646_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2646_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2646_MIs, t2646_PIs, NULL, NULL, NULL, NULL, NULL, &t2646_TI, t2646_ITIs, NULL, &t1908__CustomAttributeCache, &t2646_TI, &t2646_0_0_0, &t2646_1_0_0, NULL, &t2646_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2407.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2407_TI;
#include "t2407MD.h"

#include "t41.h"
#include "t557.h"
#include "t9.h"
#include "mscorlib_ArrayTypes.h"
#include "t2408.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t9_TI;
extern TypeInfo t2408_TI;
extern TypeInfo t21_TI;
#include "t2408MD.h"
extern MethodInfo m12443_MI;
extern MethodInfo m12445_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Button>
extern Il2CppType t316_0_0_33;
FieldInfo t2407_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2407_TI, offsetof(t2407, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2407_FIs[] =
{
	&t2407_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t9_0_0_0;
extern Il2CppType t9_0_0_0;
static ParameterInfo t2407_m12441_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12441_GM;
MethodInfo m12441_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2407_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2407_m12441_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12441_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2407_m12442_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12442_GM;
MethodInfo m12442_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2407_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2407_m12442_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12442_GM};
static MethodInfo* t2407_MIs[] =
{
	&m12441_MI,
	&m12442_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m12442_MI;
extern MethodInfo m12446_MI;
static MethodInfo* t2407_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12442_MI,
	&m12446_MI,
};
extern Il2CppType t2409_0_0_0;
extern TypeInfo t2409_TI;
extern MethodInfo m20705_MI;
extern TypeInfo t9_TI;
extern MethodInfo m12448_MI;
extern TypeInfo t9_TI;
static Il2CppRGCTXData t2407_RGCTXData[8] = 
{
	&t2409_0_0_0/* Type Usage */,
	&t2409_TI/* Class Usage */,
	&m20705_MI/* Method Usage */,
	&t9_TI/* Class Usage */,
	&m12448_MI/* Method Usage */,
	&m12443_MI/* Method Usage */,
	&t9_TI/* Class Usage */,
	&m12445_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2407_0_0_0;
extern Il2CppType t2407_1_0_0;
struct t2407;
extern Il2CppGenericClass t2407_GC;
TypeInfo t2407_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2407_MIs, NULL, t2407_FIs, NULL, &t2408_TI, NULL, NULL, &t2407_TI, NULL, t2407_VT, &EmptyCustomAttributesCache, &t2407_TI, &t2407_0_0_0, &t2407_1_0_0, NULL, &t2407_GC, NULL, NULL, NULL, t2407_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2407), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2409.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2409_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2409MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m20705(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Button>
extern Il2CppType t2409_0_0_1;
FieldInfo t2408_f0_FieldInfo = 
{
	"Delegate", &t2409_0_0_1, &t2408_TI, offsetof(t2408, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2408_FIs[] =
{
	&t2408_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2408_m12443_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12443_GM;
MethodInfo m12443_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2408_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2408_m12443_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12443_GM};
extern Il2CppType t2409_0_0_0;
static ParameterInfo t2408_m12444_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12444_GM;
MethodInfo m12444_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2408_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2408_m12444_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12444_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2408_m12445_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12445_GM;
MethodInfo m12445_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2408_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2408_m12445_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12445_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2408_m12446_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12446_GM;
MethodInfo m12446_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2408_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2408_m12446_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12446_GM};
static MethodInfo* t2408_MIs[] =
{
	&m12443_MI,
	&m12444_MI,
	&m12445_MI,
	&m12446_MI,
	NULL
};
static MethodInfo* t2408_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12445_MI,
	&m12446_MI,
};
extern TypeInfo t2409_TI;
extern TypeInfo t9_TI;
static Il2CppRGCTXData t2408_RGCTXData[5] = 
{
	&t2409_0_0_0/* Type Usage */,
	&t2409_TI/* Class Usage */,
	&m20705_MI/* Method Usage */,
	&t9_TI/* Class Usage */,
	&m12448_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2408_0_0_0;
extern Il2CppType t2408_1_0_0;
extern TypeInfo t556_TI;
struct t2408;
extern Il2CppGenericClass t2408_GC;
TypeInfo t2408_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2408_MIs, NULL, t2408_FIs, NULL, &t556_TI, NULL, NULL, &t2408_TI, NULL, t2408_VT, &EmptyCustomAttributesCache, &t2408_TI, &t2408_0_0_0, &t2408_1_0_0, NULL, &t2408_GC, NULL, NULL, NULL, t2408_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2408), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Button>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2409_m12447_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12447_GM;
MethodInfo m12447_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2409_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2409_m12447_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12447_GM};
extern Il2CppType t9_0_0_0;
static ParameterInfo t2409_m12448_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12448_GM;
MethodInfo m12448_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2409_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2409_m12448_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12448_GM};
extern Il2CppType t9_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2409_m12449_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12449_GM;
MethodInfo m12449_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2409_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2409_m12449_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12449_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2409_m12450_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12450_GM;
MethodInfo m12450_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2409_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2409_m12450_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12450_GM};
static MethodInfo* t2409_MIs[] =
{
	&m12447_MI,
	&m12448_MI,
	&m12449_MI,
	&m12450_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m12449_MI;
extern MethodInfo m12450_MI;
static MethodInfo* t2409_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12448_MI,
	&m12449_MI,
	&m12450_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2409_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2409_1_0_0;
extern TypeInfo t195_TI;
struct t2409;
extern Il2CppGenericClass t2409_GC;
TypeInfo t2409_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2409_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2409_TI, NULL, t2409_VT, &EmptyCustomAttributesCache, &t2409_TI, &t2409_0_0_0, &t2409_1_0_0, t2409_IOs, &t2409_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2409), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4126_TI;

#include "t140.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.CanvasUpdate>
extern MethodInfo m27589_MI;
static PropertyInfo t4126____Current_PropertyInfo = 
{
	&t4126_TI, "Current", &m27589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4126_PIs[] =
{
	&t4126____Current_PropertyInfo,
	NULL
};
extern Il2CppType t140_0_0_0;
extern void* RuntimeInvoker_t140 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27589_GM;
MethodInfo m27589_MI = 
{
	"get_Current", NULL, &t4126_TI, &t140_0_0_0, RuntimeInvoker_t140, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27589_GM};
static MethodInfo* t4126_MIs[] =
{
	&m27589_MI,
	NULL
};
static TypeInfo* t4126_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4126_0_0_0;
extern Il2CppType t4126_1_0_0;
struct t4126;
extern Il2CppGenericClass t4126_GC;
TypeInfo t4126_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4126_MIs, t4126_PIs, NULL, NULL, NULL, NULL, NULL, &t4126_TI, t4126_ITIs, NULL, &EmptyCustomAttributesCache, &t4126_TI, &t4126_0_0_0, &t4126_1_0_0, NULL, &t4126_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2410.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2410_TI;
#include "t2410MD.h"

extern TypeInfo t140_TI;
extern MethodInfo m12455_MI;
extern MethodInfo m20707_MI;
struct t20;
 int32_t m20707 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12451_MI;
 void m12451 (t2410 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12452_MI;
 t29 * m12452 (t2410 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12455(__this, &m12455_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t140_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12453_MI;
 void m12453 (t2410 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12454_MI;
 bool m12454 (t2410 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m12455 (t2410 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20707(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20707_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasUpdate>
extern Il2CppType t20_0_0_1;
FieldInfo t2410_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2410_TI, offsetof(t2410, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2410_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2410_TI, offsetof(t2410, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2410_FIs[] =
{
	&t2410_f0_FieldInfo,
	&t2410_f1_FieldInfo,
	NULL
};
static PropertyInfo t2410____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2410_TI, "System.Collections.IEnumerator.Current", &m12452_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2410____Current_PropertyInfo = 
{
	&t2410_TI, "Current", &m12455_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2410_PIs[] =
{
	&t2410____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2410____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2410_m12451_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12451_GM;
MethodInfo m12451_MI = 
{
	".ctor", (methodPointerType)&m12451, &t2410_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2410_m12451_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12451_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12452_GM;
MethodInfo m12452_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12452, &t2410_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12452_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12453_GM;
MethodInfo m12453_MI = 
{
	"Dispose", (methodPointerType)&m12453, &t2410_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12453_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12454_GM;
MethodInfo m12454_MI = 
{
	"MoveNext", (methodPointerType)&m12454, &t2410_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12454_GM};
extern Il2CppType t140_0_0_0;
extern void* RuntimeInvoker_t140 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12455_GM;
MethodInfo m12455_MI = 
{
	"get_Current", (methodPointerType)&m12455, &t2410_TI, &t140_0_0_0, RuntimeInvoker_t140, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12455_GM};
static MethodInfo* t2410_MIs[] =
{
	&m12451_MI,
	&m12452_MI,
	&m12453_MI,
	&m12454_MI,
	&m12455_MI,
	NULL
};
static MethodInfo* t2410_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12452_MI,
	&m12454_MI,
	&m12453_MI,
	&m12455_MI,
};
static TypeInfo* t2410_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4126_TI,
};
static Il2CppInterfaceOffsetPair t2410_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4126_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2410_0_0_0;
extern Il2CppType t2410_1_0_0;
extern Il2CppGenericClass t2410_GC;
TypeInfo t2410_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2410_MIs, t2410_PIs, t2410_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2410_TI, t2410_ITIs, t2410_VT, &EmptyCustomAttributesCache, &t2410_TI, &t2410_0_0_0, &t2410_1_0_0, t2410_IOs, &t2410_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2410)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5292_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.CanvasUpdate>
extern MethodInfo m27590_MI;
static PropertyInfo t5292____Count_PropertyInfo = 
{
	&t5292_TI, "Count", &m27590_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27591_MI;
static PropertyInfo t5292____IsReadOnly_PropertyInfo = 
{
	&t5292_TI, "IsReadOnly", &m27591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5292_PIs[] =
{
	&t5292____Count_PropertyInfo,
	&t5292____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27590_GM;
MethodInfo m27590_MI = 
{
	"get_Count", NULL, &t5292_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27590_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27591_GM;
MethodInfo m27591_MI = 
{
	"get_IsReadOnly", NULL, &t5292_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27591_GM};
extern Il2CppType t140_0_0_0;
extern Il2CppType t140_0_0_0;
static ParameterInfo t5292_m27592_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27592_GM;
MethodInfo m27592_MI = 
{
	"Add", NULL, &t5292_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5292_m27592_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27592_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27593_GM;
MethodInfo m27593_MI = 
{
	"Clear", NULL, &t5292_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27593_GM};
extern Il2CppType t140_0_0_0;
static ParameterInfo t5292_m27594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27594_GM;
MethodInfo m27594_MI = 
{
	"Contains", NULL, &t5292_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5292_m27594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27594_GM};
extern Il2CppType t3832_0_0_0;
extern Il2CppType t3832_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5292_m27595_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3832_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27595_GM;
MethodInfo m27595_MI = 
{
	"CopyTo", NULL, &t5292_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5292_m27595_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27595_GM};
extern Il2CppType t140_0_0_0;
static ParameterInfo t5292_m27596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27596_GM;
MethodInfo m27596_MI = 
{
	"Remove", NULL, &t5292_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5292_m27596_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27596_GM};
static MethodInfo* t5292_MIs[] =
{
	&m27590_MI,
	&m27591_MI,
	&m27592_MI,
	&m27593_MI,
	&m27594_MI,
	&m27595_MI,
	&m27596_MI,
	NULL
};
extern TypeInfo t5294_TI;
static TypeInfo* t5292_ITIs[] = 
{
	&t603_TI,
	&t5294_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5292_0_0_0;
extern Il2CppType t5292_1_0_0;
struct t5292;
extern Il2CppGenericClass t5292_GC;
TypeInfo t5292_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5292_MIs, t5292_PIs, NULL, NULL, NULL, NULL, NULL, &t5292_TI, t5292_ITIs, NULL, &EmptyCustomAttributesCache, &t5292_TI, &t5292_0_0_0, &t5292_1_0_0, NULL, &t5292_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.CanvasUpdate>
extern Il2CppType t4126_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27597_GM;
MethodInfo m27597_MI = 
{
	"GetEnumerator", NULL, &t5294_TI, &t4126_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27597_GM};
static MethodInfo* t5294_MIs[] =
{
	&m27597_MI,
	NULL
};
static TypeInfo* t5294_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5294_0_0_0;
extern Il2CppType t5294_1_0_0;
struct t5294;
extern Il2CppGenericClass t5294_GC;
TypeInfo t5294_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5294_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5294_TI, t5294_ITIs, NULL, &EmptyCustomAttributesCache, &t5294_TI, &t5294_0_0_0, &t5294_1_0_0, NULL, &t5294_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5293_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.CanvasUpdate>
extern MethodInfo m27598_MI;
extern MethodInfo m27599_MI;
static PropertyInfo t5293____Item_PropertyInfo = 
{
	&t5293_TI, "Item", &m27598_MI, &m27599_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5293_PIs[] =
{
	&t5293____Item_PropertyInfo,
	NULL
};
extern Il2CppType t140_0_0_0;
static ParameterInfo t5293_m27600_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27600_GM;
MethodInfo m27600_MI = 
{
	"IndexOf", NULL, &t5293_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5293_m27600_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27600_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t140_0_0_0;
static ParameterInfo t5293_m27601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27601_GM;
MethodInfo m27601_MI = 
{
	"Insert", NULL, &t5293_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5293_m27601_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27601_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5293_m27602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27602_GM;
MethodInfo m27602_MI = 
{
	"RemoveAt", NULL, &t5293_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5293_m27602_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27602_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5293_m27598_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t140_0_0_0;
extern void* RuntimeInvoker_t140_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27598_GM;
MethodInfo m27598_MI = 
{
	"get_Item", NULL, &t5293_TI, &t140_0_0_0, RuntimeInvoker_t140_t44, t5293_m27598_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27598_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t140_0_0_0;
static ParameterInfo t5293_m27599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27599_GM;
MethodInfo m27599_MI = 
{
	"set_Item", NULL, &t5293_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5293_m27599_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27599_GM};
static MethodInfo* t5293_MIs[] =
{
	&m27600_MI,
	&m27601_MI,
	&m27602_MI,
	&m27598_MI,
	&m27599_MI,
	NULL
};
static TypeInfo* t5293_ITIs[] = 
{
	&t603_TI,
	&t5292_TI,
	&t5294_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5293_0_0_0;
extern Il2CppType t5293_1_0_0;
struct t5293;
extern Il2CppGenericClass t5293_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5293_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5293_MIs, t5293_PIs, NULL, NULL, NULL, NULL, NULL, &t5293_TI, t5293_ITIs, NULL, &t1908__CustomAttributeCache, &t5293_TI, &t5293_0_0_0, &t5293_1_0_0, NULL, &t5293_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t142.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t142_TI;
#include "t142MD.h"

#include "t2411.h"
#include "t365.h"
#include "t930.h"
#include "t345.h"
#include "t144.h"
#include "t143.h"
extern TypeInfo t2411_TI;
extern TypeInfo t44_TI;
extern TypeInfo t145_TI;
extern TypeInfo t365_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2412_TI;
extern TypeInfo t930_TI;
extern TypeInfo t2413_TI;
extern TypeInfo t345_TI;
extern TypeInfo t144_TI;
#include "t2411MD.h"
#include "t365MD.h"
#include "t29MD.h"
#include "t930MD.h"
#include "t345MD.h"
#include "t144MD.h"
extern MethodInfo m12636_MI;
extern MethodInfo m12637_MI;
extern MethodInfo m12766_MI;
extern MethodInfo m12638_MI;
extern MethodInfo m12760_MI;
extern MethodInfo m12590_MI;
extern MethodInfo m12731_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m12461_MI;
extern MethodInfo m12762_MI;
extern MethodInfo m12608_MI;
extern MethodInfo m12767_MI;
extern MethodInfo m12475_MI;
extern MethodInfo m9378_MI;
extern MethodInfo m12614_MI;
extern MethodInfo m12761_MI;
extern MethodInfo m12616_MI;
extern MethodInfo m3988_MI;
extern MethodInfo m12628_MI;
extern MethodInfo m12752_MI;
extern MethodInfo m12719_MI;
extern MethodInfo m1536_MI;
extern MethodInfo m12631_MI;


// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2411_0_0_33;
FieldInfo t142_f0_FieldInfo = 
{
	"m_List", &t2411_0_0_33, &t142_TI, offsetof(t142, f0), &EmptyCustomAttributesCache};
extern Il2CppType t365_0_0_1;
FieldInfo t142_f1_FieldInfo = 
{
	"m_Dictionary", &t365_0_0_1, &t142_TI, offsetof(t142, f1), &EmptyCustomAttributesCache};
static FieldInfo* t142_FIs[] =
{
	&t142_f0_FieldInfo,
	&t142_f1_FieldInfo,
	NULL
};
extern MethodInfo m1532_MI;
static PropertyInfo t142____Count_PropertyInfo = 
{
	&t142_TI, "Count", &m1532_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12469_MI;
static PropertyInfo t142____IsReadOnly_PropertyInfo = 
{
	&t142_TI, "IsReadOnly", &m12469_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1530_MI;
extern MethodInfo m12478_MI;
static PropertyInfo t142____Item_PropertyInfo = 
{
	&t142_TI, "Item", &m1530_MI, &m12478_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t142_PIs[] =
{
	&t142____Count_PropertyInfo,
	&t142____IsReadOnly_PropertyInfo,
	&t142____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1523_GM;
MethodInfo m1523_MI = 
{
	".ctor", (methodPointerType)&m12456_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1523_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12457_GM;
MethodInfo m12457_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12458_gshared, &t142_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12457_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t142_m1534_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1534_GM;
MethodInfo m1534_MI = 
{
	"Add", (methodPointerType)&m12459_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t142_m1534_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1534_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t142_m1536_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1536_GM;
MethodInfo m1536_MI = 
{
	"Remove", (methodPointerType)&m12460_gshared, &t142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t142_m1536_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1536_GM};
extern Il2CppType t2412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12461_GM;
MethodInfo m12461_MI = 
{
	"GetEnumerator", (methodPointerType)&m12462_gshared, &t142_TI, &t2412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12461_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1533_GM;
MethodInfo m1533_MI = 
{
	"Clear", (methodPointerType)&m12463_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1533_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t142_m12464_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12464_GM;
MethodInfo m12464_MI = 
{
	"Contains", (methodPointerType)&m12465_gshared, &t142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t142_m12464_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12464_GM};
extern Il2CppType t2413_0_0_0;
extern Il2CppType t2413_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t142_m12466_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2413_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12466_GM;
MethodInfo m12466_MI = 
{
	"CopyTo", (methodPointerType)&m12467_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t142_m12466_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12466_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1532_GM;
MethodInfo m1532_MI = 
{
	"get_Count", (methodPointerType)&m12468_gshared, &t142_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1532_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12469_GM;
MethodInfo m12469_MI = 
{
	"get_IsReadOnly", (methodPointerType)&m12470_gshared, &t142_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12469_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t142_m12471_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12471_GM;
MethodInfo m12471_MI = 
{
	"IndexOf", (methodPointerType)&m12472_gshared, &t142_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t142_m12471_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12471_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t142_m12473_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12473_GM;
MethodInfo m12473_MI = 
{
	"Insert", (methodPointerType)&m12474_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t142_m12473_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12473_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t142_m12475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12475_GM;
MethodInfo m12475_MI = 
{
	"RemoveAt", (methodPointerType)&m12476_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t142_m12475_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12475_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t142_m1530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1530_GM;
MethodInfo m1530_MI = 
{
	"get_Item", (methodPointerType)&m12477_gshared, &t142_TI, &t145_0_0_0, RuntimeInvoker_t29_t44, t142_m1530_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1530_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t142_m12478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12478_GM;
MethodInfo m12478_MI = 
{
	"set_Item", (methodPointerType)&m12479_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t142_m12478_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12478_GM};
extern Il2CppType t144_0_0_0;
extern Il2CppType t144_0_0_0;
static ParameterInfo t142_m1528_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t144_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1528_GM;
MethodInfo m1528_MI = 
{
	"RemoveAll", (methodPointerType)&m12480_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t142_m1528_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1528_GM};
extern Il2CppType t143_0_0_0;
extern Il2CppType t143_0_0_0;
static ParameterInfo t142_m1529_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134217728, &EmptyCustomAttributesCache, &t143_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1529_GM;
MethodInfo m1529_MI = 
{
	"Sort", (methodPointerType)&m12481_gshared, &t142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t142_m1529_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1529_GM};
static MethodInfo* t142_MIs[] =
{
	&m1523_MI,
	&m12457_MI,
	&m1534_MI,
	&m1536_MI,
	&m12461_MI,
	&m1533_MI,
	&m12464_MI,
	&m12466_MI,
	&m1532_MI,
	&m12469_MI,
	&m12471_MI,
	&m12473_MI,
	&m12475_MI,
	&m1530_MI,
	&m12478_MI,
	&m1528_MI,
	&m1529_MI,
	NULL
};
extern MethodInfo m12471_MI;
extern MethodInfo m12473_MI;
extern MethodInfo m1534_MI;
extern MethodInfo m1533_MI;
extern MethodInfo m12464_MI;
extern MethodInfo m12466_MI;
extern MethodInfo m12457_MI;
static MethodInfo* t142_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12471_MI,
	&m12473_MI,
	&m12475_MI,
	&m1530_MI,
	&m12478_MI,
	&m1532_MI,
	&m12469_MI,
	&m1534_MI,
	&m1533_MI,
	&m12464_MI,
	&m12466_MI,
	&m1536_MI,
	&m12461_MI,
	&m12457_MI,
};
extern TypeInfo t2434_TI;
extern TypeInfo t2430_TI;
extern TypeInfo t2431_TI;
static TypeInfo* t142_ITIs[] = 
{
	&t2434_TI,
	&t2430_TI,
	&t2431_TI,
	&t603_TI,
};
static Il2CppInterfaceOffsetPair t142_IOs[] = 
{
	{ &t2434_TI, 4},
	{ &t2430_TI, 9},
	{ &t2431_TI, 16},
	{ &t603_TI, 17},
};
extern TypeInfo t2411_TI;
extern TypeInfo t365_TI;
static Il2CppRGCTXData t142_RGCTXData[22] = 
{
	&t2411_TI/* Class Usage */,
	&m12590_MI/* Method Usage */,
	&t365_TI/* Class Usage */,
	&m12731_MI/* Method Usage */,
	&m12461_MI/* Method Usage */,
	&m12762_MI/* Method Usage */,
	&m12608_MI/* Method Usage */,
	&m12636_MI/* Method Usage */,
	&m12760_MI/* Method Usage */,
	&m12767_MI/* Method Usage */,
	&m12475_MI/* Method Usage */,
	&m12614_MI/* Method Usage */,
	&m12761_MI/* Method Usage */,
	&m12616_MI/* Method Usage */,
	&m12637_MI/* Method Usage */,
	&m12766_MI/* Method Usage */,
	&m12628_MI/* Method Usage */,
	&m12638_MI/* Method Usage */,
	&m12752_MI/* Method Usage */,
	&m12719_MI/* Method Usage */,
	&m1536_MI/* Method Usage */,
	&m12631_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t142_0_0_0;
extern Il2CppType t142_1_0_0;
struct t142;
extern Il2CppGenericClass t142_GC;
extern CustomAttributesCache t274__CustomAttributeCache;
TypeInfo t142_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IndexedSet`1", "UnityEngine.UI.Collections", t142_MIs, t142_PIs, t142_FIs, NULL, &t29_TI, NULL, NULL, &t142_TI, t142_ITIs, t142_VT, &t274__CustomAttributeCache, &t142_TI, &t142_0_0_0, &t142_1_0_0, t142_IOs, &t142_GC, NULL, NULL, NULL, t142_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t142), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 17, 3, 2, 0, 0, 18, 4, 4};
#include "t2414.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2414_TI;
#include "t2414MD.h"

#include "t294.h"
#include "t2415.h"
#include "t2180.h"
#include "t2181.h"
extern TypeInfo t294_TI;
extern TypeInfo t2415_TI;
extern TypeInfo t346_TI;
extern TypeInfo t2180_TI;
#include "t294MD.h"
#include "t2415MD.h"
#include "t2180MD.h"
extern MethodInfo m10626_MI;
extern MethodInfo m10627_MI;
extern MethodInfo m12517_MI;
extern MethodInfo m10629_MI;
extern MethodInfo m12511_MI;
extern MethodInfo m10537_MI;
extern MethodInfo m12482_MI;
extern MethodInfo m12462_MI;
extern MethodInfo m12513_MI;
extern MethodInfo m10573_MI;
extern MethodInfo m12518_MI;
extern MethodInfo m12476_MI;
extern MethodInfo m10585_MI;
extern MethodInfo m12512_MI;
extern MethodInfo m10589_MI;
extern MethodInfo m10611_MI;
extern MethodInfo m12503_MI;
extern MethodInfo m10742_MI;
extern MethodInfo m12460_MI;
extern MethodInfo m10617_MI;


extern MethodInfo m12456_MI;
 void m12456_gshared (t2414 * __this, MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t294_TI));
		t294 * L_0 = (t294 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (t294 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->f0 = L_0;
		t2415 * L_1 = (t2415 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (t2415 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->f1 = L_1;
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m12458_MI;
 t29 * m12458_gshared (t2414 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (t29*)VirtFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), __this);
		return L_0;
	}
}
extern MethodInfo m12459_MI;
 void m12459_gshared (t2414 * __this, t29 * p0, MethodInfo* method)
{
	{
		t2415 * L_0 = (__this->f1);
		bool L_1 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), L_0, p0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t294 * L_2 = (__this->f0);
		VirtActionInvoker1< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), L_2, p0);
		t2415 * L_3 = (__this->f1);
		t294 * L_4 = (__this->f0);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_4);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), L_3, p0, ((int32_t)(L_5-1)));
		return;
	}
}
 bool m12460_gshared (t2414 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		t2415 * L_0 = (__this->f1);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t29 *, int32_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), L_0, p0, (&V_0));
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		return 0;
	}

IL_0017:
	{
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), __this, V_0);
		return 1;
	}
}
 t29* m12462_gshared (t2414 * __this, MethodInfo* method)
{
	{
		t930 * L_0 = (t930 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t930_TI));
		m9378(L_0, &m9378_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12463_MI;
 void m12463_gshared (t2414 * __this, MethodInfo* method)
{
	{
		t294 * L_0 = (__this->f0);
		VirtActionInvoker0::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), L_0);
		t2415 * L_1 = (__this->f1);
		VirtActionInvoker0::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), L_1);
		return;
	}
}
extern MethodInfo m12465_MI;
 bool m12465_gshared (t2414 * __this, t29 * p0, MethodInfo* method)
{
	{
		t2415 * L_0 = (__this->f1);
		bool L_1 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m12467_MI;
 void m12467_gshared (t2414 * __this, t316* p0, int32_t p1, MethodInfo* method)
{
	{
		t294 * L_0 = (__this->f0);
		VirtActionInvoker2< t316*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), L_0, p0, p1);
		return;
	}
}
extern MethodInfo m12468_MI;
 int32_t m12468_gshared (t2414 * __this, MethodInfo* method)
{
	{
		t294 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_0);
		return L_1;
	}
}
extern MethodInfo m12470_MI;
 bool m12470_gshared (t2414 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m12472_MI;
 int32_t m12472_gshared (t2414 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		t2415 * L_0 = (__this->f1);
		VirtFuncInvoker2< bool, t29 *, int32_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), L_0, p0, (&V_0));
		return V_0;
	}
}
extern MethodInfo m12474_MI;
 void m12474_gshared (t2414 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral67, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
 void m12476_gshared (t2414 * __this, int32_t p0, MethodInfo* method)
{
	t29 * V_0 = {0};
	int32_t V_1 = 0;
	t29 * V_2 = {0};
	{
		t294 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), L_0, p0);
		V_0 = L_1;
		t2415 * L_2 = (__this->f1);
		VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), L_2, V_0);
		t294 * L_3 = (__this->f0);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_3);
		if ((((uint32_t)p0) != ((uint32_t)((int32_t)(L_4-1)))))
		{
			goto IL_003e;
		}
	}
	{
		t294 * L_5 = (__this->f0);
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), L_5, p0);
		goto IL_007f;
	}

IL_003e:
	{
		t294 * L_6 = (__this->f0);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_6);
		V_1 = ((int32_t)(L_7-1));
		t294 * L_8 = (__this->f0);
		t29 * L_9 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), L_8, V_1);
		V_2 = L_9;
		t294 * L_10 = (__this->f0);
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), L_10, p0, V_2);
		t2415 * L_11 = (__this->f1);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), L_11, V_2, p0);
		t294 * L_12 = (__this->f0);
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), L_12, V_1);
	}

IL_007f:
	{
		return;
	}
}
extern MethodInfo m12477_MI;
 t29 * m12477_gshared (t2414 * __this, int32_t p0, MethodInfo* method)
{
	{
		t294 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m12479_MI;
 void m12479_gshared (t2414 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	t29 * V_0 = {0};
	{
		t294 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), L_0, p0);
		V_0 = L_1;
		t2415 * L_2 = (__this->f1);
		VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), L_2, V_0);
		t294 * L_3 = (__this->f0);
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), L_3, p0, p1);
		t2415 * L_4 = (__this->f1);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), L_4, V_0, p0);
		return;
	}
}
extern MethodInfo m12480_MI;
 void m12480_gshared (t2414 * __this, t2180 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t29 * V_1 = {0};
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0007:
	{
		t294 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), L_0, V_0);
		V_1 = L_1;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), p0, V_1);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), __this, V_1);
		goto IL_0031;
	}

IL_002d:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0031:
	{
		t294 * L_3 = (__this->f0);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_3);
		if ((((int32_t)V_0) < ((int32_t)L_4)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
extern MethodInfo m12481_MI;
 void m12481_gshared (t2414 * __this, t2181 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t29 * V_1 = {0};
	{
		t294 * L_0 = (__this->f0);
		(( void (*) (t294 * __this, t2181 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)(L_0, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		V_0 = 0;
		goto IL_0031;
	}

IL_0013:
	{
		t294 * L_1 = (__this->f0);
		t29 * L_2 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), L_1, V_0);
		V_1 = L_2;
		t2415 * L_3 = (__this->f1);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), L_3, V_1, V_0);
		V_0 = ((int32_t)(V_0+1));
	}

IL_0031:
	{
		t294 * L_4 = (__this->f0);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_4);
		if ((((int32_t)V_0) < ((int32_t)L_5)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1<System.Object>
extern Il2CppType t294_0_0_33;
FieldInfo t2414_f0_FieldInfo = 
{
	"m_List", &t294_0_0_33, &t2414_TI, offsetof(t2414, f0), &EmptyCustomAttributesCache};
extern Il2CppType t2415_0_0_1;
FieldInfo t2414_f1_FieldInfo = 
{
	"m_Dictionary", &t2415_0_0_1, &t2414_TI, offsetof(t2414, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2414_FIs[] =
{
	&t2414_f0_FieldInfo,
	&t2414_f1_FieldInfo,
	NULL
};
static PropertyInfo t2414____Count_PropertyInfo = 
{
	&t2414_TI, "Count", &m12468_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2414____IsReadOnly_PropertyInfo = 
{
	&t2414_TI, "IsReadOnly", &m12470_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2414____Item_PropertyInfo = 
{
	&t2414_TI, "Item", &m12477_MI, &m12479_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2414_PIs[] =
{
	&t2414____Count_PropertyInfo,
	&t2414____IsReadOnly_PropertyInfo,
	&t2414____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12456_GM;
MethodInfo m12456_MI = 
{
	".ctor", (methodPointerType)&m12456_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12456_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12458_GM;
MethodInfo m12458_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12458_gshared, &t2414_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12458_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2414_m12459_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12459_GM;
MethodInfo m12459_MI = 
{
	"Add", (methodPointerType)&m12459_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2414_m12459_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12459_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2414_m12460_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12460_GM;
MethodInfo m12460_MI = 
{
	"Remove", (methodPointerType)&m12460_gshared, &t2414_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2414_m12460_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12460_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12462_GM;
MethodInfo m12462_MI = 
{
	"GetEnumerator", (methodPointerType)&m12462_gshared, &t2414_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12462_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12463_GM;
MethodInfo m12463_MI = 
{
	"Clear", (methodPointerType)&m12463_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12463_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2414_m12465_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12465_GM;
MethodInfo m12465_MI = 
{
	"Contains", (methodPointerType)&m12465_gshared, &t2414_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2414_m12465_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12465_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2414_m12467_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12467_GM;
MethodInfo m12467_MI = 
{
	"CopyTo", (methodPointerType)&m12467_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2414_m12467_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12467_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12468_GM;
MethodInfo m12468_MI = 
{
	"get_Count", (methodPointerType)&m12468_gshared, &t2414_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12468_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12470_GM;
MethodInfo m12470_MI = 
{
	"get_IsReadOnly", (methodPointerType)&m12470_gshared, &t2414_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12470_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2414_m12472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12472_GM;
MethodInfo m12472_MI = 
{
	"IndexOf", (methodPointerType)&m12472_gshared, &t2414_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2414_m12472_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12472_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2414_m12474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12474_GM;
MethodInfo m12474_MI = 
{
	"Insert", (methodPointerType)&m12474_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2414_m12474_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12474_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2414_m12476_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12476_GM;
MethodInfo m12476_MI = 
{
	"RemoveAt", (methodPointerType)&m12476_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2414_m12476_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12476_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2414_m12477_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12477_GM;
MethodInfo m12477_MI = 
{
	"get_Item", (methodPointerType)&m12477_gshared, &t2414_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2414_m12477_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12477_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2414_m12479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12479_GM;
MethodInfo m12479_MI = 
{
	"set_Item", (methodPointerType)&m12479_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2414_m12479_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12479_GM};
extern Il2CppType t2180_0_0_0;
extern Il2CppType t2180_0_0_0;
static ParameterInfo t2414_m12480_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12480_GM;
MethodInfo m12480_MI = 
{
	"RemoveAll", (methodPointerType)&m12480_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2414_m12480_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12480_GM};
extern Il2CppType t2181_0_0_0;
extern Il2CppType t2181_0_0_0;
static ParameterInfo t2414_m12481_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134217728, &EmptyCustomAttributesCache, &t2181_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12481_GM;
MethodInfo m12481_MI = 
{
	"Sort", (methodPointerType)&m12481_gshared, &t2414_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2414_m12481_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12481_GM};
static MethodInfo* t2414_MIs[] =
{
	&m12456_MI,
	&m12458_MI,
	&m12459_MI,
	&m12460_MI,
	&m12462_MI,
	&m12463_MI,
	&m12465_MI,
	&m12467_MI,
	&m12468_MI,
	&m12470_MI,
	&m12472_MI,
	&m12474_MI,
	&m12476_MI,
	&m12477_MI,
	&m12479_MI,
	&m12480_MI,
	&m12481_MI,
	NULL
};
static MethodInfo* t2414_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12472_MI,
	&m12474_MI,
	&m12476_MI,
	&m12477_MI,
	&m12479_MI,
	&m12468_MI,
	&m12470_MI,
	&m12459_MI,
	&m12463_MI,
	&m12465_MI,
	&m12467_MI,
	&m12460_MI,
	&m12462_MI,
	&m12458_MI,
};
extern TypeInfo t2186_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t2414_ITIs[] = 
{
	&t2186_TI,
	&t2182_TI,
	&t2183_TI,
	&t603_TI,
};
static Il2CppInterfaceOffsetPair t2414_IOs[] = 
{
	{ &t2186_TI, 4},
	{ &t2182_TI, 9},
	{ &t2183_TI, 16},
	{ &t603_TI, 17},
};
extern TypeInfo t294_TI;
extern TypeInfo t2415_TI;
static Il2CppRGCTXData t2414_RGCTXData[22] = 
{
	&t294_TI/* Class Usage */,
	&m10537_MI/* Method Usage */,
	&t2415_TI/* Class Usage */,
	&m12482_MI/* Method Usage */,
	&m12462_MI/* Method Usage */,
	&m12513_MI/* Method Usage */,
	&m10573_MI/* Method Usage */,
	&m10626_MI/* Method Usage */,
	&m12511_MI/* Method Usage */,
	&m12518_MI/* Method Usage */,
	&m12476_MI/* Method Usage */,
	&m10585_MI/* Method Usage */,
	&m12512_MI/* Method Usage */,
	&m10589_MI/* Method Usage */,
	&m10627_MI/* Method Usage */,
	&m12517_MI/* Method Usage */,
	&m10611_MI/* Method Usage */,
	&m10629_MI/* Method Usage */,
	&m12503_MI/* Method Usage */,
	&m10742_MI/* Method Usage */,
	&m12460_MI/* Method Usage */,
	&m10617_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2414_0_0_0;
extern Il2CppType t2414_1_0_0;
struct t2414;
extern Il2CppGenericClass t2414_GC;
extern CustomAttributesCache t274__CustomAttributeCache;
TypeInfo t2414_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IndexedSet`1", "UnityEngine.UI.Collections", t2414_MIs, t2414_PIs, t2414_FIs, NULL, &t29_TI, NULL, NULL, &t2414_TI, t2414_ITIs, t2414_VT, &t274__CustomAttributeCache, &t2414_TI, &t2414_0_0_0, &t2414_1_0_0, t2414_IOs, &t2414_GC, NULL, NULL, NULL, t2414_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2414), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 17, 3, 2, 0, 0, 18, 4, 4};
#ifndef _MSC_VER
#else
#endif

#include "t338.h"
#include "t1248.h"
#include "t1258.h"
#include "t2418.h"
#include "t733.h"
#include "t735.h"
#include "t2420.h"
#include "t725.h"
#include "t2417.h"
#include "t2427.h"
#include "t2422.h"
#include "t2428.h"
#include "t2001.h"
#include "t2361.h"
extern TypeInfo t338_TI;
extern TypeInfo t2416_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t2418_TI;
extern TypeInfo t2419_TI;
extern TypeInfo t2420_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t725_TI;
extern TypeInfo t2417_TI;
extern TypeInfo t2427_TI;
extern TypeInfo t2422_TI;
extern TypeInfo t2428_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2001_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t719_TI;
extern TypeInfo t2361_TI;
extern TypeInfo t2344_TI;
extern TypeInfo t7_TI;
#include "t338MD.h"
#include "t1258MD.h"
#include "t2418MD.h"
#include "t2420MD.h"
#include "t2417MD.h"
#include "t2427MD.h"
#include "t2422MD.h"
#include "t2428MD.h"
#include "t915MD.h"
#include "t2001MD.h"
#include "t719MD.h"
#include "t2361MD.h"
#include "t733MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t2416_0_0_0;
extern Il2CppType t2419_0_0_0;
extern MethodInfo m12520_MI;
extern MethodInfo m12502_MI;
extern MethodInfo m12521_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m26621_MI;
extern MethodInfo m26620_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m12510_MI;
extern MethodInfo m12536_MI;
extern MethodInfo m12504_MI;
extern MethodInfo m12526_MI;
extern MethodInfo m12528_MI;
extern MethodInfo m12522_MI;
extern MethodInfo m12509_MI;
extern MethodInfo m12506_MI;
extern MethodInfo m12524_MI;
extern MethodInfo m12571_MI;
extern MethodInfo m20731_MI;
extern MethodInfo m12507_MI;
extern MethodInfo m12575_MI;
extern MethodInfo m20733_MI;
extern MethodInfo m12555_MI;
extern MethodInfo m12579_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m10707_MI;
extern MethodInfo m12505_MI;
extern MethodInfo m12501_MI;
extern MethodInfo m12525_MI;
extern MethodInfo m20734_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m12170_MI;
extern MethodInfo m27290_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m27426_MI;
extern MethodInfo m3965_MI;
struct t2415;
 void m20731 (t2415 * __this, t3541* p0, int32_t p1, t2417 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t2415;
#include "t295.h"
 void m20733 (t2415 * __this, t20 * p0, int32_t p1, t2427 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t2415;
 void m20734 (t2415 * __this, t2419* p0, int32_t p1, t2427 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12482 (t2415 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12504(__this, ((int32_t)10), (t29*)NULL, &m12504_MI);
		return;
	}
}
extern MethodInfo m12483_MI;
 void m12483 (t2415 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12504(__this, ((int32_t)10), p0, &m12504_MI);
		return;
	}
}
extern MethodInfo m12484_MI;
 void m12484 (t2415 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12504(__this, p0, (t29*)NULL, &m12504_MI);
		return;
	}
}
extern MethodInfo m12485_MI;
 void m12485 (t2415 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m12486_MI;
 t29 * m12486 (t2415 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t29_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m12513_MI, __this, ((t29 *)Castclass(p0, InitializedTypeInfo(&t29_TI))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t29 * L_1 = m12520(__this, p0, &m12520_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m12502_MI, __this, L_1);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m12487_MI;
 void m12487 (t2415 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t29 * L_0 = m12520(__this, p0, &m12520_MI);
		int32_t L_1 = m12521(__this, p1, &m12521_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12503_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12488_MI;
 void m12488 (t2415 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t29 * L_0 = m12520(__this, p0, &m12520_MI);
		int32_t L_1 = m12521(__this, p1, &m12521_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12511_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12489_MI;
 void m12489 (t2415 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t29_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t29 * >::Invoke(&m12517_MI, __this, ((t29 *)Castclass(p0, InitializedTypeInfo(&t29_TI))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m12490_MI;
 bool m12490 (t2415 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12491_MI;
 t29 * m12491 (t2415 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m12492_MI;
 bool m12492 (t2415 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12493_MI;
 void m12493 (t2415 * __this, t2420  p0, MethodInfo* method){
	{
		t29 * L_0 = m12526((&p0), &m12526_MI);
		int32_t L_1 = m12528((&p0), &m12528_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12511_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12494_MI;
 bool m12494 (t2415 * __this, t2420  p0, MethodInfo* method){
	{
		bool L_0 = m12522(__this, p0, &m12522_MI);
		return L_0;
	}
}
extern MethodInfo m12495_MI;
 void m12495 (t2415 * __this, t2419* p0, int32_t p1, MethodInfo* method){
	{
		m12509(__this, p0, p1, &m12509_MI);
		return;
	}
}
extern MethodInfo m12496_MI;
 bool m12496 (t2415 * __this, t2420  p0, MethodInfo* method){
	{
		bool L_0 = m12522(__this, p0, &m12522_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t29 * L_1 = m12526((&p0), &m12526_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m12517_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m12497_MI;
 void m12497 (t2415 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2419* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t2415 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t2415 * G_B4_2 = {0};
	{
		V_0 = ((t2419*)IsInst(p0, InitializedTypeInfo(&t2419_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m12509(__this, V_0, p1, &m12509_MI);
		return;
	}

IL_0013:
	{
		m12506(__this, p0, p1, &m12506_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t2415 *)(__this));
		if ((((t2415_SFs*)InitializedTypeInfo(&t2415_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t2415 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m12524_MI };
		t2417 * L_1 = (t2417 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2417_TI));
		m12571(L_1, NULL, L_0, &m12571_MI);
		((t2415_SFs*)InitializedTypeInfo(&t2415_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t2415 *)(G_B4_2));
	}

IL_0040:
	{
		m20731(G_B5_2, G_B5_1, G_B5_0, (((t2415_SFs*)InitializedTypeInfo(&t2415_TI)->static_fields)->f15), &m20731_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m12507_MI };
		t2427 * L_3 = (t2427 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2427_TI));
		m12575(L_3, NULL, L_2, &m12575_MI);
		m20733(__this, p0, p1, L_3, &m20733_MI);
		return;
	}
}
extern MethodInfo m12498_MI;
 t29 * m12498 (t2415 * __this, MethodInfo* method){
	{
		t2422  L_0 = {0};
		m12555(&L_0, __this, &m12555_MI);
		t2422  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2422_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12499_MI;
 t29* m12499 (t2415 * __this, MethodInfo* method){
	{
		t2422  L_0 = {0};
		m12555(&L_0, __this, &m12555_MI);
		t2422  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2422_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12500_MI;
 t29 * m12500 (t2415 * __this, MethodInfo* method){
	{
		t2428 * L_0 = (t2428 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2428_TI));
		m12579(L_0, __this, &m12579_MI);
		return L_0;
	}
}
 int32_t m12501 (t2415 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 int32_t m12502 (t2415 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m26620_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m12503 (t2415 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m26620_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		m12510(__this, &m12510_MI);
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t316* L_29 = (__this->f6);
		*((t29 **)(t29 **)SZArrayLdElema(L_29, V_2)) = (t29 *)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t841* L_37 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_2)) = (int32_t)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m12504 (t2415 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t2415 * G_B4_0 = {0};
	t2415 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t2415 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t2415 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t2415 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t2415 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2001_TI));
		t2001 * L_1 = m10707(NULL, &m10707_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t2415 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m12505(__this, p0, &m12505_MI);
		__this->f14 = 0;
		return;
	}
}
 void m12505 (t2415 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), p0));
		__this->f7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m12506 (t2415 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m12501_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t2420  m12507 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		t2420  L_0 = {0};
		m12525(&L_0, p0, p1, &m12525_MI);
		return L_0;
	}
}
extern MethodInfo m12508_MI;
 int32_t m12508 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m12509 (t2415 * __this, t2419* p0, int32_t p1, MethodInfo* method){
	{
		m12506(__this, (t20 *)(t20 *)p0, p1, &m12506_MI);
		t35 L_0 = { &m12507_MI };
		t2427 * L_1 = (t2427 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2427_TI));
		m12575(L_1, NULL, L_0, &m12575_MI);
		m20734(__this, p0, p1, L_1, &m20734_MI);
		return;
	}
}
 void m12510 (t2415 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t316* V_7 = {0};
	t841* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t316* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_4, (*(t29 **)(t29 **)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), V_0));
		V_8 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		t316* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t841* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m12511 (t2415 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m26620_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		m12510(__this, &m12510_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t316* L_30 = (__this->f6);
		*((t29 **)(t29 **)SZArrayLdElema(L_30, V_2)) = (t29 *)p0;
		t841* L_31 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
 void m12512 (t2415 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t316* L_2 = (__this->f6);
		t316* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t841* L_4 = (__this->f7);
		t841* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m12513 (t2415 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m26620_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12514_MI;
 bool m12514 (t2415 * __this, int32_t p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_0 = m12170(NULL, &m12170_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t841* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, V_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12515_MI;
 void m12515 (t2415 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t2419* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2419*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2419*)SZArrayNew(InitializedTypeInfo(&t2419_TI), L_4));
		m12509(__this, V_0, 0, &m12509_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m12516_MI;
 void m12516 (t2415 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2419* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2416_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2416_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t2419_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2419*)Castclass(L_10, InitializedTypeInfo(&t2419_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m12505(__this, V_0, &m12505_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t29 * L_11 = m12526(((t2420 *)(t2420 *)SZArrayLdElema(V_1, V_2)), &m12526_MI);
		int32_t L_12 = m12528(((t2420 *)(t2420 *)SZArrayLdElema(V_1, V_2)), &m12528_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12511_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m12517 (t2415 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t29 * V_4 = {0};
	int32_t V_5 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m26620_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t316* L_25 = (__this->f6);
		Initobj (&t29_TI, (&V_4));
		*((t29 **)(t29 **)SZArrayLdElema(L_25, V_2)) = (t29 *)V_4;
		t841* L_26 = (__this->f7);
		Initobj (&t44_TI, (&V_5));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m12518 (t2415 * __this, t29 * p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m26621_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m26620_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t44_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m12519_MI;
 t2418 * m12519 (t2415 * __this, MethodInfo* method){
	{
		t2418 * L_0 = (t2418 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2418_TI));
		m12536(L_0, __this, &m12536_MI);
		return L_0;
	}
}
 t29 * m12520 (t2415 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t29_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t29 *)Castclass(p0, InitializedTypeInfo(&t29_TI)));
	}
}
 int32_t m12521 (t2415 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t44_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI)))));
	}
}
 bool m12522 (t2415 * __this, t2420  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29 * L_0 = m12526((&p0), &m12526_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t29 *, int32_t* >::Invoke(&m12518_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_2 = m12170(NULL, &m12170_MI);
		int32_t L_3 = m12528((&p0), &m12528_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27426_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m12523_MI;
 t2422  m12523 (t2415 * __this, MethodInfo* method){
	{
		t2422  L_0 = {0};
		m12555(&L_0, __this, &m12555_MI);
		return L_0;
	}
}
 t725  m12524 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		t29 * L_0 = p0;
		int32_t L_1 = p1;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		t725  L_3 = {0};
		m3965(&L_3, ((t29 *)L_0), L_2, &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
extern Il2CppType t44_0_0_32849;
FieldInfo t2415_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2415_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t2415_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t2415_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t2415_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t2415_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t2415_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t2415_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t2415_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t2415_TI, offsetof(t2415, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t2415_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t2415_TI, offsetof(t2415, f5), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t2415_f6_FieldInfo = 
{
	"keySlots", &t316_0_0_1, &t2415_TI, offsetof(t2415, f6), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t2415_f7_FieldInfo = 
{
	"valueSlots", &t841_0_0_1, &t2415_TI, offsetof(t2415, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2415_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t2415_TI, offsetof(t2415, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2415_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t2415_TI, offsetof(t2415, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2415_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t2415_TI, offsetof(t2415, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2415_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t2415_TI, offsetof(t2415, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2416_0_0_1;
FieldInfo t2415_f12_FieldInfo = 
{
	"hcp", &t2416_0_0_1, &t2415_TI, offsetof(t2415, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t2415_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t2415_TI, offsetof(t2415, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2415_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t2415_TI, offsetof(t2415, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2417_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t2415_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2417_0_0_17, &t2415_TI, offsetof(t2415_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t2415_FIs[] =
{
	&t2415_f0_FieldInfo,
	&t2415_f1_FieldInfo,
	&t2415_f2_FieldInfo,
	&t2415_f3_FieldInfo,
	&t2415_f4_FieldInfo,
	&t2415_f5_FieldInfo,
	&t2415_f6_FieldInfo,
	&t2415_f7_FieldInfo,
	&t2415_f8_FieldInfo,
	&t2415_f9_FieldInfo,
	&t2415_f10_FieldInfo,
	&t2415_f11_FieldInfo,
	&t2415_f12_FieldInfo,
	&t2415_f13_FieldInfo,
	&t2415_f14_FieldInfo,
	&t2415_f15_FieldInfo,
	NULL
};
static const int32_t t2415_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t2415_f0_DefaultValue = 
{
	&t2415_f0_FieldInfo, { (char*)&t2415_f0_DefaultValueData, &t44_0_0_0 }};
static const float t2415_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t2415_f1_DefaultValue = 
{
	&t2415_f1_FieldInfo, { (char*)&t2415_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t2415_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t2415_f2_DefaultValue = 
{
	&t2415_f2_FieldInfo, { (char*)&t2415_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t2415_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t2415_f3_DefaultValue = 
{
	&t2415_f3_FieldInfo, { (char*)&t2415_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2415_FDVs[] = 
{
	&t2415_f0_DefaultValue,
	&t2415_f1_DefaultValue,
	&t2415_f2_DefaultValue,
	&t2415_f3_DefaultValue,
	NULL
};
static PropertyInfo t2415____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t2415_TI, "System.Collections.IDictionary.Item", &m12486_MI, &m12487_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2415____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2415_TI, "System.Collections.ICollection.IsSynchronized", &m12490_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2415____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2415_TI, "System.Collections.ICollection.SyncRoot", &m12491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2415____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t2415_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m12492_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2415____Count_PropertyInfo = 
{
	&t2415_TI, "Count", &m12501_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2415____Item_PropertyInfo = 
{
	&t2415_TI, "Item", &m12502_MI, &m12503_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2415____Values_PropertyInfo = 
{
	&t2415_TI, "Values", &m12519_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2415_PIs[] =
{
	&t2415____System_Collections_IDictionary_Item_PropertyInfo,
	&t2415____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2415____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2415____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t2415____Count_PropertyInfo,
	&t2415____Item_PropertyInfo,
	&t2415____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12482_GM;
MethodInfo m12482_MI = 
{
	".ctor", (methodPointerType)&m12482, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12482_GM};
extern Il2CppType t2416_0_0_0;
static ParameterInfo t2415_m12483_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12483_GM;
MethodInfo m12483_MI = 
{
	".ctor", (methodPointerType)&m12483, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2415_m12483_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12483_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12484_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12484_GM;
MethodInfo m12484_MI = 
{
	".ctor", (methodPointerType)&m12484, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2415_m12484_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12484_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t2415_m12485_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12485_GM;
MethodInfo m12485_MI = 
{
	".ctor", (methodPointerType)&m12485, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t2415_m12485_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12485_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12486_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12486_GM;
MethodInfo m12486_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12486, &t2415_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2415_m12486_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12486_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12487_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12487_GM;
MethodInfo m12487_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12487, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2415_m12487_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12487_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12488_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12488_GM;
MethodInfo m12488_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12488, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2415_m12488_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12488_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12489_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12489_GM;
MethodInfo m12489_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12489, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2415_m12489_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12489_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12490_GM;
MethodInfo m12490_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12490, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12490_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12491_GM;
MethodInfo m12491_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12491, &t2415_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12491_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12492_GM;
MethodInfo m12492_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12492, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12492_GM};
extern Il2CppType t2420_0_0_0;
extern Il2CppType t2420_0_0_0;
static ParameterInfo t2415_m12493_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12493_GM;
MethodInfo m12493_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m12493, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t2420, t2415_m12493_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12493_GM};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t2415_m12494_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12494_GM;
MethodInfo m12494_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m12494, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t2420, t2415_m12494_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12494_GM};
extern Il2CppType t2419_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12495_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2419_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12495_GM;
MethodInfo m12495_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12495, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2415_m12495_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12495_GM};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t2415_m12496_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12496_GM;
MethodInfo m12496_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m12496, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t2420, t2415_m12496_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12496_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12497_GM;
MethodInfo m12497_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12497, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2415_m12497_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12497_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12498_GM;
MethodInfo m12498_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12498, &t2415_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12498_GM};
extern Il2CppType t2421_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12499_GM;
MethodInfo m12499_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12499, &t2415_TI, &t2421_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12499_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12500_GM;
MethodInfo m12500_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12500, &t2415_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12500_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12501_GM;
MethodInfo m12501_MI = 
{
	"get_Count", (methodPointerType)&m12501, &t2415_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12501_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12502_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12502_GM;
MethodInfo m12502_MI = 
{
	"get_Item", (methodPointerType)&m12502, &t2415_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2415_m12502_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12502_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12503_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12503_GM;
MethodInfo m12503_MI = 
{
	"set_Item", (methodPointerType)&m12503, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2415_m12503_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12503_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2416_0_0_0;
static ParameterInfo t2415_m12504_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12504_GM;
MethodInfo m12504_MI = 
{
	"Init", (methodPointerType)&m12504, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2415_m12504_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12504_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12505_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12505_GM;
MethodInfo m12505_MI = 
{
	"InitArrays", (methodPointerType)&m12505, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2415_m12505_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12505_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12506_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12506_GM;
MethodInfo m12506_MI = 
{
	"CopyToCheck", (methodPointerType)&m12506, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2415_m12506_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12506_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6652_0_0_0;
extern Il2CppType t6652_0_0_0;
static ParameterInfo t2415_m27603_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6652_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27603_IGC;
extern TypeInfo m27603_gp_TRet_0_TI;
Il2CppGenericParamFull m27603_gp_TRet_0_TI_GenericParamFull = { { &m27603_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27603_gp_TElem_1_TI;
Il2CppGenericParamFull m27603_gp_TElem_1_TI_GenericParamFull = { { &m27603_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27603_IGPA[2] = 
{
	&m27603_gp_TRet_0_TI_GenericParamFull,
	&m27603_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27603_MI;
Il2CppGenericContainer m27603_IGC = { { NULL, NULL }, NULL, &m27603_MI, 2, 1, m27603_IGPA };
extern Il2CppGenericMethod m27604_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m27603_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27604_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27603_GM;
MethodInfo m27603_MI = 
{
	"Do_CopyTo", NULL, &t2415_TI, &t21_0_0_0, NULL, t2415_m27603_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27603_RGCTXData, (methodPointerType)NULL, &m27603_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12507_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12507_GM;
MethodInfo m12507_MI = 
{
	"make_pair", (methodPointerType)&m12507, &t2415_TI, &t2420_0_0_0, RuntimeInvoker_t2420_t29_t44, t2415_m12507_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12507_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12508_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12508_GM;
MethodInfo m12508_MI = 
{
	"pick_value", (methodPointerType)&m12508, &t2415_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t2415_m12508_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12508_GM};
extern Il2CppType t2419_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12509_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2419_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12509_GM;
MethodInfo m12509_MI = 
{
	"CopyTo", (methodPointerType)&m12509, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2415_m12509_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12509_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6655_0_0_0;
extern Il2CppType t6655_0_0_0;
static ParameterInfo t2415_m27605_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6655_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27605_IGC;
extern TypeInfo m27605_gp_TRet_0_TI;
Il2CppGenericParamFull m27605_gp_TRet_0_TI_GenericParamFull = { { &m27605_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27605_IGPA[1] = 
{
	&m27605_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27605_MI;
Il2CppGenericContainer m27605_IGC = { { NULL, NULL }, NULL, &m27605_MI, 1, 1, m27605_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m27606_GM;
static Il2CppRGCTXDefinition m27605_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27606_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27605_GM;
MethodInfo m27605_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t2415_TI, &t21_0_0_0, NULL, t2415_m27605_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27605_RGCTXData, (methodPointerType)NULL, &m27605_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12510_GM;
MethodInfo m12510_MI = 
{
	"Resize", (methodPointerType)&m12510, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12510_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12511_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12511_GM;
MethodInfo m12511_MI = 
{
	"Add", (methodPointerType)&m12511, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2415_m12511_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12511_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12512_GM;
MethodInfo m12512_MI = 
{
	"Clear", (methodPointerType)&m12512, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12512_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12513_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12513_GM;
MethodInfo m12513_MI = 
{
	"ContainsKey", (methodPointerType)&m12513, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2415_m12513_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12513_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12514_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12514_GM;
MethodInfo m12514_MI = 
{
	"ContainsValue", (methodPointerType)&m12514, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2415_m12514_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12514_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t2415_m12515_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12515_GM;
MethodInfo m12515_MI = 
{
	"GetObjectData", (methodPointerType)&m12515, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t2415_m12515_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12515_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12516_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12516_GM;
MethodInfo m12516_MI = 
{
	"OnDeserialization", (methodPointerType)&m12516, &t2415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2415_m12516_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12516_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12517_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12517_GM;
MethodInfo m12517_MI = 
{
	"Remove", (methodPointerType)&m12517, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2415_m12517_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12517_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_0;
static ParameterInfo t2415_m12518_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t390 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12518_GM;
MethodInfo m12518_MI = 
{
	"TryGetValue", (methodPointerType)&m12518, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t390, t2415_m12518_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12518_GM};
extern Il2CppType t2418_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12519_GM;
MethodInfo m12519_MI = 
{
	"get_Values", (methodPointerType)&m12519, &t2415_TI, &t2418_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12519_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12520_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12520_GM;
MethodInfo m12520_MI = 
{
	"ToTKey", (methodPointerType)&m12520, &t2415_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2415_m12520_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12520_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2415_m12521_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12521_GM;
MethodInfo m12521_MI = 
{
	"ToTValue", (methodPointerType)&m12521, &t2415_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2415_m12521_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12521_GM};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t2415_m12522_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12522_GM;
MethodInfo m12522_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m12522, &t2415_TI, &t40_0_0_0, RuntimeInvoker_t40_t2420, t2415_m12522_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12522_GM};
extern Il2CppType t2422_0_0_0;
extern void* RuntimeInvoker_t2422 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12523_GM;
MethodInfo m12523_MI = 
{
	"GetEnumerator", (methodPointerType)&m12523, &t2415_TI, &t2422_0_0_0, RuntimeInvoker_t2422, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12523_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2415_m12524_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m12524_GM;
MethodInfo m12524_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12524, &t2415_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t2415_m12524_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12524_GM};
static MethodInfo* t2415_MIs[] =
{
	&m12482_MI,
	&m12483_MI,
	&m12484_MI,
	&m12485_MI,
	&m12486_MI,
	&m12487_MI,
	&m12488_MI,
	&m12489_MI,
	&m12490_MI,
	&m12491_MI,
	&m12492_MI,
	&m12493_MI,
	&m12494_MI,
	&m12495_MI,
	&m12496_MI,
	&m12497_MI,
	&m12498_MI,
	&m12499_MI,
	&m12500_MI,
	&m12501_MI,
	&m12502_MI,
	&m12503_MI,
	&m12504_MI,
	&m12505_MI,
	&m12506_MI,
	&m27603_MI,
	&m12507_MI,
	&m12508_MI,
	&m12509_MI,
	&m27605_MI,
	&m12510_MI,
	&m12511_MI,
	&m12512_MI,
	&m12513_MI,
	&m12514_MI,
	&m12515_MI,
	&m12516_MI,
	&m12517_MI,
	&m12518_MI,
	&m12519_MI,
	&m12520_MI,
	&m12521_MI,
	&m12522_MI,
	&m12523_MI,
	&m12524_MI,
	NULL
};
static MethodInfo* t2415_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12498_MI,
	&m12515_MI,
	&m12501_MI,
	&m12490_MI,
	&m12491_MI,
	&m12497_MI,
	&m12501_MI,
	&m12492_MI,
	&m12493_MI,
	&m12512_MI,
	&m12494_MI,
	&m12495_MI,
	&m12496_MI,
	&m12499_MI,
	&m12517_MI,
	&m12486_MI,
	&m12487_MI,
	&m12488_MI,
	&m12500_MI,
	&m12489_MI,
	&m12516_MI,
	&m12502_MI,
	&m12503_MI,
	&m12511_MI,
	&m12513_MI,
	&m12515_MI,
	&m12516_MI,
	&m12518_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t5295_TI;
extern TypeInfo t5297_TI;
extern TypeInfo t6657_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t2415_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5295_TI,
	&t5297_TI,
	&t6657_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t2415_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5295_TI, 10},
	{ &t5297_TI, 17},
	{ &t6657_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2415_0_0_0;
extern Il2CppType t2415_1_0_0;
struct t2415;
extern Il2CppGenericClass t2415_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t2415_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t2415_MIs, t2415_PIs, t2415_FIs, NULL, &t29_TI, NULL, NULL, &t2415_TI, t2415_ITIs, t2415_VT, &t1254__CustomAttributeCache, &t2415_TI, &t2415_0_0_0, &t2415_1_0_0, t2415_IOs, &t2415_GC, NULL, t2415_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2415), 0, -1, sizeof(t2415_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
extern MethodInfo m27607_MI;
static PropertyInfo t5295____Count_PropertyInfo = 
{
	&t5295_TI, "Count", &m27607_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27608_MI;
static PropertyInfo t5295____IsReadOnly_PropertyInfo = 
{
	&t5295_TI, "IsReadOnly", &m27608_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5295_PIs[] =
{
	&t5295____Count_PropertyInfo,
	&t5295____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27607_GM;
MethodInfo m27607_MI = 
{
	"get_Count", NULL, &t5295_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27607_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27608_GM;
MethodInfo m27608_MI = 
{
	"get_IsReadOnly", NULL, &t5295_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27608_GM};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t5295_m27609_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27609_GM;
MethodInfo m27609_MI = 
{
	"Add", NULL, &t5295_TI, &t21_0_0_0, RuntimeInvoker_t21_t2420, t5295_m27609_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27609_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27610_GM;
MethodInfo m27610_MI = 
{
	"Clear", NULL, &t5295_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27610_GM};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t5295_m27611_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27611_GM;
MethodInfo m27611_MI = 
{
	"Contains", NULL, &t5295_TI, &t40_0_0_0, RuntimeInvoker_t40_t2420, t5295_m27611_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27611_GM};
extern Il2CppType t2419_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5295_m27612_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2419_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27612_GM;
MethodInfo m27612_MI = 
{
	"CopyTo", NULL, &t5295_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5295_m27612_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27612_GM};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t5295_m27613_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27613_GM;
MethodInfo m27613_MI = 
{
	"Remove", NULL, &t5295_TI, &t40_0_0_0, RuntimeInvoker_t40_t2420, t5295_m27613_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27613_GM};
static MethodInfo* t5295_MIs[] =
{
	&m27607_MI,
	&m27608_MI,
	&m27609_MI,
	&m27610_MI,
	&m27611_MI,
	&m27612_MI,
	&m27613_MI,
	NULL
};
static TypeInfo* t5295_ITIs[] = 
{
	&t603_TI,
	&t5297_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5295_0_0_0;
extern Il2CppType t5295_1_0_0;
struct t5295;
extern Il2CppGenericClass t5295_GC;
TypeInfo t5295_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5295_MIs, t5295_PIs, NULL, NULL, NULL, NULL, NULL, &t5295_TI, t5295_ITIs, NULL, &EmptyCustomAttributesCache, &t5295_TI, &t5295_0_0_0, &t5295_1_0_0, NULL, &t5295_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
extern Il2CppType t2421_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27614_GM;
MethodInfo m27614_MI = 
{
	"GetEnumerator", NULL, &t5297_TI, &t2421_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27614_GM};
static MethodInfo* t5297_MIs[] =
{
	&m27614_MI,
	NULL
};
static TypeInfo* t5297_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5297_0_0_0;
extern Il2CppType t5297_1_0_0;
struct t5297;
extern Il2CppGenericClass t5297_GC;
TypeInfo t5297_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5297_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5297_TI, t5297_ITIs, NULL, &EmptyCustomAttributesCache, &t5297_TI, &t5297_0_0_0, &t5297_1_0_0, NULL, &t5297_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2421_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
extern MethodInfo m27615_MI;
static PropertyInfo t2421____Current_PropertyInfo = 
{
	&t2421_TI, "Current", &m27615_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2421_PIs[] =
{
	&t2421____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27615_GM;
MethodInfo m27615_MI = 
{
	"get_Current", NULL, &t2421_TI, &t2420_0_0_0, RuntimeInvoker_t2420, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27615_GM};
static MethodInfo* t2421_MIs[] =
{
	&m27615_MI,
	NULL
};
static TypeInfo* t2421_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2421_0_0_0;
extern Il2CppType t2421_1_0_0;
struct t2421;
extern Il2CppGenericClass t2421_GC;
TypeInfo t2421_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2421_MIs, t2421_PIs, NULL, NULL, NULL, NULL, NULL, &t2421_TI, t2421_ITIs, NULL, &EmptyCustomAttributesCache, &t2421_TI, &t2421_0_0_0, &t2421_1_0_0, NULL, &t2421_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t446_TI;
extern MethodInfo m12527_MI;
extern MethodInfo m12529_MI;
extern MethodInfo m4008_MI;


 void m12525 (t2420 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		m12527(__this, p0, &m12527_MI);
		m12529(__this, p1, &m12529_MI);
		return;
	}
}
 t29 * m12526 (t2420 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
 void m12527 (t2420 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 int32_t m12528 (t2420 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 void m12529 (t2420 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m12530_MI;
 t7* m12530 (t2420 * __this, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t29 * L_2 = m12526(__this, &m12526_MI);
		t29 * L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t29 *)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t29 * L_4 = m12526(__this, &m12526_MI);
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		int32_t L_8 = m12528(__this, &m12528_MI);
		int32_t L_9 = L_8;
		t29 * L_10 = Box(InitializedTypeInfo(&t44_TI), &L_9);
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_10)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		int32_t L_11 = m12528(__this, &m12528_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&V_1))));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
extern Il2CppType t29_0_0_1;
FieldInfo t2420_f0_FieldInfo = 
{
	"key", &t29_0_0_1, &t2420_TI, offsetof(t2420, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2420_f1_FieldInfo = 
{
	"value", &t44_0_0_1, &t2420_TI, offsetof(t2420, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2420_FIs[] =
{
	&t2420_f0_FieldInfo,
	&t2420_f1_FieldInfo,
	NULL
};
static PropertyInfo t2420____Key_PropertyInfo = 
{
	&t2420_TI, "Key", &m12526_MI, &m12527_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2420____Value_PropertyInfo = 
{
	&t2420_TI, "Value", &m12528_MI, &m12529_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2420_PIs[] =
{
	&t2420____Key_PropertyInfo,
	&t2420____Value_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2420_m12525_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12525_GM;
MethodInfo m12525_MI = 
{
	".ctor", (methodPointerType)&m12525, &t2420_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2420_m12525_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12525_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12526_GM;
MethodInfo m12526_MI = 
{
	"get_Key", (methodPointerType)&m12526, &t2420_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12526_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2420_m12527_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12527_GM;
MethodInfo m12527_MI = 
{
	"set_Key", (methodPointerType)&m12527, &t2420_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2420_m12527_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12527_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12528_GM;
MethodInfo m12528_MI = 
{
	"get_Value", (methodPointerType)&m12528, &t2420_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12528_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2420_m12529_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12529_GM;
MethodInfo m12529_MI = 
{
	"set_Value", (methodPointerType)&m12529, &t2420_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2420_m12529_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12529_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12530_GM;
MethodInfo m12530_MI = 
{
	"ToString", (methodPointerType)&m12530, &t2420_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12530_GM};
static MethodInfo* t2420_MIs[] =
{
	&m12525_MI,
	&m12526_MI,
	&m12527_MI,
	&m12528_MI,
	&m12529_MI,
	&m12530_MI,
	NULL
};
static MethodInfo* t2420_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m12530_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2420_1_0_0;
extern Il2CppGenericClass t2420_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2420_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2420_MIs, t2420_PIs, t2420_FIs, NULL, &t110_TI, NULL, NULL, &t2420_TI, NULL, t2420_VT, &t1259__CustomAttributeCache, &t2420_TI, &t2420_0_0_0, &t2420_1_0_0, NULL, &t2420_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2420)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2423.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2423_TI;
#include "t2423MD.h"

extern MethodInfo m12535_MI;
extern MethodInfo m20718_MI;
struct t20;
 t2420  m20718 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12531_MI;
 void m12531 (t2423 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12532_MI;
 t29 * m12532 (t2423 * __this, MethodInfo* method){
	{
		t2420  L_0 = m12535(__this, &m12535_MI);
		t2420  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2420_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12533_MI;
 void m12533 (t2423 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12534_MI;
 bool m12534 (t2423 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2420  m12535 (t2423 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2420  L_8 = m20718(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20718_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
extern Il2CppType t20_0_0_1;
FieldInfo t2423_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2423_TI, offsetof(t2423, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2423_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2423_TI, offsetof(t2423, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2423_FIs[] =
{
	&t2423_f0_FieldInfo,
	&t2423_f1_FieldInfo,
	NULL
};
static PropertyInfo t2423____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2423_TI, "System.Collections.IEnumerator.Current", &m12532_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2423____Current_PropertyInfo = 
{
	&t2423_TI, "Current", &m12535_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2423_PIs[] =
{
	&t2423____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2423____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2423_m12531_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12531_GM;
MethodInfo m12531_MI = 
{
	".ctor", (methodPointerType)&m12531, &t2423_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2423_m12531_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12531_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12532_GM;
MethodInfo m12532_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12532, &t2423_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12532_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12533_GM;
MethodInfo m12533_MI = 
{
	"Dispose", (methodPointerType)&m12533, &t2423_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12533_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12534_GM;
MethodInfo m12534_MI = 
{
	"MoveNext", (methodPointerType)&m12534, &t2423_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12534_GM};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12535_GM;
MethodInfo m12535_MI = 
{
	"get_Current", (methodPointerType)&m12535, &t2423_TI, &t2420_0_0_0, RuntimeInvoker_t2420, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12535_GM};
static MethodInfo* t2423_MIs[] =
{
	&m12531_MI,
	&m12532_MI,
	&m12533_MI,
	&m12534_MI,
	&m12535_MI,
	NULL
};
static MethodInfo* t2423_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12532_MI,
	&m12534_MI,
	&m12533_MI,
	&m12535_MI,
};
static TypeInfo* t2423_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2421_TI,
};
static Il2CppInterfaceOffsetPair t2423_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2421_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2423_0_0_0;
extern Il2CppType t2423_1_0_0;
extern Il2CppGenericClass t2423_GC;
TypeInfo t2423_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2423_MIs, t2423_PIs, t2423_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2423_TI, t2423_ITIs, t2423_VT, &EmptyCustomAttributesCache, &t2423_TI, &t2423_0_0_0, &t2423_1_0_0, t2423_IOs, &t2423_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2423)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5296_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
extern MethodInfo m27616_MI;
extern MethodInfo m27617_MI;
static PropertyInfo t5296____Item_PropertyInfo = 
{
	&t5296_TI, "Item", &m27616_MI, &m27617_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5296_PIs[] =
{
	&t5296____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2420_0_0_0;
static ParameterInfo t5296_m27618_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27618_GM;
MethodInfo m27618_MI = 
{
	"IndexOf", NULL, &t5296_TI, &t44_0_0_0, RuntimeInvoker_t44_t2420, t5296_m27618_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27618_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2420_0_0_0;
static ParameterInfo t5296_m27619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27619_GM;
MethodInfo m27619_MI = 
{
	"Insert", NULL, &t5296_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2420, t5296_m27619_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27619_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5296_m27620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27620_GM;
MethodInfo m27620_MI = 
{
	"RemoveAt", NULL, &t5296_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5296_m27620_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27620_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5296_m27616_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27616_GM;
MethodInfo m27616_MI = 
{
	"get_Item", NULL, &t5296_TI, &t2420_0_0_0, RuntimeInvoker_t2420_t44, t5296_m27616_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27616_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2420_0_0_0;
static ParameterInfo t5296_m27617_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2420_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27617_GM;
MethodInfo m27617_MI = 
{
	"set_Item", NULL, &t5296_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2420, t5296_m27617_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27617_GM};
static MethodInfo* t5296_MIs[] =
{
	&m27618_MI,
	&m27619_MI,
	&m27620_MI,
	&m27616_MI,
	&m27617_MI,
	NULL
};
static TypeInfo* t5296_ITIs[] = 
{
	&t603_TI,
	&t5295_TI,
	&t5297_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5296_0_0_0;
extern Il2CppType t5296_1_0_0;
struct t5296;
extern Il2CppGenericClass t5296_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5296_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5296_MIs, t5296_PIs, NULL, NULL, NULL, NULL, NULL, &t5296_TI, t5296_ITIs, NULL, &t1908__CustomAttributeCache, &t5296_TI, &t5296_0_0_0, &t5296_1_0_0, NULL, &t5296_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.Object,System.Int32>
extern Il2CppType t29_0_0_0;
static ParameterInfo t6657_m27621_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27621_GM;
MethodInfo m27621_MI = 
{
	"Remove", NULL, &t6657_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6657_m27621_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27621_GM};
static MethodInfo* t6657_MIs[] =
{
	&m27621_MI,
	NULL
};
static TypeInfo* t6657_ITIs[] = 
{
	&t603_TI,
	&t5295_TI,
	&t5297_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6657_0_0_0;
extern Il2CppType t6657_1_0_0;
struct t6657;
extern Il2CppGenericClass t6657_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6657_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6657_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6657_TI, t6657_ITIs, NULL, &t1975__CustomAttributeCache, &t6657_TI, &t6657_0_0_0, &t6657_1_0_0, NULL, &t6657_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2425.h"
#include "t2426.h"
extern TypeInfo t2425_TI;
extern TypeInfo t2426_TI;
#include "t2426MD.h"
#include "t2425MD.h"
extern MethodInfo m9816_MI;
extern MethodInfo m12548_MI;
extern MethodInfo m12547_MI;
extern MethodInfo m12567_MI;
extern MethodInfo m20729_MI;
extern MethodInfo m20730_MI;
extern MethodInfo m12550_MI;
struct t2415;
 void m20729 (t2415 * __this, t20 * p0, int32_t p1, t2426 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t2415;
 void m20730 (t2415 * __this, t841* p0, int32_t p1, t2426 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12536 (t2418 * __this, t2415 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m12537_MI;
 void m12537 (t2418 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12538_MI;
 void m12538 (t2418 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12539_MI;
 bool m12539 (t2418 * __this, int32_t p0, MethodInfo* method){
	{
		t2415 * L_0 = (__this->f0);
		bool L_1 = m12514(L_0, p0, &m12514_MI);
		return L_1;
	}
}
extern MethodInfo m12540_MI;
 bool m12540 (t2418 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12541_MI;
 t29* m12541 (t2418 * __this, MethodInfo* method){
	{
		t2425  L_0 = m12548(__this, &m12548_MI);
		t2425  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2425_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12542_MI;
 void m12542 (t2418 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t841* V_0 = {0};
	{
		V_0 = ((t841*)IsInst(p0, InitializedTypeInfo(&t841_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t841*, int32_t >::Invoke(&m12547_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t2415 * L_0 = (__this->f0);
		m12506(L_0, p0, p1, &m12506_MI);
		t2415 * L_1 = (__this->f0);
		t35 L_2 = { &m12508_MI };
		t2426 * L_3 = (t2426 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2426_TI));
		m12567(L_3, NULL, L_2, &m12567_MI);
		m20729(L_1, p0, p1, L_3, &m20729_MI);
		return;
	}
}
extern MethodInfo m12543_MI;
 t29 * m12543 (t2418 * __this, MethodInfo* method){
	{
		t2425  L_0 = m12548(__this, &m12548_MI);
		t2425  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2425_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12544_MI;
 bool m12544 (t2418 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m12545_MI;
 bool m12545 (t2418 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12546_MI;
 t29 * m12546 (t2418 * __this, MethodInfo* method){
	{
		t2415 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m12547 (t2418 * __this, t841* p0, int32_t p1, MethodInfo* method){
	{
		t2415 * L_0 = (__this->f0);
		m12506(L_0, (t20 *)(t20 *)p0, p1, &m12506_MI);
		t2415 * L_1 = (__this->f0);
		t35 L_2 = { &m12508_MI };
		t2426 * L_3 = (t2426 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2426_TI));
		m12567(L_3, NULL, L_2, &m12567_MI);
		m20730(L_1, p0, p1, L_3, &m20730_MI);
		return;
	}
}
 t2425  m12548 (t2418 * __this, MethodInfo* method){
	{
		t2415 * L_0 = (__this->f0);
		t2425  L_1 = {0};
		m12550(&L_1, L_0, &m12550_MI);
		return L_1;
	}
}
extern MethodInfo m12549_MI;
 int32_t m12549 (t2418 * __this, MethodInfo* method){
	{
		t2415 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m12501_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
extern Il2CppType t2415_0_0_1;
FieldInfo t2418_f0_FieldInfo = 
{
	"dictionary", &t2415_0_0_1, &t2418_TI, offsetof(t2418, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2418_FIs[] =
{
	&t2418_f0_FieldInfo,
	NULL
};
static PropertyInfo t2418____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2418_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m12544_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2418____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2418_TI, "System.Collections.ICollection.IsSynchronized", &m12545_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2418____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2418_TI, "System.Collections.ICollection.SyncRoot", &m12546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2418____Count_PropertyInfo = 
{
	&t2418_TI, "Count", &m12549_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2418_PIs[] =
{
	&t2418____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2418____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2418____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2418____Count_PropertyInfo,
	NULL
};
extern Il2CppType t2415_0_0_0;
static ParameterInfo t2418_m12536_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t2415_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12536_GM;
MethodInfo m12536_MI = 
{
	".ctor", (methodPointerType)&m12536, &t2418_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2418_m12536_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12536_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2418_m12537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12537_GM;
MethodInfo m12537_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12537, &t2418_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2418_m12537_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12537_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12538_GM;
MethodInfo m12538_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12538, &t2418_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12538_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2418_m12539_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12539_GM;
MethodInfo m12539_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12539, &t2418_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2418_m12539_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12539_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2418_m12540_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12540_GM;
MethodInfo m12540_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12540, &t2418_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2418_m12540_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12540_GM};
extern Il2CppType t2424_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12541_GM;
MethodInfo m12541_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12541, &t2418_TI, &t2424_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12541_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2418_m12542_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12542_GM;
MethodInfo m12542_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12542, &t2418_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2418_m12542_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12542_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12543_GM;
MethodInfo m12543_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12543, &t2418_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12543_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12544_GM;
MethodInfo m12544_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12544, &t2418_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12544_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12545_GM;
MethodInfo m12545_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12545, &t2418_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12545_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12546_GM;
MethodInfo m12546_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12546, &t2418_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12546_GM};
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2418_m12547_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12547_GM;
MethodInfo m12547_MI = 
{
	"CopyTo", (methodPointerType)&m12547, &t2418_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2418_m12547_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12547_GM};
extern Il2CppType t2425_0_0_0;
extern void* RuntimeInvoker_t2425 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12548_GM;
MethodInfo m12548_MI = 
{
	"GetEnumerator", (methodPointerType)&m12548, &t2418_TI, &t2425_0_0_0, RuntimeInvoker_t2425, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12548_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12549_GM;
MethodInfo m12549_MI = 
{
	"get_Count", (methodPointerType)&m12549, &t2418_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12549_GM};
static MethodInfo* t2418_MIs[] =
{
	&m12536_MI,
	&m12537_MI,
	&m12538_MI,
	&m12539_MI,
	&m12540_MI,
	&m12541_MI,
	&m12542_MI,
	&m12543_MI,
	&m12544_MI,
	&m12545_MI,
	&m12546_MI,
	&m12547_MI,
	&m12548_MI,
	&m12549_MI,
	NULL
};
static MethodInfo* t2418_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12543_MI,
	&m12549_MI,
	&m12545_MI,
	&m12546_MI,
	&m12542_MI,
	&m12549_MI,
	&m12544_MI,
	&m12537_MI,
	&m12538_MI,
	&m12539_MI,
	&m12547_MI,
	&m12540_MI,
	&m12541_MI,
};
extern TypeInfo t5140_TI;
extern TypeInfo t5142_TI;
static TypeInfo* t2418_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5140_TI,
	&t5142_TI,
};
static Il2CppInterfaceOffsetPair t2418_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5140_TI, 9},
	{ &t5142_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2418_0_0_0;
extern Il2CppType t2418_1_0_0;
struct t2418;
extern Il2CppGenericClass t2418_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2418_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2418_MIs, t2418_PIs, t2418_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2418_TI, t2418_ITIs, t2418_VT, &t1252__CustomAttributeCache, &t2418_TI, &t2418_0_0_0, &t2418_1_0_0, t2418_IOs, &t2418_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2418), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12563_MI;
extern MethodInfo m12566_MI;
extern MethodInfo m12560_MI;


 void m12550 (t2425 * __this, t2415 * p0, MethodInfo* method){
	{
		t2422  L_0 = m12523(p0, &m12523_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12551_MI;
 t29 * m12551 (t2425 * __this, MethodInfo* method){
	{
		t2422 * L_0 = &(__this->f0);
		int32_t L_1 = m12563(L_0, &m12563_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m12552_MI;
 void m12552 (t2425 * __this, MethodInfo* method){
	{
		t2422 * L_0 = &(__this->f0);
		m12566(L_0, &m12566_MI);
		return;
	}
}
extern MethodInfo m12553_MI;
 bool m12553 (t2425 * __this, MethodInfo* method){
	{
		t2422 * L_0 = &(__this->f0);
		bool L_1 = m12560(L_0, &m12560_MI);
		return L_1;
	}
}
extern MethodInfo m12554_MI;
 int32_t m12554 (t2425 * __this, MethodInfo* method){
	{
		t2422 * L_0 = &(__this->f0);
		t2420 * L_1 = &(L_0->f3);
		int32_t L_2 = m12528(L_1, &m12528_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
extern Il2CppType t2422_0_0_1;
FieldInfo t2425_f0_FieldInfo = 
{
	"host_enumerator", &t2422_0_0_1, &t2425_TI, offsetof(t2425, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2425_FIs[] =
{
	&t2425_f0_FieldInfo,
	NULL
};
static PropertyInfo t2425____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2425_TI, "System.Collections.IEnumerator.Current", &m12551_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2425____Current_PropertyInfo = 
{
	&t2425_TI, "Current", &m12554_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2425_PIs[] =
{
	&t2425____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2425____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2415_0_0_0;
static ParameterInfo t2425_m12550_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t2415_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12550_GM;
MethodInfo m12550_MI = 
{
	".ctor", (methodPointerType)&m12550, &t2425_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2425_m12550_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12550_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12551_GM;
MethodInfo m12551_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12551, &t2425_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12551_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12552_GM;
MethodInfo m12552_MI = 
{
	"Dispose", (methodPointerType)&m12552, &t2425_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12552_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12553_GM;
MethodInfo m12553_MI = 
{
	"MoveNext", (methodPointerType)&m12553, &t2425_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12553_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12554_GM;
MethodInfo m12554_MI = 
{
	"get_Current", (methodPointerType)&m12554, &t2425_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12554_GM};
static MethodInfo* t2425_MIs[] =
{
	&m12550_MI,
	&m12551_MI,
	&m12552_MI,
	&m12553_MI,
	&m12554_MI,
	NULL
};
static MethodInfo* t2425_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12551_MI,
	&m12553_MI,
	&m12552_MI,
	&m12554_MI,
};
extern TypeInfo t2424_TI;
static TypeInfo* t2425_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2424_TI,
};
static Il2CppInterfaceOffsetPair t2425_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2424_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2425_0_0_0;
extern Il2CppType t2425_1_0_0;
extern Il2CppGenericClass t2425_GC;
extern TypeInfo t1252_TI;
TypeInfo t2425_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2425_MIs, t2425_PIs, t2425_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2425_TI, t2425_ITIs, t2425_VT, &EmptyCustomAttributesCache, &t2425_TI, &t2425_0_0_0, &t2425_1_0_0, t2425_IOs, &t2425_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2425)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m12565_MI;
extern MethodInfo m12562_MI;
extern MethodInfo m12564_MI;
extern MethodInfo m5150_MI;


 void m12555 (t2422 * __this, t2415 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m12556_MI;
 t29 * m12556 (t2422 * __this, MethodInfo* method){
	{
		m12565(__this, &m12565_MI);
		t2420  L_0 = (__this->f3);
		t2420  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2420_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12557_MI;
 t725  m12557 (t2422 * __this, MethodInfo* method){
	{
		m12565(__this, &m12565_MI);
		t2420 * L_0 = &(__this->f3);
		t29 * L_1 = m12526(L_0, &m12526_MI);
		t29 * L_2 = L_1;
		t2420 * L_3 = &(__this->f3);
		int32_t L_4 = m12528(L_3, &m12528_MI);
		int32_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t44_TI), &L_5);
		t725  L_7 = {0};
		m3965(&L_7, ((t29 *)L_2), L_6, &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m12558_MI;
 t29 * m12558 (t2422 * __this, MethodInfo* method){
	{
		t29 * L_0 = m12562(__this, &m12562_MI);
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m12559_MI;
 t29 * m12559 (t2422 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12563(__this, &m12563_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
 bool m12560 (t2422 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m12564(__this, &m12564_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t2415 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t2415 * L_6 = (__this->f0);
		t316* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t2415 * L_9 = (__this->f0);
		t841* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t2420  L_12 = {0};
		m12525(&L_12, (*(t29 **)(t29 **)SZArrayLdElema(L_7, L_8)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_11)), &m12525_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t2415 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m12561_MI;
 t2420  m12561 (t2422 * __this, MethodInfo* method){
	{
		t2420  L_0 = (__this->f3);
		return L_0;
	}
}
 t29 * m12562 (t2422 * __this, MethodInfo* method){
	{
		m12565(__this, &m12565_MI);
		t2420 * L_0 = &(__this->f3);
		t29 * L_1 = m12526(L_0, &m12526_MI);
		return L_1;
	}
}
 int32_t m12563 (t2422 * __this, MethodInfo* method){
	{
		m12565(__this, &m12565_MI);
		t2420 * L_0 = &(__this->f3);
		int32_t L_1 = m12528(L_0, &m12528_MI);
		return L_1;
	}
}
 void m12564 (t2422 * __this, MethodInfo* method){
	{
		t2415 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t2415 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m12565 (t2422 * __this, MethodInfo* method){
	{
		m12564(__this, &m12564_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m12566 (t2422 * __this, MethodInfo* method){
	{
		__this->f0 = (t2415 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
extern Il2CppType t2415_0_0_1;
FieldInfo t2422_f0_FieldInfo = 
{
	"dictionary", &t2415_0_0_1, &t2422_TI, offsetof(t2422, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2422_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2422_TI, offsetof(t2422, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2422_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2422_TI, offsetof(t2422, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2420_0_0_3;
FieldInfo t2422_f3_FieldInfo = 
{
	"current", &t2420_0_0_3, &t2422_TI, offsetof(t2422, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2422_FIs[] =
{
	&t2422_f0_FieldInfo,
	&t2422_f1_FieldInfo,
	&t2422_f2_FieldInfo,
	&t2422_f3_FieldInfo,
	NULL
};
static PropertyInfo t2422____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2422_TI, "System.Collections.IEnumerator.Current", &m12556_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2422____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2422_TI, "System.Collections.IDictionaryEnumerator.Entry", &m12557_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2422____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2422_TI, "System.Collections.IDictionaryEnumerator.Key", &m12558_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2422____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2422_TI, "System.Collections.IDictionaryEnumerator.Value", &m12559_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2422____Current_PropertyInfo = 
{
	&t2422_TI, "Current", &m12561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2422____CurrentKey_PropertyInfo = 
{
	&t2422_TI, "CurrentKey", &m12562_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2422____CurrentValue_PropertyInfo = 
{
	&t2422_TI, "CurrentValue", &m12563_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2422_PIs[] =
{
	&t2422____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2422____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2422____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2422____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2422____Current_PropertyInfo,
	&t2422____CurrentKey_PropertyInfo,
	&t2422____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t2415_0_0_0;
static ParameterInfo t2422_m12555_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t2415_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12555_GM;
MethodInfo m12555_MI = 
{
	".ctor", (methodPointerType)&m12555, &t2422_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2422_m12555_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12555_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12556_GM;
MethodInfo m12556_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12556, &t2422_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12556_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12557_GM;
MethodInfo m12557_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12557, &t2422_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12557_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12558_GM;
MethodInfo m12558_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12558, &t2422_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12558_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12559_GM;
MethodInfo m12559_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12559, &t2422_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12559_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12560_GM;
MethodInfo m12560_MI = 
{
	"MoveNext", (methodPointerType)&m12560, &t2422_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12560_GM};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12561_GM;
MethodInfo m12561_MI = 
{
	"get_Current", (methodPointerType)&m12561, &t2422_TI, &t2420_0_0_0, RuntimeInvoker_t2420, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12561_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12562_GM;
MethodInfo m12562_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12562, &t2422_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12562_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12563_GM;
MethodInfo m12563_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12563, &t2422_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12563_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12564_GM;
MethodInfo m12564_MI = 
{
	"VerifyState", (methodPointerType)&m12564, &t2422_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12564_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12565_GM;
MethodInfo m12565_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12565, &t2422_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12565_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12566_GM;
MethodInfo m12566_MI = 
{
	"Dispose", (methodPointerType)&m12566, &t2422_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12566_GM};
static MethodInfo* t2422_MIs[] =
{
	&m12555_MI,
	&m12556_MI,
	&m12557_MI,
	&m12558_MI,
	&m12559_MI,
	&m12560_MI,
	&m12561_MI,
	&m12562_MI,
	&m12563_MI,
	&m12564_MI,
	&m12565_MI,
	&m12566_MI,
	NULL
};
static MethodInfo* t2422_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12556_MI,
	&m12560_MI,
	&m12566_MI,
	&m12561_MI,
	&m12557_MI,
	&m12558_MI,
	&m12559_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t2422_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2421_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2422_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2421_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2422_0_0_0;
extern Il2CppType t2422_1_0_0;
extern Il2CppGenericClass t2422_GC;
TypeInfo t2422_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2422_MIs, t2422_PIs, t2422_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2422_TI, t2422_ITIs, t2422_VT, &EmptyCustomAttributesCache, &t2422_TI, &t2422_0_0_0, &t2422_1_0_0, t2422_IOs, &t2422_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2422)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m12567 (t2426 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12568_MI;
 int32_t m12568 (t2426 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12568((t2426 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12569_MI;
 t29 * m12569 (t2426 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12570_MI;
 int32_t m12570 (t2426 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2426_m12567_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12567_GM;
MethodInfo m12567_MI = 
{
	".ctor", (methodPointerType)&m12567, &t2426_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2426_m12567_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12567_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2426_m12568_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12568_GM;
MethodInfo m12568_MI = 
{
	"Invoke", (methodPointerType)&m12568, &t2426_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t2426_m12568_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12568_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2426_m12569_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12569_GM;
MethodInfo m12569_MI = 
{
	"BeginInvoke", (methodPointerType)&m12569, &t2426_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2426_m12569_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12569_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2426_m12570_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12570_GM;
MethodInfo m12570_MI = 
{
	"EndInvoke", (methodPointerType)&m12570, &t2426_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2426_m12570_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12570_GM};
static MethodInfo* t2426_MIs[] =
{
	&m12567_MI,
	&m12568_MI,
	&m12569_MI,
	&m12570_MI,
	NULL
};
static MethodInfo* t2426_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12568_MI,
	&m12569_MI,
	&m12570_MI,
};
static Il2CppInterfaceOffsetPair t2426_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2426_0_0_0;
extern Il2CppType t2426_1_0_0;
struct t2426;
extern Il2CppGenericClass t2426_GC;
TypeInfo t2426_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2426_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2426_TI, NULL, t2426_VT, &EmptyCustomAttributesCache, &t2426_TI, &t2426_0_0_0, &t2426_1_0_0, t2426_IOs, &t2426_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2426), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12571 (t2417 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12572_MI;
 t725  m12572 (t2417 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12572((t2417 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12573_MI;
 t29 * m12573 (t2417 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12574_MI;
 t725  m12574 (t2417 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2417_m12571_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12571_GM;
MethodInfo m12571_MI = 
{
	".ctor", (methodPointerType)&m12571, &t2417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2417_m12571_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12571_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2417_m12572_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12572_GM;
MethodInfo m12572_MI = 
{
	"Invoke", (methodPointerType)&m12572, &t2417_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t2417_m12572_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12572_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2417_m12573_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12573_GM;
MethodInfo m12573_MI = 
{
	"BeginInvoke", (methodPointerType)&m12573, &t2417_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2417_m12573_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12573_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2417_m12574_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12574_GM;
MethodInfo m12574_MI = 
{
	"EndInvoke", (methodPointerType)&m12574, &t2417_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2417_m12574_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12574_GM};
static MethodInfo* t2417_MIs[] =
{
	&m12571_MI,
	&m12572_MI,
	&m12573_MI,
	&m12574_MI,
	NULL
};
static MethodInfo* t2417_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12572_MI,
	&m12573_MI,
	&m12574_MI,
};
static Il2CppInterfaceOffsetPair t2417_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2417_0_0_0;
extern Il2CppType t2417_1_0_0;
struct t2417;
extern Il2CppGenericClass t2417_GC;
TypeInfo t2417_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2417_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2417_TI, NULL, t2417_VT, &EmptyCustomAttributesCache, &t2417_TI, &t2417_0_0_0, &t2417_1_0_0, t2417_IOs, &t2417_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2417), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12575 (t2427 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12576_MI;
 t2420  m12576 (t2427 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12576((t2427 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2420  (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2420  (*FunctionPointerType) (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2420  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12577_MI;
 t29 * m12577 (t2427 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12578_MI;
 t2420  m12578 (t2427 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2420 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2427_m12575_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12575_GM;
MethodInfo m12575_MI = 
{
	".ctor", (methodPointerType)&m12575, &t2427_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2427_m12575_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12575_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2427_m12576_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12576_GM;
MethodInfo m12576_MI = 
{
	"Invoke", (methodPointerType)&m12576, &t2427_TI, &t2420_0_0_0, RuntimeInvoker_t2420_t29_t44, t2427_m12576_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12576_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2427_m12577_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12577_GM;
MethodInfo m12577_MI = 
{
	"BeginInvoke", (methodPointerType)&m12577, &t2427_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2427_m12577_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12577_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2427_m12578_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2420_0_0_0;
extern void* RuntimeInvoker_t2420_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12578_GM;
MethodInfo m12578_MI = 
{
	"EndInvoke", (methodPointerType)&m12578, &t2427_TI, &t2420_0_0_0, RuntimeInvoker_t2420_t29, t2427_m12578_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12578_GM};
static MethodInfo* t2427_MIs[] =
{
	&m12575_MI,
	&m12576_MI,
	&m12577_MI,
	&m12578_MI,
	NULL
};
static MethodInfo* t2427_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12576_MI,
	&m12577_MI,
	&m12578_MI,
};
static Il2CppInterfaceOffsetPair t2427_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2427_0_0_0;
extern Il2CppType t2427_1_0_0;
struct t2427;
extern Il2CppGenericClass t2427_GC;
TypeInfo t2427_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2427_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2427_TI, NULL, t2427_VT, &EmptyCustomAttributesCache, &t2427_TI, &t2427_0_0_0, &t2427_1_0_0, t2427_IOs, &t2427_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2427), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10122_MI;
extern MethodInfo m12581_MI;


 void m12579 (t2428 * __this, t2415 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t2422  L_0 = m12523(p0, &m12523_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12580_MI;
 bool m12580 (t2428 * __this, MethodInfo* method){
	{
		t2422 * L_0 = &(__this->f0);
		bool L_1 = m12560(L_0, &m12560_MI);
		return L_1;
	}
}
 t725  m12581 (t2428 * __this, MethodInfo* method){
	{
		t2422  L_0 = (__this->f0);
		t2422  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2422_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m12582_MI;
 t29 * m12582 (t2428 * __this, MethodInfo* method){
	t2420  V_0 = {0};
	{
		t2422 * L_0 = &(__this->f0);
		t2420  L_1 = m12561(L_0, &m12561_MI);
		V_0 = L_1;
		t29 * L_2 = m12526((&V_0), &m12526_MI);
		t29 * L_3 = L_2;
		return ((t29 *)L_3);
	}
}
extern MethodInfo m12583_MI;
 t29 * m12583 (t2428 * __this, MethodInfo* method){
	t2420  V_0 = {0};
	{
		t2422 * L_0 = &(__this->f0);
		t2420  L_1 = m12561(L_0, &m12561_MI);
		V_0 = L_1;
		int32_t L_2 = m12528((&V_0), &m12528_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m12584_MI;
 t29 * m12584 (t2428 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m12581_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
extern Il2CppType t2422_0_0_1;
FieldInfo t2428_f0_FieldInfo = 
{
	"host_enumerator", &t2422_0_0_1, &t2428_TI, offsetof(t2428, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2428_FIs[] =
{
	&t2428_f0_FieldInfo,
	NULL
};
static PropertyInfo t2428____Entry_PropertyInfo = 
{
	&t2428_TI, "Entry", &m12581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2428____Key_PropertyInfo = 
{
	&t2428_TI, "Key", &m12582_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2428____Value_PropertyInfo = 
{
	&t2428_TI, "Value", &m12583_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2428____Current_PropertyInfo = 
{
	&t2428_TI, "Current", &m12584_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2428_PIs[] =
{
	&t2428____Entry_PropertyInfo,
	&t2428____Key_PropertyInfo,
	&t2428____Value_PropertyInfo,
	&t2428____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2415_0_0_0;
static ParameterInfo t2428_m12579_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t2415_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12579_GM;
MethodInfo m12579_MI = 
{
	".ctor", (methodPointerType)&m12579, &t2428_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2428_m12579_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12579_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12580_GM;
MethodInfo m12580_MI = 
{
	"MoveNext", (methodPointerType)&m12580, &t2428_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12580_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12581_GM;
MethodInfo m12581_MI = 
{
	"get_Entry", (methodPointerType)&m12581, &t2428_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12581_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12582_GM;
MethodInfo m12582_MI = 
{
	"get_Key", (methodPointerType)&m12582, &t2428_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12582_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12583_GM;
MethodInfo m12583_MI = 
{
	"get_Value", (methodPointerType)&m12583, &t2428_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12583_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12584_GM;
MethodInfo m12584_MI = 
{
	"get_Current", (methodPointerType)&m12584, &t2428_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12584_GM};
static MethodInfo* t2428_MIs[] =
{
	&m12579_MI,
	&m12580_MI,
	&m12581_MI,
	&m12582_MI,
	&m12583_MI,
	&m12584_MI,
	NULL
};
static MethodInfo* t2428_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12584_MI,
	&m12580_MI,
	&m12581_MI,
	&m12582_MI,
	&m12583_MI,
};
static TypeInfo* t2428_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2428_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2428_0_0_0;
extern Il2CppType t2428_1_0_0;
struct t2428;
extern Il2CppGenericClass t2428_GC;
TypeInfo t2428_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2428_MIs, t2428_PIs, t2428_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2428_TI, t2428_ITIs, t2428_VT, &EmptyCustomAttributesCache, &t2428_TI, &t2428_0_0_0, &t2428_1_0_0, t2428_IOs, &t2428_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2428), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ICanvasElement>
extern MethodInfo m27622_MI;
extern MethodInfo m27623_MI;
static PropertyInfo t2434____Item_PropertyInfo = 
{
	&t2434_TI, "Item", &m27622_MI, &m27623_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2434_PIs[] =
{
	&t2434____Item_PropertyInfo,
	NULL
};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2434_m27624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27624_GM;
MethodInfo m27624_MI = 
{
	"IndexOf", NULL, &t2434_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2434_m27624_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27624_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2434_m27625_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27625_GM;
MethodInfo m27625_MI = 
{
	"Insert", NULL, &t2434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2434_m27625_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27625_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2434_m27626_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27626_GM;
MethodInfo m27626_MI = 
{
	"RemoveAt", NULL, &t2434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2434_m27626_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27626_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2434_m27622_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27622_GM;
MethodInfo m27622_MI = 
{
	"get_Item", NULL, &t2434_TI, &t145_0_0_0, RuntimeInvoker_t29_t44, t2434_m27622_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27622_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2434_m27623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27623_GM;
MethodInfo m27623_MI = 
{
	"set_Item", NULL, &t2434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2434_m27623_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27623_GM};
static MethodInfo* t2434_MIs[] =
{
	&m27624_MI,
	&m27625_MI,
	&m27626_MI,
	&m27622_MI,
	&m27623_MI,
	NULL
};
static TypeInfo* t2434_ITIs[] = 
{
	&t603_TI,
	&t2430_TI,
	&t2431_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2434_0_0_0;
extern Il2CppType t2434_1_0_0;
struct t2434;
extern Il2CppGenericClass t2434_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2434_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2434_MIs, t2434_PIs, NULL, NULL, NULL, NULL, NULL, &t2434_TI, t2434_ITIs, NULL, &t1908__CustomAttributeCache, &t2434_TI, &t2434_0_0_0, &t2434_1_0_0, NULL, &t2434_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ICanvasElement>
extern MethodInfo m27627_MI;
static PropertyInfo t2430____Count_PropertyInfo = 
{
	&t2430_TI, "Count", &m27627_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27628_MI;
static PropertyInfo t2430____IsReadOnly_PropertyInfo = 
{
	&t2430_TI, "IsReadOnly", &m27628_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2430_PIs[] =
{
	&t2430____Count_PropertyInfo,
	&t2430____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27627_GM;
MethodInfo m27627_MI = 
{
	"get_Count", NULL, &t2430_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27627_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27628_GM;
MethodInfo m27628_MI = 
{
	"get_IsReadOnly", NULL, &t2430_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27628_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2430_m27629_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27629_GM;
MethodInfo m27629_MI = 
{
	"Add", NULL, &t2430_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2430_m27629_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27629_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27630_GM;
MethodInfo m27630_MI = 
{
	"Clear", NULL, &t2430_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27630_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2430_m27631_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27631_GM;
MethodInfo m27631_MI = 
{
	"Contains", NULL, &t2430_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2430_m27631_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27631_GM};
extern Il2CppType t2413_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2430_m27632_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2413_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27632_GM;
MethodInfo m27632_MI = 
{
	"CopyTo", NULL, &t2430_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2430_m27632_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27632_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2430_m27633_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27633_GM;
MethodInfo m27633_MI = 
{
	"Remove", NULL, &t2430_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2430_m27633_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27633_GM};
static MethodInfo* t2430_MIs[] =
{
	&m27627_MI,
	&m27628_MI,
	&m27629_MI,
	&m27630_MI,
	&m27631_MI,
	&m27632_MI,
	&m27633_MI,
	NULL
};
static TypeInfo* t2430_ITIs[] = 
{
	&t603_TI,
	&t2431_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2430_0_0_0;
extern Il2CppType t2430_1_0_0;
struct t2430;
extern Il2CppGenericClass t2430_GC;
TypeInfo t2430_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2430_MIs, t2430_PIs, NULL, NULL, NULL, NULL, NULL, &t2430_TI, t2430_ITIs, NULL, &EmptyCustomAttributesCache, &t2430_TI, &t2430_0_0_0, &t2430_1_0_0, NULL, &t2430_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27634_GM;
MethodInfo m27634_MI = 
{
	"GetEnumerator", NULL, &t2431_TI, &t2412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27634_GM};
static MethodInfo* t2431_MIs[] =
{
	&m27634_MI,
	NULL
};
static TypeInfo* t2431_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2431_0_0_0;
extern Il2CppType t2431_1_0_0;
struct t2431;
extern Il2CppGenericClass t2431_GC;
TypeInfo t2431_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2431_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2431_TI, t2431_ITIs, NULL, &EmptyCustomAttributesCache, &t2431_TI, &t2431_0_0_0, &t2431_1_0_0, NULL, &t2431_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
extern MethodInfo m27635_MI;
static PropertyInfo t2412____Current_PropertyInfo = 
{
	&t2412_TI, "Current", &m27635_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2412_PIs[] =
{
	&t2412____Current_PropertyInfo,
	NULL
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27635_GM;
MethodInfo m27635_MI = 
{
	"get_Current", NULL, &t2412_TI, &t145_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27635_GM};
static MethodInfo* t2412_MIs[] =
{
	&m27635_MI,
	NULL
};
static TypeInfo* t2412_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2412_0_0_0;
extern Il2CppType t2412_1_0_0;
struct t2412;
extern Il2CppGenericClass t2412_GC;
TypeInfo t2412_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2412_MIs, t2412_PIs, NULL, NULL, NULL, NULL, NULL, &t2412_TI, t2412_ITIs, NULL, &EmptyCustomAttributesCache, &t2412_TI, &t2412_0_0_0, &t2412_1_0_0, NULL, &t2412_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2429.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2429_TI;
#include "t2429MD.h"

extern MethodInfo m12589_MI;
extern MethodInfo m20736_MI;
struct t20;
#define m20736(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t20_0_0_1;
FieldInfo t2429_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2429_TI, offsetof(t2429, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2429_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2429_TI, offsetof(t2429, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2429_FIs[] =
{
	&t2429_f0_FieldInfo,
	&t2429_f1_FieldInfo,
	NULL
};
extern MethodInfo m12586_MI;
static PropertyInfo t2429____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2429_TI, "System.Collections.IEnumerator.Current", &m12586_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2429____Current_PropertyInfo = 
{
	&t2429_TI, "Current", &m12589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2429_PIs[] =
{
	&t2429____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2429____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2429_m12585_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12585_GM;
MethodInfo m12585_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2429_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2429_m12585_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12585_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12586_GM;
MethodInfo m12586_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2429_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12586_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12587_GM;
MethodInfo m12587_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2429_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12587_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12588_GM;
MethodInfo m12588_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2429_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12588_GM};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12589_GM;
MethodInfo m12589_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2429_TI, &t145_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12589_GM};
static MethodInfo* t2429_MIs[] =
{
	&m12585_MI,
	&m12586_MI,
	&m12587_MI,
	&m12588_MI,
	&m12589_MI,
	NULL
};
extern MethodInfo m12588_MI;
extern MethodInfo m12587_MI;
static MethodInfo* t2429_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12586_MI,
	&m12588_MI,
	&m12587_MI,
	&m12589_MI,
};
static TypeInfo* t2429_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2412_TI,
};
static Il2CppInterfaceOffsetPair t2429_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2412_TI, 7},
};
extern TypeInfo t145_TI;
static Il2CppRGCTXData t2429_RGCTXData[3] = 
{
	&m12589_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m20736_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2429_0_0_0;
extern Il2CppType t2429_1_0_0;
extern Il2CppGenericClass t2429_GC;
TypeInfo t2429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2429_MIs, t2429_PIs, t2429_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2429_TI, t2429_ITIs, t2429_VT, &EmptyCustomAttributesCache, &t2429_TI, &t2429_0_0_0, &t2429_1_0_0, t2429_IOs, &t2429_GC, NULL, NULL, NULL, t2429_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2429)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2433.h"
#include "t2432.h"
#include "t2438.h"
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t2433_TI;
extern TypeInfo t2432_TI;
extern TypeInfo t2438_TI;
#include "t602MD.h"
#include "t2432MD.h"
#include "t2433MD.h"
#include "t2438MD.h"
extern MethodInfo m4198_MI;
extern MethodInfo m20747_MI;
extern MethodInfo m12623_MI;
extern MethodInfo m12620_MI;
extern MethodInfo m12615_MI;
extern MethodInfo m12621_MI;
extern MethodInfo m12624_MI;
extern MethodInfo m12626_MI;
extern MethodInfo m12609_MI;
extern MethodInfo m12634_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m12635_MI;
extern MethodInfo m27632_MI;
extern MethodInfo m27634_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m12625_MI;
extern MethodInfo m12610_MI;
extern MethodInfo m12611_MI;
extern MethodInfo m12645_MI;
extern MethodInfo m20749_MI;
extern MethodInfo m12618_MI;
extern MethodInfo m12619_MI;
extern MethodInfo m12639_MI;
extern MethodInfo m12622_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m12725_MI;
extern MethodInfo m20751_MI;
extern MethodInfo m20759_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20747(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2436.h"
struct t20;
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20749(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20751(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
struct t20;
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20759(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2433  m12620 (t2411 * __this, MethodInfo* method){
	{
		t2433  L_0 = {0};
		m12639(&L_0, __this, &m12639_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t44_0_0_32849;
FieldInfo t2411_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t2411_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2413_0_0_1;
FieldInfo t2411_f1_FieldInfo = 
{
	"_items", &t2413_0_0_1, &t2411_TI, offsetof(t2411, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2411_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2411_TI, offsetof(t2411, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2411_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2411_TI, offsetof(t2411, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2413_0_0_49;
FieldInfo t2411_f4_FieldInfo = 
{
	"EmptyArray", &t2413_0_0_49, &t2411_TI, offsetof(t2411_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t2411_FIs[] =
{
	&t2411_f0_FieldInfo,
	&t2411_f1_FieldInfo,
	&t2411_f2_FieldInfo,
	&t2411_f3_FieldInfo,
	&t2411_f4_FieldInfo,
	NULL
};
static const int32_t t2411_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t2411_f0_DefaultValue = 
{
	&t2411_f0_FieldInfo, { (char*)&t2411_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2411_FDVs[] = 
{
	&t2411_f0_DefaultValue,
	NULL
};
extern MethodInfo m12601_MI;
static PropertyInfo t2411____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2411_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12601_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12602_MI;
static PropertyInfo t2411____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2411_TI, "System.Collections.ICollection.IsSynchronized", &m12602_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12603_MI;
static PropertyInfo t2411____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2411_TI, "System.Collections.ICollection.SyncRoot", &m12603_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12604_MI;
static PropertyInfo t2411____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2411_TI, "System.Collections.IList.IsFixedSize", &m12604_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12605_MI;
static PropertyInfo t2411____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2411_TI, "System.Collections.IList.IsReadOnly", &m12605_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12606_MI;
extern MethodInfo m12607_MI;
static PropertyInfo t2411____System_Collections_IList_Item_PropertyInfo = 
{
	&t2411_TI, "System.Collections.IList.Item", &m12606_MI, &m12607_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2411____Capacity_PropertyInfo = 
{
	&t2411_TI, "Capacity", &m12634_MI, &m12635_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2411____Count_PropertyInfo = 
{
	&t2411_TI, "Count", &m12636_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2411____Item_PropertyInfo = 
{
	&t2411_TI, "Item", &m12637_MI, &m12638_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2411_PIs[] =
{
	&t2411____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2411____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2411____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2411____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2411____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2411____System_Collections_IList_Item_PropertyInfo,
	&t2411____Capacity_PropertyInfo,
	&t2411____Count_PropertyInfo,
	&t2411____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12590_GM;
MethodInfo m12590_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12590_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12591_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12591_GM;
MethodInfo m12591_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2411_m12591_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12591_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12592_GM;
MethodInfo m12592_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12592_GM};
extern Il2CppType t2412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12593_GM;
MethodInfo m12593_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t2411_TI, &t2412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12593_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12594_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12594_GM;
MethodInfo m12594_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2411_m12594_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12594_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12595_GM;
MethodInfo m12595_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t2411_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12595_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2411_m12596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12596_GM;
MethodInfo m12596_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2411_m12596_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12596_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2411_m12597_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12597_GM;
MethodInfo m12597_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2411_m12597_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12597_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2411_m12598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12598_GM;
MethodInfo m12598_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2411_m12598_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12598_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2411_m12599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12599_GM;
MethodInfo m12599_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2411_m12599_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12599_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2411_m12600_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12600_GM;
MethodInfo m12600_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12600_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12600_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12601_GM;
MethodInfo m12601_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12601_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12602_GM;
MethodInfo m12602_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12602_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12603_GM;
MethodInfo m12603_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t2411_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12603_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12604_GM;
MethodInfo m12604_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12604_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12605_GM;
MethodInfo m12605_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12605_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12606_GM;
MethodInfo m12606_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t2411_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2411_m12606_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12606_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2411_m12607_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12607_GM;
MethodInfo m12607_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2411_m12607_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12607_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2411_m12608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12608_GM;
MethodInfo m12608_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12608_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12608_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12609_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12609_GM;
MethodInfo m12609_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2411_m12609_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12609_GM};
extern Il2CppType t2430_0_0_0;
static ParameterInfo t2411_m12610_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2430_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12610_GM;
MethodInfo m12610_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12610_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12610_GM};
extern Il2CppType t2431_0_0_0;
static ParameterInfo t2411_m12611_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2431_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12611_GM;
MethodInfo m12611_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12611_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12611_GM};
extern Il2CppType t2431_0_0_0;
static ParameterInfo t2411_m12612_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2431_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12612_GM;
MethodInfo m12612_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12612_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12612_GM};
extern Il2CppType t2432_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12613_GM;
MethodInfo m12613_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t2411_TI, &t2432_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12613_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12614_GM;
MethodInfo m12614_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12614_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2411_m12615_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12615_GM;
MethodInfo m12615_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2411_m12615_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12615_GM};
extern Il2CppType t2413_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12616_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2413_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12616_GM;
MethodInfo m12616_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2411_m12616_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12616_GM};
extern Il2CppType t144_0_0_0;
static ParameterInfo t2411_m12617_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t144_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12617_GM;
MethodInfo m12617_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t2411_TI, &t145_0_0_0, RuntimeInvoker_t29_t29, t2411_m12617_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12617_GM};
extern Il2CppType t144_0_0_0;
static ParameterInfo t2411_m12618_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t144_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12618_GM;
MethodInfo m12618_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12618_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12618_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t144_0_0_0;
static ParameterInfo t2411_m12619_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t144_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12619_GM;
MethodInfo m12619_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t2411_m12619_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12619_GM};
extern Il2CppType t2433_0_0_0;
extern void* RuntimeInvoker_t2433 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12620_GM;
MethodInfo m12620_MI = 
{
	"GetEnumerator", (methodPointerType)&m12620, &t2411_TI, &t2433_0_0_0, RuntimeInvoker_t2433, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12620_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2411_m12621_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12621_GM;
MethodInfo m12621_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2411_m12621_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12621_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12622_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12622_GM;
MethodInfo m12622_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t2411_m12622_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12622_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12623_GM;
MethodInfo m12623_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2411_m12623_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12623_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2411_m12624_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12624_GM;
MethodInfo m12624_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2411_m12624_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12624_GM};
extern Il2CppType t2431_0_0_0;
static ParameterInfo t2411_m12625_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2431_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12625_GM;
MethodInfo m12625_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12625_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12625_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2411_m12626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12626_GM;
MethodInfo m12626_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t2411_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2411_m12626_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12626_GM};
extern Il2CppType t144_0_0_0;
static ParameterInfo t2411_m12627_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t144_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12627_GM;
MethodInfo m12627_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2411_m12627_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12627_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12628_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12628_GM;
MethodInfo m12628_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2411_m12628_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12628_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12629_GM;
MethodInfo m12629_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12629_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12630_GM;
MethodInfo m12630_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12630_GM};
extern Il2CppType t143_0_0_0;
static ParameterInfo t2411_m12631_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t143_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12631_GM;
MethodInfo m12631_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2411_m12631_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12631_GM};
extern Il2CppType t2413_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12632_GM;
MethodInfo m12632_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t2411_TI, &t2413_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12632_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12633_GM;
MethodInfo m12633_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12633_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12634_GM;
MethodInfo m12634_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12634_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12635_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12635_GM;
MethodInfo m12635_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2411_m12635_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12635_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12636_GM;
MethodInfo m12636_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t2411_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12636_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2411_m12637_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12637_GM;
MethodInfo m12637_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t2411_TI, &t145_0_0_0, RuntimeInvoker_t29_t44, t2411_m12637_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12637_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2411_m12638_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12638_GM;
MethodInfo m12638_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t2411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2411_m12638_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12638_GM};
static MethodInfo* t2411_MIs[] =
{
	&m12590_MI,
	&m12591_MI,
	&m12592_MI,
	&m12593_MI,
	&m12594_MI,
	&m12595_MI,
	&m12596_MI,
	&m12597_MI,
	&m12598_MI,
	&m12599_MI,
	&m12600_MI,
	&m12601_MI,
	&m12602_MI,
	&m12603_MI,
	&m12604_MI,
	&m12605_MI,
	&m12606_MI,
	&m12607_MI,
	&m12608_MI,
	&m12609_MI,
	&m12610_MI,
	&m12611_MI,
	&m12612_MI,
	&m12613_MI,
	&m12614_MI,
	&m12615_MI,
	&m12616_MI,
	&m12617_MI,
	&m12618_MI,
	&m12619_MI,
	&m12620_MI,
	&m12621_MI,
	&m12622_MI,
	&m12623_MI,
	&m12624_MI,
	&m12625_MI,
	&m12626_MI,
	&m12627_MI,
	&m12628_MI,
	&m12629_MI,
	&m12630_MI,
	&m12631_MI,
	&m12632_MI,
	&m12633_MI,
	&m12634_MI,
	&m12635_MI,
	&m12636_MI,
	&m12637_MI,
	&m12638_MI,
	NULL
};
extern MethodInfo m12595_MI;
extern MethodInfo m12594_MI;
extern MethodInfo m12596_MI;
extern MethodInfo m12597_MI;
extern MethodInfo m12598_MI;
extern MethodInfo m12599_MI;
extern MethodInfo m12600_MI;
extern MethodInfo m12593_MI;
static MethodInfo* t2411_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12595_MI,
	&m12636_MI,
	&m12602_MI,
	&m12603_MI,
	&m12594_MI,
	&m12604_MI,
	&m12605_MI,
	&m12606_MI,
	&m12607_MI,
	&m12596_MI,
	&m12614_MI,
	&m12597_MI,
	&m12598_MI,
	&m12599_MI,
	&m12600_MI,
	&m12628_MI,
	&m12636_MI,
	&m12601_MI,
	&m12608_MI,
	&m12614_MI,
	&m12615_MI,
	&m12616_MI,
	&m12626_MI,
	&m12593_MI,
	&m12621_MI,
	&m12624_MI,
	&m12628_MI,
	&m12637_MI,
	&m12638_MI,
};
extern TypeInfo t868_TI;
static TypeInfo* t2411_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2430_TI,
	&t2431_TI,
	&t2434_TI,
};
static Il2CppInterfaceOffsetPair t2411_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2430_TI, 20},
	{ &t2431_TI, 27},
	{ &t2434_TI, 28},
};
extern TypeInfo t2411_TI;
extern TypeInfo t2413_TI;
extern TypeInfo t2433_TI;
extern TypeInfo t145_TI;
extern TypeInfo t2430_TI;
extern TypeInfo t2432_TI;
static Il2CppRGCTXData t2411_RGCTXData[37] = 
{
	&t2411_TI/* Static Usage */,
	&t2413_TI/* Array Usage */,
	&m12620_MI/* Method Usage */,
	&t2433_TI/* Class Usage */,
	&t145_TI/* Class Usage */,
	&m12608_MI/* Method Usage */,
	&m12615_MI/* Method Usage */,
	&m12621_MI/* Method Usage */,
	&m12623_MI/* Method Usage */,
	&m12624_MI/* Method Usage */,
	&m12626_MI/* Method Usage */,
	&m12637_MI/* Method Usage */,
	&m12638_MI/* Method Usage */,
	&m12609_MI/* Method Usage */,
	&m12634_MI/* Method Usage */,
	&m12635_MI/* Method Usage */,
	&m27627_MI/* Method Usage */,
	&m27632_MI/* Method Usage */,
	&m27634_MI/* Method Usage */,
	&m27635_MI/* Method Usage */,
	&m12625_MI/* Method Usage */,
	&t2430_TI/* Class Usage */,
	&m12610_MI/* Method Usage */,
	&m12611_MI/* Method Usage */,
	&t2432_TI/* Class Usage */,
	&m12645_MI/* Method Usage */,
	&m20749_MI/* Method Usage */,
	&m12618_MI/* Method Usage */,
	&m12619_MI/* Method Usage */,
	&m12719_MI/* Method Usage */,
	&m12639_MI/* Method Usage */,
	&m12622_MI/* Method Usage */,
	&m12628_MI/* Method Usage */,
	&m12725_MI/* Method Usage */,
	&m20751_MI/* Method Usage */,
	&m20759_MI/* Method Usage */,
	&m20747_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2411_0_0_0;
extern Il2CppType t2411_1_0_0;
struct t2411;
extern Il2CppGenericClass t2411_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2411_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t2411_MIs, t2411_PIs, t2411_FIs, NULL, &t29_TI, NULL, NULL, &t2411_TI, t2411_ITIs, t2411_VT, &t1261__CustomAttributeCache, &t2411_TI, &t2411_0_0_0, &t2411_1_0_0, t2411_IOs, &t2411_GC, NULL, t2411_FDVs, NULL, t2411_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2411), 0, -1, sizeof(t2411_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12642_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2411_0_0_1;
FieldInfo t2433_f0_FieldInfo = 
{
	"l", &t2411_0_0_1, &t2433_TI, offsetof(t2433, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2433_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2433_TI, offsetof(t2433, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2433_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2433_TI, offsetof(t2433, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t145_0_0_1;
FieldInfo t2433_f3_FieldInfo = 
{
	"current", &t145_0_0_1, &t2433_TI, offsetof(t2433, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2433_FIs[] =
{
	&t2433_f0_FieldInfo,
	&t2433_f1_FieldInfo,
	&t2433_f2_FieldInfo,
	&t2433_f3_FieldInfo,
	NULL
};
extern MethodInfo m12640_MI;
static PropertyInfo t2433____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2433_TI, "System.Collections.IEnumerator.Current", &m12640_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12644_MI;
static PropertyInfo t2433____Current_PropertyInfo = 
{
	&t2433_TI, "Current", &m12644_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2433_PIs[] =
{
	&t2433____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2433____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2411_0_0_0;
static ParameterInfo t2433_m12639_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t2411_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12639_GM;
MethodInfo m12639_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2433_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2433_m12639_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12639_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12640_GM;
MethodInfo m12640_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2433_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12640_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12641_GM;
MethodInfo m12641_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2433_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12641_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12642_GM;
MethodInfo m12642_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2433_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12642_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12643_GM;
MethodInfo m12643_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2433_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12643_GM};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12644_GM;
MethodInfo m12644_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2433_TI, &t145_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12644_GM};
static MethodInfo* t2433_MIs[] =
{
	&m12639_MI,
	&m12640_MI,
	&m12641_MI,
	&m12642_MI,
	&m12643_MI,
	&m12644_MI,
	NULL
};
extern MethodInfo m12643_MI;
extern MethodInfo m12641_MI;
static MethodInfo* t2433_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12640_MI,
	&m12643_MI,
	&m12641_MI,
	&m12644_MI,
};
static TypeInfo* t2433_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2412_TI,
};
static Il2CppInterfaceOffsetPair t2433_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2412_TI, 7},
};
extern TypeInfo t145_TI;
extern TypeInfo t2433_TI;
static Il2CppRGCTXData t2433_RGCTXData[3] = 
{
	&m12642_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&t2433_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2433_0_0_0;
extern Il2CppType t2433_1_0_0;
extern Il2CppGenericClass t2433_GC;
extern TypeInfo t1261_TI;
TypeInfo t2433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2433_MIs, t2433_PIs, t2433_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2433_TI, t2433_ITIs, t2433_VT, &EmptyCustomAttributesCache, &t2433_TI, &t2433_0_0_0, &t2433_1_0_0, t2433_IOs, &t2433_GC, NULL, NULL, NULL, t2433_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2433)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2435MD.h"
extern MethodInfo m12674_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m12706_MI;
extern MethodInfo m27631_MI;
extern MethodInfo m27624_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2434_0_0_1;
FieldInfo t2432_f0_FieldInfo = 
{
	"list", &t2434_0_0_1, &t2432_TI, offsetof(t2432, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2432_FIs[] =
{
	&t2432_f0_FieldInfo,
	NULL
};
extern MethodInfo m12651_MI;
extern MethodInfo m12652_MI;
static PropertyInfo t2432____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2432_TI, "System.Collections.Generic.IList<T>.Item", &m12651_MI, &m12652_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12653_MI;
static PropertyInfo t2432____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2432_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12653_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12663_MI;
static PropertyInfo t2432____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2432_TI, "System.Collections.ICollection.IsSynchronized", &m12663_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12664_MI;
static PropertyInfo t2432____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2432_TI, "System.Collections.ICollection.SyncRoot", &m12664_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12665_MI;
static PropertyInfo t2432____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2432_TI, "System.Collections.IList.IsFixedSize", &m12665_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12666_MI;
static PropertyInfo t2432____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2432_TI, "System.Collections.IList.IsReadOnly", &m12666_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12667_MI;
extern MethodInfo m12668_MI;
static PropertyInfo t2432____System_Collections_IList_Item_PropertyInfo = 
{
	&t2432_TI, "System.Collections.IList.Item", &m12667_MI, &m12668_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12673_MI;
static PropertyInfo t2432____Count_PropertyInfo = 
{
	&t2432_TI, "Count", &m12673_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2432____Item_PropertyInfo = 
{
	&t2432_TI, "Item", &m12674_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2432_PIs[] =
{
	&t2432____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2432____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2432____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2432____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2432____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2432____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2432____System_Collections_IList_Item_PropertyInfo,
	&t2432____Count_PropertyInfo,
	&t2432____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2434_0_0_0;
static ParameterInfo t2432_m12645_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12645_GM;
MethodInfo m12645_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2432_m12645_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12645_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2432_m12646_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12646_GM;
MethodInfo m12646_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2432_m12646_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12646_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12647_GM;
MethodInfo m12647_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12647_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2432_m12648_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12648_GM;
MethodInfo m12648_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2432_m12648_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12648_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2432_m12649_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12649_GM;
MethodInfo m12649_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2432_m12649_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12649_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12650_GM;
MethodInfo m12650_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2432_m12650_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12650_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12651_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12651_GM;
MethodInfo m12651_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2432_TI, &t145_0_0_0, RuntimeInvoker_t29_t44, t2432_m12651_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12651_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2432_m12652_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12652_GM;
MethodInfo m12652_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2432_m12652_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12652_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12653_GM;
MethodInfo m12653_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12653_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12654_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12654_GM;
MethodInfo m12654_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2432_m12654_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12654_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12655_GM;
MethodInfo m12655_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2432_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12655_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2432_m12656_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12656_GM;
MethodInfo m12656_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2432_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2432_m12656_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12656_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12657_GM;
MethodInfo m12657_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12657_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2432_m12658_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12658_GM;
MethodInfo m12658_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2432_m12658_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12658_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2432_m12659_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12659_GM;
MethodInfo m12659_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2432_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2432_m12659_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12659_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2432_m12660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12660_GM;
MethodInfo m12660_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2432_m12660_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12660_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2432_m12661_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12661_GM;
MethodInfo m12661_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2432_m12661_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12661_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12662_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12662_GM;
MethodInfo m12662_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2432_m12662_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12662_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12663_GM;
MethodInfo m12663_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12663_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12664_GM;
MethodInfo m12664_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2432_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12664_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12665_GM;
MethodInfo m12665_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12665_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12666_GM;
MethodInfo m12666_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12666_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12667_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12667_GM;
MethodInfo m12667_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2432_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2432_m12667_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12667_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2432_m12668_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12668_GM;
MethodInfo m12668_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2432_m12668_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12668_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2432_m12669_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12669_GM;
MethodInfo m12669_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2432_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2432_m12669_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12669_GM};
extern Il2CppType t2413_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12670_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2413_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12670_GM;
MethodInfo m12670_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2432_m12670_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12670_GM};
extern Il2CppType t2412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12671_GM;
MethodInfo m12671_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2432_TI, &t2412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12671_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2432_m12672_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12672_GM;
MethodInfo m12672_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2432_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2432_m12672_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12672_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12673_GM;
MethodInfo m12673_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2432_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12673_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2432_m12674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12674_GM;
MethodInfo m12674_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2432_TI, &t145_0_0_0, RuntimeInvoker_t29_t44, t2432_m12674_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12674_GM};
static MethodInfo* t2432_MIs[] =
{
	&m12645_MI,
	&m12646_MI,
	&m12647_MI,
	&m12648_MI,
	&m12649_MI,
	&m12650_MI,
	&m12651_MI,
	&m12652_MI,
	&m12653_MI,
	&m12654_MI,
	&m12655_MI,
	&m12656_MI,
	&m12657_MI,
	&m12658_MI,
	&m12659_MI,
	&m12660_MI,
	&m12661_MI,
	&m12662_MI,
	&m12663_MI,
	&m12664_MI,
	&m12665_MI,
	&m12666_MI,
	&m12667_MI,
	&m12668_MI,
	&m12669_MI,
	&m12670_MI,
	&m12671_MI,
	&m12672_MI,
	&m12673_MI,
	&m12674_MI,
	NULL
};
extern MethodInfo m12655_MI;
extern MethodInfo m12654_MI;
extern MethodInfo m12656_MI;
extern MethodInfo m12657_MI;
extern MethodInfo m12658_MI;
extern MethodInfo m12659_MI;
extern MethodInfo m12660_MI;
extern MethodInfo m12661_MI;
extern MethodInfo m12662_MI;
extern MethodInfo m12646_MI;
extern MethodInfo m12647_MI;
extern MethodInfo m12669_MI;
extern MethodInfo m12670_MI;
extern MethodInfo m12649_MI;
extern MethodInfo m12672_MI;
extern MethodInfo m12648_MI;
extern MethodInfo m12650_MI;
extern MethodInfo m12671_MI;
static MethodInfo* t2432_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12655_MI,
	&m12673_MI,
	&m12663_MI,
	&m12664_MI,
	&m12654_MI,
	&m12665_MI,
	&m12666_MI,
	&m12667_MI,
	&m12668_MI,
	&m12656_MI,
	&m12657_MI,
	&m12658_MI,
	&m12659_MI,
	&m12660_MI,
	&m12661_MI,
	&m12662_MI,
	&m12673_MI,
	&m12653_MI,
	&m12646_MI,
	&m12647_MI,
	&m12669_MI,
	&m12670_MI,
	&m12649_MI,
	&m12672_MI,
	&m12648_MI,
	&m12650_MI,
	&m12651_MI,
	&m12652_MI,
	&m12671_MI,
	&m12674_MI,
};
static TypeInfo* t2432_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
};
static Il2CppInterfaceOffsetPair t2432_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2430_TI, 20},
	{ &t2434_TI, 27},
	{ &t2431_TI, 32},
};
extern TypeInfo t145_TI;
static Il2CppRGCTXData t2432_RGCTXData[9] = 
{
	&m12674_MI/* Method Usage */,
	&m12706_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m27631_MI/* Method Usage */,
	&m27624_MI/* Method Usage */,
	&m27622_MI/* Method Usage */,
	&m27632_MI/* Method Usage */,
	&m27634_MI/* Method Usage */,
	&m27627_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2432_0_0_0;
extern Il2CppType t2432_1_0_0;
struct t2432;
extern Il2CppGenericClass t2432_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2432_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2432_MIs, t2432_PIs, t2432_FIs, NULL, &t29_TI, NULL, NULL, &t2432_TI, t2432_ITIs, t2432_VT, &t1263__CustomAttributeCache, &t2432_TI, &t2432_0_0_0, &t2432_1_0_0, t2432_IOs, &t2432_GC, NULL, NULL, NULL, t2432_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2432), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2435.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2435_TI;

extern MethodInfo m12709_MI;
extern MethodInfo m12710_MI;
extern MethodInfo m12707_MI;
extern MethodInfo m12705_MI;
extern MethodInfo m12698_MI;
extern MethodInfo m12708_MI;
extern MethodInfo m12696_MI;
extern MethodInfo m12701_MI;
extern MethodInfo m12692_MI;
extern MethodInfo m27630_MI;
extern MethodInfo m27625_MI;
extern MethodInfo m27626_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2434_0_0_1;
FieldInfo t2435_f0_FieldInfo = 
{
	"list", &t2434_0_0_1, &t2435_TI, offsetof(t2435, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2435_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2435_TI, offsetof(t2435, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2435_FIs[] =
{
	&t2435_f0_FieldInfo,
	&t2435_f1_FieldInfo,
	NULL
};
extern MethodInfo m12676_MI;
static PropertyInfo t2435____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2435_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12676_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12684_MI;
static PropertyInfo t2435____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2435_TI, "System.Collections.ICollection.IsSynchronized", &m12684_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12685_MI;
static PropertyInfo t2435____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2435_TI, "System.Collections.ICollection.SyncRoot", &m12685_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12686_MI;
static PropertyInfo t2435____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2435_TI, "System.Collections.IList.IsFixedSize", &m12686_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12687_MI;
static PropertyInfo t2435____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2435_TI, "System.Collections.IList.IsReadOnly", &m12687_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12688_MI;
extern MethodInfo m12689_MI;
static PropertyInfo t2435____System_Collections_IList_Item_PropertyInfo = 
{
	&t2435_TI, "System.Collections.IList.Item", &m12688_MI, &m12689_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12702_MI;
static PropertyInfo t2435____Count_PropertyInfo = 
{
	&t2435_TI, "Count", &m12702_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12703_MI;
extern MethodInfo m12704_MI;
static PropertyInfo t2435____Item_PropertyInfo = 
{
	&t2435_TI, "Item", &m12703_MI, &m12704_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2435_PIs[] =
{
	&t2435____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2435____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2435____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2435____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2435____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2435____System_Collections_IList_Item_PropertyInfo,
	&t2435____Count_PropertyInfo,
	&t2435____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12675_GM;
MethodInfo m12675_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12675_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12676_GM;
MethodInfo m12676_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12676_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2435_m12677_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12677_GM;
MethodInfo m12677_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2435_m12677_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12677_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12678_GM;
MethodInfo m12678_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2435_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12678_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12679_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12679_GM;
MethodInfo m12679_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2435_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2435_m12679_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12679_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12680_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12680_GM;
MethodInfo m12680_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2435_m12680_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12680_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12681_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12681_GM;
MethodInfo m12681_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2435_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2435_m12681_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12681_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12682_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12682_GM;
MethodInfo m12682_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2435_m12682_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12682_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12683_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12683_GM;
MethodInfo m12683_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2435_m12683_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12683_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12684_GM;
MethodInfo m12684_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12684_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12685_GM;
MethodInfo m12685_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2435_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12685_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12686_GM;
MethodInfo m12686_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12686_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12687_GM;
MethodInfo m12687_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12687_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2435_m12688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12688_GM;
MethodInfo m12688_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2435_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2435_m12688_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12688_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12689_GM;
MethodInfo m12689_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2435_m12689_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12689_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12690_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12690_GM;
MethodInfo m12690_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2435_m12690_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12690_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12691_GM;
MethodInfo m12691_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12691_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12692_GM;
MethodInfo m12692_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12692_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12693_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12693_GM;
MethodInfo m12693_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2435_m12693_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12693_GM};
extern Il2CppType t2413_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2435_m12694_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2413_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12694_GM;
MethodInfo m12694_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2435_m12694_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12694_GM};
extern Il2CppType t2412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12695_GM;
MethodInfo m12695_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2435_TI, &t2412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12695_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12696_GM;
MethodInfo m12696_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2435_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2435_m12696_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12696_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12697_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12697_GM;
MethodInfo m12697_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2435_m12697_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12697_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12698_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12698_GM;
MethodInfo m12698_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2435_m12698_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12698_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12699_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12699_GM;
MethodInfo m12699_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2435_m12699_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12699_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2435_m12700_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12700_GM;
MethodInfo m12700_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2435_m12700_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12700_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2435_m12701_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12701_GM;
MethodInfo m12701_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2435_m12701_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12701_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12702_GM;
MethodInfo m12702_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2435_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12702_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2435_m12703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12703_GM;
MethodInfo m12703_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2435_TI, &t145_0_0_0, RuntimeInvoker_t29_t44, t2435_m12703_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12703_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12704_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12704_GM;
MethodInfo m12704_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2435_m12704_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12704_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2435_m12705_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12705_GM;
MethodInfo m12705_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2435_m12705_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12705_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12706_GM;
MethodInfo m12706_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2435_m12706_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12706_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2435_m12707_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12707_GM;
MethodInfo m12707_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2435_TI, &t145_0_0_0, RuntimeInvoker_t29_t29, t2435_m12707_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12707_GM};
extern Il2CppType t2434_0_0_0;
static ParameterInfo t2435_m12708_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12708_GM;
MethodInfo m12708_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2435_m12708_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12708_GM};
extern Il2CppType t2434_0_0_0;
static ParameterInfo t2435_m12709_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12709_GM;
MethodInfo m12709_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2435_m12709_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12709_GM};
extern Il2CppType t2434_0_0_0;
static ParameterInfo t2435_m12710_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12710_GM;
MethodInfo m12710_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2435_m12710_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12710_GM};
static MethodInfo* t2435_MIs[] =
{
	&m12675_MI,
	&m12676_MI,
	&m12677_MI,
	&m12678_MI,
	&m12679_MI,
	&m12680_MI,
	&m12681_MI,
	&m12682_MI,
	&m12683_MI,
	&m12684_MI,
	&m12685_MI,
	&m12686_MI,
	&m12687_MI,
	&m12688_MI,
	&m12689_MI,
	&m12690_MI,
	&m12691_MI,
	&m12692_MI,
	&m12693_MI,
	&m12694_MI,
	&m12695_MI,
	&m12696_MI,
	&m12697_MI,
	&m12698_MI,
	&m12699_MI,
	&m12700_MI,
	&m12701_MI,
	&m12702_MI,
	&m12703_MI,
	&m12704_MI,
	&m12705_MI,
	&m12706_MI,
	&m12707_MI,
	&m12708_MI,
	&m12709_MI,
	&m12710_MI,
	NULL
};
extern MethodInfo m12678_MI;
extern MethodInfo m12677_MI;
extern MethodInfo m12679_MI;
extern MethodInfo m12691_MI;
extern MethodInfo m12680_MI;
extern MethodInfo m12681_MI;
extern MethodInfo m12682_MI;
extern MethodInfo m12683_MI;
extern MethodInfo m12700_MI;
extern MethodInfo m12690_MI;
extern MethodInfo m12693_MI;
extern MethodInfo m12694_MI;
extern MethodInfo m12699_MI;
extern MethodInfo m12697_MI;
extern MethodInfo m12695_MI;
static MethodInfo* t2435_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12678_MI,
	&m12702_MI,
	&m12684_MI,
	&m12685_MI,
	&m12677_MI,
	&m12686_MI,
	&m12687_MI,
	&m12688_MI,
	&m12689_MI,
	&m12679_MI,
	&m12691_MI,
	&m12680_MI,
	&m12681_MI,
	&m12682_MI,
	&m12683_MI,
	&m12700_MI,
	&m12702_MI,
	&m12676_MI,
	&m12690_MI,
	&m12691_MI,
	&m12693_MI,
	&m12694_MI,
	&m12699_MI,
	&m12696_MI,
	&m12697_MI,
	&m12700_MI,
	&m12703_MI,
	&m12704_MI,
	&m12695_MI,
	&m12692_MI,
	&m12698_MI,
	&m12701_MI,
	&m12705_MI,
};
static TypeInfo* t2435_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
};
static Il2CppInterfaceOffsetPair t2435_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2430_TI, 20},
	{ &t2434_TI, 27},
	{ &t2431_TI, 32},
};
extern TypeInfo t2411_TI;
extern TypeInfo t145_TI;
static Il2CppRGCTXData t2435_RGCTXData[25] = 
{
	&t2411_TI/* Class Usage */,
	&m12590_MI/* Method Usage */,
	&m27628_MI/* Method Usage */,
	&m27634_MI/* Method Usage */,
	&m27627_MI/* Method Usage */,
	&m12707_MI/* Method Usage */,
	&m12698_MI/* Method Usage */,
	&m12706_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m27631_MI/* Method Usage */,
	&m27624_MI/* Method Usage */,
	&m12708_MI/* Method Usage */,
	&m12696_MI/* Method Usage */,
	&m12701_MI/* Method Usage */,
	&m12709_MI/* Method Usage */,
	&m12710_MI/* Method Usage */,
	&m27622_MI/* Method Usage */,
	&m12705_MI/* Method Usage */,
	&m12692_MI/* Method Usage */,
	&m27630_MI/* Method Usage */,
	&m27632_MI/* Method Usage */,
	&m27625_MI/* Method Usage */,
	&m27626_MI/* Method Usage */,
	&m27623_MI/* Method Usage */,
	&t145_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2435_0_0_0;
extern Il2CppType t2435_1_0_0;
struct t2435;
extern Il2CppGenericClass t2435_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2435_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2435_MIs, t2435_PIs, t2435_FIs, NULL, &t29_TI, NULL, NULL, &t2435_TI, t2435_ITIs, t2435_VT, &t1262__CustomAttributeCache, &t2435_TI, &t2435_0_0_0, &t2435_1_0_0, t2435_IOs, &t2435_GC, NULL, NULL, NULL, t2435_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2435), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2436_TI;
#include "t2436MD.h"

#include "t1257.h"
#include "t2437.h"
extern TypeInfo t6658_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2437_TI;
#include "t931MD.h"
#include "t2437MD.h"
extern Il2CppType t6658_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m12716_MI;
extern MethodInfo m27636_MI;
extern MethodInfo m20748_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2436_0_0_49;
FieldInfo t2436_f0_FieldInfo = 
{
	"_default", &t2436_0_0_49, &t2436_TI, offsetof(t2436_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2436_FIs[] =
{
	&t2436_f0_FieldInfo,
	NULL
};
extern MethodInfo m12715_MI;
static PropertyInfo t2436____Default_PropertyInfo = 
{
	&t2436_TI, "Default", &m12715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2436_PIs[] =
{
	&t2436____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12711_GM;
MethodInfo m12711_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2436_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12711_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12712_GM;
MethodInfo m12712_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2436_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12712_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2436_m12713_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12713_GM;
MethodInfo m12713_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2436_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2436_m12713_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12713_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2436_m12714_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12714_GM;
MethodInfo m12714_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2436_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2436_m12714_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12714_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2436_m27636_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27636_GM;
MethodInfo m27636_MI = 
{
	"GetHashCode", NULL, &t2436_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2436_m27636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27636_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2436_m20748_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20748_GM;
MethodInfo m20748_MI = 
{
	"Equals", NULL, &t2436_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2436_m20748_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20748_GM};
extern Il2CppType t2436_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12715_GM;
MethodInfo m12715_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2436_TI, &t2436_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12715_GM};
static MethodInfo* t2436_MIs[] =
{
	&m12711_MI,
	&m12712_MI,
	&m12713_MI,
	&m12714_MI,
	&m27636_MI,
	&m20748_MI,
	&m12715_MI,
	NULL
};
extern MethodInfo m12714_MI;
extern MethodInfo m12713_MI;
static MethodInfo* t2436_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20748_MI,
	&m27636_MI,
	&m12714_MI,
	&m12713_MI,
	NULL,
	NULL,
};
extern TypeInfo t2440_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2436_ITIs[] = 
{
	&t2440_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2436_IOs[] = 
{
	{ &t2440_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2436_TI;
extern TypeInfo t2436_TI;
extern TypeInfo t2437_TI;
extern TypeInfo t145_TI;
static Il2CppRGCTXData t2436_RGCTXData[9] = 
{
	&t6658_0_0_0/* Type Usage */,
	&t145_0_0_0/* Type Usage */,
	&t2436_TI/* Class Usage */,
	&t2436_TI/* Static Usage */,
	&t2437_TI/* Class Usage */,
	&m12716_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m27636_MI/* Method Usage */,
	&m20748_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2436_0_0_0;
extern Il2CppType t2436_1_0_0;
struct t2436;
extern Il2CppGenericClass t2436_GC;
TypeInfo t2436_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2436_MIs, t2436_PIs, t2436_FIs, NULL, &t29_TI, NULL, NULL, &t2436_TI, t2436_ITIs, t2436_VT, &EmptyCustomAttributesCache, &t2436_TI, &t2436_0_0_0, &t2436_1_0_0, t2436_IOs, &t2436_GC, NULL, NULL, NULL, t2436_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2436), 0, -1, sizeof(t2436_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2440_m27637_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27637_GM;
MethodInfo m27637_MI = 
{
	"Equals", NULL, &t2440_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2440_m27637_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27637_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2440_m27638_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27638_GM;
MethodInfo m27638_MI = 
{
	"GetHashCode", NULL, &t2440_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2440_m27638_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27638_GM};
static MethodInfo* t2440_MIs[] =
{
	&m27637_MI,
	&m27638_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2440_0_0_0;
extern Il2CppType t2440_1_0_0;
struct t2440;
extern Il2CppGenericClass t2440_GC;
TypeInfo t2440_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2440_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2440_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2440_TI, &t2440_0_0_0, &t2440_1_0_0, NULL, &t2440_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t145_0_0_0;
static ParameterInfo t6658_m27639_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27639_GM;
MethodInfo m27639_MI = 
{
	"Equals", NULL, &t6658_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6658_m27639_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27639_GM};
static MethodInfo* t6658_MIs[] =
{
	&m27639_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6658_1_0_0;
struct t6658;
extern Il2CppGenericClass t6658_GC;
TypeInfo t6658_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6658_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6658_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6658_TI, &t6658_0_0_0, &t6658_1_0_0, NULL, &t6658_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12711_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ICanvasElement>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12716_GM;
MethodInfo m12716_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2437_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12716_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2437_m12717_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12717_GM;
MethodInfo m12717_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2437_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2437_m12717_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12717_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2437_m12718_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12718_GM;
MethodInfo m12718_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2437_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2437_m12718_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12718_GM};
static MethodInfo* t2437_MIs[] =
{
	&m12716_MI,
	&m12717_MI,
	&m12718_MI,
	NULL
};
extern MethodInfo m12718_MI;
extern MethodInfo m12717_MI;
static MethodInfo* t2437_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12718_MI,
	&m12717_MI,
	&m12714_MI,
	&m12713_MI,
	&m12717_MI,
	&m12718_MI,
};
static Il2CppInterfaceOffsetPair t2437_IOs[] = 
{
	{ &t2440_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2436_TI;
extern TypeInfo t2436_TI;
extern TypeInfo t2437_TI;
extern TypeInfo t145_TI;
extern TypeInfo t145_TI;
static Il2CppRGCTXData t2437_RGCTXData[11] = 
{
	&t6658_0_0_0/* Type Usage */,
	&t145_0_0_0/* Type Usage */,
	&t2436_TI/* Class Usage */,
	&t2436_TI/* Static Usage */,
	&t2437_TI/* Class Usage */,
	&m12716_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m27636_MI/* Method Usage */,
	&m20748_MI/* Method Usage */,
	&m12711_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2437_0_0_0;
extern Il2CppType t2437_1_0_0;
struct t2437;
extern Il2CppGenericClass t2437_GC;
extern TypeInfo t1256_TI;
TypeInfo t2437_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2437_MIs, NULL, NULL, NULL, &t2436_TI, NULL, &t1256_TI, &t2437_TI, NULL, t2437_VT, &EmptyCustomAttributesCache, &t2437_TI, &t2437_0_0_0, &t2437_1_0_0, t2437_IOs, &t2437_GC, NULL, NULL, NULL, t2437_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2437), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t144_m1527_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1527_GM;
MethodInfo m1527_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t144_m1527_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1527_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t144_m12719_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12719_GM;
MethodInfo m12719_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t144_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t144_m12719_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12719_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t144_m12720_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12720_GM;
MethodInfo m12720_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t144_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t144_m12720_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12720_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t144_m12721_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12721_GM;
MethodInfo m12721_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t144_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t144_m12721_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12721_GM};
static MethodInfo* t144_MIs[] =
{
	&m1527_MI,
	&m12719_MI,
	&m12720_MI,
	&m12721_MI,
	NULL
};
extern MethodInfo m12720_MI;
extern MethodInfo m12721_MI;
static MethodInfo* t144_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12719_MI,
	&m12720_MI,
	&m12721_MI,
};
static Il2CppInterfaceOffsetPair t144_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t144_1_0_0;
struct t144;
extern Il2CppGenericClass t144_GC;
TypeInfo t144_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t144_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t144_TI, NULL, t144_VT, &EmptyCustomAttributesCache, &t144_TI, &t144_0_0_0, &t144_1_0_0, t144_IOs, &t144_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t144), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2439.h"
extern TypeInfo t4131_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2439_TI;
#include "t2439MD.h"
extern Il2CppType t4131_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m12726_MI;
extern MethodInfo m27640_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t2438_0_0_49;
FieldInfo t2438_f0_FieldInfo = 
{
	"_default", &t2438_0_0_49, &t2438_TI, offsetof(t2438_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2438_FIs[] =
{
	&t2438_f0_FieldInfo,
	NULL
};
static PropertyInfo t2438____Default_PropertyInfo = 
{
	&t2438_TI, "Default", &m12725_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2438_PIs[] =
{
	&t2438____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12722_GM;
MethodInfo m12722_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2438_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12722_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12723_GM;
MethodInfo m12723_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2438_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12723_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2438_m12724_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12724_GM;
MethodInfo m12724_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2438_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2438_m12724_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12724_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2438_m27640_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27640_GM;
MethodInfo m27640_MI = 
{
	"Compare", NULL, &t2438_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2438_m27640_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27640_GM};
extern Il2CppType t2438_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12725_GM;
MethodInfo m12725_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2438_TI, &t2438_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12725_GM};
static MethodInfo* t2438_MIs[] =
{
	&m12722_MI,
	&m12723_MI,
	&m12724_MI,
	&m27640_MI,
	&m12725_MI,
	NULL
};
extern MethodInfo m12724_MI;
static MethodInfo* t2438_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27640_MI,
	&m12724_MI,
	NULL,
};
extern TypeInfo t4130_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2438_ITIs[] = 
{
	&t4130_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2438_IOs[] = 
{
	{ &t4130_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2438_TI;
extern TypeInfo t2438_TI;
extern TypeInfo t2439_TI;
extern TypeInfo t145_TI;
static Il2CppRGCTXData t2438_RGCTXData[8] = 
{
	&t4131_0_0_0/* Type Usage */,
	&t145_0_0_0/* Type Usage */,
	&t2438_TI/* Class Usage */,
	&t2438_TI/* Static Usage */,
	&t2439_TI/* Class Usage */,
	&m12726_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m27640_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2438_0_0_0;
extern Il2CppType t2438_1_0_0;
struct t2438;
extern Il2CppGenericClass t2438_GC;
TypeInfo t2438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2438_MIs, t2438_PIs, t2438_FIs, NULL, &t29_TI, NULL, NULL, &t2438_TI, t2438_ITIs, t2438_VT, &EmptyCustomAttributesCache, &t2438_TI, &t2438_0_0_0, &t2438_1_0_0, t2438_IOs, &t2438_GC, NULL, NULL, NULL, t2438_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2438), 0, -1, sizeof(t2438_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t4130_m20756_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20756_GM;
MethodInfo m20756_MI = 
{
	"Compare", NULL, &t4130_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4130_m20756_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20756_GM};
static MethodInfo* t4130_MIs[] =
{
	&m20756_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4130_0_0_0;
extern Il2CppType t4130_1_0_0;
struct t4130;
extern Il2CppGenericClass t4130_GC;
TypeInfo t4130_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4130_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4130_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4130_TI, &t4130_0_0_0, &t4130_1_0_0, NULL, &t4130_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t145_0_0_0;
static ParameterInfo t4131_m20757_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20757_GM;
MethodInfo m20757_MI = 
{
	"CompareTo", NULL, &t4131_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4131_m20757_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20757_GM};
static MethodInfo* t4131_MIs[] =
{
	&m20757_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4131_1_0_0;
struct t4131;
extern Il2CppGenericClass t4131_GC;
TypeInfo t4131_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4131_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4131_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4131_TI, &t4131_0_0_0, &t4131_1_0_0, NULL, &t4131_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m12722_MI;
extern MethodInfo m20757_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.ICanvasElement>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12726_GM;
MethodInfo m12726_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2439_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12726_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t2439_m12727_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12727_GM;
MethodInfo m12727_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2439_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2439_m12727_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12727_GM};
static MethodInfo* t2439_MIs[] =
{
	&m12726_MI,
	&m12727_MI,
	NULL
};
extern MethodInfo m12727_MI;
static MethodInfo* t2439_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12727_MI,
	&m12724_MI,
	&m12727_MI,
};
static Il2CppInterfaceOffsetPair t2439_IOs[] = 
{
	{ &t4130_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2438_TI;
extern TypeInfo t2438_TI;
extern TypeInfo t2439_TI;
extern TypeInfo t145_TI;
extern TypeInfo t145_TI;
extern TypeInfo t4131_TI;
static Il2CppRGCTXData t2439_RGCTXData[12] = 
{
	&t4131_0_0_0/* Type Usage */,
	&t145_0_0_0/* Type Usage */,
	&t2438_TI/* Class Usage */,
	&t2438_TI/* Static Usage */,
	&t2439_TI/* Class Usage */,
	&m12726_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&m27640_MI/* Method Usage */,
	&m12722_MI/* Method Usage */,
	&t145_TI/* Class Usage */,
	&t4131_TI/* Class Usage */,
	&m20757_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2439_0_0_0;
extern Il2CppType t2439_1_0_0;
struct t2439;
extern Il2CppGenericClass t2439_GC;
extern TypeInfo t1246_TI;
TypeInfo t2439_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2439_MIs, NULL, NULL, NULL, &t2438_TI, NULL, &t1246_TI, &t2439_TI, NULL, t2439_VT, &EmptyCustomAttributesCache, &t2439_TI, &t2439_0_0_0, &t2439_1_0_0, t2439_IOs, &t2439_GC, NULL, NULL, NULL, t2439_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2439), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t143_TI;
#include "t143MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.UI.ICanvasElement>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t143_m1526_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1526_GM;
MethodInfo m1526_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t143_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t143_m1526_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1526_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
static ParameterInfo t143_m12728_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12728_GM;
MethodInfo m12728_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t143_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t143_m12728_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12728_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t145_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t143_m12729_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12729_GM;
MethodInfo m12729_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t143_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t143_m12729_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12729_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t143_m12730_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12730_GM;
MethodInfo m12730_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t143_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t143_m12730_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12730_GM};
static MethodInfo* t143_MIs[] =
{
	&m1526_MI,
	&m12728_MI,
	&m12729_MI,
	&m12730_MI,
	NULL
};
extern MethodInfo m12728_MI;
extern MethodInfo m12729_MI;
extern MethodInfo m12730_MI;
static MethodInfo* t143_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12728_MI,
	&m12729_MI,
	&m12730_MI,
};
static Il2CppInterfaceOffsetPair t143_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t143_1_0_0;
struct t143;
extern Il2CppGenericClass t143_GC;
TypeInfo t143_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t143_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t143_TI, NULL, t143_VT, &EmptyCustomAttributesCache, &t143_TI, &t143_0_0_0, &t143_1_0_0, t143_IOs, &t143_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t143), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2442.h"
#include "t2444.h"
#include "t2441.h"
#include "t2450.h"
#include "t2446.h"
#include "t2451.h"
extern TypeInfo t2442_TI;
extern TypeInfo t2443_TI;
extern TypeInfo t2444_TI;
extern TypeInfo t2441_TI;
extern TypeInfo t2450_TI;
extern TypeInfo t2446_TI;
extern TypeInfo t2451_TI;
#include "t2442MD.h"
#include "t2444MD.h"
#include "t2441MD.h"
#include "t2450MD.h"
#include "t2446MD.h"
#include "t2451MD.h"
extern Il2CppType t2443_0_0_0;
extern MethodInfo m12769_MI;
extern MethodInfo m12751_MI;
extern MethodInfo m12770_MI;
extern MethodInfo m27638_MI;
extern MethodInfo m27637_MI;
extern MethodInfo m12759_MI;
extern MethodInfo m12785_MI;
extern MethodInfo m12753_MI;
extern MethodInfo m12775_MI;
extern MethodInfo m12777_MI;
extern MethodInfo m12771_MI;
extern MethodInfo m12758_MI;
extern MethodInfo m12755_MI;
extern MethodInfo m12773_MI;
extern MethodInfo m12820_MI;
extern MethodInfo m20775_MI;
extern MethodInfo m12756_MI;
extern MethodInfo m12824_MI;
extern MethodInfo m20777_MI;
extern MethodInfo m12804_MI;
extern MethodInfo m12828_MI;
extern MethodInfo m12754_MI;
extern MethodInfo m12750_MI;
extern MethodInfo m12774_MI;
extern MethodInfo m20778_MI;
struct t365;
 void m20775 (t365 * __this, t3541* p0, int32_t p1, t2441 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t365;
 void m20777 (t365 * __this, t20 * p0, int32_t p1, t2450 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t365;
 void m20778 (t365 * __this, t2443* p0, int32_t p1, t2450 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12731 (t365 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12753(__this, ((int32_t)10), (t29*)NULL, &m12753_MI);
		return;
	}
}
extern MethodInfo m12732_MI;
 void m12732 (t365 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12753(__this, ((int32_t)10), p0, &m12753_MI);
		return;
	}
}
extern MethodInfo m12733_MI;
 void m12733 (t365 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12753(__this, p0, (t29*)NULL, &m12753_MI);
		return;
	}
}
extern MethodInfo m12734_MI;
 void m12734 (t365 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m12735_MI;
 t29 * m12735 (t365 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t145_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m12762_MI, __this, ((t29 *)Castclass(p0, InitializedTypeInfo(&t145_TI))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t29 * L_1 = m12769(__this, p0, &m12769_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m12751_MI, __this, L_1);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m12736_MI;
 void m12736 (t365 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t29 * L_0 = m12769(__this, p0, &m12769_MI);
		int32_t L_1 = m12770(__this, p1, &m12770_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12752_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12737_MI;
 void m12737 (t365 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t29 * L_0 = m12769(__this, p0, &m12769_MI);
		int32_t L_1 = m12770(__this, p1, &m12770_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12760_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12738_MI;
 void m12738 (t365 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t145_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t29 * >::Invoke(&m12766_MI, __this, ((t29 *)Castclass(p0, InitializedTypeInfo(&t145_TI))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m12739_MI;
 bool m12739 (t365 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12740_MI;
 t29 * m12740 (t365 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m12741_MI;
 bool m12741 (t365 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12742_MI;
 void m12742 (t365 * __this, t2444  p0, MethodInfo* method){
	{
		t29 * L_0 = m12775((&p0), &m12775_MI);
		int32_t L_1 = m12777((&p0), &m12777_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12760_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12743_MI;
 bool m12743 (t365 * __this, t2444  p0, MethodInfo* method){
	{
		bool L_0 = m12771(__this, p0, &m12771_MI);
		return L_0;
	}
}
extern MethodInfo m12744_MI;
 void m12744 (t365 * __this, t2443* p0, int32_t p1, MethodInfo* method){
	{
		m12758(__this, p0, p1, &m12758_MI);
		return;
	}
}
extern MethodInfo m12745_MI;
 bool m12745 (t365 * __this, t2444  p0, MethodInfo* method){
	{
		bool L_0 = m12771(__this, p0, &m12771_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t29 * L_1 = m12775((&p0), &m12775_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m12766_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m12746_MI;
 void m12746 (t365 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2443* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t365 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t365 * G_B4_2 = {0};
	{
		V_0 = ((t2443*)IsInst(p0, InitializedTypeInfo(&t2443_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m12758(__this, V_0, p1, &m12758_MI);
		return;
	}

IL_0013:
	{
		m12755(__this, p0, p1, &m12755_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t365 *)(__this));
		if ((((t365_SFs*)InitializedTypeInfo(&t365_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t365 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m12773_MI };
		t2441 * L_1 = (t2441 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2441_TI));
		m12820(L_1, NULL, L_0, &m12820_MI);
		((t365_SFs*)InitializedTypeInfo(&t365_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t365 *)(G_B4_2));
	}

IL_0040:
	{
		m20775(G_B5_2, G_B5_1, G_B5_0, (((t365_SFs*)InitializedTypeInfo(&t365_TI)->static_fields)->f15), &m20775_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m12756_MI };
		t2450 * L_3 = (t2450 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2450_TI));
		m12824(L_3, NULL, L_2, &m12824_MI);
		m20777(__this, p0, p1, L_3, &m20777_MI);
		return;
	}
}
extern MethodInfo m12747_MI;
 t29 * m12747 (t365 * __this, MethodInfo* method){
	{
		t2446  L_0 = {0};
		m12804(&L_0, __this, &m12804_MI);
		t2446  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2446_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12748_MI;
 t29* m12748 (t365 * __this, MethodInfo* method){
	{
		t2446  L_0 = {0};
		m12804(&L_0, __this, &m12804_MI);
		t2446  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2446_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12749_MI;
 t29 * m12749 (t365 * __this, MethodInfo* method){
	{
		t2451 * L_0 = (t2451 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2451_TI));
		m12828(L_0, __this, &m12828_MI);
		return L_0;
	}
}
 int32_t m12750 (t365 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 int32_t m12751 (t365 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2413* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m27637_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m12752 (t365 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2413* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m27637_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		m12759(__this, &m12759_MI);
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t2413* L_29 = (__this->f6);
		*((t29 **)(t29 **)SZArrayLdElema(L_29, V_2)) = (t29 *)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t841* L_37 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_2)) = (int32_t)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m12753 (t365 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t365 * G_B4_0 = {0};
	t365 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t365 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t365 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t365 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t365 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2436_TI));
		t2436 * L_1 = m12715(NULL, &m12715_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t365 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m12754(__this, p0, &m12754_MI);
		__this->f14 = 0;
		return;
	}
}
 void m12754 (t365 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t2413*)SZArrayNew(InitializedTypeInfo(&t2413_TI), p0));
		__this->f7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m12755 (t365 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m12750_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t2444  m12756 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		t2444  L_0 = {0};
		m12774(&L_0, p0, p1, &m12774_MI);
		return L_0;
	}
}
extern MethodInfo m12757_MI;
 int32_t m12757 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m12758 (t365 * __this, t2443* p0, int32_t p1, MethodInfo* method){
	{
		m12755(__this, (t20 *)(t20 *)p0, p1, &m12755_MI);
		t35 L_0 = { &m12756_MI };
		t2450 * L_1 = (t2450 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2450_TI));
		m12824(L_1, NULL, L_0, &m12824_MI);
		m20778(__this, p0, p1, L_1, &m20778_MI);
		return;
	}
}
 void m12759 (t365 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t2413* V_7 = {0};
	t841* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t2413* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_4, (*(t29 **)(t29 **)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t2413*)SZArrayNew(InitializedTypeInfo(&t2413_TI), V_0));
		V_8 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		t2413* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t841* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m12760 (t365 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2413* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m27637_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		m12759(__this, &m12759_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t2413* L_30 = (__this->f6);
		*((t29 **)(t29 **)SZArrayLdElema(L_30, V_2)) = (t29 *)p0;
		t841* L_31 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
 void m12761 (t365 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t2413* L_2 = (__this->f6);
		t2413* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t841* L_4 = (__this->f7);
		t841* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m12762 (t365 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2413* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m27637_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12763_MI;
 bool m12763 (t365 * __this, int32_t p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_0 = m12170(NULL, &m12170_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t841* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, V_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12764_MI;
 void m12764 (t365 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t2443* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2443*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2443*)SZArrayNew(InitializedTypeInfo(&t2443_TI), L_4));
		m12758(__this, V_0, 0, &m12758_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m12765_MI;
 void m12765 (t365 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2443* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2440_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2440_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t2443_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2443*)Castclass(L_10, InitializedTypeInfo(&t2443_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m12754(__this, V_0, &m12754_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t29 * L_11 = m12775(((t2444 *)(t2444 *)SZArrayLdElema(V_1, V_2)), &m12775_MI);
		int32_t L_12 = m12777(((t2444 *)(t2444 *)SZArrayLdElema(V_1, V_2)), &m12777_MI);
		VirtActionInvoker2< t29 *, int32_t >::Invoke(&m12760_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m12766 (t365 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t29 * V_4 = {0};
	int32_t V_5 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2413* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m27637_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t2413* L_25 = (__this->f6);
		Initobj (&t145_TI, (&V_4));
		*((t29 **)(t29 **)SZArrayLdElema(L_25, V_2)) = (t29 *)V_4;
		t841* L_26 = (__this->f7);
		Initobj (&t44_TI, (&V_5));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m12767 (t365 * __this, t29 * p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m27638_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2413* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m27637_MI, L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t44_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m12768_MI;
 t2442 * m12768 (t365 * __this, MethodInfo* method){
	{
		t2442 * L_0 = (t2442 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2442_TI));
		m12785(L_0, __this, &m12785_MI);
		return L_0;
	}
}
 t29 * m12769 (t365 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t145_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t145_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t29 *)Castclass(p0, InitializedTypeInfo(&t145_TI)));
	}
}
 int32_t m12770 (t365 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t44_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI)))));
	}
}
 bool m12771 (t365 * __this, t2444  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29 * L_0 = m12775((&p0), &m12775_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t29 *, int32_t* >::Invoke(&m12767_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_2 = m12170(NULL, &m12170_MI);
		int32_t L_3 = m12777((&p0), &m12777_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27426_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m12772_MI;
 t2446  m12772 (t365 * __this, MethodInfo* method){
	{
		t2446  L_0 = {0};
		m12804(&L_0, __this, &m12804_MI);
		return L_0;
	}
}
 t725  m12773 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		t29 * L_0 = p0;
		int32_t L_1 = p1;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		t725  L_3 = {0};
		m3965(&L_3, ((t29 *)L_0), L_2, &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t44_0_0_32849;
FieldInfo t365_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t365_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t365_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t365_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t365_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t365_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t365_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t365_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t365_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t365_TI, offsetof(t365, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t365_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t365_TI, offsetof(t365, f5), &EmptyCustomAttributesCache};
extern Il2CppType t2413_0_0_1;
FieldInfo t365_f6_FieldInfo = 
{
	"keySlots", &t2413_0_0_1, &t365_TI, offsetof(t365, f6), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t365_f7_FieldInfo = 
{
	"valueSlots", &t841_0_0_1, &t365_TI, offsetof(t365, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t365_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t365_TI, offsetof(t365, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t365_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t365_TI, offsetof(t365, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t365_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t365_TI, offsetof(t365, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t365_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t365_TI, offsetof(t365, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2440_0_0_1;
FieldInfo t365_f12_FieldInfo = 
{
	"hcp", &t2440_0_0_1, &t365_TI, offsetof(t365, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t365_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t365_TI, offsetof(t365, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t365_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t365_TI, offsetof(t365, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2441_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t365_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2441_0_0_17, &t365_TI, offsetof(t365_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t365_FIs[] =
{
	&t365_f0_FieldInfo,
	&t365_f1_FieldInfo,
	&t365_f2_FieldInfo,
	&t365_f3_FieldInfo,
	&t365_f4_FieldInfo,
	&t365_f5_FieldInfo,
	&t365_f6_FieldInfo,
	&t365_f7_FieldInfo,
	&t365_f8_FieldInfo,
	&t365_f9_FieldInfo,
	&t365_f10_FieldInfo,
	&t365_f11_FieldInfo,
	&t365_f12_FieldInfo,
	&t365_f13_FieldInfo,
	&t365_f14_FieldInfo,
	&t365_f15_FieldInfo,
	NULL
};
static const int32_t t365_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t365_f0_DefaultValue = 
{
	&t365_f0_FieldInfo, { (char*)&t365_f0_DefaultValueData, &t44_0_0_0 }};
static const float t365_f1_DefaultValueData = 0.9f;
static Il2CppFieldDefaultValueEntry t365_f1_DefaultValue = 
{
	&t365_f1_FieldInfo, { (char*)&t365_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t365_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t365_f2_DefaultValue = 
{
	&t365_f2_FieldInfo, { (char*)&t365_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t365_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t365_f3_DefaultValue = 
{
	&t365_f3_FieldInfo, { (char*)&t365_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t365_FDVs[] = 
{
	&t365_f0_DefaultValue,
	&t365_f1_DefaultValue,
	&t365_f2_DefaultValue,
	&t365_f3_DefaultValue,
	NULL
};
static PropertyInfo t365____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t365_TI, "System.Collections.IDictionary.Item", &m12735_MI, &m12736_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t365____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t365_TI, "System.Collections.ICollection.IsSynchronized", &m12739_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t365____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t365_TI, "System.Collections.ICollection.SyncRoot", &m12740_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t365____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t365_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m12741_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t365____Count_PropertyInfo = 
{
	&t365_TI, "Count", &m12750_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t365____Item_PropertyInfo = 
{
	&t365_TI, "Item", &m12751_MI, &m12752_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t365____Values_PropertyInfo = 
{
	&t365_TI, "Values", &m12768_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t365_PIs[] =
{
	&t365____System_Collections_IDictionary_Item_PropertyInfo,
	&t365____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t365____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t365____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t365____Count_PropertyInfo,
	&t365____Item_PropertyInfo,
	&t365____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12731_GM;
MethodInfo m12731_MI = 
{
	".ctor", (methodPointerType)&m12731, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12731_GM};
extern Il2CppType t2440_0_0_0;
static ParameterInfo t365_m12732_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2440_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12732_GM;
MethodInfo m12732_MI = 
{
	".ctor", (methodPointerType)&m12732, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t365_m12732_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12732_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12733_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12733_GM;
MethodInfo m12733_MI = 
{
	".ctor", (methodPointerType)&m12733, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t365_m12733_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12733_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t365_m12734_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12734_GM;
MethodInfo m12734_MI = 
{
	".ctor", (methodPointerType)&m12734, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t365_m12734_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12734_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12735_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12735_GM;
MethodInfo m12735_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12735, &t365_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t365_m12735_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12735_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12736_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12736_GM;
MethodInfo m12736_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12736, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t365_m12736_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12736_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12737_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12737_GM;
MethodInfo m12737_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12737, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t365_m12737_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12737_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12738_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12738_GM;
MethodInfo m12738_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12738, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t365_m12738_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12738_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12739_GM;
MethodInfo m12739_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12739, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12739_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12740_GM;
MethodInfo m12740_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12740, &t365_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12740_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12741_GM;
MethodInfo m12741_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12741, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12741_GM};
extern Il2CppType t2444_0_0_0;
extern Il2CppType t2444_0_0_0;
static ParameterInfo t365_m12742_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12742_GM;
MethodInfo m12742_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m12742, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t2444, t365_m12742_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12742_GM};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t365_m12743_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12743_GM;
MethodInfo m12743_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m12743, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t2444, t365_m12743_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12743_GM};
extern Il2CppType t2443_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12744_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2443_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12744_GM;
MethodInfo m12744_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12744, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t365_m12744_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12744_GM};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t365_m12745_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12745_GM;
MethodInfo m12745_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m12745, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t2444, t365_m12745_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12745_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12746_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12746_GM;
MethodInfo m12746_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12746, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t365_m12746_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12746_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12747_GM;
MethodInfo m12747_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12747, &t365_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12747_GM};
extern Il2CppType t2445_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12748_GM;
MethodInfo m12748_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12748, &t365_TI, &t2445_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12748_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12749_GM;
MethodInfo m12749_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12749, &t365_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12749_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12750_GM;
MethodInfo m12750_MI = 
{
	"get_Count", (methodPointerType)&m12750, &t365_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12750_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t365_m12751_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12751_GM;
MethodInfo m12751_MI = 
{
	"get_Item", (methodPointerType)&m12751, &t365_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t365_m12751_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12751_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12752_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12752_GM;
MethodInfo m12752_MI = 
{
	"set_Item", (methodPointerType)&m12752, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t365_m12752_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12752_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2440_0_0_0;
static ParameterInfo t365_m12753_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2440_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12753_GM;
MethodInfo m12753_MI = 
{
	"Init", (methodPointerType)&m12753, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t365_m12753_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12753_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12754_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12754_GM;
MethodInfo m12754_MI = 
{
	"InitArrays", (methodPointerType)&m12754, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t365_m12754_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12754_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12755_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12755_GM;
MethodInfo m12755_MI = 
{
	"CopyToCheck", (methodPointerType)&m12755, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t365_m12755_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12755_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6659_0_0_0;
extern Il2CppType t6659_0_0_0;
static ParameterInfo t365_m27641_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6659_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27641_IGC;
extern TypeInfo m27641_gp_TRet_0_TI;
Il2CppGenericParamFull m27641_gp_TRet_0_TI_GenericParamFull = { { &m27641_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27641_gp_TElem_1_TI;
Il2CppGenericParamFull m27641_gp_TElem_1_TI_GenericParamFull = { { &m27641_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27641_IGPA[2] = 
{
	&m27641_gp_TRet_0_TI_GenericParamFull,
	&m27641_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27641_MI;
Il2CppGenericContainer m27641_IGC = { { NULL, NULL }, NULL, &m27641_MI, 2, 1, m27641_IGPA };
extern Il2CppGenericMethod m27642_GM;
static Il2CppRGCTXDefinition m27641_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27642_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27641_GM;
MethodInfo m27641_MI = 
{
	"Do_CopyTo", NULL, &t365_TI, &t21_0_0_0, NULL, t365_m27641_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27641_RGCTXData, (methodPointerType)NULL, &m27641_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12756_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12756_GM;
MethodInfo m12756_MI = 
{
	"make_pair", (methodPointerType)&m12756, &t365_TI, &t2444_0_0_0, RuntimeInvoker_t2444_t29_t44, t365_m12756_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12756_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12757_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12757_GM;
MethodInfo m12757_MI = 
{
	"pick_value", (methodPointerType)&m12757, &t365_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t365_m12757_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12757_GM};
extern Il2CppType t2443_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12758_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2443_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12758_GM;
MethodInfo m12758_MI = 
{
	"CopyTo", (methodPointerType)&m12758, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t365_m12758_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12758_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6662_0_0_0;
extern Il2CppType t6662_0_0_0;
static ParameterInfo t365_m27643_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27643_IGC;
extern TypeInfo m27643_gp_TRet_0_TI;
Il2CppGenericParamFull m27643_gp_TRet_0_TI_GenericParamFull = { { &m27643_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27643_IGPA[1] = 
{
	&m27643_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27643_MI;
Il2CppGenericContainer m27643_IGC = { { NULL, NULL }, NULL, &m27643_MI, 1, 1, m27643_IGPA };
extern Il2CppGenericMethod m27644_GM;
static Il2CppRGCTXDefinition m27643_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27644_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27643_GM;
MethodInfo m27643_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t365_TI, &t21_0_0_0, NULL, t365_m27643_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27643_RGCTXData, (methodPointerType)NULL, &m27643_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12759_GM;
MethodInfo m12759_MI = 
{
	"Resize", (methodPointerType)&m12759, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12759_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12760_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12760_GM;
MethodInfo m12760_MI = 
{
	"Add", (methodPointerType)&m12760, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t365_m12760_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12760_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12761_GM;
MethodInfo m12761_MI = 
{
	"Clear", (methodPointerType)&m12761, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12761_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t365_m12762_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12762_GM;
MethodInfo m12762_MI = 
{
	"ContainsKey", (methodPointerType)&m12762, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t365_m12762_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12762_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12763_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12763_GM;
MethodInfo m12763_MI = 
{
	"ContainsValue", (methodPointerType)&m12763, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t365_m12763_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12763_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t365_m12764_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12764_GM;
MethodInfo m12764_MI = 
{
	"GetObjectData", (methodPointerType)&m12764, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t365_m12764_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12764_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12765_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12765_GM;
MethodInfo m12765_MI = 
{
	"OnDeserialization", (methodPointerType)&m12765, &t365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t365_m12765_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12765_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t365_m12766_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12766_GM;
MethodInfo m12766_MI = 
{
	"Remove", (methodPointerType)&m12766, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t365_m12766_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12766_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_1_0_2;
static ParameterInfo t365_m12767_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t390 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12767_GM;
MethodInfo m12767_MI = 
{
	"TryGetValue", (methodPointerType)&m12767, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t390, t365_m12767_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12767_GM};
extern Il2CppType t2442_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12768_GM;
MethodInfo m12768_MI = 
{
	"get_Values", (methodPointerType)&m12768, &t365_TI, &t2442_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12768_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12769_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12769_GM;
MethodInfo m12769_MI = 
{
	"ToTKey", (methodPointerType)&m12769, &t365_TI, &t145_0_0_0, RuntimeInvoker_t29_t29, t365_m12769_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12769_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t365_m12770_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12770_GM;
MethodInfo m12770_MI = 
{
	"ToTValue", (methodPointerType)&m12770, &t365_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t365_m12770_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12770_GM};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t365_m12771_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12771_GM;
MethodInfo m12771_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m12771, &t365_TI, &t40_0_0_0, RuntimeInvoker_t40_t2444, t365_m12771_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12771_GM};
extern Il2CppType t2446_0_0_0;
extern void* RuntimeInvoker_t2446 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12772_GM;
MethodInfo m12772_MI = 
{
	"GetEnumerator", (methodPointerType)&m12772, &t365_TI, &t2446_0_0_0, RuntimeInvoker_t2446, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12772_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t365_m12773_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m12773_GM;
MethodInfo m12773_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12773, &t365_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t365_m12773_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12773_GM};
static MethodInfo* t365_MIs[] =
{
	&m12731_MI,
	&m12732_MI,
	&m12733_MI,
	&m12734_MI,
	&m12735_MI,
	&m12736_MI,
	&m12737_MI,
	&m12738_MI,
	&m12739_MI,
	&m12740_MI,
	&m12741_MI,
	&m12742_MI,
	&m12743_MI,
	&m12744_MI,
	&m12745_MI,
	&m12746_MI,
	&m12747_MI,
	&m12748_MI,
	&m12749_MI,
	&m12750_MI,
	&m12751_MI,
	&m12752_MI,
	&m12753_MI,
	&m12754_MI,
	&m12755_MI,
	&m27641_MI,
	&m12756_MI,
	&m12757_MI,
	&m12758_MI,
	&m27643_MI,
	&m12759_MI,
	&m12760_MI,
	&m12761_MI,
	&m12762_MI,
	&m12763_MI,
	&m12764_MI,
	&m12765_MI,
	&m12766_MI,
	&m12767_MI,
	&m12768_MI,
	&m12769_MI,
	&m12770_MI,
	&m12771_MI,
	&m12772_MI,
	&m12773_MI,
	NULL
};
static MethodInfo* t365_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12747_MI,
	&m12764_MI,
	&m12750_MI,
	&m12739_MI,
	&m12740_MI,
	&m12746_MI,
	&m12750_MI,
	&m12741_MI,
	&m12742_MI,
	&m12761_MI,
	&m12743_MI,
	&m12744_MI,
	&m12745_MI,
	&m12748_MI,
	&m12766_MI,
	&m12735_MI,
	&m12736_MI,
	&m12737_MI,
	&m12749_MI,
	&m12738_MI,
	&m12765_MI,
	&m12751_MI,
	&m12752_MI,
	&m12760_MI,
	&m12762_MI,
	&m12764_MI,
	&m12765_MI,
	&m12767_MI,
};
extern TypeInfo t5298_TI;
extern TypeInfo t5300_TI;
extern TypeInfo t6664_TI;
static TypeInfo* t365_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5298_TI,
	&t5300_TI,
	&t6664_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t365_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5298_TI, 10},
	{ &t5300_TI, 17},
	{ &t6664_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t365_0_0_0;
extern Il2CppType t365_1_0_0;
struct t365;
extern Il2CppGenericClass t365_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t365_MIs, t365_PIs, t365_FIs, NULL, &t29_TI, NULL, NULL, &t365_TI, t365_ITIs, t365_VT, &t1254__CustomAttributeCache, &t365_TI, &t365_0_0_0, &t365_1_0_0, t365_IOs, &t365_GC, NULL, t365_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t365), 0, -1, sizeof(t365_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
extern MethodInfo m27645_MI;
static PropertyInfo t5298____Count_PropertyInfo = 
{
	&t5298_TI, "Count", &m27645_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27646_MI;
static PropertyInfo t5298____IsReadOnly_PropertyInfo = 
{
	&t5298_TI, "IsReadOnly", &m27646_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5298_PIs[] =
{
	&t5298____Count_PropertyInfo,
	&t5298____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27645_GM;
MethodInfo m27645_MI = 
{
	"get_Count", NULL, &t5298_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27645_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27646_GM;
MethodInfo m27646_MI = 
{
	"get_IsReadOnly", NULL, &t5298_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27646_GM};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t5298_m27647_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27647_GM;
MethodInfo m27647_MI = 
{
	"Add", NULL, &t5298_TI, &t21_0_0_0, RuntimeInvoker_t21_t2444, t5298_m27647_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27647_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27648_GM;
MethodInfo m27648_MI = 
{
	"Clear", NULL, &t5298_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27648_GM};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t5298_m27649_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27649_GM;
MethodInfo m27649_MI = 
{
	"Contains", NULL, &t5298_TI, &t40_0_0_0, RuntimeInvoker_t40_t2444, t5298_m27649_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27649_GM};
extern Il2CppType t2443_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5298_m27650_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2443_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27650_GM;
MethodInfo m27650_MI = 
{
	"CopyTo", NULL, &t5298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5298_m27650_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27650_GM};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t5298_m27651_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27651_GM;
MethodInfo m27651_MI = 
{
	"Remove", NULL, &t5298_TI, &t40_0_0_0, RuntimeInvoker_t40_t2444, t5298_m27651_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27651_GM};
static MethodInfo* t5298_MIs[] =
{
	&m27645_MI,
	&m27646_MI,
	&m27647_MI,
	&m27648_MI,
	&m27649_MI,
	&m27650_MI,
	&m27651_MI,
	NULL
};
static TypeInfo* t5298_ITIs[] = 
{
	&t603_TI,
	&t5300_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5298_0_0_0;
extern Il2CppType t5298_1_0_0;
struct t5298;
extern Il2CppGenericClass t5298_GC;
TypeInfo t5298_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5298_MIs, t5298_PIs, NULL, NULL, NULL, NULL, NULL, &t5298_TI, t5298_ITIs, NULL, &EmptyCustomAttributesCache, &t5298_TI, &t5298_0_0_0, &t5298_1_0_0, NULL, &t5298_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
extern Il2CppType t2445_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27652_GM;
MethodInfo m27652_MI = 
{
	"GetEnumerator", NULL, &t5300_TI, &t2445_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27652_GM};
static MethodInfo* t5300_MIs[] =
{
	&m27652_MI,
	NULL
};
static TypeInfo* t5300_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5300_0_0_0;
extern Il2CppType t5300_1_0_0;
struct t5300;
extern Il2CppGenericClass t5300_GC;
TypeInfo t5300_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5300_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5300_TI, t5300_ITIs, NULL, &EmptyCustomAttributesCache, &t5300_TI, &t5300_0_0_0, &t5300_1_0_0, NULL, &t5300_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2445_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
extern MethodInfo m27653_MI;
static PropertyInfo t2445____Current_PropertyInfo = 
{
	&t2445_TI, "Current", &m27653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2445_PIs[] =
{
	&t2445____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27653_GM;
MethodInfo m27653_MI = 
{
	"get_Current", NULL, &t2445_TI, &t2444_0_0_0, RuntimeInvoker_t2444, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27653_GM};
static MethodInfo* t2445_MIs[] =
{
	&m27653_MI,
	NULL
};
static TypeInfo* t2445_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2445_0_0_0;
extern Il2CppType t2445_1_0_0;
struct t2445;
extern Il2CppGenericClass t2445_GC;
TypeInfo t2445_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2445_MIs, t2445_PIs, NULL, NULL, NULL, NULL, NULL, &t2445_TI, t2445_ITIs, NULL, &EmptyCustomAttributesCache, &t2445_TI, &t2445_0_0_0, &t2445_1_0_0, NULL, &t2445_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12776_MI;
extern MethodInfo m12778_MI;


 void m12774 (t2444 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		m12776(__this, p0, &m12776_MI);
		m12778(__this, p1, &m12778_MI);
		return;
	}
}
 t29 * m12775 (t2444 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
 void m12776 (t2444 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 int32_t m12777 (t2444 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 void m12778 (t2444 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m12779_MI;
 t7* m12779 (t2444 * __this, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t29 * L_2 = m12775(__this, &m12775_MI);
		t29 * L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t29 *)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t29 * L_4 = m12775(__this, &m12775_MI);
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		int32_t L_8 = m12777(__this, &m12777_MI);
		int32_t L_9 = L_8;
		t29 * L_10 = Box(InitializedTypeInfo(&t44_TI), &L_9);
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_10)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		int32_t L_11 = m12777(__this, &m12777_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&V_1))));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t145_0_0_1;
FieldInfo t2444_f0_FieldInfo = 
{
	"key", &t145_0_0_1, &t2444_TI, offsetof(t2444, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2444_f1_FieldInfo = 
{
	"value", &t44_0_0_1, &t2444_TI, offsetof(t2444, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2444_FIs[] =
{
	&t2444_f0_FieldInfo,
	&t2444_f1_FieldInfo,
	NULL
};
static PropertyInfo t2444____Key_PropertyInfo = 
{
	&t2444_TI, "Key", &m12775_MI, &m12776_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2444____Value_PropertyInfo = 
{
	&t2444_TI, "Value", &m12777_MI, &m12778_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2444_PIs[] =
{
	&t2444____Key_PropertyInfo,
	&t2444____Value_PropertyInfo,
	NULL
};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2444_m12774_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12774_GM;
MethodInfo m12774_MI = 
{
	".ctor", (methodPointerType)&m12774, &t2444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2444_m12774_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12774_GM};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12775_GM;
MethodInfo m12775_MI = 
{
	"get_Key", (methodPointerType)&m12775, &t2444_TI, &t145_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12775_GM};
extern Il2CppType t145_0_0_0;
static ParameterInfo t2444_m12776_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12776_GM;
MethodInfo m12776_MI = 
{
	"set_Key", (methodPointerType)&m12776, &t2444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2444_m12776_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12776_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12777_GM;
MethodInfo m12777_MI = 
{
	"get_Value", (methodPointerType)&m12777, &t2444_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12777_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2444_m12778_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12778_GM;
MethodInfo m12778_MI = 
{
	"set_Value", (methodPointerType)&m12778, &t2444_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2444_m12778_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12778_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12779_GM;
MethodInfo m12779_MI = 
{
	"ToString", (methodPointerType)&m12779, &t2444_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12779_GM};
static MethodInfo* t2444_MIs[] =
{
	&m12774_MI,
	&m12775_MI,
	&m12776_MI,
	&m12777_MI,
	&m12778_MI,
	&m12779_MI,
	NULL
};
static MethodInfo* t2444_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m12779_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2444_1_0_0;
extern Il2CppGenericClass t2444_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2444_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2444_MIs, t2444_PIs, t2444_FIs, NULL, &t110_TI, NULL, NULL, &t2444_TI, NULL, t2444_VT, &t1259__CustomAttributeCache, &t2444_TI, &t2444_0_0_0, &t2444_1_0_0, NULL, &t2444_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2444)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2447.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2447_TI;
#include "t2447MD.h"

extern MethodInfo m12784_MI;
extern MethodInfo m20762_MI;
struct t20;
 t2444  m20762 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12780_MI;
 void m12780 (t2447 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12781_MI;
 t29 * m12781 (t2447 * __this, MethodInfo* method){
	{
		t2444  L_0 = m12784(__this, &m12784_MI);
		t2444  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2444_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12782_MI;
 void m12782 (t2447 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12783_MI;
 bool m12783 (t2447 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2444  m12784 (t2447 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2444  L_8 = m20762(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20762_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
extern Il2CppType t20_0_0_1;
FieldInfo t2447_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2447_TI, offsetof(t2447, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2447_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2447_TI, offsetof(t2447, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2447_FIs[] =
{
	&t2447_f0_FieldInfo,
	&t2447_f1_FieldInfo,
	NULL
};
static PropertyInfo t2447____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2447_TI, "System.Collections.IEnumerator.Current", &m12781_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2447____Current_PropertyInfo = 
{
	&t2447_TI, "Current", &m12784_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2447_PIs[] =
{
	&t2447____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2447____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2447_m12780_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12780_GM;
MethodInfo m12780_MI = 
{
	".ctor", (methodPointerType)&m12780, &t2447_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2447_m12780_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12780_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12781_GM;
MethodInfo m12781_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12781, &t2447_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12781_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12782_GM;
MethodInfo m12782_MI = 
{
	"Dispose", (methodPointerType)&m12782, &t2447_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12782_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12783_GM;
MethodInfo m12783_MI = 
{
	"MoveNext", (methodPointerType)&m12783, &t2447_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12783_GM};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12784_GM;
MethodInfo m12784_MI = 
{
	"get_Current", (methodPointerType)&m12784, &t2447_TI, &t2444_0_0_0, RuntimeInvoker_t2444, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12784_GM};
static MethodInfo* t2447_MIs[] =
{
	&m12780_MI,
	&m12781_MI,
	&m12782_MI,
	&m12783_MI,
	&m12784_MI,
	NULL
};
static MethodInfo* t2447_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12781_MI,
	&m12783_MI,
	&m12782_MI,
	&m12784_MI,
};
static TypeInfo* t2447_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2445_TI,
};
static Il2CppInterfaceOffsetPair t2447_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2445_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2447_0_0_0;
extern Il2CppType t2447_1_0_0;
extern Il2CppGenericClass t2447_GC;
TypeInfo t2447_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2447_MIs, t2447_PIs, t2447_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2447_TI, t2447_ITIs, t2447_VT, &EmptyCustomAttributesCache, &t2447_TI, &t2447_0_0_0, &t2447_1_0_0, t2447_IOs, &t2447_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2447)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5299_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
extern MethodInfo m27654_MI;
extern MethodInfo m27655_MI;
static PropertyInfo t5299____Item_PropertyInfo = 
{
	&t5299_TI, "Item", &m27654_MI, &m27655_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5299_PIs[] =
{
	&t5299____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2444_0_0_0;
static ParameterInfo t5299_m27656_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27656_GM;
MethodInfo m27656_MI = 
{
	"IndexOf", NULL, &t5299_TI, &t44_0_0_0, RuntimeInvoker_t44_t2444, t5299_m27656_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27656_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2444_0_0_0;
static ParameterInfo t5299_m27657_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27657_GM;
MethodInfo m27657_MI = 
{
	"Insert", NULL, &t5299_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2444, t5299_m27657_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27657_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5299_m27658_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27658_GM;
MethodInfo m27658_MI = 
{
	"RemoveAt", NULL, &t5299_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5299_m27658_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27658_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5299_m27654_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27654_GM;
MethodInfo m27654_MI = 
{
	"get_Item", NULL, &t5299_TI, &t2444_0_0_0, RuntimeInvoker_t2444_t44, t5299_m27654_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27654_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2444_0_0_0;
static ParameterInfo t5299_m27655_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2444_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27655_GM;
MethodInfo m27655_MI = 
{
	"set_Item", NULL, &t5299_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2444, t5299_m27655_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27655_GM};
static MethodInfo* t5299_MIs[] =
{
	&m27656_MI,
	&m27657_MI,
	&m27658_MI,
	&m27654_MI,
	&m27655_MI,
	NULL
};
static TypeInfo* t5299_ITIs[] = 
{
	&t603_TI,
	&t5298_TI,
	&t5300_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5299_0_0_0;
extern Il2CppType t5299_1_0_0;
struct t5299;
extern Il2CppGenericClass t5299_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5299_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5299_MIs, t5299_PIs, NULL, NULL, NULL, NULL, NULL, &t5299_TI, t5299_ITIs, NULL, &t1908__CustomAttributeCache, &t5299_TI, &t5299_0_0_0, &t5299_1_0_0, NULL, &t5299_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t145_0_0_0;
static ParameterInfo t6664_m27659_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27659_GM;
MethodInfo m27659_MI = 
{
	"Remove", NULL, &t6664_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6664_m27659_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27659_GM};
static MethodInfo* t6664_MIs[] =
{
	&m27659_MI,
	NULL
};
static TypeInfo* t6664_ITIs[] = 
{
	&t603_TI,
	&t5298_TI,
	&t5300_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6664_0_0_0;
extern Il2CppType t6664_1_0_0;
struct t6664;
extern Il2CppGenericClass t6664_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6664_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6664_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6664_TI, t6664_ITIs, NULL, &t1975__CustomAttributeCache, &t6664_TI, &t6664_0_0_0, &t6664_1_0_0, NULL, &t6664_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2448.h"
#include "t2449.h"
extern TypeInfo t2448_TI;
extern TypeInfo t2449_TI;
#include "t2449MD.h"
#include "t2448MD.h"
extern MethodInfo m12797_MI;
extern MethodInfo m12796_MI;
extern MethodInfo m12816_MI;
extern MethodInfo m20773_MI;
extern MethodInfo m20774_MI;
extern MethodInfo m12799_MI;
struct t365;
 void m20773 (t365 * __this, t20 * p0, int32_t p1, t2449 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t365;
 void m20774 (t365 * __this, t841* p0, int32_t p1, t2449 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12785 (t2442 * __this, t365 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m12786_MI;
 void m12786 (t2442 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12787_MI;
 void m12787 (t2442 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12788_MI;
 bool m12788 (t2442 * __this, int32_t p0, MethodInfo* method){
	{
		t365 * L_0 = (__this->f0);
		bool L_1 = m12763(L_0, p0, &m12763_MI);
		return L_1;
	}
}
extern MethodInfo m12789_MI;
 bool m12789 (t2442 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12790_MI;
 t29* m12790 (t2442 * __this, MethodInfo* method){
	{
		t2448  L_0 = m12797(__this, &m12797_MI);
		t2448  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2448_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12791_MI;
 void m12791 (t2442 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t841* V_0 = {0};
	{
		V_0 = ((t841*)IsInst(p0, InitializedTypeInfo(&t841_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t841*, int32_t >::Invoke(&m12796_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t365 * L_0 = (__this->f0);
		m12755(L_0, p0, p1, &m12755_MI);
		t365 * L_1 = (__this->f0);
		t35 L_2 = { &m12757_MI };
		t2449 * L_3 = (t2449 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2449_TI));
		m12816(L_3, NULL, L_2, &m12816_MI);
		m20773(L_1, p0, p1, L_3, &m20773_MI);
		return;
	}
}
extern MethodInfo m12792_MI;
 t29 * m12792 (t2442 * __this, MethodInfo* method){
	{
		t2448  L_0 = m12797(__this, &m12797_MI);
		t2448  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2448_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12793_MI;
 bool m12793 (t2442 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m12794_MI;
 bool m12794 (t2442 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12795_MI;
 t29 * m12795 (t2442 * __this, MethodInfo* method){
	{
		t365 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m12796 (t2442 * __this, t841* p0, int32_t p1, MethodInfo* method){
	{
		t365 * L_0 = (__this->f0);
		m12755(L_0, (t20 *)(t20 *)p0, p1, &m12755_MI);
		t365 * L_1 = (__this->f0);
		t35 L_2 = { &m12757_MI };
		t2449 * L_3 = (t2449 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2449_TI));
		m12816(L_3, NULL, L_2, &m12816_MI);
		m20774(L_1, p0, p1, L_3, &m20774_MI);
		return;
	}
}
 t2448  m12797 (t2442 * __this, MethodInfo* method){
	{
		t365 * L_0 = (__this->f0);
		t2448  L_1 = {0};
		m12799(&L_1, L_0, &m12799_MI);
		return L_1;
	}
}
extern MethodInfo m12798_MI;
 int32_t m12798 (t2442 * __this, MethodInfo* method){
	{
		t365 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m12750_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t365_0_0_1;
FieldInfo t2442_f0_FieldInfo = 
{
	"dictionary", &t365_0_0_1, &t2442_TI, offsetof(t2442, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2442_FIs[] =
{
	&t2442_f0_FieldInfo,
	NULL
};
static PropertyInfo t2442____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2442_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m12793_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2442____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2442_TI, "System.Collections.ICollection.IsSynchronized", &m12794_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2442____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2442_TI, "System.Collections.ICollection.SyncRoot", &m12795_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2442____Count_PropertyInfo = 
{
	&t2442_TI, "Count", &m12798_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2442_PIs[] =
{
	&t2442____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2442____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2442____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2442____Count_PropertyInfo,
	NULL
};
extern Il2CppType t365_0_0_0;
static ParameterInfo t2442_m12785_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t365_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12785_GM;
MethodInfo m12785_MI = 
{
	".ctor", (methodPointerType)&m12785, &t2442_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2442_m12785_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12785_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2442_m12786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12786_GM;
MethodInfo m12786_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12786, &t2442_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2442_m12786_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12786_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12787_GM;
MethodInfo m12787_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12787, &t2442_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12787_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2442_m12788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12788_GM;
MethodInfo m12788_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12788, &t2442_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2442_m12788_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12788_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2442_m12789_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12789_GM;
MethodInfo m12789_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12789, &t2442_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2442_m12789_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12789_GM};
extern Il2CppType t2424_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12790_GM;
MethodInfo m12790_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12790, &t2442_TI, &t2424_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12790_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2442_m12791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12791_GM;
MethodInfo m12791_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12791, &t2442_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2442_m12791_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12791_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12792_GM;
MethodInfo m12792_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12792, &t2442_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12792_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12793_GM;
MethodInfo m12793_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12793, &t2442_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12793_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12794_GM;
MethodInfo m12794_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12794, &t2442_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12794_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12795_GM;
MethodInfo m12795_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12795, &t2442_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12795_GM};
extern Il2CppType t841_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2442_m12796_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12796_GM;
MethodInfo m12796_MI = 
{
	"CopyTo", (methodPointerType)&m12796, &t2442_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2442_m12796_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12796_GM};
extern Il2CppType t2448_0_0_0;
extern void* RuntimeInvoker_t2448 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12797_GM;
MethodInfo m12797_MI = 
{
	"GetEnumerator", (methodPointerType)&m12797, &t2442_TI, &t2448_0_0_0, RuntimeInvoker_t2448, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12797_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12798_GM;
MethodInfo m12798_MI = 
{
	"get_Count", (methodPointerType)&m12798, &t2442_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12798_GM};
static MethodInfo* t2442_MIs[] =
{
	&m12785_MI,
	&m12786_MI,
	&m12787_MI,
	&m12788_MI,
	&m12789_MI,
	&m12790_MI,
	&m12791_MI,
	&m12792_MI,
	&m12793_MI,
	&m12794_MI,
	&m12795_MI,
	&m12796_MI,
	&m12797_MI,
	&m12798_MI,
	NULL
};
static MethodInfo* t2442_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12792_MI,
	&m12798_MI,
	&m12794_MI,
	&m12795_MI,
	&m12791_MI,
	&m12798_MI,
	&m12793_MI,
	&m12786_MI,
	&m12787_MI,
	&m12788_MI,
	&m12796_MI,
	&m12789_MI,
	&m12790_MI,
};
static TypeInfo* t2442_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5140_TI,
	&t5142_TI,
};
static Il2CppInterfaceOffsetPair t2442_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5140_TI, 9},
	{ &t5142_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2442_0_0_0;
extern Il2CppType t2442_1_0_0;
struct t2442;
extern Il2CppGenericClass t2442_GC;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2442_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2442_MIs, t2442_PIs, t2442_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2442_TI, t2442_ITIs, t2442_VT, &t1252__CustomAttributeCache, &t2442_TI, &t2442_0_0_0, &t2442_1_0_0, t2442_IOs, &t2442_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2442), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12812_MI;
extern MethodInfo m12815_MI;
extern MethodInfo m12809_MI;


 void m12799 (t2448 * __this, t365 * p0, MethodInfo* method){
	{
		t2446  L_0 = m12772(p0, &m12772_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12800_MI;
 t29 * m12800 (t2448 * __this, MethodInfo* method){
	{
		t2446 * L_0 = &(__this->f0);
		int32_t L_1 = m12812(L_0, &m12812_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m12801_MI;
 void m12801 (t2448 * __this, MethodInfo* method){
	{
		t2446 * L_0 = &(__this->f0);
		m12815(L_0, &m12815_MI);
		return;
	}
}
extern MethodInfo m12802_MI;
 bool m12802 (t2448 * __this, MethodInfo* method){
	{
		t2446 * L_0 = &(__this->f0);
		bool L_1 = m12809(L_0, &m12809_MI);
		return L_1;
	}
}
extern MethodInfo m12803_MI;
 int32_t m12803 (t2448 * __this, MethodInfo* method){
	{
		t2446 * L_0 = &(__this->f0);
		t2444 * L_1 = &(L_0->f3);
		int32_t L_2 = m12777(L_1, &m12777_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t2446_0_0_1;
FieldInfo t2448_f0_FieldInfo = 
{
	"host_enumerator", &t2446_0_0_1, &t2448_TI, offsetof(t2448, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2448_FIs[] =
{
	&t2448_f0_FieldInfo,
	NULL
};
static PropertyInfo t2448____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2448_TI, "System.Collections.IEnumerator.Current", &m12800_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2448____Current_PropertyInfo = 
{
	&t2448_TI, "Current", &m12803_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2448_PIs[] =
{
	&t2448____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2448____Current_PropertyInfo,
	NULL
};
extern Il2CppType t365_0_0_0;
static ParameterInfo t2448_m12799_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t365_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12799_GM;
MethodInfo m12799_MI = 
{
	".ctor", (methodPointerType)&m12799, &t2448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2448_m12799_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12799_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12800_GM;
MethodInfo m12800_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12800, &t2448_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12800_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12801_GM;
MethodInfo m12801_MI = 
{
	"Dispose", (methodPointerType)&m12801, &t2448_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12801_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12802_GM;
MethodInfo m12802_MI = 
{
	"MoveNext", (methodPointerType)&m12802, &t2448_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12802_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12803_GM;
MethodInfo m12803_MI = 
{
	"get_Current", (methodPointerType)&m12803, &t2448_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12803_GM};
static MethodInfo* t2448_MIs[] =
{
	&m12799_MI,
	&m12800_MI,
	&m12801_MI,
	&m12802_MI,
	&m12803_MI,
	NULL
};
static MethodInfo* t2448_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12800_MI,
	&m12802_MI,
	&m12801_MI,
	&m12803_MI,
};
static TypeInfo* t2448_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2424_TI,
};
static Il2CppInterfaceOffsetPair t2448_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2424_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2448_0_0_0;
extern Il2CppType t2448_1_0_0;
extern Il2CppGenericClass t2448_GC;
TypeInfo t2448_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2448_MIs, t2448_PIs, t2448_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2448_TI, t2448_ITIs, t2448_VT, &EmptyCustomAttributesCache, &t2448_TI, &t2448_0_0_0, &t2448_1_0_0, t2448_IOs, &t2448_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2448)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12814_MI;
extern MethodInfo m12811_MI;
extern MethodInfo m12813_MI;


 void m12804 (t2446 * __this, t365 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m12805_MI;
 t29 * m12805 (t2446 * __this, MethodInfo* method){
	{
		m12814(__this, &m12814_MI);
		t2444  L_0 = (__this->f3);
		t2444  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2444_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12806_MI;
 t725  m12806 (t2446 * __this, MethodInfo* method){
	{
		m12814(__this, &m12814_MI);
		t2444 * L_0 = &(__this->f3);
		t29 * L_1 = m12775(L_0, &m12775_MI);
		t29 * L_2 = L_1;
		t2444 * L_3 = &(__this->f3);
		int32_t L_4 = m12777(L_3, &m12777_MI);
		int32_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t44_TI), &L_5);
		t725  L_7 = {0};
		m3965(&L_7, ((t29 *)L_2), L_6, &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m12807_MI;
 t29 * m12807 (t2446 * __this, MethodInfo* method){
	{
		t29 * L_0 = m12811(__this, &m12811_MI);
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m12808_MI;
 t29 * m12808 (t2446 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12812(__this, &m12812_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
 bool m12809 (t2446 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m12813(__this, &m12813_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t365 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t365 * L_6 = (__this->f0);
		t2413* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t365 * L_9 = (__this->f0);
		t841* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t2444  L_12 = {0};
		m12774(&L_12, (*(t29 **)(t29 **)SZArrayLdElema(L_7, L_8)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_11)), &m12774_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t365 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m12810_MI;
 t2444  m12810 (t2446 * __this, MethodInfo* method){
	{
		t2444  L_0 = (__this->f3);
		return L_0;
	}
}
 t29 * m12811 (t2446 * __this, MethodInfo* method){
	{
		m12814(__this, &m12814_MI);
		t2444 * L_0 = &(__this->f3);
		t29 * L_1 = m12775(L_0, &m12775_MI);
		return L_1;
	}
}
 int32_t m12812 (t2446 * __this, MethodInfo* method){
	{
		m12814(__this, &m12814_MI);
		t2444 * L_0 = &(__this->f3);
		int32_t L_1 = m12777(L_0, &m12777_MI);
		return L_1;
	}
}
 void m12813 (t2446 * __this, MethodInfo* method){
	{
		t365 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t365 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m12814 (t2446 * __this, MethodInfo* method){
	{
		m12813(__this, &m12813_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m12815 (t2446 * __this, MethodInfo* method){
	{
		__this->f0 = (t365 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t365_0_0_1;
FieldInfo t2446_f0_FieldInfo = 
{
	"dictionary", &t365_0_0_1, &t2446_TI, offsetof(t2446, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2446_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2446_TI, offsetof(t2446, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2446_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2446_TI, offsetof(t2446, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2444_0_0_3;
FieldInfo t2446_f3_FieldInfo = 
{
	"current", &t2444_0_0_3, &t2446_TI, offsetof(t2446, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2446_FIs[] =
{
	&t2446_f0_FieldInfo,
	&t2446_f1_FieldInfo,
	&t2446_f2_FieldInfo,
	&t2446_f3_FieldInfo,
	NULL
};
static PropertyInfo t2446____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2446_TI, "System.Collections.IEnumerator.Current", &m12805_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2446____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2446_TI, "System.Collections.IDictionaryEnumerator.Entry", &m12806_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2446____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2446_TI, "System.Collections.IDictionaryEnumerator.Key", &m12807_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2446____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2446_TI, "System.Collections.IDictionaryEnumerator.Value", &m12808_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2446____Current_PropertyInfo = 
{
	&t2446_TI, "Current", &m12810_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2446____CurrentKey_PropertyInfo = 
{
	&t2446_TI, "CurrentKey", &m12811_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2446____CurrentValue_PropertyInfo = 
{
	&t2446_TI, "CurrentValue", &m12812_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2446_PIs[] =
{
	&t2446____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2446____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2446____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2446____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2446____Current_PropertyInfo,
	&t2446____CurrentKey_PropertyInfo,
	&t2446____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t365_0_0_0;
static ParameterInfo t2446_m12804_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t365_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12804_GM;
MethodInfo m12804_MI = 
{
	".ctor", (methodPointerType)&m12804, &t2446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2446_m12804_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12804_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12805_GM;
MethodInfo m12805_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12805, &t2446_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12805_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12806_GM;
MethodInfo m12806_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12806, &t2446_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12806_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12807_GM;
MethodInfo m12807_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12807, &t2446_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12807_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12808_GM;
MethodInfo m12808_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12808, &t2446_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12808_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12809_GM;
MethodInfo m12809_MI = 
{
	"MoveNext", (methodPointerType)&m12809, &t2446_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12809_GM};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12810_GM;
MethodInfo m12810_MI = 
{
	"get_Current", (methodPointerType)&m12810, &t2446_TI, &t2444_0_0_0, RuntimeInvoker_t2444, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12810_GM};
extern Il2CppType t145_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12811_GM;
MethodInfo m12811_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12811, &t2446_TI, &t145_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12811_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12812_GM;
MethodInfo m12812_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12812, &t2446_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12812_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12813_GM;
MethodInfo m12813_MI = 
{
	"VerifyState", (methodPointerType)&m12813, &t2446_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12813_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12814_GM;
MethodInfo m12814_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12814, &t2446_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12814_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12815_GM;
MethodInfo m12815_MI = 
{
	"Dispose", (methodPointerType)&m12815, &t2446_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12815_GM};
static MethodInfo* t2446_MIs[] =
{
	&m12804_MI,
	&m12805_MI,
	&m12806_MI,
	&m12807_MI,
	&m12808_MI,
	&m12809_MI,
	&m12810_MI,
	&m12811_MI,
	&m12812_MI,
	&m12813_MI,
	&m12814_MI,
	&m12815_MI,
	NULL
};
static MethodInfo* t2446_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12805_MI,
	&m12809_MI,
	&m12815_MI,
	&m12810_MI,
	&m12806_MI,
	&m12807_MI,
	&m12808_MI,
};
static TypeInfo* t2446_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2445_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2446_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2445_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2446_0_0_0;
extern Il2CppType t2446_1_0_0;
extern Il2CppGenericClass t2446_GC;
TypeInfo t2446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2446_MIs, t2446_PIs, t2446_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2446_TI, t2446_ITIs, t2446_VT, &EmptyCustomAttributesCache, &t2446_TI, &t2446_0_0_0, &t2446_1_0_0, t2446_IOs, &t2446_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2446)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m12816 (t2449 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12817_MI;
 int32_t m12817 (t2449 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12817((t2449 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12818_MI;
 t29 * m12818 (t2449 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12819_MI;
 int32_t m12819 (t2449 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.ICanvasElement,System.Int32,System.Int32>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2449_m12816_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12816_GM;
MethodInfo m12816_MI = 
{
	".ctor", (methodPointerType)&m12816, &t2449_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2449_m12816_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12816_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2449_m12817_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12817_GM;
MethodInfo m12817_MI = 
{
	"Invoke", (methodPointerType)&m12817, &t2449_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t2449_m12817_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12817_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2449_m12818_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12818_GM;
MethodInfo m12818_MI = 
{
	"BeginInvoke", (methodPointerType)&m12818, &t2449_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2449_m12818_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12818_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2449_m12819_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12819_GM;
MethodInfo m12819_MI = 
{
	"EndInvoke", (methodPointerType)&m12819, &t2449_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2449_m12819_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12819_GM};
static MethodInfo* t2449_MIs[] =
{
	&m12816_MI,
	&m12817_MI,
	&m12818_MI,
	&m12819_MI,
	NULL
};
static MethodInfo* t2449_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12817_MI,
	&m12818_MI,
	&m12819_MI,
};
static Il2CppInterfaceOffsetPair t2449_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2449_0_0_0;
extern Il2CppType t2449_1_0_0;
struct t2449;
extern Il2CppGenericClass t2449_GC;
TypeInfo t2449_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2449_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2449_TI, NULL, t2449_VT, &EmptyCustomAttributesCache, &t2449_TI, &t2449_0_0_0, &t2449_1_0_0, t2449_IOs, &t2449_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2449), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12820 (t2441 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12821_MI;
 t725  m12821 (t2441 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12821((t2441 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12822_MI;
 t29 * m12822 (t2441 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12823_MI;
 t725  m12823 (t2441 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.ICanvasElement,System.Int32,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2441_m12820_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12820_GM;
MethodInfo m12820_MI = 
{
	".ctor", (methodPointerType)&m12820, &t2441_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2441_m12820_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12820_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2441_m12821_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12821_GM;
MethodInfo m12821_MI = 
{
	"Invoke", (methodPointerType)&m12821, &t2441_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t2441_m12821_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12821_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2441_m12822_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12822_GM;
MethodInfo m12822_MI = 
{
	"BeginInvoke", (methodPointerType)&m12822, &t2441_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2441_m12822_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12822_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2441_m12823_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12823_GM;
MethodInfo m12823_MI = 
{
	"EndInvoke", (methodPointerType)&m12823, &t2441_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2441_m12823_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12823_GM};
static MethodInfo* t2441_MIs[] =
{
	&m12820_MI,
	&m12821_MI,
	&m12822_MI,
	&m12823_MI,
	NULL
};
static MethodInfo* t2441_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12821_MI,
	&m12822_MI,
	&m12823_MI,
};
static Il2CppInterfaceOffsetPair t2441_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2441_0_0_0;
extern Il2CppType t2441_1_0_0;
struct t2441;
extern Il2CppGenericClass t2441_GC;
TypeInfo t2441_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2441_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2441_TI, NULL, t2441_VT, &EmptyCustomAttributesCache, &t2441_TI, &t2441_0_0_0, &t2441_1_0_0, t2441_IOs, &t2441_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2441), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12824 (t2450 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12825_MI;
 t2444  m12825 (t2450 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12825((t2450 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2444  (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2444  (*FunctionPointerType) (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2444  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12826_MI;
 t29 * m12826 (t2450 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12827_MI;
 t2444  m12827 (t2450 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2444 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.ICanvasElement,System.Int32,System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2450_m12824_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12824_GM;
MethodInfo m12824_MI = 
{
	".ctor", (methodPointerType)&m12824, &t2450_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2450_m12824_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12824_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2450_m12825_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12825_GM;
MethodInfo m12825_MI = 
{
	"Invoke", (methodPointerType)&m12825, &t2450_TI, &t2444_0_0_0, RuntimeInvoker_t2444_t29_t44, t2450_m12825_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12825_GM};
extern Il2CppType t145_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2450_m12826_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t145_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12826_GM;
MethodInfo m12826_MI = 
{
	"BeginInvoke", (methodPointerType)&m12826, &t2450_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2450_m12826_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12826_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2450_m12827_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2444_0_0_0;
extern void* RuntimeInvoker_t2444_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12827_GM;
MethodInfo m12827_MI = 
{
	"EndInvoke", (methodPointerType)&m12827, &t2450_TI, &t2444_0_0_0, RuntimeInvoker_t2444_t29, t2450_m12827_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12827_GM};
static MethodInfo* t2450_MIs[] =
{
	&m12824_MI,
	&m12825_MI,
	&m12826_MI,
	&m12827_MI,
	NULL
};
static MethodInfo* t2450_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12825_MI,
	&m12826_MI,
	&m12827_MI,
};
static Il2CppInterfaceOffsetPair t2450_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2450_0_0_0;
extern Il2CppType t2450_1_0_0;
struct t2450;
extern Il2CppGenericClass t2450_GC;
TypeInfo t2450_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2450_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2450_TI, NULL, t2450_VT, &EmptyCustomAttributesCache, &t2450_TI, &t2450_0_0_0, &t2450_1_0_0, t2450_IOs, &t2450_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2450), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12830_MI;


 void m12828 (t2451 * __this, t365 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t2446  L_0 = m12772(p0, &m12772_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12829_MI;
 bool m12829 (t2451 * __this, MethodInfo* method){
	{
		t2446 * L_0 = &(__this->f0);
		bool L_1 = m12809(L_0, &m12809_MI);
		return L_1;
	}
}
 t725  m12830 (t2451 * __this, MethodInfo* method){
	{
		t2446  L_0 = (__this->f0);
		t2446  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2446_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m12831_MI;
 t29 * m12831 (t2451 * __this, MethodInfo* method){
	t2444  V_0 = {0};
	{
		t2446 * L_0 = &(__this->f0);
		t2444  L_1 = m12810(L_0, &m12810_MI);
		V_0 = L_1;
		t29 * L_2 = m12775((&V_0), &m12775_MI);
		t29 * L_3 = L_2;
		return ((t29 *)L_3);
	}
}
extern MethodInfo m12832_MI;
 t29 * m12832 (t2451 * __this, MethodInfo* method){
	t2444  V_0 = {0};
	{
		t2446 * L_0 = &(__this->f0);
		t2444  L_1 = m12810(L_0, &m12810_MI);
		V_0 = L_1;
		int32_t L_2 = m12777((&V_0), &m12777_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m12833_MI;
 t29 * m12833 (t2451 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m12830_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.ICanvasElement,System.Int32>
extern Il2CppType t2446_0_0_1;
FieldInfo t2451_f0_FieldInfo = 
{
	"host_enumerator", &t2446_0_0_1, &t2451_TI, offsetof(t2451, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2451_FIs[] =
{
	&t2451_f0_FieldInfo,
	NULL
};
static PropertyInfo t2451____Entry_PropertyInfo = 
{
	&t2451_TI, "Entry", &m12830_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2451____Key_PropertyInfo = 
{
	&t2451_TI, "Key", &m12831_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2451____Value_PropertyInfo = 
{
	&t2451_TI, "Value", &m12832_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2451____Current_PropertyInfo = 
{
	&t2451_TI, "Current", &m12833_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2451_PIs[] =
{
	&t2451____Entry_PropertyInfo,
	&t2451____Key_PropertyInfo,
	&t2451____Value_PropertyInfo,
	&t2451____Current_PropertyInfo,
	NULL
};
extern Il2CppType t365_0_0_0;
static ParameterInfo t2451_m12828_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t365_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12828_GM;
MethodInfo m12828_MI = 
{
	".ctor", (methodPointerType)&m12828, &t2451_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2451_m12828_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12828_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12829_GM;
MethodInfo m12829_MI = 
{
	"MoveNext", (methodPointerType)&m12829, &t2451_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12829_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12830_GM;
MethodInfo m12830_MI = 
{
	"get_Entry", (methodPointerType)&m12830, &t2451_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12830_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12831_GM;
MethodInfo m12831_MI = 
{
	"get_Key", (methodPointerType)&m12831, &t2451_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12831_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12832_GM;
MethodInfo m12832_MI = 
{
	"get_Value", (methodPointerType)&m12832, &t2451_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12832_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12833_GM;
MethodInfo m12833_MI = 
{
	"get_Current", (methodPointerType)&m12833, &t2451_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12833_GM};
static MethodInfo* t2451_MIs[] =
{
	&m12828_MI,
	&m12829_MI,
	&m12830_MI,
	&m12831_MI,
	&m12832_MI,
	&m12833_MI,
	NULL
};
static MethodInfo* t2451_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12833_MI,
	&m12829_MI,
	&m12830_MI,
	&m12831_MI,
	&m12832_MI,
};
static TypeInfo* t2451_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2451_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2451_0_0_0;
extern Il2CppType t2451_1_0_0;
struct t2451;
extern Il2CppGenericClass t2451_GC;
TypeInfo t2451_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2451_MIs, t2451_PIs, t2451_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2451_TI, t2451_ITIs, t2451_VT, &EmptyCustomAttributesCache, &t2451_TI, &t2451_0_0_0, &t2451_1_0_0, t2451_IOs, &t2451_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2451), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#include "t154.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t154_TI;
#include "t154MD.h"

#include "t148.h"
#include "t350.h"
#include "UnityEngine_ArrayTypes.h"
#include "t2456.h"
#include "t2458.h"
#include "t2455.h"
#include "t2498.h"
#include "t2460.h"
#include "t2499.h"
#include "t2500.h"
#include "t2502.h"
extern TypeInfo t148_TI;
extern TypeInfo t350_TI;
extern TypeInfo t2454_TI;
extern TypeInfo t2456_TI;
extern TypeInfo t2457_TI;
extern TypeInfo t2458_TI;
extern TypeInfo t2455_TI;
extern TypeInfo t2498_TI;
extern TypeInfo t2460_TI;
extern TypeInfo t2499_TI;
extern TypeInfo t2500_TI;
extern TypeInfo t2452_TI;
extern TypeInfo t2453_TI;
extern TypeInfo t2502_TI;
extern TypeInfo t6665_TI;
#include "t2456MD.h"
#include "t2458MD.h"
#include "t2455MD.h"
#include "t2498MD.h"
#include "t2460MD.h"
#include "t2499MD.h"
#include "t2500MD.h"
#include "t2502MD.h"
extern Il2CppType t2454_0_0_0;
extern Il2CppType t2457_0_0_0;
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
extern MethodInfo m12890_MI;
extern MethodInfo m12903_MI;
extern MethodInfo m12870_MI;
extern MethodInfo m12905_MI;
extern MethodInfo m12872_MI;
extern MethodInfo m27660_MI;
extern MethodInfo m27661_MI;
extern MethodInfo m12885_MI;
extern MethodInfo m13161_MI;
extern MethodInfo m12874_MI;
extern MethodInfo m1544_MI;
extern MethodInfo m12898_MI;
extern MethodInfo m12978_MI;
extern MethodInfo m12980_MI;
extern MethodInfo m12907_MI;
extern MethodInfo m12883_MI;
extern MethodInfo m12878_MI;
extern MethodInfo m12909_MI;
extern MethodInfo m13196_MI;
extern MethodInfo m20902_MI;
extern MethodInfo m12880_MI;
extern MethodInfo m13200_MI;
extern MethodInfo m20904_MI;
extern MethodInfo m13180_MI;
extern MethodInfo m13204_MI;
extern MethodInfo m13214_MI;
extern MethodInfo m12876_MI;
extern MethodInfo m12868_MI;
extern MethodInfo m12977_MI;
extern MethodInfo m20905_MI;
extern MethodInfo m13222_MI;
extern MethodInfo m27662_MI;
extern MethodInfo m1542_MI;
extern MethodInfo m27663_MI;
struct t154;
 void m20902 (t154 * __this, t3541* p0, int32_t p1, t2455 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t154;
 void m20904 (t154 * __this, t20 * p0, int32_t p1, t2498 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t154;
 void m20905 (t154 * __this, t2457* p0, int32_t p1, t2498 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12855_MI;
 void m12855 (t154 * __this, t2458  p0, MethodInfo* method){
	{
		t148 * L_0 = m12978((&p0), &m12978_MI);
		t350 * L_1 = m12980((&p0), &m12980_MI);
		VirtActionInvoker2< t148 *, t350 * >::Invoke(&m1544_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12856_MI;
 bool m12856 (t154 * __this, t2458  p0, MethodInfo* method){
	{
		bool L_0 = m12907(__this, p0, &m12907_MI);
		return L_0;
	}
}
extern MethodInfo m12859_MI;
 bool m12859 (t154 * __this, t2458  p0, MethodInfo* method){
	{
		bool L_0 = m12907(__this, p0, &m12907_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t148 * L_1 = m12978((&p0), &m12978_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t148 * >::Invoke(&m12898_MI, __this, L_1);
		return L_2;
	}
}
 t2458  m12880 (t29 * __this, t148 * p0, t350 * p1, MethodInfo* method){
	{
		t2458  L_0 = {0};
		m12977(&L_0, p0, p1, &m12977_MI);
		return L_0;
	}
}
 bool m12907 (t154 * __this, t2458  p0, MethodInfo* method){
	t350 * V_0 = {0};
	{
		t148 * L_0 = m12978((&p0), &m12978_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t148 *, t350 ** >::Invoke(&m1542_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2502_TI));
		t2502 * L_2 = m13222(NULL, &m13222_MI);
		t350 * L_3 = m12980((&p0), &m12980_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, t350 *, t350 * >::Invoke(&m27663_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m12908_MI;
 t2460  m12908 (t154 * __this, MethodInfo* method){
	{
		t2460  L_0 = {0};
		m13180(&L_0, __this, &m13180_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t44_0_0_32849;
FieldInfo t154_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t154_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t154_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t154_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t154_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t154_TI, offsetof(t154, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t154_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t154_TI, offsetof(t154, f5), &EmptyCustomAttributesCache};
extern Il2CppType t2452_0_0_1;
FieldInfo t154_f6_FieldInfo = 
{
	"keySlots", &t2452_0_0_1, &t154_TI, offsetof(t154, f6), &EmptyCustomAttributesCache};
extern Il2CppType t2453_0_0_1;
FieldInfo t154_f7_FieldInfo = 
{
	"valueSlots", &t2453_0_0_1, &t154_TI, offsetof(t154, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t154_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t154_TI, offsetof(t154, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t154_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t154_TI, offsetof(t154, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t154_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t154_TI, offsetof(t154, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t154_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t154_TI, offsetof(t154, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2454_0_0_1;
FieldInfo t154_f12_FieldInfo = 
{
	"hcp", &t2454_0_0_1, &t154_TI, offsetof(t154, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t154_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t154_TI, offsetof(t154, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t154_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t154_TI, offsetof(t154, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2455_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t154_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2455_0_0_17, &t154_TI, offsetof(t154_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t154_FIs[] =
{
	&t154_f0_FieldInfo,
	&t154_f1_FieldInfo,
	&t154_f2_FieldInfo,
	&t154_f3_FieldInfo,
	&t154_f4_FieldInfo,
	&t154_f5_FieldInfo,
	&t154_f6_FieldInfo,
	&t154_f7_FieldInfo,
	&t154_f8_FieldInfo,
	&t154_f9_FieldInfo,
	&t154_f10_FieldInfo,
	&t154_f11_FieldInfo,
	&t154_f12_FieldInfo,
	&t154_f13_FieldInfo,
	&t154_f14_FieldInfo,
	&t154_f15_FieldInfo,
	NULL
};
static const int32_t t154_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t154_f0_DefaultValue = 
{
	&t154_f0_FieldInfo, { (char*)&t154_f0_DefaultValueData, &t44_0_0_0 }};
static const float t154_f1_DefaultValueData = 0.9f;
static Il2CppFieldDefaultValueEntry t154_f1_DefaultValue = 
{
	&t154_f1_FieldInfo, { (char*)&t154_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t154_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t154_f2_DefaultValue = 
{
	&t154_f2_FieldInfo, { (char*)&t154_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t154_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t154_f3_DefaultValue = 
{
	&t154_f3_FieldInfo, { (char*)&t154_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t154_FDVs[] = 
{
	&t154_f0_DefaultValue,
	&t154_f1_DefaultValue,
	&t154_f2_DefaultValue,
	&t154_f3_DefaultValue,
	NULL
};
extern MethodInfo m12841_MI;
extern MethodInfo m12843_MI;
static PropertyInfo t154____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t154_TI, "System.Collections.IDictionary.Item", &m12841_MI, &m12843_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12849_MI;
static PropertyInfo t154____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t154_TI, "System.Collections.ICollection.IsSynchronized", &m12849_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12851_MI;
static PropertyInfo t154____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t154_TI, "System.Collections.ICollection.SyncRoot", &m12851_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12853_MI;
static PropertyInfo t154____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t154_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m12853_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t154____Count_PropertyInfo = 
{
	&t154_TI, "Count", &m12868_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t154____Item_PropertyInfo = 
{
	&t154_TI, "Item", &m12870_MI, &m12872_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12901_MI;
static PropertyInfo t154____Values_PropertyInfo = 
{
	&t154_TI, "Values", &m12901_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t154_PIs[] =
{
	&t154____System_Collections_IDictionary_Item_PropertyInfo,
	&t154____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t154____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t154____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t154____Count_PropertyInfo,
	&t154____Item_PropertyInfo,
	&t154____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1541_GM;
MethodInfo m1541_MI = 
{
	".ctor", (methodPointerType)&m12834_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1541_GM};
extern Il2CppType t2454_0_0_0;
static ParameterInfo t154_m12835_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12835_GM;
MethodInfo m12835_MI = 
{
	".ctor", (methodPointerType)&m12836_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t154_m12835_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12835_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t154_m12837_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12837_GM;
MethodInfo m12837_MI = 
{
	".ctor", (methodPointerType)&m12838_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t154_m12837_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12837_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t154_m12839_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12839_GM;
MethodInfo m12839_MI = 
{
	".ctor", (methodPointerType)&m12840_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t154_m12839_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12839_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12841_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12841_GM;
MethodInfo m12841_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12842_gshared, &t154_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t154_m12841_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12841_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12843_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12843_GM;
MethodInfo m12843_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12844_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t154_m12843_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12843_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12845_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12845_GM;
MethodInfo m12845_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12846_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t154_m12845_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12845_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12847_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12847_GM;
MethodInfo m12847_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12848_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t154_m12847_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12847_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12849_GM;
MethodInfo m12849_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12850_gshared, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12849_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12851_GM;
MethodInfo m12851_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12852_gshared, &t154_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12851_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12853_GM;
MethodInfo m12853_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12854_gshared, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12853_GM};
extern Il2CppType t2458_0_0_0;
extern Il2CppType t2458_0_0_0;
static ParameterInfo t154_m12855_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12855_GM;
MethodInfo m12855_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m12855, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t2458, t154_m12855_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12855_GM};
extern Il2CppType t2458_0_0_0;
static ParameterInfo t154_m12856_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12856_GM;
MethodInfo m12856_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m12856, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t2458, t154_m12856_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12856_GM};
extern Il2CppType t2457_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t154_m12857_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2457_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12857_GM;
MethodInfo m12857_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12858_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t154_m12857_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12857_GM};
extern Il2CppType t2458_0_0_0;
static ParameterInfo t154_m12859_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12859_GM;
MethodInfo m12859_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m12859, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t2458, t154_m12859_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12859_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t154_m12860_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12860_GM;
MethodInfo m12860_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12861_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t154_m12860_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12860_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12862_GM;
MethodInfo m12862_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12863_gshared, &t154_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12862_GM};
extern Il2CppType t2459_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12864_GM;
MethodInfo m12864_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12865_gshared, &t154_TI, &t2459_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12864_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12866_GM;
MethodInfo m12866_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12867_gshared, &t154_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12866_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12868_GM;
MethodInfo m12868_MI = 
{
	"get_Count", (methodPointerType)&m12869_gshared, &t154_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12868_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t154_m12870_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12870_GM;
MethodInfo m12870_MI = 
{
	"get_Item", (methodPointerType)&m12871_gshared, &t154_TI, &t350_0_0_0, RuntimeInvoker_t29_t29, t154_m12870_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12870_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t154_m12872_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12872_GM;
MethodInfo m12872_MI = 
{
	"set_Item", (methodPointerType)&m12873_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t154_m12872_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12872_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2454_0_0_0;
static ParameterInfo t154_m12874_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12874_GM;
MethodInfo m12874_MI = 
{
	"Init", (methodPointerType)&m12875_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t154_m12874_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12874_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t154_m12876_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12876_GM;
MethodInfo m12876_MI = 
{
	"InitArrays", (methodPointerType)&m12877_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t154_m12876_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12876_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t154_m12878_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12878_GM;
MethodInfo m12878_MI = 
{
	"CopyToCheck", (methodPointerType)&m12879_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t154_m12878_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12878_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6666_0_0_0;
extern Il2CppType t6666_0_0_0;
static ParameterInfo t154_m27664_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6666_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27664_IGC;
extern TypeInfo m27664_gp_TRet_0_TI;
Il2CppGenericParamFull m27664_gp_TRet_0_TI_GenericParamFull = { { &m27664_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27664_gp_TElem_1_TI;
Il2CppGenericParamFull m27664_gp_TElem_1_TI_GenericParamFull = { { &m27664_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27664_IGPA[2] = 
{
	&m27664_gp_TRet_0_TI_GenericParamFull,
	&m27664_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27664_MI;
Il2CppGenericContainer m27664_IGC = { { NULL, NULL }, NULL, &m27664_MI, 2, 1, m27664_IGPA };
extern Il2CppGenericMethod m27665_GM;
static Il2CppRGCTXDefinition m27664_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27665_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27664_GM;
MethodInfo m27664_MI = 
{
	"Do_CopyTo", NULL, &t154_TI, &t21_0_0_0, NULL, t154_m27664_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27664_RGCTXData, (methodPointerType)NULL, &m27664_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t154_m12880_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12880_GM;
MethodInfo m12880_MI = 
{
	"make_pair", (methodPointerType)&m12880, &t154_TI, &t2458_0_0_0, RuntimeInvoker_t2458_t29_t29, t154_m12880_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12880_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t154_m12881_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12881_GM;
MethodInfo m12881_MI = 
{
	"pick_value", (methodPointerType)&m12882_gshared, &t154_TI, &t350_0_0_0, RuntimeInvoker_t29_t29_t29, t154_m12881_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12881_GM};
extern Il2CppType t2457_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t154_m12883_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2457_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12883_GM;
MethodInfo m12883_MI = 
{
	"CopyTo", (methodPointerType)&m12884_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t154_m12883_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12883_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6669_0_0_0;
extern Il2CppType t6669_0_0_0;
static ParameterInfo t154_m27666_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6669_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27666_IGC;
extern TypeInfo m27666_gp_TRet_0_TI;
Il2CppGenericParamFull m27666_gp_TRet_0_TI_GenericParamFull = { { &m27666_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27666_IGPA[1] = 
{
	&m27666_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27666_MI;
Il2CppGenericContainer m27666_IGC = { { NULL, NULL }, NULL, &m27666_MI, 1, 1, m27666_IGPA };
extern Il2CppGenericMethod m27667_GM;
static Il2CppRGCTXDefinition m27666_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27667_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27666_GM;
MethodInfo m27666_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t154_TI, &t21_0_0_0, NULL, t154_m27666_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27666_RGCTXData, (methodPointerType)NULL, &m27666_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12885_GM;
MethodInfo m12885_MI = 
{
	"Resize", (methodPointerType)&m12886_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12885_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t154_m1544_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1544_GM;
MethodInfo m1544_MI = 
{
	"Add", (methodPointerType)&m12887_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t154_m1544_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1544_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12888_GM;
MethodInfo m12888_MI = 
{
	"Clear", (methodPointerType)&m12889_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12888_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t154_m12890_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12890_GM;
MethodInfo m12890_MI = 
{
	"ContainsKey", (methodPointerType)&m12891_gshared, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t154_m12890_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12890_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t154_m12892_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12892_GM;
MethodInfo m12892_MI = 
{
	"ContainsValue", (methodPointerType)&m12893_gshared, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t154_m12892_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12892_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t154_m12894_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12894_GM;
MethodInfo m12894_MI = 
{
	"GetObjectData", (methodPointerType)&m12895_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t154_m12894_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12894_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12896_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12896_GM;
MethodInfo m12896_MI = 
{
	"OnDeserialization", (methodPointerType)&m12897_gshared, &t154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t154_m12896_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12896_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t154_m12898_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12898_GM;
MethodInfo m12898_MI = 
{
	"Remove", (methodPointerType)&m12899_gshared, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t154_m12898_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12898_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_1_0_2;
extern Il2CppType t350_1_0_0;
static ParameterInfo t154_m1542_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t4147 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1542_GM;
MethodInfo m1542_MI = 
{
	"TryGetValue", (methodPointerType)&m12900_gshared, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t4147, t154_m1542_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1542_GM};
extern Il2CppType t2456_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12901_GM;
MethodInfo m12901_MI = 
{
	"get_Values", (methodPointerType)&m12902_gshared, &t154_TI, &t2456_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12901_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12903_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12903_GM;
MethodInfo m12903_MI = 
{
	"ToTKey", (methodPointerType)&m12904_gshared, &t154_TI, &t148_0_0_0, RuntimeInvoker_t29_t29, t154_m12903_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12903_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t154_m12905_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12905_GM;
MethodInfo m12905_MI = 
{
	"ToTValue", (methodPointerType)&m12906_gshared, &t154_TI, &t350_0_0_0, RuntimeInvoker_t29_t29, t154_m12905_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12905_GM};
extern Il2CppType t2458_0_0_0;
static ParameterInfo t154_m12907_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12907_GM;
MethodInfo m12907_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m12907, &t154_TI, &t40_0_0_0, RuntimeInvoker_t40_t2458, t154_m12907_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12907_GM};
extern Il2CppType t2460_0_0_0;
extern void* RuntimeInvoker_t2460 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12908_GM;
MethodInfo m12908_MI = 
{
	"GetEnumerator", (methodPointerType)&m12908, &t154_TI, &t2460_0_0_0, RuntimeInvoker_t2460, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12908_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t154_m12909_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m12909_GM;
MethodInfo m12909_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12910_gshared, &t154_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t154_m12909_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12909_GM};
static MethodInfo* t154_MIs[] =
{
	&m1541_MI,
	&m12835_MI,
	&m12837_MI,
	&m12839_MI,
	&m12841_MI,
	&m12843_MI,
	&m12845_MI,
	&m12847_MI,
	&m12849_MI,
	&m12851_MI,
	&m12853_MI,
	&m12855_MI,
	&m12856_MI,
	&m12857_MI,
	&m12859_MI,
	&m12860_MI,
	&m12862_MI,
	&m12864_MI,
	&m12866_MI,
	&m12868_MI,
	&m12870_MI,
	&m12872_MI,
	&m12874_MI,
	&m12876_MI,
	&m12878_MI,
	&m27664_MI,
	&m12880_MI,
	&m12881_MI,
	&m12883_MI,
	&m27666_MI,
	&m12885_MI,
	&m1544_MI,
	&m12888_MI,
	&m12890_MI,
	&m12892_MI,
	&m12894_MI,
	&m12896_MI,
	&m12898_MI,
	&m1542_MI,
	&m12901_MI,
	&m12903_MI,
	&m12905_MI,
	&m12907_MI,
	&m12908_MI,
	&m12909_MI,
	NULL
};
extern MethodInfo m12862_MI;
extern MethodInfo m12894_MI;
extern MethodInfo m12860_MI;
extern MethodInfo m12888_MI;
extern MethodInfo m12857_MI;
extern MethodInfo m12864_MI;
extern MethodInfo m12845_MI;
extern MethodInfo m12866_MI;
extern MethodInfo m12847_MI;
extern MethodInfo m12896_MI;
static MethodInfo* t154_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12862_MI,
	&m12894_MI,
	&m12868_MI,
	&m12849_MI,
	&m12851_MI,
	&m12860_MI,
	&m12868_MI,
	&m12853_MI,
	&m12855_MI,
	&m12888_MI,
	&m12856_MI,
	&m12857_MI,
	&m12859_MI,
	&m12864_MI,
	&m12898_MI,
	&m12841_MI,
	&m12843_MI,
	&m12845_MI,
	&m12866_MI,
	&m12847_MI,
	&m12896_MI,
	&m12870_MI,
	&m12872_MI,
	&m1544_MI,
	&m12890_MI,
	&m12894_MI,
	&m12896_MI,
	&m1542_MI,
};
extern TypeInfo t5313_TI;
extern TypeInfo t5315_TI;
extern TypeInfo t6671_TI;
static TypeInfo* t154_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5313_TI,
	&t5315_TI,
	&t6671_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t154_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5313_TI, 10},
	{ &t5315_TI, 17},
	{ &t6671_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern TypeInfo t148_TI;
extern TypeInfo t350_TI;
extern TypeInfo t2457_TI;
extern TypeInfo t154_TI;
extern TypeInfo t2455_TI;
extern TypeInfo t2498_TI;
extern TypeInfo t2460_TI;
extern TypeInfo t2499_TI;
extern TypeInfo t2452_TI;
extern TypeInfo t2453_TI;
extern TypeInfo t2458_TI;
extern TypeInfo t2457_TI;
extern TypeInfo t2454_TI;
extern TypeInfo t2456_TI;
static Il2CppRGCTXData t154_RGCTXData[52] = 
{
	&m12874_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m12890_MI/* Method Usage */,
	&m12903_MI/* Method Usage */,
	&m12870_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m12905_MI/* Method Usage */,
	&m12872_MI/* Method Usage */,
	&m1544_MI/* Method Usage */,
	&m12898_MI/* Method Usage */,
	&m12978_MI/* Method Usage */,
	&m12980_MI/* Method Usage */,
	&m12907_MI/* Method Usage */,
	&m12883_MI/* Method Usage */,
	&t2457_TI/* Class Usage */,
	&m12878_MI/* Method Usage */,
	&t154_TI/* Static Usage */,
	&m12909_MI/* Method Usage */,
	&t2455_TI/* Class Usage */,
	&m13196_MI/* Method Usage */,
	&m20902_MI/* Method Usage */,
	&m12880_MI/* Method Usage */,
	&t2498_TI/* Class Usage */,
	&m13200_MI/* Method Usage */,
	&m20904_MI/* Method Usage */,
	&t2460_TI/* Class Usage */,
	&m13180_MI/* Method Usage */,
	&t2499_TI/* Class Usage */,
	&m13204_MI/* Method Usage */,
	&m27660_MI/* Method Usage */,
	&m27661_MI/* Method Usage */,
	&m12885_MI/* Method Usage */,
	&m13214_MI/* Method Usage */,
	&m12876_MI/* Method Usage */,
	&t2452_TI/* Array Usage */,
	&t2453_TI/* Array Usage */,
	&m12868_MI/* Method Usage */,
	&t2458_TI/* Class Usage */,
	&m12977_MI/* Method Usage */,
	&m20905_MI/* Method Usage */,
	&m13222_MI/* Method Usage */,
	&m27662_MI/* Method Usage */,
	&t2457_TI/* Array Usage */,
	&t2454_0_0_0/* Type Usage */,
	&t2454_TI/* Class Usage */,
	&t2457_0_0_0/* Type Usage */,
	&t2456_TI/* Class Usage */,
	&m13161_MI/* Method Usage */,
	&t148_0_0_0/* Type Usage */,
	&t350_0_0_0/* Type Usage */,
	&m1542_MI/* Method Usage */,
	&m27663_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t154_0_0_0;
extern Il2CppType t154_1_0_0;
struct t154;
extern Il2CppGenericClass t154_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t154_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t154_MIs, t154_PIs, t154_FIs, NULL, &t29_TI, NULL, NULL, &t154_TI, t154_ITIs, t154_VT, &t1254__CustomAttributeCache, &t154_TI, &t154_0_0_0, &t154_1_0_0, t154_IOs, &t154_GC, NULL, t154_FDVs, NULL, t154_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t154), 0, -1, sizeof(t154_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#include "t2461.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2461_TI;
#include "t2461MD.h"

#include "t2464.h"
#include "t2465.h"
#include "t2463.h"
#include "t2471.h"
#include "t2467.h"
#include "t2472.h"
extern TypeInfo t2464_TI;
extern TypeInfo t2462_TI;
extern TypeInfo t2465_TI;
extern TypeInfo t2463_TI;
extern TypeInfo t2471_TI;
extern TypeInfo t2467_TI;
extern TypeInfo t2472_TI;
#include "t2464MD.h"
#include "t2465MD.h"
#include "t2463MD.h"
#include "t2471MD.h"
#include "t2467MD.h"
#include "t2472MD.h"
extern Il2CppType t2462_0_0_0;
extern MethodInfo m12891_MI;
extern MethodInfo m12904_MI;
extern MethodInfo m12871_MI;
extern MethodInfo m12906_MI;
extern MethodInfo m12873_MI;
extern MethodInfo m12886_MI;
extern MethodInfo m12928_MI;
extern MethodInfo m12875_MI;
extern MethodInfo m12887_MI;
extern MethodInfo m12899_MI;
extern MethodInfo m12918_MI;
extern MethodInfo m12920_MI;
extern MethodInfo m12915_MI;
extern MethodInfo m12884_MI;
extern MethodInfo m12879_MI;
extern MethodInfo m12910_MI;
extern MethodInfo m12963_MI;
extern MethodInfo m20792_MI;
extern MethodInfo m12914_MI;
extern MethodInfo m12967_MI;
extern MethodInfo m20794_MI;
extern MethodInfo m12947_MI;
extern MethodInfo m12971_MI;
extern MethodInfo m12877_MI;
extern MethodInfo m12869_MI;
extern MethodInfo m12917_MI;
extern MethodInfo m20795_MI;
extern MethodInfo m12900_MI;
extern MethodInfo m19789_MI;
struct t2461;
 void m20792 (t2461 * __this, t3541* p0, int32_t p1, t2463 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t2461;
 void m20794 (t2461 * __this, t20 * p0, int32_t p1, t2471 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t2461;
 void m20795 (t2461 * __this, t2462* p0, int32_t p1, t2471 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12834_MI;
 void m12834_gshared (t2461 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		(( void (*) (t2461 * __this, int32_t p0, t29* p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, ((int32_t)10), (t29*)NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
extern MethodInfo m12836_MI;
 void m12836_gshared (t2461 * __this, t29* p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		(( void (*) (t2461 * __this, int32_t p0, t29* p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, ((int32_t)10), p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
extern MethodInfo m12838_MI;
 void m12838_gshared (t2461 * __this, int32_t p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		(( void (*) (t2461 * __this, int32_t p0, t29* p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, p0, (t29*)NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
extern MethodInfo m12840_MI;
 void m12840_gshared (t2461 * __this, t733 * p0, t735  p1, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m12842_MI;
 t29 * m12842_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	{
		if (!((t29 *)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t29 * L_1 = (( t29 * (*) (t2461 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		t29 * L_2 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), __this, L_1);
		t29 * L_3 = L_2;
		return ((t29 *)L_3);
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m12844_MI;
 void m12844_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t2461 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		t29 * L_1 = (( t29 * (*) (t2461 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(__this, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		VirtActionInvoker2< t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12846_MI;
 void m12846_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t2461 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		t29 * L_1 = (( t29 * (*) (t2461 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(__this, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		VirtActionInvoker2< t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12848_MI;
 void m12848_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t29 *)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m12850_MI;
 bool m12850_gshared (t2461 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m12852_MI;
 t29 * m12852_gshared (t2461 * __this, MethodInfo* method)
{
	{
		return __this;
	}
}
extern MethodInfo m12854_MI;
 bool m12854_gshared (t2461 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m12911_MI;
 void m12911 (t2461 * __this, t2465  p0, MethodInfo* method){
	{
		t29 * L_0 = m12918((&p0), &m12918_MI);
		t29 * L_1 = m12920((&p0), &m12920_MI);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m12887_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12912_MI;
 bool m12912 (t2461 * __this, t2465  p0, MethodInfo* method){
	{
		bool L_0 = m12915(__this, p0, &m12915_MI);
		return L_0;
	}
}
extern MethodInfo m12858_MI;
 void m12858_gshared (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method)
{
	{
		(( void (*) (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(__this, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return;
	}
}
extern MethodInfo m12913_MI;
 bool m12913 (t2461 * __this, t2465  p0, MethodInfo* method){
	{
		bool L_0 = m12915(__this, p0, &m12915_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t29 * L_1 = m12918((&p0), &m12918_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m12899_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m12861_MI;
 void m12861_gshared (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	t2462* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t2461 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t2461 * G_B4_2 = {0};
	{
		V_0 = ((t2462*)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		(( void (*) (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(__this, V_0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return;
	}

IL_0013:
	{
		(( void (*) (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(__this, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t2461 *)(__this));
		if ((((t2461_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t2461 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17) };
		t2463 * L_1 = (t2463 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		(( void (*) (t2463 * __this, t29 * p0, t35 p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)(L_1, NULL, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		((t2461_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t2461 *)(G_B4_2));
	}

IL_0040:
	{
		(( void (*) (t2461 * __this, t3541* p0, int32_t p1, t2463 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(G_B5_2, G_B5_1, G_B5_0, (((t2461_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->static_fields)->f15), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		return;
	}

IL_004b:
	{
		t35 L_2 = { IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21) };
		t2471 * L_3 = (t2471 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		(( void (*) (t2471 * __this, t29 * p0, t35 p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(L_3, NULL, L_2, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (t2461 * __this, t20 * p0, int32_t p1, t2471 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(__this, p0, p1, L_3, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		return;
	}
}
extern MethodInfo m12863_MI;
 t29 * m12863_gshared (t2461 * __this, MethodInfo* method)
{
	{
		t2467  L_0 = {0};
		(( void (*) (t2467 * __this, t2461 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(&L_0, __this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		t2467  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12865_MI;
 t29* m12865_gshared (t2461 * __this, MethodInfo* method)
{
	{
		t2467  L_0 = {0};
		(( void (*) (t2467 * __this, t2461 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(&L_0, __this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		t2467  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12867_MI;
 t29 * m12867_gshared (t2461 * __this, MethodInfo* method)
{
	{
		t2472 * L_0 = (t2472 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		(( void (*) (t2472 * __this, t2461 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_0, __this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return L_0;
	}
}
 int32_t m12869_gshared (t2461 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 t29 * m12871_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t316* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(t29 **)(t29 **)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m12873_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		(( void (*) (t2461 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t316* L_29 = (__this->f6);
		*((t29 **)(t29 **)SZArrayLdElema(L_29, V_2)) = (t29 *)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t316* L_37 = (__this->f7);
		*((t29 **)(t29 **)SZArrayLdElema(L_37, V_2)) = (t29 *)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m12875_gshared (t2461 * __this, int32_t p0, t29* p1, MethodInfo* method)
{
	t29* V_0 = {0};
	t2461 * G_B4_0 = {0};
	t2461 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t2461 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t2461 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t2461 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t2461 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2001_TI));
		t2001 * L_1 = (( t2001 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t2461 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		(( void (*) (t2461 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		__this->f14 = 0;
		return;
	}
}
 void m12877_gshared (t2461 * __this, int32_t p0, MethodInfo* method)
{
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34), p0));
		__this->f7 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m12879_gshared (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t2465  m12914 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t2465  L_0 = {0};
		m12917(&L_0, p0, p1, &m12917_MI);
		return L_0;
	}
}
extern MethodInfo m12882_MI;
 t29 * m12882_gshared (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		return p1;
	}
}
 void m12884_gshared (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method)
{
	{
		(( void (*) (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(__this, (t20 *)(t20 *)p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		t35 L_0 = { IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21) };
		t2471 * L_1 = (t2471 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		(( void (*) (t2471 * __this, t29 * p0, t35 p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(L_1, NULL, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (t2461 * __this, t2462* p0, int32_t p1, t2471 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)(__this, p0, p1, L_1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}
}
 void m12886_gshared (t2461 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t316* V_7 = {0};
	t316* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t316* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_4, (*(t29 **)(t29 **)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34), V_0));
		V_8 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), V_0));
		t316* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t316* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m12887_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		(( void (*) (t2461 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t316* L_30 = (__this->f6);
		*((t29 **)(t29 **)SZArrayLdElema(L_30, V_2)) = (t29 *)p0;
		t316* L_31 = (__this->f7);
		*((t29 **)(t29 **)SZArrayLdElema(L_31, V_2)) = (t29 *)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
extern MethodInfo m12889_MI;
 void m12889_gshared (t2461 * __this, MethodInfo* method)
{
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t316* L_2 = (__this->f6);
		t316* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t316* L_4 = (__this->f7);
		t316* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m12891_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12893_MI;
 bool m12893_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2001_TI));
		t2001 * L_0 = (( t2001 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t316* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41), V_0, (*(t29 **)(t29 **)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12895_MI;
 void m12895_gshared (t2461 * __this, t733 * p0, t735  p1, MethodInfo* method)
{
	t2462* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2462*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2462*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), L_4));
		(( void (*) (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(__this, V_0, 0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m12897_MI;
 void m12897_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t2462* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2462*)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		(( void (*) (t2461 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(__this, V_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t29 * L_11 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(((t2465 *)(t2465 *)SZArrayLdElema(V_1, V_2)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		t29 * L_12 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(((t2465 *)(t2465 *)SZArrayLdElema(V_1, V_2)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		VirtActionInvoker2< t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m12899_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t29 * V_4 = {0};
	t29 * V_5 = {0};
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t316* L_25 = (__this->f6);
		Initobj (&t29_TI, (&V_4));
		*((t29 **)(t29 **)SZArrayLdElema(L_25, V_2)) = (t29 *)V_4;
		t316* L_26 = (__this->f7);
		Initobj (&t29_TI, (&V_5));
		*((t29 **)(t29 **)SZArrayLdElema(L_26, V_2)) = (t29 *)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m12900_gshared (t2461 * __this, t29 * p0, t29 ** p1, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t29 * V_2 = {0};
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t316* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), L_9, (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t316* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(t29 **)(t29 **)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t29_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m12902_MI;
 t2464 * m12902_gshared (t2461 * __this, MethodInfo* method)
{
	{
		t2464 * L_0 = (t2464 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		(( void (*) (t2464 * __this, t2461 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47)->method)(L_0, __this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		return L_0;
	}
}
 t29 * m12904_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t29 *)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48)), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}
}
 t29 * m12906_gshared (t2461 * __this, t29 * p0, MethodInfo* method)
{
	t29 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49)), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t29_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49)), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)));
	}
}
 bool m12915 (t2461 * __this, t2465  p0, MethodInfo* method){
	t29 * V_0 = {0};
	{
		t29 * L_0 = m12918((&p0), &m12918_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t29 *, t29 ** >::Invoke(&m12900_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2001_TI));
		t2001 * L_2 = m10707(NULL, &m10707_MI);
		t29 * L_3 = m12920((&p0), &m12920_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, t29 *, t29 * >::Invoke(&m19789_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m12916_MI;
 t2467  m12916 (t2461 * __this, MethodInfo* method){
	{
		t2467  L_0 = {0};
		m12947(&L_0, __this, &m12947_MI);
		return L_0;
	}
}
 t725  m12910_gshared (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		t29 * L_0 = p0;
		t29 * L_1 = p1;
		t725  L_2 = {0};
		m3965(&L_2, ((t29 *)L_0), ((t29 *)L_1), &m3965_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.Object,System.Object>
extern Il2CppType t44_0_0_32849;
FieldInfo t2461_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2461_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t2461_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t2461_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t2461_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t2461_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t2461_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t2461_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t2461_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t2461_TI, offsetof(t2461, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t2461_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t2461_TI, offsetof(t2461, f5), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t2461_f6_FieldInfo = 
{
	"keySlots", &t316_0_0_1, &t2461_TI, offsetof(t2461, f6), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t2461_f7_FieldInfo = 
{
	"valueSlots", &t316_0_0_1, &t2461_TI, offsetof(t2461, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2461_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t2461_TI, offsetof(t2461, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2461_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t2461_TI, offsetof(t2461, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2461_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t2461_TI, offsetof(t2461, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2461_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t2461_TI, offsetof(t2461, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2416_0_0_1;
FieldInfo t2461_f12_FieldInfo = 
{
	"hcp", &t2416_0_0_1, &t2461_TI, offsetof(t2461, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t2461_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t2461_TI, offsetof(t2461, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2461_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t2461_TI, offsetof(t2461, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2463_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t2461_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2463_0_0_17, &t2461_TI, offsetof(t2461_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t2461_FIs[] =
{
	&t2461_f0_FieldInfo,
	&t2461_f1_FieldInfo,
	&t2461_f2_FieldInfo,
	&t2461_f3_FieldInfo,
	&t2461_f4_FieldInfo,
	&t2461_f5_FieldInfo,
	&t2461_f6_FieldInfo,
	&t2461_f7_FieldInfo,
	&t2461_f8_FieldInfo,
	&t2461_f9_FieldInfo,
	&t2461_f10_FieldInfo,
	&t2461_f11_FieldInfo,
	&t2461_f12_FieldInfo,
	&t2461_f13_FieldInfo,
	&t2461_f14_FieldInfo,
	&t2461_f15_FieldInfo,
	NULL
};
static const int32_t t2461_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t2461_f0_DefaultValue = 
{
	&t2461_f0_FieldInfo, { (char*)&t2461_f0_DefaultValueData, &t44_0_0_0 }};
static const float t2461_f1_DefaultValueData = 0.9f;
static Il2CppFieldDefaultValueEntry t2461_f1_DefaultValue = 
{
	&t2461_f1_FieldInfo, { (char*)&t2461_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t2461_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t2461_f2_DefaultValue = 
{
	&t2461_f2_FieldInfo, { (char*)&t2461_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t2461_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t2461_f3_DefaultValue = 
{
	&t2461_f3_FieldInfo, { (char*)&t2461_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2461_FDVs[] = 
{
	&t2461_f0_DefaultValue,
	&t2461_f1_DefaultValue,
	&t2461_f2_DefaultValue,
	&t2461_f3_DefaultValue,
	NULL
};
static PropertyInfo t2461____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t2461_TI, "System.Collections.IDictionary.Item", &m12842_MI, &m12844_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2461____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2461_TI, "System.Collections.ICollection.IsSynchronized", &m12850_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2461____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2461_TI, "System.Collections.ICollection.SyncRoot", &m12852_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2461____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t2461_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m12854_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2461____Count_PropertyInfo = 
{
	&t2461_TI, "Count", &m12869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2461____Item_PropertyInfo = 
{
	&t2461_TI, "Item", &m12871_MI, &m12873_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2461____Values_PropertyInfo = 
{
	&t2461_TI, "Values", &m12902_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2461_PIs[] =
{
	&t2461____System_Collections_IDictionary_Item_PropertyInfo,
	&t2461____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2461____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2461____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t2461____Count_PropertyInfo,
	&t2461____Item_PropertyInfo,
	&t2461____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12834_GM;
MethodInfo m12834_MI = 
{
	".ctor", (methodPointerType)&m12834_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12834_GM};
extern Il2CppType t2416_0_0_0;
static ParameterInfo t2461_m12836_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12836_GM;
MethodInfo m12836_MI = 
{
	".ctor", (methodPointerType)&m12836_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2461_m12836_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12836_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2461_m12838_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12838_GM;
MethodInfo m12838_MI = 
{
	".ctor", (methodPointerType)&m12838_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2461_m12838_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12838_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t2461_m12840_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12840_GM;
MethodInfo m12840_MI = 
{
	".ctor", (methodPointerType)&m12840_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t2461_m12840_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12840_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12842_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12842_GM;
MethodInfo m12842_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12842_gshared, &t2461_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2461_m12842_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12842_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12844_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12844_GM;
MethodInfo m12844_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12844_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2461_m12844_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12844_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12846_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12846_GM;
MethodInfo m12846_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12846_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2461_m12846_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12846_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12848_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12848_GM;
MethodInfo m12848_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12848_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2461_m12848_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12848_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12850_GM;
MethodInfo m12850_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12850_gshared, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12850_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12852_GM;
MethodInfo m12852_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12852_gshared, &t2461_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12852_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12854_GM;
MethodInfo m12854_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12854_gshared, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12854_GM};
extern Il2CppType t2465_0_0_0;
extern Il2CppType t2465_0_0_0;
static ParameterInfo t2461_m12911_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12911_GM;
MethodInfo m12911_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m12911, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t2465, t2461_m12911_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12911_GM};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t2461_m12912_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12912_GM;
MethodInfo m12912_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m12912, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t2465, t2461_m12912_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12912_GM};
extern Il2CppType t2462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2461_m12858_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2462_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12858_GM;
MethodInfo m12858_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12858_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2461_m12858_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12858_GM};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t2461_m12913_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12913_GM;
MethodInfo m12913_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m12913, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t2465, t2461_m12913_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12913_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2461_m12861_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12861_GM;
MethodInfo m12861_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12861_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2461_m12861_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12861_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12863_GM;
MethodInfo m12863_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12863_gshared, &t2461_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12863_GM};
extern Il2CppType t2466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12865_GM;
MethodInfo m12865_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12865_gshared, &t2461_TI, &t2466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12865_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12867_GM;
MethodInfo m12867_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12867_gshared, &t2461_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12867_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12869_GM;
MethodInfo m12869_MI = 
{
	"get_Count", (methodPointerType)&m12869_gshared, &t2461_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12869_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12871_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12871_GM;
MethodInfo m12871_MI = 
{
	"get_Item", (methodPointerType)&m12871_gshared, &t2461_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2461_m12871_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12871_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12873_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12873_GM;
MethodInfo m12873_MI = 
{
	"set_Item", (methodPointerType)&m12873_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2461_m12873_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12873_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2416_0_0_0;
static ParameterInfo t2461_m12875_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12875_GM;
MethodInfo m12875_MI = 
{
	"Init", (methodPointerType)&m12875_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2461_m12875_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12875_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2461_m12877_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12877_GM;
MethodInfo m12877_MI = 
{
	"InitArrays", (methodPointerType)&m12877_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2461_m12877_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12877_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2461_m12879_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12879_GM;
MethodInfo m12879_MI = 
{
	"CopyToCheck", (methodPointerType)&m12879_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2461_m12879_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12879_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6672_0_0_0;
extern Il2CppType t6672_0_0_0;
static ParameterInfo t2461_m27668_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6672_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27668_IGC;
extern TypeInfo m27668_gp_TRet_0_TI;
Il2CppGenericParamFull m27668_gp_TRet_0_TI_GenericParamFull = { { &m27668_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27668_gp_TElem_1_TI;
Il2CppGenericParamFull m27668_gp_TElem_1_TI_GenericParamFull = { { &m27668_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27668_IGPA[2] = 
{
	&m27668_gp_TRet_0_TI_GenericParamFull,
	&m27668_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27668_MI;
Il2CppGenericContainer m27668_IGC = { { NULL, NULL }, NULL, &m27668_MI, 2, 1, m27668_IGPA };
extern Il2CppGenericMethod m27669_GM;
static Il2CppRGCTXDefinition m27668_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27669_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27668_GM;
MethodInfo m27668_MI = 
{
	"Do_CopyTo", NULL, &t2461_TI, &t21_0_0_0, NULL, t2461_m27668_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27668_RGCTXData, (methodPointerType)NULL, &m27668_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12914_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12914_GM;
MethodInfo m12914_MI = 
{
	"make_pair", (methodPointerType)&m12914, &t2461_TI, &t2465_0_0_0, RuntimeInvoker_t2465_t29_t29, t2461_m12914_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12914_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12882_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12882_GM;
MethodInfo m12882_MI = 
{
	"pick_value", (methodPointerType)&m12882_gshared, &t2461_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t2461_m12882_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12882_GM};
extern Il2CppType t2462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2461_m12884_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2462_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12884_GM;
MethodInfo m12884_MI = 
{
	"CopyTo", (methodPointerType)&m12884_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2461_m12884_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12884_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6675_0_0_0;
extern Il2CppType t6675_0_0_0;
static ParameterInfo t2461_m27670_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6675_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27670_IGC;
extern TypeInfo m27670_gp_TRet_0_TI;
Il2CppGenericParamFull m27670_gp_TRet_0_TI_GenericParamFull = { { &m27670_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27670_IGPA[1] = 
{
	&m27670_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27670_MI;
Il2CppGenericContainer m27670_IGC = { { NULL, NULL }, NULL, &m27670_MI, 1, 1, m27670_IGPA };
extern Il2CppGenericMethod m27671_GM;
static Il2CppRGCTXDefinition m27670_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27671_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27670_GM;
MethodInfo m27670_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t2461_TI, &t21_0_0_0, NULL, t2461_m27670_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27670_RGCTXData, (methodPointerType)NULL, &m27670_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12886_GM;
MethodInfo m12886_MI = 
{
	"Resize", (methodPointerType)&m12886_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12886_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12887_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12887_GM;
MethodInfo m12887_MI = 
{
	"Add", (methodPointerType)&m12887_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2461_m12887_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12887_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12889_GM;
MethodInfo m12889_MI = 
{
	"Clear", (methodPointerType)&m12889_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12889_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12891_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12891_GM;
MethodInfo m12891_MI = 
{
	"ContainsKey", (methodPointerType)&m12891_gshared, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2461_m12891_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12891_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12893_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12893_GM;
MethodInfo m12893_MI = 
{
	"ContainsValue", (methodPointerType)&m12893_gshared, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2461_m12893_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12893_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t2461_m12895_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12895_GM;
MethodInfo m12895_MI = 
{
	"GetObjectData", (methodPointerType)&m12895_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t2461_m12895_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12895_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12897_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12897_GM;
MethodInfo m12897_MI = 
{
	"OnDeserialization", (methodPointerType)&m12897_gshared, &t2461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2461_m12897_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12897_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12899_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12899_GM;
MethodInfo m12899_MI = 
{
	"Remove", (methodPointerType)&m12899_gshared, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2461_m12899_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12899_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t29_1_0_0;
static ParameterInfo t2461_m12900_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t2022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12900_GM;
MethodInfo m12900_MI = 
{
	"TryGetValue", (methodPointerType)&m12900_gshared, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t2022, t2461_m12900_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12900_GM};
extern Il2CppType t2464_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12902_GM;
MethodInfo m12902_MI = 
{
	"get_Values", (methodPointerType)&m12902_gshared, &t2461_TI, &t2464_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12902_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12904_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12904_GM;
MethodInfo m12904_MI = 
{
	"ToTKey", (methodPointerType)&m12904_gshared, &t2461_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2461_m12904_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12904_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12906_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12906_GM;
MethodInfo m12906_MI = 
{
	"ToTValue", (methodPointerType)&m12906_gshared, &t2461_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2461_m12906_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12906_GM};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t2461_m12915_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12915_GM;
MethodInfo m12915_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m12915, &t2461_TI, &t40_0_0_0, RuntimeInvoker_t40_t2465, t2461_m12915_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12915_GM};
extern Il2CppType t2467_0_0_0;
extern void* RuntimeInvoker_t2467 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12916_GM;
MethodInfo m12916_MI = 
{
	"GetEnumerator", (methodPointerType)&m12916, &t2461_TI, &t2467_0_0_0, RuntimeInvoker_t2467, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12916_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2461_m12910_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m12910_GM;
MethodInfo m12910_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12910_gshared, &t2461_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t2461_m12910_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12910_GM};
static MethodInfo* t2461_MIs[] =
{
	&m12834_MI,
	&m12836_MI,
	&m12838_MI,
	&m12840_MI,
	&m12842_MI,
	&m12844_MI,
	&m12846_MI,
	&m12848_MI,
	&m12850_MI,
	&m12852_MI,
	&m12854_MI,
	&m12911_MI,
	&m12912_MI,
	&m12858_MI,
	&m12913_MI,
	&m12861_MI,
	&m12863_MI,
	&m12865_MI,
	&m12867_MI,
	&m12869_MI,
	&m12871_MI,
	&m12873_MI,
	&m12875_MI,
	&m12877_MI,
	&m12879_MI,
	&m27668_MI,
	&m12914_MI,
	&m12882_MI,
	&m12884_MI,
	&m27670_MI,
	&m12886_MI,
	&m12887_MI,
	&m12889_MI,
	&m12891_MI,
	&m12893_MI,
	&m12895_MI,
	&m12897_MI,
	&m12899_MI,
	&m12900_MI,
	&m12902_MI,
	&m12904_MI,
	&m12906_MI,
	&m12915_MI,
	&m12916_MI,
	&m12910_MI,
	NULL
};
static MethodInfo* t2461_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12863_MI,
	&m12895_MI,
	&m12869_MI,
	&m12850_MI,
	&m12852_MI,
	&m12861_MI,
	&m12869_MI,
	&m12854_MI,
	&m12911_MI,
	&m12889_MI,
	&m12912_MI,
	&m12858_MI,
	&m12913_MI,
	&m12865_MI,
	&m12899_MI,
	&m12842_MI,
	&m12844_MI,
	&m12846_MI,
	&m12867_MI,
	&m12848_MI,
	&m12897_MI,
	&m12871_MI,
	&m12873_MI,
	&m12887_MI,
	&m12891_MI,
	&m12895_MI,
	&m12897_MI,
	&m12900_MI,
};
extern TypeInfo t5301_TI;
extern TypeInfo t5303_TI;
extern TypeInfo t6677_TI;
static TypeInfo* t2461_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5301_TI,
	&t5303_TI,
	&t6677_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t2461_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5301_TI, 10},
	{ &t5303_TI, 17},
	{ &t6677_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2462_TI;
extern TypeInfo t2461_TI;
extern TypeInfo t2463_TI;
extern TypeInfo t2471_TI;
extern TypeInfo t2467_TI;
extern TypeInfo t2472_TI;
extern TypeInfo t316_TI;
extern TypeInfo t316_TI;
extern TypeInfo t2465_TI;
extern TypeInfo t2462_TI;
extern TypeInfo t2416_TI;
extern TypeInfo t2464_TI;
static Il2CppRGCTXData t2461_RGCTXData[52] = 
{
	&m12875_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12891_MI/* Method Usage */,
	&m12904_MI/* Method Usage */,
	&m12871_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12906_MI/* Method Usage */,
	&m12873_MI/* Method Usage */,
	&m12887_MI/* Method Usage */,
	&m12899_MI/* Method Usage */,
	&m12918_MI/* Method Usage */,
	&m12920_MI/* Method Usage */,
	&m12915_MI/* Method Usage */,
	&m12884_MI/* Method Usage */,
	&t2462_TI/* Class Usage */,
	&m12879_MI/* Method Usage */,
	&t2461_TI/* Static Usage */,
	&m12910_MI/* Method Usage */,
	&t2463_TI/* Class Usage */,
	&m12963_MI/* Method Usage */,
	&m20792_MI/* Method Usage */,
	&m12914_MI/* Method Usage */,
	&t2471_TI/* Class Usage */,
	&m12967_MI/* Method Usage */,
	&m20794_MI/* Method Usage */,
	&t2467_TI/* Class Usage */,
	&m12947_MI/* Method Usage */,
	&t2472_TI/* Class Usage */,
	&m12971_MI/* Method Usage */,
	&m26621_MI/* Method Usage */,
	&m26620_MI/* Method Usage */,
	&m12886_MI/* Method Usage */,
	&m10707_MI/* Method Usage */,
	&m12877_MI/* Method Usage */,
	&t316_TI/* Array Usage */,
	&t316_TI/* Array Usage */,
	&m12869_MI/* Method Usage */,
	&t2465_TI/* Class Usage */,
	&m12917_MI/* Method Usage */,
	&m20795_MI/* Method Usage */,
	&m10707_MI/* Method Usage */,
	&m26620_MI/* Method Usage */,
	&t2462_TI/* Array Usage */,
	&t2416_0_0_0/* Type Usage */,
	&t2416_TI/* Class Usage */,
	&t2462_0_0_0/* Type Usage */,
	&t2464_TI/* Class Usage */,
	&m12928_MI/* Method Usage */,
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&m12900_MI/* Method Usage */,
	&m19789_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2461_0_0_0;
extern Il2CppType t2461_1_0_0;
struct t2461;
extern Il2CppGenericClass t2461_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t2461_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t2461_MIs, t2461_PIs, t2461_FIs, NULL, &t29_TI, NULL, NULL, &t2461_TI, t2461_ITIs, t2461_VT, &t1254__CustomAttributeCache, &t2461_TI, &t2461_0_0_0, &t2461_1_0_0, t2461_IOs, &t2461_GC, NULL, t2461_FDVs, NULL, t2461_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2461), 0, -1, sizeof(t2461_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
extern MethodInfo m27672_MI;
static PropertyInfo t5301____Count_PropertyInfo = 
{
	&t5301_TI, "Count", &m27672_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27673_MI;
static PropertyInfo t5301____IsReadOnly_PropertyInfo = 
{
	&t5301_TI, "IsReadOnly", &m27673_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5301_PIs[] =
{
	&t5301____Count_PropertyInfo,
	&t5301____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27672_GM;
MethodInfo m27672_MI = 
{
	"get_Count", NULL, &t5301_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27672_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27673_GM;
MethodInfo m27673_MI = 
{
	"get_IsReadOnly", NULL, &t5301_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27673_GM};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t5301_m27674_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27674_GM;
MethodInfo m27674_MI = 
{
	"Add", NULL, &t5301_TI, &t21_0_0_0, RuntimeInvoker_t21_t2465, t5301_m27674_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27674_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27675_GM;
MethodInfo m27675_MI = 
{
	"Clear", NULL, &t5301_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27675_GM};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t5301_m27676_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27676_GM;
MethodInfo m27676_MI = 
{
	"Contains", NULL, &t5301_TI, &t40_0_0_0, RuntimeInvoker_t40_t2465, t5301_m27676_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27676_GM};
extern Il2CppType t2462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5301_m27677_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2462_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27677_GM;
MethodInfo m27677_MI = 
{
	"CopyTo", NULL, &t5301_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5301_m27677_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27677_GM};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t5301_m27678_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27678_GM;
MethodInfo m27678_MI = 
{
	"Remove", NULL, &t5301_TI, &t40_0_0_0, RuntimeInvoker_t40_t2465, t5301_m27678_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27678_GM};
static MethodInfo* t5301_MIs[] =
{
	&m27672_MI,
	&m27673_MI,
	&m27674_MI,
	&m27675_MI,
	&m27676_MI,
	&m27677_MI,
	&m27678_MI,
	NULL
};
static TypeInfo* t5301_ITIs[] = 
{
	&t603_TI,
	&t5303_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5301_0_0_0;
extern Il2CppType t5301_1_0_0;
struct t5301;
extern Il2CppGenericClass t5301_GC;
TypeInfo t5301_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5301_MIs, t5301_PIs, NULL, NULL, NULL, NULL, NULL, &t5301_TI, t5301_ITIs, NULL, &EmptyCustomAttributesCache, &t5301_TI, &t5301_0_0_0, &t5301_1_0_0, NULL, &t5301_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
extern Il2CppType t2466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27679_GM;
MethodInfo m27679_MI = 
{
	"GetEnumerator", NULL, &t5303_TI, &t2466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27679_GM};
static MethodInfo* t5303_MIs[] =
{
	&m27679_MI,
	NULL
};
static TypeInfo* t5303_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5303_0_0_0;
extern Il2CppType t5303_1_0_0;
struct t5303;
extern Il2CppGenericClass t5303_GC;
TypeInfo t5303_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5303_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5303_TI, t5303_ITIs, NULL, &EmptyCustomAttributesCache, &t5303_TI, &t5303_0_0_0, &t5303_1_0_0, NULL, &t5303_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2466_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
extern MethodInfo m27680_MI;
static PropertyInfo t2466____Current_PropertyInfo = 
{
	&t2466_TI, "Current", &m27680_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2466_PIs[] =
{
	&t2466____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27680_GM;
MethodInfo m27680_MI = 
{
	"get_Current", NULL, &t2466_TI, &t2465_0_0_0, RuntimeInvoker_t2465, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27680_GM};
static MethodInfo* t2466_MIs[] =
{
	&m27680_MI,
	NULL
};
static TypeInfo* t2466_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2466_0_0_0;
extern Il2CppType t2466_1_0_0;
struct t2466;
extern Il2CppGenericClass t2466_GC;
TypeInfo t2466_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2466_MIs, t2466_PIs, NULL, NULL, NULL, NULL, NULL, &t2466_TI, t2466_ITIs, NULL, &EmptyCustomAttributesCache, &t2466_TI, &t2466_0_0_0, &t2466_1_0_0, NULL, &t2466_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12919_MI;
extern MethodInfo m12921_MI;


 void m12917_gshared (t2465 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		(( void (*) (t2465 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (t2465 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
 t29 * m12918_gshared (t2465 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
 void m12919_gshared (t2465 * __this, t29 * p0, MethodInfo* method)
{
	{
		__this->f0 = p0;
		return;
	}
}
 t29 * m12920_gshared (t2465 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
 void m12921_gshared (t2465 * __this, t29 * p0, MethodInfo* method)
{
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m12922_MI;
 t7* m12922_gshared (t2465 * __this, MethodInfo* method)
{
	t29 * V_0 = {0};
	t29 * V_1 = {0};
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t29 * L_2 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		t29 * L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t29 *)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t29 * L_4 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		t29 * L_8 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		t29 * L_9 = L_8;
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!((t29 *)L_9))
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		t29 * L_10 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = L_10;
		t7* L_11 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_12 = G_B6_3;
		ArrayElementTypeCheck (L_12, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_12, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_13 = m4008(NULL, L_12, &m4008_MI);
		return L_13;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
extern Il2CppType t29_0_0_1;
FieldInfo t2465_f0_FieldInfo = 
{
	"key", &t29_0_0_1, &t2465_TI, offsetof(t2465, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2465_f1_FieldInfo = 
{
	"value", &t29_0_0_1, &t2465_TI, offsetof(t2465, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2465_FIs[] =
{
	&t2465_f0_FieldInfo,
	&t2465_f1_FieldInfo,
	NULL
};
static PropertyInfo t2465____Key_PropertyInfo = 
{
	&t2465_TI, "Key", &m12918_MI, &m12919_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2465____Value_PropertyInfo = 
{
	&t2465_TI, "Value", &m12920_MI, &m12921_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2465_PIs[] =
{
	&t2465____Key_PropertyInfo,
	&t2465____Value_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2465_m12917_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12917_GM;
MethodInfo m12917_MI = 
{
	".ctor", (methodPointerType)&m12917_gshared, &t2465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2465_m12917_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12917_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12918_GM;
MethodInfo m12918_MI = 
{
	"get_Key", (methodPointerType)&m12918_gshared, &t2465_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12918_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2465_m12919_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12919_GM;
MethodInfo m12919_MI = 
{
	"set_Key", (methodPointerType)&m12919_gshared, &t2465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2465_m12919_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12919_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12920_GM;
MethodInfo m12920_MI = 
{
	"get_Value", (methodPointerType)&m12920_gshared, &t2465_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12920_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2465_m12921_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12921_GM;
MethodInfo m12921_MI = 
{
	"set_Value", (methodPointerType)&m12921_gshared, &t2465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2465_m12921_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12921_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12922_GM;
MethodInfo m12922_MI = 
{
	"ToString", (methodPointerType)&m12922_gshared, &t2465_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12922_GM};
static MethodInfo* t2465_MIs[] =
{
	&m12917_MI,
	&m12918_MI,
	&m12919_MI,
	&m12920_MI,
	&m12921_MI,
	&m12922_MI,
	NULL
};
static MethodInfo* t2465_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m12922_MI,
};
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2465_RGCTXData[6] = 
{
	&m12919_MI/* Method Usage */,
	&m12921_MI/* Method Usage */,
	&m12918_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12920_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2465_1_0_0;
extern Il2CppGenericClass t2465_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2465_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2465_MIs, t2465_PIs, t2465_FIs, NULL, &t110_TI, NULL, NULL, &t2465_TI, NULL, t2465_VT, &t1259__CustomAttributeCache, &t2465_TI, &t2465_0_0_0, &t2465_1_0_0, NULL, &t2465_GC, NULL, NULL, NULL, t2465_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2465)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2468.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2468_TI;
#include "t2468MD.h"

extern MethodInfo m12927_MI;
extern MethodInfo m20780_MI;
struct t20;
 t2465  m20780 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12923_MI;
 void m12923 (t2468 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12924_MI;
 t29 * m12924 (t2468 * __this, MethodInfo* method){
	{
		t2465  L_0 = m12927(__this, &m12927_MI);
		t2465  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2465_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12925_MI;
 void m12925 (t2468 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12926_MI;
 bool m12926 (t2468 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2465  m12927 (t2468 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2465  L_8 = m20780(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20780_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
extern Il2CppType t20_0_0_1;
FieldInfo t2468_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2468_TI, offsetof(t2468, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2468_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2468_TI, offsetof(t2468, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2468_FIs[] =
{
	&t2468_f0_FieldInfo,
	&t2468_f1_FieldInfo,
	NULL
};
static PropertyInfo t2468____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2468_TI, "System.Collections.IEnumerator.Current", &m12924_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2468____Current_PropertyInfo = 
{
	&t2468_TI, "Current", &m12927_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2468_PIs[] =
{
	&t2468____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2468____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2468_m12923_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12923_GM;
MethodInfo m12923_MI = 
{
	".ctor", (methodPointerType)&m12923, &t2468_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2468_m12923_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12923_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12924_GM;
MethodInfo m12924_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12924, &t2468_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12924_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12925_GM;
MethodInfo m12925_MI = 
{
	"Dispose", (methodPointerType)&m12925, &t2468_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12925_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12926_GM;
MethodInfo m12926_MI = 
{
	"MoveNext", (methodPointerType)&m12926, &t2468_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12926_GM};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12927_GM;
MethodInfo m12927_MI = 
{
	"get_Current", (methodPointerType)&m12927, &t2468_TI, &t2465_0_0_0, RuntimeInvoker_t2465, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12927_GM};
static MethodInfo* t2468_MIs[] =
{
	&m12923_MI,
	&m12924_MI,
	&m12925_MI,
	&m12926_MI,
	&m12927_MI,
	NULL
};
static MethodInfo* t2468_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12924_MI,
	&m12926_MI,
	&m12925_MI,
	&m12927_MI,
};
static TypeInfo* t2468_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2466_TI,
};
static Il2CppInterfaceOffsetPair t2468_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2466_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2468_0_0_0;
extern Il2CppType t2468_1_0_0;
extern Il2CppGenericClass t2468_GC;
TypeInfo t2468_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2468_MIs, t2468_PIs, t2468_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2468_TI, t2468_ITIs, t2468_VT, &EmptyCustomAttributesCache, &t2468_TI, &t2468_0_0_0, &t2468_1_0_0, t2468_IOs, &t2468_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2468)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5302_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
extern MethodInfo m27681_MI;
extern MethodInfo m27682_MI;
static PropertyInfo t5302____Item_PropertyInfo = 
{
	&t5302_TI, "Item", &m27681_MI, &m27682_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5302_PIs[] =
{
	&t5302____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2465_0_0_0;
static ParameterInfo t5302_m27683_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27683_GM;
MethodInfo m27683_MI = 
{
	"IndexOf", NULL, &t5302_TI, &t44_0_0_0, RuntimeInvoker_t44_t2465, t5302_m27683_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27683_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2465_0_0_0;
static ParameterInfo t5302_m27684_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27684_GM;
MethodInfo m27684_MI = 
{
	"Insert", NULL, &t5302_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2465, t5302_m27684_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27684_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5302_m27685_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27685_GM;
MethodInfo m27685_MI = 
{
	"RemoveAt", NULL, &t5302_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5302_m27685_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27685_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5302_m27681_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27681_GM;
MethodInfo m27681_MI = 
{
	"get_Item", NULL, &t5302_TI, &t2465_0_0_0, RuntimeInvoker_t2465_t44, t5302_m27681_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27681_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2465_0_0_0;
static ParameterInfo t5302_m27682_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27682_GM;
MethodInfo m27682_MI = 
{
	"set_Item", NULL, &t5302_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2465, t5302_m27682_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27682_GM};
static MethodInfo* t5302_MIs[] =
{
	&m27683_MI,
	&m27684_MI,
	&m27685_MI,
	&m27681_MI,
	&m27682_MI,
	NULL
};
static TypeInfo* t5302_ITIs[] = 
{
	&t603_TI,
	&t5301_TI,
	&t5303_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5302_0_0_0;
extern Il2CppType t5302_1_0_0;
struct t5302;
extern Il2CppGenericClass t5302_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5302_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5302_MIs, t5302_PIs, NULL, NULL, NULL, NULL, NULL, &t5302_TI, t5302_ITIs, NULL, &t1908__CustomAttributeCache, &t5302_TI, &t5302_0_0_0, &t5302_1_0_0, NULL, &t5302_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.Object,System.Object>
extern Il2CppType t29_0_0_0;
static ParameterInfo t6677_m27686_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27686_GM;
MethodInfo m27686_MI = 
{
	"Remove", NULL, &t6677_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6677_m27686_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27686_GM};
static MethodInfo* t6677_MIs[] =
{
	&m27686_MI,
	NULL
};
static TypeInfo* t6677_ITIs[] = 
{
	&t603_TI,
	&t5301_TI,
	&t5303_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6677_0_0_0;
extern Il2CppType t6677_1_0_0;
struct t6677;
extern Il2CppGenericClass t6677_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6677_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6677_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6677_TI, t6677_ITIs, NULL, &t1975__CustomAttributeCache, &t6677_TI, &t6677_0_0_0, &t6677_1_0_0, NULL, &t6677_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2469.h"
#include "t2470.h"
extern TypeInfo t2469_TI;
extern TypeInfo t2470_TI;
#include "t2470MD.h"
#include "t2469MD.h"
extern MethodInfo m12940_MI;
extern MethodInfo m12939_MI;
extern MethodInfo m12959_MI;
extern MethodInfo m20791_MI;
extern MethodInfo m20790_MI;
extern MethodInfo m12942_MI;
struct t2461;
 void m20791_gshared (t2461 * __this, t20 * p0, int32_t p1, t2470 * p2, MethodInfo* method);
#define m20791(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
struct t2461;
 void m20790 (t2461 * __this, t316* p0, int32_t p1, t2470 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12928_gshared (t2464 * __this, t2461 * p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m12929_MI;
 void m12929_gshared (t2464 * __this, t29 * p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12930_MI;
 void m12930_gshared (t2464 * __this, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12931_MI;
 bool m12931_gshared (t2464 * __this, t29 * p0, MethodInfo* method)
{
	{
		t2461 * L_0 = (__this->f0);
		bool L_1 = (( bool (*) (t2461 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(L_0, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_1;
	}
}
extern MethodInfo m12932_MI;
 bool m12932_gshared (t2464 * __this, t29 * p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12933_MI;
 t29* m12933_gshared (t2464 * __this, MethodInfo* method)
{
	{
		t2469  L_0 = (( t2469  (*) (t2464 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		t2469  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12934_MI;
 void m12934_gshared (t2464 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	t316* V_0 = {0};
	{
		V_0 = ((t316*)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t316*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t2461 * L_0 = (__this->f0);
		(( void (*) (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		t2461 * L_1 = (__this->f0);
		t35 L_2 = { IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		t2470 * L_3 = (t2470 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (t2470 * __this, t29 * p0, t35 p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_3, NULL, L_2, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		(( void (*) (t2461 * __this, t20 * p0, int32_t p1, t2470 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_1, p0, p1, L_3, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
extern MethodInfo m12935_MI;
 t29 * m12935_gshared (t2464 * __this, MethodInfo* method)
{
	{
		t2469  L_0 = (( t2469  (*) (t2464 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		t2469  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12936_MI;
 bool m12936_gshared (t2464 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
extern MethodInfo m12937_MI;
 bool m12937_gshared (t2464 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m12938_MI;
 t29 * m12938_gshared (t2464 * __this, MethodInfo* method)
{
	{
		t2461 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m12939_gshared (t2464 * __this, t316* p0, int32_t p1, MethodInfo* method)
{
	{
		t2461 * L_0 = (__this->f0);
		(( void (*) (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, (t20 *)(t20 *)p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		t2461 * L_1 = (__this->f0);
		t35 L_2 = { IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		t2470 * L_3 = (t2470 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (t2470 * __this, t29 * p0, t35 p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_3, NULL, L_2, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		(( void (*) (t2461 * __this, t316* p0, int32_t p1, t2470 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_1, p0, p1, L_3, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
 t2469  m12940 (t2464 * __this, MethodInfo* method){
	{
		t2461 * L_0 = (__this->f0);
		t2469  L_1 = {0};
		m12942(&L_1, L_0, &m12942_MI);
		return L_1;
	}
}
extern MethodInfo m12941_MI;
 int32_t m12941_gshared (t2464 * __this, MethodInfo* method)
{
	{
		t2461 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
extern Il2CppType t2461_0_0_1;
FieldInfo t2464_f0_FieldInfo = 
{
	"dictionary", &t2461_0_0_1, &t2464_TI, offsetof(t2464, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2464_FIs[] =
{
	&t2464_f0_FieldInfo,
	NULL
};
static PropertyInfo t2464____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2464_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m12936_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2464____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2464_TI, "System.Collections.ICollection.IsSynchronized", &m12937_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2464____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2464_TI, "System.Collections.ICollection.SyncRoot", &m12938_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2464____Count_PropertyInfo = 
{
	&t2464_TI, "Count", &m12941_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2464_PIs[] =
{
	&t2464____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2464____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2464____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2464____Count_PropertyInfo,
	NULL
};
extern Il2CppType t2461_0_0_0;
static ParameterInfo t2464_m12928_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t2461_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12928_GM;
MethodInfo m12928_MI = 
{
	".ctor", (methodPointerType)&m12928_gshared, &t2464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2464_m12928_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12928_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2464_m12929_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12929_GM;
MethodInfo m12929_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12929_gshared, &t2464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2464_m12929_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12929_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12930_GM;
MethodInfo m12930_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12930_gshared, &t2464_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12930_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2464_m12931_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12931_GM;
MethodInfo m12931_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12931_gshared, &t2464_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2464_m12931_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12931_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2464_m12932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12932_GM;
MethodInfo m12932_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12932_gshared, &t2464_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2464_m12932_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12932_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12933_GM;
MethodInfo m12933_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12933_gshared, &t2464_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12933_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2464_m12934_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12934_GM;
MethodInfo m12934_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12934_gshared, &t2464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2464_m12934_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12934_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12935_GM;
MethodInfo m12935_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12935_gshared, &t2464_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12935_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12936_GM;
MethodInfo m12936_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12936_gshared, &t2464_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12936_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12937_GM;
MethodInfo m12937_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12937_gshared, &t2464_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12937_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12938_GM;
MethodInfo m12938_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12938_gshared, &t2464_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12938_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2464_m12939_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12939_GM;
MethodInfo m12939_MI = 
{
	"CopyTo", (methodPointerType)&m12939_gshared, &t2464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2464_m12939_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12939_GM};
extern Il2CppType t2469_0_0_0;
extern void* RuntimeInvoker_t2469 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12940_GM;
MethodInfo m12940_MI = 
{
	"GetEnumerator", (methodPointerType)&m12940, &t2464_TI, &t2469_0_0_0, RuntimeInvoker_t2469, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12940_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12941_GM;
MethodInfo m12941_MI = 
{
	"get_Count", (methodPointerType)&m12941_gshared, &t2464_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12941_GM};
static MethodInfo* t2464_MIs[] =
{
	&m12928_MI,
	&m12929_MI,
	&m12930_MI,
	&m12931_MI,
	&m12932_MI,
	&m12933_MI,
	&m12934_MI,
	&m12935_MI,
	&m12936_MI,
	&m12937_MI,
	&m12938_MI,
	&m12939_MI,
	&m12940_MI,
	&m12941_MI,
	NULL
};
static MethodInfo* t2464_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12935_MI,
	&m12941_MI,
	&m12937_MI,
	&m12938_MI,
	&m12934_MI,
	&m12941_MI,
	&m12936_MI,
	&m12929_MI,
	&m12930_MI,
	&m12931_MI,
	&m12939_MI,
	&m12932_MI,
	&m12933_MI,
};
static TypeInfo* t2464_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t2182_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2464_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t2182_TI, 9},
	{ &t2183_TI, 16},
};
extern TypeInfo t2469_TI;
extern TypeInfo t316_TI;
extern TypeInfo t2470_TI;
static Il2CppRGCTXData t2464_RGCTXData[13] = 
{
	&m12893_MI/* Method Usage */,
	&m12940_MI/* Method Usage */,
	&t2469_TI/* Class Usage */,
	&t316_TI/* Class Usage */,
	&m12939_MI/* Method Usage */,
	&m12879_MI/* Method Usage */,
	&m12882_MI/* Method Usage */,
	&t2470_TI/* Class Usage */,
	&m12959_MI/* Method Usage */,
	&m20791_MI/* Method Usage */,
	&m20790_MI/* Method Usage */,
	&m12942_MI/* Method Usage */,
	&m12869_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2464_0_0_0;
extern Il2CppType t2464_1_0_0;
struct t2464;
extern Il2CppGenericClass t2464_GC;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2464_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2464_MIs, t2464_PIs, t2464_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2464_TI, t2464_ITIs, t2464_VT, &t1252__CustomAttributeCache, &t2464_TI, &t2464_0_0_0, &t2464_1_0_0, t2464_IOs, &t2464_GC, NULL, NULL, NULL, t2464_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2464), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12955_MI;
extern MethodInfo m12958_MI;
extern MethodInfo m12952_MI;


 void m12942_gshared (t2469 * __this, t2461 * p0, MethodInfo* method)
{
	{
		t2467  L_0 = (( t2467  (*) (t2461 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12943_MI;
 t29 * m12943_gshared (t2469 * __this, MethodInfo* method)
{
	{
		t2467 * L_0 = &(__this->f0);
		t29 * L_1 = (( t29 * (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		t29 * L_2 = L_1;
		return ((t29 *)L_2);
	}
}
extern MethodInfo m12944_MI;
 void m12944_gshared (t2469 * __this, MethodInfo* method)
{
	{
		t2467 * L_0 = &(__this->f0);
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern MethodInfo m12945_MI;
 bool m12945_gshared (t2469 * __this, MethodInfo* method)
{
	{
		t2467 * L_0 = &(__this->f0);
		bool L_1 = (( bool (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
extern MethodInfo m12946_MI;
 t29 * m12946_gshared (t2469 * __this, MethodInfo* method)
{
	{
		t2467 * L_0 = &(__this->f0);
		t2465 * L_1 = &(L_0->f3);
		t29 * L_2 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
extern Il2CppType t2467_0_0_1;
FieldInfo t2469_f0_FieldInfo = 
{
	"host_enumerator", &t2467_0_0_1, &t2469_TI, offsetof(t2469, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2469_FIs[] =
{
	&t2469_f0_FieldInfo,
	NULL
};
static PropertyInfo t2469____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2469_TI, "System.Collections.IEnumerator.Current", &m12943_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2469____Current_PropertyInfo = 
{
	&t2469_TI, "Current", &m12946_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2469_PIs[] =
{
	&t2469____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2469____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2461_0_0_0;
static ParameterInfo t2469_m12942_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t2461_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12942_GM;
MethodInfo m12942_MI = 
{
	".ctor", (methodPointerType)&m12942_gshared, &t2469_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2469_m12942_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12942_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12943_GM;
MethodInfo m12943_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12943_gshared, &t2469_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12943_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12944_GM;
MethodInfo m12944_MI = 
{
	"Dispose", (methodPointerType)&m12944_gshared, &t2469_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12944_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12945_GM;
MethodInfo m12945_MI = 
{
	"MoveNext", (methodPointerType)&m12945_gshared, &t2469_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12945_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12946_GM;
MethodInfo m12946_MI = 
{
	"get_Current", (methodPointerType)&m12946_gshared, &t2469_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12946_GM};
static MethodInfo* t2469_MIs[] =
{
	&m12942_MI,
	&m12943_MI,
	&m12944_MI,
	&m12945_MI,
	&m12946_MI,
	NULL
};
static MethodInfo* t2469_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12943_MI,
	&m12945_MI,
	&m12944_MI,
	&m12946_MI,
};
static TypeInfo* t2469_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t2469_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t346_TI, 7},
};
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2469_RGCTXData[6] = 
{
	&m12916_MI/* Method Usage */,
	&m12955_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12958_MI/* Method Usage */,
	&m12952_MI/* Method Usage */,
	&m12920_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2469_0_0_0;
extern Il2CppType t2469_1_0_0;
extern Il2CppGenericClass t2469_GC;
TypeInfo t2469_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2469_MIs, t2469_PIs, t2469_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2469_TI, t2469_ITIs, t2469_VT, &EmptyCustomAttributesCache, &t2469_TI, &t2469_0_0_0, &t2469_1_0_0, t2469_IOs, &t2469_GC, NULL, NULL, NULL, t2469_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2469)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12957_MI;
extern MethodInfo m12954_MI;
extern MethodInfo m12956_MI;


 void m12947_gshared (t2467 * __this, t2461 * p0, MethodInfo* method)
{
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m12948_MI;
 t29 * m12948_gshared (t2467 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t2465  L_0 = (__this->f3);
		t2465  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern MethodInfo m12949_MI;
 t725  m12949_gshared (t2467 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t2465 * L_0 = &(__this->f3);
		t29 * L_1 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		t29 * L_2 = L_1;
		t2465 * L_3 = &(__this->f3);
		t29 * L_4 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		t29 * L_5 = L_4;
		t725  L_6 = {0};
		m3965(&L_6, ((t29 *)L_2), ((t29 *)L_5), &m3965_MI);
		return L_6;
	}
}
extern MethodInfo m12950_MI;
 t29 * m12950_gshared (t2467 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m12951_MI;
 t29 * m12951_gshared (t2467 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
 bool m12952_gshared (t2467 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t2461 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t2461 * L_6 = (__this->f0);
		t316* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t2461 * L_9 = (__this->f0);
		t316* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t2465  L_12 = {0};
		(( void (*) (t2465 * __this, t29 * p0, t29 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(&L_12, (*(t29 **)(t29 **)SZArrayLdElema(L_7, L_8)), (*(t29 **)(t29 **)SZArrayLdElema(L_10, L_11)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t2461 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m12953_MI;
 t2465  m12953 (t2467 * __this, MethodInfo* method){
	{
		t2465  L_0 = (__this->f3);
		return L_0;
	}
}
 t29 * m12954_gshared (t2467 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t2465 * L_0 = &(__this->f3);
		t29 * L_1 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
 t29 * m12955_gshared (t2467 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t2465 * L_0 = &(__this->f3);
		t29 * L_1 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
 void m12956_gshared (t2467 * __this, MethodInfo* method)
{
	{
		t2461 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t2461 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m12957_gshared (t2467 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m12958_gshared (t2467 * __this, MethodInfo* method)
{
	{
		__this->f0 = (t2461 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
extern Il2CppType t2461_0_0_1;
FieldInfo t2467_f0_FieldInfo = 
{
	"dictionary", &t2461_0_0_1, &t2467_TI, offsetof(t2467, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2467_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2467_TI, offsetof(t2467, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2467_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2467_TI, offsetof(t2467, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2465_0_0_3;
FieldInfo t2467_f3_FieldInfo = 
{
	"current", &t2465_0_0_3, &t2467_TI, offsetof(t2467, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2467_FIs[] =
{
	&t2467_f0_FieldInfo,
	&t2467_f1_FieldInfo,
	&t2467_f2_FieldInfo,
	&t2467_f3_FieldInfo,
	NULL
};
static PropertyInfo t2467____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2467_TI, "System.Collections.IEnumerator.Current", &m12948_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2467____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2467_TI, "System.Collections.IDictionaryEnumerator.Entry", &m12949_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2467____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2467_TI, "System.Collections.IDictionaryEnumerator.Key", &m12950_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2467____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2467_TI, "System.Collections.IDictionaryEnumerator.Value", &m12951_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2467____Current_PropertyInfo = 
{
	&t2467_TI, "Current", &m12953_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2467____CurrentKey_PropertyInfo = 
{
	&t2467_TI, "CurrentKey", &m12954_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2467____CurrentValue_PropertyInfo = 
{
	&t2467_TI, "CurrentValue", &m12955_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2467_PIs[] =
{
	&t2467____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2467____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2467____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2467____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2467____Current_PropertyInfo,
	&t2467____CurrentKey_PropertyInfo,
	&t2467____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t2461_0_0_0;
static ParameterInfo t2467_m12947_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t2461_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12947_GM;
MethodInfo m12947_MI = 
{
	".ctor", (methodPointerType)&m12947_gshared, &t2467_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2467_m12947_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12947_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12948_GM;
MethodInfo m12948_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12948_gshared, &t2467_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12948_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12949_GM;
MethodInfo m12949_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12949_gshared, &t2467_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12949_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12950_GM;
MethodInfo m12950_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12950_gshared, &t2467_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12950_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12951_GM;
MethodInfo m12951_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12951_gshared, &t2467_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12951_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12952_GM;
MethodInfo m12952_MI = 
{
	"MoveNext", (methodPointerType)&m12952_gshared, &t2467_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12952_GM};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12953_GM;
MethodInfo m12953_MI = 
{
	"get_Current", (methodPointerType)&m12953, &t2467_TI, &t2465_0_0_0, RuntimeInvoker_t2465, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12953_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12954_GM;
MethodInfo m12954_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12954_gshared, &t2467_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12954_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12955_GM;
MethodInfo m12955_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12955_gshared, &t2467_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12955_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12956_GM;
MethodInfo m12956_MI = 
{
	"VerifyState", (methodPointerType)&m12956_gshared, &t2467_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12956_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12957_GM;
MethodInfo m12957_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12957_gshared, &t2467_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12957_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12958_GM;
MethodInfo m12958_MI = 
{
	"Dispose", (methodPointerType)&m12958_gshared, &t2467_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12958_GM};
static MethodInfo* t2467_MIs[] =
{
	&m12947_MI,
	&m12948_MI,
	&m12949_MI,
	&m12950_MI,
	&m12951_MI,
	&m12952_MI,
	&m12953_MI,
	&m12954_MI,
	&m12955_MI,
	&m12956_MI,
	&m12957_MI,
	&m12958_MI,
	NULL
};
static MethodInfo* t2467_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12948_MI,
	&m12952_MI,
	&m12958_MI,
	&m12953_MI,
	&m12949_MI,
	&m12950_MI,
	&m12951_MI,
};
static TypeInfo* t2467_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2466_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2467_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2466_TI, 7},
	{ &t722_TI, 8},
};
extern TypeInfo t2465_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2467_RGCTXData[10] = 
{
	&m12957_MI/* Method Usage */,
	&t2465_TI/* Class Usage */,
	&m12918_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12920_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12954_MI/* Method Usage */,
	&m12955_MI/* Method Usage */,
	&m12956_MI/* Method Usage */,
	&m12917_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2467_0_0_0;
extern Il2CppType t2467_1_0_0;
extern Il2CppGenericClass t2467_GC;
TypeInfo t2467_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2467_MIs, t2467_PIs, t2467_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2467_TI, t2467_ITIs, t2467_VT, &EmptyCustomAttributesCache, &t2467_TI, &t2467_0_0_0, &t2467_1_0_0, t2467_IOs, &t2467_GC, NULL, NULL, NULL, t2467_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2467)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m12959_gshared (t2470 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12960_MI;
 t29 * m12960_gshared (t2470 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m12960((t2470 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12961_MI;
 t29 * m12961_gshared (t2470 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12962_MI;
 t29 * m12962_gshared (t2470 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2470_m12959_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12959_GM;
MethodInfo m12959_MI = 
{
	".ctor", (methodPointerType)&m12959_gshared, &t2470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2470_m12959_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12959_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2470_m12960_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12960_GM;
MethodInfo m12960_MI = 
{
	"Invoke", (methodPointerType)&m12960_gshared, &t2470_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t2470_m12960_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12960_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2470_m12961_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12961_GM;
MethodInfo m12961_MI = 
{
	"BeginInvoke", (methodPointerType)&m12961_gshared, &t2470_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2470_m12961_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12961_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2470_m12962_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12962_GM;
MethodInfo m12962_MI = 
{
	"EndInvoke", (methodPointerType)&m12962_gshared, &t2470_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2470_m12962_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12962_GM};
static MethodInfo* t2470_MIs[] =
{
	&m12959_MI,
	&m12960_MI,
	&m12961_MI,
	&m12962_MI,
	NULL
};
static MethodInfo* t2470_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12960_MI,
	&m12961_MI,
	&m12962_MI,
};
static Il2CppInterfaceOffsetPair t2470_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2470_0_0_0;
extern Il2CppType t2470_1_0_0;
struct t2470;
extern Il2CppGenericClass t2470_GC;
TypeInfo t2470_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2470_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2470_TI, NULL, t2470_VT, &EmptyCustomAttributesCache, &t2470_TI, &t2470_0_0_0, &t2470_1_0_0, t2470_IOs, &t2470_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2470), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12963 (t2463 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12964_MI;
 t725  m12964 (t2463 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12964((t2463 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12965_MI;
 t29 * m12965 (t2463 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12966_MI;
 t725  m12966 (t2463 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2463_m12963_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12963_GM;
MethodInfo m12963_MI = 
{
	".ctor", (methodPointerType)&m12963, &t2463_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2463_m12963_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12963_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2463_m12964_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12964_GM;
MethodInfo m12964_MI = 
{
	"Invoke", (methodPointerType)&m12964, &t2463_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t2463_m12964_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12964_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2463_m12965_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12965_GM;
MethodInfo m12965_MI = 
{
	"BeginInvoke", (methodPointerType)&m12965, &t2463_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2463_m12965_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12965_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2463_m12966_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12966_GM;
MethodInfo m12966_MI = 
{
	"EndInvoke", (methodPointerType)&m12966, &t2463_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2463_m12966_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12966_GM};
static MethodInfo* t2463_MIs[] =
{
	&m12963_MI,
	&m12964_MI,
	&m12965_MI,
	&m12966_MI,
	NULL
};
static MethodInfo* t2463_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12964_MI,
	&m12965_MI,
	&m12966_MI,
};
static Il2CppInterfaceOffsetPair t2463_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2463_0_0_0;
extern Il2CppType t2463_1_0_0;
struct t2463;
extern Il2CppGenericClass t2463_GC;
TypeInfo t2463_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2463_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2463_TI, NULL, t2463_VT, &EmptyCustomAttributesCache, &t2463_TI, &t2463_0_0_0, &t2463_1_0_0, t2463_IOs, &t2463_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2463), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12967 (t2471 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12968_MI;
 t2465  m12968 (t2471 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12968((t2471 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2465  (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2465  (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2465  (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m12969_MI;
 t29 * m12969 (t2471 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12970_MI;
 t2465  m12970 (t2471 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2465 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2471_m12967_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12967_GM;
MethodInfo m12967_MI = 
{
	".ctor", (methodPointerType)&m12967, &t2471_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2471_m12967_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12967_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2471_m12968_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12968_GM;
MethodInfo m12968_MI = 
{
	"Invoke", (methodPointerType)&m12968, &t2471_TI, &t2465_0_0_0, RuntimeInvoker_t2465_t29_t29, t2471_m12968_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12968_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2471_m12969_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12969_GM;
MethodInfo m12969_MI = 
{
	"BeginInvoke", (methodPointerType)&m12969, &t2471_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2471_m12969_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12969_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2471_m12970_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2465_0_0_0;
extern void* RuntimeInvoker_t2465_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12970_GM;
MethodInfo m12970_MI = 
{
	"EndInvoke", (methodPointerType)&m12970, &t2471_TI, &t2465_0_0_0, RuntimeInvoker_t2465_t29, t2471_m12970_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12970_GM};
static MethodInfo* t2471_MIs[] =
{
	&m12967_MI,
	&m12968_MI,
	&m12969_MI,
	&m12970_MI,
	NULL
};
static MethodInfo* t2471_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12968_MI,
	&m12969_MI,
	&m12970_MI,
};
static Il2CppInterfaceOffsetPair t2471_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2471_0_0_0;
extern Il2CppType t2471_1_0_0;
struct t2471;
extern Il2CppGenericClass t2471_GC;
TypeInfo t2471_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2471_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2471_TI, NULL, t2471_VT, &EmptyCustomAttributesCache, &t2471_TI, &t2471_0_0_0, &t2471_1_0_0, t2471_IOs, &t2471_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2471), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12973_MI;


 void m12971_gshared (t2472 * __this, t2461 * p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		t2467  L_0 = (( t2467  (*) (t2461 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12972_MI;
 bool m12972_gshared (t2472 * __this, MethodInfo* method)
{
	{
		t2467 * L_0 = &(__this->f0);
		bool L_1 = (( bool (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
 t725  m12973_gshared (t2472 * __this, MethodInfo* method)
{
	{
		t2467  L_0 = (__this->f0);
		t2467  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m12974_MI;
 t29 * m12974_gshared (t2472 * __this, MethodInfo* method)
{
	t2465  V_0 = {0};
	{
		t2467 * L_0 = &(__this->f0);
		t2465  L_1 = (( t2465  (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = L_1;
		t29 * L_2 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((&V_0), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		t29 * L_3 = L_2;
		return ((t29 *)L_3);
	}
}
extern MethodInfo m12975_MI;
 t29 * m12975_gshared (t2472 * __this, MethodInfo* method)
{
	t2465  V_0 = {0};
	{
		t2467 * L_0 = &(__this->f0);
		t2465  L_1 = (( t2465  (*) (t2467 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = L_1;
		t29 * L_2 = (( t29 * (*) (t2465 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((&V_0), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		t29 * L_3 = L_2;
		return ((t29 *)L_3);
	}
}
extern MethodInfo m12976_MI;
 t29 * m12976_gshared (t2472 * __this, MethodInfo* method)
{
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
extern Il2CppType t2467_0_0_1;
FieldInfo t2472_f0_FieldInfo = 
{
	"host_enumerator", &t2467_0_0_1, &t2472_TI, offsetof(t2472, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2472_FIs[] =
{
	&t2472_f0_FieldInfo,
	NULL
};
static PropertyInfo t2472____Entry_PropertyInfo = 
{
	&t2472_TI, "Entry", &m12973_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2472____Key_PropertyInfo = 
{
	&t2472_TI, "Key", &m12974_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2472____Value_PropertyInfo = 
{
	&t2472_TI, "Value", &m12975_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2472____Current_PropertyInfo = 
{
	&t2472_TI, "Current", &m12976_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2472_PIs[] =
{
	&t2472____Entry_PropertyInfo,
	&t2472____Key_PropertyInfo,
	&t2472____Value_PropertyInfo,
	&t2472____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2461_0_0_0;
static ParameterInfo t2472_m12971_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t2461_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12971_GM;
MethodInfo m12971_MI = 
{
	".ctor", (methodPointerType)&m12971_gshared, &t2472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2472_m12971_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12971_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12972_GM;
MethodInfo m12972_MI = 
{
	"MoveNext", (methodPointerType)&m12972_gshared, &t2472_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12972_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12973_GM;
MethodInfo m12973_MI = 
{
	"get_Entry", (methodPointerType)&m12973_gshared, &t2472_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12973_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12974_GM;
MethodInfo m12974_MI = 
{
	"get_Key", (methodPointerType)&m12974_gshared, &t2472_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12974_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12975_GM;
MethodInfo m12975_MI = 
{
	"get_Value", (methodPointerType)&m12975_gshared, &t2472_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12975_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12976_GM;
MethodInfo m12976_MI = 
{
	"get_Current", (methodPointerType)&m12976_gshared, &t2472_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12976_GM};
static MethodInfo* t2472_MIs[] =
{
	&m12971_MI,
	&m12972_MI,
	&m12973_MI,
	&m12974_MI,
	&m12975_MI,
	&m12976_MI,
	NULL
};
static MethodInfo* t2472_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12976_MI,
	&m12972_MI,
	&m12973_MI,
	&m12974_MI,
	&m12975_MI,
};
static TypeInfo* t2472_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2472_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern TypeInfo t2467_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2472_RGCTXData[9] = 
{
	&m12916_MI/* Method Usage */,
	&m12952_MI/* Method Usage */,
	&t2467_TI/* Class Usage */,
	&m12953_MI/* Method Usage */,
	&m12918_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12920_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m12973_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2472_0_0_0;
extern Il2CppType t2472_1_0_0;
struct t2472;
extern Il2CppGenericClass t2472_GC;
TypeInfo t2472_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2472_MIs, t2472_PIs, t2472_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2472_TI, t2472_ITIs, t2472_VT, &EmptyCustomAttributesCache, &t2472_TI, &t2472_0_0_0, &t2472_1_0_0, t2472_IOs, &t2472_GC, NULL, NULL, NULL, t2472_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2472), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>>
extern MethodInfo m27687_MI;
static PropertyInfo t5313____Count_PropertyInfo = 
{
	&t5313_TI, "Count", &m27687_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27688_MI;
static PropertyInfo t5313____IsReadOnly_PropertyInfo = 
{
	&t5313_TI, "IsReadOnly", &m27688_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5313_PIs[] =
{
	&t5313____Count_PropertyInfo,
	&t5313____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27687_GM;
MethodInfo m27687_MI = 
{
	"get_Count", NULL, &t5313_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27687_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27688_GM;
MethodInfo m27688_MI = 
{
	"get_IsReadOnly", NULL, &t5313_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27688_GM};
extern Il2CppType t2458_0_0_0;
static ParameterInfo t5313_m27689_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27689_GM;
MethodInfo m27689_MI = 
{
	"Add", NULL, &t5313_TI, &t21_0_0_0, RuntimeInvoker_t21_t2458, t5313_m27689_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27689_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27690_GM;
MethodInfo m27690_MI = 
{
	"Clear", NULL, &t5313_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27690_GM};
extern Il2CppType t2458_0_0_0;
static ParameterInfo t5313_m27691_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27691_GM;
MethodInfo m27691_MI = 
{
	"Contains", NULL, &t5313_TI, &t40_0_0_0, RuntimeInvoker_t40_t2458, t5313_m27691_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27691_GM};
extern Il2CppType t2457_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5313_m27692_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2457_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27692_GM;
MethodInfo m27692_MI = 
{
	"CopyTo", NULL, &t5313_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5313_m27692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27692_GM};
extern Il2CppType t2458_0_0_0;
static ParameterInfo t5313_m27693_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27693_GM;
MethodInfo m27693_MI = 
{
	"Remove", NULL, &t5313_TI, &t40_0_0_0, RuntimeInvoker_t40_t2458, t5313_m27693_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27693_GM};
static MethodInfo* t5313_MIs[] =
{
	&m27687_MI,
	&m27688_MI,
	&m27689_MI,
	&m27690_MI,
	&m27691_MI,
	&m27692_MI,
	&m27693_MI,
	NULL
};
static TypeInfo* t5313_ITIs[] = 
{
	&t603_TI,
	&t5315_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5313_0_0_0;
extern Il2CppType t5313_1_0_0;
struct t5313;
extern Il2CppGenericClass t5313_GC;
TypeInfo t5313_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5313_MIs, t5313_PIs, NULL, NULL, NULL, NULL, NULL, &t5313_TI, t5313_ITIs, NULL, &EmptyCustomAttributesCache, &t5313_TI, &t5313_0_0_0, &t5313_1_0_0, NULL, &t5313_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>>
extern Il2CppType t2459_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27694_GM;
MethodInfo m27694_MI = 
{
	"GetEnumerator", NULL, &t5315_TI, &t2459_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27694_GM};
static MethodInfo* t5315_MIs[] =
{
	&m27694_MI,
	NULL
};
static TypeInfo* t5315_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5315_0_0_0;
extern Il2CppType t5315_1_0_0;
struct t5315;
extern Il2CppGenericClass t5315_GC;
TypeInfo t5315_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5315_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5315_TI, t5315_ITIs, NULL, &EmptyCustomAttributesCache, &t5315_TI, &t5315_0_0_0, &t5315_1_0_0, NULL, &t5315_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2459_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>>
extern MethodInfo m27695_MI;
static PropertyInfo t2459____Current_PropertyInfo = 
{
	&t2459_TI, "Current", &m27695_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2459_PIs[] =
{
	&t2459____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27695_GM;
MethodInfo m27695_MI = 
{
	"get_Current", NULL, &t2459_TI, &t2458_0_0_0, RuntimeInvoker_t2458, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27695_GM};
static MethodInfo* t2459_MIs[] =
{
	&m27695_MI,
	NULL
};
static TypeInfo* t2459_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2459_0_0_0;
extern Il2CppType t2459_1_0_0;
struct t2459;
extern Il2CppGenericClass t2459_GC;
TypeInfo t2459_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2459_MIs, t2459_PIs, NULL, NULL, NULL, NULL, NULL, &t2459_TI, t2459_ITIs, NULL, &EmptyCustomAttributesCache, &t2459_TI, &t2459_0_0_0, &t2459_1_0_0, NULL, &t2459_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12979_MI;
extern MethodInfo m12981_MI;


// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t148_0_0_1;
FieldInfo t2458_f0_FieldInfo = 
{
	"key", &t148_0_0_1, &t2458_TI, offsetof(t2458, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t350_0_0_1;
FieldInfo t2458_f1_FieldInfo = 
{
	"value", &t350_0_0_1, &t2458_TI, offsetof(t2458, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2458_FIs[] =
{
	&t2458_f0_FieldInfo,
	&t2458_f1_FieldInfo,
	NULL
};
static PropertyInfo t2458____Key_PropertyInfo = 
{
	&t2458_TI, "Key", &m12978_MI, &m12979_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2458____Value_PropertyInfo = 
{
	&t2458_TI, "Value", &m12980_MI, &m12981_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2458_PIs[] =
{
	&t2458____Key_PropertyInfo,
	&t2458____Value_PropertyInfo,
	NULL
};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2458_m12977_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12977_GM;
MethodInfo m12977_MI = 
{
	".ctor", (methodPointerType)&m12917_gshared, &t2458_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2458_m12977_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12977_GM};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12978_GM;
MethodInfo m12978_MI = 
{
	"get_Key", (methodPointerType)&m12918_gshared, &t2458_TI, &t148_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12978_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t2458_m12979_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12979_GM;
MethodInfo m12979_MI = 
{
	"set_Key", (methodPointerType)&m12919_gshared, &t2458_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2458_m12979_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12979_GM};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12980_GM;
MethodInfo m12980_MI = 
{
	"get_Value", (methodPointerType)&m12920_gshared, &t2458_TI, &t350_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12980_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t2458_m12981_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12981_GM;
MethodInfo m12981_MI = 
{
	"set_Value", (methodPointerType)&m12921_gshared, &t2458_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2458_m12981_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12981_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12982_GM;
MethodInfo m12982_MI = 
{
	"ToString", (methodPointerType)&m12922_gshared, &t2458_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12982_GM};
static MethodInfo* t2458_MIs[] =
{
	&m12977_MI,
	&m12978_MI,
	&m12979_MI,
	&m12980_MI,
	&m12981_MI,
	&m12982_MI,
	NULL
};
extern MethodInfo m12982_MI;
static MethodInfo* t2458_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m12982_MI,
};
extern TypeInfo t148_TI;
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2458_RGCTXData[6] = 
{
	&m12979_MI/* Method Usage */,
	&m12981_MI/* Method Usage */,
	&m12978_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m12980_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2458_1_0_0;
extern Il2CppGenericClass t2458_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2458_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2458_MIs, t2458_PIs, t2458_FIs, NULL, &t110_TI, NULL, NULL, &t2458_TI, NULL, t2458_VT, &t1259__CustomAttributeCache, &t2458_TI, &t2458_0_0_0, &t2458_1_0_0, NULL, &t2458_GC, NULL, NULL, NULL, t2458_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2458)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
#include "t350MD.h"

#include "t15.h"
#include "t2480.h"
#include "t2477.h"
#include "t2478.h"
#include "t2490.h"
#include "t2479.h"
extern TypeInfo t15_TI;
extern TypeInfo t2473_TI;
extern TypeInfo t2480_TI;
extern TypeInfo t2475_TI;
extern TypeInfo t2476_TI;
extern TypeInfo t2474_TI;
extern TypeInfo t2477_TI;
extern TypeInfo t2478_TI;
extern TypeInfo t2490_TI;
#include "t2477MD.h"
#include "t2478MD.h"
#include "t2480MD.h"
#include "t2490MD.h"
extern MethodInfo m1547_MI;
extern MethodInfo m13026_MI;
extern MethodInfo m20852_MI;
extern MethodInfo m13014_MI;
extern MethodInfo m13011_MI;
extern MethodInfo m1549_MI;
extern MethodInfo m13006_MI;
extern MethodInfo m13012_MI;
extern MethodInfo m13015_MI;
extern MethodInfo m1550_MI;
extern MethodInfo m13000_MI;
extern MethodInfo m13024_MI;
extern MethodInfo m13025_MI;
extern MethodInfo m27696_MI;
extern MethodInfo m27697_MI;
extern MethodInfo m27698_MI;
extern MethodInfo m27699_MI;
extern MethodInfo m13016_MI;
extern MethodInfo m13001_MI;
extern MethodInfo m13002_MI;
extern MethodInfo m13058_MI;
extern MethodInfo m20854_MI;
extern MethodInfo m13009_MI;
extern MethodInfo m13010_MI;
extern MethodInfo m13133_MI;
extern MethodInfo m13052_MI;
extern MethodInfo m13013_MI;
extern MethodInfo m13018_MI;
extern MethodInfo m13139_MI;
extern MethodInfo m20856_MI;
extern MethodInfo m20864_MI;
struct t20;
#define m20852(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2488.h"
#define m20854(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
#define m20856(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#define m20864(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2480  m13011 (t350 * __this, MethodInfo* method){
	{
		t2480  L_0 = {0};
		m13052(&L_0, __this, &m13052_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.Text>
extern Il2CppType t44_0_0_32849;
FieldInfo t350_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t350_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2473_0_0_1;
FieldInfo t350_f1_FieldInfo = 
{
	"_items", &t2473_0_0_1, &t350_TI, offsetof(t350, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t350_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t350_TI, offsetof(t350, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t350_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t350_TI, offsetof(t350, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2473_0_0_49;
FieldInfo t350_f4_FieldInfo = 
{
	"EmptyArray", &t2473_0_0_49, &t350_TI, offsetof(t350_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t350_FIs[] =
{
	&t350_f0_FieldInfo,
	&t350_f1_FieldInfo,
	&t350_f2_FieldInfo,
	&t350_f3_FieldInfo,
	&t350_f4_FieldInfo,
	NULL
};
static const int32_t t350_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t350_f0_DefaultValue = 
{
	&t350_f0_FieldInfo, { (char*)&t350_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t350_FDVs[] = 
{
	&t350_f0_DefaultValue,
	NULL
};
extern MethodInfo m12993_MI;
static PropertyInfo t350____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t350_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12993_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12994_MI;
static PropertyInfo t350____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t350_TI, "System.Collections.ICollection.IsSynchronized", &m12994_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12995_MI;
static PropertyInfo t350____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t350_TI, "System.Collections.ICollection.SyncRoot", &m12995_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12996_MI;
static PropertyInfo t350____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t350_TI, "System.Collections.IList.IsFixedSize", &m12996_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12997_MI;
static PropertyInfo t350____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t350_TI, "System.Collections.IList.IsReadOnly", &m12997_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12998_MI;
extern MethodInfo m12999_MI;
static PropertyInfo t350____System_Collections_IList_Item_PropertyInfo = 
{
	&t350_TI, "System.Collections.IList.Item", &m12998_MI, &m12999_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t350____Capacity_PropertyInfo = 
{
	&t350_TI, "Capacity", &m13024_MI, &m13025_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1548_MI;
static PropertyInfo t350____Count_PropertyInfo = 
{
	&t350_TI, "Count", &m1548_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t350____Item_PropertyInfo = 
{
	&t350_TI, "Item", &m1547_MI, &m13026_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t350_PIs[] =
{
	&t350____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t350____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t350____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t350____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t350____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t350____System_Collections_IList_Item_PropertyInfo,
	&t350____Capacity_PropertyInfo,
	&t350____Count_PropertyInfo,
	&t350____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1543_GM;
MethodInfo m1543_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1543_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m12983_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12983_GM;
MethodInfo m12983_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t350_m12983_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12983_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12984_GM;
MethodInfo m12984_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12984_GM};
extern Il2CppType t2474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12985_GM;
MethodInfo m12985_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t350_TI, &t2474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12985_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m12986_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12986_GM;
MethodInfo m12986_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t350_m12986_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12986_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12987_GM;
MethodInfo m12987_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t350_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12987_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t350_m12988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12988_GM;
MethodInfo m12988_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t350_m12988_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12988_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t350_m12989_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12989_GM;
MethodInfo m12989_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t350_m12989_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12989_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t350_m12990_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12990_GM;
MethodInfo m12990_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t350_m12990_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12990_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t350_m12991_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12991_GM;
MethodInfo m12991_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t350_m12991_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12991_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t350_m12992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12992_GM;
MethodInfo m12992_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m12992_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12992_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12993_GM;
MethodInfo m12993_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12993_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12994_GM;
MethodInfo m12994_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12994_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12995_GM;
MethodInfo m12995_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t350_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12995_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12996_GM;
MethodInfo m12996_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12996_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12997_GM;
MethodInfo m12997_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12997_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m12998_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12998_GM;
MethodInfo m12998_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t350_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t350_m12998_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12998_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t350_m12999_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12999_GM;
MethodInfo m12999_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t350_m12999_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12999_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t350_m1549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1549_GM;
MethodInfo m1549_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m1549_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1549_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m13000_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13000_GM;
MethodInfo m13000_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t350_m13000_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13000_GM};
extern Il2CppType t2475_0_0_0;
extern Il2CppType t2475_0_0_0;
static ParameterInfo t350_m13001_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2475_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13001_GM;
MethodInfo m13001_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m13001_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13001_GM};
extern Il2CppType t2476_0_0_0;
extern Il2CppType t2476_0_0_0;
static ParameterInfo t350_m13002_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2476_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13002_GM;
MethodInfo m13002_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m13002_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13002_GM};
extern Il2CppType t2476_0_0_0;
static ParameterInfo t350_m13003_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2476_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13003_GM;
MethodInfo m13003_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m13003_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13003_GM};
extern Il2CppType t2477_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13004_GM;
MethodInfo m13004_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t350_TI, &t2477_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13004_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13005_GM;
MethodInfo m13005_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13005_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t350_m13006_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13006_GM;
MethodInfo m13006_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t350_m13006_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13006_GM};
extern Il2CppType t2473_0_0_0;
extern Il2CppType t2473_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m13007_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2473_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13007_GM;
MethodInfo m13007_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t350_m13007_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13007_GM};
extern Il2CppType t2478_0_0_0;
extern Il2CppType t2478_0_0_0;
static ParameterInfo t350_m13008_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2478_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13008_GM;
MethodInfo m13008_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t350_TI, &t15_0_0_0, RuntimeInvoker_t29_t29, t350_m13008_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13008_GM};
extern Il2CppType t2478_0_0_0;
static ParameterInfo t350_m13009_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2478_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13009_GM;
MethodInfo m13009_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m13009_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13009_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2478_0_0_0;
static ParameterInfo t350_m13010_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2478_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13010_GM;
MethodInfo m13010_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t350_m13010_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13010_GM};
extern Il2CppType t2480_0_0_0;
extern void* RuntimeInvoker_t2480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13011_GM;
MethodInfo m13011_MI = 
{
	"GetEnumerator", (methodPointerType)&m13011, &t350_TI, &t2480_0_0_0, RuntimeInvoker_t2480, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13011_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t350_m13012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13012_GM;
MethodInfo m13012_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t350_m13012_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13012_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m13013_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13013_GM;
MethodInfo m13013_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t350_m13013_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13013_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m13014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13014_GM;
MethodInfo m13014_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t350_m13014_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13014_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t350_m13015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13015_GM;
MethodInfo m13015_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t350_m13015_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13015_GM};
extern Il2CppType t2476_0_0_0;
static ParameterInfo t350_m13016_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2476_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13016_GM;
MethodInfo m13016_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m13016_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13016_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t350_m1550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1550_GM;
MethodInfo m1550_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t350_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t350_m1550_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1550_GM};
extern Il2CppType t2478_0_0_0;
static ParameterInfo t350_m13017_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2478_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13017_GM;
MethodInfo m13017_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t350_m13017_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13017_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m13018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13018_GM;
MethodInfo m13018_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t350_m13018_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13018_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13019_GM;
MethodInfo m13019_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13019_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13020_GM;
MethodInfo m13020_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13020_GM};
extern Il2CppType t2479_0_0_0;
extern Il2CppType t2479_0_0_0;
static ParameterInfo t350_m13021_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2479_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13021_GM;
MethodInfo m13021_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t350_m13021_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13021_GM};
extern Il2CppType t2473_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13022_GM;
MethodInfo m13022_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t350_TI, &t2473_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13022_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13023_GM;
MethodInfo m13023_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13023_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13024_GM;
MethodInfo m13024_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13024_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m13025_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13025_GM;
MethodInfo m13025_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t350_m13025_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13025_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1548_GM;
MethodInfo m1548_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t350_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1548_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t350_m1547_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1547_GM;
MethodInfo m1547_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t350_TI, &t15_0_0_0, RuntimeInvoker_t29_t44, t350_m1547_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1547_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t350_m13026_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13026_GM;
MethodInfo m13026_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t350_m13026_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13026_GM};
static MethodInfo* t350_MIs[] =
{
	&m1543_MI,
	&m12983_MI,
	&m12984_MI,
	&m12985_MI,
	&m12986_MI,
	&m12987_MI,
	&m12988_MI,
	&m12989_MI,
	&m12990_MI,
	&m12991_MI,
	&m12992_MI,
	&m12993_MI,
	&m12994_MI,
	&m12995_MI,
	&m12996_MI,
	&m12997_MI,
	&m12998_MI,
	&m12999_MI,
	&m1549_MI,
	&m13000_MI,
	&m13001_MI,
	&m13002_MI,
	&m13003_MI,
	&m13004_MI,
	&m13005_MI,
	&m13006_MI,
	&m13007_MI,
	&m13008_MI,
	&m13009_MI,
	&m13010_MI,
	&m13011_MI,
	&m13012_MI,
	&m13013_MI,
	&m13014_MI,
	&m13015_MI,
	&m13016_MI,
	&m1550_MI,
	&m13017_MI,
	&m13018_MI,
	&m13019_MI,
	&m13020_MI,
	&m13021_MI,
	&m13022_MI,
	&m13023_MI,
	&m13024_MI,
	&m13025_MI,
	&m1548_MI,
	&m1547_MI,
	&m13026_MI,
	NULL
};
extern MethodInfo m12987_MI;
extern MethodInfo m12986_MI;
extern MethodInfo m12988_MI;
extern MethodInfo m13005_MI;
extern MethodInfo m12989_MI;
extern MethodInfo m12990_MI;
extern MethodInfo m12991_MI;
extern MethodInfo m12992_MI;
extern MethodInfo m13007_MI;
extern MethodInfo m12985_MI;
static MethodInfo* t350_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12987_MI,
	&m1548_MI,
	&m12994_MI,
	&m12995_MI,
	&m12986_MI,
	&m12996_MI,
	&m12997_MI,
	&m12998_MI,
	&m12999_MI,
	&m12988_MI,
	&m13005_MI,
	&m12989_MI,
	&m12990_MI,
	&m12991_MI,
	&m12992_MI,
	&m13018_MI,
	&m1548_MI,
	&m12993_MI,
	&m1549_MI,
	&m13005_MI,
	&m13006_MI,
	&m13007_MI,
	&m1550_MI,
	&m12985_MI,
	&m13012_MI,
	&m13015_MI,
	&m13018_MI,
	&m1547_MI,
	&m13026_MI,
};
extern TypeInfo t2486_TI;
static TypeInfo* t350_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2475_TI,
	&t2476_TI,
	&t2486_TI,
};
static Il2CppInterfaceOffsetPair t350_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2475_TI, 20},
	{ &t2476_TI, 27},
	{ &t2486_TI, 28},
};
extern TypeInfo t350_TI;
extern TypeInfo t2473_TI;
extern TypeInfo t2480_TI;
extern TypeInfo t15_TI;
extern TypeInfo t2475_TI;
extern TypeInfo t2477_TI;
static Il2CppRGCTXData t350_RGCTXData[37] = 
{
	&t350_TI/* Static Usage */,
	&t2473_TI/* Array Usage */,
	&m13011_MI/* Method Usage */,
	&t2480_TI/* Class Usage */,
	&t15_TI/* Class Usage */,
	&m1549_MI/* Method Usage */,
	&m13006_MI/* Method Usage */,
	&m13012_MI/* Method Usage */,
	&m13014_MI/* Method Usage */,
	&m13015_MI/* Method Usage */,
	&m1550_MI/* Method Usage */,
	&m1547_MI/* Method Usage */,
	&m13026_MI/* Method Usage */,
	&m13000_MI/* Method Usage */,
	&m13024_MI/* Method Usage */,
	&m13025_MI/* Method Usage */,
	&m27696_MI/* Method Usage */,
	&m27697_MI/* Method Usage */,
	&m27698_MI/* Method Usage */,
	&m27699_MI/* Method Usage */,
	&m13016_MI/* Method Usage */,
	&t2475_TI/* Class Usage */,
	&m13001_MI/* Method Usage */,
	&m13002_MI/* Method Usage */,
	&t2477_TI/* Class Usage */,
	&m13058_MI/* Method Usage */,
	&m20854_MI/* Method Usage */,
	&m13009_MI/* Method Usage */,
	&m13010_MI/* Method Usage */,
	&m13133_MI/* Method Usage */,
	&m13052_MI/* Method Usage */,
	&m13013_MI/* Method Usage */,
	&m13018_MI/* Method Usage */,
	&m13139_MI/* Method Usage */,
	&m20856_MI/* Method Usage */,
	&m20864_MI/* Method Usage */,
	&m20852_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
struct t350;
extern Il2CppGenericClass t350_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t350_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t350_MIs, t350_PIs, t350_FIs, NULL, &t29_TI, NULL, NULL, &t350_TI, t350_ITIs, t350_VT, &t1261__CustomAttributeCache, &t350_TI, &t350_0_0_0, &t350_1_0_0, t350_IOs, &t350_GC, NULL, t350_FDVs, NULL, t350_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t350), 0, -1, sizeof(t350_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Text>
static PropertyInfo t2475____Count_PropertyInfo = 
{
	&t2475_TI, "Count", &m27696_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27700_MI;
static PropertyInfo t2475____IsReadOnly_PropertyInfo = 
{
	&t2475_TI, "IsReadOnly", &m27700_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2475_PIs[] =
{
	&t2475____Count_PropertyInfo,
	&t2475____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27696_GM;
MethodInfo m27696_MI = 
{
	"get_Count", NULL, &t2475_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27696_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27700_GM;
MethodInfo m27700_MI = 
{
	"get_IsReadOnly", NULL, &t2475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27700_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2475_m27701_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27701_GM;
MethodInfo m27701_MI = 
{
	"Add", NULL, &t2475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2475_m27701_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27701_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27702_GM;
MethodInfo m27702_MI = 
{
	"Clear", NULL, &t2475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27702_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2475_m27703_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27703_GM;
MethodInfo m27703_MI = 
{
	"Contains", NULL, &t2475_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2475_m27703_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27703_GM};
extern Il2CppType t2473_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2475_m27697_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2473_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27697_GM;
MethodInfo m27697_MI = 
{
	"CopyTo", NULL, &t2475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2475_m27697_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27697_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2475_m27704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27704_GM;
MethodInfo m27704_MI = 
{
	"Remove", NULL, &t2475_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2475_m27704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27704_GM};
static MethodInfo* t2475_MIs[] =
{
	&m27696_MI,
	&m27700_MI,
	&m27701_MI,
	&m27702_MI,
	&m27703_MI,
	&m27697_MI,
	&m27704_MI,
	NULL
};
static TypeInfo* t2475_ITIs[] = 
{
	&t603_TI,
	&t2476_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2475_1_0_0;
struct t2475;
extern Il2CppGenericClass t2475_GC;
TypeInfo t2475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2475_MIs, t2475_PIs, NULL, NULL, NULL, NULL, NULL, &t2475_TI, t2475_ITIs, NULL, &EmptyCustomAttributesCache, &t2475_TI, &t2475_0_0_0, &t2475_1_0_0, NULL, &t2475_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Text>
extern Il2CppType t2474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27698_GM;
MethodInfo m27698_MI = 
{
	"GetEnumerator", NULL, &t2476_TI, &t2474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27698_GM};
static MethodInfo* t2476_MIs[] =
{
	&m27698_MI,
	NULL
};
static TypeInfo* t2476_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2476_1_0_0;
struct t2476;
extern Il2CppGenericClass t2476_GC;
TypeInfo t2476_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2476_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2476_TI, t2476_ITIs, NULL, &EmptyCustomAttributesCache, &t2476_TI, &t2476_0_0_0, &t2476_1_0_0, NULL, &t2476_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Text>
static PropertyInfo t2474____Current_PropertyInfo = 
{
	&t2474_TI, "Current", &m27699_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2474_PIs[] =
{
	&t2474____Current_PropertyInfo,
	NULL
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27699_GM;
MethodInfo m27699_MI = 
{
	"get_Current", NULL, &t2474_TI, &t15_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27699_GM};
static MethodInfo* t2474_MIs[] =
{
	&m27699_MI,
	NULL
};
static TypeInfo* t2474_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2474_0_0_0;
extern Il2CppType t2474_1_0_0;
struct t2474;
extern Il2CppGenericClass t2474_GC;
TypeInfo t2474_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2474_MIs, t2474_PIs, NULL, NULL, NULL, NULL, NULL, &t2474_TI, t2474_ITIs, NULL, &EmptyCustomAttributesCache, &t2474_TI, &t2474_0_0_0, &t2474_1_0_0, NULL, &t2474_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2481.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2481_TI;
#include "t2481MD.h"

extern MethodInfo m13031_MI;
extern MethodInfo m20797_MI;
struct t20;
#define m20797(__this, p0, method) (t15 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Text>
extern Il2CppType t20_0_0_1;
FieldInfo t2481_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2481_TI, offsetof(t2481, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2481_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2481_TI, offsetof(t2481, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2481_FIs[] =
{
	&t2481_f0_FieldInfo,
	&t2481_f1_FieldInfo,
	NULL
};
extern MethodInfo m13028_MI;
static PropertyInfo t2481____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2481_TI, "System.Collections.IEnumerator.Current", &m13028_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2481____Current_PropertyInfo = 
{
	&t2481_TI, "Current", &m13031_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2481_PIs[] =
{
	&t2481____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2481____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2481_m13027_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13027_GM;
MethodInfo m13027_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2481_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2481_m13027_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13027_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13028_GM;
MethodInfo m13028_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2481_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13028_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13029_GM;
MethodInfo m13029_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2481_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13029_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13030_GM;
MethodInfo m13030_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2481_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13030_GM};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13031_GM;
MethodInfo m13031_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2481_TI, &t15_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13031_GM};
static MethodInfo* t2481_MIs[] =
{
	&m13027_MI,
	&m13028_MI,
	&m13029_MI,
	&m13030_MI,
	&m13031_MI,
	NULL
};
extern MethodInfo m13030_MI;
extern MethodInfo m13029_MI;
static MethodInfo* t2481_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13028_MI,
	&m13030_MI,
	&m13029_MI,
	&m13031_MI,
};
static TypeInfo* t2481_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2474_TI,
};
static Il2CppInterfaceOffsetPair t2481_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2474_TI, 7},
};
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2481_RGCTXData[3] = 
{
	&m13031_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m20797_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2481_0_0_0;
extern Il2CppType t2481_1_0_0;
extern Il2CppGenericClass t2481_GC;
TypeInfo t2481_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2481_MIs, t2481_PIs, t2481_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2481_TI, t2481_ITIs, t2481_VT, &EmptyCustomAttributesCache, &t2481_TI, &t2481_0_0_0, &t2481_1_0_0, t2481_IOs, &t2481_GC, NULL, NULL, NULL, t2481_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2481)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Text>
extern MethodInfo m27705_MI;
extern MethodInfo m27706_MI;
static PropertyInfo t2486____Item_PropertyInfo = 
{
	&t2486_TI, "Item", &m27705_MI, &m27706_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2486_PIs[] =
{
	&t2486____Item_PropertyInfo,
	NULL
};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2486_m27707_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27707_GM;
MethodInfo m27707_MI = 
{
	"IndexOf", NULL, &t2486_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2486_m27707_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27707_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2486_m27708_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27708_GM;
MethodInfo m27708_MI = 
{
	"Insert", NULL, &t2486_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2486_m27708_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27708_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2486_m27709_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27709_GM;
MethodInfo m27709_MI = 
{
	"RemoveAt", NULL, &t2486_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2486_m27709_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27709_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2486_m27705_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27705_GM;
MethodInfo m27705_MI = 
{
	"get_Item", NULL, &t2486_TI, &t15_0_0_0, RuntimeInvoker_t29_t44, t2486_m27705_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27705_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2486_m27706_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27706_GM;
MethodInfo m27706_MI = 
{
	"set_Item", NULL, &t2486_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2486_m27706_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27706_GM};
static MethodInfo* t2486_MIs[] =
{
	&m27707_MI,
	&m27708_MI,
	&m27709_MI,
	&m27705_MI,
	&m27706_MI,
	NULL
};
static TypeInfo* t2486_ITIs[] = 
{
	&t603_TI,
	&t2475_TI,
	&t2476_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2486_0_0_0;
extern Il2CppType t2486_1_0_0;
struct t2486;
extern Il2CppGenericClass t2486_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2486_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2486_MIs, t2486_PIs, NULL, NULL, NULL, NULL, NULL, &t2486_TI, t2486_ITIs, NULL, &t1908__CustomAttributeCache, &t2486_TI, &t2486_0_0_0, &t2486_1_0_0, NULL, &t2486_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5304_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ILayoutElement>
extern MethodInfo m27710_MI;
static PropertyInfo t5304____Count_PropertyInfo = 
{
	&t5304_TI, "Count", &m27710_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27711_MI;
static PropertyInfo t5304____IsReadOnly_PropertyInfo = 
{
	&t5304_TI, "IsReadOnly", &m27711_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5304_PIs[] =
{
	&t5304____Count_PropertyInfo,
	&t5304____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27710_GM;
MethodInfo m27710_MI = 
{
	"get_Count", NULL, &t5304_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27710_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27711_GM;
MethodInfo m27711_MI = 
{
	"get_IsReadOnly", NULL, &t5304_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27711_GM};
extern Il2CppType t271_0_0_0;
extern Il2CppType t271_0_0_0;
static ParameterInfo t5304_m27712_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27712_GM;
MethodInfo m27712_MI = 
{
	"Add", NULL, &t5304_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5304_m27712_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27712_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27713_GM;
MethodInfo m27713_MI = 
{
	"Clear", NULL, &t5304_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27713_GM};
extern Il2CppType t271_0_0_0;
static ParameterInfo t5304_m27714_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27714_GM;
MethodInfo m27714_MI = 
{
	"Contains", NULL, &t5304_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5304_m27714_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27714_GM};
extern Il2CppType t3833_0_0_0;
extern Il2CppType t3833_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5304_m27715_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3833_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27715_GM;
MethodInfo m27715_MI = 
{
	"CopyTo", NULL, &t5304_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5304_m27715_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27715_GM};
extern Il2CppType t271_0_0_0;
static ParameterInfo t5304_m27716_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27716_GM;
MethodInfo m27716_MI = 
{
	"Remove", NULL, &t5304_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5304_m27716_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27716_GM};
static MethodInfo* t5304_MIs[] =
{
	&m27710_MI,
	&m27711_MI,
	&m27712_MI,
	&m27713_MI,
	&m27714_MI,
	&m27715_MI,
	&m27716_MI,
	NULL
};
extern TypeInfo t5306_TI;
static TypeInfo* t5304_ITIs[] = 
{
	&t603_TI,
	&t5306_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5304_0_0_0;
extern Il2CppType t5304_1_0_0;
struct t5304;
extern Il2CppGenericClass t5304_GC;
TypeInfo t5304_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5304_MIs, t5304_PIs, NULL, NULL, NULL, NULL, NULL, &t5304_TI, t5304_ITIs, NULL, &EmptyCustomAttributesCache, &t5304_TI, &t5304_0_0_0, &t5304_1_0_0, NULL, &t5304_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ILayoutElement>
extern Il2CppType t4135_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27717_GM;
MethodInfo m27717_MI = 
{
	"GetEnumerator", NULL, &t5306_TI, &t4135_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27717_GM};
static MethodInfo* t5306_MIs[] =
{
	&m27717_MI,
	NULL
};
static TypeInfo* t5306_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5306_0_0_0;
extern Il2CppType t5306_1_0_0;
struct t5306;
extern Il2CppGenericClass t5306_GC;
TypeInfo t5306_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5306_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5306_TI, t5306_ITIs, NULL, &EmptyCustomAttributesCache, &t5306_TI, &t5306_0_0_0, &t5306_1_0_0, NULL, &t5306_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4135_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutElement>
extern MethodInfo m27718_MI;
static PropertyInfo t4135____Current_PropertyInfo = 
{
	&t4135_TI, "Current", &m27718_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4135_PIs[] =
{
	&t4135____Current_PropertyInfo,
	NULL
};
extern Il2CppType t271_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27718_GM;
MethodInfo m27718_MI = 
{
	"get_Current", NULL, &t4135_TI, &t271_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27718_GM};
static MethodInfo* t4135_MIs[] =
{
	&m27718_MI,
	NULL
};
static TypeInfo* t4135_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4135_0_0_0;
extern Il2CppType t4135_1_0_0;
struct t4135;
extern Il2CppGenericClass t4135_GC;
TypeInfo t4135_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4135_MIs, t4135_PIs, NULL, NULL, NULL, NULL, NULL, &t4135_TI, t4135_ITIs, NULL, &EmptyCustomAttributesCache, &t4135_TI, &t4135_0_0_0, &t4135_1_0_0, NULL, &t4135_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2482.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2482_TI;
#include "t2482MD.h"

extern TypeInfo t271_TI;
extern MethodInfo m13036_MI;
extern MethodInfo m20808_MI;
struct t20;
#define m20808(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutElement>
extern Il2CppType t20_0_0_1;
FieldInfo t2482_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2482_TI, offsetof(t2482, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2482_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2482_TI, offsetof(t2482, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2482_FIs[] =
{
	&t2482_f0_FieldInfo,
	&t2482_f1_FieldInfo,
	NULL
};
extern MethodInfo m13033_MI;
static PropertyInfo t2482____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2482_TI, "System.Collections.IEnumerator.Current", &m13033_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2482____Current_PropertyInfo = 
{
	&t2482_TI, "Current", &m13036_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2482_PIs[] =
{
	&t2482____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2482____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2482_m13032_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13032_GM;
MethodInfo m13032_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2482_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2482_m13032_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13032_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13033_GM;
MethodInfo m13033_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2482_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13033_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13034_GM;
MethodInfo m13034_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2482_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13034_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13035_GM;
MethodInfo m13035_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2482_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13035_GM};
extern Il2CppType t271_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13036_GM;
MethodInfo m13036_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2482_TI, &t271_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13036_GM};
static MethodInfo* t2482_MIs[] =
{
	&m13032_MI,
	&m13033_MI,
	&m13034_MI,
	&m13035_MI,
	&m13036_MI,
	NULL
};
extern MethodInfo m13035_MI;
extern MethodInfo m13034_MI;
static MethodInfo* t2482_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13033_MI,
	&m13035_MI,
	&m13034_MI,
	&m13036_MI,
};
static TypeInfo* t2482_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4135_TI,
};
static Il2CppInterfaceOffsetPair t2482_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4135_TI, 7},
};
extern TypeInfo t271_TI;
static Il2CppRGCTXData t2482_RGCTXData[3] = 
{
	&m13036_MI/* Method Usage */,
	&t271_TI/* Class Usage */,
	&m20808_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2482_0_0_0;
extern Il2CppType t2482_1_0_0;
extern Il2CppGenericClass t2482_GC;
TypeInfo t2482_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2482_MIs, t2482_PIs, t2482_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2482_TI, t2482_ITIs, t2482_VT, &EmptyCustomAttributesCache, &t2482_TI, &t2482_0_0_0, &t2482_1_0_0, t2482_IOs, &t2482_GC, NULL, NULL, NULL, t2482_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2482)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5305_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutElement>
extern MethodInfo m27719_MI;
extern MethodInfo m27720_MI;
static PropertyInfo t5305____Item_PropertyInfo = 
{
	&t5305_TI, "Item", &m27719_MI, &m27720_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5305_PIs[] =
{
	&t5305____Item_PropertyInfo,
	NULL
};
extern Il2CppType t271_0_0_0;
static ParameterInfo t5305_m27721_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27721_GM;
MethodInfo m27721_MI = 
{
	"IndexOf", NULL, &t5305_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5305_m27721_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27721_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t271_0_0_0;
static ParameterInfo t5305_m27722_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27722_GM;
MethodInfo m27722_MI = 
{
	"Insert", NULL, &t5305_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5305_m27722_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27722_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5305_m27723_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27723_GM;
MethodInfo m27723_MI = 
{
	"RemoveAt", NULL, &t5305_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5305_m27723_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27723_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5305_m27719_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t271_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27719_GM;
MethodInfo m27719_MI = 
{
	"get_Item", NULL, &t5305_TI, &t271_0_0_0, RuntimeInvoker_t29_t44, t5305_m27719_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27719_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t271_0_0_0;
static ParameterInfo t5305_m27720_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27720_GM;
MethodInfo m27720_MI = 
{
	"set_Item", NULL, &t5305_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5305_m27720_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27720_GM};
static MethodInfo* t5305_MIs[] =
{
	&m27721_MI,
	&m27722_MI,
	&m27723_MI,
	&m27719_MI,
	&m27720_MI,
	NULL
};
static TypeInfo* t5305_ITIs[] = 
{
	&t603_TI,
	&t5304_TI,
	&t5306_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5305_0_0_0;
extern Il2CppType t5305_1_0_0;
struct t5305;
extern Il2CppGenericClass t5305_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5305_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5305_MIs, t5305_PIs, NULL, NULL, NULL, NULL, NULL, &t5305_TI, t5305_ITIs, NULL, &t1908__CustomAttributeCache, &t5305_TI, &t5305_0_0_0, &t5305_1_0_0, NULL, &t5305_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5307_TI;

#include "t182.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.MaskableGraphic>
extern MethodInfo m27724_MI;
static PropertyInfo t5307____Count_PropertyInfo = 
{
	&t5307_TI, "Count", &m27724_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27725_MI;
static PropertyInfo t5307____IsReadOnly_PropertyInfo = 
{
	&t5307_TI, "IsReadOnly", &m27725_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5307_PIs[] =
{
	&t5307____Count_PropertyInfo,
	&t5307____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27724_GM;
MethodInfo m27724_MI = 
{
	"get_Count", NULL, &t5307_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27724_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27725_GM;
MethodInfo m27725_MI = 
{
	"get_IsReadOnly", NULL, &t5307_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27725_GM};
extern Il2CppType t182_0_0_0;
extern Il2CppType t182_0_0_0;
static ParameterInfo t5307_m27726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27726_GM;
MethodInfo m27726_MI = 
{
	"Add", NULL, &t5307_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5307_m27726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27726_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27727_GM;
MethodInfo m27727_MI = 
{
	"Clear", NULL, &t5307_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27727_GM};
extern Il2CppType t182_0_0_0;
static ParameterInfo t5307_m27728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27728_GM;
MethodInfo m27728_MI = 
{
	"Contains", NULL, &t5307_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5307_m27728_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27728_GM};
extern Il2CppType t3834_0_0_0;
extern Il2CppType t3834_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5307_m27729_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3834_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27729_GM;
MethodInfo m27729_MI = 
{
	"CopyTo", NULL, &t5307_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5307_m27729_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27729_GM};
extern Il2CppType t182_0_0_0;
static ParameterInfo t5307_m27730_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27730_GM;
MethodInfo m27730_MI = 
{
	"Remove", NULL, &t5307_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5307_m27730_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27730_GM};
static MethodInfo* t5307_MIs[] =
{
	&m27724_MI,
	&m27725_MI,
	&m27726_MI,
	&m27727_MI,
	&m27728_MI,
	&m27729_MI,
	&m27730_MI,
	NULL
};
extern TypeInfo t5309_TI;
static TypeInfo* t5307_ITIs[] = 
{
	&t603_TI,
	&t5309_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5307_0_0_0;
extern Il2CppType t5307_1_0_0;
struct t5307;
extern Il2CppGenericClass t5307_GC;
TypeInfo t5307_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5307_MIs, t5307_PIs, NULL, NULL, NULL, NULL, NULL, &t5307_TI, t5307_ITIs, NULL, &EmptyCustomAttributesCache, &t5307_TI, &t5307_0_0_0, &t5307_1_0_0, NULL, &t5307_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType t4137_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27731_GM;
MethodInfo m27731_MI = 
{
	"GetEnumerator", NULL, &t5309_TI, &t4137_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27731_GM};
static MethodInfo* t5309_MIs[] =
{
	&m27731_MI,
	NULL
};
static TypeInfo* t5309_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5309_0_0_0;
extern Il2CppType t5309_1_0_0;
struct t5309;
extern Il2CppGenericClass t5309_GC;
TypeInfo t5309_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5309_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5309_TI, t5309_ITIs, NULL, &EmptyCustomAttributesCache, &t5309_TI, &t5309_0_0_0, &t5309_1_0_0, NULL, &t5309_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4137_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.MaskableGraphic>
extern MethodInfo m27732_MI;
static PropertyInfo t4137____Current_PropertyInfo = 
{
	&t4137_TI, "Current", &m27732_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4137_PIs[] =
{
	&t4137____Current_PropertyInfo,
	NULL
};
extern Il2CppType t182_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27732_GM;
MethodInfo m27732_MI = 
{
	"get_Current", NULL, &t4137_TI, &t182_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27732_GM};
static MethodInfo* t4137_MIs[] =
{
	&m27732_MI,
	NULL
};
static TypeInfo* t4137_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4137_0_0_0;
extern Il2CppType t4137_1_0_0;
struct t4137;
extern Il2CppGenericClass t4137_GC;
TypeInfo t4137_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4137_MIs, t4137_PIs, NULL, NULL, NULL, NULL, NULL, &t4137_TI, t4137_ITIs, NULL, &EmptyCustomAttributesCache, &t4137_TI, &t4137_0_0_0, &t4137_1_0_0, NULL, &t4137_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2483.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2483_TI;
#include "t2483MD.h"

extern TypeInfo t182_TI;
extern MethodInfo m13041_MI;
extern MethodInfo m20819_MI;
struct t20;
#define m20819(__this, p0, method) (t182 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType t20_0_0_1;
FieldInfo t2483_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2483_TI, offsetof(t2483, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2483_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2483_TI, offsetof(t2483, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2483_FIs[] =
{
	&t2483_f0_FieldInfo,
	&t2483_f1_FieldInfo,
	NULL
};
extern MethodInfo m13038_MI;
static PropertyInfo t2483____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2483_TI, "System.Collections.IEnumerator.Current", &m13038_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2483____Current_PropertyInfo = 
{
	&t2483_TI, "Current", &m13041_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2483_PIs[] =
{
	&t2483____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2483____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2483_m13037_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13037_GM;
MethodInfo m13037_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2483_m13037_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13037_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13038_GM;
MethodInfo m13038_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2483_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13038_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13039_GM;
MethodInfo m13039_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13039_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13040_GM;
MethodInfo m13040_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2483_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13040_GM};
extern Il2CppType t182_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13041_GM;
MethodInfo m13041_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2483_TI, &t182_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13041_GM};
static MethodInfo* t2483_MIs[] =
{
	&m13037_MI,
	&m13038_MI,
	&m13039_MI,
	&m13040_MI,
	&m13041_MI,
	NULL
};
extern MethodInfo m13040_MI;
extern MethodInfo m13039_MI;
static MethodInfo* t2483_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13038_MI,
	&m13040_MI,
	&m13039_MI,
	&m13041_MI,
};
static TypeInfo* t2483_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4137_TI,
};
static Il2CppInterfaceOffsetPair t2483_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4137_TI, 7},
};
extern TypeInfo t182_TI;
static Il2CppRGCTXData t2483_RGCTXData[3] = 
{
	&m13041_MI/* Method Usage */,
	&t182_TI/* Class Usage */,
	&m20819_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2483_0_0_0;
extern Il2CppType t2483_1_0_0;
extern Il2CppGenericClass t2483_GC;
TypeInfo t2483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2483_MIs, t2483_PIs, t2483_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2483_TI, t2483_ITIs, t2483_VT, &EmptyCustomAttributesCache, &t2483_TI, &t2483_0_0_0, &t2483_1_0_0, t2483_IOs, &t2483_GC, NULL, NULL, NULL, t2483_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2483)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5308_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.MaskableGraphic>
extern MethodInfo m27733_MI;
extern MethodInfo m27734_MI;
static PropertyInfo t5308____Item_PropertyInfo = 
{
	&t5308_TI, "Item", &m27733_MI, &m27734_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5308_PIs[] =
{
	&t5308____Item_PropertyInfo,
	NULL
};
extern Il2CppType t182_0_0_0;
static ParameterInfo t5308_m27735_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27735_GM;
MethodInfo m27735_MI = 
{
	"IndexOf", NULL, &t5308_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5308_m27735_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27735_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t182_0_0_0;
static ParameterInfo t5308_m27736_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27736_GM;
MethodInfo m27736_MI = 
{
	"Insert", NULL, &t5308_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5308_m27736_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27736_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5308_m27737_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27737_GM;
MethodInfo m27737_MI = 
{
	"RemoveAt", NULL, &t5308_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5308_m27737_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27737_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5308_m27733_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t182_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27733_GM;
MethodInfo m27733_MI = 
{
	"get_Item", NULL, &t5308_TI, &t182_0_0_0, RuntimeInvoker_t29_t44, t5308_m27733_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27733_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t182_0_0_0;
static ParameterInfo t5308_m27734_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27734_GM;
MethodInfo m27734_MI = 
{
	"set_Item", NULL, &t5308_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5308_m27734_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27734_GM};
static MethodInfo* t5308_MIs[] =
{
	&m27735_MI,
	&m27736_MI,
	&m27737_MI,
	&m27733_MI,
	&m27734_MI,
	NULL
};
static TypeInfo* t5308_ITIs[] = 
{
	&t603_TI,
	&t5307_TI,
	&t5309_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5308_0_0_0;
extern Il2CppType t5308_1_0_0;
struct t5308;
extern Il2CppGenericClass t5308_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5308_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5308_MIs, t5308_PIs, NULL, NULL, NULL, NULL, NULL, &t5308_TI, t5308_ITIs, NULL, &t1908__CustomAttributeCache, &t5308_TI, &t5308_0_0_0, &t5308_1_0_0, NULL, &t5308_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5310_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaskable>
extern MethodInfo m27738_MI;
static PropertyInfo t5310____Count_PropertyInfo = 
{
	&t5310_TI, "Count", &m27738_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27739_MI;
static PropertyInfo t5310____IsReadOnly_PropertyInfo = 
{
	&t5310_TI, "IsReadOnly", &m27739_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5310_PIs[] =
{
	&t5310____Count_PropertyInfo,
	&t5310____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27738_GM;
MethodInfo m27738_MI = 
{
	"get_Count", NULL, &t5310_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27738_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27739_GM;
MethodInfo m27739_MI = 
{
	"get_IsReadOnly", NULL, &t5310_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27739_GM};
extern Il2CppType t369_0_0_0;
extern Il2CppType t369_0_0_0;
static ParameterInfo t5310_m27740_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t369_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27740_GM;
MethodInfo m27740_MI = 
{
	"Add", NULL, &t5310_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5310_m27740_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27740_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27741_GM;
MethodInfo m27741_MI = 
{
	"Clear", NULL, &t5310_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27741_GM};
extern Il2CppType t369_0_0_0;
static ParameterInfo t5310_m27742_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t369_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27742_GM;
MethodInfo m27742_MI = 
{
	"Contains", NULL, &t5310_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5310_m27742_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27742_GM};
extern Il2CppType t3835_0_0_0;
extern Il2CppType t3835_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5310_m27743_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3835_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27743_GM;
MethodInfo m27743_MI = 
{
	"CopyTo", NULL, &t5310_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5310_m27743_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27743_GM};
extern Il2CppType t369_0_0_0;
static ParameterInfo t5310_m27744_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t369_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27744_GM;
MethodInfo m27744_MI = 
{
	"Remove", NULL, &t5310_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5310_m27744_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27744_GM};
static MethodInfo* t5310_MIs[] =
{
	&m27738_MI,
	&m27739_MI,
	&m27740_MI,
	&m27741_MI,
	&m27742_MI,
	&m27743_MI,
	&m27744_MI,
	NULL
};
extern TypeInfo t5312_TI;
static TypeInfo* t5310_ITIs[] = 
{
	&t603_TI,
	&t5312_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5310_0_0_0;
extern Il2CppType t5310_1_0_0;
struct t5310;
extern Il2CppGenericClass t5310_GC;
TypeInfo t5310_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5310_MIs, t5310_PIs, NULL, NULL, NULL, NULL, NULL, &t5310_TI, t5310_ITIs, NULL, &EmptyCustomAttributesCache, &t5310_TI, &t5310_0_0_0, &t5310_1_0_0, NULL, &t5310_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMaskable>
extern Il2CppType t4139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27745_GM;
MethodInfo m27745_MI = 
{
	"GetEnumerator", NULL, &t5312_TI, &t4139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27745_GM};
static MethodInfo* t5312_MIs[] =
{
	&m27745_MI,
	NULL
};
static TypeInfo* t5312_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5312_0_0_0;
extern Il2CppType t5312_1_0_0;
struct t5312;
extern Il2CppGenericClass t5312_GC;
TypeInfo t5312_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5312_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5312_TI, t5312_ITIs, NULL, &EmptyCustomAttributesCache, &t5312_TI, &t5312_0_0_0, &t5312_1_0_0, NULL, &t5312_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
