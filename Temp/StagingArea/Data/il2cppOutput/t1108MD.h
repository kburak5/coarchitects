﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1108;
struct t7;
struct t781;
struct t66;
struct t67;
struct t29;
struct t1034;
#include "t35.h"
#include "t751.h"
#include "t1311.h"
#include "t1314.h"
#include "t1313.h"
#include "t1064.h"

 void m7030 (t1108 * __this, t35 p0, int32_t p1, bool p2, int32_t p3, bool p4, bool p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7031 (t1108 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7032 (t1108 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7033 (t1108 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, bool p5, bool p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7034 (t1108 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, bool p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7035 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7036 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7037 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5182 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7038 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7039 (t1108 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7040 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7041 (t1108 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5183 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7042 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7043 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7044 (t1108 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7045 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7046 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7047 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7048 (t1108 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7049 (t1108 * __this, int64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7050 (t1108 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7051 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7052 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7053 (t1108 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7054 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7055 (t1108 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7056 (t1108 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7057 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7058 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7059 (t1108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7060 (t1108 * __this, t35 p0, t781* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7061 (t1108 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7062 (t1108 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7063 (t1108 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
