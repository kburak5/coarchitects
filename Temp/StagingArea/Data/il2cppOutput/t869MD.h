﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t869;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t866.h"

 void m3705 (t869 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m3706 (t869 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3707 (t869 * __this, t866  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m3708 (t869 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
