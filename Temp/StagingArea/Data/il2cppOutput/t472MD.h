﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t472;
struct t29;
struct t469;
struct t2857;
struct t2344;
struct t733;
struct t2858;
struct t20;
struct t136;
struct t2860;
struct t722;
#include "t735.h"
#include "t2859.h"
#include "t2861.h"
#include "t725.h"

 void m2846 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15499 (t472 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15500 (t472 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15501 (t472 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15502 (t472 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15503 (t472 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15504 (t472 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15505 (t472 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15506 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15507 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15508 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15509 (t472 * __this, t2859  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15510 (t472 * __this, t2859  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15511 (t472 * __this, t2858* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15512 (t472 * __this, t2859  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15513 (t472 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15514 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m15515 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15516 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15517 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15518 (t472 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2848 (t472 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15519 (t472 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15520 (t472 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15521 (t472 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2859  m15522 (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15523 (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15524 (t472 * __this, t2858* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15525 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15526 (t472 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15527 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15528 (t472 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15529 (t472 * __this, t469 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15530 (t472 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15531 (t472 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15532 (t472 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2847 (t472 * __this, int32_t p0, t469 ** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2857 * m15533 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15534 (t472 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15535 (t472 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15536 (t472 * __this, t2859  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2861  m15537 (t472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15538 (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
