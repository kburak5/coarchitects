﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2615;
struct t29;
struct t20;
#include "t209.h"

 void m14079 (t2615 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14080 (t2615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14081 (t2615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14082 (t2615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14083 (t2615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
