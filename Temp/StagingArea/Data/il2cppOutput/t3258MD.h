﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3258;
struct t29;
struct t20;
#include "t1067.h"

 void m18086 (t3258 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18087 (t3258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18088 (t3258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18089 (t3258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18090 (t3258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
