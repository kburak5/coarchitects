﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t127;
struct t336;
#include "t23.h"

 t23  m1486 (t127 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1487 (t127 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1489 (t127 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t336 * m1488 (t127 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
