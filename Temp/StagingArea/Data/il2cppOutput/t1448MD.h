﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1448;
struct t7;
struct t29;

 void m7852 (t1448 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7853 (t1448 * __this, t7* p0, t29 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7854 (t1448 * __this, t7* p0, t29 * p1, bool p2, t7* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
