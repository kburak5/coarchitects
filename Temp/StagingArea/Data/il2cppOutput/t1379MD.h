﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1379;
struct t42;
struct t7;
struct t638;
struct t29;
struct t316;
struct t295;
struct t631;
struct t633;
struct t733;
#include "t1343.h"
#include "t1342.h"
#include "t1150.h"
#include "t630.h"
#include "t735.h"

 void m7648 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t638* m7649 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7650 (t1379 * __this, t29 * p0, t316* p1, t295 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7651 (t1379 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7652 (t1379 * __this, int32_t p0, t631 * p1, t316* p2, t633 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1343  m7653 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7654 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7655 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7656 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7657 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7658 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7659 (t1379 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7660 (t1379 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7661 (t1379 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7662 (t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7663 (t1379 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
