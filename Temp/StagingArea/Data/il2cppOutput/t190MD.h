﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t190;
struct t2607;
struct t557;
struct t7;
struct t29;
struct t556;

#include "t2315MD.h"
#define m1693(__this, method) (void)m11776_gshared((t2315 *)__this, method)
#define m14041(__this, p0, method) (void)m11778_gshared((t2315 *)__this, (t2120 *)p0, method)
#define m14042(__this, p0, method) (void)m11780_gshared((t2315 *)__this, (t2120 *)p0, method)
#define m1694(__this, p0, p1, method) (t557 *)m11781_gshared((t2315 *)__this, (t7*)p0, (t29 *)p1, method)
#define m1695(__this, p0, p1, method) (t556 *)m11782_gshared((t2315 *)__this, (t29 *)p0, (t557 *)p1, method)
#define m1696(__this, p0, method) (t556 *)m11783_gshared((t29 *)__this, (t2120 *)p0, method)
#define m1769(__this, p0, method) (void)m11784_gshared((t2315 *)__this, (t29 *)p0, method)
