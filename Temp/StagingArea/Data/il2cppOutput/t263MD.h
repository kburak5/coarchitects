﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t263;
struct t466;
struct t7;
#include "t35.h"
#include "t164.h"

 void m1988 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2257 (t263 * __this, t466 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2258 (t263 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2259 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2260 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2261 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1968 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2262 (t263 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2263 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2264 (t263 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1969 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2265 (t263 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2266 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2267 (t263 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1962 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1963 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m2268 (t263 * __this, t164  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m2269 (t29 * __this, t263 * p0, t164 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2270 (t263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
