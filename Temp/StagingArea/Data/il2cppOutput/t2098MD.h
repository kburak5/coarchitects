﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2098;
#include "t816.h"

 void m10269 (t2098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19469 (t2098 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19470 (t2098 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
