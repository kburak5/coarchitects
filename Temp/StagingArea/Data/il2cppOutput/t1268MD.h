﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1268;
struct t7;
struct t29;
struct t731;
struct t726;

 void m6639 (t1268 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6640 (t1268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6641 (t1268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6642 (t1268 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6643 (t1268 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6644 (t1268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6645 (t1268 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
