﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2315;
struct t2120;
struct t557;
struct t7;
struct t29;
struct t556;

 void m11776_gshared (t2315 * __this, MethodInfo* method);
#define m11776(__this, method) (void)m11776_gshared((t2315 *)__this, method)
 void m11778_gshared (t2315 * __this, t2120 * p0, MethodInfo* method);
#define m11778(__this, p0, method) (void)m11778_gshared((t2315 *)__this, (t2120 *)p0, method)
 void m11780_gshared (t2315 * __this, t2120 * p0, MethodInfo* method);
#define m11780(__this, p0, method) (void)m11780_gshared((t2315 *)__this, (t2120 *)p0, method)
 t557 * m11781_gshared (t2315 * __this, t7* p0, t29 * p1, MethodInfo* method);
#define m11781(__this, p0, p1, method) (t557 *)m11781_gshared((t2315 *)__this, (t7*)p0, (t29 *)p1, method)
 t556 * m11782_gshared (t2315 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m11782(__this, p0, p1, method) (t556 *)m11782_gshared((t2315 *)__this, (t29 *)p0, (t557 *)p1, method)
 t556 * m11783_gshared (t29 * __this, t2120 * p0, MethodInfo* method);
#define m11783(__this, p0, method) (t556 *)m11783_gshared((t29 *)__this, (t2120 *)p0, method)
 void m11784_gshared (t2315 * __this, t29 * p0, MethodInfo* method);
#define m11784(__this, p0, method) (void)m11784_gshared((t2315 *)__this, (t29 *)p0, method)
