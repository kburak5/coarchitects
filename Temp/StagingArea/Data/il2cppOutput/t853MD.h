﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t853;
struct t721;
struct t446;
struct t764;
struct t836;

 void m3591 (t853 * __this, t764* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3592 (t853 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3593 (t853 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3594 (t853 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3595 (t853 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3596 (t853 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3597 (t853 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m3598 (t853 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3599 (t853 * __this, t446* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
