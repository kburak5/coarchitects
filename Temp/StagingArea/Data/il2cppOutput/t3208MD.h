﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3208;
struct t29;
struct t20;
#include "t784.h"

 void m17840 (t3208 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17841 (t3208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17842 (t3208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17843 (t3208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17844 (t3208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
