﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t986;
struct t781;
struct t988;

 void m4391 (t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4392 (t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4393 (t986 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4394 (t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4395 (t986 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4396 (t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4397 (t986 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4398 (t986 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4399 (t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4400 (t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4401 (t986 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4402 (t986 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4403 (t986 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4404 (t986 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4405 (t986 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
