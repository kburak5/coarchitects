﻿#pragma once
#include <stdint.h>
struct t7;
#include "t110.h"
#include "t811.h"
struct t805 
{
	int32_t f0;
	t7* f1;
};
// Native definition for marshalling of: System.Security.Cryptography.X509Certificates.X509ChainStatus
struct t805_marshaled
{
	int32_t f0;
	char* f1;
};
