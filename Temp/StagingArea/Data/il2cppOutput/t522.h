﻿#pragma once
#include <stdint.h>
struct t7;
#include "t110.h"
#include "t23.h"
#include "t362.h"
struct t522 
{
	t7* f0;
	t23  f1;
	t362  f2;
	t23  f3;
	int32_t f4;
};
// Native definition for marshalling of: UnityEngine.SkeletonBone
struct t522_marshaled
{
	char* f0;
	t23  f1;
	t362  f2;
	t23  f3;
	int32_t f4;
};
