﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t657;
struct t41;
struct t41_marshaled;
struct t557;
struct t316;

 void m2975 (t657 * __this, t41 * p0, t557 * p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17281 (t657 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
