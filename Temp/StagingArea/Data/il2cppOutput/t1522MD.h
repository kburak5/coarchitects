﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1522;
struct t29;
struct t7;
struct t731;
#include "t1520.h"

 void m8151 (t1522 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8152 (t1522 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1520  m8153 (t1522 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8154 (t1522 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8155 (t1522 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8156 (t1522 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
