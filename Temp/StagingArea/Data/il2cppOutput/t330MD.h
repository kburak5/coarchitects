﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t330;
struct t332;
struct t507;
struct t25;
#include "t17.h"

 t17  m1470 (t330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1471 (t330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1616 (t330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t332 * m1472 (t330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t507 * m2606 (t330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m1474 (t330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
