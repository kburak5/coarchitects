﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t475;
struct t29;
struct t474;
struct t2872;
struct t20;
struct t136;
struct t2873;
struct t2874;
struct t2875;
struct t2871;
struct t2876;
struct t2877;
#include "t588.h"

#include "t294MD.h"
#define m2853(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m15616(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m15617(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m15618(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m15619(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m15620(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m15621(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m15622(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m15623(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m15624(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m15625(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m15626(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m15627(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m15628(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m15629(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m15630(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m15631(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m15632(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m15633(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m15634(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m15635(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m15636(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m15637(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m15638(__this, method) (t2875 *)m10583_gshared((t294 *)__this, method)
#define m15639(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m15640(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m15641(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m15642(__this, p0, method) (t474 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m15643(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m15644(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t588  m2850 (t475 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m15645(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m15646(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m15647(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m15648(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m15649(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m15650(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m15651(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m15652(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m15653(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m15654(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m15655(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m15656(__this, method) (t2871*)m10619_gshared((t294 *)__this, method)
#define m15657(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m15658(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m15659(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m2854(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m2855(__this, p0, method) (t474 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m15660(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
