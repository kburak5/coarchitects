﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1001;
struct t7;
struct t781;

 void m4466 (t1001 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4467 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4468 (t1001 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4469 (t1001 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4470 (t1001 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4471 (t1001 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4472 (t1001 * __this, t781* p0, int32_t p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4473 (t1001 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4474 (t1001 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4475 (t1001 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4476 (t1001 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
