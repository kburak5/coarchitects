﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1687;
struct t1687_marshaled;

void t1687_marshal(const t1687& unmarshaled, t1687_marshaled& marshaled);
void t1687_marshal_back(const t1687_marshaled& marshaled, t1687& unmarshaled);
void t1687_marshal_cleanup(t1687_marshaled& marshaled);
