﻿#pragma once
#include <stdint.h>
#include "t448.h"
#include "t35.h"
struct t203  : public t448
{
	t35 f0;
};
// Native definition for marshalling of: UnityEngine.Coroutine
struct t203_marshaled
{
	t35 f0;
};
