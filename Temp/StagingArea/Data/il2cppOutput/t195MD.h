﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t195;
struct t733;
struct t29;
struct t1132;
struct t353;
#include "t735.h"

 void m1701 (t195 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1699 (t195 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1700 (t195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1132* m1703 (t195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m1704 (t195 * __this, t353 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5841 (t195 * __this, t195 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t195 * m5842 (t29 * __this, t195 * p0, t195 * p1, t195 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m1705 (t195 * __this, t353 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
