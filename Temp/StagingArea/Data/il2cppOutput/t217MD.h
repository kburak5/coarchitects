﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t217;
struct t2;
struct t213;
struct t6;
struct t136;
struct t64;
struct t139;
struct t25;
#include "t212.h"
#include "t215.h"
#include "t140.h"

 void m734 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m735 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m736 (t217 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m737 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m738 (t217 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m739 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m740 (t217 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m741 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m742 (t217 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m743 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m744 (t217 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t213 * m745 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m746 (t217 * __this, t213 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m747 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m748 (t217 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m749 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m750 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m751 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m752 (t217 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m753 (t217 * __this, float p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m754 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m755 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m756 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m757 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m758 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m759 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m760 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m761 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m762 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m763 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m764 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m765 (t217 * __this, t64 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m766 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m767 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m768 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m769 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m770 (t217 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m771 (t217 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m772 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m773 (t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
