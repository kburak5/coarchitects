﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3189;
struct t29;
struct t7;
struct t66;
struct t67;
#include "t35.h"
#include "t725.h"

 void m17795 (t3189 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17796 (t3189 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17797 (t3189 * __this, t7* p0, bool p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17798 (t3189 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
