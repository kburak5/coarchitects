﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t459;
struct t459_marshaled;

 void m2117 (t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2118 (t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2119 (t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2120 (t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t459_marshal(const t459& unmarshaled, t459_marshaled& marshaled);
void t459_marshal_back(const t459_marshaled& marshaled, t459& unmarshaled);
void t459_marshal_cleanup(t459_marshaled& marshaled);
