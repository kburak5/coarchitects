﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3230;
struct t29;
struct t20;
#include "t845.h"

 void m17946 (t3230 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17947 (t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17948 (t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17949 (t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m17950 (t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
