﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t375;
struct t375_marshaled;

 void m1708 (t375 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t375_marshal(const t375& unmarshaled, t375_marshaled& marshaled);
void t375_marshal_back(const t375_marshaled& marshaled, t375& unmarshaled);
void t375_marshal_cleanup(t375_marshaled& marshaled);
