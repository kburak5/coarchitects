﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3129;
struct t29;
struct t20;
#include "t564.h"

 void m17275 (t3129 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17276 (t3129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17277 (t3129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17278 (t3129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17279 (t3129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
