﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t871;
struct t7;
struct t872;
struct t719;
struct t873;
struct t874;
struct t875;
struct t876;
struct t731;
struct t305;
#include "t842.h"
#include "t849.h"

 void m3720 (t871 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3721 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3722 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3723 (t29 * __this, t7* p0, int32_t* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3724 (t29 * __this, t7* p0, int32_t* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3725 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t872 * m3726 (t871 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3727 (t871 * __this, t719 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3728 (t871 * __this, t873 * p0, int32_t p1, t874 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3729 (t871 * __this, int32_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3730 (t871 * __this, t876 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3731 (t871 * __this, int32_t* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3732 (t871 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3733 (t871 * __this, int32_t* p0, int32_t* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3734 (t871 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3735 (t871 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3736 (t871 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3737 (t871 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3738 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3739 (t871 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3740 (t29 * __this, uint16_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3741 (t871 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3742 (t871 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3743 (t871 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3744 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3745 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3746 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3747 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3748 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3749 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t305 * m3750 (t871 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
