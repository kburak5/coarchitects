﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1498;
struct t29;
struct t1451;
struct t66;
struct t67;
#include "t35.h"

 void m9628 (t1498 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9629 (t1498 * __this, t1451* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9630 (t1498 * __this, t1451* p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9631 (t1498 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
