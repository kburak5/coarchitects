﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3160;
struct t29;
struct t20;

 void m17575 (t3160 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17576 (t3160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17577 (t3160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17578 (t3160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17579 (t3160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
