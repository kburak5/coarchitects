﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1221;
struct t7;
struct t1222;
struct t781;
struct t997;
struct t1223;
struct t1212;
struct t1219;
#include "t936.h"

 void m6427 (t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6428 (t1221 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6429 (t1221 * __this, t781* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6430 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6431 (t1221 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6432 (t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6433 (t1221 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1222 * m6434 (t1221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6435 (t1221 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t997 * m6436 (t1221 * __this, t7* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6437 (t1221 * __this, t7* p0, t781* p1, int32_t p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6438 (t1221 * __this, t1223 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t936  m6439 (t1221 * __this, bool* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6440 (t1221 * __this, t1212 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6441 (t1221 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6442 (t1221 * __this, t781* p0, t781* p1, int32_t p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6443 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
