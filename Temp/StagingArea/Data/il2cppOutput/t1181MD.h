﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1181;
struct t1176;
struct t633;
struct t1183;
struct t1184;
struct t7;
struct t1174;
#include "t35.h"

 void m6100 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1176 * m6101 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6102 (t29 * __this, t633 * p0, t1176 * p1, t1183** p2, t1184** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6103 (t29 * __this, t7* p0, t1174 ** p1, uint8_t** p2, uint8_t** p3, t1174 ** p4, uint8_t** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6104 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6105 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6106 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6107 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6108 (t29 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6109 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6110 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6111 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6112 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6113 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6114 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6115 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m6116 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6117 (t29 * __this, uint8_t* p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6118 (t29 * __this, t7* p0, t1174 ** p1, uint8_t** p2, uint8_t** p3, t1174 ** p4, uint8_t** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6119 (t29 * __this, t7* p0, t1174 ** p1, uint8_t** p2, uint8_t** p3, t1174 ** p4, uint8_t** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
