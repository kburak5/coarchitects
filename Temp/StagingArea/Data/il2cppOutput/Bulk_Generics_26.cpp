﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t3253.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t3253_TI;
#include "t3253MD.h"

#include "t29.h"
#include "t1022.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t20.h"
#include "t40.h"
extern TypeInfo t1022_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18065_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m23883_MI;
struct t20;
#include "t915.h"
 int32_t m23883 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18061_MI;
 void m18061 (t3253 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18062_MI;
 t29 * m18062 (t3253 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18065(__this, &m18065_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1022_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18063_MI;
 void m18063 (t3253 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18064_MI;
 bool m18064 (t3253 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18065 (t3253 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23883(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23883_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern Il2CppType t20_0_0_1;
FieldInfo t3253_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3253_TI, offsetof(t3253, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3253_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3253_TI, offsetof(t3253, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3253_FIs[] =
{
	&t3253_f0_FieldInfo,
	&t3253_f1_FieldInfo,
	NULL
};
static PropertyInfo t3253____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3253_TI, "System.Collections.IEnumerator.Current", &m18062_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3253____Current_PropertyInfo = 
{
	&t3253_TI, "Current", &m18065_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3253_PIs[] =
{
	&t3253____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3253____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3253_m18061_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18061_GM;
MethodInfo m18061_MI = 
{
	".ctor", (methodPointerType)&m18061, &t3253_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3253_m18061_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18061_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18062_GM;
MethodInfo m18062_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18062, &t3253_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18062_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18063_GM;
MethodInfo m18063_MI = 
{
	"Dispose", (methodPointerType)&m18063, &t3253_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18063_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18064_GM;
MethodInfo m18064_MI = 
{
	"MoveNext", (methodPointerType)&m18064, &t3253_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18064_GM};
extern Il2CppType t1022_0_0_0;
extern void* RuntimeInvoker_t1022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18065_GM;
MethodInfo m18065_MI = 
{
	"get_Current", (methodPointerType)&m18065, &t3253_TI, &t1022_0_0_0, RuntimeInvoker_t1022, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18065_GM};
static MethodInfo* t3253_MIs[] =
{
	&m18061_MI,
	&m18062_MI,
	&m18063_MI,
	&m18064_MI,
	&m18065_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3253_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18062_MI,
	&m18064_MI,
	&m18063_MI,
	&m18065_MI,
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
extern TypeInfo t4645_TI;
static TypeInfo* t3253_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4645_TI,
};
static Il2CppInterfaceOffsetPair t3253_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4645_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3253_0_0_0;
extern Il2CppType t3253_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3253_GC;
extern TypeInfo t20_TI;
TypeInfo t3253_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3253_MIs, t3253_PIs, t3253_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3253_TI, t3253_ITIs, t3253_VT, &EmptyCustomAttributesCache, &t3253_TI, &t3253_0_0_0, &t3253_1_0_0, t3253_IOs, &t3253_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3253)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6000_TI;

#include "Mono.Security_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo m31291_MI;
static PropertyInfo t6000____Count_PropertyInfo = 
{
	&t6000_TI, "Count", &m31291_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31292_MI;
static PropertyInfo t6000____IsReadOnly_PropertyInfo = 
{
	&t6000_TI, "IsReadOnly", &m31292_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6000_PIs[] =
{
	&t6000____Count_PropertyInfo,
	&t6000____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31291_GM;
MethodInfo m31291_MI = 
{
	"get_Count", NULL, &t6000_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31291_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31292_GM;
MethodInfo m31292_MI = 
{
	"get_IsReadOnly", NULL, &t6000_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31292_GM};
extern Il2CppType t1022_0_0_0;
extern Il2CppType t1022_0_0_0;
static ParameterInfo t6000_m31293_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1022_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31293_GM;
MethodInfo m31293_MI = 
{
	"Add", NULL, &t6000_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6000_m31293_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31293_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31294_GM;
MethodInfo m31294_MI = 
{
	"Clear", NULL, &t6000_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31294_GM};
extern Il2CppType t1022_0_0_0;
static ParameterInfo t6000_m31295_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1022_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31295_GM;
MethodInfo m31295_MI = 
{
	"Contains", NULL, &t6000_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6000_m31295_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31295_GM};
extern Il2CppType t3935_0_0_0;
extern Il2CppType t3935_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6000_m31296_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3935_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31296_GM;
MethodInfo m31296_MI = 
{
	"CopyTo", NULL, &t6000_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6000_m31296_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31296_GM};
extern Il2CppType t1022_0_0_0;
static ParameterInfo t6000_m31297_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1022_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31297_GM;
MethodInfo m31297_MI = 
{
	"Remove", NULL, &t6000_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6000_m31297_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31297_GM};
static MethodInfo* t6000_MIs[] =
{
	&m31291_MI,
	&m31292_MI,
	&m31293_MI,
	&m31294_MI,
	&m31295_MI,
	&m31296_MI,
	&m31297_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6002_TI;
static TypeInfo* t6000_ITIs[] = 
{
	&t603_TI,
	&t6002_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6000_0_0_0;
extern Il2CppType t6000_1_0_0;
struct t6000;
extern Il2CppGenericClass t6000_GC;
TypeInfo t6000_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6000_MIs, t6000_PIs, NULL, NULL, NULL, NULL, NULL, &t6000_TI, t6000_ITIs, NULL, &EmptyCustomAttributesCache, &t6000_TI, &t6000_0_0_0, &t6000_1_0_0, NULL, &t6000_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern Il2CppType t4645_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31298_GM;
MethodInfo m31298_MI = 
{
	"GetEnumerator", NULL, &t6002_TI, &t4645_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31298_GM};
static MethodInfo* t6002_MIs[] =
{
	&m31298_MI,
	NULL
};
static TypeInfo* t6002_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6002_0_0_0;
extern Il2CppType t6002_1_0_0;
struct t6002;
extern Il2CppGenericClass t6002_GC;
TypeInfo t6002_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6002_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6002_TI, t6002_ITIs, NULL, &EmptyCustomAttributesCache, &t6002_TI, &t6002_0_0_0, &t6002_1_0_0, NULL, &t6002_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6001_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo m31299_MI;
extern MethodInfo m31300_MI;
static PropertyInfo t6001____Item_PropertyInfo = 
{
	&t6001_TI, "Item", &m31299_MI, &m31300_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6001_PIs[] =
{
	&t6001____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1022_0_0_0;
static ParameterInfo t6001_m31301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1022_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31301_GM;
MethodInfo m31301_MI = 
{
	"IndexOf", NULL, &t6001_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6001_m31301_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31301_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1022_0_0_0;
static ParameterInfo t6001_m31302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1022_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31302_GM;
MethodInfo m31302_MI = 
{
	"Insert", NULL, &t6001_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6001_m31302_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31302_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6001_m31303_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31303_GM;
MethodInfo m31303_MI = 
{
	"RemoveAt", NULL, &t6001_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6001_m31303_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31303_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6001_m31299_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1022_0_0_0;
extern void* RuntimeInvoker_t1022_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31299_GM;
MethodInfo m31299_MI = 
{
	"get_Item", NULL, &t6001_TI, &t1022_0_0_0, RuntimeInvoker_t1022_t44, t6001_m31299_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31299_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1022_0_0_0;
static ParameterInfo t6001_m31300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1022_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31300_GM;
MethodInfo m31300_MI = 
{
	"set_Item", NULL, &t6001_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6001_m31300_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31300_GM};
static MethodInfo* t6001_MIs[] =
{
	&m31301_MI,
	&m31302_MI,
	&m31303_MI,
	&m31299_MI,
	&m31300_MI,
	NULL
};
static TypeInfo* t6001_ITIs[] = 
{
	&t603_TI,
	&t6000_TI,
	&t6002_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6001_0_0_0;
extern Il2CppType t6001_1_0_0;
struct t6001;
extern Il2CppGenericClass t6001_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6001_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6001_MIs, t6001_PIs, NULL, NULL, NULL, NULL, NULL, &t6001_TI, t6001_ITIs, NULL, &t1908__CustomAttributeCache, &t6001_TI, &t6001_0_0_0, &t6001_1_0_0, NULL, &t6001_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4647_TI;

#include "t1044.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo m31304_MI;
static PropertyInfo t4647____Current_PropertyInfo = 
{
	&t4647_TI, "Current", &m31304_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4647_PIs[] =
{
	&t4647____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1044_0_0_0;
extern void* RuntimeInvoker_t1044 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31304_GM;
MethodInfo m31304_MI = 
{
	"get_Current", NULL, &t4647_TI, &t1044_0_0_0, RuntimeInvoker_t1044, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31304_GM};
static MethodInfo* t4647_MIs[] =
{
	&m31304_MI,
	NULL
};
static TypeInfo* t4647_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4647_0_0_0;
extern Il2CppType t4647_1_0_0;
struct t4647;
extern Il2CppGenericClass t4647_GC;
TypeInfo t4647_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4647_MIs, t4647_PIs, NULL, NULL, NULL, NULL, NULL, &t4647_TI, t4647_ITIs, NULL, &EmptyCustomAttributesCache, &t4647_TI, &t4647_0_0_0, &t4647_1_0_0, NULL, &t4647_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3254.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3254_TI;
#include "t3254MD.h"

extern TypeInfo t1044_TI;
extern MethodInfo m18070_MI;
extern MethodInfo m23894_MI;
struct t20;
 int32_t m23894 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18066_MI;
 void m18066 (t3254 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18067_MI;
 t29 * m18067 (t3254 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18070(__this, &m18070_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1044_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18068_MI;
 void m18068 (t3254 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18069_MI;
 bool m18069 (t3254 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18070 (t3254 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23894(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23894_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
extern Il2CppType t20_0_0_1;
FieldInfo t3254_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3254_TI, offsetof(t3254, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3254_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3254_TI, offsetof(t3254, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3254_FIs[] =
{
	&t3254_f0_FieldInfo,
	&t3254_f1_FieldInfo,
	NULL
};
static PropertyInfo t3254____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3254_TI, "System.Collections.IEnumerator.Current", &m18067_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3254____Current_PropertyInfo = 
{
	&t3254_TI, "Current", &m18070_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3254_PIs[] =
{
	&t3254____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3254____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3254_m18066_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18066_GM;
MethodInfo m18066_MI = 
{
	".ctor", (methodPointerType)&m18066, &t3254_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3254_m18066_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18066_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18067_GM;
MethodInfo m18067_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18067, &t3254_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18067_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18068_GM;
MethodInfo m18068_MI = 
{
	"Dispose", (methodPointerType)&m18068, &t3254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18068_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18069_GM;
MethodInfo m18069_MI = 
{
	"MoveNext", (methodPointerType)&m18069, &t3254_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18069_GM};
extern Il2CppType t1044_0_0_0;
extern void* RuntimeInvoker_t1044 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18070_GM;
MethodInfo m18070_MI = 
{
	"get_Current", (methodPointerType)&m18070, &t3254_TI, &t1044_0_0_0, RuntimeInvoker_t1044, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18070_GM};
static MethodInfo* t3254_MIs[] =
{
	&m18066_MI,
	&m18067_MI,
	&m18068_MI,
	&m18069_MI,
	&m18070_MI,
	NULL
};
static MethodInfo* t3254_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18067_MI,
	&m18069_MI,
	&m18068_MI,
	&m18070_MI,
};
static TypeInfo* t3254_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4647_TI,
};
static Il2CppInterfaceOffsetPair t3254_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4647_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3254_0_0_0;
extern Il2CppType t3254_1_0_0;
extern Il2CppGenericClass t3254_GC;
TypeInfo t3254_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3254_MIs, t3254_PIs, t3254_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3254_TI, t3254_ITIs, t3254_VT, &EmptyCustomAttributesCache, &t3254_TI, &t3254_0_0_0, &t3254_1_0_0, t3254_IOs, &t3254_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3254)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6003_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo m31305_MI;
static PropertyInfo t6003____Count_PropertyInfo = 
{
	&t6003_TI, "Count", &m31305_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31306_MI;
static PropertyInfo t6003____IsReadOnly_PropertyInfo = 
{
	&t6003_TI, "IsReadOnly", &m31306_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6003_PIs[] =
{
	&t6003____Count_PropertyInfo,
	&t6003____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31305_GM;
MethodInfo m31305_MI = 
{
	"get_Count", NULL, &t6003_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31305_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31306_GM;
MethodInfo m31306_MI = 
{
	"get_IsReadOnly", NULL, &t6003_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31306_GM};
extern Il2CppType t1044_0_0_0;
extern Il2CppType t1044_0_0_0;
static ParameterInfo t6003_m31307_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31307_GM;
MethodInfo m31307_MI = 
{
	"Add", NULL, &t6003_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6003_m31307_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31307_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31308_GM;
MethodInfo m31308_MI = 
{
	"Clear", NULL, &t6003_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31308_GM};
extern Il2CppType t1044_0_0_0;
static ParameterInfo t6003_m31309_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1044_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31309_GM;
MethodInfo m31309_MI = 
{
	"Contains", NULL, &t6003_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6003_m31309_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31309_GM};
extern Il2CppType t3936_0_0_0;
extern Il2CppType t3936_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6003_m31310_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3936_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31310_GM;
MethodInfo m31310_MI = 
{
	"CopyTo", NULL, &t6003_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6003_m31310_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31310_GM};
extern Il2CppType t1044_0_0_0;
static ParameterInfo t6003_m31311_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1044_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31311_GM;
MethodInfo m31311_MI = 
{
	"Remove", NULL, &t6003_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6003_m31311_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31311_GM};
static MethodInfo* t6003_MIs[] =
{
	&m31305_MI,
	&m31306_MI,
	&m31307_MI,
	&m31308_MI,
	&m31309_MI,
	&m31310_MI,
	&m31311_MI,
	NULL
};
extern TypeInfo t6005_TI;
static TypeInfo* t6003_ITIs[] = 
{
	&t603_TI,
	&t6005_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6003_0_0_0;
extern Il2CppType t6003_1_0_0;
struct t6003;
extern Il2CppGenericClass t6003_GC;
TypeInfo t6003_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6003_MIs, t6003_PIs, NULL, NULL, NULL, NULL, NULL, &t6003_TI, t6003_ITIs, NULL, &EmptyCustomAttributesCache, &t6003_TI, &t6003_0_0_0, &t6003_1_0_0, NULL, &t6003_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>
extern Il2CppType t4647_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31312_GM;
MethodInfo m31312_MI = 
{
	"GetEnumerator", NULL, &t6005_TI, &t4647_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31312_GM};
static MethodInfo* t6005_MIs[] =
{
	&m31312_MI,
	NULL
};
static TypeInfo* t6005_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6005_0_0_0;
extern Il2CppType t6005_1_0_0;
struct t6005;
extern Il2CppGenericClass t6005_GC;
TypeInfo t6005_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6005_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6005_TI, t6005_ITIs, NULL, &EmptyCustomAttributesCache, &t6005_TI, &t6005_0_0_0, &t6005_1_0_0, NULL, &t6005_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6004_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo m31313_MI;
extern MethodInfo m31314_MI;
static PropertyInfo t6004____Item_PropertyInfo = 
{
	&t6004_TI, "Item", &m31313_MI, &m31314_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6004_PIs[] =
{
	&t6004____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1044_0_0_0;
static ParameterInfo t6004_m31315_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1044_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31315_GM;
MethodInfo m31315_MI = 
{
	"IndexOf", NULL, &t6004_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6004_m31315_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31315_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1044_0_0_0;
static ParameterInfo t6004_m31316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31316_GM;
MethodInfo m31316_MI = 
{
	"Insert", NULL, &t6004_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6004_m31316_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31316_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6004_m31317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31317_GM;
MethodInfo m31317_MI = 
{
	"RemoveAt", NULL, &t6004_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6004_m31317_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31317_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6004_m31313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1044_0_0_0;
extern void* RuntimeInvoker_t1044_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31313_GM;
MethodInfo m31313_MI = 
{
	"get_Item", NULL, &t6004_TI, &t1044_0_0_0, RuntimeInvoker_t1044_t44, t6004_m31313_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31313_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1044_0_0_0;
static ParameterInfo t6004_m31314_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31314_GM;
MethodInfo m31314_MI = 
{
	"set_Item", NULL, &t6004_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6004_m31314_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31314_GM};
static MethodInfo* t6004_MIs[] =
{
	&m31315_MI,
	&m31316_MI,
	&m31317_MI,
	&m31313_MI,
	&m31314_MI,
	NULL
};
static TypeInfo* t6004_ITIs[] = 
{
	&t603_TI,
	&t6003_TI,
	&t6005_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6004_0_0_0;
extern Il2CppType t6004_1_0_0;
struct t6004;
extern Il2CppGenericClass t6004_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6004_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6004_MIs, t6004_PIs, NULL, NULL, NULL, NULL, NULL, &t6004_TI, t6004_ITIs, NULL, &t1908__CustomAttributeCache, &t6004_TI, &t6004_0_0_0, &t6004_1_0_0, NULL, &t6004_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4649_TI;

#include "t1021.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo m31318_MI;
static PropertyInfo t4649____Current_PropertyInfo = 
{
	&t4649_TI, "Current", &m31318_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4649_PIs[] =
{
	&t4649____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1021_0_0_0;
extern void* RuntimeInvoker_t1021 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31318_GM;
MethodInfo m31318_MI = 
{
	"get_Current", NULL, &t4649_TI, &t1021_0_0_0, RuntimeInvoker_t1021, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31318_GM};
static MethodInfo* t4649_MIs[] =
{
	&m31318_MI,
	NULL
};
static TypeInfo* t4649_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4649_0_0_0;
extern Il2CppType t4649_1_0_0;
struct t4649;
extern Il2CppGenericClass t4649_GC;
TypeInfo t4649_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4649_MIs, t4649_PIs, NULL, NULL, NULL, NULL, NULL, &t4649_TI, t4649_ITIs, NULL, &EmptyCustomAttributesCache, &t4649_TI, &t4649_0_0_0, &t4649_1_0_0, NULL, &t4649_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3255.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3255_TI;
#include "t3255MD.h"

extern TypeInfo t1021_TI;
extern MethodInfo m18075_MI;
extern MethodInfo m23905_MI;
struct t20;
 int32_t m23905 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18071_MI;
 void m18071 (t3255 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18072_MI;
 t29 * m18072 (t3255 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18075(__this, &m18075_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1021_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18073_MI;
 void m18073 (t3255 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18074_MI;
 bool m18074 (t3255 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18075 (t3255 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23905(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23905_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern Il2CppType t20_0_0_1;
FieldInfo t3255_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3255_TI, offsetof(t3255, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3255_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3255_TI, offsetof(t3255, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3255_FIs[] =
{
	&t3255_f0_FieldInfo,
	&t3255_f1_FieldInfo,
	NULL
};
static PropertyInfo t3255____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3255_TI, "System.Collections.IEnumerator.Current", &m18072_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3255____Current_PropertyInfo = 
{
	&t3255_TI, "Current", &m18075_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3255_PIs[] =
{
	&t3255____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3255____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3255_m18071_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18071_GM;
MethodInfo m18071_MI = 
{
	".ctor", (methodPointerType)&m18071, &t3255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3255_m18071_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18071_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18072_GM;
MethodInfo m18072_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18072, &t3255_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18072_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18073_GM;
MethodInfo m18073_MI = 
{
	"Dispose", (methodPointerType)&m18073, &t3255_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18073_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18074_GM;
MethodInfo m18074_MI = 
{
	"MoveNext", (methodPointerType)&m18074, &t3255_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18074_GM};
extern Il2CppType t1021_0_0_0;
extern void* RuntimeInvoker_t1021 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18075_GM;
MethodInfo m18075_MI = 
{
	"get_Current", (methodPointerType)&m18075, &t3255_TI, &t1021_0_0_0, RuntimeInvoker_t1021, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18075_GM};
static MethodInfo* t3255_MIs[] =
{
	&m18071_MI,
	&m18072_MI,
	&m18073_MI,
	&m18074_MI,
	&m18075_MI,
	NULL
};
static MethodInfo* t3255_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18072_MI,
	&m18074_MI,
	&m18073_MI,
	&m18075_MI,
};
static TypeInfo* t3255_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4649_TI,
};
static Il2CppInterfaceOffsetPair t3255_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4649_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3255_0_0_0;
extern Il2CppType t3255_1_0_0;
extern Il2CppGenericClass t3255_GC;
TypeInfo t3255_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3255_MIs, t3255_PIs, t3255_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3255_TI, t3255_ITIs, t3255_VT, &EmptyCustomAttributesCache, &t3255_TI, &t3255_0_0_0, &t3255_1_0_0, t3255_IOs, &t3255_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3255)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6006_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo m31319_MI;
static PropertyInfo t6006____Count_PropertyInfo = 
{
	&t6006_TI, "Count", &m31319_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31320_MI;
static PropertyInfo t6006____IsReadOnly_PropertyInfo = 
{
	&t6006_TI, "IsReadOnly", &m31320_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6006_PIs[] =
{
	&t6006____Count_PropertyInfo,
	&t6006____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31319_GM;
MethodInfo m31319_MI = 
{
	"get_Count", NULL, &t6006_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31319_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31320_GM;
MethodInfo m31320_MI = 
{
	"get_IsReadOnly", NULL, &t6006_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31320_GM};
extern Il2CppType t1021_0_0_0;
extern Il2CppType t1021_0_0_0;
static ParameterInfo t6006_m31321_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1021_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31321_GM;
MethodInfo m31321_MI = 
{
	"Add", NULL, &t6006_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6006_m31321_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31321_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31322_GM;
MethodInfo m31322_MI = 
{
	"Clear", NULL, &t6006_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31322_GM};
extern Il2CppType t1021_0_0_0;
static ParameterInfo t6006_m31323_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1021_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31323_GM;
MethodInfo m31323_MI = 
{
	"Contains", NULL, &t6006_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6006_m31323_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31323_GM};
extern Il2CppType t3937_0_0_0;
extern Il2CppType t3937_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6006_m31324_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3937_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31324_GM;
MethodInfo m31324_MI = 
{
	"CopyTo", NULL, &t6006_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6006_m31324_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31324_GM};
extern Il2CppType t1021_0_0_0;
static ParameterInfo t6006_m31325_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1021_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31325_GM;
MethodInfo m31325_MI = 
{
	"Remove", NULL, &t6006_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6006_m31325_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31325_GM};
static MethodInfo* t6006_MIs[] =
{
	&m31319_MI,
	&m31320_MI,
	&m31321_MI,
	&m31322_MI,
	&m31323_MI,
	&m31324_MI,
	&m31325_MI,
	NULL
};
extern TypeInfo t6008_TI;
static TypeInfo* t6006_ITIs[] = 
{
	&t603_TI,
	&t6008_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6006_0_0_0;
extern Il2CppType t6006_1_0_0;
struct t6006;
extern Il2CppGenericClass t6006_GC;
TypeInfo t6006_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6006_MIs, t6006_PIs, NULL, NULL, NULL, NULL, NULL, &t6006_TI, t6006_ITIs, NULL, &EmptyCustomAttributesCache, &t6006_TI, &t6006_0_0_0, &t6006_1_0_0, NULL, &t6006_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern Il2CppType t4649_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31326_GM;
MethodInfo m31326_MI = 
{
	"GetEnumerator", NULL, &t6008_TI, &t4649_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31326_GM};
static MethodInfo* t6008_MIs[] =
{
	&m31326_MI,
	NULL
};
static TypeInfo* t6008_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6008_0_0_0;
extern Il2CppType t6008_1_0_0;
struct t6008;
extern Il2CppGenericClass t6008_GC;
TypeInfo t6008_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6008_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6008_TI, t6008_ITIs, NULL, &EmptyCustomAttributesCache, &t6008_TI, &t6008_0_0_0, &t6008_1_0_0, NULL, &t6008_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6007_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo m31327_MI;
extern MethodInfo m31328_MI;
static PropertyInfo t6007____Item_PropertyInfo = 
{
	&t6007_TI, "Item", &m31327_MI, &m31328_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6007_PIs[] =
{
	&t6007____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1021_0_0_0;
static ParameterInfo t6007_m31329_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1021_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31329_GM;
MethodInfo m31329_MI = 
{
	"IndexOf", NULL, &t6007_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6007_m31329_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31329_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1021_0_0_0;
static ParameterInfo t6007_m31330_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1021_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31330_GM;
MethodInfo m31330_MI = 
{
	"Insert", NULL, &t6007_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6007_m31330_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31330_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6007_m31331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31331_GM;
MethodInfo m31331_MI = 
{
	"RemoveAt", NULL, &t6007_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6007_m31331_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31331_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6007_m31327_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1021_0_0_0;
extern void* RuntimeInvoker_t1021_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31327_GM;
MethodInfo m31327_MI = 
{
	"get_Item", NULL, &t6007_TI, &t1021_0_0_0, RuntimeInvoker_t1021_t44, t6007_m31327_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31327_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1021_0_0_0;
static ParameterInfo t6007_m31328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1021_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31328_GM;
MethodInfo m31328_MI = 
{
	"set_Item", NULL, &t6007_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6007_m31328_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31328_GM};
static MethodInfo* t6007_MIs[] =
{
	&m31329_MI,
	&m31330_MI,
	&m31331_MI,
	&m31327_MI,
	&m31328_MI,
	NULL
};
static TypeInfo* t6007_ITIs[] = 
{
	&t603_TI,
	&t6006_TI,
	&t6008_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6007_0_0_0;
extern Il2CppType t6007_1_0_0;
struct t6007;
extern Il2CppGenericClass t6007_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6007_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6007_MIs, t6007_PIs, NULL, NULL, NULL, NULL, NULL, &t6007_TI, t6007_ITIs, NULL, &t1908__CustomAttributeCache, &t6007_TI, &t6007_0_0_0, &t6007_1_0_0, NULL, &t6007_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4651_TI;

#include "t1043.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo m31332_MI;
static PropertyInfo t4651____Current_PropertyInfo = 
{
	&t4651_TI, "Current", &m31332_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4651_PIs[] =
{
	&t4651____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1043_0_0_0;
extern void* RuntimeInvoker_t1043 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31332_GM;
MethodInfo m31332_MI = 
{
	"get_Current", NULL, &t4651_TI, &t1043_0_0_0, RuntimeInvoker_t1043, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31332_GM};
static MethodInfo* t4651_MIs[] =
{
	&m31332_MI,
	NULL
};
static TypeInfo* t4651_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4651_0_0_0;
extern Il2CppType t4651_1_0_0;
struct t4651;
extern Il2CppGenericClass t4651_GC;
TypeInfo t4651_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4651_MIs, t4651_PIs, NULL, NULL, NULL, NULL, NULL, &t4651_TI, t4651_ITIs, NULL, &EmptyCustomAttributesCache, &t4651_TI, &t4651_0_0_0, &t4651_1_0_0, NULL, &t4651_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3256.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3256_TI;
#include "t3256MD.h"

extern TypeInfo t1043_TI;
extern MethodInfo m18080_MI;
extern MethodInfo m23916_MI;
struct t20;
 int32_t m23916 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18076_MI;
 void m18076 (t3256 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18077_MI;
 t29 * m18077 (t3256 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18080(__this, &m18080_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1043_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18078_MI;
 void m18078 (t3256 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18079_MI;
 bool m18079 (t3256 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18080 (t3256 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23916(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23916_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern Il2CppType t20_0_0_1;
FieldInfo t3256_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3256_TI, offsetof(t3256, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3256_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3256_TI, offsetof(t3256, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3256_FIs[] =
{
	&t3256_f0_FieldInfo,
	&t3256_f1_FieldInfo,
	NULL
};
static PropertyInfo t3256____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3256_TI, "System.Collections.IEnumerator.Current", &m18077_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3256____Current_PropertyInfo = 
{
	&t3256_TI, "Current", &m18080_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3256_PIs[] =
{
	&t3256____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3256____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3256_m18076_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18076_GM;
MethodInfo m18076_MI = 
{
	".ctor", (methodPointerType)&m18076, &t3256_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3256_m18076_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18076_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18077_GM;
MethodInfo m18077_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18077, &t3256_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18077_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18078_GM;
MethodInfo m18078_MI = 
{
	"Dispose", (methodPointerType)&m18078, &t3256_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18078_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18079_GM;
MethodInfo m18079_MI = 
{
	"MoveNext", (methodPointerType)&m18079, &t3256_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18079_GM};
extern Il2CppType t1043_0_0_0;
extern void* RuntimeInvoker_t1043 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18080_GM;
MethodInfo m18080_MI = 
{
	"get_Current", (methodPointerType)&m18080, &t3256_TI, &t1043_0_0_0, RuntimeInvoker_t1043, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18080_GM};
static MethodInfo* t3256_MIs[] =
{
	&m18076_MI,
	&m18077_MI,
	&m18078_MI,
	&m18079_MI,
	&m18080_MI,
	NULL
};
static MethodInfo* t3256_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18077_MI,
	&m18079_MI,
	&m18078_MI,
	&m18080_MI,
};
static TypeInfo* t3256_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4651_TI,
};
static Il2CppInterfaceOffsetPair t3256_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4651_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3256_0_0_0;
extern Il2CppType t3256_1_0_0;
extern Il2CppGenericClass t3256_GC;
TypeInfo t3256_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3256_MIs, t3256_PIs, t3256_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3256_TI, t3256_ITIs, t3256_VT, &EmptyCustomAttributesCache, &t3256_TI, &t3256_0_0_0, &t3256_1_0_0, t3256_IOs, &t3256_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3256)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6009_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo m31333_MI;
static PropertyInfo t6009____Count_PropertyInfo = 
{
	&t6009_TI, "Count", &m31333_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31334_MI;
static PropertyInfo t6009____IsReadOnly_PropertyInfo = 
{
	&t6009_TI, "IsReadOnly", &m31334_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6009_PIs[] =
{
	&t6009____Count_PropertyInfo,
	&t6009____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31333_GM;
MethodInfo m31333_MI = 
{
	"get_Count", NULL, &t6009_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31333_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31334_GM;
MethodInfo m31334_MI = 
{
	"get_IsReadOnly", NULL, &t6009_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31334_GM};
extern Il2CppType t1043_0_0_0;
extern Il2CppType t1043_0_0_0;
static ParameterInfo t6009_m31335_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1043_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31335_GM;
MethodInfo m31335_MI = 
{
	"Add", NULL, &t6009_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6009_m31335_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31335_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31336_GM;
MethodInfo m31336_MI = 
{
	"Clear", NULL, &t6009_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31336_GM};
extern Il2CppType t1043_0_0_0;
static ParameterInfo t6009_m31337_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1043_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31337_GM;
MethodInfo m31337_MI = 
{
	"Contains", NULL, &t6009_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6009_m31337_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31337_GM};
extern Il2CppType t3938_0_0_0;
extern Il2CppType t3938_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6009_m31338_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3938_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31338_GM;
MethodInfo m31338_MI = 
{
	"CopyTo", NULL, &t6009_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6009_m31338_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31338_GM};
extern Il2CppType t1043_0_0_0;
static ParameterInfo t6009_m31339_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1043_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31339_GM;
MethodInfo m31339_MI = 
{
	"Remove", NULL, &t6009_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6009_m31339_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31339_GM};
static MethodInfo* t6009_MIs[] =
{
	&m31333_MI,
	&m31334_MI,
	&m31335_MI,
	&m31336_MI,
	&m31337_MI,
	&m31338_MI,
	&m31339_MI,
	NULL
};
extern TypeInfo t6011_TI;
static TypeInfo* t6009_ITIs[] = 
{
	&t603_TI,
	&t6011_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6009_0_0_0;
extern Il2CppType t6009_1_0_0;
struct t6009;
extern Il2CppGenericClass t6009_GC;
TypeInfo t6009_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6009_MIs, t6009_PIs, NULL, NULL, NULL, NULL, NULL, &t6009_TI, t6009_ITIs, NULL, &EmptyCustomAttributesCache, &t6009_TI, &t6009_0_0_0, &t6009_1_0_0, NULL, &t6009_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern Il2CppType t4651_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31340_GM;
MethodInfo m31340_MI = 
{
	"GetEnumerator", NULL, &t6011_TI, &t4651_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31340_GM};
static MethodInfo* t6011_MIs[] =
{
	&m31340_MI,
	NULL
};
static TypeInfo* t6011_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6011_0_0_0;
extern Il2CppType t6011_1_0_0;
struct t6011;
extern Il2CppGenericClass t6011_GC;
TypeInfo t6011_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6011_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6011_TI, t6011_ITIs, NULL, &EmptyCustomAttributesCache, &t6011_TI, &t6011_0_0_0, &t6011_1_0_0, NULL, &t6011_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6010_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo m31341_MI;
extern MethodInfo m31342_MI;
static PropertyInfo t6010____Item_PropertyInfo = 
{
	&t6010_TI, "Item", &m31341_MI, &m31342_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6010_PIs[] =
{
	&t6010____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1043_0_0_0;
static ParameterInfo t6010_m31343_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1043_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31343_GM;
MethodInfo m31343_MI = 
{
	"IndexOf", NULL, &t6010_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6010_m31343_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31343_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1043_0_0_0;
static ParameterInfo t6010_m31344_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1043_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31344_GM;
MethodInfo m31344_MI = 
{
	"Insert", NULL, &t6010_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6010_m31344_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31344_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6010_m31345_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31345_GM;
MethodInfo m31345_MI = 
{
	"RemoveAt", NULL, &t6010_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6010_m31345_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31345_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6010_m31341_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1043_0_0_0;
extern void* RuntimeInvoker_t1043_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31341_GM;
MethodInfo m31341_MI = 
{
	"get_Item", NULL, &t6010_TI, &t1043_0_0_0, RuntimeInvoker_t1043_t44, t6010_m31341_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31341_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1043_0_0_0;
static ParameterInfo t6010_m31342_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1043_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31342_GM;
MethodInfo m31342_MI = 
{
	"set_Item", NULL, &t6010_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6010_m31342_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31342_GM};
static MethodInfo* t6010_MIs[] =
{
	&m31343_MI,
	&m31344_MI,
	&m31345_MI,
	&m31341_MI,
	&m31342_MI,
	NULL
};
static TypeInfo* t6010_ITIs[] = 
{
	&t603_TI,
	&t6009_TI,
	&t6011_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6010_0_0_0;
extern Il2CppType t6010_1_0_0;
struct t6010;
extern Il2CppGenericClass t6010_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6010_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6010_MIs, t6010_PIs, NULL, NULL, NULL, NULL, NULL, &t6010_TI, t6010_ITIs, NULL, &t1908__CustomAttributeCache, &t6010_TI, &t6010_0_0_0, &t6010_1_0_0, NULL, &t6010_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4653_TI;

#include "t1026.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo m31346_MI;
static PropertyInfo t4653____Current_PropertyInfo = 
{
	&t4653_TI, "Current", &m31346_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4653_PIs[] =
{
	&t4653____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1026_0_0_0;
extern void* RuntimeInvoker_t1026 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31346_GM;
MethodInfo m31346_MI = 
{
	"get_Current", NULL, &t4653_TI, &t1026_0_0_0, RuntimeInvoker_t1026, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31346_GM};
static MethodInfo* t4653_MIs[] =
{
	&m31346_MI,
	NULL
};
static TypeInfo* t4653_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4653_0_0_0;
extern Il2CppType t4653_1_0_0;
struct t4653;
extern Il2CppGenericClass t4653_GC;
TypeInfo t4653_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4653_MIs, t4653_PIs, NULL, NULL, NULL, NULL, NULL, &t4653_TI, t4653_ITIs, NULL, &EmptyCustomAttributesCache, &t4653_TI, &t4653_0_0_0, &t4653_1_0_0, NULL, &t4653_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3257.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3257_TI;
#include "t3257MD.h"

extern TypeInfo t1026_TI;
extern MethodInfo m18085_MI;
extern MethodInfo m23927_MI;
struct t20;
 int32_t m23927 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18081_MI;
 void m18081 (t3257 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18082_MI;
 t29 * m18082 (t3257 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18085(__this, &m18085_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1026_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18083_MI;
 void m18083 (t3257 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18084_MI;
 bool m18084 (t3257 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18085 (t3257 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23927(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23927_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern Il2CppType t20_0_0_1;
FieldInfo t3257_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3257_TI, offsetof(t3257, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3257_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3257_TI, offsetof(t3257, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3257_FIs[] =
{
	&t3257_f0_FieldInfo,
	&t3257_f1_FieldInfo,
	NULL
};
static PropertyInfo t3257____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3257_TI, "System.Collections.IEnumerator.Current", &m18082_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3257____Current_PropertyInfo = 
{
	&t3257_TI, "Current", &m18085_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3257_PIs[] =
{
	&t3257____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3257____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3257_m18081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18081_GM;
MethodInfo m18081_MI = 
{
	".ctor", (methodPointerType)&m18081, &t3257_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3257_m18081_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18081_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18082_GM;
MethodInfo m18082_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18082, &t3257_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18082_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18083_GM;
MethodInfo m18083_MI = 
{
	"Dispose", (methodPointerType)&m18083, &t3257_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18083_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18084_GM;
MethodInfo m18084_MI = 
{
	"MoveNext", (methodPointerType)&m18084, &t3257_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18084_GM};
extern Il2CppType t1026_0_0_0;
extern void* RuntimeInvoker_t1026 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18085_GM;
MethodInfo m18085_MI = 
{
	"get_Current", (methodPointerType)&m18085, &t3257_TI, &t1026_0_0_0, RuntimeInvoker_t1026, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18085_GM};
static MethodInfo* t3257_MIs[] =
{
	&m18081_MI,
	&m18082_MI,
	&m18083_MI,
	&m18084_MI,
	&m18085_MI,
	NULL
};
static MethodInfo* t3257_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18082_MI,
	&m18084_MI,
	&m18083_MI,
	&m18085_MI,
};
static TypeInfo* t3257_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4653_TI,
};
static Il2CppInterfaceOffsetPair t3257_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4653_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3257_0_0_0;
extern Il2CppType t3257_1_0_0;
extern Il2CppGenericClass t3257_GC;
TypeInfo t3257_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3257_MIs, t3257_PIs, t3257_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3257_TI, t3257_ITIs, t3257_VT, &EmptyCustomAttributesCache, &t3257_TI, &t3257_0_0_0, &t3257_1_0_0, t3257_IOs, &t3257_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3257)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6012_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo m31347_MI;
static PropertyInfo t6012____Count_PropertyInfo = 
{
	&t6012_TI, "Count", &m31347_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31348_MI;
static PropertyInfo t6012____IsReadOnly_PropertyInfo = 
{
	&t6012_TI, "IsReadOnly", &m31348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6012_PIs[] =
{
	&t6012____Count_PropertyInfo,
	&t6012____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31347_GM;
MethodInfo m31347_MI = 
{
	"get_Count", NULL, &t6012_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31347_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31348_GM;
MethodInfo m31348_MI = 
{
	"get_IsReadOnly", NULL, &t6012_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31348_GM};
extern Il2CppType t1026_0_0_0;
extern Il2CppType t1026_0_0_0;
static ParameterInfo t6012_m31349_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1026_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31349_GM;
MethodInfo m31349_MI = 
{
	"Add", NULL, &t6012_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6012_m31349_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31349_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31350_GM;
MethodInfo m31350_MI = 
{
	"Clear", NULL, &t6012_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31350_GM};
extern Il2CppType t1026_0_0_0;
static ParameterInfo t6012_m31351_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1026_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31351_GM;
MethodInfo m31351_MI = 
{
	"Contains", NULL, &t6012_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6012_m31351_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31351_GM};
extern Il2CppType t3939_0_0_0;
extern Il2CppType t3939_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6012_m31352_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3939_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31352_GM;
MethodInfo m31352_MI = 
{
	"CopyTo", NULL, &t6012_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6012_m31352_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31352_GM};
extern Il2CppType t1026_0_0_0;
static ParameterInfo t6012_m31353_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1026_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31353_GM;
MethodInfo m31353_MI = 
{
	"Remove", NULL, &t6012_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6012_m31353_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31353_GM};
static MethodInfo* t6012_MIs[] =
{
	&m31347_MI,
	&m31348_MI,
	&m31349_MI,
	&m31350_MI,
	&m31351_MI,
	&m31352_MI,
	&m31353_MI,
	NULL
};
extern TypeInfo t6014_TI;
static TypeInfo* t6012_ITIs[] = 
{
	&t603_TI,
	&t6014_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6012_0_0_0;
extern Il2CppType t6012_1_0_0;
struct t6012;
extern Il2CppGenericClass t6012_GC;
TypeInfo t6012_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6012_MIs, t6012_PIs, NULL, NULL, NULL, NULL, NULL, &t6012_TI, t6012_ITIs, NULL, &EmptyCustomAttributesCache, &t6012_TI, &t6012_0_0_0, &t6012_1_0_0, NULL, &t6012_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern Il2CppType t4653_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31354_GM;
MethodInfo m31354_MI = 
{
	"GetEnumerator", NULL, &t6014_TI, &t4653_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31354_GM};
static MethodInfo* t6014_MIs[] =
{
	&m31354_MI,
	NULL
};
static TypeInfo* t6014_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6014_0_0_0;
extern Il2CppType t6014_1_0_0;
struct t6014;
extern Il2CppGenericClass t6014_GC;
TypeInfo t6014_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6014_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6014_TI, t6014_ITIs, NULL, &EmptyCustomAttributesCache, &t6014_TI, &t6014_0_0_0, &t6014_1_0_0, NULL, &t6014_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6013_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo m31355_MI;
extern MethodInfo m31356_MI;
static PropertyInfo t6013____Item_PropertyInfo = 
{
	&t6013_TI, "Item", &m31355_MI, &m31356_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6013_PIs[] =
{
	&t6013____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1026_0_0_0;
static ParameterInfo t6013_m31357_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1026_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31357_GM;
MethodInfo m31357_MI = 
{
	"IndexOf", NULL, &t6013_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6013_m31357_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31357_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1026_0_0_0;
static ParameterInfo t6013_m31358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1026_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31358_GM;
MethodInfo m31358_MI = 
{
	"Insert", NULL, &t6013_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6013_m31358_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31358_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6013_m31359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31359_GM;
MethodInfo m31359_MI = 
{
	"RemoveAt", NULL, &t6013_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6013_m31359_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31359_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6013_m31355_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1026_0_0_0;
extern void* RuntimeInvoker_t1026_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31355_GM;
MethodInfo m31355_MI = 
{
	"get_Item", NULL, &t6013_TI, &t1026_0_0_0, RuntimeInvoker_t1026_t44, t6013_m31355_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31355_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1026_0_0_0;
static ParameterInfo t6013_m31356_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1026_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31356_GM;
MethodInfo m31356_MI = 
{
	"set_Item", NULL, &t6013_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6013_m31356_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31356_GM};
static MethodInfo* t6013_MIs[] =
{
	&m31357_MI,
	&m31358_MI,
	&m31359_MI,
	&m31355_MI,
	&m31356_MI,
	NULL
};
static TypeInfo* t6013_ITIs[] = 
{
	&t603_TI,
	&t6012_TI,
	&t6014_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6013_0_0_0;
extern Il2CppType t6013_1_0_0;
struct t6013;
extern Il2CppGenericClass t6013_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6013_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6013_MIs, t6013_PIs, NULL, NULL, NULL, NULL, NULL, &t6013_TI, t6013_ITIs, NULL, &t1908__CustomAttributeCache, &t6013_TI, &t6013_0_0_0, &t6013_1_0_0, NULL, &t6013_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4655_TI;

#include "t1067.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo m31360_MI;
static PropertyInfo t4655____Current_PropertyInfo = 
{
	&t4655_TI, "Current", &m31360_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4655_PIs[] =
{
	&t4655____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1067_0_0_0;
extern void* RuntimeInvoker_t1067 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31360_GM;
MethodInfo m31360_MI = 
{
	"get_Current", NULL, &t4655_TI, &t1067_0_0_0, RuntimeInvoker_t1067, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31360_GM};
static MethodInfo* t4655_MIs[] =
{
	&m31360_MI,
	NULL
};
static TypeInfo* t4655_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4655_0_0_0;
extern Il2CppType t4655_1_0_0;
struct t4655;
extern Il2CppGenericClass t4655_GC;
TypeInfo t4655_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4655_MIs, t4655_PIs, NULL, NULL, NULL, NULL, NULL, &t4655_TI, t4655_ITIs, NULL, &EmptyCustomAttributesCache, &t4655_TI, &t4655_0_0_0, &t4655_1_0_0, NULL, &t4655_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3258.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3258_TI;
#include "t3258MD.h"

extern TypeInfo t1067_TI;
extern MethodInfo m18090_MI;
extern MethodInfo m23938_MI;
struct t20;
 int32_t m23938 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18086_MI;
 void m18086 (t3258 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18087_MI;
 t29 * m18087 (t3258 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18090(__this, &m18090_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1067_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18088_MI;
 void m18088 (t3258 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18089_MI;
 bool m18089 (t3258 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18090 (t3258 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23938(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23938_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern Il2CppType t20_0_0_1;
FieldInfo t3258_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3258_TI, offsetof(t3258, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3258_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3258_TI, offsetof(t3258, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3258_FIs[] =
{
	&t3258_f0_FieldInfo,
	&t3258_f1_FieldInfo,
	NULL
};
static PropertyInfo t3258____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3258_TI, "System.Collections.IEnumerator.Current", &m18087_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3258____Current_PropertyInfo = 
{
	&t3258_TI, "Current", &m18090_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3258_PIs[] =
{
	&t3258____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3258____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3258_m18086_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18086_GM;
MethodInfo m18086_MI = 
{
	".ctor", (methodPointerType)&m18086, &t3258_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3258_m18086_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18086_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18087_GM;
MethodInfo m18087_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18087, &t3258_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18087_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18088_GM;
MethodInfo m18088_MI = 
{
	"Dispose", (methodPointerType)&m18088, &t3258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18088_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18089_GM;
MethodInfo m18089_MI = 
{
	"MoveNext", (methodPointerType)&m18089, &t3258_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18089_GM};
extern Il2CppType t1067_0_0_0;
extern void* RuntimeInvoker_t1067 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18090_GM;
MethodInfo m18090_MI = 
{
	"get_Current", (methodPointerType)&m18090, &t3258_TI, &t1067_0_0_0, RuntimeInvoker_t1067, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18090_GM};
static MethodInfo* t3258_MIs[] =
{
	&m18086_MI,
	&m18087_MI,
	&m18088_MI,
	&m18089_MI,
	&m18090_MI,
	NULL
};
static MethodInfo* t3258_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18087_MI,
	&m18089_MI,
	&m18088_MI,
	&m18090_MI,
};
static TypeInfo* t3258_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4655_TI,
};
static Il2CppInterfaceOffsetPair t3258_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4655_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3258_0_0_0;
extern Il2CppType t3258_1_0_0;
extern Il2CppGenericClass t3258_GC;
TypeInfo t3258_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3258_MIs, t3258_PIs, t3258_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3258_TI, t3258_ITIs, t3258_VT, &EmptyCustomAttributesCache, &t3258_TI, &t3258_0_0_0, &t3258_1_0_0, t3258_IOs, &t3258_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3258)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6015_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo m31361_MI;
static PropertyInfo t6015____Count_PropertyInfo = 
{
	&t6015_TI, "Count", &m31361_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31362_MI;
static PropertyInfo t6015____IsReadOnly_PropertyInfo = 
{
	&t6015_TI, "IsReadOnly", &m31362_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6015_PIs[] =
{
	&t6015____Count_PropertyInfo,
	&t6015____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31361_GM;
MethodInfo m31361_MI = 
{
	"get_Count", NULL, &t6015_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31361_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31362_GM;
MethodInfo m31362_MI = 
{
	"get_IsReadOnly", NULL, &t6015_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31362_GM};
extern Il2CppType t1067_0_0_0;
extern Il2CppType t1067_0_0_0;
static ParameterInfo t6015_m31363_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1067_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31363_GM;
MethodInfo m31363_MI = 
{
	"Add", NULL, &t6015_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6015_m31363_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31363_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31364_GM;
MethodInfo m31364_MI = 
{
	"Clear", NULL, &t6015_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31364_GM};
extern Il2CppType t1067_0_0_0;
static ParameterInfo t6015_m31365_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1067_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31365_GM;
MethodInfo m31365_MI = 
{
	"Contains", NULL, &t6015_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6015_m31365_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31365_GM};
extern Il2CppType t1066_0_0_0;
extern Il2CppType t1066_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6015_m31366_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1066_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31366_GM;
MethodInfo m31366_MI = 
{
	"CopyTo", NULL, &t6015_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6015_m31366_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31366_GM};
extern Il2CppType t1067_0_0_0;
static ParameterInfo t6015_m31367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1067_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31367_GM;
MethodInfo m31367_MI = 
{
	"Remove", NULL, &t6015_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6015_m31367_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31367_GM};
static MethodInfo* t6015_MIs[] =
{
	&m31361_MI,
	&m31362_MI,
	&m31363_MI,
	&m31364_MI,
	&m31365_MI,
	&m31366_MI,
	&m31367_MI,
	NULL
};
extern TypeInfo t6017_TI;
static TypeInfo* t6015_ITIs[] = 
{
	&t603_TI,
	&t6017_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6015_0_0_0;
extern Il2CppType t6015_1_0_0;
struct t6015;
extern Il2CppGenericClass t6015_GC;
TypeInfo t6015_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6015_MIs, t6015_PIs, NULL, NULL, NULL, NULL, NULL, &t6015_TI, t6015_ITIs, NULL, &EmptyCustomAttributesCache, &t6015_TI, &t6015_0_0_0, &t6015_1_0_0, NULL, &t6015_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern Il2CppType t4655_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31368_GM;
MethodInfo m31368_MI = 
{
	"GetEnumerator", NULL, &t6017_TI, &t4655_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31368_GM};
static MethodInfo* t6017_MIs[] =
{
	&m31368_MI,
	NULL
};
static TypeInfo* t6017_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6017_0_0_0;
extern Il2CppType t6017_1_0_0;
struct t6017;
extern Il2CppGenericClass t6017_GC;
TypeInfo t6017_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6017_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6017_TI, t6017_ITIs, NULL, &EmptyCustomAttributesCache, &t6017_TI, &t6017_0_0_0, &t6017_1_0_0, NULL, &t6017_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6016_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo m31369_MI;
extern MethodInfo m31370_MI;
static PropertyInfo t6016____Item_PropertyInfo = 
{
	&t6016_TI, "Item", &m31369_MI, &m31370_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6016_PIs[] =
{
	&t6016____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1067_0_0_0;
static ParameterInfo t6016_m31371_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1067_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31371_GM;
MethodInfo m31371_MI = 
{
	"IndexOf", NULL, &t6016_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6016_m31371_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31371_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1067_0_0_0;
static ParameterInfo t6016_m31372_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1067_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31372_GM;
MethodInfo m31372_MI = 
{
	"Insert", NULL, &t6016_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6016_m31372_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31372_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6016_m31373_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31373_GM;
MethodInfo m31373_MI = 
{
	"RemoveAt", NULL, &t6016_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6016_m31373_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31373_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6016_m31369_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1067_0_0_0;
extern void* RuntimeInvoker_t1067_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31369_GM;
MethodInfo m31369_MI = 
{
	"get_Item", NULL, &t6016_TI, &t1067_0_0_0, RuntimeInvoker_t1067_t44, t6016_m31369_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31369_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1067_0_0_0;
static ParameterInfo t6016_m31370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1067_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31370_GM;
MethodInfo m31370_MI = 
{
	"set_Item", NULL, &t6016_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6016_m31370_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31370_GM};
static MethodInfo* t6016_MIs[] =
{
	&m31371_MI,
	&m31372_MI,
	&m31373_MI,
	&m31369_MI,
	&m31370_MI,
	NULL
};
static TypeInfo* t6016_ITIs[] = 
{
	&t603_TI,
	&t6015_TI,
	&t6017_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6016_0_0_0;
extern Il2CppType t6016_1_0_0;
struct t6016;
extern Il2CppGenericClass t6016_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6016_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6016_MIs, t6016_PIs, NULL, NULL, NULL, NULL, NULL, &t6016_TI, t6016_ITIs, NULL, &t1908__CustomAttributeCache, &t6016_TI, &t6016_0_0_0, &t6016_1_0_0, NULL, &t6016_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4657_TI;

#include "t1037.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo m31374_MI;
static PropertyInfo t4657____Current_PropertyInfo = 
{
	&t4657_TI, "Current", &m31374_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4657_PIs[] =
{
	&t4657____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1037_0_0_0;
extern void* RuntimeInvoker_t1037 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31374_GM;
MethodInfo m31374_MI = 
{
	"get_Current", NULL, &t4657_TI, &t1037_0_0_0, RuntimeInvoker_t1037, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31374_GM};
static MethodInfo* t4657_MIs[] =
{
	&m31374_MI,
	NULL
};
static TypeInfo* t4657_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4657_0_0_0;
extern Il2CppType t4657_1_0_0;
struct t4657;
extern Il2CppGenericClass t4657_GC;
TypeInfo t4657_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4657_MIs, t4657_PIs, NULL, NULL, NULL, NULL, NULL, &t4657_TI, t4657_ITIs, NULL, &EmptyCustomAttributesCache, &t4657_TI, &t4657_0_0_0, &t4657_1_0_0, NULL, &t4657_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3259.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3259_TI;
#include "t3259MD.h"

extern TypeInfo t1037_TI;
extern MethodInfo m18095_MI;
extern MethodInfo m23949_MI;
struct t20;
 uint8_t m23949 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18091_MI;
 void m18091 (t3259 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18092_MI;
 t29 * m18092 (t3259 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m18095(__this, &m18095_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1037_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18093_MI;
 void m18093 (t3259 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18094_MI;
 bool m18094 (t3259 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m18095 (t3259 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m23949(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23949_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern Il2CppType t20_0_0_1;
FieldInfo t3259_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3259_TI, offsetof(t3259, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3259_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3259_TI, offsetof(t3259, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3259_FIs[] =
{
	&t3259_f0_FieldInfo,
	&t3259_f1_FieldInfo,
	NULL
};
static PropertyInfo t3259____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3259_TI, "System.Collections.IEnumerator.Current", &m18092_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3259____Current_PropertyInfo = 
{
	&t3259_TI, "Current", &m18095_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3259_PIs[] =
{
	&t3259____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3259____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3259_m18091_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18091_GM;
MethodInfo m18091_MI = 
{
	".ctor", (methodPointerType)&m18091, &t3259_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3259_m18091_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18091_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18092_GM;
MethodInfo m18092_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18092, &t3259_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18092_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18093_GM;
MethodInfo m18093_MI = 
{
	"Dispose", (methodPointerType)&m18093, &t3259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18093_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18094_GM;
MethodInfo m18094_MI = 
{
	"MoveNext", (methodPointerType)&m18094, &t3259_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18094_GM};
extern Il2CppType t1037_0_0_0;
extern void* RuntimeInvoker_t1037 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18095_GM;
MethodInfo m18095_MI = 
{
	"get_Current", (methodPointerType)&m18095, &t3259_TI, &t1037_0_0_0, RuntimeInvoker_t1037, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18095_GM};
static MethodInfo* t3259_MIs[] =
{
	&m18091_MI,
	&m18092_MI,
	&m18093_MI,
	&m18094_MI,
	&m18095_MI,
	NULL
};
static MethodInfo* t3259_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18092_MI,
	&m18094_MI,
	&m18093_MI,
	&m18095_MI,
};
static TypeInfo* t3259_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4657_TI,
};
static Il2CppInterfaceOffsetPair t3259_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4657_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3259_0_0_0;
extern Il2CppType t3259_1_0_0;
extern Il2CppGenericClass t3259_GC;
TypeInfo t3259_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3259_MIs, t3259_PIs, t3259_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3259_TI, t3259_ITIs, t3259_VT, &EmptyCustomAttributesCache, &t3259_TI, &t3259_0_0_0, &t3259_1_0_0, t3259_IOs, &t3259_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3259)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6018_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo m31375_MI;
static PropertyInfo t6018____Count_PropertyInfo = 
{
	&t6018_TI, "Count", &m31375_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31376_MI;
static PropertyInfo t6018____IsReadOnly_PropertyInfo = 
{
	&t6018_TI, "IsReadOnly", &m31376_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6018_PIs[] =
{
	&t6018____Count_PropertyInfo,
	&t6018____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31375_GM;
MethodInfo m31375_MI = 
{
	"get_Count", NULL, &t6018_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31375_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31376_GM;
MethodInfo m31376_MI = 
{
	"get_IsReadOnly", NULL, &t6018_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31376_GM};
extern Il2CppType t1037_0_0_0;
extern Il2CppType t1037_0_0_0;
static ParameterInfo t6018_m31377_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1037_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31377_GM;
MethodInfo m31377_MI = 
{
	"Add", NULL, &t6018_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6018_m31377_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31377_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31378_GM;
MethodInfo m31378_MI = 
{
	"Clear", NULL, &t6018_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31378_GM};
extern Il2CppType t1037_0_0_0;
static ParameterInfo t6018_m31379_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1037_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31379_GM;
MethodInfo m31379_MI = 
{
	"Contains", NULL, &t6018_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6018_m31379_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31379_GM};
extern Il2CppType t3940_0_0_0;
extern Il2CppType t3940_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6018_m31380_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3940_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31380_GM;
MethodInfo m31380_MI = 
{
	"CopyTo", NULL, &t6018_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6018_m31380_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31380_GM};
extern Il2CppType t1037_0_0_0;
static ParameterInfo t6018_m31381_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1037_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31381_GM;
MethodInfo m31381_MI = 
{
	"Remove", NULL, &t6018_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6018_m31381_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31381_GM};
static MethodInfo* t6018_MIs[] =
{
	&m31375_MI,
	&m31376_MI,
	&m31377_MI,
	&m31378_MI,
	&m31379_MI,
	&m31380_MI,
	&m31381_MI,
	NULL
};
extern TypeInfo t6020_TI;
static TypeInfo* t6018_ITIs[] = 
{
	&t603_TI,
	&t6020_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6018_0_0_0;
extern Il2CppType t6018_1_0_0;
struct t6018;
extern Il2CppGenericClass t6018_GC;
TypeInfo t6018_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6018_MIs, t6018_PIs, NULL, NULL, NULL, NULL, NULL, &t6018_TI, t6018_ITIs, NULL, &EmptyCustomAttributesCache, &t6018_TI, &t6018_0_0_0, &t6018_1_0_0, NULL, &t6018_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern Il2CppType t4657_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31382_GM;
MethodInfo m31382_MI = 
{
	"GetEnumerator", NULL, &t6020_TI, &t4657_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31382_GM};
static MethodInfo* t6020_MIs[] =
{
	&m31382_MI,
	NULL
};
static TypeInfo* t6020_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6020_0_0_0;
extern Il2CppType t6020_1_0_0;
struct t6020;
extern Il2CppGenericClass t6020_GC;
TypeInfo t6020_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6020_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6020_TI, t6020_ITIs, NULL, &EmptyCustomAttributesCache, &t6020_TI, &t6020_0_0_0, &t6020_1_0_0, NULL, &t6020_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6019_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo m31383_MI;
extern MethodInfo m31384_MI;
static PropertyInfo t6019____Item_PropertyInfo = 
{
	&t6019_TI, "Item", &m31383_MI, &m31384_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6019_PIs[] =
{
	&t6019____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1037_0_0_0;
static ParameterInfo t6019_m31385_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1037_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31385_GM;
MethodInfo m31385_MI = 
{
	"IndexOf", NULL, &t6019_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6019_m31385_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31385_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1037_0_0_0;
static ParameterInfo t6019_m31386_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1037_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31386_GM;
MethodInfo m31386_MI = 
{
	"Insert", NULL, &t6019_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6019_m31386_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31386_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6019_m31387_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31387_GM;
MethodInfo m31387_MI = 
{
	"RemoveAt", NULL, &t6019_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6019_m31387_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31387_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6019_m31383_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1037_0_0_0;
extern void* RuntimeInvoker_t1037_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31383_GM;
MethodInfo m31383_MI = 
{
	"get_Item", NULL, &t6019_TI, &t1037_0_0_0, RuntimeInvoker_t1037_t44, t6019_m31383_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31383_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1037_0_0_0;
static ParameterInfo t6019_m31384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1037_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31384_GM;
MethodInfo m31384_MI = 
{
	"set_Item", NULL, &t6019_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6019_m31384_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31384_GM};
static MethodInfo* t6019_MIs[] =
{
	&m31385_MI,
	&m31386_MI,
	&m31387_MI,
	&m31383_MI,
	&m31384_MI,
	NULL
};
static TypeInfo* t6019_ITIs[] = 
{
	&t603_TI,
	&t6018_TI,
	&t6020_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6019_0_0_0;
extern Il2CppType t6019_1_0_0;
struct t6019;
extern Il2CppGenericClass t6019_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6019_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6019_MIs, t6019_PIs, NULL, NULL, NULL, NULL, NULL, &t6019_TI, t6019_ITIs, NULL, &t1908__CustomAttributeCache, &t6019_TI, &t6019_0_0_0, &t6019_1_0_0, NULL, &t6019_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4659_TI;

#include "t1128.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>
extern MethodInfo m31388_MI;
static PropertyInfo t4659____Current_PropertyInfo = 
{
	&t4659_TI, "Current", &m31388_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4659_PIs[] =
{
	&t4659____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1128_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31388_GM;
MethodInfo m31388_MI = 
{
	"get_Current", NULL, &t4659_TI, &t1128_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31388_GM};
static MethodInfo* t4659_MIs[] =
{
	&m31388_MI,
	NULL
};
static TypeInfo* t4659_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4659_0_0_0;
extern Il2CppType t4659_1_0_0;
struct t4659;
extern Il2CppGenericClass t4659_GC;
TypeInfo t4659_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4659_MIs, t4659_PIs, NULL, NULL, NULL, NULL, NULL, &t4659_TI, t4659_ITIs, NULL, &EmptyCustomAttributesCache, &t4659_TI, &t4659_0_0_0, &t4659_1_0_0, NULL, &t4659_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3260.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3260_TI;
#include "t3260MD.h"

extern TypeInfo t1128_TI;
extern MethodInfo m18100_MI;
extern MethodInfo m23960_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m23960(__this, p0, method) (t1128 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.SerializableAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3260_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3260_TI, offsetof(t3260, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3260_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3260_TI, offsetof(t3260, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3260_FIs[] =
{
	&t3260_f0_FieldInfo,
	&t3260_f1_FieldInfo,
	NULL
};
extern MethodInfo m18097_MI;
static PropertyInfo t3260____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3260_TI, "System.Collections.IEnumerator.Current", &m18097_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3260____Current_PropertyInfo = 
{
	&t3260_TI, "Current", &m18100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3260_PIs[] =
{
	&t3260____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3260____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3260_m18096_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18096_GM;
MethodInfo m18096_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3260_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3260_m18096_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18096_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18097_GM;
MethodInfo m18097_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3260_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18097_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18098_GM;
MethodInfo m18098_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18098_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18099_GM;
MethodInfo m18099_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3260_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18099_GM};
extern Il2CppType t1128_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18100_GM;
MethodInfo m18100_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3260_TI, &t1128_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18100_GM};
static MethodInfo* t3260_MIs[] =
{
	&m18096_MI,
	&m18097_MI,
	&m18098_MI,
	&m18099_MI,
	&m18100_MI,
	NULL
};
extern MethodInfo m18099_MI;
extern MethodInfo m18098_MI;
static MethodInfo* t3260_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18097_MI,
	&m18099_MI,
	&m18098_MI,
	&m18100_MI,
};
static TypeInfo* t3260_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4659_TI,
};
static Il2CppInterfaceOffsetPair t3260_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4659_TI, 7},
};
extern TypeInfo t1128_TI;
static Il2CppRGCTXData t3260_RGCTXData[3] = 
{
	&m18100_MI/* Method Usage */,
	&t1128_TI/* Class Usage */,
	&m23960_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3260_0_0_0;
extern Il2CppType t3260_1_0_0;
extern Il2CppGenericClass t3260_GC;
TypeInfo t3260_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3260_MIs, t3260_PIs, t3260_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3260_TI, t3260_ITIs, t3260_VT, &EmptyCustomAttributesCache, &t3260_TI, &t3260_0_0_0, &t3260_1_0_0, t3260_IOs, &t3260_GC, NULL, NULL, NULL, t3260_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3260)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6021_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.SerializableAttribute>
extern MethodInfo m31389_MI;
static PropertyInfo t6021____Count_PropertyInfo = 
{
	&t6021_TI, "Count", &m31389_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31390_MI;
static PropertyInfo t6021____IsReadOnly_PropertyInfo = 
{
	&t6021_TI, "IsReadOnly", &m31390_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6021_PIs[] =
{
	&t6021____Count_PropertyInfo,
	&t6021____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31389_GM;
MethodInfo m31389_MI = 
{
	"get_Count", NULL, &t6021_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31389_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31390_GM;
MethodInfo m31390_MI = 
{
	"get_IsReadOnly", NULL, &t6021_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31390_GM};
extern Il2CppType t1128_0_0_0;
extern Il2CppType t1128_0_0_0;
static ParameterInfo t6021_m31391_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31391_GM;
MethodInfo m31391_MI = 
{
	"Add", NULL, &t6021_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6021_m31391_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31391_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31392_GM;
MethodInfo m31392_MI = 
{
	"Clear", NULL, &t6021_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31392_GM};
extern Il2CppType t1128_0_0_0;
static ParameterInfo t6021_m31393_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1128_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31393_GM;
MethodInfo m31393_MI = 
{
	"Contains", NULL, &t6021_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6021_m31393_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31393_GM};
extern Il2CppType t3558_0_0_0;
extern Il2CppType t3558_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6021_m31394_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3558_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31394_GM;
MethodInfo m31394_MI = 
{
	"CopyTo", NULL, &t6021_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6021_m31394_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31394_GM};
extern Il2CppType t1128_0_0_0;
static ParameterInfo t6021_m31395_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1128_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31395_GM;
MethodInfo m31395_MI = 
{
	"Remove", NULL, &t6021_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6021_m31395_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31395_GM};
static MethodInfo* t6021_MIs[] =
{
	&m31389_MI,
	&m31390_MI,
	&m31391_MI,
	&m31392_MI,
	&m31393_MI,
	&m31394_MI,
	&m31395_MI,
	NULL
};
extern TypeInfo t6023_TI;
static TypeInfo* t6021_ITIs[] = 
{
	&t603_TI,
	&t6023_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6021_0_0_0;
extern Il2CppType t6021_1_0_0;
struct t6021;
extern Il2CppGenericClass t6021_GC;
TypeInfo t6021_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6021_MIs, t6021_PIs, NULL, NULL, NULL, NULL, NULL, &t6021_TI, t6021_ITIs, NULL, &EmptyCustomAttributesCache, &t6021_TI, &t6021_0_0_0, &t6021_1_0_0, NULL, &t6021_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>
extern Il2CppType t4659_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31396_GM;
MethodInfo m31396_MI = 
{
	"GetEnumerator", NULL, &t6023_TI, &t4659_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31396_GM};
static MethodInfo* t6023_MIs[] =
{
	&m31396_MI,
	NULL
};
static TypeInfo* t6023_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6023_0_0_0;
extern Il2CppType t6023_1_0_0;
struct t6023;
extern Il2CppGenericClass t6023_GC;
TypeInfo t6023_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6023_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6023_TI, t6023_ITIs, NULL, &EmptyCustomAttributesCache, &t6023_TI, &t6023_0_0_0, &t6023_1_0_0, NULL, &t6023_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6022_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.SerializableAttribute>
extern MethodInfo m31397_MI;
extern MethodInfo m31398_MI;
static PropertyInfo t6022____Item_PropertyInfo = 
{
	&t6022_TI, "Item", &m31397_MI, &m31398_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6022_PIs[] =
{
	&t6022____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1128_0_0_0;
static ParameterInfo t6022_m31399_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1128_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31399_GM;
MethodInfo m31399_MI = 
{
	"IndexOf", NULL, &t6022_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6022_m31399_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31399_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1128_0_0_0;
static ParameterInfo t6022_m31400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31400_GM;
MethodInfo m31400_MI = 
{
	"Insert", NULL, &t6022_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6022_m31400_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31400_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6022_m31401_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31401_GM;
MethodInfo m31401_MI = 
{
	"RemoveAt", NULL, &t6022_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6022_m31401_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31401_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6022_m31397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1128_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31397_GM;
MethodInfo m31397_MI = 
{
	"get_Item", NULL, &t6022_TI, &t1128_0_0_0, RuntimeInvoker_t29_t44, t6022_m31397_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31397_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1128_0_0_0;
static ParameterInfo t6022_m31398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31398_GM;
MethodInfo m31398_MI = 
{
	"set_Item", NULL, &t6022_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6022_m31398_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31398_GM};
static MethodInfo* t6022_MIs[] =
{
	&m31399_MI,
	&m31400_MI,
	&m31401_MI,
	&m31397_MI,
	&m31398_MI,
	NULL
};
static TypeInfo* t6022_ITIs[] = 
{
	&t603_TI,
	&t6021_TI,
	&t6023_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6022_0_0_0;
extern Il2CppType t6022_1_0_0;
struct t6022;
extern Il2CppGenericClass t6022_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6022_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6022_MIs, t6022_PIs, NULL, NULL, NULL, NULL, NULL, &t6022_TI, t6022_ITIs, NULL, &t1908__CustomAttributeCache, &t6022_TI, &t6022_0_0_0, &t6022_1_0_0, NULL, &t6022_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4661_TI;

#include "t629.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.AttributeUsageAttribute>
extern MethodInfo m31402_MI;
static PropertyInfo t4661____Current_PropertyInfo = 
{
	&t4661_TI, "Current", &m31402_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4661_PIs[] =
{
	&t4661____Current_PropertyInfo,
	NULL
};
extern Il2CppType t629_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31402_GM;
MethodInfo m31402_MI = 
{
	"get_Current", NULL, &t4661_TI, &t629_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31402_GM};
static MethodInfo* t4661_MIs[] =
{
	&m31402_MI,
	NULL
};
static TypeInfo* t4661_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4661_0_0_0;
extern Il2CppType t4661_1_0_0;
struct t4661;
extern Il2CppGenericClass t4661_GC;
TypeInfo t4661_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4661_MIs, t4661_PIs, NULL, NULL, NULL, NULL, NULL, &t4661_TI, t4661_ITIs, NULL, &EmptyCustomAttributesCache, &t4661_TI, &t4661_0_0_0, &t4661_1_0_0, NULL, &t4661_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3261.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3261_TI;
#include "t3261MD.h"

extern TypeInfo t629_TI;
extern MethodInfo m18105_MI;
extern MethodInfo m23971_MI;
struct t20;
#define m23971(__this, p0, method) (t629 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3261_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3261_TI, offsetof(t3261, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3261_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3261_TI, offsetof(t3261, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3261_FIs[] =
{
	&t3261_f0_FieldInfo,
	&t3261_f1_FieldInfo,
	NULL
};
extern MethodInfo m18102_MI;
static PropertyInfo t3261____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3261_TI, "System.Collections.IEnumerator.Current", &m18102_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3261____Current_PropertyInfo = 
{
	&t3261_TI, "Current", &m18105_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3261_PIs[] =
{
	&t3261____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3261____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3261_m18101_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18101_GM;
MethodInfo m18101_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3261_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3261_m18101_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18101_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18102_GM;
MethodInfo m18102_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3261_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18102_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18103_GM;
MethodInfo m18103_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3261_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18103_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18104_GM;
MethodInfo m18104_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3261_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18104_GM};
extern Il2CppType t629_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18105_GM;
MethodInfo m18105_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3261_TI, &t629_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18105_GM};
static MethodInfo* t3261_MIs[] =
{
	&m18101_MI,
	&m18102_MI,
	&m18103_MI,
	&m18104_MI,
	&m18105_MI,
	NULL
};
extern MethodInfo m18104_MI;
extern MethodInfo m18103_MI;
static MethodInfo* t3261_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18102_MI,
	&m18104_MI,
	&m18103_MI,
	&m18105_MI,
};
static TypeInfo* t3261_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4661_TI,
};
static Il2CppInterfaceOffsetPair t3261_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4661_TI, 7},
};
extern TypeInfo t629_TI;
static Il2CppRGCTXData t3261_RGCTXData[3] = 
{
	&m18105_MI/* Method Usage */,
	&t629_TI/* Class Usage */,
	&m23971_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3261_0_0_0;
extern Il2CppType t3261_1_0_0;
extern Il2CppGenericClass t3261_GC;
TypeInfo t3261_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3261_MIs, t3261_PIs, t3261_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3261_TI, t3261_ITIs, t3261_VT, &EmptyCustomAttributesCache, &t3261_TI, &t3261_0_0_0, &t3261_1_0_0, t3261_IOs, &t3261_GC, NULL, NULL, NULL, t3261_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3261)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6024_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>
extern MethodInfo m31403_MI;
static PropertyInfo t6024____Count_PropertyInfo = 
{
	&t6024_TI, "Count", &m31403_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31404_MI;
static PropertyInfo t6024____IsReadOnly_PropertyInfo = 
{
	&t6024_TI, "IsReadOnly", &m31404_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6024_PIs[] =
{
	&t6024____Count_PropertyInfo,
	&t6024____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31403_GM;
MethodInfo m31403_MI = 
{
	"get_Count", NULL, &t6024_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31403_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31404_GM;
MethodInfo m31404_MI = 
{
	"get_IsReadOnly", NULL, &t6024_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31404_GM};
extern Il2CppType t629_0_0_0;
extern Il2CppType t629_0_0_0;
static ParameterInfo t6024_m31405_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t629_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31405_GM;
MethodInfo m31405_MI = 
{
	"Add", NULL, &t6024_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6024_m31405_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31405_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31406_GM;
MethodInfo m31406_MI = 
{
	"Clear", NULL, &t6024_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31406_GM};
extern Il2CppType t629_0_0_0;
static ParameterInfo t6024_m31407_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31407_GM;
MethodInfo m31407_MI = 
{
	"Contains", NULL, &t6024_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6024_m31407_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31407_GM};
extern Il2CppType t3559_0_0_0;
extern Il2CppType t3559_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6024_m31408_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3559_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31408_GM;
MethodInfo m31408_MI = 
{
	"CopyTo", NULL, &t6024_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6024_m31408_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31408_GM};
extern Il2CppType t629_0_0_0;
static ParameterInfo t6024_m31409_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31409_GM;
MethodInfo m31409_MI = 
{
	"Remove", NULL, &t6024_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6024_m31409_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31409_GM};
static MethodInfo* t6024_MIs[] =
{
	&m31403_MI,
	&m31404_MI,
	&m31405_MI,
	&m31406_MI,
	&m31407_MI,
	&m31408_MI,
	&m31409_MI,
	NULL
};
extern TypeInfo t6026_TI;
static TypeInfo* t6024_ITIs[] = 
{
	&t603_TI,
	&t6026_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6024_0_0_0;
extern Il2CppType t6024_1_0_0;
struct t6024;
extern Il2CppGenericClass t6024_GC;
TypeInfo t6024_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6024_MIs, t6024_PIs, NULL, NULL, NULL, NULL, NULL, &t6024_TI, t6024_ITIs, NULL, &EmptyCustomAttributesCache, &t6024_TI, &t6024_0_0_0, &t6024_1_0_0, NULL, &t6024_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.AttributeUsageAttribute>
extern Il2CppType t4661_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31410_GM;
MethodInfo m31410_MI = 
{
	"GetEnumerator", NULL, &t6026_TI, &t4661_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31410_GM};
static MethodInfo* t6026_MIs[] =
{
	&m31410_MI,
	NULL
};
static TypeInfo* t6026_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6026_0_0_0;
extern Il2CppType t6026_1_0_0;
struct t6026;
extern Il2CppGenericClass t6026_GC;
TypeInfo t6026_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6026_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6026_TI, t6026_ITIs, NULL, &EmptyCustomAttributesCache, &t6026_TI, &t6026_0_0_0, &t6026_1_0_0, NULL, &t6026_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6025_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.AttributeUsageAttribute>
extern MethodInfo m31411_MI;
extern MethodInfo m31412_MI;
static PropertyInfo t6025____Item_PropertyInfo = 
{
	&t6025_TI, "Item", &m31411_MI, &m31412_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6025_PIs[] =
{
	&t6025____Item_PropertyInfo,
	NULL
};
extern Il2CppType t629_0_0_0;
static ParameterInfo t6025_m31413_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31413_GM;
MethodInfo m31413_MI = 
{
	"IndexOf", NULL, &t6025_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6025_m31413_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31413_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t629_0_0_0;
static ParameterInfo t6025_m31414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t629_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31414_GM;
MethodInfo m31414_MI = 
{
	"Insert", NULL, &t6025_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6025_m31414_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31414_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6025_m31415_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31415_GM;
MethodInfo m31415_MI = 
{
	"RemoveAt", NULL, &t6025_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6025_m31415_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31415_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6025_m31411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t629_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31411_GM;
MethodInfo m31411_MI = 
{
	"get_Item", NULL, &t6025_TI, &t629_0_0_0, RuntimeInvoker_t29_t44, t6025_m31411_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31411_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t629_0_0_0;
static ParameterInfo t6025_m31412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t629_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31412_GM;
MethodInfo m31412_MI = 
{
	"set_Item", NULL, &t6025_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6025_m31412_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31412_GM};
static MethodInfo* t6025_MIs[] =
{
	&m31413_MI,
	&m31414_MI,
	&m31415_MI,
	&m31411_MI,
	&m31412_MI,
	NULL
};
static TypeInfo* t6025_ITIs[] = 
{
	&t603_TI,
	&t6024_TI,
	&t6026_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6025_0_0_0;
extern Il2CppType t6025_1_0_0;
struct t6025;
extern Il2CppGenericClass t6025_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6025_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6025_MIs, t6025_PIs, NULL, NULL, NULL, NULL, NULL, &t6025_TI, t6025_ITIs, NULL, &t1908__CustomAttributeCache, &t6025_TI, &t6025_0_0_0, &t6025_1_0_0, NULL, &t6025_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4663_TI;

#include "t437.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern MethodInfo m31416_MI;
static PropertyInfo t4663____Current_PropertyInfo = 
{
	&t4663_TI, "Current", &m31416_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4663_PIs[] =
{
	&t4663____Current_PropertyInfo,
	NULL
};
extern Il2CppType t437_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31416_GM;
MethodInfo m31416_MI = 
{
	"get_Current", NULL, &t4663_TI, &t437_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31416_GM};
static MethodInfo* t4663_MIs[] =
{
	&m31416_MI,
	NULL
};
static TypeInfo* t4663_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4663_0_0_0;
extern Il2CppType t4663_1_0_0;
struct t4663;
extern Il2CppGenericClass t4663_GC;
TypeInfo t4663_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4663_MIs, t4663_PIs, NULL, NULL, NULL, NULL, NULL, &t4663_TI, t4663_ITIs, NULL, &EmptyCustomAttributesCache, &t4663_TI, &t4663_0_0_0, &t4663_1_0_0, NULL, &t4663_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3262.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3262_TI;
#include "t3262MD.h"

extern TypeInfo t437_TI;
extern MethodInfo m18110_MI;
extern MethodInfo m23982_MI;
struct t20;
#define m23982(__this, p0, method) (t437 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3262_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3262_TI, offsetof(t3262, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3262_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3262_TI, offsetof(t3262, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3262_FIs[] =
{
	&t3262_f0_FieldInfo,
	&t3262_f1_FieldInfo,
	NULL
};
extern MethodInfo m18107_MI;
static PropertyInfo t3262____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3262_TI, "System.Collections.IEnumerator.Current", &m18107_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3262____Current_PropertyInfo = 
{
	&t3262_TI, "Current", &m18110_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3262_PIs[] =
{
	&t3262____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3262____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3262_m18106_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18106_GM;
MethodInfo m18106_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3262_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3262_m18106_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18106_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18107_GM;
MethodInfo m18107_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3262_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18107_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18108_GM;
MethodInfo m18108_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18108_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18109_GM;
MethodInfo m18109_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3262_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18109_GM};
extern Il2CppType t437_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18110_GM;
MethodInfo m18110_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3262_TI, &t437_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18110_GM};
static MethodInfo* t3262_MIs[] =
{
	&m18106_MI,
	&m18107_MI,
	&m18108_MI,
	&m18109_MI,
	&m18110_MI,
	NULL
};
extern MethodInfo m18109_MI;
extern MethodInfo m18108_MI;
static MethodInfo* t3262_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18107_MI,
	&m18109_MI,
	&m18108_MI,
	&m18110_MI,
};
static TypeInfo* t3262_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4663_TI,
};
static Il2CppInterfaceOffsetPair t3262_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4663_TI, 7},
};
extern TypeInfo t437_TI;
static Il2CppRGCTXData t3262_RGCTXData[3] = 
{
	&m18110_MI/* Method Usage */,
	&t437_TI/* Class Usage */,
	&m23982_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3262_0_0_0;
extern Il2CppType t3262_1_0_0;
extern Il2CppGenericClass t3262_GC;
TypeInfo t3262_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3262_MIs, t3262_PIs, t3262_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3262_TI, t3262_ITIs, t3262_VT, &EmptyCustomAttributesCache, &t3262_TI, &t3262_0_0_0, &t3262_1_0_0, t3262_IOs, &t3262_GC, NULL, NULL, NULL, t3262_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3262)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6027_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern MethodInfo m31417_MI;
static PropertyInfo t6027____Count_PropertyInfo = 
{
	&t6027_TI, "Count", &m31417_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31418_MI;
static PropertyInfo t6027____IsReadOnly_PropertyInfo = 
{
	&t6027_TI, "IsReadOnly", &m31418_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6027_PIs[] =
{
	&t6027____Count_PropertyInfo,
	&t6027____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31417_GM;
MethodInfo m31417_MI = 
{
	"get_Count", NULL, &t6027_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31417_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31418_GM;
MethodInfo m31418_MI = 
{
	"get_IsReadOnly", NULL, &t6027_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31418_GM};
extern Il2CppType t437_0_0_0;
extern Il2CppType t437_0_0_0;
static ParameterInfo t6027_m31419_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31419_GM;
MethodInfo m31419_MI = 
{
	"Add", NULL, &t6027_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6027_m31419_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31419_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31420_GM;
MethodInfo m31420_MI = 
{
	"Clear", NULL, &t6027_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31420_GM};
extern Il2CppType t437_0_0_0;
static ParameterInfo t6027_m31421_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t437_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31421_GM;
MethodInfo m31421_MI = 
{
	"Contains", NULL, &t6027_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6027_m31421_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31421_GM};
extern Il2CppType t3560_0_0_0;
extern Il2CppType t3560_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6027_m31422_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3560_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31422_GM;
MethodInfo m31422_MI = 
{
	"CopyTo", NULL, &t6027_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6027_m31422_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31422_GM};
extern Il2CppType t437_0_0_0;
static ParameterInfo t6027_m31423_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t437_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31423_GM;
MethodInfo m31423_MI = 
{
	"Remove", NULL, &t6027_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6027_m31423_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31423_GM};
static MethodInfo* t6027_MIs[] =
{
	&m31417_MI,
	&m31418_MI,
	&m31419_MI,
	&m31420_MI,
	&m31421_MI,
	&m31422_MI,
	&m31423_MI,
	NULL
};
extern TypeInfo t6029_TI;
static TypeInfo* t6027_ITIs[] = 
{
	&t603_TI,
	&t6029_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6027_0_0_0;
extern Il2CppType t6027_1_0_0;
struct t6027;
extern Il2CppGenericClass t6027_GC;
TypeInfo t6027_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6027_MIs, t6027_PIs, NULL, NULL, NULL, NULL, NULL, &t6027_TI, t6027_ITIs, NULL, &EmptyCustomAttributesCache, &t6027_TI, &t6027_0_0_0, &t6027_1_0_0, NULL, &t6027_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern Il2CppType t4663_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31424_GM;
MethodInfo m31424_MI = 
{
	"GetEnumerator", NULL, &t6029_TI, &t4663_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31424_GM};
static MethodInfo* t6029_MIs[] =
{
	&m31424_MI,
	NULL
};
static TypeInfo* t6029_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6029_0_0_0;
extern Il2CppType t6029_1_0_0;
struct t6029;
extern Il2CppGenericClass t6029_GC;
TypeInfo t6029_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6029_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6029_TI, t6029_ITIs, NULL, &EmptyCustomAttributesCache, &t6029_TI, &t6029_0_0_0, &t6029_1_0_0, NULL, &t6029_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6028_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern MethodInfo m31425_MI;
extern MethodInfo m31426_MI;
static PropertyInfo t6028____Item_PropertyInfo = 
{
	&t6028_TI, "Item", &m31425_MI, &m31426_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6028_PIs[] =
{
	&t6028____Item_PropertyInfo,
	NULL
};
extern Il2CppType t437_0_0_0;
static ParameterInfo t6028_m31427_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t437_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31427_GM;
MethodInfo m31427_MI = 
{
	"IndexOf", NULL, &t6028_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6028_m31427_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31427_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t437_0_0_0;
static ParameterInfo t6028_m31428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31428_GM;
MethodInfo m31428_MI = 
{
	"Insert", NULL, &t6028_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6028_m31428_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31428_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6028_m31429_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31429_GM;
MethodInfo m31429_MI = 
{
	"RemoveAt", NULL, &t6028_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6028_m31429_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31429_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6028_m31425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t437_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31425_GM;
MethodInfo m31425_MI = 
{
	"get_Item", NULL, &t6028_TI, &t437_0_0_0, RuntimeInvoker_t29_t44, t6028_m31425_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31425_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t437_0_0_0;
static ParameterInfo t6028_m31426_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31426_GM;
MethodInfo m31426_MI = 
{
	"set_Item", NULL, &t6028_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6028_m31426_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31426_GM};
static MethodInfo* t6028_MIs[] =
{
	&m31427_MI,
	&m31428_MI,
	&m31429_MI,
	&m31425_MI,
	&m31426_MI,
	NULL
};
static TypeInfo* t6028_ITIs[] = 
{
	&t603_TI,
	&t6027_TI,
	&t6029_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6028_0_0_0;
extern Il2CppType t6028_1_0_0;
struct t6028;
extern Il2CppGenericClass t6028_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6028_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6028_MIs, t6028_PIs, NULL, NULL, NULL, NULL, NULL, &t6028_TI, t6028_ITIs, NULL, &t1908__CustomAttributeCache, &t6028_TI, &t6028_0_0_0, &t6028_1_0_0, NULL, &t6028_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4664_TI;

#include "t919.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Int64>
extern MethodInfo m31430_MI;
static PropertyInfo t4664____Current_PropertyInfo = 
{
	&t4664_TI, "Current", &m31430_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4664_PIs[] =
{
	&t4664____Current_PropertyInfo,
	NULL
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31430_GM;
MethodInfo m31430_MI = 
{
	"get_Current", NULL, &t4664_TI, &t919_0_0_0, RuntimeInvoker_t919, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31430_GM};
static MethodInfo* t4664_MIs[] =
{
	&m31430_MI,
	NULL
};
static TypeInfo* t4664_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4664_0_0_0;
extern Il2CppType t4664_1_0_0;
struct t4664;
extern Il2CppGenericClass t4664_GC;
TypeInfo t4664_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4664_MIs, t4664_PIs, NULL, NULL, NULL, NULL, NULL, &t4664_TI, t4664_ITIs, NULL, &EmptyCustomAttributesCache, &t4664_TI, &t4664_0_0_0, &t4664_1_0_0, NULL, &t4664_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3263.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3263_TI;
#include "t3263MD.h"

extern TypeInfo t919_TI;
extern MethodInfo m18115_MI;
extern MethodInfo m23993_MI;
struct t20;
 int64_t m23993 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18111_MI;
 void m18111 (t3263 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18112_MI;
 t29 * m18112 (t3263 * __this, MethodInfo* method){
	{
		int64_t L_0 = m18115(__this, &m18115_MI);
		int64_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t919_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18113_MI;
 void m18113 (t3263 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18114_MI;
 bool m18114 (t3263 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int64_t m18115 (t3263 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int64_t L_8 = m23993(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23993_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Int64>
extern Il2CppType t20_0_0_1;
FieldInfo t3263_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3263_TI, offsetof(t3263, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3263_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3263_TI, offsetof(t3263, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3263_FIs[] =
{
	&t3263_f0_FieldInfo,
	&t3263_f1_FieldInfo,
	NULL
};
static PropertyInfo t3263____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3263_TI, "System.Collections.IEnumerator.Current", &m18112_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3263____Current_PropertyInfo = 
{
	&t3263_TI, "Current", &m18115_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3263_PIs[] =
{
	&t3263____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3263____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3263_m18111_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18111_GM;
MethodInfo m18111_MI = 
{
	".ctor", (methodPointerType)&m18111, &t3263_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3263_m18111_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18111_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18112_GM;
MethodInfo m18112_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18112, &t3263_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18112_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18113_GM;
MethodInfo m18113_MI = 
{
	"Dispose", (methodPointerType)&m18113, &t3263_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18113_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18114_GM;
MethodInfo m18114_MI = 
{
	"MoveNext", (methodPointerType)&m18114, &t3263_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18114_GM};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18115_GM;
MethodInfo m18115_MI = 
{
	"get_Current", (methodPointerType)&m18115, &t3263_TI, &t919_0_0_0, RuntimeInvoker_t919, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18115_GM};
static MethodInfo* t3263_MIs[] =
{
	&m18111_MI,
	&m18112_MI,
	&m18113_MI,
	&m18114_MI,
	&m18115_MI,
	NULL
};
static MethodInfo* t3263_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18112_MI,
	&m18114_MI,
	&m18113_MI,
	&m18115_MI,
};
static TypeInfo* t3263_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4664_TI,
};
static Il2CppInterfaceOffsetPair t3263_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4664_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3263_0_0_0;
extern Il2CppType t3263_1_0_0;
extern Il2CppGenericClass t3263_GC;
TypeInfo t3263_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3263_MIs, t3263_PIs, t3263_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3263_TI, t3263_ITIs, t3263_VT, &EmptyCustomAttributesCache, &t3263_TI, &t3263_0_0_0, &t3263_1_0_0, t3263_IOs, &t3263_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3263)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6030_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Int64>
extern MethodInfo m31431_MI;
static PropertyInfo t6030____Count_PropertyInfo = 
{
	&t6030_TI, "Count", &m31431_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31432_MI;
static PropertyInfo t6030____IsReadOnly_PropertyInfo = 
{
	&t6030_TI, "IsReadOnly", &m31432_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6030_PIs[] =
{
	&t6030____Count_PropertyInfo,
	&t6030____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31431_GM;
MethodInfo m31431_MI = 
{
	"get_Count", NULL, &t6030_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31431_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31432_GM;
MethodInfo m31432_MI = 
{
	"get_IsReadOnly", NULL, &t6030_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31432_GM};
extern Il2CppType t919_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t6030_m31433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31433_GM;
MethodInfo m31433_MI = 
{
	"Add", NULL, &t6030_TI, &t21_0_0_0, RuntimeInvoker_t21_t919, t6030_m31433_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31433_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31434_GM;
MethodInfo m31434_MI = 
{
	"Clear", NULL, &t6030_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31434_GM};
extern Il2CppType t919_0_0_0;
static ParameterInfo t6030_m31435_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31435_GM;
MethodInfo m31435_MI = 
{
	"Contains", NULL, &t6030_TI, &t40_0_0_0, RuntimeInvoker_t40_t919, t6030_m31435_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31435_GM};
extern Il2CppType t1139_0_0_0;
extern Il2CppType t1139_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6030_m31436_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1139_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31436_GM;
MethodInfo m31436_MI = 
{
	"CopyTo", NULL, &t6030_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6030_m31436_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31436_GM};
extern Il2CppType t919_0_0_0;
static ParameterInfo t6030_m31437_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31437_GM;
MethodInfo m31437_MI = 
{
	"Remove", NULL, &t6030_TI, &t40_0_0_0, RuntimeInvoker_t40_t919, t6030_m31437_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31437_GM};
static MethodInfo* t6030_MIs[] =
{
	&m31431_MI,
	&m31432_MI,
	&m31433_MI,
	&m31434_MI,
	&m31435_MI,
	&m31436_MI,
	&m31437_MI,
	NULL
};
extern TypeInfo t6032_TI;
static TypeInfo* t6030_ITIs[] = 
{
	&t603_TI,
	&t6032_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6030_0_0_0;
extern Il2CppType t6030_1_0_0;
struct t6030;
extern Il2CppGenericClass t6030_GC;
TypeInfo t6030_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6030_MIs, t6030_PIs, NULL, NULL, NULL, NULL, NULL, &t6030_TI, t6030_ITIs, NULL, &EmptyCustomAttributesCache, &t6030_TI, &t6030_0_0_0, &t6030_1_0_0, NULL, &t6030_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Int64>
extern Il2CppType t4664_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31438_GM;
MethodInfo m31438_MI = 
{
	"GetEnumerator", NULL, &t6032_TI, &t4664_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31438_GM};
static MethodInfo* t6032_MIs[] =
{
	&m31438_MI,
	NULL
};
static TypeInfo* t6032_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6032_0_0_0;
extern Il2CppType t6032_1_0_0;
struct t6032;
extern Il2CppGenericClass t6032_GC;
TypeInfo t6032_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6032_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6032_TI, t6032_ITIs, NULL, &EmptyCustomAttributesCache, &t6032_TI, &t6032_0_0_0, &t6032_1_0_0, NULL, &t6032_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6031_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Int64>
extern MethodInfo m31439_MI;
extern MethodInfo m31440_MI;
static PropertyInfo t6031____Item_PropertyInfo = 
{
	&t6031_TI, "Item", &m31439_MI, &m31440_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6031_PIs[] =
{
	&t6031____Item_PropertyInfo,
	NULL
};
extern Il2CppType t919_0_0_0;
static ParameterInfo t6031_m31441_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31441_GM;
MethodInfo m31441_MI = 
{
	"IndexOf", NULL, &t6031_TI, &t44_0_0_0, RuntimeInvoker_t44_t919, t6031_m31441_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31441_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t6031_m31442_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31442_GM;
MethodInfo m31442_MI = 
{
	"Insert", NULL, &t6031_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t919, t6031_m31442_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31442_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6031_m31443_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31443_GM;
MethodInfo m31443_MI = 
{
	"RemoveAt", NULL, &t6031_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6031_m31443_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31443_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6031_m31439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31439_GM;
MethodInfo m31439_MI = 
{
	"get_Item", NULL, &t6031_TI, &t919_0_0_0, RuntimeInvoker_t919_t44, t6031_m31439_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31439_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t6031_m31440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31440_GM;
MethodInfo m31440_MI = 
{
	"set_Item", NULL, &t6031_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t919, t6031_m31440_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31440_GM};
static MethodInfo* t6031_MIs[] =
{
	&m31441_MI,
	&m31442_MI,
	&m31443_MI,
	&m31439_MI,
	&m31440_MI,
	NULL
};
static TypeInfo* t6031_ITIs[] = 
{
	&t603_TI,
	&t6030_TI,
	&t6032_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6031_0_0_0;
extern Il2CppType t6031_1_0_0;
struct t6031;
extern Il2CppGenericClass t6031_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6031_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6031_MIs, t6031_PIs, NULL, NULL, NULL, NULL, NULL, &t6031_TI, t6031_ITIs, NULL, &t1908__CustomAttributeCache, &t6031_TI, &t6031_0_0_0, &t6031_1_0_0, NULL, &t6031_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6033_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int64>>
extern MethodInfo m31444_MI;
static PropertyInfo t6033____Count_PropertyInfo = 
{
	&t6033_TI, "Count", &m31444_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31445_MI;
static PropertyInfo t6033____IsReadOnly_PropertyInfo = 
{
	&t6033_TI, "IsReadOnly", &m31445_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6033_PIs[] =
{
	&t6033____Count_PropertyInfo,
	&t6033____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31444_GM;
MethodInfo m31444_MI = 
{
	"get_Count", NULL, &t6033_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31444_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31445_GM;
MethodInfo m31445_MI = 
{
	"get_IsReadOnly", NULL, &t6033_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31445_GM};
extern Il2CppType t1713_0_0_0;
extern Il2CppType t1713_0_0_0;
static ParameterInfo t6033_m31446_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1713_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31446_GM;
MethodInfo m31446_MI = 
{
	"Add", NULL, &t6033_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6033_m31446_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31446_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31447_GM;
MethodInfo m31447_MI = 
{
	"Clear", NULL, &t6033_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31447_GM};
extern Il2CppType t1713_0_0_0;
static ParameterInfo t6033_m31448_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1713_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31448_GM;
MethodInfo m31448_MI = 
{
	"Contains", NULL, &t6033_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6033_m31448_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31448_GM};
extern Il2CppType t3561_0_0_0;
extern Il2CppType t3561_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6033_m31449_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3561_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31449_GM;
MethodInfo m31449_MI = 
{
	"CopyTo", NULL, &t6033_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6033_m31449_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31449_GM};
extern Il2CppType t1713_0_0_0;
static ParameterInfo t6033_m31450_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1713_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31450_GM;
MethodInfo m31450_MI = 
{
	"Remove", NULL, &t6033_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6033_m31450_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31450_GM};
static MethodInfo* t6033_MIs[] =
{
	&m31444_MI,
	&m31445_MI,
	&m31446_MI,
	&m31447_MI,
	&m31448_MI,
	&m31449_MI,
	&m31450_MI,
	NULL
};
extern TypeInfo t6035_TI;
static TypeInfo* t6033_ITIs[] = 
{
	&t603_TI,
	&t6035_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6033_0_0_0;
extern Il2CppType t6033_1_0_0;
struct t6033;
extern Il2CppGenericClass t6033_GC;
TypeInfo t6033_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6033_MIs, t6033_PIs, NULL, NULL, NULL, NULL, NULL, &t6033_TI, t6033_ITIs, NULL, &EmptyCustomAttributesCache, &t6033_TI, &t6033_0_0_0, &t6033_1_0_0, NULL, &t6033_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Int64>>
extern Il2CppType t4666_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31451_GM;
MethodInfo m31451_MI = 
{
	"GetEnumerator", NULL, &t6035_TI, &t4666_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31451_GM};
static MethodInfo* t6035_MIs[] =
{
	&m31451_MI,
	NULL
};
static TypeInfo* t6035_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6035_0_0_0;
extern Il2CppType t6035_1_0_0;
struct t6035;
extern Il2CppGenericClass t6035_GC;
TypeInfo t6035_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6035_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6035_TI, t6035_ITIs, NULL, &EmptyCustomAttributesCache, &t6035_TI, &t6035_0_0_0, &t6035_1_0_0, NULL, &t6035_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4666_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Int64>>
extern MethodInfo m31452_MI;
static PropertyInfo t4666____Current_PropertyInfo = 
{
	&t4666_TI, "Current", &m31452_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4666_PIs[] =
{
	&t4666____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1713_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31452_GM;
MethodInfo m31452_MI = 
{
	"get_Current", NULL, &t4666_TI, &t1713_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31452_GM};
static MethodInfo* t4666_MIs[] =
{
	&m31452_MI,
	NULL
};
static TypeInfo* t4666_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4666_0_0_0;
extern Il2CppType t4666_1_0_0;
struct t4666;
extern Il2CppGenericClass t4666_GC;
TypeInfo t4666_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4666_MIs, t4666_PIs, NULL, NULL, NULL, NULL, NULL, &t4666_TI, t4666_ITIs, NULL, &EmptyCustomAttributesCache, &t4666_TI, &t4666_0_0_0, &t4666_1_0_0, NULL, &t4666_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1713_TI;



// Metadata Definition System.IComparable`1<System.Int64>
extern Il2CppType t919_0_0_0;
static ParameterInfo t1713_m31453_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31453_GM;
MethodInfo m31453_MI = 
{
	"CompareTo", NULL, &t1713_TI, &t44_0_0_0, RuntimeInvoker_t44_t919, t1713_m31453_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31453_GM};
static MethodInfo* t1713_MIs[] =
{
	&m31453_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1713_1_0_0;
struct t1713;
extern Il2CppGenericClass t1713_GC;
TypeInfo t1713_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1713_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1713_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1713_TI, &t1713_0_0_0, &t1713_1_0_0, NULL, &t1713_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3264.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3264_TI;
#include "t3264MD.h"

extern MethodInfo m18120_MI;
extern MethodInfo m24004_MI;
struct t20;
#define m24004(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Int64>>
extern Il2CppType t20_0_0_1;
FieldInfo t3264_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3264_TI, offsetof(t3264, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3264_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3264_TI, offsetof(t3264, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3264_FIs[] =
{
	&t3264_f0_FieldInfo,
	&t3264_f1_FieldInfo,
	NULL
};
extern MethodInfo m18117_MI;
static PropertyInfo t3264____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3264_TI, "System.Collections.IEnumerator.Current", &m18117_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3264____Current_PropertyInfo = 
{
	&t3264_TI, "Current", &m18120_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3264_PIs[] =
{
	&t3264____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3264____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3264_m18116_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18116_GM;
MethodInfo m18116_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3264_m18116_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18116_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18117_GM;
MethodInfo m18117_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3264_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18117_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18118_GM;
MethodInfo m18118_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18118_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18119_GM;
MethodInfo m18119_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3264_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18119_GM};
extern Il2CppType t1713_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18120_GM;
MethodInfo m18120_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3264_TI, &t1713_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18120_GM};
static MethodInfo* t3264_MIs[] =
{
	&m18116_MI,
	&m18117_MI,
	&m18118_MI,
	&m18119_MI,
	&m18120_MI,
	NULL
};
extern MethodInfo m18119_MI;
extern MethodInfo m18118_MI;
static MethodInfo* t3264_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18117_MI,
	&m18119_MI,
	&m18118_MI,
	&m18120_MI,
};
static TypeInfo* t3264_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4666_TI,
};
static Il2CppInterfaceOffsetPair t3264_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4666_TI, 7},
};
extern TypeInfo t1713_TI;
static Il2CppRGCTXData t3264_RGCTXData[3] = 
{
	&m18120_MI/* Method Usage */,
	&t1713_TI/* Class Usage */,
	&m24004_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3264_0_0_0;
extern Il2CppType t3264_1_0_0;
extern Il2CppGenericClass t3264_GC;
TypeInfo t3264_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3264_MIs, t3264_PIs, t3264_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3264_TI, t3264_ITIs, t3264_VT, &EmptyCustomAttributesCache, &t3264_TI, &t3264_0_0_0, &t3264_1_0_0, t3264_IOs, &t3264_GC, NULL, NULL, NULL, t3264_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3264)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6034_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Int64>>
extern MethodInfo m31454_MI;
extern MethodInfo m31455_MI;
static PropertyInfo t6034____Item_PropertyInfo = 
{
	&t6034_TI, "Item", &m31454_MI, &m31455_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6034_PIs[] =
{
	&t6034____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1713_0_0_0;
static ParameterInfo t6034_m31456_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1713_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31456_GM;
MethodInfo m31456_MI = 
{
	"IndexOf", NULL, &t6034_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6034_m31456_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31456_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1713_0_0_0;
static ParameterInfo t6034_m31457_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1713_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31457_GM;
MethodInfo m31457_MI = 
{
	"Insert", NULL, &t6034_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6034_m31457_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31457_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6034_m31458_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31458_GM;
MethodInfo m31458_MI = 
{
	"RemoveAt", NULL, &t6034_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6034_m31458_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31458_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6034_m31454_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1713_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31454_GM;
MethodInfo m31454_MI = 
{
	"get_Item", NULL, &t6034_TI, &t1713_0_0_0, RuntimeInvoker_t29_t44, t6034_m31454_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31454_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1713_0_0_0;
static ParameterInfo t6034_m31455_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1713_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31455_GM;
MethodInfo m31455_MI = 
{
	"set_Item", NULL, &t6034_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6034_m31455_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31455_GM};
static MethodInfo* t6034_MIs[] =
{
	&m31456_MI,
	&m31457_MI,
	&m31458_MI,
	&m31454_MI,
	&m31455_MI,
	NULL
};
static TypeInfo* t6034_ITIs[] = 
{
	&t603_TI,
	&t6033_TI,
	&t6035_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6034_0_0_0;
extern Il2CppType t6034_1_0_0;
struct t6034;
extern Il2CppGenericClass t6034_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6034_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6034_MIs, t6034_PIs, NULL, NULL, NULL, NULL, NULL, &t6034_TI, t6034_ITIs, NULL, &t1908__CustomAttributeCache, &t6034_TI, &t6034_0_0_0, &t6034_1_0_0, NULL, &t6034_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6036_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int64>>
extern MethodInfo m31459_MI;
static PropertyInfo t6036____Count_PropertyInfo = 
{
	&t6036_TI, "Count", &m31459_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31460_MI;
static PropertyInfo t6036____IsReadOnly_PropertyInfo = 
{
	&t6036_TI, "IsReadOnly", &m31460_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6036_PIs[] =
{
	&t6036____Count_PropertyInfo,
	&t6036____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31459_GM;
MethodInfo m31459_MI = 
{
	"get_Count", NULL, &t6036_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31459_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31460_GM;
MethodInfo m31460_MI = 
{
	"get_IsReadOnly", NULL, &t6036_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31460_GM};
extern Il2CppType t1714_0_0_0;
extern Il2CppType t1714_0_0_0;
static ParameterInfo t6036_m31461_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1714_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31461_GM;
MethodInfo m31461_MI = 
{
	"Add", NULL, &t6036_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6036_m31461_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31461_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31462_GM;
MethodInfo m31462_MI = 
{
	"Clear", NULL, &t6036_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31462_GM};
extern Il2CppType t1714_0_0_0;
static ParameterInfo t6036_m31463_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1714_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31463_GM;
MethodInfo m31463_MI = 
{
	"Contains", NULL, &t6036_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6036_m31463_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31463_GM};
extern Il2CppType t3562_0_0_0;
extern Il2CppType t3562_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6036_m31464_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3562_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31464_GM;
MethodInfo m31464_MI = 
{
	"CopyTo", NULL, &t6036_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6036_m31464_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31464_GM};
extern Il2CppType t1714_0_0_0;
static ParameterInfo t6036_m31465_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1714_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31465_GM;
MethodInfo m31465_MI = 
{
	"Remove", NULL, &t6036_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6036_m31465_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31465_GM};
static MethodInfo* t6036_MIs[] =
{
	&m31459_MI,
	&m31460_MI,
	&m31461_MI,
	&m31462_MI,
	&m31463_MI,
	&m31464_MI,
	&m31465_MI,
	NULL
};
extern TypeInfo t6038_TI;
static TypeInfo* t6036_ITIs[] = 
{
	&t603_TI,
	&t6038_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6036_0_0_0;
extern Il2CppType t6036_1_0_0;
struct t6036;
extern Il2CppGenericClass t6036_GC;
TypeInfo t6036_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6036_MIs, t6036_PIs, NULL, NULL, NULL, NULL, NULL, &t6036_TI, t6036_ITIs, NULL, &EmptyCustomAttributesCache, &t6036_TI, &t6036_0_0_0, &t6036_1_0_0, NULL, &t6036_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Int64>>
extern Il2CppType t4668_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31466_GM;
MethodInfo m31466_MI = 
{
	"GetEnumerator", NULL, &t6038_TI, &t4668_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31466_GM};
static MethodInfo* t6038_MIs[] =
{
	&m31466_MI,
	NULL
};
static TypeInfo* t6038_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6038_0_0_0;
extern Il2CppType t6038_1_0_0;
struct t6038;
extern Il2CppGenericClass t6038_GC;
TypeInfo t6038_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6038_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6038_TI, t6038_ITIs, NULL, &EmptyCustomAttributesCache, &t6038_TI, &t6038_0_0_0, &t6038_1_0_0, NULL, &t6038_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4668_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Int64>>
extern MethodInfo m31467_MI;
static PropertyInfo t4668____Current_PropertyInfo = 
{
	&t4668_TI, "Current", &m31467_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4668_PIs[] =
{
	&t4668____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1714_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31467_GM;
MethodInfo m31467_MI = 
{
	"get_Current", NULL, &t4668_TI, &t1714_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31467_GM};
static MethodInfo* t4668_MIs[] =
{
	&m31467_MI,
	NULL
};
static TypeInfo* t4668_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4668_0_0_0;
extern Il2CppType t4668_1_0_0;
struct t4668;
extern Il2CppGenericClass t4668_GC;
TypeInfo t4668_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4668_MIs, t4668_PIs, NULL, NULL, NULL, NULL, NULL, &t4668_TI, t4668_ITIs, NULL, &EmptyCustomAttributesCache, &t4668_TI, &t4668_0_0_0, &t4668_1_0_0, NULL, &t4668_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1714_TI;



// Metadata Definition System.IEquatable`1<System.Int64>
extern Il2CppType t919_0_0_0;
static ParameterInfo t1714_m31468_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31468_GM;
MethodInfo m31468_MI = 
{
	"Equals", NULL, &t1714_TI, &t40_0_0_0, RuntimeInvoker_t40_t919, t1714_m31468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31468_GM};
static MethodInfo* t1714_MIs[] =
{
	&m31468_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1714_1_0_0;
struct t1714;
extern Il2CppGenericClass t1714_GC;
TypeInfo t1714_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1714_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1714_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1714_TI, &t1714_0_0_0, &t1714_1_0_0, NULL, &t1714_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3265.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3265_TI;
#include "t3265MD.h"

extern MethodInfo m18125_MI;
extern MethodInfo m24015_MI;
struct t20;
#define m24015(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int64>>
extern Il2CppType t20_0_0_1;
FieldInfo t3265_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3265_TI, offsetof(t3265, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3265_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3265_TI, offsetof(t3265, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3265_FIs[] =
{
	&t3265_f0_FieldInfo,
	&t3265_f1_FieldInfo,
	NULL
};
extern MethodInfo m18122_MI;
static PropertyInfo t3265____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3265_TI, "System.Collections.IEnumerator.Current", &m18122_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3265____Current_PropertyInfo = 
{
	&t3265_TI, "Current", &m18125_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3265_PIs[] =
{
	&t3265____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3265____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3265_m18121_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18121_GM;
MethodInfo m18121_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3265_m18121_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18121_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18122_GM;
MethodInfo m18122_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3265_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18122_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18123_GM;
MethodInfo m18123_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3265_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18123_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18124_GM;
MethodInfo m18124_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3265_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18124_GM};
extern Il2CppType t1714_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18125_GM;
MethodInfo m18125_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3265_TI, &t1714_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18125_GM};
static MethodInfo* t3265_MIs[] =
{
	&m18121_MI,
	&m18122_MI,
	&m18123_MI,
	&m18124_MI,
	&m18125_MI,
	NULL
};
extern MethodInfo m18124_MI;
extern MethodInfo m18123_MI;
static MethodInfo* t3265_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18122_MI,
	&m18124_MI,
	&m18123_MI,
	&m18125_MI,
};
static TypeInfo* t3265_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4668_TI,
};
static Il2CppInterfaceOffsetPair t3265_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4668_TI, 7},
};
extern TypeInfo t1714_TI;
static Il2CppRGCTXData t3265_RGCTXData[3] = 
{
	&m18125_MI/* Method Usage */,
	&t1714_TI/* Class Usage */,
	&m24015_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3265_0_0_0;
extern Il2CppType t3265_1_0_0;
extern Il2CppGenericClass t3265_GC;
TypeInfo t3265_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3265_MIs, t3265_PIs, t3265_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3265_TI, t3265_ITIs, t3265_VT, &EmptyCustomAttributesCache, &t3265_TI, &t3265_0_0_0, &t3265_1_0_0, t3265_IOs, &t3265_GC, NULL, NULL, NULL, t3265_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3265)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6037_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Int64>>
extern MethodInfo m31469_MI;
extern MethodInfo m31470_MI;
static PropertyInfo t6037____Item_PropertyInfo = 
{
	&t6037_TI, "Item", &m31469_MI, &m31470_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6037_PIs[] =
{
	&t6037____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1714_0_0_0;
static ParameterInfo t6037_m31471_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1714_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31471_GM;
MethodInfo m31471_MI = 
{
	"IndexOf", NULL, &t6037_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6037_m31471_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31471_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1714_0_0_0;
static ParameterInfo t6037_m31472_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1714_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31472_GM;
MethodInfo m31472_MI = 
{
	"Insert", NULL, &t6037_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6037_m31472_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31472_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6037_m31473_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31473_GM;
MethodInfo m31473_MI = 
{
	"RemoveAt", NULL, &t6037_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6037_m31473_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31473_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6037_m31469_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1714_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31469_GM;
MethodInfo m31469_MI = 
{
	"get_Item", NULL, &t6037_TI, &t1714_0_0_0, RuntimeInvoker_t29_t44, t6037_m31469_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31469_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1714_0_0_0;
static ParameterInfo t6037_m31470_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1714_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31470_GM;
MethodInfo m31470_MI = 
{
	"set_Item", NULL, &t6037_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6037_m31470_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31470_GM};
static MethodInfo* t6037_MIs[] =
{
	&m31471_MI,
	&m31472_MI,
	&m31473_MI,
	&m31469_MI,
	&m31470_MI,
	NULL
};
static TypeInfo* t6037_ITIs[] = 
{
	&t603_TI,
	&t6036_TI,
	&t6038_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6037_0_0_0;
extern Il2CppType t6037_1_0_0;
struct t6037;
extern Il2CppGenericClass t6037_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6037_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6037_MIs, t6037_PIs, NULL, NULL, NULL, NULL, NULL, &t6037_TI, t6037_ITIs, NULL, &t1908__CustomAttributeCache, &t6037_TI, &t6037_0_0_0, &t6037_1_0_0, NULL, &t6037_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4670_TI;

#include "t707.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.CLSCompliantAttribute>
extern MethodInfo m31474_MI;
static PropertyInfo t4670____Current_PropertyInfo = 
{
	&t4670_TI, "Current", &m31474_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4670_PIs[] =
{
	&t4670____Current_PropertyInfo,
	NULL
};
extern Il2CppType t707_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31474_GM;
MethodInfo m31474_MI = 
{
	"get_Current", NULL, &t4670_TI, &t707_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31474_GM};
static MethodInfo* t4670_MIs[] =
{
	&m31474_MI,
	NULL
};
static TypeInfo* t4670_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4670_0_0_0;
extern Il2CppType t4670_1_0_0;
struct t4670;
extern Il2CppGenericClass t4670_GC;
TypeInfo t4670_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4670_MIs, t4670_PIs, NULL, NULL, NULL, NULL, NULL, &t4670_TI, t4670_ITIs, NULL, &EmptyCustomAttributesCache, &t4670_TI, &t4670_0_0_0, &t4670_1_0_0, NULL, &t4670_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3266.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3266_TI;
#include "t3266MD.h"

extern TypeInfo t707_TI;
extern MethodInfo m18130_MI;
extern MethodInfo m24026_MI;
struct t20;
#define m24026(__this, p0, method) (t707 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.CLSCompliantAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3266_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3266_TI, offsetof(t3266, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3266_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3266_TI, offsetof(t3266, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3266_FIs[] =
{
	&t3266_f0_FieldInfo,
	&t3266_f1_FieldInfo,
	NULL
};
extern MethodInfo m18127_MI;
static PropertyInfo t3266____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3266_TI, "System.Collections.IEnumerator.Current", &m18127_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3266____Current_PropertyInfo = 
{
	&t3266_TI, "Current", &m18130_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3266_PIs[] =
{
	&t3266____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3266____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3266_m18126_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18126_GM;
MethodInfo m18126_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3266_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3266_m18126_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18126_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18127_GM;
MethodInfo m18127_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3266_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18127_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18128_GM;
MethodInfo m18128_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3266_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18128_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18129_GM;
MethodInfo m18129_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3266_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18129_GM};
extern Il2CppType t707_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18130_GM;
MethodInfo m18130_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3266_TI, &t707_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18130_GM};
static MethodInfo* t3266_MIs[] =
{
	&m18126_MI,
	&m18127_MI,
	&m18128_MI,
	&m18129_MI,
	&m18130_MI,
	NULL
};
extern MethodInfo m18129_MI;
extern MethodInfo m18128_MI;
static MethodInfo* t3266_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18127_MI,
	&m18129_MI,
	&m18128_MI,
	&m18130_MI,
};
static TypeInfo* t3266_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4670_TI,
};
static Il2CppInterfaceOffsetPair t3266_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4670_TI, 7},
};
extern TypeInfo t707_TI;
static Il2CppRGCTXData t3266_RGCTXData[3] = 
{
	&m18130_MI/* Method Usage */,
	&t707_TI/* Class Usage */,
	&m24026_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3266_0_0_0;
extern Il2CppType t3266_1_0_0;
extern Il2CppGenericClass t3266_GC;
TypeInfo t3266_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3266_MIs, t3266_PIs, t3266_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3266_TI, t3266_ITIs, t3266_VT, &EmptyCustomAttributesCache, &t3266_TI, &t3266_0_0_0, &t3266_1_0_0, t3266_IOs, &t3266_GC, NULL, NULL, NULL, t3266_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3266)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6039_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.CLSCompliantAttribute>
extern MethodInfo m31475_MI;
static PropertyInfo t6039____Count_PropertyInfo = 
{
	&t6039_TI, "Count", &m31475_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31476_MI;
static PropertyInfo t6039____IsReadOnly_PropertyInfo = 
{
	&t6039_TI, "IsReadOnly", &m31476_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6039_PIs[] =
{
	&t6039____Count_PropertyInfo,
	&t6039____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31475_GM;
MethodInfo m31475_MI = 
{
	"get_Count", NULL, &t6039_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31475_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31476_GM;
MethodInfo m31476_MI = 
{
	"get_IsReadOnly", NULL, &t6039_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31476_GM};
extern Il2CppType t707_0_0_0;
extern Il2CppType t707_0_0_0;
static ParameterInfo t6039_m31477_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t707_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31477_GM;
MethodInfo m31477_MI = 
{
	"Add", NULL, &t6039_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6039_m31477_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31477_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31478_GM;
MethodInfo m31478_MI = 
{
	"Clear", NULL, &t6039_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31478_GM};
extern Il2CppType t707_0_0_0;
static ParameterInfo t6039_m31479_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t707_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31479_GM;
MethodInfo m31479_MI = 
{
	"Contains", NULL, &t6039_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6039_m31479_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31479_GM};
extern Il2CppType t3563_0_0_0;
extern Il2CppType t3563_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6039_m31480_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3563_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31480_GM;
MethodInfo m31480_MI = 
{
	"CopyTo", NULL, &t6039_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6039_m31480_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31480_GM};
extern Il2CppType t707_0_0_0;
static ParameterInfo t6039_m31481_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t707_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31481_GM;
MethodInfo m31481_MI = 
{
	"Remove", NULL, &t6039_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6039_m31481_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31481_GM};
static MethodInfo* t6039_MIs[] =
{
	&m31475_MI,
	&m31476_MI,
	&m31477_MI,
	&m31478_MI,
	&m31479_MI,
	&m31480_MI,
	&m31481_MI,
	NULL
};
extern TypeInfo t6041_TI;
static TypeInfo* t6039_ITIs[] = 
{
	&t603_TI,
	&t6041_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6039_0_0_0;
extern Il2CppType t6039_1_0_0;
struct t6039;
extern Il2CppGenericClass t6039_GC;
TypeInfo t6039_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6039_MIs, t6039_PIs, NULL, NULL, NULL, NULL, NULL, &t6039_TI, t6039_ITIs, NULL, &EmptyCustomAttributesCache, &t6039_TI, &t6039_0_0_0, &t6039_1_0_0, NULL, &t6039_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.CLSCompliantAttribute>
extern Il2CppType t4670_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31482_GM;
MethodInfo m31482_MI = 
{
	"GetEnumerator", NULL, &t6041_TI, &t4670_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31482_GM};
static MethodInfo* t6041_MIs[] =
{
	&m31482_MI,
	NULL
};
static TypeInfo* t6041_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6041_0_0_0;
extern Il2CppType t6041_1_0_0;
struct t6041;
extern Il2CppGenericClass t6041_GC;
TypeInfo t6041_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6041_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6041_TI, t6041_ITIs, NULL, &EmptyCustomAttributesCache, &t6041_TI, &t6041_0_0_0, &t6041_1_0_0, NULL, &t6041_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6040_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.CLSCompliantAttribute>
extern MethodInfo m31483_MI;
extern MethodInfo m31484_MI;
static PropertyInfo t6040____Item_PropertyInfo = 
{
	&t6040_TI, "Item", &m31483_MI, &m31484_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6040_PIs[] =
{
	&t6040____Item_PropertyInfo,
	NULL
};
extern Il2CppType t707_0_0_0;
static ParameterInfo t6040_m31485_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t707_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31485_GM;
MethodInfo m31485_MI = 
{
	"IndexOf", NULL, &t6040_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6040_m31485_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31485_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t707_0_0_0;
static ParameterInfo t6040_m31486_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t707_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31486_GM;
MethodInfo m31486_MI = 
{
	"Insert", NULL, &t6040_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6040_m31486_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31486_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6040_m31487_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31487_GM;
MethodInfo m31487_MI = 
{
	"RemoveAt", NULL, &t6040_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6040_m31487_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31487_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6040_m31483_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t707_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31483_GM;
MethodInfo m31483_MI = 
{
	"get_Item", NULL, &t6040_TI, &t707_0_0_0, RuntimeInvoker_t29_t44, t6040_m31483_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31483_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t707_0_0_0;
static ParameterInfo t6040_m31484_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t707_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31484_GM;
MethodInfo m31484_MI = 
{
	"set_Item", NULL, &t6040_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6040_m31484_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31484_GM};
static MethodInfo* t6040_MIs[] =
{
	&m31485_MI,
	&m31486_MI,
	&m31487_MI,
	&m31483_MI,
	&m31484_MI,
	NULL
};
static TypeInfo* t6040_ITIs[] = 
{
	&t603_TI,
	&t6039_TI,
	&t6041_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6040_0_0_0;
extern Il2CppType t6040_1_0_0;
struct t6040;
extern Il2CppGenericClass t6040_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6040_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6040_MIs, t6040_PIs, NULL, NULL, NULL, NULL, NULL, &t6040_TI, t6040_ITIs, NULL, &t1908__CustomAttributeCache, &t6040_TI, &t6040_0_0_0, &t6040_1_0_0, NULL, &t6040_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4671_TI;

#include "t1089.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UInt64>
extern MethodInfo m31488_MI;
static PropertyInfo t4671____Current_PropertyInfo = 
{
	&t4671_TI, "Current", &m31488_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4671_PIs[] =
{
	&t4671____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1089_0_0_0;
extern void* RuntimeInvoker_t1089 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31488_GM;
MethodInfo m31488_MI = 
{
	"get_Current", NULL, &t4671_TI, &t1089_0_0_0, RuntimeInvoker_t1089, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31488_GM};
static MethodInfo* t4671_MIs[] =
{
	&m31488_MI,
	NULL
};
static TypeInfo* t4671_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4671_0_0_0;
extern Il2CppType t4671_1_0_0;
struct t4671;
extern Il2CppGenericClass t4671_GC;
TypeInfo t4671_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4671_MIs, t4671_PIs, NULL, NULL, NULL, NULL, NULL, &t4671_TI, t4671_ITIs, NULL, &EmptyCustomAttributesCache, &t4671_TI, &t4671_0_0_0, &t4671_1_0_0, NULL, &t4671_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3267.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3267_TI;
#include "t3267MD.h"

extern TypeInfo t1089_TI;
extern MethodInfo m18135_MI;
extern MethodInfo m24037_MI;
struct t20;
 uint64_t m24037 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18131_MI;
 void m18131 (t3267 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18132_MI;
 t29 * m18132 (t3267 * __this, MethodInfo* method){
	{
		uint64_t L_0 = m18135(__this, &m18135_MI);
		uint64_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1089_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18133_MI;
 void m18133 (t3267 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18134_MI;
 bool m18134 (t3267 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint64_t m18135 (t3267 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint64_t L_8 = m24037(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24037_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UInt64>
extern Il2CppType t20_0_0_1;
FieldInfo t3267_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3267_TI, offsetof(t3267, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3267_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3267_TI, offsetof(t3267, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3267_FIs[] =
{
	&t3267_f0_FieldInfo,
	&t3267_f1_FieldInfo,
	NULL
};
static PropertyInfo t3267____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3267_TI, "System.Collections.IEnumerator.Current", &m18132_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3267____Current_PropertyInfo = 
{
	&t3267_TI, "Current", &m18135_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3267_PIs[] =
{
	&t3267____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3267____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3267_m18131_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18131_GM;
MethodInfo m18131_MI = 
{
	".ctor", (methodPointerType)&m18131, &t3267_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3267_m18131_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18131_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18132_GM;
MethodInfo m18132_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18132, &t3267_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18132_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18133_GM;
MethodInfo m18133_MI = 
{
	"Dispose", (methodPointerType)&m18133, &t3267_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18133_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18134_GM;
MethodInfo m18134_MI = 
{
	"MoveNext", (methodPointerType)&m18134, &t3267_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18134_GM};
extern Il2CppType t1089_0_0_0;
extern void* RuntimeInvoker_t1089 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18135_GM;
MethodInfo m18135_MI = 
{
	"get_Current", (methodPointerType)&m18135, &t3267_TI, &t1089_0_0_0, RuntimeInvoker_t1089, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18135_GM};
static MethodInfo* t3267_MIs[] =
{
	&m18131_MI,
	&m18132_MI,
	&m18133_MI,
	&m18134_MI,
	&m18135_MI,
	NULL
};
static MethodInfo* t3267_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18132_MI,
	&m18134_MI,
	&m18133_MI,
	&m18135_MI,
};
static TypeInfo* t3267_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4671_TI,
};
static Il2CppInterfaceOffsetPair t3267_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4671_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3267_0_0_0;
extern Il2CppType t3267_1_0_0;
extern Il2CppGenericClass t3267_GC;
TypeInfo t3267_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3267_MIs, t3267_PIs, t3267_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3267_TI, t3267_ITIs, t3267_VT, &EmptyCustomAttributesCache, &t3267_TI, &t3267_0_0_0, &t3267_1_0_0, t3267_IOs, &t3267_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3267)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6042_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UInt64>
extern MethodInfo m31489_MI;
static PropertyInfo t6042____Count_PropertyInfo = 
{
	&t6042_TI, "Count", &m31489_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31490_MI;
static PropertyInfo t6042____IsReadOnly_PropertyInfo = 
{
	&t6042_TI, "IsReadOnly", &m31490_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6042_PIs[] =
{
	&t6042____Count_PropertyInfo,
	&t6042____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31489_GM;
MethodInfo m31489_MI = 
{
	"get_Count", NULL, &t6042_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31489_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31490_GM;
MethodInfo m31490_MI = 
{
	"get_IsReadOnly", NULL, &t6042_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31490_GM};
extern Il2CppType t1089_0_0_0;
extern Il2CppType t1089_0_0_0;
static ParameterInfo t6042_m31491_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31491_GM;
MethodInfo m31491_MI = 
{
	"Add", NULL, &t6042_TI, &t21_0_0_0, RuntimeInvoker_t21_t919, t6042_m31491_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31491_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31492_GM;
MethodInfo m31492_MI = 
{
	"Clear", NULL, &t6042_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31492_GM};
extern Il2CppType t1089_0_0_0;
static ParameterInfo t6042_m31493_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31493_GM;
MethodInfo m31493_MI = 
{
	"Contains", NULL, &t6042_TI, &t40_0_0_0, RuntimeInvoker_t40_t919, t6042_m31493_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31493_GM};
extern Il2CppType t1552_0_0_0;
extern Il2CppType t1552_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6042_m31494_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1552_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31494_GM;
MethodInfo m31494_MI = 
{
	"CopyTo", NULL, &t6042_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6042_m31494_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31494_GM};
extern Il2CppType t1089_0_0_0;
static ParameterInfo t6042_m31495_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31495_GM;
MethodInfo m31495_MI = 
{
	"Remove", NULL, &t6042_TI, &t40_0_0_0, RuntimeInvoker_t40_t919, t6042_m31495_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31495_GM};
static MethodInfo* t6042_MIs[] =
{
	&m31489_MI,
	&m31490_MI,
	&m31491_MI,
	&m31492_MI,
	&m31493_MI,
	&m31494_MI,
	&m31495_MI,
	NULL
};
extern TypeInfo t6044_TI;
static TypeInfo* t6042_ITIs[] = 
{
	&t603_TI,
	&t6044_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6042_0_0_0;
extern Il2CppType t6042_1_0_0;
struct t6042;
extern Il2CppGenericClass t6042_GC;
TypeInfo t6042_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6042_MIs, t6042_PIs, NULL, NULL, NULL, NULL, NULL, &t6042_TI, t6042_ITIs, NULL, &EmptyCustomAttributesCache, &t6042_TI, &t6042_0_0_0, &t6042_1_0_0, NULL, &t6042_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UInt64>
extern Il2CppType t4671_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31496_GM;
MethodInfo m31496_MI = 
{
	"GetEnumerator", NULL, &t6044_TI, &t4671_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31496_GM};
static MethodInfo* t6044_MIs[] =
{
	&m31496_MI,
	NULL
};
static TypeInfo* t6044_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6044_0_0_0;
extern Il2CppType t6044_1_0_0;
struct t6044;
extern Il2CppGenericClass t6044_GC;
TypeInfo t6044_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6044_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6044_TI, t6044_ITIs, NULL, &EmptyCustomAttributesCache, &t6044_TI, &t6044_0_0_0, &t6044_1_0_0, NULL, &t6044_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6043_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UInt64>
extern MethodInfo m31497_MI;
extern MethodInfo m31498_MI;
static PropertyInfo t6043____Item_PropertyInfo = 
{
	&t6043_TI, "Item", &m31497_MI, &m31498_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6043_PIs[] =
{
	&t6043____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1089_0_0_0;
static ParameterInfo t6043_m31499_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31499_GM;
MethodInfo m31499_MI = 
{
	"IndexOf", NULL, &t6043_TI, &t44_0_0_0, RuntimeInvoker_t44_t919, t6043_m31499_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31499_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1089_0_0_0;
static ParameterInfo t6043_m31500_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31500_GM;
MethodInfo m31500_MI = 
{
	"Insert", NULL, &t6043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t919, t6043_m31500_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31500_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6043_m31501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31501_GM;
MethodInfo m31501_MI = 
{
	"RemoveAt", NULL, &t6043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6043_m31501_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31501_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6043_m31497_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1089_0_0_0;
extern void* RuntimeInvoker_t1089_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31497_GM;
MethodInfo m31497_MI = 
{
	"get_Item", NULL, &t6043_TI, &t1089_0_0_0, RuntimeInvoker_t1089_t44, t6043_m31497_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31497_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1089_0_0_0;
static ParameterInfo t6043_m31498_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31498_GM;
MethodInfo m31498_MI = 
{
	"set_Item", NULL, &t6043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t919, t6043_m31498_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31498_GM};
static MethodInfo* t6043_MIs[] =
{
	&m31499_MI,
	&m31500_MI,
	&m31501_MI,
	&m31497_MI,
	&m31498_MI,
	NULL
};
static TypeInfo* t6043_ITIs[] = 
{
	&t603_TI,
	&t6042_TI,
	&t6044_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6043_0_0_0;
extern Il2CppType t6043_1_0_0;
struct t6043;
extern Il2CppGenericClass t6043_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6043_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6043_MIs, t6043_PIs, NULL, NULL, NULL, NULL, NULL, &t6043_TI, t6043_ITIs, NULL, &t1908__CustomAttributeCache, &t6043_TI, &t6043_0_0_0, &t6043_1_0_0, NULL, &t6043_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6045_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.UInt64>>
extern MethodInfo m31502_MI;
static PropertyInfo t6045____Count_PropertyInfo = 
{
	&t6045_TI, "Count", &m31502_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31503_MI;
static PropertyInfo t6045____IsReadOnly_PropertyInfo = 
{
	&t6045_TI, "IsReadOnly", &m31503_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6045_PIs[] =
{
	&t6045____Count_PropertyInfo,
	&t6045____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31502_GM;
MethodInfo m31502_MI = 
{
	"get_Count", NULL, &t6045_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31502_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31503_GM;
MethodInfo m31503_MI = 
{
	"get_IsReadOnly", NULL, &t6045_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31503_GM};
extern Il2CppType t1719_0_0_0;
extern Il2CppType t1719_0_0_0;
static ParameterInfo t6045_m31504_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1719_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31504_GM;
MethodInfo m31504_MI = 
{
	"Add", NULL, &t6045_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6045_m31504_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31504_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31505_GM;
MethodInfo m31505_MI = 
{
	"Clear", NULL, &t6045_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31505_GM};
extern Il2CppType t1719_0_0_0;
static ParameterInfo t6045_m31506_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1719_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31506_GM;
MethodInfo m31506_MI = 
{
	"Contains", NULL, &t6045_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6045_m31506_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31506_GM};
extern Il2CppType t3564_0_0_0;
extern Il2CppType t3564_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6045_m31507_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3564_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31507_GM;
MethodInfo m31507_MI = 
{
	"CopyTo", NULL, &t6045_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6045_m31507_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31507_GM};
extern Il2CppType t1719_0_0_0;
static ParameterInfo t6045_m31508_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1719_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31508_GM;
MethodInfo m31508_MI = 
{
	"Remove", NULL, &t6045_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6045_m31508_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31508_GM};
static MethodInfo* t6045_MIs[] =
{
	&m31502_MI,
	&m31503_MI,
	&m31504_MI,
	&m31505_MI,
	&m31506_MI,
	&m31507_MI,
	&m31508_MI,
	NULL
};
extern TypeInfo t6047_TI;
static TypeInfo* t6045_ITIs[] = 
{
	&t603_TI,
	&t6047_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6045_0_0_0;
extern Il2CppType t6045_1_0_0;
struct t6045;
extern Il2CppGenericClass t6045_GC;
TypeInfo t6045_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6045_MIs, t6045_PIs, NULL, NULL, NULL, NULL, NULL, &t6045_TI, t6045_ITIs, NULL, &EmptyCustomAttributesCache, &t6045_TI, &t6045_0_0_0, &t6045_1_0_0, NULL, &t6045_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.UInt64>>
extern Il2CppType t4673_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31509_GM;
MethodInfo m31509_MI = 
{
	"GetEnumerator", NULL, &t6047_TI, &t4673_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31509_GM};
static MethodInfo* t6047_MIs[] =
{
	&m31509_MI,
	NULL
};
static TypeInfo* t6047_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6047_0_0_0;
extern Il2CppType t6047_1_0_0;
struct t6047;
extern Il2CppGenericClass t6047_GC;
TypeInfo t6047_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6047_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6047_TI, t6047_ITIs, NULL, &EmptyCustomAttributesCache, &t6047_TI, &t6047_0_0_0, &t6047_1_0_0, NULL, &t6047_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4673_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.UInt64>>
extern MethodInfo m31510_MI;
static PropertyInfo t4673____Current_PropertyInfo = 
{
	&t4673_TI, "Current", &m31510_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4673_PIs[] =
{
	&t4673____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1719_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31510_GM;
MethodInfo m31510_MI = 
{
	"get_Current", NULL, &t4673_TI, &t1719_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31510_GM};
static MethodInfo* t4673_MIs[] =
{
	&m31510_MI,
	NULL
};
static TypeInfo* t4673_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4673_0_0_0;
extern Il2CppType t4673_1_0_0;
struct t4673;
extern Il2CppGenericClass t4673_GC;
TypeInfo t4673_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4673_MIs, t4673_PIs, NULL, NULL, NULL, NULL, NULL, &t4673_TI, t4673_ITIs, NULL, &EmptyCustomAttributesCache, &t4673_TI, &t4673_0_0_0, &t4673_1_0_0, NULL, &t4673_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1719_TI;



// Metadata Definition System.IComparable`1<System.UInt64>
extern Il2CppType t1089_0_0_0;
static ParameterInfo t1719_m31511_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31511_GM;
MethodInfo m31511_MI = 
{
	"CompareTo", NULL, &t1719_TI, &t44_0_0_0, RuntimeInvoker_t44_t919, t1719_m31511_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31511_GM};
static MethodInfo* t1719_MIs[] =
{
	&m31511_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1719_1_0_0;
struct t1719;
extern Il2CppGenericClass t1719_GC;
TypeInfo t1719_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1719_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1719_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1719_TI, &t1719_0_0_0, &t1719_1_0_0, NULL, &t1719_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3268.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3268_TI;
#include "t3268MD.h"

extern MethodInfo m18140_MI;
extern MethodInfo m24048_MI;
struct t20;
#define m24048(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.UInt64>>
extern Il2CppType t20_0_0_1;
FieldInfo t3268_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3268_TI, offsetof(t3268, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3268_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3268_TI, offsetof(t3268, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3268_FIs[] =
{
	&t3268_f0_FieldInfo,
	&t3268_f1_FieldInfo,
	NULL
};
extern MethodInfo m18137_MI;
static PropertyInfo t3268____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3268_TI, "System.Collections.IEnumerator.Current", &m18137_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3268____Current_PropertyInfo = 
{
	&t3268_TI, "Current", &m18140_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3268_PIs[] =
{
	&t3268____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3268____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3268_m18136_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18136_GM;
MethodInfo m18136_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3268_m18136_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18136_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18137_GM;
MethodInfo m18137_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3268_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18137_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18138_GM;
MethodInfo m18138_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18138_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18139_GM;
MethodInfo m18139_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3268_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18139_GM};
extern Il2CppType t1719_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18140_GM;
MethodInfo m18140_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3268_TI, &t1719_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18140_GM};
static MethodInfo* t3268_MIs[] =
{
	&m18136_MI,
	&m18137_MI,
	&m18138_MI,
	&m18139_MI,
	&m18140_MI,
	NULL
};
extern MethodInfo m18139_MI;
extern MethodInfo m18138_MI;
static MethodInfo* t3268_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18137_MI,
	&m18139_MI,
	&m18138_MI,
	&m18140_MI,
};
static TypeInfo* t3268_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4673_TI,
};
static Il2CppInterfaceOffsetPair t3268_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4673_TI, 7},
};
extern TypeInfo t1719_TI;
static Il2CppRGCTXData t3268_RGCTXData[3] = 
{
	&m18140_MI/* Method Usage */,
	&t1719_TI/* Class Usage */,
	&m24048_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3268_0_0_0;
extern Il2CppType t3268_1_0_0;
extern Il2CppGenericClass t3268_GC;
TypeInfo t3268_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3268_MIs, t3268_PIs, t3268_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3268_TI, t3268_ITIs, t3268_VT, &EmptyCustomAttributesCache, &t3268_TI, &t3268_0_0_0, &t3268_1_0_0, t3268_IOs, &t3268_GC, NULL, NULL, NULL, t3268_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3268)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6046_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.UInt64>>
extern MethodInfo m31512_MI;
extern MethodInfo m31513_MI;
static PropertyInfo t6046____Item_PropertyInfo = 
{
	&t6046_TI, "Item", &m31512_MI, &m31513_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6046_PIs[] =
{
	&t6046____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1719_0_0_0;
static ParameterInfo t6046_m31514_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1719_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31514_GM;
MethodInfo m31514_MI = 
{
	"IndexOf", NULL, &t6046_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6046_m31514_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31514_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1719_0_0_0;
static ParameterInfo t6046_m31515_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1719_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31515_GM;
MethodInfo m31515_MI = 
{
	"Insert", NULL, &t6046_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6046_m31515_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31515_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6046_m31516_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31516_GM;
MethodInfo m31516_MI = 
{
	"RemoveAt", NULL, &t6046_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6046_m31516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31516_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6046_m31512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1719_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31512_GM;
MethodInfo m31512_MI = 
{
	"get_Item", NULL, &t6046_TI, &t1719_0_0_0, RuntimeInvoker_t29_t44, t6046_m31512_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31512_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1719_0_0_0;
static ParameterInfo t6046_m31513_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1719_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31513_GM;
MethodInfo m31513_MI = 
{
	"set_Item", NULL, &t6046_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6046_m31513_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31513_GM};
static MethodInfo* t6046_MIs[] =
{
	&m31514_MI,
	&m31515_MI,
	&m31516_MI,
	&m31512_MI,
	&m31513_MI,
	NULL
};
static TypeInfo* t6046_ITIs[] = 
{
	&t603_TI,
	&t6045_TI,
	&t6047_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6046_0_0_0;
extern Il2CppType t6046_1_0_0;
struct t6046;
extern Il2CppGenericClass t6046_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6046_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6046_MIs, t6046_PIs, NULL, NULL, NULL, NULL, NULL, &t6046_TI, t6046_ITIs, NULL, &t1908__CustomAttributeCache, &t6046_TI, &t6046_0_0_0, &t6046_1_0_0, NULL, &t6046_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6048_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.UInt64>>
extern MethodInfo m31517_MI;
static PropertyInfo t6048____Count_PropertyInfo = 
{
	&t6048_TI, "Count", &m31517_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31518_MI;
static PropertyInfo t6048____IsReadOnly_PropertyInfo = 
{
	&t6048_TI, "IsReadOnly", &m31518_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6048_PIs[] =
{
	&t6048____Count_PropertyInfo,
	&t6048____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31517_GM;
MethodInfo m31517_MI = 
{
	"get_Count", NULL, &t6048_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31517_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31518_GM;
MethodInfo m31518_MI = 
{
	"get_IsReadOnly", NULL, &t6048_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31518_GM};
extern Il2CppType t1720_0_0_0;
extern Il2CppType t1720_0_0_0;
static ParameterInfo t6048_m31519_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1720_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31519_GM;
MethodInfo m31519_MI = 
{
	"Add", NULL, &t6048_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6048_m31519_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31519_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31520_GM;
MethodInfo m31520_MI = 
{
	"Clear", NULL, &t6048_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31520_GM};
extern Il2CppType t1720_0_0_0;
static ParameterInfo t6048_m31521_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1720_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31521_GM;
MethodInfo m31521_MI = 
{
	"Contains", NULL, &t6048_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6048_m31521_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31521_GM};
extern Il2CppType t3565_0_0_0;
extern Il2CppType t3565_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6048_m31522_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3565_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31522_GM;
MethodInfo m31522_MI = 
{
	"CopyTo", NULL, &t6048_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6048_m31522_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31522_GM};
extern Il2CppType t1720_0_0_0;
static ParameterInfo t6048_m31523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1720_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31523_GM;
MethodInfo m31523_MI = 
{
	"Remove", NULL, &t6048_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6048_m31523_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31523_GM};
static MethodInfo* t6048_MIs[] =
{
	&m31517_MI,
	&m31518_MI,
	&m31519_MI,
	&m31520_MI,
	&m31521_MI,
	&m31522_MI,
	&m31523_MI,
	NULL
};
extern TypeInfo t6050_TI;
static TypeInfo* t6048_ITIs[] = 
{
	&t603_TI,
	&t6050_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6048_0_0_0;
extern Il2CppType t6048_1_0_0;
struct t6048;
extern Il2CppGenericClass t6048_GC;
TypeInfo t6048_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6048_MIs, t6048_PIs, NULL, NULL, NULL, NULL, NULL, &t6048_TI, t6048_ITIs, NULL, &EmptyCustomAttributesCache, &t6048_TI, &t6048_0_0_0, &t6048_1_0_0, NULL, &t6048_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.UInt64>>
extern Il2CppType t4675_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31524_GM;
MethodInfo m31524_MI = 
{
	"GetEnumerator", NULL, &t6050_TI, &t4675_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31524_GM};
static MethodInfo* t6050_MIs[] =
{
	&m31524_MI,
	NULL
};
static TypeInfo* t6050_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6050_0_0_0;
extern Il2CppType t6050_1_0_0;
struct t6050;
extern Il2CppGenericClass t6050_GC;
TypeInfo t6050_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6050_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6050_TI, t6050_ITIs, NULL, &EmptyCustomAttributesCache, &t6050_TI, &t6050_0_0_0, &t6050_1_0_0, NULL, &t6050_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4675_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.UInt64>>
extern MethodInfo m31525_MI;
static PropertyInfo t4675____Current_PropertyInfo = 
{
	&t4675_TI, "Current", &m31525_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4675_PIs[] =
{
	&t4675____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1720_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31525_GM;
MethodInfo m31525_MI = 
{
	"get_Current", NULL, &t4675_TI, &t1720_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31525_GM};
static MethodInfo* t4675_MIs[] =
{
	&m31525_MI,
	NULL
};
static TypeInfo* t4675_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4675_0_0_0;
extern Il2CppType t4675_1_0_0;
struct t4675;
extern Il2CppGenericClass t4675_GC;
TypeInfo t4675_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4675_MIs, t4675_PIs, NULL, NULL, NULL, NULL, NULL, &t4675_TI, t4675_ITIs, NULL, &EmptyCustomAttributesCache, &t4675_TI, &t4675_0_0_0, &t4675_1_0_0, NULL, &t4675_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1720_TI;



// Metadata Definition System.IEquatable`1<System.UInt64>
extern Il2CppType t1089_0_0_0;
static ParameterInfo t1720_m31526_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31526_GM;
MethodInfo m31526_MI = 
{
	"Equals", NULL, &t1720_TI, &t40_0_0_0, RuntimeInvoker_t40_t919, t1720_m31526_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31526_GM};
static MethodInfo* t1720_MIs[] =
{
	&m31526_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1720_1_0_0;
struct t1720;
extern Il2CppGenericClass t1720_GC;
TypeInfo t1720_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1720_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1720_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1720_TI, &t1720_0_0_0, &t1720_1_0_0, NULL, &t1720_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3269.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3269_TI;
#include "t3269MD.h"

extern MethodInfo m18145_MI;
extern MethodInfo m24059_MI;
struct t20;
#define m24059(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.UInt64>>
extern Il2CppType t20_0_0_1;
FieldInfo t3269_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3269_TI, offsetof(t3269, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3269_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3269_TI, offsetof(t3269, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3269_FIs[] =
{
	&t3269_f0_FieldInfo,
	&t3269_f1_FieldInfo,
	NULL
};
extern MethodInfo m18142_MI;
static PropertyInfo t3269____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3269_TI, "System.Collections.IEnumerator.Current", &m18142_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3269____Current_PropertyInfo = 
{
	&t3269_TI, "Current", &m18145_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3269_PIs[] =
{
	&t3269____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3269____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3269_m18141_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18141_GM;
MethodInfo m18141_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3269_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3269_m18141_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18141_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18142_GM;
MethodInfo m18142_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3269_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18142_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18143_GM;
MethodInfo m18143_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3269_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18143_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18144_GM;
MethodInfo m18144_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3269_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18144_GM};
extern Il2CppType t1720_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18145_GM;
MethodInfo m18145_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3269_TI, &t1720_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18145_GM};
static MethodInfo* t3269_MIs[] =
{
	&m18141_MI,
	&m18142_MI,
	&m18143_MI,
	&m18144_MI,
	&m18145_MI,
	NULL
};
extern MethodInfo m18144_MI;
extern MethodInfo m18143_MI;
static MethodInfo* t3269_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18142_MI,
	&m18144_MI,
	&m18143_MI,
	&m18145_MI,
};
static TypeInfo* t3269_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4675_TI,
};
static Il2CppInterfaceOffsetPair t3269_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4675_TI, 7},
};
extern TypeInfo t1720_TI;
static Il2CppRGCTXData t3269_RGCTXData[3] = 
{
	&m18145_MI/* Method Usage */,
	&t1720_TI/* Class Usage */,
	&m24059_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3269_0_0_0;
extern Il2CppType t3269_1_0_0;
extern Il2CppGenericClass t3269_GC;
TypeInfo t3269_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3269_MIs, t3269_PIs, t3269_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3269_TI, t3269_ITIs, t3269_VT, &EmptyCustomAttributesCache, &t3269_TI, &t3269_0_0_0, &t3269_1_0_0, t3269_IOs, &t3269_GC, NULL, NULL, NULL, t3269_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3269)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6049_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.UInt64>>
extern MethodInfo m31527_MI;
extern MethodInfo m31528_MI;
static PropertyInfo t6049____Item_PropertyInfo = 
{
	&t6049_TI, "Item", &m31527_MI, &m31528_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6049_PIs[] =
{
	&t6049____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1720_0_0_0;
static ParameterInfo t6049_m31529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1720_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31529_GM;
MethodInfo m31529_MI = 
{
	"IndexOf", NULL, &t6049_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6049_m31529_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31529_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1720_0_0_0;
static ParameterInfo t6049_m31530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1720_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31530_GM;
MethodInfo m31530_MI = 
{
	"Insert", NULL, &t6049_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6049_m31530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31530_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6049_m31531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31531_GM;
MethodInfo m31531_MI = 
{
	"RemoveAt", NULL, &t6049_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6049_m31531_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31531_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6049_m31527_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1720_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31527_GM;
MethodInfo m31527_MI = 
{
	"get_Item", NULL, &t6049_TI, &t1720_0_0_0, RuntimeInvoker_t29_t44, t6049_m31527_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31527_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1720_0_0_0;
static ParameterInfo t6049_m31528_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1720_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31528_GM;
MethodInfo m31528_MI = 
{
	"set_Item", NULL, &t6049_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6049_m31528_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31528_GM};
static MethodInfo* t6049_MIs[] =
{
	&m31529_MI,
	&m31530_MI,
	&m31531_MI,
	&m31527_MI,
	&m31528_MI,
	NULL
};
static TypeInfo* t6049_ITIs[] = 
{
	&t603_TI,
	&t6048_TI,
	&t6050_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6049_0_0_0;
extern Il2CppType t6049_1_0_0;
struct t6049;
extern Il2CppGenericClass t6049_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6049_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6049_MIs, t6049_PIs, NULL, NULL, NULL, NULL, NULL, &t6049_TI, t6049_ITIs, NULL, &t1908__CustomAttributeCache, &t6049_TI, &t6049_0_0_0, &t6049_1_0_0, NULL, &t6049_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4676_TI;

#include "t297.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.SByte>
extern MethodInfo m31532_MI;
static PropertyInfo t4676____Current_PropertyInfo = 
{
	&t4676_TI, "Current", &m31532_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4676_PIs[] =
{
	&t4676____Current_PropertyInfo,
	NULL
};
extern Il2CppType t297_0_0_0;
extern void* RuntimeInvoker_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31532_GM;
MethodInfo m31532_MI = 
{
	"get_Current", NULL, &t4676_TI, &t297_0_0_0, RuntimeInvoker_t297, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31532_GM};
static MethodInfo* t4676_MIs[] =
{
	&m31532_MI,
	NULL
};
static TypeInfo* t4676_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4676_0_0_0;
extern Il2CppType t4676_1_0_0;
struct t4676;
extern Il2CppGenericClass t4676_GC;
TypeInfo t4676_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4676_MIs, t4676_PIs, NULL, NULL, NULL, NULL, NULL, &t4676_TI, t4676_ITIs, NULL, &EmptyCustomAttributesCache, &t4676_TI, &t4676_0_0_0, &t4676_1_0_0, NULL, &t4676_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3270.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3270_TI;
#include "t3270MD.h"

extern TypeInfo t297_TI;
extern MethodInfo m18150_MI;
extern MethodInfo m24070_MI;
struct t20;
 int8_t m24070 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18146_MI;
 void m18146 (t3270 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18147_MI;
 t29 * m18147 (t3270 * __this, MethodInfo* method){
	{
		int8_t L_0 = m18150(__this, &m18150_MI);
		int8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t297_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18148_MI;
 void m18148 (t3270 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18149_MI;
 bool m18149 (t3270 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int8_t m18150 (t3270 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int8_t L_8 = m24070(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24070_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.SByte>
extern Il2CppType t20_0_0_1;
FieldInfo t3270_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3270_TI, offsetof(t3270, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3270_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3270_TI, offsetof(t3270, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3270_FIs[] =
{
	&t3270_f0_FieldInfo,
	&t3270_f1_FieldInfo,
	NULL
};
static PropertyInfo t3270____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3270_TI, "System.Collections.IEnumerator.Current", &m18147_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3270____Current_PropertyInfo = 
{
	&t3270_TI, "Current", &m18150_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3270_PIs[] =
{
	&t3270____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3270____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3270_m18146_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18146_GM;
MethodInfo m18146_MI = 
{
	".ctor", (methodPointerType)&m18146, &t3270_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3270_m18146_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18146_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18147_GM;
MethodInfo m18147_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18147, &t3270_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18147_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18148_GM;
MethodInfo m18148_MI = 
{
	"Dispose", (methodPointerType)&m18148, &t3270_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18148_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18149_GM;
MethodInfo m18149_MI = 
{
	"MoveNext", (methodPointerType)&m18149, &t3270_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18149_GM};
extern Il2CppType t297_0_0_0;
extern void* RuntimeInvoker_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18150_GM;
MethodInfo m18150_MI = 
{
	"get_Current", (methodPointerType)&m18150, &t3270_TI, &t297_0_0_0, RuntimeInvoker_t297, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18150_GM};
static MethodInfo* t3270_MIs[] =
{
	&m18146_MI,
	&m18147_MI,
	&m18148_MI,
	&m18149_MI,
	&m18150_MI,
	NULL
};
static MethodInfo* t3270_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18147_MI,
	&m18149_MI,
	&m18148_MI,
	&m18150_MI,
};
static TypeInfo* t3270_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4676_TI,
};
static Il2CppInterfaceOffsetPair t3270_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4676_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3270_0_0_0;
extern Il2CppType t3270_1_0_0;
extern Il2CppGenericClass t3270_GC;
TypeInfo t3270_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3270_MIs, t3270_PIs, t3270_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3270_TI, t3270_ITIs, t3270_VT, &EmptyCustomAttributesCache, &t3270_TI, &t3270_0_0_0, &t3270_1_0_0, t3270_IOs, &t3270_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3270)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6051_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.SByte>
extern MethodInfo m31533_MI;
static PropertyInfo t6051____Count_PropertyInfo = 
{
	&t6051_TI, "Count", &m31533_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31534_MI;
static PropertyInfo t6051____IsReadOnly_PropertyInfo = 
{
	&t6051_TI, "IsReadOnly", &m31534_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6051_PIs[] =
{
	&t6051____Count_PropertyInfo,
	&t6051____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31533_GM;
MethodInfo m31533_MI = 
{
	"get_Count", NULL, &t6051_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31533_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31534_GM;
MethodInfo m31534_MI = 
{
	"get_IsReadOnly", NULL, &t6051_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31534_GM};
extern Il2CppType t297_0_0_0;
extern Il2CppType t297_0_0_0;
static ParameterInfo t6051_m31535_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31535_GM;
MethodInfo m31535_MI = 
{
	"Add", NULL, &t6051_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t6051_m31535_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31535_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31536_GM;
MethodInfo m31536_MI = 
{
	"Clear", NULL, &t6051_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31536_GM};
extern Il2CppType t297_0_0_0;
static ParameterInfo t6051_m31537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31537_GM;
MethodInfo m31537_MI = 
{
	"Contains", NULL, &t6051_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t6051_m31537_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31537_GM};
extern Il2CppType t1595_0_0_0;
extern Il2CppType t1595_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6051_m31538_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1595_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31538_GM;
MethodInfo m31538_MI = 
{
	"CopyTo", NULL, &t6051_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6051_m31538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31538_GM};
extern Il2CppType t297_0_0_0;
static ParameterInfo t6051_m31539_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31539_GM;
MethodInfo m31539_MI = 
{
	"Remove", NULL, &t6051_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t6051_m31539_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31539_GM};
static MethodInfo* t6051_MIs[] =
{
	&m31533_MI,
	&m31534_MI,
	&m31535_MI,
	&m31536_MI,
	&m31537_MI,
	&m31538_MI,
	&m31539_MI,
	NULL
};
extern TypeInfo t6053_TI;
static TypeInfo* t6051_ITIs[] = 
{
	&t603_TI,
	&t6053_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6051_0_0_0;
extern Il2CppType t6051_1_0_0;
struct t6051;
extern Il2CppGenericClass t6051_GC;
TypeInfo t6051_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6051_MIs, t6051_PIs, NULL, NULL, NULL, NULL, NULL, &t6051_TI, t6051_ITIs, NULL, &EmptyCustomAttributesCache, &t6051_TI, &t6051_0_0_0, &t6051_1_0_0, NULL, &t6051_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.SByte>
extern Il2CppType t4676_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31540_GM;
MethodInfo m31540_MI = 
{
	"GetEnumerator", NULL, &t6053_TI, &t4676_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31540_GM};
static MethodInfo* t6053_MIs[] =
{
	&m31540_MI,
	NULL
};
static TypeInfo* t6053_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6053_0_0_0;
extern Il2CppType t6053_1_0_0;
struct t6053;
extern Il2CppGenericClass t6053_GC;
TypeInfo t6053_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6053_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6053_TI, t6053_ITIs, NULL, &EmptyCustomAttributesCache, &t6053_TI, &t6053_0_0_0, &t6053_1_0_0, NULL, &t6053_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6052_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.SByte>
extern MethodInfo m31541_MI;
extern MethodInfo m31542_MI;
static PropertyInfo t6052____Item_PropertyInfo = 
{
	&t6052_TI, "Item", &m31541_MI, &m31542_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6052_PIs[] =
{
	&t6052____Item_PropertyInfo,
	NULL
};
extern Il2CppType t297_0_0_0;
static ParameterInfo t6052_m31543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31543_GM;
MethodInfo m31543_MI = 
{
	"IndexOf", NULL, &t6052_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t6052_m31543_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31543_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t297_0_0_0;
static ParameterInfo t6052_m31544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31544_GM;
MethodInfo m31544_MI = 
{
	"Insert", NULL, &t6052_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t6052_m31544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31544_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6052_m31545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31545_GM;
MethodInfo m31545_MI = 
{
	"RemoveAt", NULL, &t6052_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6052_m31545_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31545_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6052_m31541_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t297_0_0_0;
extern void* RuntimeInvoker_t297_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31541_GM;
MethodInfo m31541_MI = 
{
	"get_Item", NULL, &t6052_TI, &t297_0_0_0, RuntimeInvoker_t297_t44, t6052_m31541_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31541_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t297_0_0_0;
static ParameterInfo t6052_m31542_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31542_GM;
MethodInfo m31542_MI = 
{
	"set_Item", NULL, &t6052_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t6052_m31542_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31542_GM};
static MethodInfo* t6052_MIs[] =
{
	&m31543_MI,
	&m31544_MI,
	&m31545_MI,
	&m31541_MI,
	&m31542_MI,
	NULL
};
static TypeInfo* t6052_ITIs[] = 
{
	&t603_TI,
	&t6051_TI,
	&t6053_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6052_0_0_0;
extern Il2CppType t6052_1_0_0;
struct t6052;
extern Il2CppGenericClass t6052_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6052_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6052_MIs, t6052_PIs, NULL, NULL, NULL, NULL, NULL, &t6052_TI, t6052_ITIs, NULL, &t1908__CustomAttributeCache, &t6052_TI, &t6052_0_0_0, &t6052_1_0_0, NULL, &t6052_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6054_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.SByte>>
extern MethodInfo m31546_MI;
static PropertyInfo t6054____Count_PropertyInfo = 
{
	&t6054_TI, "Count", &m31546_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31547_MI;
static PropertyInfo t6054____IsReadOnly_PropertyInfo = 
{
	&t6054_TI, "IsReadOnly", &m31547_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6054_PIs[] =
{
	&t6054____Count_PropertyInfo,
	&t6054____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31546_GM;
MethodInfo m31546_MI = 
{
	"get_Count", NULL, &t6054_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31546_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31547_GM;
MethodInfo m31547_MI = 
{
	"get_IsReadOnly", NULL, &t6054_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31547_GM};
extern Il2CppType t1724_0_0_0;
extern Il2CppType t1724_0_0_0;
static ParameterInfo t6054_m31548_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1724_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31548_GM;
MethodInfo m31548_MI = 
{
	"Add", NULL, &t6054_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6054_m31548_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31548_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31549_GM;
MethodInfo m31549_MI = 
{
	"Clear", NULL, &t6054_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31549_GM};
extern Il2CppType t1724_0_0_0;
static ParameterInfo t6054_m31550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1724_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31550_GM;
MethodInfo m31550_MI = 
{
	"Contains", NULL, &t6054_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6054_m31550_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31550_GM};
extern Il2CppType t3566_0_0_0;
extern Il2CppType t3566_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6054_m31551_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3566_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31551_GM;
MethodInfo m31551_MI = 
{
	"CopyTo", NULL, &t6054_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6054_m31551_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31551_GM};
extern Il2CppType t1724_0_0_0;
static ParameterInfo t6054_m31552_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1724_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31552_GM;
MethodInfo m31552_MI = 
{
	"Remove", NULL, &t6054_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6054_m31552_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31552_GM};
static MethodInfo* t6054_MIs[] =
{
	&m31546_MI,
	&m31547_MI,
	&m31548_MI,
	&m31549_MI,
	&m31550_MI,
	&m31551_MI,
	&m31552_MI,
	NULL
};
extern TypeInfo t6056_TI;
static TypeInfo* t6054_ITIs[] = 
{
	&t603_TI,
	&t6056_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6054_0_0_0;
extern Il2CppType t6054_1_0_0;
struct t6054;
extern Il2CppGenericClass t6054_GC;
TypeInfo t6054_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6054_MIs, t6054_PIs, NULL, NULL, NULL, NULL, NULL, &t6054_TI, t6054_ITIs, NULL, &EmptyCustomAttributesCache, &t6054_TI, &t6054_0_0_0, &t6054_1_0_0, NULL, &t6054_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.SByte>>
extern Il2CppType t4678_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31553_GM;
MethodInfo m31553_MI = 
{
	"GetEnumerator", NULL, &t6056_TI, &t4678_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31553_GM};
static MethodInfo* t6056_MIs[] =
{
	&m31553_MI,
	NULL
};
static TypeInfo* t6056_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6056_0_0_0;
extern Il2CppType t6056_1_0_0;
struct t6056;
extern Il2CppGenericClass t6056_GC;
TypeInfo t6056_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6056_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6056_TI, t6056_ITIs, NULL, &EmptyCustomAttributesCache, &t6056_TI, &t6056_0_0_0, &t6056_1_0_0, NULL, &t6056_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4678_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.SByte>>
extern MethodInfo m31554_MI;
static PropertyInfo t4678____Current_PropertyInfo = 
{
	&t4678_TI, "Current", &m31554_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4678_PIs[] =
{
	&t4678____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1724_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31554_GM;
MethodInfo m31554_MI = 
{
	"get_Current", NULL, &t4678_TI, &t1724_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31554_GM};
static MethodInfo* t4678_MIs[] =
{
	&m31554_MI,
	NULL
};
static TypeInfo* t4678_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4678_0_0_0;
extern Il2CppType t4678_1_0_0;
struct t4678;
extern Il2CppGenericClass t4678_GC;
TypeInfo t4678_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4678_MIs, t4678_PIs, NULL, NULL, NULL, NULL, NULL, &t4678_TI, t4678_ITIs, NULL, &EmptyCustomAttributesCache, &t4678_TI, &t4678_0_0_0, &t4678_1_0_0, NULL, &t4678_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1724_TI;



// Metadata Definition System.IComparable`1<System.SByte>
extern Il2CppType t297_0_0_0;
static ParameterInfo t1724_m31555_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31555_GM;
MethodInfo m31555_MI = 
{
	"CompareTo", NULL, &t1724_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t1724_m31555_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31555_GM};
static MethodInfo* t1724_MIs[] =
{
	&m31555_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1724_1_0_0;
struct t1724;
extern Il2CppGenericClass t1724_GC;
TypeInfo t1724_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1724_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1724_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1724_TI, &t1724_0_0_0, &t1724_1_0_0, NULL, &t1724_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3271.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3271_TI;
#include "t3271MD.h"

extern MethodInfo m18155_MI;
extern MethodInfo m24081_MI;
struct t20;
#define m24081(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.SByte>>
extern Il2CppType t20_0_0_1;
FieldInfo t3271_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3271_TI, offsetof(t3271, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3271_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3271_TI, offsetof(t3271, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3271_FIs[] =
{
	&t3271_f0_FieldInfo,
	&t3271_f1_FieldInfo,
	NULL
};
extern MethodInfo m18152_MI;
static PropertyInfo t3271____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3271_TI, "System.Collections.IEnumerator.Current", &m18152_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3271____Current_PropertyInfo = 
{
	&t3271_TI, "Current", &m18155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3271_PIs[] =
{
	&t3271____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3271____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3271_m18151_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18151_GM;
MethodInfo m18151_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3271_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3271_m18151_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18151_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18152_GM;
MethodInfo m18152_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3271_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18152_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18153_GM;
MethodInfo m18153_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3271_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18153_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18154_GM;
MethodInfo m18154_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3271_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18154_GM};
extern Il2CppType t1724_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18155_GM;
MethodInfo m18155_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3271_TI, &t1724_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18155_GM};
static MethodInfo* t3271_MIs[] =
{
	&m18151_MI,
	&m18152_MI,
	&m18153_MI,
	&m18154_MI,
	&m18155_MI,
	NULL
};
extern MethodInfo m18154_MI;
extern MethodInfo m18153_MI;
static MethodInfo* t3271_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18152_MI,
	&m18154_MI,
	&m18153_MI,
	&m18155_MI,
};
static TypeInfo* t3271_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4678_TI,
};
static Il2CppInterfaceOffsetPair t3271_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4678_TI, 7},
};
extern TypeInfo t1724_TI;
static Il2CppRGCTXData t3271_RGCTXData[3] = 
{
	&m18155_MI/* Method Usage */,
	&t1724_TI/* Class Usage */,
	&m24081_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3271_0_0_0;
extern Il2CppType t3271_1_0_0;
extern Il2CppGenericClass t3271_GC;
TypeInfo t3271_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3271_MIs, t3271_PIs, t3271_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3271_TI, t3271_ITIs, t3271_VT, &EmptyCustomAttributesCache, &t3271_TI, &t3271_0_0_0, &t3271_1_0_0, t3271_IOs, &t3271_GC, NULL, NULL, NULL, t3271_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3271)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6055_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.SByte>>
extern MethodInfo m31556_MI;
extern MethodInfo m31557_MI;
static PropertyInfo t6055____Item_PropertyInfo = 
{
	&t6055_TI, "Item", &m31556_MI, &m31557_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6055_PIs[] =
{
	&t6055____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1724_0_0_0;
static ParameterInfo t6055_m31558_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1724_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31558_GM;
MethodInfo m31558_MI = 
{
	"IndexOf", NULL, &t6055_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6055_m31558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31558_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1724_0_0_0;
static ParameterInfo t6055_m31559_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1724_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31559_GM;
MethodInfo m31559_MI = 
{
	"Insert", NULL, &t6055_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6055_m31559_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31559_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6055_m31560_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31560_GM;
MethodInfo m31560_MI = 
{
	"RemoveAt", NULL, &t6055_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6055_m31560_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31560_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6055_m31556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1724_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31556_GM;
MethodInfo m31556_MI = 
{
	"get_Item", NULL, &t6055_TI, &t1724_0_0_0, RuntimeInvoker_t29_t44, t6055_m31556_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1724_0_0_0;
static ParameterInfo t6055_m31557_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1724_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31557_GM;
MethodInfo m31557_MI = 
{
	"set_Item", NULL, &t6055_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6055_m31557_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31557_GM};
static MethodInfo* t6055_MIs[] =
{
	&m31558_MI,
	&m31559_MI,
	&m31560_MI,
	&m31556_MI,
	&m31557_MI,
	NULL
};
static TypeInfo* t6055_ITIs[] = 
{
	&t603_TI,
	&t6054_TI,
	&t6056_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6055_0_0_0;
extern Il2CppType t6055_1_0_0;
struct t6055;
extern Il2CppGenericClass t6055_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6055_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6055_MIs, t6055_PIs, NULL, NULL, NULL, NULL, NULL, &t6055_TI, t6055_ITIs, NULL, &t1908__CustomAttributeCache, &t6055_TI, &t6055_0_0_0, &t6055_1_0_0, NULL, &t6055_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6057_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.SByte>>
extern MethodInfo m31561_MI;
static PropertyInfo t6057____Count_PropertyInfo = 
{
	&t6057_TI, "Count", &m31561_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31562_MI;
static PropertyInfo t6057____IsReadOnly_PropertyInfo = 
{
	&t6057_TI, "IsReadOnly", &m31562_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6057_PIs[] =
{
	&t6057____Count_PropertyInfo,
	&t6057____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31561_GM;
MethodInfo m31561_MI = 
{
	"get_Count", NULL, &t6057_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31561_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31562_GM;
MethodInfo m31562_MI = 
{
	"get_IsReadOnly", NULL, &t6057_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31562_GM};
extern Il2CppType t1725_0_0_0;
extern Il2CppType t1725_0_0_0;
static ParameterInfo t6057_m31563_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1725_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31563_GM;
MethodInfo m31563_MI = 
{
	"Add", NULL, &t6057_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6057_m31563_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31563_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31564_GM;
MethodInfo m31564_MI = 
{
	"Clear", NULL, &t6057_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31564_GM};
extern Il2CppType t1725_0_0_0;
static ParameterInfo t6057_m31565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1725_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31565_GM;
MethodInfo m31565_MI = 
{
	"Contains", NULL, &t6057_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6057_m31565_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31565_GM};
extern Il2CppType t3567_0_0_0;
extern Il2CppType t3567_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6057_m31566_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3567_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31566_GM;
MethodInfo m31566_MI = 
{
	"CopyTo", NULL, &t6057_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6057_m31566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31566_GM};
extern Il2CppType t1725_0_0_0;
static ParameterInfo t6057_m31567_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1725_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31567_GM;
MethodInfo m31567_MI = 
{
	"Remove", NULL, &t6057_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6057_m31567_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31567_GM};
static MethodInfo* t6057_MIs[] =
{
	&m31561_MI,
	&m31562_MI,
	&m31563_MI,
	&m31564_MI,
	&m31565_MI,
	&m31566_MI,
	&m31567_MI,
	NULL
};
extern TypeInfo t6059_TI;
static TypeInfo* t6057_ITIs[] = 
{
	&t603_TI,
	&t6059_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6057_0_0_0;
extern Il2CppType t6057_1_0_0;
struct t6057;
extern Il2CppGenericClass t6057_GC;
TypeInfo t6057_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6057_MIs, t6057_PIs, NULL, NULL, NULL, NULL, NULL, &t6057_TI, t6057_ITIs, NULL, &EmptyCustomAttributesCache, &t6057_TI, &t6057_0_0_0, &t6057_1_0_0, NULL, &t6057_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
