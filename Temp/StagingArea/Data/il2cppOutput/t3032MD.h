﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3032;
struct t29;
struct t388;
struct t20;
struct t136;
struct t530;
struct t3029;
#include "t381.h"

 void m16583 (t3032 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16584 (t3032 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16585 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16586 (t3032 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16587 (t3032 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16588 (t3032 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16589 (t3032 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16590 (t3032 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16591 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16592 (t3032 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16593 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16594 (t3032 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16595 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16596 (t3032 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16597 (t3032 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16598 (t3032 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16599 (t3032 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16600 (t3032 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16601 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16602 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16603 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16604 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16605 (t3032 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16606 (t3032 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16607 (t3032 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16608 (t3032 * __this, t530* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m16609 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16610 (t3032 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16611 (t3032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16612 (t3032 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
