﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1594;
struct t29;
struct t200;
struct t781;
struct t1305;
struct t7;

 void m8698 (t1594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8699 (t1594 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8700 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8701 (t1594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8702 (t1594 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8703 (t29 * __this, t200* p0, int32_t p1, int32_t p2, bool p3, int32_t p4, bool p5, bool p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8704 (t1594 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8705 (t29 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, bool p5, int32_t* p6, bool* p7, bool p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8706 (t1594 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8707 (t29 * __this, t781* p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8708 (t1594 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8709 (t29 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, int32_t* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8710 (t1594 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8711 (t1594 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8712 (t1594 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1305 * m8713 (t1594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8714 (t1594 * __this, uint16_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8715 (t1594 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8716 (t1594 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8717 (t1594 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8718 (t1594 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
