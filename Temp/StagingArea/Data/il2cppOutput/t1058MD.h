﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1058;
struct t29;
struct t745;
struct t841;
struct t66;
struct t67;
#include "t35.h"

 void m5084 (t1058 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5085 (t1058 * __this, t745 * p0, t841* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5086 (t1058 * __this, t745 * p0, t841* p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5087 (t1058 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
