﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t23;
struct t29;
struct t7;
#include "t23.h"

 void m49 (t23 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1583 (t23 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1849 (t29 * __this, t23  p0, t23  p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1852 (t23 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1853 (t23 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2321 (t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2322 (t23 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2323 (t29 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1883 (t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2324 (t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2325 (t23 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1622 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1475 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2326 (t29 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2327 (t29 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1887 (t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1862 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1863 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1395 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1620 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2328 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1892 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1893 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1890 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1891 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1803 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1624 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1925 (t29 * __this, t23  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2329 (t29 * __this, t23  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2330 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1788 (t29 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
