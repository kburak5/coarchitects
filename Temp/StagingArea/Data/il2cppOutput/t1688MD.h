﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1688;
struct t1688_marshaled;

void t1688_marshal(const t1688& unmarshaled, t1688_marshaled& marshaled);
void t1688_marshal_back(const t1688_marshaled& marshaled, t1688& unmarshaled);
void t1688_marshal_cleanup(t1688_marshaled& marshaled);
