﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3287;
struct t29;
struct t20;
#include "t1131.h"

 void m18307 (t3287 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18308 (t3287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18309 (t3287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18310 (t3287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1131  m18311 (t3287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
