﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t855;
struct t29;

 void m3600 (t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3601 (t855 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3602 (t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3603 (t855 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3604 (t855 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3605 (t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3606 (t855 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
