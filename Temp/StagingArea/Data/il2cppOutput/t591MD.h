﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t591;
struct t29;
struct t7;

 void m9503 (t591 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9504 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t591 * m4027 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t591 * m2862 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9505 (t591 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9506 (t591 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9507 (t591 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
