﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2194;
struct t29;

 void m10738_gshared (t2194 * __this, MethodInfo* method);
#define m10738(__this, method) (void)m10738_gshared((t2194 *)__this, method)
 int32_t m10739_gshared (t2194 * __this, t29 * p0, MethodInfo* method);
#define m10739(__this, p0, method) (int32_t)m10739_gshared((t2194 *)__this, (t29 *)p0, method)
 bool m10740_gshared (t2194 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m10740(__this, p0, p1, method) (bool)m10740_gshared((t2194 *)__this, (t29 *)p0, (t29 *)p1, method)
