﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2598;
struct t29;
struct t20;
#include "t177.h"

 void m14001 (t2598 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14002 (t2598 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14003 (t2598 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14004 (t2598 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14005 (t2598 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
