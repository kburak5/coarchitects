﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3479;
struct t29;
struct t20;
#include "t1600.h"

 void m19342 (t3479 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19343 (t3479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19344 (t3479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19345 (t3479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19346 (t3479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
