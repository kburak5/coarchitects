﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3052;
struct t29;
struct t20;
#include "t361.h"

 void m16813 (t3052 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16814 (t3052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16815 (t3052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16816 (t3052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16817 (t3052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
