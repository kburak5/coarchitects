﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3131;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m17286 (t3131 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17287 (t3131 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17288 (t3131 * __this, int32_t p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17289 (t3131 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
