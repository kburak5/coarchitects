﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t22;
struct t1094;
struct t29;
struct t42;
struct t7;
#include "t465.h"
#include "t1126.h"
#include "t1127.h"

 bool m5639 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5640 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5641 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5642 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5643 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5644 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5645 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5646 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5647 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5648 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5649 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5650 (float* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5651 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5652 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5653 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5654 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5655 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1304 (float* __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2870 (float* __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2841 (float* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5656 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5657 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5658 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5659 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5660 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m65 (float* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5661 (float* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2871 (float* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5662 (float* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5663 (float* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
