﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1048;
struct t1034;
struct t781;
struct t29;
struct t295;
struct t1050;
struct t67;

 void m4771 (t1048 * __this, t67 * p0, t29 * p1, t781* p2, t1034 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1034 * m4772 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4773 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4774 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4775 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m4776 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4777 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1050 * m4778 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4779 (t1048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4780 (t1048 * __this, t295 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4781 (t1048 * __this, t295 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4782 (t1048 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
