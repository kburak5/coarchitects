﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1410;
struct t20;
struct t200;
#include "t35.h"

 void m7747 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7748 (t29 * __this, t35 p0, int32_t p1, t20 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7749 (t29 * __this, t35 p0, t200* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
