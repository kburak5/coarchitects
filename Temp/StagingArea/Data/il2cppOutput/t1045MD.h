﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1045;
struct t1034;
struct t762;
struct t761;
struct t781;
struct t745;
struct t841;
struct t7;
struct t777;

 void m4766 (t1045 * __this, t1034 * p0, t762 * p1, t761 * p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4767 (t1045 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4768 (t1045 * __this, t745 * p0, t841* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4769 (t29 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m4770 (t29 * __this, t745 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
