﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2426;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m12567 (t2426 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12568 (t2426 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12569 (t2426 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12570 (t2426 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
