﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3106;
struct t204;
struct t204_marshaled;
struct t7;
#include "t552.h"

 void m17158 (t3106 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t204 * m17159 (t3106 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17160 (t3106 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17161 (t3106 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17162 (t3106 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m17163 (t3106 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
