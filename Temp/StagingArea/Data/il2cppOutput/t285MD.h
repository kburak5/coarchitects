﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t285;
struct t163;
#include "t132.h"
#include "t17.h"
#include "t287.h"

 void m1239 (t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1240 (t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1241 (t285 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1242 (t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1243 (t285 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1244 (t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1245 (t285 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1246 (t285 * __this, t163 * p0, t287  p1, int32_t p2, int32_t p3, float p4, float p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1247 (t285 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
