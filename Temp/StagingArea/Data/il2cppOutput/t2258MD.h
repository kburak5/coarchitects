﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2258;
struct t29;
struct t20;
#include "t57.h"

 void m11303 (t2258 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11304 (t2258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11305 (t2258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11306 (t2258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11307 (t2258 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
