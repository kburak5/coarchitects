﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3484;
struct t29;
#include "t465.h"

 void m19366 (t3484 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19367 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19368 (t3484 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19369 (t3484 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3484 * m19370 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
