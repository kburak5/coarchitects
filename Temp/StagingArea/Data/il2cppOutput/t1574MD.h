﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1574;
struct t200;
struct t7;
struct t781;
struct t1575;
struct t1576;
struct t1305;

 void m8531 (t1574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8532 (t1574 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8533 (t1574 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8534 (t1574 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8535 (t1574 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, t1575 ** p5, t200** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8536 (t1574 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8537 (t1574 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, t1575 ** p5, t200** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8538 (t1574 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8539 (t1574 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8540 (t1574 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, t1576 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8541 (t1574 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8542 (t1574 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8543 (t1574 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8544 (t1574 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8545 (t1574 * __this, uint16_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1305 * m8546 (t1574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
