﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2914;
struct t29;
struct t20;
#include "t2911.h"

 void m15952 (t2914 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15953 (t2914 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15954 (t2914 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15955 (t2914 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2911  m15956 (t2914 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
