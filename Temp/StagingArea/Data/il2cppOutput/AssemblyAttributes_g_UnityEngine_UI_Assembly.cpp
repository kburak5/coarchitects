﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo t429_TI;
#include "t429.h"
#include "t429MD.h"
extern MethodInfo m2047_MI;
extern TypeInfo t430_TI;
#include "t430.h"
#include "t430MD.h"
extern MethodInfo m2048_MI;
extern TypeInfo t431_TI;
#include "t431.h"
#include "t431MD.h"
extern MethodInfo m2049_MI;
extern TypeInfo t432_TI;
#include "t432.h"
#include "t432MD.h"
extern MethodInfo m2050_MI;
extern TypeInfo t433_TI;
#include "t433.h"
#include "t433MD.h"
extern MethodInfo m2051_MI;
extern TypeInfo t434_TI;
#include "t434.h"
#include "t434MD.h"
extern MethodInfo m2052_MI;
extern TypeInfo t46_TI;
#include "t46.h"
#include "t46MD.h"
extern MethodInfo m71_MI;
extern MethodInfo m72_MI;
extern TypeInfo t435_TI;
#include "t435.h"
#include "t435MD.h"
extern MethodInfo m2053_MI;
extern TypeInfo t436_TI;
#include "t436.h"
#include "t436MD.h"
extern MethodInfo m2054_MI;
extern TypeInfo t437_TI;
#include "t437.h"
#include "t437MD.h"
extern MethodInfo m2055_MI;
extern TypeInfo t438_TI;
#include "t438.h"
#include "t438MD.h"
extern MethodInfo m2056_MI;
void g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t429 * tmp;
		tmp = (t429 *)il2cpp_codegen_object_new (&t429_TI);
		m2047(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), &m2047_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t430 * tmp;
		tmp = (t430 *)il2cpp_codegen_object_new (&t430_TI);
		m2048(tmp, il2cpp_codegen_string_new_wrapper(""), &m2048_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t431 * tmp;
		tmp = (t431 *)il2cpp_codegen_object_new (&t431_TI);
		m2049(tmp, il2cpp_codegen_string_new_wrapper(""), &m2049_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t432 * tmp;
		tmp = (t432 *)il2cpp_codegen_object_new (&t432_TI);
		m2050(tmp, il2cpp_codegen_string_new_wrapper("Microsoft"), &m2050_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t433 * tmp;
		tmp = (t433 *)il2cpp_codegen_object_new (&t433_TI);
		m2051(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), &m2051_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		t434 * tmp;
		tmp = (t434 *)il2cpp_codegen_object_new (&t434_TI);
		m2052(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Microsoft 2013"), &m2052_MI);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		t46 * tmp;
		tmp = (t46 *)il2cpp_codegen_object_new (&t46_TI);
		m71(tmp, &m71_MI);
		m72(tmp, true, &m72_MI);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		t435 * tmp;
		tmp = (t435 *)il2cpp_codegen_object_new (&t435_TI);
		m2053(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), &m2053_MI);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("d4f464c7-9b15-460d-b4bc-2cacd1c1df73"), &m2054_MI);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, false, &m2055_MI);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		t438 * tmp;
		tmp = (t438 *)il2cpp_codegen_object_new (&t438_TI);
		m2056(tmp, il2cpp_codegen_string_new_wrapper(""), &m2056_MI);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_UnityEngine_UI_Assembly__CustomAttributeCache = {
11,
NULL,
&g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator
};
