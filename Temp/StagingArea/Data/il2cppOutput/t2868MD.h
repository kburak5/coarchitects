﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2868;
struct t29;
struct t472;
#include "t725.h"

 void m15598 (t2868 * __this, t472 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15599 (t2868 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15600 (t2868 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15601 (t2868 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15602 (t2868 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15603 (t2868 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
