﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3236;
struct t29;
struct t20;
#include "t899.h"

 void m17976 (t3236 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17977 (t3236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17978 (t3236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17979 (t3236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17980 (t3236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
