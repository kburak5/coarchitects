﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1597;
struct t200;
struct t781;
struct t7;
struct t29;
struct t1576;
struct t1305;

 void m8721 (t1597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8722 (t1597 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8723 (t1597 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8724 (t29 * __this, t200* p0, int32_t p1, int32_t p2, uint16_t* p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8725 (t29 * __this, uint16_t* p0, int32_t p1, uint16_t* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8726 (t1597 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8727 (t1597 * __this, uint16_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8728 (t29 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, uint16_t* p5, bool p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8729 (t29 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, uint16_t* p4, bool p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8730 (t1597 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8731 (t1597 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8732 (t1597 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8733 (t29 * __this, t781* p0, int32_t p1, int32_t p2, uint32_t p3, uint32_t p4, t29 * p5, t1576 ** p6, t781** p7, bool p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8734 (t29 * __this, uint8_t* p0, int32_t p1, uint32_t p2, uint32_t p3, t29 * p4, t1576 ** p5, t781** p6, bool p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8735 (t29 * __this, t29 * p0, t1576 ** p1, t781** p2, uint8_t* p3, int64_t p4, uint32_t p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8736 (t29 * __this, t29 * p0, t1576 ** p1, t781** p2, uint8_t* p3, int64_t p4, uint32_t p5, uint16_t* p6, int32_t* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8737 (t1597 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8738 (t29 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, uint32_t* p5, uint32_t* p6, t29 * p7, t1576 ** p8, t781** p9, bool p10, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8739 (t29 * __this, uint8_t* p0, int32_t p1, uint16_t* p2, int32_t p3, uint32_t* p4, uint32_t* p5, t29 * p6, t1576 ** p7, t781** p8, bool p9, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8740 (t1597 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8741 (t1597 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8742 (t1597 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1305 * m8743 (t1597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8744 (t1597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8745 (t1597 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8746 (t1597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8747 (t1597 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8748 (t1597 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
