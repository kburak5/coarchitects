﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1039;
struct t7;
struct t781;
struct t1038;
struct t1020;

 void m4698 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4699 (t29 * __this, t7* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4700 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1038 * m4701 (t29 * __this, t1020 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4702 (t29 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4703 (t29 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
