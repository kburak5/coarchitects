﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t131;
struct t133;
#include "t132.h"
#include "t128.h"

 t132  m342 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m343 (t131 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m344 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m345 (t131 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m346 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m347 (t131 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m348 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m349 (t131 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m350 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m351 (t131 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m352 (t131 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m353 (t131 * __this, t133 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m354 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m355 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m356 (t131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
