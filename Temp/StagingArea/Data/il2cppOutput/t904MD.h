﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t904;
struct t904_marshaled;

void t904_marshal(const t904& unmarshaled, t904_marshaled& marshaled);
void t904_marshal_back(const t904_marshaled& marshaled, t904& unmarshaled);
void t904_marshal_cleanup(t904_marshaled& marshaled);
