﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1548;
struct t781;

 void m8392 (t1548 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8393 (t1548 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8394 (t1548 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8395 (t1548 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
