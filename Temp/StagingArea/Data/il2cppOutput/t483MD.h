﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t483;
struct t29;
struct t466;
struct t7;
struct t592;
struct t2096;
struct t733;
struct t2892;
struct t20;
struct t136;
struct t2894;
struct t722;
#include "t735.h"
#include "t2893.h"
#include "t2895.h"
#include "t725.h"

#include "t2461MD.h"
#define m15779(__this, method) (void)m12834_gshared((t2461 *)__this, method)
#define m2863(__this, p0, method) (void)m12836_gshared((t2461 *)__this, (t29*)p0, method)
#define m15780(__this, p0, method) (void)m12838_gshared((t2461 *)__this, (int32_t)p0, method)
#define m15781(__this, p0, p1, method) (void)m12840_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
#define m15782(__this, p0, method) (t29 *)m12842_gshared((t2461 *)__this, (t29 *)p0, method)
#define m15783(__this, p0, p1, method) (void)m12844_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m15784(__this, p0, p1, method) (void)m12846_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m15785(__this, p0, method) (void)m12848_gshared((t2461 *)__this, (t29 *)p0, method)
#define m15786(__this, method) (bool)m12850_gshared((t2461 *)__this, method)
#define m15787(__this, method) (t29 *)m12852_gshared((t2461 *)__this, method)
#define m15788(__this, method) (bool)m12854_gshared((t2461 *)__this, method)
 void m15789 (t483 * __this, t2893  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15790 (t483 * __this, t2893  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m15791(__this, p0, p1, method) (void)m12858_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
 bool m15792 (t483 * __this, t2893  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m15793(__this, p0, p1, method) (void)m12861_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m15794(__this, method) (t29 *)m12863_gshared((t2461 *)__this, method)
#define m15795(__this, method) (t29*)m12865_gshared((t2461 *)__this, method)
#define m15796(__this, method) (t29 *)m12867_gshared((t2461 *)__this, method)
#define m15797(__this, method) (int32_t)m12869_gshared((t2461 *)__this, method)
#define m15798(__this, p0, method) (t466 *)m12871_gshared((t2461 *)__this, (t29 *)p0, method)
#define m2864(__this, p0, p1, method) (void)m12873_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m15799(__this, p0, p1, method) (void)m12875_gshared((t2461 *)__this, (int32_t)p0, (t29*)p1, method)
#define m15800(__this, p0, method) (void)m12877_gshared((t2461 *)__this, (int32_t)p0, method)
#define m15801(__this, p0, p1, method) (void)m12879_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
 t2893  m15802 (t29 * __this, t7* p0, t466 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m15803(__this, p0, p1, method) (t466 *)m12882_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m15804(__this, p0, p1, method) (void)m12884_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
#define m15805(__this, method) (void)m12886_gshared((t2461 *)__this, method)
#define m15806(__this, p0, p1, method) (void)m12887_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m15807(__this, method) (void)m12889_gshared((t2461 *)__this, method)
#define m15808(__this, p0, method) (bool)m12891_gshared((t2461 *)__this, (t29 *)p0, method)
#define m15809(__this, p0, method) (bool)m12893_gshared((t2461 *)__this, (t29 *)p0, method)
#define m15810(__this, p0, p1, method) (void)m12895_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
#define m15811(__this, p0, method) (void)m12897_gshared((t2461 *)__this, (t29 *)p0, method)
#define m15812(__this, p0, method) (bool)m12899_gshared((t2461 *)__this, (t29 *)p0, method)
#define m2865(__this, p0, p1, method) (bool)m12900_gshared((t2461 *)__this, (t29 *)p0, (t29 **)p1, method)
#define m2866(__this, method) (t592 *)m12902_gshared((t2461 *)__this, method)
#define m15813(__this, p0, method) (t7*)m12904_gshared((t2461 *)__this, (t29 *)p0, method)
#define m15814(__this, p0, method) (t466 *)m12906_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m15815 (t483 * __this, t2893  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2895  m15816 (t483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m15817(__this, p0, p1, method) (t725 )m12910_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
