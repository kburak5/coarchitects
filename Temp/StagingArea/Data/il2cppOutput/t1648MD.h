﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1648;
struct t781;
struct t29;
struct t292;
struct t7;
struct t1094;
#include "t1648.h"

 void m9255 (t1648 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9256 (t1648 * __this, int32_t p0, int16_t p1, int16_t p2, uint8_t p3, uint8_t p4, uint8_t p5, uint8_t p6, uint8_t p7, uint8_t p8, uint8_t p9, uint8_t p10, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9257 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9258 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9259 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9260 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9261 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9262 (t1648 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9263 (t1648 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9264 (t1648 * __this, t1648  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9265 (t1648 * __this, t1648  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9266 (t1648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9267 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1648  m9268 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9269 (t29 * __this, t292 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9270 (t29 * __this, t292 * p0, int16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9271 (t29 * __this, t292 * p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9272 (t1648 * __this, bool p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9273 (t1648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9274 (t1648 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9275 (t1648 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
