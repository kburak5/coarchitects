﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t169;
struct t3;
struct t155;
struct t171;

 void m501 (t169 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m502 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t169 * m503 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m504 (t29 * __this, t3 * p0, t155 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m505 (t29 * __this, t3 * p0, t155 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m506 (t29 * __this, t3 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
