﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t576;
struct t29;
struct t7;

 void m2826 (t576 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2827 (t576 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2828 (t576 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2829 (t576 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
