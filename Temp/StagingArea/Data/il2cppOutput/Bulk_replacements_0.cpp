﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t963.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t963_TI;
#include "t963MD.h"


#include "t20.h"

// Metadata Definition <Module>
static MethodInfo* t963_MIs[] =
{
	NULL
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType t963_0_0_0;
extern Il2CppType t963_1_0_0;
struct t963;
TypeInfo t963_TI = 
{
	&g_replacements_dll_Image, NULL, "<Module>", "", t963_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t963_TI, NULL, NULL, &EmptyCustomAttributesCache, &t963_TI, &t963_0_0_0, &t963_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t963), 0, -1, 0, 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#include "t964.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t964_TI;
#include "t964MD.h"

#include "t40.h"


extern MethodInfo m4289_MI;
 bool m4289 (t29 * __this, MethodInfo* method){
	{
		return 0;
	}
}
// Metadata Definition Replacements.MSCompatUnicodeTable
static PropertyInfo t964____IsReady_PropertyInfo = 
{
	&t964_TI, "IsReady", &m4289_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t964_PIs[] =
{
	&t964____IsReady_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m4289_MI = 
{
	"get_IsReady", (methodPointerType)&m4289, &t964_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 1, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t964_MIs[] =
{
	&m4289_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t964_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType t964_0_0_0;
extern Il2CppType t964_1_0_0;
extern TypeInfo t29_TI;
struct t964;
TypeInfo t964_TI = 
{
	&g_replacements_dll_Image, NULL, "MSCompatUnicodeTable", "Replacements", t964_MIs, t964_PIs, NULL, NULL, &t29_TI, NULL, NULL, &t964_TI, NULL, t964_VT, &EmptyCustomAttributesCache, &t964_TI, &t964_0_0_0, &t964_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t964), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 4, 0, 0};
#include "t965.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t965_TI;
#include "t965MD.h"

#include "t7.h"
#include "t29.h"
#include "t345.h"
#include "t21.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
extern MethodInfo m3988_MI;


extern MethodInfo m4290_MI;
 t7* m4290 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral555, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition Replacements.SecurityElement
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t965_m4290_ParameterInfos[] = 
{
	{"__this", 0, 134217729, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4290_MI = 
{
	"ToString", (methodPointerType)&m4290, &t965_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t965_m4290_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 2, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t965_MIs[] =
{
	&m4290_MI,
	NULL
};
static MethodInfo* t965_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType t965_0_0_0;
extern Il2CppType t965_1_0_0;
struct t965;
TypeInfo t965_TI = 
{
	&g_replacements_dll_Image, NULL, "SecurityElement", "Replacements", t965_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t965_TI, NULL, t965_VT, &EmptyCustomAttributesCache, &t965_TI, &t965_0_0_0, &t965_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t965), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 0};
#include "t966.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t966_TI;
#include "t966MD.h"

#include "t42.h"
#include "mscorlib_ArrayTypes.h"


extern MethodInfo m4291_MI;
 t29 * m4291 (t29 * __this, t7* p0, t29 * p1, t7** p2, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral556, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m4292_MI;
 t29 * m4292 (t29 * __this, t42 * p0, t7* p1, t316* p2, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral557, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition Replacements.RemotingServices
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t7_1_0_2;
extern Il2CppType t7_1_0_0;
static ParameterInfo t966_m4291_ParameterInfos[] = 
{
	{"url", 0, 134217730, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"channelData", 1, 134217731, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"objectUri", 2, 134217732, &EmptyCustomAttributesCache, &t7_1_0_2},
};
extern Il2CppType t967_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t639 (MethodInfo* method, void* obj, void** args);
MethodInfo m4291_MI = 
{
	"GetClientChannelSinkChain", (methodPointerType)&m4291, &t966_TI, &t967_0_0_0, RuntimeInvoker_t29_t29_t29_t639, t966_m4291_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 3, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t966_m4292_ParameterInfos[] = 
{
	{"objectType", 0, 134217733, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"url", 1, 134217734, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"activationAttributes", 2, 134217735, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4292_MI = 
{
	"CreateClientProxy", (methodPointerType)&m4292, &t966_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t966_m4292_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 4, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t966_MIs[] =
{
	&m4291_MI,
	&m4292_MI,
	NULL
};
static MethodInfo* t966_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType t966_0_0_0;
extern Il2CppType t966_1_0_0;
struct t966;
TypeInfo t966_TI = 
{
	&g_replacements_dll_Image, NULL, "RemotingServices", "Replacements", t966_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t966_TI, NULL, t966_VT, &EmptyCustomAttributesCache, &t966_TI, &t966_0_0_0, &t966_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t966), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 4, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
