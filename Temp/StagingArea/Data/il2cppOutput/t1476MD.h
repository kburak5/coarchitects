﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1476;
struct t29;
struct t733;
#include "t735.h"

 void m9607 (t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9608 (t1476 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9609 (t1476 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9610 (t1476 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9611 (t1476 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9612 (t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9613 (t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9614 (t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9615 (t1476 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
