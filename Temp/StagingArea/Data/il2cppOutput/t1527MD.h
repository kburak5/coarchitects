﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1527;
struct t997;
struct t781;
struct t975;

 void m8189 (t1527 * __this, t997 * p0, bool p1, t781* p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8190 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8191 (t1527 * __this, uint32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8192 (t29 * __this, t781* p0, t781* p1, t975* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8193 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8194 (t1527 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8195 (t1527 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8196 (t1527 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8197 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
