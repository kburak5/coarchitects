﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3219;
struct t29;
struct t20;
#include "t815.h"

 void m17895 (t3219 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17896 (t3219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17897 (t3219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17898 (t3219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17899 (t3219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
