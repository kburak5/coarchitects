﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t800;
struct t29;
struct t731;
struct t868;
struct t20;
struct t136;

 void m4155 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4133 (t800 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4132 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4131 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4138 (t800 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4140 (t800 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4141 (t800 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4142 (t800 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4143 (t800 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4134 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4135 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4136 (t800 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4137 (t800 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4096 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4130 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4139 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4144 (t800 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m4123 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4268 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4145 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4146 (t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4147 (t800 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4148 (t800 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4149 (t800 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4150 (t800 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4151 (t800 * __this, int32_t p0, t29 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4152 (t800 * __this, int32_t p0, t29 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4153 (t800 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
