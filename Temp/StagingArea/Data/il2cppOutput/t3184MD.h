﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3184;
struct t29;
struct t20;
#include "t751.h"

 void m17683 (t3184 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17684 (t3184 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17685 (t3184 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17686 (t3184 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17687 (t3184 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
