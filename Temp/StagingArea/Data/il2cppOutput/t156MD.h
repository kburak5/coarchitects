﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t156;
struct t162;
struct t7;

 void m1907 (t156 * __this, t156 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m1915 (t156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m2444 (t156 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m2445 (t156 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2446 (t156 * __this, t7* p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2447 (t156 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1909 (t156 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1903 (t156 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2448 (t156 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2449 (t29 * __this, t156 * p0, t156 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
