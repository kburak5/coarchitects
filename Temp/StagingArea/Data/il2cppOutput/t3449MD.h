﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3449;
struct t29;
struct t20;
#include "t1513.h"

 void m19115 (t3449 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19116 (t3449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19117 (t3449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19118 (t3449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m19119 (t3449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
