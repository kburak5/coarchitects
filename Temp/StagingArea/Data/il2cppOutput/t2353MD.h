﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2353;
struct t29;
struct t20;
#include "t1248.h"

 void m12102 (t2353 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12103 (t2353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12104 (t2353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12105 (t2353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1248  m12106 (t2353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
