﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1697;
struct t1697_marshaled;

void t1697_marshal(const t1697& unmarshaled, t1697_marshaled& marshaled);
void t1697_marshal_back(const t1697_marshaled& marshaled, t1697& unmarshaled);
void t1697_marshal_cleanup(t1697_marshaled& marshaled);
