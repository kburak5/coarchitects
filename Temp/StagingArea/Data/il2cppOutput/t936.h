﻿#pragma once
#include <stdint.h>
struct t781;
#include "t110.h"
struct t936 
{
	int32_t f0;
	t781* f1;
	t781* f2;
	t781* f3;
	t781* f4;
	t781* f5;
	t781* f6;
	t781* f7;
};
// Native definition for marshalling of: System.Security.Cryptography.DSAParameters
struct t936_marshaled
{
	int32_t f0;
	uint8_t* f1;
	uint8_t* f2;
	uint8_t* f3;
	uint8_t* f4;
	uint8_t* f5;
	uint8_t* f6;
	uint8_t* f7;
};
