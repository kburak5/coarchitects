﻿#pragma once
#include <stdint.h>
#include "t110.h"
struct t521 
{
	int32_t f0;
	int32_t f1;
	int32_t f2;
	float f3;
	bool f4;
	int32_t f5;
};
// Native definition for marshalling of: UnityEngine.AnimatorTransitionInfo
struct t521_marshaled
{
	int32_t f0;
	int32_t f1;
	int32_t f2;
	float f3;
	int32_t f4;
	int32_t f5;
};
