﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1699;
struct t1699_marshaled;

void t1699_marshal(const t1699& unmarshaled, t1699_marshaled& marshaled);
void t1699_marshal_back(const t1699_marshaled& marshaled, t1699& unmarshaled);
void t1699_marshal_cleanup(t1699_marshaled& marshaled);
