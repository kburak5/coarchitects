﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1068;
struct t745;
struct t1020;

 void m5022 (t1068 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m5023 (t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5024 (t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5025 (t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5026 (t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5027 (t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5028 (t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m5029 (t1068 * __this, t745 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
