﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t329;
struct t7;
#include "t23.h"

 void m2415 (t329 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1467 (t329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1468 (t329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1747 (t329 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2416 (t329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
