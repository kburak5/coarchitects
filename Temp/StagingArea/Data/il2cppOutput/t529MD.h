﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t529;
struct t29;
struct t3041;
struct t20;
struct t136;
struct t389;
struct t3042;
struct t3043;
struct t531;
struct t3044;
struct t3045;
#include "t380.h"
#include "t3046.h"

 void m16671 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2905 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16672 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m16673 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16674 (t529 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16675 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16676 (t529 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16677 (t529 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16678 (t529 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16679 (t529 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16680 (t529 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16681 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16682 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16683 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16684 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16685 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16686 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16687 (t529 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16688 (t529 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16689 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16690 (t529 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16691 (t529 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16692 (t529 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3043 * m16693 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16694 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16695 (t529 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16696 (t529 * __this, t531* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16697 (t529 * __this, t3044 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16698 (t29 * __this, t3044 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16699 (t529 * __this, int32_t p0, int32_t p1, t3044 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3046  m16700 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16701 (t529 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16702 (t529 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16703 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16704 (t529 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16705 (t529 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16706 (t529 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16707 (t529 * __this, t3044 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16708 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16709 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16710 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16711 (t529 * __this, t3045 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t531* m16712 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16713 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16714 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16715 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16716 (t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16717 (t529 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16718 (t529 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
