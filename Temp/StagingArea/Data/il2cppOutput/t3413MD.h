﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3413;
struct t29;
struct t20;
#include "t1405.h"

 void m18935 (t3413 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18936 (t3413 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18937 (t3413 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18938 (t3413 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18939 (t3413 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
