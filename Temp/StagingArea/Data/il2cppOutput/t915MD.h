﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t915;
struct t7;
struct t29;
struct t733;
#include "t735.h"

 void m4198 (t915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3975 (t915 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3968 (t915 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8860 (t915 * __this, t7* p0, t29 * p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8861 (t915 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8862 (t915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8863 (t915 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
