﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t547;
struct t7;
struct t29;
struct t548;

 void m2749 (t547 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2750 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2751 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2752 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2753 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2754 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2755 (t29 * __this, t29 * p0, t7** p1, t7** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2756 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2757 (t29 * __this, t548 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
