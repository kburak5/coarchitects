﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t105;
struct t29;
struct t31;
struct t2215;
struct t20;
struct t136;
struct t2216;
struct t2217;
struct t2218;
struct t2214;
struct t2219;
struct t2220;
#include "t2221.h"

#include "t294MD.h"
#define m10909(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m10910(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m10911(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m10912(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m10913(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m10914(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m10915(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m10916(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m10917(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m10918(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m10919(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m10920(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m10921(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m10922(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m10923(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m10924(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m10925(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m10926(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m10927(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m10928(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m10929(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m10930(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m10931(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m10932(__this, method) (t2218 *)m10583_gshared((t294 *)__this, method)
#define m1366(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m10933(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m10934(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m10935(__this, p0, method) (t29 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m10936(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m10937(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2221  m10938 (t105 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m10939(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m10940(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m10941(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m10942(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m10943(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m10944(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m10945(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m10946(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m10947(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m10948(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m10949(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m10950(__this, method) (t2214*)m10619_gshared((t294 *)__this, method)
#define m10951(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m10952(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m10953(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m10954(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m10955(__this, p0, method) (t29 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m10956(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
