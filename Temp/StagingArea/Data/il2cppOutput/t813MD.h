﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t813;
struct t779;
struct t29;
struct t20;
struct t136;

 void m3490 (t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3491 (t813 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3492 (t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3493 (t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3494 (t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t779 * m3495 (t813 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3496 (t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3497 (t813 * __this, t779 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
