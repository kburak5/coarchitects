﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1624;
struct t29;
struct t7;

 void m8881 (t1624 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8882 (t1624 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8883 (t1624 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8884 (t1624 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8885 (t1624 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
