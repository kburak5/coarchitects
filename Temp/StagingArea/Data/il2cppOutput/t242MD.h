﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t242;
struct t39;
struct t246;

 void m1010 (t242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1011 (t242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1012 (t242 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1013 (t242 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1014 (t242 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1015 (t242 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1016 (t242 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1017 (t242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m1018 (t242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1019 (t242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1020 (t29 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1021 (t29 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
