﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1551;
struct t781;

 void m8406 (t1551 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8407 (t1551 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8408 (t1551 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8409 (t1551 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8410 (t1551 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8411 (t1551 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8412 (t1551 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8413 (t1551 * __this, uint64_t p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8414 (t1551 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8415 (t1551 * __this, uint64_t p0, uint64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8416 (t1551 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
