﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t857;
struct t840;
struct t858;
struct t7;
struct t852;
#include "t843.h"
#include "t844.h"
#include "t849.h"
#include "t845.h"

 void m3607 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3608 (t29 * __this, uint16_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3609 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3610 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3611 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3612 (t857 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3613 (t857 * __this, uint16_t p0, bool p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3614 (t857 * __this, uint16_t p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3615 (t857 * __this, uint16_t p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3616 (t857 * __this, uint16_t p0, uint16_t p1, bool p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3617 (t857 * __this, uint16_t p0, t858 * p1, bool p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3618 (t857 * __this, t7* p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3619 (t857 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3620 (t857 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3621 (t857 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3622 (t857 * __this, int32_t p0, int32_t p1, bool p2, t852 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3623 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3624 (t857 * __this, int32_t p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3625 (t857 * __this, int32_t p0, t852 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3626 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3627 (t857 * __this, t852 * p0, t852 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3628 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3629 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3630 (t857 * __this, int32_t p0, int32_t p1, bool p2, t852 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3631 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3632 (t857 * __this, int32_t p0, int32_t p1, bool p2, t852 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3633 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3634 (t857 * __this, bool p0, int32_t p1, t852 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3635 (t857 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t852 * m3636 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3637 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3638 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3639 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3640 (t29 * __this, bool p0, bool p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3641 (t857 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3642 (t857 * __this, uint16_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3643 (t857 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3644 (t857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3645 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3646 (t857 * __this, t852 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
