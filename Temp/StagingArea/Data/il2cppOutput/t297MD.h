﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t297;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 bool m5441 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5442 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5443 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5444 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5445 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5446 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5447 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5448 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5449 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5450 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5451 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5452 (int8_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5453 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5454 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5455 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5456 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5457 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5458 (int8_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5459 (int8_t* __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5460 (int8_t* __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5461 (t29 * __this, t7* p0, bool p1, int8_t* p2, t295 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5462 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5463 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5464 (t29 * __this, t7* p0, int8_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5465 (int8_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5466 (int8_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5467 (int8_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5468 (int8_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
