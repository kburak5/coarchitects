﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2441;
struct t29;
struct t145;
struct t66;
struct t67;
#include "t35.h"
#include "t725.h"

 void m12820 (t2441 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12821 (t2441 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12822 (t2441 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12823 (t2441 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
