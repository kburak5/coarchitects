﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t979;
struct t980;
struct t972;
#include "t977.h"

 void m4361 (t979 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4362 (t979 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t980 * m4363 (t979 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4364 (t979 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
