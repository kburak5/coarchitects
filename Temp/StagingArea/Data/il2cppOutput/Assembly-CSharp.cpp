﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo t0_TI;
extern TypeInfo t1_TI;
extern TypeInfo t5_TI;
extern TypeInfo t8_TI;
extern TypeInfo t10_TI;
extern TypeInfo t11_TI;
extern TypeInfo t12_TI;
extern TypeInfo t13_TI;
extern TypeInfo t14_TI;
extern TypeInfo t18_TI;
extern TypeInfo t19_TI;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DCSharp_Assembly_Types[12] = 
{
	&t0_TI,
	&t1_TI,
	&t5_TI,
	&t8_TI,
	&t10_TI,
	&t11_TI,
	&t12_TI,
	&t13_TI,
	&t14_TI,
	&t18_TI,
	&t19_TI,
	NULL,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern CustomAttributesCache g_AssemblyU2DCSharp_Assembly__CustomAttributeCache;
Il2CppAssembly g_AssemblyU2DCSharp_Assembly = 
{
	{ "Assembly-CSharp", 0, 0, 0, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DCSharp_dll_Image,
	&g_AssemblyU2DCSharp_Assembly__CustomAttributeCache,
};
Il2CppImage g_AssemblyU2DCSharp_dll_Image = 
{
	 "Assembly-CSharp.dll" ,
	&g_AssemblyU2DCSharp_Assembly,
	g_AssemblyU2DCSharp_Assembly_Types,
	11,
	NULL,
};
static void s_AssemblyU2DCSharpRegistration()
{
	RegisterAssembly (&g_AssemblyU2DCSharp_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_AssemblyU2DCSharpRegistrationVariable(&s_AssemblyU2DCSharpRegistration, NULL);
