﻿#pragma once
#include <stdint.h>
#include "t110.h"
struct t866 
{
	int32_t f0;
	int32_t f1;
	bool f2;
};
// Native definition for marshalling of: System.Text.RegularExpressions.Interval
struct t866_marshaled
{
	int32_t f0;
	int32_t f1;
	int32_t f2;
};
