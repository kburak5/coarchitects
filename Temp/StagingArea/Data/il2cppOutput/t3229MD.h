﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3229;
struct t29;
struct t20;
#include "t844.h"

 void m17941 (t3229 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17942 (t3229 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17943 (t3229 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17944 (t3229 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m17945 (t3229 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
