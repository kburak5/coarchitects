﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2359;
struct t29;
struct t6;
struct t66;
struct t67;
#include "t35.h"
#include "t322.h"

 void m12156 (t2359 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t322  m12157 (t2359 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12158 (t2359 * __this, int32_t p0, t6 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t322  m12159 (t2359 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
