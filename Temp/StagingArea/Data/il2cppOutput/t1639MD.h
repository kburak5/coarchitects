﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1639;
struct t29;

 void m9223 (t1639 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9224 (t1639 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9225 (t1639 * __this, int16_t p0, int16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
