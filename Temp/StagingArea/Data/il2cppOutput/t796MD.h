﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t796;
struct t791;
struct t29;
struct t797;
#include "t798.h"

 void m3327 (t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3328 (t796 * __this, t796 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t791 * m3329 (t796 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3330 (t796 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3331 (t796 * __this, t796 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3332 (t796 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t796 * m3333 (t796 * __this, int32_t p0, t29 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t797 * m3334 (t796 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
