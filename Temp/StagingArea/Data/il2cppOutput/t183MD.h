﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t183;
struct t29;
struct t7;
#include "t183.h"

 void m1581 (t183 * __this, float p0, float p1, float p2, float p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1671 (t183 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1673 (t183 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2408 (t183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2409 (t183 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2410 (t183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2411 (t29 * __this, t183  p0, t183  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2412 (t29 * __this, t183  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1648 (t183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m1654 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m2413 (t29 * __this, t183  p0, t183  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m1666 (t29 * __this, t183  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2414 (t29 * __this, t183  p0, t183  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
