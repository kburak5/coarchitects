﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t50;
struct t52;
struct t16;
struct t53;
struct t6;
struct t56;
struct t7;
#include "t57.h"

 void m73 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m74 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t50 * m75 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m76 (t29 * __this, t50 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m77 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m78 (t50 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m79 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m80 (t50 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t52 * m81 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m82 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m83 (t50 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m84 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m85 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m86 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m87 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m88 (t50 * __this, t16 * p0, t53 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t53 * m89 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m90 (t50 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m91 (t29 * __this, t57  p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m92 (t50 * __this, t6 * p0, t56 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m93 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m94 (t50 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m95 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m96 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m97 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m98 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m99 (t50 * __this, t52 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m100 (t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
