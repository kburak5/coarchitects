﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5680_TI;

#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Single>>
extern MethodInfo m29715_MI;
extern MethodInfo m29716_MI;
static PropertyInfo t5680____Item_PropertyInfo = 
{
	&t5680_TI, "Item", &m29715_MI, &m29716_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5680_PIs[] =
{
	&t5680____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1752_0_0_0;
extern Il2CppType t1752_0_0_0;
static ParameterInfo t5680_m29717_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1752_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29717_GM;
MethodInfo m29717_MI = 
{
	"IndexOf", NULL, &t5680_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5680_m29717_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29717_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1752_0_0_0;
static ParameterInfo t5680_m29718_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1752_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29718_GM;
MethodInfo m29718_MI = 
{
	"Insert", NULL, &t5680_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5680_m29718_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29718_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5680_m29719_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29719_GM;
MethodInfo m29719_MI = 
{
	"RemoveAt", NULL, &t5680_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5680_m29719_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29719_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5680_m29715_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1752_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29715_GM;
MethodInfo m29715_MI = 
{
	"get_Item", NULL, &t5680_TI, &t1752_0_0_0, RuntimeInvoker_t29_t44, t5680_m29715_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29715_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1752_0_0_0;
static ParameterInfo t5680_m29716_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1752_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29716_GM;
MethodInfo m29716_MI = 
{
	"set_Item", NULL, &t5680_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5680_m29716_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29716_GM};
static MethodInfo* t5680_MIs[] =
{
	&m29717_MI,
	&m29718_MI,
	&m29719_MI,
	&m29715_MI,
	&m29716_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5679_TI;
extern TypeInfo t5681_TI;
static TypeInfo* t5680_ITIs[] = 
{
	&t603_TI,
	&t5679_TI,
	&t5681_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5680_0_0_0;
extern Il2CppType t5680_1_0_0;
struct t5680;
extern Il2CppGenericClass t5680_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5680_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5680_MIs, t5680_PIs, NULL, NULL, NULL, NULL, NULL, &t5680_TI, t5680_ITIs, NULL, &t1908__CustomAttributeCache, &t5680_TI, &t5680_0_0_0, &t5680_1_0_0, NULL, &t5680_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4427_TI;

#include "t512.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AnimationEventSource>
extern MethodInfo m29720_MI;
static PropertyInfo t4427____Current_PropertyInfo = 
{
	&t4427_TI, "Current", &m29720_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4427_PIs[] =
{
	&t4427____Current_PropertyInfo,
	NULL
};
extern Il2CppType t512_0_0_0;
extern void* RuntimeInvoker_t512 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29720_GM;
MethodInfo m29720_MI = 
{
	"get_Current", NULL, &t4427_TI, &t512_0_0_0, RuntimeInvoker_t512, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29720_GM};
static MethodInfo* t4427_MIs[] =
{
	&m29720_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4427_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4427_0_0_0;
extern Il2CppType t4427_1_0_0;
struct t4427;
extern Il2CppGenericClass t4427_GC;
TypeInfo t4427_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4427_MIs, t4427_PIs, NULL, NULL, NULL, NULL, NULL, &t4427_TI, t4427_ITIs, NULL, &EmptyCustomAttributesCache, &t4427_TI, &t4427_0_0_0, &t4427_1_0_0, NULL, &t4427_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3009.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3009_TI;
#include "t3009MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t512_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m16453_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m22594_MI;
struct t20;
#include "t915.h"
 int32_t m22594 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16449_MI;
 void m16449 (t3009 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16450_MI;
 t29 * m16450 (t3009 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16453(__this, &m16453_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t512_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16451_MI;
 void m16451 (t3009 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16452_MI;
 bool m16452 (t3009 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16453 (t3009 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22594(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22594_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>
extern Il2CppType t20_0_0_1;
FieldInfo t3009_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3009_TI, offsetof(t3009, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3009_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3009_TI, offsetof(t3009, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3009_FIs[] =
{
	&t3009_f0_FieldInfo,
	&t3009_f1_FieldInfo,
	NULL
};
static PropertyInfo t3009____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3009_TI, "System.Collections.IEnumerator.Current", &m16450_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3009____Current_PropertyInfo = 
{
	&t3009_TI, "Current", &m16453_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3009_PIs[] =
{
	&t3009____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3009____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3009_m16449_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16449_GM;
MethodInfo m16449_MI = 
{
	".ctor", (methodPointerType)&m16449, &t3009_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3009_m16449_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16449_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16450_GM;
MethodInfo m16450_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16450, &t3009_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16450_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16451_GM;
MethodInfo m16451_MI = 
{
	"Dispose", (methodPointerType)&m16451, &t3009_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16451_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16452_GM;
MethodInfo m16452_MI = 
{
	"MoveNext", (methodPointerType)&m16452, &t3009_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16452_GM};
extern Il2CppType t512_0_0_0;
extern void* RuntimeInvoker_t512 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16453_GM;
MethodInfo m16453_MI = 
{
	"get_Current", (methodPointerType)&m16453, &t3009_TI, &t512_0_0_0, RuntimeInvoker_t512, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16453_GM};
static MethodInfo* t3009_MIs[] =
{
	&m16449_MI,
	&m16450_MI,
	&m16451_MI,
	&m16452_MI,
	&m16453_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3009_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16450_MI,
	&m16452_MI,
	&m16451_MI,
	&m16453_MI,
};
static TypeInfo* t3009_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4427_TI,
};
static Il2CppInterfaceOffsetPair t3009_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4427_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3009_0_0_0;
extern Il2CppType t3009_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3009_GC;
extern TypeInfo t20_TI;
TypeInfo t3009_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3009_MIs, t3009_PIs, t3009_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3009_TI, t3009_ITIs, t3009_VT, &EmptyCustomAttributesCache, &t3009_TI, &t3009_0_0_0, &t3009_1_0_0, t3009_IOs, &t3009_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3009)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5682_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>
extern MethodInfo m29721_MI;
static PropertyInfo t5682____Count_PropertyInfo = 
{
	&t5682_TI, "Count", &m29721_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29722_MI;
static PropertyInfo t5682____IsReadOnly_PropertyInfo = 
{
	&t5682_TI, "IsReadOnly", &m29722_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5682_PIs[] =
{
	&t5682____Count_PropertyInfo,
	&t5682____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29721_GM;
MethodInfo m29721_MI = 
{
	"get_Count", NULL, &t5682_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29721_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29722_GM;
MethodInfo m29722_MI = 
{
	"get_IsReadOnly", NULL, &t5682_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29722_GM};
extern Il2CppType t512_0_0_0;
extern Il2CppType t512_0_0_0;
static ParameterInfo t5682_m29723_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t512_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29723_GM;
MethodInfo m29723_MI = 
{
	"Add", NULL, &t5682_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5682_m29723_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29723_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29724_GM;
MethodInfo m29724_MI = 
{
	"Clear", NULL, &t5682_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29724_GM};
extern Il2CppType t512_0_0_0;
static ParameterInfo t5682_m29725_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t512_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29725_GM;
MethodInfo m29725_MI = 
{
	"Contains", NULL, &t5682_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5682_m29725_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29725_GM};
extern Il2CppType t3767_0_0_0;
extern Il2CppType t3767_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5682_m29726_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3767_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29726_GM;
MethodInfo m29726_MI = 
{
	"CopyTo", NULL, &t5682_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5682_m29726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29726_GM};
extern Il2CppType t512_0_0_0;
static ParameterInfo t5682_m29727_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t512_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29727_GM;
MethodInfo m29727_MI = 
{
	"Remove", NULL, &t5682_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5682_m29727_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29727_GM};
static MethodInfo* t5682_MIs[] =
{
	&m29721_MI,
	&m29722_MI,
	&m29723_MI,
	&m29724_MI,
	&m29725_MI,
	&m29726_MI,
	&m29727_MI,
	NULL
};
extern TypeInfo t5684_TI;
static TypeInfo* t5682_ITIs[] = 
{
	&t603_TI,
	&t5684_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5682_0_0_0;
extern Il2CppType t5682_1_0_0;
struct t5682;
extern Il2CppGenericClass t5682_GC;
TypeInfo t5682_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5682_MIs, t5682_PIs, NULL, NULL, NULL, NULL, NULL, &t5682_TI, t5682_ITIs, NULL, &EmptyCustomAttributesCache, &t5682_TI, &t5682_0_0_0, &t5682_1_0_0, NULL, &t5682_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AnimationEventSource>
extern Il2CppType t4427_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29728_GM;
MethodInfo m29728_MI = 
{
	"GetEnumerator", NULL, &t5684_TI, &t4427_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29728_GM};
static MethodInfo* t5684_MIs[] =
{
	&m29728_MI,
	NULL
};
static TypeInfo* t5684_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5684_0_0_0;
extern Il2CppType t5684_1_0_0;
struct t5684;
extern Il2CppGenericClass t5684_GC;
TypeInfo t5684_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5684_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5684_TI, t5684_ITIs, NULL, &EmptyCustomAttributesCache, &t5684_TI, &t5684_0_0_0, &t5684_1_0_0, NULL, &t5684_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5683_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>
extern MethodInfo m29729_MI;
extern MethodInfo m29730_MI;
static PropertyInfo t5683____Item_PropertyInfo = 
{
	&t5683_TI, "Item", &m29729_MI, &m29730_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5683_PIs[] =
{
	&t5683____Item_PropertyInfo,
	NULL
};
extern Il2CppType t512_0_0_0;
static ParameterInfo t5683_m29731_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t512_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29731_GM;
MethodInfo m29731_MI = 
{
	"IndexOf", NULL, &t5683_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5683_m29731_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29731_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t512_0_0_0;
static ParameterInfo t5683_m29732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t512_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29732_GM;
MethodInfo m29732_MI = 
{
	"Insert", NULL, &t5683_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5683_m29732_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29732_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5683_m29733_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29733_GM;
MethodInfo m29733_MI = 
{
	"RemoveAt", NULL, &t5683_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5683_m29733_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29733_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5683_m29729_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t512_0_0_0;
extern void* RuntimeInvoker_t512_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29729_GM;
MethodInfo m29729_MI = 
{
	"get_Item", NULL, &t5683_TI, &t512_0_0_0, RuntimeInvoker_t512_t44, t5683_m29729_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29729_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t512_0_0_0;
static ParameterInfo t5683_m29730_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t512_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29730_GM;
MethodInfo m29730_MI = 
{
	"set_Item", NULL, &t5683_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5683_m29730_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29730_GM};
static MethodInfo* t5683_MIs[] =
{
	&m29731_MI,
	&m29732_MI,
	&m29733_MI,
	&m29729_MI,
	&m29730_MI,
	NULL
};
static TypeInfo* t5683_ITIs[] = 
{
	&t603_TI,
	&t5682_TI,
	&t5684_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5683_0_0_0;
extern Il2CppType t5683_1_0_0;
struct t5683;
extern Il2CppGenericClass t5683_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5683_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5683_MIs, t5683_PIs, NULL, NULL, NULL, NULL, NULL, &t5683_TI, t5683_ITIs, NULL, &t1908__CustomAttributeCache, &t5683_TI, &t5683_0_0_0, &t5683_1_0_0, NULL, &t5683_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4429_TI;

#include "t517.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Keyframe>
extern MethodInfo m29734_MI;
static PropertyInfo t4429____Current_PropertyInfo = 
{
	&t4429_TI, "Current", &m29734_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4429_PIs[] =
{
	&t4429____Current_PropertyInfo,
	NULL
};
extern Il2CppType t517_0_0_0;
extern void* RuntimeInvoker_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29734_GM;
MethodInfo m29734_MI = 
{
	"get_Current", NULL, &t4429_TI, &t517_0_0_0, RuntimeInvoker_t517, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29734_GM};
static MethodInfo* t4429_MIs[] =
{
	&m29734_MI,
	NULL
};
static TypeInfo* t4429_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4429_0_0_0;
extern Il2CppType t4429_1_0_0;
struct t4429;
extern Il2CppGenericClass t4429_GC;
TypeInfo t4429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4429_MIs, t4429_PIs, NULL, NULL, NULL, NULL, NULL, &t4429_TI, t4429_ITIs, NULL, &EmptyCustomAttributesCache, &t4429_TI, &t4429_0_0_0, &t4429_1_0_0, NULL, &t4429_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3010.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3010_TI;
#include "t3010MD.h"

extern TypeInfo t517_TI;
extern MethodInfo m16458_MI;
extern MethodInfo m22605_MI;
struct t20;
 t517  m22605 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16454_MI;
 void m16454 (t3010 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16455_MI;
 t29 * m16455 (t3010 * __this, MethodInfo* method){
	{
		t517  L_0 = m16458(__this, &m16458_MI);
		t517  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t517_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16456_MI;
 void m16456 (t3010 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16457_MI;
 bool m16457 (t3010 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t517  m16458 (t3010 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t517  L_8 = m22605(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22605_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
extern Il2CppType t20_0_0_1;
FieldInfo t3010_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3010_TI, offsetof(t3010, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3010_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3010_TI, offsetof(t3010, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3010_FIs[] =
{
	&t3010_f0_FieldInfo,
	&t3010_f1_FieldInfo,
	NULL
};
static PropertyInfo t3010____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3010_TI, "System.Collections.IEnumerator.Current", &m16455_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3010____Current_PropertyInfo = 
{
	&t3010_TI, "Current", &m16458_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3010_PIs[] =
{
	&t3010____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3010____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3010_m16454_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16454_GM;
MethodInfo m16454_MI = 
{
	".ctor", (methodPointerType)&m16454, &t3010_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3010_m16454_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16454_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16455_GM;
MethodInfo m16455_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16455, &t3010_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16455_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16456_GM;
MethodInfo m16456_MI = 
{
	"Dispose", (methodPointerType)&m16456, &t3010_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16456_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16457_GM;
MethodInfo m16457_MI = 
{
	"MoveNext", (methodPointerType)&m16457, &t3010_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16457_GM};
extern Il2CppType t517_0_0_0;
extern void* RuntimeInvoker_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16458_GM;
MethodInfo m16458_MI = 
{
	"get_Current", (methodPointerType)&m16458, &t3010_TI, &t517_0_0_0, RuntimeInvoker_t517, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16458_GM};
static MethodInfo* t3010_MIs[] =
{
	&m16454_MI,
	&m16455_MI,
	&m16456_MI,
	&m16457_MI,
	&m16458_MI,
	NULL
};
static MethodInfo* t3010_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16455_MI,
	&m16457_MI,
	&m16456_MI,
	&m16458_MI,
};
static TypeInfo* t3010_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4429_TI,
};
static Il2CppInterfaceOffsetPair t3010_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4429_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3010_0_0_0;
extern Il2CppType t3010_1_0_0;
extern Il2CppGenericClass t3010_GC;
TypeInfo t3010_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3010_MIs, t3010_PIs, t3010_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3010_TI, t3010_ITIs, t3010_VT, &EmptyCustomAttributesCache, &t3010_TI, &t3010_0_0_0, &t3010_1_0_0, t3010_IOs, &t3010_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3010)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5685_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>
extern MethodInfo m29735_MI;
static PropertyInfo t5685____Count_PropertyInfo = 
{
	&t5685_TI, "Count", &m29735_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29736_MI;
static PropertyInfo t5685____IsReadOnly_PropertyInfo = 
{
	&t5685_TI, "IsReadOnly", &m29736_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5685_PIs[] =
{
	&t5685____Count_PropertyInfo,
	&t5685____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29735_GM;
MethodInfo m29735_MI = 
{
	"get_Count", NULL, &t5685_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29735_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29736_GM;
MethodInfo m29736_MI = 
{
	"get_IsReadOnly", NULL, &t5685_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29736_GM};
extern Il2CppType t517_0_0_0;
extern Il2CppType t517_0_0_0;
static ParameterInfo t5685_m29737_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29737_GM;
MethodInfo m29737_MI = 
{
	"Add", NULL, &t5685_TI, &t21_0_0_0, RuntimeInvoker_t21_t517, t5685_m29737_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29737_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29738_GM;
MethodInfo m29738_MI = 
{
	"Clear", NULL, &t5685_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29738_GM};
extern Il2CppType t517_0_0_0;
static ParameterInfo t5685_m29739_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t517_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29739_GM;
MethodInfo m29739_MI = 
{
	"Contains", NULL, &t5685_TI, &t40_0_0_0, RuntimeInvoker_t40_t517, t5685_m29739_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29739_GM};
extern Il2CppType t519_0_0_0;
extern Il2CppType t519_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5685_m29740_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t519_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29740_GM;
MethodInfo m29740_MI = 
{
	"CopyTo", NULL, &t5685_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5685_m29740_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29740_GM};
extern Il2CppType t517_0_0_0;
static ParameterInfo t5685_m29741_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t517_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29741_GM;
MethodInfo m29741_MI = 
{
	"Remove", NULL, &t5685_TI, &t40_0_0_0, RuntimeInvoker_t40_t517, t5685_m29741_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29741_GM};
static MethodInfo* t5685_MIs[] =
{
	&m29735_MI,
	&m29736_MI,
	&m29737_MI,
	&m29738_MI,
	&m29739_MI,
	&m29740_MI,
	&m29741_MI,
	NULL
};
extern TypeInfo t5687_TI;
static TypeInfo* t5685_ITIs[] = 
{
	&t603_TI,
	&t5687_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5685_0_0_0;
extern Il2CppType t5685_1_0_0;
struct t5685;
extern Il2CppGenericClass t5685_GC;
TypeInfo t5685_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5685_MIs, t5685_PIs, NULL, NULL, NULL, NULL, NULL, &t5685_TI, t5685_ITIs, NULL, &EmptyCustomAttributesCache, &t5685_TI, &t5685_0_0_0, &t5685_1_0_0, NULL, &t5685_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Keyframe>
extern Il2CppType t4429_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29742_GM;
MethodInfo m29742_MI = 
{
	"GetEnumerator", NULL, &t5687_TI, &t4429_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29742_GM};
static MethodInfo* t5687_MIs[] =
{
	&m29742_MI,
	NULL
};
static TypeInfo* t5687_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5687_0_0_0;
extern Il2CppType t5687_1_0_0;
struct t5687;
extern Il2CppGenericClass t5687_GC;
TypeInfo t5687_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5687_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5687_TI, t5687_ITIs, NULL, &EmptyCustomAttributesCache, &t5687_TI, &t5687_0_0_0, &t5687_1_0_0, NULL, &t5687_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5686_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Keyframe>
extern MethodInfo m29743_MI;
extern MethodInfo m29744_MI;
static PropertyInfo t5686____Item_PropertyInfo = 
{
	&t5686_TI, "Item", &m29743_MI, &m29744_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5686_PIs[] =
{
	&t5686____Item_PropertyInfo,
	NULL
};
extern Il2CppType t517_0_0_0;
static ParameterInfo t5686_m29745_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t517_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29745_GM;
MethodInfo m29745_MI = 
{
	"IndexOf", NULL, &t5686_TI, &t44_0_0_0, RuntimeInvoker_t44_t517, t5686_m29745_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29745_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t517_0_0_0;
static ParameterInfo t5686_m29746_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29746_GM;
MethodInfo m29746_MI = 
{
	"Insert", NULL, &t5686_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t517, t5686_m29746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29746_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5686_m29747_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29747_GM;
MethodInfo m29747_MI = 
{
	"RemoveAt", NULL, &t5686_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5686_m29747_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29747_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5686_m29743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t517_0_0_0;
extern void* RuntimeInvoker_t517_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29743_GM;
MethodInfo m29743_MI = 
{
	"get_Item", NULL, &t5686_TI, &t517_0_0_0, RuntimeInvoker_t517_t44, t5686_m29743_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29743_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t517_0_0_0;
static ParameterInfo t5686_m29744_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29744_GM;
MethodInfo m29744_MI = 
{
	"set_Item", NULL, &t5686_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t517, t5686_m29744_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29744_GM};
static MethodInfo* t5686_MIs[] =
{
	&m29745_MI,
	&m29746_MI,
	&m29747_MI,
	&m29743_MI,
	&m29744_MI,
	NULL
};
static TypeInfo* t5686_ITIs[] = 
{
	&t603_TI,
	&t5685_TI,
	&t5687_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5686_0_0_0;
extern Il2CppType t5686_1_0_0;
struct t5686;
extern Il2CppGenericClass t5686_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5686_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5686_MIs, t5686_PIs, NULL, NULL, NULL, NULL, NULL, &t5686_TI, t5686_ITIs, NULL, &t1908__CustomAttributeCache, &t5686_TI, &t5686_0_0_0, &t5686_1_0_0, NULL, &t5686_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4431_TI;

#include "t229.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Animator>
extern MethodInfo m29748_MI;
static PropertyInfo t4431____Current_PropertyInfo = 
{
	&t4431_TI, "Current", &m29748_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4431_PIs[] =
{
	&t4431____Current_PropertyInfo,
	NULL
};
extern Il2CppType t229_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29748_GM;
MethodInfo m29748_MI = 
{
	"get_Current", NULL, &t4431_TI, &t229_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29748_GM};
static MethodInfo* t4431_MIs[] =
{
	&m29748_MI,
	NULL
};
static TypeInfo* t4431_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4431_0_0_0;
extern Il2CppType t4431_1_0_0;
struct t4431;
extern Il2CppGenericClass t4431_GC;
TypeInfo t4431_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4431_MIs, t4431_PIs, NULL, NULL, NULL, NULL, NULL, &t4431_TI, t4431_ITIs, NULL, &EmptyCustomAttributesCache, &t4431_TI, &t4431_0_0_0, &t4431_1_0_0, NULL, &t4431_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3011.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3011_TI;
#include "t3011MD.h"

extern TypeInfo t229_TI;
extern MethodInfo m16463_MI;
extern MethodInfo m22616_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m22616(__this, p0, method) (t229 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Animator>
extern Il2CppType t20_0_0_1;
FieldInfo t3011_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3011_TI, offsetof(t3011, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3011_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3011_TI, offsetof(t3011, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3011_FIs[] =
{
	&t3011_f0_FieldInfo,
	&t3011_f1_FieldInfo,
	NULL
};
extern MethodInfo m16460_MI;
static PropertyInfo t3011____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3011_TI, "System.Collections.IEnumerator.Current", &m16460_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3011____Current_PropertyInfo = 
{
	&t3011_TI, "Current", &m16463_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3011_PIs[] =
{
	&t3011____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3011____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3011_m16459_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16459_GM;
MethodInfo m16459_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3011_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3011_m16459_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16459_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16460_GM;
MethodInfo m16460_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3011_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16460_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16461_GM;
MethodInfo m16461_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3011_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16461_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16462_GM;
MethodInfo m16462_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3011_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16462_GM};
extern Il2CppType t229_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16463_GM;
MethodInfo m16463_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3011_TI, &t229_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16463_GM};
static MethodInfo* t3011_MIs[] =
{
	&m16459_MI,
	&m16460_MI,
	&m16461_MI,
	&m16462_MI,
	&m16463_MI,
	NULL
};
extern MethodInfo m16462_MI;
extern MethodInfo m16461_MI;
static MethodInfo* t3011_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16460_MI,
	&m16462_MI,
	&m16461_MI,
	&m16463_MI,
};
static TypeInfo* t3011_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4431_TI,
};
static Il2CppInterfaceOffsetPair t3011_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4431_TI, 7},
};
extern TypeInfo t229_TI;
static Il2CppRGCTXData t3011_RGCTXData[3] = 
{
	&m16463_MI/* Method Usage */,
	&t229_TI/* Class Usage */,
	&m22616_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3011_0_0_0;
extern Il2CppType t3011_1_0_0;
extern Il2CppGenericClass t3011_GC;
TypeInfo t3011_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3011_MIs, t3011_PIs, t3011_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3011_TI, t3011_ITIs, t3011_VT, &EmptyCustomAttributesCache, &t3011_TI, &t3011_0_0_0, &t3011_1_0_0, t3011_IOs, &t3011_GC, NULL, NULL, NULL, t3011_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3011)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5688_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Animator>
extern MethodInfo m29749_MI;
static PropertyInfo t5688____Count_PropertyInfo = 
{
	&t5688_TI, "Count", &m29749_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29750_MI;
static PropertyInfo t5688____IsReadOnly_PropertyInfo = 
{
	&t5688_TI, "IsReadOnly", &m29750_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5688_PIs[] =
{
	&t5688____Count_PropertyInfo,
	&t5688____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29749_GM;
MethodInfo m29749_MI = 
{
	"get_Count", NULL, &t5688_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29749_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29750_GM;
MethodInfo m29750_MI = 
{
	"get_IsReadOnly", NULL, &t5688_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29750_GM};
extern Il2CppType t229_0_0_0;
extern Il2CppType t229_0_0_0;
static ParameterInfo t5688_m29751_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29751_GM;
MethodInfo m29751_MI = 
{
	"Add", NULL, &t5688_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5688_m29751_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29751_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29752_GM;
MethodInfo m29752_MI = 
{
	"Clear", NULL, &t5688_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29752_GM};
extern Il2CppType t229_0_0_0;
static ParameterInfo t5688_m29753_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29753_GM;
MethodInfo m29753_MI = 
{
	"Contains", NULL, &t5688_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5688_m29753_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29753_GM};
extern Il2CppType t3768_0_0_0;
extern Il2CppType t3768_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5688_m29754_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3768_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29754_GM;
MethodInfo m29754_MI = 
{
	"CopyTo", NULL, &t5688_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5688_m29754_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29754_GM};
extern Il2CppType t229_0_0_0;
static ParameterInfo t5688_m29755_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29755_GM;
MethodInfo m29755_MI = 
{
	"Remove", NULL, &t5688_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5688_m29755_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29755_GM};
static MethodInfo* t5688_MIs[] =
{
	&m29749_MI,
	&m29750_MI,
	&m29751_MI,
	&m29752_MI,
	&m29753_MI,
	&m29754_MI,
	&m29755_MI,
	NULL
};
extern TypeInfo t5690_TI;
static TypeInfo* t5688_ITIs[] = 
{
	&t603_TI,
	&t5690_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5688_0_0_0;
extern Il2CppType t5688_1_0_0;
struct t5688;
extern Il2CppGenericClass t5688_GC;
TypeInfo t5688_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5688_MIs, t5688_PIs, NULL, NULL, NULL, NULL, NULL, &t5688_TI, t5688_ITIs, NULL, &EmptyCustomAttributesCache, &t5688_TI, &t5688_0_0_0, &t5688_1_0_0, NULL, &t5688_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Animator>
extern Il2CppType t4431_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29756_GM;
MethodInfo m29756_MI = 
{
	"GetEnumerator", NULL, &t5690_TI, &t4431_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29756_GM};
static MethodInfo* t5690_MIs[] =
{
	&m29756_MI,
	NULL
};
static TypeInfo* t5690_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5690_0_0_0;
extern Il2CppType t5690_1_0_0;
struct t5690;
extern Il2CppGenericClass t5690_GC;
TypeInfo t5690_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5690_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5690_TI, t5690_ITIs, NULL, &EmptyCustomAttributesCache, &t5690_TI, &t5690_0_0_0, &t5690_1_0_0, NULL, &t5690_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5689_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Animator>
extern MethodInfo m29757_MI;
extern MethodInfo m29758_MI;
static PropertyInfo t5689____Item_PropertyInfo = 
{
	&t5689_TI, "Item", &m29757_MI, &m29758_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5689_PIs[] =
{
	&t5689____Item_PropertyInfo,
	NULL
};
extern Il2CppType t229_0_0_0;
static ParameterInfo t5689_m29759_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29759_GM;
MethodInfo m29759_MI = 
{
	"IndexOf", NULL, &t5689_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5689_m29759_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29759_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t229_0_0_0;
static ParameterInfo t5689_m29760_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29760_GM;
MethodInfo m29760_MI = 
{
	"Insert", NULL, &t5689_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5689_m29760_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29760_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5689_m29761_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29761_GM;
MethodInfo m29761_MI = 
{
	"RemoveAt", NULL, &t5689_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5689_m29761_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29761_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5689_m29757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t229_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29757_GM;
MethodInfo m29757_MI = 
{
	"get_Item", NULL, &t5689_TI, &t229_0_0_0, RuntimeInvoker_t29_t44, t5689_m29757_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29757_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t229_0_0_0;
static ParameterInfo t5689_m29758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29758_GM;
MethodInfo m29758_MI = 
{
	"set_Item", NULL, &t5689_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5689_m29758_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29758_GM};
static MethodInfo* t5689_MIs[] =
{
	&m29759_MI,
	&m29760_MI,
	&m29761_MI,
	&m29757_MI,
	&m29758_MI,
	NULL
};
static TypeInfo* t5689_ITIs[] = 
{
	&t603_TI,
	&t5688_TI,
	&t5690_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5689_0_0_0;
extern Il2CppType t5689_1_0_0;
struct t5689;
extern Il2CppGenericClass t5689_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5689_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5689_MIs, t5689_PIs, NULL, NULL, NULL, NULL, NULL, &t5689_TI, t5689_ITIs, NULL, &t1908__CustomAttributeCache, &t5689_TI, &t5689_0_0_0, &t5689_1_0_0, NULL, &t5689_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3012.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3012_TI;
#include "t3012MD.h"

#include "t41.h"
#include "t557.h"
#include "mscorlib_ArrayTypes.h"
#include "t3013.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t3013_TI;
extern TypeInfo t21_TI;
#include "t3013MD.h"
extern MethodInfo m16466_MI;
extern MethodInfo m16468_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>
extern Il2CppType t316_0_0_33;
FieldInfo t3012_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3012_TI, offsetof(t3012, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3012_FIs[] =
{
	&t3012_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t229_0_0_0;
static ParameterInfo t3012_m16464_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16464_GM;
MethodInfo m16464_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3012_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3012_m16464_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16464_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t3012_m16465_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16465_GM;
MethodInfo m16465_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3012_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3012_m16465_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16465_GM};
static MethodInfo* t3012_MIs[] =
{
	&m16464_MI,
	&m16465_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m16465_MI;
extern MethodInfo m16469_MI;
static MethodInfo* t3012_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16465_MI,
	&m16469_MI,
};
extern Il2CppType t3014_0_0_0;
extern TypeInfo t3014_TI;
extern MethodInfo m22626_MI;
extern TypeInfo t229_TI;
extern MethodInfo m16471_MI;
extern TypeInfo t229_TI;
static Il2CppRGCTXData t3012_RGCTXData[8] = 
{
	&t3014_0_0_0/* Type Usage */,
	&t3014_TI/* Class Usage */,
	&m22626_MI/* Method Usage */,
	&t229_TI/* Class Usage */,
	&m16471_MI/* Method Usage */,
	&m16466_MI/* Method Usage */,
	&t229_TI/* Class Usage */,
	&m16468_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3012_0_0_0;
extern Il2CppType t3012_1_0_0;
struct t3012;
extern Il2CppGenericClass t3012_GC;
TypeInfo t3012_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3012_MIs, NULL, t3012_FIs, NULL, &t3013_TI, NULL, NULL, &t3012_TI, NULL, t3012_VT, &EmptyCustomAttributesCache, &t3012_TI, &t3012_0_0_0, &t3012_1_0_0, NULL, &t3012_GC, NULL, NULL, NULL, t3012_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3012), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3014.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t3014_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t3014MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m22626(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>
extern Il2CppType t3014_0_0_1;
FieldInfo t3013_f0_FieldInfo = 
{
	"Delegate", &t3014_0_0_1, &t3013_TI, offsetof(t3013, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3013_FIs[] =
{
	&t3013_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3013_m16466_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16466_GM;
MethodInfo m16466_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3013_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3013_m16466_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16466_GM};
extern Il2CppType t3014_0_0_0;
static ParameterInfo t3013_m16467_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3014_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16467_GM;
MethodInfo m16467_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3013_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3013_m16467_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16467_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3013_m16468_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16468_GM;
MethodInfo m16468_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3013_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3013_m16468_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16468_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3013_m16469_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16469_GM;
MethodInfo m16469_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3013_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3013_m16469_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16469_GM};
static MethodInfo* t3013_MIs[] =
{
	&m16466_MI,
	&m16467_MI,
	&m16468_MI,
	&m16469_MI,
	NULL
};
static MethodInfo* t3013_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16468_MI,
	&m16469_MI,
};
extern TypeInfo t3014_TI;
extern TypeInfo t229_TI;
static Il2CppRGCTXData t3013_RGCTXData[5] = 
{
	&t3014_0_0_0/* Type Usage */,
	&t3014_TI/* Class Usage */,
	&m22626_MI/* Method Usage */,
	&t229_TI/* Class Usage */,
	&m16471_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3013_0_0_0;
extern Il2CppType t3013_1_0_0;
extern TypeInfo t556_TI;
struct t3013;
extern Il2CppGenericClass t3013_GC;
TypeInfo t3013_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3013_MIs, NULL, t3013_FIs, NULL, &t556_TI, NULL, NULL, &t3013_TI, NULL, t3013_VT, &EmptyCustomAttributesCache, &t3013_TI, &t3013_0_0_0, &t3013_1_0_0, NULL, &t3013_GC, NULL, NULL, NULL, t3013_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3013), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Animator>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3014_m16470_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16470_GM;
MethodInfo m16470_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3014_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3014_m16470_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16470_GM};
extern Il2CppType t229_0_0_0;
static ParameterInfo t3014_m16471_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16471_GM;
MethodInfo m16471_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3014_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3014_m16471_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16471_GM};
extern Il2CppType t229_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3014_m16472_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16472_GM;
MethodInfo m16472_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3014_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3014_m16472_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16472_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3014_m16473_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16473_GM;
MethodInfo m16473_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3014_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3014_m16473_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16473_GM};
static MethodInfo* t3014_MIs[] =
{
	&m16470_MI,
	&m16471_MI,
	&m16472_MI,
	&m16473_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m16472_MI;
extern MethodInfo m16473_MI;
static MethodInfo* t3014_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16471_MI,
	&m16472_MI,
	&m16473_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t3014_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3014_1_0_0;
extern TypeInfo t195_TI;
struct t3014;
extern Il2CppGenericClass t3014_GC;
TypeInfo t3014_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3014_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3014_TI, NULL, t3014_VT, &EmptyCustomAttributesCache, &t3014_TI, &t3014_0_0_0, &t3014_1_0_0, t3014_IOs, &t3014_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3014), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4433_TI;

#include "t398.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RuntimeAnimatorController>
extern MethodInfo m29762_MI;
static PropertyInfo t4433____Current_PropertyInfo = 
{
	&t4433_TI, "Current", &m29762_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4433_PIs[] =
{
	&t4433____Current_PropertyInfo,
	NULL
};
extern Il2CppType t398_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29762_GM;
MethodInfo m29762_MI = 
{
	"get_Current", NULL, &t4433_TI, &t398_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29762_GM};
static MethodInfo* t4433_MIs[] =
{
	&m29762_MI,
	NULL
};
static TypeInfo* t4433_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4433_0_0_0;
extern Il2CppType t4433_1_0_0;
struct t4433;
extern Il2CppGenericClass t4433_GC;
TypeInfo t4433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4433_MIs, t4433_PIs, NULL, NULL, NULL, NULL, NULL, &t4433_TI, t4433_ITIs, NULL, &EmptyCustomAttributesCache, &t4433_TI, &t4433_0_0_0, &t4433_1_0_0, NULL, &t4433_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3015.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3015_TI;
#include "t3015MD.h"

extern TypeInfo t398_TI;
extern MethodInfo m16478_MI;
extern MethodInfo m22628_MI;
struct t20;
#define m22628(__this, p0, method) (t398 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType t20_0_0_1;
FieldInfo t3015_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3015_TI, offsetof(t3015, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3015_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3015_TI, offsetof(t3015, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3015_FIs[] =
{
	&t3015_f0_FieldInfo,
	&t3015_f1_FieldInfo,
	NULL
};
extern MethodInfo m16475_MI;
static PropertyInfo t3015____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3015_TI, "System.Collections.IEnumerator.Current", &m16475_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3015____Current_PropertyInfo = 
{
	&t3015_TI, "Current", &m16478_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3015_PIs[] =
{
	&t3015____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3015____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3015_m16474_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16474_GM;
MethodInfo m16474_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3015_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3015_m16474_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16474_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16475_GM;
MethodInfo m16475_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3015_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16475_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16476_GM;
MethodInfo m16476_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3015_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16476_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16477_GM;
MethodInfo m16477_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3015_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16477_GM};
extern Il2CppType t398_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16478_GM;
MethodInfo m16478_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3015_TI, &t398_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16478_GM};
static MethodInfo* t3015_MIs[] =
{
	&m16474_MI,
	&m16475_MI,
	&m16476_MI,
	&m16477_MI,
	&m16478_MI,
	NULL
};
extern MethodInfo m16477_MI;
extern MethodInfo m16476_MI;
static MethodInfo* t3015_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16475_MI,
	&m16477_MI,
	&m16476_MI,
	&m16478_MI,
};
static TypeInfo* t3015_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4433_TI,
};
static Il2CppInterfaceOffsetPair t3015_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4433_TI, 7},
};
extern TypeInfo t398_TI;
static Il2CppRGCTXData t3015_RGCTXData[3] = 
{
	&m16478_MI/* Method Usage */,
	&t398_TI/* Class Usage */,
	&m22628_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3015_0_0_0;
extern Il2CppType t3015_1_0_0;
extern Il2CppGenericClass t3015_GC;
TypeInfo t3015_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3015_MIs, t3015_PIs, t3015_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3015_TI, t3015_ITIs, t3015_VT, &EmptyCustomAttributesCache, &t3015_TI, &t3015_0_0_0, &t3015_1_0_0, t3015_IOs, &t3015_GC, NULL, NULL, NULL, t3015_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3015)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5691_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>
extern MethodInfo m29763_MI;
static PropertyInfo t5691____Count_PropertyInfo = 
{
	&t5691_TI, "Count", &m29763_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29764_MI;
static PropertyInfo t5691____IsReadOnly_PropertyInfo = 
{
	&t5691_TI, "IsReadOnly", &m29764_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5691_PIs[] =
{
	&t5691____Count_PropertyInfo,
	&t5691____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29763_GM;
MethodInfo m29763_MI = 
{
	"get_Count", NULL, &t5691_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29763_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29764_GM;
MethodInfo m29764_MI = 
{
	"get_IsReadOnly", NULL, &t5691_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29764_GM};
extern Il2CppType t398_0_0_0;
extern Il2CppType t398_0_0_0;
static ParameterInfo t5691_m29765_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29765_GM;
MethodInfo m29765_MI = 
{
	"Add", NULL, &t5691_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5691_m29765_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29765_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29766_GM;
MethodInfo m29766_MI = 
{
	"Clear", NULL, &t5691_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29766_GM};
extern Il2CppType t398_0_0_0;
static ParameterInfo t5691_m29767_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29767_GM;
MethodInfo m29767_MI = 
{
	"Contains", NULL, &t5691_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5691_m29767_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29767_GM};
extern Il2CppType t3769_0_0_0;
extern Il2CppType t3769_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5691_m29768_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3769_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29768_GM;
MethodInfo m29768_MI = 
{
	"CopyTo", NULL, &t5691_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5691_m29768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29768_GM};
extern Il2CppType t398_0_0_0;
static ParameterInfo t5691_m29769_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29769_GM;
MethodInfo m29769_MI = 
{
	"Remove", NULL, &t5691_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5691_m29769_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29769_GM};
static MethodInfo* t5691_MIs[] =
{
	&m29763_MI,
	&m29764_MI,
	&m29765_MI,
	&m29766_MI,
	&m29767_MI,
	&m29768_MI,
	&m29769_MI,
	NULL
};
extern TypeInfo t5693_TI;
static TypeInfo* t5691_ITIs[] = 
{
	&t603_TI,
	&t5693_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5691_0_0_0;
extern Il2CppType t5691_1_0_0;
struct t5691;
extern Il2CppGenericClass t5691_GC;
TypeInfo t5691_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5691_MIs, t5691_PIs, NULL, NULL, NULL, NULL, NULL, &t5691_TI, t5691_ITIs, NULL, &EmptyCustomAttributesCache, &t5691_TI, &t5691_0_0_0, &t5691_1_0_0, NULL, &t5691_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType t4433_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29770_GM;
MethodInfo m29770_MI = 
{
	"GetEnumerator", NULL, &t5693_TI, &t4433_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29770_GM};
static MethodInfo* t5693_MIs[] =
{
	&m29770_MI,
	NULL
};
static TypeInfo* t5693_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5693_0_0_0;
extern Il2CppType t5693_1_0_0;
struct t5693;
extern Il2CppGenericClass t5693_GC;
TypeInfo t5693_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5693_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5693_TI, t5693_ITIs, NULL, &EmptyCustomAttributesCache, &t5693_TI, &t5693_0_0_0, &t5693_1_0_0, NULL, &t5693_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5692_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>
extern MethodInfo m29771_MI;
extern MethodInfo m29772_MI;
static PropertyInfo t5692____Item_PropertyInfo = 
{
	&t5692_TI, "Item", &m29771_MI, &m29772_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5692_PIs[] =
{
	&t5692____Item_PropertyInfo,
	NULL
};
extern Il2CppType t398_0_0_0;
static ParameterInfo t5692_m29773_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29773_GM;
MethodInfo m29773_MI = 
{
	"IndexOf", NULL, &t5692_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5692_m29773_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29773_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t398_0_0_0;
static ParameterInfo t5692_m29774_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29774_GM;
MethodInfo m29774_MI = 
{
	"Insert", NULL, &t5692_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5692_m29774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29774_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5692_m29775_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29775_GM;
MethodInfo m29775_MI = 
{
	"RemoveAt", NULL, &t5692_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5692_m29775_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29775_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5692_m29771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t398_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29771_GM;
MethodInfo m29771_MI = 
{
	"get_Item", NULL, &t5692_TI, &t398_0_0_0, RuntimeInvoker_t29_t44, t5692_m29771_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29771_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t398_0_0_0;
static ParameterInfo t5692_m29772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29772_GM;
MethodInfo m29772_MI = 
{
	"set_Item", NULL, &t5692_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5692_m29772_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29772_GM};
static MethodInfo* t5692_MIs[] =
{
	&m29773_MI,
	&m29774_MI,
	&m29775_MI,
	&m29771_MI,
	&m29772_MI,
	NULL
};
static TypeInfo* t5692_ITIs[] = 
{
	&t603_TI,
	&t5691_TI,
	&t5693_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5692_0_0_0;
extern Il2CppType t5692_1_0_0;
struct t5692;
extern Il2CppGenericClass t5692_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5692_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5692_MIs, t5692_PIs, NULL, NULL, NULL, NULL, NULL, &t5692_TI, t5692_ITIs, NULL, &t1908__CustomAttributeCache, &t5692_TI, &t5692_0_0_0, &t5692_1_0_0, NULL, &t5692_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3016.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3016_TI;
#include "t3016MD.h"

#include "t3017.h"
extern TypeInfo t3017_TI;
#include "t3017MD.h"
extern MethodInfo m16481_MI;
extern MethodInfo m16483_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType t316_0_0_33;
FieldInfo t3016_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3016_TI, offsetof(t3016, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3016_FIs[] =
{
	&t3016_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t398_0_0_0;
static ParameterInfo t3016_m16479_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16479_GM;
MethodInfo m16479_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3016_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3016_m16479_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16479_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3016_m16480_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16480_GM;
MethodInfo m16480_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3016_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3016_m16480_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16480_GM};
static MethodInfo* t3016_MIs[] =
{
	&m16479_MI,
	&m16480_MI,
	NULL
};
extern MethodInfo m16480_MI;
extern MethodInfo m16484_MI;
static MethodInfo* t3016_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16480_MI,
	&m16484_MI,
};
extern Il2CppType t3018_0_0_0;
extern TypeInfo t3018_TI;
extern MethodInfo m22638_MI;
extern TypeInfo t398_TI;
extern MethodInfo m16486_MI;
extern TypeInfo t398_TI;
static Il2CppRGCTXData t3016_RGCTXData[8] = 
{
	&t3018_0_0_0/* Type Usage */,
	&t3018_TI/* Class Usage */,
	&m22638_MI/* Method Usage */,
	&t398_TI/* Class Usage */,
	&m16486_MI/* Method Usage */,
	&m16481_MI/* Method Usage */,
	&t398_TI/* Class Usage */,
	&m16483_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3016_0_0_0;
extern Il2CppType t3016_1_0_0;
struct t3016;
extern Il2CppGenericClass t3016_GC;
TypeInfo t3016_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3016_MIs, NULL, t3016_FIs, NULL, &t3017_TI, NULL, NULL, &t3016_TI, NULL, t3016_VT, &EmptyCustomAttributesCache, &t3016_TI, &t3016_0_0_0, &t3016_1_0_0, NULL, &t3016_GC, NULL, NULL, NULL, t3016_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3016), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3018.h"
extern TypeInfo t3018_TI;
#include "t3018MD.h"
struct t556;
#define m22638(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType t3018_0_0_1;
FieldInfo t3017_f0_FieldInfo = 
{
	"Delegate", &t3018_0_0_1, &t3017_TI, offsetof(t3017, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3017_FIs[] =
{
	&t3017_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3017_m16481_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16481_GM;
MethodInfo m16481_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3017_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3017_m16481_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16481_GM};
extern Il2CppType t3018_0_0_0;
static ParameterInfo t3017_m16482_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16482_GM;
MethodInfo m16482_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3017_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3017_m16482_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16482_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3017_m16483_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16483_GM;
MethodInfo m16483_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3017_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3017_m16483_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16483_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3017_m16484_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16484_GM;
MethodInfo m16484_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3017_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3017_m16484_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16484_GM};
static MethodInfo* t3017_MIs[] =
{
	&m16481_MI,
	&m16482_MI,
	&m16483_MI,
	&m16484_MI,
	NULL
};
static MethodInfo* t3017_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16483_MI,
	&m16484_MI,
};
extern TypeInfo t3018_TI;
extern TypeInfo t398_TI;
static Il2CppRGCTXData t3017_RGCTXData[5] = 
{
	&t3018_0_0_0/* Type Usage */,
	&t3018_TI/* Class Usage */,
	&m22638_MI/* Method Usage */,
	&t398_TI/* Class Usage */,
	&m16486_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3017_0_0_0;
extern Il2CppType t3017_1_0_0;
struct t3017;
extern Il2CppGenericClass t3017_GC;
TypeInfo t3017_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3017_MIs, NULL, t3017_FIs, NULL, &t556_TI, NULL, NULL, &t3017_TI, NULL, t3017_VT, &EmptyCustomAttributesCache, &t3017_TI, &t3017_0_0_0, &t3017_1_0_0, NULL, &t3017_GC, NULL, NULL, NULL, t3017_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3017), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3018_m16485_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16485_GM;
MethodInfo m16485_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3018_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3018_m16485_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16485_GM};
extern Il2CppType t398_0_0_0;
static ParameterInfo t3018_m16486_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16486_GM;
MethodInfo m16486_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3018_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3018_m16486_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16486_GM};
extern Il2CppType t398_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3018_m16487_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t398_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16487_GM;
MethodInfo m16487_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3018_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3018_m16487_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16487_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3018_m16488_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16488_GM;
MethodInfo m16488_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3018_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3018_m16488_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16488_GM};
static MethodInfo* t3018_MIs[] =
{
	&m16485_MI,
	&m16486_MI,
	&m16487_MI,
	&m16488_MI,
	NULL
};
extern MethodInfo m16487_MI;
extern MethodInfo m16488_MI;
static MethodInfo* t3018_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16486_MI,
	&m16487_MI,
	&m16488_MI,
};
static Il2CppInterfaceOffsetPair t3018_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3018_1_0_0;
struct t3018;
extern Il2CppGenericClass t3018_GC;
TypeInfo t3018_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3018_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3018_TI, NULL, t3018_VT, &EmptyCustomAttributesCache, &t3018_TI, &t3018_0_0_0, &t3018_1_0_0, t3018_IOs, &t3018_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3018), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4435_TI;

#include "t525.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Terrain>
extern MethodInfo m29776_MI;
static PropertyInfo t4435____Current_PropertyInfo = 
{
	&t4435_TI, "Current", &m29776_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4435_PIs[] =
{
	&t4435____Current_PropertyInfo,
	NULL
};
extern Il2CppType t525_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29776_GM;
MethodInfo m29776_MI = 
{
	"get_Current", NULL, &t4435_TI, &t525_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29776_GM};
static MethodInfo* t4435_MIs[] =
{
	&m29776_MI,
	NULL
};
static TypeInfo* t4435_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4435_0_0_0;
extern Il2CppType t4435_1_0_0;
struct t4435;
extern Il2CppGenericClass t4435_GC;
TypeInfo t4435_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4435_MIs, t4435_PIs, NULL, NULL, NULL, NULL, NULL, &t4435_TI, t4435_ITIs, NULL, &EmptyCustomAttributesCache, &t4435_TI, &t4435_0_0_0, &t4435_1_0_0, NULL, &t4435_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3019.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3019_TI;
#include "t3019MD.h"

extern TypeInfo t525_TI;
extern MethodInfo m16493_MI;
extern MethodInfo m22640_MI;
struct t20;
#define m22640(__this, p0, method) (t525 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Terrain>
extern Il2CppType t20_0_0_1;
FieldInfo t3019_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3019_TI, offsetof(t3019, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3019_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3019_TI, offsetof(t3019, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3019_FIs[] =
{
	&t3019_f0_FieldInfo,
	&t3019_f1_FieldInfo,
	NULL
};
extern MethodInfo m16490_MI;
static PropertyInfo t3019____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3019_TI, "System.Collections.IEnumerator.Current", &m16490_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3019____Current_PropertyInfo = 
{
	&t3019_TI, "Current", &m16493_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3019_PIs[] =
{
	&t3019____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3019____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3019_m16489_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16489_GM;
MethodInfo m16489_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3019_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3019_m16489_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16489_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16490_GM;
MethodInfo m16490_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3019_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16490_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16491_GM;
MethodInfo m16491_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3019_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16491_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16492_GM;
MethodInfo m16492_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3019_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16492_GM};
extern Il2CppType t525_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16493_GM;
MethodInfo m16493_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3019_TI, &t525_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16493_GM};
static MethodInfo* t3019_MIs[] =
{
	&m16489_MI,
	&m16490_MI,
	&m16491_MI,
	&m16492_MI,
	&m16493_MI,
	NULL
};
extern MethodInfo m16492_MI;
extern MethodInfo m16491_MI;
static MethodInfo* t3019_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16490_MI,
	&m16492_MI,
	&m16491_MI,
	&m16493_MI,
};
static TypeInfo* t3019_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4435_TI,
};
static Il2CppInterfaceOffsetPair t3019_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4435_TI, 7},
};
extern TypeInfo t525_TI;
static Il2CppRGCTXData t3019_RGCTXData[3] = 
{
	&m16493_MI/* Method Usage */,
	&t525_TI/* Class Usage */,
	&m22640_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3019_0_0_0;
extern Il2CppType t3019_1_0_0;
extern Il2CppGenericClass t3019_GC;
TypeInfo t3019_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3019_MIs, t3019_PIs, t3019_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3019_TI, t3019_ITIs, t3019_VT, &EmptyCustomAttributesCache, &t3019_TI, &t3019_0_0_0, &t3019_1_0_0, t3019_IOs, &t3019_GC, NULL, NULL, NULL, t3019_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3019)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5694_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Terrain>
extern MethodInfo m29777_MI;
static PropertyInfo t5694____Count_PropertyInfo = 
{
	&t5694_TI, "Count", &m29777_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29778_MI;
static PropertyInfo t5694____IsReadOnly_PropertyInfo = 
{
	&t5694_TI, "IsReadOnly", &m29778_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5694_PIs[] =
{
	&t5694____Count_PropertyInfo,
	&t5694____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29777_GM;
MethodInfo m29777_MI = 
{
	"get_Count", NULL, &t5694_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29777_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29778_GM;
MethodInfo m29778_MI = 
{
	"get_IsReadOnly", NULL, &t5694_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29778_GM};
extern Il2CppType t525_0_0_0;
extern Il2CppType t525_0_0_0;
static ParameterInfo t5694_m29779_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29779_GM;
MethodInfo m29779_MI = 
{
	"Add", NULL, &t5694_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5694_m29779_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29779_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29780_GM;
MethodInfo m29780_MI = 
{
	"Clear", NULL, &t5694_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29780_GM};
extern Il2CppType t525_0_0_0;
static ParameterInfo t5694_m29781_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29781_GM;
MethodInfo m29781_MI = 
{
	"Contains", NULL, &t5694_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5694_m29781_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29781_GM};
extern Il2CppType t3770_0_0_0;
extern Il2CppType t3770_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5694_m29782_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3770_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29782_GM;
MethodInfo m29782_MI = 
{
	"CopyTo", NULL, &t5694_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5694_m29782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29782_GM};
extern Il2CppType t525_0_0_0;
static ParameterInfo t5694_m29783_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29783_GM;
MethodInfo m29783_MI = 
{
	"Remove", NULL, &t5694_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5694_m29783_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29783_GM};
static MethodInfo* t5694_MIs[] =
{
	&m29777_MI,
	&m29778_MI,
	&m29779_MI,
	&m29780_MI,
	&m29781_MI,
	&m29782_MI,
	&m29783_MI,
	NULL
};
extern TypeInfo t5696_TI;
static TypeInfo* t5694_ITIs[] = 
{
	&t603_TI,
	&t5696_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5694_0_0_0;
extern Il2CppType t5694_1_0_0;
struct t5694;
extern Il2CppGenericClass t5694_GC;
TypeInfo t5694_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5694_MIs, t5694_PIs, NULL, NULL, NULL, NULL, NULL, &t5694_TI, t5694_ITIs, NULL, &EmptyCustomAttributesCache, &t5694_TI, &t5694_0_0_0, &t5694_1_0_0, NULL, &t5694_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Terrain>
extern Il2CppType t4435_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29784_GM;
MethodInfo m29784_MI = 
{
	"GetEnumerator", NULL, &t5696_TI, &t4435_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29784_GM};
static MethodInfo* t5696_MIs[] =
{
	&m29784_MI,
	NULL
};
static TypeInfo* t5696_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5696_0_0_0;
extern Il2CppType t5696_1_0_0;
struct t5696;
extern Il2CppGenericClass t5696_GC;
TypeInfo t5696_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5696_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5696_TI, t5696_ITIs, NULL, &EmptyCustomAttributesCache, &t5696_TI, &t5696_0_0_0, &t5696_1_0_0, NULL, &t5696_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5695_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Terrain>
extern MethodInfo m29785_MI;
extern MethodInfo m29786_MI;
static PropertyInfo t5695____Item_PropertyInfo = 
{
	&t5695_TI, "Item", &m29785_MI, &m29786_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5695_PIs[] =
{
	&t5695____Item_PropertyInfo,
	NULL
};
extern Il2CppType t525_0_0_0;
static ParameterInfo t5695_m29787_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29787_GM;
MethodInfo m29787_MI = 
{
	"IndexOf", NULL, &t5695_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5695_m29787_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29787_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t525_0_0_0;
static ParameterInfo t5695_m29788_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29788_GM;
MethodInfo m29788_MI = 
{
	"Insert", NULL, &t5695_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5695_m29788_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29788_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5695_m29789_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29789_GM;
MethodInfo m29789_MI = 
{
	"RemoveAt", NULL, &t5695_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5695_m29789_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29789_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5695_m29785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t525_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29785_GM;
MethodInfo m29785_MI = 
{
	"get_Item", NULL, &t5695_TI, &t525_0_0_0, RuntimeInvoker_t29_t44, t5695_m29785_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29785_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t525_0_0_0;
static ParameterInfo t5695_m29786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29786_GM;
MethodInfo m29786_MI = 
{
	"set_Item", NULL, &t5695_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5695_m29786_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29786_GM};
static MethodInfo* t5695_MIs[] =
{
	&m29787_MI,
	&m29788_MI,
	&m29789_MI,
	&m29785_MI,
	&m29786_MI,
	NULL
};
static TypeInfo* t5695_ITIs[] = 
{
	&t603_TI,
	&t5694_TI,
	&t5696_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5695_0_0_0;
extern Il2CppType t5695_1_0_0;
struct t5695;
extern Il2CppGenericClass t5695_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5695_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5695_MIs, t5695_PIs, NULL, NULL, NULL, NULL, NULL, &t5695_TI, t5695_ITIs, NULL, &t1908__CustomAttributeCache, &t5695_TI, &t5695_0_0_0, &t5695_1_0_0, NULL, &t5695_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3020.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3020_TI;
#include "t3020MD.h"

#include "t3021.h"
extern TypeInfo t3021_TI;
#include "t3021MD.h"
extern MethodInfo m16496_MI;
extern MethodInfo m16498_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>
extern Il2CppType t316_0_0_33;
FieldInfo t3020_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3020_TI, offsetof(t3020, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3020_FIs[] =
{
	&t3020_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t525_0_0_0;
static ParameterInfo t3020_m16494_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16494_GM;
MethodInfo m16494_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3020_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3020_m16494_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16494_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3020_m16495_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16495_GM;
MethodInfo m16495_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3020_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3020_m16495_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16495_GM};
static MethodInfo* t3020_MIs[] =
{
	&m16494_MI,
	&m16495_MI,
	NULL
};
extern MethodInfo m16495_MI;
extern MethodInfo m16499_MI;
static MethodInfo* t3020_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16495_MI,
	&m16499_MI,
};
extern Il2CppType t3022_0_0_0;
extern TypeInfo t3022_TI;
extern MethodInfo m22650_MI;
extern TypeInfo t525_TI;
extern MethodInfo m16501_MI;
extern TypeInfo t525_TI;
static Il2CppRGCTXData t3020_RGCTXData[8] = 
{
	&t3022_0_0_0/* Type Usage */,
	&t3022_TI/* Class Usage */,
	&m22650_MI/* Method Usage */,
	&t525_TI/* Class Usage */,
	&m16501_MI/* Method Usage */,
	&m16496_MI/* Method Usage */,
	&t525_TI/* Class Usage */,
	&m16498_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3020_0_0_0;
extern Il2CppType t3020_1_0_0;
struct t3020;
extern Il2CppGenericClass t3020_GC;
TypeInfo t3020_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3020_MIs, NULL, t3020_FIs, NULL, &t3021_TI, NULL, NULL, &t3020_TI, NULL, t3020_VT, &EmptyCustomAttributesCache, &t3020_TI, &t3020_0_0_0, &t3020_1_0_0, NULL, &t3020_GC, NULL, NULL, NULL, t3020_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3020), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3022.h"
extern TypeInfo t3022_TI;
#include "t3022MD.h"
struct t556;
#define m22650(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>
extern Il2CppType t3022_0_0_1;
FieldInfo t3021_f0_FieldInfo = 
{
	"Delegate", &t3022_0_0_1, &t3021_TI, offsetof(t3021, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3021_FIs[] =
{
	&t3021_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3021_m16496_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16496_GM;
MethodInfo m16496_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3021_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3021_m16496_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16496_GM};
extern Il2CppType t3022_0_0_0;
static ParameterInfo t3021_m16497_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3022_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16497_GM;
MethodInfo m16497_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3021_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3021_m16497_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16497_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3021_m16498_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16498_GM;
MethodInfo m16498_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3021_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3021_m16498_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16498_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3021_m16499_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16499_GM;
MethodInfo m16499_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3021_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3021_m16499_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16499_GM};
static MethodInfo* t3021_MIs[] =
{
	&m16496_MI,
	&m16497_MI,
	&m16498_MI,
	&m16499_MI,
	NULL
};
static MethodInfo* t3021_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16498_MI,
	&m16499_MI,
};
extern TypeInfo t3022_TI;
extern TypeInfo t525_TI;
static Il2CppRGCTXData t3021_RGCTXData[5] = 
{
	&t3022_0_0_0/* Type Usage */,
	&t3022_TI/* Class Usage */,
	&m22650_MI/* Method Usage */,
	&t525_TI/* Class Usage */,
	&m16501_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3021_0_0_0;
extern Il2CppType t3021_1_0_0;
struct t3021;
extern Il2CppGenericClass t3021_GC;
TypeInfo t3021_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3021_MIs, NULL, t3021_FIs, NULL, &t556_TI, NULL, NULL, &t3021_TI, NULL, t3021_VT, &EmptyCustomAttributesCache, &t3021_TI, &t3021_0_0_0, &t3021_1_0_0, NULL, &t3021_GC, NULL, NULL, NULL, t3021_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3021), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3022_m16500_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16500_GM;
MethodInfo m16500_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3022_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3022_m16500_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16500_GM};
extern Il2CppType t525_0_0_0;
static ParameterInfo t3022_m16501_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16501_GM;
MethodInfo m16501_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3022_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3022_m16501_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16501_GM};
extern Il2CppType t525_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3022_m16502_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t525_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16502_GM;
MethodInfo m16502_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3022_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3022_m16502_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16502_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3022_m16503_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16503_GM;
MethodInfo m16503_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3022_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3022_m16503_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16503_GM};
static MethodInfo* t3022_MIs[] =
{
	&m16500_MI,
	&m16501_MI,
	&m16502_MI,
	&m16503_MI,
	NULL
};
extern MethodInfo m16502_MI;
extern MethodInfo m16503_MI;
static MethodInfo* t3022_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16501_MI,
	&m16502_MI,
	&m16503_MI,
};
static Il2CppInterfaceOffsetPair t3022_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3022_1_0_0;
struct t3022;
extern Il2CppGenericClass t3022_GC;
TypeInfo t3022_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3022_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3022_TI, NULL, t3022_VT, &EmptyCustomAttributesCache, &t3022_TI, &t3022_0_0_0, &t3022_1_0_0, t3022_IOs, &t3022_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3022), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4436_TI;

#include "t150.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextAnchor>
extern MethodInfo m29790_MI;
static PropertyInfo t4436____Current_PropertyInfo = 
{
	&t4436_TI, "Current", &m29790_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4436_PIs[] =
{
	&t4436____Current_PropertyInfo,
	NULL
};
extern Il2CppType t150_0_0_0;
extern void* RuntimeInvoker_t150 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29790_GM;
MethodInfo m29790_MI = 
{
	"get_Current", NULL, &t4436_TI, &t150_0_0_0, RuntimeInvoker_t150, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29790_GM};
static MethodInfo* t4436_MIs[] =
{
	&m29790_MI,
	NULL
};
static TypeInfo* t4436_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4436_0_0_0;
extern Il2CppType t4436_1_0_0;
struct t4436;
extern Il2CppGenericClass t4436_GC;
TypeInfo t4436_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4436_MIs, t4436_PIs, NULL, NULL, NULL, NULL, NULL, &t4436_TI, t4436_ITIs, NULL, &EmptyCustomAttributesCache, &t4436_TI, &t4436_0_0_0, &t4436_1_0_0, NULL, &t4436_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3023.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3023_TI;
#include "t3023MD.h"

extern TypeInfo t150_TI;
extern MethodInfo m16508_MI;
extern MethodInfo m22652_MI;
struct t20;
 int32_t m22652 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16504_MI;
 void m16504 (t3023 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16505_MI;
 t29 * m16505 (t3023 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16508(__this, &m16508_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t150_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16506_MI;
 void m16506 (t3023 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16507_MI;
 bool m16507 (t3023 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16508 (t3023 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22652(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22652_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>
extern Il2CppType t20_0_0_1;
FieldInfo t3023_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3023_TI, offsetof(t3023, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3023_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3023_TI, offsetof(t3023, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3023_FIs[] =
{
	&t3023_f0_FieldInfo,
	&t3023_f1_FieldInfo,
	NULL
};
static PropertyInfo t3023____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3023_TI, "System.Collections.IEnumerator.Current", &m16505_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3023____Current_PropertyInfo = 
{
	&t3023_TI, "Current", &m16508_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3023_PIs[] =
{
	&t3023____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3023____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3023_m16504_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16504_GM;
MethodInfo m16504_MI = 
{
	".ctor", (methodPointerType)&m16504, &t3023_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3023_m16504_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16504_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16505_GM;
MethodInfo m16505_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16505, &t3023_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16505_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16506_GM;
MethodInfo m16506_MI = 
{
	"Dispose", (methodPointerType)&m16506, &t3023_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16506_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16507_GM;
MethodInfo m16507_MI = 
{
	"MoveNext", (methodPointerType)&m16507, &t3023_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16507_GM};
extern Il2CppType t150_0_0_0;
extern void* RuntimeInvoker_t150 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16508_GM;
MethodInfo m16508_MI = 
{
	"get_Current", (methodPointerType)&m16508, &t3023_TI, &t150_0_0_0, RuntimeInvoker_t150, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16508_GM};
static MethodInfo* t3023_MIs[] =
{
	&m16504_MI,
	&m16505_MI,
	&m16506_MI,
	&m16507_MI,
	&m16508_MI,
	NULL
};
static MethodInfo* t3023_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16505_MI,
	&m16507_MI,
	&m16506_MI,
	&m16508_MI,
};
static TypeInfo* t3023_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4436_TI,
};
static Il2CppInterfaceOffsetPair t3023_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4436_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3023_0_0_0;
extern Il2CppType t3023_1_0_0;
extern Il2CppGenericClass t3023_GC;
TypeInfo t3023_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3023_MIs, t3023_PIs, t3023_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3023_TI, t3023_ITIs, t3023_VT, &EmptyCustomAttributesCache, &t3023_TI, &t3023_0_0_0, &t3023_1_0_0, t3023_IOs, &t3023_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3023)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5697_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>
extern MethodInfo m29791_MI;
static PropertyInfo t5697____Count_PropertyInfo = 
{
	&t5697_TI, "Count", &m29791_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29792_MI;
static PropertyInfo t5697____IsReadOnly_PropertyInfo = 
{
	&t5697_TI, "IsReadOnly", &m29792_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5697_PIs[] =
{
	&t5697____Count_PropertyInfo,
	&t5697____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29791_GM;
MethodInfo m29791_MI = 
{
	"get_Count", NULL, &t5697_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29791_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29792_GM;
MethodInfo m29792_MI = 
{
	"get_IsReadOnly", NULL, &t5697_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29792_GM};
extern Il2CppType t150_0_0_0;
extern Il2CppType t150_0_0_0;
static ParameterInfo t5697_m29793_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29793_GM;
MethodInfo m29793_MI = 
{
	"Add", NULL, &t5697_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5697_m29793_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29793_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29794_GM;
MethodInfo m29794_MI = 
{
	"Clear", NULL, &t5697_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29794_GM};
extern Il2CppType t150_0_0_0;
static ParameterInfo t5697_m29795_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29795_GM;
MethodInfo m29795_MI = 
{
	"Contains", NULL, &t5697_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5697_m29795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29795_GM};
extern Il2CppType t3771_0_0_0;
extern Il2CppType t3771_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5697_m29796_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3771_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29796_GM;
MethodInfo m29796_MI = 
{
	"CopyTo", NULL, &t5697_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5697_m29796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29796_GM};
extern Il2CppType t150_0_0_0;
static ParameterInfo t5697_m29797_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29797_GM;
MethodInfo m29797_MI = 
{
	"Remove", NULL, &t5697_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5697_m29797_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29797_GM};
static MethodInfo* t5697_MIs[] =
{
	&m29791_MI,
	&m29792_MI,
	&m29793_MI,
	&m29794_MI,
	&m29795_MI,
	&m29796_MI,
	&m29797_MI,
	NULL
};
extern TypeInfo t5699_TI;
static TypeInfo* t5697_ITIs[] = 
{
	&t603_TI,
	&t5699_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5697_0_0_0;
extern Il2CppType t5697_1_0_0;
struct t5697;
extern Il2CppGenericClass t5697_GC;
TypeInfo t5697_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5697_MIs, t5697_PIs, NULL, NULL, NULL, NULL, NULL, &t5697_TI, t5697_ITIs, NULL, &EmptyCustomAttributesCache, &t5697_TI, &t5697_0_0_0, &t5697_1_0_0, NULL, &t5697_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextAnchor>
extern Il2CppType t4436_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29798_GM;
MethodInfo m29798_MI = 
{
	"GetEnumerator", NULL, &t5699_TI, &t4436_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29798_GM};
static MethodInfo* t5699_MIs[] =
{
	&m29798_MI,
	NULL
};
static TypeInfo* t5699_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5699_0_0_0;
extern Il2CppType t5699_1_0_0;
struct t5699;
extern Il2CppGenericClass t5699_GC;
TypeInfo t5699_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5699_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5699_TI, t5699_ITIs, NULL, &EmptyCustomAttributesCache, &t5699_TI, &t5699_0_0_0, &t5699_1_0_0, NULL, &t5699_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5698_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextAnchor>
extern MethodInfo m29799_MI;
extern MethodInfo m29800_MI;
static PropertyInfo t5698____Item_PropertyInfo = 
{
	&t5698_TI, "Item", &m29799_MI, &m29800_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5698_PIs[] =
{
	&t5698____Item_PropertyInfo,
	NULL
};
extern Il2CppType t150_0_0_0;
static ParameterInfo t5698_m29801_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29801_GM;
MethodInfo m29801_MI = 
{
	"IndexOf", NULL, &t5698_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5698_m29801_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29801_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t150_0_0_0;
static ParameterInfo t5698_m29802_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29802_GM;
MethodInfo m29802_MI = 
{
	"Insert", NULL, &t5698_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5698_m29802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29802_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5698_m29803_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29803_GM;
MethodInfo m29803_MI = 
{
	"RemoveAt", NULL, &t5698_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5698_m29803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29803_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5698_m29799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t150_0_0_0;
extern void* RuntimeInvoker_t150_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29799_GM;
MethodInfo m29799_MI = 
{
	"get_Item", NULL, &t5698_TI, &t150_0_0_0, RuntimeInvoker_t150_t44, t5698_m29799_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29799_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t150_0_0_0;
static ParameterInfo t5698_m29800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29800_GM;
MethodInfo m29800_MI = 
{
	"set_Item", NULL, &t5698_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5698_m29800_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29800_GM};
static MethodInfo* t5698_MIs[] =
{
	&m29801_MI,
	&m29802_MI,
	&m29803_MI,
	&m29799_MI,
	&m29800_MI,
	NULL
};
static TypeInfo* t5698_ITIs[] = 
{
	&t603_TI,
	&t5697_TI,
	&t5699_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5698_0_0_0;
extern Il2CppType t5698_1_0_0;
struct t5698;
extern Il2CppGenericClass t5698_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5698_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5698_MIs, t5698_PIs, NULL, NULL, NULL, NULL, NULL, &t5698_TI, t5698_ITIs, NULL, &t1908__CustomAttributeCache, &t5698_TI, &t5698_0_0_0, &t5698_1_0_0, NULL, &t5698_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4438_TI;

#include "t151.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.HorizontalWrapMode>
extern MethodInfo m29804_MI;
static PropertyInfo t4438____Current_PropertyInfo = 
{
	&t4438_TI, "Current", &m29804_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4438_PIs[] =
{
	&t4438____Current_PropertyInfo,
	NULL
};
extern Il2CppType t151_0_0_0;
extern void* RuntimeInvoker_t151 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29804_GM;
MethodInfo m29804_MI = 
{
	"get_Current", NULL, &t4438_TI, &t151_0_0_0, RuntimeInvoker_t151, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29804_GM};
static MethodInfo* t4438_MIs[] =
{
	&m29804_MI,
	NULL
};
static TypeInfo* t4438_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4438_0_0_0;
extern Il2CppType t4438_1_0_0;
struct t4438;
extern Il2CppGenericClass t4438_GC;
TypeInfo t4438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4438_MIs, t4438_PIs, NULL, NULL, NULL, NULL, NULL, &t4438_TI, t4438_ITIs, NULL, &EmptyCustomAttributesCache, &t4438_TI, &t4438_0_0_0, &t4438_1_0_0, NULL, &t4438_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3024.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3024_TI;
#include "t3024MD.h"

extern TypeInfo t151_TI;
extern MethodInfo m16513_MI;
extern MethodInfo m22663_MI;
struct t20;
 int32_t m22663 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16509_MI;
 void m16509 (t3024 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16510_MI;
 t29 * m16510 (t3024 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16513(__this, &m16513_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t151_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16511_MI;
 void m16511 (t3024 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16512_MI;
 bool m16512 (t3024 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16513 (t3024 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22663(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22663_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3024_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3024_TI, offsetof(t3024, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3024_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3024_TI, offsetof(t3024, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3024_FIs[] =
{
	&t3024_f0_FieldInfo,
	&t3024_f1_FieldInfo,
	NULL
};
static PropertyInfo t3024____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3024_TI, "System.Collections.IEnumerator.Current", &m16510_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3024____Current_PropertyInfo = 
{
	&t3024_TI, "Current", &m16513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3024_PIs[] =
{
	&t3024____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3024____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3024_m16509_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16509_GM;
MethodInfo m16509_MI = 
{
	".ctor", (methodPointerType)&m16509, &t3024_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3024_m16509_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16509_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16510_GM;
MethodInfo m16510_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16510, &t3024_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16510_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16511_GM;
MethodInfo m16511_MI = 
{
	"Dispose", (methodPointerType)&m16511, &t3024_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16511_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16512_GM;
MethodInfo m16512_MI = 
{
	"MoveNext", (methodPointerType)&m16512, &t3024_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16512_GM};
extern Il2CppType t151_0_0_0;
extern void* RuntimeInvoker_t151 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16513_GM;
MethodInfo m16513_MI = 
{
	"get_Current", (methodPointerType)&m16513, &t3024_TI, &t151_0_0_0, RuntimeInvoker_t151, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16513_GM};
static MethodInfo* t3024_MIs[] =
{
	&m16509_MI,
	&m16510_MI,
	&m16511_MI,
	&m16512_MI,
	&m16513_MI,
	NULL
};
static MethodInfo* t3024_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16510_MI,
	&m16512_MI,
	&m16511_MI,
	&m16513_MI,
};
static TypeInfo* t3024_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4438_TI,
};
static Il2CppInterfaceOffsetPair t3024_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4438_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3024_0_0_0;
extern Il2CppType t3024_1_0_0;
extern Il2CppGenericClass t3024_GC;
TypeInfo t3024_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3024_MIs, t3024_PIs, t3024_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3024_TI, t3024_ITIs, t3024_VT, &EmptyCustomAttributesCache, &t3024_TI, &t3024_0_0_0, &t3024_1_0_0, t3024_IOs, &t3024_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3024)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5700_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>
extern MethodInfo m29805_MI;
static PropertyInfo t5700____Count_PropertyInfo = 
{
	&t5700_TI, "Count", &m29805_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29806_MI;
static PropertyInfo t5700____IsReadOnly_PropertyInfo = 
{
	&t5700_TI, "IsReadOnly", &m29806_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5700_PIs[] =
{
	&t5700____Count_PropertyInfo,
	&t5700____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29805_GM;
MethodInfo m29805_MI = 
{
	"get_Count", NULL, &t5700_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29805_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29806_GM;
MethodInfo m29806_MI = 
{
	"get_IsReadOnly", NULL, &t5700_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29806_GM};
extern Il2CppType t151_0_0_0;
extern Il2CppType t151_0_0_0;
static ParameterInfo t5700_m29807_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29807_GM;
MethodInfo m29807_MI = 
{
	"Add", NULL, &t5700_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5700_m29807_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29807_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29808_GM;
MethodInfo m29808_MI = 
{
	"Clear", NULL, &t5700_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29808_GM};
extern Il2CppType t151_0_0_0;
static ParameterInfo t5700_m29809_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29809_GM;
MethodInfo m29809_MI = 
{
	"Contains", NULL, &t5700_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5700_m29809_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29809_GM};
extern Il2CppType t3772_0_0_0;
extern Il2CppType t3772_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5700_m29810_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3772_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29810_GM;
MethodInfo m29810_MI = 
{
	"CopyTo", NULL, &t5700_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5700_m29810_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29810_GM};
extern Il2CppType t151_0_0_0;
static ParameterInfo t5700_m29811_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29811_GM;
MethodInfo m29811_MI = 
{
	"Remove", NULL, &t5700_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5700_m29811_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29811_GM};
static MethodInfo* t5700_MIs[] =
{
	&m29805_MI,
	&m29806_MI,
	&m29807_MI,
	&m29808_MI,
	&m29809_MI,
	&m29810_MI,
	&m29811_MI,
	NULL
};
extern TypeInfo t5702_TI;
static TypeInfo* t5700_ITIs[] = 
{
	&t603_TI,
	&t5702_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5700_0_0_0;
extern Il2CppType t5700_1_0_0;
struct t5700;
extern Il2CppGenericClass t5700_GC;
TypeInfo t5700_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5700_MIs, t5700_PIs, NULL, NULL, NULL, NULL, NULL, &t5700_TI, t5700_ITIs, NULL, &EmptyCustomAttributesCache, &t5700_TI, &t5700_0_0_0, &t5700_1_0_0, NULL, &t5700_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.HorizontalWrapMode>
extern Il2CppType t4438_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29812_GM;
MethodInfo m29812_MI = 
{
	"GetEnumerator", NULL, &t5702_TI, &t4438_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29812_GM};
static MethodInfo* t5702_MIs[] =
{
	&m29812_MI,
	NULL
};
static TypeInfo* t5702_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5702_0_0_0;
extern Il2CppType t5702_1_0_0;
struct t5702;
extern Il2CppGenericClass t5702_GC;
TypeInfo t5702_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5702_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5702_TI, t5702_ITIs, NULL, &EmptyCustomAttributesCache, &t5702_TI, &t5702_0_0_0, &t5702_1_0_0, NULL, &t5702_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5701_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>
extern MethodInfo m29813_MI;
extern MethodInfo m29814_MI;
static PropertyInfo t5701____Item_PropertyInfo = 
{
	&t5701_TI, "Item", &m29813_MI, &m29814_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5701_PIs[] =
{
	&t5701____Item_PropertyInfo,
	NULL
};
extern Il2CppType t151_0_0_0;
static ParameterInfo t5701_m29815_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29815_GM;
MethodInfo m29815_MI = 
{
	"IndexOf", NULL, &t5701_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5701_m29815_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29815_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t151_0_0_0;
static ParameterInfo t5701_m29816_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29816_GM;
MethodInfo m29816_MI = 
{
	"Insert", NULL, &t5701_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5701_m29816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29816_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5701_m29817_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29817_GM;
MethodInfo m29817_MI = 
{
	"RemoveAt", NULL, &t5701_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5701_m29817_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29817_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5701_m29813_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t151_0_0_0;
extern void* RuntimeInvoker_t151_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29813_GM;
MethodInfo m29813_MI = 
{
	"get_Item", NULL, &t5701_TI, &t151_0_0_0, RuntimeInvoker_t151_t44, t5701_m29813_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29813_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t151_0_0_0;
static ParameterInfo t5701_m29814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29814_GM;
MethodInfo m29814_MI = 
{
	"set_Item", NULL, &t5701_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5701_m29814_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29814_GM};
static MethodInfo* t5701_MIs[] =
{
	&m29815_MI,
	&m29816_MI,
	&m29817_MI,
	&m29813_MI,
	&m29814_MI,
	NULL
};
static TypeInfo* t5701_ITIs[] = 
{
	&t603_TI,
	&t5700_TI,
	&t5702_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5701_0_0_0;
extern Il2CppType t5701_1_0_0;
struct t5701;
extern Il2CppGenericClass t5701_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5701_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5701_MIs, t5701_PIs, NULL, NULL, NULL, NULL, NULL, &t5701_TI, t5701_ITIs, NULL, &t1908__CustomAttributeCache, &t5701_TI, &t5701_0_0_0, &t5701_1_0_0, NULL, &t5701_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4440_TI;

#include "t152.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.VerticalWrapMode>
extern MethodInfo m29818_MI;
static PropertyInfo t4440____Current_PropertyInfo = 
{
	&t4440_TI, "Current", &m29818_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4440_PIs[] =
{
	&t4440____Current_PropertyInfo,
	NULL
};
extern Il2CppType t152_0_0_0;
extern void* RuntimeInvoker_t152 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29818_GM;
MethodInfo m29818_MI = 
{
	"get_Current", NULL, &t4440_TI, &t152_0_0_0, RuntimeInvoker_t152, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29818_GM};
static MethodInfo* t4440_MIs[] =
{
	&m29818_MI,
	NULL
};
static TypeInfo* t4440_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4440_0_0_0;
extern Il2CppType t4440_1_0_0;
struct t4440;
extern Il2CppGenericClass t4440_GC;
TypeInfo t4440_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4440_MIs, t4440_PIs, NULL, NULL, NULL, NULL, NULL, &t4440_TI, t4440_ITIs, NULL, &EmptyCustomAttributesCache, &t4440_TI, &t4440_0_0_0, &t4440_1_0_0, NULL, &t4440_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3025.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3025_TI;
#include "t3025MD.h"

extern TypeInfo t152_TI;
extern MethodInfo m16518_MI;
extern MethodInfo m22674_MI;
struct t20;
 int32_t m22674 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16514_MI;
 void m16514 (t3025 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16515_MI;
 t29 * m16515 (t3025 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16518(__this, &m16518_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t152_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16516_MI;
 void m16516 (t3025 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16517_MI;
 bool m16517 (t3025 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16518 (t3025 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22674(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22674_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3025_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3025_TI, offsetof(t3025, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3025_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3025_TI, offsetof(t3025, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3025_FIs[] =
{
	&t3025_f0_FieldInfo,
	&t3025_f1_FieldInfo,
	NULL
};
static PropertyInfo t3025____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3025_TI, "System.Collections.IEnumerator.Current", &m16515_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3025____Current_PropertyInfo = 
{
	&t3025_TI, "Current", &m16518_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3025_PIs[] =
{
	&t3025____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3025____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3025_m16514_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16514_GM;
MethodInfo m16514_MI = 
{
	".ctor", (methodPointerType)&m16514, &t3025_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3025_m16514_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16514_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16515_GM;
MethodInfo m16515_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16515, &t3025_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16515_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16516_GM;
MethodInfo m16516_MI = 
{
	"Dispose", (methodPointerType)&m16516, &t3025_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16516_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16517_GM;
MethodInfo m16517_MI = 
{
	"MoveNext", (methodPointerType)&m16517, &t3025_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16517_GM};
extern Il2CppType t152_0_0_0;
extern void* RuntimeInvoker_t152 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16518_GM;
MethodInfo m16518_MI = 
{
	"get_Current", (methodPointerType)&m16518, &t3025_TI, &t152_0_0_0, RuntimeInvoker_t152, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16518_GM};
static MethodInfo* t3025_MIs[] =
{
	&m16514_MI,
	&m16515_MI,
	&m16516_MI,
	&m16517_MI,
	&m16518_MI,
	NULL
};
static MethodInfo* t3025_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16515_MI,
	&m16517_MI,
	&m16516_MI,
	&m16518_MI,
};
static TypeInfo* t3025_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4440_TI,
};
static Il2CppInterfaceOffsetPair t3025_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4440_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3025_0_0_0;
extern Il2CppType t3025_1_0_0;
extern Il2CppGenericClass t3025_GC;
TypeInfo t3025_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3025_MIs, t3025_PIs, t3025_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3025_TI, t3025_ITIs, t3025_VT, &EmptyCustomAttributesCache, &t3025_TI, &t3025_0_0_0, &t3025_1_0_0, t3025_IOs, &t3025_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3025)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5703_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>
extern MethodInfo m29819_MI;
static PropertyInfo t5703____Count_PropertyInfo = 
{
	&t5703_TI, "Count", &m29819_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29820_MI;
static PropertyInfo t5703____IsReadOnly_PropertyInfo = 
{
	&t5703_TI, "IsReadOnly", &m29820_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5703_PIs[] =
{
	&t5703____Count_PropertyInfo,
	&t5703____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29819_GM;
MethodInfo m29819_MI = 
{
	"get_Count", NULL, &t5703_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29819_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29820_GM;
MethodInfo m29820_MI = 
{
	"get_IsReadOnly", NULL, &t5703_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29820_GM};
extern Il2CppType t152_0_0_0;
extern Il2CppType t152_0_0_0;
static ParameterInfo t5703_m29821_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29821_GM;
MethodInfo m29821_MI = 
{
	"Add", NULL, &t5703_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5703_m29821_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29821_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29822_GM;
MethodInfo m29822_MI = 
{
	"Clear", NULL, &t5703_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29822_GM};
extern Il2CppType t152_0_0_0;
static ParameterInfo t5703_m29823_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29823_GM;
MethodInfo m29823_MI = 
{
	"Contains", NULL, &t5703_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5703_m29823_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29823_GM};
extern Il2CppType t3773_0_0_0;
extern Il2CppType t3773_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5703_m29824_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3773_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29824_GM;
MethodInfo m29824_MI = 
{
	"CopyTo", NULL, &t5703_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5703_m29824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29824_GM};
extern Il2CppType t152_0_0_0;
static ParameterInfo t5703_m29825_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29825_GM;
MethodInfo m29825_MI = 
{
	"Remove", NULL, &t5703_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5703_m29825_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29825_GM};
static MethodInfo* t5703_MIs[] =
{
	&m29819_MI,
	&m29820_MI,
	&m29821_MI,
	&m29822_MI,
	&m29823_MI,
	&m29824_MI,
	&m29825_MI,
	NULL
};
extern TypeInfo t5705_TI;
static TypeInfo* t5703_ITIs[] = 
{
	&t603_TI,
	&t5705_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5703_0_0_0;
extern Il2CppType t5703_1_0_0;
struct t5703;
extern Il2CppGenericClass t5703_GC;
TypeInfo t5703_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5703_MIs, t5703_PIs, NULL, NULL, NULL, NULL, NULL, &t5703_TI, t5703_ITIs, NULL, &EmptyCustomAttributesCache, &t5703_TI, &t5703_0_0_0, &t5703_1_0_0, NULL, &t5703_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.VerticalWrapMode>
extern Il2CppType t4440_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29826_GM;
MethodInfo m29826_MI = 
{
	"GetEnumerator", NULL, &t5705_TI, &t4440_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29826_GM};
static MethodInfo* t5705_MIs[] =
{
	&m29826_MI,
	NULL
};
static TypeInfo* t5705_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5705_0_0_0;
extern Il2CppType t5705_1_0_0;
struct t5705;
extern Il2CppGenericClass t5705_GC;
TypeInfo t5705_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5705_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5705_TI, t5705_ITIs, NULL, &EmptyCustomAttributesCache, &t5705_TI, &t5705_0_0_0, &t5705_1_0_0, NULL, &t5705_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5704_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>
extern MethodInfo m29827_MI;
extern MethodInfo m29828_MI;
static PropertyInfo t5704____Item_PropertyInfo = 
{
	&t5704_TI, "Item", &m29827_MI, &m29828_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5704_PIs[] =
{
	&t5704____Item_PropertyInfo,
	NULL
};
extern Il2CppType t152_0_0_0;
static ParameterInfo t5704_m29829_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29829_GM;
MethodInfo m29829_MI = 
{
	"IndexOf", NULL, &t5704_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5704_m29829_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29829_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t152_0_0_0;
static ParameterInfo t5704_m29830_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29830_GM;
MethodInfo m29830_MI = 
{
	"Insert", NULL, &t5704_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5704_m29830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29830_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5704_m29831_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29831_GM;
MethodInfo m29831_MI = 
{
	"RemoveAt", NULL, &t5704_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5704_m29831_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29831_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5704_m29827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t152_0_0_0;
extern void* RuntimeInvoker_t152_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29827_GM;
MethodInfo m29827_MI = 
{
	"get_Item", NULL, &t5704_TI, &t152_0_0_0, RuntimeInvoker_t152_t44, t5704_m29827_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29827_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t152_0_0_0;
static ParameterInfo t5704_m29828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29828_GM;
MethodInfo m29828_MI = 
{
	"set_Item", NULL, &t5704_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5704_m29828_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29828_GM};
static MethodInfo* t5704_MIs[] =
{
	&m29829_MI,
	&m29830_MI,
	&m29831_MI,
	&m29827_MI,
	&m29828_MI,
	NULL
};
static TypeInfo* t5704_ITIs[] = 
{
	&t603_TI,
	&t5703_TI,
	&t5705_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5704_0_0_0;
extern Il2CppType t5704_1_0_0;
struct t5704;
extern Il2CppGenericClass t5704_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5704_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5704_MIs, t5704_PIs, NULL, NULL, NULL, NULL, NULL, &t5704_TI, t5704_ITIs, NULL, &t1908__CustomAttributeCache, &t5704_TI, &t5704_0_0_0, &t5704_1_0_0, NULL, &t5704_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3026.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3026_TI;
#include "t3026MD.h"

#include "t148.h"
#include "t3027.h"
extern TypeInfo t148_TI;
extern TypeInfo t3027_TI;
#include "t3027MD.h"
extern MethodInfo m16521_MI;
extern MethodInfo m16523_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>
extern Il2CppType t316_0_0_33;
FieldInfo t3026_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3026_TI, offsetof(t3026, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3026_FIs[] =
{
	&t3026_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t3026_m16519_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16519_GM;
MethodInfo m16519_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3026_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3026_m16519_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16519_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3026_m16520_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16520_GM;
MethodInfo m16520_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3026_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3026_m16520_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16520_GM};
static MethodInfo* t3026_MIs[] =
{
	&m16519_MI,
	&m16520_MI,
	NULL
};
extern MethodInfo m16520_MI;
extern MethodInfo m16524_MI;
static MethodInfo* t3026_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16520_MI,
	&m16524_MI,
};
extern Il2CppType t3028_0_0_0;
extern TypeInfo t3028_TI;
extern MethodInfo m22684_MI;
extern TypeInfo t148_TI;
extern MethodInfo m16526_MI;
extern TypeInfo t148_TI;
static Il2CppRGCTXData t3026_RGCTXData[8] = 
{
	&t3028_0_0_0/* Type Usage */,
	&t3028_TI/* Class Usage */,
	&m22684_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m16526_MI/* Method Usage */,
	&m16521_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m16523_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3026_0_0_0;
extern Il2CppType t3026_1_0_0;
struct t3026;
extern Il2CppGenericClass t3026_GC;
TypeInfo t3026_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3026_MIs, NULL, t3026_FIs, NULL, &t3027_TI, NULL, NULL, &t3026_TI, NULL, t3026_VT, &EmptyCustomAttributesCache, &t3026_TI, &t3026_0_0_0, &t3026_1_0_0, NULL, &t3026_GC, NULL, NULL, NULL, t3026_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3026), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3028.h"
extern TypeInfo t3028_TI;
#include "t3028MD.h"
struct t556;
#define m22684(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Font>
extern Il2CppType t3028_0_0_1;
FieldInfo t3027_f0_FieldInfo = 
{
	"Delegate", &t3028_0_0_1, &t3027_TI, offsetof(t3027, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3027_FIs[] =
{
	&t3027_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3027_m16521_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16521_GM;
MethodInfo m16521_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3027_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3027_m16521_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16521_GM};
extern Il2CppType t3028_0_0_0;
static ParameterInfo t3027_m16522_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3028_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16522_GM;
MethodInfo m16522_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3027_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3027_m16522_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16522_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3027_m16523_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16523_GM;
MethodInfo m16523_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3027_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3027_m16523_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16523_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3027_m16524_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16524_GM;
MethodInfo m16524_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3027_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3027_m16524_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16524_GM};
static MethodInfo* t3027_MIs[] =
{
	&m16521_MI,
	&m16522_MI,
	&m16523_MI,
	&m16524_MI,
	NULL
};
static MethodInfo* t3027_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16523_MI,
	&m16524_MI,
};
extern TypeInfo t3028_TI;
extern TypeInfo t148_TI;
static Il2CppRGCTXData t3027_RGCTXData[5] = 
{
	&t3028_0_0_0/* Type Usage */,
	&t3028_TI/* Class Usage */,
	&m22684_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m16526_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3027_0_0_0;
extern Il2CppType t3027_1_0_0;
struct t3027;
extern Il2CppGenericClass t3027_GC;
TypeInfo t3027_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3027_MIs, NULL, t3027_FIs, NULL, &t556_TI, NULL, NULL, &t3027_TI, NULL, t3027_VT, &EmptyCustomAttributesCache, &t3027_TI, &t3027_0_0_0, &t3027_1_0_0, NULL, &t3027_GC, NULL, NULL, NULL, t3027_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3027), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Font>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3028_m16525_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16525_GM;
MethodInfo m16525_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3028_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3028_m16525_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16525_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t3028_m16526_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16526_GM;
MethodInfo m16526_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3028_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3028_m16526_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16526_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3028_m16527_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16527_GM;
MethodInfo m16527_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3028_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3028_m16527_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16527_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3028_m16528_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16528_GM;
MethodInfo m16528_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3028_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3028_m16528_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16528_GM};
static MethodInfo* t3028_MIs[] =
{
	&m16525_MI,
	&m16526_MI,
	&m16527_MI,
	&m16528_MI,
	NULL
};
extern MethodInfo m16527_MI;
extern MethodInfo m16528_MI;
static MethodInfo* t3028_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16526_MI,
	&m16527_MI,
	&m16528_MI,
};
static Il2CppInterfaceOffsetPair t3028_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3028_1_0_0;
struct t3028;
extern Il2CppGenericClass t3028_GC;
TypeInfo t3028_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3028_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3028_TI, NULL, t3028_VT, &EmptyCustomAttributesCache, &t3028_TI, &t3028_0_0_0, &t3028_1_0_0, t3028_IOs, &t3028_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3028), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t528.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t528_TI;
#include "t528MD.h"

#include "t381.h"
#include "t3035.h"
#include "t3032.h"
#include "t3033.h"
#include "t338.h"
#include "t3039.h"
#include "t3034.h"
extern TypeInfo t381_TI;
extern TypeInfo t44_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t530_TI;
extern TypeInfo t3035_TI;
extern TypeInfo t40_TI;
extern TypeInfo t3030_TI;
extern TypeInfo t3031_TI;
extern TypeInfo t3029_TI;
extern TypeInfo t3032_TI;
extern TypeInfo t338_TI;
extern TypeInfo t3033_TI;
extern TypeInfo t3039_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t3032MD.h"
#include "t338MD.h"
#include "t3033MD.h"
#include "t3035MD.h"
#include "t3039MD.h"
extern MethodInfo m16575_MI;
extern MethodInfo m16576_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m22686_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m16561_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m16558_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m16546_MI;
extern MethodInfo m16553_MI;
extern MethodInfo m16559_MI;
extern MethodInfo m16562_MI;
extern MethodInfo m16564_MI;
extern MethodInfo m16547_MI;
extern MethodInfo m16572_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m16573_MI;
extern MethodInfo m28147_MI;
extern MethodInfo m28152_MI;
extern MethodInfo m28154_MI;
extern MethodInfo m28155_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m16563_MI;
extern MethodInfo m16548_MI;
extern MethodInfo m16549_MI;
extern MethodInfo m16583_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m22688_MI;
extern MethodInfo m16556_MI;
extern MethodInfo m16557_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m16658_MI;
extern MethodInfo m16577_MI;
extern MethodInfo m16560_MI;
extern MethodInfo m16566_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m16664_MI;
extern MethodInfo m22690_MI;
extern MethodInfo m22698_MI;
extern MethodInfo m5951_MI;
struct t20;
 void m22686 (t29 * __this, t530** p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
#include "t3037.h"
 int32_t m22688 (t29 * __this, t530* p0, t381  p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m22690 (t29 * __this, t530* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
#include "t295.h"
 void m22698 (t29 * __this, t530* p0, int32_t p1, t3034 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16529_MI;
 void m16529 (t528 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t528_TI));
		__this->f1 = (((t528_SFs*)InitializedTypeInfo(&t528_TI)->static_fields)->f4);
		return;
	}
}
extern MethodInfo m2904_MI;
 void m2904 (t528 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		__this->f1 = ((t530*)SZArrayNew(InitializedTypeInfo(&t530_TI), p0));
		return;
	}
}
extern MethodInfo m16530_MI;
 void m16530 (t29 * __this, MethodInfo* method){
	{
		((t528_SFs*)InitializedTypeInfo(&t528_TI)->static_fields)->f4 = ((t530*)SZArrayNew(InitializedTypeInfo(&t530_TI), 0));
		return;
	}
}
extern MethodInfo m16531_MI;
 t29* m16531 (t528 * __this, MethodInfo* method){
	{
		t3035  L_0 = m16558(__this, &m16558_MI);
		t3035  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3035_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m16532_MI;
 void m16532 (t528 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m16533_MI;
 t29 * m16533 (t528 * __this, MethodInfo* method){
	{
		t3035  L_0 = m16558(__this, &m16558_MI);
		t3035  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3035_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m16534_MI;
 int32_t m16534 (t528 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker1< t381  >::Invoke(&m16546_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
			int32_t L_0 = (__this->f2);
			V_0 = ((int32_t)(L_0-1));
			// IL_0015: leave.s IL_002a
			goto IL_002a;
		}

IL_0017:
		{
			// IL_0017: leave.s IL_001f
			goto IL_001f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0019;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001c;
		throw e;
	}

IL_0019:
	{ // begin catch(System.NullReferenceException)
		// IL_001a: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001c:
	{ // begin catch(System.InvalidCastException)
		// IL_001d: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		return V_0;
	}
}
extern MethodInfo m16535_MI;
 bool m16535 (t528 * __this, t29 * p0, MethodInfo* method){
	bool V_0 = false;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (bool)VirtFuncInvoker1< bool, t381  >::Invoke(&m16553_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m16536_MI;
 int32_t m16536 (t528 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t381  >::Invoke(&m16559_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return (-1);
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m16537_MI;
 void m16537 (t528 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		m16561(__this, p0, &m16561_MI);
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t381  >::Invoke(&m16562_MI, __this, p0, ((*(t381 *)((t381 *)UnBox (p1, InitializedTypeInfo(&t381_TI))))));
			// IL_0014: leave.s IL_0029
			goto IL_0029;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_001e
			goto IL_001e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0018;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001b;
		throw e;
	}

IL_0018:
	{ // begin catch(System.NullReferenceException)
		// IL_0019: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001b:
	{ // begin catch(System.InvalidCastException)
		// IL_001c: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001e:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0029:
	{
		return;
	}
}
extern MethodInfo m16538_MI;
 void m16538 (t528 * __this, t29 * p0, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtFuncInvoker1< bool, t381  >::Invoke(&m16564_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
			// IL_000d: leave.s IL_0017
			goto IL_0017;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return;
	}
}
extern MethodInfo m16539_MI;
 bool m16539 (t528 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16540_MI;
 bool m16540 (t528 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16541_MI;
 t29 * m16541 (t528 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m16542_MI;
 bool m16542 (t528 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16543_MI;
 bool m16543 (t528 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16544_MI;
 t29 * m16544 (t528 * __this, int32_t p0, MethodInfo* method){
	{
		t381  L_0 = (t381 )VirtFuncInvoker1< t381 , int32_t >::Invoke(&m16575_MI, __this, p0);
		t381  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t381_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16545_MI;
 void m16545 (t528 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t381  >::Invoke(&m16576_MI, __this, p0, ((*(t381 *)((t381 *)UnBox (p1, InitializedTypeInfo(&t381_TI))))));
			// IL_000d: leave.s IL_0022
			goto IL_0022;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral401, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0022:
	{
		return;
	}
}
 void m16546 (t528 * __this, t381  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		t530* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		m16547(__this, 1, &m16547_MI);
	}

IL_0017:
	{
		t530* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->f2 = ((int32_t)(L_4+1));
		*((t381 *)(t381 *)SZArrayLdElema(L_2, V_0)) = (t381 )p0;
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		return;
	}
}
 void m16547 (t528 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((int32_t)(L_0+p0));
		t530* L_1 = (__this->f1);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = m16572(__this, &m16572_MI);
		int32_t L_3 = m5139(NULL, ((int32_t)((int32_t)L_2*(int32_t)2)), 4, &m5139_MI);
		int32_t L_4 = m5139(NULL, L_3, V_0, &m5139_MI);
		m16573(__this, L_4, &m16573_MI);
	}

IL_002e:
	{
		return;
	}
}
 void m16548 (t528 * __this, t29* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m28147_MI, p0);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		m16547(__this, V_0, &m16547_MI);
		t530* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		InterfaceActionInvoker2< t530*, int32_t >::Invoke(&m28152_MI, p0, L_1, L_2);
		int32_t L_3 = (__this->f2);
		__this->f2 = ((int32_t)(L_3+V_0));
		return;
	}
}
 void m16549 (t528 * __this, t29* p0, MethodInfo* method){
	t381  V_0 = {0};
	t29* V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t29* L_0 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28154_MI, p0);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0017;
		}

IL_0009:
		{
			t381  L_1 = (t381 )InterfaceFuncInvoker0< t381  >::Invoke(&m28155_MI, V_1);
			V_0 = L_1;
			VirtActionInvoker1< t381  >::Invoke(&m16546_MI, __this, V_0);
		}

IL_0017:
		{
			bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_2)
			{
				goto IL_0009;
			}
		}

IL_001f:
		{
			// IL_001f: leave.s IL_002c
			leaveInstructions[0] = 0x2C; // 1
			THROW_SENTINEL(IL_002c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0021;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0021;
	}

IL_0021:
	{ // begin finally (depth: 1)
		{
			if (V_1)
			{
				goto IL_0025;
			}
		}

IL_0024:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0025:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_1);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_002c:
	{
		return;
	}
}
extern MethodInfo m16550_MI;
 void m16550 (t528 * __this, t29* p0, MethodInfo* method){
	t29* V_0 = {0};
	{
		m16563(__this, p0, &m16563_MI);
		V_0 = ((t29*)IsInst(p0, InitializedTypeInfo(&t3030_TI)));
		if (!V_0)
		{
			goto IL_001a;
		}
	}
	{
		m16548(__this, V_0, &m16548_MI);
		goto IL_0021;
	}

IL_001a:
	{
		m16549(__this, p0, &m16549_MI);
	}

IL_0021:
	{
		int32_t L_0 = (__this->f3);
		__this->f3 = ((int32_t)(L_0+1));
		return;
	}
}
extern MethodInfo m16551_MI;
 t3032 * m16551 (t528 * __this, MethodInfo* method){
	{
		t3032 * L_0 = (t3032 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3032_TI));
		m16583(L_0, __this, &m16583_MI);
		return L_0;
	}
}
extern MethodInfo m16552_MI;
 void m16552 (t528 * __this, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		t530* L_1 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		__this->f2 = 0;
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
 bool m16553 (t528 * __this, t381  p0, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m22688(NULL, L_0, p0, 0, L_1, &m22688_MI);
		return ((((int32_t)((((int32_t)L_2) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m16554_MI;
 void m16554 (t528 * __this, t530* p0, int32_t p1, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, (t20 *)(t20 *)p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m16555_MI;
 t381  m16555 (t528 * __this, t3033 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t381  V_1 = {0};
	t381  G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t528_TI));
		m16556(NULL, p0, &m16556_MI);
		int32_t L_0 = (__this->f2);
		int32_t L_1 = m16557(__this, 0, L_0, p0, &m16557_MI);
		V_0 = L_1;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		t530* L_2 = (__this->f1);
		int32_t L_3 = V_0;
		G_B3_0 = (*(t381 *)(t381 *)SZArrayLdElema(L_2, L_3));
		goto IL_0030;
	}

IL_0027:
	{
		Initobj (&t381_TI, (&V_1));
		G_B3_0 = V_1;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
 void m16556 (t29 * __this, t3033 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1033, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 int32_t m16557 (t528 * __this, int32_t p0, int32_t p1, t3033 * p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)(p0+p1));
		V_1 = p0;
		goto IL_0022;
	}

IL_0008:
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = V_1;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t381  >::Invoke(&m16658_MI, p2, (*(t381 *)(t381 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return V_1;
	}

IL_001e:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0022:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0008;
		}
	}
	{
		return (-1);
	}
}
 t3035  m16558 (t528 * __this, MethodInfo* method){
	{
		t3035  L_0 = {0};
		m16577(&L_0, __this, &m16577_MI);
		return L_0;
	}
}
 int32_t m16559 (t528 * __this, t381  p0, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m22688(NULL, L_0, p0, 0, L_1, &m22688_MI);
		return L_2;
	}
}
 void m16560 (t528 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		p0 = ((int32_t)(p0-p1));
	}

IL_000b:
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)p0) >= ((int32_t)L_0)))
		{
			goto IL_0031;
		}
	}
	{
		t530* L_1 = (__this->f1);
		t530* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_1, p0, (t20 *)(t20 *)L_2, ((int32_t)(p0+p1)), ((int32_t)(L_3-p0)), &m5952_MI);
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		__this->f2 = ((int32_t)(L_4+p1));
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		t530* L_5 = (__this->f1);
		int32_t L_6 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_5, L_6, ((-p1)), &m5112_MI);
	}

IL_0056:
	{
		return;
	}
}
 void m16561 (t528 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) <= ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		return;
	}
}
 void m16562 (t528 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		m16561(__this, p0, &m16561_MI);
		int32_t L_0 = (__this->f2);
		t530* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		m16547(__this, 1, &m16547_MI);
	}

IL_001e:
	{
		m16560(__this, p0, 1, &m16560_MI);
		t530* L_2 = (__this->f1);
		*((t381 *)(t381 *)SZArrayLdElema(L_2, p0)) = (t381 )p1;
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
 void m16563 (t528 * __this, t29* p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1177, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 bool m16564 (t528 * __this, t381  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t381  >::Invoke(&m16559_MI, __this, p0);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker1< int32_t >::Invoke(&m16566_MI, __this, V_0);
	}

IL_0013:
	{
		return ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m16565_MI;
 int32_t m16565 (t528 * __this, t3033 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t528_TI));
		m16556(NULL, p0, &m16556_MI);
		V_0 = 0;
		V_1 = 0;
		V_0 = 0;
		goto IL_0028;
	}

IL_000e:
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = V_0;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t381  >::Invoke(&m16658_MI, p0, (*(t381 *)(t381 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0031;
	}

IL_0024:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0028:
	{
		int32_t L_3 = (__this->f2);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_000e;
		}
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		if ((((uint32_t)V_0) != ((uint32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		return 0;
	}

IL_003c:
	{
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		V_1 = ((int32_t)(V_0+1));
		goto IL_0084;
	}

IL_0050:
	{
		t530* L_6 = (__this->f1);
		int32_t L_7 = V_1;
		bool L_8 = (bool)VirtFuncInvoker1< bool, t381  >::Invoke(&m16658_MI, p0, (*(t381 *)(t381 *)SZArrayLdElema(L_6, L_7)));
		if (L_8)
		{
			goto IL_0080;
		}
	}
	{
		t530* L_9 = (__this->f1);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)(L_10+1));
		t530* L_11 = (__this->f1);
		int32_t L_12 = V_1;
		*((t381 *)(t381 *)SZArrayLdElema(L_9, L_10)) = (t381 )(*(t381 *)(t381 *)SZArrayLdElema(L_11, L_12));
	}

IL_0080:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0084:
	{
		int32_t L_13 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)L_13)))
		{
			goto IL_0050;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_1-V_0))) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		t530* L_14 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_14, V_0, ((int32_t)(V_1-V_0)), &m5112_MI);
	}

IL_00a2:
	{
		__this->f2 = V_0;
		return ((int32_t)(V_1-V_0));
	}
}
 void m16566 (t528 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		m16560(__this, p0, (-1), &m16560_MI);
		t530* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_2, L_3, 1, &m5112_MI);
		int32_t L_4 = (__this->f3);
		__this->f3 = ((int32_t)(L_4+1));
		return;
	}
}
extern MethodInfo m16567_MI;
 void m16567 (t528 * __this, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5171(NULL, (t20 *)(t20 *)L_0, 0, L_1, &m5171_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m16568_MI;
 void m16568 (t528 * __this, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3039_TI));
		t3039 * L_2 = m16664(NULL, &m16664_MI);
		m22690(NULL, L_0, 0, L_1, L_2, &m22690_MI);
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
extern MethodInfo m16569_MI;
 void m16569 (t528 * __this, t3034 * p0, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m22698(NULL, L_0, L_1, p0, &m22698_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m16570_MI;
 t530* m16570 (t528 * __this, MethodInfo* method){
	t530* V_0 = {0};
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((t530*)SZArrayNew(InitializedTypeInfo(&t530_TI), L_0));
		t530* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		m5951(NULL, (t20 *)(t20 *)L_1, (t20 *)(t20 *)V_0, L_2, &m5951_MI);
		return V_0;
	}
}
extern MethodInfo m16571_MI;
 void m16571 (t528 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		m16573(__this, L_0, &m16573_MI);
		return;
	}
}
 int32_t m16572 (t528 * __this, MethodInfo* method){
	{
		t530* L_0 = (__this->f1);
		return (((int32_t)(((t20 *)L_0)->max_length)));
	}
}
 void m16573 (t528 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) >= ((uint32_t)L_0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m4198(L_1, &m4198_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t530** L_2 = &(__this->f1);
		m22686(NULL, L_2, p0, &m22686_MI);
		return;
	}
}
extern MethodInfo m16574_MI;
 int32_t m16574 (t528 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 t381  m16575 (t528 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		t530* L_2 = (__this->f1);
		int32_t L_3 = p0;
		return (*(t381 *)(t381 *)SZArrayLdElema(L_2, L_3));
	}
}
 void m16576 (t528 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		m16561(__this, p0, &m16561_MI);
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) != ((uint32_t)L_0)))
		{
			goto IL_001b;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001b:
	{
		t530* L_2 = (__this->f1);
		*((t381 *)(t381 *)SZArrayLdElema(L_2, p0)) = (t381 )p1;
		return;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UICharInfo>
extern Il2CppType t44_0_0_32849;
FieldInfo t528_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t528_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t530_0_0_1;
FieldInfo t528_f1_FieldInfo = 
{
	"_items", &t530_0_0_1, &t528_TI, offsetof(t528, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t528_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t528_TI, offsetof(t528, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t528_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t528_TI, offsetof(t528, f3), &EmptyCustomAttributesCache};
extern Il2CppType t530_0_0_49;
FieldInfo t528_f4_FieldInfo = 
{
	"EmptyArray", &t530_0_0_49, &t528_TI, offsetof(t528_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t528_FIs[] =
{
	&t528_f0_FieldInfo,
	&t528_f1_FieldInfo,
	&t528_f2_FieldInfo,
	&t528_f3_FieldInfo,
	&t528_f4_FieldInfo,
	NULL
};
static const int32_t t528_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t528_f0_DefaultValue = 
{
	&t528_f0_FieldInfo, { (char*)&t528_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t528_FDVs[] = 
{
	&t528_f0_DefaultValue,
	NULL
};
static PropertyInfo t528____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t528_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16539_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t528_TI, "System.Collections.ICollection.IsSynchronized", &m16540_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t528_TI, "System.Collections.ICollection.SyncRoot", &m16541_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t528_TI, "System.Collections.IList.IsFixedSize", &m16542_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t528_TI, "System.Collections.IList.IsReadOnly", &m16543_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____System_Collections_IList_Item_PropertyInfo = 
{
	&t528_TI, "System.Collections.IList.Item", &m16544_MI, &m16545_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____Capacity_PropertyInfo = 
{
	&t528_TI, "Capacity", &m16572_MI, &m16573_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____Count_PropertyInfo = 
{
	&t528_TI, "Count", &m16574_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t528____Item_PropertyInfo = 
{
	&t528_TI, "Item", &m16575_MI, &m16576_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t528_PIs[] =
{
	&t528____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t528____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t528____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t528____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t528____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t528____System_Collections_IList_Item_PropertyInfo,
	&t528____Capacity_PropertyInfo,
	&t528____Count_PropertyInfo,
	&t528____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16529_GM;
MethodInfo m16529_MI = 
{
	".ctor", (methodPointerType)&m16529, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16529_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m2904_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2904_GM;
MethodInfo m2904_MI = 
{
	".ctor", (methodPointerType)&m2904, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t528_m2904_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2904_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16530_GM;
MethodInfo m16530_MI = 
{
	".cctor", (methodPointerType)&m16530, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16530_GM};
extern Il2CppType t3029_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16531_GM;
MethodInfo m16531_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m16531, &t528_TI, &t3029_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16531_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16532_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16532_GM;
MethodInfo m16532_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m16532, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t528_m16532_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16532_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16533_GM;
MethodInfo m16533_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m16533, &t528_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16533_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t528_m16534_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16534_GM;
MethodInfo m16534_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m16534, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t528_m16534_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16534_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t528_m16535_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16535_GM;
MethodInfo m16535_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m16535, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t528_m16535_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16535_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t528_m16536_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16536_GM;
MethodInfo m16536_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m16536, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t528_m16536_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16536_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t528_m16537_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16537_GM;
MethodInfo m16537_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m16537, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t528_m16537_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16537_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t528_m16538_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16538_GM;
MethodInfo m16538_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m16538, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16538_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16538_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16539_GM;
MethodInfo m16539_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m16539, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16539_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16540_GM;
MethodInfo m16540_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m16540, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16540_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16541_GM;
MethodInfo m16541_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m16541, &t528_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16541_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16542_GM;
MethodInfo m16542_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m16542, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16542_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16543_GM;
MethodInfo m16543_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m16543, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16543_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16544_GM;
MethodInfo m16544_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m16544, &t528_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t528_m16544_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16544_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t528_m16545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16545_GM;
MethodInfo m16545_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m16545, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t528_m16545_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16545_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t528_m16546_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16546_GM;
MethodInfo m16546_MI = 
{
	"Add", (methodPointerType)&m16546, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t381, t528_m16546_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16546_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16547_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16547_GM;
MethodInfo m16547_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m16547, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t528_m16547_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16547_GM};
extern Il2CppType t3030_0_0_0;
extern Il2CppType t3030_0_0_0;
static ParameterInfo t528_m16548_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3030_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16548_GM;
MethodInfo m16548_MI = 
{
	"AddCollection", (methodPointerType)&m16548, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16548_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16548_GM};
extern Il2CppType t3031_0_0_0;
extern Il2CppType t3031_0_0_0;
static ParameterInfo t528_m16549_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3031_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16549_GM;
MethodInfo m16549_MI = 
{
	"AddEnumerable", (methodPointerType)&m16549, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16549_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16549_GM};
extern Il2CppType t3031_0_0_0;
static ParameterInfo t528_m16550_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3031_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16550_GM;
MethodInfo m16550_MI = 
{
	"AddRange", (methodPointerType)&m16550, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16550_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16550_GM};
extern Il2CppType t3032_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16551_GM;
MethodInfo m16551_MI = 
{
	"AsReadOnly", (methodPointerType)&m16551, &t528_TI, &t3032_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16551_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16552_GM;
MethodInfo m16552_MI = 
{
	"Clear", (methodPointerType)&m16552, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16552_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t528_m16553_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16553_GM;
MethodInfo m16553_MI = 
{
	"Contains", (methodPointerType)&m16553, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t528_m16553_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16553_GM};
extern Il2CppType t530_0_0_0;
extern Il2CppType t530_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16554_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t530_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16554_GM;
MethodInfo m16554_MI = 
{
	"CopyTo", (methodPointerType)&m16554, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t528_m16554_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16554_GM};
extern Il2CppType t3033_0_0_0;
extern Il2CppType t3033_0_0_0;
static ParameterInfo t528_m16555_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3033_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16555_GM;
MethodInfo m16555_MI = 
{
	"Find", (methodPointerType)&m16555, &t528_TI, &t381_0_0_0, RuntimeInvoker_t381_t29, t528_m16555_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16555_GM};
extern Il2CppType t3033_0_0_0;
static ParameterInfo t528_m16556_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3033_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16556_GM;
MethodInfo m16556_MI = 
{
	"CheckMatch", (methodPointerType)&m16556, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16556_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3033_0_0_0;
static ParameterInfo t528_m16557_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t3033_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16557_GM;
MethodInfo m16557_MI = 
{
	"GetIndex", (methodPointerType)&m16557, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t528_m16557_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16557_GM};
extern Il2CppType t3035_0_0_0;
extern void* RuntimeInvoker_t3035 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16558_GM;
MethodInfo m16558_MI = 
{
	"GetEnumerator", (methodPointerType)&m16558, &t528_TI, &t3035_0_0_0, RuntimeInvoker_t3035, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16558_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t528_m16559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16559_GM;
MethodInfo m16559_MI = 
{
	"IndexOf", (methodPointerType)&m16559, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t528_m16559_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16559_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16560_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16560_GM;
MethodInfo m16560_MI = 
{
	"Shift", (methodPointerType)&m16560, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t528_m16560_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16560_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16561_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16561_GM;
MethodInfo m16561_MI = 
{
	"CheckIndex", (methodPointerType)&m16561, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t528_m16561_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16561_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t528_m16562_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16562_GM;
MethodInfo m16562_MI = 
{
	"Insert", (methodPointerType)&m16562, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t528_m16562_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16562_GM};
extern Il2CppType t3031_0_0_0;
static ParameterInfo t528_m16563_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3031_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16563_GM;
MethodInfo m16563_MI = 
{
	"CheckCollection", (methodPointerType)&m16563, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16563_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16563_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t528_m16564_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16564_GM;
MethodInfo m16564_MI = 
{
	"Remove", (methodPointerType)&m16564, &t528_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t528_m16564_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16564_GM};
extern Il2CppType t3033_0_0_0;
static ParameterInfo t528_m16565_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3033_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16565_GM;
MethodInfo m16565_MI = 
{
	"RemoveAll", (methodPointerType)&m16565, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t528_m16565_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16565_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16566_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16566_GM;
MethodInfo m16566_MI = 
{
	"RemoveAt", (methodPointerType)&m16566, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t528_m16566_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16566_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16567_GM;
MethodInfo m16567_MI = 
{
	"Reverse", (methodPointerType)&m16567, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16567_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16568_GM;
MethodInfo m16568_MI = 
{
	"Sort", (methodPointerType)&m16568, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16568_GM};
extern Il2CppType t3034_0_0_0;
extern Il2CppType t3034_0_0_0;
static ParameterInfo t528_m16569_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3034_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16569_GM;
MethodInfo m16569_MI = 
{
	"Sort", (methodPointerType)&m16569, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t528_m16569_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16569_GM};
extern Il2CppType t530_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16570_GM;
MethodInfo m16570_MI = 
{
	"ToArray", (methodPointerType)&m16570, &t528_TI, &t530_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16570_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16571_GM;
MethodInfo m16571_MI = 
{
	"TrimExcess", (methodPointerType)&m16571, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16571_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16572_GM;
MethodInfo m16572_MI = 
{
	"get_Capacity", (methodPointerType)&m16572, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16572_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16573_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16573_GM;
MethodInfo m16573_MI = 
{
	"set_Capacity", (methodPointerType)&m16573, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t528_m16573_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16573_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16574_GM;
MethodInfo m16574_MI = 
{
	"get_Count", (methodPointerType)&m16574, &t528_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16574_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t528_m16575_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16575_GM;
MethodInfo m16575_MI = 
{
	"get_Item", (methodPointerType)&m16575, &t528_TI, &t381_0_0_0, RuntimeInvoker_t381_t44, t528_m16575_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16575_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t528_m16576_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16576_GM;
MethodInfo m16576_MI = 
{
	"set_Item", (methodPointerType)&m16576, &t528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t528_m16576_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16576_GM};
static MethodInfo* t528_MIs[] =
{
	&m16529_MI,
	&m2904_MI,
	&m16530_MI,
	&m16531_MI,
	&m16532_MI,
	&m16533_MI,
	&m16534_MI,
	&m16535_MI,
	&m16536_MI,
	&m16537_MI,
	&m16538_MI,
	&m16539_MI,
	&m16540_MI,
	&m16541_MI,
	&m16542_MI,
	&m16543_MI,
	&m16544_MI,
	&m16545_MI,
	&m16546_MI,
	&m16547_MI,
	&m16548_MI,
	&m16549_MI,
	&m16550_MI,
	&m16551_MI,
	&m16552_MI,
	&m16553_MI,
	&m16554_MI,
	&m16555_MI,
	&m16556_MI,
	&m16557_MI,
	&m16558_MI,
	&m16559_MI,
	&m16560_MI,
	&m16561_MI,
	&m16562_MI,
	&m16563_MI,
	&m16564_MI,
	&m16565_MI,
	&m16566_MI,
	&m16567_MI,
	&m16568_MI,
	&m16569_MI,
	&m16570_MI,
	&m16571_MI,
	&m16572_MI,
	&m16573_MI,
	&m16574_MI,
	&m16575_MI,
	&m16576_MI,
	NULL
};
static MethodInfo* t528_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16533_MI,
	&m16574_MI,
	&m16540_MI,
	&m16541_MI,
	&m16532_MI,
	&m16542_MI,
	&m16543_MI,
	&m16544_MI,
	&m16545_MI,
	&m16534_MI,
	&m16552_MI,
	&m16535_MI,
	&m16536_MI,
	&m16537_MI,
	&m16538_MI,
	&m16566_MI,
	&m16574_MI,
	&m16539_MI,
	&m16546_MI,
	&m16552_MI,
	&m16553_MI,
	&m16554_MI,
	&m16564_MI,
	&m16531_MI,
	&m16559_MI,
	&m16562_MI,
	&m16566_MI,
	&m16575_MI,
	&m16576_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t388_TI;
static TypeInfo* t528_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3030_TI,
	&t3031_TI,
	&t388_TI,
};
static Il2CppInterfaceOffsetPair t528_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3030_TI, 20},
	{ &t3031_TI, 27},
	{ &t388_TI, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t528_0_0_0;
extern Il2CppType t528_1_0_0;
struct t528;
extern Il2CppGenericClass t528_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t528_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t528_MIs, t528_PIs, t528_FIs, NULL, &t29_TI, NULL, NULL, &t528_TI, t528_ITIs, t528_VT, &t1261__CustomAttributeCache, &t528_TI, &t528_0_0_0, &t528_1_0_0, t528_IOs, &t528_GC, NULL, t528_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t528), 0, -1, sizeof(t528_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m16580_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


 void m16577 (t3035 * __this, t528 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f3);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m16578_MI;
 t29 * m16578 (t3035 * __this, MethodInfo* method){
	{
		m16580(__this, &m16580_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		t381  L_2 = (__this->f3);
		t381  L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t381_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m16579_MI;
 void m16579 (t3035 * __this, MethodInfo* method){
	{
		__this->f0 = (t528 *)NULL;
		return;
	}
}
 void m16580 (t3035 * __this, MethodInfo* method){
	{
		t528 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		t3035  L_1 = (*(t3035 *)__this);
		t29 * L_2 = Box(InitializedTypeInfo(&t3035_TI), &L_1);
		t42 * L_3 = m1430(L_2, &m1430_MI);
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_3);
		t1101 * L_5 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_5, L_4, &m5150_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0023:
	{
		int32_t L_6 = (__this->f2);
		t528 * L_7 = (__this->f0);
		int32_t L_8 = (L_7->f3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0041;
		}
	}
	{
		t914 * L_9 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_9, (t7*) &_stringLiteral1178, &m3964_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0041:
	{
		return;
	}
}
extern MethodInfo m16581_MI;
 bool m16581 (t3035 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m16580(__this, &m16580_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_1 = (__this->f1);
		t528 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_004d;
		}
	}
	{
		t528 * L_4 = (__this->f0);
		t530* L_5 = (L_4->f1);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		int32_t L_8 = V_0;
		__this->f3 = (*(t381 *)(t381 *)SZArrayLdElema(L_5, L_8));
		return 1;
	}

IL_004d:
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m16582_MI;
 t381  m16582 (t3035 * __this, MethodInfo* method){
	{
		t381  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
extern Il2CppType t528_0_0_1;
FieldInfo t3035_f0_FieldInfo = 
{
	"l", &t528_0_0_1, &t3035_TI, offsetof(t3035, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3035_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3035_TI, offsetof(t3035, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3035_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t3035_TI, offsetof(t3035, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t381_0_0_1;
FieldInfo t3035_f3_FieldInfo = 
{
	"current", &t381_0_0_1, &t3035_TI, offsetof(t3035, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3035_FIs[] =
{
	&t3035_f0_FieldInfo,
	&t3035_f1_FieldInfo,
	&t3035_f2_FieldInfo,
	&t3035_f3_FieldInfo,
	NULL
};
static PropertyInfo t3035____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3035_TI, "System.Collections.IEnumerator.Current", &m16578_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3035____Current_PropertyInfo = 
{
	&t3035_TI, "Current", &m16582_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3035_PIs[] =
{
	&t3035____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3035____Current_PropertyInfo,
	NULL
};
extern Il2CppType t528_0_0_0;
static ParameterInfo t3035_m16577_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t528_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16577_GM;
MethodInfo m16577_MI = 
{
	".ctor", (methodPointerType)&m16577, &t3035_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3035_m16577_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16577_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16578_GM;
MethodInfo m16578_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16578, &t3035_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16578_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16579_GM;
MethodInfo m16579_MI = 
{
	"Dispose", (methodPointerType)&m16579, &t3035_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16579_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16580_GM;
MethodInfo m16580_MI = 
{
	"VerifyState", (methodPointerType)&m16580, &t3035_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16580_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16581_GM;
MethodInfo m16581_MI = 
{
	"MoveNext", (methodPointerType)&m16581, &t3035_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16581_GM};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16582_GM;
MethodInfo m16582_MI = 
{
	"get_Current", (methodPointerType)&m16582, &t3035_TI, &t381_0_0_0, RuntimeInvoker_t381, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16582_GM};
static MethodInfo* t3035_MIs[] =
{
	&m16577_MI,
	&m16578_MI,
	&m16579_MI,
	&m16580_MI,
	&m16581_MI,
	&m16582_MI,
	NULL
};
static MethodInfo* t3035_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16578_MI,
	&m16581_MI,
	&m16579_MI,
	&m16582_MI,
};
static TypeInfo* t3035_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3029_TI,
};
static Il2CppInterfaceOffsetPair t3035_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3029_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3035_0_0_0;
extern Il2CppType t3035_1_0_0;
extern Il2CppGenericClass t3035_GC;
extern TypeInfo t1261_TI;
TypeInfo t3035_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3035_MIs, t3035_PIs, t3035_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t3035_TI, t3035_ITIs, t3035_VT, &EmptyCustomAttributesCache, &t3035_TI, &t3035_0_0_0, &t3035_1_0_0, t3035_IOs, &t3035_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3035)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t3036MD.h"
extern MethodInfo m16612_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m1753_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m16644_MI;
extern MethodInfo m28151_MI;
extern MethodInfo m28144_MI;


 void m16583 (t3032 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1179, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m16584_MI;
 void m16584 (t3032 * __this, t381  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16585_MI;
 void m16585 (t3032 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16586_MI;
 void m16586 (t3032 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16587_MI;
 bool m16587 (t3032 * __this, t381  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16588_MI;
 void m16588 (t3032 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16589_MI;
 t381  m16589 (t3032 * __this, int32_t p0, MethodInfo* method){
	{
		t381  L_0 = (t381 )VirtFuncInvoker1< t381 , int32_t >::Invoke(&m16612_MI, __this, p0);
		return L_0;
	}
}
extern MethodInfo m16590_MI;
 void m16590 (t3032 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16591_MI;
 bool m16591 (t3032 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m16592_MI;
 void m16592 (t3032 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m16593_MI;
 t29 * m16593 (t3032 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16594_MI;
 int32_t m16594 (t3032 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16595_MI;
 void m16595 (t3032 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16596_MI;
 bool m16596 (t3032 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16644(NULL, p0, &m16644_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t381  >::Invoke(&m28151_MI, L_1, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m16597_MI;
 int32_t m16597 (t3032 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16644(NULL, p0, &m16644_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t381  >::Invoke(&m28144_MI, L_1, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m16598_MI;
 void m16598 (t3032 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16599_MI;
 void m16599 (t3032 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16600_MI;
 void m16600 (t3032 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16601_MI;
 bool m16601 (t3032 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16602_MI;
 t29 * m16602 (t3032 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m16603_MI;
 bool m16603 (t3032 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m16604_MI;
 bool m16604 (t3032 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m16605_MI;
 t29 * m16605 (t3032 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t381  L_1 = (t381 )InterfaceFuncInvoker1< t381 , int32_t >::Invoke(&m1753_MI, L_0, p0);
		t381  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t381_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m16606_MI;
 void m16606 (t3032 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16607_MI;
 bool m16607 (t3032 * __this, t381  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t381  >::Invoke(&m28151_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16608_MI;
 void m16608 (t3032 * __this, t530* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t530*, int32_t >::Invoke(&m28152_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m16609_MI;
 t29* m16609 (t3032 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16610_MI;
 int32_t m16610 (t3032 * __this, t381  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t381  >::Invoke(&m28144_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16611_MI;
 int32_t m16611 (t3032 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m28147_MI, L_0);
		return L_1;
	}
}
 t381  m16612 (t3032 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t381  L_1 = (t381 )InterfaceFuncInvoker1< t381 , int32_t >::Invoke(&m1753_MI, L_0, p0);
		return L_1;
	}
}
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
extern Il2CppType t388_0_0_1;
FieldInfo t3032_f0_FieldInfo = 
{
	"list", &t388_0_0_1, &t3032_TI, offsetof(t3032, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3032_FIs[] =
{
	&t3032_f0_FieldInfo,
	NULL
};
static PropertyInfo t3032____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3032_TI, "System.Collections.Generic.IList<T>.Item", &m16589_MI, &m16590_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3032_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3032_TI, "System.Collections.ICollection.IsSynchronized", &m16601_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3032_TI, "System.Collections.ICollection.SyncRoot", &m16602_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3032_TI, "System.Collections.IList.IsFixedSize", &m16603_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3032_TI, "System.Collections.IList.IsReadOnly", &m16604_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____System_Collections_IList_Item_PropertyInfo = 
{
	&t3032_TI, "System.Collections.IList.Item", &m16605_MI, &m16606_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____Count_PropertyInfo = 
{
	&t3032_TI, "Count", &m16611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3032____Item_PropertyInfo = 
{
	&t3032_TI, "Item", &m16612_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3032_PIs[] =
{
	&t3032____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3032____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3032____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3032____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3032____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3032____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3032____System_Collections_IList_Item_PropertyInfo,
	&t3032____Count_PropertyInfo,
	&t3032____Item_PropertyInfo,
	NULL
};
extern Il2CppType t388_0_0_0;
extern Il2CppType t388_0_0_0;
static ParameterInfo t3032_m16583_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t388_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16583_GM;
MethodInfo m16583_MI = 
{
	".ctor", (methodPointerType)&m16583, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3032_m16583_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16583_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3032_m16584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16584_GM;
MethodInfo m16584_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m16584, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t381, t3032_m16584_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16584_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16585_GM;
MethodInfo m16585_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m16585, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16585_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3032_m16586_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16586_GM;
MethodInfo m16586_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m16586, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t3032_m16586_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16586_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3032_m16587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16587_GM;
MethodInfo m16587_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m16587, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3032_m16587_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16587_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16588_GM;
MethodInfo m16588_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m16588, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3032_m16588_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16588_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16589_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16589_GM;
MethodInfo m16589_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m16589, &t3032_TI, &t381_0_0_0, RuntimeInvoker_t381_t44, t3032_m16589_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16589_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3032_m16590_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16590_GM;
MethodInfo m16590_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m16590, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t3032_m16590_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16590_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16591_GM;
MethodInfo m16591_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m16591, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16591_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16592_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16592_GM;
MethodInfo m16592_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m16592, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3032_m16592_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16592_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16593_GM;
MethodInfo m16593_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m16593, &t3032_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16593_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3032_m16594_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16594_GM;
MethodInfo m16594_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m16594, &t3032_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3032_m16594_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16594_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16595_GM;
MethodInfo m16595_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m16595, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16595_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3032_m16596_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16596_GM;
MethodInfo m16596_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m16596, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3032_m16596_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16596_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3032_m16597_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16597_GM;
MethodInfo m16597_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m16597, &t3032_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3032_m16597_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16597_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3032_m16598_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16598_GM;
MethodInfo m16598_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m16598, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3032_m16598_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16598_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3032_m16599_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16599_GM;
MethodInfo m16599_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m16599, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3032_m16599_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16599_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16600_GM;
MethodInfo m16600_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m16600, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3032_m16600_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16600_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16601_GM;
MethodInfo m16601_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m16601, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16601_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16602_GM;
MethodInfo m16602_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m16602, &t3032_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16602_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16603_GM;
MethodInfo m16603_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m16603, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16603_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16604_GM;
MethodInfo m16604_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m16604, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16604_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16605_GM;
MethodInfo m16605_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m16605, &t3032_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3032_m16605_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16605_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3032_m16606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16606_GM;
MethodInfo m16606_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m16606, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3032_m16606_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16606_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3032_m16607_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16607_GM;
MethodInfo m16607_MI = 
{
	"Contains", (methodPointerType)&m16607, &t3032_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3032_m16607_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16607_GM};
extern Il2CppType t530_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16608_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t530_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16608_GM;
MethodInfo m16608_MI = 
{
	"CopyTo", (methodPointerType)&m16608, &t3032_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3032_m16608_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16608_GM};
extern Il2CppType t3029_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16609_GM;
MethodInfo m16609_MI = 
{
	"GetEnumerator", (methodPointerType)&m16609, &t3032_TI, &t3029_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16609_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3032_m16610_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16610_GM;
MethodInfo m16610_MI = 
{
	"IndexOf", (methodPointerType)&m16610, &t3032_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t3032_m16610_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16610_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16611_GM;
MethodInfo m16611_MI = 
{
	"get_Count", (methodPointerType)&m16611, &t3032_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16611_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3032_m16612_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16612_GM;
MethodInfo m16612_MI = 
{
	"get_Item", (methodPointerType)&m16612, &t3032_TI, &t381_0_0_0, RuntimeInvoker_t381_t44, t3032_m16612_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16612_GM};
static MethodInfo* t3032_MIs[] =
{
	&m16583_MI,
	&m16584_MI,
	&m16585_MI,
	&m16586_MI,
	&m16587_MI,
	&m16588_MI,
	&m16589_MI,
	&m16590_MI,
	&m16591_MI,
	&m16592_MI,
	&m16593_MI,
	&m16594_MI,
	&m16595_MI,
	&m16596_MI,
	&m16597_MI,
	&m16598_MI,
	&m16599_MI,
	&m16600_MI,
	&m16601_MI,
	&m16602_MI,
	&m16603_MI,
	&m16604_MI,
	&m16605_MI,
	&m16606_MI,
	&m16607_MI,
	&m16608_MI,
	&m16609_MI,
	&m16610_MI,
	&m16611_MI,
	&m16612_MI,
	NULL
};
static MethodInfo* t3032_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16593_MI,
	&m16611_MI,
	&m16601_MI,
	&m16602_MI,
	&m16592_MI,
	&m16603_MI,
	&m16604_MI,
	&m16605_MI,
	&m16606_MI,
	&m16594_MI,
	&m16595_MI,
	&m16596_MI,
	&m16597_MI,
	&m16598_MI,
	&m16599_MI,
	&m16600_MI,
	&m16611_MI,
	&m16591_MI,
	&m16584_MI,
	&m16585_MI,
	&m16607_MI,
	&m16608_MI,
	&m16587_MI,
	&m16610_MI,
	&m16586_MI,
	&m16588_MI,
	&m16589_MI,
	&m16590_MI,
	&m16609_MI,
	&m16612_MI,
};
static TypeInfo* t3032_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3030_TI,
	&t388_TI,
	&t3031_TI,
};
static Il2CppInterfaceOffsetPair t3032_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3030_TI, 20},
	{ &t388_TI, 27},
	{ &t3031_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3032_0_0_0;
extern Il2CppType t3032_1_0_0;
struct t3032;
extern Il2CppGenericClass t3032_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3032_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3032_MIs, t3032_PIs, t3032_FIs, NULL, &t29_TI, NULL, NULL, &t3032_TI, t3032_ITIs, t3032_VT, &t1263__CustomAttributeCache, &t3032_TI, &t3032_0_0_0, &t3032_1_0_0, t3032_IOs, &t3032_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3032), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3036.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3036_TI;

extern MethodInfo m28148_MI;
extern MethodInfo m16647_MI;
extern MethodInfo m16648_MI;
extern MethodInfo m16645_MI;
extern MethodInfo m16643_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m16636_MI;
extern MethodInfo m16646_MI;
extern MethodInfo m16634_MI;
extern MethodInfo m16639_MI;
extern MethodInfo m16630_MI;
extern MethodInfo m28150_MI;
extern MethodInfo m28145_MI;
extern MethodInfo m28146_MI;
extern MethodInfo m28143_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


extern MethodInfo m16613_MI;
 void m16613 (t3036 * __this, MethodInfo* method){
	t528 * V_0 = {0};
	t29 * V_1 = {0};
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t528_TI));
		t528 * L_0 = (t528 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t528_TI));
		m16529(L_0, &m16529_MI);
		V_0 = L_0;
		V_1 = V_0;
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, V_1);
		__this->f1 = L_1;
		__this->f0 = V_0;
		return;
	}
}
extern MethodInfo m16614_MI;
 bool m16614 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m28148_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16615_MI;
 void m16615 (t3036 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m16616_MI;
 t29 * m16616 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16617_MI;
 int32_t m16617 (t3036 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m28147_MI, L_0);
		V_0 = L_1;
		t381  L_2 = m16645(NULL, p0, &m16645_MI);
		VirtActionInvoker2< int32_t, t381  >::Invoke(&m16636_MI, __this, V_0, L_2);
		return V_0;
	}
}
extern MethodInfo m16618_MI;
 bool m16618 (t3036 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16644(NULL, p0, &m16644_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t381  >::Invoke(&m28151_MI, L_1, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m16619_MI;
 int32_t m16619 (t3036 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16644(NULL, p0, &m16644_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t381  >::Invoke(&m28144_MI, L_1, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m16620_MI;
 void m16620 (t3036 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t381  L_0 = m16645(NULL, p1, &m16645_MI);
		VirtActionInvoker2< int32_t, t381  >::Invoke(&m16636_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m16621_MI;
 void m16621 (t3036 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		m16646(NULL, L_0, &m16646_MI);
		t381  L_1 = m16645(NULL, p0, &m16645_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t381  >::Invoke(&m16634_MI, __this, L_1);
		V_0 = L_2;
		VirtActionInvoker1< int32_t >::Invoke(&m16639_MI, __this, V_0);
		return;
	}
}
extern MethodInfo m16622_MI;
 bool m16622 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m16647(NULL, L_0, &m16647_MI);
		return L_1;
	}
}
extern MethodInfo m16623_MI;
 t29 * m16623 (t3036 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m16624_MI;
 bool m16624 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m16648(NULL, L_0, &m16648_MI);
		return L_1;
	}
}
extern MethodInfo m16625_MI;
 bool m16625 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m28148_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16626_MI;
 t29 * m16626 (t3036 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t381  L_1 = (t381 )InterfaceFuncInvoker1< t381 , int32_t >::Invoke(&m1753_MI, L_0, p0);
		t381  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t381_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m16627_MI;
 void m16627 (t3036 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t381  L_0 = m16645(NULL, p1, &m16645_MI);
		VirtActionInvoker2< int32_t, t381  >::Invoke(&m16643_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m16628_MI;
 void m16628 (t3036 * __this, t381  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m28147_MI, L_0);
		V_0 = L_1;
		VirtActionInvoker2< int32_t, t381  >::Invoke(&m16636_MI, __this, V_0, p0);
		return;
	}
}
extern MethodInfo m16629_MI;
 void m16629 (t3036 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m16630_MI, __this);
		return;
	}
}
 void m16630 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker0::Invoke(&m28150_MI, L_0);
		return;
	}
}
extern MethodInfo m16631_MI;
 bool m16631 (t3036 * __this, t381  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t381  >::Invoke(&m28151_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16632_MI;
 void m16632 (t3036 * __this, t530* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t530*, int32_t >::Invoke(&m28152_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m16633_MI;
 t29* m16633 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28154_MI, L_0);
		return L_1;
	}
}
 int32_t m16634 (t3036 * __this, t381  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t381  >::Invoke(&m28144_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16635_MI;
 void m16635 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t381  >::Invoke(&m16636_MI, __this, p0, p1);
		return;
	}
}
 void m16636 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t381  >::Invoke(&m28145_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m16637_MI;
 bool m16637 (t3036 * __this, t381  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t381  >::Invoke(&m16634_MI, __this, p0);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		VirtActionInvoker1< int32_t >::Invoke(&m16639_MI, __this, V_0);
		return 1;
	}
}
extern MethodInfo m16638_MI;
 void m16638 (t3036 * __this, int32_t p0, MethodInfo* method){
	{
		VirtActionInvoker1< int32_t >::Invoke(&m16639_MI, __this, p0);
		return;
	}
}
 void m16639 (t3036 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker1< int32_t >::Invoke(&m28146_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m16640_MI;
 int32_t m16640 (t3036 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m28147_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16641_MI;
 t381  m16641 (t3036 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t381  L_1 = (t381 )InterfaceFuncInvoker1< t381 , int32_t >::Invoke(&m1753_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16642_MI;
 void m16642 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t381  >::Invoke(&m16643_MI, __this, p0, p1);
		return;
	}
}
 void m16643 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t381  >::Invoke(&m28143_MI, L_0, p0, p1);
		return;
	}
}
 bool m16644 (t29 * __this, t29 * p0, MethodInfo* method){
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t381_TI))))
		{
			goto IL_0022;
		}
	}
	{
		if (p0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t381_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		G_B4_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
	}

IL_0020:
	{
		G_B6_0 = G_B4_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		return G_B6_0;
	}
}
 t381  m16645 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16644(NULL, p0, &m16644_MI);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		return ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI)))));
	}

IL_000f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 void m16646 (t29 * __this, t29* p0, MethodInfo* method){
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m28148_MI, p0);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		t345 * L_1 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_1, &m1516_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
 bool m16647 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t674_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9815_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
 bool m16648 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t868_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9817_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
extern Il2CppType t388_0_0_1;
FieldInfo t3036_f0_FieldInfo = 
{
	"list", &t388_0_0_1, &t3036_TI, offsetof(t3036, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3036_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3036_TI, offsetof(t3036, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3036_FIs[] =
{
	&t3036_f0_FieldInfo,
	&t3036_f1_FieldInfo,
	NULL
};
static PropertyInfo t3036____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3036_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16614_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3036_TI, "System.Collections.ICollection.IsSynchronized", &m16622_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3036_TI, "System.Collections.ICollection.SyncRoot", &m16623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3036_TI, "System.Collections.IList.IsFixedSize", &m16624_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3036_TI, "System.Collections.IList.IsReadOnly", &m16625_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____System_Collections_IList_Item_PropertyInfo = 
{
	&t3036_TI, "System.Collections.IList.Item", &m16626_MI, &m16627_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____Count_PropertyInfo = 
{
	&t3036_TI, "Count", &m16640_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3036____Item_PropertyInfo = 
{
	&t3036_TI, "Item", &m16641_MI, &m16642_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3036_PIs[] =
{
	&t3036____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3036____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3036____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3036____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3036____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3036____System_Collections_IList_Item_PropertyInfo,
	&t3036____Count_PropertyInfo,
	&t3036____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16613_GM;
MethodInfo m16613_MI = 
{
	".ctor", (methodPointerType)&m16613, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16613_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16614_GM;
MethodInfo m16614_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m16614, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16614_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3036_m16615_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16615_GM;
MethodInfo m16615_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m16615, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3036_m16615_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16615_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16616_GM;
MethodInfo m16616_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m16616, &t3036_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16616_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16617_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16617_GM;
MethodInfo m16617_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m16617, &t3036_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3036_m16617_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16617_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16618_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16618_GM;
MethodInfo m16618_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m16618, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3036_m16618_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16618_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16619_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16619_GM;
MethodInfo m16619_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m16619, &t3036_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3036_m16619_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16619_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16620_GM;
MethodInfo m16620_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m16620, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3036_m16620_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16620_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16621_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16621_GM;
MethodInfo m16621_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m16621, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3036_m16621_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16621_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16622_GM;
MethodInfo m16622_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m16622, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16622_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16623_GM;
MethodInfo m16623_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m16623, &t3036_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16623_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16624_GM;
MethodInfo m16624_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m16624, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16624_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16625_GM;
MethodInfo m16625_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m16625, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16625_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3036_m16626_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16626_GM;
MethodInfo m16626_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m16626, &t3036_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3036_m16626_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16626_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16627_GM;
MethodInfo m16627_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m16627, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3036_m16627_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16627_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16628_GM;
MethodInfo m16628_MI = 
{
	"Add", (methodPointerType)&m16628, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t381, t3036_m16628_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16628_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16629_GM;
MethodInfo m16629_MI = 
{
	"Clear", (methodPointerType)&m16629, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16629_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16630_GM;
MethodInfo m16630_MI = 
{
	"ClearItems", (methodPointerType)&m16630, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16630_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16631_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16631_GM;
MethodInfo m16631_MI = 
{
	"Contains", (methodPointerType)&m16631, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3036_m16631_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16631_GM};
extern Il2CppType t530_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3036_m16632_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t530_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16632_GM;
MethodInfo m16632_MI = 
{
	"CopyTo", (methodPointerType)&m16632, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3036_m16632_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16632_GM};
extern Il2CppType t3029_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16633_GM;
MethodInfo m16633_MI = 
{
	"GetEnumerator", (methodPointerType)&m16633, &t3036_TI, &t3029_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16633_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16634_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16634_GM;
MethodInfo m16634_MI = 
{
	"IndexOf", (methodPointerType)&m16634, &t3036_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t3036_m16634_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16634_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16635_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16635_GM;
MethodInfo m16635_MI = 
{
	"Insert", (methodPointerType)&m16635, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t3036_m16635_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16635_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16636_GM;
MethodInfo m16636_MI = 
{
	"InsertItem", (methodPointerType)&m16636, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t3036_m16636_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16636_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16637_GM;
MethodInfo m16637_MI = 
{
	"Remove", (methodPointerType)&m16637, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3036_m16637_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16637_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3036_m16638_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16638_GM;
MethodInfo m16638_MI = 
{
	"RemoveAt", (methodPointerType)&m16638, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3036_m16638_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16638_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3036_m16639_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16639_GM;
MethodInfo m16639_MI = 
{
	"RemoveItem", (methodPointerType)&m16639, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3036_m16639_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16639_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16640_GM;
MethodInfo m16640_MI = 
{
	"get_Count", (methodPointerType)&m16640, &t3036_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16640_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3036_m16641_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16641_GM;
MethodInfo m16641_MI = 
{
	"get_Item", (methodPointerType)&m16641, &t3036_TI, &t381_0_0_0, RuntimeInvoker_t381_t44, t3036_m16641_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16641_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16642_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16642_GM;
MethodInfo m16642_MI = 
{
	"set_Item", (methodPointerType)&m16642, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t3036_m16642_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16642_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3036_m16643_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16643_GM;
MethodInfo m16643_MI = 
{
	"SetItem", (methodPointerType)&m16643, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t3036_m16643_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16643_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16644_GM;
MethodInfo m16644_MI = 
{
	"IsValidItem", (methodPointerType)&m16644, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3036_m16644_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16644_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3036_m16645_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16645_GM;
MethodInfo m16645_MI = 
{
	"ConvertItem", (methodPointerType)&m16645, &t3036_TI, &t381_0_0_0, RuntimeInvoker_t381_t29, t3036_m16645_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16645_GM};
extern Il2CppType t388_0_0_0;
static ParameterInfo t3036_m16646_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t388_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16646_GM;
MethodInfo m16646_MI = 
{
	"CheckWritable", (methodPointerType)&m16646, &t3036_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3036_m16646_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16646_GM};
extern Il2CppType t388_0_0_0;
static ParameterInfo t3036_m16647_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t388_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16647_GM;
MethodInfo m16647_MI = 
{
	"IsSynchronized", (methodPointerType)&m16647, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3036_m16647_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16647_GM};
extern Il2CppType t388_0_0_0;
static ParameterInfo t3036_m16648_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t388_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16648_GM;
MethodInfo m16648_MI = 
{
	"IsFixedSize", (methodPointerType)&m16648, &t3036_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3036_m16648_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16648_GM};
static MethodInfo* t3036_MIs[] =
{
	&m16613_MI,
	&m16614_MI,
	&m16615_MI,
	&m16616_MI,
	&m16617_MI,
	&m16618_MI,
	&m16619_MI,
	&m16620_MI,
	&m16621_MI,
	&m16622_MI,
	&m16623_MI,
	&m16624_MI,
	&m16625_MI,
	&m16626_MI,
	&m16627_MI,
	&m16628_MI,
	&m16629_MI,
	&m16630_MI,
	&m16631_MI,
	&m16632_MI,
	&m16633_MI,
	&m16634_MI,
	&m16635_MI,
	&m16636_MI,
	&m16637_MI,
	&m16638_MI,
	&m16639_MI,
	&m16640_MI,
	&m16641_MI,
	&m16642_MI,
	&m16643_MI,
	&m16644_MI,
	&m16645_MI,
	&m16646_MI,
	&m16647_MI,
	&m16648_MI,
	NULL
};
static MethodInfo* t3036_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16616_MI,
	&m16640_MI,
	&m16622_MI,
	&m16623_MI,
	&m16615_MI,
	&m16624_MI,
	&m16625_MI,
	&m16626_MI,
	&m16627_MI,
	&m16617_MI,
	&m16629_MI,
	&m16618_MI,
	&m16619_MI,
	&m16620_MI,
	&m16621_MI,
	&m16638_MI,
	&m16640_MI,
	&m16614_MI,
	&m16628_MI,
	&m16629_MI,
	&m16631_MI,
	&m16632_MI,
	&m16637_MI,
	&m16634_MI,
	&m16635_MI,
	&m16638_MI,
	&m16641_MI,
	&m16642_MI,
	&m16633_MI,
	&m16630_MI,
	&m16636_MI,
	&m16639_MI,
	&m16643_MI,
};
static TypeInfo* t3036_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3030_TI,
	&t388_TI,
	&t3031_TI,
};
static Il2CppInterfaceOffsetPair t3036_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3030_TI, 20},
	{ &t388_TI, 27},
	{ &t3031_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3036_0_0_0;
extern Il2CppType t3036_1_0_0;
struct t3036;
extern Il2CppGenericClass t3036_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3036_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3036_MIs, t3036_PIs, t3036_FIs, NULL, &t29_TI, NULL, NULL, &t3036_TI, t3036_ITIs, t3036_VT, &t1262__CustomAttributeCache, &t3036_TI, &t3036_0_0_0, &t3036_1_0_0, t3036_IOs, &t3036_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3036), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3037_TI;
#include "t3037MD.h"

#include "t1257.h"
#include "t3038.h"
extern TypeInfo t6736_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3038_TI;
#include "t931MD.h"
#include "t3038MD.h"
extern Il2CppType t6736_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m16654_MI;
extern MethodInfo m29832_MI;
extern MethodInfo m22687_MI;


extern MethodInfo m16649_MI;
 void m16649 (t3037 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m16650_MI;
 void m16650 (t29 * __this, MethodInfo* method){
	t3038 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3038 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3038_TI));
	m16654(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m16654_MI);
	((t3037_SFs*)InitializedTypeInfo(&t3037_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m16651_MI;
 int32_t m16651 (t3037 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t381  >::Invoke(&m29832_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))));
		return L_0;
	}
}
extern MethodInfo m16652_MI;
 bool m16652 (t3037 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t381 , t381  >::Invoke(&m22687_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))), ((*(t381 *)((t381 *)UnBox (p1, InitializedTypeInfo(&t381_TI))))));
		return L_0;
	}
}
extern MethodInfo m16653_MI;
 t3037 * m16653 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3037_TI));
		return (((t3037_SFs*)InitializedTypeInfo(&t3037_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
extern Il2CppType t3037_0_0_49;
FieldInfo t3037_f0_FieldInfo = 
{
	"_default", &t3037_0_0_49, &t3037_TI, offsetof(t3037_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3037_FIs[] =
{
	&t3037_f0_FieldInfo,
	NULL
};
static PropertyInfo t3037____Default_PropertyInfo = 
{
	&t3037_TI, "Default", &m16653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3037_PIs[] =
{
	&t3037____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16649_GM;
MethodInfo m16649_MI = 
{
	".ctor", (methodPointerType)&m16649, &t3037_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16649_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16650_GM;
MethodInfo m16650_MI = 
{
	".cctor", (methodPointerType)&m16650, &t3037_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16650_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3037_m16651_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16651_GM;
MethodInfo m16651_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m16651, &t3037_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3037_m16651_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16651_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3037_m16652_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16652_GM;
MethodInfo m16652_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m16652, &t3037_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3037_m16652_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16652_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3037_m29832_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29832_GM;
MethodInfo m29832_MI = 
{
	"GetHashCode", NULL, &t3037_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t3037_m29832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29832_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3037_m22687_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22687_GM;
MethodInfo m22687_MI = 
{
	"Equals", NULL, &t3037_TI, &t40_0_0_0, RuntimeInvoker_t40_t381_t381, t3037_m22687_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22687_GM};
extern Il2CppType t3037_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16653_GM;
MethodInfo m16653_MI = 
{
	"get_Default", (methodPointerType)&m16653, &t3037_TI, &t3037_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16653_GM};
static MethodInfo* t3037_MIs[] =
{
	&m16649_MI,
	&m16650_MI,
	&m16651_MI,
	&m16652_MI,
	&m29832_MI,
	&m22687_MI,
	&m16653_MI,
	NULL
};
static MethodInfo* t3037_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m22687_MI,
	&m29832_MI,
	&m16652_MI,
	&m16651_MI,
	NULL,
	NULL,
};
extern TypeInfo t6737_TI;
extern TypeInfo t734_TI;
static TypeInfo* t3037_ITIs[] = 
{
	&t6737_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3037_IOs[] = 
{
	{ &t6737_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3037_0_0_0;
extern Il2CppType t3037_1_0_0;
struct t3037;
extern Il2CppGenericClass t3037_GC;
TypeInfo t3037_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3037_MIs, t3037_PIs, t3037_FIs, NULL, &t29_TI, NULL, NULL, &t3037_TI, t3037_ITIs, t3037_VT, &EmptyCustomAttributesCache, &t3037_TI, &t3037_0_0_0, &t3037_1_0_0, t3037_IOs, &t3037_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3037), 0, -1, sizeof(t3037_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UICharInfo>
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t6737_m29833_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29833_GM;
MethodInfo m29833_MI = 
{
	"Equals", NULL, &t6737_TI, &t40_0_0_0, RuntimeInvoker_t40_t381_t381, t6737_m29833_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29833_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t6737_m29834_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29834_GM;
MethodInfo m29834_MI = 
{
	"GetHashCode", NULL, &t6737_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t6737_m29834_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29834_GM};
static MethodInfo* t6737_MIs[] =
{
	&m29833_MI,
	&m29834_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6737_0_0_0;
extern Il2CppType t6737_1_0_0;
struct t6737;
extern Il2CppGenericClass t6737_GC;
TypeInfo t6737_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6737_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6737_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6737_TI, &t6737_0_0_0, &t6737_1_0_0, NULL, &t6737_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UICharInfo>
extern Il2CppType t381_0_0_0;
static ParameterInfo t6736_m29835_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29835_GM;
MethodInfo m29835_MI = 
{
	"Equals", NULL, &t6736_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t6736_m29835_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29835_GM};
static MethodInfo* t6736_MIs[] =
{
	&m29835_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6736_1_0_0;
struct t6736;
extern Il2CppGenericClass t6736_GC;
TypeInfo t6736_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6736_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6736_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6736_TI, &t6736_0_0_0, &t6736_1_0_0, NULL, &t6736_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m16654 (t3038 * __this, MethodInfo* method){
	{
		m16649(__this, &m16649_MI);
		return;
	}
}
extern MethodInfo m16655_MI;
 int32_t m16655 (t3038 * __this, t381  p0, MethodInfo* method){
	{
		t381  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t381_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t381_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m16656_MI;
 bool m16656 (t3038 * __this, t381  p0, t381  p1, MethodInfo* method){
	{
		t381  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t381_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t381  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t381_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t381  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t381_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t381_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16654_GM;
MethodInfo m16654_MI = 
{
	".ctor", (methodPointerType)&m16654, &t3038_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16654_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3038_m16655_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16655_GM;
MethodInfo m16655_MI = 
{
	"GetHashCode", (methodPointerType)&m16655, &t3038_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t3038_m16655_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16655_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3038_m16656_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16656_GM;
MethodInfo m16656_MI = 
{
	"Equals", (methodPointerType)&m16656, &t3038_TI, &t40_0_0_0, RuntimeInvoker_t40_t381_t381, t3038_m16656_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16656_GM};
static MethodInfo* t3038_MIs[] =
{
	&m16654_MI,
	&m16655_MI,
	&m16656_MI,
	NULL
};
static MethodInfo* t3038_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16656_MI,
	&m16655_MI,
	&m16652_MI,
	&m16651_MI,
	&m16655_MI,
	&m16656_MI,
};
static Il2CppInterfaceOffsetPair t3038_IOs[] = 
{
	{ &t6737_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3038_0_0_0;
extern Il2CppType t3038_1_0_0;
struct t3038;
extern Il2CppGenericClass t3038_GC;
extern TypeInfo t1256_TI;
TypeInfo t3038_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3038_MIs, NULL, NULL, NULL, &t3037_TI, NULL, &t1256_TI, &t3038_TI, NULL, t3038_VT, &EmptyCustomAttributesCache, &t3038_TI, &t3038_0_0_0, &t3038_1_0_0, t3038_IOs, &t3038_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3038), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m16657_MI;
 void m16657 (t3033 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 bool m16658 (t3033 * __this, t381  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m16658((t3033 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t381  p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef bool (*FunctionPointerType) (t29 * __this, t381  p0, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m16659_MI;
 t29 * m16659 (t3033 * __this, t381  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t381_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m16660_MI;
 bool m16660 (t3033 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Predicate`1<UnityEngine.UICharInfo>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3033_m16657_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16657_GM;
MethodInfo m16657_MI = 
{
	".ctor", (methodPointerType)&m16657, &t3033_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3033_m16657_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16657_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3033_m16658_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16658_GM;
MethodInfo m16658_MI = 
{
	"Invoke", (methodPointerType)&m16658, &t3033_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3033_m16658_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16658_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3033_m16659_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t381_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16659_GM;
MethodInfo m16659_MI = 
{
	"BeginInvoke", (methodPointerType)&m16659, &t3033_TI, &t66_0_0_0, RuntimeInvoker_t29_t381_t29_t29, t3033_m16659_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16659_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3033_m16660_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16660_GM;
MethodInfo m16660_MI = 
{
	"EndInvoke", (methodPointerType)&m16660, &t3033_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3033_m16660_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16660_GM};
static MethodInfo* t3033_MIs[] =
{
	&m16657_MI,
	&m16658_MI,
	&m16659_MI,
	&m16660_MI,
	NULL
};
static MethodInfo* t3033_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16658_MI,
	&m16659_MI,
	&m16660_MI,
};
static Il2CppInterfaceOffsetPair t3033_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3033_1_0_0;
struct t3033;
extern Il2CppGenericClass t3033_GC;
TypeInfo t3033_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t3033_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3033_TI, NULL, t3033_VT, &EmptyCustomAttributesCache, &t3033_TI, &t3033_0_0_0, &t3033_1_0_0, t3033_IOs, &t3033_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3033), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t3040.h"
extern TypeInfo t4443_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t3040_TI;
#include "t3040MD.h"
extern Il2CppType t4443_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m16665_MI;
extern MethodInfo m29836_MI;
extern MethodInfo m8852_MI;


extern MethodInfo m16661_MI;
 void m16661 (t3039 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m16662_MI;
 void m16662 (t29 * __this, MethodInfo* method){
	t3040 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3040 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3040_TI));
	m16665(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m16665_MI);
	((t3039_SFs*)InitializedTypeInfo(&t3039_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m16663_MI;
 int32_t m16663 (t3039 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t381_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t381_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t381 , t381  >::Invoke(&m29836_MI, __this, ((*(t381 *)((t381 *)UnBox (p0, InitializedTypeInfo(&t381_TI))))), ((*(t381 *)((t381 *)UnBox (p1, InitializedTypeInfo(&t381_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 t3039 * m16664 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3039_TI));
		return (((t3039_SFs*)InitializedTypeInfo(&t3039_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
extern Il2CppType t3039_0_0_49;
FieldInfo t3039_f0_FieldInfo = 
{
	"_default", &t3039_0_0_49, &t3039_TI, offsetof(t3039_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3039_FIs[] =
{
	&t3039_f0_FieldInfo,
	NULL
};
static PropertyInfo t3039____Default_PropertyInfo = 
{
	&t3039_TI, "Default", &m16664_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3039_PIs[] =
{
	&t3039____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16661_GM;
MethodInfo m16661_MI = 
{
	".ctor", (methodPointerType)&m16661, &t3039_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16661_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16662_GM;
MethodInfo m16662_MI = 
{
	".cctor", (methodPointerType)&m16662, &t3039_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16662_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3039_m16663_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16663_GM;
MethodInfo m16663_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m16663, &t3039_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3039_m16663_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16663_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3039_m29836_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29836_GM;
MethodInfo m29836_MI = 
{
	"Compare", NULL, &t3039_TI, &t44_0_0_0, RuntimeInvoker_t44_t381_t381, t3039_m29836_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29836_GM};
extern Il2CppType t3039_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16664_GM;
MethodInfo m16664_MI = 
{
	"get_Default", (methodPointerType)&m16664, &t3039_TI, &t3039_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16664_GM};
static MethodInfo* t3039_MIs[] =
{
	&m16661_MI,
	&m16662_MI,
	&m16663_MI,
	&m29836_MI,
	&m16664_MI,
	NULL
};
static MethodInfo* t3039_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29836_MI,
	&m16663_MI,
	NULL,
};
extern TypeInfo t4442_TI;
extern TypeInfo t726_TI;
static TypeInfo* t3039_ITIs[] = 
{
	&t4442_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3039_IOs[] = 
{
	{ &t4442_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3039_0_0_0;
extern Il2CppType t3039_1_0_0;
struct t3039;
extern Il2CppGenericClass t3039_GC;
TypeInfo t3039_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3039_MIs, t3039_PIs, t3039_FIs, NULL, &t29_TI, NULL, NULL, &t3039_TI, t3039_ITIs, t3039_VT, &EmptyCustomAttributesCache, &t3039_TI, &t3039_0_0_0, &t3039_1_0_0, t3039_IOs, &t3039_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3039), 0, -1, sizeof(t3039_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UICharInfo>
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t4442_m22695_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22695_GM;
MethodInfo m22695_MI = 
{
	"Compare", NULL, &t4442_TI, &t44_0_0_0, RuntimeInvoker_t44_t381_t381, t4442_m22695_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22695_GM};
static MethodInfo* t4442_MIs[] =
{
	&m22695_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4442_0_0_0;
extern Il2CppType t4442_1_0_0;
struct t4442;
extern Il2CppGenericClass t4442_GC;
TypeInfo t4442_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4442_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4442_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4442_TI, &t4442_0_0_0, &t4442_1_0_0, NULL, &t4442_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UICharInfo>
extern Il2CppType t381_0_0_0;
static ParameterInfo t4443_m22696_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22696_GM;
MethodInfo m22696_MI = 
{
	"CompareTo", NULL, &t4443_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t4443_m22696_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m22696_GM};
static MethodInfo* t4443_MIs[] =
{
	&m22696_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4443_1_0_0;
struct t4443;
extern Il2CppGenericClass t4443_GC;
TypeInfo t4443_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4443_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4443_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4443_TI, &t4443_0_0_0, &t4443_1_0_0, NULL, &t4443_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m22696_MI;
extern MethodInfo m9672_MI;


 void m16665 (t3040 * __this, MethodInfo* method){
	{
		m16661(__this, &m16661_MI);
		return;
	}
}
extern MethodInfo m16666_MI;
 int32_t m16666 (t3040 * __this, t381  p0, t381  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t381  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t381_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t381  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t381_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t381  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t381_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t381  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t381_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t4443_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t381  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t381_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t381  >::Invoke(&m22696_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t4443_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t381  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t381_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t381  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t381_TI), &L_13);
		t381  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t381_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16665_GM;
MethodInfo m16665_MI = 
{
	".ctor", (methodPointerType)&m16665, &t3040_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16665_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3040_m16666_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16666_GM;
MethodInfo m16666_MI = 
{
	"Compare", (methodPointerType)&m16666, &t3040_TI, &t44_0_0_0, RuntimeInvoker_t44_t381_t381, t3040_m16666_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16666_GM};
static MethodInfo* t3040_MIs[] =
{
	&m16665_MI,
	&m16666_MI,
	NULL
};
static MethodInfo* t3040_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16666_MI,
	&m16663_MI,
	&m16666_MI,
};
static Il2CppInterfaceOffsetPair t3040_IOs[] = 
{
	{ &t4442_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3040_0_0_0;
extern Il2CppType t3040_1_0_0;
struct t3040;
extern Il2CppGenericClass t3040_GC;
extern TypeInfo t1246_TI;
TypeInfo t3040_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3040_MIs, NULL, NULL, NULL, &t3039_TI, NULL, &t1246_TI, &t3040_TI, NULL, t3040_VT, &EmptyCustomAttributesCache, &t3040_TI, &t3040_0_0_0, &t3040_1_0_0, t3040_IOs, &t3040_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3040), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3034_TI;
#include "t3034MD.h"



extern MethodInfo m16667_MI;
 void m16667 (t3034 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m16668_MI;
 int32_t m16668 (t3034 * __this, t381  p0, t381  p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m16668((t3034 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t381  p0, t381  p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef int32_t (*FunctionPointerType) (t29 * __this, t381  p0, t381  p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m16669_MI;
 t29 * m16669 (t3034 * __this, t381  p0, t381  p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t381_TI), &p0);
	__d_args[1] = Box(InitializedTypeInfo(&t381_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m16670_MI;
 int32_t m16670 (t3034 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<UnityEngine.UICharInfo>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3034_m16667_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16667_GM;
MethodInfo m16667_MI = 
{
	".ctor", (methodPointerType)&m16667, &t3034_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3034_m16667_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16667_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t3034_m16668_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16668_GM;
MethodInfo m16668_MI = 
{
	"Invoke", (methodPointerType)&m16668, &t3034_TI, &t44_0_0_0, RuntimeInvoker_t44_t381_t381, t3034_m16668_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16668_GM};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3034_m16669_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t381_t381_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16669_GM;
MethodInfo m16669_MI = 
{
	"BeginInvoke", (methodPointerType)&m16669, &t3034_TI, &t66_0_0_0, RuntimeInvoker_t29_t381_t381_t29_t29, t3034_m16669_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m16669_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3034_m16670_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16670_GM;
MethodInfo m16670_MI = 
{
	"EndInvoke", (methodPointerType)&m16670, &t3034_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3034_m16670_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16670_GM};
static MethodInfo* t3034_MIs[] =
{
	&m16667_MI,
	&m16668_MI,
	&m16669_MI,
	&m16670_MI,
	NULL
};
static MethodInfo* t3034_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16668_MI,
	&m16669_MI,
	&m16670_MI,
};
static Il2CppInterfaceOffsetPair t3034_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3034_1_0_0;
struct t3034;
extern Il2CppGenericClass t3034_GC;
TypeInfo t3034_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3034_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3034_TI, NULL, t3034_VT, &EmptyCustomAttributesCache, &t3034_TI, &t3034_0_0_0, &t3034_1_0_0, t3034_IOs, &t3034_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3034), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t529.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t529_TI;
#include "t529MD.h"

#include "t380.h"
#include "t3046.h"
#include "t3043.h"
#include "t3044.h"
#include "t3050.h"
#include "t3045.h"
extern TypeInfo t380_TI;
extern TypeInfo t531_TI;
extern TypeInfo t3046_TI;
extern TypeInfo t389_TI;
extern TypeInfo t3042_TI;
extern TypeInfo t3041_TI;
extern TypeInfo t3043_TI;
extern TypeInfo t3044_TI;
extern TypeInfo t3050_TI;
#include "t3043MD.h"
#include "t3044MD.h"
#include "t3046MD.h"
#include "t3050MD.h"
extern MethodInfo m16717_MI;
extern MethodInfo m16718_MI;
extern MethodInfo m22701_MI;
extern MethodInfo m16703_MI;
extern MethodInfo m16700_MI;
extern MethodInfo m16688_MI;
extern MethodInfo m16695_MI;
extern MethodInfo m16701_MI;
extern MethodInfo m16704_MI;
extern MethodInfo m16706_MI;
extern MethodInfo m16689_MI;
extern MethodInfo m16714_MI;
extern MethodInfo m16715_MI;
extern MethodInfo m1775_MI;
extern MethodInfo m28139_MI;
extern MethodInfo m28141_MI;
extern MethodInfo m28142_MI;
extern MethodInfo m16705_MI;
extern MethodInfo m16690_MI;
extern MethodInfo m16691_MI;
extern MethodInfo m16725_MI;
extern MethodInfo m22703_MI;
extern MethodInfo m16698_MI;
extern MethodInfo m16699_MI;
extern MethodInfo m16800_MI;
extern MethodInfo m16719_MI;
extern MethodInfo m16702_MI;
extern MethodInfo m16708_MI;
extern MethodInfo m16806_MI;
extern MethodInfo m22705_MI;
extern MethodInfo m22713_MI;
struct t20;
 void m22701 (t29 * __this, t531** p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
#include "t3048.h"
 int32_t m22703 (t29 * __this, t531* p0, t380  p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m22705 (t29 * __this, t531* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m22713 (t29 * __this, t531* p0, int32_t p1, t3045 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16671_MI;
 void m16671 (t529 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t529_TI));
		__this->f1 = (((t529_SFs*)InitializedTypeInfo(&t529_TI)->static_fields)->f4);
		return;
	}
}
extern MethodInfo m2905_MI;
 void m2905 (t529 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		__this->f1 = ((t531*)SZArrayNew(InitializedTypeInfo(&t531_TI), p0));
		return;
	}
}
extern MethodInfo m16672_MI;
 void m16672 (t29 * __this, MethodInfo* method){
	{
		((t529_SFs*)InitializedTypeInfo(&t529_TI)->static_fields)->f4 = ((t531*)SZArrayNew(InitializedTypeInfo(&t531_TI), 0));
		return;
	}
}
extern MethodInfo m16673_MI;
 t29* m16673 (t529 * __this, MethodInfo* method){
	{
		t3046  L_0 = m16700(__this, &m16700_MI);
		t3046  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3046_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m16674_MI;
 void m16674 (t529 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m16675_MI;
 t29 * m16675 (t529 * __this, MethodInfo* method){
	{
		t3046  L_0 = m16700(__this, &m16700_MI);
		t3046  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3046_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m16676_MI;
 int32_t m16676 (t529 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker1< t380  >::Invoke(&m16688_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
			int32_t L_0 = (__this->f2);
			V_0 = ((int32_t)(L_0-1));
			// IL_0015: leave.s IL_002a
			goto IL_002a;
		}

IL_0017:
		{
			// IL_0017: leave.s IL_001f
			goto IL_001f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0019;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001c;
		throw e;
	}

IL_0019:
	{ // begin catch(System.NullReferenceException)
		// IL_001a: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001c:
	{ // begin catch(System.InvalidCastException)
		// IL_001d: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		return V_0;
	}
}
extern MethodInfo m16677_MI;
 bool m16677 (t529 * __this, t29 * p0, MethodInfo* method){
	bool V_0 = false;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (bool)VirtFuncInvoker1< bool, t380  >::Invoke(&m16695_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m16678_MI;
 int32_t m16678 (t529 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t380  >::Invoke(&m16701_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return (-1);
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m16679_MI;
 void m16679 (t529 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		m16703(__this, p0, &m16703_MI);
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t380  >::Invoke(&m16704_MI, __this, p0, ((*(t380 *)((t380 *)UnBox (p1, InitializedTypeInfo(&t380_TI))))));
			// IL_0014: leave.s IL_0029
			goto IL_0029;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_001e
			goto IL_001e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0018;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001b;
		throw e;
	}

IL_0018:
	{ // begin catch(System.NullReferenceException)
		// IL_0019: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001b:
	{ // begin catch(System.InvalidCastException)
		// IL_001c: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001e:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0029:
	{
		return;
	}
}
extern MethodInfo m16680_MI;
 void m16680 (t529 * __this, t29 * p0, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtFuncInvoker1< bool, t380  >::Invoke(&m16706_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
			// IL_000d: leave.s IL_0017
			goto IL_0017;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return;
	}
}
extern MethodInfo m16681_MI;
 bool m16681 (t529 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16682_MI;
 bool m16682 (t529 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16683_MI;
 t29 * m16683 (t529 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m16684_MI;
 bool m16684 (t529 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16685_MI;
 bool m16685 (t529 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16686_MI;
 t29 * m16686 (t529 * __this, int32_t p0, MethodInfo* method){
	{
		t380  L_0 = (t380 )VirtFuncInvoker1< t380 , int32_t >::Invoke(&m16717_MI, __this, p0);
		t380  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t380_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16687_MI;
 void m16687 (t529 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t380  >::Invoke(&m16718_MI, __this, p0, ((*(t380 *)((t380 *)UnBox (p1, InitializedTypeInfo(&t380_TI))))));
			// IL_000d: leave.s IL_0022
			goto IL_0022;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral401, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0022:
	{
		return;
	}
}
 void m16688 (t529 * __this, t380  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		t531* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		m16689(__this, 1, &m16689_MI);
	}

IL_0017:
	{
		t531* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->f2 = ((int32_t)(L_4+1));
		*((t380 *)(t380 *)SZArrayLdElema(L_2, V_0)) = (t380 )p0;
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		return;
	}
}
 void m16689 (t529 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((int32_t)(L_0+p0));
		t531* L_1 = (__this->f1);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = m16714(__this, &m16714_MI);
		int32_t L_3 = m5139(NULL, ((int32_t)((int32_t)L_2*(int32_t)2)), 4, &m5139_MI);
		int32_t L_4 = m5139(NULL, L_3, V_0, &m5139_MI);
		m16715(__this, L_4, &m16715_MI);
	}

IL_002e:
	{
		return;
	}
}
 void m16690 (t529 * __this, t29* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1775_MI, p0);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		m16689(__this, V_0, &m16689_MI);
		t531* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		InterfaceActionInvoker2< t531*, int32_t >::Invoke(&m28139_MI, p0, L_1, L_2);
		int32_t L_3 = (__this->f2);
		__this->f2 = ((int32_t)(L_3+V_0));
		return;
	}
}
 void m16691 (t529 * __this, t29* p0, MethodInfo* method){
	t380  V_0 = {0};
	t29* V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t29* L_0 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28141_MI, p0);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0017;
		}

IL_0009:
		{
			t380  L_1 = (t380 )InterfaceFuncInvoker0< t380  >::Invoke(&m28142_MI, V_1);
			V_0 = L_1;
			VirtActionInvoker1< t380  >::Invoke(&m16688_MI, __this, V_0);
		}

IL_0017:
		{
			bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_2)
			{
				goto IL_0009;
			}
		}

IL_001f:
		{
			// IL_001f: leave.s IL_002c
			leaveInstructions[0] = 0x2C; // 1
			THROW_SENTINEL(IL_002c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0021;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0021;
	}

IL_0021:
	{ // begin finally (depth: 1)
		{
			if (V_1)
			{
				goto IL_0025;
			}
		}

IL_0024:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0025:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_1);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_002c:
	{
		return;
	}
}
extern MethodInfo m16692_MI;
 void m16692 (t529 * __this, t29* p0, MethodInfo* method){
	t29* V_0 = {0};
	{
		m16705(__this, p0, &m16705_MI);
		V_0 = ((t29*)IsInst(p0, InitializedTypeInfo(&t389_TI)));
		if (!V_0)
		{
			goto IL_001a;
		}
	}
	{
		m16690(__this, V_0, &m16690_MI);
		goto IL_0021;
	}

IL_001a:
	{
		m16691(__this, p0, &m16691_MI);
	}

IL_0021:
	{
		int32_t L_0 = (__this->f3);
		__this->f3 = ((int32_t)(L_0+1));
		return;
	}
}
extern MethodInfo m16693_MI;
 t3043 * m16693 (t529 * __this, MethodInfo* method){
	{
		t3043 * L_0 = (t3043 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3043_TI));
		m16725(L_0, __this, &m16725_MI);
		return L_0;
	}
}
extern MethodInfo m16694_MI;
 void m16694 (t529 * __this, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		t531* L_1 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		__this->f2 = 0;
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
 bool m16695 (t529 * __this, t380  p0, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m22703(NULL, L_0, p0, 0, L_1, &m22703_MI);
		return ((((int32_t)((((int32_t)L_2) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m16696_MI;
 void m16696 (t529 * __this, t531* p0, int32_t p1, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, (t20 *)(t20 *)p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m16697_MI;
 t380  m16697 (t529 * __this, t3044 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t380  V_1 = {0};
	t380  G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t529_TI));
		m16698(NULL, p0, &m16698_MI);
		int32_t L_0 = (__this->f2);
		int32_t L_1 = m16699(__this, 0, L_0, p0, &m16699_MI);
		V_0 = L_1;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		t531* L_2 = (__this->f1);
		int32_t L_3 = V_0;
		G_B3_0 = (*(t380 *)(t380 *)SZArrayLdElema(L_2, L_3));
		goto IL_0030;
	}

IL_0027:
	{
		Initobj (&t380_TI, (&V_1));
		G_B3_0 = V_1;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
 void m16698 (t29 * __this, t3044 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1033, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 int32_t m16699 (t529 * __this, int32_t p0, int32_t p1, t3044 * p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)(p0+p1));
		V_1 = p0;
		goto IL_0022;
	}

IL_0008:
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = V_1;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t380  >::Invoke(&m16800_MI, p2, (*(t380 *)(t380 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return V_1;
	}

IL_001e:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0022:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0008;
		}
	}
	{
		return (-1);
	}
}
 t3046  m16700 (t529 * __this, MethodInfo* method){
	{
		t3046  L_0 = {0};
		m16719(&L_0, __this, &m16719_MI);
		return L_0;
	}
}
 int32_t m16701 (t529 * __this, t380  p0, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m22703(NULL, L_0, p0, 0, L_1, &m22703_MI);
		return L_2;
	}
}
 void m16702 (t529 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		p0 = ((int32_t)(p0-p1));
	}

IL_000b:
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)p0) >= ((int32_t)L_0)))
		{
			goto IL_0031;
		}
	}
	{
		t531* L_1 = (__this->f1);
		t531* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_1, p0, (t20 *)(t20 *)L_2, ((int32_t)(p0+p1)), ((int32_t)(L_3-p0)), &m5952_MI);
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		__this->f2 = ((int32_t)(L_4+p1));
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		t531* L_5 = (__this->f1);
		int32_t L_6 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_5, L_6, ((-p1)), &m5112_MI);
	}

IL_0056:
	{
		return;
	}
}
 void m16703 (t529 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) <= ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		return;
	}
}
 void m16704 (t529 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		m16703(__this, p0, &m16703_MI);
		int32_t L_0 = (__this->f2);
		t531* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		m16689(__this, 1, &m16689_MI);
	}

IL_001e:
	{
		m16702(__this, p0, 1, &m16702_MI);
		t531* L_2 = (__this->f1);
		*((t380 *)(t380 *)SZArrayLdElema(L_2, p0)) = (t380 )p1;
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
 void m16705 (t529 * __this, t29* p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1177, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 bool m16706 (t529 * __this, t380  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t380  >::Invoke(&m16701_MI, __this, p0);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker1< int32_t >::Invoke(&m16708_MI, __this, V_0);
	}

IL_0013:
	{
		return ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m16707_MI;
 int32_t m16707 (t529 * __this, t3044 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t529_TI));
		m16698(NULL, p0, &m16698_MI);
		V_0 = 0;
		V_1 = 0;
		V_0 = 0;
		goto IL_0028;
	}

IL_000e:
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = V_0;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t380  >::Invoke(&m16800_MI, p0, (*(t380 *)(t380 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0031;
	}

IL_0024:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0028:
	{
		int32_t L_3 = (__this->f2);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_000e;
		}
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		if ((((uint32_t)V_0) != ((uint32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		return 0;
	}

IL_003c:
	{
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		V_1 = ((int32_t)(V_0+1));
		goto IL_0084;
	}

IL_0050:
	{
		t531* L_6 = (__this->f1);
		int32_t L_7 = V_1;
		bool L_8 = (bool)VirtFuncInvoker1< bool, t380  >::Invoke(&m16800_MI, p0, (*(t380 *)(t380 *)SZArrayLdElema(L_6, L_7)));
		if (L_8)
		{
			goto IL_0080;
		}
	}
	{
		t531* L_9 = (__this->f1);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)(L_10+1));
		t531* L_11 = (__this->f1);
		int32_t L_12 = V_1;
		*((t380 *)(t380 *)SZArrayLdElema(L_9, L_10)) = (t380 )(*(t380 *)(t380 *)SZArrayLdElema(L_11, L_12));
	}

IL_0080:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0084:
	{
		int32_t L_13 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)L_13)))
		{
			goto IL_0050;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_1-V_0))) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		t531* L_14 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_14, V_0, ((int32_t)(V_1-V_0)), &m5112_MI);
	}

IL_00a2:
	{
		__this->f2 = V_0;
		return ((int32_t)(V_1-V_0));
	}
}
 void m16708 (t529 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		m16702(__this, p0, (-1), &m16702_MI);
		t531* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_2, L_3, 1, &m5112_MI);
		int32_t L_4 = (__this->f3);
		__this->f3 = ((int32_t)(L_4+1));
		return;
	}
}
extern MethodInfo m16709_MI;
 void m16709 (t529 * __this, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5171(NULL, (t20 *)(t20 *)L_0, 0, L_1, &m5171_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m16710_MI;
 void m16710 (t529 * __this, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3050_TI));
		t3050 * L_2 = m16806(NULL, &m16806_MI);
		m22705(NULL, L_0, 0, L_1, L_2, &m22705_MI);
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
extern MethodInfo m16711_MI;
 void m16711 (t529 * __this, t3045 * p0, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m22713(NULL, L_0, L_1, p0, &m22713_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m16712_MI;
 t531* m16712 (t529 * __this, MethodInfo* method){
	t531* V_0 = {0};
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((t531*)SZArrayNew(InitializedTypeInfo(&t531_TI), L_0));
		t531* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		m5951(NULL, (t20 *)(t20 *)L_1, (t20 *)(t20 *)V_0, L_2, &m5951_MI);
		return V_0;
	}
}
extern MethodInfo m16713_MI;
 void m16713 (t529 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		m16715(__this, L_0, &m16715_MI);
		return;
	}
}
 int32_t m16714 (t529 * __this, MethodInfo* method){
	{
		t531* L_0 = (__this->f1);
		return (((int32_t)(((t20 *)L_0)->max_length)));
	}
}
 void m16715 (t529 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) >= ((uint32_t)L_0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m4198(L_1, &m4198_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t531** L_2 = &(__this->f1);
		m22701(NULL, L_2, p0, &m22701_MI);
		return;
	}
}
extern MethodInfo m16716_MI;
 int32_t m16716 (t529 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 t380  m16717 (t529 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		t531* L_2 = (__this->f1);
		int32_t L_3 = p0;
		return (*(t380 *)(t380 *)SZArrayLdElema(L_2, L_3));
	}
}
 void m16718 (t529 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		m16703(__this, p0, &m16703_MI);
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) != ((uint32_t)L_0)))
		{
			goto IL_001b;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001b:
	{
		t531* L_2 = (__this->f1);
		*((t380 *)(t380 *)SZArrayLdElema(L_2, p0)) = (t380 )p1;
		return;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UILineInfo>
extern Il2CppType t44_0_0_32849;
FieldInfo t529_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t529_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t531_0_0_1;
FieldInfo t529_f1_FieldInfo = 
{
	"_items", &t531_0_0_1, &t529_TI, offsetof(t529, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t529_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t529_TI, offsetof(t529, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t529_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t529_TI, offsetof(t529, f3), &EmptyCustomAttributesCache};
extern Il2CppType t531_0_0_49;
FieldInfo t529_f4_FieldInfo = 
{
	"EmptyArray", &t531_0_0_49, &t529_TI, offsetof(t529_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t529_FIs[] =
{
	&t529_f0_FieldInfo,
	&t529_f1_FieldInfo,
	&t529_f2_FieldInfo,
	&t529_f3_FieldInfo,
	&t529_f4_FieldInfo,
	NULL
};
static const int32_t t529_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t529_f0_DefaultValue = 
{
	&t529_f0_FieldInfo, { (char*)&t529_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t529_FDVs[] = 
{
	&t529_f0_DefaultValue,
	NULL
};
static PropertyInfo t529____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t529_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16681_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t529_TI, "System.Collections.ICollection.IsSynchronized", &m16682_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t529_TI, "System.Collections.ICollection.SyncRoot", &m16683_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t529_TI, "System.Collections.IList.IsFixedSize", &m16684_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t529_TI, "System.Collections.IList.IsReadOnly", &m16685_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____System_Collections_IList_Item_PropertyInfo = 
{
	&t529_TI, "System.Collections.IList.Item", &m16686_MI, &m16687_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____Capacity_PropertyInfo = 
{
	&t529_TI, "Capacity", &m16714_MI, &m16715_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____Count_PropertyInfo = 
{
	&t529_TI, "Count", &m16716_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t529____Item_PropertyInfo = 
{
	&t529_TI, "Item", &m16717_MI, &m16718_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t529_PIs[] =
{
	&t529____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t529____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t529____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t529____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t529____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t529____System_Collections_IList_Item_PropertyInfo,
	&t529____Capacity_PropertyInfo,
	&t529____Count_PropertyInfo,
	&t529____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16671_GM;
MethodInfo m16671_MI = 
{
	".ctor", (methodPointerType)&m16671, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16671_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m2905_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2905_GM;
MethodInfo m2905_MI = 
{
	".ctor", (methodPointerType)&m2905, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t529_m2905_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2905_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16672_GM;
MethodInfo m16672_MI = 
{
	".cctor", (methodPointerType)&m16672, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16672_GM};
extern Il2CppType t3041_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16673_GM;
MethodInfo m16673_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m16673, &t529_TI, &t3041_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16673_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16674_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16674_GM;
MethodInfo m16674_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m16674, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t529_m16674_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16674_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16675_GM;
MethodInfo m16675_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m16675, &t529_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16675_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t529_m16676_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16676_GM;
MethodInfo m16676_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m16676, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t529_m16676_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16676_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t529_m16677_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16677_GM;
MethodInfo m16677_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m16677, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t529_m16677_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16677_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t529_m16678_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16678_GM;
MethodInfo m16678_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m16678, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t529_m16678_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16678_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t529_m16679_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16679_GM;
MethodInfo m16679_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m16679, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t529_m16679_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16679_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t529_m16680_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16680_GM;
MethodInfo m16680_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m16680, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16680_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16680_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16681_GM;
MethodInfo m16681_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m16681, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16681_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16682_GM;
MethodInfo m16682_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m16682, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16682_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16683_GM;
MethodInfo m16683_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m16683, &t529_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16683_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16684_GM;
MethodInfo m16684_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m16684, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16684_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16685_GM;
MethodInfo m16685_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m16685, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16685_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16686_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16686_GM;
MethodInfo m16686_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m16686, &t529_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t529_m16686_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16686_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t529_m16687_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16687_GM;
MethodInfo m16687_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m16687, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t529_m16687_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16687_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t529_m16688_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16688_GM;
MethodInfo m16688_MI = 
{
	"Add", (methodPointerType)&m16688, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t380, t529_m16688_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16688_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16689_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16689_GM;
MethodInfo m16689_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m16689, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t529_m16689_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16689_GM};
extern Il2CppType t389_0_0_0;
extern Il2CppType t389_0_0_0;
static ParameterInfo t529_m16690_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t389_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16690_GM;
MethodInfo m16690_MI = 
{
	"AddCollection", (methodPointerType)&m16690, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16690_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16690_GM};
extern Il2CppType t3042_0_0_0;
extern Il2CppType t3042_0_0_0;
static ParameterInfo t529_m16691_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3042_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16691_GM;
MethodInfo m16691_MI = 
{
	"AddEnumerable", (methodPointerType)&m16691, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16691_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16691_GM};
extern Il2CppType t3042_0_0_0;
static ParameterInfo t529_m16692_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3042_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16692_GM;
MethodInfo m16692_MI = 
{
	"AddRange", (methodPointerType)&m16692, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16692_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16692_GM};
extern Il2CppType t3043_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16693_GM;
MethodInfo m16693_MI = 
{
	"AsReadOnly", (methodPointerType)&m16693, &t529_TI, &t3043_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16693_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16694_GM;
MethodInfo m16694_MI = 
{
	"Clear", (methodPointerType)&m16694, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16694_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t529_m16695_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16695_GM;
MethodInfo m16695_MI = 
{
	"Contains", (methodPointerType)&m16695, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t529_m16695_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16695_GM};
extern Il2CppType t531_0_0_0;
extern Il2CppType t531_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16696_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t531_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16696_GM;
MethodInfo m16696_MI = 
{
	"CopyTo", (methodPointerType)&m16696, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t529_m16696_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16696_GM};
extern Il2CppType t3044_0_0_0;
extern Il2CppType t3044_0_0_0;
static ParameterInfo t529_m16697_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3044_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16697_GM;
MethodInfo m16697_MI = 
{
	"Find", (methodPointerType)&m16697, &t529_TI, &t380_0_0_0, RuntimeInvoker_t380_t29, t529_m16697_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16697_GM};
extern Il2CppType t3044_0_0_0;
static ParameterInfo t529_m16698_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16698_GM;
MethodInfo m16698_MI = 
{
	"CheckMatch", (methodPointerType)&m16698, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16698_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16698_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3044_0_0_0;
static ParameterInfo t529_m16699_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t3044_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16699_GM;
MethodInfo m16699_MI = 
{
	"GetIndex", (methodPointerType)&m16699, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t529_m16699_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16699_GM};
extern Il2CppType t3046_0_0_0;
extern void* RuntimeInvoker_t3046 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16700_GM;
MethodInfo m16700_MI = 
{
	"GetEnumerator", (methodPointerType)&m16700, &t529_TI, &t3046_0_0_0, RuntimeInvoker_t3046, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16700_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t529_m16701_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16701_GM;
MethodInfo m16701_MI = 
{
	"IndexOf", (methodPointerType)&m16701, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t529_m16701_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16701_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16702_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16702_GM;
MethodInfo m16702_MI = 
{
	"Shift", (methodPointerType)&m16702, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t529_m16702_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16702_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16703_GM;
MethodInfo m16703_MI = 
{
	"CheckIndex", (methodPointerType)&m16703, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t529_m16703_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16703_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t529_m16704_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16704_GM;
MethodInfo m16704_MI = 
{
	"Insert", (methodPointerType)&m16704, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t529_m16704_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16704_GM};
extern Il2CppType t3042_0_0_0;
static ParameterInfo t529_m16705_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3042_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16705_GM;
MethodInfo m16705_MI = 
{
	"CheckCollection", (methodPointerType)&m16705, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16705_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16705_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t529_m16706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16706_GM;
MethodInfo m16706_MI = 
{
	"Remove", (methodPointerType)&m16706, &t529_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t529_m16706_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16706_GM};
extern Il2CppType t3044_0_0_0;
static ParameterInfo t529_m16707_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3044_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16707_GM;
MethodInfo m16707_MI = 
{
	"RemoveAll", (methodPointerType)&m16707, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t529_m16707_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16707_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16708_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16708_GM;
MethodInfo m16708_MI = 
{
	"RemoveAt", (methodPointerType)&m16708, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t529_m16708_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16708_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16709_GM;
MethodInfo m16709_MI = 
{
	"Reverse", (methodPointerType)&m16709, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16709_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16710_GM;
MethodInfo m16710_MI = 
{
	"Sort", (methodPointerType)&m16710, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16710_GM};
extern Il2CppType t3045_0_0_0;
extern Il2CppType t3045_0_0_0;
static ParameterInfo t529_m16711_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3045_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16711_GM;
MethodInfo m16711_MI = 
{
	"Sort", (methodPointerType)&m16711, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t529_m16711_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16711_GM};
extern Il2CppType t531_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16712_GM;
MethodInfo m16712_MI = 
{
	"ToArray", (methodPointerType)&m16712, &t529_TI, &t531_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16712_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16713_GM;
MethodInfo m16713_MI = 
{
	"TrimExcess", (methodPointerType)&m16713, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16713_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16714_GM;
MethodInfo m16714_MI = 
{
	"get_Capacity", (methodPointerType)&m16714, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16714_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16715_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16715_GM;
MethodInfo m16715_MI = 
{
	"set_Capacity", (methodPointerType)&m16715, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t529_m16715_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16715_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16716_GM;
MethodInfo m16716_MI = 
{
	"get_Count", (methodPointerType)&m16716, &t529_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16716_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t529_m16717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16717_GM;
MethodInfo m16717_MI = 
{
	"get_Item", (methodPointerType)&m16717, &t529_TI, &t380_0_0_0, RuntimeInvoker_t380_t44, t529_m16717_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16717_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t529_m16718_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16718_GM;
MethodInfo m16718_MI = 
{
	"set_Item", (methodPointerType)&m16718, &t529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t529_m16718_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16718_GM};
static MethodInfo* t529_MIs[] =
{
	&m16671_MI,
	&m2905_MI,
	&m16672_MI,
	&m16673_MI,
	&m16674_MI,
	&m16675_MI,
	&m16676_MI,
	&m16677_MI,
	&m16678_MI,
	&m16679_MI,
	&m16680_MI,
	&m16681_MI,
	&m16682_MI,
	&m16683_MI,
	&m16684_MI,
	&m16685_MI,
	&m16686_MI,
	&m16687_MI,
	&m16688_MI,
	&m16689_MI,
	&m16690_MI,
	&m16691_MI,
	&m16692_MI,
	&m16693_MI,
	&m16694_MI,
	&m16695_MI,
	&m16696_MI,
	&m16697_MI,
	&m16698_MI,
	&m16699_MI,
	&m16700_MI,
	&m16701_MI,
	&m16702_MI,
	&m16703_MI,
	&m16704_MI,
	&m16705_MI,
	&m16706_MI,
	&m16707_MI,
	&m16708_MI,
	&m16709_MI,
	&m16710_MI,
	&m16711_MI,
	&m16712_MI,
	&m16713_MI,
	&m16714_MI,
	&m16715_MI,
	&m16716_MI,
	&m16717_MI,
	&m16718_MI,
	NULL
};
static MethodInfo* t529_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16675_MI,
	&m16716_MI,
	&m16682_MI,
	&m16683_MI,
	&m16674_MI,
	&m16684_MI,
	&m16685_MI,
	&m16686_MI,
	&m16687_MI,
	&m16676_MI,
	&m16694_MI,
	&m16677_MI,
	&m16678_MI,
	&m16679_MI,
	&m16680_MI,
	&m16708_MI,
	&m16716_MI,
	&m16681_MI,
	&m16688_MI,
	&m16694_MI,
	&m16695_MI,
	&m16696_MI,
	&m16706_MI,
	&m16673_MI,
	&m16701_MI,
	&m16704_MI,
	&m16708_MI,
	&m16717_MI,
	&m16718_MI,
};
extern TypeInfo t387_TI;
static TypeInfo* t529_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t389_TI,
	&t3042_TI,
	&t387_TI,
};
static Il2CppInterfaceOffsetPair t529_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t389_TI, 20},
	{ &t3042_TI, 27},
	{ &t387_TI, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t529_0_0_0;
extern Il2CppType t529_1_0_0;
struct t529;
extern Il2CppGenericClass t529_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t529_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t529_MIs, t529_PIs, t529_FIs, NULL, &t29_TI, NULL, NULL, &t529_TI, t529_ITIs, t529_VT, &t1261__CustomAttributeCache, &t529_TI, &t529_0_0_0, &t529_1_0_0, t529_IOs, &t529_GC, NULL, t529_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t529), 0, -1, sizeof(t529_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m16722_MI;


 void m16719 (t3046 * __this, t529 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f3);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m16720_MI;
 t29 * m16720 (t3046 * __this, MethodInfo* method){
	{
		m16722(__this, &m16722_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		t380  L_2 = (__this->f3);
		t380  L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t380_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m16721_MI;
 void m16721 (t3046 * __this, MethodInfo* method){
	{
		__this->f0 = (t529 *)NULL;
		return;
	}
}
 void m16722 (t3046 * __this, MethodInfo* method){
	{
		t529 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		t3046  L_1 = (*(t3046 *)__this);
		t29 * L_2 = Box(InitializedTypeInfo(&t3046_TI), &L_1);
		t42 * L_3 = m1430(L_2, &m1430_MI);
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_3);
		t1101 * L_5 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_5, L_4, &m5150_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0023:
	{
		int32_t L_6 = (__this->f2);
		t529 * L_7 = (__this->f0);
		int32_t L_8 = (L_7->f3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0041;
		}
	}
	{
		t914 * L_9 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_9, (t7*) &_stringLiteral1178, &m3964_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0041:
	{
		return;
	}
}
extern MethodInfo m16723_MI;
 bool m16723 (t3046 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m16722(__this, &m16722_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_1 = (__this->f1);
		t529 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_004d;
		}
	}
	{
		t529 * L_4 = (__this->f0);
		t531* L_5 = (L_4->f1);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		int32_t L_8 = V_0;
		__this->f3 = (*(t380 *)(t380 *)SZArrayLdElema(L_5, L_8));
		return 1;
	}

IL_004d:
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m16724_MI;
 t380  m16724 (t3046 * __this, MethodInfo* method){
	{
		t380  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
extern Il2CppType t529_0_0_1;
FieldInfo t3046_f0_FieldInfo = 
{
	"l", &t529_0_0_1, &t3046_TI, offsetof(t3046, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3046_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3046_TI, offsetof(t3046, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3046_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t3046_TI, offsetof(t3046, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t380_0_0_1;
FieldInfo t3046_f3_FieldInfo = 
{
	"current", &t380_0_0_1, &t3046_TI, offsetof(t3046, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3046_FIs[] =
{
	&t3046_f0_FieldInfo,
	&t3046_f1_FieldInfo,
	&t3046_f2_FieldInfo,
	&t3046_f3_FieldInfo,
	NULL
};
static PropertyInfo t3046____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3046_TI, "System.Collections.IEnumerator.Current", &m16720_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3046____Current_PropertyInfo = 
{
	&t3046_TI, "Current", &m16724_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3046_PIs[] =
{
	&t3046____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3046____Current_PropertyInfo,
	NULL
};
extern Il2CppType t529_0_0_0;
static ParameterInfo t3046_m16719_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t529_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16719_GM;
MethodInfo m16719_MI = 
{
	".ctor", (methodPointerType)&m16719, &t3046_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3046_m16719_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16719_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16720_GM;
MethodInfo m16720_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16720, &t3046_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16720_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16721_GM;
MethodInfo m16721_MI = 
{
	"Dispose", (methodPointerType)&m16721, &t3046_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16721_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16722_GM;
MethodInfo m16722_MI = 
{
	"VerifyState", (methodPointerType)&m16722, &t3046_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16722_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16723_GM;
MethodInfo m16723_MI = 
{
	"MoveNext", (methodPointerType)&m16723, &t3046_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16723_GM};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16724_GM;
MethodInfo m16724_MI = 
{
	"get_Current", (methodPointerType)&m16724, &t3046_TI, &t380_0_0_0, RuntimeInvoker_t380, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16724_GM};
static MethodInfo* t3046_MIs[] =
{
	&m16719_MI,
	&m16720_MI,
	&m16721_MI,
	&m16722_MI,
	&m16723_MI,
	&m16724_MI,
	NULL
};
static MethodInfo* t3046_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16720_MI,
	&m16723_MI,
	&m16721_MI,
	&m16724_MI,
};
static TypeInfo* t3046_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3041_TI,
};
static Il2CppInterfaceOffsetPair t3046_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3041_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3046_0_0_0;
extern Il2CppType t3046_1_0_0;
extern Il2CppGenericClass t3046_GC;
TypeInfo t3046_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3046_MIs, t3046_PIs, t3046_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t3046_TI, t3046_ITIs, t3046_VT, &EmptyCustomAttributesCache, &t3046_TI, &t3046_0_0_0, &t3046_1_0_0, t3046_IOs, &t3046_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3046)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t3047MD.h"
extern MethodInfo m16754_MI;
extern MethodInfo m1749_MI;
extern MethodInfo m16786_MI;
extern MethodInfo m28138_MI;
extern MethodInfo m28132_MI;


 void m16725 (t3043 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1179, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m16726_MI;
 void m16726 (t3043 * __this, t380  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16727_MI;
 void m16727 (t3043 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16728_MI;
 void m16728 (t3043 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16729_MI;
 bool m16729 (t3043 * __this, t380  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16730_MI;
 void m16730 (t3043 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16731_MI;
 t380  m16731 (t3043 * __this, int32_t p0, MethodInfo* method){
	{
		t380  L_0 = (t380 )VirtFuncInvoker1< t380 , int32_t >::Invoke(&m16754_MI, __this, p0);
		return L_0;
	}
}
extern MethodInfo m16732_MI;
 void m16732 (t3043 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16733_MI;
 bool m16733 (t3043 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m16734_MI;
 void m16734 (t3043 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m16735_MI;
 t29 * m16735 (t3043 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16736_MI;
 int32_t m16736 (t3043 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16737_MI;
 void m16737 (t3043 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16738_MI;
 bool m16738 (t3043 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16786(NULL, p0, &m16786_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t380  >::Invoke(&m28138_MI, L_1, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m16739_MI;
 int32_t m16739 (t3043 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16786(NULL, p0, &m16786_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t380  >::Invoke(&m28132_MI, L_1, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m16740_MI;
 void m16740 (t3043 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16741_MI;
 void m16741 (t3043 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16742_MI;
 void m16742 (t3043 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16743_MI;
 bool m16743 (t3043 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m16744_MI;
 t29 * m16744 (t3043 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m16745_MI;
 bool m16745 (t3043 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m16746_MI;
 bool m16746 (t3043 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m16747_MI;
 t29 * m16747 (t3043 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t380  L_1 = (t380 )InterfaceFuncInvoker1< t380 , int32_t >::Invoke(&m1749_MI, L_0, p0);
		t380  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t380_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m16748_MI;
 void m16748 (t3043 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m16749_MI;
 bool m16749 (t3043 * __this, t380  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t380  >::Invoke(&m28138_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16750_MI;
 void m16750 (t3043 * __this, t531* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t531*, int32_t >::Invoke(&m28139_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m16751_MI;
 t29* m16751 (t3043 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28141_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16752_MI;
 int32_t m16752 (t3043 * __this, t380  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t380  >::Invoke(&m28132_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16753_MI;
 int32_t m16753 (t3043 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1775_MI, L_0);
		return L_1;
	}
}
 t380  m16754 (t3043 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t380  L_1 = (t380 )InterfaceFuncInvoker1< t380 , int32_t >::Invoke(&m1749_MI, L_0, p0);
		return L_1;
	}
}
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
extern Il2CppType t387_0_0_1;
FieldInfo t3043_f0_FieldInfo = 
{
	"list", &t387_0_0_1, &t3043_TI, offsetof(t3043, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3043_FIs[] =
{
	&t3043_f0_FieldInfo,
	NULL
};
static PropertyInfo t3043____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3043_TI, "System.Collections.Generic.IList<T>.Item", &m16731_MI, &m16732_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3043_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16733_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3043_TI, "System.Collections.ICollection.IsSynchronized", &m16743_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3043_TI, "System.Collections.ICollection.SyncRoot", &m16744_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3043_TI, "System.Collections.IList.IsFixedSize", &m16745_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3043_TI, "System.Collections.IList.IsReadOnly", &m16746_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____System_Collections_IList_Item_PropertyInfo = 
{
	&t3043_TI, "System.Collections.IList.Item", &m16747_MI, &m16748_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____Count_PropertyInfo = 
{
	&t3043_TI, "Count", &m16753_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3043____Item_PropertyInfo = 
{
	&t3043_TI, "Item", &m16754_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3043_PIs[] =
{
	&t3043____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3043____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3043____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3043____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3043____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3043____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3043____System_Collections_IList_Item_PropertyInfo,
	&t3043____Count_PropertyInfo,
	&t3043____Item_PropertyInfo,
	NULL
};
extern Il2CppType t387_0_0_0;
extern Il2CppType t387_0_0_0;
static ParameterInfo t3043_m16725_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t387_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16725_GM;
MethodInfo m16725_MI = 
{
	".ctor", (methodPointerType)&m16725, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3043_m16725_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16725_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3043_m16726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16726_GM;
MethodInfo m16726_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m16726, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t380, t3043_m16726_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16726_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16727_GM;
MethodInfo m16727_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m16727, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16727_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3043_m16728_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16728_GM;
MethodInfo m16728_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m16728, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t3043_m16728_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16728_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3043_m16729_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16729_GM;
MethodInfo m16729_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m16729, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t3043_m16729_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16729_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16730_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16730_GM;
MethodInfo m16730_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m16730, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3043_m16730_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16730_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16731_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16731_GM;
MethodInfo m16731_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m16731, &t3043_TI, &t380_0_0_0, RuntimeInvoker_t380_t44, t3043_m16731_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16731_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3043_m16732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16732_GM;
MethodInfo m16732_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m16732, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t3043_m16732_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16732_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16733_GM;
MethodInfo m16733_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m16733, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16733_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16734_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16734_GM;
MethodInfo m16734_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m16734, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3043_m16734_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16734_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16735_GM;
MethodInfo m16735_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m16735, &t3043_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16735_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3043_m16736_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16736_GM;
MethodInfo m16736_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m16736, &t3043_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3043_m16736_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16736_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16737_GM;
MethodInfo m16737_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m16737, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16737_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3043_m16738_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16738_GM;
MethodInfo m16738_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m16738, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3043_m16738_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16738_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3043_m16739_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16739_GM;
MethodInfo m16739_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m16739, &t3043_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3043_m16739_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16739_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3043_m16740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16740_GM;
MethodInfo m16740_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m16740, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3043_m16740_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16740_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3043_m16741_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16741_GM;
MethodInfo m16741_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m16741, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3043_m16741_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16741_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16742_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16742_GM;
MethodInfo m16742_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m16742, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3043_m16742_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16742_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16743_GM;
MethodInfo m16743_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m16743, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16743_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16744_GM;
MethodInfo m16744_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m16744, &t3043_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16744_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16745_GM;
MethodInfo m16745_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m16745, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16745_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16746_GM;
MethodInfo m16746_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m16746, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16746_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16747_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16747_GM;
MethodInfo m16747_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m16747, &t3043_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3043_m16747_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16747_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3043_m16748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16748_GM;
MethodInfo m16748_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m16748, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3043_m16748_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16748_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3043_m16749_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16749_GM;
MethodInfo m16749_MI = 
{
	"Contains", (methodPointerType)&m16749, &t3043_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t3043_m16749_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16749_GM};
extern Il2CppType t531_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16750_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t531_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16750_GM;
MethodInfo m16750_MI = 
{
	"CopyTo", (methodPointerType)&m16750, &t3043_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3043_m16750_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16750_GM};
extern Il2CppType t3041_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16751_GM;
MethodInfo m16751_MI = 
{
	"GetEnumerator", (methodPointerType)&m16751, &t3043_TI, &t3041_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16751_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3043_m16752_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16752_GM;
MethodInfo m16752_MI = 
{
	"IndexOf", (methodPointerType)&m16752, &t3043_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t3043_m16752_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16752_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16753_GM;
MethodInfo m16753_MI = 
{
	"get_Count", (methodPointerType)&m16753, &t3043_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16753_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3043_m16754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16754_GM;
MethodInfo m16754_MI = 
{
	"get_Item", (methodPointerType)&m16754, &t3043_TI, &t380_0_0_0, RuntimeInvoker_t380_t44, t3043_m16754_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16754_GM};
static MethodInfo* t3043_MIs[] =
{
	&m16725_MI,
	&m16726_MI,
	&m16727_MI,
	&m16728_MI,
	&m16729_MI,
	&m16730_MI,
	&m16731_MI,
	&m16732_MI,
	&m16733_MI,
	&m16734_MI,
	&m16735_MI,
	&m16736_MI,
	&m16737_MI,
	&m16738_MI,
	&m16739_MI,
	&m16740_MI,
	&m16741_MI,
	&m16742_MI,
	&m16743_MI,
	&m16744_MI,
	&m16745_MI,
	&m16746_MI,
	&m16747_MI,
	&m16748_MI,
	&m16749_MI,
	&m16750_MI,
	&m16751_MI,
	&m16752_MI,
	&m16753_MI,
	&m16754_MI,
	NULL
};
static MethodInfo* t3043_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16735_MI,
	&m16753_MI,
	&m16743_MI,
	&m16744_MI,
	&m16734_MI,
	&m16745_MI,
	&m16746_MI,
	&m16747_MI,
	&m16748_MI,
	&m16736_MI,
	&m16737_MI,
	&m16738_MI,
	&m16739_MI,
	&m16740_MI,
	&m16741_MI,
	&m16742_MI,
	&m16753_MI,
	&m16733_MI,
	&m16726_MI,
	&m16727_MI,
	&m16749_MI,
	&m16750_MI,
	&m16729_MI,
	&m16752_MI,
	&m16728_MI,
	&m16730_MI,
	&m16731_MI,
	&m16732_MI,
	&m16751_MI,
	&m16754_MI,
};
static TypeInfo* t3043_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t389_TI,
	&t387_TI,
	&t3042_TI,
};
static Il2CppInterfaceOffsetPair t3043_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t389_TI, 20},
	{ &t387_TI, 27},
	{ &t3042_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3043_0_0_0;
extern Il2CppType t3043_1_0_0;
struct t3043;
extern Il2CppGenericClass t3043_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3043_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3043_MIs, t3043_PIs, t3043_FIs, NULL, &t29_TI, NULL, NULL, &t3043_TI, t3043_ITIs, t3043_VT, &t1263__CustomAttributeCache, &t3043_TI, &t3043_0_0_0, &t3043_1_0_0, t3043_IOs, &t3043_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3043), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3047.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3047_TI;

extern MethodInfo m28135_MI;
extern MethodInfo m16789_MI;
extern MethodInfo m16790_MI;
extern MethodInfo m16787_MI;
extern MethodInfo m16785_MI;
extern MethodInfo m16778_MI;
extern MethodInfo m16788_MI;
extern MethodInfo m16776_MI;
extern MethodInfo m16781_MI;
extern MethodInfo m16772_MI;
extern MethodInfo m28137_MI;
extern MethodInfo m28133_MI;
extern MethodInfo m28134_MI;
extern MethodInfo m28131_MI;


extern MethodInfo m16755_MI;
 void m16755 (t3047 * __this, MethodInfo* method){
	t529 * V_0 = {0};
	t29 * V_1 = {0};
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t529_TI));
		t529 * L_0 = (t529 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t529_TI));
		m16671(L_0, &m16671_MI);
		V_0 = L_0;
		V_1 = V_0;
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, V_1);
		__this->f1 = L_1;
		__this->f0 = V_0;
		return;
	}
}
extern MethodInfo m16756_MI;
 bool m16756 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m28135_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16757_MI;
 void m16757 (t3047 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m16758_MI;
 t29 * m16758 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28141_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16759_MI;
 int32_t m16759 (t3047 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1775_MI, L_0);
		V_0 = L_1;
		t380  L_2 = m16787(NULL, p0, &m16787_MI);
		VirtActionInvoker2< int32_t, t380  >::Invoke(&m16778_MI, __this, V_0, L_2);
		return V_0;
	}
}
extern MethodInfo m16760_MI;
 bool m16760 (t3047 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16786(NULL, p0, &m16786_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t380  >::Invoke(&m28138_MI, L_1, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m16761_MI;
 int32_t m16761 (t3047 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16786(NULL, p0, &m16786_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t380  >::Invoke(&m28132_MI, L_1, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m16762_MI;
 void m16762 (t3047 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t380  L_0 = m16787(NULL, p1, &m16787_MI);
		VirtActionInvoker2< int32_t, t380  >::Invoke(&m16778_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m16763_MI;
 void m16763 (t3047 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		m16788(NULL, L_0, &m16788_MI);
		t380  L_1 = m16787(NULL, p0, &m16787_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t380  >::Invoke(&m16776_MI, __this, L_1);
		V_0 = L_2;
		VirtActionInvoker1< int32_t >::Invoke(&m16781_MI, __this, V_0);
		return;
	}
}
extern MethodInfo m16764_MI;
 bool m16764 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m16789(NULL, L_0, &m16789_MI);
		return L_1;
	}
}
extern MethodInfo m16765_MI;
 t29 * m16765 (t3047 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m16766_MI;
 bool m16766 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m16790(NULL, L_0, &m16790_MI);
		return L_1;
	}
}
extern MethodInfo m16767_MI;
 bool m16767 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m28135_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16768_MI;
 t29 * m16768 (t3047 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t380  L_1 = (t380 )InterfaceFuncInvoker1< t380 , int32_t >::Invoke(&m1749_MI, L_0, p0);
		t380  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t380_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m16769_MI;
 void m16769 (t3047 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t380  L_0 = m16787(NULL, p1, &m16787_MI);
		VirtActionInvoker2< int32_t, t380  >::Invoke(&m16785_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m16770_MI;
 void m16770 (t3047 * __this, t380  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1775_MI, L_0);
		V_0 = L_1;
		VirtActionInvoker2< int32_t, t380  >::Invoke(&m16778_MI, __this, V_0, p0);
		return;
	}
}
extern MethodInfo m16771_MI;
 void m16771 (t3047 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m16772_MI, __this);
		return;
	}
}
 void m16772 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker0::Invoke(&m28137_MI, L_0);
		return;
	}
}
extern MethodInfo m16773_MI;
 bool m16773 (t3047 * __this, t380  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t380  >::Invoke(&m28138_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16774_MI;
 void m16774 (t3047 * __this, t531* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t531*, int32_t >::Invoke(&m28139_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m16775_MI;
 t29* m16775 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m28141_MI, L_0);
		return L_1;
	}
}
 int32_t m16776 (t3047 * __this, t380  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t380  >::Invoke(&m28132_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16777_MI;
 void m16777 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t380  >::Invoke(&m16778_MI, __this, p0, p1);
		return;
	}
}
 void m16778 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t380  >::Invoke(&m28133_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m16779_MI;
 bool m16779 (t3047 * __this, t380  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t380  >::Invoke(&m16776_MI, __this, p0);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		VirtActionInvoker1< int32_t >::Invoke(&m16781_MI, __this, V_0);
		return 1;
	}
}
extern MethodInfo m16780_MI;
 void m16780 (t3047 * __this, int32_t p0, MethodInfo* method){
	{
		VirtActionInvoker1< int32_t >::Invoke(&m16781_MI, __this, p0);
		return;
	}
}
 void m16781 (t3047 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker1< int32_t >::Invoke(&m28134_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m16782_MI;
 int32_t m16782 (t3047 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1775_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m16783_MI;
 t380  m16783 (t3047 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t380  L_1 = (t380 )InterfaceFuncInvoker1< t380 , int32_t >::Invoke(&m1749_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m16784_MI;
 void m16784 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t380  >::Invoke(&m16785_MI, __this, p0, p1);
		return;
	}
}
 void m16785 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t380  >::Invoke(&m28131_MI, L_0, p0, p1);
		return;
	}
}
 bool m16786 (t29 * __this, t29 * p0, MethodInfo* method){
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t380_TI))))
		{
			goto IL_0022;
		}
	}
	{
		if (p0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t380_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		G_B4_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
	}

IL_0020:
	{
		G_B6_0 = G_B4_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		return G_B6_0;
	}
}
 t380  m16787 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m16786(NULL, p0, &m16786_MI);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		return ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI)))));
	}

IL_000f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 void m16788 (t29 * __this, t29* p0, MethodInfo* method){
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m28135_MI, p0);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		t345 * L_1 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_1, &m1516_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
 bool m16789 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t674_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9815_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
 bool m16790 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t868_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9817_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
extern Il2CppType t387_0_0_1;
FieldInfo t3047_f0_FieldInfo = 
{
	"list", &t387_0_0_1, &t3047_TI, offsetof(t3047, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3047_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3047_TI, offsetof(t3047, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3047_FIs[] =
{
	&t3047_f0_FieldInfo,
	&t3047_f1_FieldInfo,
	NULL
};
static PropertyInfo t3047____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3047_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16756_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3047_TI, "System.Collections.ICollection.IsSynchronized", &m16764_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3047_TI, "System.Collections.ICollection.SyncRoot", &m16765_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3047_TI, "System.Collections.IList.IsFixedSize", &m16766_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3047_TI, "System.Collections.IList.IsReadOnly", &m16767_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____System_Collections_IList_Item_PropertyInfo = 
{
	&t3047_TI, "System.Collections.IList.Item", &m16768_MI, &m16769_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____Count_PropertyInfo = 
{
	&t3047_TI, "Count", &m16782_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3047____Item_PropertyInfo = 
{
	&t3047_TI, "Item", &m16783_MI, &m16784_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3047_PIs[] =
{
	&t3047____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3047____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3047____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3047____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3047____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3047____System_Collections_IList_Item_PropertyInfo,
	&t3047____Count_PropertyInfo,
	&t3047____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16755_GM;
MethodInfo m16755_MI = 
{
	".ctor", (methodPointerType)&m16755, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16755_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16756_GM;
MethodInfo m16756_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m16756, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16756_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3047_m16757_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16757_GM;
MethodInfo m16757_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m16757, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3047_m16757_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16757_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16758_GM;
MethodInfo m16758_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m16758, &t3047_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16758_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16759_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16759_GM;
MethodInfo m16759_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m16759, &t3047_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3047_m16759_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16759_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16760_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16760_GM;
MethodInfo m16760_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m16760, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3047_m16760_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16760_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16761_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16761_GM;
MethodInfo m16761_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m16761, &t3047_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3047_m16761_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16761_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16762_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16762_GM;
MethodInfo m16762_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m16762, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3047_m16762_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16762_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16763_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16763_GM;
MethodInfo m16763_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m16763, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3047_m16763_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16763_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16764_GM;
MethodInfo m16764_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m16764, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16764_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16765_GM;
MethodInfo m16765_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m16765, &t3047_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16765_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16766_GM;
MethodInfo m16766_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m16766, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16766_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16767_GM;
MethodInfo m16767_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m16767, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16767_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3047_m16768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16768_GM;
MethodInfo m16768_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m16768, &t3047_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3047_m16768_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16768_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16769_GM;
MethodInfo m16769_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m16769, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3047_m16769_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16769_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16770_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16770_GM;
MethodInfo m16770_MI = 
{
	"Add", (methodPointerType)&m16770, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t380, t3047_m16770_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16770_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16771_GM;
MethodInfo m16771_MI = 
{
	"Clear", (methodPointerType)&m16771, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16771_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16772_GM;
MethodInfo m16772_MI = 
{
	"ClearItems", (methodPointerType)&m16772, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16772_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16773_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16773_GM;
MethodInfo m16773_MI = 
{
	"Contains", (methodPointerType)&m16773, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t3047_m16773_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16773_GM};
extern Il2CppType t531_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3047_m16774_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t531_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16774_GM;
MethodInfo m16774_MI = 
{
	"CopyTo", (methodPointerType)&m16774, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3047_m16774_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16774_GM};
extern Il2CppType t3041_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16775_GM;
MethodInfo m16775_MI = 
{
	"GetEnumerator", (methodPointerType)&m16775, &t3047_TI, &t3041_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16775_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16776_GM;
MethodInfo m16776_MI = 
{
	"IndexOf", (methodPointerType)&m16776, &t3047_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t3047_m16776_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16776_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16777_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16777_GM;
MethodInfo m16777_MI = 
{
	"Insert", (methodPointerType)&m16777, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t3047_m16777_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16777_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16778_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16778_GM;
MethodInfo m16778_MI = 
{
	"InsertItem", (methodPointerType)&m16778, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t3047_m16778_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16778_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16779_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16779_GM;
MethodInfo m16779_MI = 
{
	"Remove", (methodPointerType)&m16779, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t3047_m16779_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16779_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3047_m16780_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16780_GM;
MethodInfo m16780_MI = 
{
	"RemoveAt", (methodPointerType)&m16780, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3047_m16780_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16780_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3047_m16781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16781_GM;
MethodInfo m16781_MI = 
{
	"RemoveItem", (methodPointerType)&m16781, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3047_m16781_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16781_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16782_GM;
MethodInfo m16782_MI = 
{
	"get_Count", (methodPointerType)&m16782, &t3047_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16782_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3047_m16783_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16783_GM;
MethodInfo m16783_MI = 
{
	"get_Item", (methodPointerType)&m16783, &t3047_TI, &t380_0_0_0, RuntimeInvoker_t380_t44, t3047_m16783_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16783_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16784_GM;
MethodInfo m16784_MI = 
{
	"set_Item", (methodPointerType)&m16784, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t3047_m16784_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16784_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3047_m16785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16785_GM;
MethodInfo m16785_MI = 
{
	"SetItem", (methodPointerType)&m16785, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t3047_m16785_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16785_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16786_GM;
MethodInfo m16786_MI = 
{
	"IsValidItem", (methodPointerType)&m16786, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3047_m16786_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16786_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3047_m16787_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16787_GM;
MethodInfo m16787_MI = 
{
	"ConvertItem", (methodPointerType)&m16787, &t3047_TI, &t380_0_0_0, RuntimeInvoker_t380_t29, t3047_m16787_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16787_GM};
extern Il2CppType t387_0_0_0;
static ParameterInfo t3047_m16788_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t387_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16788_GM;
MethodInfo m16788_MI = 
{
	"CheckWritable", (methodPointerType)&m16788, &t3047_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3047_m16788_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16788_GM};
extern Il2CppType t387_0_0_0;
static ParameterInfo t3047_m16789_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t387_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16789_GM;
MethodInfo m16789_MI = 
{
	"IsSynchronized", (methodPointerType)&m16789, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3047_m16789_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16789_GM};
extern Il2CppType t387_0_0_0;
static ParameterInfo t3047_m16790_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t387_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16790_GM;
MethodInfo m16790_MI = 
{
	"IsFixedSize", (methodPointerType)&m16790, &t3047_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3047_m16790_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16790_GM};
static MethodInfo* t3047_MIs[] =
{
	&m16755_MI,
	&m16756_MI,
	&m16757_MI,
	&m16758_MI,
	&m16759_MI,
	&m16760_MI,
	&m16761_MI,
	&m16762_MI,
	&m16763_MI,
	&m16764_MI,
	&m16765_MI,
	&m16766_MI,
	&m16767_MI,
	&m16768_MI,
	&m16769_MI,
	&m16770_MI,
	&m16771_MI,
	&m16772_MI,
	&m16773_MI,
	&m16774_MI,
	&m16775_MI,
	&m16776_MI,
	&m16777_MI,
	&m16778_MI,
	&m16779_MI,
	&m16780_MI,
	&m16781_MI,
	&m16782_MI,
	&m16783_MI,
	&m16784_MI,
	&m16785_MI,
	&m16786_MI,
	&m16787_MI,
	&m16788_MI,
	&m16789_MI,
	&m16790_MI,
	NULL
};
static MethodInfo* t3047_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16758_MI,
	&m16782_MI,
	&m16764_MI,
	&m16765_MI,
	&m16757_MI,
	&m16766_MI,
	&m16767_MI,
	&m16768_MI,
	&m16769_MI,
	&m16759_MI,
	&m16771_MI,
	&m16760_MI,
	&m16761_MI,
	&m16762_MI,
	&m16763_MI,
	&m16780_MI,
	&m16782_MI,
	&m16756_MI,
	&m16770_MI,
	&m16771_MI,
	&m16773_MI,
	&m16774_MI,
	&m16779_MI,
	&m16776_MI,
	&m16777_MI,
	&m16780_MI,
	&m16783_MI,
	&m16784_MI,
	&m16775_MI,
	&m16772_MI,
	&m16778_MI,
	&m16781_MI,
	&m16785_MI,
};
static TypeInfo* t3047_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t389_TI,
	&t387_TI,
	&t3042_TI,
};
static Il2CppInterfaceOffsetPair t3047_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t389_TI, 20},
	{ &t387_TI, 27},
	{ &t3042_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3047_0_0_0;
extern Il2CppType t3047_1_0_0;
struct t3047;
extern Il2CppGenericClass t3047_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3047_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3047_MIs, t3047_PIs, t3047_FIs, NULL, &t29_TI, NULL, NULL, &t3047_TI, t3047_ITIs, t3047_VT, &t1262__CustomAttributeCache, &t3047_TI, &t3047_0_0_0, &t3047_1_0_0, t3047_IOs, &t3047_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3047), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3048_TI;
#include "t3048MD.h"

#include "t3049.h"
extern TypeInfo t6738_TI;
extern TypeInfo t3049_TI;
#include "t3049MD.h"
extern Il2CppType t6738_0_0_0;
extern MethodInfo m16796_MI;
extern MethodInfo m29837_MI;
extern MethodInfo m22702_MI;


extern MethodInfo m16791_MI;
 void m16791 (t3048 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m16792_MI;
 void m16792 (t29 * __this, MethodInfo* method){
	t3049 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3049 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3049_TI));
	m16796(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m16796_MI);
	((t3048_SFs*)InitializedTypeInfo(&t3048_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m16793_MI;
 int32_t m16793 (t3048 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t380  >::Invoke(&m29837_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))));
		return L_0;
	}
}
extern MethodInfo m16794_MI;
 bool m16794 (t3048 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t380 , t380  >::Invoke(&m22702_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))), ((*(t380 *)((t380 *)UnBox (p1, InitializedTypeInfo(&t380_TI))))));
		return L_0;
	}
}
extern MethodInfo m16795_MI;
 t3048 * m16795 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3048_TI));
		return (((t3048_SFs*)InitializedTypeInfo(&t3048_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
extern Il2CppType t3048_0_0_49;
FieldInfo t3048_f0_FieldInfo = 
{
	"_default", &t3048_0_0_49, &t3048_TI, offsetof(t3048_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3048_FIs[] =
{
	&t3048_f0_FieldInfo,
	NULL
};
static PropertyInfo t3048____Default_PropertyInfo = 
{
	&t3048_TI, "Default", &m16795_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3048_PIs[] =
{
	&t3048____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16791_GM;
MethodInfo m16791_MI = 
{
	".ctor", (methodPointerType)&m16791, &t3048_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16791_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16792_GM;
MethodInfo m16792_MI = 
{
	".cctor", (methodPointerType)&m16792, &t3048_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16792_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3048_m16793_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16793_GM;
MethodInfo m16793_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m16793, &t3048_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3048_m16793_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16793_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3048_m16794_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16794_GM;
MethodInfo m16794_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m16794, &t3048_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3048_m16794_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16794_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3048_m29837_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29837_GM;
MethodInfo m29837_MI = 
{
	"GetHashCode", NULL, &t3048_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t3048_m29837_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29837_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3048_m22702_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22702_GM;
MethodInfo m22702_MI = 
{
	"Equals", NULL, &t3048_TI, &t40_0_0_0, RuntimeInvoker_t40_t380_t380, t3048_m22702_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22702_GM};
extern Il2CppType t3048_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16795_GM;
MethodInfo m16795_MI = 
{
	"get_Default", (methodPointerType)&m16795, &t3048_TI, &t3048_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16795_GM};
static MethodInfo* t3048_MIs[] =
{
	&m16791_MI,
	&m16792_MI,
	&m16793_MI,
	&m16794_MI,
	&m29837_MI,
	&m22702_MI,
	&m16795_MI,
	NULL
};
static MethodInfo* t3048_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m22702_MI,
	&m29837_MI,
	&m16794_MI,
	&m16793_MI,
	NULL,
	NULL,
};
extern TypeInfo t6739_TI;
static TypeInfo* t3048_ITIs[] = 
{
	&t6739_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3048_IOs[] = 
{
	{ &t6739_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3048_0_0_0;
extern Il2CppType t3048_1_0_0;
struct t3048;
extern Il2CppGenericClass t3048_GC;
TypeInfo t3048_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3048_MIs, t3048_PIs, t3048_FIs, NULL, &t29_TI, NULL, NULL, &t3048_TI, t3048_ITIs, t3048_VT, &EmptyCustomAttributesCache, &t3048_TI, &t3048_0_0_0, &t3048_1_0_0, t3048_IOs, &t3048_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3048), 0, -1, sizeof(t3048_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UILineInfo>
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t6739_m29838_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29838_GM;
MethodInfo m29838_MI = 
{
	"Equals", NULL, &t6739_TI, &t40_0_0_0, RuntimeInvoker_t40_t380_t380, t6739_m29838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29838_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t6739_m29839_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29839_GM;
MethodInfo m29839_MI = 
{
	"GetHashCode", NULL, &t6739_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t6739_m29839_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29839_GM};
static MethodInfo* t6739_MIs[] =
{
	&m29838_MI,
	&m29839_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6739_0_0_0;
extern Il2CppType t6739_1_0_0;
struct t6739;
extern Il2CppGenericClass t6739_GC;
TypeInfo t6739_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6739_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6739_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6739_TI, &t6739_0_0_0, &t6739_1_0_0, NULL, &t6739_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UILineInfo>
extern Il2CppType t380_0_0_0;
static ParameterInfo t6738_m29840_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29840_GM;
MethodInfo m29840_MI = 
{
	"Equals", NULL, &t6738_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t6738_m29840_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29840_GM};
static MethodInfo* t6738_MIs[] =
{
	&m29840_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6738_1_0_0;
struct t6738;
extern Il2CppGenericClass t6738_GC;
TypeInfo t6738_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6738_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6738_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6738_TI, &t6738_0_0_0, &t6738_1_0_0, NULL, &t6738_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m16796 (t3049 * __this, MethodInfo* method){
	{
		m16791(__this, &m16791_MI);
		return;
	}
}
extern MethodInfo m16797_MI;
 int32_t m16797 (t3049 * __this, t380  p0, MethodInfo* method){
	{
		t380  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t380_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t380_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m16798_MI;
 bool m16798 (t3049 * __this, t380  p0, t380  p1, MethodInfo* method){
	{
		t380  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t380_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t380  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t380_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t380  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t380_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t380_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16796_GM;
MethodInfo m16796_MI = 
{
	".ctor", (methodPointerType)&m16796, &t3049_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16796_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3049_m16797_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16797_GM;
MethodInfo m16797_MI = 
{
	"GetHashCode", (methodPointerType)&m16797, &t3049_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t3049_m16797_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16797_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3049_m16798_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16798_GM;
MethodInfo m16798_MI = 
{
	"Equals", (methodPointerType)&m16798, &t3049_TI, &t40_0_0_0, RuntimeInvoker_t40_t380_t380, t3049_m16798_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16798_GM};
static MethodInfo* t3049_MIs[] =
{
	&m16796_MI,
	&m16797_MI,
	&m16798_MI,
	NULL
};
static MethodInfo* t3049_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16798_MI,
	&m16797_MI,
	&m16794_MI,
	&m16793_MI,
	&m16797_MI,
	&m16798_MI,
};
static Il2CppInterfaceOffsetPair t3049_IOs[] = 
{
	{ &t6739_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3049_0_0_0;
extern Il2CppType t3049_1_0_0;
struct t3049;
extern Il2CppGenericClass t3049_GC;
TypeInfo t3049_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3049_MIs, NULL, NULL, NULL, &t3048_TI, NULL, &t1256_TI, &t3049_TI, NULL, t3049_VT, &EmptyCustomAttributesCache, &t3049_TI, &t3049_0_0_0, &t3049_1_0_0, t3049_IOs, &t3049_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3049), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m16799_MI;
 void m16799 (t3044 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 bool m16800 (t3044 * __this, t380  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m16800((t3044 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t380  p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef bool (*FunctionPointerType) (t29 * __this, t380  p0, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m16801_MI;
 t29 * m16801 (t3044 * __this, t380  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t380_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m16802_MI;
 bool m16802 (t3044 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Predicate`1<UnityEngine.UILineInfo>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3044_m16799_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16799_GM;
MethodInfo m16799_MI = 
{
	".ctor", (methodPointerType)&m16799, &t3044_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3044_m16799_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16799_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t3044_m16800_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16800_GM;
MethodInfo m16800_MI = 
{
	"Invoke", (methodPointerType)&m16800, &t3044_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t3044_m16800_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16800_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3044_m16801_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t380_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16801_GM;
MethodInfo m16801_MI = 
{
	"BeginInvoke", (methodPointerType)&m16801, &t3044_TI, &t66_0_0_0, RuntimeInvoker_t29_t380_t29_t29, t3044_m16801_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16801_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3044_m16802_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16802_GM;
MethodInfo m16802_MI = 
{
	"EndInvoke", (methodPointerType)&m16802, &t3044_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3044_m16802_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16802_GM};
static MethodInfo* t3044_MIs[] =
{
	&m16799_MI,
	&m16800_MI,
	&m16801_MI,
	&m16802_MI,
	NULL
};
static MethodInfo* t3044_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16800_MI,
	&m16801_MI,
	&m16802_MI,
};
static Il2CppInterfaceOffsetPair t3044_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3044_1_0_0;
struct t3044;
extern Il2CppGenericClass t3044_GC;
TypeInfo t3044_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t3044_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3044_TI, NULL, t3044_VT, &EmptyCustomAttributesCache, &t3044_TI, &t3044_0_0_0, &t3044_1_0_0, t3044_IOs, &t3044_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3044), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t3051.h"
extern TypeInfo t4446_TI;
extern TypeInfo t3051_TI;
#include "t3051MD.h"
extern Il2CppType t4446_0_0_0;
extern MethodInfo m16807_MI;
extern MethodInfo m29841_MI;


extern MethodInfo m16803_MI;
 void m16803 (t3050 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m16804_MI;
 void m16804 (t29 * __this, MethodInfo* method){
	t3051 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3051 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3051_TI));
	m16807(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m16807_MI);
	((t3050_SFs*)InitializedTypeInfo(&t3050_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m16805_MI;
 int32_t m16805 (t3050 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t380_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t380_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t380 , t380  >::Invoke(&m29841_MI, __this, ((*(t380 *)((t380 *)UnBox (p0, InitializedTypeInfo(&t380_TI))))), ((*(t380 *)((t380 *)UnBox (p1, InitializedTypeInfo(&t380_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 t3050 * m16806 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3050_TI));
		return (((t3050_SFs*)InitializedTypeInfo(&t3050_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
extern Il2CppType t3050_0_0_49;
FieldInfo t3050_f0_FieldInfo = 
{
	"_default", &t3050_0_0_49, &t3050_TI, offsetof(t3050_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3050_FIs[] =
{
	&t3050_f0_FieldInfo,
	NULL
};
static PropertyInfo t3050____Default_PropertyInfo = 
{
	&t3050_TI, "Default", &m16806_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3050_PIs[] =
{
	&t3050____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16803_GM;
MethodInfo m16803_MI = 
{
	".ctor", (methodPointerType)&m16803, &t3050_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16803_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16804_GM;
MethodInfo m16804_MI = 
{
	".cctor", (methodPointerType)&m16804, &t3050_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16804_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3050_m16805_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16805_GM;
MethodInfo m16805_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m16805, &t3050_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3050_m16805_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16805_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3050_m29841_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29841_GM;
MethodInfo m29841_MI = 
{
	"Compare", NULL, &t3050_TI, &t44_0_0_0, RuntimeInvoker_t44_t380_t380, t3050_m29841_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29841_GM};
extern Il2CppType t3050_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16806_GM;
MethodInfo m16806_MI = 
{
	"get_Default", (methodPointerType)&m16806, &t3050_TI, &t3050_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16806_GM};
static MethodInfo* t3050_MIs[] =
{
	&m16803_MI,
	&m16804_MI,
	&m16805_MI,
	&m29841_MI,
	&m16806_MI,
	NULL
};
static MethodInfo* t3050_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29841_MI,
	&m16805_MI,
	NULL,
};
extern TypeInfo t4445_TI;
static TypeInfo* t3050_ITIs[] = 
{
	&t4445_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3050_IOs[] = 
{
	{ &t4445_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3050_0_0_0;
extern Il2CppType t3050_1_0_0;
struct t3050;
extern Il2CppGenericClass t3050_GC;
TypeInfo t3050_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3050_MIs, t3050_PIs, t3050_FIs, NULL, &t29_TI, NULL, NULL, &t3050_TI, t3050_ITIs, t3050_VT, &EmptyCustomAttributesCache, &t3050_TI, &t3050_0_0_0, &t3050_1_0_0, t3050_IOs, &t3050_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3050), 0, -1, sizeof(t3050_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UILineInfo>
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t4445_m22710_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22710_GM;
MethodInfo m22710_MI = 
{
	"Compare", NULL, &t4445_TI, &t44_0_0_0, RuntimeInvoker_t44_t380_t380, t4445_m22710_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22710_GM};
static MethodInfo* t4445_MIs[] =
{
	&m22710_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4445_0_0_0;
extern Il2CppType t4445_1_0_0;
struct t4445;
extern Il2CppGenericClass t4445_GC;
TypeInfo t4445_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4445_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4445_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4445_TI, &t4445_0_0_0, &t4445_1_0_0, NULL, &t4445_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UILineInfo>
extern Il2CppType t380_0_0_0;
static ParameterInfo t4446_m22711_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22711_GM;
MethodInfo m22711_MI = 
{
	"CompareTo", NULL, &t4446_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t4446_m22711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m22711_GM};
static MethodInfo* t4446_MIs[] =
{
	&m22711_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4446_1_0_0;
struct t4446;
extern Il2CppGenericClass t4446_GC;
TypeInfo t4446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4446_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4446_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4446_TI, &t4446_0_0_0, &t4446_1_0_0, NULL, &t4446_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m22711_MI;


 void m16807 (t3051 * __this, MethodInfo* method){
	{
		m16803(__this, &m16803_MI);
		return;
	}
}
extern MethodInfo m16808_MI;
 int32_t m16808 (t3051 * __this, t380  p0, t380  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t380  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t380_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t380  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t380_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t380  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t380_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t380  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t380_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t4446_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t380  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t380_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t380  >::Invoke(&m22711_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t4446_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t380  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t380_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t380  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t380_TI), &L_13);
		t380  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t380_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16807_GM;
MethodInfo m16807_MI = 
{
	".ctor", (methodPointerType)&m16807, &t3051_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16807_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3051_m16808_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16808_GM;
MethodInfo m16808_MI = 
{
	"Compare", (methodPointerType)&m16808, &t3051_TI, &t44_0_0_0, RuntimeInvoker_t44_t380_t380, t3051_m16808_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16808_GM};
static MethodInfo* t3051_MIs[] =
{
	&m16807_MI,
	&m16808_MI,
	NULL
};
static MethodInfo* t3051_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16808_MI,
	&m16805_MI,
	&m16808_MI,
};
static Il2CppInterfaceOffsetPair t3051_IOs[] = 
{
	{ &t4445_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3051_0_0_0;
extern Il2CppType t3051_1_0_0;
struct t3051;
extern Il2CppGenericClass t3051_GC;
TypeInfo t3051_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3051_MIs, NULL, NULL, NULL, &t3050_TI, NULL, &t1246_TI, &t3051_TI, NULL, t3051_VT, &EmptyCustomAttributesCache, &t3051_TI, &t3051_0_0_0, &t3051_1_0_0, t3051_IOs, &t3051_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3051), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3045_TI;
#include "t3045MD.h"



extern MethodInfo m16809_MI;
 void m16809 (t3045 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m16810_MI;
 int32_t m16810 (t3045 * __this, t380  p0, t380  p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m16810((t3045 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t380  p0, t380  p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef int32_t (*FunctionPointerType) (t29 * __this, t380  p0, t380  p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m16811_MI;
 t29 * m16811 (t3045 * __this, t380  p0, t380  p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t380_TI), &p0);
	__d_args[1] = Box(InitializedTypeInfo(&t380_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m16812_MI;
 int32_t m16812 (t3045 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<UnityEngine.UILineInfo>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3045_m16809_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16809_GM;
MethodInfo m16809_MI = 
{
	".ctor", (methodPointerType)&m16809, &t3045_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3045_m16809_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16809_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t3045_m16810_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16810_GM;
MethodInfo m16810_MI = 
{
	"Invoke", (methodPointerType)&m16810, &t3045_TI, &t44_0_0_0, RuntimeInvoker_t44_t380_t380, t3045_m16810_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16810_GM};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3045_m16811_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t380_t380_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16811_GM;
MethodInfo m16811_MI = 
{
	"BeginInvoke", (methodPointerType)&m16811, &t3045_TI, &t66_0_0_0, RuntimeInvoker_t29_t380_t380_t29_t29, t3045_m16811_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m16811_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3045_m16812_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16812_GM;
MethodInfo m16812_MI = 
{
	"EndInvoke", (methodPointerType)&m16812, &t3045_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3045_m16812_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16812_GM};
static MethodInfo* t3045_MIs[] =
{
	&m16809_MI,
	&m16810_MI,
	&m16811_MI,
	&m16812_MI,
	NULL
};
static MethodInfo* t3045_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16810_MI,
	&m16811_MI,
	&m16812_MI,
};
static Il2CppInterfaceOffsetPair t3045_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3045_1_0_0;
struct t3045;
extern Il2CppGenericClass t3045_GC;
TypeInfo t3045_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3045_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3045_TI, NULL, t3045_VT, &EmptyCustomAttributesCache, &t3045_TI, &t3045_0_0_0, &t3045_1_0_0, t3045_IOs, &t3045_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3045), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4448_TI;

#include "t361.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RenderMode>
extern MethodInfo m29842_MI;
static PropertyInfo t4448____Current_PropertyInfo = 
{
	&t4448_TI, "Current", &m29842_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4448_PIs[] =
{
	&t4448____Current_PropertyInfo,
	NULL
};
extern Il2CppType t361_0_0_0;
extern void* RuntimeInvoker_t361 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29842_GM;
MethodInfo m29842_MI = 
{
	"get_Current", NULL, &t4448_TI, &t361_0_0_0, RuntimeInvoker_t361, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29842_GM};
static MethodInfo* t4448_MIs[] =
{
	&m29842_MI,
	NULL
};
static TypeInfo* t4448_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4448_0_0_0;
extern Il2CppType t4448_1_0_0;
struct t4448;
extern Il2CppGenericClass t4448_GC;
TypeInfo t4448_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4448_MIs, t4448_PIs, NULL, NULL, NULL, NULL, NULL, &t4448_TI, t4448_ITIs, NULL, &EmptyCustomAttributesCache, &t4448_TI, &t4448_0_0_0, &t4448_1_0_0, NULL, &t4448_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3052.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3052_TI;
#include "t3052MD.h"

extern TypeInfo t361_TI;
extern MethodInfo m16817_MI;
extern MethodInfo m22716_MI;
struct t20;
 int32_t m22716 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16813_MI;
 void m16813 (t3052 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16814_MI;
 t29 * m16814 (t3052 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16817(__this, &m16817_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t361_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16815_MI;
 void m16815 (t3052 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16816_MI;
 bool m16816 (t3052 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16817 (t3052 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22716(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22716_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RenderMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3052_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3052_TI, offsetof(t3052, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3052_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3052_TI, offsetof(t3052, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3052_FIs[] =
{
	&t3052_f0_FieldInfo,
	&t3052_f1_FieldInfo,
	NULL
};
static PropertyInfo t3052____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3052_TI, "System.Collections.IEnumerator.Current", &m16814_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3052____Current_PropertyInfo = 
{
	&t3052_TI, "Current", &m16817_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3052_PIs[] =
{
	&t3052____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3052____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3052_m16813_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16813_GM;
MethodInfo m16813_MI = 
{
	".ctor", (methodPointerType)&m16813, &t3052_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3052_m16813_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16813_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16814_GM;
MethodInfo m16814_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16814, &t3052_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16814_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16815_GM;
MethodInfo m16815_MI = 
{
	"Dispose", (methodPointerType)&m16815, &t3052_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16815_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16816_GM;
MethodInfo m16816_MI = 
{
	"MoveNext", (methodPointerType)&m16816, &t3052_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16816_GM};
extern Il2CppType t361_0_0_0;
extern void* RuntimeInvoker_t361 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16817_GM;
MethodInfo m16817_MI = 
{
	"get_Current", (methodPointerType)&m16817, &t3052_TI, &t361_0_0_0, RuntimeInvoker_t361, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16817_GM};
static MethodInfo* t3052_MIs[] =
{
	&m16813_MI,
	&m16814_MI,
	&m16815_MI,
	&m16816_MI,
	&m16817_MI,
	NULL
};
static MethodInfo* t3052_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16814_MI,
	&m16816_MI,
	&m16815_MI,
	&m16817_MI,
};
static TypeInfo* t3052_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4448_TI,
};
static Il2CppInterfaceOffsetPair t3052_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4448_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3052_0_0_0;
extern Il2CppType t3052_1_0_0;
extern Il2CppGenericClass t3052_GC;
TypeInfo t3052_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3052_MIs, t3052_PIs, t3052_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3052_TI, t3052_ITIs, t3052_VT, &EmptyCustomAttributesCache, &t3052_TI, &t3052_0_0_0, &t3052_1_0_0, t3052_IOs, &t3052_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3052)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5706_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RenderMode>
extern MethodInfo m29843_MI;
static PropertyInfo t5706____Count_PropertyInfo = 
{
	&t5706_TI, "Count", &m29843_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29844_MI;
static PropertyInfo t5706____IsReadOnly_PropertyInfo = 
{
	&t5706_TI, "IsReadOnly", &m29844_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5706_PIs[] =
{
	&t5706____Count_PropertyInfo,
	&t5706____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29843_GM;
MethodInfo m29843_MI = 
{
	"get_Count", NULL, &t5706_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29843_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29844_GM;
MethodInfo m29844_MI = 
{
	"get_IsReadOnly", NULL, &t5706_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29844_GM};
extern Il2CppType t361_0_0_0;
extern Il2CppType t361_0_0_0;
static ParameterInfo t5706_m29845_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t361_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29845_GM;
MethodInfo m29845_MI = 
{
	"Add", NULL, &t5706_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5706_m29845_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29845_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29846_GM;
MethodInfo m29846_MI = 
{
	"Clear", NULL, &t5706_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29846_GM};
extern Il2CppType t361_0_0_0;
static ParameterInfo t5706_m29847_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t361_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29847_GM;
MethodInfo m29847_MI = 
{
	"Contains", NULL, &t5706_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5706_m29847_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29847_GM};
extern Il2CppType t3774_0_0_0;
extern Il2CppType t3774_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5706_m29848_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3774_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29848_GM;
MethodInfo m29848_MI = 
{
	"CopyTo", NULL, &t5706_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5706_m29848_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29848_GM};
extern Il2CppType t361_0_0_0;
static ParameterInfo t5706_m29849_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t361_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29849_GM;
MethodInfo m29849_MI = 
{
	"Remove", NULL, &t5706_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5706_m29849_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29849_GM};
static MethodInfo* t5706_MIs[] =
{
	&m29843_MI,
	&m29844_MI,
	&m29845_MI,
	&m29846_MI,
	&m29847_MI,
	&m29848_MI,
	&m29849_MI,
	NULL
};
extern TypeInfo t5708_TI;
static TypeInfo* t5706_ITIs[] = 
{
	&t603_TI,
	&t5708_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5706_0_0_0;
extern Il2CppType t5706_1_0_0;
struct t5706;
extern Il2CppGenericClass t5706_GC;
TypeInfo t5706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5706_MIs, t5706_PIs, NULL, NULL, NULL, NULL, NULL, &t5706_TI, t5706_ITIs, NULL, &EmptyCustomAttributesCache, &t5706_TI, &t5706_0_0_0, &t5706_1_0_0, NULL, &t5706_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RenderMode>
extern Il2CppType t4448_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29850_GM;
MethodInfo m29850_MI = 
{
	"GetEnumerator", NULL, &t5708_TI, &t4448_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29850_GM};
static MethodInfo* t5708_MIs[] =
{
	&m29850_MI,
	NULL
};
static TypeInfo* t5708_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5708_0_0_0;
extern Il2CppType t5708_1_0_0;
struct t5708;
extern Il2CppGenericClass t5708_GC;
TypeInfo t5708_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5708_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5708_TI, t5708_ITIs, NULL, &EmptyCustomAttributesCache, &t5708_TI, &t5708_0_0_0, &t5708_1_0_0, NULL, &t5708_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5707_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RenderMode>
extern MethodInfo m29851_MI;
extern MethodInfo m29852_MI;
static PropertyInfo t5707____Item_PropertyInfo = 
{
	&t5707_TI, "Item", &m29851_MI, &m29852_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5707_PIs[] =
{
	&t5707____Item_PropertyInfo,
	NULL
};
extern Il2CppType t361_0_0_0;
static ParameterInfo t5707_m29853_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t361_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29853_GM;
MethodInfo m29853_MI = 
{
	"IndexOf", NULL, &t5707_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5707_m29853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29853_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t361_0_0_0;
static ParameterInfo t5707_m29854_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t361_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29854_GM;
MethodInfo m29854_MI = 
{
	"Insert", NULL, &t5707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5707_m29854_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29854_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5707_m29855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29855_GM;
MethodInfo m29855_MI = 
{
	"RemoveAt", NULL, &t5707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5707_m29855_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29855_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5707_m29851_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t361_0_0_0;
extern void* RuntimeInvoker_t361_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29851_GM;
MethodInfo m29851_MI = 
{
	"get_Item", NULL, &t5707_TI, &t361_0_0_0, RuntimeInvoker_t361_t44, t5707_m29851_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29851_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t361_0_0_0;
static ParameterInfo t5707_m29852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t361_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29852_GM;
MethodInfo m29852_MI = 
{
	"set_Item", NULL, &t5707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5707_m29852_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29852_GM};
static MethodInfo* t5707_MIs[] =
{
	&m29853_MI,
	&m29854_MI,
	&m29855_MI,
	&m29851_MI,
	&m29852_MI,
	NULL
};
static TypeInfo* t5707_ITIs[] = 
{
	&t603_TI,
	&t5706_TI,
	&t5708_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5707_0_0_0;
extern Il2CppType t5707_1_0_0;
struct t5707;
extern Il2CppGenericClass t5707_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5707_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5707_MIs, t5707_PIs, NULL, NULL, NULL, NULL, NULL, &t5707_TI, t5707_ITIs, NULL, &t1908__CustomAttributeCache, &t5707_TI, &t5707_0_0_0, &t5707_1_0_0, NULL, &t5707_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3053.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3053_TI;
#include "t3053MD.h"

#include "t3.h"
#include "t3054.h"
extern TypeInfo t3_TI;
extern TypeInfo t3054_TI;
#include "t3054MD.h"
extern MethodInfo m16820_MI;
extern MethodInfo m16822_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Canvas>
extern Il2CppType t316_0_0_33;
FieldInfo t3053_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3053_TI, offsetof(t3053, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3053_FIs[] =
{
	&t3053_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t3053_m16818_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16818_GM;
MethodInfo m16818_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3053_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3053_m16818_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16818_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3053_m16819_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16819_GM;
MethodInfo m16819_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3053_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3053_m16819_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16819_GM};
static MethodInfo* t3053_MIs[] =
{
	&m16818_MI,
	&m16819_MI,
	NULL
};
extern MethodInfo m16819_MI;
extern MethodInfo m16823_MI;
static MethodInfo* t3053_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16819_MI,
	&m16823_MI,
};
extern Il2CppType t3055_0_0_0;
extern TypeInfo t3055_TI;
extern MethodInfo m22726_MI;
extern TypeInfo t3_TI;
extern MethodInfo m16825_MI;
extern TypeInfo t3_TI;
static Il2CppRGCTXData t3053_RGCTXData[8] = 
{
	&t3055_0_0_0/* Type Usage */,
	&t3055_TI/* Class Usage */,
	&m22726_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m16825_MI/* Method Usage */,
	&m16820_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m16822_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3053_0_0_0;
extern Il2CppType t3053_1_0_0;
struct t3053;
extern Il2CppGenericClass t3053_GC;
TypeInfo t3053_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3053_MIs, NULL, t3053_FIs, NULL, &t3054_TI, NULL, NULL, &t3053_TI, NULL, t3053_VT, &EmptyCustomAttributesCache, &t3053_TI, &t3053_0_0_0, &t3053_1_0_0, NULL, &t3053_GC, NULL, NULL, NULL, t3053_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3053), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3055.h"
extern TypeInfo t3055_TI;
#include "t3055MD.h"
struct t556;
#define m22726(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Canvas>
extern Il2CppType t3055_0_0_1;
FieldInfo t3054_f0_FieldInfo = 
{
	"Delegate", &t3055_0_0_1, &t3054_TI, offsetof(t3054, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3054_FIs[] =
{
	&t3054_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3054_m16820_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16820_GM;
MethodInfo m16820_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3054_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3054_m16820_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16820_GM};
extern Il2CppType t3055_0_0_0;
static ParameterInfo t3054_m16821_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3055_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16821_GM;
MethodInfo m16821_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3054_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3054_m16821_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16821_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3054_m16822_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16822_GM;
MethodInfo m16822_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3054_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3054_m16822_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16822_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3054_m16823_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16823_GM;
MethodInfo m16823_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3054_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3054_m16823_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16823_GM};
static MethodInfo* t3054_MIs[] =
{
	&m16820_MI,
	&m16821_MI,
	&m16822_MI,
	&m16823_MI,
	NULL
};
static MethodInfo* t3054_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16822_MI,
	&m16823_MI,
};
extern TypeInfo t3055_TI;
extern TypeInfo t3_TI;
static Il2CppRGCTXData t3054_RGCTXData[5] = 
{
	&t3055_0_0_0/* Type Usage */,
	&t3055_TI/* Class Usage */,
	&m22726_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m16825_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3054_0_0_0;
extern Il2CppType t3054_1_0_0;
struct t3054;
extern Il2CppGenericClass t3054_GC;
TypeInfo t3054_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3054_MIs, NULL, t3054_FIs, NULL, &t556_TI, NULL, NULL, &t3054_TI, NULL, t3054_VT, &EmptyCustomAttributesCache, &t3054_TI, &t3054_0_0_0, &t3054_1_0_0, NULL, &t3054_GC, NULL, NULL, NULL, t3054_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3054), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Canvas>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3055_m16824_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16824_GM;
MethodInfo m16824_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3055_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3055_m16824_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16824_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t3055_m16825_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16825_GM;
MethodInfo m16825_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3055_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3055_m16825_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16825_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3055_m16826_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16826_GM;
MethodInfo m16826_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3055_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3055_m16826_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16826_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3055_m16827_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16827_GM;
MethodInfo m16827_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3055_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3055_m16827_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16827_GM};
static MethodInfo* t3055_MIs[] =
{
	&m16824_MI,
	&m16825_MI,
	&m16826_MI,
	&m16827_MI,
	NULL
};
extern MethodInfo m16826_MI;
extern MethodInfo m16827_MI;
static MethodInfo* t3055_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16825_MI,
	&m16826_MI,
	&m16827_MI,
};
static Il2CppInterfaceOffsetPair t3055_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3055_1_0_0;
struct t3055;
extern Il2CppGenericClass t3055_GC;
TypeInfo t3055_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3055_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3055_TI, NULL, t3055_VT, &EmptyCustomAttributesCache, &t3055_TI, &t3055_0_0_0, &t3055_1_0_0, t3055_IOs, &t3055_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3055), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3056.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3056_TI;
#include "t3056MD.h"

#include "t352.h"
#include "t3057.h"
extern TypeInfo t352_TI;
extern TypeInfo t3057_TI;
#include "t3057MD.h"
extern MethodInfo m16830_MI;
extern MethodInfo m16832_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t3056_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3056_TI, offsetof(t3056, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3056_FIs[] =
{
	&t3056_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t3056_m16828_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16828_GM;
MethodInfo m16828_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3056_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3056_m16828_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16828_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3056_m16829_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16829_GM;
MethodInfo m16829_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3056_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3056_m16829_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16829_GM};
static MethodInfo* t3056_MIs[] =
{
	&m16828_MI,
	&m16829_MI,
	NULL
};
extern MethodInfo m16829_MI;
extern MethodInfo m16833_MI;
static MethodInfo* t3056_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16829_MI,
	&m16833_MI,
};
extern Il2CppType t3058_0_0_0;
extern TypeInfo t3058_TI;
extern MethodInfo m22727_MI;
extern TypeInfo t352_TI;
extern MethodInfo m16835_MI;
extern TypeInfo t352_TI;
static Il2CppRGCTXData t3056_RGCTXData[8] = 
{
	&t3058_0_0_0/* Type Usage */,
	&t3058_TI/* Class Usage */,
	&m22727_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m16835_MI/* Method Usage */,
	&m16830_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m16832_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3056_0_0_0;
extern Il2CppType t3056_1_0_0;
struct t3056;
extern Il2CppGenericClass t3056_GC;
TypeInfo t3056_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3056_MIs, NULL, t3056_FIs, NULL, &t3057_TI, NULL, NULL, &t3056_TI, NULL, t3056_VT, &EmptyCustomAttributesCache, &t3056_TI, &t3056_0_0_0, &t3056_1_0_0, NULL, &t3056_GC, NULL, NULL, NULL, t3056_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3056), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3058.h"
extern TypeInfo t3058_TI;
#include "t3058MD.h"
struct t556;
#define m22727(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasGroup>
extern Il2CppType t3058_0_0_1;
FieldInfo t3057_f0_FieldInfo = 
{
	"Delegate", &t3058_0_0_1, &t3057_TI, offsetof(t3057, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3057_FIs[] =
{
	&t3057_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3057_m16830_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16830_GM;
MethodInfo m16830_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3057_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3057_m16830_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16830_GM};
extern Il2CppType t3058_0_0_0;
static ParameterInfo t3057_m16831_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3058_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16831_GM;
MethodInfo m16831_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3057_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3057_m16831_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16831_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3057_m16832_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16832_GM;
MethodInfo m16832_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3057_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3057_m16832_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16832_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3057_m16833_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16833_GM;
MethodInfo m16833_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3057_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3057_m16833_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16833_GM};
static MethodInfo* t3057_MIs[] =
{
	&m16830_MI,
	&m16831_MI,
	&m16832_MI,
	&m16833_MI,
	NULL
};
static MethodInfo* t3057_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16832_MI,
	&m16833_MI,
};
extern TypeInfo t3058_TI;
extern TypeInfo t352_TI;
static Il2CppRGCTXData t3057_RGCTXData[5] = 
{
	&t3058_0_0_0/* Type Usage */,
	&t3058_TI/* Class Usage */,
	&m22727_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m16835_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3057_0_0_0;
extern Il2CppType t3057_1_0_0;
struct t3057;
extern Il2CppGenericClass t3057_GC;
TypeInfo t3057_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3057_MIs, NULL, t3057_FIs, NULL, &t556_TI, NULL, NULL, &t3057_TI, NULL, t3057_VT, &EmptyCustomAttributesCache, &t3057_TI, &t3057_0_0_0, &t3057_1_0_0, NULL, &t3057_GC, NULL, NULL, NULL, t3057_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3057), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.CanvasGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3058_m16834_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16834_GM;
MethodInfo m16834_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3058_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3058_m16834_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16834_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t3058_m16835_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16835_GM;
MethodInfo m16835_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3058_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3058_m16835_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16835_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3058_m16836_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16836_GM;
MethodInfo m16836_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3058_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3058_m16836_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16836_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3058_m16837_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16837_GM;
MethodInfo m16837_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3058_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3058_m16837_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16837_GM};
static MethodInfo* t3058_MIs[] =
{
	&m16834_MI,
	&m16835_MI,
	&m16836_MI,
	&m16837_MI,
	NULL
};
extern MethodInfo m16836_MI;
extern MethodInfo m16837_MI;
static MethodInfo* t3058_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16835_MI,
	&m16836_MI,
	&m16837_MI,
};
static Il2CppInterfaceOffsetPair t3058_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3058_1_0_0;
struct t3058;
extern Il2CppGenericClass t3058_GC;
TypeInfo t3058_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3058_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3058_TI, NULL, t3058_VT, &EmptyCustomAttributesCache, &t3058_TI, &t3058_0_0_0, &t3058_1_0_0, t3058_IOs, &t3058_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3058), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4450_TI;

#include "t159.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.CanvasRenderer>
extern MethodInfo m29856_MI;
static PropertyInfo t4450____Current_PropertyInfo = 
{
	&t4450_TI, "Current", &m29856_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4450_PIs[] =
{
	&t4450____Current_PropertyInfo,
	NULL
};
extern Il2CppType t159_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29856_GM;
MethodInfo m29856_MI = 
{
	"get_Current", NULL, &t4450_TI, &t159_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29856_GM};
static MethodInfo* t4450_MIs[] =
{
	&m29856_MI,
	NULL
};
static TypeInfo* t4450_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4450_0_0_0;
extern Il2CppType t4450_1_0_0;
struct t4450;
extern Il2CppGenericClass t4450_GC;
TypeInfo t4450_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4450_MIs, t4450_PIs, NULL, NULL, NULL, NULL, NULL, &t4450_TI, t4450_ITIs, NULL, &EmptyCustomAttributesCache, &t4450_TI, &t4450_0_0_0, &t4450_1_0_0, NULL, &t4450_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3059.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3059_TI;
#include "t3059MD.h"

extern TypeInfo t159_TI;
extern MethodInfo m16842_MI;
extern MethodInfo m22729_MI;
struct t20;
#define m22729(__this, p0, method) (t159 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.CanvasRenderer>
extern Il2CppType t20_0_0_1;
FieldInfo t3059_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3059_TI, offsetof(t3059, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3059_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3059_TI, offsetof(t3059, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3059_FIs[] =
{
	&t3059_f0_FieldInfo,
	&t3059_f1_FieldInfo,
	NULL
};
extern MethodInfo m16839_MI;
static PropertyInfo t3059____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3059_TI, "System.Collections.IEnumerator.Current", &m16839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3059____Current_PropertyInfo = 
{
	&t3059_TI, "Current", &m16842_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3059_PIs[] =
{
	&t3059____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3059____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3059_m16838_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16838_GM;
MethodInfo m16838_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3059_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3059_m16838_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16838_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16839_GM;
MethodInfo m16839_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3059_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16839_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16840_GM;
MethodInfo m16840_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3059_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16840_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16841_GM;
MethodInfo m16841_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3059_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16841_GM};
extern Il2CppType t159_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16842_GM;
MethodInfo m16842_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3059_TI, &t159_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16842_GM};
static MethodInfo* t3059_MIs[] =
{
	&m16838_MI,
	&m16839_MI,
	&m16840_MI,
	&m16841_MI,
	&m16842_MI,
	NULL
};
extern MethodInfo m16841_MI;
extern MethodInfo m16840_MI;
static MethodInfo* t3059_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16839_MI,
	&m16841_MI,
	&m16840_MI,
	&m16842_MI,
};
static TypeInfo* t3059_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4450_TI,
};
static Il2CppInterfaceOffsetPair t3059_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4450_TI, 7},
};
extern TypeInfo t159_TI;
static Il2CppRGCTXData t3059_RGCTXData[3] = 
{
	&m16842_MI/* Method Usage */,
	&t159_TI/* Class Usage */,
	&m22729_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3059_0_0_0;
extern Il2CppType t3059_1_0_0;
extern Il2CppGenericClass t3059_GC;
TypeInfo t3059_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3059_MIs, t3059_PIs, t3059_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3059_TI, t3059_ITIs, t3059_VT, &EmptyCustomAttributesCache, &t3059_TI, &t3059_0_0_0, &t3059_1_0_0, t3059_IOs, &t3059_GC, NULL, NULL, NULL, t3059_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3059)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5709_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.CanvasRenderer>
extern MethodInfo m29857_MI;
static PropertyInfo t5709____Count_PropertyInfo = 
{
	&t5709_TI, "Count", &m29857_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29858_MI;
static PropertyInfo t5709____IsReadOnly_PropertyInfo = 
{
	&t5709_TI, "IsReadOnly", &m29858_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5709_PIs[] =
{
	&t5709____Count_PropertyInfo,
	&t5709____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29857_GM;
MethodInfo m29857_MI = 
{
	"get_Count", NULL, &t5709_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29857_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29858_GM;
MethodInfo m29858_MI = 
{
	"get_IsReadOnly", NULL, &t5709_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29858_GM};
extern Il2CppType t159_0_0_0;
extern Il2CppType t159_0_0_0;
static ParameterInfo t5709_m29859_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29859_GM;
MethodInfo m29859_MI = 
{
	"Add", NULL, &t5709_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5709_m29859_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29859_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29860_GM;
MethodInfo m29860_MI = 
{
	"Clear", NULL, &t5709_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29860_GM};
extern Il2CppType t159_0_0_0;
static ParameterInfo t5709_m29861_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29861_GM;
MethodInfo m29861_MI = 
{
	"Contains", NULL, &t5709_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5709_m29861_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29861_GM};
extern Il2CppType t3775_0_0_0;
extern Il2CppType t3775_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5709_m29862_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3775_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29862_GM;
MethodInfo m29862_MI = 
{
	"CopyTo", NULL, &t5709_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5709_m29862_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29862_GM};
extern Il2CppType t159_0_0_0;
static ParameterInfo t5709_m29863_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29863_GM;
MethodInfo m29863_MI = 
{
	"Remove", NULL, &t5709_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5709_m29863_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29863_GM};
static MethodInfo* t5709_MIs[] =
{
	&m29857_MI,
	&m29858_MI,
	&m29859_MI,
	&m29860_MI,
	&m29861_MI,
	&m29862_MI,
	&m29863_MI,
	NULL
};
extern TypeInfo t5711_TI;
static TypeInfo* t5709_ITIs[] = 
{
	&t603_TI,
	&t5711_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5709_0_0_0;
extern Il2CppType t5709_1_0_0;
struct t5709;
extern Il2CppGenericClass t5709_GC;
TypeInfo t5709_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5709_MIs, t5709_PIs, NULL, NULL, NULL, NULL, NULL, &t5709_TI, t5709_ITIs, NULL, &EmptyCustomAttributesCache, &t5709_TI, &t5709_0_0_0, &t5709_1_0_0, NULL, &t5709_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.CanvasRenderer>
extern Il2CppType t4450_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29864_GM;
MethodInfo m29864_MI = 
{
	"GetEnumerator", NULL, &t5711_TI, &t4450_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29864_GM};
static MethodInfo* t5711_MIs[] =
{
	&m29864_MI,
	NULL
};
static TypeInfo* t5711_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5711_0_0_0;
extern Il2CppType t5711_1_0_0;
struct t5711;
extern Il2CppGenericClass t5711_GC;
TypeInfo t5711_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5711_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5711_TI, t5711_ITIs, NULL, &EmptyCustomAttributesCache, &t5711_TI, &t5711_0_0_0, &t5711_1_0_0, NULL, &t5711_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5710_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.CanvasRenderer>
extern MethodInfo m29865_MI;
extern MethodInfo m29866_MI;
static PropertyInfo t5710____Item_PropertyInfo = 
{
	&t5710_TI, "Item", &m29865_MI, &m29866_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5710_PIs[] =
{
	&t5710____Item_PropertyInfo,
	NULL
};
extern Il2CppType t159_0_0_0;
static ParameterInfo t5710_m29867_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29867_GM;
MethodInfo m29867_MI = 
{
	"IndexOf", NULL, &t5710_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5710_m29867_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29867_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t159_0_0_0;
static ParameterInfo t5710_m29868_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29868_GM;
MethodInfo m29868_MI = 
{
	"Insert", NULL, &t5710_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5710_m29868_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29868_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5710_m29869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29869_GM;
MethodInfo m29869_MI = 
{
	"RemoveAt", NULL, &t5710_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5710_m29869_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29869_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5710_m29865_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t159_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29865_GM;
MethodInfo m29865_MI = 
{
	"get_Item", NULL, &t5710_TI, &t159_0_0_0, RuntimeInvoker_t29_t44, t5710_m29865_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29865_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t159_0_0_0;
static ParameterInfo t5710_m29866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29866_GM;
MethodInfo m29866_MI = 
{
	"set_Item", NULL, &t5710_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5710_m29866_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29866_GM};
static MethodInfo* t5710_MIs[] =
{
	&m29867_MI,
	&m29868_MI,
	&m29869_MI,
	&m29865_MI,
	&m29866_MI,
	NULL
};
static TypeInfo* t5710_ITIs[] = 
{
	&t603_TI,
	&t5709_TI,
	&t5711_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5710_0_0_0;
extern Il2CppType t5710_1_0_0;
struct t5710;
extern Il2CppGenericClass t5710_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5710_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5710_MIs, t5710_PIs, NULL, NULL, NULL, NULL, NULL, &t5710_TI, t5710_ITIs, NULL, &t1908__CustomAttributeCache, &t5710_TI, &t5710_0_0_0, &t5710_1_0_0, NULL, &t5710_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3060.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3060_TI;
#include "t3060MD.h"

#include "t3061.h"
extern TypeInfo t3061_TI;
#include "t3061MD.h"
extern MethodInfo m16845_MI;
extern MethodInfo m16847_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasRenderer>
extern Il2CppType t316_0_0_33;
FieldInfo t3060_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3060_TI, offsetof(t3060, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3060_FIs[] =
{
	&t3060_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t159_0_0_0;
static ParameterInfo t3060_m16843_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16843_GM;
MethodInfo m16843_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3060_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3060_m16843_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16843_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3060_m16844_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16844_GM;
MethodInfo m16844_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3060_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3060_m16844_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16844_GM};
static MethodInfo* t3060_MIs[] =
{
	&m16843_MI,
	&m16844_MI,
	NULL
};
extern MethodInfo m16844_MI;
extern MethodInfo m16848_MI;
static MethodInfo* t3060_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16844_MI,
	&m16848_MI,
};
extern Il2CppType t3062_0_0_0;
extern TypeInfo t3062_TI;
extern MethodInfo m22739_MI;
extern TypeInfo t159_TI;
extern MethodInfo m16850_MI;
extern TypeInfo t159_TI;
static Il2CppRGCTXData t3060_RGCTXData[8] = 
{
	&t3062_0_0_0/* Type Usage */,
	&t3062_TI/* Class Usage */,
	&m22739_MI/* Method Usage */,
	&t159_TI/* Class Usage */,
	&m16850_MI/* Method Usage */,
	&m16845_MI/* Method Usage */,
	&t159_TI/* Class Usage */,
	&m16847_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3060_0_0_0;
extern Il2CppType t3060_1_0_0;
struct t3060;
extern Il2CppGenericClass t3060_GC;
TypeInfo t3060_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3060_MIs, NULL, t3060_FIs, NULL, &t3061_TI, NULL, NULL, &t3060_TI, NULL, t3060_VT, &EmptyCustomAttributesCache, &t3060_TI, &t3060_0_0_0, &t3060_1_0_0, NULL, &t3060_GC, NULL, NULL, NULL, t3060_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3060), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3062.h"
extern TypeInfo t3062_TI;
#include "t3062MD.h"
struct t556;
#define m22739(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasRenderer>
extern Il2CppType t3062_0_0_1;
FieldInfo t3061_f0_FieldInfo = 
{
	"Delegate", &t3062_0_0_1, &t3061_TI, offsetof(t3061, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3061_FIs[] =
{
	&t3061_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3061_m16845_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16845_GM;
MethodInfo m16845_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3061_m16845_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16845_GM};
extern Il2CppType t3062_0_0_0;
static ParameterInfo t3061_m16846_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3062_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16846_GM;
MethodInfo m16846_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3061_m16846_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16846_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3061_m16847_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16847_GM;
MethodInfo m16847_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3061_m16847_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16847_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3061_m16848_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16848_GM;
MethodInfo m16848_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3061_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3061_m16848_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16848_GM};
static MethodInfo* t3061_MIs[] =
{
	&m16845_MI,
	&m16846_MI,
	&m16847_MI,
	&m16848_MI,
	NULL
};
static MethodInfo* t3061_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16847_MI,
	&m16848_MI,
};
extern TypeInfo t3062_TI;
extern TypeInfo t159_TI;
static Il2CppRGCTXData t3061_RGCTXData[5] = 
{
	&t3062_0_0_0/* Type Usage */,
	&t3062_TI/* Class Usage */,
	&m22739_MI/* Method Usage */,
	&t159_TI/* Class Usage */,
	&m16850_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3061_0_0_0;
extern Il2CppType t3061_1_0_0;
struct t3061;
extern Il2CppGenericClass t3061_GC;
TypeInfo t3061_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3061_MIs, NULL, t3061_FIs, NULL, &t556_TI, NULL, NULL, &t3061_TI, NULL, t3061_VT, &EmptyCustomAttributesCache, &t3061_TI, &t3061_0_0_0, &t3061_1_0_0, NULL, &t3061_GC, NULL, NULL, NULL, t3061_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3061), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.CanvasRenderer>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3062_m16849_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16849_GM;
MethodInfo m16849_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3062_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3062_m16849_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16849_GM};
extern Il2CppType t159_0_0_0;
static ParameterInfo t3062_m16850_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16850_GM;
MethodInfo m16850_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3062_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3062_m16850_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16850_GM};
extern Il2CppType t159_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3062_m16851_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t159_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16851_GM;
MethodInfo m16851_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3062_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3062_m16851_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16851_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3062_m16852_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16852_GM;
MethodInfo m16852_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3062_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3062_m16852_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16852_GM};
static MethodInfo* t3062_MIs[] =
{
	&m16849_MI,
	&m16850_MI,
	&m16851_MI,
	&m16852_MI,
	NULL
};
extern MethodInfo m16851_MI;
extern MethodInfo m16852_MI;
static MethodInfo* t3062_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16850_MI,
	&m16851_MI,
	&m16852_MI,
};
static Il2CppInterfaceOffsetPair t3062_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3062_1_0_0;
struct t3062;
extern Il2CppGenericClass t3062_GC;
TypeInfo t3062_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3062_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3062_TI, NULL, t3062_VT, &EmptyCustomAttributesCache, &t3062_TI, &t3062_0_0_0, &t3062_1_0_0, t3062_IOs, &t3062_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3062), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
