﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1578;
struct t1576;
struct t29;

 void m8550 (t1578 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1576 * m8551 (t1578 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8552 (t1578 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8553 (t1578 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
