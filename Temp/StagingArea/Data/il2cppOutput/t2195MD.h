﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2195;
struct t29;

 void m10745_gshared (t2195 * __this, MethodInfo* method);
#define m10745(__this, method) (void)m10745_gshared((t2195 *)__this, method)
 void m10746_gshared (t29 * __this, MethodInfo* method);
#define m10746(__this, method) (void)m10746_gshared((t29 *)__this, method)
 int32_t m10747_gshared (t2195 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m10747(__this, p0, p1, method) (int32_t)m10747_gshared((t2195 *)__this, (t29 *)p0, (t29 *)p1, method)
 t2195 * m10748_gshared (t29 * __this, MethodInfo* method);
#define m10748(__this, method) (t2195 *)m10748_gshared((t29 *)__this, method)
