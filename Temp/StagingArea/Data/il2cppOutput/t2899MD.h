﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2899;
struct t29;
struct t7;
struct t466;
struct t66;
struct t67;
#include "t35.h"
#include "t2893.h"

 void m15867 (t2899 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2893  m15868 (t2899 * __this, t7* p0, t466 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15869 (t2899 * __this, t7* p0, t466 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2893  m15870 (t2899 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
