﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2256;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t57.h"

 void m11388 (t2256 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11389 (t2256 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11390 (t2256 * __this, t57  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11391 (t2256 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
