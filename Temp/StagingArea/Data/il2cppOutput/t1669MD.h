﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1669;
struct t633;
struct t7;

 void m9508 (t1669 * __this, t633 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9509 (t1669 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9510 (t1669 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9511 (t1669 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
