﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2562;
struct t3;
struct t366;
struct t7;

#include "t2465MD.h"
#define m13756(__this, p0, p1, method) (void)m12917_gshared((t2465 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13757(__this, method) (t3 *)m12918_gshared((t2465 *)__this, method)
#define m13758(__this, p0, method) (void)m12919_gshared((t2465 *)__this, (t29 *)p0, method)
#define m13759(__this, method) (t366 *)m12920_gshared((t2465 *)__this, method)
#define m13760(__this, p0, method) (void)m12921_gshared((t2465 *)__this, (t29 *)p0, method)
#define m13761(__this, method) (t7*)m12922_gshared((t2465 *)__this, method)
