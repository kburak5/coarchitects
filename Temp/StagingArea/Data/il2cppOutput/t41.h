﻿#pragma once
#include <stdint.h>
#include "t29.h"
#include "t35.h"
struct t41  : public t29
{
	int32_t f0;
	t35 f1;
};
// Native definition for marshalling of: UnityEngine.Object
struct t41_marshaled
{
	int32_t f0;
	t35 f1;
};
