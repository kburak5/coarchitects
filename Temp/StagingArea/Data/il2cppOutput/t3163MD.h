﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3163;
struct t557;
struct t7;
struct t29;
struct t556;

 void m17590_gshared (t3163 * __this, MethodInfo* method);
#define m17590(__this, method) (void)m17590_gshared((t3163 *)__this, method)
 t557 * m17591_gshared (t3163 * __this, t7* p0, t29 * p1, MethodInfo* method);
#define m17591(__this, p0, p1, method) (t557 *)m17591_gshared((t3163 *)__this, (t7*)p0, (t29 *)p1, method)
 t556 * m17592_gshared (t3163 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17592(__this, p0, p1, method) (t556 *)m17592_gshared((t3163 *)__this, (t29 *)p0, (t557 *)p1, method)
