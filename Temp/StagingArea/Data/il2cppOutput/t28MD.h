﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t28;
struct t25;
struct t16;
struct t42;
struct t29;
struct t268;

 void m2551 (t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m39 (t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m1392 (t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t28 * m1987 (t28 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2552 (t28 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1555 (t28 * __this, t42 * p0, t268 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
