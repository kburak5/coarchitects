﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1083;
struct t1083_marshaled;

void t1083_marshal(const t1083& unmarshaled, t1083_marshaled& marshaled);
void t1083_marshal_back(const t1083_marshaled& marshaled, t1083& unmarshaled);
void t1083_marshal_cleanup(t1083_marshaled& marshaled);
