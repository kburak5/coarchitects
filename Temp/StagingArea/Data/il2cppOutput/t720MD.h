﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t720;
struct t29;
struct t726;
struct t136;
struct t723;
struct t20;
struct t722;

 void m3096 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3097 (t720 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3098 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t723 * m3099 (t720 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t723 * m3100 (t720 * __this, t29 * p0, t723 ** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3101 (t720 * __this, t29 * p0, t29 * p1, t723 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3102 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3103 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3104 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3105 (t720 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3106 (t720 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3107 (t720 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3108 (t720 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3109 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3110 (t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3111 (t720 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
