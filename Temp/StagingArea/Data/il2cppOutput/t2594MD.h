﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2594;
struct t29;
struct t20;
#include "t173.h"

 void m13981 (t2594 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13982 (t2594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13983 (t2594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13984 (t2594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13985 (t2594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
