﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t323;
struct t29;
struct t6;
struct t118;
#include "t725.h"
#include "t322.h"

 void m12133 (t323 * __this, t118 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12134 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12135 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12136 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12137 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1436 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t322  m1433 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12138 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m12139 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12140 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12141 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12142 (t323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
