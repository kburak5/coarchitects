﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1165;
struct t42;
struct t7;
struct t1425;
struct t1466;

 void m8023 (t1165 * __this, t7* p0, t1425 * p1, t42 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m8024 (t1165 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1466 * m8025 (t1165 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
