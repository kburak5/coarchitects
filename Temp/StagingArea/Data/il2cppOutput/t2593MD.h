﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2593;
struct t29;
struct t20;
#include "t172.h"

 void m13976 (t2593 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13977 (t2593 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13978 (t2593 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13979 (t2593 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13980 (t2593 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
