﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2902;
struct t7;

 void m15882 (t2902 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15883 (t2902 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15884 (t2902 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
