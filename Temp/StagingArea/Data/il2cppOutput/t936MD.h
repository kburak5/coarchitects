﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t936;
struct t936_marshaled;

void t936_marshal(const t936& unmarshaled, t936_marshaled& marshaled);
void t936_marshal_back(const t936_marshaled& marshaled, t936& unmarshaled);
void t936_marshal_cleanup(t936_marshaled& marshaled);
