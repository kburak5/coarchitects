﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2504;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m13226_gshared (t2504 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m13226(__this, p0, p1, method) (void)m13226_gshared((t2504 *)__this, (t29 *)p0, (t35)p1, method)
 void m13227_gshared (t2504 * __this, t29 * p0, MethodInfo* method);
#define m13227(__this, p0, method) (void)m13227_gshared((t2504 *)__this, (t29 *)p0, method)
 t29 * m13229_gshared (t2504 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method);
#define m13229(__this, p0, p1, p2, method) (t29 *)m13229_gshared((t2504 *)__this, (t29 *)p0, (t67 *)p1, (t29 *)p2, method)
 void m13231_gshared (t2504 * __this, t29 * p0, MethodInfo* method);
#define m13231(__this, p0, method) (void)m13231_gshared((t2504 *)__this, (t29 *)p0, method)
