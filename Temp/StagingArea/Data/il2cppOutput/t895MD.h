﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t895;
struct t895_marshaled;
struct t7;

 void m3870 (t895 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t895_marshal(const t895& unmarshaled, t895_marshaled& marshaled);
void t895_marshal_back(const t895_marshaled& marshaled, t895& unmarshaled);
void t895_marshal_cleanup(t895_marshaled& marshaled);
