﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t807;
struct t796;
struct t823;
struct t822;
#include "t785.h"
#include "t784.h"
#include "t775.h"

 void m3447 (t807 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t796 * m3448 (t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t823 * m3449 (t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t822 * m3450 (t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3451 (t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3452 (t807 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
