﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2683.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2683_TI;
#include "t2683MD.h"

#include "t29.h"
#include "t235.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t20.h"
#include "t40.h"
extern TypeInfo t235_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m14544_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m21463_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m21463(__this, p0, method) (t235 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t20_0_0_1;
FieldInfo t2683_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2683_TI, offsetof(t2683, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2683_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2683_TI, offsetof(t2683, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2683_FIs[] =
{
	&t2683_f0_FieldInfo,
	&t2683_f1_FieldInfo,
	NULL
};
extern MethodInfo m14541_MI;
static PropertyInfo t2683____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2683_TI, "System.Collections.IEnumerator.Current", &m14541_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2683____Current_PropertyInfo = 
{
	&t2683_TI, "Current", &m14544_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2683_PIs[] =
{
	&t2683____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2683____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2683_m14540_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14540_GM;
MethodInfo m14540_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2683_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2683_m14540_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14540_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14541_GM;
MethodInfo m14541_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2683_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14541_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14542_GM;
MethodInfo m14542_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2683_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14542_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14543_GM;
MethodInfo m14543_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2683_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14543_GM};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14544_GM;
MethodInfo m14544_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2683_TI, &t235_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14544_GM};
static MethodInfo* t2683_MIs[] =
{
	&m14540_MI,
	&m14541_MI,
	&m14542_MI,
	&m14543_MI,
	&m14544_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m14543_MI;
extern MethodInfo m14542_MI;
static MethodInfo* t2683_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14541_MI,
	&m14543_MI,
	&m14542_MI,
	&m14544_MI,
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
extern TypeInfo t2676_TI;
static TypeInfo* t2683_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2676_TI,
};
static Il2CppInterfaceOffsetPair t2683_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2676_TI, 7},
};
extern TypeInfo t235_TI;
static Il2CppRGCTXData t2683_RGCTXData[3] = 
{
	&m14544_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m21463_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2683_0_0_0;
extern Il2CppType t2683_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2683_GC;
extern TypeInfo t20_TI;
TypeInfo t2683_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2683_MIs, t2683_PIs, t2683_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2683_TI, t2683_ITIs, t2683_VT, &EmptyCustomAttributesCache, &t2683_TI, &t2683_0_0_0, &t2683_1_0_0, t2683_IOs, &t2683_GC, NULL, NULL, NULL, t2683_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2683)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2684_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern MethodInfo m28427_MI;
extern MethodInfo m28428_MI;
static PropertyInfo t2684____Item_PropertyInfo = 
{
	&t2684_TI, "Item", &m28427_MI, &m28428_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2684_PIs[] =
{
	&t2684____Item_PropertyInfo,
	NULL
};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2684_m28429_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28429_GM;
MethodInfo m28429_MI = 
{
	"IndexOf", NULL, &t2684_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2684_m28429_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28429_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2684_m28430_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28430_GM;
MethodInfo m28430_MI = 
{
	"Insert", NULL, &t2684_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2684_m28430_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28430_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2684_m28431_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28431_GM;
MethodInfo m28431_MI = 
{
	"RemoveAt", NULL, &t2684_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2684_m28431_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28431_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2684_m28427_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28427_GM;
MethodInfo m28427_MI = 
{
	"get_Item", NULL, &t2684_TI, &t235_0_0_0, RuntimeInvoker_t29_t44, t2684_m28427_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28427_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2684_m28428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28428_GM;
MethodInfo m28428_MI = 
{
	"set_Item", NULL, &t2684_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2684_m28428_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28428_GM};
static MethodInfo* t2684_MIs[] =
{
	&m28429_MI,
	&m28430_MI,
	&m28431_MI,
	&m28427_MI,
	&m28428_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t2677_TI;
extern TypeInfo t2678_TI;
static TypeInfo* t2684_ITIs[] = 
{
	&t603_TI,
	&t2677_TI,
	&t2678_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2684_0_0_0;
extern Il2CppType t2684_1_0_0;
struct t2684;
extern Il2CppGenericClass t2684_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2684_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2684_MIs, t2684_PIs, NULL, NULL, NULL, NULL, NULL, &t2684_TI, t2684_ITIs, NULL, &t1908__CustomAttributeCache, &t2684_TI, &t2684_0_0_0, &t2684_1_0_0, NULL, &t2684_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2682.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2682_TI;
#include "t2682MD.h"

#include "t237.h"
#include "t42.h"
#include "t1101.h"
#include "UnityEngine.UI_ArrayTypes.h"
extern TypeInfo t237_TI;
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t29MD.h"
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m14548_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t237_0_0_1;
FieldInfo t2682_f0_FieldInfo = 
{
	"l", &t237_0_0_1, &t2682_TI, offsetof(t2682, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2682_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2682_TI, offsetof(t2682, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2682_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2682_TI, offsetof(t2682, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t235_0_0_1;
FieldInfo t2682_f3_FieldInfo = 
{
	"current", &t235_0_0_1, &t2682_TI, offsetof(t2682, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2682_FIs[] =
{
	&t2682_f0_FieldInfo,
	&t2682_f1_FieldInfo,
	&t2682_f2_FieldInfo,
	&t2682_f3_FieldInfo,
	NULL
};
extern MethodInfo m14546_MI;
static PropertyInfo t2682____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2682_TI, "System.Collections.IEnumerator.Current", &m14546_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14550_MI;
static PropertyInfo t2682____Current_PropertyInfo = 
{
	&t2682_TI, "Current", &m14550_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2682_PIs[] =
{
	&t2682____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2682____Current_PropertyInfo,
	NULL
};
extern Il2CppType t237_0_0_0;
extern Il2CppType t237_0_0_0;
static ParameterInfo t2682_m14545_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t237_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14545_GM;
MethodInfo m14545_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2682_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2682_m14545_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14545_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14546_GM;
MethodInfo m14546_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2682_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14546_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14547_GM;
MethodInfo m14547_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2682_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14547_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14548_GM;
MethodInfo m14548_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2682_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14548_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14549_GM;
MethodInfo m14549_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2682_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14549_GM};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14550_GM;
MethodInfo m14550_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2682_TI, &t235_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14550_GM};
static MethodInfo* t2682_MIs[] =
{
	&m14545_MI,
	&m14546_MI,
	&m14547_MI,
	&m14548_MI,
	&m14549_MI,
	&m14550_MI,
	NULL
};
extern MethodInfo m14549_MI;
extern MethodInfo m14547_MI;
static MethodInfo* t2682_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14546_MI,
	&m14549_MI,
	&m14547_MI,
	&m14550_MI,
};
static TypeInfo* t2682_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2676_TI,
};
static Il2CppInterfaceOffsetPair t2682_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2676_TI, 7},
};
extern TypeInfo t235_TI;
extern TypeInfo t2682_TI;
static Il2CppRGCTXData t2682_RGCTXData[3] = 
{
	&m14548_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&t2682_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2682_0_0_0;
extern Il2CppType t2682_1_0_0;
extern Il2CppGenericClass t2682_GC;
extern TypeInfo t1261_TI;
TypeInfo t2682_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2682_MIs, t2682_PIs, t2682_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2682_TI, t2682_ITIs, t2682_VT, &EmptyCustomAttributesCache, &t2682_TI, &t2682_0_0_0, &t2682_1_0_0, t2682_IOs, &t2682_GC, NULL, NULL, NULL, t2682_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2682)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#include "t2679.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2679_TI;
#include "t2679MD.h"

#include "t345.h"
#include "t338.h"
extern TypeInfo t44_TI;
extern TypeInfo t345_TI;
extern TypeInfo t338_TI;
extern TypeInfo t674_TI;
extern TypeInfo t21_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2675_TI;
#include "t345MD.h"
#include "t338MD.h"
#include "t2685MD.h"
extern MethodInfo m14580_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m28418_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m14612_MI;
extern MethodInfo m28425_MI;
extern MethodInfo m28429_MI;
extern MethodInfo m28419_MI;
extern MethodInfo m28420_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t2684_0_0_1;
FieldInfo t2679_f0_FieldInfo = 
{
	"list", &t2684_0_0_1, &t2679_TI, offsetof(t2679, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2679_FIs[] =
{
	&t2679_f0_FieldInfo,
	NULL
};
extern MethodInfo m14557_MI;
extern MethodInfo m14558_MI;
static PropertyInfo t2679____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2679_TI, "System.Collections.Generic.IList<T>.Item", &m14557_MI, &m14558_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14559_MI;
static PropertyInfo t2679____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2679_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14559_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14569_MI;
static PropertyInfo t2679____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2679_TI, "System.Collections.ICollection.IsSynchronized", &m14569_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14570_MI;
static PropertyInfo t2679____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2679_TI, "System.Collections.ICollection.SyncRoot", &m14570_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14571_MI;
static PropertyInfo t2679____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2679_TI, "System.Collections.IList.IsFixedSize", &m14571_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14572_MI;
static PropertyInfo t2679____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2679_TI, "System.Collections.IList.IsReadOnly", &m14572_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14573_MI;
extern MethodInfo m14574_MI;
static PropertyInfo t2679____System_Collections_IList_Item_PropertyInfo = 
{
	&t2679_TI, "System.Collections.IList.Item", &m14573_MI, &m14574_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14579_MI;
static PropertyInfo t2679____Count_PropertyInfo = 
{
	&t2679_TI, "Count", &m14579_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2679____Item_PropertyInfo = 
{
	&t2679_TI, "Item", &m14580_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2679_PIs[] =
{
	&t2679____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2679____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2679____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2679____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2679____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2679____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2679____System_Collections_IList_Item_PropertyInfo,
	&t2679____Count_PropertyInfo,
	&t2679____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2684_0_0_0;
static ParameterInfo t2679_m14551_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2684_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14551_GM;
MethodInfo m14551_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2679_m14551_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14551_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2679_m14552_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14552_GM;
MethodInfo m14552_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2679_m14552_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14552_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14553_GM;
MethodInfo m14553_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14553_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2679_m14554_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14554_GM;
MethodInfo m14554_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2679_m14554_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14554_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2679_m14555_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14555_GM;
MethodInfo m14555_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2679_m14555_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14555_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14556_GM;
MethodInfo m14556_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2679_m14556_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14556_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14557_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14557_GM;
MethodInfo m14557_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2679_TI, &t235_0_0_0, RuntimeInvoker_t29_t44, t2679_m14557_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14557_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2679_m14558_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14558_GM;
MethodInfo m14558_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2679_m14558_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14558_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14559_GM;
MethodInfo m14559_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14559_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14560_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14560_GM;
MethodInfo m14560_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2679_m14560_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14560_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14561_GM;
MethodInfo m14561_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2679_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14561_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2679_m14562_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14562_GM;
MethodInfo m14562_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2679_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2679_m14562_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14562_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14563_GM;
MethodInfo m14563_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14563_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2679_m14564_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14564_GM;
MethodInfo m14564_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2679_m14564_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14564_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2679_m14565_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14565_GM;
MethodInfo m14565_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2679_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2679_m14565_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14565_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2679_m14566_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14566_GM;
MethodInfo m14566_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2679_m14566_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14566_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2679_m14567_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14567_GM;
MethodInfo m14567_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2679_m14567_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14567_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14568_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14568_GM;
MethodInfo m14568_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2679_m14568_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14568_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14569_GM;
MethodInfo m14569_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14569_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14570_GM;
MethodInfo m14570_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2679_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14570_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14571_GM;
MethodInfo m14571_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14571_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14572_GM;
MethodInfo m14572_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14572_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14573_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14573_GM;
MethodInfo m14573_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2679_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2679_m14573_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14573_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2679_m14574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14574_GM;
MethodInfo m14574_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2679_m14574_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14574_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2679_m14575_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14575_GM;
MethodInfo m14575_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2679_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2679_m14575_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14575_GM};
extern Il2CppType t2675_0_0_0;
extern Il2CppType t2675_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14576_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2675_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14576_GM;
MethodInfo m14576_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2679_m14576_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14576_GM};
extern Il2CppType t2676_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14577_GM;
MethodInfo m14577_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2679_TI, &t2676_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14577_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2679_m14578_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14578_GM;
MethodInfo m14578_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2679_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2679_m14578_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14578_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14579_GM;
MethodInfo m14579_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2679_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14579_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2679_m14580_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14580_GM;
MethodInfo m14580_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2679_TI, &t235_0_0_0, RuntimeInvoker_t29_t44, t2679_m14580_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14580_GM};
static MethodInfo* t2679_MIs[] =
{
	&m14551_MI,
	&m14552_MI,
	&m14553_MI,
	&m14554_MI,
	&m14555_MI,
	&m14556_MI,
	&m14557_MI,
	&m14558_MI,
	&m14559_MI,
	&m14560_MI,
	&m14561_MI,
	&m14562_MI,
	&m14563_MI,
	&m14564_MI,
	&m14565_MI,
	&m14566_MI,
	&m14567_MI,
	&m14568_MI,
	&m14569_MI,
	&m14570_MI,
	&m14571_MI,
	&m14572_MI,
	&m14573_MI,
	&m14574_MI,
	&m14575_MI,
	&m14576_MI,
	&m14577_MI,
	&m14578_MI,
	&m14579_MI,
	&m14580_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m14561_MI;
extern MethodInfo m14560_MI;
extern MethodInfo m14562_MI;
extern MethodInfo m14563_MI;
extern MethodInfo m14564_MI;
extern MethodInfo m14565_MI;
extern MethodInfo m14566_MI;
extern MethodInfo m14567_MI;
extern MethodInfo m14568_MI;
extern MethodInfo m14552_MI;
extern MethodInfo m14553_MI;
extern MethodInfo m14575_MI;
extern MethodInfo m14576_MI;
extern MethodInfo m14555_MI;
extern MethodInfo m14578_MI;
extern MethodInfo m14554_MI;
extern MethodInfo m14556_MI;
extern MethodInfo m14577_MI;
static MethodInfo* t2679_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14561_MI,
	&m14579_MI,
	&m14569_MI,
	&m14570_MI,
	&m14560_MI,
	&m14571_MI,
	&m14572_MI,
	&m14573_MI,
	&m14574_MI,
	&m14562_MI,
	&m14563_MI,
	&m14564_MI,
	&m14565_MI,
	&m14566_MI,
	&m14567_MI,
	&m14568_MI,
	&m14579_MI,
	&m14559_MI,
	&m14552_MI,
	&m14553_MI,
	&m14575_MI,
	&m14576_MI,
	&m14555_MI,
	&m14578_MI,
	&m14554_MI,
	&m14556_MI,
	&m14557_MI,
	&m14558_MI,
	&m14577_MI,
	&m14580_MI,
};
extern TypeInfo t868_TI;
static TypeInfo* t2679_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2677_TI,
	&t2684_TI,
	&t2678_TI,
};
static Il2CppInterfaceOffsetPair t2679_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2677_TI, 20},
	{ &t2684_TI, 27},
	{ &t2678_TI, 32},
};
extern TypeInfo t235_TI;
static Il2CppRGCTXData t2679_RGCTXData[9] = 
{
	&m14580_MI/* Method Usage */,
	&m14612_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m28425_MI/* Method Usage */,
	&m28429_MI/* Method Usage */,
	&m28427_MI/* Method Usage */,
	&m28419_MI/* Method Usage */,
	&m28420_MI/* Method Usage */,
	&m28418_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2679_0_0_0;
extern Il2CppType t2679_1_0_0;
extern TypeInfo t29_TI;
struct t2679;
extern Il2CppGenericClass t2679_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2679_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2679_MIs, t2679_PIs, t2679_FIs, NULL, &t29_TI, NULL, NULL, &t2679_TI, t2679_ITIs, t2679_VT, &t1263__CustomAttributeCache, &t2679_TI, &t2679_0_0_0, &t2679_1_0_0, t2679_IOs, &t2679_GC, NULL, NULL, NULL, t2679_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2679), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2685.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2685_TI;

#include "t43.h"
#include "t305.h"
extern TypeInfo t305_TI;
#include "t237MD.h"
#include "t305MD.h"
extern MethodInfo m28422_MI;
extern MethodInfo m14615_MI;
extern MethodInfo m14616_MI;
extern MethodInfo m14613_MI;
extern MethodInfo m14611_MI;
extern MethodInfo m1902_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m14604_MI;
extern MethodInfo m14614_MI;
extern MethodInfo m14602_MI;
extern MethodInfo m14607_MI;
extern MethodInfo m14598_MI;
extern MethodInfo m28424_MI;
extern MethodInfo m28430_MI;
extern MethodInfo m28431_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t2684_0_0_1;
FieldInfo t2685_f0_FieldInfo = 
{
	"list", &t2684_0_0_1, &t2685_TI, offsetof(t2685, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2685_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2685_TI, offsetof(t2685, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2685_FIs[] =
{
	&t2685_f0_FieldInfo,
	&t2685_f1_FieldInfo,
	NULL
};
extern MethodInfo m14582_MI;
static PropertyInfo t2685____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2685_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14582_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14590_MI;
static PropertyInfo t2685____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2685_TI, "System.Collections.ICollection.IsSynchronized", &m14590_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14591_MI;
static PropertyInfo t2685____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2685_TI, "System.Collections.ICollection.SyncRoot", &m14591_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14592_MI;
static PropertyInfo t2685____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2685_TI, "System.Collections.IList.IsFixedSize", &m14592_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14593_MI;
static PropertyInfo t2685____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2685_TI, "System.Collections.IList.IsReadOnly", &m14593_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14594_MI;
extern MethodInfo m14595_MI;
static PropertyInfo t2685____System_Collections_IList_Item_PropertyInfo = 
{
	&t2685_TI, "System.Collections.IList.Item", &m14594_MI, &m14595_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14608_MI;
static PropertyInfo t2685____Count_PropertyInfo = 
{
	&t2685_TI, "Count", &m14608_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14609_MI;
extern MethodInfo m14610_MI;
static PropertyInfo t2685____Item_PropertyInfo = 
{
	&t2685_TI, "Item", &m14609_MI, &m14610_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2685_PIs[] =
{
	&t2685____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2685____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2685____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2685____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2685____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2685____System_Collections_IList_Item_PropertyInfo,
	&t2685____Count_PropertyInfo,
	&t2685____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14581_GM;
MethodInfo m14581_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14581_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14582_GM;
MethodInfo m14582_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14582_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2685_m14583_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14583_GM;
MethodInfo m14583_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2685_m14583_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14583_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14584_GM;
MethodInfo m14584_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2685_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14584_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14585_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14585_GM;
MethodInfo m14585_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2685_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2685_m14585_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14585_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14586_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14586_GM;
MethodInfo m14586_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2685_m14586_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14586_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14587_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14587_GM;
MethodInfo m14587_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2685_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2685_m14587_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14587_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14588_GM;
MethodInfo m14588_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2685_m14588_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14588_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14589_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14589_GM;
MethodInfo m14589_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2685_m14589_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14589_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14590_GM;
MethodInfo m14590_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14590_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14591_GM;
MethodInfo m14591_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2685_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14591_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14592_GM;
MethodInfo m14592_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14592_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14593_GM;
MethodInfo m14593_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14593_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2685_m14594_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14594_GM;
MethodInfo m14594_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2685_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2685_m14594_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14594_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14595_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14595_GM;
MethodInfo m14595_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2685_m14595_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14595_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14596_GM;
MethodInfo m14596_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2685_m14596_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14596_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14597_GM;
MethodInfo m14597_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14597_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14598_GM;
MethodInfo m14598_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14598_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14599_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14599_GM;
MethodInfo m14599_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2685_m14599_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14599_GM};
extern Il2CppType t2675_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2685_m14600_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2675_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14600_GM;
MethodInfo m14600_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2685_m14600_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14600_GM};
extern Il2CppType t2676_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14601_GM;
MethodInfo m14601_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2685_TI, &t2676_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14601_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14602_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14602_GM;
MethodInfo m14602_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2685_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2685_m14602_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14602_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14603_GM;
MethodInfo m14603_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2685_m14603_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14603_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14604_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14604_GM;
MethodInfo m14604_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2685_m14604_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14604_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14605_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14605_GM;
MethodInfo m14605_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2685_m14605_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14605_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2685_m14606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14606_GM;
MethodInfo m14606_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2685_m14606_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14606_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2685_m14607_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14607_GM;
MethodInfo m14607_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2685_m14607_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14607_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14608_GM;
MethodInfo m14608_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2685_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14608_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2685_m14609_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14609_GM;
MethodInfo m14609_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2685_TI, &t235_0_0_0, RuntimeInvoker_t29_t44, t2685_m14609_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14609_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14610_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14610_GM;
MethodInfo m14610_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2685_m14610_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14610_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2685_m14611_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14611_GM;
MethodInfo m14611_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2685_m14611_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14611_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14612_GM;
MethodInfo m14612_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2685_m14612_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14612_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2685_m14613_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14613_GM;
MethodInfo m14613_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2685_TI, &t235_0_0_0, RuntimeInvoker_t29_t29, t2685_m14613_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14613_GM};
extern Il2CppType t2684_0_0_0;
static ParameterInfo t2685_m14614_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2684_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14614_GM;
MethodInfo m14614_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2685_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2685_m14614_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14614_GM};
extern Il2CppType t2684_0_0_0;
static ParameterInfo t2685_m14615_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2684_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14615_GM;
MethodInfo m14615_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2685_m14615_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14615_GM};
extern Il2CppType t2684_0_0_0;
static ParameterInfo t2685_m14616_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2684_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14616_GM;
MethodInfo m14616_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2685_m14616_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14616_GM};
static MethodInfo* t2685_MIs[] =
{
	&m14581_MI,
	&m14582_MI,
	&m14583_MI,
	&m14584_MI,
	&m14585_MI,
	&m14586_MI,
	&m14587_MI,
	&m14588_MI,
	&m14589_MI,
	&m14590_MI,
	&m14591_MI,
	&m14592_MI,
	&m14593_MI,
	&m14594_MI,
	&m14595_MI,
	&m14596_MI,
	&m14597_MI,
	&m14598_MI,
	&m14599_MI,
	&m14600_MI,
	&m14601_MI,
	&m14602_MI,
	&m14603_MI,
	&m14604_MI,
	&m14605_MI,
	&m14606_MI,
	&m14607_MI,
	&m14608_MI,
	&m14609_MI,
	&m14610_MI,
	&m14611_MI,
	&m14612_MI,
	&m14613_MI,
	&m14614_MI,
	&m14615_MI,
	&m14616_MI,
	NULL
};
extern MethodInfo m14584_MI;
extern MethodInfo m14583_MI;
extern MethodInfo m14585_MI;
extern MethodInfo m14597_MI;
extern MethodInfo m14586_MI;
extern MethodInfo m14587_MI;
extern MethodInfo m14588_MI;
extern MethodInfo m14589_MI;
extern MethodInfo m14606_MI;
extern MethodInfo m14596_MI;
extern MethodInfo m14599_MI;
extern MethodInfo m14600_MI;
extern MethodInfo m14605_MI;
extern MethodInfo m14603_MI;
extern MethodInfo m14601_MI;
static MethodInfo* t2685_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14584_MI,
	&m14608_MI,
	&m14590_MI,
	&m14591_MI,
	&m14583_MI,
	&m14592_MI,
	&m14593_MI,
	&m14594_MI,
	&m14595_MI,
	&m14585_MI,
	&m14597_MI,
	&m14586_MI,
	&m14587_MI,
	&m14588_MI,
	&m14589_MI,
	&m14606_MI,
	&m14608_MI,
	&m14582_MI,
	&m14596_MI,
	&m14597_MI,
	&m14599_MI,
	&m14600_MI,
	&m14605_MI,
	&m14602_MI,
	&m14603_MI,
	&m14606_MI,
	&m14609_MI,
	&m14610_MI,
	&m14601_MI,
	&m14598_MI,
	&m14604_MI,
	&m14607_MI,
	&m14611_MI,
};
static TypeInfo* t2685_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2677_TI,
	&t2684_TI,
	&t2678_TI,
};
static Il2CppInterfaceOffsetPair t2685_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2677_TI, 20},
	{ &t2684_TI, 27},
	{ &t2678_TI, 32},
};
extern TypeInfo t237_TI;
extern TypeInfo t235_TI;
static Il2CppRGCTXData t2685_RGCTXData[25] = 
{
	&t237_TI/* Class Usage */,
	&m1902_MI/* Method Usage */,
	&m28422_MI/* Method Usage */,
	&m28420_MI/* Method Usage */,
	&m28418_MI/* Method Usage */,
	&m14613_MI/* Method Usage */,
	&m14604_MI/* Method Usage */,
	&m14612_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m28425_MI/* Method Usage */,
	&m28429_MI/* Method Usage */,
	&m14614_MI/* Method Usage */,
	&m14602_MI/* Method Usage */,
	&m14607_MI/* Method Usage */,
	&m14615_MI/* Method Usage */,
	&m14616_MI/* Method Usage */,
	&m28427_MI/* Method Usage */,
	&m14611_MI/* Method Usage */,
	&m14598_MI/* Method Usage */,
	&m28424_MI/* Method Usage */,
	&m28419_MI/* Method Usage */,
	&m28430_MI/* Method Usage */,
	&m28431_MI/* Method Usage */,
	&m28428_MI/* Method Usage */,
	&t235_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2685_0_0_0;
extern Il2CppType t2685_1_0_0;
struct t2685;
extern Il2CppGenericClass t2685_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2685_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2685_MIs, t2685_PIs, t2685_FIs, NULL, &t29_TI, NULL, NULL, &t2685_TI, t2685_ITIs, t2685_VT, &t1262__CustomAttributeCache, &t2685_TI, &t2685_0_0_0, &t2685_1_0_0, t2685_IOs, &t2685_GC, NULL, NULL, NULL, t2685_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2685), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#include "t2686.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2686_TI;
#include "t2686MD.h"

#include "t1257.h"
#include "mscorlib_ArrayTypes.h"
#include "t2687.h"
extern TypeInfo t6704_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2687_TI;
#include "t931MD.h"
#include "t2687MD.h"
extern Il2CppType t6704_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m14622_MI;
extern MethodInfo m28432_MI;
extern MethodInfo m21475_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t2686_0_0_49;
FieldInfo t2686_f0_FieldInfo = 
{
	"_default", &t2686_0_0_49, &t2686_TI, offsetof(t2686_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2686_FIs[] =
{
	&t2686_f0_FieldInfo,
	NULL
};
extern MethodInfo m14621_MI;
static PropertyInfo t2686____Default_PropertyInfo = 
{
	&t2686_TI, "Default", &m14621_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2686_PIs[] =
{
	&t2686____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14617_GM;
MethodInfo m14617_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2686_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14617_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14618_GM;
MethodInfo m14618_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2686_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14618_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2686_m14619_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14619_GM;
MethodInfo m14619_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2686_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2686_m14619_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14619_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2686_m14620_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14620_GM;
MethodInfo m14620_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2686_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2686_m14620_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14620_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2686_m28432_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28432_GM;
MethodInfo m28432_MI = 
{
	"GetHashCode", NULL, &t2686_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2686_m28432_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28432_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2686_m21475_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21475_GM;
MethodInfo m21475_MI = 
{
	"Equals", NULL, &t2686_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2686_m21475_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21475_GM};
extern Il2CppType t2686_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14621_GM;
MethodInfo m14621_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2686_TI, &t2686_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14621_GM};
static MethodInfo* t2686_MIs[] =
{
	&m14617_MI,
	&m14618_MI,
	&m14619_MI,
	&m14620_MI,
	&m28432_MI,
	&m21475_MI,
	&m14621_MI,
	NULL
};
extern MethodInfo m14620_MI;
extern MethodInfo m14619_MI;
static MethodInfo* t2686_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m21475_MI,
	&m28432_MI,
	&m14620_MI,
	&m14619_MI,
	NULL,
	NULL,
};
extern TypeInfo t6705_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2686_ITIs[] = 
{
	&t6705_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2686_IOs[] = 
{
	{ &t6705_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2686_TI;
extern TypeInfo t2686_TI;
extern TypeInfo t2687_TI;
extern TypeInfo t235_TI;
static Il2CppRGCTXData t2686_RGCTXData[9] = 
{
	&t6704_0_0_0/* Type Usage */,
	&t235_0_0_0/* Type Usage */,
	&t2686_TI/* Class Usage */,
	&t2686_TI/* Static Usage */,
	&t2687_TI/* Class Usage */,
	&m14622_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m28432_MI/* Method Usage */,
	&m21475_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2686_0_0_0;
extern Il2CppType t2686_1_0_0;
struct t2686;
extern Il2CppGenericClass t2686_GC;
TypeInfo t2686_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2686_MIs, t2686_PIs, t2686_FIs, NULL, &t29_TI, NULL, NULL, &t2686_TI, t2686_ITIs, t2686_VT, &EmptyCustomAttributesCache, &t2686_TI, &t2686_0_0_0, &t2686_1_0_0, t2686_IOs, &t2686_GC, NULL, NULL, NULL, t2686_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2686), 0, -1, sizeof(t2686_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t6705_m28433_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28433_GM;
MethodInfo m28433_MI = 
{
	"Equals", NULL, &t6705_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6705_m28433_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28433_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t6705_m28434_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28434_GM;
MethodInfo m28434_MI = 
{
	"GetHashCode", NULL, &t6705_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6705_m28434_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28434_GM};
static MethodInfo* t6705_MIs[] =
{
	&m28433_MI,
	&m28434_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6705_0_0_0;
extern Il2CppType t6705_1_0_0;
struct t6705;
extern Il2CppGenericClass t6705_GC;
TypeInfo t6705_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6705_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6705_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6705_TI, &t6705_0_0_0, &t6705_1_0_0, NULL, &t6705_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t235_0_0_0;
static ParameterInfo t6704_m28435_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28435_GM;
MethodInfo m28435_MI = 
{
	"Equals", NULL, &t6704_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6704_m28435_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28435_GM};
static MethodInfo* t6704_MIs[] =
{
	&m28435_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6704_1_0_0;
struct t6704;
extern Il2CppGenericClass t6704_GC;
TypeInfo t6704_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6704_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6704_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6704_TI, &t6704_0_0_0, &t6704_1_0_0, NULL, &t6704_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14617_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14622_GM;
MethodInfo m14622_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2687_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14622_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2687_m14623_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14623_GM;
MethodInfo m14623_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2687_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2687_m14623_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14623_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2687_m14624_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14624_GM;
MethodInfo m14624_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2687_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2687_m14624_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14624_GM};
static MethodInfo* t2687_MIs[] =
{
	&m14622_MI,
	&m14623_MI,
	&m14624_MI,
	NULL
};
extern MethodInfo m14624_MI;
extern MethodInfo m14623_MI;
static MethodInfo* t2687_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14624_MI,
	&m14623_MI,
	&m14620_MI,
	&m14619_MI,
	&m14623_MI,
	&m14624_MI,
};
static Il2CppInterfaceOffsetPair t2687_IOs[] = 
{
	{ &t6705_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2686_TI;
extern TypeInfo t2686_TI;
extern TypeInfo t2687_TI;
extern TypeInfo t235_TI;
extern TypeInfo t235_TI;
static Il2CppRGCTXData t2687_RGCTXData[11] = 
{
	&t6704_0_0_0/* Type Usage */,
	&t235_0_0_0/* Type Usage */,
	&t2686_TI/* Class Usage */,
	&t2686_TI/* Static Usage */,
	&t2687_TI/* Class Usage */,
	&m14622_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m28432_MI/* Method Usage */,
	&m21475_MI/* Method Usage */,
	&m14617_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2687_0_0_0;
extern Il2CppType t2687_1_0_0;
struct t2687;
extern Il2CppGenericClass t2687_GC;
extern TypeInfo t1256_TI;
TypeInfo t2687_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2687_MIs, NULL, NULL, NULL, &t2686_TI, NULL, &t1256_TI, &t2687_TI, NULL, t2687_VT, &EmptyCustomAttributesCache, &t2687_TI, &t2687_0_0_0, &t2687_1_0_0, t2687_IOs, &t2687_GC, NULL, NULL, NULL, t2687_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2687), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t2680.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2680_TI;
#include "t2680MD.h"

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2680_m14625_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14625_GM;
MethodInfo m14625_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2680_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2680_m14625_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14625_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2680_m14626_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14626_GM;
MethodInfo m14626_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2680_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2680_m14626_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14626_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2680_m14627_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14627_GM;
MethodInfo m14627_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2680_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2680_m14627_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14627_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2680_m14628_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14628_GM;
MethodInfo m14628_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2680_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2680_m14628_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14628_GM};
static MethodInfo* t2680_MIs[] =
{
	&m14625_MI,
	&m14626_MI,
	&m14627_MI,
	&m14628_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m14626_MI;
extern MethodInfo m14627_MI;
extern MethodInfo m14628_MI;
static MethodInfo* t2680_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14626_MI,
	&m14627_MI,
	&m14628_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2680_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2680_0_0_0;
extern Il2CppType t2680_1_0_0;
extern TypeInfo t195_TI;
struct t2680;
extern Il2CppGenericClass t2680_GC;
TypeInfo t2680_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2680_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2680_TI, NULL, t2680_VT, &EmptyCustomAttributesCache, &t2680_TI, &t2680_0_0_0, &t2680_1_0_0, t2680_IOs, &t2680_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2680), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2688.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2688_TI;
#include "t2688MD.h"

#include "t1247.h"
#include "t2689.h"
extern TypeInfo t4248_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2689_TI;
#include "t2689MD.h"
extern Il2CppType t4248_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m14633_MI;
extern MethodInfo m28436_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t2688_0_0_49;
FieldInfo t2688_f0_FieldInfo = 
{
	"_default", &t2688_0_0_49, &t2688_TI, offsetof(t2688_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2688_FIs[] =
{
	&t2688_f0_FieldInfo,
	NULL
};
extern MethodInfo m14632_MI;
static PropertyInfo t2688____Default_PropertyInfo = 
{
	&t2688_TI, "Default", &m14632_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2688_PIs[] =
{
	&t2688____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14629_GM;
MethodInfo m14629_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2688_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14629_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14630_GM;
MethodInfo m14630_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2688_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14630_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2688_m14631_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14631_GM;
MethodInfo m14631_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2688_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2688_m14631_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14631_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2688_m28436_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28436_GM;
MethodInfo m28436_MI = 
{
	"Compare", NULL, &t2688_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2688_m28436_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28436_GM};
extern Il2CppType t2688_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14632_GM;
MethodInfo m14632_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2688_TI, &t2688_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14632_GM};
static MethodInfo* t2688_MIs[] =
{
	&m14629_MI,
	&m14630_MI,
	&m14631_MI,
	&m28436_MI,
	&m14632_MI,
	NULL
};
extern MethodInfo m14631_MI;
static MethodInfo* t2688_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m28436_MI,
	&m14631_MI,
	NULL,
};
extern TypeInfo t4247_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2688_ITIs[] = 
{
	&t4247_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2688_IOs[] = 
{
	{ &t4247_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2688_TI;
extern TypeInfo t2688_TI;
extern TypeInfo t2689_TI;
extern TypeInfo t235_TI;
static Il2CppRGCTXData t2688_RGCTXData[8] = 
{
	&t4248_0_0_0/* Type Usage */,
	&t235_0_0_0/* Type Usage */,
	&t2688_TI/* Class Usage */,
	&t2688_TI/* Static Usage */,
	&t2689_TI/* Class Usage */,
	&m14633_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m28436_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2688_0_0_0;
extern Il2CppType t2688_1_0_0;
struct t2688;
extern Il2CppGenericClass t2688_GC;
TypeInfo t2688_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2688_MIs, t2688_PIs, t2688_FIs, NULL, &t29_TI, NULL, NULL, &t2688_TI, t2688_ITIs, t2688_VT, &EmptyCustomAttributesCache, &t2688_TI, &t2688_0_0_0, &t2688_1_0_0, t2688_IOs, &t2688_GC, NULL, NULL, NULL, t2688_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2688), 0, -1, sizeof(t2688_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t4247_m21483_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21483_GM;
MethodInfo m21483_MI = 
{
	"Compare", NULL, &t4247_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4247_m21483_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21483_GM};
static MethodInfo* t4247_MIs[] =
{
	&m21483_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4247_0_0_0;
extern Il2CppType t4247_1_0_0;
struct t4247;
extern Il2CppGenericClass t4247_GC;
TypeInfo t4247_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4247_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4247_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4247_TI, &t4247_0_0_0, &t4247_1_0_0, NULL, &t4247_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t235_0_0_0;
static ParameterInfo t4248_m21484_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21484_GM;
MethodInfo m21484_MI = 
{
	"CompareTo", NULL, &t4248_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4248_m21484_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m21484_GM};
static MethodInfo* t4248_MIs[] =
{
	&m21484_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4248_1_0_0;
struct t4248;
extern Il2CppGenericClass t4248_GC;
TypeInfo t4248_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4248_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4248_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4248_TI, &t4248_0_0_0, &t4248_1_0_0, NULL, &t4248_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m14629_MI;
extern MethodInfo m21484_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14633_GM;
MethodInfo m14633_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2689_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14633_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2689_m14634_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14634_GM;
MethodInfo m14634_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2689_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2689_m14634_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14634_GM};
static MethodInfo* t2689_MIs[] =
{
	&m14633_MI,
	&m14634_MI,
	NULL
};
extern MethodInfo m14634_MI;
static MethodInfo* t2689_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14634_MI,
	&m14631_MI,
	&m14634_MI,
};
static Il2CppInterfaceOffsetPair t2689_IOs[] = 
{
	{ &t4247_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2688_TI;
extern TypeInfo t2688_TI;
extern TypeInfo t2689_TI;
extern TypeInfo t235_TI;
extern TypeInfo t235_TI;
extern TypeInfo t4248_TI;
static Il2CppRGCTXData t2689_RGCTXData[12] = 
{
	&t4248_0_0_0/* Type Usage */,
	&t235_0_0_0/* Type Usage */,
	&t2688_TI/* Class Usage */,
	&t2688_TI/* Static Usage */,
	&t2689_TI/* Class Usage */,
	&m14633_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&m28436_MI/* Method Usage */,
	&m14629_MI/* Method Usage */,
	&t235_TI/* Class Usage */,
	&t4248_TI/* Class Usage */,
	&m21484_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2689_0_0_0;
extern Il2CppType t2689_1_0_0;
struct t2689;
extern Il2CppGenericClass t2689_GC;
extern TypeInfo t1246_TI;
TypeInfo t2689_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2689_MIs, NULL, NULL, NULL, &t2688_TI, NULL, &t1246_TI, &t2689_TI, NULL, t2689_VT, &EmptyCustomAttributesCache, &t2689_TI, &t2689_0_0_0, &t2689_1_0_0, t2689_IOs, &t2689_GC, NULL, NULL, NULL, t2689_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2689), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2681.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2681_TI;
#include "t2681MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2681_m14635_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14635_GM;
MethodInfo m14635_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2681_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2681_m14635_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14635_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t2681_m14636_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14636_GM;
MethodInfo m14636_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2681_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2681_m14636_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14636_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2681_m14637_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14637_GM;
MethodInfo m14637_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2681_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2681_m14637_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m14637_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2681_m14638_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14638_GM;
MethodInfo m14638_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2681_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2681_m14638_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14638_GM};
static MethodInfo* t2681_MIs[] =
{
	&m14635_MI,
	&m14636_MI,
	&m14637_MI,
	&m14638_MI,
	NULL
};
extern MethodInfo m14636_MI;
extern MethodInfo m14637_MI;
extern MethodInfo m14638_MI;
static MethodInfo* t2681_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14636_MI,
	&m14637_MI,
	&m14638_MI,
};
static Il2CppInterfaceOffsetPair t2681_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2681_0_0_0;
extern Il2CppType t2681_1_0_0;
struct t2681;
extern Il2CppGenericClass t2681_GC;
TypeInfo t2681_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2681_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2681_TI, NULL, t2681_VT, &EmptyCustomAttributesCache, &t2681_TI, &t2681_0_0_0, &t2681_1_0_0, t2681_IOs, &t2681_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2681), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2690.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2690_TI;
#include "t2690MD.h"

#include "t41.h"
#include "t557.h"
#include "t15.h"
#include "t2691.h"
extern TypeInfo t316_TI;
extern TypeInfo t15_TI;
extern TypeInfo t2691_TI;
#include "t2691MD.h"
extern MethodInfo m14641_MI;
extern MethodInfo m14643_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Text>
extern Il2CppType t316_0_0_33;
FieldInfo t2690_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2690_TI, offsetof(t2690, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2690_FIs[] =
{
	&t2690_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2690_m14639_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14639_GM;
MethodInfo m14639_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2690_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2690_m14639_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14639_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2690_m14640_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14640_GM;
MethodInfo m14640_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2690_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2690_m14640_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14640_GM};
static MethodInfo* t2690_MIs[] =
{
	&m14639_MI,
	&m14640_MI,
	NULL
};
extern MethodInfo m14640_MI;
extern MethodInfo m14644_MI;
static MethodInfo* t2690_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14640_MI,
	&m14644_MI,
};
extern Il2CppType t2692_0_0_0;
extern TypeInfo t2692_TI;
extern MethodInfo m21488_MI;
extern TypeInfo t15_TI;
extern MethodInfo m14646_MI;
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2690_RGCTXData[8] = 
{
	&t2692_0_0_0/* Type Usage */,
	&t2692_TI/* Class Usage */,
	&m21488_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m14646_MI/* Method Usage */,
	&m14641_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m14643_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2690_0_0_0;
extern Il2CppType t2690_1_0_0;
struct t2690;
extern Il2CppGenericClass t2690_GC;
TypeInfo t2690_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2690_MIs, NULL, t2690_FIs, NULL, &t2691_TI, NULL, NULL, &t2690_TI, NULL, t2690_VT, &EmptyCustomAttributesCache, &t2690_TI, &t2690_0_0_0, &t2690_1_0_0, NULL, &t2690_GC, NULL, NULL, NULL, t2690_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2690), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2692.h"
#include "t353.h"
extern TypeInfo t2692_TI;
#include "t556MD.h"
#include "t353MD.h"
#include "t2692MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m21488(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Text>
extern Il2CppType t2692_0_0_1;
FieldInfo t2691_f0_FieldInfo = 
{
	"Delegate", &t2692_0_0_1, &t2691_TI, offsetof(t2691, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2691_FIs[] =
{
	&t2691_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2691_m14641_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14641_GM;
MethodInfo m14641_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2691_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2691_m14641_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14641_GM};
extern Il2CppType t2692_0_0_0;
static ParameterInfo t2691_m14642_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2692_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14642_GM;
MethodInfo m14642_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2691_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2691_m14642_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14642_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2691_m14643_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14643_GM;
MethodInfo m14643_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2691_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2691_m14643_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14643_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2691_m14644_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14644_GM;
MethodInfo m14644_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2691_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2691_m14644_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14644_GM};
static MethodInfo* t2691_MIs[] =
{
	&m14641_MI,
	&m14642_MI,
	&m14643_MI,
	&m14644_MI,
	NULL
};
static MethodInfo* t2691_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14643_MI,
	&m14644_MI,
};
extern TypeInfo t2692_TI;
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2691_RGCTXData[5] = 
{
	&t2692_0_0_0/* Type Usage */,
	&t2692_TI/* Class Usage */,
	&m21488_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m14646_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2691_0_0_0;
extern Il2CppType t2691_1_0_0;
extern TypeInfo t556_TI;
struct t2691;
extern Il2CppGenericClass t2691_GC;
TypeInfo t2691_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2691_MIs, NULL, t2691_FIs, NULL, &t556_TI, NULL, NULL, &t2691_TI, NULL, t2691_VT, &EmptyCustomAttributesCache, &t2691_TI, &t2691_0_0_0, &t2691_1_0_0, NULL, &t2691_GC, NULL, NULL, NULL, t2691_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2691), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Text>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2692_m14645_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14645_GM;
MethodInfo m14645_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2692_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2692_m14645_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14645_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2692_m14646_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14646_GM;
MethodInfo m14646_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2692_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2692_m14646_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14646_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2692_m14647_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14647_GM;
MethodInfo m14647_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2692_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2692_m14647_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14647_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2692_m14648_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14648_GM;
MethodInfo m14648_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2692_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2692_m14648_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14648_GM};
static MethodInfo* t2692_MIs[] =
{
	&m14645_MI,
	&m14646_MI,
	&m14647_MI,
	&m14648_MI,
	NULL
};
extern MethodInfo m14647_MI;
extern MethodInfo m14648_MI;
static MethodInfo* t2692_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14646_MI,
	&m14647_MI,
	&m14648_MI,
};
static Il2CppInterfaceOffsetPair t2692_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2692_1_0_0;
struct t2692;
extern Il2CppGenericClass t2692_GC;
TypeInfo t2692_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2692_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2692_TI, NULL, t2692_VT, &EmptyCustomAttributesCache, &t2692_TI, &t2692_0_0_0, &t2692_1_0_0, t2692_IOs, &t2692_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2692), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2705_TI;

#include "t39.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Toggle>
extern MethodInfo m28437_MI;
static PropertyInfo t2705____Current_PropertyInfo = 
{
	&t2705_TI, "Current", &m28437_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2705_PIs[] =
{
	&t2705____Current_PropertyInfo,
	NULL
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28437_GM;
MethodInfo m28437_MI = 
{
	"get_Current", NULL, &t2705_TI, &t39_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28437_GM};
static MethodInfo* t2705_MIs[] =
{
	&m28437_MI,
	NULL
};
static TypeInfo* t2705_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2705_0_0_0;
extern Il2CppType t2705_1_0_0;
struct t2705;
extern Il2CppGenericClass t2705_GC;
TypeInfo t2705_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2705_MIs, t2705_PIs, NULL, NULL, NULL, NULL, NULL, &t2705_TI, t2705_ITIs, NULL, &EmptyCustomAttributesCache, &t2705_TI, &t2705_0_0_0, &t2705_1_0_0, NULL, &t2705_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2693.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2693_TI;
#include "t2693MD.h"

extern TypeInfo t39_TI;
extern MethodInfo m14653_MI;
extern MethodInfo m21490_MI;
struct t20;
#define m21490(__this, p0, method) (t39 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Toggle>
extern Il2CppType t20_0_0_1;
FieldInfo t2693_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2693_TI, offsetof(t2693, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2693_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2693_TI, offsetof(t2693, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2693_FIs[] =
{
	&t2693_f0_FieldInfo,
	&t2693_f1_FieldInfo,
	NULL
};
extern MethodInfo m14650_MI;
static PropertyInfo t2693____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2693_TI, "System.Collections.IEnumerator.Current", &m14650_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2693____Current_PropertyInfo = 
{
	&t2693_TI, "Current", &m14653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2693_PIs[] =
{
	&t2693____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2693____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2693_m14649_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14649_GM;
MethodInfo m14649_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2693_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2693_m14649_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14649_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14650_GM;
MethodInfo m14650_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2693_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14650_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14651_GM;
MethodInfo m14651_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2693_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14651_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14652_GM;
MethodInfo m14652_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2693_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14652_GM};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14653_GM;
MethodInfo m14653_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2693_TI, &t39_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14653_GM};
static MethodInfo* t2693_MIs[] =
{
	&m14649_MI,
	&m14650_MI,
	&m14651_MI,
	&m14652_MI,
	&m14653_MI,
	NULL
};
extern MethodInfo m14652_MI;
extern MethodInfo m14651_MI;
static MethodInfo* t2693_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14650_MI,
	&m14652_MI,
	&m14651_MI,
	&m14653_MI,
};
static TypeInfo* t2693_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2705_TI,
};
static Il2CppInterfaceOffsetPair t2693_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2705_TI, 7},
};
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2693_RGCTXData[3] = 
{
	&m14653_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m21490_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2693_0_0_0;
extern Il2CppType t2693_1_0_0;
extern Il2CppGenericClass t2693_GC;
TypeInfo t2693_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2693_MIs, t2693_PIs, t2693_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2693_TI, t2693_ITIs, t2693_VT, &EmptyCustomAttributesCache, &t2693_TI, &t2693_0_0_0, &t2693_1_0_0, t2693_IOs, &t2693_GC, NULL, NULL, NULL, t2693_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2693)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2706_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Toggle>
extern MethodInfo m28438_MI;
static PropertyInfo t2706____Count_PropertyInfo = 
{
	&t2706_TI, "Count", &m28438_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28439_MI;
static PropertyInfo t2706____IsReadOnly_PropertyInfo = 
{
	&t2706_TI, "IsReadOnly", &m28439_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2706_PIs[] =
{
	&t2706____Count_PropertyInfo,
	&t2706____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28438_GM;
MethodInfo m28438_MI = 
{
	"get_Count", NULL, &t2706_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28438_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28439_GM;
MethodInfo m28439_MI = 
{
	"get_IsReadOnly", NULL, &t2706_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28439_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2706_m28440_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28440_GM;
MethodInfo m28440_MI = 
{
	"Add", NULL, &t2706_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2706_m28440_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28440_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28441_GM;
MethodInfo m28441_MI = 
{
	"Clear", NULL, &t2706_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28441_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2706_m28442_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28442_GM;
MethodInfo m28442_MI = 
{
	"Contains", NULL, &t2706_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2706_m28442_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28442_GM};
extern Il2CppType t2704_0_0_0;
extern Il2CppType t2704_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2706_m28443_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2704_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28443_GM;
MethodInfo m28443_MI = 
{
	"CopyTo", NULL, &t2706_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2706_m28443_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28443_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2706_m28444_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28444_GM;
MethodInfo m28444_MI = 
{
	"Remove", NULL, &t2706_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2706_m28444_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28444_GM};
static MethodInfo* t2706_MIs[] =
{
	&m28438_MI,
	&m28439_MI,
	&m28440_MI,
	&m28441_MI,
	&m28442_MI,
	&m28443_MI,
	&m28444_MI,
	NULL
};
extern TypeInfo t246_TI;
static TypeInfo* t2706_ITIs[] = 
{
	&t603_TI,
	&t246_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2706_0_0_0;
extern Il2CppType t2706_1_0_0;
struct t2706;
extern Il2CppGenericClass t2706_GC;
TypeInfo t2706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2706_MIs, t2706_PIs, NULL, NULL, NULL, NULL, NULL, &t2706_TI, t2706_ITIs, NULL, &EmptyCustomAttributesCache, &t2706_TI, &t2706_0_0_0, &t2706_1_0_0, NULL, &t2706_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle>
extern Il2CppType t2705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28445_GM;
MethodInfo m28445_MI = 
{
	"GetEnumerator", NULL, &t246_TI, &t2705_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28445_GM};
static MethodInfo* t246_MIs[] =
{
	&m28445_MI,
	NULL
};
static TypeInfo* t246_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t246_0_0_0;
extern Il2CppType t246_1_0_0;
struct t246;
extern Il2CppGenericClass t246_GC;
TypeInfo t246_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t246_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t246_TI, t246_ITIs, NULL, &EmptyCustomAttributesCache, &t246_TI, &t246_0_0_0, &t246_1_0_0, NULL, &t246_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2710_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Toggle>
extern MethodInfo m28446_MI;
extern MethodInfo m28447_MI;
static PropertyInfo t2710____Item_PropertyInfo = 
{
	&t2710_TI, "Item", &m28446_MI, &m28447_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2710_PIs[] =
{
	&t2710____Item_PropertyInfo,
	NULL
};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2710_m28448_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28448_GM;
MethodInfo m28448_MI = 
{
	"IndexOf", NULL, &t2710_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2710_m28448_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28448_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2710_m28449_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28449_GM;
MethodInfo m28449_MI = 
{
	"Insert", NULL, &t2710_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2710_m28449_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28449_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2710_m28450_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28450_GM;
MethodInfo m28450_MI = 
{
	"RemoveAt", NULL, &t2710_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2710_m28450_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28450_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2710_m28446_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28446_GM;
MethodInfo m28446_MI = 
{
	"get_Item", NULL, &t2710_TI, &t39_0_0_0, RuntimeInvoker_t29_t44, t2710_m28446_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28446_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2710_m28447_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28447_GM;
MethodInfo m28447_MI = 
{
	"set_Item", NULL, &t2710_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2710_m28447_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28447_GM};
static MethodInfo* t2710_MIs[] =
{
	&m28448_MI,
	&m28449_MI,
	&m28450_MI,
	&m28446_MI,
	&m28447_MI,
	NULL
};
static TypeInfo* t2710_ITIs[] = 
{
	&t603_TI,
	&t2706_TI,
	&t246_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2710_0_0_0;
extern Il2CppType t2710_1_0_0;
struct t2710;
extern Il2CppGenericClass t2710_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2710_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2710_MIs, t2710_PIs, NULL, NULL, NULL, NULL, NULL, &t2710_TI, t2710_ITIs, NULL, &t1908__CustomAttributeCache, &t2710_TI, &t2710_0_0_0, &t2710_1_0_0, NULL, &t2710_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2694.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2694_TI;
#include "t2694MD.h"

#include "t2695.h"
extern TypeInfo t2695_TI;
#include "t2695MD.h"
extern MethodInfo m14656_MI;
extern MethodInfo m14658_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Toggle>
extern Il2CppType t316_0_0_33;
FieldInfo t2694_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2694_TI, offsetof(t2694, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2694_FIs[] =
{
	&t2694_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2694_m14654_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14654_GM;
MethodInfo m14654_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2694_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2694_m14654_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14654_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2694_m14655_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14655_GM;
MethodInfo m14655_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2694_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2694_m14655_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14655_GM};
static MethodInfo* t2694_MIs[] =
{
	&m14654_MI,
	&m14655_MI,
	NULL
};
extern MethodInfo m14655_MI;
extern MethodInfo m14659_MI;
static MethodInfo* t2694_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14655_MI,
	&m14659_MI,
};
extern Il2CppType t2696_0_0_0;
extern TypeInfo t2696_TI;
extern MethodInfo m21500_MI;
extern TypeInfo t39_TI;
extern MethodInfo m14661_MI;
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2694_RGCTXData[8] = 
{
	&t2696_0_0_0/* Type Usage */,
	&t2696_TI/* Class Usage */,
	&m21500_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m14661_MI/* Method Usage */,
	&m14656_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m14658_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2694_0_0_0;
extern Il2CppType t2694_1_0_0;
struct t2694;
extern Il2CppGenericClass t2694_GC;
TypeInfo t2694_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2694_MIs, NULL, t2694_FIs, NULL, &t2695_TI, NULL, NULL, &t2694_TI, NULL, t2694_VT, &EmptyCustomAttributesCache, &t2694_TI, &t2694_0_0_0, &t2694_1_0_0, NULL, &t2694_GC, NULL, NULL, NULL, t2694_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2694), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2696.h"
extern TypeInfo t2696_TI;
#include "t2696MD.h"
struct t556;
#define m21500(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Toggle>
extern Il2CppType t2696_0_0_1;
FieldInfo t2695_f0_FieldInfo = 
{
	"Delegate", &t2696_0_0_1, &t2695_TI, offsetof(t2695, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2695_FIs[] =
{
	&t2695_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2695_m14656_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14656_GM;
MethodInfo m14656_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2695_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2695_m14656_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14656_GM};
extern Il2CppType t2696_0_0_0;
static ParameterInfo t2695_m14657_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2696_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14657_GM;
MethodInfo m14657_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2695_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2695_m14657_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14657_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2695_m14658_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14658_GM;
MethodInfo m14658_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2695_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2695_m14658_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14658_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2695_m14659_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14659_GM;
MethodInfo m14659_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2695_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2695_m14659_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14659_GM};
static MethodInfo* t2695_MIs[] =
{
	&m14656_MI,
	&m14657_MI,
	&m14658_MI,
	&m14659_MI,
	NULL
};
static MethodInfo* t2695_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14658_MI,
	&m14659_MI,
};
extern TypeInfo t2696_TI;
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2695_RGCTXData[5] = 
{
	&t2696_0_0_0/* Type Usage */,
	&t2696_TI/* Class Usage */,
	&m21500_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m14661_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2695_0_0_0;
extern Il2CppType t2695_1_0_0;
struct t2695;
extern Il2CppGenericClass t2695_GC;
TypeInfo t2695_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2695_MIs, NULL, t2695_FIs, NULL, &t556_TI, NULL, NULL, &t2695_TI, NULL, t2695_VT, &EmptyCustomAttributesCache, &t2695_TI, &t2695_0_0_0, &t2695_1_0_0, NULL, &t2695_GC, NULL, NULL, NULL, t2695_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2695), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Toggle>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2696_m14660_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14660_GM;
MethodInfo m14660_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2696_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2696_m14660_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14660_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2696_m14661_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14661_GM;
MethodInfo m14661_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2696_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2696_m14661_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14661_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2696_m14662_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14662_GM;
MethodInfo m14662_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2696_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2696_m14662_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14662_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2696_m14663_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14663_GM;
MethodInfo m14663_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2696_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2696_m14663_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14663_GM};
static MethodInfo* t2696_MIs[] =
{
	&m14660_MI,
	&m14661_MI,
	&m14662_MI,
	&m14663_MI,
	NULL
};
extern MethodInfo m14662_MI;
extern MethodInfo m14663_MI;
static MethodInfo* t2696_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14661_MI,
	&m14662_MI,
	&m14663_MI,
};
static Il2CppInterfaceOffsetPair t2696_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2696_1_0_0;
struct t2696;
extern Il2CppGenericClass t2696_GC;
TypeInfo t2696_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2696_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2696_TI, NULL, t2696_VT, &EmptyCustomAttributesCache, &t2696_TI, &t2696_0_0_0, &t2696_1_0_0, t2696_IOs, &t2696_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2696), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t241.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t241_TI;
#include "t241MD.h"

#include "t2697.h"
#include "t2698.h"
extern TypeInfo t2698_TI;
#include "t566MD.h"
#include "t2698MD.h"
extern Il2CppType t40_0_0_0;
extern MethodInfo m2812_MI;
extern MethodInfo m14666_MI;
extern MethodInfo m2817_MI;
extern MethodInfo m2818_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m14671_MI;
extern MethodInfo m14672_MI;
extern MethodInfo m2819_MI;


extern MethodInfo m1928_MI;
 void m1928 (t241 * __this, MethodInfo* method){
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m14664_MI;
 void m14664 (t241 * __this, t2697 * p0, MethodInfo* method){
	{
		t556 * L_0 = m14666(NULL, p0, &m14666_MI);
		m2817(__this, L_0, &m2817_MI);
		return;
	}
}
extern MethodInfo m14665_MI;
 void m14665 (t241 * __this, t2697 * p0, MethodInfo* method){
	{
		t29 * L_0 = m2953(p0, &m2953_MI);
		t557 * L_1 = m2951(p0, &m2951_MI);
		m2818(__this, L_0, L_1, &m2818_MI);
		return;
	}
}
extern MethodInfo m1929_MI;
 t557 * m1929 (t241 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t40_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t557 * L_2 = m2820(NULL, p1, p0, L_0, &m2820_MI);
		return L_2;
	}
}
extern MethodInfo m1930_MI;
 t556 * m1930 (t241 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		t2698 * L_0 = (t2698 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2698_TI));
		m14671(L_0, p0, p1, &m14671_MI);
		return L_0;
	}
}
 t556 * m14666 (t29 * __this, t2697 * p0, MethodInfo* method){
	{
		t2698 * L_0 = (t2698 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2698_TI));
		m14672(L_0, p0, &m14672_MI);
		return L_0;
	}
}
extern MethodInfo m1931_MI;
 void m1931 (t241 * __this, bool p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f4);
		bool L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t40_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		t316* L_3 = (__this->f4);
		m2819(__this, L_3, &m2819_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`1<System.Boolean>
extern Il2CppType t316_0_0_33;
FieldInfo t241_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t241_TI, offsetof(t241, f4), &EmptyCustomAttributesCache};
static FieldInfo* t241_FIs[] =
{
	&t241_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1928_GM;
MethodInfo m1928_MI = 
{
	".ctor", (methodPointerType)&m1928, &t241_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1928_GM};
extern Il2CppType t2697_0_0_0;
extern Il2CppType t2697_0_0_0;
static ParameterInfo t241_m14664_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2697_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14664_GM;
MethodInfo m14664_MI = 
{
	"AddListener", (methodPointerType)&m14664, &t241_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t241_m14664_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14664_GM};
extern Il2CppType t2697_0_0_0;
static ParameterInfo t241_m14665_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2697_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14665_GM;
MethodInfo m14665_MI = 
{
	"RemoveListener", (methodPointerType)&m14665, &t241_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t241_m14665_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14665_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t241_m1929_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1929_GM;
MethodInfo m1929_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m1929, &t241_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t241_m1929_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1929_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t241_m1930_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1930_GM;
MethodInfo m1930_MI = 
{
	"GetDelegate", (methodPointerType)&m1930, &t241_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t241_m1930_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1930_GM};
extern Il2CppType t2697_0_0_0;
static ParameterInfo t241_m14666_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t2697_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14666_GM;
MethodInfo m14666_MI = 
{
	"GetDelegate", (methodPointerType)&m14666, &t241_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t241_m14666_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14666_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t241_m1931_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1931_GM;
MethodInfo m1931_MI = 
{
	"Invoke", (methodPointerType)&m1931, &t241_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t241_m1931_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1931_GM};
static MethodInfo* t241_MIs[] =
{
	&m1928_MI,
	&m14664_MI,
	&m14665_MI,
	&m1929_MI,
	&m1930_MI,
	&m14666_MI,
	&m1931_MI,
	NULL
};
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
static MethodInfo* t241_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1929_MI,
	&m1930_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t241_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t241_0_0_0;
extern Il2CppType t241_1_0_0;
extern TypeInfo t566_TI;
struct t241;
extern Il2CppGenericClass t241_GC;
TypeInfo t241_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t241_MIs, NULL, t241_FIs, NULL, &t566_TI, NULL, NULL, &t241_TI, NULL, t241_VT, &EmptyCustomAttributesCache, &t241_TI, &t241_0_0_0, &t241_1_0_0, t241_IOs, &t241_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t241), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2697_TI;
#include "t2697MD.h"



extern MethodInfo m14667_MI;
 void m14667 (t2697 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m14668_MI;
 void m14668 (t2697 * __this, bool p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m14668((t2697 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, bool p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, bool p0, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m14669_MI;
 t29 * m14669 (t2697 * __this, bool p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t40_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m14670_MI;
 void m14670 (t2697 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Boolean>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2697_m14667_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14667_GM;
MethodInfo m14667_MI = 
{
	".ctor", (methodPointerType)&m14667, &t2697_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2697_m14667_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14667_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t2697_m14668_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14668_GM;
MethodInfo m14668_MI = 
{
	"Invoke", (methodPointerType)&m14668, &t2697_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t2697_m14668_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14668_GM};
extern Il2CppType t40_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2697_m14669_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14669_GM;
MethodInfo m14669_MI = 
{
	"BeginInvoke", (methodPointerType)&m14669, &t2697_TI, &t66_0_0_0, RuntimeInvoker_t29_t297_t29_t29, t2697_m14669_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14669_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2697_m14670_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14670_GM;
MethodInfo m14670_MI = 
{
	"EndInvoke", (methodPointerType)&m14670, &t2697_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2697_m14670_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14670_GM};
static MethodInfo* t2697_MIs[] =
{
	&m14667_MI,
	&m14668_MI,
	&m14669_MI,
	&m14670_MI,
	NULL
};
static MethodInfo* t2697_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14668_MI,
	&m14669_MI,
	&m14670_MI,
};
static Il2CppInterfaceOffsetPair t2697_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2697_1_0_0;
struct t2697;
extern Il2CppGenericClass t2697_GC;
TypeInfo t2697_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2697_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2697_TI, NULL, t2697_VT, &EmptyCustomAttributesCache, &t2697_TI, &t2697_0_0_0, &t2697_1_0_0, t2697_IOs, &t2697_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2697), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m21501_MI;
struct t556;
 void m21501 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m14671 (t2698 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m2790(__this, p0, p1, &m2790_MI);
		t2697 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t2697_0_0_0), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t2697 *)IsInst(L_2, InitializedTypeInfo(&t2697_TI))), &m1597_MI);
		__this->f0 = ((t2697 *)Castclass(L_3, InitializedTypeInfo(&t2697_TI)));
		return;
	}
}
 void m14672 (t2698 * __this, t2697 * p0, MethodInfo* method){
	{
		m2789(__this, &m2789_MI);
		t2697 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t2697 *)Castclass(L_1, InitializedTypeInfo(&t2697_TI)));
		return;
	}
}
extern MethodInfo m14673_MI;
 void m14673 (t2698 * __this, t316* p0, MethodInfo* method){
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		m21501(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), &m21501_MI);
		t2697 * L_2 = (__this->f0);
		bool L_3 = m2791(NULL, L_2, &m2791_MI);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		t2697 * L_4 = (__this->f0);
		int32_t L_5 = 0;
		VirtActionInvoker1< bool >::Invoke(&m14668_MI, L_4, ((*(bool*)((bool*)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(p0, L_5)), InitializedTypeInfo(&t40_TI))))));
	}

IL_003f:
	{
		return;
	}
}
extern MethodInfo m14674_MI;
 bool m14674 (t2698 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t2697 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t2697 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.Boolean>
extern Il2CppType t2697_0_0_1;
FieldInfo t2698_f0_FieldInfo = 
{
	"Delegate", &t2697_0_0_1, &t2698_TI, offsetof(t2698, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2698_FIs[] =
{
	&t2698_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2698_m14671_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14671_GM;
MethodInfo m14671_MI = 
{
	".ctor", (methodPointerType)&m14671, &t2698_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2698_m14671_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14671_GM};
extern Il2CppType t2697_0_0_0;
static ParameterInfo t2698_m14672_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2697_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14672_GM;
MethodInfo m14672_MI = 
{
	".ctor", (methodPointerType)&m14672, &t2698_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2698_m14672_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14672_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2698_m14673_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14673_GM;
MethodInfo m14673_MI = 
{
	"Invoke", (methodPointerType)&m14673, &t2698_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2698_m14673_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14673_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2698_m14674_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14674_GM;
MethodInfo m14674_MI = 
{
	"Find", (methodPointerType)&m14674, &t2698_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2698_m14674_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14674_GM};
static MethodInfo* t2698_MIs[] =
{
	&m14671_MI,
	&m14672_MI,
	&m14673_MI,
	&m14674_MI,
	NULL
};
static MethodInfo* t2698_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14673_MI,
	&m14674_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2698_0_0_0;
extern Il2CppType t2698_1_0_0;
struct t2698;
extern Il2CppGenericClass t2698_GC;
TypeInfo t2698_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2698_MIs, NULL, t2698_FIs, NULL, &t556_TI, NULL, NULL, &t2698_TI, NULL, t2698_VT, &EmptyCustomAttributesCache, &t2698_TI, &t2698_0_0_0, &t2698_1_0_0, NULL, &t2698_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2698), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4251_TI;

#include "t239.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Toggle/ToggleTransition>
extern MethodInfo m28451_MI;
static PropertyInfo t4251____Current_PropertyInfo = 
{
	&t4251_TI, "Current", &m28451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4251_PIs[] =
{
	&t4251____Current_PropertyInfo,
	NULL
};
extern Il2CppType t239_0_0_0;
extern void* RuntimeInvoker_t239 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28451_GM;
MethodInfo m28451_MI = 
{
	"get_Current", NULL, &t4251_TI, &t239_0_0_0, RuntimeInvoker_t239, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28451_GM};
static MethodInfo* t4251_MIs[] =
{
	&m28451_MI,
	NULL
};
static TypeInfo* t4251_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4251_0_0_0;
extern Il2CppType t4251_1_0_0;
struct t4251;
extern Il2CppGenericClass t4251_GC;
TypeInfo t4251_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4251_MIs, t4251_PIs, NULL, NULL, NULL, NULL, NULL, &t4251_TI, t4251_ITIs, NULL, &EmptyCustomAttributesCache, &t4251_TI, &t4251_0_0_0, &t4251_1_0_0, NULL, &t4251_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2699.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2699_TI;
#include "t2699MD.h"

extern TypeInfo t239_TI;
extern MethodInfo m14679_MI;
extern MethodInfo m21503_MI;
struct t20;
 int32_t m21503 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14675_MI;
 void m14675 (t2699 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14676_MI;
 t29 * m14676 (t2699 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14679(__this, &m14679_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t239_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14677_MI;
 void m14677 (t2699 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14678_MI;
 bool m14678 (t2699 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14679 (t2699 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21503(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21503_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Toggle/ToggleTransition>
extern Il2CppType t20_0_0_1;
FieldInfo t2699_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2699_TI, offsetof(t2699, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2699_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2699_TI, offsetof(t2699, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2699_FIs[] =
{
	&t2699_f0_FieldInfo,
	&t2699_f1_FieldInfo,
	NULL
};
static PropertyInfo t2699____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2699_TI, "System.Collections.IEnumerator.Current", &m14676_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2699____Current_PropertyInfo = 
{
	&t2699_TI, "Current", &m14679_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2699_PIs[] =
{
	&t2699____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2699____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2699_m14675_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14675_GM;
MethodInfo m14675_MI = 
{
	".ctor", (methodPointerType)&m14675, &t2699_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2699_m14675_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14675_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14676_GM;
MethodInfo m14676_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14676, &t2699_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14676_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14677_GM;
MethodInfo m14677_MI = 
{
	"Dispose", (methodPointerType)&m14677, &t2699_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14677_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14678_GM;
MethodInfo m14678_MI = 
{
	"MoveNext", (methodPointerType)&m14678, &t2699_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14678_GM};
extern Il2CppType t239_0_0_0;
extern void* RuntimeInvoker_t239 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14679_GM;
MethodInfo m14679_MI = 
{
	"get_Current", (methodPointerType)&m14679, &t2699_TI, &t239_0_0_0, RuntimeInvoker_t239, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14679_GM};
static MethodInfo* t2699_MIs[] =
{
	&m14675_MI,
	&m14676_MI,
	&m14677_MI,
	&m14678_MI,
	&m14679_MI,
	NULL
};
static MethodInfo* t2699_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14676_MI,
	&m14678_MI,
	&m14677_MI,
	&m14679_MI,
};
static TypeInfo* t2699_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4251_TI,
};
static Il2CppInterfaceOffsetPair t2699_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4251_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2699_0_0_0;
extern Il2CppType t2699_1_0_0;
extern Il2CppGenericClass t2699_GC;
TypeInfo t2699_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2699_MIs, t2699_PIs, t2699_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2699_TI, t2699_ITIs, t2699_VT, &EmptyCustomAttributesCache, &t2699_TI, &t2699_0_0_0, &t2699_1_0_0, t2699_IOs, &t2699_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2699)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5430_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Toggle/ToggleTransition>
extern MethodInfo m28452_MI;
static PropertyInfo t5430____Count_PropertyInfo = 
{
	&t5430_TI, "Count", &m28452_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28453_MI;
static PropertyInfo t5430____IsReadOnly_PropertyInfo = 
{
	&t5430_TI, "IsReadOnly", &m28453_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5430_PIs[] =
{
	&t5430____Count_PropertyInfo,
	&t5430____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28452_GM;
MethodInfo m28452_MI = 
{
	"get_Count", NULL, &t5430_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28452_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28453_GM;
MethodInfo m28453_MI = 
{
	"get_IsReadOnly", NULL, &t5430_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28453_GM};
extern Il2CppType t239_0_0_0;
extern Il2CppType t239_0_0_0;
static ParameterInfo t5430_m28454_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t239_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28454_GM;
MethodInfo m28454_MI = 
{
	"Add", NULL, &t5430_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5430_m28454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28454_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28455_GM;
MethodInfo m28455_MI = 
{
	"Clear", NULL, &t5430_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28455_GM};
extern Il2CppType t239_0_0_0;
static ParameterInfo t5430_m28456_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t239_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28456_GM;
MethodInfo m28456_MI = 
{
	"Contains", NULL, &t5430_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5430_m28456_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28456_GM};
extern Il2CppType t3863_0_0_0;
extern Il2CppType t3863_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5430_m28457_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3863_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28457_GM;
MethodInfo m28457_MI = 
{
	"CopyTo", NULL, &t5430_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5430_m28457_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28457_GM};
extern Il2CppType t239_0_0_0;
static ParameterInfo t5430_m28458_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t239_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28458_GM;
MethodInfo m28458_MI = 
{
	"Remove", NULL, &t5430_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5430_m28458_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28458_GM};
static MethodInfo* t5430_MIs[] =
{
	&m28452_MI,
	&m28453_MI,
	&m28454_MI,
	&m28455_MI,
	&m28456_MI,
	&m28457_MI,
	&m28458_MI,
	NULL
};
extern TypeInfo t5432_TI;
static TypeInfo* t5430_ITIs[] = 
{
	&t603_TI,
	&t5432_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5430_0_0_0;
extern Il2CppType t5430_1_0_0;
struct t5430;
extern Il2CppGenericClass t5430_GC;
TypeInfo t5430_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5430_MIs, t5430_PIs, NULL, NULL, NULL, NULL, NULL, &t5430_TI, t5430_ITIs, NULL, &EmptyCustomAttributesCache, &t5430_TI, &t5430_0_0_0, &t5430_1_0_0, NULL, &t5430_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle/ToggleTransition>
extern Il2CppType t4251_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28459_GM;
MethodInfo m28459_MI = 
{
	"GetEnumerator", NULL, &t5432_TI, &t4251_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28459_GM};
static MethodInfo* t5432_MIs[] =
{
	&m28459_MI,
	NULL
};
static TypeInfo* t5432_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5432_0_0_0;
extern Il2CppType t5432_1_0_0;
struct t5432;
extern Il2CppGenericClass t5432_GC;
TypeInfo t5432_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5432_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5432_TI, t5432_ITIs, NULL, &EmptyCustomAttributesCache, &t5432_TI, &t5432_0_0_0, &t5432_1_0_0, NULL, &t5432_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5431_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Toggle/ToggleTransition>
extern MethodInfo m28460_MI;
extern MethodInfo m28461_MI;
static PropertyInfo t5431____Item_PropertyInfo = 
{
	&t5431_TI, "Item", &m28460_MI, &m28461_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5431_PIs[] =
{
	&t5431____Item_PropertyInfo,
	NULL
};
extern Il2CppType t239_0_0_0;
static ParameterInfo t5431_m28462_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t239_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28462_GM;
MethodInfo m28462_MI = 
{
	"IndexOf", NULL, &t5431_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5431_m28462_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28462_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t239_0_0_0;
static ParameterInfo t5431_m28463_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t239_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28463_GM;
MethodInfo m28463_MI = 
{
	"Insert", NULL, &t5431_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5431_m28463_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28463_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5431_m28464_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28464_GM;
MethodInfo m28464_MI = 
{
	"RemoveAt", NULL, &t5431_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5431_m28464_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28464_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5431_m28460_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t239_0_0_0;
extern void* RuntimeInvoker_t239_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28460_GM;
MethodInfo m28460_MI = 
{
	"get_Item", NULL, &t5431_TI, &t239_0_0_0, RuntimeInvoker_t239_t44, t5431_m28460_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28460_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t239_0_0_0;
static ParameterInfo t5431_m28461_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t239_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28461_GM;
MethodInfo m28461_MI = 
{
	"set_Item", NULL, &t5431_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5431_m28461_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28461_GM};
static MethodInfo* t5431_MIs[] =
{
	&m28462_MI,
	&m28463_MI,
	&m28464_MI,
	&m28460_MI,
	&m28461_MI,
	NULL
};
static TypeInfo* t5431_ITIs[] = 
{
	&t603_TI,
	&t5430_TI,
	&t5432_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5431_0_0_0;
extern Il2CppType t5431_1_0_0;
struct t5431;
extern Il2CppGenericClass t5431_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5431_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5431_MIs, t5431_PIs, NULL, NULL, NULL, NULL, NULL, &t5431_TI, t5431_ITIs, NULL, &t1908__CustomAttributeCache, &t5431_TI, &t5431_0_0_0, &t5431_1_0_0, NULL, &t5431_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4253_TI;

#include "t242.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ToggleGroup>
extern MethodInfo m28465_MI;
static PropertyInfo t4253____Current_PropertyInfo = 
{
	&t4253_TI, "Current", &m28465_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4253_PIs[] =
{
	&t4253____Current_PropertyInfo,
	NULL
};
extern Il2CppType t242_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28465_GM;
MethodInfo m28465_MI = 
{
	"get_Current", NULL, &t4253_TI, &t242_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28465_GM};
static MethodInfo* t4253_MIs[] =
{
	&m28465_MI,
	NULL
};
static TypeInfo* t4253_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4253_0_0_0;
extern Il2CppType t4253_1_0_0;
struct t4253;
extern Il2CppGenericClass t4253_GC;
TypeInfo t4253_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4253_MIs, t4253_PIs, NULL, NULL, NULL, NULL, NULL, &t4253_TI, t4253_ITIs, NULL, &EmptyCustomAttributesCache, &t4253_TI, &t4253_0_0_0, &t4253_1_0_0, NULL, &t4253_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2700.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2700_TI;
#include "t2700MD.h"

extern TypeInfo t242_TI;
extern MethodInfo m14684_MI;
extern MethodInfo m21514_MI;
struct t20;
#define m21514(__this, p0, method) (t242 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ToggleGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2700_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2700_TI, offsetof(t2700, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2700_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2700_TI, offsetof(t2700, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2700_FIs[] =
{
	&t2700_f0_FieldInfo,
	&t2700_f1_FieldInfo,
	NULL
};
extern MethodInfo m14681_MI;
static PropertyInfo t2700____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2700_TI, "System.Collections.IEnumerator.Current", &m14681_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2700____Current_PropertyInfo = 
{
	&t2700_TI, "Current", &m14684_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2700_PIs[] =
{
	&t2700____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2700____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2700_m14680_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14680_GM;
MethodInfo m14680_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2700_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2700_m14680_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14680_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14681_GM;
MethodInfo m14681_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2700_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14681_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14682_GM;
MethodInfo m14682_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2700_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14682_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14683_GM;
MethodInfo m14683_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2700_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14683_GM};
extern Il2CppType t242_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14684_GM;
MethodInfo m14684_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2700_TI, &t242_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14684_GM};
static MethodInfo* t2700_MIs[] =
{
	&m14680_MI,
	&m14681_MI,
	&m14682_MI,
	&m14683_MI,
	&m14684_MI,
	NULL
};
extern MethodInfo m14683_MI;
extern MethodInfo m14682_MI;
static MethodInfo* t2700_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14681_MI,
	&m14683_MI,
	&m14682_MI,
	&m14684_MI,
};
static TypeInfo* t2700_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4253_TI,
};
static Il2CppInterfaceOffsetPair t2700_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4253_TI, 7},
};
extern TypeInfo t242_TI;
static Il2CppRGCTXData t2700_RGCTXData[3] = 
{
	&m14684_MI/* Method Usage */,
	&t242_TI/* Class Usage */,
	&m21514_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2700_0_0_0;
extern Il2CppType t2700_1_0_0;
extern Il2CppGenericClass t2700_GC;
TypeInfo t2700_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2700_MIs, t2700_PIs, t2700_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2700_TI, t2700_ITIs, t2700_VT, &EmptyCustomAttributesCache, &t2700_TI, &t2700_0_0_0, &t2700_1_0_0, t2700_IOs, &t2700_GC, NULL, NULL, NULL, t2700_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2700)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5433_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ToggleGroup>
extern MethodInfo m28466_MI;
static PropertyInfo t5433____Count_PropertyInfo = 
{
	&t5433_TI, "Count", &m28466_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28467_MI;
static PropertyInfo t5433____IsReadOnly_PropertyInfo = 
{
	&t5433_TI, "IsReadOnly", &m28467_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5433_PIs[] =
{
	&t5433____Count_PropertyInfo,
	&t5433____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28466_GM;
MethodInfo m28466_MI = 
{
	"get_Count", NULL, &t5433_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28466_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28467_GM;
MethodInfo m28467_MI = 
{
	"get_IsReadOnly", NULL, &t5433_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28467_GM};
extern Il2CppType t242_0_0_0;
extern Il2CppType t242_0_0_0;
static ParameterInfo t5433_m28468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28468_GM;
MethodInfo m28468_MI = 
{
	"Add", NULL, &t5433_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5433_m28468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28468_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28469_GM;
MethodInfo m28469_MI = 
{
	"Clear", NULL, &t5433_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28469_GM};
extern Il2CppType t242_0_0_0;
static ParameterInfo t5433_m28470_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28470_GM;
MethodInfo m28470_MI = 
{
	"Contains", NULL, &t5433_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5433_m28470_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28470_GM};
extern Il2CppType t3864_0_0_0;
extern Il2CppType t3864_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5433_m28471_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3864_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28471_GM;
MethodInfo m28471_MI = 
{
	"CopyTo", NULL, &t5433_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5433_m28471_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28471_GM};
extern Il2CppType t242_0_0_0;
static ParameterInfo t5433_m28472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28472_GM;
MethodInfo m28472_MI = 
{
	"Remove", NULL, &t5433_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5433_m28472_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28472_GM};
static MethodInfo* t5433_MIs[] =
{
	&m28466_MI,
	&m28467_MI,
	&m28468_MI,
	&m28469_MI,
	&m28470_MI,
	&m28471_MI,
	&m28472_MI,
	NULL
};
extern TypeInfo t5435_TI;
static TypeInfo* t5433_ITIs[] = 
{
	&t603_TI,
	&t5435_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5433_0_0_0;
extern Il2CppType t5433_1_0_0;
struct t5433;
extern Il2CppGenericClass t5433_GC;
TypeInfo t5433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5433_MIs, t5433_PIs, NULL, NULL, NULL, NULL, NULL, &t5433_TI, t5433_ITIs, NULL, &EmptyCustomAttributesCache, &t5433_TI, &t5433_0_0_0, &t5433_1_0_0, NULL, &t5433_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ToggleGroup>
extern Il2CppType t4253_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28473_GM;
MethodInfo m28473_MI = 
{
	"GetEnumerator", NULL, &t5435_TI, &t4253_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28473_GM};
static MethodInfo* t5435_MIs[] =
{
	&m28473_MI,
	NULL
};
static TypeInfo* t5435_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5435_0_0_0;
extern Il2CppType t5435_1_0_0;
struct t5435;
extern Il2CppGenericClass t5435_GC;
TypeInfo t5435_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5435_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5435_TI, t5435_ITIs, NULL, &EmptyCustomAttributesCache, &t5435_TI, &t5435_0_0_0, &t5435_1_0_0, NULL, &t5435_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5434_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ToggleGroup>
extern MethodInfo m28474_MI;
extern MethodInfo m28475_MI;
static PropertyInfo t5434____Item_PropertyInfo = 
{
	&t5434_TI, "Item", &m28474_MI, &m28475_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5434_PIs[] =
{
	&t5434____Item_PropertyInfo,
	NULL
};
extern Il2CppType t242_0_0_0;
static ParameterInfo t5434_m28476_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28476_GM;
MethodInfo m28476_MI = 
{
	"IndexOf", NULL, &t5434_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5434_m28476_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28476_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t242_0_0_0;
static ParameterInfo t5434_m28477_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28477_GM;
MethodInfo m28477_MI = 
{
	"Insert", NULL, &t5434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5434_m28477_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28477_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5434_m28478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28478_GM;
MethodInfo m28478_MI = 
{
	"RemoveAt", NULL, &t5434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5434_m28478_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28478_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5434_m28474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t242_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28474_GM;
MethodInfo m28474_MI = 
{
	"get_Item", NULL, &t5434_TI, &t242_0_0_0, RuntimeInvoker_t29_t44, t5434_m28474_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28474_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t242_0_0_0;
static ParameterInfo t5434_m28475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28475_GM;
MethodInfo m28475_MI = 
{
	"set_Item", NULL, &t5434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5434_m28475_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28475_GM};
static MethodInfo* t5434_MIs[] =
{
	&m28476_MI,
	&m28477_MI,
	&m28478_MI,
	&m28474_MI,
	&m28475_MI,
	NULL
};
static TypeInfo* t5434_ITIs[] = 
{
	&t603_TI,
	&t5433_TI,
	&t5435_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5434_0_0_0;
extern Il2CppType t5434_1_0_0;
struct t5434;
extern Il2CppGenericClass t5434_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5434_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5434_MIs, t5434_PIs, NULL, NULL, NULL, NULL, NULL, &t5434_TI, t5434_ITIs, NULL, &t1908__CustomAttributeCache, &t5434_TI, &t5434_0_0_0, &t5434_1_0_0, NULL, &t5434_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2701.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2701_TI;
#include "t2701MD.h"

#include "t2702.h"
extern TypeInfo t2702_TI;
#include "t2702MD.h"
extern MethodInfo m14687_MI;
extern MethodInfo m14689_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ToggleGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t2701_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2701_TI, offsetof(t2701, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2701_FIs[] =
{
	&t2701_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t242_0_0_0;
static ParameterInfo t2701_m14685_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14685_GM;
MethodInfo m14685_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2701_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2701_m14685_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14685_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2701_m14686_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14686_GM;
MethodInfo m14686_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2701_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2701_m14686_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14686_GM};
static MethodInfo* t2701_MIs[] =
{
	&m14685_MI,
	&m14686_MI,
	NULL
};
extern MethodInfo m14686_MI;
extern MethodInfo m14690_MI;
static MethodInfo* t2701_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14686_MI,
	&m14690_MI,
};
extern Il2CppType t2703_0_0_0;
extern TypeInfo t2703_TI;
extern MethodInfo m21524_MI;
extern TypeInfo t242_TI;
extern MethodInfo m14692_MI;
extern TypeInfo t242_TI;
static Il2CppRGCTXData t2701_RGCTXData[8] = 
{
	&t2703_0_0_0/* Type Usage */,
	&t2703_TI/* Class Usage */,
	&m21524_MI/* Method Usage */,
	&t242_TI/* Class Usage */,
	&m14692_MI/* Method Usage */,
	&m14687_MI/* Method Usage */,
	&t242_TI/* Class Usage */,
	&m14689_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2701_0_0_0;
extern Il2CppType t2701_1_0_0;
struct t2701;
extern Il2CppGenericClass t2701_GC;
TypeInfo t2701_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2701_MIs, NULL, t2701_FIs, NULL, &t2702_TI, NULL, NULL, &t2701_TI, NULL, t2701_VT, &EmptyCustomAttributesCache, &t2701_TI, &t2701_0_0_0, &t2701_1_0_0, NULL, &t2701_GC, NULL, NULL, NULL, t2701_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2701), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2703.h"
extern TypeInfo t2703_TI;
#include "t2703MD.h"
struct t556;
#define m21524(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ToggleGroup>
extern Il2CppType t2703_0_0_1;
FieldInfo t2702_f0_FieldInfo = 
{
	"Delegate", &t2703_0_0_1, &t2702_TI, offsetof(t2702, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2702_FIs[] =
{
	&t2702_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2702_m14687_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14687_GM;
MethodInfo m14687_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2702_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2702_m14687_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14687_GM};
extern Il2CppType t2703_0_0_0;
static ParameterInfo t2702_m14688_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2703_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14688_GM;
MethodInfo m14688_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2702_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2702_m14688_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14688_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2702_m14689_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14689_GM;
MethodInfo m14689_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2702_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2702_m14689_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14689_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2702_m14690_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14690_GM;
MethodInfo m14690_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2702_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2702_m14690_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14690_GM};
static MethodInfo* t2702_MIs[] =
{
	&m14687_MI,
	&m14688_MI,
	&m14689_MI,
	&m14690_MI,
	NULL
};
static MethodInfo* t2702_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14689_MI,
	&m14690_MI,
};
extern TypeInfo t2703_TI;
extern TypeInfo t242_TI;
static Il2CppRGCTXData t2702_RGCTXData[5] = 
{
	&t2703_0_0_0/* Type Usage */,
	&t2703_TI/* Class Usage */,
	&m21524_MI/* Method Usage */,
	&t242_TI/* Class Usage */,
	&m14692_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2702_0_0_0;
extern Il2CppType t2702_1_0_0;
struct t2702;
extern Il2CppGenericClass t2702_GC;
TypeInfo t2702_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2702_MIs, NULL, t2702_FIs, NULL, &t556_TI, NULL, NULL, &t2702_TI, NULL, t2702_VT, &EmptyCustomAttributesCache, &t2702_TI, &t2702_0_0_0, &t2702_1_0_0, NULL, &t2702_GC, NULL, NULL, NULL, t2702_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2702), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.ToggleGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2703_m14691_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14691_GM;
MethodInfo m14691_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2703_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2703_m14691_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14691_GM};
extern Il2CppType t242_0_0_0;
static ParameterInfo t2703_m14692_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14692_GM;
MethodInfo m14692_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2703_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2703_m14692_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14692_GM};
extern Il2CppType t242_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2703_m14693_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t242_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14693_GM;
MethodInfo m14693_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2703_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2703_m14693_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14693_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2703_m14694_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14694_GM;
MethodInfo m14694_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2703_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2703_m14694_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14694_GM};
static MethodInfo* t2703_MIs[] =
{
	&m14691_MI,
	&m14692_MI,
	&m14693_MI,
	&m14694_MI,
	NULL
};
extern MethodInfo m14693_MI;
extern MethodInfo m14694_MI;
static MethodInfo* t2703_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14692_MI,
	&m14693_MI,
	&m14694_MI,
};
static Il2CppInterfaceOffsetPair t2703_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2703_1_0_0;
struct t2703;
extern Il2CppGenericClass t2703_GC;
TypeInfo t2703_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2703_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2703_TI, NULL, t2703_VT, &EmptyCustomAttributesCache, &t2703_TI, &t2703_0_0_0, &t2703_1_0_0, t2703_IOs, &t2703_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2703), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t243.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t243_TI;
#include "t243MD.h"

#include "t2709.h"
#include "t2707.h"
#include "t244.h"
#include "t2714.h"
#include "t2708.h"
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2704_TI;
extern TypeInfo t2709_TI;
extern TypeInfo t2707_TI;
extern TypeInfo t244_TI;
extern TypeInfo t2714_TI;
#include "t915MD.h"
#include "t602MD.h"
#include "t2707MD.h"
#include "t244MD.h"
#include "t2709MD.h"
#include "t2714MD.h"
extern MethodInfo m1936_MI;
extern MethodInfo m14736_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m21526_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m14724_MI;
extern MethodInfo m14721_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m1939_MI;
extern MethodInfo m1933_MI;
extern MethodInfo m14722_MI;
extern MethodInfo m14725_MI;
extern MethodInfo m1938_MI;
extern MethodInfo m14712_MI;
extern MethodInfo m14734_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m14735_MI;
extern MethodInfo m28443_MI;
extern MethodInfo m28445_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m14726_MI;
extern MethodInfo m14713_MI;
extern MethodInfo m14714_MI;
extern MethodInfo m14743_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m21528_MI;
extern MethodInfo m14719_MI;
extern MethodInfo m14720_MI;
extern MethodInfo m14817_MI;
extern MethodInfo m14737_MI;
extern MethodInfo m14723_MI;
extern MethodInfo m14728_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m14823_MI;
extern MethodInfo m21530_MI;
extern MethodInfo m21538_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m21526(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2712.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m21528(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m21530(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m21538(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2709  m14721 (t243 * __this, MethodInfo* method){
	{
		t2709  L_0 = {0};
		m14737(&L_0, __this, &m14737_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
extern Il2CppType t44_0_0_32849;
FieldInfo t243_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t243_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2704_0_0_1;
FieldInfo t243_f1_FieldInfo = 
{
	"_items", &t2704_0_0_1, &t243_TI, offsetof(t243, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t243_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t243_TI, offsetof(t243, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t243_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t243_TI, offsetof(t243, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2704_0_0_49;
FieldInfo t243_f4_FieldInfo = 
{
	"EmptyArray", &t2704_0_0_49, &t243_TI, offsetof(t243_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t243_FIs[] =
{
	&t243_f0_FieldInfo,
	&t243_f1_FieldInfo,
	&t243_f2_FieldInfo,
	&t243_f3_FieldInfo,
	&t243_f4_FieldInfo,
	NULL
};
static const int32_t t243_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t243_f0_DefaultValue = 
{
	&t243_f0_FieldInfo, { (char*)&t243_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t243_FDVs[] = 
{
	&t243_f0_DefaultValue,
	NULL
};
extern MethodInfo m14705_MI;
static PropertyInfo t243____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t243_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14705_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14706_MI;
static PropertyInfo t243____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t243_TI, "System.Collections.ICollection.IsSynchronized", &m14706_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14707_MI;
static PropertyInfo t243____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t243_TI, "System.Collections.ICollection.SyncRoot", &m14707_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14708_MI;
static PropertyInfo t243____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t243_TI, "System.Collections.IList.IsFixedSize", &m14708_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14709_MI;
static PropertyInfo t243____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t243_TI, "System.Collections.IList.IsReadOnly", &m14709_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14710_MI;
extern MethodInfo m14711_MI;
static PropertyInfo t243____System_Collections_IList_Item_PropertyInfo = 
{
	&t243_TI, "System.Collections.IList.Item", &m14710_MI, &m14711_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t243____Capacity_PropertyInfo = 
{
	&t243_TI, "Capacity", &m14734_MI, &m14735_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1937_MI;
static PropertyInfo t243____Count_PropertyInfo = 
{
	&t243_TI, "Count", &m1937_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t243____Item_PropertyInfo = 
{
	&t243_TI, "Item", &m1936_MI, &m14736_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t243_PIs[] =
{
	&t243____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t243____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t243____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t243____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t243____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t243____System_Collections_IList_Item_PropertyInfo,
	&t243____Capacity_PropertyInfo,
	&t243____Count_PropertyInfo,
	&t243____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1932_GM;
MethodInfo m1932_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1932_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14695_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14695_GM;
MethodInfo m14695_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t243_m14695_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14695_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14696_GM;
MethodInfo m14696_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14696_GM};
extern Il2CppType t2705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14697_GM;
MethodInfo m14697_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t243_TI, &t2705_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14697_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14698_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14698_GM;
MethodInfo m14698_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t243_m14698_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14698_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14699_GM;
MethodInfo m14699_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t243_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14699_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t243_m14700_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14700_GM;
MethodInfo m14700_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t243_m14700_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14700_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t243_m14701_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14701_GM;
MethodInfo m14701_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t243_m14701_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14701_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t243_m14702_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14702_GM;
MethodInfo m14702_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t243_m14702_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14702_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t243_m14703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14703_GM;
MethodInfo m14703_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t243_m14703_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14703_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t243_m14704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14704_GM;
MethodInfo m14704_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14704_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14704_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14705_GM;
MethodInfo m14705_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14705_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14706_GM;
MethodInfo m14706_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14706_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14707_GM;
MethodInfo m14707_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t243_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14707_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14708_GM;
MethodInfo m14708_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14708_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14709_GM;
MethodInfo m14709_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14709_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14710_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14710_GM;
MethodInfo m14710_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t243_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t243_m14710_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14710_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t243_m14711_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14711_GM;
MethodInfo m14711_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t243_m14711_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14711_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t243_m1939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1939_GM;
MethodInfo m1939_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m1939_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1939_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14712_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14712_GM;
MethodInfo m14712_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t243_m14712_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14712_GM};
extern Il2CppType t2706_0_0_0;
static ParameterInfo t243_m14713_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14713_GM;
MethodInfo m14713_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14713_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14713_GM};
extern Il2CppType t246_0_0_0;
static ParameterInfo t243_m14714_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t246_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14714_GM;
MethodInfo m14714_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14714_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14714_GM};
extern Il2CppType t246_0_0_0;
static ParameterInfo t243_m14715_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t246_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14715_GM;
MethodInfo m14715_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14715_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14715_GM};
extern Il2CppType t2707_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14716_GM;
MethodInfo m14716_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t243_TI, &t2707_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14716_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14717_GM;
MethodInfo m14717_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14717_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t243_m1933_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1933_GM;
MethodInfo m1933_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t243_m1933_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1933_GM};
extern Il2CppType t2704_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14718_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2704_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14718_GM;
MethodInfo m14718_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t243_m14718_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14718_GM};
extern Il2CppType t244_0_0_0;
extern Il2CppType t244_0_0_0;
static ParameterInfo t243_m1941_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t244_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1941_GM;
MethodInfo m1941_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t243_TI, &t39_0_0_0, RuntimeInvoker_t29_t29, t243_m1941_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1941_GM};
extern Il2CppType t244_0_0_0;
static ParameterInfo t243_m14719_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t244_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14719_GM;
MethodInfo m14719_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14719_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14719_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t244_0_0_0;
static ParameterInfo t243_m14720_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t244_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14720_GM;
MethodInfo m14720_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t243_m14720_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14720_GM};
extern Il2CppType t2709_0_0_0;
extern void* RuntimeInvoker_t2709 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14721_GM;
MethodInfo m14721_MI = 
{
	"GetEnumerator", (methodPointerType)&m14721, &t243_TI, &t2709_0_0_0, RuntimeInvoker_t2709, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14721_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t243_m14722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14722_GM;
MethodInfo m14722_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t243_m14722_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14722_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14723_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14723_GM;
MethodInfo m14723_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t243_m14723_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14723_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14724_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14724_GM;
MethodInfo m14724_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t243_m14724_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14724_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t243_m14725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14725_GM;
MethodInfo m14725_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t243_m14725_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14725_GM};
extern Il2CppType t246_0_0_0;
static ParameterInfo t243_m14726_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t246_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14726_GM;
MethodInfo m14726_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14726_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14726_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t243_m1938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1938_GM;
MethodInfo m1938_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t243_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t243_m1938_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1938_GM};
extern Il2CppType t244_0_0_0;
static ParameterInfo t243_m14727_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t244_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14727_GM;
MethodInfo m14727_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t243_m14727_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14727_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14728_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14728_GM;
MethodInfo m14728_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t243_m14728_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14728_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14729_GM;
MethodInfo m14729_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14729_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14730_GM;
MethodInfo m14730_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14730_GM};
extern Il2CppType t2708_0_0_0;
extern Il2CppType t2708_0_0_0;
static ParameterInfo t243_m14731_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2708_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14731_GM;
MethodInfo m14731_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t243_m14731_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14731_GM};
extern Il2CppType t2704_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14732_GM;
MethodInfo m14732_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t243_TI, &t2704_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14732_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14733_GM;
MethodInfo m14733_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14733_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14734_GM;
MethodInfo m14734_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14734_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m14735_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14735_GM;
MethodInfo m14735_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t243_m14735_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14735_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1937_GM;
MethodInfo m1937_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t243_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1937_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t243_m1936_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1936_GM;
MethodInfo m1936_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t243_TI, &t39_0_0_0, RuntimeInvoker_t29_t44, t243_m1936_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1936_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t243_m14736_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14736_GM;
MethodInfo m14736_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t243_m14736_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14736_GM};
static MethodInfo* t243_MIs[] =
{
	&m1932_MI,
	&m14695_MI,
	&m14696_MI,
	&m14697_MI,
	&m14698_MI,
	&m14699_MI,
	&m14700_MI,
	&m14701_MI,
	&m14702_MI,
	&m14703_MI,
	&m14704_MI,
	&m14705_MI,
	&m14706_MI,
	&m14707_MI,
	&m14708_MI,
	&m14709_MI,
	&m14710_MI,
	&m14711_MI,
	&m1939_MI,
	&m14712_MI,
	&m14713_MI,
	&m14714_MI,
	&m14715_MI,
	&m14716_MI,
	&m14717_MI,
	&m1933_MI,
	&m14718_MI,
	&m1941_MI,
	&m14719_MI,
	&m14720_MI,
	&m14721_MI,
	&m14722_MI,
	&m14723_MI,
	&m14724_MI,
	&m14725_MI,
	&m14726_MI,
	&m1938_MI,
	&m14727_MI,
	&m14728_MI,
	&m14729_MI,
	&m14730_MI,
	&m14731_MI,
	&m14732_MI,
	&m14733_MI,
	&m14734_MI,
	&m14735_MI,
	&m1937_MI,
	&m1936_MI,
	&m14736_MI,
	NULL
};
extern MethodInfo m14699_MI;
extern MethodInfo m14698_MI;
extern MethodInfo m14700_MI;
extern MethodInfo m14717_MI;
extern MethodInfo m14701_MI;
extern MethodInfo m14702_MI;
extern MethodInfo m14703_MI;
extern MethodInfo m14704_MI;
extern MethodInfo m14718_MI;
extern MethodInfo m14697_MI;
static MethodInfo* t243_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14699_MI,
	&m1937_MI,
	&m14706_MI,
	&m14707_MI,
	&m14698_MI,
	&m14708_MI,
	&m14709_MI,
	&m14710_MI,
	&m14711_MI,
	&m14700_MI,
	&m14717_MI,
	&m14701_MI,
	&m14702_MI,
	&m14703_MI,
	&m14704_MI,
	&m14728_MI,
	&m1937_MI,
	&m14705_MI,
	&m1939_MI,
	&m14717_MI,
	&m1933_MI,
	&m14718_MI,
	&m1938_MI,
	&m14697_MI,
	&m14722_MI,
	&m14725_MI,
	&m14728_MI,
	&m1936_MI,
	&m14736_MI,
};
static TypeInfo* t243_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2706_TI,
	&t246_TI,
	&t2710_TI,
};
static Il2CppInterfaceOffsetPair t243_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2706_TI, 20},
	{ &t246_TI, 27},
	{ &t2710_TI, 28},
};
extern TypeInfo t243_TI;
extern TypeInfo t2704_TI;
extern TypeInfo t2709_TI;
extern TypeInfo t39_TI;
extern TypeInfo t2706_TI;
extern TypeInfo t2707_TI;
static Il2CppRGCTXData t243_RGCTXData[37] = 
{
	&t243_TI/* Static Usage */,
	&t2704_TI/* Array Usage */,
	&m14721_MI/* Method Usage */,
	&t2709_TI/* Class Usage */,
	&t39_TI/* Class Usage */,
	&m1939_MI/* Method Usage */,
	&m1933_MI/* Method Usage */,
	&m14722_MI/* Method Usage */,
	&m14724_MI/* Method Usage */,
	&m14725_MI/* Method Usage */,
	&m1938_MI/* Method Usage */,
	&m1936_MI/* Method Usage */,
	&m14736_MI/* Method Usage */,
	&m14712_MI/* Method Usage */,
	&m14734_MI/* Method Usage */,
	&m14735_MI/* Method Usage */,
	&m28438_MI/* Method Usage */,
	&m28443_MI/* Method Usage */,
	&m28445_MI/* Method Usage */,
	&m28437_MI/* Method Usage */,
	&m14726_MI/* Method Usage */,
	&t2706_TI/* Class Usage */,
	&m14713_MI/* Method Usage */,
	&m14714_MI/* Method Usage */,
	&t2707_TI/* Class Usage */,
	&m14743_MI/* Method Usage */,
	&m21528_MI/* Method Usage */,
	&m14719_MI/* Method Usage */,
	&m14720_MI/* Method Usage */,
	&m14817_MI/* Method Usage */,
	&m14737_MI/* Method Usage */,
	&m14723_MI/* Method Usage */,
	&m14728_MI/* Method Usage */,
	&m14823_MI/* Method Usage */,
	&m21530_MI/* Method Usage */,
	&m21538_MI/* Method Usage */,
	&m21526_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t243_0_0_0;
extern Il2CppType t243_1_0_0;
struct t243;
extern Il2CppGenericClass t243_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t243_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t243_MIs, t243_PIs, t243_FIs, NULL, &t29_TI, NULL, NULL, &t243_TI, t243_ITIs, t243_VT, &t1261__CustomAttributeCache, &t243_TI, &t243_0_0_0, &t243_1_0_0, t243_IOs, &t243_GC, NULL, t243_FDVs, NULL, t243_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t243), 0, -1, sizeof(t243_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14740_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>
extern Il2CppType t243_0_0_1;
FieldInfo t2709_f0_FieldInfo = 
{
	"l", &t243_0_0_1, &t2709_TI, offsetof(t2709, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2709_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2709_TI, offsetof(t2709, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2709_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2709_TI, offsetof(t2709, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t39_0_0_1;
FieldInfo t2709_f3_FieldInfo = 
{
	"current", &t39_0_0_1, &t2709_TI, offsetof(t2709, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2709_FIs[] =
{
	&t2709_f0_FieldInfo,
	&t2709_f1_FieldInfo,
	&t2709_f2_FieldInfo,
	&t2709_f3_FieldInfo,
	NULL
};
extern MethodInfo m14738_MI;
static PropertyInfo t2709____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2709_TI, "System.Collections.IEnumerator.Current", &m14738_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14742_MI;
static PropertyInfo t2709____Current_PropertyInfo = 
{
	&t2709_TI, "Current", &m14742_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2709_PIs[] =
{
	&t2709____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2709____Current_PropertyInfo,
	NULL
};
extern Il2CppType t243_0_0_0;
static ParameterInfo t2709_m14737_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t243_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14737_GM;
MethodInfo m14737_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2709_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2709_m14737_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14737_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14738_GM;
MethodInfo m14738_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2709_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14738_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14739_GM;
MethodInfo m14739_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2709_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14739_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14740_GM;
MethodInfo m14740_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2709_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14740_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14741_GM;
MethodInfo m14741_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2709_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14741_GM};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14742_GM;
MethodInfo m14742_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2709_TI, &t39_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14742_GM};
static MethodInfo* t2709_MIs[] =
{
	&m14737_MI,
	&m14738_MI,
	&m14739_MI,
	&m14740_MI,
	&m14741_MI,
	&m14742_MI,
	NULL
};
extern MethodInfo m14741_MI;
extern MethodInfo m14739_MI;
static MethodInfo* t2709_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14738_MI,
	&m14741_MI,
	&m14739_MI,
	&m14742_MI,
};
static TypeInfo* t2709_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2705_TI,
};
static Il2CppInterfaceOffsetPair t2709_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2705_TI, 7},
};
extern TypeInfo t39_TI;
extern TypeInfo t2709_TI;
static Il2CppRGCTXData t2709_RGCTXData[3] = 
{
	&m14740_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&t2709_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2709_0_0_0;
extern Il2CppType t2709_1_0_0;
extern Il2CppGenericClass t2709_GC;
TypeInfo t2709_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2709_MIs, t2709_PIs, t2709_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2709_TI, t2709_ITIs, t2709_VT, &EmptyCustomAttributesCache, &t2709_TI, &t2709_0_0_0, &t2709_1_0_0, t2709_IOs, &t2709_GC, NULL, NULL, NULL, t2709_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2709)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2711MD.h"
extern MethodInfo m14772_MI;
extern MethodInfo m14804_MI;
extern MethodInfo m28442_MI;
extern MethodInfo m28448_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Toggle>
extern Il2CppType t2710_0_0_1;
FieldInfo t2707_f0_FieldInfo = 
{
	"list", &t2710_0_0_1, &t2707_TI, offsetof(t2707, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2707_FIs[] =
{
	&t2707_f0_FieldInfo,
	NULL
};
extern MethodInfo m14749_MI;
extern MethodInfo m14750_MI;
static PropertyInfo t2707____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2707_TI, "System.Collections.Generic.IList<T>.Item", &m14749_MI, &m14750_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14751_MI;
static PropertyInfo t2707____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2707_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14751_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14761_MI;
static PropertyInfo t2707____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2707_TI, "System.Collections.ICollection.IsSynchronized", &m14761_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14762_MI;
static PropertyInfo t2707____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2707_TI, "System.Collections.ICollection.SyncRoot", &m14762_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14763_MI;
static PropertyInfo t2707____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2707_TI, "System.Collections.IList.IsFixedSize", &m14763_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14764_MI;
static PropertyInfo t2707____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2707_TI, "System.Collections.IList.IsReadOnly", &m14764_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14765_MI;
extern MethodInfo m14766_MI;
static PropertyInfo t2707____System_Collections_IList_Item_PropertyInfo = 
{
	&t2707_TI, "System.Collections.IList.Item", &m14765_MI, &m14766_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14771_MI;
static PropertyInfo t2707____Count_PropertyInfo = 
{
	&t2707_TI, "Count", &m14771_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2707____Item_PropertyInfo = 
{
	&t2707_TI, "Item", &m14772_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2707_PIs[] =
{
	&t2707____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2707____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2707____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2707____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2707____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2707____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2707____System_Collections_IList_Item_PropertyInfo,
	&t2707____Count_PropertyInfo,
	&t2707____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2710_0_0_0;
static ParameterInfo t2707_m14743_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2710_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14743_GM;
MethodInfo m14743_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2707_m14743_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14743_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2707_m14744_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14744_GM;
MethodInfo m14744_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2707_m14744_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14744_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14745_GM;
MethodInfo m14745_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14745_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2707_m14746_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14746_GM;
MethodInfo m14746_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2707_m14746_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14746_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2707_m14747_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14747_GM;
MethodInfo m14747_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2707_m14747_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14747_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14748_GM;
MethodInfo m14748_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2707_m14748_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14748_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14749_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14749_GM;
MethodInfo m14749_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2707_TI, &t39_0_0_0, RuntimeInvoker_t29_t44, t2707_m14749_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14749_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2707_m14750_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14750_GM;
MethodInfo m14750_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2707_m14750_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14750_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14751_GM;
MethodInfo m14751_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14751_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14752_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14752_GM;
MethodInfo m14752_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2707_m14752_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14752_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14753_GM;
MethodInfo m14753_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2707_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14753_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2707_m14754_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14754_GM;
MethodInfo m14754_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2707_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2707_m14754_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14754_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14755_GM;
MethodInfo m14755_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14755_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2707_m14756_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14756_GM;
MethodInfo m14756_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2707_m14756_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14756_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2707_m14757_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14757_GM;
MethodInfo m14757_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2707_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2707_m14757_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14757_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2707_m14758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14758_GM;
MethodInfo m14758_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2707_m14758_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14758_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2707_m14759_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14759_GM;
MethodInfo m14759_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2707_m14759_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14759_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14760_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14760_GM;
MethodInfo m14760_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2707_m14760_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14760_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14761_GM;
MethodInfo m14761_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14761_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14762_GM;
MethodInfo m14762_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2707_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14762_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14763_GM;
MethodInfo m14763_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14763_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14764_GM;
MethodInfo m14764_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14764_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14765_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14765_GM;
MethodInfo m14765_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2707_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2707_m14765_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14765_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2707_m14766_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14766_GM;
MethodInfo m14766_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2707_m14766_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14766_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2707_m14767_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14767_GM;
MethodInfo m14767_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2707_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2707_m14767_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14767_GM};
extern Il2CppType t2704_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14768_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2704_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14768_GM;
MethodInfo m14768_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2707_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2707_m14768_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14768_GM};
extern Il2CppType t2705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14769_GM;
MethodInfo m14769_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2707_TI, &t2705_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14769_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2707_m14770_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14770_GM;
MethodInfo m14770_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2707_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2707_m14770_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14770_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14771_GM;
MethodInfo m14771_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2707_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14771_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2707_m14772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14772_GM;
MethodInfo m14772_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2707_TI, &t39_0_0_0, RuntimeInvoker_t29_t44, t2707_m14772_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14772_GM};
static MethodInfo* t2707_MIs[] =
{
	&m14743_MI,
	&m14744_MI,
	&m14745_MI,
	&m14746_MI,
	&m14747_MI,
	&m14748_MI,
	&m14749_MI,
	&m14750_MI,
	&m14751_MI,
	&m14752_MI,
	&m14753_MI,
	&m14754_MI,
	&m14755_MI,
	&m14756_MI,
	&m14757_MI,
	&m14758_MI,
	&m14759_MI,
	&m14760_MI,
	&m14761_MI,
	&m14762_MI,
	&m14763_MI,
	&m14764_MI,
	&m14765_MI,
	&m14766_MI,
	&m14767_MI,
	&m14768_MI,
	&m14769_MI,
	&m14770_MI,
	&m14771_MI,
	&m14772_MI,
	NULL
};
extern MethodInfo m14753_MI;
extern MethodInfo m14752_MI;
extern MethodInfo m14754_MI;
extern MethodInfo m14755_MI;
extern MethodInfo m14756_MI;
extern MethodInfo m14757_MI;
extern MethodInfo m14758_MI;
extern MethodInfo m14759_MI;
extern MethodInfo m14760_MI;
extern MethodInfo m14744_MI;
extern MethodInfo m14745_MI;
extern MethodInfo m14767_MI;
extern MethodInfo m14768_MI;
extern MethodInfo m14747_MI;
extern MethodInfo m14770_MI;
extern MethodInfo m14746_MI;
extern MethodInfo m14748_MI;
extern MethodInfo m14769_MI;
static MethodInfo* t2707_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14753_MI,
	&m14771_MI,
	&m14761_MI,
	&m14762_MI,
	&m14752_MI,
	&m14763_MI,
	&m14764_MI,
	&m14765_MI,
	&m14766_MI,
	&m14754_MI,
	&m14755_MI,
	&m14756_MI,
	&m14757_MI,
	&m14758_MI,
	&m14759_MI,
	&m14760_MI,
	&m14771_MI,
	&m14751_MI,
	&m14744_MI,
	&m14745_MI,
	&m14767_MI,
	&m14768_MI,
	&m14747_MI,
	&m14770_MI,
	&m14746_MI,
	&m14748_MI,
	&m14749_MI,
	&m14750_MI,
	&m14769_MI,
	&m14772_MI,
};
static TypeInfo* t2707_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2706_TI,
	&t2710_TI,
	&t246_TI,
};
static Il2CppInterfaceOffsetPair t2707_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2706_TI, 20},
	{ &t2710_TI, 27},
	{ &t246_TI, 32},
};
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2707_RGCTXData[9] = 
{
	&m14772_MI/* Method Usage */,
	&m14804_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m28442_MI/* Method Usage */,
	&m28448_MI/* Method Usage */,
	&m28446_MI/* Method Usage */,
	&m28443_MI/* Method Usage */,
	&m28445_MI/* Method Usage */,
	&m28438_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2707_0_0_0;
extern Il2CppType t2707_1_0_0;
struct t2707;
extern Il2CppGenericClass t2707_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2707_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2707_MIs, t2707_PIs, t2707_FIs, NULL, &t29_TI, NULL, NULL, &t2707_TI, t2707_ITIs, t2707_VT, &t1263__CustomAttributeCache, &t2707_TI, &t2707_0_0_0, &t2707_1_0_0, t2707_IOs, &t2707_GC, NULL, NULL, NULL, t2707_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2707), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2711.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2711_TI;

extern MethodInfo m14807_MI;
extern MethodInfo m14808_MI;
extern MethodInfo m14805_MI;
extern MethodInfo m14803_MI;
extern MethodInfo m1932_MI;
extern MethodInfo m14796_MI;
extern MethodInfo m14806_MI;
extern MethodInfo m14794_MI;
extern MethodInfo m14799_MI;
extern MethodInfo m14790_MI;
extern MethodInfo m28441_MI;
extern MethodInfo m28449_MI;
extern MethodInfo m28450_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UI.Toggle>
extern Il2CppType t2710_0_0_1;
FieldInfo t2711_f0_FieldInfo = 
{
	"list", &t2710_0_0_1, &t2711_TI, offsetof(t2711, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2711_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2711_TI, offsetof(t2711, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2711_FIs[] =
{
	&t2711_f0_FieldInfo,
	&t2711_f1_FieldInfo,
	NULL
};
extern MethodInfo m14774_MI;
static PropertyInfo t2711____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2711_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14774_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14782_MI;
static PropertyInfo t2711____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2711_TI, "System.Collections.ICollection.IsSynchronized", &m14782_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14783_MI;
static PropertyInfo t2711____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2711_TI, "System.Collections.ICollection.SyncRoot", &m14783_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14784_MI;
static PropertyInfo t2711____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2711_TI, "System.Collections.IList.IsFixedSize", &m14784_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14785_MI;
static PropertyInfo t2711____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2711_TI, "System.Collections.IList.IsReadOnly", &m14785_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14786_MI;
extern MethodInfo m14787_MI;
static PropertyInfo t2711____System_Collections_IList_Item_PropertyInfo = 
{
	&t2711_TI, "System.Collections.IList.Item", &m14786_MI, &m14787_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14800_MI;
static PropertyInfo t2711____Count_PropertyInfo = 
{
	&t2711_TI, "Count", &m14800_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14801_MI;
extern MethodInfo m14802_MI;
static PropertyInfo t2711____Item_PropertyInfo = 
{
	&t2711_TI, "Item", &m14801_MI, &m14802_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2711_PIs[] =
{
	&t2711____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2711____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2711____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2711____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2711____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2711____System_Collections_IList_Item_PropertyInfo,
	&t2711____Count_PropertyInfo,
	&t2711____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14773_GM;
MethodInfo m14773_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14773_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14774_GM;
MethodInfo m14774_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14774_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2711_m14775_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14775_GM;
MethodInfo m14775_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2711_m14775_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14775_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14776_GM;
MethodInfo m14776_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2711_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14776_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14777_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14777_GM;
MethodInfo m14777_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2711_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2711_m14777_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14777_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14778_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14778_GM;
MethodInfo m14778_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2711_m14778_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14778_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14779_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14779_GM;
MethodInfo m14779_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2711_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2711_m14779_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14779_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14780_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14780_GM;
MethodInfo m14780_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2711_m14780_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14780_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14781_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14781_GM;
MethodInfo m14781_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2711_m14781_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14781_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14782_GM;
MethodInfo m14782_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14782_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14783_GM;
MethodInfo m14783_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2711_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14783_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14784_GM;
MethodInfo m14784_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14784_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14785_GM;
MethodInfo m14785_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14785_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2711_m14786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14786_GM;
MethodInfo m14786_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2711_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2711_m14786_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14786_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14787_GM;
MethodInfo m14787_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2711_m14787_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14787_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14788_GM;
MethodInfo m14788_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2711_m14788_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14788_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14789_GM;
MethodInfo m14789_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14789_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14790_GM;
MethodInfo m14790_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14790_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14791_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14791_GM;
MethodInfo m14791_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2711_m14791_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14791_GM};
extern Il2CppType t2704_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2711_m14792_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2704_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14792_GM;
MethodInfo m14792_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2711_m14792_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14792_GM};
extern Il2CppType t2705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14793_GM;
MethodInfo m14793_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2711_TI, &t2705_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14793_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14794_GM;
MethodInfo m14794_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2711_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2711_m14794_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14794_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14795_GM;
MethodInfo m14795_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2711_m14795_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14795_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14796_GM;
MethodInfo m14796_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2711_m14796_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14796_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14797_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14797_GM;
MethodInfo m14797_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2711_m14797_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14797_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2711_m14798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14798_GM;
MethodInfo m14798_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2711_m14798_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14798_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2711_m14799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14799_GM;
MethodInfo m14799_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2711_m14799_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14799_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14800_GM;
MethodInfo m14800_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2711_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14800_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2711_m14801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14801_GM;
MethodInfo m14801_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2711_TI, &t39_0_0_0, RuntimeInvoker_t29_t44, t2711_m14801_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14801_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14802_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14802_GM;
MethodInfo m14802_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2711_m14802_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14802_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2711_m14803_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14803_GM;
MethodInfo m14803_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2711_m14803_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14803_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14804_GM;
MethodInfo m14804_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2711_m14804_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14804_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2711_m14805_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14805_GM;
MethodInfo m14805_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2711_TI, &t39_0_0_0, RuntimeInvoker_t29_t29, t2711_m14805_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14805_GM};
extern Il2CppType t2710_0_0_0;
static ParameterInfo t2711_m14806_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2710_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14806_GM;
MethodInfo m14806_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2711_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2711_m14806_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14806_GM};
extern Il2CppType t2710_0_0_0;
static ParameterInfo t2711_m14807_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2710_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14807_GM;
MethodInfo m14807_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2711_m14807_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14807_GM};
extern Il2CppType t2710_0_0_0;
static ParameterInfo t2711_m14808_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2710_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14808_GM;
MethodInfo m14808_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2711_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2711_m14808_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14808_GM};
static MethodInfo* t2711_MIs[] =
{
	&m14773_MI,
	&m14774_MI,
	&m14775_MI,
	&m14776_MI,
	&m14777_MI,
	&m14778_MI,
	&m14779_MI,
	&m14780_MI,
	&m14781_MI,
	&m14782_MI,
	&m14783_MI,
	&m14784_MI,
	&m14785_MI,
	&m14786_MI,
	&m14787_MI,
	&m14788_MI,
	&m14789_MI,
	&m14790_MI,
	&m14791_MI,
	&m14792_MI,
	&m14793_MI,
	&m14794_MI,
	&m14795_MI,
	&m14796_MI,
	&m14797_MI,
	&m14798_MI,
	&m14799_MI,
	&m14800_MI,
	&m14801_MI,
	&m14802_MI,
	&m14803_MI,
	&m14804_MI,
	&m14805_MI,
	&m14806_MI,
	&m14807_MI,
	&m14808_MI,
	NULL
};
extern MethodInfo m14776_MI;
extern MethodInfo m14775_MI;
extern MethodInfo m14777_MI;
extern MethodInfo m14789_MI;
extern MethodInfo m14778_MI;
extern MethodInfo m14779_MI;
extern MethodInfo m14780_MI;
extern MethodInfo m14781_MI;
extern MethodInfo m14798_MI;
extern MethodInfo m14788_MI;
extern MethodInfo m14791_MI;
extern MethodInfo m14792_MI;
extern MethodInfo m14797_MI;
extern MethodInfo m14795_MI;
extern MethodInfo m14793_MI;
static MethodInfo* t2711_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14776_MI,
	&m14800_MI,
	&m14782_MI,
	&m14783_MI,
	&m14775_MI,
	&m14784_MI,
	&m14785_MI,
	&m14786_MI,
	&m14787_MI,
	&m14777_MI,
	&m14789_MI,
	&m14778_MI,
	&m14779_MI,
	&m14780_MI,
	&m14781_MI,
	&m14798_MI,
	&m14800_MI,
	&m14774_MI,
	&m14788_MI,
	&m14789_MI,
	&m14791_MI,
	&m14792_MI,
	&m14797_MI,
	&m14794_MI,
	&m14795_MI,
	&m14798_MI,
	&m14801_MI,
	&m14802_MI,
	&m14793_MI,
	&m14790_MI,
	&m14796_MI,
	&m14799_MI,
	&m14803_MI,
};
static TypeInfo* t2711_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2706_TI,
	&t2710_TI,
	&t246_TI,
};
static Il2CppInterfaceOffsetPair t2711_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2706_TI, 20},
	{ &t2710_TI, 27},
	{ &t246_TI, 32},
};
extern TypeInfo t243_TI;
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2711_RGCTXData[25] = 
{
	&t243_TI/* Class Usage */,
	&m1932_MI/* Method Usage */,
	&m28439_MI/* Method Usage */,
	&m28445_MI/* Method Usage */,
	&m28438_MI/* Method Usage */,
	&m14805_MI/* Method Usage */,
	&m14796_MI/* Method Usage */,
	&m14804_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m28442_MI/* Method Usage */,
	&m28448_MI/* Method Usage */,
	&m14806_MI/* Method Usage */,
	&m14794_MI/* Method Usage */,
	&m14799_MI/* Method Usage */,
	&m14807_MI/* Method Usage */,
	&m14808_MI/* Method Usage */,
	&m28446_MI/* Method Usage */,
	&m14803_MI/* Method Usage */,
	&m14790_MI/* Method Usage */,
	&m28441_MI/* Method Usage */,
	&m28443_MI/* Method Usage */,
	&m28449_MI/* Method Usage */,
	&m28450_MI/* Method Usage */,
	&m28447_MI/* Method Usage */,
	&t39_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2711_0_0_0;
extern Il2CppType t2711_1_0_0;
struct t2711;
extern Il2CppGenericClass t2711_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2711_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2711_MIs, t2711_PIs, t2711_FIs, NULL, &t29_TI, NULL, NULL, &t2711_TI, t2711_ITIs, t2711_VT, &t1262__CustomAttributeCache, &t2711_TI, &t2711_0_0_0, &t2711_1_0_0, t2711_IOs, &t2711_GC, NULL, NULL, NULL, t2711_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2711), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2712_TI;
#include "t2712MD.h"

#include "t2713.h"
extern TypeInfo t6706_TI;
extern TypeInfo t2713_TI;
#include "t2713MD.h"
extern Il2CppType t6706_0_0_0;
extern MethodInfo m14814_MI;
extern MethodInfo m28479_MI;
extern MethodInfo m21527_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Toggle>
extern Il2CppType t2712_0_0_49;
FieldInfo t2712_f0_FieldInfo = 
{
	"_default", &t2712_0_0_49, &t2712_TI, offsetof(t2712_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2712_FIs[] =
{
	&t2712_f0_FieldInfo,
	NULL
};
extern MethodInfo m14813_MI;
static PropertyInfo t2712____Default_PropertyInfo = 
{
	&t2712_TI, "Default", &m14813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2712_PIs[] =
{
	&t2712____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14809_GM;
MethodInfo m14809_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2712_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14809_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14810_GM;
MethodInfo m14810_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2712_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14810_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2712_m14811_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14811_GM;
MethodInfo m14811_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2712_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2712_m14811_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14811_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2712_m14812_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14812_GM;
MethodInfo m14812_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2712_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2712_m14812_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14812_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2712_m28479_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28479_GM;
MethodInfo m28479_MI = 
{
	"GetHashCode", NULL, &t2712_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2712_m28479_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28479_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2712_m21527_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21527_GM;
MethodInfo m21527_MI = 
{
	"Equals", NULL, &t2712_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2712_m21527_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21527_GM};
extern Il2CppType t2712_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14813_GM;
MethodInfo m14813_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2712_TI, &t2712_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14813_GM};
static MethodInfo* t2712_MIs[] =
{
	&m14809_MI,
	&m14810_MI,
	&m14811_MI,
	&m14812_MI,
	&m28479_MI,
	&m21527_MI,
	&m14813_MI,
	NULL
};
extern MethodInfo m14812_MI;
extern MethodInfo m14811_MI;
static MethodInfo* t2712_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m21527_MI,
	&m28479_MI,
	&m14812_MI,
	&m14811_MI,
	NULL,
	NULL,
};
extern TypeInfo t6707_TI;
static TypeInfo* t2712_ITIs[] = 
{
	&t6707_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2712_IOs[] = 
{
	{ &t6707_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2712_TI;
extern TypeInfo t2712_TI;
extern TypeInfo t2713_TI;
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2712_RGCTXData[9] = 
{
	&t6706_0_0_0/* Type Usage */,
	&t39_0_0_0/* Type Usage */,
	&t2712_TI/* Class Usage */,
	&t2712_TI/* Static Usage */,
	&t2713_TI/* Class Usage */,
	&m14814_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m28479_MI/* Method Usage */,
	&m21527_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2712_0_0_0;
extern Il2CppType t2712_1_0_0;
struct t2712;
extern Il2CppGenericClass t2712_GC;
TypeInfo t2712_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2712_MIs, t2712_PIs, t2712_FIs, NULL, &t29_TI, NULL, NULL, &t2712_TI, t2712_ITIs, t2712_VT, &EmptyCustomAttributesCache, &t2712_TI, &t2712_0_0_0, &t2712_1_0_0, t2712_IOs, &t2712_GC, NULL, NULL, NULL, t2712_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2712), 0, -1, sizeof(t2712_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.Toggle>
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t6707_m28480_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28480_GM;
MethodInfo m28480_MI = 
{
	"Equals", NULL, &t6707_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6707_m28480_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28480_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t6707_m28481_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28481_GM;
MethodInfo m28481_MI = 
{
	"GetHashCode", NULL, &t6707_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6707_m28481_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28481_GM};
static MethodInfo* t6707_MIs[] =
{
	&m28480_MI,
	&m28481_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6707_0_0_0;
extern Il2CppType t6707_1_0_0;
struct t6707;
extern Il2CppGenericClass t6707_GC;
TypeInfo t6707_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6707_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6707_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6707_TI, &t6707_0_0_0, &t6707_1_0_0, NULL, &t6707_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.Toggle>
extern Il2CppType t39_0_0_0;
static ParameterInfo t6706_m28482_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28482_GM;
MethodInfo m28482_MI = 
{
	"Equals", NULL, &t6706_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6706_m28482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28482_GM};
static MethodInfo* t6706_MIs[] =
{
	&m28482_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6706_1_0_0;
struct t6706;
extern Il2CppGenericClass t6706_GC;
TypeInfo t6706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6706_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6706_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6706_TI, &t6706_0_0_0, &t6706_1_0_0, NULL, &t6706_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14809_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Toggle>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14814_GM;
MethodInfo m14814_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2713_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14814_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t2713_m14815_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14815_GM;
MethodInfo m14815_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2713_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2713_m14815_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14815_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2713_m14816_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14816_GM;
MethodInfo m14816_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2713_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2713_m14816_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14816_GM};
static MethodInfo* t2713_MIs[] =
{
	&m14814_MI,
	&m14815_MI,
	&m14816_MI,
	NULL
};
extern MethodInfo m14816_MI;
extern MethodInfo m14815_MI;
static MethodInfo* t2713_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14816_MI,
	&m14815_MI,
	&m14812_MI,
	&m14811_MI,
	&m14815_MI,
	&m14816_MI,
};
static Il2CppInterfaceOffsetPair t2713_IOs[] = 
{
	{ &t6707_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2712_TI;
extern TypeInfo t2712_TI;
extern TypeInfo t2713_TI;
extern TypeInfo t39_TI;
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2713_RGCTXData[11] = 
{
	&t6706_0_0_0/* Type Usage */,
	&t39_0_0_0/* Type Usage */,
	&t2712_TI/* Class Usage */,
	&t2712_TI/* Static Usage */,
	&t2713_TI/* Class Usage */,
	&m14814_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m28479_MI/* Method Usage */,
	&m21527_MI/* Method Usage */,
	&m14809_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2713_0_0_0;
extern Il2CppType t2713_1_0_0;
struct t2713;
extern Il2CppGenericClass t2713_GC;
TypeInfo t2713_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2713_MIs, NULL, NULL, NULL, &t2712_TI, NULL, &t1256_TI, &t2713_TI, NULL, t2713_VT, &EmptyCustomAttributesCache, &t2713_TI, &t2713_0_0_0, &t2713_1_0_0, t2713_IOs, &t2713_GC, NULL, NULL, NULL, t2713_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2713), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.UI.Toggle>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t244_m1940_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1940_GM;
MethodInfo m1940_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t244_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t244_m1940_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1940_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t244_m14817_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14817_GM;
MethodInfo m14817_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t244_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t244_m14817_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14817_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t244_m14818_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14818_GM;
MethodInfo m14818_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t244_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t244_m14818_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14818_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t244_m14819_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14819_GM;
MethodInfo m14819_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t244_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t244_m14819_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14819_GM};
static MethodInfo* t244_MIs[] =
{
	&m1940_MI,
	&m14817_MI,
	&m14818_MI,
	&m14819_MI,
	NULL
};
extern MethodInfo m14818_MI;
extern MethodInfo m14819_MI;
static MethodInfo* t244_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14817_MI,
	&m14818_MI,
	&m14819_MI,
};
static Il2CppInterfaceOffsetPair t244_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t244_1_0_0;
struct t244;
extern Il2CppGenericClass t244_GC;
TypeInfo t244_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t244_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t244_TI, NULL, t244_VT, &EmptyCustomAttributesCache, &t244_TI, &t244_0_0_0, &t244_1_0_0, t244_IOs, &t244_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t244), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2715.h"
extern TypeInfo t4256_TI;
extern TypeInfo t2715_TI;
#include "t2715MD.h"
extern Il2CppType t4256_0_0_0;
extern MethodInfo m14824_MI;
extern MethodInfo m28483_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UI.Toggle>
extern Il2CppType t2714_0_0_49;
FieldInfo t2714_f0_FieldInfo = 
{
	"_default", &t2714_0_0_49, &t2714_TI, offsetof(t2714_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2714_FIs[] =
{
	&t2714_f0_FieldInfo,
	NULL
};
static PropertyInfo t2714____Default_PropertyInfo = 
{
	&t2714_TI, "Default", &m14823_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2714_PIs[] =
{
	&t2714____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14820_GM;
MethodInfo m14820_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2714_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14820_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14821_GM;
MethodInfo m14821_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2714_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14821_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2714_m14822_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14822_GM;
MethodInfo m14822_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2714_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2714_m14822_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14822_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2714_m28483_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28483_GM;
MethodInfo m28483_MI = 
{
	"Compare", NULL, &t2714_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2714_m28483_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28483_GM};
extern Il2CppType t2714_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14823_GM;
MethodInfo m14823_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2714_TI, &t2714_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14823_GM};
static MethodInfo* t2714_MIs[] =
{
	&m14820_MI,
	&m14821_MI,
	&m14822_MI,
	&m28483_MI,
	&m14823_MI,
	NULL
};
extern MethodInfo m14822_MI;
static MethodInfo* t2714_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m28483_MI,
	&m14822_MI,
	NULL,
};
extern TypeInfo t4255_TI;
static TypeInfo* t2714_ITIs[] = 
{
	&t4255_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2714_IOs[] = 
{
	{ &t4255_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2714_TI;
extern TypeInfo t2714_TI;
extern TypeInfo t2715_TI;
extern TypeInfo t39_TI;
static Il2CppRGCTXData t2714_RGCTXData[8] = 
{
	&t4256_0_0_0/* Type Usage */,
	&t39_0_0_0/* Type Usage */,
	&t2714_TI/* Class Usage */,
	&t2714_TI/* Static Usage */,
	&t2715_TI/* Class Usage */,
	&m14824_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m28483_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2714_0_0_0;
extern Il2CppType t2714_1_0_0;
struct t2714;
extern Il2CppGenericClass t2714_GC;
TypeInfo t2714_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2714_MIs, t2714_PIs, t2714_FIs, NULL, &t29_TI, NULL, NULL, &t2714_TI, t2714_ITIs, t2714_VT, &EmptyCustomAttributesCache, &t2714_TI, &t2714_0_0_0, &t2714_1_0_0, t2714_IOs, &t2714_GC, NULL, NULL, NULL, t2714_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2714), 0, -1, sizeof(t2714_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UI.Toggle>
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t4255_m21535_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21535_GM;
MethodInfo m21535_MI = 
{
	"Compare", NULL, &t4255_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4255_m21535_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21535_GM};
static MethodInfo* t4255_MIs[] =
{
	&m21535_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4255_0_0_0;
extern Il2CppType t4255_1_0_0;
struct t4255;
extern Il2CppGenericClass t4255_GC;
TypeInfo t4255_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4255_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4255_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4255_TI, &t4255_0_0_0, &t4255_1_0_0, NULL, &t4255_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UI.Toggle>
extern Il2CppType t39_0_0_0;
static ParameterInfo t4256_m21536_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21536_GM;
MethodInfo m21536_MI = 
{
	"CompareTo", NULL, &t4256_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4256_m21536_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m21536_GM};
static MethodInfo* t4256_MIs[] =
{
	&m21536_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4256_1_0_0;
struct t4256;
extern Il2CppGenericClass t4256_GC;
TypeInfo t4256_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4256_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4256_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4256_TI, &t4256_0_0_0, &t4256_1_0_0, NULL, &t4256_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14820_MI;
extern MethodInfo m21536_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Toggle>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14824_GM;
MethodInfo m14824_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2715_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14824_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2715_m14825_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14825_GM;
MethodInfo m14825_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2715_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2715_m14825_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14825_GM};
static MethodInfo* t2715_MIs[] =
{
	&m14824_MI,
	&m14825_MI,
	NULL
};
extern MethodInfo m14825_MI;
static MethodInfo* t2715_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14825_MI,
	&m14822_MI,
	&m14825_MI,
};
static Il2CppInterfaceOffsetPair t2715_IOs[] = 
{
	{ &t4255_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2714_TI;
extern TypeInfo t2714_TI;
extern TypeInfo t2715_TI;
extern TypeInfo t39_TI;
extern TypeInfo t39_TI;
extern TypeInfo t4256_TI;
static Il2CppRGCTXData t2715_RGCTXData[12] = 
{
	&t4256_0_0_0/* Type Usage */,
	&t39_0_0_0/* Type Usage */,
	&t2714_TI/* Class Usage */,
	&t2714_TI/* Static Usage */,
	&t2715_TI/* Class Usage */,
	&m14824_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&m28483_MI/* Method Usage */,
	&m14820_MI/* Method Usage */,
	&t39_TI/* Class Usage */,
	&t4256_TI/* Class Usage */,
	&m21536_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2715_0_0_0;
extern Il2CppType t2715_1_0_0;
struct t2715;
extern Il2CppGenericClass t2715_GC;
TypeInfo t2715_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2715_MIs, NULL, NULL, NULL, &t2714_TI, NULL, &t1246_TI, &t2715_TI, NULL, t2715_VT, &EmptyCustomAttributesCache, &t2715_TI, &t2715_0_0_0, &t2715_1_0_0, t2715_IOs, &t2715_GC, NULL, NULL, NULL, t2715_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2715), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2708_TI;
#include "t2708MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.UI.Toggle>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2708_m14826_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14826_GM;
MethodInfo m14826_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2708_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2708_m14826_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14826_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
static ParameterInfo t2708_m14827_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14827_GM;
MethodInfo m14827_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2708_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2708_m14827_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14827_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2708_m14828_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14828_GM;
MethodInfo m14828_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2708_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2708_m14828_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m14828_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2708_m14829_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14829_GM;
MethodInfo m14829_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2708_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2708_m14829_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14829_GM};
static MethodInfo* t2708_MIs[] =
{
	&m14826_MI,
	&m14827_MI,
	&m14828_MI,
	&m14829_MI,
	NULL
};
extern MethodInfo m14827_MI;
extern MethodInfo m14828_MI;
extern MethodInfo m14829_MI;
static MethodInfo* t2708_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14827_MI,
	&m14828_MI,
	&m14829_MI,
};
static Il2CppInterfaceOffsetPair t2708_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2708_1_0_0;
struct t2708;
extern Il2CppGenericClass t2708_GC;
TypeInfo t2708_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2708_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2708_TI, NULL, t2708_VT, &EmptyCustomAttributesCache, &t2708_TI, &t2708_0_0_0, &t2708_1_0_0, t2708_IOs, &t2708_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2708), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t245.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t245_TI;
#include "t245MD.h"



extern MethodInfo m1942_MI;
 void m1942 (t245 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m14830_MI;
 bool m14830 (t245 * __this, t39 * p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m14830((t245 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t39 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t39 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m14831_MI;
 t29 * m14831 (t245 * __this, t39 * p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m14832_MI;
 bool m14832 (t245 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t245_m1942_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1942_GM;
MethodInfo m1942_MI = 
{
	".ctor", (methodPointerType)&m1942, &t245_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t245_m1942_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1942_GM};
extern Il2CppType t39_0_0_0;
static ParameterInfo t245_m14830_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14830_GM;
MethodInfo m14830_MI = 
{
	"Invoke", (methodPointerType)&m14830, &t245_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t245_m14830_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14830_GM};
extern Il2CppType t39_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t245_m14831_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t39_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14831_GM;
MethodInfo m14831_MI = 
{
	"BeginInvoke", (methodPointerType)&m14831, &t245_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t245_m14831_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14831_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t245_m14832_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14832_GM;
MethodInfo m14832_MI = 
{
	"EndInvoke", (methodPointerType)&m14832, &t245_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t245_m14832_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14832_GM};
static MethodInfo* t245_MIs[] =
{
	&m1942_MI,
	&m14830_MI,
	&m14831_MI,
	&m14832_MI,
	NULL
};
static MethodInfo* t245_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14830_MI,
	&m14831_MI,
	&m14832_MI,
};
static Il2CppInterfaceOffsetPair t245_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t245_0_0_0;
extern Il2CppType t245_1_0_0;
struct t245;
extern Il2CppGenericClass t245_GC;
TypeInfo t245_TI = 
{
	&g_System_Core_dll_Image, NULL, "Func`2", "System", t245_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t245_TI, NULL, t245_VT, &EmptyCustomAttributesCache, &t245_TI, &t245_0_0_0, &t245_1_0_0, t245_IOs, &t245_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t245), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t407.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t407_TI;
#include "t407MD.h"



extern MethodInfo m14833_MI;
 void m14833 (t407 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m14834_MI;
 bool m14834 (t407 * __this, t29 * p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m14834((t407 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m14835_MI;
 t29 * m14835 (t407 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m14836_MI;
 bool m14836 (t407 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Func`2<System.Object,System.Boolean>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t407_m14833_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14833_GM;
MethodInfo m14833_MI = 
{
	".ctor", (methodPointerType)&m14833, &t407_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t407_m14833_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14833_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t407_m14834_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14834_GM;
MethodInfo m14834_MI = 
{
	"Invoke", (methodPointerType)&m14834, &t407_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t407_m14834_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14834_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t407_m14835_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14835_GM;
MethodInfo m14835_MI = 
{
	"BeginInvoke", (methodPointerType)&m14835, &t407_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t407_m14835_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14835_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t407_m14836_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14836_GM;
MethodInfo m14836_MI = 
{
	"EndInvoke", (methodPointerType)&m14836, &t407_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t407_m14836_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14836_GM};
static MethodInfo* t407_MIs[] =
{
	&m14833_MI,
	&m14834_MI,
	&m14835_MI,
	&m14836_MI,
	NULL
};
static MethodInfo* t407_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14834_MI,
	&m14835_MI,
	&m14836_MI,
};
static Il2CppInterfaceOffsetPair t407_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t407_0_0_0;
extern Il2CppType t407_1_0_0;
struct t407;
extern Il2CppGenericClass t407_GC;
TypeInfo t407_TI = 
{
	&g_System_Core_dll_Image, NULL, "Func`2", "System", t407_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t407_TI, NULL, t407_VT, &EmptyCustomAttributesCache, &t407_TI, &t407_0_0_0, &t407_1_0_0, t407_IOs, &t407_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t407), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2716.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2716_TI;
#include "t2716MD.h"

#include "t344.h"
extern TypeInfo t346_TI;
extern TypeInfo t2183_TI;
#include "t1601MD.h"
extern MethodInfo m14841_MI;
extern MethodInfo m8777_MI;
extern MethodInfo m14837_MI;
extern MethodInfo m26261_MI;
extern MethodInfo m26253_MI;


 void m14837_gshared (t2716 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m14838_MI;
 t29 * m14838_gshared (t2716 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m14839_MI;
 t29 * m14839_gshared (t2716 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f5);
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m14840_MI;
 t29 * m14840_gshared (t2716 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (t29*)VirtFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), __this);
		return L_0;
	}
}
 t29* m14841_gshared (t2716 * __this, MethodInfo* method)
{
	t2716 * V_0 = {0};
	{
		int32_t* L_0 = &(__this->f4);
		int32_t L_1 = m8777(NULL, L_0, 0, ((int32_t)-2), &m8777_MI);
		if ((((uint32_t)L_1) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		t2716 * L_2 = (t2716 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (t2716 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = L_2;
		t29* L_3 = (__this->f6);
		V_0->f0 = L_3;
		t407 * L_4 = (__this->f7);
		V_0->f3 = L_4;
		return V_0;
	}
}
extern MethodInfo m14842_MI;
 bool m14842_gshared (t2716 * __this, MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		int32_t L_0 = (__this->f4);
		V_0 = L_0;
		__this->f4 = (-1);
		V_1 = 0;
		if (V_0 == 0)
		{
			goto IL_0023;
		}
		if (V_0 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		t29* L_1 = (__this->f0);
		t29* L_2 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_1);
		__this->f1 = L_2;
		V_0 = ((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			if (((uint32_t)(V_0-1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			t29* L_3 = (__this->f1);
			t29 * L_4 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), L_3);
			__this->f2 = L_4;
			t407 * L_5 = (__this->f3);
			t29 * L_6 = (__this->f2);
			bool L_7 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), L_5, L_6);
			if (!L_7)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			t29 * L_8 = (__this->f2);
			__this->f5 = L_8;
			__this->f4 = 1;
			V_1 = 1;
			// IL_0084: leave IL_00c0
			leaveInstructions[0] = 0xC0; // 1
			THROW_SENTINEL(IL_00c0);
			// finally target depth: 1
		}

IL_0089:
		{
			t29* L_9 = (__this->f1);
			bool L_10 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, L_9);
			if (L_10)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			// IL_0099: leave IL_00b7
			leaveInstructions[0] = 0xB7; // 1
			THROW_SENTINEL(IL_00b7);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_009e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_009e;
	}

IL_009e:
	{ // begin finally (depth: 1)
		{
			if (!V_1)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xC0:
					goto IL_00c0;
				case 0xB7:
					goto IL_00b7;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_00a2:
		{
			t29* L_11 = (__this->f1);
			if (L_11)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xC0:
					goto IL_00c0;
				case 0xB7:
					goto IL_00b7;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_00ab:
		{
			t29* L_12 = (__this->f1);
			InterfaceActionInvoker0::Invoke(&m1428_MI, L_12);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xC0:
					goto IL_00c0;
				case 0xB7:
					goto IL_00b7;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_00b7:
	{
		__this->f4 = (-1);
	}

IL_00be:
	{
		return 0;
	}

IL_00c0:
	{
		return 1;
	}
	// Dead block : IL_00c2: ldloc.2
}
extern MethodInfo m14843_MI;
 void m14843_gshared (t2716 * __this, MethodInfo* method)
{
	uint32_t V_0 = 0;
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		int32_t L_0 = (__this->f4);
		V_0 = L_0;
		__this->f4 = (-1);
		if (V_0 == 0)
		{
			goto IL_003b;
		}
		if (V_0 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		// IL_0021: leave IL_003b
		leaveInstructions[0] = 0x3B; // 1
		THROW_SENTINEL(IL_003b);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0026;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0026;
	}

IL_0026:
	{ // begin finally (depth: 1)
		{
			t29* L_1 = (__this->f1);
			if (L_1)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x3B:
					goto IL_003b;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_002f:
		{
			t29* L_2 = (__this->f1);
			InterfaceActionInvoker0::Invoke(&m1428_MI, L_2);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x3B:
					goto IL_003b;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_003b:
	{
		return;
	}
}
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
extern Il2CppType t2183_0_0_3;
FieldInfo t2716_f0_FieldInfo = 
{
	"source", &t2183_0_0_3, &t2716_TI, offsetof(t2716, f0), &EmptyCustomAttributesCache};
extern Il2CppType t346_0_0_3;
FieldInfo t2716_f1_FieldInfo = 
{
	"<$s_97>__0", &t346_0_0_3, &t2716_TI, offsetof(t2716, f1), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_3;
FieldInfo t2716_f2_FieldInfo = 
{
	"<element>__1", &t29_0_0_3, &t2716_TI, offsetof(t2716, f2), &EmptyCustomAttributesCache};
extern Il2CppType t407_0_0_3;
FieldInfo t2716_f3_FieldInfo = 
{
	"predicate", &t407_0_0_3, &t2716_TI, offsetof(t2716, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t2716_f4_FieldInfo = 
{
	"$PC", &t44_0_0_3, &t2716_TI, offsetof(t2716, f4), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_3;
FieldInfo t2716_f5_FieldInfo = 
{
	"$current", &t29_0_0_3, &t2716_TI, offsetof(t2716, f5), &EmptyCustomAttributesCache};
extern Il2CppType t2183_0_0_3;
FieldInfo t2716_f6_FieldInfo = 
{
	"<$>source", &t2183_0_0_3, &t2716_TI, offsetof(t2716, f6), &EmptyCustomAttributesCache};
extern Il2CppType t407_0_0_3;
FieldInfo t2716_f7_FieldInfo = 
{
	"<$>predicate", &t407_0_0_3, &t2716_TI, offsetof(t2716, f7), &EmptyCustomAttributesCache};
static FieldInfo* t2716_FIs[] =
{
	&t2716_f0_FieldInfo,
	&t2716_f1_FieldInfo,
	&t2716_f2_FieldInfo,
	&t2716_f3_FieldInfo,
	&t2716_f4_FieldInfo,
	&t2716_f5_FieldInfo,
	&t2716_f6_FieldInfo,
	&t2716_f7_FieldInfo,
	NULL
};
static PropertyInfo t2716____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&t2716_TI, "System.Collections.Generic.IEnumerator<TSource>.Current", &m14838_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2716____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2716_TI, "System.Collections.IEnumerator.Current", &m14839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2716_PIs[] =
{
	&t2716____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&t2716____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14837_GM;
MethodInfo m14837_MI = 
{
	".ctor", (methodPointerType)&m14837_gshared, &t2716_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14837_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3045;
extern Il2CppGenericMethod m14838_GM;
MethodInfo m14838_MI = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current", (methodPointerType)&m14838_gshared, &t2716_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3045, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14838_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3046;
extern Il2CppGenericMethod m14839_GM;
MethodInfo m14839_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14839_gshared, &t2716_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3046, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14839_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3047;
extern Il2CppGenericMethod m14840_GM;
MethodInfo m14840_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m14840_gshared, &t2716_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3047, 481, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14840_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3048;
extern Il2CppGenericMethod m14841_GM;
MethodInfo m14841_MI = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator", (methodPointerType)&m14841_gshared, &t2716_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3048, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14841_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14842_GM;
MethodInfo m14842_MI = 
{
	"MoveNext", (methodPointerType)&m14842_gshared, &t2716_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14842_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3050;
extern Il2CppGenericMethod m14843_GM;
MethodInfo m14843_MI = 
{
	"Dispose", (methodPointerType)&m14843_gshared, &t2716_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t689__CustomAttributeCache_m3050, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14843_GM};
static MethodInfo* t2716_MIs[] =
{
	&m14837_MI,
	&m14838_MI,
	&m14839_MI,
	&m14840_MI,
	&m14841_MI,
	&m14842_MI,
	&m14843_MI,
	NULL
};
static MethodInfo* t2716_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14839_MI,
	&m14842_MI,
	&m14843_MI,
	&m14840_MI,
	&m14841_MI,
	&m14838_MI,
};
static TypeInfo* t2716_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t603_TI,
	&t2183_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t2716_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t603_TI, 7},
	{ &t2183_TI, 8},
	{ &t346_TI, 9},
};
extern TypeInfo t29_TI;
extern TypeInfo t2716_TI;
static Il2CppRGCTXData t2716_RGCTXData[7] = 
{
	&t29_TI/* Class Usage */,
	&m14841_MI/* Method Usage */,
	&t2716_TI/* Class Usage */,
	&m14837_MI/* Method Usage */,
	&m26261_MI/* Method Usage */,
	&m26253_MI/* Method Usage */,
	&m14834_MI/* Method Usage */,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t2716_0_0_0;
extern Il2CppType t2716_1_0_0;
struct t2716;
extern Il2CppGenericClass t2716_GC;
extern TypeInfo t406_TI;
extern CustomAttributesCache t689__CustomAttributeCache;
extern CustomAttributesCache t689__CustomAttributeCache_m3045;
extern CustomAttributesCache t689__CustomAttributeCache_m3046;
extern CustomAttributesCache t689__CustomAttributeCache_m3047;
extern CustomAttributesCache t689__CustomAttributeCache_m3048;
extern CustomAttributesCache t689__CustomAttributeCache_m3050;
TypeInfo t2716_TI = 
{
	&g_System_Core_dll_Image, NULL, "<CreateWhereIterator>c__Iterator1D`1", "", t2716_MIs, t2716_PIs, t2716_FIs, NULL, &t29_TI, NULL, &t406_TI, &t2716_TI, t2716_ITIs, t2716_VT, &t689__CustomAttributeCache, &t2716_TI, &t2716_0_0_0, &t2716_1_0_0, t2716_IOs, &t2716_GC, NULL, NULL, NULL, t2716_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2716), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 2, 8, 0, 0, 10, 5, 5};
#include "t2717.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2717_TI;
#include "t2717MD.h"

extern MethodInfo m14848_MI;
extern MethodInfo m14844_MI;


// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>
extern Il2CppType t246_0_0_3;
FieldInfo t2717_f0_FieldInfo = 
{
	"source", &t246_0_0_3, &t2717_TI, offsetof(t2717, f0), &EmptyCustomAttributesCache};
extern Il2CppType t2705_0_0_3;
FieldInfo t2717_f1_FieldInfo = 
{
	"<$s_97>__0", &t2705_0_0_3, &t2717_TI, offsetof(t2717, f1), &EmptyCustomAttributesCache};
extern Il2CppType t39_0_0_3;
FieldInfo t2717_f2_FieldInfo = 
{
	"<element>__1", &t39_0_0_3, &t2717_TI, offsetof(t2717, f2), &EmptyCustomAttributesCache};
extern Il2CppType t245_0_0_3;
FieldInfo t2717_f3_FieldInfo = 
{
	"predicate", &t245_0_0_3, &t2717_TI, offsetof(t2717, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t2717_f4_FieldInfo = 
{
	"$PC", &t44_0_0_3, &t2717_TI, offsetof(t2717, f4), &EmptyCustomAttributesCache};
extern Il2CppType t39_0_0_3;
FieldInfo t2717_f5_FieldInfo = 
{
	"$current", &t39_0_0_3, &t2717_TI, offsetof(t2717, f5), &EmptyCustomAttributesCache};
extern Il2CppType t246_0_0_3;
FieldInfo t2717_f6_FieldInfo = 
{
	"<$>source", &t246_0_0_3, &t2717_TI, offsetof(t2717, f6), &EmptyCustomAttributesCache};
extern Il2CppType t245_0_0_3;
FieldInfo t2717_f7_FieldInfo = 
{
	"<$>predicate", &t245_0_0_3, &t2717_TI, offsetof(t2717, f7), &EmptyCustomAttributesCache};
static FieldInfo* t2717_FIs[] =
{
	&t2717_f0_FieldInfo,
	&t2717_f1_FieldInfo,
	&t2717_f2_FieldInfo,
	&t2717_f3_FieldInfo,
	&t2717_f4_FieldInfo,
	&t2717_f5_FieldInfo,
	&t2717_f6_FieldInfo,
	&t2717_f7_FieldInfo,
	NULL
};
extern MethodInfo m14845_MI;
static PropertyInfo t2717____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&t2717_TI, "System.Collections.Generic.IEnumerator<TSource>.Current", &m14845_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14846_MI;
static PropertyInfo t2717____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2717_TI, "System.Collections.IEnumerator.Current", &m14846_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2717_PIs[] =
{
	&t2717____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&t2717____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14844_GM;
MethodInfo m14844_MI = 
{
	".ctor", (methodPointerType)&m14837_gshared, &t2717_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14844_GM};
extern Il2CppType t39_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3045;
extern Il2CppGenericMethod m14845_GM;
MethodInfo m14845_MI = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current", (methodPointerType)&m14838_gshared, &t2717_TI, &t39_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3045, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14845_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3046;
extern Il2CppGenericMethod m14846_GM;
MethodInfo m14846_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14839_gshared, &t2717_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3046, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14846_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3047;
extern Il2CppGenericMethod m14847_GM;
MethodInfo m14847_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m14840_gshared, &t2717_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3047, 481, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14847_GM};
extern Il2CppType t2705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3048;
extern Il2CppGenericMethod m14848_GM;
MethodInfo m14848_MI = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator", (methodPointerType)&m14841_gshared, &t2717_TI, &t2705_0_0_0, RuntimeInvoker_t29, NULL, &t689__CustomAttributeCache_m3048, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14848_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14849_GM;
MethodInfo m14849_MI = 
{
	"MoveNext", (methodPointerType)&m14842_gshared, &t2717_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14849_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t689__CustomAttributeCache_m3050;
extern Il2CppGenericMethod m14850_GM;
MethodInfo m14850_MI = 
{
	"Dispose", (methodPointerType)&m14843_gshared, &t2717_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t689__CustomAttributeCache_m3050, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14850_GM};
static MethodInfo* t2717_MIs[] =
{
	&m14844_MI,
	&m14845_MI,
	&m14846_MI,
	&m14847_MI,
	&m14848_MI,
	&m14849_MI,
	&m14850_MI,
	NULL
};
extern MethodInfo m14849_MI;
extern MethodInfo m14850_MI;
extern MethodInfo m14847_MI;
static MethodInfo* t2717_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14846_MI,
	&m14849_MI,
	&m14850_MI,
	&m14847_MI,
	&m14848_MI,
	&m14845_MI,
};
static TypeInfo* t2717_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t603_TI,
	&t246_TI,
	&t2705_TI,
};
static Il2CppInterfaceOffsetPair t2717_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t603_TI, 7},
	{ &t246_TI, 8},
	{ &t2705_TI, 9},
};
extern TypeInfo t39_TI;
extern TypeInfo t2717_TI;
static Il2CppRGCTXData t2717_RGCTXData[7] = 
{
	&t39_TI/* Class Usage */,
	&m14848_MI/* Method Usage */,
	&t2717_TI/* Class Usage */,
	&m14844_MI/* Method Usage */,
	&m28445_MI/* Method Usage */,
	&m28437_MI/* Method Usage */,
	&m14830_MI/* Method Usage */,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t2717_0_0_0;
extern Il2CppType t2717_1_0_0;
struct t2717;
extern Il2CppGenericClass t2717_GC;
extern CustomAttributesCache t689__CustomAttributeCache;
extern CustomAttributesCache t689__CustomAttributeCache_m3045;
extern CustomAttributesCache t689__CustomAttributeCache_m3046;
extern CustomAttributesCache t689__CustomAttributeCache_m3047;
extern CustomAttributesCache t689__CustomAttributeCache_m3048;
extern CustomAttributesCache t689__CustomAttributeCache_m3050;
TypeInfo t2717_TI = 
{
	&g_System_Core_dll_Image, NULL, "<CreateWhereIterator>c__Iterator1D`1", "", t2717_MIs, t2717_PIs, t2717_FIs, NULL, &t29_TI, NULL, &t406_TI, &t2717_TI, t2717_ITIs, t2717_VT, &t689__CustomAttributeCache, &t2717_TI, &t2717_0_0_0, &t2717_1_0_0, t2717_IOs, &t2717_GC, NULL, NULL, NULL, t2717_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2717), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 2, 8, 0, 0, 10, 5, 5};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4258_TI;

#include "t248.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.AspectRatioFitter>
extern MethodInfo m28484_MI;
static PropertyInfo t4258____Current_PropertyInfo = 
{
	&t4258_TI, "Current", &m28484_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4258_PIs[] =
{
	&t4258____Current_PropertyInfo,
	NULL
};
extern Il2CppType t248_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28484_GM;
MethodInfo m28484_MI = 
{
	"get_Current", NULL, &t4258_TI, &t248_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28484_GM};
static MethodInfo* t4258_MIs[] =
{
	&m28484_MI,
	NULL
};
static TypeInfo* t4258_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4258_0_0_0;
extern Il2CppType t4258_1_0_0;
struct t4258;
extern Il2CppGenericClass t4258_GC;
TypeInfo t4258_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4258_MIs, t4258_PIs, NULL, NULL, NULL, NULL, NULL, &t4258_TI, t4258_ITIs, NULL, &EmptyCustomAttributesCache, &t4258_TI, &t4258_0_0_0, &t4258_1_0_0, NULL, &t4258_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2718.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2718_TI;
#include "t2718MD.h"

extern TypeInfo t248_TI;
extern MethodInfo m14855_MI;
extern MethodInfo m21543_MI;
struct t20;
#define m21543(__this, p0, method) (t248 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.AspectRatioFitter>
extern Il2CppType t20_0_0_1;
FieldInfo t2718_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2718_TI, offsetof(t2718, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2718_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2718_TI, offsetof(t2718, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2718_FIs[] =
{
	&t2718_f0_FieldInfo,
	&t2718_f1_FieldInfo,
	NULL
};
extern MethodInfo m14852_MI;
static PropertyInfo t2718____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2718_TI, "System.Collections.IEnumerator.Current", &m14852_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2718____Current_PropertyInfo = 
{
	&t2718_TI, "Current", &m14855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2718_PIs[] =
{
	&t2718____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2718____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2718_m14851_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14851_GM;
MethodInfo m14851_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2718_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2718_m14851_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14851_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14852_GM;
MethodInfo m14852_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2718_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14852_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14853_GM;
MethodInfo m14853_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2718_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14853_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14854_GM;
MethodInfo m14854_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2718_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14854_GM};
extern Il2CppType t248_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14855_GM;
MethodInfo m14855_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2718_TI, &t248_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14855_GM};
static MethodInfo* t2718_MIs[] =
{
	&m14851_MI,
	&m14852_MI,
	&m14853_MI,
	&m14854_MI,
	&m14855_MI,
	NULL
};
extern MethodInfo m14854_MI;
extern MethodInfo m14853_MI;
static MethodInfo* t2718_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14852_MI,
	&m14854_MI,
	&m14853_MI,
	&m14855_MI,
};
static TypeInfo* t2718_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4258_TI,
};
static Il2CppInterfaceOffsetPair t2718_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4258_TI, 7},
};
extern TypeInfo t248_TI;
static Il2CppRGCTXData t2718_RGCTXData[3] = 
{
	&m14855_MI/* Method Usage */,
	&t248_TI/* Class Usage */,
	&m21543_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2718_0_0_0;
extern Il2CppType t2718_1_0_0;
extern Il2CppGenericClass t2718_GC;
TypeInfo t2718_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2718_MIs, t2718_PIs, t2718_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2718_TI, t2718_ITIs, t2718_VT, &EmptyCustomAttributesCache, &t2718_TI, &t2718_0_0_0, &t2718_1_0_0, t2718_IOs, &t2718_GC, NULL, NULL, NULL, t2718_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2718)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5436_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.AspectRatioFitter>
extern MethodInfo m28485_MI;
static PropertyInfo t5436____Count_PropertyInfo = 
{
	&t5436_TI, "Count", &m28485_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28486_MI;
static PropertyInfo t5436____IsReadOnly_PropertyInfo = 
{
	&t5436_TI, "IsReadOnly", &m28486_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5436_PIs[] =
{
	&t5436____Count_PropertyInfo,
	&t5436____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28485_GM;
MethodInfo m28485_MI = 
{
	"get_Count", NULL, &t5436_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28485_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28486_GM;
MethodInfo m28486_MI = 
{
	"get_IsReadOnly", NULL, &t5436_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28486_GM};
extern Il2CppType t248_0_0_0;
extern Il2CppType t248_0_0_0;
static ParameterInfo t5436_m28487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28487_GM;
MethodInfo m28487_MI = 
{
	"Add", NULL, &t5436_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5436_m28487_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28487_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28488_GM;
MethodInfo m28488_MI = 
{
	"Clear", NULL, &t5436_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28488_GM};
extern Il2CppType t248_0_0_0;
static ParameterInfo t5436_m28489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28489_GM;
MethodInfo m28489_MI = 
{
	"Contains", NULL, &t5436_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5436_m28489_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28489_GM};
extern Il2CppType t3865_0_0_0;
extern Il2CppType t3865_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5436_m28490_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3865_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28490_GM;
MethodInfo m28490_MI = 
{
	"CopyTo", NULL, &t5436_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5436_m28490_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28490_GM};
extern Il2CppType t248_0_0_0;
static ParameterInfo t5436_m28491_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28491_GM;
MethodInfo m28491_MI = 
{
	"Remove", NULL, &t5436_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5436_m28491_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28491_GM};
static MethodInfo* t5436_MIs[] =
{
	&m28485_MI,
	&m28486_MI,
	&m28487_MI,
	&m28488_MI,
	&m28489_MI,
	&m28490_MI,
	&m28491_MI,
	NULL
};
extern TypeInfo t5438_TI;
static TypeInfo* t5436_ITIs[] = 
{
	&t603_TI,
	&t5438_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5436_0_0_0;
extern Il2CppType t5436_1_0_0;
struct t5436;
extern Il2CppGenericClass t5436_GC;
TypeInfo t5436_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5436_MIs, t5436_PIs, NULL, NULL, NULL, NULL, NULL, &t5436_TI, t5436_ITIs, NULL, &EmptyCustomAttributesCache, &t5436_TI, &t5436_0_0_0, &t5436_1_0_0, NULL, &t5436_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.AspectRatioFitter>
extern Il2CppType t4258_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28492_GM;
MethodInfo m28492_MI = 
{
	"GetEnumerator", NULL, &t5438_TI, &t4258_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28492_GM};
static MethodInfo* t5438_MIs[] =
{
	&m28492_MI,
	NULL
};
static TypeInfo* t5438_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5438_0_0_0;
extern Il2CppType t5438_1_0_0;
struct t5438;
extern Il2CppGenericClass t5438_GC;
TypeInfo t5438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5438_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5438_TI, t5438_ITIs, NULL, &EmptyCustomAttributesCache, &t5438_TI, &t5438_0_0_0, &t5438_1_0_0, NULL, &t5438_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5437_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.AspectRatioFitter>
extern MethodInfo m28493_MI;
extern MethodInfo m28494_MI;
static PropertyInfo t5437____Item_PropertyInfo = 
{
	&t5437_TI, "Item", &m28493_MI, &m28494_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5437_PIs[] =
{
	&t5437____Item_PropertyInfo,
	NULL
};
extern Il2CppType t248_0_0_0;
static ParameterInfo t5437_m28495_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28495_GM;
MethodInfo m28495_MI = 
{
	"IndexOf", NULL, &t5437_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5437_m28495_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28495_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t248_0_0_0;
static ParameterInfo t5437_m28496_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28496_GM;
MethodInfo m28496_MI = 
{
	"Insert", NULL, &t5437_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5437_m28496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28496_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5437_m28497_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28497_GM;
MethodInfo m28497_MI = 
{
	"RemoveAt", NULL, &t5437_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5437_m28497_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28497_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5437_m28493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t248_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28493_GM;
MethodInfo m28493_MI = 
{
	"get_Item", NULL, &t5437_TI, &t248_0_0_0, RuntimeInvoker_t29_t44, t5437_m28493_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28493_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t248_0_0_0;
static ParameterInfo t5437_m28494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28494_GM;
MethodInfo m28494_MI = 
{
	"set_Item", NULL, &t5437_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5437_m28494_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28494_GM};
static MethodInfo* t5437_MIs[] =
{
	&m28495_MI,
	&m28496_MI,
	&m28497_MI,
	&m28493_MI,
	&m28494_MI,
	NULL
};
static TypeInfo* t5437_ITIs[] = 
{
	&t603_TI,
	&t5436_TI,
	&t5438_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5437_0_0_0;
extern Il2CppType t5437_1_0_0;
struct t5437;
extern Il2CppGenericClass t5437_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5437_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5437_MIs, t5437_PIs, NULL, NULL, NULL, NULL, NULL, &t5437_TI, t5437_ITIs, NULL, &t1908__CustomAttributeCache, &t5437_TI, &t5437_0_0_0, &t5437_1_0_0, NULL, &t5437_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5439_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ILayoutController>
extern MethodInfo m28498_MI;
static PropertyInfo t5439____Count_PropertyInfo = 
{
	&t5439_TI, "Count", &m28498_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28499_MI;
static PropertyInfo t5439____IsReadOnly_PropertyInfo = 
{
	&t5439_TI, "IsReadOnly", &m28499_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5439_PIs[] =
{
	&t5439____Count_PropertyInfo,
	&t5439____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28498_GM;
MethodInfo m28498_MI = 
{
	"get_Count", NULL, &t5439_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28498_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28499_GM;
MethodInfo m28499_MI = 
{
	"get_IsReadOnly", NULL, &t5439_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28499_GM};
extern Il2CppType t409_0_0_0;
extern Il2CppType t409_0_0_0;
static ParameterInfo t5439_m28500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28500_GM;
MethodInfo m28500_MI = 
{
	"Add", NULL, &t5439_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5439_m28500_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28500_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28501_GM;
MethodInfo m28501_MI = 
{
	"Clear", NULL, &t5439_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28501_GM};
extern Il2CppType t409_0_0_0;
static ParameterInfo t5439_m28502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t409_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28502_GM;
MethodInfo m28502_MI = 
{
	"Contains", NULL, &t5439_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5439_m28502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28502_GM};
extern Il2CppType t3866_0_0_0;
extern Il2CppType t3866_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5439_m28503_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3866_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28503_GM;
MethodInfo m28503_MI = 
{
	"CopyTo", NULL, &t5439_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5439_m28503_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28503_GM};
extern Il2CppType t409_0_0_0;
static ParameterInfo t5439_m28504_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t409_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28504_GM;
MethodInfo m28504_MI = 
{
	"Remove", NULL, &t5439_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5439_m28504_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28504_GM};
static MethodInfo* t5439_MIs[] =
{
	&m28498_MI,
	&m28499_MI,
	&m28500_MI,
	&m28501_MI,
	&m28502_MI,
	&m28503_MI,
	&m28504_MI,
	NULL
};
extern TypeInfo t5441_TI;
static TypeInfo* t5439_ITIs[] = 
{
	&t603_TI,
	&t5441_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5439_0_0_0;
extern Il2CppType t5439_1_0_0;
struct t5439;
extern Il2CppGenericClass t5439_GC;
TypeInfo t5439_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5439_MIs, t5439_PIs, NULL, NULL, NULL, NULL, NULL, &t5439_TI, t5439_ITIs, NULL, &EmptyCustomAttributesCache, &t5439_TI, &t5439_0_0_0, &t5439_1_0_0, NULL, &t5439_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ILayoutController>
extern Il2CppType t4260_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28505_GM;
MethodInfo m28505_MI = 
{
	"GetEnumerator", NULL, &t5441_TI, &t4260_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28505_GM};
static MethodInfo* t5441_MIs[] =
{
	&m28505_MI,
	NULL
};
static TypeInfo* t5441_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5441_0_0_0;
extern Il2CppType t5441_1_0_0;
struct t5441;
extern Il2CppGenericClass t5441_GC;
TypeInfo t5441_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5441_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5441_TI, t5441_ITIs, NULL, &EmptyCustomAttributesCache, &t5441_TI, &t5441_0_0_0, &t5441_1_0_0, NULL, &t5441_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4260_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutController>
extern MethodInfo m28506_MI;
static PropertyInfo t4260____Current_PropertyInfo = 
{
	&t4260_TI, "Current", &m28506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4260_PIs[] =
{
	&t4260____Current_PropertyInfo,
	NULL
};
extern Il2CppType t409_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28506_GM;
MethodInfo m28506_MI = 
{
	"get_Current", NULL, &t4260_TI, &t409_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28506_GM};
static MethodInfo* t4260_MIs[] =
{
	&m28506_MI,
	NULL
};
static TypeInfo* t4260_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4260_0_0_0;
extern Il2CppType t4260_1_0_0;
struct t4260;
extern Il2CppGenericClass t4260_GC;
TypeInfo t4260_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4260_MIs, t4260_PIs, NULL, NULL, NULL, NULL, NULL, &t4260_TI, t4260_ITIs, NULL, &EmptyCustomAttributesCache, &t4260_TI, &t4260_0_0_0, &t4260_1_0_0, NULL, &t4260_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2719.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2719_TI;
#include "t2719MD.h"

extern TypeInfo t409_TI;
extern MethodInfo m14860_MI;
extern MethodInfo m21554_MI;
struct t20;
#define m21554(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutController>
extern Il2CppType t20_0_0_1;
FieldInfo t2719_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2719_TI, offsetof(t2719, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2719_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2719_TI, offsetof(t2719, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2719_FIs[] =
{
	&t2719_f0_FieldInfo,
	&t2719_f1_FieldInfo,
	NULL
};
extern MethodInfo m14857_MI;
static PropertyInfo t2719____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2719_TI, "System.Collections.IEnumerator.Current", &m14857_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2719____Current_PropertyInfo = 
{
	&t2719_TI, "Current", &m14860_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2719_PIs[] =
{
	&t2719____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2719____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2719_m14856_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14856_GM;
MethodInfo m14856_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2719_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2719_m14856_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14856_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14857_GM;
MethodInfo m14857_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2719_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14857_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14858_GM;
MethodInfo m14858_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2719_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14858_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14859_GM;
MethodInfo m14859_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2719_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14859_GM};
extern Il2CppType t409_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14860_GM;
MethodInfo m14860_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2719_TI, &t409_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14860_GM};
static MethodInfo* t2719_MIs[] =
{
	&m14856_MI,
	&m14857_MI,
	&m14858_MI,
	&m14859_MI,
	&m14860_MI,
	NULL
};
extern MethodInfo m14859_MI;
extern MethodInfo m14858_MI;
static MethodInfo* t2719_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14857_MI,
	&m14859_MI,
	&m14858_MI,
	&m14860_MI,
};
static TypeInfo* t2719_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4260_TI,
};
static Il2CppInterfaceOffsetPair t2719_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4260_TI, 7},
};
extern TypeInfo t409_TI;
static Il2CppRGCTXData t2719_RGCTXData[3] = 
{
	&m14860_MI/* Method Usage */,
	&t409_TI/* Class Usage */,
	&m21554_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2719_0_0_0;
extern Il2CppType t2719_1_0_0;
extern Il2CppGenericClass t2719_GC;
TypeInfo t2719_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2719_MIs, t2719_PIs, t2719_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2719_TI, t2719_ITIs, t2719_VT, &EmptyCustomAttributesCache, &t2719_TI, &t2719_0_0_0, &t2719_1_0_0, t2719_IOs, &t2719_GC, NULL, NULL, NULL, t2719_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2719)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5440_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutController>
extern MethodInfo m28507_MI;
extern MethodInfo m28508_MI;
static PropertyInfo t5440____Item_PropertyInfo = 
{
	&t5440_TI, "Item", &m28507_MI, &m28508_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5440_PIs[] =
{
	&t5440____Item_PropertyInfo,
	NULL
};
extern Il2CppType t409_0_0_0;
static ParameterInfo t5440_m28509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t409_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28509_GM;
MethodInfo m28509_MI = 
{
	"IndexOf", NULL, &t5440_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5440_m28509_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28509_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t409_0_0_0;
static ParameterInfo t5440_m28510_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28510_GM;
MethodInfo m28510_MI = 
{
	"Insert", NULL, &t5440_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5440_m28510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28510_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5440_m28511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28511_GM;
MethodInfo m28511_MI = 
{
	"RemoveAt", NULL, &t5440_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5440_m28511_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28511_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5440_m28507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t409_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28507_GM;
MethodInfo m28507_MI = 
{
	"get_Item", NULL, &t5440_TI, &t409_0_0_0, RuntimeInvoker_t29_t44, t5440_m28507_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28507_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t409_0_0_0;
static ParameterInfo t5440_m28508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28508_GM;
MethodInfo m28508_MI = 
{
	"set_Item", NULL, &t5440_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5440_m28508_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28508_GM};
static MethodInfo* t5440_MIs[] =
{
	&m28509_MI,
	&m28510_MI,
	&m28511_MI,
	&m28507_MI,
	&m28508_MI,
	NULL
};
static TypeInfo* t5440_ITIs[] = 
{
	&t603_TI,
	&t5439_TI,
	&t5441_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5440_0_0_0;
extern Il2CppType t5440_1_0_0;
struct t5440;
extern Il2CppGenericClass t5440_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5440_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5440_MIs, t5440_PIs, NULL, NULL, NULL, NULL, NULL, &t5440_TI, t5440_ITIs, NULL, &t1908__CustomAttributeCache, &t5440_TI, &t5440_0_0_0, &t5440_1_0_0, NULL, &t5440_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5442_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ILayoutSelfController>
extern MethodInfo m28512_MI;
static PropertyInfo t5442____Count_PropertyInfo = 
{
	&t5442_TI, "Count", &m28512_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28513_MI;
static PropertyInfo t5442____IsReadOnly_PropertyInfo = 
{
	&t5442_TI, "IsReadOnly", &m28513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5442_PIs[] =
{
	&t5442____Count_PropertyInfo,
	&t5442____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28512_GM;
MethodInfo m28512_MI = 
{
	"get_Count", NULL, &t5442_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28512_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28513_GM;
MethodInfo m28513_MI = 
{
	"get_IsReadOnly", NULL, &t5442_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28513_GM};
extern Il2CppType t410_0_0_0;
extern Il2CppType t410_0_0_0;
static ParameterInfo t5442_m28514_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t410_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28514_GM;
MethodInfo m28514_MI = 
{
	"Add", NULL, &t5442_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5442_m28514_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28514_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28515_GM;
MethodInfo m28515_MI = 
{
	"Clear", NULL, &t5442_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28515_GM};
extern Il2CppType t410_0_0_0;
static ParameterInfo t5442_m28516_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t410_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28516_GM;
MethodInfo m28516_MI = 
{
	"Contains", NULL, &t5442_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5442_m28516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28516_GM};
extern Il2CppType t3867_0_0_0;
extern Il2CppType t3867_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5442_m28517_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3867_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28517_GM;
MethodInfo m28517_MI = 
{
	"CopyTo", NULL, &t5442_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5442_m28517_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28517_GM};
extern Il2CppType t410_0_0_0;
static ParameterInfo t5442_m28518_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t410_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28518_GM;
MethodInfo m28518_MI = 
{
	"Remove", NULL, &t5442_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5442_m28518_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28518_GM};
static MethodInfo* t5442_MIs[] =
{
	&m28512_MI,
	&m28513_MI,
	&m28514_MI,
	&m28515_MI,
	&m28516_MI,
	&m28517_MI,
	&m28518_MI,
	NULL
};
extern TypeInfo t5444_TI;
static TypeInfo* t5442_ITIs[] = 
{
	&t603_TI,
	&t5444_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5442_0_0_0;
extern Il2CppType t5442_1_0_0;
struct t5442;
extern Il2CppGenericClass t5442_GC;
TypeInfo t5442_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5442_MIs, t5442_PIs, NULL, NULL, NULL, NULL, NULL, &t5442_TI, t5442_ITIs, NULL, &EmptyCustomAttributesCache, &t5442_TI, &t5442_0_0_0, &t5442_1_0_0, NULL, &t5442_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ILayoutSelfController>
extern Il2CppType t4262_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28519_GM;
MethodInfo m28519_MI = 
{
	"GetEnumerator", NULL, &t5444_TI, &t4262_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28519_GM};
static MethodInfo* t5444_MIs[] =
{
	&m28519_MI,
	NULL
};
static TypeInfo* t5444_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5444_0_0_0;
extern Il2CppType t5444_1_0_0;
struct t5444;
extern Il2CppGenericClass t5444_GC;
TypeInfo t5444_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5444_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5444_TI, t5444_ITIs, NULL, &EmptyCustomAttributesCache, &t5444_TI, &t5444_0_0_0, &t5444_1_0_0, NULL, &t5444_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4262_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutSelfController>
extern MethodInfo m28520_MI;
static PropertyInfo t4262____Current_PropertyInfo = 
{
	&t4262_TI, "Current", &m28520_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4262_PIs[] =
{
	&t4262____Current_PropertyInfo,
	NULL
};
extern Il2CppType t410_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28520_GM;
MethodInfo m28520_MI = 
{
	"get_Current", NULL, &t4262_TI, &t410_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28520_GM};
static MethodInfo* t4262_MIs[] =
{
	&m28520_MI,
	NULL
};
static TypeInfo* t4262_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4262_0_0_0;
extern Il2CppType t4262_1_0_0;
struct t4262;
extern Il2CppGenericClass t4262_GC;
TypeInfo t4262_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4262_MIs, t4262_PIs, NULL, NULL, NULL, NULL, NULL, &t4262_TI, t4262_ITIs, NULL, &EmptyCustomAttributesCache, &t4262_TI, &t4262_0_0_0, &t4262_1_0_0, NULL, &t4262_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2720.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2720_TI;
#include "t2720MD.h"

extern TypeInfo t410_TI;
extern MethodInfo m14865_MI;
extern MethodInfo m21565_MI;
struct t20;
#define m21565(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutSelfController>
extern Il2CppType t20_0_0_1;
FieldInfo t2720_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2720_TI, offsetof(t2720, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2720_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2720_TI, offsetof(t2720, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2720_FIs[] =
{
	&t2720_f0_FieldInfo,
	&t2720_f1_FieldInfo,
	NULL
};
extern MethodInfo m14862_MI;
static PropertyInfo t2720____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2720_TI, "System.Collections.IEnumerator.Current", &m14862_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2720____Current_PropertyInfo = 
{
	&t2720_TI, "Current", &m14865_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2720_PIs[] =
{
	&t2720____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2720____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2720_m14861_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14861_GM;
MethodInfo m14861_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2720_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2720_m14861_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14861_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14862_GM;
MethodInfo m14862_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2720_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14862_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14863_GM;
MethodInfo m14863_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2720_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14863_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14864_GM;
MethodInfo m14864_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2720_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14864_GM};
extern Il2CppType t410_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14865_GM;
MethodInfo m14865_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2720_TI, &t410_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14865_GM};
static MethodInfo* t2720_MIs[] =
{
	&m14861_MI,
	&m14862_MI,
	&m14863_MI,
	&m14864_MI,
	&m14865_MI,
	NULL
};
extern MethodInfo m14864_MI;
extern MethodInfo m14863_MI;
static MethodInfo* t2720_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14862_MI,
	&m14864_MI,
	&m14863_MI,
	&m14865_MI,
};
static TypeInfo* t2720_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4262_TI,
};
static Il2CppInterfaceOffsetPair t2720_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4262_TI, 7},
};
extern TypeInfo t410_TI;
static Il2CppRGCTXData t2720_RGCTXData[3] = 
{
	&m14865_MI/* Method Usage */,
	&t410_TI/* Class Usage */,
	&m21565_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2720_0_0_0;
extern Il2CppType t2720_1_0_0;
extern Il2CppGenericClass t2720_GC;
TypeInfo t2720_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2720_MIs, t2720_PIs, t2720_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2720_TI, t2720_ITIs, t2720_VT, &EmptyCustomAttributesCache, &t2720_TI, &t2720_0_0_0, &t2720_1_0_0, t2720_IOs, &t2720_GC, NULL, NULL, NULL, t2720_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2720)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5443_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutSelfController>
extern MethodInfo m28521_MI;
extern MethodInfo m28522_MI;
static PropertyInfo t5443____Item_PropertyInfo = 
{
	&t5443_TI, "Item", &m28521_MI, &m28522_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5443_PIs[] =
{
	&t5443____Item_PropertyInfo,
	NULL
};
extern Il2CppType t410_0_0_0;
static ParameterInfo t5443_m28523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t410_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28523_GM;
MethodInfo m28523_MI = 
{
	"IndexOf", NULL, &t5443_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5443_m28523_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28523_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t410_0_0_0;
static ParameterInfo t5443_m28524_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t410_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28524_GM;
MethodInfo m28524_MI = 
{
	"Insert", NULL, &t5443_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5443_m28524_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28524_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5443_m28525_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28525_GM;
MethodInfo m28525_MI = 
{
	"RemoveAt", NULL, &t5443_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5443_m28525_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28525_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5443_m28521_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t410_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28521_GM;
MethodInfo m28521_MI = 
{
	"get_Item", NULL, &t5443_TI, &t410_0_0_0, RuntimeInvoker_t29_t44, t5443_m28521_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28521_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t410_0_0_0;
static ParameterInfo t5443_m28522_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t410_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28522_GM;
MethodInfo m28522_MI = 
{
	"set_Item", NULL, &t5443_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5443_m28522_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28522_GM};
static MethodInfo* t5443_MIs[] =
{
	&m28523_MI,
	&m28524_MI,
	&m28525_MI,
	&m28521_MI,
	&m28522_MI,
	NULL
};
static TypeInfo* t5443_ITIs[] = 
{
	&t603_TI,
	&t5442_TI,
	&t5444_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5443_0_0_0;
extern Il2CppType t5443_1_0_0;
struct t5443;
extern Il2CppGenericClass t5443_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5443_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5443_MIs, t5443_PIs, NULL, NULL, NULL, NULL, NULL, &t5443_TI, t5443_ITIs, NULL, &t1908__CustomAttributeCache, &t5443_TI, &t5443_0_0_0, &t5443_1_0_0, NULL, &t5443_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2721.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2721_TI;
#include "t2721MD.h"

#include "t2722.h"
extern TypeInfo t2722_TI;
#include "t2722MD.h"
extern MethodInfo m14868_MI;
extern MethodInfo m14870_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.AspectRatioFitter>
extern Il2CppType t316_0_0_33;
FieldInfo t2721_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2721_TI, offsetof(t2721, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2721_FIs[] =
{
	&t2721_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t248_0_0_0;
static ParameterInfo t2721_m14866_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14866_GM;
MethodInfo m14866_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2721_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2721_m14866_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14866_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2721_m14867_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14867_GM;
MethodInfo m14867_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2721_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2721_m14867_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14867_GM};
static MethodInfo* t2721_MIs[] =
{
	&m14866_MI,
	&m14867_MI,
	NULL
};
extern MethodInfo m14867_MI;
extern MethodInfo m14871_MI;
static MethodInfo* t2721_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14867_MI,
	&m14871_MI,
};
extern Il2CppType t2723_0_0_0;
extern TypeInfo t2723_TI;
extern MethodInfo m21575_MI;
extern TypeInfo t248_TI;
extern MethodInfo m14873_MI;
extern TypeInfo t248_TI;
static Il2CppRGCTXData t2721_RGCTXData[8] = 
{
	&t2723_0_0_0/* Type Usage */,
	&t2723_TI/* Class Usage */,
	&m21575_MI/* Method Usage */,
	&t248_TI/* Class Usage */,
	&m14873_MI/* Method Usage */,
	&m14868_MI/* Method Usage */,
	&t248_TI/* Class Usage */,
	&m14870_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2721_0_0_0;
extern Il2CppType t2721_1_0_0;
struct t2721;
extern Il2CppGenericClass t2721_GC;
TypeInfo t2721_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2721_MIs, NULL, t2721_FIs, NULL, &t2722_TI, NULL, NULL, &t2721_TI, NULL, t2721_VT, &EmptyCustomAttributesCache, &t2721_TI, &t2721_0_0_0, &t2721_1_0_0, NULL, &t2721_GC, NULL, NULL, NULL, t2721_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2721), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2723.h"
extern TypeInfo t2723_TI;
#include "t2723MD.h"
struct t556;
#define m21575(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>
extern Il2CppType t2723_0_0_1;
FieldInfo t2722_f0_FieldInfo = 
{
	"Delegate", &t2723_0_0_1, &t2722_TI, offsetof(t2722, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2722_FIs[] =
{
	&t2722_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2722_m14868_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14868_GM;
MethodInfo m14868_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2722_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2722_m14868_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14868_GM};
extern Il2CppType t2723_0_0_0;
static ParameterInfo t2722_m14869_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2723_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14869_GM;
MethodInfo m14869_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2722_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2722_m14869_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14869_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2722_m14870_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14870_GM;
MethodInfo m14870_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2722_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2722_m14870_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14870_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2722_m14871_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14871_GM;
MethodInfo m14871_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2722_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2722_m14871_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14871_GM};
static MethodInfo* t2722_MIs[] =
{
	&m14868_MI,
	&m14869_MI,
	&m14870_MI,
	&m14871_MI,
	NULL
};
static MethodInfo* t2722_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14870_MI,
	&m14871_MI,
};
extern TypeInfo t2723_TI;
extern TypeInfo t248_TI;
static Il2CppRGCTXData t2722_RGCTXData[5] = 
{
	&t2723_0_0_0/* Type Usage */,
	&t2723_TI/* Class Usage */,
	&m21575_MI/* Method Usage */,
	&t248_TI/* Class Usage */,
	&m14873_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2722_0_0_0;
extern Il2CppType t2722_1_0_0;
struct t2722;
extern Il2CppGenericClass t2722_GC;
TypeInfo t2722_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2722_MIs, NULL, t2722_FIs, NULL, &t556_TI, NULL, NULL, &t2722_TI, NULL, t2722_VT, &EmptyCustomAttributesCache, &t2722_TI, &t2722_0_0_0, &t2722_1_0_0, NULL, &t2722_GC, NULL, NULL, NULL, t2722_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2722), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.AspectRatioFitter>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2723_m14872_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14872_GM;
MethodInfo m14872_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2723_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2723_m14872_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14872_GM};
extern Il2CppType t248_0_0_0;
static ParameterInfo t2723_m14873_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14873_GM;
MethodInfo m14873_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2723_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2723_m14873_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14873_GM};
extern Il2CppType t248_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2723_m14874_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t248_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14874_GM;
MethodInfo m14874_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2723_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2723_m14874_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14874_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2723_m14875_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14875_GM;
MethodInfo m14875_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2723_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2723_m14875_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14875_GM};
static MethodInfo* t2723_MIs[] =
{
	&m14872_MI,
	&m14873_MI,
	&m14874_MI,
	&m14875_MI,
	NULL
};
extern MethodInfo m14874_MI;
extern MethodInfo m14875_MI;
static MethodInfo* t2723_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14873_MI,
	&m14874_MI,
	&m14875_MI,
};
static Il2CppInterfaceOffsetPair t2723_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2723_1_0_0;
struct t2723;
extern Il2CppGenericClass t2723_GC;
TypeInfo t2723_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2723_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2723_TI, NULL, t2723_VT, &EmptyCustomAttributesCache, &t2723_TI, &t2723_0_0_0, &t2723_1_0_0, t2723_IOs, &t2723_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2723), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4264_TI;

#include "t247.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
extern MethodInfo m28526_MI;
static PropertyInfo t4264____Current_PropertyInfo = 
{
	&t4264_TI, "Current", &m28526_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4264_PIs[] =
{
	&t4264____Current_PropertyInfo,
	NULL
};
extern Il2CppType t247_0_0_0;
extern void* RuntimeInvoker_t247 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28526_GM;
MethodInfo m28526_MI = 
{
	"get_Current", NULL, &t4264_TI, &t247_0_0_0, RuntimeInvoker_t247, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28526_GM};
static MethodInfo* t4264_MIs[] =
{
	&m28526_MI,
	NULL
};
static TypeInfo* t4264_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4264_0_0_0;
extern Il2CppType t4264_1_0_0;
struct t4264;
extern Il2CppGenericClass t4264_GC;
TypeInfo t4264_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4264_MIs, t4264_PIs, NULL, NULL, NULL, NULL, NULL, &t4264_TI, t4264_ITIs, NULL, &EmptyCustomAttributesCache, &t4264_TI, &t4264_0_0_0, &t4264_1_0_0, NULL, &t4264_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2724.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2724_TI;
#include "t2724MD.h"

extern TypeInfo t247_TI;
extern MethodInfo m14880_MI;
extern MethodInfo m21577_MI;
struct t20;
 int32_t m21577 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14876_MI;
 void m14876 (t2724 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14877_MI;
 t29 * m14877 (t2724 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14880(__this, &m14880_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t247_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14878_MI;
 void m14878 (t2724 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14879_MI;
 bool m14879 (t2724 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14880 (t2724 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21577(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21577_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2724_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2724_TI, offsetof(t2724, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2724_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2724_TI, offsetof(t2724, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2724_FIs[] =
{
	&t2724_f0_FieldInfo,
	&t2724_f1_FieldInfo,
	NULL
};
static PropertyInfo t2724____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2724_TI, "System.Collections.IEnumerator.Current", &m14877_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2724____Current_PropertyInfo = 
{
	&t2724_TI, "Current", &m14880_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2724_PIs[] =
{
	&t2724____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2724____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2724_m14876_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14876_GM;
MethodInfo m14876_MI = 
{
	".ctor", (methodPointerType)&m14876, &t2724_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2724_m14876_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14876_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14877_GM;
MethodInfo m14877_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14877, &t2724_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14877_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14878_GM;
MethodInfo m14878_MI = 
{
	"Dispose", (methodPointerType)&m14878, &t2724_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14878_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14879_GM;
MethodInfo m14879_MI = 
{
	"MoveNext", (methodPointerType)&m14879, &t2724_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14879_GM};
extern Il2CppType t247_0_0_0;
extern void* RuntimeInvoker_t247 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14880_GM;
MethodInfo m14880_MI = 
{
	"get_Current", (methodPointerType)&m14880, &t2724_TI, &t247_0_0_0, RuntimeInvoker_t247, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14880_GM};
static MethodInfo* t2724_MIs[] =
{
	&m14876_MI,
	&m14877_MI,
	&m14878_MI,
	&m14879_MI,
	&m14880_MI,
	NULL
};
static MethodInfo* t2724_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14877_MI,
	&m14879_MI,
	&m14878_MI,
	&m14880_MI,
};
static TypeInfo* t2724_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4264_TI,
};
static Il2CppInterfaceOffsetPair t2724_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4264_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2724_0_0_0;
extern Il2CppType t2724_1_0_0;
extern Il2CppGenericClass t2724_GC;
TypeInfo t2724_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2724_MIs, t2724_PIs, t2724_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2724_TI, t2724_ITIs, t2724_VT, &EmptyCustomAttributesCache, &t2724_TI, &t2724_0_0_0, &t2724_1_0_0, t2724_IOs, &t2724_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2724)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5445_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
extern MethodInfo m28527_MI;
static PropertyInfo t5445____Count_PropertyInfo = 
{
	&t5445_TI, "Count", &m28527_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28528_MI;
static PropertyInfo t5445____IsReadOnly_PropertyInfo = 
{
	&t5445_TI, "IsReadOnly", &m28528_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5445_PIs[] =
{
	&t5445____Count_PropertyInfo,
	&t5445____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28527_GM;
MethodInfo m28527_MI = 
{
	"get_Count", NULL, &t5445_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28527_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28528_GM;
MethodInfo m28528_MI = 
{
	"get_IsReadOnly", NULL, &t5445_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28528_GM};
extern Il2CppType t247_0_0_0;
extern Il2CppType t247_0_0_0;
static ParameterInfo t5445_m28529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28529_GM;
MethodInfo m28529_MI = 
{
	"Add", NULL, &t5445_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5445_m28529_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28529_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28530_GM;
MethodInfo m28530_MI = 
{
	"Clear", NULL, &t5445_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28530_GM};
extern Il2CppType t247_0_0_0;
static ParameterInfo t5445_m28531_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28531_GM;
MethodInfo m28531_MI = 
{
	"Contains", NULL, &t5445_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5445_m28531_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28531_GM};
extern Il2CppType t3868_0_0_0;
extern Il2CppType t3868_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5445_m28532_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3868_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28532_GM;
MethodInfo m28532_MI = 
{
	"CopyTo", NULL, &t5445_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5445_m28532_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28532_GM};
extern Il2CppType t247_0_0_0;
static ParameterInfo t5445_m28533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28533_GM;
MethodInfo m28533_MI = 
{
	"Remove", NULL, &t5445_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5445_m28533_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28533_GM};
static MethodInfo* t5445_MIs[] =
{
	&m28527_MI,
	&m28528_MI,
	&m28529_MI,
	&m28530_MI,
	&m28531_MI,
	&m28532_MI,
	&m28533_MI,
	NULL
};
extern TypeInfo t5447_TI;
static TypeInfo* t5445_ITIs[] = 
{
	&t603_TI,
	&t5447_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5445_0_0_0;
extern Il2CppType t5445_1_0_0;
struct t5445;
extern Il2CppGenericClass t5445_GC;
TypeInfo t5445_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5445_MIs, t5445_PIs, NULL, NULL, NULL, NULL, NULL, &t5445_TI, t5445_ITIs, NULL, &EmptyCustomAttributesCache, &t5445_TI, &t5445_0_0_0, &t5445_1_0_0, NULL, &t5445_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
extern Il2CppType t4264_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28534_GM;
MethodInfo m28534_MI = 
{
	"GetEnumerator", NULL, &t5447_TI, &t4264_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28534_GM};
static MethodInfo* t5447_MIs[] =
{
	&m28534_MI,
	NULL
};
static TypeInfo* t5447_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5447_0_0_0;
extern Il2CppType t5447_1_0_0;
struct t5447;
extern Il2CppGenericClass t5447_GC;
TypeInfo t5447_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5447_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5447_TI, t5447_ITIs, NULL, &EmptyCustomAttributesCache, &t5447_TI, &t5447_0_0_0, &t5447_1_0_0, NULL, &t5447_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5446_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
extern MethodInfo m28535_MI;
extern MethodInfo m28536_MI;
static PropertyInfo t5446____Item_PropertyInfo = 
{
	&t5446_TI, "Item", &m28535_MI, &m28536_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5446_PIs[] =
{
	&t5446____Item_PropertyInfo,
	NULL
};
extern Il2CppType t247_0_0_0;
static ParameterInfo t5446_m28537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28537_GM;
MethodInfo m28537_MI = 
{
	"IndexOf", NULL, &t5446_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5446_m28537_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28537_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t247_0_0_0;
static ParameterInfo t5446_m28538_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28538_GM;
MethodInfo m28538_MI = 
{
	"Insert", NULL, &t5446_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5446_m28538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28538_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5446_m28539_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28539_GM;
MethodInfo m28539_MI = 
{
	"RemoveAt", NULL, &t5446_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5446_m28539_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28539_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5446_m28535_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t247_0_0_0;
extern void* RuntimeInvoker_t247_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28535_GM;
MethodInfo m28535_MI = 
{
	"get_Item", NULL, &t5446_TI, &t247_0_0_0, RuntimeInvoker_t247_t44, t5446_m28535_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28535_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t247_0_0_0;
static ParameterInfo t5446_m28536_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28536_GM;
MethodInfo m28536_MI = 
{
	"set_Item", NULL, &t5446_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5446_m28536_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28536_GM};
static MethodInfo* t5446_MIs[] =
{
	&m28537_MI,
	&m28538_MI,
	&m28539_MI,
	&m28535_MI,
	&m28536_MI,
	NULL
};
static TypeInfo* t5446_ITIs[] = 
{
	&t603_TI,
	&t5445_TI,
	&t5447_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5446_0_0_0;
extern Il2CppType t5446_1_0_0;
struct t5446;
extern Il2CppGenericClass t5446_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5446_MIs, t5446_PIs, NULL, NULL, NULL, NULL, NULL, &t5446_TI, t5446_ITIs, NULL, &t1908__CustomAttributeCache, &t5446_TI, &t5446_0_0_0, &t5446_1_0_0, NULL, &t5446_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4266_TI;

#include "t252.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.CanvasScaler>
extern MethodInfo m28540_MI;
static PropertyInfo t4266____Current_PropertyInfo = 
{
	&t4266_TI, "Current", &m28540_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4266_PIs[] =
{
	&t4266____Current_PropertyInfo,
	NULL
};
extern Il2CppType t252_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28540_GM;
MethodInfo m28540_MI = 
{
	"get_Current", NULL, &t4266_TI, &t252_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28540_GM};
static MethodInfo* t4266_MIs[] =
{
	&m28540_MI,
	NULL
};
static TypeInfo* t4266_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4266_0_0_0;
extern Il2CppType t4266_1_0_0;
struct t4266;
extern Il2CppGenericClass t4266_GC;
TypeInfo t4266_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4266_MIs, t4266_PIs, NULL, NULL, NULL, NULL, NULL, &t4266_TI, t4266_ITIs, NULL, &EmptyCustomAttributesCache, &t4266_TI, &t4266_0_0_0, &t4266_1_0_0, NULL, &t4266_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2725.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2725_TI;
#include "t2725MD.h"

extern TypeInfo t252_TI;
extern MethodInfo m14885_MI;
extern MethodInfo m21588_MI;
struct t20;
#define m21588(__this, p0, method) (t252 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler>
extern Il2CppType t20_0_0_1;
FieldInfo t2725_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2725_TI, offsetof(t2725, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2725_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2725_TI, offsetof(t2725, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2725_FIs[] =
{
	&t2725_f0_FieldInfo,
	&t2725_f1_FieldInfo,
	NULL
};
extern MethodInfo m14882_MI;
static PropertyInfo t2725____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2725_TI, "System.Collections.IEnumerator.Current", &m14882_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2725____Current_PropertyInfo = 
{
	&t2725_TI, "Current", &m14885_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2725_PIs[] =
{
	&t2725____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2725____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2725_m14881_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14881_GM;
MethodInfo m14881_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2725_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2725_m14881_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14881_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14882_GM;
MethodInfo m14882_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2725_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14882_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14883_GM;
MethodInfo m14883_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2725_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14883_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14884_GM;
MethodInfo m14884_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2725_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14884_GM};
extern Il2CppType t252_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14885_GM;
MethodInfo m14885_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2725_TI, &t252_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14885_GM};
static MethodInfo* t2725_MIs[] =
{
	&m14881_MI,
	&m14882_MI,
	&m14883_MI,
	&m14884_MI,
	&m14885_MI,
	NULL
};
extern MethodInfo m14884_MI;
extern MethodInfo m14883_MI;
static MethodInfo* t2725_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14882_MI,
	&m14884_MI,
	&m14883_MI,
	&m14885_MI,
};
static TypeInfo* t2725_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4266_TI,
};
static Il2CppInterfaceOffsetPair t2725_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4266_TI, 7},
};
extern TypeInfo t252_TI;
static Il2CppRGCTXData t2725_RGCTXData[3] = 
{
	&m14885_MI/* Method Usage */,
	&t252_TI/* Class Usage */,
	&m21588_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2725_0_0_0;
extern Il2CppType t2725_1_0_0;
extern Il2CppGenericClass t2725_GC;
TypeInfo t2725_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2725_MIs, t2725_PIs, t2725_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2725_TI, t2725_ITIs, t2725_VT, &EmptyCustomAttributesCache, &t2725_TI, &t2725_0_0_0, &t2725_1_0_0, t2725_IOs, &t2725_GC, NULL, NULL, NULL, t2725_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2725)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5448_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.CanvasScaler>
extern MethodInfo m28541_MI;
static PropertyInfo t5448____Count_PropertyInfo = 
{
	&t5448_TI, "Count", &m28541_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28542_MI;
static PropertyInfo t5448____IsReadOnly_PropertyInfo = 
{
	&t5448_TI, "IsReadOnly", &m28542_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5448_PIs[] =
{
	&t5448____Count_PropertyInfo,
	&t5448____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28541_GM;
MethodInfo m28541_MI = 
{
	"get_Count", NULL, &t5448_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28541_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28542_GM;
MethodInfo m28542_MI = 
{
	"get_IsReadOnly", NULL, &t5448_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28542_GM};
extern Il2CppType t252_0_0_0;
extern Il2CppType t252_0_0_0;
static ParameterInfo t5448_m28543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28543_GM;
MethodInfo m28543_MI = 
{
	"Add", NULL, &t5448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5448_m28543_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28543_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28544_GM;
MethodInfo m28544_MI = 
{
	"Clear", NULL, &t5448_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28544_GM};
extern Il2CppType t252_0_0_0;
static ParameterInfo t5448_m28545_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28545_GM;
MethodInfo m28545_MI = 
{
	"Contains", NULL, &t5448_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5448_m28545_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28545_GM};
extern Il2CppType t3869_0_0_0;
extern Il2CppType t3869_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5448_m28546_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3869_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28546_GM;
MethodInfo m28546_MI = 
{
	"CopyTo", NULL, &t5448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5448_m28546_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28546_GM};
extern Il2CppType t252_0_0_0;
static ParameterInfo t5448_m28547_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28547_GM;
MethodInfo m28547_MI = 
{
	"Remove", NULL, &t5448_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5448_m28547_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28547_GM};
static MethodInfo* t5448_MIs[] =
{
	&m28541_MI,
	&m28542_MI,
	&m28543_MI,
	&m28544_MI,
	&m28545_MI,
	&m28546_MI,
	&m28547_MI,
	NULL
};
extern TypeInfo t5450_TI;
static TypeInfo* t5448_ITIs[] = 
{
	&t603_TI,
	&t5450_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5448_0_0_0;
extern Il2CppType t5448_1_0_0;
struct t5448;
extern Il2CppGenericClass t5448_GC;
TypeInfo t5448_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5448_MIs, t5448_PIs, NULL, NULL, NULL, NULL, NULL, &t5448_TI, t5448_ITIs, NULL, &EmptyCustomAttributesCache, &t5448_TI, &t5448_0_0_0, &t5448_1_0_0, NULL, &t5448_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.CanvasScaler>
extern Il2CppType t4266_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28548_GM;
MethodInfo m28548_MI = 
{
	"GetEnumerator", NULL, &t5450_TI, &t4266_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28548_GM};
static MethodInfo* t5450_MIs[] =
{
	&m28548_MI,
	NULL
};
static TypeInfo* t5450_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5450_0_0_0;
extern Il2CppType t5450_1_0_0;
struct t5450;
extern Il2CppGenericClass t5450_GC;
TypeInfo t5450_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5450_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5450_TI, t5450_ITIs, NULL, &EmptyCustomAttributesCache, &t5450_TI, &t5450_0_0_0, &t5450_1_0_0, NULL, &t5450_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5449_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.CanvasScaler>
extern MethodInfo m28549_MI;
extern MethodInfo m28550_MI;
static PropertyInfo t5449____Item_PropertyInfo = 
{
	&t5449_TI, "Item", &m28549_MI, &m28550_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5449_PIs[] =
{
	&t5449____Item_PropertyInfo,
	NULL
};
extern Il2CppType t252_0_0_0;
static ParameterInfo t5449_m28551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28551_GM;
MethodInfo m28551_MI = 
{
	"IndexOf", NULL, &t5449_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5449_m28551_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28551_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t252_0_0_0;
static ParameterInfo t5449_m28552_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28552_GM;
MethodInfo m28552_MI = 
{
	"Insert", NULL, &t5449_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5449_m28552_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28552_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5449_m28553_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28553_GM;
MethodInfo m28553_MI = 
{
	"RemoveAt", NULL, &t5449_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5449_m28553_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28553_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5449_m28549_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t252_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28549_GM;
MethodInfo m28549_MI = 
{
	"get_Item", NULL, &t5449_TI, &t252_0_0_0, RuntimeInvoker_t29_t44, t5449_m28549_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28549_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t252_0_0_0;
static ParameterInfo t5449_m28550_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28550_GM;
MethodInfo m28550_MI = 
{
	"set_Item", NULL, &t5449_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5449_m28550_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28550_GM};
static MethodInfo* t5449_MIs[] =
{
	&m28551_MI,
	&m28552_MI,
	&m28553_MI,
	&m28549_MI,
	&m28550_MI,
	NULL
};
static TypeInfo* t5449_ITIs[] = 
{
	&t603_TI,
	&t5448_TI,
	&t5450_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5449_0_0_0;
extern Il2CppType t5449_1_0_0;
struct t5449;
extern Il2CppGenericClass t5449_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5449_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5449_MIs, t5449_PIs, NULL, NULL, NULL, NULL, NULL, &t5449_TI, t5449_ITIs, NULL, &t1908__CustomAttributeCache, &t5449_TI, &t5449_0_0_0, &t5449_1_0_0, NULL, &t5449_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2726.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2726_TI;
#include "t2726MD.h"

#include "t2727.h"
extern TypeInfo t2727_TI;
#include "t2727MD.h"
extern MethodInfo m14888_MI;
extern MethodInfo m14890_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.CanvasScaler>
extern Il2CppType t316_0_0_33;
FieldInfo t2726_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2726_TI, offsetof(t2726, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2726_FIs[] =
{
	&t2726_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t252_0_0_0;
static ParameterInfo t2726_m14886_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14886_GM;
MethodInfo m14886_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2726_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2726_m14886_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14886_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2726_m14887_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14887_GM;
MethodInfo m14887_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2726_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2726_m14887_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14887_GM};
static MethodInfo* t2726_MIs[] =
{
	&m14886_MI,
	&m14887_MI,
	NULL
};
extern MethodInfo m14887_MI;
extern MethodInfo m14891_MI;
static MethodInfo* t2726_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14887_MI,
	&m14891_MI,
};
extern Il2CppType t2728_0_0_0;
extern TypeInfo t2728_TI;
extern MethodInfo m21598_MI;
extern TypeInfo t252_TI;
extern MethodInfo m14893_MI;
extern TypeInfo t252_TI;
static Il2CppRGCTXData t2726_RGCTXData[8] = 
{
	&t2728_0_0_0/* Type Usage */,
	&t2728_TI/* Class Usage */,
	&m21598_MI/* Method Usage */,
	&t252_TI/* Class Usage */,
	&m14893_MI/* Method Usage */,
	&m14888_MI/* Method Usage */,
	&t252_TI/* Class Usage */,
	&m14890_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2726_0_0_0;
extern Il2CppType t2726_1_0_0;
struct t2726;
extern Il2CppGenericClass t2726_GC;
TypeInfo t2726_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2726_MIs, NULL, t2726_FIs, NULL, &t2727_TI, NULL, NULL, &t2726_TI, NULL, t2726_VT, &EmptyCustomAttributesCache, &t2726_TI, &t2726_0_0_0, &t2726_1_0_0, NULL, &t2726_GC, NULL, NULL, NULL, t2726_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2726), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2728.h"
extern TypeInfo t2728_TI;
#include "t2728MD.h"
struct t556;
#define m21598(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.CanvasScaler>
extern Il2CppType t2728_0_0_1;
FieldInfo t2727_f0_FieldInfo = 
{
	"Delegate", &t2728_0_0_1, &t2727_TI, offsetof(t2727, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2727_FIs[] =
{
	&t2727_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2727_m14888_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14888_GM;
MethodInfo m14888_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2727_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2727_m14888_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14888_GM};
extern Il2CppType t2728_0_0_0;
static ParameterInfo t2727_m14889_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2728_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14889_GM;
MethodInfo m14889_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2727_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2727_m14889_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14889_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2727_m14890_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14890_GM;
MethodInfo m14890_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2727_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2727_m14890_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14890_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2727_m14891_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14891_GM;
MethodInfo m14891_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2727_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2727_m14891_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14891_GM};
static MethodInfo* t2727_MIs[] =
{
	&m14888_MI,
	&m14889_MI,
	&m14890_MI,
	&m14891_MI,
	NULL
};
static MethodInfo* t2727_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14890_MI,
	&m14891_MI,
};
extern TypeInfo t2728_TI;
extern TypeInfo t252_TI;
static Il2CppRGCTXData t2727_RGCTXData[5] = 
{
	&t2728_0_0_0/* Type Usage */,
	&t2728_TI/* Class Usage */,
	&m21598_MI/* Method Usage */,
	&t252_TI/* Class Usage */,
	&m14893_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2727_0_0_0;
extern Il2CppType t2727_1_0_0;
struct t2727;
extern Il2CppGenericClass t2727_GC;
TypeInfo t2727_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2727_MIs, NULL, t2727_FIs, NULL, &t556_TI, NULL, NULL, &t2727_TI, NULL, t2727_VT, &EmptyCustomAttributesCache, &t2727_TI, &t2727_0_0_0, &t2727_1_0_0, NULL, &t2727_GC, NULL, NULL, NULL, t2727_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2727), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.CanvasScaler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2728_m14892_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14892_GM;
MethodInfo m14892_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2728_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2728_m14892_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14892_GM};
extern Il2CppType t252_0_0_0;
static ParameterInfo t2728_m14893_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14893_GM;
MethodInfo m14893_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2728_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2728_m14893_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14893_GM};
extern Il2CppType t252_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2728_m14894_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t252_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14894_GM;
MethodInfo m14894_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2728_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2728_m14894_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14894_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2728_m14895_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14895_GM;
MethodInfo m14895_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2728_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2728_m14895_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14895_GM};
static MethodInfo* t2728_MIs[] =
{
	&m14892_MI,
	&m14893_MI,
	&m14894_MI,
	&m14895_MI,
	NULL
};
extern MethodInfo m14894_MI;
extern MethodInfo m14895_MI;
static MethodInfo* t2728_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14893_MI,
	&m14894_MI,
	&m14895_MI,
};
static Il2CppInterfaceOffsetPair t2728_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2728_1_0_0;
struct t2728;
extern Il2CppGenericClass t2728_GC;
TypeInfo t2728_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2728_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2728_TI, NULL, t2728_VT, &EmptyCustomAttributesCache, &t2728_TI, &t2728_0_0_0, &t2728_1_0_0, t2728_IOs, &t2728_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2728), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4268_TI;

#include "t249.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>
extern MethodInfo m28554_MI;
static PropertyInfo t4268____Current_PropertyInfo = 
{
	&t4268_TI, "Current", &m28554_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4268_PIs[] =
{
	&t4268____Current_PropertyInfo,
	NULL
};
extern Il2CppType t249_0_0_0;
extern void* RuntimeInvoker_t249 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28554_GM;
MethodInfo m28554_MI = 
{
	"get_Current", NULL, &t4268_TI, &t249_0_0_0, RuntimeInvoker_t249, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28554_GM};
static MethodInfo* t4268_MIs[] =
{
	&m28554_MI,
	NULL
};
static TypeInfo* t4268_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4268_0_0_0;
extern Il2CppType t4268_1_0_0;
struct t4268;
extern Il2CppGenericClass t4268_GC;
TypeInfo t4268_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4268_MIs, t4268_PIs, NULL, NULL, NULL, NULL, NULL, &t4268_TI, t4268_ITIs, NULL, &EmptyCustomAttributesCache, &t4268_TI, &t4268_0_0_0, &t4268_1_0_0, NULL, &t4268_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2729.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2729_TI;
#include "t2729MD.h"

extern TypeInfo t249_TI;
extern MethodInfo m14900_MI;
extern MethodInfo m21600_MI;
struct t20;
 int32_t m21600 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14896_MI;
 void m14896 (t2729 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14897_MI;
 t29 * m14897 (t2729 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14900(__this, &m14900_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t249_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14898_MI;
 void m14898 (t2729 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14899_MI;
 bool m14899 (t2729 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14900 (t2729 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21600(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21600_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2729_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2729_TI, offsetof(t2729, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2729_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2729_TI, offsetof(t2729, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2729_FIs[] =
{
	&t2729_f0_FieldInfo,
	&t2729_f1_FieldInfo,
	NULL
};
static PropertyInfo t2729____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2729_TI, "System.Collections.IEnumerator.Current", &m14897_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2729____Current_PropertyInfo = 
{
	&t2729_TI, "Current", &m14900_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2729_PIs[] =
{
	&t2729____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2729____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2729_m14896_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14896_GM;
MethodInfo m14896_MI = 
{
	".ctor", (methodPointerType)&m14896, &t2729_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2729_m14896_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14896_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14897_GM;
MethodInfo m14897_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14897, &t2729_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14897_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14898_GM;
MethodInfo m14898_MI = 
{
	"Dispose", (methodPointerType)&m14898, &t2729_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14898_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14899_GM;
MethodInfo m14899_MI = 
{
	"MoveNext", (methodPointerType)&m14899, &t2729_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14899_GM};
extern Il2CppType t249_0_0_0;
extern void* RuntimeInvoker_t249 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14900_GM;
MethodInfo m14900_MI = 
{
	"get_Current", (methodPointerType)&m14900, &t2729_TI, &t249_0_0_0, RuntimeInvoker_t249, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14900_GM};
static MethodInfo* t2729_MIs[] =
{
	&m14896_MI,
	&m14897_MI,
	&m14898_MI,
	&m14899_MI,
	&m14900_MI,
	NULL
};
static MethodInfo* t2729_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14897_MI,
	&m14899_MI,
	&m14898_MI,
	&m14900_MI,
};
static TypeInfo* t2729_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4268_TI,
};
static Il2CppInterfaceOffsetPair t2729_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4268_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2729_0_0_0;
extern Il2CppType t2729_1_0_0;
extern Il2CppGenericClass t2729_GC;
TypeInfo t2729_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2729_MIs, t2729_PIs, t2729_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2729_TI, t2729_ITIs, t2729_VT, &EmptyCustomAttributesCache, &t2729_TI, &t2729_0_0_0, &t2729_1_0_0, t2729_IOs, &t2729_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2729)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5451_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.CanvasScaler/ScaleMode>
extern MethodInfo m28555_MI;
static PropertyInfo t5451____Count_PropertyInfo = 
{
	&t5451_TI, "Count", &m28555_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28556_MI;
static PropertyInfo t5451____IsReadOnly_PropertyInfo = 
{
	&t5451_TI, "IsReadOnly", &m28556_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5451_PIs[] =
{
	&t5451____Count_PropertyInfo,
	&t5451____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28555_GM;
MethodInfo m28555_MI = 
{
	"get_Count", NULL, &t5451_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28555_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28556_GM;
MethodInfo m28556_MI = 
{
	"get_IsReadOnly", NULL, &t5451_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28556_GM};
extern Il2CppType t249_0_0_0;
extern Il2CppType t249_0_0_0;
static ParameterInfo t5451_m28557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28557_GM;
MethodInfo m28557_MI = 
{
	"Add", NULL, &t5451_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5451_m28557_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28557_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28558_GM;
MethodInfo m28558_MI = 
{
	"Clear", NULL, &t5451_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28558_GM};
extern Il2CppType t249_0_0_0;
static ParameterInfo t5451_m28559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28559_GM;
MethodInfo m28559_MI = 
{
	"Contains", NULL, &t5451_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5451_m28559_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28559_GM};
extern Il2CppType t3870_0_0_0;
extern Il2CppType t3870_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5451_m28560_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3870_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28560_GM;
MethodInfo m28560_MI = 
{
	"CopyTo", NULL, &t5451_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5451_m28560_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28560_GM};
extern Il2CppType t249_0_0_0;
static ParameterInfo t5451_m28561_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28561_GM;
MethodInfo m28561_MI = 
{
	"Remove", NULL, &t5451_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5451_m28561_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28561_GM};
static MethodInfo* t5451_MIs[] =
{
	&m28555_MI,
	&m28556_MI,
	&m28557_MI,
	&m28558_MI,
	&m28559_MI,
	&m28560_MI,
	&m28561_MI,
	NULL
};
extern TypeInfo t5453_TI;
static TypeInfo* t5451_ITIs[] = 
{
	&t603_TI,
	&t5453_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5451_0_0_0;
extern Il2CppType t5451_1_0_0;
struct t5451;
extern Il2CppGenericClass t5451_GC;
TypeInfo t5451_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5451_MIs, t5451_PIs, NULL, NULL, NULL, NULL, NULL, &t5451_TI, t5451_ITIs, NULL, &EmptyCustomAttributesCache, &t5451_TI, &t5451_0_0_0, &t5451_1_0_0, NULL, &t5451_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.CanvasScaler/ScaleMode>
extern Il2CppType t4268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28562_GM;
MethodInfo m28562_MI = 
{
	"GetEnumerator", NULL, &t5453_TI, &t4268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28562_GM};
static MethodInfo* t5453_MIs[] =
{
	&m28562_MI,
	NULL
};
static TypeInfo* t5453_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5453_0_0_0;
extern Il2CppType t5453_1_0_0;
struct t5453;
extern Il2CppGenericClass t5453_GC;
TypeInfo t5453_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5453_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5453_TI, t5453_ITIs, NULL, &EmptyCustomAttributesCache, &t5453_TI, &t5453_0_0_0, &t5453_1_0_0, NULL, &t5453_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5452_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.CanvasScaler/ScaleMode>
extern MethodInfo m28563_MI;
extern MethodInfo m28564_MI;
static PropertyInfo t5452____Item_PropertyInfo = 
{
	&t5452_TI, "Item", &m28563_MI, &m28564_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5452_PIs[] =
{
	&t5452____Item_PropertyInfo,
	NULL
};
extern Il2CppType t249_0_0_0;
static ParameterInfo t5452_m28565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28565_GM;
MethodInfo m28565_MI = 
{
	"IndexOf", NULL, &t5452_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5452_m28565_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28565_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t249_0_0_0;
static ParameterInfo t5452_m28566_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28566_GM;
MethodInfo m28566_MI = 
{
	"Insert", NULL, &t5452_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5452_m28566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28566_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5452_m28567_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28567_GM;
MethodInfo m28567_MI = 
{
	"RemoveAt", NULL, &t5452_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5452_m28567_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28567_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5452_m28563_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t249_0_0_0;
extern void* RuntimeInvoker_t249_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28563_GM;
MethodInfo m28563_MI = 
{
	"get_Item", NULL, &t5452_TI, &t249_0_0_0, RuntimeInvoker_t249_t44, t5452_m28563_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28563_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t249_0_0_0;
static ParameterInfo t5452_m28564_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28564_GM;
MethodInfo m28564_MI = 
{
	"set_Item", NULL, &t5452_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5452_m28564_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28564_GM};
static MethodInfo* t5452_MIs[] =
{
	&m28565_MI,
	&m28566_MI,
	&m28567_MI,
	&m28563_MI,
	&m28564_MI,
	NULL
};
static TypeInfo* t5452_ITIs[] = 
{
	&t603_TI,
	&t5451_TI,
	&t5453_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5452_0_0_0;
extern Il2CppType t5452_1_0_0;
struct t5452;
extern Il2CppGenericClass t5452_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5452_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5452_MIs, t5452_PIs, NULL, NULL, NULL, NULL, NULL, &t5452_TI, t5452_ITIs, NULL, &t1908__CustomAttributeCache, &t5452_TI, &t5452_0_0_0, &t5452_1_0_0, NULL, &t5452_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4270_TI;

#include "t250.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>
extern MethodInfo m28568_MI;
static PropertyInfo t4270____Current_PropertyInfo = 
{
	&t4270_TI, "Current", &m28568_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4270_PIs[] =
{
	&t4270____Current_PropertyInfo,
	NULL
};
extern Il2CppType t250_0_0_0;
extern void* RuntimeInvoker_t250 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28568_GM;
MethodInfo m28568_MI = 
{
	"get_Current", NULL, &t4270_TI, &t250_0_0_0, RuntimeInvoker_t250, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28568_GM};
static MethodInfo* t4270_MIs[] =
{
	&m28568_MI,
	NULL
};
static TypeInfo* t4270_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4270_0_0_0;
extern Il2CppType t4270_1_0_0;
struct t4270;
extern Il2CppGenericClass t4270_GC;
TypeInfo t4270_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4270_MIs, t4270_PIs, NULL, NULL, NULL, NULL, NULL, &t4270_TI, t4270_ITIs, NULL, &EmptyCustomAttributesCache, &t4270_TI, &t4270_0_0_0, &t4270_1_0_0, NULL, &t4270_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2730.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2730_TI;
#include "t2730MD.h"

extern TypeInfo t250_TI;
extern MethodInfo m14905_MI;
extern MethodInfo m21611_MI;
struct t20;
 int32_t m21611 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14901_MI;
 void m14901 (t2730 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14902_MI;
 t29 * m14902 (t2730 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14905(__this, &m14905_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t250_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14903_MI;
 void m14903 (t2730 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14904_MI;
 bool m14904 (t2730 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14905 (t2730 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21611(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21611_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2730_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2730_TI, offsetof(t2730, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2730_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2730_TI, offsetof(t2730, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2730_FIs[] =
{
	&t2730_f0_FieldInfo,
	&t2730_f1_FieldInfo,
	NULL
};
static PropertyInfo t2730____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2730_TI, "System.Collections.IEnumerator.Current", &m14902_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2730____Current_PropertyInfo = 
{
	&t2730_TI, "Current", &m14905_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2730_PIs[] =
{
	&t2730____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2730____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2730_m14901_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14901_GM;
MethodInfo m14901_MI = 
{
	".ctor", (methodPointerType)&m14901, &t2730_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2730_m14901_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14901_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14902_GM;
MethodInfo m14902_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14902, &t2730_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14902_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14903_GM;
MethodInfo m14903_MI = 
{
	"Dispose", (methodPointerType)&m14903, &t2730_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14903_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14904_GM;
MethodInfo m14904_MI = 
{
	"MoveNext", (methodPointerType)&m14904, &t2730_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14904_GM};
extern Il2CppType t250_0_0_0;
extern void* RuntimeInvoker_t250 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14905_GM;
MethodInfo m14905_MI = 
{
	"get_Current", (methodPointerType)&m14905, &t2730_TI, &t250_0_0_0, RuntimeInvoker_t250, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14905_GM};
static MethodInfo* t2730_MIs[] =
{
	&m14901_MI,
	&m14902_MI,
	&m14903_MI,
	&m14904_MI,
	&m14905_MI,
	NULL
};
static MethodInfo* t2730_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14902_MI,
	&m14904_MI,
	&m14903_MI,
	&m14905_MI,
};
static TypeInfo* t2730_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4270_TI,
};
static Il2CppInterfaceOffsetPair t2730_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4270_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2730_0_0_0;
extern Il2CppType t2730_1_0_0;
extern Il2CppGenericClass t2730_GC;
TypeInfo t2730_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2730_MIs, t2730_PIs, t2730_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2730_TI, t2730_ITIs, t2730_VT, &EmptyCustomAttributesCache, &t2730_TI, &t2730_0_0_0, &t2730_1_0_0, t2730_IOs, &t2730_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2730)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5454_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>
extern MethodInfo m28569_MI;
static PropertyInfo t5454____Count_PropertyInfo = 
{
	&t5454_TI, "Count", &m28569_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28570_MI;
static PropertyInfo t5454____IsReadOnly_PropertyInfo = 
{
	&t5454_TI, "IsReadOnly", &m28570_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5454_PIs[] =
{
	&t5454____Count_PropertyInfo,
	&t5454____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28569_GM;
MethodInfo m28569_MI = 
{
	"get_Count", NULL, &t5454_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28569_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28570_GM;
MethodInfo m28570_MI = 
{
	"get_IsReadOnly", NULL, &t5454_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28570_GM};
extern Il2CppType t250_0_0_0;
extern Il2CppType t250_0_0_0;
static ParameterInfo t5454_m28571_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28571_GM;
MethodInfo m28571_MI = 
{
	"Add", NULL, &t5454_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5454_m28571_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28571_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28572_GM;
MethodInfo m28572_MI = 
{
	"Clear", NULL, &t5454_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28572_GM};
extern Il2CppType t250_0_0_0;
static ParameterInfo t5454_m28573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28573_GM;
MethodInfo m28573_MI = 
{
	"Contains", NULL, &t5454_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5454_m28573_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28573_GM};
extern Il2CppType t3871_0_0_0;
extern Il2CppType t3871_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5454_m28574_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3871_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28574_GM;
MethodInfo m28574_MI = 
{
	"CopyTo", NULL, &t5454_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5454_m28574_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28574_GM};
extern Il2CppType t250_0_0_0;
static ParameterInfo t5454_m28575_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28575_GM;
MethodInfo m28575_MI = 
{
	"Remove", NULL, &t5454_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5454_m28575_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28575_GM};
static MethodInfo* t5454_MIs[] =
{
	&m28569_MI,
	&m28570_MI,
	&m28571_MI,
	&m28572_MI,
	&m28573_MI,
	&m28574_MI,
	&m28575_MI,
	NULL
};
extern TypeInfo t5456_TI;
static TypeInfo* t5454_ITIs[] = 
{
	&t603_TI,
	&t5456_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5454_0_0_0;
extern Il2CppType t5454_1_0_0;
struct t5454;
extern Il2CppGenericClass t5454_GC;
TypeInfo t5454_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5454_MIs, t5454_PIs, NULL, NULL, NULL, NULL, NULL, &t5454_TI, t5454_ITIs, NULL, &EmptyCustomAttributesCache, &t5454_TI, &t5454_0_0_0, &t5454_1_0_0, NULL, &t5454_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>
extern Il2CppType t4270_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28576_GM;
MethodInfo m28576_MI = 
{
	"GetEnumerator", NULL, &t5456_TI, &t4270_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28576_GM};
static MethodInfo* t5456_MIs[] =
{
	&m28576_MI,
	NULL
};
static TypeInfo* t5456_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5456_0_0_0;
extern Il2CppType t5456_1_0_0;
struct t5456;
extern Il2CppGenericClass t5456_GC;
TypeInfo t5456_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5456_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5456_TI, t5456_ITIs, NULL, &EmptyCustomAttributesCache, &t5456_TI, &t5456_0_0_0, &t5456_1_0_0, NULL, &t5456_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
