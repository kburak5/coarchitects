﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t6;
struct t16;
struct t24;
struct t50;
struct t7;
#include "t57.h"
#include "t17.h"
#include "t23.h"
#include "t111.h"

 void m193 (t6 * __this, t50 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m194 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m195 (t6 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m196 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m197 (t6 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m198 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m199 (t6 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m200 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m201 (t6 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m202 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m203 (t6 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m204 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m205 (t6 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m206 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m207 (t6 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m208 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m209 (t6 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m210 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m211 (t6 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m212 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m213 (t6 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m214 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m215 (t6 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m216 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m217 (t6 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m218 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m219 (t6 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m220 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m221 (t6 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m222 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m223 (t6 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m224 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m225 (t6 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m226 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m227 (t6 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m228 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m229 (t6 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m230 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m231 (t6 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m232 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m233 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t24 * m234 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t24 * m235 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m236 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m237 (t6 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m238 (t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
