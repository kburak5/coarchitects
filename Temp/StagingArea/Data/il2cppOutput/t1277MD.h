﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1277;
struct t29;
struct t719;
struct t20;
struct t136;

 void m6703 (t1277 * __this, t719 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6704 (t1277 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6705 (t1277 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6706 (t1277 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6707 (t1277 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6708 (t1277 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
