﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1626;
struct t733;
struct t1094;
struct t29;
struct t42;
struct t7;
#include "t735.h"
#include "t465.h"
#include "t1126.h"

 void m9093 (t1626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9094 (t1626 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9095 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9096 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m9097 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9098 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9099 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m9100 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9101 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m9102 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9103 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9104 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9105 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9106 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9107 (t1626 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9108 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9109 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9110 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9111 (t1626 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9112 (t1626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9113 (t1626 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
