﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2697;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m14667 (t2697 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14668 (t2697 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14669 (t2697 * __this, bool p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14670 (t2697 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
