﻿#pragma once
#include <stdint.h>
#include "t110.h"
struct t1187 
{
	int32_t f0;
	uint8_t* f1;
};
// Native definition for marshalling of: Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
struct t1187_marshaled
{
	int32_t f0;
	uint8_t* f1;
};
