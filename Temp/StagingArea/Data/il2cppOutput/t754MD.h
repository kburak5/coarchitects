﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t754;
struct t748;
struct t29;
struct t745;
struct t756;
#include "t741.h"

 void m3166 (t754 * __this, t748 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3167 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3168 (t29 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
