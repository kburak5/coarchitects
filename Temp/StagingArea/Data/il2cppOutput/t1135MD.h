﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1135;
struct t29;
struct t20;

 void m5887 (t1135 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5888 (t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5889 (t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
