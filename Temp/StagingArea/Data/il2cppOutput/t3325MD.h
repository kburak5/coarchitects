﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3325;
struct t29;
struct t20;
#include "t1200.h"

 void m18507 (t3325 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18508 (t3325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18509 (t3325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18510 (t3325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18511 (t3325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
