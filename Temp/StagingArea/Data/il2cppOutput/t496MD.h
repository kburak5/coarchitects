﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t496;
struct t29;
struct t24;
struct t66;
struct t67;
#include "t35.h"

 void m2480 (t496 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2481 (t496 * __this, t24 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2482 (t496 * __this, t24 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2483 (t496 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
