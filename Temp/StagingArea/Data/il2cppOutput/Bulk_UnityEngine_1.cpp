﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t41.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t41_TI;
#include "t41MD.h"

#include "t7.h"
#include "t21.h"
#include "t385.h"
#include "t22.h"
#include "t40.h"
#include "t29.h"
#include "t44.h"
#include "t35.h"
extern TypeInfo t35_TI;
#include "t29MD.h"
#include "t35MD.h"
extern MethodInfo m1331_MI;
extern MethodInfo m2545_MI;
extern MethodInfo m2546_MI;
extern MethodInfo m2547_MI;
extern MethodInfo m2549_MI;
extern MethodInfo m2548_MI;
extern MethodInfo m2550_MI;
extern MethodInfo m2888_MI;

#include "t20.h"

extern MethodInfo m2544_MI;
 void m2544 (t41 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
 void m2545 (t29 * __this, t41 * p0, float p1, MethodInfo* method){
	typedef void (*m2545_ftn) (t41 *, float);
	static m2545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(p0, p1);
}
extern MethodInfo m67_MI;
 void m67 (t29 * __this, t41 * p0, MethodInfo* method){
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		m2545(NULL, p0, V_0, &m2545_MI);
		return;
	}
}
 void m2546 (t29 * __this, t41 * p0, bool p1, MethodInfo* method){
	typedef void (*m2546_ftn) (t41 *, bool);
	static m2546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(p0, p1);
}
extern MethodInfo m1820_MI;
 void m1820 (t29 * __this, t41 * p0, MethodInfo* method){
	bool V_0 = false;
	{
		V_0 = 0;
		m2546(NULL, p0, V_0, &m2546_MI);
		return;
	}
}
extern MethodInfo m1385_MI;
 t7* m1385 (t41 * __this, MethodInfo* method){
	typedef t7* (*m1385_ftn) (t41 *);
	static m1385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1908_MI;
 void m1908 (t41 * __this, t7* p0, MethodInfo* method){
	typedef void (*m1908_ftn) (t41 *, t7*);
	static m1908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1778_MI;
 void m1778 (t41 * __this, int32_t p0, MethodInfo* method){
	typedef void (*m1778_ftn) (t41 *, int32_t);
	static m1778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m48_MI;
 t7* m48 (t41 * __this, MethodInfo* method){
	typedef t7* (*m48_ftn) (t41 *);
	static m48_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m48_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m45_MI;
 bool m45 (t41 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m2547(NULL, __this, ((t41 *)IsInst(p0, InitializedTypeInfo(&t41_TI))), &m2547_MI);
		return L_0;
	}
}
extern MethodInfo m47_MI;
 int32_t m47 (t41 * __this, MethodInfo* method){
	{
		int32_t L_0 = m2549(__this, &m2549_MI);
		return L_0;
	}
}
 bool m2547 (t29 * __this, t41 * p0, t41 * p1, MethodInfo* method){
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = ((((t41 *)p0) == ((t29 *)NULL))? 1 : 0);
		V_1 = ((((t41 *)p1) == ((t29 *)NULL))? 1 : 0);
		if (!V_1)
		{
			goto IL_0018;
		}
	}
	{
		if (!V_0)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		if (!V_1)
		{
			goto IL_0028;
		}
	}
	{
		bool L_0 = m2548(NULL, p0, &m2548_MI);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		if (!V_0)
		{
			goto IL_0038;
		}
	}
	{
		bool L_1 = m2548(NULL, p1, &m2548_MI);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		int32_t L_2 = (p0->f0);
		int32_t L_3 = (p1->f0);
		return ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
 bool m2548 (t29 * __this, t41 * p0, MethodInfo* method){
	{
		t35 L_0 = m2550(p0, &m2550_MI);
		bool L_1 = m2888(NULL, L_0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2888_MI);
		return L_1;
	}
}
 int32_t m2549 (t41 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
 t35 m2550 (t41 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m1293_MI;
 bool m1293 (t29 * __this, t41 * p0, MethodInfo* method){
	{
		bool L_0 = m2547(NULL, p0, (t41 *)NULL, &m2547_MI);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m1297_MI;
 bool m1297 (t29 * __this, t41 * p0, t41 * p1, MethodInfo* method){
	{
		bool L_0 = m2547(NULL, p0, p1, &m2547_MI);
		return L_0;
	}
}
extern MethodInfo m1300_MI;
 bool m1300 (t29 * __this, t41 * p0, t41 * p1, MethodInfo* method){
	{
		bool L_0 = m2547(NULL, p0, p1, &m2547_MI);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
void t41_marshal(const t41& unmarshaled, t41_marshaled& marshaled)
{
	marshaled.f0 = unmarshaled.f0;
	marshaled.f1 = unmarshaled.f1;
}
void t41_marshal_back(const t41_marshaled& marshaled, t41& unmarshaled)
{
	unmarshaled.f0 = marshaled.f0;
	unmarshaled.f1 = marshaled.f1;
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
void t41_marshal_cleanup(t41_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.Object
extern Il2CppType t44_0_0_1;
FieldInfo t41_f0_FieldInfo = 
{
	"m_InstanceID", &t44_0_0_1, &t41_TI, offsetof(t41, f0), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_1;
FieldInfo t41_f1_FieldInfo = 
{
	"m_CachedPtr", &t35_0_0_1, &t41_TI, offsetof(t41, f1), &EmptyCustomAttributesCache};
static FieldInfo* t41_FIs[] =
{
	&t41_f0_FieldInfo,
	&t41_f1_FieldInfo,
	NULL
};
static PropertyInfo t41____name_PropertyInfo = 
{
	&t41_TI, "name", &m1385_MI, &m1908_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t41____hideFlags_PropertyInfo = 
{
	&t41_TI, "hideFlags", NULL, &m1778_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t41_PIs[] =
{
	&t41____name_PropertyInfo,
	&t41____hideFlags_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2544_MI = 
{
	".ctor", (methodPointerType)&m2544, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 711, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t41__CustomAttributeCache_t41_m2545_Arg1_ParameterInfo;
static ParameterInfo t41_m2545_ParameterInfos[] = 
{
	{"obj", 0, 134218436, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"t", 1, 134218437, &t41__CustomAttributeCache_t41_m2545_Arg1_ParameterInfo, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m2545;
MethodInfo m2545_MI = 
{
	"Destroy", (methodPointerType)&m2545, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t22, t41_m2545_ParameterInfos, &t41__CustomAttributeCache_m2545, 150, 4096, 255, 2, false, false, 712, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m67_ParameterInfos[] = 
{
	{"obj", 0, 134218438, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m67;
MethodInfo m67_MI = 
{
	"Destroy", (methodPointerType)&m67, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t41_m67_ParameterInfos, &t41__CustomAttributeCache_m67, 150, 0, 255, 1, false, false, 713, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern CustomAttributesCache t41__CustomAttributeCache_t41_m2546_Arg1_ParameterInfo;
static ParameterInfo t41_m2546_ParameterInfos[] = 
{
	{"obj", 0, 134218439, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"allowDestroyingAssets", 1, 134218440, &t41__CustomAttributeCache_t41_m2546_Arg1_ParameterInfo, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m2546;
MethodInfo m2546_MI = 
{
	"DestroyImmediate", (methodPointerType)&m2546, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t41_m2546_ParameterInfos, &t41__CustomAttributeCache_m2546, 150, 4096, 255, 2, false, false, 714, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m1820_ParameterInfos[] = 
{
	{"obj", 0, 134218441, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m1820;
MethodInfo m1820_MI = 
{
	"DestroyImmediate", (methodPointerType)&m1820, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t41_m1820_ParameterInfos, &t41__CustomAttributeCache_m1820, 150, 0, 255, 1, false, false, 715, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m1385;
MethodInfo m1385_MI = 
{
	"get_name", (methodPointerType)&m1385, &t41_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &t41__CustomAttributeCache_m1385, 2182, 4096, 255, 0, false, false, 716, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t41_m1908_ParameterInfos[] = 
{
	{"value", 0, 134218442, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m1908;
MethodInfo m1908_MI = 
{
	"set_name", (methodPointerType)&m1908, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t41_m1908_ParameterInfos, &t41__CustomAttributeCache_m1908, 2182, 4096, 255, 1, false, false, 717, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t385_0_0_0;
extern Il2CppType t385_0_0_0;
static ParameterInfo t41_m1778_ParameterInfos[] = 
{
	{"value", 0, 134218443, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m1778;
MethodInfo m1778_MI = 
{
	"set_hideFlags", (methodPointerType)&m1778, &t41_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t41_m1778_ParameterInfos, &t41__CustomAttributeCache_m1778, 2182, 4096, 255, 1, false, false, 718, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t41__CustomAttributeCache_m48;
MethodInfo m48_MI = 
{
	"ToString", (methodPointerType)&m48, &t41_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &t41__CustomAttributeCache_m48, 198, 4096, 3, 0, false, false, 719, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t41_m45_ParameterInfos[] = 
{
	{"o", 0, 134218444, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m45_MI = 
{
	"Equals", (methodPointerType)&m45, &t41_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t41_m45_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 720, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m47_MI = 
{
	"GetHashCode", (methodPointerType)&m47, &t41_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 721, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m2547_ParameterInfos[] = 
{
	{"lhs", 0, 134218445, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"rhs", 1, 134218446, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2547_MI = 
{
	"CompareBaseObjects", (methodPointerType)&m2547, &t41_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t41_m2547_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 722, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m2548_ParameterInfos[] = 
{
	{"o", 0, 134218447, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2548_MI = 
{
	"IsNativeObjectAlive", (methodPointerType)&m2548, &t41_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t41_m2548_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 723, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2549_MI = 
{
	"GetInstanceID", (methodPointerType)&m2549, &t41_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 724, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern void* RuntimeInvoker_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m2550_MI = 
{
	"GetCachedPtr", (methodPointerType)&m2550, &t41_TI, &t35_0_0_0, RuntimeInvoker_t35, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 725, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m1293_ParameterInfos[] = 
{
	{"exists", 0, 134218448, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1293_MI = 
{
	"op_Implicit", (methodPointerType)&m1293, &t41_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t41_m1293_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 1, false, false, 726, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m1297_ParameterInfos[] = 
{
	{"x", 0, 134218449, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"y", 1, 134218450, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1297_MI = 
{
	"op_Equality", (methodPointerType)&m1297, &t41_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t41_m1297_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 727, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t41_m1300_ParameterInfos[] = 
{
	{"x", 0, 134218451, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"y", 1, 134218452, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1300_MI = 
{
	"op_Inequality", (methodPointerType)&m1300, &t41_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t41_m1300_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 728, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t41_MIs[] =
{
	&m2544_MI,
	&m2545_MI,
	&m67_MI,
	&m2546_MI,
	&m1820_MI,
	&m1385_MI,
	&m1908_MI,
	&m1778_MI,
	&m48_MI,
	&m45_MI,
	&m47_MI,
	&m2547_MI,
	&m2548_MI,
	&m2549_MI,
	&m2550_MI,
	&m1293_MI,
	&m1297_MI,
	&m1300_MI,
	NULL
};
extern MethodInfo m46_MI;
static MethodInfo* t41_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern TypeInfo t532_TI;
#include "t532.h"
#include "t532MD.h"
extern MethodInfo m2731_MI;
void t41_CustomAttributesCacheGenerator_m2545(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t576_TI;
#include "t576.h"
#include "t576MD.h"
extern MethodInfo m2826_MI;
void t41_CustomAttributesCacheGenerator_t41_m2545_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t577_TI;
#include "t577.h"
#include "t577MD.h"
extern MethodInfo m2830_MI;
void t41_CustomAttributesCacheGenerator_m67(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t577 * tmp;
		tmp = (t577 *)il2cpp_codegen_object_new (&t577_TI);
		m2830(tmp, &m2830_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_m2546(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_t41_m2546_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("false"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_m1820(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t577 * tmp;
		tmp = (t577 *)il2cpp_codegen_object_new (&t577_TI);
		m2830(tmp, &m2830_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_m1385(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_m1908(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_m1778(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t41_CustomAttributesCacheGenerator_m48(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t41__CustomAttributeCache_m2545 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m2545
};
CustomAttributesCache t41__CustomAttributeCache_t41_m2545_Arg1_ParameterInfo = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_t41_m2545_Arg1_ParameterInfo
};
CustomAttributesCache t41__CustomAttributeCache_m67 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m67
};
CustomAttributesCache t41__CustomAttributeCache_m2546 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m2546
};
CustomAttributesCache t41__CustomAttributeCache_t41_m2546_Arg1_ParameterInfo = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_t41_m2546_Arg1_ParameterInfo
};
CustomAttributesCache t41__CustomAttributeCache_m1820 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m1820
};
CustomAttributesCache t41__CustomAttributeCache_m1385 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m1385
};
CustomAttributesCache t41__CustomAttributeCache_m1908 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m1908
};
CustomAttributesCache t41__CustomAttributeCache_m1778 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m1778
};
CustomAttributesCache t41__CustomAttributeCache_m48 = {
1,
NULL,
&t41_CustomAttributesCacheGenerator_m48
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t41_1_0_0;
extern TypeInfo t29_TI;
struct t41;
extern CustomAttributesCache t41__CustomAttributeCache_m2545;
extern CustomAttributesCache t41__CustomAttributeCache_m67;
extern CustomAttributesCache t41__CustomAttributeCache_m2546;
extern CustomAttributesCache t41__CustomAttributeCache_m1820;
extern CustomAttributesCache t41__CustomAttributeCache_m1385;
extern CustomAttributesCache t41__CustomAttributeCache_m1908;
extern CustomAttributesCache t41__CustomAttributeCache_m1778;
extern CustomAttributesCache t41__CustomAttributeCache_m48;
TypeInfo t41_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Object", "UnityEngine", t41_MIs, t41_PIs, t41_FIs, NULL, &t29_TI, NULL, NULL, &t41_TI, NULL, t41_VT, &EmptyCustomAttributesCache, &t41_TI, &t41_0_0_0, &t41_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t41_marshal, (methodPointerType)t41_marshal_back, (methodPointerType)t41_marshal_cleanup, sizeof (t41), 0, sizeof(t41_marshaled), 0, 0, -1, 1048585, 0, false, false, false, false, false, false, false, false, false, false, false, false, 18, 2, 2, 0, 0, 4, 0, 0};
#include "t28.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t28_TI;
#include "t28MD.h"

#include "t25.h"
#include "t16.h"
#include "t42.h"
#include "t268.h"
#include "t16MD.h"
extern MethodInfo m1392_MI;
extern MethodInfo m2553_MI;
extern MethodInfo m2552_MI;


extern MethodInfo m2551_MI;
 void m2551 (t28 * __this, MethodInfo* method){
	{
		m2544(__this, &m2544_MI);
		return;
	}
}
extern MethodInfo m39_MI;
 t25 * m39 (t28 * __this, MethodInfo* method){
	typedef t25 * (*m39_ftn) (t28 *);
	static m39_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m39_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
 t16 * m1392 (t28 * __this, MethodInfo* method){
	typedef t16 * (*m1392_ftn) (t28 *);
	static m1392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1987_MI;
 t28 * m1987 (t28 * __this, t42 * p0, MethodInfo* method){
	{
		t16 * L_0 = m1392(__this, &m1392_MI);
		t28 * L_1 = m2553(L_0, p0, &m2553_MI);
		return L_1;
	}
}
 void m2552 (t28 * __this, t42 * p0, t29 * p1, MethodInfo* method){
	typedef void (*m2552_ftn) (t28 *, t42 *, t29 *);
	static m2552_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2552_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, p0, p1);
}
extern MethodInfo m1555_MI;
 void m1555 (t28 * __this, t42 * p0, t268 * p1, MethodInfo* method){
	{
		m2552(__this, p0, p1, &m2552_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Component
static PropertyInfo t28____transform_PropertyInfo = 
{
	&t28_TI, "transform", &m39_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t28____gameObject_PropertyInfo = 
{
	&t28_TI, "gameObject", &m1392_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t28_PIs[] =
{
	&t28____transform_PropertyInfo,
	&t28____gameObject_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2551_MI = 
{
	".ctor", (methodPointerType)&m2551, &t28_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 729, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t28__CustomAttributeCache_m39;
MethodInfo m39_MI = 
{
	"get_transform", (methodPointerType)&m39, &t28_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &t28__CustomAttributeCache_m39, 2182, 4096, 255, 0, false, false, 730, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t16_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t28__CustomAttributeCache_m1392;
MethodInfo m1392_MI = 
{
	"get_gameObject", (methodPointerType)&m1392, &t28_TI, &t16_0_0_0, RuntimeInvoker_t29, NULL, &t28__CustomAttributeCache_m1392, 2182, 4096, 255, 0, false, false, 731, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t28_m1987_ParameterInfos[] = 
{
	{"type", 0, 134218453, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t28__CustomAttributeCache_m1987;
MethodInfo m1987_MI = 
{
	"GetComponent", (methodPointerType)&m1987, &t28_TI, &t28_0_0_0, RuntimeInvoker_t29_t29, t28_m1987_ParameterInfos, &t28__CustomAttributeCache_m1987, 134, 0, 255, 1, false, false, 732, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType m2889_gp_0_0_0_0;
extern Il2CppGenericContainer m2889_IGC;
extern TypeInfo m2889_gp_T_0_TI;
Il2CppGenericParamFull m2889_gp_T_0_TI_GenericParamFull = { { &m2889_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2889_IGPA[1] = 
{
	&m2889_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2889_MI;
Il2CppGenericContainer m2889_IGC = { { NULL, NULL }, NULL, &m2889_MI, 1, 1, m2889_IGPA };
extern Il2CppType m2889_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2889_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2889_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m2889_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache t28__CustomAttributeCache_m2889;
MethodInfo m2889_MI = 
{
	"GetComponent", NULL, &t28_TI, &m2889_gp_0_0_0_0, NULL, NULL, &t28__CustomAttributeCache_m2889, 134, 0, 255, 0, true, false, 733, m2889_RGCTXData, (methodPointerType)NULL, &m2889_IGC};
extern Il2CppType t40_0_0_0;
extern Il2CppType t609_0_0_0;
extern Il2CppType t609_0_0_0;
static ParameterInfo t28_m2890_ParameterInfos[] = 
{
	{"includeInactive", 0, 134218454, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"result", 1, 134218455, &EmptyCustomAttributesCache, &t609_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2890_IGC;
extern TypeInfo m2890_gp_T_0_TI;
Il2CppGenericParamFull m2890_gp_T_0_TI_GenericParamFull = { { &m2890_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2890_IGPA[1] = 
{
	&m2890_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2890_MI;
Il2CppGenericContainer m2890_IGC = { { NULL, NULL }, NULL, &m2890_MI, 1, 1, m2890_IGPA };
extern Il2CppGenericMethod m2891_GM;
static Il2CppRGCTXDefinition m2890_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m2891_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2890_MI = 
{
	"GetComponentsInChildren", NULL, &t28_TI, &t21_0_0_0, NULL, t28_m2890_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, true, false, 734, m2890_RGCTXData, (methodPointerType)NULL, &m2890_IGC};
extern Il2CppType t611_0_0_0;
extern Il2CppType t611_0_0_0;
static ParameterInfo t28_m2892_ParameterInfos[] = 
{
	{"results", 0, 134218456, &EmptyCustomAttributesCache, &t611_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2892_IGC;
extern TypeInfo m2892_gp_T_0_TI;
Il2CppGenericParamFull m2892_gp_T_0_TI_GenericParamFull = { { &m2892_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2892_IGPA[1] = 
{
	&m2892_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2892_MI;
Il2CppGenericContainer m2892_IGC = { { NULL, NULL }, NULL, &m2892_MI, 1, 1, m2892_IGPA };
extern Il2CppGenericMethod m2893_GM;
static Il2CppRGCTXDefinition m2892_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m2893_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2892_MI = 
{
	"GetComponentsInChildren", NULL, &t28_TI, &t21_0_0_0, NULL, t28_m2892_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, true, false, 735, m2892_RGCTXData, (methodPointerType)NULL, &m2892_IGC};
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t28_m2552_ParameterInfos[] = 
{
	{"searchType", 0, 134218457, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"resultList", 1, 134218458, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t28__CustomAttributeCache_m2552;
MethodInfo m2552_MI = 
{
	"GetComponentsForListInternal", (methodPointerType)&m2552, &t28_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t28_m2552_ParameterInfos, &t28__CustomAttributeCache_m2552, 129, 4096, 255, 2, false, false, 736, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t268_0_0_0;
extern Il2CppType t268_0_0_0;
static ParameterInfo t28_m1555_ParameterInfos[] = 
{
	{"type", 0, 134218459, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"results", 1, 134218460, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1555_MI = 
{
	"GetComponents", (methodPointerType)&m1555, &t28_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t28_m1555_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 737, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t613_0_0_0;
extern Il2CppType t613_0_0_0;
static ParameterInfo t28_m2894_ParameterInfos[] = 
{
	{"results", 0, 134218461, &EmptyCustomAttributesCache, &t613_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2894_IGC;
extern TypeInfo m2894_gp_T_0_TI;
Il2CppGenericParamFull m2894_gp_T_0_TI_GenericParamFull = { { &m2894_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2894_IGPA[1] = 
{
	&m2894_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2894_MI;
Il2CppGenericContainer m2894_IGC = { { NULL, NULL }, NULL, &m2894_MI, 1, 1, m2894_IGPA };
extern Il2CppType m2894_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2894_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2894_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2894_MI = 
{
	"GetComponents", NULL, &t28_TI, &t21_0_0_0, NULL, t28_m2894_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, true, false, 738, m2894_RGCTXData, (methodPointerType)NULL, &m2894_IGC};
static MethodInfo* t28_MIs[] =
{
	&m2551_MI,
	&m39_MI,
	&m1392_MI,
	&m1987_MI,
	&m2889_MI,
	&m2890_MI,
	&m2892_MI,
	&m2552_MI,
	&m1555_MI,
	&m2894_MI,
	NULL
};
static MethodInfo* t28_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t28_CustomAttributesCacheGenerator_m39(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t28_CustomAttributesCacheGenerator_m1392(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t579_TI;
#include "t579.h"
#include "t579MD.h"
extern MethodInfo m2831_MI;
void t28_CustomAttributesCacheGenerator_m1987(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t579 * tmp;
		tmp = (t579 *)il2cpp_codegen_object_new (&t579_TI);
		m2831(tmp, 0, &m2831_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t615_TI;
#include "t615.h"
#include "t615MD.h"
extern MethodInfo m2895_MI;
void t28_CustomAttributesCacheGenerator_m2889(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t615 * tmp;
		tmp = (t615 *)il2cpp_codegen_object_new (&t615_TI);
		m2895(tmp, &m2895_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t28_CustomAttributesCacheGenerator_m2552(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t28__CustomAttributeCache_m39 = {
1,
NULL,
&t28_CustomAttributesCacheGenerator_m39
};
CustomAttributesCache t28__CustomAttributeCache_m1392 = {
1,
NULL,
&t28_CustomAttributesCacheGenerator_m1392
};
CustomAttributesCache t28__CustomAttributeCache_m1987 = {
1,
NULL,
&t28_CustomAttributesCacheGenerator_m1987
};
CustomAttributesCache t28__CustomAttributeCache_m2889 = {
1,
NULL,
&t28_CustomAttributesCacheGenerator_m2889
};
CustomAttributesCache t28__CustomAttributeCache_m2552 = {
1,
NULL,
&t28_CustomAttributesCacheGenerator_m2552
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_1_0_0;
struct t28;
extern CustomAttributesCache t28__CustomAttributeCache_m39;
extern CustomAttributesCache t28__CustomAttributeCache_m1392;
extern CustomAttributesCache t28__CustomAttributeCache_m1987;
extern CustomAttributesCache t28__CustomAttributeCache_m2889;
extern CustomAttributesCache t28__CustomAttributeCache_m2552;
TypeInfo t28_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Component", "UnityEngine", t28_MIs, t28_PIs, NULL, NULL, &t41_TI, NULL, NULL, &t28_TI, NULL, t28_VT, &EmptyCustomAttributesCache, &t28_TI, &t28_0_0_0, &t28_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t28), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 10, 2, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t16_TI;

#include "t445.h"
extern MethodInfo m2558_MI;
extern MethodInfo m2556_MI;


extern MethodInfo m1777_MI;
 void m1777 (t16 * __this, t7* p0, MethodInfo* method){
	{
		m2544(__this, &m2544_MI);
		m2558(NULL, __this, p0, &m2558_MI);
		return;
	}
}
 t28 * m2553 (t16 * __this, t42 * p0, MethodInfo* method){
	typedef t28 * (*m2553_ftn) (t16 *, t42 *);
	static m2553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, p0);
}
extern MethodInfo m2554_MI;
 t20 * m2554 (t16 * __this, t42 * p0, bool p1, bool p2, bool p3, bool p4, t29 * p5, MethodInfo* method){
	typedef t20 * (*m2554_ftn) (t16 *, t42 *, bool, bool, bool, bool, t29 *);
	static m2554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, p0, p1, p2, p3, p4, p5);
}
extern MethodInfo m1363_MI;
 t25 * m1363 (t16 * __this, MethodInfo* method){
	typedef t25 * (*m1363_ftn) (t16 *);
	static m1363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1781_MI;
 int32_t m1781 (t16 * __this, MethodInfo* method){
	typedef int32_t (*m1781_ftn) (t16 *);
	static m1781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1782_MI;
 void m1782 (t16 * __this, int32_t p0, MethodInfo* method){
	typedef void (*m1782_ftn) (t16 *, int32_t);
	static m1782_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1782_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1393_MI;
 bool m1393 (t16 * __this, MethodInfo* method){
	typedef bool (*m1393_ftn) (t16 *);
	static m1393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m2555_MI;
 void m2555 (t16 * __this, t7* p0, t29 * p1, int32_t p2, MethodInfo* method){
	typedef void (*m2555_ftn) (t16 *, t7*, t29 *, int32_t);
	static m2555_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2555_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, p0, p1, p2);
}
 t28 * m2556 (t16 * __this, t42 * p0, MethodInfo* method){
	typedef t28 * (*m2556_ftn) (t16 *, t42 *);
	static m2556_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2556_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, p0);
}
extern MethodInfo m2557_MI;
 t28 * m2557 (t16 * __this, t42 * p0, MethodInfo* method){
	{
		t28 * L_0 = m2556(__this, p0, &m2556_MI);
		return L_0;
	}
}
 void m2558 (t29 * __this, t16 * p0, t7* p1, MethodInfo* method){
	typedef void (*m2558_ftn) (t16 *, t7*);
	static m2558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(p0, p1);
}
// Metadata Definition UnityEngine.GameObject
static PropertyInfo t16____transform_PropertyInfo = 
{
	&t16_TI, "transform", &m1363_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t16____layer_PropertyInfo = 
{
	&t16_TI, "layer", &m1781_MI, &m1782_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t16____activeInHierarchy_PropertyInfo = 
{
	&t16_TI, "activeInHierarchy", &m1393_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t16_PIs[] =
{
	&t16____transform_PropertyInfo,
	&t16____layer_PropertyInfo,
	&t16____activeInHierarchy_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t16_m1777_ParameterInfos[] = 
{
	{"name", 0, 134218462, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1777_MI = 
{
	".ctor", (methodPointerType)&m1777, &t16_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t16_m1777_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 739, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t16_m2553_ParameterInfos[] = 
{
	{"type", 0, 134218463, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m2553;
MethodInfo m2553_MI = 
{
	"GetComponent", (methodPointerType)&m2553, &t16_TI, &t28_0_0_0, RuntimeInvoker_t29_t29, t16_m2553_ParameterInfos, &t16__CustomAttributeCache_m2553, 134, 4096, 255, 1, false, false, 740, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType m2896_gp_0_0_0_0;
extern Il2CppGenericContainer m2896_IGC;
extern TypeInfo m2896_gp_T_0_TI;
Il2CppGenericParamFull m2896_gp_T_0_TI_GenericParamFull = { { &m2896_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2896_IGPA[1] = 
{
	&m2896_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2896_MI;
Il2CppGenericContainer m2896_IGC = { { NULL, NULL }, NULL, &m2896_MI, 1, 1, m2896_IGPA };
extern Il2CppType m2896_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2896_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2896_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m2896_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache t16__CustomAttributeCache_m2896;
MethodInfo m2896_MI = 
{
	"GetComponent", NULL, &t16_TI, &m2896_gp_0_0_0_0, NULL, NULL, &t16__CustomAttributeCache_m2896, 134, 0, 255, 0, true, false, 741, m2896_RGCTXData, (methodPointerType)NULL, &m2896_IGC};
extern Il2CppType t617_0_0_0;
extern Il2CppType t617_0_0_0;
static ParameterInfo t16_m2897_ParameterInfos[] = 
{
	{"results", 0, 134218464, &EmptyCustomAttributesCache, &t617_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2897_IGC;
extern TypeInfo m2897_gp_T_0_TI;
Il2CppGenericParamFull m2897_gp_T_0_TI_GenericParamFull = { { &m2897_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2897_IGPA[1] = 
{
	&m2897_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2897_MI;
Il2CppGenericContainer m2897_IGC = { { NULL, NULL }, NULL, &m2897_MI, 1, 1, m2897_IGPA };
extern Il2CppType m2897_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2897_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2897_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2897_MI = 
{
	"GetComponents", NULL, &t16_TI, &t21_0_0_0, NULL, t16_m2897_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, true, false, 742, m2897_RGCTXData, (methodPointerType)NULL, &m2897_IGC};
extern Il2CppType t40_0_0_0;
extern Il2CppType t619_0_0_0;
extern Il2CppType t619_0_0_0;
static ParameterInfo t16_m2898_ParameterInfos[] = 
{
	{"includeInactive", 0, 134218465, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"results", 1, 134218466, &EmptyCustomAttributesCache, &t619_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2898_IGC;
extern TypeInfo m2898_gp_T_0_TI;
Il2CppGenericParamFull m2898_gp_T_0_TI_GenericParamFull = { { &m2898_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2898_IGPA[1] = 
{
	&m2898_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2898_MI;
Il2CppGenericContainer m2898_IGC = { { NULL, NULL }, NULL, &m2898_MI, 1, 1, m2898_IGPA };
extern Il2CppType m2898_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2898_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2898_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2898_MI = 
{
	"GetComponentsInChildren", NULL, &t16_TI, &t21_0_0_0, NULL, t16_m2898_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, true, false, 743, m2898_RGCTXData, (methodPointerType)NULL, &m2898_IGC};
extern Il2CppType t40_0_0_0;
extern Il2CppType t621_0_0_0;
extern Il2CppType t621_0_0_0;
static ParameterInfo t16_m2899_ParameterInfos[] = 
{
	{"includeInactive", 0, 134218467, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"results", 1, 134218468, &EmptyCustomAttributesCache, &t621_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2899_IGC;
extern TypeInfo m2899_gp_T_0_TI;
Il2CppGenericParamFull m2899_gp_T_0_TI_GenericParamFull = { { &m2899_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2899_IGPA[1] = 
{
	&m2899_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2899_MI;
Il2CppGenericContainer m2899_IGC = { { NULL, NULL }, NULL, &m2899_MI, 1, 1, m2899_IGPA };
extern Il2CppType m2899_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2899_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2899_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2899_MI = 
{
	"GetComponentsInParent", NULL, &t16_TI, &t21_0_0_0, NULL, t16_m2899_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, true, false, 744, m2899_RGCTXData, (methodPointerType)NULL, &m2899_IGC};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t16_m2554_ParameterInfos[] = 
{
	{"type", 0, 134218469, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"useSearchTypeAsArrayReturnType", 1, 134218470, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"recursive", 2, 134218471, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"includeInactive", 3, 134218472, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 4, 134218473, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resultList", 5, 134218474, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t20_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t297_t297_t297_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m2554;
MethodInfo m2554_MI = 
{
	"GetComponentsInternal", (methodPointerType)&m2554, &t16_TI, &t20_0_0_0, RuntimeInvoker_t29_t29_t297_t297_t297_t297_t29, t16_m2554_ParameterInfos, &t16__CustomAttributeCache_m2554, 129, 4096, 255, 6, false, false, 745, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m1363;
MethodInfo m1363_MI = 
{
	"get_transform", (methodPointerType)&m1363, &t16_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &t16__CustomAttributeCache_m1363, 2182, 4096, 255, 0, false, false, 746, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m1781;
MethodInfo m1781_MI = 
{
	"get_layer", (methodPointerType)&m1781, &t16_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t16__CustomAttributeCache_m1781, 2182, 4096, 255, 0, false, false, 747, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t16_m1782_ParameterInfos[] = 
{
	{"value", 0, 134218475, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m1782;
MethodInfo m1782_MI = 
{
	"set_layer", (methodPointerType)&m1782, &t16_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t16_m1782_ParameterInfos, &t16__CustomAttributeCache_m1782, 2182, 4096, 255, 1, false, false, 748, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m1393;
MethodInfo m1393_MI = 
{
	"get_activeInHierarchy", (methodPointerType)&m1393, &t16_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t16__CustomAttributeCache_m1393, 2182, 4096, 255, 0, false, false, 749, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
extern CustomAttributesCache t16__CustomAttributeCache_t16_m2555_Arg1_ParameterInfo;
extern Il2CppType t445_0_0_0;
extern Il2CppType t445_0_0_0;
extern CustomAttributesCache t16__CustomAttributeCache_t16_m2555_Arg2_ParameterInfo;
static ParameterInfo t16_m2555_ParameterInfos[] = 
{
	{"methodName", 0, 134218476, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134218477, &t16__CustomAttributeCache_t16_m2555_Arg1_ParameterInfo, &t29_0_0_0},
	{"options", 2, 134218478, &t16__CustomAttributeCache_t16_m2555_Arg2_ParameterInfo, &t445_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m2555;
MethodInfo m2555_MI = 
{
	"SendMessage", (methodPointerType)&m2555, &t16_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44, t16_m2555_ParameterInfos, &t16__CustomAttributeCache_m2555, 134, 4096, 255, 3, false, false, 750, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t16_m2556_ParameterInfos[] = 
{
	{"componentType", 0, 134218479, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m2556;
MethodInfo m2556_MI = 
{
	"Internal_AddComponentWithType", (methodPointerType)&m2556, &t16_TI, &t28_0_0_0, RuntimeInvoker_t29_t29, t16_m2556_ParameterInfos, &t16__CustomAttributeCache_m2556, 129, 4096, 255, 1, false, false, 751, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t16_m2557_ParameterInfos[] = 
{
	{"componentType", 0, 134218480, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m2557;
MethodInfo m2557_MI = 
{
	"AddComponent", (methodPointerType)&m2557, &t16_TI, &t28_0_0_0, RuntimeInvoker_t29_t29, t16_m2557_ParameterInfos, &t16__CustomAttributeCache_m2557, 134, 0, 255, 1, false, false, 752, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType m2900_gp_0_0_0_0;
extern Il2CppGenericContainer m2900_IGC;
extern TypeInfo m2900_gp_T_0_TI;
static const Il2CppType* m2900_gp_T_0_TI_constraints[] = { 
&t28_0_0_0 , 
 NULL };
Il2CppGenericParamFull m2900_gp_T_0_TI_GenericParamFull = { { &m2900_IGC, 0}, {NULL, "T", 0, 0, m2900_gp_T_0_TI_constraints} };
static Il2CppGenericParamFull* m2900_IGPA[1] = 
{
	&m2900_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2900_MI;
Il2CppGenericContainer m2900_IGC = { { NULL, NULL }, NULL, &m2900_MI, 1, 1, m2900_IGPA };
extern Il2CppType m2900_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2900_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m2900_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m2900_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2900_MI = 
{
	"AddComponent", NULL, &t16_TI, &m2900_gp_0_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, true, false, 753, m2900_RGCTXData, (methodPointerType)NULL, &m2900_IGC};
extern Il2CppType t16_0_0_0;
extern Il2CppType t16_0_0_0;
extern CustomAttributesCache t16__CustomAttributeCache_t16_m2558_Arg0_ParameterInfo;
extern Il2CppType t7_0_0_0;
static ParameterInfo t16_m2558_ParameterInfos[] = 
{
	{"mono", 0, 134218481, &t16__CustomAttributeCache_t16_m2558_Arg0_ParameterInfo, &t16_0_0_0},
	{"name", 1, 134218482, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t16__CustomAttributeCache_m2558;
MethodInfo m2558_MI = 
{
	"Internal_CreateGameObject", (methodPointerType)&m2558, &t16_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t16_m2558_ParameterInfos, &t16__CustomAttributeCache_m2558, 145, 4096, 255, 2, false, false, 754, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t16_MIs[] =
{
	&m1777_MI,
	&m2553_MI,
	&m2896_MI,
	&m2897_MI,
	&m2898_MI,
	&m2899_MI,
	&m2554_MI,
	&m1363_MI,
	&m1781_MI,
	&m1782_MI,
	&m1393_MI,
	&m2555_MI,
	&m2556_MI,
	&m2557_MI,
	&m2900_MI,
	&m2558_MI,
	NULL
};
static MethodInfo* t16_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t16_CustomAttributesCacheGenerator_m2553(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t579 * tmp;
		tmp = (t579 *)il2cpp_codegen_object_new (&t579_TI);
		m2831(tmp, 0, &m2831_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m2896(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t615 * tmp;
		tmp = (t615 *)il2cpp_codegen_object_new (&t615_TI);
		m2895(tmp, &m2895_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m2554(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m1363(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m1781(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m1782(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m1393(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m2555(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_t16_m2555_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("null"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_t16_m2555_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m2556(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m2557(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t579 * tmp;
		tmp = (t579 *)il2cpp_codegen_object_new (&t579_TI);
		m2831(tmp, 0, &m2831_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t16_CustomAttributesCacheGenerator_m2558(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t539_TI;
#include "t539.h"
#include "t539MD.h"
extern MethodInfo m2739_MI;
void t16_CustomAttributesCacheGenerator_t16_m2558_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t539 * tmp;
		tmp = (t539 *)il2cpp_codegen_object_new (&t539_TI);
		m2739(tmp, &m2739_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t16__CustomAttributeCache_m2553 = {
2,
NULL,
&t16_CustomAttributesCacheGenerator_m2553
};
CustomAttributesCache t16__CustomAttributeCache_m2896 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m2896
};
CustomAttributesCache t16__CustomAttributeCache_m2554 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m2554
};
CustomAttributesCache t16__CustomAttributeCache_m1363 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m1363
};
CustomAttributesCache t16__CustomAttributeCache_m1781 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m1781
};
CustomAttributesCache t16__CustomAttributeCache_m1782 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m1782
};
CustomAttributesCache t16__CustomAttributeCache_m1393 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m1393
};
CustomAttributesCache t16__CustomAttributeCache_m2555 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m2555
};
CustomAttributesCache t16__CustomAttributeCache_t16_m2555_Arg1_ParameterInfo = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_t16_m2555_Arg1_ParameterInfo
};
CustomAttributesCache t16__CustomAttributeCache_t16_m2555_Arg2_ParameterInfo = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_t16_m2555_Arg2_ParameterInfo
};
CustomAttributesCache t16__CustomAttributeCache_m2556 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m2556
};
CustomAttributesCache t16__CustomAttributeCache_m2557 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m2557
};
CustomAttributesCache t16__CustomAttributeCache_m2558 = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_m2558
};
CustomAttributesCache t16__CustomAttributeCache_t16_m2558_Arg0_ParameterInfo = {
1,
NULL,
&t16_CustomAttributesCacheGenerator_t16_m2558_Arg0_ParameterInfo
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t16_1_0_0;
struct t16;
extern CustomAttributesCache t16__CustomAttributeCache_m2553;
extern CustomAttributesCache t16__CustomAttributeCache_m2896;
extern CustomAttributesCache t16__CustomAttributeCache_m2554;
extern CustomAttributesCache t16__CustomAttributeCache_m1363;
extern CustomAttributesCache t16__CustomAttributeCache_m1781;
extern CustomAttributesCache t16__CustomAttributeCache_m1782;
extern CustomAttributesCache t16__CustomAttributeCache_m1393;
extern CustomAttributesCache t16__CustomAttributeCache_m2555;
extern CustomAttributesCache t16__CustomAttributeCache_m2556;
extern CustomAttributesCache t16__CustomAttributeCache_m2557;
extern CustomAttributesCache t16__CustomAttributeCache_m2558;
TypeInfo t16_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GameObject", "UnityEngine", t16_MIs, t16_PIs, NULL, NULL, &t41_TI, NULL, NULL, &t16_TI, NULL, t16_VT, &EmptyCustomAttributesCache, &t16_TI, &t16_0_0_0, &t16_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t16), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 16, 3, 0, 0, 0, 4, 0, 0};
#include "t504.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t504_TI;
#include "t504MD.h"

#include "t25MD.h"
extern MethodInfo m1991_MI;
extern MethodInfo m1993_MI;


extern MethodInfo m2559_MI;
 void m2559 (t504 * __this, t25 * p0, MethodInfo* method){
	{
		__this->f1 = (-1);
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m2560_MI;
 t29 * m2560 (t504 * __this, MethodInfo* method){
	{
		t25 * L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		t25 * L_2 = m1991(L_0, L_1, &m1991_MI);
		return L_2;
	}
}
extern MethodInfo m2561_MI;
 bool m2561 (t504 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t25 * L_0 = (__this->f0);
		int32_t L_1 = m1993(L_0, &m1993_MI);
		V_0 = L_1;
		int32_t L_2 = (__this->f1);
		int32_t L_3 = ((int32_t)(L_2+1));
		V_1 = L_3;
		__this->f1 = L_3;
		return ((((int32_t)V_1) < ((int32_t)V_0))? 1 : 0);
	}
}
// Metadata Definition UnityEngine.Transform/Enumerator
extern Il2CppType t25_0_0_1;
FieldInfo t504_f0_FieldInfo = 
{
	"outer", &t25_0_0_1, &t504_TI, offsetof(t504, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t504_f1_FieldInfo = 
{
	"currentIndex", &t44_0_0_1, &t504_TI, offsetof(t504, f1), &EmptyCustomAttributesCache};
static FieldInfo* t504_FIs[] =
{
	&t504_f0_FieldInfo,
	&t504_f1_FieldInfo,
	NULL
};
static PropertyInfo t504____Current_PropertyInfo = 
{
	&t504_TI, "Current", &m2560_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t504_PIs[] =
{
	&t504____Current_PropertyInfo,
	NULL
};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t504_m2559_ParameterInfos[] = 
{
	{"outer", 0, 134218509, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2559_MI = 
{
	".ctor", (methodPointerType)&m2559, &t504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t504_m2559_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, false, 790, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2560_MI = 
{
	"get_Current", (methodPointerType)&m2560, &t504_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 791, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2561_MI = 
{
	"MoveNext", (methodPointerType)&m2561, &t504_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, false, 792, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t504_MIs[] =
{
	&m2559_MI,
	&m2560_MI,
	&m2561_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t504_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2560_MI,
	&m2561_MI,
};
extern TypeInfo t136_TI;
static TypeInfo* t504_ITIs[] = 
{
	&t136_TI,
};
static Il2CppInterfaceOffsetPair t504_IOs[] = 
{
	{ &t136_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t504_0_0_0;
extern Il2CppType t504_1_0_0;
struct t504;
extern TypeInfo t25_TI;
TypeInfo t504_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Enumerator", "", t504_MIs, t504_PIs, t504_FIs, NULL, &t29_TI, NULL, &t25_TI, &t504_TI, t504_ITIs, t504_VT, &EmptyCustomAttributesCache, &t504_TI, &t504_0_0_0, &t504_1_0_0, t504_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t504), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 1, 2, 0, 0, 6, 1, 1};
#ifndef _MSC_VER
#else
#endif

#include "t23.h"
#include "t362.h"
#include "t2.h"
#include "t396.h"
extern TypeInfo t2_TI;
#include "t23MD.h"
#include "t362MD.h"
#include "t293MD.h"
extern MethodInfo m2562_MI;
extern MethodInfo m2563_MI;
extern MethodInfo m2564_MI;
extern MethodInfo m2565_MI;
extern MethodInfo m1619_MI;
extern MethodInfo m1620_MI;
extern MethodInfo m1621_MI;
extern MethodInfo m2566_MI;
extern MethodInfo m2567_MI;
extern MethodInfo m2568_MI;
extern MethodInfo m2569_MI;
extern MethodInfo m2570_MI;
extern MethodInfo m2571_MI;
extern MethodInfo m1904_MI;
extern MethodInfo m2572_MI;
extern MethodInfo m2574_MI;
extern MethodInfo m2573_MI;
extern MethodInfo m2575_MI;
extern MethodInfo m2576_MI;


 void m2562 (t25 * __this, t23 * p0, MethodInfo* method){
	typedef void (*m2562_ftn) (t25 *, t23 *);
	static m2562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, p0);
}
 void m2563 (t25 * __this, t23 * p0, MethodInfo* method){
	typedef void (*m2563_ftn) (t25 *, t23 *);
	static m2563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m40_MI;
 t23  m40 (t25 * __this, MethodInfo* method){
	t23  V_0 = {0};
	{
		m2562(__this, (&V_0), &m2562_MI);
		return V_0;
	}
}
extern MethodInfo m69_MI;
 void m69 (t25 * __this, t23  p0, MethodInfo* method){
	{
		m2563(__this, (&p0), &m2563_MI);
		return;
	}
}
 void m2564 (t25 * __this, t23 * p0, MethodInfo* method){
	typedef void (*m2564_ftn) (t25 *, t23 *);
	static m2564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, p0);
}
 void m2565 (t25 * __this, t23 * p0, MethodInfo* method){
	typedef void (*m2565_ftn) (t25 *, t23 *);
	static m2565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m42_MI;
 t23  m42 (t25 * __this, MethodInfo* method){
	t23  V_0 = {0};
	{
		m2564(__this, (&V_0), &m2564_MI);
		return V_0;
	}
}
extern MethodInfo m44_MI;
 void m44 (t25 * __this, t23  p0, MethodInfo* method){
	{
		m2565(__this, (&p0), &m2565_MI);
		return;
	}
}
extern MethodInfo m1623_MI;
 t23  m1623 (t25 * __this, MethodInfo* method){
	{
		t362  L_0 = m1619(__this, &m1619_MI);
		t23  L_1 = m1620(NULL, &m1620_MI);
		t23  L_2 = m1621(NULL, L_0, L_1, &m1621_MI);
		return L_2;
	}
}
 void m2566 (t25 * __this, t362 * p0, MethodInfo* method){
	typedef void (*m2566_ftn) (t25 *, t362 *);
	static m2566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, p0);
}
 t362  m1619 (t25 * __this, MethodInfo* method){
	t362  V_0 = {0};
	{
		m2566(__this, (&V_0), &m2566_MI);
		return V_0;
	}
}
 void m2567 (t25 * __this, t362 * p0, MethodInfo* method){
	typedef void (*m2567_ftn) (t25 *, t362 *);
	static m2567_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2567_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, p0);
}
 void m2568 (t25 * __this, t362 * p0, MethodInfo* method){
	typedef void (*m2568_ftn) (t25 *, t362 *);
	static m2568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1789_MI;
 t362  m1789 (t25 * __this, MethodInfo* method){
	t362  V_0 = {0};
	{
		m2567(__this, (&V_0), &m2567_MI);
		return V_0;
	}
}
extern MethodInfo m1795_MI;
 void m1795 (t25 * __this, t362  p0, MethodInfo* method){
	{
		m2568(__this, (&p0), &m2568_MI);
		return;
	}
}
 void m2569 (t25 * __this, t23 * p0, MethodInfo* method){
	typedef void (*m2569_ftn) (t25 *, t23 *);
	static m2569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, p0);
}
 void m2570 (t25 * __this, t23 * p0, MethodInfo* method){
	typedef void (*m2570_ftn) (t25 *, t23 *);
	static m2570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1791_MI;
 t23  m1791 (t25 * __this, MethodInfo* method){
	t23  V_0 = {0};
	{
		m2569(__this, (&V_0), &m2569_MI);
		return V_0;
	}
}
extern MethodInfo m50_MI;
 void m50 (t25 * __this, t23  p0, MethodInfo* method){
	{
		m2570(__this, (&p0), &m2570_MI);
		return;
	}
}
extern MethodInfo m1365_MI;
 t25 * m1365 (t25 * __this, MethodInfo* method){
	{
		t25 * L_0 = m2571(__this, &m2571_MI);
		return L_0;
	}
}
extern MethodInfo m1819_MI;
 void m1819 (t25 * __this, t25 * p0, MethodInfo* method){
	{
		if (!((t2 *)IsInst(__this, InitializedTypeInfo(&t2_TI))))
		{
			goto IL_0016;
		}
	}
	{
		m1904(NULL, (t7*) &_stringLiteral145, __this, &m1904_MI);
	}

IL_0016:
	{
		m2572(__this, p0, &m2572_MI);
		return;
	}
}
 t25 * m2571 (t25 * __this, MethodInfo* method){
	typedef t25 * (*m2571_ftn) (t25 *);
	static m2571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
 void m2572 (t25 * __this, t25 * p0, MethodInfo* method){
	typedef void (*m2572_ftn) (t25 *, t25 *);
	static m2572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1779_MI;
 void m1779 (t25 * __this, t25 * p0, MethodInfo* method){
	{
		m2573(__this, p0, 1, &m2573_MI);
		return;
	}
}
 void m2573 (t25 * __this, t25 * p0, bool p1, MethodInfo* method){
	typedef void (*m2573_ftn) (t25 *, t25 *, bool);
	static m2573_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2573_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, p0, p1);
}
 void m2574 (t25 * __this, t396 * p0, MethodInfo* method){
	typedef void (*m2574_ftn) (t25 *, t396 *);
	static m2574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1859_MI;
 t396  m1859 (t25 * __this, MethodInfo* method){
	t396  V_0 = {0};
	{
		m2574(__this, (&V_0), &m2574_MI);
		return V_0;
	}
}
extern MethodInfo m1885_MI;
 t23  m1885 (t25 * __this, t23  p0, MethodInfo* method){
	{
		t23  L_0 = m2575(NULL, __this, (&p0), &m2575_MI);
		return L_0;
	}
}
 t23  m2575 (t29 * __this, t25 * p0, t23 * p1, MethodInfo* method){
	typedef t23  (*m2575_ftn) (t25 *, t23 *);
	static m2575_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2575_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(p0, p1);
}
extern MethodInfo m1744_MI;
 t23  m1744 (t25 * __this, t23  p0, MethodInfo* method){
	{
		t23  L_0 = m2576(NULL, __this, (&p0), &m2576_MI);
		return L_0;
	}
}
 t23  m2576 (t29 * __this, t25 * p0, t23 * p1, MethodInfo* method){
	typedef t23  (*m2576_ftn) (t25 *, t23 *);
	static m2576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(p0, p1);
}
 int32_t m1993 (t25 * __this, MethodInfo* method){
	typedef int32_t (*m1993_ftn) (t25 *);
	static m1993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1780_MI;
 void m1780 (t25 * __this, MethodInfo* method){
	typedef void (*m1780_ftn) (t25 *);
	static m1780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
extern MethodInfo m2577_MI;
 t29 * m2577 (t25 * __this, MethodInfo* method){
	{
		t504 * L_0 = (t504 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t504_TI));
		m2559(L_0, __this, &m2559_MI);
		return L_0;
	}
}
 t25 * m1991 (t25 * __this, int32_t p0, MethodInfo* method){
	typedef t25 * (*m1991_ftn) (t25 *, int32_t);
	static m1991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, p0);
}
// Metadata Definition UnityEngine.Transform
static PropertyInfo t25____position_PropertyInfo = 
{
	&t25_TI, "position", &m40_MI, &m69_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____localPosition_PropertyInfo = 
{
	&t25_TI, "localPosition", &m42_MI, &m44_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____forward_PropertyInfo = 
{
	&t25_TI, "forward", &m1623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____rotation_PropertyInfo = 
{
	&t25_TI, "rotation", &m1619_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____localRotation_PropertyInfo = 
{
	&t25_TI, "localRotation", &m1789_MI, &m1795_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____localScale_PropertyInfo = 
{
	&t25_TI, "localScale", &m1791_MI, &m50_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____parent_PropertyInfo = 
{
	&t25_TI, "parent", &m1365_MI, &m1819_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____parentInternal_PropertyInfo = 
{
	&t25_TI, "parentInternal", &m2571_MI, &m2572_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____worldToLocalMatrix_PropertyInfo = 
{
	&t25_TI, "worldToLocalMatrix", &m1859_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t25____childCount_PropertyInfo = 
{
	&t25_TI, "childCount", &m1993_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t25_PIs[] =
{
	&t25____position_PropertyInfo,
	&t25____localPosition_PropertyInfo,
	&t25____forward_PropertyInfo,
	&t25____rotation_PropertyInfo,
	&t25____localRotation_PropertyInfo,
	&t25____localScale_PropertyInfo,
	&t25____parent_PropertyInfo,
	&t25____parentInternal_PropertyInfo,
	&t25____worldToLocalMatrix_PropertyInfo,
	&t25____childCount_PropertyInfo,
	NULL
};
extern Il2CppType t23_1_0_2;
extern Il2CppType t23_1_0_0;
static ParameterInfo t25_m2562_ParameterInfos[] = 
{
	{"value", 0, 134218483, &EmptyCustomAttributesCache, &t23_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2562;
MethodInfo m2562_MI = 
{
	"INTERNAL_get_position", (methodPointerType)&m2562, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t587, t25_m2562_ParameterInfos, &t25__CustomAttributeCache_m2562, 129, 4096, 255, 1, false, false, 755, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_0;
static ParameterInfo t25_m2563_ParameterInfos[] = 
{
	{"value", 0, 134218484, &EmptyCustomAttributesCache, &t23_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2563;
MethodInfo m2563_MI = 
{
	"INTERNAL_set_position", (methodPointerType)&m2563, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t587, t25_m2563_ParameterInfos, &t25__CustomAttributeCache_m2563, 129, 4096, 255, 1, false, false, 756, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m40_MI = 
{
	"get_position", (methodPointerType)&m40, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 757, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern Il2CppType t23_0_0_0;
static ParameterInfo t25_m69_ParameterInfos[] = 
{
	{"value", 0, 134218485, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m69_MI = 
{
	"set_position", (methodPointerType)&m69, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t23, t25_m69_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 758, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_2;
static ParameterInfo t25_m2564_ParameterInfos[] = 
{
	{"value", 0, 134218486, &EmptyCustomAttributesCache, &t23_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2564;
MethodInfo m2564_MI = 
{
	"INTERNAL_get_localPosition", (methodPointerType)&m2564, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t587, t25_m2564_ParameterInfos, &t25__CustomAttributeCache_m2564, 129, 4096, 255, 1, false, false, 759, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_0;
static ParameterInfo t25_m2565_ParameterInfos[] = 
{
	{"value", 0, 134218487, &EmptyCustomAttributesCache, &t23_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2565;
MethodInfo m2565_MI = 
{
	"INTERNAL_set_localPosition", (methodPointerType)&m2565, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t587, t25_m2565_ParameterInfos, &t25__CustomAttributeCache_m2565, 129, 4096, 255, 1, false, false, 760, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m42_MI = 
{
	"get_localPosition", (methodPointerType)&m42, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 761, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
static ParameterInfo t25_m44_ParameterInfos[] = 
{
	{"value", 0, 134218488, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m44_MI = 
{
	"set_localPosition", (methodPointerType)&m44, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t23, t25_m44_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 762, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m1623_MI = 
{
	"get_forward", (methodPointerType)&m1623, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 763, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t362_1_0_2;
extern Il2CppType t362_1_0_0;
static ParameterInfo t25_m2566_ParameterInfos[] = 
{
	{"value", 0, 134218489, &EmptyCustomAttributesCache, &t362_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t596 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2566;
MethodInfo m2566_MI = 
{
	"INTERNAL_get_rotation", (methodPointerType)&m2566, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t596, t25_m2566_ParameterInfos, &t25__CustomAttributeCache_m2566, 129, 4096, 255, 1, false, false, 764, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t362_0_0_0;
extern void* RuntimeInvoker_t362 (MethodInfo* method, void* obj, void** args);
MethodInfo m1619_MI = 
{
	"get_rotation", (methodPointerType)&m1619, &t25_TI, &t362_0_0_0, RuntimeInvoker_t362, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 765, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t362_1_0_2;
static ParameterInfo t25_m2567_ParameterInfos[] = 
{
	{"value", 0, 134218490, &EmptyCustomAttributesCache, &t362_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t596 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2567;
MethodInfo m2567_MI = 
{
	"INTERNAL_get_localRotation", (methodPointerType)&m2567, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t596, t25_m2567_ParameterInfos, &t25__CustomAttributeCache_m2567, 129, 4096, 255, 1, false, false, 766, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t362_1_0_0;
static ParameterInfo t25_m2568_ParameterInfos[] = 
{
	{"value", 0, 134218491, &EmptyCustomAttributesCache, &t362_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t596 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2568;
MethodInfo m2568_MI = 
{
	"INTERNAL_set_localRotation", (methodPointerType)&m2568, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t596, t25_m2568_ParameterInfos, &t25__CustomAttributeCache_m2568, 129, 4096, 255, 1, false, false, 767, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t362_0_0_0;
extern void* RuntimeInvoker_t362 (MethodInfo* method, void* obj, void** args);
MethodInfo m1789_MI = 
{
	"get_localRotation", (methodPointerType)&m1789, &t25_TI, &t362_0_0_0, RuntimeInvoker_t362, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 768, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t362_0_0_0;
extern Il2CppType t362_0_0_0;
static ParameterInfo t25_m1795_ParameterInfos[] = 
{
	{"value", 0, 134218492, &EmptyCustomAttributesCache, &t362_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t362 (MethodInfo* method, void* obj, void** args);
MethodInfo m1795_MI = 
{
	"set_localRotation", (methodPointerType)&m1795, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t362, t25_m1795_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 769, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_2;
static ParameterInfo t25_m2569_ParameterInfos[] = 
{
	{"value", 0, 134218493, &EmptyCustomAttributesCache, &t23_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2569;
MethodInfo m2569_MI = 
{
	"INTERNAL_get_localScale", (methodPointerType)&m2569, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t587, t25_m2569_ParameterInfos, &t25__CustomAttributeCache_m2569, 129, 4096, 255, 1, false, false, 770, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_0;
static ParameterInfo t25_m2570_ParameterInfos[] = 
{
	{"value", 0, 134218494, &EmptyCustomAttributesCache, &t23_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2570;
MethodInfo m2570_MI = 
{
	"INTERNAL_set_localScale", (methodPointerType)&m2570, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t587, t25_m2570_ParameterInfos, &t25__CustomAttributeCache_m2570, 129, 4096, 255, 1, false, false, 771, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m1791_MI = 
{
	"get_localScale", (methodPointerType)&m1791, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 772, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
static ParameterInfo t25_m50_ParameterInfos[] = 
{
	{"value", 0, 134218495, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m50_MI = 
{
	"set_localScale", (methodPointerType)&m50, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t23, t25_m50_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 773, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1365_MI = 
{
	"get_parent", (methodPointerType)&m1365, &t25_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 774, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
static ParameterInfo t25_m1819_ParameterInfos[] = 
{
	{"value", 0, 134218496, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1819_MI = 
{
	"set_parent", (methodPointerType)&m1819, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t25_m1819_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 775, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2571;
MethodInfo m2571_MI = 
{
	"get_parentInternal", (methodPointerType)&m2571, &t25_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &t25__CustomAttributeCache_m2571, 2179, 4096, 255, 0, false, false, 776, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
static ParameterInfo t25_m2572_ParameterInfos[] = 
{
	{"value", 0, 134218497, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2572;
MethodInfo m2572_MI = 
{
	"set_parentInternal", (methodPointerType)&m2572, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t25_m2572_ParameterInfos, &t25__CustomAttributeCache_m2572, 2179, 4096, 255, 1, false, false, 777, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
static ParameterInfo t25_m1779_ParameterInfos[] = 
{
	{"parent", 0, 134218498, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1779_MI = 
{
	"SetParent", (methodPointerType)&m1779, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t25_m1779_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 778, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t25_m2573_ParameterInfos[] = 
{
	{"parent", 0, 134218499, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"worldPositionStays", 1, 134218500, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2573;
MethodInfo m2573_MI = 
{
	"SetParent", (methodPointerType)&m2573, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t25_m2573_ParameterInfos, &t25__CustomAttributeCache_m2573, 134, 4096, 255, 2, false, false, 779, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t396_1_0_2;
extern Il2CppType t396_1_0_0;
static ParameterInfo t25_m2574_ParameterInfos[] = 
{
	{"value", 0, 134218501, &EmptyCustomAttributesCache, &t396_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t597 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2574;
MethodInfo m2574_MI = 
{
	"INTERNAL_get_worldToLocalMatrix", (methodPointerType)&m2574, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21_t597, t25_m2574_ParameterInfos, &t25__CustomAttributeCache_m2574, 129, 4096, 255, 1, false, false, 780, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t396_0_0_0;
extern void* RuntimeInvoker_t396 (MethodInfo* method, void* obj, void** args);
MethodInfo m1859_MI = 
{
	"get_worldToLocalMatrix", (methodPointerType)&m1859, &t25_TI, &t396_0_0_0, RuntimeInvoker_t396, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 781, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
static ParameterInfo t25_m1885_ParameterInfos[] = 
{
	{"position", 0, 134218502, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m1885_MI = 
{
	"TransformPoint", (methodPointerType)&m1885, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23_t23, t25_m1885_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 782, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern Il2CppType t23_1_0_0;
static ParameterInfo t25_m2575_ParameterInfos[] = 
{
	{"self", 0, 134218503, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"position", 1, 134218504, &EmptyCustomAttributesCache, &t23_1_0_0},
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23_t29_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2575;
MethodInfo m2575_MI = 
{
	"INTERNAL_CALL_TransformPoint", (methodPointerType)&m2575, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23_t29_t587, t25_m2575_ParameterInfos, &t25__CustomAttributeCache_m2575, 145, 4096, 255, 2, false, false, 783, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
static ParameterInfo t25_m1744_ParameterInfos[] = 
{
	{"position", 0, 134218505, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m1744_MI = 
{
	"InverseTransformPoint", (methodPointerType)&m1744, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23_t23, t25_m1744_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 784, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern Il2CppType t23_1_0_0;
static ParameterInfo t25_m2576_ParameterInfos[] = 
{
	{"self", 0, 134218506, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"position", 1, 134218507, &EmptyCustomAttributesCache, &t23_1_0_0},
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23_t29_t587 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m2576;
MethodInfo m2576_MI = 
{
	"INTERNAL_CALL_InverseTransformPoint", (methodPointerType)&m2576, &t25_TI, &t23_0_0_0, RuntimeInvoker_t23_t29_t587, t25_m2576_ParameterInfos, &t25__CustomAttributeCache_m2576, 145, 4096, 255, 2, false, false, 785, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m1993;
MethodInfo m1993_MI = 
{
	"get_childCount", (methodPointerType)&m1993, &t25_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t25__CustomAttributeCache_m1993, 2182, 4096, 255, 0, false, false, 786, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m1780;
MethodInfo m1780_MI = 
{
	"SetAsFirstSibling", (methodPointerType)&m1780, &t25_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t25__CustomAttributeCache_m1780, 134, 4096, 255, 0, false, false, 787, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2577_MI = 
{
	"GetEnumerator", (methodPointerType)&m2577, &t25_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 4, 0, false, false, 788, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t25_m1991_ParameterInfos[] = 
{
	{"index", 0, 134218508, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t25__CustomAttributeCache_m1991;
MethodInfo m1991_MI = 
{
	"GetChild", (methodPointerType)&m1991, &t25_TI, &t25_0_0_0, RuntimeInvoker_t29_t44, t25_m1991_ParameterInfos, &t25__CustomAttributeCache_m1991, 134, 4096, 255, 1, false, false, 789, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t25_MIs[] =
{
	&m2562_MI,
	&m2563_MI,
	&m40_MI,
	&m69_MI,
	&m2564_MI,
	&m2565_MI,
	&m42_MI,
	&m44_MI,
	&m1623_MI,
	&m2566_MI,
	&m1619_MI,
	&m2567_MI,
	&m2568_MI,
	&m1789_MI,
	&m1795_MI,
	&m2569_MI,
	&m2570_MI,
	&m1791_MI,
	&m50_MI,
	&m1365_MI,
	&m1819_MI,
	&m2571_MI,
	&m2572_MI,
	&m1779_MI,
	&m2573_MI,
	&m2574_MI,
	&m1859_MI,
	&m1885_MI,
	&m2575_MI,
	&m1744_MI,
	&m2576_MI,
	&m1993_MI,
	&m1780_MI,
	&m2577_MI,
	&m1991_MI,
	NULL
};
extern TypeInfo t504_TI;
static TypeInfo* t25_TI__nestedTypes[2] =
{
	&t504_TI,
	NULL
};
static MethodInfo* t25_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m2577_MI,
};
extern TypeInfo t603_TI;
static TypeInfo* t25_ITIs[] = 
{
	&t603_TI,
};
static Il2CppInterfaceOffsetPair t25_IOs[] = 
{
	{ &t603_TI, 4},
};
void t25_CustomAttributesCacheGenerator_m2562(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2563(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2564(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2565(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2566(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2567(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2568(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2569(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2570(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2571(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2572(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2573(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2574(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2575(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m2576(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m1993(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m1780(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t25_CustomAttributesCacheGenerator_m1991(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t25__CustomAttributeCache_m2562 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2562
};
CustomAttributesCache t25__CustomAttributeCache_m2563 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2563
};
CustomAttributesCache t25__CustomAttributeCache_m2564 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2564
};
CustomAttributesCache t25__CustomAttributeCache_m2565 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2565
};
CustomAttributesCache t25__CustomAttributeCache_m2566 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2566
};
CustomAttributesCache t25__CustomAttributeCache_m2567 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2567
};
CustomAttributesCache t25__CustomAttributeCache_m2568 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2568
};
CustomAttributesCache t25__CustomAttributeCache_m2569 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2569
};
CustomAttributesCache t25__CustomAttributeCache_m2570 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2570
};
CustomAttributesCache t25__CustomAttributeCache_m2571 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2571
};
CustomAttributesCache t25__CustomAttributeCache_m2572 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2572
};
CustomAttributesCache t25__CustomAttributeCache_m2573 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2573
};
CustomAttributesCache t25__CustomAttributeCache_m2574 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2574
};
CustomAttributesCache t25__CustomAttributeCache_m2575 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2575
};
CustomAttributesCache t25__CustomAttributeCache_m2576 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m2576
};
CustomAttributesCache t25__CustomAttributeCache_m1993 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m1993
};
CustomAttributesCache t25__CustomAttributeCache_m1780 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m1780
};
CustomAttributesCache t25__CustomAttributeCache_m1991 = {
1,
NULL,
&t25_CustomAttributesCacheGenerator_m1991
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t25_1_0_0;
struct t25;
extern CustomAttributesCache t25__CustomAttributeCache_m2562;
extern CustomAttributesCache t25__CustomAttributeCache_m2563;
extern CustomAttributesCache t25__CustomAttributeCache_m2564;
extern CustomAttributesCache t25__CustomAttributeCache_m2565;
extern CustomAttributesCache t25__CustomAttributeCache_m2566;
extern CustomAttributesCache t25__CustomAttributeCache_m2567;
extern CustomAttributesCache t25__CustomAttributeCache_m2568;
extern CustomAttributesCache t25__CustomAttributeCache_m2569;
extern CustomAttributesCache t25__CustomAttributeCache_m2570;
extern CustomAttributesCache t25__CustomAttributeCache_m2571;
extern CustomAttributesCache t25__CustomAttributeCache_m2572;
extern CustomAttributesCache t25__CustomAttributeCache_m2573;
extern CustomAttributesCache t25__CustomAttributeCache_m2574;
extern CustomAttributesCache t25__CustomAttributeCache_m2575;
extern CustomAttributesCache t25__CustomAttributeCache_m2576;
extern CustomAttributesCache t25__CustomAttributeCache_m1993;
extern CustomAttributesCache t25__CustomAttributeCache_m1780;
extern CustomAttributesCache t25__CustomAttributeCache_m1991;
TypeInfo t25_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Transform", "UnityEngine", t25_MIs, t25_PIs, NULL, NULL, &t28_TI, t25_TI__nestedTypes, NULL, &t25_TI, t25_ITIs, t25_VT, &EmptyCustomAttributesCache, &t25_TI, &t25_0_0_0, &t25_1_0_0, t25_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t25), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 35, 10, 0, 0, 1, 5, 1, 1};
#include "t328.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t328_TI;
#include "t328MD.h"



extern MethodInfo m2578_MI;
 float m2578 (t29 * __this, MethodInfo* method){
	typedef float (*m2578_ftn) ();
	static m2578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
extern MethodInfo m1445_MI;
 float m1445 (t29 * __this, MethodInfo* method){
	typedef float (*m1445_ftn) ();
	static m1445_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1445_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
extern MethodInfo m1515_MI;
 float m1515 (t29 * __this, MethodInfo* method){
	typedef float (*m1515_ftn) ();
	static m1515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// Metadata Definition UnityEngine.Time
static PropertyInfo t328____deltaTime_PropertyInfo = 
{
	&t328_TI, "deltaTime", &m2578_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t328____unscaledTime_PropertyInfo = 
{
	&t328_TI, "unscaledTime", &m1445_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t328____unscaledDeltaTime_PropertyInfo = 
{
	&t328_TI, "unscaledDeltaTime", &m1515_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t328_PIs[] =
{
	&t328____deltaTime_PropertyInfo,
	&t328____unscaledTime_PropertyInfo,
	&t328____unscaledDeltaTime_PropertyInfo,
	NULL
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t328__CustomAttributeCache_m2578;
MethodInfo m2578_MI = 
{
	"get_deltaTime", (methodPointerType)&m2578, &t328_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &t328__CustomAttributeCache_m2578, 2198, 4096, 255, 0, false, false, 793, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t328__CustomAttributeCache_m1445;
MethodInfo m1445_MI = 
{
	"get_unscaledTime", (methodPointerType)&m1445, &t328_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &t328__CustomAttributeCache_m1445, 2198, 4096, 255, 0, false, false, 794, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t328__CustomAttributeCache_m1515;
MethodInfo m1515_MI = 
{
	"get_unscaledDeltaTime", (methodPointerType)&m1515, &t328_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &t328__CustomAttributeCache_m1515, 2198, 4096, 255, 0, false, false, 795, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t328_MIs[] =
{
	&m2578_MI,
	&m1445_MI,
	&m1515_MI,
	NULL
};
static MethodInfo* t328_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t328_CustomAttributesCacheGenerator_m2578(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t328_CustomAttributesCacheGenerator_m1445(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t328_CustomAttributesCacheGenerator_m1515(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t328__CustomAttributeCache_m2578 = {
1,
NULL,
&t328_CustomAttributesCacheGenerator_m2578
};
CustomAttributesCache t328__CustomAttributeCache_m1445 = {
1,
NULL,
&t328_CustomAttributesCacheGenerator_m1445
};
CustomAttributesCache t328__CustomAttributeCache_m1515 = {
1,
NULL,
&t328_CustomAttributesCacheGenerator_m1515
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t328_0_0_0;
extern Il2CppType t328_1_0_0;
struct t328;
extern CustomAttributesCache t328__CustomAttributeCache_m2578;
extern CustomAttributesCache t328__CustomAttributeCache_m1445;
extern CustomAttributesCache t328__CustomAttributeCache_m1515;
TypeInfo t328_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Time", "UnityEngine", t328_MIs, t328_PIs, NULL, NULL, &t29_TI, NULL, NULL, &t328_TI, NULL, t328_VT, &EmptyCustomAttributesCache, &t328_TI, &t328_0_0_0, &t328_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t328), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 3, 0, 0, 0, 4, 0, 0};
#include "t448.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t448_TI;
#include "t448MD.h"



extern MethodInfo m2579_MI;
 void m2579 (t448 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
void t448_marshal(const t448& unmarshaled, t448_marshaled& marshaled)
{
}
void t448_marshal_back(const t448_marshaled& marshaled, t448& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
void t448_marshal_cleanup(t448_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.YieldInstruction
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2579_MI = 
{
	".ctor", (methodPointerType)&m2579, &t448_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 796, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t448_MIs[] =
{
	&m2579_MI,
	NULL
};
static MethodInfo* t448_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t448_0_0_0;
extern Il2CppType t448_1_0_0;
struct t448;
TypeInfo t448_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "YieldInstruction", "UnityEngine", t448_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t448_TI, NULL, t448_VT, &EmptyCustomAttributesCache, &t448_TI, &t448_0_0_0, &t448_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t448_marshal, (methodPointerType)t448_marshal_back, (methodPointerType)t448_marshal_cleanup, sizeof (t448), 0, sizeof(t448_marshaled), 0, 0, -1, 1048585, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 0};
#include "t505.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t505_TI;
#include "t505MD.h"

#include "t132.h"


extern MethodInfo m2580_MI;
 t23  m2580 (t505 * __this, MethodInfo* method){
	{
		t23  L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2581_MI;
 void m2581 (t505 * __this, t23  p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m2582_MI;
 t23  m2582 (t505 * __this, MethodInfo* method){
	{
		t23  L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2583_MI;
 void m2583 (t505 * __this, t23  p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m2584_MI;
 float m2584 (t505 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m2585_MI;
 void m2585 (t505 * __this, float p0, MethodInfo* method){
	{
		__this->f5 = p0;
		return;
	}
}
extern MethodInfo m2586_MI;
 float m2586 (t505 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m2587_MI;
 void m2587 (t505 * __this, float p0, MethodInfo* method){
	{
		__this->f6 = p0;
		return;
	}
}
extern MethodInfo m2588_MI;
 float m2588 (t505 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2589_MI;
 void m2589 (t505 * __this, float p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
extern MethodInfo m2590_MI;
 float m2590 (t505 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m2591_MI;
 void m2591 (t505 * __this, float p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m2592_MI;
 float m2592 (t505 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m2593_MI;
 void m2593 (t505 * __this, float p0, MethodInfo* method){
	{
		__this->f4 = p0;
		return;
	}
}
extern MethodInfo m2594_MI;
 t132  m2594 (t505 * __this, MethodInfo* method){
	{
		t132  L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m2595_MI;
 void m2595 (t505 * __this, t132  p0, MethodInfo* method){
	{
		__this->f7 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.Particle
extern Il2CppType t23_0_0_1;
FieldInfo t505_f0_FieldInfo = 
{
	"m_Position", &t23_0_0_1, &t505_TI, offsetof(t505, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_1;
FieldInfo t505_f1_FieldInfo = 
{
	"m_Velocity", &t23_0_0_1, &t505_TI, offsetof(t505, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t505_f2_FieldInfo = 
{
	"m_Size", &t22_0_0_1, &t505_TI, offsetof(t505, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t505_f3_FieldInfo = 
{
	"m_Rotation", &t22_0_0_1, &t505_TI, offsetof(t505, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t505_f4_FieldInfo = 
{
	"m_AngularVelocity", &t22_0_0_1, &t505_TI, offsetof(t505, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t505_f5_FieldInfo = 
{
	"m_Energy", &t22_0_0_1, &t505_TI, offsetof(t505, f5) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t505_f6_FieldInfo = 
{
	"m_StartEnergy", &t22_0_0_1, &t505_TI, offsetof(t505, f6) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t132_0_0_1;
FieldInfo t505_f7_FieldInfo = 
{
	"m_Color", &t132_0_0_1, &t505_TI, offsetof(t505, f7) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t505_FIs[] =
{
	&t505_f0_FieldInfo,
	&t505_f1_FieldInfo,
	&t505_f2_FieldInfo,
	&t505_f3_FieldInfo,
	&t505_f4_FieldInfo,
	&t505_f5_FieldInfo,
	&t505_f6_FieldInfo,
	&t505_f7_FieldInfo,
	NULL
};
static PropertyInfo t505____position_PropertyInfo = 
{
	&t505_TI, "position", &m2580_MI, &m2581_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____velocity_PropertyInfo = 
{
	&t505_TI, "velocity", &m2582_MI, &m2583_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____energy_PropertyInfo = 
{
	&t505_TI, "energy", &m2584_MI, &m2585_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____startEnergy_PropertyInfo = 
{
	&t505_TI, "startEnergy", &m2586_MI, &m2587_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____size_PropertyInfo = 
{
	&t505_TI, "size", &m2588_MI, &m2589_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____rotation_PropertyInfo = 
{
	&t505_TI, "rotation", &m2590_MI, &m2591_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____angularVelocity_PropertyInfo = 
{
	&t505_TI, "angularVelocity", &m2592_MI, &m2593_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t505____color_PropertyInfo = 
{
	&t505_TI, "color", &m2594_MI, &m2595_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t505_PIs[] =
{
	&t505____position_PropertyInfo,
	&t505____velocity_PropertyInfo,
	&t505____energy_PropertyInfo,
	&t505____startEnergy_PropertyInfo,
	&t505____size_PropertyInfo,
	&t505____rotation_PropertyInfo,
	&t505____angularVelocity_PropertyInfo,
	&t505____color_PropertyInfo,
	NULL
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m2580_MI = 
{
	"get_position", (methodPointerType)&m2580, &t505_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 797, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
static ParameterInfo t505_m2581_ParameterInfos[] = 
{
	{"value", 0, 134218510, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m2581_MI = 
{
	"set_position", (methodPointerType)&m2581, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t23, t505_m2581_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 798, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m2582_MI = 
{
	"get_velocity", (methodPointerType)&m2582, &t505_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 799, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
static ParameterInfo t505_m2583_ParameterInfos[] = 
{
	{"value", 0, 134218511, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m2583_MI = 
{
	"set_velocity", (methodPointerType)&m2583, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t23, t505_m2583_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 800, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2584_MI = 
{
	"get_energy", (methodPointerType)&m2584, &t505_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 801, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t505_m2585_ParameterInfos[] = 
{
	{"value", 0, 134218512, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2585_MI = 
{
	"set_energy", (methodPointerType)&m2585, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t505_m2585_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 802, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2586_MI = 
{
	"get_startEnergy", (methodPointerType)&m2586, &t505_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 803, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t505_m2587_ParameterInfos[] = 
{
	{"value", 0, 134218513, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2587_MI = 
{
	"set_startEnergy", (methodPointerType)&m2587, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t505_m2587_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 804, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2588_MI = 
{
	"get_size", (methodPointerType)&m2588, &t505_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 805, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t505_m2589_ParameterInfos[] = 
{
	{"value", 0, 134218514, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2589_MI = 
{
	"set_size", (methodPointerType)&m2589, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t505_m2589_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 806, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2590_MI = 
{
	"get_rotation", (methodPointerType)&m2590, &t505_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 807, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t505_m2591_ParameterInfos[] = 
{
	{"value", 0, 134218515, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2591_MI = 
{
	"set_rotation", (methodPointerType)&m2591, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t505_m2591_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 808, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2592_MI = 
{
	"get_angularVelocity", (methodPointerType)&m2592, &t505_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 809, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t505_m2593_ParameterInfos[] = 
{
	{"value", 0, 134218516, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2593_MI = 
{
	"set_angularVelocity", (methodPointerType)&m2593, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t505_m2593_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 810, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t132_0_0_0;
extern void* RuntimeInvoker_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m2594_MI = 
{
	"get_color", (methodPointerType)&m2594, &t505_TI, &t132_0_0_0, RuntimeInvoker_t132, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 811, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t132_0_0_0;
extern Il2CppType t132_0_0_0;
static ParameterInfo t505_m2595_ParameterInfos[] = 
{
	{"value", 0, 134218517, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m2595_MI = 
{
	"set_color", (methodPointerType)&m2595, &t505_TI, &t21_0_0_0, RuntimeInvoker_t21_t132, t505_m2595_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 812, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t505_MIs[] =
{
	&m2580_MI,
	&m2581_MI,
	&m2582_MI,
	&m2583_MI,
	&m2584_MI,
	&m2585_MI,
	&m2586_MI,
	&m2587_MI,
	&m2588_MI,
	&m2589_MI,
	&m2590_MI,
	&m2591_MI,
	&m2592_MI,
	&m2593_MI,
	&m2594_MI,
	&m2595_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t505_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t505_0_0_0;
extern Il2CppType t505_1_0_0;
extern TypeInfo t110_TI;
TypeInfo t505_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Particle", "UnityEngine", t505_MIs, t505_PIs, t505_FIs, NULL, &t110_TI, NULL, NULL, &t505_TI, NULL, t505_VT, &EmptyCustomAttributesCache, &t505_TI, &t505_0_0_0, &t505_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t505)+ sizeof (Il2CppObject), 0, sizeof(t505 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 16, 8, 8, 0, 0, 4, 0, 0};
#include "t337.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t337_TI;
#include "t337MD.h"

#include "t127.h"
#include "t329.h"
#include "UnityEngine_ArrayTypes.h"
#include "t329MD.h"
extern MethodInfo m2597_MI;
extern MethodInfo m2596_MI;
extern MethodInfo m1467_MI;
extern MethodInfo m1468_MI;
extern MethodInfo m2598_MI;
extern MethodInfo m2599_MI;
extern MethodInfo m2600_MI;


 bool m2596 (t29 * __this, t23  p0, t23  p1, t127 * p2, float p3, int32_t p4, MethodInfo* method){
	{
		bool L_0 = m2597(NULL, (&p0), (&p1), p2, p3, p4, &m2597_MI);
		return L_0;
	}
}
 bool m2597 (t29 * __this, t23 * p0, t23 * p1, t127 * p2, float p3, int32_t p4, MethodInfo* method){
	typedef bool (*m2597_ftn) (t23 *, t23 *, t127 *, float, int32_t);
	static m2597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)");
	return _il2cpp_icall_func(p0, p1, p2, p3, p4);
}
 bool m2598 (t29 * __this, t23  p0, t23  p1, t127 * p2, float p3, int32_t p4, MethodInfo* method){
	{
		bool L_0 = m2596(NULL, p0, p1, p2, p3, p4, &m2596_MI);
		return L_0;
	}
}
extern MethodInfo m1614_MI;
 bool m1614 (t29 * __this, t329  p0, t127 * p1, float p2, int32_t p3, MethodInfo* method){
	{
		t23  L_0 = m1467((&p0), &m1467_MI);
		t23  L_1 = m1468((&p0), &m1468_MI);
		bool L_2 = m2598(NULL, L_0, L_1, p1, p2, p3, &m2598_MI);
		return L_2;
	}
}
extern MethodInfo m1483_MI;
 t339* m1483 (t29 * __this, t329  p0, float p1, int32_t p2, MethodInfo* method){
	{
		t23  L_0 = m1467((&p0), &m1467_MI);
		t23  L_1 = m1468((&p0), &m1468_MI);
		t339* L_2 = m2599(NULL, L_0, L_1, p1, p2, &m2599_MI);
		return L_2;
	}
}
 t339* m2599 (t29 * __this, t23  p0, t23  p1, float p2, int32_t p3, MethodInfo* method){
	{
		t339* L_0 = m2600(NULL, (&p0), (&p1), p2, p3, &m2600_MI);
		return L_0;
	}
}
 t339* m2600 (t29 * __this, t23 * p0, t23 * p1, float p2, int32_t p3, MethodInfo* method){
	typedef t339* (*m2600_ftn) (t23 *, t23 *, float, int32_t);
	static m2600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)");
	return _il2cpp_icall_func(p0, p1, p2, p3);
}
// Metadata Definition UnityEngine.Physics
extern Il2CppType t23_0_0_0;
extern Il2CppType t23_0_0_0;
extern Il2CppType t127_1_0_2;
extern Il2CppType t127_1_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t337_m2596_ParameterInfos[] = 
{
	{"origin", 0, 134218518, &EmptyCustomAttributesCache, &t23_0_0_0},
	{"direction", 1, 134218519, &EmptyCustomAttributesCache, &t23_0_0_0},
	{"hitInfo", 2, 134218520, &EmptyCustomAttributesCache, &t127_1_0_2},
	{"maxDistance", 3, 134218521, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layermask", 4, 134218522, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t23_t23_t624_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2596_MI = 
{
	"Internal_Raycast", (methodPointerType)&m2596, &t337_TI, &t40_0_0_0, RuntimeInvoker_t40_t23_t23_t624_t22_t44, t337_m2596_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 5, false, false, 813, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_0;
extern Il2CppType t23_1_0_0;
extern Il2CppType t127_1_0_2;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t337_m2597_ParameterInfos[] = 
{
	{"origin", 0, 134218523, &EmptyCustomAttributesCache, &t23_1_0_0},
	{"direction", 1, 134218524, &EmptyCustomAttributesCache, &t23_1_0_0},
	{"hitInfo", 2, 134218525, &EmptyCustomAttributesCache, &t127_1_0_2},
	{"maxDistance", 3, 134218526, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layermask", 4, 134218527, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t587_t587_t624_t22_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t337__CustomAttributeCache_m2597;
MethodInfo m2597_MI = 
{
	"INTERNAL_CALL_Internal_Raycast", (methodPointerType)&m2597, &t337_TI, &t40_0_0_0, RuntimeInvoker_t40_t587_t587_t624_t22_t44, t337_m2597_ParameterInfos, &t337__CustomAttributeCache_m2597, 145, 4096, 255, 5, false, false, 814, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern Il2CppType t23_0_0_0;
extern Il2CppType t127_1_0_2;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m2598_Arg3_ParameterInfo;
extern Il2CppType t44_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m2598_Arg4_ParameterInfo;
static ParameterInfo t337_m2598_ParameterInfos[] = 
{
	{"origin", 0, 134218528, &EmptyCustomAttributesCache, &t23_0_0_0},
	{"direction", 1, 134218529, &EmptyCustomAttributesCache, &t23_0_0_0},
	{"hitInfo", 2, 134218530, &EmptyCustomAttributesCache, &t127_1_0_2},
	{"maxDistance", 3, 134218531, &t337__CustomAttributeCache_t337_m2598_Arg3_ParameterInfo, &t22_0_0_0},
	{"layerMask", 4, 134218532, &t337__CustomAttributeCache_t337_m2598_Arg4_ParameterInfo, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t23_t23_t624_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2598_MI = 
{
	"Raycast", (methodPointerType)&m2598, &t337_TI, &t40_0_0_0, RuntimeInvoker_t40_t23_t23_t624_t22_t44, t337_m2598_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 5, false, false, 815, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t329_0_0_0;
extern Il2CppType t329_0_0_0;
extern Il2CppType t127_1_0_2;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m1614_Arg2_ParameterInfo;
extern Il2CppType t44_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m1614_Arg3_ParameterInfo;
static ParameterInfo t337_m1614_ParameterInfos[] = 
{
	{"ray", 0, 134218533, &EmptyCustomAttributesCache, &t329_0_0_0},
	{"hitInfo", 1, 134218534, &EmptyCustomAttributesCache, &t127_1_0_2},
	{"maxDistance", 2, 134218535, &t337__CustomAttributeCache_t337_m1614_Arg2_ParameterInfo, &t22_0_0_0},
	{"layerMask", 3, 134218536, &t337__CustomAttributeCache_t337_m1614_Arg3_ParameterInfo, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t329_t624_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1614_MI = 
{
	"Raycast", (methodPointerType)&m1614, &t337_TI, &t40_0_0_0, RuntimeInvoker_t40_t329_t624_t22_t44, t337_m1614_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 816, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t329_0_0_0;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m1483_Arg1_ParameterInfo;
extern Il2CppType t44_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m1483_Arg2_ParameterInfo;
static ParameterInfo t337_m1483_ParameterInfos[] = 
{
	{"ray", 0, 134218537, &EmptyCustomAttributesCache, &t329_0_0_0},
	{"maxDistance", 1, 134218538, &t337__CustomAttributeCache_t337_m1483_Arg1_ParameterInfo, &t22_0_0_0},
	{"layerMask", 2, 134218539, &t337__CustomAttributeCache_t337_m1483_Arg2_ParameterInfo, &t44_0_0_0},
};
extern Il2CppType t339_0_0_0;
extern void* RuntimeInvoker_t29_t329_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1483_MI = 
{
	"RaycastAll", (methodPointerType)&m1483, &t337_TI, &t339_0_0_0, RuntimeInvoker_t29_t329_t22_t44, t337_m1483_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 817, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern Il2CppType t23_0_0_0;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m2599_Arg2_ParameterInfo;
extern Il2CppType t44_0_0_0;
extern CustomAttributesCache t337__CustomAttributeCache_t337_m2599_Arg3_ParameterInfo;
static ParameterInfo t337_m2599_ParameterInfos[] = 
{
	{"origin", 0, 134218540, &EmptyCustomAttributesCache, &t23_0_0_0},
	{"direction", 1, 134218541, &EmptyCustomAttributesCache, &t23_0_0_0},
	{"maxDistance", 2, 134218542, &t337__CustomAttributeCache_t337_m2599_Arg2_ParameterInfo, &t22_0_0_0},
	{"layermask", 3, 134218543, &t337__CustomAttributeCache_t337_m2599_Arg3_ParameterInfo, &t44_0_0_0},
};
extern Il2CppType t339_0_0_0;
extern void* RuntimeInvoker_t29_t23_t23_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2599_MI = 
{
	"RaycastAll", (methodPointerType)&m2599, &t337_TI, &t339_0_0_0, RuntimeInvoker_t29_t23_t23_t22_t44, t337_m2599_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 818, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_1_0_0;
extern Il2CppType t23_1_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t337_m2600_ParameterInfos[] = 
{
	{"origin", 0, 134218544, &EmptyCustomAttributesCache, &t23_1_0_0},
	{"direction", 1, 134218545, &EmptyCustomAttributesCache, &t23_1_0_0},
	{"maxDistance", 2, 134218546, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layermask", 3, 134218547, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t339_0_0_0;
extern void* RuntimeInvoker_t29_t587_t587_t22_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t337__CustomAttributeCache_m2600;
MethodInfo m2600_MI = 
{
	"INTERNAL_CALL_RaycastAll", (methodPointerType)&m2600, &t337_TI, &t339_0_0_0, RuntimeInvoker_t29_t587_t587_t22_t44, t337_m2600_ParameterInfos, &t337__CustomAttributeCache_m2600, 145, 4096, 255, 4, false, false, 819, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t337_MIs[] =
{
	&m2596_MI,
	&m2597_MI,
	&m2598_MI,
	&m1614_MI,
	&m1483_MI,
	&m2599_MI,
	&m2600_MI,
	NULL
};
static MethodInfo* t337_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t337_CustomAttributesCacheGenerator_m2597(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m2598_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m2598_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m1614_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m1614_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m1483_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m1483_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m2599_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_t337_m2599_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t337_CustomAttributesCacheGenerator_m2600(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t337__CustomAttributeCache_m2597 = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_m2597
};
CustomAttributesCache t337__CustomAttributeCache_t337_m2598_Arg3_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m2598_Arg3_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m2598_Arg4_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m2598_Arg4_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m1614_Arg2_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m1614_Arg2_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m1614_Arg3_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m1614_Arg3_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m1483_Arg1_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m1483_Arg1_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m1483_Arg2_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m1483_Arg2_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m2599_Arg2_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m2599_Arg2_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_t337_m2599_Arg3_ParameterInfo = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_t337_m2599_Arg3_ParameterInfo
};
CustomAttributesCache t337__CustomAttributeCache_m2600 = {
1,
NULL,
&t337_CustomAttributesCacheGenerator_m2600
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t337_0_0_0;
extern Il2CppType t337_1_0_0;
struct t337;
extern CustomAttributesCache t337__CustomAttributeCache_m2597;
extern CustomAttributesCache t337__CustomAttributeCache_m2600;
TypeInfo t337_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Physics", "UnityEngine", t337_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t337_TI, NULL, t337_VT, &EmptyCustomAttributesCache, &t337_TI, &t337_0_0_0, &t337_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t337), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 0, 0, 0, 0, 4, 0, 0};
#include "t336.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t336_TI;
#include "t336MD.h"



// Metadata Definition UnityEngine.Collider
static MethodInfo* t336_MIs[] =
{
	NULL
};
static MethodInfo* t336_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t336_0_0_0;
extern Il2CppType t336_1_0_0;
struct t336;
TypeInfo t336_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Collider", "UnityEngine", t336_MIs, NULL, NULL, NULL, &t28_TI, NULL, NULL, &t336_TI, NULL, t336_VT, &EmptyCustomAttributesCache, &t336_TI, &t336_0_0_0, &t336_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t336), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t127_TI;
#include "t127MD.h"



extern MethodInfo m1486_MI;
 t23  m1486 (t127 * __this, MethodInfo* method){
	{
		t23  L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m1487_MI;
 t23  m1487 (t127 * __this, MethodInfo* method){
	{
		t23  L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m1489_MI;
 float m1489 (t127 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1488_MI;
 t336 * m1488 (t127 * __this, MethodInfo* method){
	{
		t336 * L_0 = (__this->f5);
		return L_0;
	}
}
// Metadata Definition UnityEngine.RaycastHit
extern Il2CppType t23_0_0_1;
FieldInfo t127_f0_FieldInfo = 
{
	"m_Point", &t23_0_0_1, &t127_TI, offsetof(t127, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_1;
FieldInfo t127_f1_FieldInfo = 
{
	"m_Normal", &t23_0_0_1, &t127_TI, offsetof(t127, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t127_f2_FieldInfo = 
{
	"m_FaceID", &t44_0_0_1, &t127_TI, offsetof(t127, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t127_f3_FieldInfo = 
{
	"m_Distance", &t22_0_0_1, &t127_TI, offsetof(t127, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t127_f4_FieldInfo = 
{
	"m_UV", &t17_0_0_1, &t127_TI, offsetof(t127, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t336_0_0_1;
FieldInfo t127_f5_FieldInfo = 
{
	"m_Collider", &t336_0_0_1, &t127_TI, offsetof(t127, f5) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t127_FIs[] =
{
	&t127_f0_FieldInfo,
	&t127_f1_FieldInfo,
	&t127_f2_FieldInfo,
	&t127_f3_FieldInfo,
	&t127_f4_FieldInfo,
	&t127_f5_FieldInfo,
	NULL
};
static PropertyInfo t127____point_PropertyInfo = 
{
	&t127_TI, "point", &m1486_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t127____normal_PropertyInfo = 
{
	&t127_TI, "normal", &m1487_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t127____distance_PropertyInfo = 
{
	&t127_TI, "distance", &m1489_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t127____collider_PropertyInfo = 
{
	&t127_TI, "collider", &m1488_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t127_PIs[] =
{
	&t127____point_PropertyInfo,
	&t127____normal_PropertyInfo,
	&t127____distance_PropertyInfo,
	&t127____collider_PropertyInfo,
	NULL
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m1486_MI = 
{
	"get_point", (methodPointerType)&m1486, &t127_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 820, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m1487_MI = 
{
	"get_normal", (methodPointerType)&m1487, &t127_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 821, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1489_MI = 
{
	"get_distance", (methodPointerType)&m1489, &t127_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 822, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t336_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1488_MI = 
{
	"get_collider", (methodPointerType)&m1488, &t127_TI, &t336_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 823, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t127_MIs[] =
{
	&m1486_MI,
	&m1487_MI,
	&m1489_MI,
	&m1488_MI,
	NULL
};
static MethodInfo* t127_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t127_0_0_0;
TypeInfo t127_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RaycastHit", "UnityEngine", t127_MIs, t127_PIs, t127_FIs, NULL, &t110_TI, NULL, NULL, &t127_TI, NULL, t127_VT, &EmptyCustomAttributesCache, &t127_TI, &t127_0_0_0, &t127_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t127)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 4, 4, 6, 0, 0, 4, 0, 0};
#include "t333.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t333_TI;
#include "t333MD.h"

#include "t506.h"
#include "t17.h"
#include "t330.h"
extern TypeInfo t506_TI;
#include "t506MD.h"
extern MethodInfo m2901_MI;
extern MethodInfo m2603_MI;
extern MethodInfo m2604_MI;
extern MethodInfo m2602_MI;
extern MethodInfo m2605_MI;


extern MethodInfo m2601_MI;
 void m2601 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t506_TI));
		t506 * L_0 = (t506 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t506_TI));
		m2901(L_0, &m2901_MI);
		((t333_SFs*)InitializedTypeInfo(&t333_TI)->static_fields)->f0 = L_0;
		return;
	}
}
 void m2602 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, float p4, float p5, t330 * p6, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t333_TI));
		m2603(NULL, (&p0), (&p1), p2, p3, p4, p5, p6, &m2603_MI);
		return;
	}
}
 void m2603 (t29 * __this, t17 * p0, t17 * p1, float p2, int32_t p3, float p4, float p5, t330 * p6, MethodInfo* method){
	typedef void (*m2603_ftn) (t17 *, t17 *, float, int32_t, float, float, t330 *);
	static m2603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(p0, p1, p2, p3, p4, p5, p6);
}
extern MethodInfo m1615_MI;
 t330  m1615 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t333_TI));
		t330  L_0 = m2604(NULL, p0, p1, p2, p3, V_1, V_0, &m2604_MI);
		return L_0;
	}
}
 t330  m2604 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, float p4, float p5, MethodInfo* method){
	t330  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t333_TI));
		m2602(NULL, p0, p1, p2, p3, p4, p5, (&V_0), &m2602_MI);
		return V_0;
	}
}
extern MethodInfo m1469_MI;
 t335* m1469 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t333_TI));
		t335* L_0 = m2605(NULL, (&p0), (&p1), p2, p3, V_1, V_0, &m2605_MI);
		return L_0;
	}
}
 t335* m2605 (t29 * __this, t17 * p0, t17 * p1, float p2, int32_t p3, float p4, float p5, MethodInfo* method){
	typedef t335* (*m2605_ftn) (t17 *, t17 *, float, int32_t, float, float);
	static m2605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(p0, p1, p2, p3, p4, p5);
}
// Metadata Definition UnityEngine.Physics2D
extern Il2CppType t506_0_0_17;
FieldInfo t333_f0_FieldInfo = 
{
	"m_LastDisabledRigidbody2D", &t506_0_0_17, &t333_TI, offsetof(t333_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t333_FIs[] =
{
	&t333_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2601_MI = 
{
	".cctor", (methodPointerType)&m2601, &t333_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 824, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t330_1_0_2;
extern Il2CppType t330_1_0_0;
static ParameterInfo t333_m2602_ParameterInfos[] = 
{
	{"origin", 0, 134218548, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"direction", 1, 134218549, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"distance", 2, 134218550, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layerMask", 3, 134218551, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minDepth", 4, 134218552, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"maxDepth", 5, 134218553, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"raycastHit", 6, 134218554, &EmptyCustomAttributesCache, &t330_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17_t17_t22_t44_t22_t22_t625 (MethodInfo* method, void* obj, void** args);
MethodInfo m2602_MI = 
{
	"Internal_Raycast", (methodPointerType)&m2602, &t333_TI, &t21_0_0_0, RuntimeInvoker_t21_t17_t17_t22_t44_t22_t22_t625, t333_m2602_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 7, false, false, 825, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_1_0_0;
extern Il2CppType t17_1_0_0;
extern Il2CppType t17_1_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t330_1_0_2;
static ParameterInfo t333_m2603_ParameterInfos[] = 
{
	{"origin", 0, 134218555, &EmptyCustomAttributesCache, &t17_1_0_0},
	{"direction", 1, 134218556, &EmptyCustomAttributesCache, &t17_1_0_0},
	{"distance", 2, 134218557, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layerMask", 3, 134218558, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minDepth", 4, 134218559, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"maxDepth", 5, 134218560, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"raycastHit", 6, 134218561, &EmptyCustomAttributesCache, &t330_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t594_t594_t22_t44_t22_t22_t625 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t333__CustomAttributeCache_m2603;
MethodInfo m2603_MI = 
{
	"INTERNAL_CALL_Internal_Raycast", (methodPointerType)&m2603, &t333_TI, &t21_0_0_0, RuntimeInvoker_t21_t594_t594_t22_t44_t22_t22_t625, t333_m2603_ParameterInfos, &t333__CustomAttributeCache_m2603, 145, 4096, 255, 7, false, false, 826, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t333_m1615_ParameterInfos[] = 
{
	{"origin", 0, 134218562, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"direction", 1, 134218563, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"distance", 2, 134218564, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layerMask", 3, 134218565, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t330_0_0_0;
extern void* RuntimeInvoker_t330_t17_t17_t22_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t333__CustomAttributeCache_m1615;
MethodInfo m1615_MI = 
{
	"Raycast", (methodPointerType)&m1615, &t333_TI, &t330_0_0_0, RuntimeInvoker_t330_t17_t17_t22_t44, t333_m1615_ParameterInfos, &t333__CustomAttributeCache_m1615, 150, 0, 255, 4, false, false, 827, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg2_ParameterInfo;
extern Il2CppType t44_0_0_0;
extern CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg3_ParameterInfo;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg4_ParameterInfo;
extern Il2CppType t22_0_0_0;
extern CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg5_ParameterInfo;
static ParameterInfo t333_m2604_ParameterInfos[] = 
{
	{"origin", 0, 134218566, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"direction", 1, 134218567, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"distance", 2, 134218568, &t333__CustomAttributeCache_t333_m2604_Arg2_ParameterInfo, &t22_0_0_0},
	{"layerMask", 3, 134218569, &t333__CustomAttributeCache_t333_m2604_Arg3_ParameterInfo, &t44_0_0_0},
	{"minDepth", 4, 134218570, &t333__CustomAttributeCache_t333_m2604_Arg4_ParameterInfo, &t22_0_0_0},
	{"maxDepth", 5, 134218571, &t333__CustomAttributeCache_t333_m2604_Arg5_ParameterInfo, &t22_0_0_0},
};
extern Il2CppType t330_0_0_0;
extern void* RuntimeInvoker_t330_t17_t17_t22_t44_t22_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2604_MI = 
{
	"Raycast", (methodPointerType)&m2604, &t333_TI, &t330_0_0_0, RuntimeInvoker_t330_t17_t17_t22_t44_t22_t22, t333_m2604_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 6, false, false, 828, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t333_m1469_ParameterInfos[] = 
{
	{"origin", 0, 134218572, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"direction", 1, 134218573, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"distance", 2, 134218574, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layerMask", 3, 134218575, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t335_0_0_0;
extern void* RuntimeInvoker_t29_t17_t17_t22_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t333__CustomAttributeCache_m1469;
MethodInfo m1469_MI = 
{
	"RaycastAll", (methodPointerType)&m1469, &t333_TI, &t335_0_0_0, RuntimeInvoker_t29_t17_t17_t22_t44, t333_m1469_ParameterInfos, &t333__CustomAttributeCache_m1469, 150, 0, 255, 4, false, false, 829, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_1_0_0;
extern Il2CppType t17_1_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t333_m2605_ParameterInfos[] = 
{
	{"origin", 0, 134218576, &EmptyCustomAttributesCache, &t17_1_0_0},
	{"direction", 1, 134218577, &EmptyCustomAttributesCache, &t17_1_0_0},
	{"distance", 2, 134218578, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"layerMask", 3, 134218579, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minDepth", 4, 134218580, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"maxDepth", 5, 134218581, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t335_0_0_0;
extern void* RuntimeInvoker_t29_t594_t594_t22_t44_t22_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t333__CustomAttributeCache_m2605;
MethodInfo m2605_MI = 
{
	"INTERNAL_CALL_RaycastAll", (methodPointerType)&m2605, &t333_TI, &t335_0_0_0, RuntimeInvoker_t29_t594_t594_t22_t44_t22_t22, t333_m2605_ParameterInfos, &t333__CustomAttributeCache_m2605, 145, 4096, 255, 6, false, false, 830, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t333_MIs[] =
{
	&m2601_MI,
	&m2602_MI,
	&m2603_MI,
	&m1615_MI,
	&m2604_MI,
	&m1469_MI,
	&m2605_MI,
	NULL
};
static MethodInfo* t333_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t333_CustomAttributesCacheGenerator_m2603(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_m1615(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t577 * tmp;
		tmp = (t577 *)il2cpp_codegen_object_new (&t577_TI);
		m2830(tmp, &m2830_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_t333_m2604_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_t333_m2604_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_t333_m2604_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("-Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_t333_m2604_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t576 * tmp;
		tmp = (t576 *)il2cpp_codegen_object_new (&t576_TI);
		m2826(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), &m2826_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_m1469(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t577 * tmp;
		tmp = (t577 *)il2cpp_codegen_object_new (&t577_TI);
		m2830(tmp, &m2830_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t333_CustomAttributesCacheGenerator_m2605(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t333__CustomAttributeCache_m2603 = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_m2603
};
CustomAttributesCache t333__CustomAttributeCache_m1615 = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_m1615
};
CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg2_ParameterInfo = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_t333_m2604_Arg2_ParameterInfo
};
CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg3_ParameterInfo = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_t333_m2604_Arg3_ParameterInfo
};
CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg4_ParameterInfo = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_t333_m2604_Arg4_ParameterInfo
};
CustomAttributesCache t333__CustomAttributeCache_t333_m2604_Arg5_ParameterInfo = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_t333_m2604_Arg5_ParameterInfo
};
CustomAttributesCache t333__CustomAttributeCache_m1469 = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_m1469
};
CustomAttributesCache t333__CustomAttributeCache_m2605 = {
1,
NULL,
&t333_CustomAttributesCacheGenerator_m2605
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t333_0_0_0;
extern Il2CppType t333_1_0_0;
struct t333;
extern CustomAttributesCache t333__CustomAttributeCache_m2603;
extern CustomAttributesCache t333__CustomAttributeCache_m1615;
extern CustomAttributesCache t333__CustomAttributeCache_m1469;
extern CustomAttributesCache t333__CustomAttributeCache_m2605;
TypeInfo t333_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Physics2D", "UnityEngine", t333_MIs, NULL, t333_FIs, NULL, &t29_TI, NULL, NULL, &t333_TI, NULL, t333_VT, &EmptyCustomAttributesCache, &t333_TI, &t333_0_0_0, &t333_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t333), 0, -1, sizeof(t333_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, true, false, false, 7, 0, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t330_TI;
#include "t330MD.h"

#include "t332.h"
#include "t507.h"
#include "t332MD.h"
extern MethodInfo m1472_MI;
extern MethodInfo m2607_MI;
extern MethodInfo m2606_MI;


extern MethodInfo m1470_MI;
 t17  m1470 (t330 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m1471_MI;
 t17  m1471 (t330 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1616_MI;
 float m1616 (t330 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f4);
		return L_0;
	}
}
 t332 * m1472 (t330 * __this, MethodInfo* method){
	{
		t332 * L_0 = (__this->f5);
		return L_0;
	}
}
 t507 * m2606 (t330 * __this, MethodInfo* method){
	t507 * G_B3_0 = {0};
	{
		t332 * L_0 = m1472(__this, &m1472_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		t332 * L_2 = m1472(__this, &m1472_MI);
		t507 * L_3 = m2607(L_2, &m2607_MI);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((t507 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern MethodInfo m1474_MI;
 t25 * m1474 (t330 * __this, MethodInfo* method){
	t507 * V_0 = {0};
	{
		t507 * L_0 = m2606(__this, &m2606_MI);
		V_0 = L_0;
		bool L_1 = m1300(NULL, V_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		t25 * L_2 = m39(V_0, &m39_MI);
		return L_2;
	}

IL_001a:
	{
		t332 * L_3 = m1472(__this, &m1472_MI);
		bool L_4 = m1300(NULL, L_3, (t41 *)NULL, &m1300_MI);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		t332 * L_5 = m1472(__this, &m1472_MI);
		t25 * L_6 = m39(L_5, &m39_MI);
		return L_6;
	}

IL_0037:
	{
		return (t25 *)NULL;
	}
}
// Metadata Definition UnityEngine.RaycastHit2D
extern Il2CppType t17_0_0_1;
FieldInfo t330_f0_FieldInfo = 
{
	"m_Centroid", &t17_0_0_1, &t330_TI, offsetof(t330, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t330_f1_FieldInfo = 
{
	"m_Point", &t17_0_0_1, &t330_TI, offsetof(t330, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t330_f2_FieldInfo = 
{
	"m_Normal", &t17_0_0_1, &t330_TI, offsetof(t330, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t330_f3_FieldInfo = 
{
	"m_Distance", &t22_0_0_1, &t330_TI, offsetof(t330, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t330_f4_FieldInfo = 
{
	"m_Fraction", &t22_0_0_1, &t330_TI, offsetof(t330, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t332_0_0_1;
FieldInfo t330_f5_FieldInfo = 
{
	"m_Collider", &t332_0_0_1, &t330_TI, offsetof(t330, f5) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t330_FIs[] =
{
	&t330_f0_FieldInfo,
	&t330_f1_FieldInfo,
	&t330_f2_FieldInfo,
	&t330_f3_FieldInfo,
	&t330_f4_FieldInfo,
	&t330_f5_FieldInfo,
	NULL
};
static PropertyInfo t330____point_PropertyInfo = 
{
	&t330_TI, "point", &m1470_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t330____normal_PropertyInfo = 
{
	&t330_TI, "normal", &m1471_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t330____fraction_PropertyInfo = 
{
	&t330_TI, "fraction", &m1616_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t330____collider_PropertyInfo = 
{
	&t330_TI, "collider", &m1472_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t330____rigidbody_PropertyInfo = 
{
	&t330_TI, "rigidbody", &m2606_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t330____transform_PropertyInfo = 
{
	&t330_TI, "transform", &m1474_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t330_PIs[] =
{
	&t330____point_PropertyInfo,
	&t330____normal_PropertyInfo,
	&t330____fraction_PropertyInfo,
	&t330____collider_PropertyInfo,
	&t330____rigidbody_PropertyInfo,
	&t330____transform_PropertyInfo,
	NULL
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1470_MI = 
{
	"get_point", (methodPointerType)&m1470, &t330_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 831, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1471_MI = 
{
	"get_normal", (methodPointerType)&m1471, &t330_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 832, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1616_MI = 
{
	"get_fraction", (methodPointerType)&m1616, &t330_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 833, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t332_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1472_MI = 
{
	"get_collider", (methodPointerType)&m1472, &t330_TI, &t332_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 834, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2606_MI = 
{
	"get_rigidbody", (methodPointerType)&m2606, &t330_TI, &t507_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 835, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1474_MI = 
{
	"get_transform", (methodPointerType)&m1474, &t330_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 836, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t330_MIs[] =
{
	&m1470_MI,
	&m1471_MI,
	&m1616_MI,
	&m1472_MI,
	&m2606_MI,
	&m1474_MI,
	NULL
};
static MethodInfo* t330_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t330_0_0_0;
TypeInfo t330_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RaycastHit2D", "UnityEngine", t330_MIs, t330_PIs, t330_FIs, NULL, &t110_TI, NULL, NULL, &t330_TI, NULL, t330_VT, &EmptyCustomAttributesCache, &t330_TI, &t330_0_0_0, &t330_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t330)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 6, 6, 6, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t507_TI;
#include "t507MD.h"



// Metadata Definition UnityEngine.Rigidbody2D
static MethodInfo* t507_MIs[] =
{
	NULL
};
static MethodInfo* t507_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_1_0_0;
struct t507;
TypeInfo t507_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Rigidbody2D", "UnityEngine", t507_MIs, NULL, NULL, NULL, &t28_TI, NULL, NULL, &t507_TI, NULL, t507_VT, &EmptyCustomAttributesCache, &t507_TI, &t507_0_0_0, &t507_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t507), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t332_TI;



 t507 * m2607 (t332 * __this, MethodInfo* method){
	typedef t507 * (*m2607_ftn) (t332 *);
	static m2607_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2607_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// Metadata Definition UnityEngine.Collider2D
static PropertyInfo t332____attachedRigidbody_PropertyInfo = 
{
	&t332_TI, "attachedRigidbody", &m2607_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t332_PIs[] =
{
	&t332____attachedRigidbody_PropertyInfo,
	NULL
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t332__CustomAttributeCache_m2607;
MethodInfo m2607_MI = 
{
	"get_attachedRigidbody", (methodPointerType)&m2607, &t332_TI, &t507_0_0_0, RuntimeInvoker_t29, NULL, &t332__CustomAttributeCache_m2607, 2182, 4096, 255, 0, false, false, 837, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t332_MIs[] =
{
	&m2607_MI,
	NULL
};
static MethodInfo* t332_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t332_CustomAttributesCacheGenerator_m2607(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t332__CustomAttributeCache_m2607 = {
1,
NULL,
&t332_CustomAttributesCacheGenerator_m2607
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t332_0_0_0;
extern Il2CppType t332_1_0_0;
extern TypeInfo t317_TI;
struct t332;
extern CustomAttributesCache t332__CustomAttributeCache_m2607;
TypeInfo t332_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Collider2D", "UnityEngine", t332_MIs, t332_PIs, NULL, NULL, &t317_TI, NULL, NULL, &t332_TI, NULL, t332_VT, &EmptyCustomAttributesCache, &t332_TI, &t332_0_0_0, &t332_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t332), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 4, 0, 0};
#include "t508.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t508_TI;
#include "t508MD.h"

#include "mscorlib_ArrayTypes.h"
#include "t67.h"


extern MethodInfo m2608_MI;
 void m2608 (t508 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m2609_MI;
 void m2609 (t508 * __this, t509* p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m2609((t508 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t509* p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t509* p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
void pinvoke_delegate_wrapper_t508(Il2CppObject* delegate, t509* p0)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	float* _p0_marshaled = { 0 };
	_p0_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)p0);

	// Native function invocation
	_il2cpp_pinvoke_func(_p0_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

}
extern MethodInfo m2610_MI;
 t29 * m2610 (t508 * __this, t509* p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m2611_MI;
 void m2611 (t508 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.AudioClip/PCMReaderCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t508_m2608_ParameterInfos[] = 
{
	{"object", 0, 134218584, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218585, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m2608_MI = 
{
	".ctor", (methodPointerType)&m2608, &t508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t508_m2608_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 840, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t509_0_0_0;
extern Il2CppType t509_0_0_0;
static ParameterInfo t508_m2609_ParameterInfos[] = 
{
	{"data", 0, 134218586, &EmptyCustomAttributesCache, &t509_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2609_MI = 
{
	"Invoke", (methodPointerType)&m2609, &t508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t508_m2609_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 841, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t509_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t508_m2610_ParameterInfos[] = 
{
	{"data", 0, 134218587, &EmptyCustomAttributesCache, &t509_0_0_0},
	{"callback", 1, 134218588, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134218589, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2610_MI = 
{
	"BeginInvoke", (methodPointerType)&m2610, &t508_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t508_m2610_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 842, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t508_m2611_ParameterInfos[] = 
{
	{"result", 0, 134218590, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2611_MI = 
{
	"EndInvoke", (methodPointerType)&m2611, &t508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t508_m2611_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 843, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t508_MIs[] =
{
	&m2608_MI,
	&m2609_MI,
	&m2610_MI,
	&m2611_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t508_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2609_MI,
	&m2610_MI,
	&m2611_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t508_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t508_0_0_0;
extern Il2CppType t508_1_0_0;
extern TypeInfo t195_TI;
struct t508;
extern TypeInfo t511_TI;
TypeInfo t508_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PCMReaderCallback", "", t508_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t511_TI, &t508_TI, NULL, t508_VT, &EmptyCustomAttributesCache, &t508_TI, &t508_0_0_0, &t508_1_0_0, t508_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t508, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t508), 0, sizeof(methodPointerType), 0, 0, -1, 258, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t510.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t510_TI;
#include "t510MD.h"



extern MethodInfo m2612_MI;
 void m2612 (t510 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m2613_MI;
 void m2613 (t510 * __this, int32_t p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m2613((t510 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, int32_t p0, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
void pinvoke_delegate_wrapper_t510(Il2CppObject* delegate, int32_t p0)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(p0);

	// Marshaling cleanup of parameter 'p0' native representation

}
extern MethodInfo m2614_MI;
 t29 * m2614 (t510 * __this, int32_t p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m2615_MI;
 void m2615 (t510 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.AudioClip/PCMSetPositionCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t510_m2612_ParameterInfos[] = 
{
	{"object", 0, 134218591, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218592, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m2612_MI = 
{
	".ctor", (methodPointerType)&m2612, &t510_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t510_m2612_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 844, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t510_m2613_ParameterInfos[] = 
{
	{"position", 0, 134218593, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2613_MI = 
{
	"Invoke", (methodPointerType)&m2613, &t510_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t510_m2613_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 845, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t510_m2614_ParameterInfos[] = 
{
	{"position", 0, 134218594, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 1, 134218595, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134218596, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2614_MI = 
{
	"BeginInvoke", (methodPointerType)&m2614, &t510_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29, t510_m2614_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 846, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t510_m2615_ParameterInfos[] = 
{
	{"result", 0, 134218597, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2615_MI = 
{
	"EndInvoke", (methodPointerType)&m2615, &t510_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t510_m2615_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 847, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t510_MIs[] =
{
	&m2612_MI,
	&m2613_MI,
	&m2614_MI,
	&m2615_MI,
	NULL
};
static MethodInfo* t510_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2613_MI,
	&m2614_MI,
	&m2615_MI,
};
static Il2CppInterfaceOffsetPair t510_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t510_0_0_0;
extern Il2CppType t510_1_0_0;
struct t510;
TypeInfo t510_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PCMSetPositionCallback", "", t510_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t511_TI, &t510_TI, NULL, t510_VT, &EmptyCustomAttributesCache, &t510_TI, &t510_0_0_0, &t510_1_0_0, t510_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t510, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t510), 0, sizeof(methodPointerType), 0, 0, -1, 258, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t511.h"
#ifndef _MSC_VER
#else
#endif
#include "t511MD.h"

extern TypeInfo t21_TI;
extern TypeInfo t509_TI;
extern TypeInfo t22_TI;
extern TypeInfo t44_TI;


extern MethodInfo m2616_MI;
 void m2616 (t511 * __this, t509* p0, MethodInfo* method){
	{
		t508 * L_0 = (__this->f2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		t508 * L_1 = (__this->f2);
		VirtActionInvoker1< t509* >::Invoke(&m2609_MI, L_1, p0);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m2617_MI;
 void m2617 (t511 * __this, int32_t p0, MethodInfo* method){
	{
		t510 * L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		t510 * L_1 = (__this->f3);
		VirtActionInvoker1< int32_t >::Invoke(&m2613_MI, L_1, p0);
	}

IL_0017:
	{
		return;
	}
}
// Metadata Definition UnityEngine.AudioClip
extern Il2CppType t508_0_0_1;
FieldInfo t511_f2_FieldInfo = 
{
	"m_PCMReaderCallback", &t508_0_0_1, &t511_TI, offsetof(t511, f2), &EmptyCustomAttributesCache};
extern Il2CppType t510_0_0_1;
FieldInfo t511_f3_FieldInfo = 
{
	"m_PCMSetPositionCallback", &t510_0_0_1, &t511_TI, offsetof(t511, f3), &EmptyCustomAttributesCache};
static FieldInfo* t511_FIs[] =
{
	&t511_f2_FieldInfo,
	&t511_f3_FieldInfo,
	NULL
};
extern Il2CppType t509_0_0_0;
static ParameterInfo t511_m2616_ParameterInfos[] = 
{
	{"data", 0, 134218582, &EmptyCustomAttributesCache, &t509_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2616_MI = 
{
	"InvokePCMReaderCallback_Internal", (methodPointerType)&m2616, &t511_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t511_m2616_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 838, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t511_m2617_ParameterInfos[] = 
{
	{"position", 0, 134218583, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2617_MI = 
{
	"InvokePCMSetPositionCallback_Internal", (methodPointerType)&m2617, &t511_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t511_m2617_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 839, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t511_MIs[] =
{
	&m2616_MI,
	&m2617_MI,
	NULL
};
extern TypeInfo t508_TI;
extern TypeInfo t510_TI;
static TypeInfo* t511_TI__nestedTypes[3] =
{
	&t508_TI,
	&t510_TI,
	NULL
};
static MethodInfo* t511_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t511_0_0_0;
extern Il2CppType t511_1_0_0;
struct t511;
TypeInfo t511_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AudioClip", "UnityEngine", t511_MIs, NULL, t511_FIs, NULL, &t41_TI, t511_TI__nestedTypes, NULL, &t511_TI, NULL, t511_VT, &EmptyCustomAttributesCache, &t511_TI, &t511_0_0_0, &t511_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t511), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 2, 0, 2, 4, 0, 0};
#include "t512.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t512_TI;
#include "t512MD.h"



// Metadata Definition UnityEngine.AnimationEventSource
extern Il2CppType t44_0_0_1542;
FieldInfo t512_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t512_TI, offsetof(t512, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t512_0_0_32854;
FieldInfo t512_f2_FieldInfo = 
{
	"NoSource", &t512_0_0_32854, &t512_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t512_0_0_32854;
FieldInfo t512_f3_FieldInfo = 
{
	"Legacy", &t512_0_0_32854, &t512_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t512_0_0_32854;
FieldInfo t512_f4_FieldInfo = 
{
	"Animator", &t512_0_0_32854, &t512_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t512_FIs[] =
{
	&t512_f1_FieldInfo,
	&t512_f2_FieldInfo,
	&t512_f3_FieldInfo,
	&t512_f4_FieldInfo,
	NULL
};
static const int32_t t512_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t512_f2_DefaultValue = 
{
	&t512_f2_FieldInfo, { (char*)&t512_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t512_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t512_f3_DefaultValue = 
{
	&t512_f3_FieldInfo, { (char*)&t512_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t512_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t512_f4_DefaultValue = 
{
	&t512_f4_FieldInfo, { (char*)&t512_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t512_FDVs[] = 
{
	&t512_f2_DefaultValue,
	&t512_f3_DefaultValue,
	&t512_f4_DefaultValue,
	NULL
};
static MethodInfo* t512_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t512_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t512_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t512_0_0_0;
extern Il2CppType t512_1_0_0;
extern TypeInfo t49_TI;
TypeInfo t512_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimationEventSource", "UnityEngine", t512_MIs, NULL, t512_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t512_VT, &EmptyCustomAttributesCache, &t44_TI, &t512_0_0_0, &t512_1_0_0, t512_IOs, NULL, NULL, t512_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t512)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t513.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t513_TI;
#include "t513MD.h"

#include "t514.h"
#include "t515.h"
#include "t516.h"
extern TypeInfo t7_TI;
#include "t7MD.h"
extern MethodInfo m2635_MI;
extern MethodInfo m1296_MI;
extern MethodInfo m2636_MI;


extern MethodInfo m2618_MI;
 void m2618 (t513 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f1 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f2 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f3 = (t41 *)NULL;
		__this->f4 = (0.0f);
		__this->f5 = 0;
		__this->f6 = 0;
		__this->f7 = 0;
		__this->f8 = (t514 *)NULL;
		return;
	}
}
extern MethodInfo m2619_MI;
 t7* m2619 (t513 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2620_MI;
 void m2620 (t513 * __this, t7* p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
extern MethodInfo m2621_MI;
 t7* m2621 (t513 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2622_MI;
 void m2622 (t513 * __this, t7* p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
extern MethodInfo m2623_MI;
 float m2623 (t513 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m2624_MI;
 void m2624 (t513 * __this, float p0, MethodInfo* method){
	{
		__this->f4 = p0;
		return;
	}
}
extern MethodInfo m2625_MI;
 int32_t m2625 (t513 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m2626_MI;
 void m2626 (t513 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f5 = p0;
		return;
	}
}
extern MethodInfo m2627_MI;
 t41 * m2627 (t513 * __this, MethodInfo* method){
	{
		t41 * L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m2628_MI;
 void m2628 (t513 * __this, t41 * p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m2629_MI;
 t7* m2629 (t513 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2630_MI;
 void m2630 (t513 * __this, t7* p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m2631_MI;
 float m2631 (t513 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2632_MI;
 void m2632 (t513 * __this, float p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m2633_MI;
 int32_t m2633 (t513 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f6);
		return (int32_t)(L_0);
	}
}
extern MethodInfo m2634_MI;
 void m2634 (t513 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f6 = p0;
		return;
	}
}
 bool m2635 (t513 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
 bool m2636 (t513 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
extern MethodInfo m2637_MI;
 t514 * m2637 (t513 * __this, MethodInfo* method){
	{
		bool L_0 = m2635(__this, &m2635_MI);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		m1296(NULL, (t7*) &_stringLiteral146, &m1296_MI);
	}

IL_0015:
	{
		t514 * L_1 = (__this->f8);
		return L_1;
	}
}
extern MethodInfo m2638_MI;
 t515  m2638 (t513 * __this, MethodInfo* method){
	{
		bool L_0 = m2636(__this, &m2636_MI);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		m1296(NULL, (t7*) &_stringLiteral147, &m1296_MI);
	}

IL_0015:
	{
		t515  L_1 = (__this->f9);
		return L_1;
	}
}
extern MethodInfo m2639_MI;
 t516  m2639 (t513 * __this, MethodInfo* method){
	{
		bool L_0 = m2636(__this, &m2636_MI);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		m1296(NULL, (t7*) &_stringLiteral148, &m1296_MI);
	}

IL_0015:
	{
		t516  L_1 = (__this->f10);
		return L_1;
	}
}
// Metadata Definition UnityEngine.AnimationEvent
extern Il2CppType t22_0_0_3;
FieldInfo t513_f0_FieldInfo = 
{
	"m_Time", &t22_0_0_3, &t513_TI, offsetof(t513, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t513_f1_FieldInfo = 
{
	"m_FunctionName", &t7_0_0_3, &t513_TI, offsetof(t513, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t513_f2_FieldInfo = 
{
	"m_StringParameter", &t7_0_0_3, &t513_TI, offsetof(t513, f2), &EmptyCustomAttributesCache};
extern Il2CppType t41_0_0_3;
FieldInfo t513_f3_FieldInfo = 
{
	"m_ObjectReferenceParameter", &t41_0_0_3, &t513_TI, offsetof(t513, f3), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_3;
FieldInfo t513_f4_FieldInfo = 
{
	"m_FloatParameter", &t22_0_0_3, &t513_TI, offsetof(t513, f4), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t513_f5_FieldInfo = 
{
	"m_IntParameter", &t44_0_0_3, &t513_TI, offsetof(t513, f5), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t513_f6_FieldInfo = 
{
	"m_MessageOptions", &t44_0_0_3, &t513_TI, offsetof(t513, f6), &EmptyCustomAttributesCache};
extern Il2CppType t512_0_0_3;
FieldInfo t513_f7_FieldInfo = 
{
	"m_Source", &t512_0_0_3, &t513_TI, offsetof(t513, f7), &EmptyCustomAttributesCache};
extern Il2CppType t514_0_0_3;
FieldInfo t513_f8_FieldInfo = 
{
	"m_StateSender", &t514_0_0_3, &t513_TI, offsetof(t513, f8), &EmptyCustomAttributesCache};
extern Il2CppType t515_0_0_3;
FieldInfo t513_f9_FieldInfo = 
{
	"m_AnimatorStateInfo", &t515_0_0_3, &t513_TI, offsetof(t513, f9), &EmptyCustomAttributesCache};
extern Il2CppType t516_0_0_3;
FieldInfo t513_f10_FieldInfo = 
{
	"m_AnimatorClipInfo", &t516_0_0_3, &t513_TI, offsetof(t513, f10), &EmptyCustomAttributesCache};
static FieldInfo* t513_FIs[] =
{
	&t513_f0_FieldInfo,
	&t513_f1_FieldInfo,
	&t513_f2_FieldInfo,
	&t513_f3_FieldInfo,
	&t513_f4_FieldInfo,
	&t513_f5_FieldInfo,
	&t513_f6_FieldInfo,
	&t513_f7_FieldInfo,
	&t513_f8_FieldInfo,
	&t513_f9_FieldInfo,
	&t513_f10_FieldInfo,
	NULL
};
extern CustomAttributesCache t513__CustomAttributeCache_t513____data_PropertyInfo;
static PropertyInfo t513____data_PropertyInfo = 
{
	&t513_TI, "data", &m2619_MI, &m2620_MI, 0, &t513__CustomAttributeCache_t513____data_PropertyInfo};
static PropertyInfo t513____stringParameter_PropertyInfo = 
{
	&t513_TI, "stringParameter", &m2621_MI, &m2622_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____floatParameter_PropertyInfo = 
{
	&t513_TI, "floatParameter", &m2623_MI, &m2624_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____intParameter_PropertyInfo = 
{
	&t513_TI, "intParameter", &m2625_MI, &m2626_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____objectReferenceParameter_PropertyInfo = 
{
	&t513_TI, "objectReferenceParameter", &m2627_MI, &m2628_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____functionName_PropertyInfo = 
{
	&t513_TI, "functionName", &m2629_MI, &m2630_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____time_PropertyInfo = 
{
	&t513_TI, "time", &m2631_MI, &m2632_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____messageOptions_PropertyInfo = 
{
	&t513_TI, "messageOptions", &m2633_MI, &m2634_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____isFiredByLegacy_PropertyInfo = 
{
	&t513_TI, "isFiredByLegacy", &m2635_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____isFiredByAnimator_PropertyInfo = 
{
	&t513_TI, "isFiredByAnimator", &m2636_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____animationState_PropertyInfo = 
{
	&t513_TI, "animationState", &m2637_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____animatorStateInfo_PropertyInfo = 
{
	&t513_TI, "animatorStateInfo", &m2638_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t513____animatorClipInfo_PropertyInfo = 
{
	&t513_TI, "animatorClipInfo", &m2639_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t513_PIs[] =
{
	&t513____data_PropertyInfo,
	&t513____stringParameter_PropertyInfo,
	&t513____floatParameter_PropertyInfo,
	&t513____intParameter_PropertyInfo,
	&t513____objectReferenceParameter_PropertyInfo,
	&t513____functionName_PropertyInfo,
	&t513____time_PropertyInfo,
	&t513____messageOptions_PropertyInfo,
	&t513____isFiredByLegacy_PropertyInfo,
	&t513____isFiredByAnimator_PropertyInfo,
	&t513____animationState_PropertyInfo,
	&t513____animatorStateInfo_PropertyInfo,
	&t513____animatorClipInfo_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2618_MI = 
{
	".ctor", (methodPointerType)&m2618, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 848, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2619_MI = 
{
	"get_data", (methodPointerType)&m2619, &t513_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 849, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t513_m2620_ParameterInfos[] = 
{
	{"value", 0, 134218598, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2620_MI = 
{
	"set_data", (methodPointerType)&m2620, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t513_m2620_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 850, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2621_MI = 
{
	"get_stringParameter", (methodPointerType)&m2621, &t513_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 851, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t513_m2622_ParameterInfos[] = 
{
	{"value", 0, 134218599, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2622_MI = 
{
	"set_stringParameter", (methodPointerType)&m2622, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t513_m2622_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 852, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2623_MI = 
{
	"get_floatParameter", (methodPointerType)&m2623, &t513_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 853, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t513_m2624_ParameterInfos[] = 
{
	{"value", 0, 134218600, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2624_MI = 
{
	"set_floatParameter", (methodPointerType)&m2624, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t513_m2624_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 854, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2625_MI = 
{
	"get_intParameter", (methodPointerType)&m2625, &t513_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 855, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t513_m2626_ParameterInfos[] = 
{
	{"value", 0, 134218601, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2626_MI = 
{
	"set_intParameter", (methodPointerType)&m2626, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t513_m2626_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 856, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2627_MI = 
{
	"get_objectReferenceParameter", (methodPointerType)&m2627, &t513_TI, &t41_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 857, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
static ParameterInfo t513_m2628_ParameterInfos[] = 
{
	{"value", 0, 134218602, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2628_MI = 
{
	"set_objectReferenceParameter", (methodPointerType)&m2628, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t513_m2628_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 858, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2629_MI = 
{
	"get_functionName", (methodPointerType)&m2629, &t513_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 859, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t513_m2630_ParameterInfos[] = 
{
	{"value", 0, 134218603, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2630_MI = 
{
	"set_functionName", (methodPointerType)&m2630, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t513_m2630_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 860, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2631_MI = 
{
	"get_time", (methodPointerType)&m2631, &t513_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 861, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t513_m2632_ParameterInfos[] = 
{
	{"value", 0, 134218604, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2632_MI = 
{
	"set_time", (methodPointerType)&m2632, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t513_m2632_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 862, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t445_0_0_0;
extern void* RuntimeInvoker_t445 (MethodInfo* method, void* obj, void** args);
MethodInfo m2633_MI = 
{
	"get_messageOptions", (methodPointerType)&m2633, &t513_TI, &t445_0_0_0, RuntimeInvoker_t445, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 863, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t445_0_0_0;
static ParameterInfo t513_m2634_ParameterInfos[] = 
{
	{"value", 0, 134218605, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2634_MI = 
{
	"set_messageOptions", (methodPointerType)&m2634, &t513_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t513_m2634_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 864, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2635_MI = 
{
	"get_isFiredByLegacy", (methodPointerType)&m2635, &t513_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 865, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2636_MI = 
{
	"get_isFiredByAnimator", (methodPointerType)&m2636, &t513_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 866, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t514_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2637_MI = 
{
	"get_animationState", (methodPointerType)&m2637, &t513_TI, &t514_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 867, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t515_0_0_0;
extern void* RuntimeInvoker_t515 (MethodInfo* method, void* obj, void** args);
MethodInfo m2638_MI = 
{
	"get_animatorStateInfo", (methodPointerType)&m2638, &t513_TI, &t515_0_0_0, RuntimeInvoker_t515, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 868, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t516_0_0_0;
extern void* RuntimeInvoker_t516 (MethodInfo* method, void* obj, void** args);
MethodInfo m2639_MI = 
{
	"get_animatorClipInfo", (methodPointerType)&m2639, &t513_TI, &t516_0_0_0, RuntimeInvoker_t516, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 869, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t513_MIs[] =
{
	&m2618_MI,
	&m2619_MI,
	&m2620_MI,
	&m2621_MI,
	&m2622_MI,
	&m2623_MI,
	&m2624_MI,
	&m2625_MI,
	&m2626_MI,
	&m2627_MI,
	&m2628_MI,
	&m2629_MI,
	&m2630_MI,
	&m2631_MI,
	&m2632_MI,
	&m2633_MI,
	&m2634_MI,
	&m2635_MI,
	&m2636_MI,
	&m2637_MI,
	&m2638_MI,
	&m2639_MI,
	NULL
};
static MethodInfo* t513_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t327_TI;
#include "t327.h"
#include "t327MD.h"
extern MethodInfo m2838_MI;
void t513_CustomAttributesCacheGenerator_t513____data_PropertyInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("Use stringParameter instead"), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t513__CustomAttributeCache_t513____data_PropertyInfo = {
1,
NULL,
&t513_CustomAttributesCacheGenerator_t513____data_PropertyInfo
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t513_0_0_0;
extern Il2CppType t513_1_0_0;
struct t513;
extern CustomAttributesCache t513__CustomAttributeCache_t513____data_PropertyInfo;
TypeInfo t513_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimationEvent", "UnityEngine", t513_MIs, t513_PIs, t513_FIs, NULL, &t29_TI, NULL, NULL, &t513_TI, NULL, t513_VT, &EmptyCustomAttributesCache, &t513_TI, &t513_0_0_0, &t513_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t513), 0, -1, 0, 0, -1, 1057033, 0, false, false, false, false, false, false, false, false, false, false, false, false, 22, 13, 11, 0, 0, 4, 0, 0};
#include "t517.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t517_TI;
#include "t517MD.h"



// Metadata Definition UnityEngine.Keyframe
extern Il2CppType t22_0_0_1;
FieldInfo t517_f0_FieldInfo = 
{
	"m_Time", &t22_0_0_1, &t517_TI, offsetof(t517, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t517_f1_FieldInfo = 
{
	"m_Value", &t22_0_0_1, &t517_TI, offsetof(t517, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t517_f2_FieldInfo = 
{
	"m_InTangent", &t22_0_0_1, &t517_TI, offsetof(t517, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t517_f3_FieldInfo = 
{
	"m_OutTangent", &t22_0_0_1, &t517_TI, offsetof(t517, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t517_FIs[] =
{
	&t517_f0_FieldInfo,
	&t517_f1_FieldInfo,
	&t517_f2_FieldInfo,
	&t517_f3_FieldInfo,
	NULL
};
static MethodInfo* t517_MIs[] =
{
	NULL
};
static MethodInfo* t517_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t517_0_0_0;
extern Il2CppType t517_1_0_0;
TypeInfo t517_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Keyframe", "UnityEngine", t517_MIs, NULL, t517_FIs, NULL, &t110_TI, NULL, NULL, &t517_TI, NULL, t517_VT, &EmptyCustomAttributesCache, &t517_TI, &t517_0_0_0, &t517_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t517)+ sizeof (Il2CppObject), 0, sizeof(t517 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 4, 0, 0, 4, 0, 0};
#include "t518.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t518_TI;
#include "t518MD.h"

extern MethodInfo m2644_MI;
extern MethodInfo m2642_MI;


extern MethodInfo m2640_MI;
 void m2640 (t518 * __this, t519* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m2644(__this, p0, &m2644_MI);
		return;
	}
}
extern MethodInfo m2641_MI;
 void m2641 (t518 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m2644(__this, (t519*)(t519*)NULL, &m2644_MI);
		return;
	}
}
 void m2642 (t518 * __this, MethodInfo* method){
	typedef void (*m2642_ftn) (t518 *);
	static m2642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
extern MethodInfo m2643_MI;
 void m2643 (t518 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		m2642(__this, &m2642_MI);
		// IL_0006: leave IL_0012
		leaveInstructions[0] = 0x12; // 1
		THROW_SENTINEL(IL_0012);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_000b;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_000b;
	}

IL_000b:
	{ // begin finally (depth: 1)
		m46(__this, &m46_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x12:
				goto IL_0012;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0012:
	{
		return;
	}
}
 void m2644 (t518 * __this, t519* p0, MethodInfo* method){
	typedef void (*m2644_ftn) (t518 *, t519*);
	static m2644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, p0);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
void t518_marshal(const t518& unmarshaled, t518_marshaled& marshaled)
{
	marshaled.f0 = unmarshaled.f0;
}
void t518_marshal_back(const t518_marshaled& marshaled, t518& unmarshaled)
{
	unmarshaled.f0 = marshaled.f0;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
void t518_marshal_cleanup(t518_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.AnimationCurve
extern Il2CppType t35_0_0_3;
FieldInfo t518_f0_FieldInfo = 
{
	"m_Ptr", &t35_0_0_3, &t518_TI, offsetof(t518, f0), &EmptyCustomAttributesCache};
static FieldInfo* t518_FIs[] =
{
	&t518_f0_FieldInfo,
	NULL
};
extern Il2CppType t519_0_0_0;
extern Il2CppType t519_0_0_0;
extern CustomAttributesCache t518__CustomAttributeCache_t518_m2640_Arg0_ParameterInfo;
static ParameterInfo t518_m2640_ParameterInfos[] = 
{
	{"keys", 0, 134218606, &t518__CustomAttributeCache_t518_m2640_Arg0_ParameterInfo, &t519_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2640_MI = 
{
	".ctor", (methodPointerType)&m2640, &t518_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t518_m2640_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 870, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2641_MI = 
{
	".ctor", (methodPointerType)&m2641, &t518_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 871, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t518__CustomAttributeCache_m2642;
MethodInfo m2642_MI = 
{
	"Cleanup", (methodPointerType)&m2642, &t518_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t518__CustomAttributeCache_m2642, 129, 4096, 255, 0, false, false, 872, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2643_MI = 
{
	"Finalize", (methodPointerType)&m2643, &t518_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 1, 0, false, false, 873, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t519_0_0_0;
static ParameterInfo t518_m2644_ParameterInfos[] = 
{
	{"keys", 0, 134218607, &EmptyCustomAttributesCache, &t519_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t518__CustomAttributeCache_m2644;
MethodInfo m2644_MI = 
{
	"Init", (methodPointerType)&m2644, &t518_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t518_m2644_ParameterInfos, &t518__CustomAttributeCache_m2644, 129, 4096, 255, 1, false, false, 874, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t518_MIs[] =
{
	&m2640_MI,
	&m2641_MI,
	&m2642_MI,
	&m2643_MI,
	&m2644_MI,
	NULL
};
static MethodInfo* t518_VT[] =
{
	&m1321_MI,
	&m2643_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t425_TI;
#include "t425.h"
#include "t425MD.h"
extern MethodInfo m2025_MI;
void t518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t425 * tmp;
		tmp = (t425 *)il2cpp_codegen_object_new (&t425_TI);
		m2025(tmp, il2cpp_codegen_string_new_wrapper("Item"), &m2025_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t391_TI;
#include "t391.h"
#include "t391MD.h"
extern MethodInfo m1817_MI;
void t518_CustomAttributesCacheGenerator_t518_m2640_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t391 * tmp;
		tmp = (t391 *)il2cpp_codegen_object_new (&t391_TI);
		m1817(tmp, &m1817_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t518_CustomAttributesCacheGenerator_m2642(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t518_CustomAttributesCacheGenerator_m2644(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t518__CustomAttributeCache = {
1,
NULL,
&t518_CustomAttributesCacheGenerator
};
CustomAttributesCache t518__CustomAttributeCache_t518_m2640_Arg0_ParameterInfo = {
1,
NULL,
&t518_CustomAttributesCacheGenerator_t518_m2640_Arg0_ParameterInfo
};
CustomAttributesCache t518__CustomAttributeCache_m2642 = {
1,
NULL,
&t518_CustomAttributesCacheGenerator_m2642
};
CustomAttributesCache t518__CustomAttributeCache_m2644 = {
1,
NULL,
&t518_CustomAttributesCacheGenerator_m2644
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t518_0_0_0;
extern Il2CppType t518_1_0_0;
struct t518;
extern CustomAttributesCache t518__CustomAttributeCache;
extern CustomAttributesCache t518__CustomAttributeCache_m2642;
extern CustomAttributesCache t518__CustomAttributeCache_m2644;
TypeInfo t518_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimationCurve", "UnityEngine", t518_MIs, NULL, t518_FIs, NULL, &t29_TI, NULL, NULL, &t518_TI, NULL, t518_VT, &t518__CustomAttributeCache, &t518_TI, &t518_0_0_0, &t518_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t518_marshal, (methodPointerType)t518_marshal_back, (methodPointerType)t518_marshal_cleanup, sizeof (t518), 0, sizeof(t518_marshaled), 0, 0, -1, 1048841, 0, false, false, false, false, false, false, false, false, true, false, false, false, 5, 0, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t514_TI;
#include "t514MD.h"



// Metadata Definition UnityEngine.AnimationState
static MethodInfo* t514_MIs[] =
{
	NULL
};
extern MethodInfo m2779_MI;
extern MethodInfo m2780_MI;
static MethodInfo* t514_VT[] =
{
	&m2779_MI,
	&m46_MI,
	&m2780_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t514_0_0_0;
extern Il2CppType t514_1_0_0;
extern TypeInfo t520_TI;
struct t514;
TypeInfo t514_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimationState", "UnityEngine", t514_MIs, NULL, NULL, NULL, &t520_TI, NULL, NULL, &t514_TI, NULL, t514_VT, &EmptyCustomAttributesCache, &t514_TI, &t514_0_0_0, &t514_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t514), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t516_TI;
#include "t516MD.h"



// Metadata Definition UnityEngine.AnimatorClipInfo
extern Il2CppType t44_0_0_1;
FieldInfo t516_f0_FieldInfo = 
{
	"m_ClipInstanceID", &t44_0_0_1, &t516_TI, offsetof(t516, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t516_f1_FieldInfo = 
{
	"m_Weight", &t22_0_0_1, &t516_TI, offsetof(t516, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t516_FIs[] =
{
	&t516_f0_FieldInfo,
	&t516_f1_FieldInfo,
	NULL
};
static MethodInfo* t516_MIs[] =
{
	NULL
};
static MethodInfo* t516_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t516_0_0_0;
extern Il2CppType t516_1_0_0;
TypeInfo t516_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimatorClipInfo", "UnityEngine", t516_MIs, NULL, t516_FIs, NULL, &t110_TI, NULL, NULL, &t516_TI, NULL, t516_VT, &EmptyCustomAttributesCache, &t516_TI, &t516_0_0_0, &t516_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t516)+ sizeof (Il2CppObject), 0, sizeof(t516 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t515_TI;
#include "t515MD.h"

#include "t229MD.h"
extern MethodInfo m2663_MI;


extern MethodInfo m2645_MI;
 bool m2645 (t515 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = m2663(NULL, p0, &m2663_MI);
		V_0 = L_0;
		int32_t L_1 = (__this->f2);
		if ((((int32_t)V_0) == ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = (__this->f0);
		if ((((int32_t)V_0) == ((int32_t)L_2)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = (__this->f1);
		G_B4_0 = ((((int32_t)V_0) == ((int32_t)L_3))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
extern MethodInfo m2646_MI;
 int32_t m2646 (t515 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2647_MI;
 int32_t m2647 (t515 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2648_MI;
 int32_t m2648 (t515 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2649_MI;
 float m2649 (t515 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m2650_MI;
 float m2650 (t515 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m2651_MI;
 int32_t m2651 (t515 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m2652_MI;
 bool m2652 (t515 * __this, t7* p0, MethodInfo* method){
	{
		int32_t L_0 = m2663(NULL, p0, &m2663_MI);
		int32_t L_1 = (__this->f5);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m2653_MI;
 bool m2653 (t515 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f6);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Metadata Definition UnityEngine.AnimatorStateInfo
extern Il2CppType t44_0_0_1;
FieldInfo t515_f0_FieldInfo = 
{
	"m_Name", &t44_0_0_1, &t515_TI, offsetof(t515, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t515_f1_FieldInfo = 
{
	"m_Path", &t44_0_0_1, &t515_TI, offsetof(t515, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t515_f2_FieldInfo = 
{
	"m_FullPath", &t44_0_0_1, &t515_TI, offsetof(t515, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t515_f3_FieldInfo = 
{
	"m_NormalizedTime", &t22_0_0_1, &t515_TI, offsetof(t515, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t515_f4_FieldInfo = 
{
	"m_Length", &t22_0_0_1, &t515_TI, offsetof(t515, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t515_f5_FieldInfo = 
{
	"m_Tag", &t44_0_0_1, &t515_TI, offsetof(t515, f5) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t515_f6_FieldInfo = 
{
	"m_Loop", &t44_0_0_1, &t515_TI, offsetof(t515, f6) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t515_FIs[] =
{
	&t515_f0_FieldInfo,
	&t515_f1_FieldInfo,
	&t515_f2_FieldInfo,
	&t515_f3_FieldInfo,
	&t515_f4_FieldInfo,
	&t515_f5_FieldInfo,
	&t515_f6_FieldInfo,
	NULL
};
static PropertyInfo t515____fullPathHash_PropertyInfo = 
{
	&t515_TI, "fullPathHash", &m2646_MI, NULL, 0, &EmptyCustomAttributesCache};
extern CustomAttributesCache t515__CustomAttributeCache_t515____nameHash_PropertyInfo;
static PropertyInfo t515____nameHash_PropertyInfo = 
{
	&t515_TI, "nameHash", &m2647_MI, NULL, 0, &t515__CustomAttributeCache_t515____nameHash_PropertyInfo};
static PropertyInfo t515____shortNameHash_PropertyInfo = 
{
	&t515_TI, "shortNameHash", &m2648_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t515____normalizedTime_PropertyInfo = 
{
	&t515_TI, "normalizedTime", &m2649_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t515____length_PropertyInfo = 
{
	&t515_TI, "length", &m2650_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t515____tagHash_PropertyInfo = 
{
	&t515_TI, "tagHash", &m2651_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t515____loop_PropertyInfo = 
{
	&t515_TI, "loop", &m2653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t515_PIs[] =
{
	&t515____fullPathHash_PropertyInfo,
	&t515____nameHash_PropertyInfo,
	&t515____shortNameHash_PropertyInfo,
	&t515____normalizedTime_PropertyInfo,
	&t515____length_PropertyInfo,
	&t515____tagHash_PropertyInfo,
	&t515____loop_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t515_m2645_ParameterInfos[] = 
{
	{"name", 0, 134218608, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2645_MI = 
{
	"IsName", (methodPointerType)&m2645, &t515_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t515_m2645_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 875, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2646_MI = 
{
	"get_fullPathHash", (methodPointerType)&m2646, &t515_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 876, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2647_MI = 
{
	"get_nameHash", (methodPointerType)&m2647, &t515_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 877, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2648_MI = 
{
	"get_shortNameHash", (methodPointerType)&m2648, &t515_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 878, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2649_MI = 
{
	"get_normalizedTime", (methodPointerType)&m2649, &t515_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 879, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2650_MI = 
{
	"get_length", (methodPointerType)&m2650, &t515_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 880, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2651_MI = 
{
	"get_tagHash", (methodPointerType)&m2651, &t515_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 881, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t515_m2652_ParameterInfos[] = 
{
	{"tag", 0, 134218609, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2652_MI = 
{
	"IsTag", (methodPointerType)&m2652, &t515_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t515_m2652_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 882, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2653_MI = 
{
	"get_loop", (methodPointerType)&m2653, &t515_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 883, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t515_MIs[] =
{
	&m2645_MI,
	&m2646_MI,
	&m2647_MI,
	&m2648_MI,
	&m2649_MI,
	&m2650_MI,
	&m2651_MI,
	&m2652_MI,
	&m2653_MI,
	NULL
};
static MethodInfo* t515_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
void t515_CustomAttributesCacheGenerator_t515____nameHash_PropertyInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("Use AnimatorStateInfo.fullPathHash instead."), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t515__CustomAttributeCache_t515____nameHash_PropertyInfo = {
1,
NULL,
&t515_CustomAttributesCacheGenerator_t515____nameHash_PropertyInfo
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t515_0_0_0;
extern Il2CppType t515_1_0_0;
extern CustomAttributesCache t515__CustomAttributeCache_t515____nameHash_PropertyInfo;
TypeInfo t515_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimatorStateInfo", "UnityEngine", t515_MIs, t515_PIs, t515_FIs, NULL, &t110_TI, NULL, NULL, &t515_TI, NULL, t515_VT, &EmptyCustomAttributesCache, &t515_TI, &t515_0_0_0, &t515_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t515)+ sizeof (Il2CppObject), 0, sizeof(t515 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 9, 7, 7, 0, 0, 4, 0, 0};
#include "t521.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t521_TI;
#include "t521MD.h"



extern MethodInfo m2654_MI;
 bool m2654 (t521 * __this, t7* p0, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = m2663(NULL, p0, &m2663_MI);
		int32_t L_1 = (__this->f2);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = m2663(NULL, p0, &m2663_MI);
		int32_t L_3 = (__this->f0);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern MethodInfo m2655_MI;
 bool m2655 (t521 * __this, t7* p0, MethodInfo* method){
	{
		int32_t L_0 = m2663(NULL, p0, &m2663_MI);
		int32_t L_1 = (__this->f1);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m2656_MI;
 int32_t m2656 (t521 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2657_MI;
 int32_t m2657 (t521 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2658_MI;
 int32_t m2658 (t521 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2659_MI;
 float m2659 (t521 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m2660_MI;
 bool m2660 (t521 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m2661_MI;
 bool m2661 (t521 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m2662_MI;
 bool m2662 (t521 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
void t521_marshal(const t521& unmarshaled, t521_marshaled& marshaled)
{
	marshaled.f0 = unmarshaled.f0;
	marshaled.f1 = unmarshaled.f1;
	marshaled.f2 = unmarshaled.f2;
	marshaled.f3 = unmarshaled.f3;
	marshaled.f4 = unmarshaled.f4;
	marshaled.f5 = unmarshaled.f5;
}
void t521_marshal_back(const t521_marshaled& marshaled, t521& unmarshaled)
{
	unmarshaled.f0 = marshaled.f0;
	unmarshaled.f1 = marshaled.f1;
	unmarshaled.f2 = marshaled.f2;
	unmarshaled.f3 = marshaled.f3;
	unmarshaled.f4 = marshaled.f4;
	unmarshaled.f5 = marshaled.f5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
void t521_marshal_cleanup(t521_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.AnimatorTransitionInfo
extern Il2CppType t44_0_0_1;
FieldInfo t521_f0_FieldInfo = 
{
	"m_FullPath", &t44_0_0_1, &t521_TI, offsetof(t521, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t521_f1_FieldInfo = 
{
	"m_UserName", &t44_0_0_1, &t521_TI, offsetof(t521, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t521_f2_FieldInfo = 
{
	"m_Name", &t44_0_0_1, &t521_TI, offsetof(t521, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t521_f3_FieldInfo = 
{
	"m_NormalizedTime", &t22_0_0_1, &t521_TI, offsetof(t521, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t521_f4_FieldInfo = 
{
	"m_AnyState", &t40_0_0_1, &t521_TI, offsetof(t521, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t521_f5_FieldInfo = 
{
	"m_TransitionType", &t44_0_0_1, &t521_TI, offsetof(t521, f5) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t521_FIs[] =
{
	&t521_f0_FieldInfo,
	&t521_f1_FieldInfo,
	&t521_f2_FieldInfo,
	&t521_f3_FieldInfo,
	&t521_f4_FieldInfo,
	&t521_f5_FieldInfo,
	NULL
};
static PropertyInfo t521____fullPathHash_PropertyInfo = 
{
	&t521_TI, "fullPathHash", &m2656_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t521____nameHash_PropertyInfo = 
{
	&t521_TI, "nameHash", &m2657_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t521____userNameHash_PropertyInfo = 
{
	&t521_TI, "userNameHash", &m2658_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t521____normalizedTime_PropertyInfo = 
{
	&t521_TI, "normalizedTime", &m2659_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t521____anyState_PropertyInfo = 
{
	&t521_TI, "anyState", &m2660_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t521____entry_PropertyInfo = 
{
	&t521_TI, "entry", &m2661_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t521____exit_PropertyInfo = 
{
	&t521_TI, "exit", &m2662_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t521_PIs[] =
{
	&t521____fullPathHash_PropertyInfo,
	&t521____nameHash_PropertyInfo,
	&t521____userNameHash_PropertyInfo,
	&t521____normalizedTime_PropertyInfo,
	&t521____anyState_PropertyInfo,
	&t521____entry_PropertyInfo,
	&t521____exit_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t521_m2654_ParameterInfos[] = 
{
	{"name", 0, 134218610, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2654_MI = 
{
	"IsName", (methodPointerType)&m2654, &t521_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t521_m2654_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 884, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t521_m2655_ParameterInfos[] = 
{
	{"name", 0, 134218611, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2655_MI = 
{
	"IsUserName", (methodPointerType)&m2655, &t521_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t521_m2655_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 885, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2656_MI = 
{
	"get_fullPathHash", (methodPointerType)&m2656, &t521_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 886, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2657_MI = 
{
	"get_nameHash", (methodPointerType)&m2657, &t521_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 887, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2658_MI = 
{
	"get_userNameHash", (methodPointerType)&m2658, &t521_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 888, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2659_MI = 
{
	"get_normalizedTime", (methodPointerType)&m2659, &t521_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 889, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2660_MI = 
{
	"get_anyState", (methodPointerType)&m2660, &t521_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 890, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2661_MI = 
{
	"get_entry", (methodPointerType)&m2661, &t521_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 891, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2662_MI = 
{
	"get_exit", (methodPointerType)&m2662, &t521_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 892, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t521_MIs[] =
{
	&m2654_MI,
	&m2655_MI,
	&m2656_MI,
	&m2657_MI,
	&m2658_MI,
	&m2659_MI,
	&m2660_MI,
	&m2661_MI,
	&m2662_MI,
	NULL
};
static MethodInfo* t521_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t521_0_0_0;
extern Il2CppType t521_1_0_0;
TypeInfo t521_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimatorTransitionInfo", "UnityEngine", t521_MIs, t521_PIs, t521_FIs, NULL, &t110_TI, NULL, NULL, &t521_TI, NULL, t521_VT, &EmptyCustomAttributesCache, &t521_TI, &t521_0_0_0, &t521_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t521_marshal, (methodPointerType)t521_marshal_back, (methodPointerType)t521_marshal_cleanup, sizeof (t521)+ sizeof (Il2CppObject), 0, sizeof(t521_marshaled), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 9, 7, 6, 0, 0, 4, 0, 0};
#include "t229.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t229_TI;

#include "t398.h"
extern MethodInfo m2664_MI;
extern MethodInfo m2665_MI;


extern MethodInfo m1896_MI;
 void m1896 (t229 * __this, t7* p0, MethodInfo* method){
	{
		m2664(__this, p0, &m2664_MI);
		return;
	}
}
extern MethodInfo m1895_MI;
 void m1895 (t229 * __this, t7* p0, MethodInfo* method){
	{
		m2665(__this, p0, &m2665_MI);
		return;
	}
}
extern MethodInfo m1894_MI;
 t398 * m1894 (t229 * __this, MethodInfo* method){
	typedef t398 * (*m1894_ftn) (t229 *);
	static m1894_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1894_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
 int32_t m2663 (t29 * __this, t7* p0, MethodInfo* method){
	typedef int32_t (*m2663_ftn) (t7*);
	static m2663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(p0);
}
 void m2664 (t229 * __this, t7* p0, MethodInfo* method){
	typedef void (*m2664_ftn) (t229 *, t7*);
	static m2664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, p0);
}
 void m2665 (t229 * __this, t7* p0, MethodInfo* method){
	typedef void (*m2665_ftn) (t229 *, t7*);
	static m2665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, p0);
}
// Metadata Definition UnityEngine.Animator
static PropertyInfo t229____runtimeAnimatorController_PropertyInfo = 
{
	&t229_TI, "runtimeAnimatorController", &m1894_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t229_PIs[] =
{
	&t229____runtimeAnimatorController_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t229_m1896_ParameterInfos[] = 
{
	{"name", 0, 134218612, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1896_MI = 
{
	"SetTrigger", (methodPointerType)&m1896, &t229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t229_m1896_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 893, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t229_m1895_ParameterInfos[] = 
{
	{"name", 0, 134218613, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1895_MI = 
{
	"ResetTrigger", (methodPointerType)&m1895, &t229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t229_m1895_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 894, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t398_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t229__CustomAttributeCache_m1894;
MethodInfo m1894_MI = 
{
	"get_runtimeAnimatorController", (methodPointerType)&m1894, &t229_TI, &t398_0_0_0, RuntimeInvoker_t29, NULL, &t229__CustomAttributeCache_m1894, 2182, 4096, 255, 0, false, false, 895, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t229_m2663_ParameterInfos[] = 
{
	{"name", 0, 134218614, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t229__CustomAttributeCache_m2663;
MethodInfo m2663_MI = 
{
	"StringToHash", (methodPointerType)&m2663, &t229_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t229_m2663_ParameterInfos, &t229__CustomAttributeCache_m2663, 150, 4096, 255, 1, false, false, 896, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t229_m2664_ParameterInfos[] = 
{
	{"name", 0, 134218615, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t229__CustomAttributeCache_m2664;
MethodInfo m2664_MI = 
{
	"SetTriggerString", (methodPointerType)&m2664, &t229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t229_m2664_ParameterInfos, &t229__CustomAttributeCache_m2664, 129, 4096, 255, 1, false, false, 897, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t229_m2665_ParameterInfos[] = 
{
	{"name", 0, 134218616, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t229__CustomAttributeCache_m2665;
MethodInfo m2665_MI = 
{
	"ResetTriggerString", (methodPointerType)&m2665, &t229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t229_m2665_ParameterInfos, &t229__CustomAttributeCache_m2665, 129, 4096, 255, 1, false, false, 898, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t229_MIs[] =
{
	&m1896_MI,
	&m1895_MI,
	&m1894_MI,
	&m2663_MI,
	&m2664_MI,
	&m2665_MI,
	NULL
};
static MethodInfo* t229_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t229_CustomAttributesCacheGenerator_m1894(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t229_CustomAttributesCacheGenerator_m2663(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t229_CustomAttributesCacheGenerator_m2664(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t229_CustomAttributesCacheGenerator_m2665(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t229__CustomAttributeCache_m1894 = {
1,
NULL,
&t229_CustomAttributesCacheGenerator_m1894
};
CustomAttributesCache t229__CustomAttributeCache_m2663 = {
1,
NULL,
&t229_CustomAttributesCacheGenerator_m2663
};
CustomAttributesCache t229__CustomAttributeCache_m2664 = {
1,
NULL,
&t229_CustomAttributesCacheGenerator_m2664
};
CustomAttributesCache t229__CustomAttributeCache_m2665 = {
1,
NULL,
&t229_CustomAttributesCacheGenerator_m2665
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t229_0_0_0;
extern Il2CppType t229_1_0_0;
struct t229;
extern CustomAttributesCache t229__CustomAttributeCache_m1894;
extern CustomAttributesCache t229__CustomAttributeCache_m2663;
extern CustomAttributesCache t229__CustomAttributeCache_m2664;
extern CustomAttributesCache t229__CustomAttributeCache_m2665;
TypeInfo t229_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Animator", "UnityEngine", t229_MIs, t229_PIs, NULL, NULL, &t317_TI, NULL, NULL, &t229_TI, NULL, t229_VT, &EmptyCustomAttributesCache, &t229_TI, &t229_0_0_0, &t229_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t229), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 1, 0, 0, 0, 4, 0, 0};
#include "t522.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t522_TI;
#include "t522MD.h"



// Conversion methods for marshalling of: UnityEngine.SkeletonBone
void t522_marshal(const t522& unmarshaled, t522_marshaled& marshaled)
{
	marshaled.f0 = il2cpp_codegen_marshal_string(unmarshaled.f0);
	marshaled.f1 = unmarshaled.f1;
	marshaled.f2 = unmarshaled.f2;
	marshaled.f3 = unmarshaled.f3;
	marshaled.f4 = unmarshaled.f4;
}
void t522_marshal_back(const t522_marshaled& marshaled, t522& unmarshaled)
{
	unmarshaled.f0 = il2cpp_codegen_marshal_string_result(marshaled.f0);
	unmarshaled.f1 = marshaled.f1;
	unmarshaled.f2 = marshaled.f2;
	unmarshaled.f3 = marshaled.f3;
	unmarshaled.f4 = marshaled.f4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
void t522_marshal_cleanup(t522_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.f0);
	marshaled.f0 = NULL;
}
// Metadata Definition UnityEngine.SkeletonBone
extern Il2CppType t7_0_0_6;
FieldInfo t522_f0_FieldInfo = 
{
	"name", &t7_0_0_6, &t522_TI, offsetof(t522, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_6;
FieldInfo t522_f1_FieldInfo = 
{
	"position", &t23_0_0_6, &t522_TI, offsetof(t522, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t362_0_0_6;
FieldInfo t522_f2_FieldInfo = 
{
	"rotation", &t362_0_0_6, &t522_TI, offsetof(t522, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_6;
FieldInfo t522_f3_FieldInfo = 
{
	"scale", &t23_0_0_6, &t522_TI, offsetof(t522, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t522_f4_FieldInfo = 
{
	"transformModified", &t44_0_0_6, &t522_TI, offsetof(t522, f4) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t522_FIs[] =
{
	&t522_f0_FieldInfo,
	&t522_f1_FieldInfo,
	&t522_f2_FieldInfo,
	&t522_f3_FieldInfo,
	&t522_f4_FieldInfo,
	NULL
};
static MethodInfo* t522_MIs[] =
{
	NULL
};
static MethodInfo* t522_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t522_0_0_0;
extern Il2CppType t522_1_0_0;
TypeInfo t522_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SkeletonBone", "UnityEngine", t522_MIs, NULL, t522_FIs, NULL, &t110_TI, NULL, NULL, &t522_TI, NULL, t522_VT, &EmptyCustomAttributesCache, &t522_TI, &t522_0_0_0, &t522_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t522_marshal, (methodPointerType)t522_marshal_back, (methodPointerType)t522_marshal_cleanup, sizeof (t522)+ sizeof (Il2CppObject), 0, sizeof(t522_marshaled), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 4, 0, 0};
#include "t523.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t523_TI;
#include "t523MD.h"



// Metadata Definition UnityEngine.HumanLimit
extern Il2CppType t23_0_0_1;
FieldInfo t523_f0_FieldInfo = 
{
	"m_Min", &t23_0_0_1, &t523_TI, offsetof(t523, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_1;
FieldInfo t523_f1_FieldInfo = 
{
	"m_Max", &t23_0_0_1, &t523_TI, offsetof(t523, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_1;
FieldInfo t523_f2_FieldInfo = 
{
	"m_Center", &t23_0_0_1, &t523_TI, offsetof(t523, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_1;
FieldInfo t523_f3_FieldInfo = 
{
	"m_AxisLength", &t22_0_0_1, &t523_TI, offsetof(t523, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t523_f4_FieldInfo = 
{
	"m_UseDefaultValues", &t44_0_0_1, &t523_TI, offsetof(t523, f4) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t523_FIs[] =
{
	&t523_f0_FieldInfo,
	&t523_f1_FieldInfo,
	&t523_f2_FieldInfo,
	&t523_f3_FieldInfo,
	&t523_f4_FieldInfo,
	NULL
};
static MethodInfo* t523_MIs[] =
{
	NULL
};
static MethodInfo* t523_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t523_0_0_0;
extern Il2CppType t523_1_0_0;
TypeInfo t523_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HumanLimit", "UnityEngine", t523_MIs, NULL, t523_FIs, NULL, &t110_TI, NULL, NULL, &t523_TI, NULL, t523_VT, &EmptyCustomAttributesCache, &t523_TI, &t523_0_0_0, &t523_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t523)+ sizeof (Il2CppObject), 0, sizeof(t523 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 5, 0, 0, 4, 0, 0};
#include "t524.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t524_TI;
#include "t524MD.h"



extern MethodInfo m2666_MI;
 t7* m2666 (t524 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2667_MI;
 void m2667 (t524 * __this, t7* p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m2668_MI;
 t7* m2668 (t524 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2669_MI;
 void m2669 (t524 * __this, t7* p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
void t524_marshal(const t524& unmarshaled, t524_marshaled& marshaled)
{
	marshaled.f0 = il2cpp_codegen_marshal_string(unmarshaled.f0);
	marshaled.f1 = il2cpp_codegen_marshal_string(unmarshaled.f1);
	marshaled.f2 = unmarshaled.f2;
}
void t524_marshal_back(const t524_marshaled& marshaled, t524& unmarshaled)
{
	unmarshaled.f0 = il2cpp_codegen_marshal_string_result(marshaled.f0);
	unmarshaled.f1 = il2cpp_codegen_marshal_string_result(marshaled.f1);
	unmarshaled.f2 = marshaled.f2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
void t524_marshal_cleanup(t524_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.f0);
	marshaled.f0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.f1);
	marshaled.f1 = NULL;
}
// Metadata Definition UnityEngine.HumanBone
extern Il2CppType t7_0_0_1;
FieldInfo t524_f0_FieldInfo = 
{
	"m_BoneName", &t7_0_0_1, &t524_TI, offsetof(t524, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t524_f1_FieldInfo = 
{
	"m_HumanName", &t7_0_0_1, &t524_TI, offsetof(t524, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t523_0_0_6;
FieldInfo t524_f2_FieldInfo = 
{
	"limit", &t523_0_0_6, &t524_TI, offsetof(t524, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t524_FIs[] =
{
	&t524_f0_FieldInfo,
	&t524_f1_FieldInfo,
	&t524_f2_FieldInfo,
	NULL
};
static PropertyInfo t524____boneName_PropertyInfo = 
{
	&t524_TI, "boneName", &m2666_MI, &m2667_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t524____humanName_PropertyInfo = 
{
	&t524_TI, "humanName", &m2668_MI, &m2669_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t524_PIs[] =
{
	&t524____boneName_PropertyInfo,
	&t524____humanName_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2666_MI = 
{
	"get_boneName", (methodPointerType)&m2666, &t524_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 899, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t524_m2667_ParameterInfos[] = 
{
	{"value", 0, 134218617, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2667_MI = 
{
	"set_boneName", (methodPointerType)&m2667, &t524_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t524_m2667_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 900, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2668_MI = 
{
	"get_humanName", (methodPointerType)&m2668, &t524_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 901, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t524_m2669_ParameterInfos[] = 
{
	{"value", 0, 134218618, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2669_MI = 
{
	"set_humanName", (methodPointerType)&m2669, &t524_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t524_m2669_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 902, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t524_MIs[] =
{
	&m2666_MI,
	&m2667_MI,
	&m2668_MI,
	&m2669_MI,
	NULL
};
static MethodInfo* t524_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t524_0_0_0;
extern Il2CppType t524_1_0_0;
TypeInfo t524_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HumanBone", "UnityEngine", t524_MIs, t524_PIs, t524_FIs, NULL, &t110_TI, NULL, NULL, &t524_TI, NULL, t524_VT, &EmptyCustomAttributesCache, &t524_TI, &t524_0_0_0, &t524_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t524_marshal, (methodPointerType)t524_marshal_back, (methodPointerType)t524_marshal_cleanup, sizeof (t524)+ sizeof (Il2CppObject), 0, sizeof(t524_marshaled), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 4, 2, 3, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t398_TI;
#include "t398MD.h"



// Metadata Definition UnityEngine.RuntimeAnimatorController
static MethodInfo* t398_MIs[] =
{
	NULL
};
static MethodInfo* t398_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t398_0_0_0;
extern Il2CppType t398_1_0_0;
struct t398;
TypeInfo t398_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RuntimeAnimatorController", "UnityEngine", t398_MIs, NULL, NULL, NULL, &t41_TI, NULL, NULL, &t398_TI, NULL, t398_VT, &EmptyCustomAttributesCache, &t398_TI, &t398_0_0_0, &t398_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t398), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t525.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t525_TI;
#include "t525MD.h"

#include "t317MD.h"
extern MethodInfo m2479_MI;


extern MethodInfo m2670_MI;
 void m2670 (t525 * __this, MethodInfo* method){
	{
		m2479(__this, &m2479_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Terrain
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2670_MI = 
{
	".ctor", (methodPointerType)&m2670, &t525_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 903, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t525_MIs[] =
{
	&m2670_MI,
	NULL
};
static MethodInfo* t525_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t525_0_0_0;
extern Il2CppType t525_1_0_0;
struct t525;
TypeInfo t525_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Terrain", "UnityEngine", t525_MIs, NULL, NULL, NULL, &t317_TI, NULL, NULL, &t525_TI, NULL, t525_VT, &EmptyCustomAttributesCache, &t525_TI, &t525_0_0_0, &t525_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t525), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 0};
#include "t150.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t150_TI;
#include "t150MD.h"



// Metadata Definition UnityEngine.TextAnchor
extern Il2CppType t44_0_0_1542;
FieldInfo t150_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t150_TI, offsetof(t150, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f2_FieldInfo = 
{
	"UpperLeft", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f3_FieldInfo = 
{
	"UpperCenter", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f4_FieldInfo = 
{
	"UpperRight", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f5_FieldInfo = 
{
	"MiddleLeft", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f6_FieldInfo = 
{
	"MiddleCenter", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f7_FieldInfo = 
{
	"MiddleRight", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f8_FieldInfo = 
{
	"LowerLeft", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f9_FieldInfo = 
{
	"LowerCenter", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_32854;
FieldInfo t150_f10_FieldInfo = 
{
	"LowerRight", &t150_0_0_32854, &t150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t150_FIs[] =
{
	&t150_f1_FieldInfo,
	&t150_f2_FieldInfo,
	&t150_f3_FieldInfo,
	&t150_f4_FieldInfo,
	&t150_f5_FieldInfo,
	&t150_f6_FieldInfo,
	&t150_f7_FieldInfo,
	&t150_f8_FieldInfo,
	&t150_f9_FieldInfo,
	&t150_f10_FieldInfo,
	NULL
};
static const int32_t t150_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t150_f2_DefaultValue = 
{
	&t150_f2_FieldInfo, { (char*)&t150_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t150_f3_DefaultValue = 
{
	&t150_f3_FieldInfo, { (char*)&t150_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t150_f4_DefaultValue = 
{
	&t150_f4_FieldInfo, { (char*)&t150_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t150_f5_DefaultValue = 
{
	&t150_f5_FieldInfo, { (char*)&t150_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t150_f6_DefaultValue = 
{
	&t150_f6_FieldInfo, { (char*)&t150_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t150_f7_DefaultValue = 
{
	&t150_f7_FieldInfo, { (char*)&t150_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t150_f8_DefaultValue = 
{
	&t150_f8_FieldInfo, { (char*)&t150_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t150_f9_DefaultValue = 
{
	&t150_f9_FieldInfo, { (char*)&t150_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t150_f10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t150_f10_DefaultValue = 
{
	&t150_f10_FieldInfo, { (char*)&t150_f10_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t150_FDVs[] = 
{
	&t150_f2_DefaultValue,
	&t150_f3_DefaultValue,
	&t150_f4_DefaultValue,
	&t150_f5_DefaultValue,
	&t150_f6_DefaultValue,
	&t150_f7_DefaultValue,
	&t150_f8_DefaultValue,
	&t150_f9_DefaultValue,
	&t150_f10_DefaultValue,
	NULL
};
static MethodInfo* t150_MIs[] =
{
	NULL
};
static MethodInfo* t150_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t150_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t150_0_0_0;
extern Il2CppType t150_1_0_0;
TypeInfo t150_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextAnchor", "UnityEngine", t150_MIs, NULL, t150_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t150_VT, &EmptyCustomAttributesCache, &t44_TI, &t150_0_0_0, &t150_1_0_0, t150_IOs, NULL, NULL, t150_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t150)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 10, 0, 0, 23, 0, 3};
#include "t151.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t151_TI;
#include "t151MD.h"



// Metadata Definition UnityEngine.HorizontalWrapMode
extern Il2CppType t44_0_0_1542;
FieldInfo t151_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t151_TI, offsetof(t151, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t151_0_0_32854;
FieldInfo t151_f2_FieldInfo = 
{
	"Wrap", &t151_0_0_32854, &t151_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t151_0_0_32854;
FieldInfo t151_f3_FieldInfo = 
{
	"Overflow", &t151_0_0_32854, &t151_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t151_FIs[] =
{
	&t151_f1_FieldInfo,
	&t151_f2_FieldInfo,
	&t151_f3_FieldInfo,
	NULL
};
static const int32_t t151_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t151_f2_DefaultValue = 
{
	&t151_f2_FieldInfo, { (char*)&t151_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t151_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t151_f3_DefaultValue = 
{
	&t151_f3_FieldInfo, { (char*)&t151_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t151_FDVs[] = 
{
	&t151_f2_DefaultValue,
	&t151_f3_DefaultValue,
	NULL
};
static MethodInfo* t151_MIs[] =
{
	NULL
};
static MethodInfo* t151_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t151_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t151_0_0_0;
extern Il2CppType t151_1_0_0;
TypeInfo t151_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HorizontalWrapMode", "UnityEngine", t151_MIs, NULL, t151_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t151_VT, &EmptyCustomAttributesCache, &t44_TI, &t151_0_0_0, &t151_1_0_0, t151_IOs, NULL, NULL, t151_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t151)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t152.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t152_TI;
#include "t152MD.h"



// Metadata Definition UnityEngine.VerticalWrapMode
extern Il2CppType t44_0_0_1542;
FieldInfo t152_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t152_TI, offsetof(t152, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t152_0_0_32854;
FieldInfo t152_f2_FieldInfo = 
{
	"Truncate", &t152_0_0_32854, &t152_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t152_0_0_32854;
FieldInfo t152_f3_FieldInfo = 
{
	"Overflow", &t152_0_0_32854, &t152_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t152_FIs[] =
{
	&t152_f1_FieldInfo,
	&t152_f2_FieldInfo,
	&t152_f3_FieldInfo,
	NULL
};
static const int32_t t152_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t152_f2_DefaultValue = 
{
	&t152_f2_FieldInfo, { (char*)&t152_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t152_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t152_f3_DefaultValue = 
{
	&t152_f3_FieldInfo, { (char*)&t152_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t152_FDVs[] = 
{
	&t152_f2_DefaultValue,
	&t152_f3_DefaultValue,
	NULL
};
static MethodInfo* t152_MIs[] =
{
	NULL
};
static MethodInfo* t152_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t152_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t152_0_0_0;
extern Il2CppType t152_1_0_0;
TypeInfo t152_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "VerticalWrapMode", "UnityEngine", t152_MIs, NULL, t152_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t152_VT, &EmptyCustomAttributesCache, &t44_TI, &t152_0_0_0, &t152_1_0_0, t152_IOs, NULL, NULL, t152_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t152)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t526.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t526_TI;
#include "t526MD.h"

#include "t164.h"
extern TypeInfo t17_TI;
#include "t164MD.h"
#include "t17MD.h"
extern MethodInfo m1573_MI;
extern MethodInfo m1574_MI;
extern MethodInfo m1579_MI;
extern MethodInfo m1580_MI;
extern MethodInfo m62_MI;
extern MethodInfo m2679_MI;
extern MethodInfo m2682_MI;
extern MethodInfo m2680_MI;
extern MethodInfo m2681_MI;


extern MethodInfo m2671_MI;
 int32_t m2671 (t526 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return (((int32_t)L_0));
	}
}
extern MethodInfo m2672_MI;
 int32_t m2672 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f2);
		float L_1 = m1573(L_0, &m1573_MI);
		return (((int32_t)L_1));
	}
}
extern MethodInfo m2673_MI;
 int32_t m2673 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f2);
		float L_1 = m1574(L_0, &m1574_MI);
		return (((int32_t)((-L_1))));
	}
}
extern MethodInfo m2674_MI;
 int32_t m2674 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f2);
		float L_1 = m1579(L_0, &m1579_MI);
		return (((int32_t)L_1));
	}
}
extern MethodInfo m2675_MI;
 int32_t m2675 (t526 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f7);
		t164 * L_1 = &(__this->f2);
		float L_2 = m1580(L_1, &m1580_MI);
		t164 * L_3 = &(__this->f2);
		float L_4 = m1574(L_3, &m1574_MI);
		return ((int32_t)(L_0+(((int32_t)((float)(L_2+L_4))))));
	}
}
extern MethodInfo m2676_MI;
 int32_t m2676 (t526 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f7);
		t164 * L_1 = &(__this->f2);
		float L_2 = m1580(L_1, &m1580_MI);
		return ((int32_t)(L_0+(((int32_t)L_2))));
	}
}
extern MethodInfo m2677_MI;
 int32_t m2677 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f2);
		float L_1 = m1579(L_0, &m1579_MI);
		return (((int32_t)L_1));
	}
}
extern MethodInfo m2678_MI;
 int32_t m2678 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f2);
		float L_1 = m1579(L_0, &m1579_MI);
		t164 * L_2 = &(__this->f2);
		float L_3 = m1573(L_2, &m1573_MI);
		return (((int32_t)((float)(L_1+L_3))));
	}
}
 t17  m2679 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f1);
		float L_1 = m1579(L_0, &m1579_MI);
		t164 * L_2 = &(__this->f1);
		float L_3 = m1580(L_2, &m1580_MI);
		t17  L_4 = {0};
		m62(&L_4, L_1, L_3, &m62_MI);
		return L_4;
	}
}
 t17  m2680 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f1);
		float L_1 = m1579(L_0, &m1579_MI);
		t164 * L_2 = &(__this->f1);
		float L_3 = m1573(L_2, &m1573_MI);
		t164 * L_4 = &(__this->f1);
		float L_5 = m1580(L_4, &m1580_MI);
		t17  L_6 = {0};
		m62(&L_6, ((float)(L_1+L_3)), L_5, &m62_MI);
		return L_6;
	}
}
 t17  m2681 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f1);
		float L_1 = m1579(L_0, &m1579_MI);
		t164 * L_2 = &(__this->f1);
		float L_3 = m1573(L_2, &m1573_MI);
		t164 * L_4 = &(__this->f1);
		float L_5 = m1580(L_4, &m1580_MI);
		t164 * L_6 = &(__this->f1);
		float L_7 = m1574(L_6, &m1574_MI);
		t17  L_8 = {0};
		m62(&L_8, ((float)(L_1+L_3)), ((float)(L_5+L_7)), &m62_MI);
		return L_8;
	}
}
 t17  m2682 (t526 * __this, MethodInfo* method){
	{
		t164 * L_0 = &(__this->f1);
		float L_1 = m1579(L_0, &m1579_MI);
		t164 * L_2 = &(__this->f1);
		float L_3 = m1580(L_2, &m1580_MI);
		t164 * L_4 = &(__this->f1);
		float L_5 = m1574(L_4, &m1574_MI);
		t17  L_6 = {0};
		m62(&L_6, L_1, ((float)(L_3+L_5)), &m62_MI);
		return L_6;
	}
}
extern MethodInfo m2683_MI;
 t17  m2683 (t526 * __this, MethodInfo* method){
	t17  G_B3_0 = {0};
	{
		bool L_0 = (__this->f6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		t17  L_1 = m2679(__this, &m2679_MI);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		t17  L_2 = m2679(__this, &m2679_MI);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern MethodInfo m2684_MI;
 t17  m2684 (t526 * __this, MethodInfo* method){
	t17  G_B3_0 = {0};
	{
		bool L_0 = (__this->f6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		t17  L_1 = m2682(__this, &m2682_MI);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		t17  L_2 = m2680(__this, &m2680_MI);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern MethodInfo m2685_MI;
 t17  m2685 (t526 * __this, MethodInfo* method){
	t17  G_B3_0 = {0};
	{
		bool L_0 = (__this->f6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		t17  L_1 = m2681(__this, &m2681_MI);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		t17  L_2 = m2681(__this, &m2681_MI);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern MethodInfo m2686_MI;
 t17  m2686 (t526 * __this, MethodInfo* method){
	t17  G_B3_0 = {0};
	{
		bool L_0 = (__this->f6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		t17  L_1 = m2680(__this, &m2680_MI);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		t17  L_2 = m2682(__this, &m2682_MI);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
#include "t149.h"
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
void t526_marshal(const t526& unmarshaled, t526_marshaled& marshaled)
{
	marshaled.f0 = unmarshaled.f0;
	marshaled.f1 = unmarshaled.f1;
	marshaled.f2 = unmarshaled.f2;
	marshaled.f3 = unmarshaled.f3;
	marshaled.f4 = unmarshaled.f4;
	marshaled.f5 = unmarshaled.f5;
	marshaled.f6 = unmarshaled.f6;
	marshaled.f7 = unmarshaled.f7;
}
void t526_marshal_back(const t526_marshaled& marshaled, t526& unmarshaled)
{
	unmarshaled.f0 = marshaled.f0;
	unmarshaled.f1 = marshaled.f1;
	unmarshaled.f2 = marshaled.f2;
	unmarshaled.f3 = marshaled.f3;
	unmarshaled.f4 = marshaled.f4;
	unmarshaled.f5 = marshaled.f5;
	unmarshaled.f6 = marshaled.f6;
	unmarshaled.f7 = marshaled.f7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
void t526_marshal_cleanup(t526_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.CharacterInfo
extern Il2CppType t44_0_0_6;
FieldInfo t526_f0_FieldInfo = 
{
	"index", &t44_0_0_6, &t526_TI, offsetof(t526, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t164_0_0_6;
extern CustomAttributesCache t526__CustomAttributeCache_uv;
FieldInfo t526_f1_FieldInfo = 
{
	"uv", &t164_0_0_6, &t526_TI, offsetof(t526, f1) + sizeof(t29), &t526__CustomAttributeCache_uv};
extern Il2CppType t164_0_0_6;
extern CustomAttributesCache t526__CustomAttributeCache_vert;
FieldInfo t526_f2_FieldInfo = 
{
	"vert", &t164_0_0_6, &t526_TI, offsetof(t526, f2) + sizeof(t29), &t526__CustomAttributeCache_vert};
extern Il2CppType t22_0_0_6;
extern CustomAttributesCache t526__CustomAttributeCache_width;
FieldInfo t526_f3_FieldInfo = 
{
	"width", &t22_0_0_6, &t526_TI, offsetof(t526, f3) + sizeof(t29), &t526__CustomAttributeCache_width};
extern Il2CppType t44_0_0_6;
FieldInfo t526_f4_FieldInfo = 
{
	"size", &t44_0_0_6, &t526_TI, offsetof(t526, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t149_0_0_6;
FieldInfo t526_f5_FieldInfo = 
{
	"style", &t149_0_0_6, &t526_TI, offsetof(t526, f5) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
extern CustomAttributesCache t526__CustomAttributeCache_flipped;
FieldInfo t526_f6_FieldInfo = 
{
	"flipped", &t40_0_0_6, &t526_TI, offsetof(t526, f6) + sizeof(t29), &t526__CustomAttributeCache_flipped};
extern Il2CppType t44_0_0_1;
FieldInfo t526_f7_FieldInfo = 
{
	"ascent", &t44_0_0_1, &t526_TI, offsetof(t526, f7) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t526_FIs[] =
{
	&t526_f0_FieldInfo,
	&t526_f1_FieldInfo,
	&t526_f2_FieldInfo,
	&t526_f3_FieldInfo,
	&t526_f4_FieldInfo,
	&t526_f5_FieldInfo,
	&t526_f6_FieldInfo,
	&t526_f7_FieldInfo,
	NULL
};
static PropertyInfo t526____advance_PropertyInfo = 
{
	&t526_TI, "advance", &m2671_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____glyphWidth_PropertyInfo = 
{
	&t526_TI, "glyphWidth", &m2672_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____glyphHeight_PropertyInfo = 
{
	&t526_TI, "glyphHeight", &m2673_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____bearing_PropertyInfo = 
{
	&t526_TI, "bearing", &m2674_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____minY_PropertyInfo = 
{
	&t526_TI, "minY", &m2675_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____maxY_PropertyInfo = 
{
	&t526_TI, "maxY", &m2676_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____minX_PropertyInfo = 
{
	&t526_TI, "minX", &m2677_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____maxX_PropertyInfo = 
{
	&t526_TI, "maxX", &m2678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvBottomLeftUnFlipped_PropertyInfo = 
{
	&t526_TI, "uvBottomLeftUnFlipped", &m2679_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvBottomRightUnFlipped_PropertyInfo = 
{
	&t526_TI, "uvBottomRightUnFlipped", &m2680_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvTopRightUnFlipped_PropertyInfo = 
{
	&t526_TI, "uvTopRightUnFlipped", &m2681_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvTopLeftUnFlipped_PropertyInfo = 
{
	&t526_TI, "uvTopLeftUnFlipped", &m2682_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvBottomLeft_PropertyInfo = 
{
	&t526_TI, "uvBottomLeft", &m2683_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvBottomRight_PropertyInfo = 
{
	&t526_TI, "uvBottomRight", &m2684_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvTopRight_PropertyInfo = 
{
	&t526_TI, "uvTopRight", &m2685_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t526____uvTopLeft_PropertyInfo = 
{
	&t526_TI, "uvTopLeft", &m2686_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t526_PIs[] =
{
	&t526____advance_PropertyInfo,
	&t526____glyphWidth_PropertyInfo,
	&t526____glyphHeight_PropertyInfo,
	&t526____bearing_PropertyInfo,
	&t526____minY_PropertyInfo,
	&t526____maxY_PropertyInfo,
	&t526____minX_PropertyInfo,
	&t526____maxX_PropertyInfo,
	&t526____uvBottomLeftUnFlipped_PropertyInfo,
	&t526____uvBottomRightUnFlipped_PropertyInfo,
	&t526____uvTopRightUnFlipped_PropertyInfo,
	&t526____uvTopLeftUnFlipped_PropertyInfo,
	&t526____uvBottomLeft_PropertyInfo,
	&t526____uvBottomRight_PropertyInfo,
	&t526____uvTopRight_PropertyInfo,
	&t526____uvTopLeft_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2671_MI = 
{
	"get_advance", (methodPointerType)&m2671, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 904, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2672_MI = 
{
	"get_glyphWidth", (methodPointerType)&m2672, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 905, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2673_MI = 
{
	"get_glyphHeight", (methodPointerType)&m2673, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 906, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2674_MI = 
{
	"get_bearing", (methodPointerType)&m2674, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 907, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2675_MI = 
{
	"get_minY", (methodPointerType)&m2675, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 908, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2676_MI = 
{
	"get_maxY", (methodPointerType)&m2676, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 909, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2677_MI = 
{
	"get_minX", (methodPointerType)&m2677, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 910, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2678_MI = 
{
	"get_maxX", (methodPointerType)&m2678, &t526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 911, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2679_MI = 
{
	"get_uvBottomLeftUnFlipped", (methodPointerType)&m2679, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 912, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2680_MI = 
{
	"get_uvBottomRightUnFlipped", (methodPointerType)&m2680, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 913, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2681_MI = 
{
	"get_uvTopRightUnFlipped", (methodPointerType)&m2681, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 914, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2682_MI = 
{
	"get_uvTopLeftUnFlipped", (methodPointerType)&m2682, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 915, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2683_MI = 
{
	"get_uvBottomLeft", (methodPointerType)&m2683, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 916, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2684_MI = 
{
	"get_uvBottomRight", (methodPointerType)&m2684, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 917, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2685_MI = 
{
	"get_uvTopRight", (methodPointerType)&m2685, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 918, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2686_MI = 
{
	"get_uvTopLeft", (methodPointerType)&m2686, &t526_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 919, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t526_MIs[] =
{
	&m2671_MI,
	&m2672_MI,
	&m2673_MI,
	&m2674_MI,
	&m2675_MI,
	&m2676_MI,
	&m2677_MI,
	&m2678_MI,
	&m2679_MI,
	&m2680_MI,
	&m2681_MI,
	&m2682_MI,
	&m2683_MI,
	&m2684_MI,
	&m2685_MI,
	&m2686_MI,
	NULL
};
static MethodInfo* t526_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
void t526_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead."), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t526_CustomAttributesCacheGenerator_vert(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead."), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t526_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.width is deprecated. Use advance instead."), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t526_CustomAttributesCacheGenerator_flipped(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation."), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t526__CustomAttributeCache_uv = {
1,
NULL,
&t526_CustomAttributesCacheGenerator_uv
};
CustomAttributesCache t526__CustomAttributeCache_vert = {
1,
NULL,
&t526_CustomAttributesCacheGenerator_vert
};
CustomAttributesCache t526__CustomAttributeCache_width = {
1,
NULL,
&t526_CustomAttributesCacheGenerator_width
};
CustomAttributesCache t526__CustomAttributeCache_flipped = {
1,
NULL,
&t526_CustomAttributesCacheGenerator_flipped
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t526_0_0_0;
extern Il2CppType t526_1_0_0;
extern CustomAttributesCache t526__CustomAttributeCache_uv;
extern CustomAttributesCache t526__CustomAttributeCache_vert;
extern CustomAttributesCache t526__CustomAttributeCache_width;
extern CustomAttributesCache t526__CustomAttributeCache_flipped;
TypeInfo t526_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CharacterInfo", "UnityEngine", t526_MIs, t526_PIs, t526_FIs, NULL, &t110_TI, NULL, NULL, &t526_TI, NULL, t526_VT, &EmptyCustomAttributesCache, &t526_TI, &t526_0_0_0, &t526_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t526_marshal, (methodPointerType)t526_marshal_back, (methodPointerType)t526_marshal_cleanup, sizeof (t526)+ sizeof (Il2CppObject), 0, sizeof(t526_marshaled), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 16, 16, 8, 0, 0, 4, 0, 0};
#include "t527.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t527_TI;
#include "t527MD.h"



extern MethodInfo m2687_MI;
 void m2687 (t527 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m2688_MI;
 void m2688 (t527 * __this, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m2688((t527 *)__this->f9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,(MethodInfo*)(__this->f3.f0));
}
void pinvoke_delegate_wrapper_t527(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
extern MethodInfo m2689_MI;
 t29 * m2689 (t527 * __this, t67 * p0, t29 * p1, MethodInfo* method){
	void *__d_args[1] = {0};
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p0, (Il2CppObject*)p1);
}
extern MethodInfo m2690_MI;
 void m2690 (t527 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Font/FontTextureRebuildCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t527_m2687_ParameterInfos[] = 
{
	{"object", 0, 134218623, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218624, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m2687_MI = 
{
	".ctor", (methodPointerType)&m2687, &t527_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t527_m2687_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 927, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2688_MI = 
{
	"Invoke", (methodPointerType)&m2688, &t527_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 3, 10, 0, false, false, 928, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t527_m2689_ParameterInfos[] = 
{
	{"callback", 0, 134218625, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 1, 134218626, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2689_MI = 
{
	"BeginInvoke", (methodPointerType)&m2689, &t527_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29, t527_m2689_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 2, false, false, 929, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t527_m2690_ParameterInfos[] = 
{
	{"result", 0, 134218627, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2690_MI = 
{
	"EndInvoke", (methodPointerType)&m2690, &t527_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t527_m2690_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 930, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t527_MIs[] =
{
	&m2687_MI,
	&m2688_MI,
	&m2689_MI,
	&m2690_MI,
	NULL
};
static MethodInfo* t527_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2688_MI,
	&m2689_MI,
	&m2690_MI,
};
static Il2CppInterfaceOffsetPair t527_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern TypeInfo t606_TI;
#include "t606.h"
#include "t606MD.h"
extern MethodInfo m2886_MI;
void t527_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t606 * tmp;
		tmp = (t606 *)il2cpp_codegen_object_new (&t606_TI);
		m2886(tmp, 1, &m2886_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t527__CustomAttributeCache = {
1,
NULL,
&t527_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t527_0_0_0;
extern Il2CppType t527_1_0_0;
struct t527;
extern TypeInfo t148_TI;
extern CustomAttributesCache t527__CustomAttributeCache;
TypeInfo t527_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "FontTextureRebuildCallback", "", t527_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t148_TI, &t527_TI, NULL, t527_VT, &t527__CustomAttributeCache, &t527_TI, &t527_0_0_0, &t527_1_0_0, t527_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t527, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t527), 0, sizeof(methodPointerType), 0, 0, -1, 258, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t148.h"
#ifndef _MSC_VER
#else
#endif
#include "t148MD.h"

#include "t156.h"
#include "t351.h"
#include "t353.h"
#include "t194.h"
extern TypeInfo t351_TI;
#include "t353MD.h"
#include "t351MD.h"
extern MethodInfo m1597_MI;
extern MethodInfo m1598_MI;
extern MethodInfo m2902_MI;


extern MethodInfo m1546_MI;
 void m1546 (t29 * __this, t351 * p0, MethodInfo* method){
	{
		t353 * L_0 = m1597(NULL, (((t148_SFs*)InitializedTypeInfo(&t148_TI)->static_fields)->f2), p0, &m1597_MI);
		((t148_SFs*)InitializedTypeInfo(&t148_TI)->static_fields)->f2 = ((t351 *)Castclass(L_0, InitializedTypeInfo(&t351_TI)));
		return;
	}
}
extern MethodInfo m2691_MI;
 void m2691 (t29 * __this, t351 * p0, MethodInfo* method){
	{
		t353 * L_0 = m1598(NULL, (((t148_SFs*)InitializedTypeInfo(&t148_TI)->static_fields)->f2), p0, &m1598_MI);
		((t148_SFs*)InitializedTypeInfo(&t148_TI)->static_fields)->f2 = ((t351 *)Castclass(L_0, InitializedTypeInfo(&t351_TI)));
		return;
	}
}
extern MethodInfo m1914_MI;
 t156 * m1914 (t148 * __this, MethodInfo* method){
	typedef t156 * (*m1914_ftn) (t148 *);
	static m1914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1758_MI;
 bool m1758 (t148 * __this, uint16_t p0, MethodInfo* method){
	typedef bool (*m1758_ftn) (t148 *, uint16_t);
	static m1758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, p0);
}
extern MethodInfo m2692_MI;
 void m2692 (t29 * __this, t148 * p0, MethodInfo* method){
	t351 * V_0 = {0};
	{
		V_0 = (((t148_SFs*)InitializedTypeInfo(&t148_TI)->static_fields)->f2);
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker1< t148 * >::Invoke(&m2902_MI, V_0, p0);
	}

IL_0013:
	{
		t527 * L_0 = (p0->f3);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t527 * L_1 = (p0->f3);
		VirtActionInvoker0::Invoke(&m2688_MI, L_1);
	}

IL_0029:
	{
		return;
	}
}
extern MethodInfo m1916_MI;
 bool m1916 (t148 * __this, MethodInfo* method){
	typedef bool (*m1916_ftn) (t148 *);
	static m1916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1918_MI;
 int32_t m1918 (t148 * __this, MethodInfo* method){
	typedef int32_t (*m1918_ftn) (t148 *);
	static m1918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// Metadata Definition UnityEngine.Font
extern Il2CppType t351_0_0_17;
FieldInfo t148_f2_FieldInfo = 
{
	"textureRebuilt", &t351_0_0_17, &t148_TI, offsetof(t148_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t527_0_0_1;
FieldInfo t148_f3_FieldInfo = 
{
	"m_FontTextureRebuildCallback", &t527_0_0_1, &t148_TI, offsetof(t148, f3), &EmptyCustomAttributesCache};
static FieldInfo* t148_FIs[] =
{
	&t148_f2_FieldInfo,
	&t148_f3_FieldInfo,
	NULL
};
static PropertyInfo t148____material_PropertyInfo = 
{
	&t148_TI, "material", &m1914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t148____dynamic_PropertyInfo = 
{
	&t148_TI, "dynamic", &m1916_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t148____fontSize_PropertyInfo = 
{
	&t148_TI, "fontSize", &m1918_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t148_PIs[] =
{
	&t148____material_PropertyInfo,
	&t148____dynamic_PropertyInfo,
	&t148____fontSize_PropertyInfo,
	NULL
};
extern Il2CppType t351_0_0_0;
static EventInfo t148____textureRebuilt_EventInfo = 
{
	"textureRebuilt", &t351_0_0_0, &t148_TI, &m1546_MI, &m2691_MI, NULL, &EmptyCustomAttributesCache};
static EventInfo* t148__EventInfos[] =
{
	&t148____textureRebuilt_EventInfo,
	NULL
};
extern Il2CppType t351_0_0_0;
extern Il2CppType t351_0_0_0;
static ParameterInfo t148_m1546_ParameterInfos[] = 
{
	{"value", 0, 134218619, &EmptyCustomAttributesCache, &t351_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1546_MI = 
{
	"add_textureRebuilt", (methodPointerType)&m1546, &t148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t148_m1546_ParameterInfos, &EmptyCustomAttributesCache, 2198, 32, 255, 1, false, false, 920, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t351_0_0_0;
static ParameterInfo t148_m2691_ParameterInfos[] = 
{
	{"value", 0, 134218620, &EmptyCustomAttributesCache, &t351_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2691_MI = 
{
	"remove_textureRebuilt", (methodPointerType)&m2691, &t148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t148_m2691_ParameterInfos, &EmptyCustomAttributesCache, 2198, 32, 255, 1, false, false, 921, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t148__CustomAttributeCache_m1914;
MethodInfo m1914_MI = 
{
	"get_material", (methodPointerType)&m1914, &t148_TI, &t156_0_0_0, RuntimeInvoker_t29, NULL, &t148__CustomAttributeCache_m1914, 2182, 4096, 255, 0, false, false, 922, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t148_m1758_ParameterInfos[] = 
{
	{"c", 0, 134218621, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t148__CustomAttributeCache_m1758;
MethodInfo m1758_MI = 
{
	"HasCharacter", (methodPointerType)&m1758, &t148_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t148_m1758_ParameterInfos, &t148__CustomAttributeCache_m1758, 134, 4096, 255, 1, false, false, 923, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t148_m2692_ParameterInfos[] = 
{
	{"font", 0, 134218622, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2692_MI = 
{
	"InvokeTextureRebuilt_Internal", (methodPointerType)&m2692, &t148_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t148_m2692_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 924, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t148__CustomAttributeCache_m1916;
MethodInfo m1916_MI = 
{
	"get_dynamic", (methodPointerType)&m1916, &t148_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t148__CustomAttributeCache_m1916, 2182, 4096, 255, 0, false, false, 925, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t148__CustomAttributeCache_m1918;
MethodInfo m1918_MI = 
{
	"get_fontSize", (methodPointerType)&m1918, &t148_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t148__CustomAttributeCache_m1918, 2182, 4096, 255, 0, false, false, 926, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t148_MIs[] =
{
	&m1546_MI,
	&m2691_MI,
	&m1914_MI,
	&m1758_MI,
	&m2692_MI,
	&m1916_MI,
	&m1918_MI,
	NULL
};
extern TypeInfo t527_TI;
static TypeInfo* t148_TI__nestedTypes[2] =
{
	&t527_TI,
	NULL
};
static MethodInfo* t148_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t148_CustomAttributesCacheGenerator_m1914(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t148_CustomAttributesCacheGenerator_m1758(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t148_CustomAttributesCacheGenerator_m1916(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t148_CustomAttributesCacheGenerator_m1918(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t148__CustomAttributeCache_m1914 = {
1,
NULL,
&t148_CustomAttributesCacheGenerator_m1914
};
CustomAttributesCache t148__CustomAttributeCache_m1758 = {
1,
NULL,
&t148_CustomAttributesCacheGenerator_m1758
};
CustomAttributesCache t148__CustomAttributeCache_m1916 = {
1,
NULL,
&t148_CustomAttributesCacheGenerator_m1916
};
CustomAttributesCache t148__CustomAttributeCache_m1918 = {
1,
NULL,
&t148_CustomAttributesCacheGenerator_m1918
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t148_1_0_0;
struct t148;
extern CustomAttributesCache t148__CustomAttributeCache_m1914;
extern CustomAttributesCache t148__CustomAttributeCache_m1758;
extern CustomAttributesCache t148__CustomAttributeCache_m1916;
extern CustomAttributesCache t148__CustomAttributeCache_m1918;
TypeInfo t148_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Font", "UnityEngine", t148_MIs, t148_PIs, t148_FIs, t148__EventInfos, &t41_TI, t148_TI__nestedTypes, NULL, &t148_TI, NULL, t148_VT, &EmptyCustomAttributesCache, &t148_TI, &t148_0_0_0, &t148_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t148), 0, -1, sizeof(t148_SFs), 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 3, 2, 1, 1, 4, 0, 0};
#include "t381.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t381_TI;
#include "t381MD.h"



// Metadata Definition UnityEngine.UICharInfo
extern Il2CppType t17_0_0_6;
FieldInfo t381_f0_FieldInfo = 
{
	"cursorPos", &t17_0_0_6, &t381_TI, offsetof(t381, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_6;
FieldInfo t381_f1_FieldInfo = 
{
	"charWidth", &t22_0_0_6, &t381_TI, offsetof(t381, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t381_FIs[] =
{
	&t381_f0_FieldInfo,
	&t381_f1_FieldInfo,
	NULL
};
static MethodInfo* t381_MIs[] =
{
	NULL
};
static MethodInfo* t381_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_1_0_0;
TypeInfo t381_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UICharInfo", "UnityEngine", t381_MIs, NULL, t381_FIs, NULL, &t110_TI, NULL, NULL, &t381_TI, NULL, t381_VT, &EmptyCustomAttributesCache, &t381_TI, &t381_0_0_0, &t381_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t381)+ sizeof (Il2CppObject), 0, sizeof(t381 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 2, 0, 0, 4, 0, 0};
#include "t380.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t380_TI;
#include "t380MD.h"



// Metadata Definition UnityEngine.UILineInfo
extern Il2CppType t44_0_0_6;
FieldInfo t380_f0_FieldInfo = 
{
	"startCharIdx", &t44_0_0_6, &t380_TI, offsetof(t380, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t380_f1_FieldInfo = 
{
	"height", &t44_0_0_6, &t380_TI, offsetof(t380, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t380_FIs[] =
{
	&t380_f0_FieldInfo,
	&t380_f1_FieldInfo,
	NULL
};
static MethodInfo* t380_MIs[] =
{
	NULL
};
static MethodInfo* t380_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_1_0_0;
TypeInfo t380_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UILineInfo", "UnityEngine", t380_MIs, NULL, t380_FIs, NULL, &t110_TI, NULL, NULL, &t380_TI, NULL, t380_VT, &EmptyCustomAttributesCache, &t380_TI, &t380_0_0_0, &t380_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t380)+ sizeof (Il2CppObject), 0, sizeof(t380 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 2, 0, 0, 4, 0, 0};
#include "t202.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t202_TI;
#include "t202MD.h"

#include "t163.h"
#include "t528.h"
#include "t529.h"
#include "t184.h"
#include "t238.h"
extern TypeInfo t27_TI;
extern TypeInfo t163_TI;
extern TypeInfo t528_TI;
extern TypeInfo t529_TI;
extern TypeInfo t324_TI;
extern TypeInfo t238_TI;
#include "t27MD.h"
#include "t163MD.h"
#include "t528MD.h"
#include "t529MD.h"
#include "t238MD.h"
extern MethodInfo m1772_MI;
extern MethodInfo m1715_MI;
extern MethodInfo m2699_MI;
extern MethodInfo m1763_MI;
extern MethodInfo m1765_MI;
extern MethodInfo m2711_MI;
extern MethodInfo m2709_MI;
extern MethodInfo m2710_MI;
extern MethodInfo m1912_MI;
extern MethodInfo m2903_MI;
extern MethodInfo m2904_MI;
extern MethodInfo m2905_MI;
extern MethodInfo m2694_MI;
extern MethodInfo m2695_MI;
extern MethodInfo m2697_MI;
extern MethodInfo m2698_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m2505_MI;
extern MethodInfo m2703_MI;
extern MethodInfo m2705_MI;
extern MethodInfo m2700_MI;
extern MethodInfo m1774_MI;
extern MethodInfo m1776_MI;
extern MethodInfo m1713_MI;
extern MethodInfo m2778_MI;
extern MethodInfo m2712_MI;
extern MethodInfo m2708_MI;
extern MethodInfo m2696_MI;


extern MethodInfo m1709_MI;
 void m1709 (t202 * __this, MethodInfo* method){
	{
		m1912(__this, ((int32_t)50), &m1912_MI);
		return;
	}
}
 void m1912 (t202 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t163_TI));
		t163 * L_0 = (t163 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t163_TI));
		m2903(L_0, ((int32_t)((int32_t)((int32_t)(p0+1))*(int32_t)4)), &m2903_MI);
		__this->f5 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t528_TI));
		t528 * L_1 = (t528 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t528_TI));
		m2904(L_1, ((int32_t)(p0+1)), &m2904_MI);
		__this->f6 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t529_TI));
		t529 * L_2 = (t529 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t529_TI));
		m2905(L_2, ((int32_t)20), &m2905_MI);
		__this->f7 = L_2;
		m2694(__this, &m2694_MI);
		return;
	}
}
extern MethodInfo m2693_MI;
 void m2693 (t202 * __this, MethodInfo* method){
	{
		m2695(__this, &m2695_MI);
		return;
	}
}
 void m2694 (t202 * __this, MethodInfo* method){
	typedef void (*m2694_ftn) (t202 *);
	static m2694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
 void m2695 (t202 * __this, MethodInfo* method){
	typedef void (*m2695_ftn) (t202 *);
	static m2695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
 bool m2696 (t202 * __this, t7* p0, t148 * p1, t132  p2, int32_t p3, float p4, int32_t p5, bool p6, bool p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, bool p12, int32_t p13, t17  p14, t17  p15, bool p16, MethodInfo* method){
	{
		float L_0 = ((&p14)->f1);
		float L_1 = ((&p14)->f2);
		float L_2 = ((&p15)->f1);
		float L_3 = ((&p15)->f2);
		bool L_4 = m2697(__this, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, L_0, L_1, L_2, L_3, p16, &m2697_MI);
		return L_4;
	}
}
 bool m2697 (t202 * __this, t7* p0, t148 * p1, t132  p2, int32_t p3, float p4, int32_t p5, bool p6, bool p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, bool p12, int32_t p13, float p14, float p15, float p16, float p17, bool p18, MethodInfo* method){
	{
		bool L_0 = m2698(NULL, __this, p0, p1, (&p2), p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, &m2698_MI);
		return L_0;
	}
}
 bool m2698 (t29 * __this, t202 * p0, t7* p1, t148 * p2, t132 * p3, int32_t p4, float p5, int32_t p6, bool p7, bool p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, bool p13, int32_t p14, float p15, float p16, float p17, float p18, bool p19, MethodInfo* method){
	typedef bool (*m2698_ftn) (t202 *, t7*, t148 *, t132 *, int32_t, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static m2698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19);
}
 t164  m1776 (t202 * __this, MethodInfo* method){
	typedef t164  (*m1776_ftn) (t202 *);
	static m1776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_rectExtents()");
	return _il2cpp_icall_func(__this);
}
 int32_t m2699 (t202 * __this, MethodInfo* method){
	typedef int32_t (*m2699_ftn) (t202 *);
	static m2699_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2699_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
 void m2700 (t202 * __this, t29 * p0, MethodInfo* method){
	typedef void (*m2700_ftn) (t202 *, t29 *);
	static m2700_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2700_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m2701_MI;
 t201* m2701 (t202 * __this, MethodInfo* method){
	typedef t201* (*m2701_ftn) (t202 *);
	static m2701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m2702_MI;
 int32_t m2702 (t202 * __this, MethodInfo* method){
	typedef int32_t (*m2702_ftn) (t202 *);
	static m2702_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2702_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1751_MI;
 int32_t m1751 (t202 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t7* L_0 = (__this->f1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_1 = m1772(NULL, L_0, &m1772_MI);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		t7* L_2 = (__this->f1);
		int32_t L_3 = m1715(L_2, &m1715_MI);
		int32_t L_4 = m2699(__this, &m2699_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_5 = m1763(NULL, 0, ((int32_t)((int32_t)((int32_t)(L_4-4))/(int32_t)4)), &m1763_MI);
		int32_t L_6 = m1765(NULL, L_3, L_5, &m1765_MI);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
 void m2703 (t202 * __this, t29 * p0, MethodInfo* method){
	typedef void (*m2703_ftn) (t202 *, t29 *);
	static m2703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m2704_MI;
 t530* m2704 (t202 * __this, MethodInfo* method){
	typedef t530* (*m2704_ftn) (t202 *);
	static m2704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1750_MI;
 int32_t m1750 (t202 * __this, MethodInfo* method){
	typedef int32_t (*m1750_ftn) (t202 *);
	static m1750_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1750_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
 void m2705 (t202 * __this, t29 * p0, MethodInfo* method){
	typedef void (*m2705_ftn) (t202 *, t29 *);
	static m2705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m2706_MI;
 t531* m2706 (t202 * __this, MethodInfo* method){
	typedef t531* (*m2706_ftn) (t202 *);
	static m2706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1801_MI;
 int32_t m1801 (t202 * __this, MethodInfo* method){
	typedef int32_t (*m1801_ftn) (t202 *);
	static m1801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m2707_MI;
 void m2707 (t202 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(&m1428_MI, __this);
		// IL_0006: leave IL_0012
		leaveInstructions[0] = 0x12; // 1
		THROW_SENTINEL(IL_0012);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_000b;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_000b;
	}

IL_000b:
	{ // begin finally (depth: 1)
		m46(__this, &m46_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x12:
				goto IL_0012;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0012:
	{
		return;
	}
}
 t238  m2708 (t202 * __this, t238  p0, MethodInfo* method){
	{
		t148 * L_0 = ((&p0)->f0);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		t148 * L_2 = ((&p0)->f0);
		bool L_3 = m1916(L_2, &m1916_MI);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		return p0;
	}

IL_0025:
	{
		int32_t L_4 = ((&p0)->f2);
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_5 = ((&p0)->f5);
		if (!L_5)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		m2505(NULL, (t7*) &_stringLiteral149, &m2505_MI);
		(&p0)->f2 = 0;
		(&p0)->f5 = 0;
	}

IL_0057:
	{
		bool L_6 = ((&p0)->f7);
		if (!L_6)
		{
			goto IL_0075;
		}
	}
	{
		m2505(NULL, (t7*) &_stringLiteral150, &m2505_MI);
		(&p0)->f7 = 0;
	}

IL_0075:
	{
		return p0;
	}
}
extern MethodInfo m1921_MI;
 void m1921 (t202 * __this, MethodInfo* method){
	{
		__this->f3 = 0;
		return;
	}
}
 void m2709 (t202 * __this, t528 * p0, MethodInfo* method){
	{
		m2703(__this, p0, &m2703_MI);
		return;
	}
}
 void m2710 (t202 * __this, t529 * p0, MethodInfo* method){
	{
		m2705(__this, p0, &m2705_MI);
		return;
	}
}
 void m2711 (t202 * __this, t163 * p0, MethodInfo* method){
	{
		m2700(__this, p0, &m2700_MI);
		return;
	}
}
extern MethodInfo m1919_MI;
 float m1919 (t202 * __this, t7* p0, t238  p1, MethodInfo* method){
	t164  V_0 = {0};
	{
		(&p1)->f12 = 1;
		(&p1)->f11 = 1;
		(&p1)->f10 = 1;
		m1774(__this, p0, p1, &m1774_MI);
		t164  L_0 = m1776(__this, &m1776_MI);
		V_0 = L_0;
		float L_1 = m1573((&V_0), &m1573_MI);
		return L_1;
	}
}
extern MethodInfo m1920_MI;
 float m1920 (t202 * __this, t7* p0, t238  p1, MethodInfo* method){
	t164  V_0 = {0};
	{
		(&p1)->f11 = 1;
		(&p1)->f10 = 1;
		m1774(__this, p0, p1, &m1774_MI);
		t164  L_0 = m1776(__this, &m1776_MI);
		V_0 = L_0;
		float L_1 = m1574((&V_0), &m1574_MI);
		return L_1;
	}
}
 bool m1774 (t202 * __this, t7* p0, t238  p1, MethodInfo* method){
	{
		bool L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		t7* L_1 = (__this->f1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, p0, L_1, &m1713_MI);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		t238  L_3 = (__this->f2);
		bool L_4 = m2778((&p1), L_3, &m2778_MI);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		bool L_5 = (__this->f4);
		return L_5;
	}

IL_0035:
	{
		bool L_6 = m2712(__this, p0, p1, &m2712_MI);
		return L_6;
	}
}
 bool m2712 (t202 * __this, t7* p0, t238  p1, MethodInfo* method){
	t238  V_0 = {0};
	{
		__this->f1 = p0;
		__this->f3 = 1;
		__this->f8 = 0;
		__this->f9 = 0;
		__this->f10 = 0;
		__this->f2 = p1;
		t238  L_0 = m2708(__this, p1, &m2708_MI);
		V_0 = L_0;
		t148 * L_1 = ((&V_0)->f0);
		t132  L_2 = ((&V_0)->f1);
		int32_t L_3 = ((&V_0)->f2);
		float L_4 = ((&V_0)->f3);
		int32_t L_5 = ((&V_0)->f5);
		bool L_6 = ((&V_0)->f4);
		bool L_7 = ((&V_0)->f7);
		int32_t L_8 = ((&V_0)->f8);
		int32_t L_9 = ((&V_0)->f9);
		int32_t L_10 = ((&V_0)->f11);
		int32_t L_11 = ((&V_0)->f12);
		bool L_12 = ((&V_0)->f10);
		int32_t L_13 = ((&V_0)->f6);
		t17  L_14 = ((&V_0)->f13);
		t17  L_15 = ((&V_0)->f14);
		bool L_16 = ((&V_0)->f15);
		bool L_17 = m2696(__this, p0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, &m2696_MI);
		__this->f4 = L_17;
		bool L_18 = (__this->f4);
		return L_18;
	}
}
extern MethodInfo m1923_MI;
 t29* m1923 (t202 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		t163 * L_1 = (__this->f5);
		m2711(__this, L_1, &m2711_MI);
		__this->f8 = 1;
	}

IL_001e:
	{
		t163 * L_2 = (__this->f5);
		return L_2;
	}
}
extern MethodInfo m1752_MI;
 t29* m1752 (t202 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		t528 * L_1 = (__this->f6);
		m2709(__this, L_1, &m2709_MI);
		__this->f9 = 1;
	}

IL_001e:
	{
		t528 * L_2 = (__this->f6);
		return L_2;
	}
}
extern MethodInfo m1748_MI;
 t29* m1748 (t202 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		t529 * L_1 = (__this->f7);
		m2710(__this, L_1, &m2710_MI);
		__this->f10 = 1;
	}

IL_001e:
	{
		t529 * L_2 = (__this->f7);
		return L_2;
	}
}
// Metadata Definition UnityEngine.TextGenerator
extern Il2CppType t35_0_0_3;
FieldInfo t202_f0_FieldInfo = 
{
	"m_Ptr", &t35_0_0_3, &t202_TI, offsetof(t202, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t202_f1_FieldInfo = 
{
	"m_LastString", &t7_0_0_1, &t202_TI, offsetof(t202, f1), &EmptyCustomAttributesCache};
extern Il2CppType t238_0_0_1;
FieldInfo t202_f2_FieldInfo = 
{
	"m_LastSettings", &t238_0_0_1, &t202_TI, offsetof(t202, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t202_f3_FieldInfo = 
{
	"m_HasGenerated", &t40_0_0_1, &t202_TI, offsetof(t202, f3), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t202_f4_FieldInfo = 
{
	"m_LastValid", &t40_0_0_1, &t202_TI, offsetof(t202, f4), &EmptyCustomAttributesCache};
extern Il2CppType t163_0_0_33;
FieldInfo t202_f5_FieldInfo = 
{
	"m_Verts", &t163_0_0_33, &t202_TI, offsetof(t202, f5), &EmptyCustomAttributesCache};
extern Il2CppType t528_0_0_33;
FieldInfo t202_f6_FieldInfo = 
{
	"m_Characters", &t528_0_0_33, &t202_TI, offsetof(t202, f6), &EmptyCustomAttributesCache};
extern Il2CppType t529_0_0_33;
FieldInfo t202_f7_FieldInfo = 
{
	"m_Lines", &t529_0_0_33, &t202_TI, offsetof(t202, f7), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t202_f8_FieldInfo = 
{
	"m_CachedVerts", &t40_0_0_1, &t202_TI, offsetof(t202, f8), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t202_f9_FieldInfo = 
{
	"m_CachedCharacters", &t40_0_0_1, &t202_TI, offsetof(t202, f9), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t202_f10_FieldInfo = 
{
	"m_CachedLines", &t40_0_0_1, &t202_TI, offsetof(t202, f10), &EmptyCustomAttributesCache};
static FieldInfo* t202_FIs[] =
{
	&t202_f0_FieldInfo,
	&t202_f1_FieldInfo,
	&t202_f2_FieldInfo,
	&t202_f3_FieldInfo,
	&t202_f4_FieldInfo,
	&t202_f5_FieldInfo,
	&t202_f6_FieldInfo,
	&t202_f7_FieldInfo,
	&t202_f8_FieldInfo,
	&t202_f9_FieldInfo,
	&t202_f10_FieldInfo,
	NULL
};
static PropertyInfo t202____rectExtents_PropertyInfo = 
{
	&t202_TI, "rectExtents", &m1776_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____vertexCount_PropertyInfo = 
{
	&t202_TI, "vertexCount", &m2699_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____characterCount_PropertyInfo = 
{
	&t202_TI, "characterCount", &m2702_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____characterCountVisible_PropertyInfo = 
{
	&t202_TI, "characterCountVisible", &m1751_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____lineCount_PropertyInfo = 
{
	&t202_TI, "lineCount", &m1750_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____fontSizeUsedForBestFit_PropertyInfo = 
{
	&t202_TI, "fontSizeUsedForBestFit", &m1801_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____verts_PropertyInfo = 
{
	&t202_TI, "verts", &m1923_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____characters_PropertyInfo = 
{
	&t202_TI, "characters", &m1752_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t202____lines_PropertyInfo = 
{
	&t202_TI, "lines", &m1748_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t202_PIs[] =
{
	&t202____rectExtents_PropertyInfo,
	&t202____vertexCount_PropertyInfo,
	&t202____characterCount_PropertyInfo,
	&t202____characterCountVisible_PropertyInfo,
	&t202____lineCount_PropertyInfo,
	&t202____fontSizeUsedForBestFit_PropertyInfo,
	&t202____verts_PropertyInfo,
	&t202____characters_PropertyInfo,
	&t202____lines_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1709_MI = 
{
	".ctor", (methodPointerType)&m1709, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 931, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t202_m1912_ParameterInfos[] = 
{
	{"initialCapacity", 0, 134218628, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1912_MI = 
{
	".ctor", (methodPointerType)&m1912, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t202_m1912_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 932, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2693_MI = 
{
	"System.IDisposable.Dispose", (methodPointerType)&m2693, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, false, 933, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2694;
MethodInfo m2694_MI = 
{
	"Init", (methodPointerType)&m2694, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t202__CustomAttributeCache_m2694, 129, 4096, 255, 0, false, false, 934, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2695;
MethodInfo m2695_MI = 
{
	"Dispose_cpp", (methodPointerType)&m2695, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t202__CustomAttributeCache_m2695, 129, 4096, 255, 0, false, false, 935, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t148_0_0_0;
extern Il2CppType t132_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t149_0_0_0;
extern Il2CppType t149_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t152_0_0_0;
extern Il2CppType t151_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t150_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t202_m2696_ParameterInfos[] = 
{
	{"str", 0, 134218629, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"font", 1, 134218630, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"color", 2, 134218631, &EmptyCustomAttributesCache, &t132_0_0_0},
	{"fontSize", 3, 134218632, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lineSpacing", 4, 134218633, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"style", 5, 134218634, &EmptyCustomAttributesCache, &t149_0_0_0},
	{"richText", 6, 134218635, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resizeTextForBestFit", 7, 134218636, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resizeTextMinSize", 8, 134218637, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"resizeTextMaxSize", 9, 134218638, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"verticalOverFlow", 10, 134218639, &EmptyCustomAttributesCache, &t152_0_0_0},
	{"horizontalOverflow", 11, 134218640, &EmptyCustomAttributesCache, &t151_0_0_0},
	{"updateBounds", 12, 134218641, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"anchor", 13, 134218642, &EmptyCustomAttributesCache, &t150_0_0_0},
	{"extents", 14, 134218643, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"pivot", 15, 134218644, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"generateOutOfBounds", 16, 134218645, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t132_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t17_t17_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m2696_MI = 
{
	"Populate_Internal", (methodPointerType)&m2696, &t202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t132_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t17_t17_t297, t202_m2696_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 17, false, false, 936, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t148_0_0_0;
extern Il2CppType t132_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t149_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t150_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t202_m2697_ParameterInfos[] = 
{
	{"str", 0, 134218646, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"font", 1, 134218647, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"color", 2, 134218648, &EmptyCustomAttributesCache, &t132_0_0_0},
	{"fontSize", 3, 134218649, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lineSpacing", 4, 134218650, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"style", 5, 134218651, &EmptyCustomAttributesCache, &t149_0_0_0},
	{"richText", 6, 134218652, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resizeTextForBestFit", 7, 134218653, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resizeTextMinSize", 8, 134218654, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"resizeTextMaxSize", 9, 134218655, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"verticalOverFlow", 10, 134218656, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"horizontalOverflow", 11, 134218657, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"updateBounds", 12, 134218658, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"anchor", 13, 134218659, &EmptyCustomAttributesCache, &t150_0_0_0},
	{"extentsX", 14, 134218660, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"extentsY", 15, 134218661, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"pivotX", 16, 134218662, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"pivotY", 17, 134218663, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"generateOutOfBounds", 18, 134218664, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t132_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t22_t22_t22_t22_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m2697_MI = 
{
	"Populate_Internal_cpp", (methodPointerType)&m2697, &t202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t132_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t22_t22_t22_t22_t297, t202_m2697_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 19, false, false, 937, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t202_0_0_0;
extern Il2CppType t202_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t148_0_0_0;
extern Il2CppType t132_1_0_0;
extern Il2CppType t132_1_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t149_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t150_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t202_m2698_ParameterInfos[] = 
{
	{"self", 0, 134218665, &EmptyCustomAttributesCache, &t202_0_0_0},
	{"str", 1, 134218666, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"font", 2, 134218667, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"color", 3, 134218668, &EmptyCustomAttributesCache, &t132_1_0_0},
	{"fontSize", 4, 134218669, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lineSpacing", 5, 134218670, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"style", 6, 134218671, &EmptyCustomAttributesCache, &t149_0_0_0},
	{"richText", 7, 134218672, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resizeTextForBestFit", 8, 134218673, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"resizeTextMinSize", 9, 134218674, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"resizeTextMaxSize", 10, 134218675, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"verticalOverFlow", 11, 134218676, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"horizontalOverflow", 12, 134218677, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"updateBounds", 13, 134218678, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"anchor", 14, 134218679, &EmptyCustomAttributesCache, &t150_0_0_0},
	{"extentsX", 15, 134218680, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"extentsY", 16, 134218681, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"pivotX", 17, 134218682, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"pivotY", 18, 134218683, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"generateOutOfBounds", 19, 134218684, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t29_t400_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t22_t22_t22_t22_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2698;
MethodInfo m2698_MI = 
{
	"INTERNAL_CALL_Populate_Internal_cpp", (methodPointerType)&m2698, &t202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t29_t400_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t22_t22_t22_t22_t297, t202_m2698_ParameterInfos, &t202__CustomAttributeCache_m2698, 145, 4096, 255, 20, false, false, 938, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t164_0_0_0;
extern void* RuntimeInvoker_t164 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m1776;
MethodInfo m1776_MI = 
{
	"get_rectExtents", (methodPointerType)&m1776, &t202_TI, &t164_0_0_0, RuntimeInvoker_t164, NULL, &t202__CustomAttributeCache_m1776, 2182, 4096, 255, 0, false, false, 939, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2699;
MethodInfo m2699_MI = 
{
	"get_vertexCount", (methodPointerType)&m2699, &t202_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t202__CustomAttributeCache_m2699, 2182, 4096, 255, 0, false, false, 940, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t202_m2700_ParameterInfos[] = 
{
	{"vertices", 0, 134218685, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2700;
MethodInfo m2700_MI = 
{
	"GetVerticesInternal", (methodPointerType)&m2700, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t202_m2700_ParameterInfos, &t202__CustomAttributeCache_m2700, 129, 4096, 255, 1, false, false, 941, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t201_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2701;
MethodInfo m2701_MI = 
{
	"GetVerticesArray", (methodPointerType)&m2701, &t202_TI, &t201_0_0_0, RuntimeInvoker_t29, NULL, &t202__CustomAttributeCache_m2701, 134, 4096, 255, 0, false, false, 942, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2702;
MethodInfo m2702_MI = 
{
	"get_characterCount", (methodPointerType)&m2702, &t202_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t202__CustomAttributeCache_m2702, 2182, 4096, 255, 0, false, false, 943, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1751_MI = 
{
	"get_characterCountVisible", (methodPointerType)&m1751, &t202_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 944, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t202_m2703_ParameterInfos[] = 
{
	{"characters", 0, 134218686, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2703;
MethodInfo m2703_MI = 
{
	"GetCharactersInternal", (methodPointerType)&m2703, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t202_m2703_ParameterInfos, &t202__CustomAttributeCache_m2703, 129, 4096, 255, 1, false, false, 945, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t530_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2704;
MethodInfo m2704_MI = 
{
	"GetCharactersArray", (methodPointerType)&m2704, &t202_TI, &t530_0_0_0, RuntimeInvoker_t29, NULL, &t202__CustomAttributeCache_m2704, 134, 4096, 255, 0, false, false, 946, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m1750;
MethodInfo m1750_MI = 
{
	"get_lineCount", (methodPointerType)&m1750, &t202_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t202__CustomAttributeCache_m1750, 2182, 4096, 255, 0, false, false, 947, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t202_m2705_ParameterInfos[] = 
{
	{"lines", 0, 134218687, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2705;
MethodInfo m2705_MI = 
{
	"GetLinesInternal", (methodPointerType)&m2705, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t202_m2705_ParameterInfos, &t202__CustomAttributeCache_m2705, 129, 4096, 255, 1, false, false, 948, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t531_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m2706;
MethodInfo m2706_MI = 
{
	"GetLinesArray", (methodPointerType)&m2706, &t202_TI, &t531_0_0_0, RuntimeInvoker_t29, NULL, &t202__CustomAttributeCache_m2706, 134, 4096, 255, 0, false, false, 949, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t202__CustomAttributeCache_m1801;
MethodInfo m1801_MI = 
{
	"get_fontSizeUsedForBestFit", (methodPointerType)&m1801, &t202_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t202__CustomAttributeCache_m1801, 2182, 4096, 255, 0, false, false, 950, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2707_MI = 
{
	"Finalize", (methodPointerType)&m2707, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 1, 0, false, false, 951, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t238_0_0_0;
extern Il2CppType t238_0_0_0;
static ParameterInfo t202_m2708_ParameterInfos[] = 
{
	{"settings", 0, 134218688, &EmptyCustomAttributesCache, &t238_0_0_0},
};
extern Il2CppType t238_0_0_0;
extern void* RuntimeInvoker_t238_t238 (MethodInfo* method, void* obj, void** args);
MethodInfo m2708_MI = 
{
	"ValidatedSettings", (methodPointerType)&m2708, &t202_TI, &t238_0_0_0, RuntimeInvoker_t238_t238, t202_m2708_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 952, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1921_MI = 
{
	"Invalidate", (methodPointerType)&m1921, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 953, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t528_0_0_0;
extern Il2CppType t528_0_0_0;
static ParameterInfo t202_m2709_ParameterInfos[] = 
{
	{"characters", 0, 134218689, &EmptyCustomAttributesCache, &t528_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2709_MI = 
{
	"GetCharacters", (methodPointerType)&m2709, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t202_m2709_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 954, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t529_0_0_0;
extern Il2CppType t529_0_0_0;
static ParameterInfo t202_m2710_ParameterInfos[] = 
{
	{"lines", 0, 134218690, &EmptyCustomAttributesCache, &t529_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2710_MI = 
{
	"GetLines", (methodPointerType)&m2710, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t202_m2710_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 955, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
extern Il2CppType t163_0_0_0;
static ParameterInfo t202_m2711_ParameterInfos[] = 
{
	{"vertices", 0, 134218691, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2711_MI = 
{
	"GetVertices", (methodPointerType)&m2711, &t202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t202_m2711_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 956, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t238_0_0_0;
static ParameterInfo t202_m1919_ParameterInfos[] = 
{
	{"str", 0, 134218692, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"settings", 1, 134218693, &EmptyCustomAttributesCache, &t238_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t238 (MethodInfo* method, void* obj, void** args);
MethodInfo m1919_MI = 
{
	"GetPreferredWidth", (methodPointerType)&m1919, &t202_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t238, t202_m1919_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 957, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t238_0_0_0;
static ParameterInfo t202_m1920_ParameterInfos[] = 
{
	{"str", 0, 134218694, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"settings", 1, 134218695, &EmptyCustomAttributesCache, &t238_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t238 (MethodInfo* method, void* obj, void** args);
MethodInfo m1920_MI = 
{
	"GetPreferredHeight", (methodPointerType)&m1920, &t202_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t238, t202_m1920_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 958, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t238_0_0_0;
static ParameterInfo t202_m1774_ParameterInfos[] = 
{
	{"str", 0, 134218696, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"settings", 1, 134218697, &EmptyCustomAttributesCache, &t238_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t238 (MethodInfo* method, void* obj, void** args);
MethodInfo m1774_MI = 
{
	"Populate", (methodPointerType)&m1774, &t202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t238, t202_m1774_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 959, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t238_0_0_0;
static ParameterInfo t202_m2712_ParameterInfos[] = 
{
	{"str", 0, 134218698, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"settings", 1, 134218699, &EmptyCustomAttributesCache, &t238_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t238 (MethodInfo* method, void* obj, void** args);
MethodInfo m2712_MI = 
{
	"PopulateAlways", (methodPointerType)&m2712, &t202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t238, t202_m2712_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 960, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t403_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1923_MI = 
{
	"get_verts", (methodPointerType)&m1923, &t202_TI, &t403_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 961, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t388_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1752_MI = 
{
	"get_characters", (methodPointerType)&m1752, &t202_TI, &t388_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 962, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t387_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1748_MI = 
{
	"get_lines", (methodPointerType)&m1748, &t202_TI, &t387_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 963, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t202_MIs[] =
{
	&m1709_MI,
	&m1912_MI,
	&m2693_MI,
	&m2694_MI,
	&m2695_MI,
	&m2696_MI,
	&m2697_MI,
	&m2698_MI,
	&m1776_MI,
	&m2699_MI,
	&m2700_MI,
	&m2701_MI,
	&m2702_MI,
	&m1751_MI,
	&m2703_MI,
	&m2704_MI,
	&m1750_MI,
	&m2705_MI,
	&m2706_MI,
	&m1801_MI,
	&m2707_MI,
	&m2708_MI,
	&m1921_MI,
	&m2709_MI,
	&m2710_MI,
	&m2711_MI,
	&m1919_MI,
	&m1920_MI,
	&m1774_MI,
	&m2712_MI,
	&m1923_MI,
	&m1752_MI,
	&m1748_MI,
	NULL
};
static MethodInfo* t202_VT[] =
{
	&m1321_MI,
	&m2707_MI,
	&m1322_MI,
	&m1332_MI,
	&m2693_MI,
};
static TypeInfo* t202_ITIs[] = 
{
	&t324_TI,
};
static Il2CppInterfaceOffsetPair t202_IOs[] = 
{
	{ &t324_TI, 4},
};
void t202_CustomAttributesCacheGenerator_m2694(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2695(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2698(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m1776(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2699(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2700(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2701(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2702(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2703(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2704(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m1750(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2705(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m2706(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t202_CustomAttributesCacheGenerator_m1801(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t202__CustomAttributeCache_m2694 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2694
};
CustomAttributesCache t202__CustomAttributeCache_m2695 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2695
};
CustomAttributesCache t202__CustomAttributeCache_m2698 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2698
};
CustomAttributesCache t202__CustomAttributeCache_m1776 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m1776
};
CustomAttributesCache t202__CustomAttributeCache_m2699 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2699
};
CustomAttributesCache t202__CustomAttributeCache_m2700 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2700
};
CustomAttributesCache t202__CustomAttributeCache_m2701 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2701
};
CustomAttributesCache t202__CustomAttributeCache_m2702 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2702
};
CustomAttributesCache t202__CustomAttributeCache_m2703 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2703
};
CustomAttributesCache t202__CustomAttributeCache_m2704 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2704
};
CustomAttributesCache t202__CustomAttributeCache_m1750 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m1750
};
CustomAttributesCache t202__CustomAttributeCache_m2705 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2705
};
CustomAttributesCache t202__CustomAttributeCache_m2706 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m2706
};
CustomAttributesCache t202__CustomAttributeCache_m1801 = {
1,
NULL,
&t202_CustomAttributesCacheGenerator_m1801
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t202_1_0_0;
struct t202;
extern CustomAttributesCache t202__CustomAttributeCache_m2694;
extern CustomAttributesCache t202__CustomAttributeCache_m2695;
extern CustomAttributesCache t202__CustomAttributeCache_m2698;
extern CustomAttributesCache t202__CustomAttributeCache_m1776;
extern CustomAttributesCache t202__CustomAttributeCache_m2699;
extern CustomAttributesCache t202__CustomAttributeCache_m2700;
extern CustomAttributesCache t202__CustomAttributeCache_m2701;
extern CustomAttributesCache t202__CustomAttributeCache_m2702;
extern CustomAttributesCache t202__CustomAttributeCache_m2703;
extern CustomAttributesCache t202__CustomAttributeCache_m2704;
extern CustomAttributesCache t202__CustomAttributeCache_m1750;
extern CustomAttributesCache t202__CustomAttributeCache_m2705;
extern CustomAttributesCache t202__CustomAttributeCache_m2706;
extern CustomAttributesCache t202__CustomAttributeCache_m1801;
TypeInfo t202_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextGenerator", "UnityEngine", t202_MIs, t202_PIs, t202_FIs, NULL, &t29_TI, NULL, NULL, &t202_TI, t202_ITIs, t202_VT, &EmptyCustomAttributesCache, &t202_TI, &t202_0_0_0, &t202_1_0_0, t202_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t202), 0, -1, 0, 0, -1, 1048841, 0, false, false, false, false, false, false, false, false, true, false, false, false, 33, 9, 11, 0, 0, 5, 1, 1};
#include "t361.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t361_TI;
#include "t361MD.h"



// Metadata Definition UnityEngine.RenderMode
extern Il2CppType t44_0_0_1542;
FieldInfo t361_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t361_TI, offsetof(t361, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t361_0_0_32854;
FieldInfo t361_f2_FieldInfo = 
{
	"ScreenSpaceOverlay", &t361_0_0_32854, &t361_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t361_0_0_32854;
FieldInfo t361_f3_FieldInfo = 
{
	"ScreenSpaceCamera", &t361_0_0_32854, &t361_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t361_0_0_32854;
FieldInfo t361_f4_FieldInfo = 
{
	"WorldSpace", &t361_0_0_32854, &t361_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t361_FIs[] =
{
	&t361_f1_FieldInfo,
	&t361_f2_FieldInfo,
	&t361_f3_FieldInfo,
	&t361_f4_FieldInfo,
	NULL
};
static const int32_t t361_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t361_f2_DefaultValue = 
{
	&t361_f2_FieldInfo, { (char*)&t361_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t361_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t361_f3_DefaultValue = 
{
	&t361_f3_FieldInfo, { (char*)&t361_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t361_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t361_f4_DefaultValue = 
{
	&t361_f4_FieldInfo, { (char*)&t361_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t361_FDVs[] = 
{
	&t361_f2_DefaultValue,
	&t361_f3_DefaultValue,
	&t361_f4_DefaultValue,
	NULL
};
static MethodInfo* t361_MIs[] =
{
	NULL
};
static MethodInfo* t361_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t361_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t361_0_0_0;
extern Il2CppType t361_1_0_0;
TypeInfo t361_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RenderMode", "UnityEngine", t361_MIs, NULL, t361_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t361_VT, &EmptyCustomAttributesCache, &t44_TI, &t361_0_0_0, &t361_1_0_0, t361_IOs, NULL, NULL, t361_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t361)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t347.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t347_TI;
#include "t347MD.h"



extern MethodInfo m1524_MI;
 void m1524 (t347 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m2713_MI;
 void m2713 (t347 * __this, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m2713((t347 *)__this->f9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,(MethodInfo*)(__this->f3.f0));
}
void pinvoke_delegate_wrapper_t347(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
extern MethodInfo m2714_MI;
 t29 * m2714 (t347 * __this, t67 * p0, t29 * p1, MethodInfo* method){
	void *__d_args[1] = {0};
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p0, (Il2CppObject*)p1);
}
extern MethodInfo m2715_MI;
 void m2715 (t347 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Canvas/WillRenderCanvases
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t347_m1524_ParameterInfos[] = 
{
	{"object", 0, 134218704, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218705, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m1524_MI = 
{
	".ctor", (methodPointerType)&m1524, &t347_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t347_m1524_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 981, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2713_MI = 
{
	"Invoke", (methodPointerType)&m2713, &t347_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 3, 10, 0, false, false, 982, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t347_m2714_ParameterInfos[] = 
{
	{"callback", 0, 134218706, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 1, 134218707, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2714_MI = 
{
	"BeginInvoke", (methodPointerType)&m2714, &t347_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29, t347_m2714_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 2, false, false, 983, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t347_m2715_ParameterInfos[] = 
{
	{"result", 0, 134218708, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2715_MI = 
{
	"EndInvoke", (methodPointerType)&m2715, &t347_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t347_m2715_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 984, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t347_MIs[] =
{
	&m1524_MI,
	&m2713_MI,
	&m2714_MI,
	&m2715_MI,
	NULL
};
static MethodInfo* t347_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2713_MI,
	&m2714_MI,
	&m2715_MI,
};
static Il2CppInterfaceOffsetPair t347_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t347_0_0_0;
extern Il2CppType t347_1_0_0;
struct t347;
extern TypeInfo t3_TI;
TypeInfo t347_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "WillRenderCanvases", "", t347_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t3_TI, &t347_TI, NULL, t347_VT, &EmptyCustomAttributesCache, &t347_TI, &t347_0_0_0, &t347_1_0_0, t347_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t347, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t347), 0, sizeof(methodPointerType), 0, 0, -1, 258, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3.h"
#ifndef _MSC_VER
#else
#endif
#include "t3MD.h"

#include "t24.h"
extern MethodInfo m2717_MI;


extern MethodInfo m1525_MI;
 void m1525 (t29 * __this, t347 * p0, MethodInfo* method){
	{
		t353 * L_0 = m1597(NULL, (((t3_SFs*)InitializedTypeInfo(&t3_TI)->static_fields)->f2), p0, &m1597_MI);
		((t3_SFs*)InitializedTypeInfo(&t3_TI)->static_fields)->f2 = ((t347 *)Castclass(L_0, InitializedTypeInfo(&t347_TI)));
		return;
	}
}
extern MethodInfo m2716_MI;
 void m2716 (t29 * __this, t347 * p0, MethodInfo* method){
	{
		t353 * L_0 = m1598(NULL, (((t3_SFs*)InitializedTypeInfo(&t3_TI)->static_fields)->f2), p0, &m1598_MI);
		((t3_SFs*)InitializedTypeInfo(&t3_TI)->static_fields)->f2 = ((t347 *)Castclass(L_0, InitializedTypeInfo(&t347_TI)));
		return;
	}
}
extern MethodInfo m1605_MI;
 int32_t m1605 (t3 * __this, MethodInfo* method){
	typedef int32_t (*m1605_ftn) (t3 *);
	static m1605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1947_MI;
 bool m1947 (t3 * __this, MethodInfo* method){
	typedef bool (*m1947_ftn) (t3 *);
	static m1947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1609_MI;
 t24 * m1609 (t3 * __this, MethodInfo* method){
	typedef t24 * (*m1609_ftn) (t3 *);
	static m1609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1917_MI;
 float m1917 (t3 * __this, MethodInfo* method){
	typedef float (*m1917_ftn) (t3 *);
	static m1917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1951_MI;
 void m1951 (t3 * __this, float p0, MethodInfo* method){
	typedef void (*m1951_ftn) (t3 *, float);
	static m1951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1650_MI;
 float m1650 (t3 * __this, MethodInfo* method){
	typedef float (*m1650_ftn) (t3 *);
	static m1650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1952_MI;
 void m1952 (t3 * __this, float p0, MethodInfo* method){
	typedef void (*m1952_ftn) (t3 *, float);
	static m1952_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1952_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1588_MI;
 bool m1588 (t3 * __this, MethodInfo* method){
	typedef bool (*m1588_ftn) (t3 *);
	static m1588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1607_MI;
 int32_t m1607 (t3 * __this, MethodInfo* method){
	typedef int32_t (*m1607_ftn) (t3 *);
	static m1607_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1607_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1606_MI;
 int32_t m1606 (t3 * __this, MethodInfo* method){
	typedef int32_t (*m1606_ftn) (t3 *);
	static m1606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1625_MI;
 int32_t m1625 (t3 * __this, MethodInfo* method){
	typedef int32_t (*m1625_ftn) (t3 *);
	static m1625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1551_MI;
 t156 * m1551 (t29 * __this, MethodInfo* method){
	typedef t156 * (*m1551_ftn) ();
	static m1551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
extern MethodInfo m1913_MI;
 t156 * m1913 (t29 * __this, MethodInfo* method){
	typedef t156 * (*m1913_ftn) ();
	static m1913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasTextMaterial()");
	return _il2cpp_icall_func();
}
 void m2717 (t29 * __this, MethodInfo* method){
	{
		if (!(((t3_SFs*)InitializedTypeInfo(&t3_TI)->static_fields)->f2))
		{
			goto IL_0014;
		}
	}
	{
		VirtActionInvoker0::Invoke(&m2713_MI, (((t3_SFs*)InitializedTypeInfo(&t3_TI)->static_fields)->f2));
	}

IL_0014:
	{
		return;
	}
}
extern MethodInfo m1846_MI;
 void m1846 (t29 * __this, MethodInfo* method){
	{
		m2717(NULL, &m2717_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Canvas
extern Il2CppType t347_0_0_17;
FieldInfo t3_f2_FieldInfo = 
{
	"willRenderCanvases", &t347_0_0_17, &t3_TI, offsetof(t3_SFs, f2), &EmptyCustomAttributesCache};
static FieldInfo* t3_FIs[] =
{
	&t3_f2_FieldInfo,
	NULL
};
static PropertyInfo t3____renderMode_PropertyInfo = 
{
	&t3_TI, "renderMode", &m1605_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____isRootCanvas_PropertyInfo = 
{
	&t3_TI, "isRootCanvas", &m1947_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____worldCamera_PropertyInfo = 
{
	&t3_TI, "worldCamera", &m1609_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____scaleFactor_PropertyInfo = 
{
	&t3_TI, "scaleFactor", &m1917_MI, &m1951_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____referencePixelsPerUnit_PropertyInfo = 
{
	&t3_TI, "referencePixelsPerUnit", &m1650_MI, &m1952_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____pixelPerfect_PropertyInfo = 
{
	&t3_TI, "pixelPerfect", &m1588_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____renderOrder_PropertyInfo = 
{
	&t3_TI, "renderOrder", &m1607_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____sortingOrder_PropertyInfo = 
{
	&t3_TI, "sortingOrder", &m1606_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3____sortingLayerID_PropertyInfo = 
{
	&t3_TI, "sortingLayerID", &m1625_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3_PIs[] =
{
	&t3____renderMode_PropertyInfo,
	&t3____isRootCanvas_PropertyInfo,
	&t3____worldCamera_PropertyInfo,
	&t3____scaleFactor_PropertyInfo,
	&t3____referencePixelsPerUnit_PropertyInfo,
	&t3____pixelPerfect_PropertyInfo,
	&t3____renderOrder_PropertyInfo,
	&t3____sortingOrder_PropertyInfo,
	&t3____sortingLayerID_PropertyInfo,
	NULL
};
extern Il2CppType t347_0_0_0;
static EventInfo t3____willRenderCanvases_EventInfo = 
{
	"willRenderCanvases", &t347_0_0_0, &t3_TI, &m1525_MI, &m2716_MI, NULL, &EmptyCustomAttributesCache};
static EventInfo* t3__EventInfos[] =
{
	&t3____willRenderCanvases_EventInfo,
	NULL
};
extern Il2CppType t347_0_0_0;
static ParameterInfo t3_m1525_ParameterInfos[] = 
{
	{"value", 0, 134218700, &EmptyCustomAttributesCache, &t347_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1525_MI = 
{
	"add_willRenderCanvases", (methodPointerType)&m1525, &t3_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3_m1525_ParameterInfos, &EmptyCustomAttributesCache, 2198, 32, 255, 1, false, false, 964, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t347_0_0_0;
static ParameterInfo t3_m2716_ParameterInfos[] = 
{
	{"value", 0, 134218701, &EmptyCustomAttributesCache, &t347_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2716_MI = 
{
	"remove_willRenderCanvases", (methodPointerType)&m2716, &t3_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3_m2716_ParameterInfos, &EmptyCustomAttributesCache, 2198, 32, 255, 1, false, false, 965, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t361_0_0_0;
extern void* RuntimeInvoker_t361 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1605;
MethodInfo m1605_MI = 
{
	"get_renderMode", (methodPointerType)&m1605, &t3_TI, &t361_0_0_0, RuntimeInvoker_t361, NULL, &t3__CustomAttributeCache_m1605, 2182, 4096, 255, 0, false, false, 966, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1947;
MethodInfo m1947_MI = 
{
	"get_isRootCanvas", (methodPointerType)&m1947, &t3_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t3__CustomAttributeCache_m1947, 2182, 4096, 255, 0, false, false, 967, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t24_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1609;
MethodInfo m1609_MI = 
{
	"get_worldCamera", (methodPointerType)&m1609, &t3_TI, &t24_0_0_0, RuntimeInvoker_t29, NULL, &t3__CustomAttributeCache_m1609, 2182, 4096, 255, 0, false, false, 968, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1917;
MethodInfo m1917_MI = 
{
	"get_scaleFactor", (methodPointerType)&m1917, &t3_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &t3__CustomAttributeCache_m1917, 2182, 4096, 255, 0, false, false, 969, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t3_m1951_ParameterInfos[] = 
{
	{"value", 0, 134218702, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1951;
MethodInfo m1951_MI = 
{
	"set_scaleFactor", (methodPointerType)&m1951, &t3_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t3_m1951_ParameterInfos, &t3__CustomAttributeCache_m1951, 2182, 4096, 255, 1, false, false, 970, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1650;
MethodInfo m1650_MI = 
{
	"get_referencePixelsPerUnit", (methodPointerType)&m1650, &t3_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &t3__CustomAttributeCache_m1650, 2182, 4096, 255, 0, false, false, 971, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t3_m1952_ParameterInfos[] = 
{
	{"value", 0, 134218703, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1952;
MethodInfo m1952_MI = 
{
	"set_referencePixelsPerUnit", (methodPointerType)&m1952, &t3_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t3_m1952_ParameterInfos, &t3__CustomAttributeCache_m1952, 2182, 4096, 255, 1, false, false, 972, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1588;
MethodInfo m1588_MI = 
{
	"get_pixelPerfect", (methodPointerType)&m1588, &t3_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t3__CustomAttributeCache_m1588, 2182, 4096, 255, 0, false, false, 973, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1607;
MethodInfo m1607_MI = 
{
	"get_renderOrder", (methodPointerType)&m1607, &t3_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t3__CustomAttributeCache_m1607, 2182, 4096, 255, 0, false, false, 974, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1606;
MethodInfo m1606_MI = 
{
	"get_sortingOrder", (methodPointerType)&m1606, &t3_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t3__CustomAttributeCache_m1606, 2182, 4096, 255, 0, false, false, 975, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1625;
MethodInfo m1625_MI = 
{
	"get_sortingLayerID", (methodPointerType)&m1625, &t3_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t3__CustomAttributeCache_m1625, 2182, 4096, 255, 0, false, false, 976, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1551;
MethodInfo m1551_MI = 
{
	"GetDefaultCanvasMaterial", (methodPointerType)&m1551, &t3_TI, &t156_0_0_0, RuntimeInvoker_t29, NULL, &t3__CustomAttributeCache_m1551, 150, 4096, 255, 0, false, false, 977, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t3__CustomAttributeCache_m1913;
MethodInfo m1913_MI = 
{
	"GetDefaultCanvasTextMaterial", (methodPointerType)&m1913, &t3_TI, &t156_0_0_0, RuntimeInvoker_t29, NULL, &t3__CustomAttributeCache_m1913, 150, 4096, 255, 0, false, false, 978, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2717_MI = 
{
	"SendWillRenderCanvases", (methodPointerType)&m2717, &t3_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 145, 0, 255, 0, false, false, 979, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1846_MI = 
{
	"ForceUpdateCanvases", (methodPointerType)&m1846, &t3_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 150, 0, 255, 0, false, false, 980, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t3_MIs[] =
{
	&m1525_MI,
	&m2716_MI,
	&m1605_MI,
	&m1947_MI,
	&m1609_MI,
	&m1917_MI,
	&m1951_MI,
	&m1650_MI,
	&m1952_MI,
	&m1588_MI,
	&m1607_MI,
	&m1606_MI,
	&m1625_MI,
	&m1551_MI,
	&m1913_MI,
	&m2717_MI,
	&m1846_MI,
	NULL
};
extern TypeInfo t347_TI;
static TypeInfo* t3_TI__nestedTypes[2] =
{
	&t347_TI,
	NULL
};
static MethodInfo* t3_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t3_CustomAttributesCacheGenerator_m1605(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1947(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1609(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1917(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1951(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1650(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1952(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1588(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1607(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1606(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1625(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1551(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t3_CustomAttributesCacheGenerator_m1913(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t3__CustomAttributeCache_m1605 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1605
};
CustomAttributesCache t3__CustomAttributeCache_m1947 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1947
};
CustomAttributesCache t3__CustomAttributeCache_m1609 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1609
};
CustomAttributesCache t3__CustomAttributeCache_m1917 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1917
};
CustomAttributesCache t3__CustomAttributeCache_m1951 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1951
};
CustomAttributesCache t3__CustomAttributeCache_m1650 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1650
};
CustomAttributesCache t3__CustomAttributeCache_m1952 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1952
};
CustomAttributesCache t3__CustomAttributeCache_m1588 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1588
};
CustomAttributesCache t3__CustomAttributeCache_m1607 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1607
};
CustomAttributesCache t3__CustomAttributeCache_m1606 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1606
};
CustomAttributesCache t3__CustomAttributeCache_m1625 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1625
};
CustomAttributesCache t3__CustomAttributeCache_m1551 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1551
};
CustomAttributesCache t3__CustomAttributeCache_m1913 = {
1,
NULL,
&t3_CustomAttributesCacheGenerator_m1913
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_1_0_0;
struct t3;
extern CustomAttributesCache t3__CustomAttributeCache_m1605;
extern CustomAttributesCache t3__CustomAttributeCache_m1947;
extern CustomAttributesCache t3__CustomAttributeCache_m1609;
extern CustomAttributesCache t3__CustomAttributeCache_m1917;
extern CustomAttributesCache t3__CustomAttributeCache_m1951;
extern CustomAttributesCache t3__CustomAttributeCache_m1650;
extern CustomAttributesCache t3__CustomAttributeCache_m1952;
extern CustomAttributesCache t3__CustomAttributeCache_m1588;
extern CustomAttributesCache t3__CustomAttributeCache_m1607;
extern CustomAttributesCache t3__CustomAttributeCache_m1606;
extern CustomAttributesCache t3__CustomAttributeCache_m1625;
extern CustomAttributesCache t3__CustomAttributeCache_m1551;
extern CustomAttributesCache t3__CustomAttributeCache_m1913;
TypeInfo t3_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Canvas", "UnityEngine", t3_MIs, t3_PIs, t3_FIs, t3__EventInfos, &t317_TI, t3_TI__nestedTypes, NULL, &t3_TI, NULL, t3_VT, &EmptyCustomAttributesCache, &t3_TI, &t3_0_0_0, &t3_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3), 0, -1, sizeof(t3_SFs), 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 17, 9, 1, 1, 1, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t357_TI;



// Metadata Definition UnityEngine.ICanvasRaycastFilter
extern Il2CppType t17_0_0_0;
extern Il2CppType t24_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t357_m1587_ParameterInfos[] = 
{
	{"sp", 0, 134218709, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"eventCamera", 1, 134218710, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t17_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1587_MI = 
{
	"IsRaycastLocationValid", NULL, &t357_TI, &t40_0_0_0, RuntimeInvoker_t40_t17_t29, t357_m1587_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, false, 985, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t357_MIs[] =
{
	&m1587_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t357_0_0_0;
extern Il2CppType t357_1_0_0;
struct t357;
TypeInfo t357_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ICanvasRaycastFilter", "UnityEngine", t357_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t357_TI, NULL, NULL, &EmptyCustomAttributesCache, &t357_TI, &t357_0_0_0, &t357_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t352.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t352_TI;
#include "t352MD.h"

extern MethodInfo m2718_MI;


extern MethodInfo m1878_MI;
 bool m1878 (t352 * __this, MethodInfo* method){
	typedef bool (*m1878_ftn) (t352 *);
	static m1878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
 bool m2718 (t352 * __this, MethodInfo* method){
	typedef bool (*m2718_ftn) (t352 *);
	static m2718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m1586_MI;
 bool m1586 (t352 * __this, MethodInfo* method){
	typedef bool (*m1586_ftn) (t352 *);
	static m1586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m2719_MI;
 bool m2719 (t352 * __this, t17  p0, t24 * p1, MethodInfo* method){
	{
		bool L_0 = m2718(__this, &m2718_MI);
		return L_0;
	}
}
// Metadata Definition UnityEngine.CanvasGroup
static PropertyInfo t352____interactable_PropertyInfo = 
{
	&t352_TI, "interactable", &m1878_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t352____blocksRaycasts_PropertyInfo = 
{
	&t352_TI, "blocksRaycasts", &m2718_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t352____ignoreParentGroups_PropertyInfo = 
{
	&t352_TI, "ignoreParentGroups", &m1586_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t352_PIs[] =
{
	&t352____interactable_PropertyInfo,
	&t352____blocksRaycasts_PropertyInfo,
	&t352____ignoreParentGroups_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t352__CustomAttributeCache_m1878;
MethodInfo m1878_MI = 
{
	"get_interactable", (methodPointerType)&m1878, &t352_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t352__CustomAttributeCache_m1878, 2182, 4096, 255, 0, false, false, 986, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t352__CustomAttributeCache_m2718;
MethodInfo m2718_MI = 
{
	"get_blocksRaycasts", (methodPointerType)&m2718, &t352_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t352__CustomAttributeCache_m2718, 2182, 4096, 255, 0, false, false, 987, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t352__CustomAttributeCache_m1586;
MethodInfo m1586_MI = 
{
	"get_ignoreParentGroups", (methodPointerType)&m1586, &t352_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t352__CustomAttributeCache_m1586, 2182, 4096, 255, 0, false, false, 988, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t352_m2719_ParameterInfos[] = 
{
	{"sp", 0, 134218711, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"eventCamera", 1, 134218712, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t17_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2719_MI = 
{
	"IsRaycastLocationValid", (methodPointerType)&m2719, &t352_TI, &t40_0_0_0, RuntimeInvoker_t40_t17_t29, t352_m2719_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 2, false, false, 989, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t352_MIs[] =
{
	&m1878_MI,
	&m2718_MI,
	&m1586_MI,
	&m2719_MI,
	NULL
};
static MethodInfo* t352_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m2719_MI,
};
static TypeInfo* t352_ITIs[] = 
{
	&t357_TI,
};
static Il2CppInterfaceOffsetPair t352_IOs[] = 
{
	{ &t357_TI, 4},
};
void t352_CustomAttributesCacheGenerator_m1878(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t352_CustomAttributesCacheGenerator_m2718(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t352_CustomAttributesCacheGenerator_m1586(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t352__CustomAttributeCache_m1878 = {
1,
NULL,
&t352_CustomAttributesCacheGenerator_m1878
};
CustomAttributesCache t352__CustomAttributeCache_m2718 = {
1,
NULL,
&t352_CustomAttributesCacheGenerator_m2718
};
CustomAttributesCache t352__CustomAttributeCache_m1586 = {
1,
NULL,
&t352_CustomAttributesCacheGenerator_m1586
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_1_0_0;
struct t352;
extern CustomAttributesCache t352__CustomAttributeCache_m1878;
extern CustomAttributesCache t352__CustomAttributeCache_m2718;
extern CustomAttributesCache t352__CustomAttributeCache_m1586;
TypeInfo t352_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CanvasGroup", "UnityEngine", t352_MIs, t352_PIs, NULL, NULL, &t28_TI, NULL, NULL, &t352_TI, t352_ITIs, t352_VT, &EmptyCustomAttributesCache, &t352_TI, &t352_0_0_0, &t352_1_0_0, t352_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t352), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 3, 0, 0, 0, 5, 1, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t184_TI;
#include "t184MD.h"

#include "t287.h"
#include "t348.h"
#include "t183.h"
extern TypeInfo t287_TI;
extern TypeInfo t183_TI;
#include "t287MD.h"
#include "t183MD.h"
extern MethodInfo m1537_MI;
extern MethodInfo m1581_MI;
extern MethodInfo m1395_MI;
extern MethodInfo m2328_MI;
extern MethodInfo m1394_MI;


extern MethodInfo m2720_MI;
 void m2720 (t29 * __this, MethodInfo* method){
	t184  V_0 = {0};
	{
		t287  L_0 = {0};
		m1537(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), &m1537_MI);
		((t184_SFs*)InitializedTypeInfo(&t184_TI)->static_fields)->f6 = L_0;
		t183  L_1 = {0};
		m1581(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), &m1581_MI);
		((t184_SFs*)InitializedTypeInfo(&t184_TI)->static_fields)->f7 = L_1;
		Initobj (&t184_TI, (&V_0));
		t23  L_2 = m1395(NULL, &m1395_MI);
		(&V_0)->f0 = L_2;
		t23  L_3 = m2328(NULL, &m2328_MI);
		(&V_0)->f1 = L_3;
		(&V_0)->f5 = (((t184_SFs*)InitializedTypeInfo(&t184_TI)->static_fields)->f7);
		(&V_0)->f2 = (((t184_SFs*)InitializedTypeInfo(&t184_TI)->static_fields)->f6);
		t17  L_4 = m1394(NULL, &m1394_MI);
		(&V_0)->f3 = L_4;
		t17  L_5 = m1394(NULL, &m1394_MI);
		(&V_0)->f4 = L_5;
		((t184_SFs*)InitializedTypeInfo(&t184_TI)->static_fields)->f8 = V_0;
		return;
	}
}
// Metadata Definition UnityEngine.UIVertex
extern Il2CppType t23_0_0_6;
FieldInfo t184_f0_FieldInfo = 
{
	"position", &t23_0_0_6, &t184_TI, offsetof(t184, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t23_0_0_6;
FieldInfo t184_f1_FieldInfo = 
{
	"normal", &t23_0_0_6, &t184_TI, offsetof(t184, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t287_0_0_6;
FieldInfo t184_f2_FieldInfo = 
{
	"color", &t287_0_0_6, &t184_TI, offsetof(t184, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t184_f3_FieldInfo = 
{
	"uv0", &t17_0_0_6, &t184_TI, offsetof(t184, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t184_f4_FieldInfo = 
{
	"uv1", &t17_0_0_6, &t184_TI, offsetof(t184, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t183_0_0_6;
FieldInfo t184_f5_FieldInfo = 
{
	"tangent", &t183_0_0_6, &t184_TI, offsetof(t184, f5) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t287_0_0_49;
FieldInfo t184_f6_FieldInfo = 
{
	"s_DefaultColor", &t287_0_0_49, &t184_TI, offsetof(t184_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t183_0_0_49;
FieldInfo t184_f7_FieldInfo = 
{
	"s_DefaultTangent", &t183_0_0_49, &t184_TI, offsetof(t184_SFs, f7), &EmptyCustomAttributesCache};
extern Il2CppType t184_0_0_22;
FieldInfo t184_f8_FieldInfo = 
{
	"simpleVert", &t184_0_0_22, &t184_TI, offsetof(t184_SFs, f8), &EmptyCustomAttributesCache};
static FieldInfo* t184_FIs[] =
{
	&t184_f0_FieldInfo,
	&t184_f1_FieldInfo,
	&t184_f2_FieldInfo,
	&t184_f3_FieldInfo,
	&t184_f4_FieldInfo,
	&t184_f5_FieldInfo,
	&t184_f6_FieldInfo,
	&t184_f7_FieldInfo,
	&t184_f8_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2720_MI = 
{
	".cctor", (methodPointerType)&m2720, &t184_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 990, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t184_MIs[] =
{
	&m2720_MI,
	NULL
};
static MethodInfo* t184_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_1_0_0;
TypeInfo t184_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UIVertex", "UnityEngine", t184_MIs, NULL, t184_FIs, NULL, &t110_TI, NULL, NULL, &t184_TI, NULL, t184_VT, &EmptyCustomAttributesCache, &t184_TI, &t184_0_0_0, &t184_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t184)+ sizeof (Il2CppObject), 0, sizeof(t184 ), sizeof(t184_SFs), 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, true, true, true, 1, 0, 9, 0, 0, 4, 0, 0};
#include "t159.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t159_TI;
#include "t159MD.h"

#include "t162.h"
#include "t626.h"
extern TypeInfo t316_TI;
extern TypeInfo t626_TI;
#include "t494MD.h"
extern MethodInfo m2721_MI;
extern MethodInfo m1786_MI;
extern MethodInfo m2469_MI;
extern MethodInfo m1601_MI;
extern MethodInfo m2722_MI;
extern MethodInfo m2723_MI;


extern MethodInfo m1593_MI;
 void m1593 (t159 * __this, t132  p0, MethodInfo* method){
	{
		m2721(NULL, __this, (&p0), &m2721_MI);
		return;
	}
}
 void m2721 (t29 * __this, t159 * p0, t132 * p1, MethodInfo* method){
	typedef void (*m2721_ftn) (t159 *, t132 *);
	static m2721_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2721_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(p0, p1);
}
extern MethodInfo m1591_MI;
 t132  m1591 (t159 * __this, MethodInfo* method){
	typedef t132  (*m1591_ftn) (t159 *);
	static m1591_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1591_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m2005_MI;
 void m2005 (t159 * __this, bool p0, MethodInfo* method){
	typedef void (*m2005_ftn) (t159 *, bool);
	static m2005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_isMask(System.Boolean)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1578_MI;
 void m1578 (t159 * __this, t156 * p0, t162 * p1, MethodInfo* method){
	typedef void (*m1578_ftn) (t159 *, t156 *, t162 *);
	static m1578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, p0, p1);
}
extern MethodInfo m1576_MI;
 void m1576 (t159 * __this, t163 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0039;
		}
	}
	{
		t316* L_1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		uint16_t L_2 = ((int32_t)65535);
		t29 * L_3 = Box(InitializedTypeInfo(&t626_TI), &L_2);
		ArrayElementTypeCheck (L_1, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(L_1, 0)) = (t29 *)L_3;
		t7* L_4 = m2469(NULL, (t7*) &_stringLiteral151, L_1, &m2469_MI);
		m1904(NULL, L_4, __this, &m1904_MI);
		VirtActionInvoker0::Invoke(&m1601_MI, p0);
	}

IL_0039:
	{
		m2722(__this, p0, &m2722_MI);
		return;
	}
}
 void m2722 (t159 * __this, t29 * p0, MethodInfo* method){
	typedef void (*m2722_ftn) (t159 *, t29 *);
	static m2722_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2722_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, p0);
}
extern MethodInfo m1737_MI;
 void m1737 (t159 * __this, t201* p0, int32_t p1, MethodInfo* method){
	{
		if ((((int32_t)p1) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0031;
		}
	}
	{
		t316* L_0 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		uint16_t L_1 = ((int32_t)65535);
		t29 * L_2 = Box(InitializedTypeInfo(&t626_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		t7* L_3 = m2469(NULL, (t7*) &_stringLiteral151, L_0, &m2469_MI);
		m1904(NULL, L_3, __this, &m1904_MI);
		p1 = 0;
	}

IL_0031:
	{
		m2723(__this, p0, p1, &m2723_MI);
		return;
	}
}
 void m2723 (t159 * __this, t201* p0, int32_t p1, MethodInfo* method){
	typedef void (*m2723_ftn) (t159 *, t201*, int32_t);
	static m2723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)");
	_il2cpp_icall_func(__this, p0, p1);
}
extern MethodInfo m1569_MI;
 void m1569 (t159 * __this, MethodInfo* method){
	typedef void (*m1569_ftn) (t159 *);
	static m1569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
extern MethodInfo m1552_MI;
 int32_t m1552 (t159 * __this, MethodInfo* method){
	typedef int32_t (*m1552_ftn) (t159 *);
	static m1552_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1552_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// Metadata Definition UnityEngine.CanvasRenderer
static PropertyInfo t159____isMask_PropertyInfo = 
{
	&t159_TI, "isMask", NULL, &m2005_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t159____absoluteDepth_PropertyInfo = 
{
	&t159_TI, "absoluteDepth", &m1552_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t159_PIs[] =
{
	&t159____isMask_PropertyInfo,
	&t159____absoluteDepth_PropertyInfo,
	NULL
};
extern Il2CppType t132_0_0_0;
static ParameterInfo t159_m1593_ParameterInfos[] = 
{
	{"color", 0, 134218713, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m1593_MI = 
{
	"SetColor", (methodPointerType)&m1593, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t132, t159_m1593_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 991, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t159_0_0_0;
extern Il2CppType t159_0_0_0;
extern Il2CppType t132_1_0_0;
static ParameterInfo t159_m2721_ParameterInfos[] = 
{
	{"self", 0, 134218714, &EmptyCustomAttributesCache, &t159_0_0_0},
	{"color", 1, 134218715, &EmptyCustomAttributesCache, &t132_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t400 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m2721;
MethodInfo m2721_MI = 
{
	"INTERNAL_CALL_SetColor", (methodPointerType)&m2721, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t400, t159_m2721_ParameterInfos, &t159__CustomAttributeCache_m2721, 145, 4096, 255, 2, false, false, 992, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t132_0_0_0;
extern void* RuntimeInvoker_t132 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m1591;
MethodInfo m1591_MI = 
{
	"GetColor", (methodPointerType)&m1591, &t159_TI, &t132_0_0_0, RuntimeInvoker_t132, NULL, &t159__CustomAttributeCache_m1591, 134, 4096, 255, 0, false, false, 993, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t159_m2005_ParameterInfos[] = 
{
	{"value", 0, 134218716, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m2005;
MethodInfo m2005_MI = 
{
	"set_isMask", (methodPointerType)&m2005, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t159_m2005_ParameterInfos, &t159__CustomAttributeCache_m2005, 2182, 4096, 255, 1, false, false, 994, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
extern Il2CppType t156_0_0_0;
extern Il2CppType t162_0_0_0;
extern Il2CppType t162_0_0_0;
static ParameterInfo t159_m1578_ParameterInfos[] = 
{
	{"material", 0, 134218717, &EmptyCustomAttributesCache, &t156_0_0_0},
	{"texture", 1, 134218718, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m1578;
MethodInfo m1578_MI = 
{
	"SetMaterial", (methodPointerType)&m1578, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t159_m1578_ParameterInfos, &t159__CustomAttributeCache_m1578, 134, 4096, 255, 2, false, false, 995, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
static ParameterInfo t159_m1576_ParameterInfos[] = 
{
	{"vertices", 0, 134218719, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1576_MI = 
{
	"SetVertices", (methodPointerType)&m1576, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t159_m1576_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 996, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t159_m2722_ParameterInfos[] = 
{
	{"vertices", 0, 134218720, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m2722;
MethodInfo m2722_MI = 
{
	"SetVerticesInternal", (methodPointerType)&m2722, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t159_m2722_ParameterInfos, &t159__CustomAttributeCache_m2722, 129, 4096, 255, 1, false, false, 997, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t201_0_0_0;
extern Il2CppType t201_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t159_m1737_ParameterInfos[] = 
{
	{"vertices", 0, 134218721, &EmptyCustomAttributesCache, &t201_0_0_0},
	{"size", 1, 134218722, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1737_MI = 
{
	"SetVertices", (methodPointerType)&m1737, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t159_m1737_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 998, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t201_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t159_m2723_ParameterInfos[] = 
{
	{"vertices", 0, 134218723, &EmptyCustomAttributesCache, &t201_0_0_0},
	{"size", 1, 134218724, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m2723;
MethodInfo m2723_MI = 
{
	"SetVerticesInternalArray", (methodPointerType)&m2723, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t159_m2723_ParameterInfos, &t159__CustomAttributeCache_m2723, 129, 4096, 255, 2, false, false, 999, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m1569;
MethodInfo m1569_MI = 
{
	"Clear", (methodPointerType)&m1569, &t159_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t159__CustomAttributeCache_m1569, 134, 4096, 255, 0, false, false, 1000, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t159__CustomAttributeCache_m1552;
MethodInfo m1552_MI = 
{
	"get_absoluteDepth", (methodPointerType)&m1552, &t159_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t159__CustomAttributeCache_m1552, 2182, 4096, 255, 0, false, false, 1001, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t159_MIs[] =
{
	&m1593_MI,
	&m2721_MI,
	&m1591_MI,
	&m2005_MI,
	&m1578_MI,
	&m1576_MI,
	&m2722_MI,
	&m1737_MI,
	&m2723_MI,
	&m1569_MI,
	&m1552_MI,
	NULL
};
static MethodInfo* t159_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t159_CustomAttributesCacheGenerator_m2721(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m1591(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m2005(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m1578(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m2722(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m2723(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m1569(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t159_CustomAttributesCacheGenerator_m1552(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t159__CustomAttributeCache_m2721 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m2721
};
CustomAttributesCache t159__CustomAttributeCache_m1591 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m1591
};
CustomAttributesCache t159__CustomAttributeCache_m2005 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m2005
};
CustomAttributesCache t159__CustomAttributeCache_m1578 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m1578
};
CustomAttributesCache t159__CustomAttributeCache_m2722 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m2722
};
CustomAttributesCache t159__CustomAttributeCache_m2723 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m2723
};
CustomAttributesCache t159__CustomAttributeCache_m1569 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m1569
};
CustomAttributesCache t159__CustomAttributeCache_m1552 = {
1,
NULL,
&t159_CustomAttributesCacheGenerator_m1552
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t159_1_0_0;
struct t159;
extern CustomAttributesCache t159__CustomAttributeCache_m2721;
extern CustomAttributesCache t159__CustomAttributeCache_m1591;
extern CustomAttributesCache t159__CustomAttributeCache_m2005;
extern CustomAttributesCache t159__CustomAttributeCache_m1578;
extern CustomAttributesCache t159__CustomAttributeCache_m2722;
extern CustomAttributesCache t159__CustomAttributeCache_m2723;
extern CustomAttributesCache t159__CustomAttributeCache_m1569;
extern CustomAttributesCache t159__CustomAttributeCache_m1552;
TypeInfo t159_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CanvasRenderer", "UnityEngine", t159_MIs, t159_PIs, NULL, NULL, &t28_TI, NULL, NULL, &t159_TI, NULL, t159_VT, &EmptyCustomAttributesCache, &t159_TI, &t159_0_0_0, &t159_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t159), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 11, 2, 0, 0, 0, 4, 0, 0};
#include "t358.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t358_TI;
#include "t358MD.h"

#include "t379.h"
extern TypeInfo t223_TI;
extern TypeInfo t23_TI;
extern TypeInfo t329_TI;
#include "t379MD.h"
#include "t24MD.h"
#include "t2MD.h"
extern MethodInfo m2725_MI;
extern MethodInfo m2726_MI;
extern MethodInfo m2727_MI;
extern MethodInfo m1463_MI;
extern MethodInfo m2729_MI;
extern MethodInfo m1745_MI;
extern MethodInfo m1746_MI;
extern MethodInfo m1747_MI;
extern MethodInfo m2728_MI;
extern MethodInfo m1419_MI;
extern MethodInfo m1464_MI;
extern MethodInfo m2415_MI;
extern MethodInfo m1836_MI;
extern MethodInfo m1658_MI;
extern MethodInfo m1672_MI;
extern MethodInfo m1687_MI;
extern MethodInfo m1798_MI;
extern MethodInfo m1794_MI;
extern MethodInfo m1797_MI;
extern MethodInfo m1662_MI;
extern MethodInfo m1793_MI;
extern MethodInfo m1796_MI;
extern MethodInfo m1663_MI;
extern MethodInfo m1835_MI;
extern MethodInfo m2730_MI;
extern MethodInfo m61_MI;
extern MethodInfo m63_MI;


extern MethodInfo m2724_MI;
 void m2724 (t29 * __this, MethodInfo* method){
	{
		((t358_SFs*)InitializedTypeInfo(&t358_TI)->static_fields)->f0 = ((t223*)SZArrayNew(InitializedTypeInfo(&t223_TI), 4));
		return;
	}
}
extern MethodInfo m1628_MI;
 bool m1628 (t29 * __this, t2 * p0, t17  p1, t24 * p2, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_0 = m2725(NULL, p0, (&p1), p2, &m2725_MI);
		return L_0;
	}
}
 bool m2725 (t29 * __this, t2 * p0, t17 * p1, t24 * p2, MethodInfo* method){
	typedef bool (*m2725_ftn) (t2 *, t17 *, t24 *);
	static m2725_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2725_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(p0, p1, p2);
}
extern MethodInfo m1589_MI;
 t17  m1589 (t29 * __this, t17  p0, t25 * p1, t3 * p2, MethodInfo* method){
	t17  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m2726(NULL, p0, p1, p2, (&V_0), &m2726_MI);
		return V_0;
	}
}
 void m2726 (t29 * __this, t17  p0, t25 * p1, t3 * p2, t17 * p3, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m2727(NULL, (&p0), p1, p2, p3, &m2727_MI);
		return;
	}
}
 void m2727 (t29 * __this, t17 * p0, t25 * p1, t3 * p2, t17 * p3, MethodInfo* method){
	typedef void (*m2727_ftn) (t17 *, t25 *, t3 *, t17 *);
	static m2727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(p0, p1, p2, p3);
}
extern MethodInfo m1590_MI;
 t164  m1590 (t29 * __this, t2 * p0, t3 * p1, MethodInfo* method){
	typedef t164  (*m1590_ftn) (t2 *, t3 *);
	static m1590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m1590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(p0, p1);
}
 bool m2728 (t29 * __this, t2 * p0, t17  p1, t24 * p2, t23 * p3, MethodInfo* method){
	t329  V_0 = {0};
	t379  V_1 = {0};
	float V_2 = 0.0f;
	{
		t17  L_0 = m1394(NULL, &m1394_MI);
		t23  L_1 = m1463(NULL, L_0, &m1463_MI);
		*p3 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		t329  L_2 = m2729(NULL, p2, p1, &m2729_MI);
		V_0 = L_2;
		t362  L_3 = m1619(p0, &m1619_MI);
		t23  L_4 = m2328(NULL, &m2328_MI);
		t23  L_5 = m1621(NULL, L_3, L_4, &m1621_MI);
		t23  L_6 = m40(p0, &m40_MI);
		m1745((&V_1), L_5, L_6, &m1745_MI);
		bool L_7 = m1746((&V_1), V_0, (&V_2), &m1746_MI);
		if (L_7)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		t23  L_8 = m1747((&V_0), V_2, &m1747_MI);
		*p3 = L_8;
		return 1;
	}
}
extern MethodInfo m1677_MI;
 bool m1677 (t29 * __this, t2 * p0, t17  p1, t24 * p2, t17 * p3, MethodInfo* method){
	t23  V_0 = {0};
	{
		t17  L_0 = m1394(NULL, &m1394_MI);
		*p3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_1 = m2728(NULL, p0, p1, p2, (&V_0), &m2728_MI);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		t23  L_2 = m1744(p0, V_0, &m1744_MI);
		t17  L_3 = m1419(NULL, L_2, &m1419_MI);
		*p3 = L_3;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
 t329  m2729 (t29 * __this, t24 * p0, t17  p1, MethodInfo* method){
	t23  V_0 = {0};
	{
		bool L_0 = m1300(NULL, p0, (t41 *)NULL, &m1300_MI);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		t23  L_1 = m1463(NULL, p1, &m1463_MI);
		t329  L_2 = m1464(p0, L_1, &m1464_MI);
		return L_2;
	}

IL_0019:
	{
		t23  L_3 = m1463(NULL, p1, &m1463_MI);
		V_0 = L_3;
		t23 * L_4 = (&V_0);
		float L_5 = (L_4->f3);
		L_4->f3 = ((float)(L_5-(100.0f)));
		t23  L_6 = m1620(NULL, &m1620_MI);
		t329  L_7 = {0};
		m2415(&L_7, V_0, L_6, &m2415_MI);
		return L_7;
	}
}
 void m1836 (t29 * __this, t2 * p0, int32_t p1, bool p2, bool p3, MethodInfo* method){
	int32_t V_0 = 0;
	t2 * V_1 = {0};
	t17  V_2 = {0};
	t17  V_3 = {0};
	t17  V_4 = {0};
	t17  V_5 = {0};
	float V_6 = 0.0f;
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		if (!p3)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		t25 * L_1 = m1991(p0, V_0, &m1991_MI);
		V_1 = ((t2 *)IsInst(L_1, InitializedTypeInfo(&t2_TI)));
		bool L_2 = m1300(NULL, V_1, (t41 *)NULL, &m1300_MI);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m1836(NULL, V_1, p1, 0, 1, &m1836_MI);
	}

IL_003c:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0040:
	{
		int32_t L_3 = m1993(p0, &m1993_MI);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		t17  L_4 = m1658(p0, &m1658_MI);
		V_2 = L_4;
		float L_5 = m1672((&V_2), p1, &m1672_MI);
		m1687((&V_2), p1, ((float)((1.0f)-L_5)), &m1687_MI);
		m1798(p0, V_2, &m1798_MI);
		if (!p2)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		t17  L_6 = m1794(p0, &m1794_MI);
		V_3 = L_6;
		float L_7 = m1672((&V_3), p1, &m1672_MI);
		m1687((&V_3), p1, ((-L_7)), &m1687_MI);
		m1797(p0, V_3, &m1797_MI);
		t17  L_8 = m1662(p0, &m1662_MI);
		V_4 = L_8;
		t17  L_9 = m1793(p0, &m1793_MI);
		V_5 = L_9;
		float L_10 = m1672((&V_4), p1, &m1672_MI);
		V_6 = L_10;
		float L_11 = m1672((&V_5), p1, &m1672_MI);
		m1687((&V_4), p1, ((float)((1.0f)-L_11)), &m1687_MI);
		m1687((&V_5), p1, ((float)((1.0f)-V_6)), &m1687_MI);
		m1796(p0, V_4, &m1796_MI);
		m1663(p0, V_5, &m1663_MI);
		return;
	}
}
 void m1835 (t29 * __this, t2 * p0, bool p1, bool p2, MethodInfo* method){
	int32_t V_0 = 0;
	t2 * V_1 = {0};
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		if (!p2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		t25 * L_1 = m1991(p0, V_0, &m1991_MI);
		V_1 = ((t2 *)IsInst(L_1, InitializedTypeInfo(&t2_TI)));
		bool L_2 = m1300(NULL, V_1, (t41 *)NULL, &m1300_MI);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m1835(NULL, V_1, 0, 1, &m1835_MI);
	}

IL_003b:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_003f:
	{
		int32_t L_3 = m1993(p0, &m1993_MI);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		t17  L_4 = m1658(p0, &m1658_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		t17  L_5 = m2730(NULL, L_4, &m2730_MI);
		m1798(p0, L_5, &m1798_MI);
		t17  L_6 = m61(p0, &m61_MI);
		t17  L_7 = m2730(NULL, L_6, &m2730_MI);
		m63(p0, L_7, &m63_MI);
		if (!p1)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		t17  L_8 = m1794(p0, &m1794_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		t17  L_9 = m2730(NULL, L_8, &m2730_MI);
		m1797(p0, L_9, &m1797_MI);
		t17  L_10 = m1662(p0, &m1662_MI);
		t17  L_11 = m2730(NULL, L_10, &m2730_MI);
		m1796(p0, L_11, &m1796_MI);
		t17  L_12 = m1793(p0, &m1793_MI);
		t17  L_13 = m2730(NULL, L_12, &m2730_MI);
		m1663(p0, L_13, &m1663_MI);
		return;
	}
}
 t17  m2730 (t29 * __this, t17  p0, MethodInfo* method){
	{
		float L_0 = ((&p0)->f2);
		float L_1 = ((&p0)->f1);
		t17  L_2 = {0};
		m62(&L_2, L_0, L_1, &m62_MI);
		return L_2;
	}
}
// Metadata Definition UnityEngine.RectTransformUtility
extern Il2CppType t223_0_0_17;
FieldInfo t358_f0_FieldInfo = 
{
	"s_Corners", &t223_0_0_17, &t358_TI, offsetof(t358_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t358_FIs[] =
{
	&t358_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2724_MI = 
{
	".cctor", (methodPointerType)&m2724, &t358_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1002, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t358_m1628_ParameterInfos[] = 
{
	{"rect", 0, 134218725, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"screenPoint", 1, 134218726, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"cam", 2, 134218727, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t17_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1628_MI = 
{
	"RectangleContainsScreenPoint", (methodPointerType)&m1628, &t358_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t17_t29, t358_m1628_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1003, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t17_1_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t358_m2725_ParameterInfos[] = 
{
	{"rect", 0, 134218728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"screenPoint", 1, 134218729, &EmptyCustomAttributesCache, &t17_1_0_0},
	{"cam", 2, 134218730, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t594_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t358__CustomAttributeCache_m2725;
MethodInfo m2725_MI = 
{
	"INTERNAL_CALL_RectangleContainsScreenPoint", (methodPointerType)&m2725, &t358_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t594_t29, t358_m2725_ParameterInfos, &t358__CustomAttributeCache_m2725, 145, 4096, 255, 3, false, false, 1004, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t25_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t358_m1589_ParameterInfos[] = 
{
	{"point", 0, 134218731, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"elementTransform", 1, 134218732, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"canvas", 2, 134218733, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17_t17_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1589_MI = 
{
	"PixelAdjustPoint", (methodPointerType)&m1589, &t358_TI, &t17_0_0_0, RuntimeInvoker_t17_t17_t29_t29, t358_m1589_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1005, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t25_0_0_0;
extern Il2CppType t3_0_0_0;
extern Il2CppType t17_1_0_2;
static ParameterInfo t358_m2726_ParameterInfos[] = 
{
	{"point", 0, 134218734, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"elementTransform", 1, 134218735, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"canvas", 2, 134218736, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"output", 3, 134218737, &EmptyCustomAttributesCache, &t17_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17_t29_t29_t594 (MethodInfo* method, void* obj, void** args);
MethodInfo m2726_MI = 
{
	"PixelAdjustPoint", (methodPointerType)&m2726, &t358_TI, &t21_0_0_0, RuntimeInvoker_t21_t17_t29_t29_t594, t358_m2726_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 4, false, false, 1006, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_1_0_0;
extern Il2CppType t25_0_0_0;
extern Il2CppType t3_0_0_0;
extern Il2CppType t17_1_0_2;
static ParameterInfo t358_m2727_ParameterInfos[] = 
{
	{"point", 0, 134218738, &EmptyCustomAttributesCache, &t17_1_0_0},
	{"elementTransform", 1, 134218739, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"canvas", 2, 134218740, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"output", 3, 134218741, &EmptyCustomAttributesCache, &t17_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t594_t29_t29_t594 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t358__CustomAttributeCache_m2727;
MethodInfo m2727_MI = 
{
	"INTERNAL_CALL_PixelAdjustPoint", (methodPointerType)&m2727, &t358_TI, &t21_0_0_0, RuntimeInvoker_t21_t594_t29_t29_t594, t358_m2727_ParameterInfos, &t358__CustomAttributeCache_m2727, 145, 4096, 255, 4, false, false, 1007, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t358_m1590_ParameterInfos[] = 
{
	{"rectTransform", 0, 134218742, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"canvas", 1, 134218743, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t164_0_0_0;
extern void* RuntimeInvoker_t164_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t358__CustomAttributeCache_m1590;
MethodInfo m1590_MI = 
{
	"PixelAdjustRect", (methodPointerType)&m1590, &t358_TI, &t164_0_0_0, RuntimeInvoker_t164_t29_t29, t358_m1590_ParameterInfos, &t358__CustomAttributeCache_m1590, 150, 4096, 255, 2, false, false, 1008, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t24_0_0_0;
extern Il2CppType t23_1_0_2;
static ParameterInfo t358_m2728_ParameterInfos[] = 
{
	{"rect", 0, 134218744, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"screenPoint", 1, 134218745, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"cam", 2, 134218746, &EmptyCustomAttributesCache, &t24_0_0_0},
	{"worldPoint", 3, 134218747, &EmptyCustomAttributesCache, &t23_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t17_t29_t587 (MethodInfo* method, void* obj, void** args);
MethodInfo m2728_MI = 
{
	"ScreenPointToWorldPointInRectangle", (methodPointerType)&m2728, &t358_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t17_t29_t587, t358_m2728_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 1009, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t17_0_0_0;
extern Il2CppType t24_0_0_0;
extern Il2CppType t17_1_0_2;
static ParameterInfo t358_m1677_ParameterInfos[] = 
{
	{"rect", 0, 134218748, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"screenPoint", 1, 134218749, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"cam", 2, 134218750, &EmptyCustomAttributesCache, &t24_0_0_0},
	{"localPoint", 3, 134218751, &EmptyCustomAttributesCache, &t17_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t17_t29_t594 (MethodInfo* method, void* obj, void** args);
MethodInfo m1677_MI = 
{
	"ScreenPointToLocalPointInRectangle", (methodPointerType)&m1677, &t358_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t17_t29_t594, t358_m1677_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 1010, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t24_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t358_m2729_ParameterInfos[] = 
{
	{"cam", 0, 134218752, &EmptyCustomAttributesCache, &t24_0_0_0},
	{"screenPos", 1, 134218753, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t329_0_0_0;
extern void* RuntimeInvoker_t329_t29_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2729_MI = 
{
	"ScreenPointToRay", (methodPointerType)&m2729, &t358_TI, &t329_0_0_0, RuntimeInvoker_t329_t29_t17, t358_m2729_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 1011, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t358_m1836_ParameterInfos[] = 
{
	{"rect", 0, 134218754, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"axis", 1, 134218755, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"keepPositioning", 2, 134218756, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"recursive", 3, 134218757, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1836_MI = 
{
	"FlipLayoutOnAxis", (methodPointerType)&m1836, &t358_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t297_t297, t358_m1836_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 1012, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t358_m1835_ParameterInfos[] = 
{
	{"rect", 0, 134218758, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"keepPositioning", 1, 134218759, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"recursive", 2, 134218760, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1835_MI = 
{
	"FlipLayoutAxes", (methodPointerType)&m1835, &t358_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297_t297, t358_m1835_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1013, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t358_m2730_ParameterInfos[] = 
{
	{"input", 0, 134218761, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2730_MI = 
{
	"GetTransposed", (methodPointerType)&m2730, &t358_TI, &t17_0_0_0, RuntimeInvoker_t17_t17, t358_m2730_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1014, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t358_MIs[] =
{
	&m2724_MI,
	&m1628_MI,
	&m2725_MI,
	&m1589_MI,
	&m2726_MI,
	&m2727_MI,
	&m1590_MI,
	&m2728_MI,
	&m1677_MI,
	&m2729_MI,
	&m1836_MI,
	&m1835_MI,
	&m2730_MI,
	NULL
};
static MethodInfo* t358_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t358_CustomAttributesCacheGenerator_m2725(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t358_CustomAttributesCacheGenerator_m2727(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t358_CustomAttributesCacheGenerator_m1590(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t532 * tmp;
		tmp = (t532 *)il2cpp_codegen_object_new (&t532_TI);
		m2731(tmp, &m2731_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t358__CustomAttributeCache_m2725 = {
1,
NULL,
&t358_CustomAttributesCacheGenerator_m2725
};
CustomAttributesCache t358__CustomAttributeCache_m2727 = {
1,
NULL,
&t358_CustomAttributesCacheGenerator_m2727
};
CustomAttributesCache t358__CustomAttributeCache_m1590 = {
1,
NULL,
&t358_CustomAttributesCacheGenerator_m1590
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t358_0_0_0;
extern Il2CppType t358_1_0_0;
struct t358;
extern CustomAttributesCache t358__CustomAttributeCache_m2725;
extern CustomAttributesCache t358__CustomAttributeCache_m2727;
extern CustomAttributesCache t358__CustomAttributeCache_m1590;
TypeInfo t358_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RectTransformUtility", "UnityEngine", t358_MIs, NULL, t358_FIs, NULL, &t29_TI, NULL, NULL, &t358_TI, NULL, t358_VT, &EmptyCustomAttributesCache, &t358_TI, &t358_0_0_0, &t358_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t358), 0, -1, sizeof(t358_SFs), 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, true, false, false, 13, 0, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t490MD.h"
extern MethodInfo m2881_MI;


 void m2731 (t532 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.WrapperlessIcall
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2731_MI = 
{
	".ctor", (methodPointerType)&m2731, &t532_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1015, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t532_MIs[] =
{
	&m2731_MI,
	NULL
};
extern MethodInfo m2882_MI;
extern MethodInfo m2883_MI;
static MethodInfo* t532_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
extern TypeInfo t604_TI;
static Il2CppInterfaceOffsetPair t532_IOs[] = 
{
	{ &t604_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t532_0_0_0;
extern Il2CppType t532_1_0_0;
extern TypeInfo t490_TI;
struct t532;
TypeInfo t532_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "WrapperlessIcall", "UnityEngine", t532_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t532_TI, NULL, t532_VT, &EmptyCustomAttributesCache, &t532_TI, &t532_0_0_0, &t532_1_0_0, t532_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t532), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t533.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t533_TI;
#include "t533MD.h"

#include "t359.h"
#include "t360.h"
#include "t318.h"
#include "t627.h"
#include "t4.h"
#include "t43.h"
#include "t296.h"
#include "t628.h"
extern TypeInfo t534_TI;
extern TypeInfo t359_TI;
extern TypeInfo t535_TI;
extern TypeInfo t360_TI;
extern TypeInfo t536_TI;
extern TypeInfo t318_TI;
extern TypeInfo t627_TI;
extern TypeInfo t42_TI;
extern TypeInfo t4_TI;
extern TypeInfo t296_TI;
extern TypeInfo t40_TI;
extern TypeInfo t537_TI;
extern TypeInfo t628_TI;
#include "t627MD.h"
#include "t42MD.h"
#include "t296MD.h"
#include "t628MD.h"
extern Il2CppType t4_0_0_0;
extern Il2CppType t359_0_0_0;
extern Il2CppType t318_0_0_0;
extern Il2CppType t360_0_0_0;
extern MethodInfo m2906_MI;
extern MethodInfo m2907_MI;
extern MethodInfo m2908_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2909_MI;
extern MethodInfo m2910_MI;
extern MethodInfo m2911_MI;
extern MethodInfo m2912_MI;
extern MethodInfo m2913_MI;
extern MethodInfo m2914_MI;


extern MethodInfo m2732_MI;
 void m2732 (t29 * __this, MethodInfo* method){
	{
		((t533_SFs*)InitializedTypeInfo(&t533_TI)->static_fields)->f0 = ((t534*)SZArrayNew(InitializedTypeInfo(&t534_TI), 1));
		((t533_SFs*)InitializedTypeInfo(&t533_TI)->static_fields)->f1 = ((t535*)SZArrayNew(InitializedTypeInfo(&t535_TI), 1));
		((t533_SFs*)InitializedTypeInfo(&t533_TI)->static_fields)->f2 = ((t536*)SZArrayNew(InitializedTypeInfo(&t536_TI), 1));
		return;
	}
}
extern MethodInfo m2733_MI;
 t42 * m2733 (t29 * __this, t42 * p0, MethodInfo* method){
	t627 * V_0 = {0};
	t42 * V_1 = {0};
	t316* V_2 = {0};
	{
		t627 * L_0 = (t627 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t627_TI));
		m2906(L_0, &m2906_MI);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		m2907(V_0, p0, &m2907_MI);
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		p0 = L_1;
	}

IL_001a:
	{
		if (!p0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t4_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_2)))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (t42 *)NULL;
		goto IL_005a;
	}

IL_0037:
	{
		t42 * L_3 = m2909(V_0, &m2909_MI);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t359_0_0_0), &m1554_MI);
		t316* L_5 = (t316*)VirtFuncInvoker2< t316*, t42 *, bool >::Invoke(&m2910_MI, V_1, L_4, 0);
		V_2 = L_5;
		if (!(((int32_t)(((t20 *)V_2)->max_length))))
		{
			goto IL_005a;
		}
	}
	{
		return V_1;
	}

IL_005a:
	{
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2911_MI, V_0);
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (t42 *)NULL;
	}
}
extern MethodInfo m2734_MI;
 t537* m2734 (t29 * __this, t42 * p0, MethodInfo* method){
	t628 * V_0 = {0};
	t316* V_1 = {0};
	int32_t V_2 = 0;
	t318 * V_3 = {0};
	t537* V_4 = {0};
	{
		V_0 = (t628 *)NULL;
		goto IL_00d9;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t318_0_0_0), &m1554_MI);
		t316* L_1 = (t316*)VirtFuncInvoker2< t316*, t42 *, bool >::Invoke(&m2910_MI, p0, L_0, 0);
		V_1 = L_1;
		V_2 = 0;
		goto IL_00c8;
	}

IL_0020:
	{
		int32_t L_2 = V_2;
		V_3 = ((t318 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_1, L_2)), InitializedTypeInfo(&t318_TI)));
		if (V_0)
		{
			goto IL_0073;
		}
	}
	{
		if ((((uint32_t)(((int32_t)(((t20 *)V_1)->max_length)))) != ((uint32_t)1)))
		{
			goto IL_0073;
		}
	}
	{
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t4_0_0_0), &m1554_MI);
		if ((((t42 *)L_3) != ((t42 *)L_4)))
		{
			goto IL_0073;
		}
	}
	{
		t537* L_5 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 3));
		t42 * L_6 = (V_3->f0);
		ArrayElementTypeCheck (L_5, L_6);
		*((t42 **)(t42 **)SZArrayLdElema(L_5, 0)) = (t42 *)L_6;
		t537* L_7 = L_5;
		t42 * L_8 = (V_3->f1);
		ArrayElementTypeCheck (L_7, L_8);
		*((t42 **)(t42 **)SZArrayLdElema(L_7, 1)) = (t42 *)L_8;
		t537* L_9 = L_7;
		t42 * L_10 = (V_3->f2);
		ArrayElementTypeCheck (L_9, L_10);
		*((t42 **)(t42 **)SZArrayLdElema(L_9, 2)) = (t42 *)L_10;
		V_4 = L_9;
		return V_4;
	}

IL_0073:
	{
		if (V_0)
		{
			goto IL_007f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t628_TI));
		t628 * L_11 = (t628 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t628_TI));
		m2912(L_11, &m2912_MI);
		V_0 = L_11;
	}

IL_007f:
	{
		t42 * L_12 = (V_3->f0);
		if (!L_12)
		{
			goto IL_0096;
		}
	}
	{
		t42 * L_13 = (V_3->f0);
		VirtActionInvoker1< t42 * >::Invoke(&m2913_MI, V_0, L_13);
	}

IL_0096:
	{
		t42 * L_14 = (V_3->f1);
		if (!L_14)
		{
			goto IL_00ad;
		}
	}
	{
		t42 * L_15 = (V_3->f1);
		VirtActionInvoker1< t42 * >::Invoke(&m2913_MI, V_0, L_15);
	}

IL_00ad:
	{
		t42 * L_16 = (V_3->f2);
		if (!L_16)
		{
			goto IL_00c4;
		}
	}
	{
		t42 * L_17 = (V_3->f2);
		VirtActionInvoker1< t42 * >::Invoke(&m2913_MI, V_0, L_17);
	}

IL_00c4:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_00c8:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0020;
		}
	}
	{
		t42 * L_18 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		p0 = L_18;
	}

IL_00d9:
	{
		if (!p0)
		{
			goto IL_00ef;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_19 = m1554(NULL, LoadTypeToken(&t4_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_19)))
		{
			goto IL_0007;
		}
	}

IL_00ef:
	{
		if (V_0)
		{
			goto IL_00f7;
		}
	}
	{
		return (t537*)NULL;
	}

IL_00f7:
	{
		t537* L_20 = m2914(V_0, &m2914_MI);
		return L_20;
	}
}
extern MethodInfo m2735_MI;
 bool m2735 (t29 * __this, t42 * p0, MethodInfo* method){
	t316* V_0 = {0};
	{
		goto IL_0029;
	}

IL_0005:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t360_0_0_0), &m1554_MI);
		t316* L_1 = (t316*)VirtFuncInvoker2< t316*, t42 *, bool >::Invoke(&m2910_MI, p0, L_0, 0);
		V_0 = L_1;
		if (!(((int32_t)(((t20 *)V_0)->max_length))))
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		p0 = L_2;
	}

IL_0029:
	{
		if (!p0)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t4_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_3)))
		{
			goto IL_0005;
		}
	}

IL_003f:
	{
		return 0;
	}
}
// Metadata Definition UnityEngine.AttributeHelperEngine
extern Il2CppType t534_0_0_22;
FieldInfo t533_f0_FieldInfo = 
{
	"_disallowMultipleComponentArray", &t534_0_0_22, &t533_TI, offsetof(t533_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t535_0_0_22;
FieldInfo t533_f1_FieldInfo = 
{
	"_executeInEditModeArray", &t535_0_0_22, &t533_TI, offsetof(t533_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t536_0_0_22;
FieldInfo t533_f2_FieldInfo = 
{
	"_requireComponentArray", &t536_0_0_22, &t533_TI, offsetof(t533_SFs, f2), &EmptyCustomAttributesCache};
static FieldInfo* t533_FIs[] =
{
	&t533_f0_FieldInfo,
	&t533_f1_FieldInfo,
	&t533_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2732_MI = 
{
	".cctor", (methodPointerType)&m2732, &t533_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1016, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t533_m2733_ParameterInfos[] = 
{
	{"type", 0, 134218762, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2733_MI = 
{
	"GetParentTypeDisallowingMultipleInclusion", (methodPointerType)&m2733, &t533_TI, &t42_0_0_0, RuntimeInvoker_t29_t29, t533_m2733_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1017, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t533_m2734_ParameterInfos[] = 
{
	{"klass", 0, 134218763, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2734_MI = 
{
	"GetRequiredComponents", (methodPointerType)&m2734, &t533_TI, &t537_0_0_0, RuntimeInvoker_t29_t29, t533_m2734_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1018, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t533_m2735_ParameterInfos[] = 
{
	{"klass", 0, 134218764, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2735_MI = 
{
	"CheckIsEditorScript", (methodPointerType)&m2735, &t533_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t533_m2735_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1019, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t533_MIs[] =
{
	&m2732_MI,
	&m2733_MI,
	&m2734_MI,
	&m2735_MI,
	NULL
};
static MethodInfo* t533_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t533_0_0_0;
extern Il2CppType t533_1_0_0;
struct t533;
TypeInfo t533_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AttributeHelperEngine", "UnityEngine", t533_MIs, NULL, t533_FIs, NULL, &t29_TI, NULL, NULL, &t533_TI, NULL, t533_VT, &EmptyCustomAttributesCache, &t533_TI, &t533_0_0_0, &t533_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t533), 0, -1, sizeof(t533_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 0, 3, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
#include "t359MD.h"



extern MethodInfo m1603_MI;
 void m1603 (t359 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1603_MI = 
{
	".ctor", (methodPointerType)&m1603, &t359_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1020, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t359_MIs[] =
{
	&m1603_MI,
	NULL
};
static MethodInfo* t359_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t359_IOs[] = 
{
	{ &t604_TI, 4},
};
extern TypeInfo t629_TI;
#include "t629.h"
#include "t629MD.h"
extern MethodInfo m2915_MI;
extern MethodInfo m2916_MI;
void t359_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t359__CustomAttributeCache = {
1,
NULL,
&t359_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t359_1_0_0;
struct t359;
extern CustomAttributesCache t359__CustomAttributeCache;
TypeInfo t359_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DisallowMultipleComponent", "UnityEngine", t359_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t359_TI, NULL, t359_VT, &t359__CustomAttributeCache, &t359_TI, &t359_0_0_0, &t359_1_0_0, t359_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t359), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#ifndef _MSC_VER
#else
#endif
#include "t318MD.h"



extern MethodInfo m1404_MI;
 void m1404 (t318 * __this, t42 * p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.RequireComponent
extern Il2CppType t42_0_0_6;
FieldInfo t318_f0_FieldInfo = 
{
	"m_Type0", &t42_0_0_6, &t318_TI, offsetof(t318, f0), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_6;
FieldInfo t318_f1_FieldInfo = 
{
	"m_Type1", &t42_0_0_6, &t318_TI, offsetof(t318, f1), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_6;
FieldInfo t318_f2_FieldInfo = 
{
	"m_Type2", &t42_0_0_6, &t318_TI, offsetof(t318, f2), &EmptyCustomAttributesCache};
static FieldInfo* t318_FIs[] =
{
	&t318_f0_FieldInfo,
	&t318_f1_FieldInfo,
	&t318_f2_FieldInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t318_m1404_ParameterInfos[] = 
{
	{"requiredComponent", 0, 134218765, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1404_MI = 
{
	".ctor", (methodPointerType)&m1404, &t318_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t318_m1404_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1021, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t318_MIs[] =
{
	&m1404_MI,
	NULL
};
static MethodInfo* t318_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t318_IOs[] = 
{
	{ &t604_TI, 4},
};
extern MethodInfo m2917_MI;
void t318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		m2917(tmp, true, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t318__CustomAttributeCache = {
1,
NULL,
&t318_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t318_1_0_0;
struct t318;
extern CustomAttributesCache t318__CustomAttributeCache;
TypeInfo t318_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RequireComponent", "UnityEngine", t318_MIs, NULL, t318_FIs, NULL, &t490_TI, NULL, NULL, &t318_TI, NULL, t318_VT, &t318__CustomAttributeCache, &t318_TI, &t318_0_0_0, &t318_1_0_0, t318_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t318), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 3, 0, 0, 4, 0, 1};
#include "t298.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t298_TI;
#include "t298MD.h"



extern MethodInfo m1317_MI;
 void m1317 (t298 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		__this->f1 = 0;
		return;
	}
}
extern MethodInfo m1519_MI;
 void m1519 (t298 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
// Metadata Definition UnityEngine.AddComponentMenu
extern Il2CppType t7_0_0_1;
FieldInfo t298_f0_FieldInfo = 
{
	"m_AddComponentMenu", &t7_0_0_1, &t298_TI, offsetof(t298, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t298_f1_FieldInfo = 
{
	"m_Ordering", &t44_0_0_1, &t298_TI, offsetof(t298, f1), &EmptyCustomAttributesCache};
static FieldInfo* t298_FIs[] =
{
	&t298_f0_FieldInfo,
	&t298_f1_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t298_m1317_ParameterInfos[] = 
{
	{"menuName", 0, 134218766, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1317_MI = 
{
	".ctor", (methodPointerType)&m1317, &t298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t298_m1317_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1022, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t298_m1519_ParameterInfos[] = 
{
	{"menuName", 0, 134218767, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"order", 1, 134218768, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1519_MI = 
{
	".ctor", (methodPointerType)&m1519, &t298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t298_m1519_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1023, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t298_MIs[] =
{
	&m1317_MI,
	&m1519_MI,
	NULL
};
static MethodInfo* t298_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t298_IOs[] = 
{
	{ &t604_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t298_0_0_0;
extern Il2CppType t298_1_0_0;
struct t298;
TypeInfo t298_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AddComponentMenu", "UnityEngine", t298_MIs, NULL, t298_FIs, NULL, &t490_TI, NULL, NULL, &t298_TI, NULL, t298_VT, &EmptyCustomAttributesCache, &t298_TI, &t298_0_0_0, &t298_1_0_0, t298_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t298), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 2, 0, 0, 4, 0, 1};
#ifndef _MSC_VER
#else
#endif
#include "t360MD.h"



extern MethodInfo m1604_MI;
 void m1604 (t360 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.ExecuteInEditMode
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1604_MI = 
{
	".ctor", (methodPointerType)&m1604, &t360_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1024, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t360_MIs[] =
{
	&m1604_MI,
	NULL
};
static MethodInfo* t360_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t360_IOs[] = 
{
	{ &t604_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t360_1_0_0;
struct t360;
TypeInfo t360_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ExecuteInEditMode", "UnityEngine", t360_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t360_TI, NULL, t360_VT, &EmptyCustomAttributesCache, &t360_TI, &t360_0_0_0, &t360_1_0_0, t360_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t360), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t538.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t538_TI;
#include "t538MD.h"

#include "t630.h"
#include "t631.h"
#include "t632.h"
#include "t633.h"
extern TypeInfo t630_TI;
extern TypeInfo t631_TI;
extern TypeInfo t634_TI;
extern TypeInfo t632_TI;
extern TypeInfo t633_TI;
extern TypeInfo t446_TI;
extern MethodInfo m1430_MI;
extern MethodInfo m2918_MI;


extern MethodInfo m2736_MI;
 void m2736 (t538 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m2737_MI;
 t29 * m2737 (t29 * __this, t29 * p0, t7* p1, t29 * p2, MethodInfo* method){
	t316* V_0 = {0};
	{
		V_0 = (t316*)NULL;
		if (!p2)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		ArrayElementTypeCheck (V_0, p2);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, 0)) = (t29 *)p2;
	}

IL_0013:
	{
		t42 * L_0 = m1430(p0, &m1430_MI);
		t29 * L_1 = (t29 *)VirtFuncInvoker8< t29 *, t7*, int32_t, t631 *, t29 *, t316*, t634*, t633 *, t446* >::Invoke(&m2918_MI, L_0, p1, ((int32_t)308), (t631 *)NULL, p0, V_0, (t634*)(t634*)NULL, (t633 *)NULL, (t446*)(t446*)NULL);
		return L_1;
	}
}
extern MethodInfo m2738_MI;
 t29 * m2738 (t29 * __this, t42 * p0, t7* p1, t29 * p2, MethodInfo* method){
	t316* V_0 = {0};
	{
		V_0 = (t316*)NULL;
		if (!p2)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		ArrayElementTypeCheck (V_0, p2);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, 0)) = (t29 *)p2;
	}

IL_0013:
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker8< t29 *, t7*, int32_t, t631 *, t29 *, t316*, t634*, t633 *, t446* >::Invoke(&m2918_MI, p0, p1, ((int32_t)312), (t631 *)NULL, NULL, V_0, (t634*)(t634*)NULL, (t633 *)NULL, (t446*)(t446*)NULL);
		return L_0;
	}
}
// Metadata Definition UnityEngine.SetupCoroutine
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2736_MI = 
{
	".ctor", (methodPointerType)&m2736, &t538_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1025, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t538_m2737_ParameterInfos[] = 
{
	{"behaviour", 0, 134218769, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"name", 1, 134218770, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"variable", 2, 134218771, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2737_MI = 
{
	"InvokeMember", (methodPointerType)&m2737, &t538_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t538_m2737_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1026, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t538_m2738_ParameterInfos[] = 
{
	{"klass", 0, 134218772, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"name", 1, 134218773, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"variable", 2, 134218774, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2738_MI = 
{
	"InvokeStatic", (methodPointerType)&m2738, &t538_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t538_m2738_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1027, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t538_MIs[] =
{
	&m2736_MI,
	&m2737_MI,
	&m2738_MI,
	NULL
};
static MethodInfo* t538_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t538_0_0_0;
extern Il2CppType t538_1_0_0;
struct t538;
TypeInfo t538_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SetupCoroutine", "UnityEngine", t538_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t538_TI, NULL, t538_VT, &EmptyCustomAttributesCache, &t538_TI, &t538_0_0_0, &t538_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t538), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m2739 (t539 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.WritableAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2739_MI = 
{
	".ctor", (methodPointerType)&m2739, &t539_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1028, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t539_MIs[] =
{
	&m2739_MI,
	NULL
};
static MethodInfo* t539_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t539_IOs[] = 
{
	{ &t604_TI, 4},
};
void t539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 2048, &m2915_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t539__CustomAttributeCache = {
1,
NULL,
&t539_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t539_0_0_0;
extern Il2CppType t539_1_0_0;
struct t539;
extern CustomAttributesCache t539__CustomAttributeCache;
TypeInfo t539_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "WritableAttribute", "UnityEngine", t539_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t539_TI, NULL, t539_VT, &t539__CustomAttributeCache, &t539_TI, &t539_0_0_0, &t539_1_0_0, t539_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t539), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t540.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t540_TI;
#include "t540MD.h"



extern MethodInfo m2740_MI;
 void m2740 (t540 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2740_MI = 
{
	".ctor", (methodPointerType)&m2740, &t540_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1029, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t540_MIs[] =
{
	&m2740_MI,
	NULL
};
static MethodInfo* t540_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t540_IOs[] = 
{
	{ &t604_TI, 4},
};
void t540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t540__CustomAttributeCache = {
1,
NULL,
&t540_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t540_0_0_0;
extern Il2CppType t540_1_0_0;
struct t540;
extern CustomAttributesCache t540__CustomAttributeCache;
TypeInfo t540_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AssemblyIsEditorAssembly", "UnityEngine", t540_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t540_TI, NULL, t540_VT, &t540__CustomAttributeCache, &t540_TI, &t540_0_0_0, &t540_1_0_0, t540_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t540), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t503.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t503_TI;
#include "t503MD.h"



// Metadata Definition UnityEngine.RenderBuffer
extern Il2CppType t44_0_0_3;
FieldInfo t503_f0_FieldInfo = 
{
	"m_RenderTextureInstanceID", &t44_0_0_3, &t503_TI, offsetof(t503, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_3;
FieldInfo t503_f1_FieldInfo = 
{
	"m_BufferPtr", &t35_0_0_3, &t503_TI, offsetof(t503, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t503_FIs[] =
{
	&t503_f0_FieldInfo,
	&t503_f1_FieldInfo,
	NULL
};
static MethodInfo* t503_MIs[] =
{
	NULL
};
static MethodInfo* t503_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t503_0_0_0;
extern Il2CppType t503_1_0_0;
TypeInfo t503_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RenderBuffer", "UnityEngine", t503_MIs, NULL, t503_FIs, NULL, &t110_TI, NULL, NULL, &t503_TI, NULL, t503_VT, &EmptyCustomAttributesCache, &t503_TI, &t503_0_0_0, &t503_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t503)+ sizeof (Il2CppObject), 0, sizeof(t503 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 2, 0, 0, 4, 0, 0};
#include "t498.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t498_TI;
#include "t498MD.h"



// Metadata Definition UnityEngine.CameraClearFlags
extern Il2CppType t44_0_0_1542;
FieldInfo t498_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t498_TI, offsetof(t498, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t498_0_0_32854;
FieldInfo t498_f2_FieldInfo = 
{
	"Skybox", &t498_0_0_32854, &t498_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t498_0_0_32854;
FieldInfo t498_f3_FieldInfo = 
{
	"Color", &t498_0_0_32854, &t498_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t498_0_0_32854;
FieldInfo t498_f4_FieldInfo = 
{
	"SolidColor", &t498_0_0_32854, &t498_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t498_0_0_32854;
FieldInfo t498_f5_FieldInfo = 
{
	"Depth", &t498_0_0_32854, &t498_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t498_0_0_32854;
FieldInfo t498_f6_FieldInfo = 
{
	"Nothing", &t498_0_0_32854, &t498_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t498_FIs[] =
{
	&t498_f1_FieldInfo,
	&t498_f2_FieldInfo,
	&t498_f3_FieldInfo,
	&t498_f4_FieldInfo,
	&t498_f5_FieldInfo,
	&t498_f6_FieldInfo,
	NULL
};
static const int32_t t498_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t498_f2_DefaultValue = 
{
	&t498_f2_FieldInfo, { (char*)&t498_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t498_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t498_f3_DefaultValue = 
{
	&t498_f3_FieldInfo, { (char*)&t498_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t498_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t498_f4_DefaultValue = 
{
	&t498_f4_FieldInfo, { (char*)&t498_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t498_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t498_f5_DefaultValue = 
{
	&t498_f5_FieldInfo, { (char*)&t498_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t498_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t498_f6_DefaultValue = 
{
	&t498_f6_FieldInfo, { (char*)&t498_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t498_FDVs[] = 
{
	&t498_f2_DefaultValue,
	&t498_f3_DefaultValue,
	&t498_f4_DefaultValue,
	&t498_f5_DefaultValue,
	&t498_f6_DefaultValue,
	NULL
};
static MethodInfo* t498_MIs[] =
{
	NULL
};
static MethodInfo* t498_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t498_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t498_0_0_0;
extern Il2CppType t498_1_0_0;
TypeInfo t498_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CameraClearFlags", "UnityEngine", t498_MIs, NULL, t498_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t498_VT, &EmptyCustomAttributesCache, &t44_TI, &t498_0_0_0, &t498_1_0_0, t498_IOs, NULL, NULL, t498_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t498)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#include "t541.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t541_TI;
#include "t541MD.h"



// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern Il2CppType t454_0_0_6;
FieldInfo t541_f0_FieldInfo = 
{
	"probe", &t454_0_0_6, &t541_TI, offsetof(t541, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_6;
FieldInfo t541_f1_FieldInfo = 
{
	"weight", &t22_0_0_6, &t541_TI, offsetof(t541, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t541_FIs[] =
{
	&t541_f0_FieldInfo,
	&t541_f1_FieldInfo,
	NULL
};
static MethodInfo* t541_MIs[] =
{
	NULL
};
static MethodInfo* t541_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t541_0_0_0;
extern Il2CppType t541_1_0_0;
TypeInfo t541_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ReflectionProbeBlendInfo", "UnityEngine.Rendering", t541_MIs, NULL, t541_FIs, NULL, &t110_TI, NULL, NULL, &t541_TI, NULL, t541_VT, &EmptyCustomAttributesCache, &t541_TI, &t541_0_0_0, &t541_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t541)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 2, 0, 0, 4, 0, 0};
#include "t542.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t542_TI;
#include "t542MD.h"



extern MethodInfo m2741_MI;
 void m2741 (t542 * __this, t7* p0, MethodInfo* method){
	{
		t16 * L_0 = (__this->f0);
		m2555(L_0, p0, NULL, 1, &m2555_MI);
		return;
	}
}
extern MethodInfo m2742_MI;
 bool m2742 (t29 * __this, t542  p0, t542  p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t16 * L_0 = ((&p0)->f0);
		t16 * L_1 = ((&p1)->f0);
		bool L_2 = m1297(NULL, L_0, L_1, &m1297_MI);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		t24 * L_3 = ((&p0)->f1);
		t24 * L_4 = ((&p1)->f1);
		bool L_5 = m1297(NULL, L_3, L_4, &m1297_MI);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
extern MethodInfo m2743_MI;
 bool m2743 (t29 * __this, t542  p0, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t16 * L_0 = ((&p0)->f0);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		t24 * L_2 = ((&p0)->f1);
		bool L_3 = m1300(NULL, L_2, (t41 *)NULL, &m1300_MI);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern Il2CppType t16_0_0_6;
FieldInfo t542_f0_FieldInfo = 
{
	"target", &t16_0_0_6, &t542_TI, offsetof(t542, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t24_0_0_6;
FieldInfo t542_f1_FieldInfo = 
{
	"camera", &t24_0_0_6, &t542_TI, offsetof(t542, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t542_FIs[] =
{
	&t542_f0_FieldInfo,
	&t542_f1_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t542_m2741_ParameterInfos[] = 
{
	{"name", 0, 134218779, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2741_MI = 
{
	"SendMessage", (methodPointerType)&m2741, &t542_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t542_m2741_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1033, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t542_0_0_0;
extern Il2CppType t542_0_0_0;
extern Il2CppType t542_0_0_0;
static ParameterInfo t542_m2742_ParameterInfos[] = 
{
	{"lhs", 0, 134218780, &EmptyCustomAttributesCache, &t542_0_0_0},
	{"rhs", 1, 134218781, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t542_t542 (MethodInfo* method, void* obj, void** args);
MethodInfo m2742_MI = 
{
	"Compare", (methodPointerType)&m2742, &t542_TI, &t40_0_0_0, RuntimeInvoker_t40_t542_t542, t542_m2742_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 1034, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t542_0_0_0;
static ParameterInfo t542_m2743_ParameterInfos[] = 
{
	{"exists", 0, 134218782, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t542 (MethodInfo* method, void* obj, void** args);
MethodInfo m2743_MI = 
{
	"op_Implicit", (methodPointerType)&m2743, &t542_TI, &t40_0_0_0, RuntimeInvoker_t40_t542, t542_m2743_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 1, false, false, 1035, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t542_MIs[] =
{
	&m2741_MI,
	&m2742_MI,
	&m2743_MI,
	NULL
};
static MethodInfo* t542_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t542_1_0_0;
extern TypeInfo t543_TI;
TypeInfo t542_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HitInfo", "", t542_MIs, NULL, t542_FIs, NULL, &t110_TI, NULL, &t543_TI, &t542_TI, NULL, t542_VT, &EmptyCustomAttributesCache, &t542_TI, &t542_0_0_0, &t542_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t542)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048843, 0, true, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 2, 0, 0, 4, 0, 0};
#include "t543.h"
#ifndef _MSC_VER
#else
#endif
#include "t543MD.h"

#include "t456.h"
#include "t455.h"
#include "t453.h"
extern TypeInfo t544_TI;
extern TypeInfo t26_TI;
extern TypeInfo t497_TI;
extern TypeInfo t24_TI;
#include "t26MD.h"
#include "t456MD.h"
extern MethodInfo m37_MI;
extern MethodInfo m2492_MI;
extern MethodInfo m2493_MI;
extern MethodInfo m2487_MI;
extern MethodInfo m2486_MI;
extern MethodInfo m2343_MI;
extern MethodInfo m2919_MI;
extern MethodInfo m2113_MI;
extern MethodInfo m2484_MI;
extern MethodInfo m1442_MI;
extern MethodInfo m1465_MI;
extern MethodInfo m1466_MI;
extern MethodInfo m1401_MI;
extern MethodInfo m1480_MI;
extern MethodInfo m2497_MI;
extern MethodInfo m2488_MI;
extern MethodInfo m2499_MI;
extern MethodInfo m2746_MI;
extern MethodInfo m1417_MI;
extern MethodInfo m1460_MI;
struct t28;
struct t28;
 t29 * m68_gshared (t28 * __this, MethodInfo* method);
#define m68(__this, method) (t29 *)m68_gshared((t28 *)__this, method)
#define m2919(__this, method) (t456 *)m68_gshared((t28 *)__this, method)


extern MethodInfo m2744_MI;
 void m2744 (t29 * __this, MethodInfo* method){
	t542  V_0 = {0};
	t542  V_1 = {0};
	t542  V_2 = {0};
	t542  V_3 = {0};
	t542  V_4 = {0};
	t542  V_5 = {0};
	t542  V_6 = {0};
	t542  V_7 = {0};
	t542  V_8 = {0};
	{
		t544* L_0 = ((t544*)SZArrayNew(InitializedTypeInfo(&t544_TI), 3));
		Initobj (&t542_TI, (&V_0));
		*((t542 *)(t542 *)SZArrayLdElema(L_0, 0)) = V_0;
		t544* L_1 = L_0;
		Initobj (&t542_TI, (&V_1));
		*((t542 *)(t542 *)SZArrayLdElema(L_1, 1)) = V_1;
		t544* L_2 = L_1;
		Initobj (&t542_TI, (&V_2));
		*((t542 *)(t542 *)SZArrayLdElema(L_2, 2)) = V_2;
		((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f3 = L_2;
		t544* L_3 = ((t544*)SZArrayNew(InitializedTypeInfo(&t544_TI), 3));
		Initobj (&t542_TI, (&V_3));
		*((t542 *)(t542 *)SZArrayLdElema(L_3, 0)) = V_3;
		t544* L_4 = L_3;
		Initobj (&t542_TI, (&V_4));
		*((t542 *)(t542 *)SZArrayLdElema(L_4, 1)) = V_4;
		t544* L_5 = L_4;
		Initobj (&t542_TI, (&V_5));
		*((t542 *)(t542 *)SZArrayLdElema(L_5, 2)) = V_5;
		((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4 = L_5;
		t544* L_6 = ((t544*)SZArrayNew(InitializedTypeInfo(&t544_TI), 3));
		Initobj (&t542_TI, (&V_6));
		*((t542 *)(t542 *)SZArrayLdElema(L_6, 0)) = V_6;
		t544* L_7 = L_6;
		Initobj (&t542_TI, (&V_7));
		*((t542 *)(t542 *)SZArrayLdElema(L_7, 1)) = V_7;
		t544* L_8 = L_7;
		Initobj (&t542_TI, (&V_8));
		*((t542 *)(t542 *)SZArrayLdElema(L_8, 2)) = V_8;
		((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5 = L_8;
		return;
	}
}
extern MethodInfo m2745_MI;
 void m2745 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t23  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t24 * V_3 = {0};
	t497* V_4 = {0};
	int32_t V_5 = 0;
	t164  V_6 = {0};
	t456 * V_7 = {0};
	t455 * V_8 = {0};
	t329  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	t16 * V_12 = {0};
	t16 * V_13 = {0};
	int32_t V_14 = 0;
	t542  V_15 = {0};
	t23  V_16 = {0};
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t26_TI));
		t23  L_0 = m37(NULL, &m37_MI);
		V_0 = L_0;
		int32_t L_1 = m2492(NULL, &m2492_MI);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		if (!(((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f6))
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		if ((((int32_t)(((int32_t)(((t20 *)(((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f6))->max_length)))) == ((int32_t)V_1)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f6 = ((t497*)SZArrayNew(InitializedTypeInfo(&t497_TI), V_1));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		m2493(NULL, (((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f6), &m2493_MI);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		Initobj (&t542_TI, (&V_15));
		*((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), V_2)) = V_15;
		V_2 = ((int32_t)(V_2+1));
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)(((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5))->max_length))))))
		{
			goto IL_0040;
		}
	}
	{
		if (p0)
		{
			goto IL_02bf;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		V_4 = (((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f6);
		V_5 = 0;
		goto IL_02b4;
	}

IL_0080:
	{
		int32_t L_2 = V_5;
		V_3 = (*(t24 **)(t24 **)SZArrayLdElema(V_4, L_2));
		bool L_3 = m1297(NULL, V_3, (t41 *)NULL, &m1297_MI);
		if (L_3)
		{
			goto IL_00a9;
		}
	}
	{
		if (!p1)
		{
			goto IL_00ae;
		}
	}
	{
		t453 * L_4 = m2487(V_3, &m2487_MI);
		bool L_5 = m1300(NULL, L_4, (t41 *)NULL, &m1300_MI);
		if (!L_5)
		{
			goto IL_00ae;
		}
	}

IL_00a9:
	{
		goto IL_02ae;
	}

IL_00ae:
	{
		t164  L_6 = m2486(V_3, &m2486_MI);
		V_6 = L_6;
		bool L_7 = m2343((&V_6), V_0, &m2343_MI);
		if (L_7)
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_02ae;
	}

IL_00c8:
	{
		t456 * L_8 = m2919(V_3, &m2919_MI);
		V_7 = L_8;
		bool L_9 = m1293(NULL, V_7, &m1293_MI);
		if (!L_9)
		{
			goto IL_0141;
		}
	}
	{
		t455 * L_10 = m2113(V_7, V_0, &m2113_MI);
		V_8 = L_10;
		bool L_11 = m1293(NULL, V_8, &m1293_MI);
		if (!L_11)
		{
			goto IL_011f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		t16 * L_12 = m1392(V_8, &m1392_MI);
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 0))->f0 = L_12;
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 0))->f1 = V_3;
		goto IL_0141;
	}

IL_011f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 0))->f0 = (t16 *)NULL;
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 0))->f1 = (t24 *)NULL;
	}

IL_0141:
	{
		int32_t L_13 = m2484(V_3, &m2484_MI);
		if (L_13)
		{
			goto IL_0151;
		}
	}
	{
		goto IL_02ae;
	}

IL_0151:
	{
		t329  L_14 = m1464(V_3, V_0, &m1464_MI);
		V_9 = L_14;
		t23  L_15 = m1468((&V_9), &m1468_MI);
		V_16 = L_15;
		float L_16 = ((&V_16)->f3);
		V_10 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		bool L_17 = m1442(NULL, (0.0f), V_10, &m1442_MI);
		if (!L_17)
		{
			goto IL_0187;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_019c;
	}

IL_0187:
	{
		float L_18 = m1465(V_3, &m1465_MI);
		float L_19 = m1466(V_3, &m1466_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_20 = fabsf(((float)((float)((float)(L_18-L_19))/(float)V_10)));
		G_B23_0 = L_20;
	}

IL_019c:
	{
		V_11 = G_B23_0;
		int32_t L_21 = m1480(V_3, &m1480_MI);
		int32_t L_22 = m2484(V_3, &m2484_MI);
		t16 * L_23 = m2497(V_3, V_9, V_11, ((int32_t)((int32_t)L_21&(int32_t)L_22)), &m2497_MI);
		V_12 = L_23;
		bool L_24 = m1300(NULL, V_12, (t41 *)NULL, &m1300_MI);
		if (!L_24)
		{
			goto IL_01ec;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 1))->f0 = V_12;
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 1))->f1 = V_3;
		goto IL_0226;
	}

IL_01ec:
	{
		int32_t L_25 = m2488(V_3, &m2488_MI);
		if ((((int32_t)L_25) == ((int32_t)1)))
		{
			goto IL_0204;
		}
	}
	{
		int32_t L_26 = m2488(V_3, &m2488_MI);
		if ((((uint32_t)L_26) != ((uint32_t)2)))
		{
			goto IL_0226;
		}
	}

IL_0204:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 1))->f0 = (t16 *)NULL;
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 1))->f1 = (t24 *)NULL;
	}

IL_0226:
	{
		int32_t L_27 = m1480(V_3, &m1480_MI);
		int32_t L_28 = m2484(V_3, &m2484_MI);
		t16 * L_29 = m2499(V_3, V_9, V_11, ((int32_t)((int32_t)L_27&(int32_t)L_28)), &m2499_MI);
		V_13 = L_29;
		bool L_30 = m1300(NULL, V_13, (t41 *)NULL, &m1300_MI);
		if (!L_30)
		{
			goto IL_0274;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 2))->f0 = V_13;
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 2))->f1 = V_3;
		goto IL_02ae;
	}

IL_0274:
	{
		int32_t L_31 = m2488(V_3, &m2488_MI);
		if ((((int32_t)L_31) == ((int32_t)1)))
		{
			goto IL_028c;
		}
	}
	{
		int32_t L_32 = m2488(V_3, &m2488_MI);
		if ((((uint32_t)L_32) != ((uint32_t)2)))
		{
			goto IL_02ae;
		}
	}

IL_028c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 2))->f0 = (t16 *)NULL;
		((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), 2))->f1 = (t24 *)NULL;
	}

IL_02ae:
	{
		V_5 = ((int32_t)(V_5+1));
	}

IL_02b4:
	{
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((t20 *)V_4)->max_length))))))
		{
			goto IL_0080;
		}
	}

IL_02bf:
	{
		V_14 = 0;
		goto IL_02e5;
	}

IL_02c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		m2746(NULL, V_14, (*(t542 *)((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5), V_14))), &m2746_MI);
		V_14 = ((int32_t)(V_14+1));
	}

IL_02e5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		if ((((int32_t)V_14) < ((int32_t)(((int32_t)(((t20 *)(((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f5))->max_length))))))
		{
			goto IL_02c7;
		}
	}
	{
		return;
	}
}
 void m2746 (t29 * __this, int32_t p0, t542  p1, MethodInfo* method){
	bool V_0 = false;
	bool V_1 = false;
	t542  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t26_TI));
		bool L_0 = m1417(NULL, 0, &m1417_MI);
		V_0 = L_0;
		bool L_1 = m1460(NULL, 0, &m1460_MI);
		V_1 = L_1;
		if (!V_0)
		{
			goto IL_004a;
		}
	}
	{
		bool L_2 = m2743(NULL, p1, &m2743_MI);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		*((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0)) = p1;
		m2741(((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0)), (t7*) &_stringLiteral152, &m2741_MI);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		if (V_1)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		bool L_3 = m2743(NULL, (*(t542 *)((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0))), &m2743_MI);
		if (!L_3)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		bool L_4 = m2742(NULL, p1, (*(t542 *)((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0))), &m2742_MI);
		if (!L_4)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		m2741(((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0)), (t7*) &_stringLiteral153, &m2741_MI);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		m2741(((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0)), (t7*) &_stringLiteral154, &m2741_MI);
		Initobj (&t542_TI, (&V_2));
		*((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0)) = V_2;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		bool L_5 = m2743(NULL, (*(t542 *)((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0))), &m2743_MI);
		if (!L_5)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		m2741(((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f4), p0)), (t7*) &_stringLiteral155, &m2741_MI);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		bool L_6 = m2742(NULL, p1, (*(t542 *)((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f3), p0))), &m2742_MI);
		if (!L_6)
		{
			goto IL_0133;
		}
	}
	{
		bool L_7 = m2743(NULL, p1, &m2743_MI);
		if (!L_7)
		{
			goto IL_012e;
		}
	}
	{
		m2741((&p1), (t7*) &_stringLiteral156, &m2741_MI);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		bool L_8 = m2743(NULL, (*(t542 *)((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f3), p0))), &m2743_MI);
		if (!L_8)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		m2741(((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f3), p0)), (t7*) &_stringLiteral157, &m2741_MI);
	}

IL_0162:
	{
		bool L_9 = m2743(NULL, p1, &m2743_MI);
		if (!L_9)
		{
			goto IL_0185;
		}
	}
	{
		m2741((&p1), (t7*) &_stringLiteral158, &m2741_MI);
		m2741((&p1), (t7*) &_stringLiteral156, &m2741_MI);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t543_TI));
		*((t542 *)(t542 *)SZArrayLdElema((((t543_SFs*)InitializedTypeInfo(&t543_TI)->static_fields)->f3), p0)) = p1;
		return;
	}
}
// Metadata Definition UnityEngine.SendMouseEvents
extern Il2CppType t44_0_0_32849;
FieldInfo t543_f0_FieldInfo = 
{
	"m_HitIndexGUI", &t44_0_0_32849, &t543_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t543_f1_FieldInfo = 
{
	"m_HitIndexPhysics3D", &t44_0_0_32849, &t543_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t543_f2_FieldInfo = 
{
	"m_HitIndexPhysics2D", &t44_0_0_32849, &t543_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t544_0_0_49;
FieldInfo t543_f3_FieldInfo = 
{
	"m_LastHit", &t544_0_0_49, &t543_TI, offsetof(t543_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t544_0_0_49;
FieldInfo t543_f4_FieldInfo = 
{
	"m_MouseDownHit", &t544_0_0_49, &t543_TI, offsetof(t543_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t544_0_0_49;
FieldInfo t543_f5_FieldInfo = 
{
	"m_CurrentHit", &t544_0_0_49, &t543_TI, offsetof(t543_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t497_0_0_17;
FieldInfo t543_f6_FieldInfo = 
{
	"m_Cameras", &t497_0_0_17, &t543_TI, offsetof(t543_SFs, f6), &EmptyCustomAttributesCache};
static FieldInfo* t543_FIs[] =
{
	&t543_f0_FieldInfo,
	&t543_f1_FieldInfo,
	&t543_f2_FieldInfo,
	&t543_f3_FieldInfo,
	&t543_f4_FieldInfo,
	&t543_f5_FieldInfo,
	&t543_f6_FieldInfo,
	NULL
};
static const int32_t t543_f0_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t543_f0_DefaultValue = 
{
	&t543_f0_FieldInfo, { (char*)&t543_f0_DefaultValueData, &t44_0_0_0 }};
static const int32_t t543_f1_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t543_f1_DefaultValue = 
{
	&t543_f1_FieldInfo, { (char*)&t543_f1_DefaultValueData, &t44_0_0_0 }};
static const int32_t t543_f2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t543_f2_DefaultValue = 
{
	&t543_f2_FieldInfo, { (char*)&t543_f2_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t543_FDVs[] = 
{
	&t543_f0_DefaultValue,
	&t543_f1_DefaultValue,
	&t543_f2_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2744_MI = 
{
	".cctor", (methodPointerType)&m2744, &t543_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1030, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t543_m2745_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134218775, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"skipRTCameras", 1, 134218776, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2745_MI = 
{
	"DoSendMouseEvents", (methodPointerType)&m2745, &t543_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t543_m2745_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 1031, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t542_0_0_0;
static ParameterInfo t543_m2746_ParameterInfos[] = 
{
	{"i", 0, 134218777, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hit", 1, 134218778, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t542 (MethodInfo* method, void* obj, void** args);
MethodInfo m2746_MI = 
{
	"SendEvents", (methodPointerType)&m2746, &t543_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t542, t543_m2746_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 1032, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t543_MIs[] =
{
	&m2744_MI,
	&m2745_MI,
	&m2746_MI,
	NULL
};
extern TypeInfo t542_TI;
static TypeInfo* t543_TI__nestedTypes[2] =
{
	&t542_TI,
	NULL
};
static MethodInfo* t543_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t543_0_0_0;
extern Il2CppType t543_1_0_0;
struct t543;
TypeInfo t543_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SendMouseEvents", "UnityEngine", t543_MIs, NULL, t543_FIs, NULL, &t29_TI, t543_TI__nestedTypes, NULL, &t543_TI, NULL, t543_VT, &EmptyCustomAttributesCache, &t543_TI, &t543_0_0_0, &t543_1_0_0, NULL, NULL, NULL, t543_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t543), 0, -1, sizeof(t543_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 3, 0, 7, 0, 1, 4, 0, 0};
#include "t545.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t545_TI;
#include "t545MD.h"



extern MethodInfo m2747_MI;
 void m2747 (t545 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.PropertyAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2747_MI = 
{
	".ctor", (methodPointerType)&m2747, &t545_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1036, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t545_MIs[] =
{
	&m2747_MI,
	NULL
};
static MethodInfo* t545_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t545_IOs[] = 
{
	{ &t604_TI, 4},
};
void t545_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, true, &m2916_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t545__CustomAttributeCache = {
1,
NULL,
&t545_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t545_0_0_0;
extern Il2CppType t545_1_0_0;
struct t545;
extern CustomAttributesCache t545__CustomAttributeCache;
TypeInfo t545_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PropertyAttribute", "UnityEngine", t545_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t545_TI, NULL, t545_VT, &t545__CustomAttributeCache, &t545_TI, &t545_0_0_0, &t545_1_0_0, t545_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t545), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t399.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t399_TI;
#include "t399MD.h"



extern MethodInfo m1897_MI;
 void m1897 (t399 * __this, t7* p0, MethodInfo* method){
	{
		m2747(__this, &m2747_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.TooltipAttribute
extern Il2CppType t7_0_0_38;
FieldInfo t399_f0_FieldInfo = 
{
	"tooltip", &t7_0_0_38, &t399_TI, offsetof(t399, f0), &EmptyCustomAttributesCache};
static FieldInfo* t399_FIs[] =
{
	&t399_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t399_m1897_ParameterInfos[] = 
{
	{"tooltip", 0, 134218783, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1897_MI = 
{
	".ctor", (methodPointerType)&m1897, &t399_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t399_m1897_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1037, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t399_MIs[] =
{
	&m1897_MI,
	NULL
};
static MethodInfo* t399_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t399_IOs[] = 
{
	{ &t604_TI, 4},
};
void t399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, true, &m2916_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t399__CustomAttributeCache = {
1,
NULL,
&t399_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t399_0_0_0;
extern Il2CppType t399_1_0_0;
struct t399;
extern CustomAttributesCache t399__CustomAttributeCache;
TypeInfo t399_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TooltipAttribute", "UnityEngine", t399_MIs, NULL, t399_FIs, NULL, &t545_TI, NULL, NULL, &t399_TI, NULL, t399_VT, &t399__CustomAttributeCache, &t399_TI, &t399_0_0_0, &t399_1_0_0, t399_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t399), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t394.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t394_TI;
#include "t394MD.h"



extern MethodInfo m1837_MI;
 void m1837 (t394 * __this, float p0, MethodInfo* method){
	{
		m2747(__this, &m2747_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.SpaceAttribute
extern Il2CppType t22_0_0_38;
FieldInfo t394_f0_FieldInfo = 
{
	"height", &t22_0_0_38, &t394_TI, offsetof(t394, f0), &EmptyCustomAttributesCache};
static FieldInfo* t394_FIs[] =
{
	&t394_f0_FieldInfo,
	NULL
};
extern Il2CppType t22_0_0_0;
static ParameterInfo t394_m1837_ParameterInfos[] = 
{
	{"height", 0, 134218784, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1837_MI = 
{
	".ctor", (methodPointerType)&m1837, &t394_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t394_m1837_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1038, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t394_MIs[] =
{
	&m1837_MI,
	NULL
};
static MethodInfo* t394_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t394_IOs[] = 
{
	{ &t604_TI, 4},
};
void t394_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, true, &m2916_MI);
		m2917(tmp, true, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t394__CustomAttributeCache = {
1,
NULL,
&t394_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t394_0_0_0;
extern Il2CppType t394_1_0_0;
struct t394;
extern CustomAttributesCache t394__CustomAttributeCache;
TypeInfo t394_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SpaceAttribute", "UnityEngine", t394_MIs, NULL, t394_FIs, NULL, &t545_TI, NULL, NULL, &t394_TI, NULL, t394_VT, &t394__CustomAttributeCache, &t394_TI, &t394_0_0_0, &t394_1_0_0, t394_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t394), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t349.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t349_TI;
#include "t349MD.h"



extern MethodInfo m1539_MI;
 void m1539 (t349 * __this, float p0, float p1, MethodInfo* method){
	{
		m2747(__this, &m2747_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
// Metadata Definition UnityEngine.RangeAttribute
extern Il2CppType t22_0_0_38;
FieldInfo t349_f0_FieldInfo = 
{
	"min", &t22_0_0_38, &t349_TI, offsetof(t349, f0), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_38;
FieldInfo t349_f1_FieldInfo = 
{
	"max", &t22_0_0_38, &t349_TI, offsetof(t349, f1), &EmptyCustomAttributesCache};
static FieldInfo* t349_FIs[] =
{
	&t349_f0_FieldInfo,
	&t349_f1_FieldInfo,
	NULL
};
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t349_m1539_ParameterInfos[] = 
{
	{"min", 0, 134218785, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"max", 1, 134218786, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1539_MI = 
{
	".ctor", (methodPointerType)&m1539, &t349_TI, &t21_0_0_0, RuntimeInvoker_t21_t22_t22, t349_m1539_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1039, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t349_MIs[] =
{
	&m1539_MI,
	NULL
};
static MethodInfo* t349_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t349_IOs[] = 
{
	{ &t604_TI, 4},
};
void t349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, true, &m2916_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t349__CustomAttributeCache = {
1,
NULL,
&t349_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t349_0_0_0;
extern Il2CppType t349_1_0_0;
struct t349;
extern CustomAttributesCache t349__CustomAttributeCache;
TypeInfo t349_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RangeAttribute", "UnityEngine", t349_MIs, NULL, t349_FIs, NULL, &t545_TI, NULL, NULL, &t349_TI, NULL, t349_VT, &t349__CustomAttributeCache, &t349_TI, &t349_0_0_0, &t349_1_0_0, t349_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t349), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 2, 0, 0, 4, 0, 1};
#include "t405.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t405_TI;
#include "t405MD.h"



extern MethodInfo m1927_MI;
 void m1927 (t405 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		m2747(__this, &m2747_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
// Metadata Definition UnityEngine.TextAreaAttribute
extern Il2CppType t44_0_0_38;
FieldInfo t405_f0_FieldInfo = 
{
	"minLines", &t44_0_0_38, &t405_TI, offsetof(t405, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_38;
FieldInfo t405_f1_FieldInfo = 
{
	"maxLines", &t44_0_0_38, &t405_TI, offsetof(t405, f1), &EmptyCustomAttributesCache};
static FieldInfo* t405_FIs[] =
{
	&t405_f0_FieldInfo,
	&t405_f1_FieldInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t405_m1927_ParameterInfos[] = 
{
	{"minLines", 0, 134218787, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"maxLines", 1, 134218788, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1927_MI = 
{
	".ctor", (methodPointerType)&m1927, &t405_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t405_m1927_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1040, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t405_MIs[] =
{
	&m1927_MI,
	NULL
};
static MethodInfo* t405_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t405_IOs[] = 
{
	{ &t604_TI, 4},
};
void t405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, true, &m2916_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t405__CustomAttributeCache = {
1,
NULL,
&t405_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t405_0_0_0;
extern Il2CppType t405_1_0_0;
struct t405;
extern CustomAttributesCache t405__CustomAttributeCache;
TypeInfo t405_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextAreaAttribute", "UnityEngine", t405_MIs, NULL, t405_FIs, NULL, &t545_TI, NULL, NULL, &t405_TI, NULL, t405_VT, &t405__CustomAttributeCache, &t405_TI, &t405_0_0_0, &t405_1_0_0, t405_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t405), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 2, 0, 0, 4, 0, 1};
#include "t397.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t397_TI;
#include "t397MD.h"



extern MethodInfo m1866_MI;
 void m1866 (t397 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1866_MI = 
{
	".ctor", (methodPointerType)&m1866, &t397_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1041, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t397_MIs[] =
{
	&m1866_MI,
	NULL
};
static MethodInfo* t397_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t397_IOs[] = 
{
	{ &t604_TI, 4},
};
void t397_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		m2916(tmp, true, &m2916_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t397__CustomAttributeCache = {
1,
NULL,
&t397_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t397_0_0_0;
extern Il2CppType t397_1_0_0;
struct t397;
extern CustomAttributesCache t397__CustomAttributeCache;
TypeInfo t397_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SelectionBaseAttribute", "UnityEngine", t397_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t397_TI, NULL, t397_VT, &t397__CustomAttributeCache, &t397_TI, &t397_0_0_0, &t397_1_0_0, t397_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t397), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t546.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t546_TI;
#include "t546MD.h"



extern MethodInfo m2748_MI;
 void m2748 (t546 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition UnityEngine.SliderState
extern Il2CppType t22_0_0_6;
FieldInfo t546_f0_FieldInfo = 
{
	"dragStartPos", &t22_0_0_6, &t546_TI, offsetof(t546, f0), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_6;
FieldInfo t546_f1_FieldInfo = 
{
	"dragStartValue", &t22_0_0_6, &t546_TI, offsetof(t546, f1), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t546_f2_FieldInfo = 
{
	"isDragging", &t40_0_0_6, &t546_TI, offsetof(t546, f2), &EmptyCustomAttributesCache};
static FieldInfo* t546_FIs[] =
{
	&t546_f0_FieldInfo,
	&t546_f1_FieldInfo,
	&t546_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2748_MI = 
{
	".ctor", (methodPointerType)&m2748, &t546_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1042, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t546_MIs[] =
{
	&m2748_MI,
	NULL
};
static MethodInfo* t546_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t546_0_0_0;
extern Il2CppType t546_1_0_0;
struct t546;
TypeInfo t546_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SliderState", "UnityEngine", t546_MIs, NULL, t546_FIs, NULL, &t29_TI, NULL, NULL, &t546_TI, NULL, t546_VT, &EmptyCustomAttributesCache, &t546_TI, &t546_0_0_0, &t546_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t546), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 3, 0, 0, 4, 0, 0};
#include "t547.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t547_TI;
#include "t547MD.h"

#include "t548.h"
#include "t295.h"
#include "t292.h"
#include "t305.h"
#include "t635.h"
#include "t636.h"
#include "t637.h"
extern TypeInfo t548_TI;
extern TypeInfo t305_TI;
extern TypeInfo t295_TI;
extern TypeInfo t292_TI;
extern TypeInfo t200_TI;
extern TypeInfo t194_TI;
extern TypeInfo t635_TI;
extern TypeInfo t636_TI;
extern TypeInfo t638_TI;
extern TypeInfo t637_TI;
#include "t548MD.h"
#include "t305MD.h"
#include "t295MD.h"
#include "t292MD.h"
#include "t635MD.h"
#include "t636MD.h"
#include "t637MD.h"
#include "t44MD.h"
extern MethodInfo m2920_MI;
extern MethodInfo m2757_MI;
extern MethodInfo m2921_MI;
extern MethodInfo m2922_MI;
extern MethodInfo m2755_MI;
extern MethodInfo m1685_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2860_MI;
extern MethodInfo m2923_MI;
extern MethodInfo m2861_MI;
extern MethodInfo m2924_MI;
extern MethodInfo m1684_MI;
extern MethodInfo m2925_MI;
extern MethodInfo m66_MI;
extern MethodInfo m2858_MI;
extern MethodInfo m2926_MI;
extern MethodInfo m2927_MI;
extern MethodInfo m1315_MI;
extern MethodInfo m2928_MI;
extern MethodInfo m1741_MI;
extern MethodInfo m2753_MI;
extern MethodInfo m2929_MI;
extern MethodInfo m1742_MI;
extern MethodInfo m2930_MI;
extern MethodInfo m1766_MI;
extern MethodInfo m2931_MI;
extern MethodInfo m2932_MI;
extern MethodInfo m2933_MI;
extern MethodInfo m2934_MI;
extern MethodInfo m1768_MI;
extern MethodInfo m2935_MI;
extern MethodInfo m2936_MI;
extern MethodInfo m2937_MI;
extern MethodInfo m2938_MI;
extern MethodInfo m2939_MI;
extern MethodInfo m2940_MI;
extern MethodInfo m2941_MI;
extern MethodInfo m2942_MI;
extern MethodInfo m2943_MI;
extern MethodInfo m2944_MI;


extern MethodInfo m2749_MI;
 void m2749 (t547 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m2750_MI;
 void m2750 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		((t547_SFs*)InitializedTypeInfo(&t547_TI)->static_fields)->f0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		return;
	}
}
extern MethodInfo m2751_MI;
 void m2751 (t29 * __this, t7* p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		((t547_SFs*)InitializedTypeInfo(&t547_TI)->static_fields)->f0 = p0;
		return;
	}
}
extern MethodInfo m2752_MI;
 t7* m2752 (t29 * __this, MethodInfo* method){
	t548 * V_0 = {0};
	t7* V_1 = {0};
	{
		t548 * L_0 = (t548 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t548_TI));
		m2920(L_0, 1, 1, &m2920_MI);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		t7* L_1 = m2757(NULL, V_0, &m2757_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2921_MI, L_1);
		V_1 = L_2;
		return V_1;
	}
}
 bool m2753 (t29 * __this, t29 * p0, MethodInfo* method){
	t7* V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		V_0 = ((t7*)Castclass(p0, (&t7_TI)));
		bool L_0 = m2922(V_0, (t7*) &_stringLiteral159, &m2922_MI);
		if (L_0)
		{
			goto IL_0064;
		}
	}
	{
		bool L_1 = m2922(V_0, (t7*) &_stringLiteral160, &m2922_MI);
		if (L_1)
		{
			goto IL_0064;
		}
	}
	{
		bool L_2 = m2922(V_0, (t7*) &_stringLiteral161, &m2922_MI);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		bool L_3 = m2922(V_0, (t7*) &_stringLiteral162, &m2922_MI);
		if (L_3)
		{
			goto IL_0064;
		}
	}
	{
		bool L_4 = m2922(V_0, (t7*) &_stringLiteral163, &m2922_MI);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		bool L_5 = m2922(V_0, (t7*) &_stringLiteral164, &m2922_MI);
		G_B7_0 = ((int32_t)(L_5));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
extern MethodInfo m2754_MI;
 t7* m2754 (t29 * __this, t29 * p0, MethodInfo* method){
	t7* V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		V_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		V_1 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		m2755(NULL, p0, (&V_0), (&V_1), &m2755_MI);
		t7* L_0 = m1685(NULL, V_0, (t7*) &_stringLiteral88, V_1, &m1685_MI);
		return L_0;
	}
}
 void m2755 (t29 * __this, t29 * p0, t7** p1, t7** p2, MethodInfo* method){
	t295 * V_0 = {0};
	t292 * V_1 = {0};
	t7* V_2 = {0};
	t7* V_3 = {0};
	t7* V_4 = {0};
	t548 * V_5 = {0};
	int32_t G_B7_0 = 0;
	{
		if (p0)
		{
			goto IL_0011;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		V_0 = ((t295 *)IsInst(p0, InitializedTypeInfo(&t295_TI)));
		if (V_0)
		{
			goto IL_0029;
		}
	}
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral166, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0029:
	{
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2860_MI, V_0);
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2860_MI, V_0);
		int32_t L_4 = m1715(L_3, &m1715_MI);
		G_B7_0 = ((int32_t)((int32_t)L_4*(int32_t)2));
	}

IL_004b:
	{
		t292 * L_5 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_5, G_B7_0, &m2923_MI);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		*((t29 **)(p1)) = (t29 *)(((t7_SFs*)(&t7_TI)->static_fields)->f2);
		V_2 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		goto IL_00ff;
	}

IL_0063:
	{
		int32_t L_6 = m1715(V_2, &m1715_MI);
		if (L_6)
		{
			goto IL_007a;
		}
	}
	{
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2860_MI, V_0);
		V_2 = L_7;
		goto IL_008c;
	}

IL_007a:
	{
		t7* L_8 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2860_MI, V_0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = m1685(NULL, L_8, (t7*) &_stringLiteral88, V_2, &m1685_MI);
		V_2 = L_9;
	}

IL_008c:
	{
		t42 * L_10 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2861_MI, V_0);
		t7* L_11 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, L_10);
		V_3 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		V_4 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1684_MI, V_0);
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		t7* L_13 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1684_MI, V_0);
		V_4 = L_13;
	}

IL_00b2:
	{
		t7* L_14 = m2925(V_4, &m2925_MI);
		int32_t L_15 = m1715(L_14, &m1715_MI);
		if (!L_15)
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_16 = m66(NULL, V_3, (t7*) &_stringLiteral167, &m66_MI);
		V_3 = L_16;
		t7* L_17 = m66(NULL, V_3, V_4, &m66_MI);
		V_3 = L_17;
	}

IL_00d8:
	{
		*((t29 **)(p1)) = (t29 *)V_3;
		t295 * L_18 = (t295 *)VirtFuncInvoker0< t295 * >::Invoke(&m2858_MI, V_0);
		if (!L_18)
		{
			goto IL_00f8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_19 = m2926(NULL, (t7*) &_stringLiteral168, V_3, (t7*) &_stringLiteral88, V_2, &m2926_MI);
		V_2 = L_19;
	}

IL_00f8:
	{
		t295 * L_20 = (t295 *)VirtFuncInvoker0< t295 * >::Invoke(&m2858_MI, V_0);
		V_0 = L_20;
	}

IL_00ff:
	{
		if (V_0)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_21 = m66(NULL, V_2, (t7*) &_stringLiteral88, &m66_MI);
		m2927(V_1, L_21, &m2927_MI);
		t548 * L_22 = (t548 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t548_TI));
		m2920(L_22, 1, 1, &m2920_MI);
		V_5 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		t7* L_23 = m2757(NULL, V_5, &m2757_MI);
		m2927(V_1, L_23, &m2927_MI);
		t7* L_24 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_1);
		*((t29 **)(p2)) = (t29 *)L_24;
		return;
	}
}
extern MethodInfo m2756_MI;
 t7* m2756 (t29 * __this, t7* p0, bool p1, MethodInfo* method){
	t446* V_0 = {0};
	t292 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t7* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		if (p0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_000c:
	{
		t200* L_0 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0)) = (uint16_t)((int32_t)10);
		t446* L_1 = m2928(p0, L_0, &m2928_MI);
		V_0 = L_1;
		int32_t L_2 = m1715(p0, &m1715_MI);
		t292 * L_3 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_3, L_2, &m2923_MI);
		V_1 = L_3;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		int32_t L_4 = V_2;
		t7* L_5 = m2925((*(t7**)(t7**)SZArrayLdElema(V_0, L_4)), &m2925_MI);
		ArrayElementTypeCheck (V_0, L_5);
		*((t7**)(t7**)SZArrayLdElema(V_0, V_2)) = (t7*)L_5;
		V_2 = ((int32_t)(V_2+1));
	}

IL_0040:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		int32_t L_6 = V_3;
		V_4 = (*(t7**)(t7**)SZArrayLdElema(V_0, L_6));
		int32_t L_7 = m1715(V_4, &m1715_MI);
		if (!L_7)
		{
			goto IL_0070;
		}
	}
	{
		uint16_t L_8 = m1741(V_4, 0, &m1741_MI);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)10))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		bool L_9 = m2922(V_4, (t7*) &_stringLiteral169, &m2922_MI);
		if (!L_9)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		if (!p1)
		{
			goto IL_00a7;
		}
	}
	{
		bool L_10 = m2922(V_4, (t7*) &_stringLiteral170, &m2922_MI);
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		if (!p1)
		{
			goto IL_00fa;
		}
	}
	{
		if ((((int32_t)V_3) >= ((int32_t)((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		bool L_11 = m2753(NULL, V_4, &m2753_MI);
		if (!L_11)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_12 = ((int32_t)(V_3+1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		bool L_13 = m2753(NULL, (*(t7**)(t7**)SZArrayLdElema(V_0, L_12)), &m2753_MI);
		if (!L_13)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		int32_t L_14 = m2929(V_4, (t7*) &_stringLiteral171, &m2929_MI);
		V_5 = L_14;
		if ((((int32_t)V_5) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		t7* L_15 = m1742(V_4, 0, V_5, &m1742_MI);
		V_4 = L_15;
	}

IL_00fa:
	{
		int32_t L_16 = m2929(V_4, (t7*) &_stringLiteral172, &m2929_MI);
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		int32_t L_17 = m2929(V_4, (t7*) &_stringLiteral173, &m2929_MI);
		if ((((int32_t)L_17) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		int32_t L_18 = m2929(V_4, (t7*) &_stringLiteral174, &m2929_MI);
		if ((((int32_t)L_18) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		if (!p1)
		{
			goto IL_016c;
		}
	}
	{
		bool L_19 = m2922(V_4, (t7*) &_stringLiteral175, &m2922_MI);
		if (!L_19)
		{
			goto IL_016c;
		}
	}
	{
		bool L_20 = m2930(V_4, (t7*) &_stringLiteral176, &m2930_MI);
		if (!L_20)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		bool L_21 = m2922(V_4, (t7*) &_stringLiteral177, &m2922_MI);
		if (!L_21)
		{
			goto IL_0188;
		}
	}
	{
		t7* L_22 = m1766(V_4, 0, 3, &m1766_MI);
		V_4 = L_22;
	}

IL_0188:
	{
		int32_t L_23 = m2929(V_4, (t7*) &_stringLiteral178, &m2929_MI);
		V_6 = L_23;
		V_7 = (-1);
		if ((((int32_t)V_6) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		int32_t L_24 = m2931(V_4, (t7*) &_stringLiteral176, V_6, &m2931_MI);
		V_7 = L_24;
	}

IL_01b1:
	{
		if ((((int32_t)V_6) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_6)))
		{
			goto IL_01d4;
		}
	}
	{
		t7* L_25 = m1766(V_4, V_6, ((int32_t)(((int32_t)(V_7-V_6))+1)), &m1766_MI);
		V_4 = L_25;
	}

IL_01d4:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_26 = m2932(V_4, (t7*) &_stringLiteral179, (((t7_SFs*)(&t7_TI)->static_fields)->f2), &m2932_MI);
		V_4 = L_26;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		t7* L_27 = m2932(V_4, (((t547_SFs*)InitializedTypeInfo(&t547_TI)->static_fields)->f0), (((t7_SFs*)(&t7_TI)->static_fields)->f2), &m2932_MI);
		V_4 = L_27;
		t7* L_28 = m2933(V_4, ((int32_t)92), ((int32_t)47), &m2933_MI);
		V_4 = L_28;
		int32_t L_29 = m2934(V_4, (t7*) &_stringLiteral180, &m2934_MI);
		V_8 = L_29;
		if ((((int32_t)V_8) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		t7* L_30 = m1766(V_4, V_8, 5, &m1766_MI);
		V_4 = L_30;
		t7* L_31 = m1768(V_4, V_8, (t7*) &_stringLiteral181, &m1768_MI);
		V_4 = L_31;
		int32_t L_32 = m1715(V_4, &m1715_MI);
		t7* L_33 = m1768(V_4, L_32, (t7*) &_stringLiteral60, &m1768_MI);
		V_4 = L_33;
	}

IL_024e:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_34 = m66(NULL, V_4, (t7*) &_stringLiteral88, &m66_MI);
		m2927(V_1, L_34, &m2927_MI);
	}

IL_0261:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0265:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		t7* L_35 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_1);
		return L_35;
	}
}
 t7* m2757 (t29 * __this, t548 * p0, MethodInfo* method){
	t292 * V_0 = {0};
	int32_t V_1 = 0;
	t635 * V_2 = {0};
	t636 * V_3 = {0};
	t42 * V_4 = {0};
	t7* V_5 = {0};
	int32_t V_6 = 0;
	t638* V_7 = {0};
	bool V_8 = false;
	t7* V_9 = {0};
	int32_t V_10 = 0;
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_0, ((int32_t)255), &m2923_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_01c9;
	}

IL_0012:
	{
		t635 * L_1 = (t635 *)VirtFuncInvoker1< t635 *, int32_t >::Invoke(&m2935_MI, p0, V_1);
		V_2 = L_1;
		t636 * L_2 = (t636 *)VirtFuncInvoker0< t636 * >::Invoke(&m2936_MI, V_2);
		V_3 = L_2;
		if (V_3)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_01c5;
	}

IL_002c:
	{
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, V_3);
		V_4 = L_3;
		if (V_4)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_01c5;
	}

IL_0040:
	{
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2938_MI, V_4);
		V_5 = L_4;
		if (!V_5)
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_5 = m1715(V_5, &m1715_MI);
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		m2927(V_0, V_5, &m2927_MI);
		m2927(V_0, (t7*) &_stringLiteral52, &m2927_MI);
	}

IL_0071:
	{
		t7* L_6 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, V_4);
		m2927(V_0, L_6, &m2927_MI);
		m2927(V_0, (t7*) &_stringLiteral182, &m2927_MI);
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, V_3);
		m2927(V_0, L_7, &m2927_MI);
		m2927(V_0, (t7*) &_stringLiteral183, &m2927_MI);
		V_6 = 0;
		t638* L_8 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_3);
		V_7 = L_8;
		V_8 = 1;
		goto IL_00ee;
	}

IL_00b7:
	{
		if (V_8)
		{
			goto IL_00cf;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral184, &m2927_MI);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = 0;
	}

IL_00d2:
	{
		int32_t L_9 = V_6;
		t42 * L_10 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_7, L_9)));
		t7* L_11 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, L_10);
		m2927(V_0, L_11, &m2927_MI);
		V_6 = ((int32_t)(V_6+1));
	}

IL_00ee:
	{
		if ((((int32_t)V_6) < ((int32_t)(((int32_t)(((t20 *)V_7)->max_length))))))
		{
			goto IL_00b7;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral60, &m2927_MI);
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2941_MI, V_2);
		V_9 = L_12;
		if (!V_9)
		{
			goto IL_01b9;
		}
	}
	{
		t7* L_13 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_14 = m1713(NULL, L_13, (t7*) &_stringLiteral185, &m1713_MI);
		if (!L_14)
		{
			goto IL_0140;
		}
	}
	{
		t7* L_15 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2938_MI, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_16 = m1713(NULL, L_15, (t7*) &_stringLiteral186, &m1713_MI);
		if (L_16)
		{
			goto IL_01b9;
		}
	}

IL_0140:
	{
		m2927(V_0, (t7*) &_stringLiteral181, &m2927_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		bool L_17 = m2922(V_9, (((t547_SFs*)InitializedTypeInfo(&t547_TI)->static_fields)->f0), &m2922_MI);
		if (!L_17)
		{
			goto IL_0182;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t547_TI));
		int32_t L_18 = m1715((((t547_SFs*)InitializedTypeInfo(&t547_TI)->static_fields)->f0), &m1715_MI);
		int32_t L_19 = m1715(V_9, &m1715_MI);
		int32_t L_20 = m1715((((t547_SFs*)InitializedTypeInfo(&t547_TI)->static_fields)->f0), &m1715_MI);
		t7* L_21 = m1742(V_9, L_18, ((int32_t)(L_19-L_20)), &m1742_MI);
		V_9 = L_21;
	}

IL_0182:
	{
		m2927(V_0, V_9, &m2927_MI);
		m2927(V_0, (t7*) &_stringLiteral182, &m2927_MI);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2942_MI, V_2);
		V_10 = L_22;
		t7* L_23 = m2943((&V_10), &m2943_MI);
		m2927(V_0, L_23, &m2927_MI);
		m2927(V_0, (t7*) &_stringLiteral60, &m2927_MI);
	}

IL_01b9:
	{
		m2927(V_0, (t7*) &_stringLiteral88, &m2927_MI);
	}

IL_01c5:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_01c9:
	{
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2944_MI, p0);
		if ((((int32_t)V_1) < ((int32_t)L_24)))
		{
			goto IL_0012;
		}
	}
	{
		t7* L_25 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_25;
	}
}
// Metadata Definition UnityEngine.StackTraceUtility
extern Il2CppType t7_0_0_17;
FieldInfo t547_f0_FieldInfo = 
{
	"projectFolder", &t7_0_0_17, &t547_TI, offsetof(t547_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t547_FIs[] =
{
	&t547_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2749_MI = 
{
	".ctor", (methodPointerType)&m2749, &t547_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1043, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2750_MI = 
{
	".cctor", (methodPointerType)&m2750, &t547_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1044, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t547_m2751_ParameterInfos[] = 
{
	{"folder", 0, 134218789, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2751_MI = 
{
	"SetProjectFolder", (methodPointerType)&m2751, &t547_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t547_m2751_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 1045, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t547__CustomAttributeCache_m2752;
MethodInfo m2752_MI = 
{
	"ExtractStackTrace", (methodPointerType)&m2752, &t547_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &t547__CustomAttributeCache_m2752, 150, 0, 255, 0, false, false, 1046, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t547_m2753_ParameterInfos[] = 
{
	{"name", 0, 134218790, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2753_MI = 
{
	"IsSystemStacktraceType", (methodPointerType)&m2753, &t547_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t547_m2753_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1047, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t547_m2754_ParameterInfos[] = 
{
	{"exception", 0, 134218791, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2754_MI = 
{
	"ExtractStringFromException", (methodPointerType)&m2754, &t547_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t547_m2754_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1048, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t7_1_0_2;
extern Il2CppType t7_1_0_0;
extern Il2CppType t7_1_0_2;
static ParameterInfo t547_m2755_ParameterInfos[] = 
{
	{"exceptiono", 0, 134218792, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"message", 1, 134218793, &EmptyCustomAttributesCache, &t7_1_0_2},
	{"stackTrace", 2, 134218794, &EmptyCustomAttributesCache, &t7_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t639_t639 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t547__CustomAttributeCache_m2755;
MethodInfo m2755_MI = 
{
	"ExtractStringFromExceptionInternal", (methodPointerType)&m2755, &t547_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t639_t639, t547_m2755_ParameterInfos, &t547__CustomAttributeCache_m2755, 147, 0, 255, 3, false, false, 1049, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t547_m2756_ParameterInfos[] = 
{
	{"oldString", 0, 134218795, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"stripEngineInternalInformation", 1, 134218796, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m2756_MI = 
{
	"PostprocessStacktrace", (methodPointerType)&m2756, &t547_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t297, t547_m2756_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 1050, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t548_0_0_0;
extern Il2CppType t548_0_0_0;
static ParameterInfo t547_m2757_ParameterInfos[] = 
{
	{"stackTrace", 0, 134218797, &EmptyCustomAttributesCache, &t548_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t547__CustomAttributeCache_m2757;
MethodInfo m2757_MI = 
{
	"ExtractFormattedStackTrace", (methodPointerType)&m2757, &t547_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t547_m2757_ParameterInfos, &t547__CustomAttributeCache_m2757, 147, 0, 255, 1, false, false, 1051, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t547_MIs[] =
{
	&m2749_MI,
	&m2750_MI,
	&m2751_MI,
	&m2752_MI,
	&m2753_MI,
	&m2754_MI,
	&m2755_MI,
	&m2756_MI,
	&m2757_MI,
	NULL
};
static MethodInfo* t547_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t547_CustomAttributesCacheGenerator_m2752(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t615 * tmp;
		tmp = (t615 *)il2cpp_codegen_object_new (&t615_TI);
		m2895(tmp, &m2895_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t547_CustomAttributesCacheGenerator_m2755(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t615 * tmp;
		tmp = (t615 *)il2cpp_codegen_object_new (&t615_TI);
		m2895(tmp, &m2895_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t547_CustomAttributesCacheGenerator_m2757(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t615 * tmp;
		tmp = (t615 *)il2cpp_codegen_object_new (&t615_TI);
		m2895(tmp, &m2895_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t547__CustomAttributeCache_m2752 = {
1,
NULL,
&t547_CustomAttributesCacheGenerator_m2752
};
CustomAttributesCache t547__CustomAttributeCache_m2755 = {
1,
NULL,
&t547_CustomAttributesCacheGenerator_m2755
};
CustomAttributesCache t547__CustomAttributeCache_m2757 = {
1,
NULL,
&t547_CustomAttributesCacheGenerator_m2757
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t547_0_0_0;
extern Il2CppType t547_1_0_0;
struct t547;
extern CustomAttributesCache t547__CustomAttributeCache_m2752;
extern CustomAttributesCache t547__CustomAttributeCache_m2755;
extern CustomAttributesCache t547__CustomAttributeCache_m2757;
TypeInfo t547_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "StackTraceUtility", "UnityEngine", t547_MIs, NULL, t547_FIs, NULL, &t29_TI, NULL, NULL, &t547_TI, NULL, t547_VT, &EmptyCustomAttributesCache, &t547_TI, &t547_0_0_0, &t547_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t547), 0, -1, sizeof(t547_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, true, false, false, 9, 0, 1, 0, 0, 4, 0, 0};
#include "t367.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t367_TI;
#include "t367MD.h"

extern MethodInfo m2945_MI;
extern MethodInfo m2946_MI;
extern MethodInfo m2947_MI;


extern MethodInfo m2758_MI;
 void m2758 (t367 * __this, MethodInfo* method){
	{
		m2945(__this, (t7*) &_stringLiteral187, &m2945_MI);
		m2946(__this, ((int32_t)-2147467261), &m2946_MI);
		return;
	}
}
extern MethodInfo m2759_MI;
 void m2759 (t367 * __this, t7* p0, MethodInfo* method){
	{
		m2945(__this, p0, &m2945_MI);
		m2946(__this, ((int32_t)-2147467261), &m2946_MI);
		return;
	}
}
extern MethodInfo m2760_MI;
 void m2760 (t367 * __this, t7* p0, t295 * p1, MethodInfo* method){
	{
		m2947(__this, p0, p1, &m2947_MI);
		m2946(__this, ((int32_t)-2147467261), &m2946_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UnityException
extern Il2CppType t44_0_0_32849;
FieldInfo t367_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t367_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t367_f12_FieldInfo = 
{
	"unityStackTrace", &t7_0_0_1, &t367_TI, offsetof(t367, f12), &EmptyCustomAttributesCache};
static FieldInfo* t367_FIs[] =
{
	&t367_f11_FieldInfo,
	&t367_f12_FieldInfo,
	NULL
};
static const int32_t t367_f11_DefaultValueData = -2147467261;
static Il2CppFieldDefaultValueEntry t367_f11_DefaultValue = 
{
	&t367_f11_FieldInfo, { (char*)&t367_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t367_FDVs[] = 
{
	&t367_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2758_MI = 
{
	".ctor", (methodPointerType)&m2758, &t367_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1052, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t367_m2759_ParameterInfos[] = 
{
	{"message", 0, 134218798, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2759_MI = 
{
	".ctor", (methodPointerType)&m2759, &t367_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t367_m2759_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1053, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t295_0_0_0;
extern Il2CppType t295_0_0_0;
static ParameterInfo t367_m2760_ParameterInfos[] = 
{
	{"message", 0, 134218799, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"innerException", 1, 134218800, &EmptyCustomAttributesCache, &t295_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2760_MI = 
{
	".ctor", (methodPointerType)&m2760, &t367_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t367_m2760_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1054, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t367_MIs[] =
{
	&m2758_MI,
	&m2759_MI,
	&m2760_MI,
	NULL
};
extern MethodInfo m2856_MI;
extern MethodInfo m2857_MI;
extern MethodInfo m2859_MI;
static MethodInfo* t367_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
extern TypeInfo t590_TI;
static Il2CppInterfaceOffsetPair t367_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t367_0_0_0;
extern Il2CppType t367_1_0_0;
struct t367;
TypeInfo t367_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityException", "UnityEngine", t367_MIs, NULL, t367_FIs, NULL, &t295_TI, NULL, NULL, &t367_TI, NULL, t367_VT, &EmptyCustomAttributesCache, &t367_TI, &t367_0_0_0, &t367_1_0_0, t367_IOs, NULL, NULL, t367_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t367), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 2, 0, 0, 11, 0, 2};
#include "t549.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t549_TI;
#include "t549MD.h"



extern MethodInfo m2761_MI;
 void m2761 (t549 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2761_MI = 
{
	".ctor", (methodPointerType)&m2761, &t549_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1055, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t549_MIs[] =
{
	&m2761_MI,
	NULL
};
static MethodInfo* t549_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t549_IOs[] = 
{
	{ &t604_TI, 4},
};
void t549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		m2917(tmp, false, &m2917_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t549__CustomAttributeCache = {
1,
NULL,
&t549_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t549_0_0_0;
extern Il2CppType t549_1_0_0;
struct t549;
extern CustomAttributesCache t549__CustomAttributeCache;
TypeInfo t549_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SharedBetweenAnimatorsAttribute", "UnityEngine", t549_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t549_TI, NULL, t549_VT, &t549__CustomAttributeCache, &t549_TI, &t549_0_0_0, &t549_1_0_0, t549_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t549), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t550.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t550_TI;
#include "t550MD.h"

#include "t450MD.h"
extern MethodInfo m2075_MI;


extern MethodInfo m2762_MI;
 void m2762 (t550 * __this, MethodInfo* method){
	{
		m2075(__this, &m2075_MI);
		return;
	}
}
extern MethodInfo m2763_MI;
 void m2763 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2764_MI;
 void m2764 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2765_MI;
 void m2765 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2766_MI;
 void m2766 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2767_MI;
 void m2767 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2768_MI;
 void m2768 (t550 * __this, t229 * p0, int32_t p1, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2769_MI;
 void m2769 (t550 * __this, t229 * p0, int32_t p1, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition UnityEngine.StateMachineBehaviour
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2762_MI = 
{
	".ctor", (methodPointerType)&m2762, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1056, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t515_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2763_ParameterInfos[] = 
{
	{"animator", 0, 134218801, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateInfo", 1, 134218802, &EmptyCustomAttributesCache, &t515_0_0_0},
	{"layerIndex", 2, 134218803, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t515_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2763_MI = 
{
	"OnStateEnter", (methodPointerType)&m2763, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t515_t44, t550_m2763_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 4, 3, false, false, 1057, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t515_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2764_ParameterInfos[] = 
{
	{"animator", 0, 134218804, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateInfo", 1, 134218805, &EmptyCustomAttributesCache, &t515_0_0_0},
	{"layerIndex", 2, 134218806, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t515_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2764_MI = 
{
	"OnStateUpdate", (methodPointerType)&m2764, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t515_t44, t550_m2764_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 5, 3, false, false, 1058, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t515_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2765_ParameterInfos[] = 
{
	{"animator", 0, 134218807, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateInfo", 1, 134218808, &EmptyCustomAttributesCache, &t515_0_0_0},
	{"layerIndex", 2, 134218809, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t515_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2765_MI = 
{
	"OnStateExit", (methodPointerType)&m2765, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t515_t44, t550_m2765_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 6, 3, false, false, 1059, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t515_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2766_ParameterInfos[] = 
{
	{"animator", 0, 134218810, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateInfo", 1, 134218811, &EmptyCustomAttributesCache, &t515_0_0_0},
	{"layerIndex", 2, 134218812, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t515_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2766_MI = 
{
	"OnStateMove", (methodPointerType)&m2766, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t515_t44, t550_m2766_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 3, false, false, 1060, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t515_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2767_ParameterInfos[] = 
{
	{"animator", 0, 134218813, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateInfo", 1, 134218814, &EmptyCustomAttributesCache, &t515_0_0_0},
	{"layerIndex", 2, 134218815, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t515_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2767_MI = 
{
	"OnStateIK", (methodPointerType)&m2767, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t515_t44, t550_m2767_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 8, 3, false, false, 1061, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2768_ParameterInfos[] = 
{
	{"animator", 0, 134218816, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateMachinePathHash", 1, 134218817, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2768_MI = 
{
	"OnStateMachineEnter", (methodPointerType)&m2768, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t550_m2768_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 9, 2, false, false, 1062, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t550_m2769_ParameterInfos[] = 
{
	{"animator", 0, 134218818, &EmptyCustomAttributesCache, &t229_0_0_0},
	{"stateMachinePathHash", 1, 134218819, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2769_MI = 
{
	"OnStateMachineExit", (methodPointerType)&m2769, &t550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t550_m2769_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 10, 2, false, false, 1063, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t550_MIs[] =
{
	&m2762_MI,
	&m2763_MI,
	&m2764_MI,
	&m2765_MI,
	&m2766_MI,
	&m2767_MI,
	&m2768_MI,
	&m2769_MI,
	NULL
};
static MethodInfo* t550_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m2763_MI,
	&m2764_MI,
	&m2765_MI,
	&m2766_MI,
	&m2767_MI,
	&m2768_MI,
	&m2769_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t550_0_0_0;
extern Il2CppType t550_1_0_0;
extern TypeInfo t450_TI;
struct t550;
TypeInfo t550_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "StateMachineBehaviour", "UnityEngine", t550_MIs, NULL, NULL, NULL, &t450_TI, NULL, NULL, &t550_TI, NULL, t550_VT, &EmptyCustomAttributesCache, &t550_TI, &t550_0_0_0, &t550_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t550), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, false, false, false, 8, 0, 0, 0, 0, 11, 0, 0};
#include "t551.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t551_TI;
#include "t551MD.h"



// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern Il2CppType t348_0_0_1542;
FieldInfo t551_f1_FieldInfo = 
{
	"value__", &t348_0_0_1542, &t551_TI, offsetof(t551, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t551_0_0_32854;
FieldInfo t551_f2_FieldInfo = 
{
	"WORDS", &t551_0_0_32854, &t551_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t551_0_0_32854;
FieldInfo t551_f3_FieldInfo = 
{
	"PARAGRAPHS", &t551_0_0_32854, &t551_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t551_FIs[] =
{
	&t551_f1_FieldInfo,
	&t551_f2_FieldInfo,
	&t551_f3_FieldInfo,
	NULL
};
static const uint8_t t551_f2_DefaultValueData = 0;
extern Il2CppType t348_0_0_0;
static Il2CppFieldDefaultValueEntry t551_f2_DefaultValue = 
{
	&t551_f2_FieldInfo, { (char*)&t551_f2_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t551_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t551_f3_DefaultValue = 
{
	&t551_f3_FieldInfo, { (char*)&t551_f3_DefaultValueData, &t348_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t551_FDVs[] = 
{
	&t551_f2_DefaultValue,
	&t551_f3_DefaultValue,
	NULL
};
static MethodInfo* t551_MIs[] =
{
	NULL
};
static MethodInfo* t551_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t551_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t551_0_0_0;
extern Il2CppType t551_1_0_0;
extern TypeInfo t348_TI;
extern TypeInfo t377_TI;
TypeInfo t551_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DblClickSnapping", "", t551_MIs, NULL, t551_FIs, NULL, &t49_TI, NULL, &t377_TI, &t348_TI, NULL, t551_VT, &EmptyCustomAttributesCache, &t348_TI, &t551_0_0_0, &t551_1_0_0, t551_IOs, NULL, NULL, t551_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t551)+ sizeof (Il2CppObject), sizeof (uint8_t), sizeof(uint8_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t552.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t552_TI;
#include "t552MD.h"



// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern Il2CppType t44_0_0_1542;
FieldInfo t552_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t552_TI, offsetof(t552, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f2_FieldInfo = 
{
	"MoveLeft", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f3_FieldInfo = 
{
	"MoveRight", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f4_FieldInfo = 
{
	"MoveUp", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f5_FieldInfo = 
{
	"MoveDown", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f6_FieldInfo = 
{
	"MoveLineStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f7_FieldInfo = 
{
	"MoveLineEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f8_FieldInfo = 
{
	"MoveTextStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f9_FieldInfo = 
{
	"MoveTextEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f10_FieldInfo = 
{
	"MovePageUp", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f11_FieldInfo = 
{
	"MovePageDown", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f12_FieldInfo = 
{
	"MoveGraphicalLineStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f13_FieldInfo = 
{
	"MoveGraphicalLineEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f14_FieldInfo = 
{
	"MoveWordLeft", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f15_FieldInfo = 
{
	"MoveWordRight", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f16_FieldInfo = 
{
	"MoveParagraphForward", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f17_FieldInfo = 
{
	"MoveParagraphBackward", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f18_FieldInfo = 
{
	"MoveToStartOfNextWord", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f19_FieldInfo = 
{
	"MoveToEndOfPreviousWord", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f20_FieldInfo = 
{
	"SelectLeft", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f21_FieldInfo = 
{
	"SelectRight", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f22_FieldInfo = 
{
	"SelectUp", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f23_FieldInfo = 
{
	"SelectDown", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f24_FieldInfo = 
{
	"SelectTextStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f25_FieldInfo = 
{
	"SelectTextEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f26_FieldInfo = 
{
	"SelectPageUp", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f27_FieldInfo = 
{
	"SelectPageDown", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f28_FieldInfo = 
{
	"ExpandSelectGraphicalLineStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f29_FieldInfo = 
{
	"ExpandSelectGraphicalLineEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f30_FieldInfo = 
{
	"SelectGraphicalLineStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f31_FieldInfo = 
{
	"SelectGraphicalLineEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f32_FieldInfo = 
{
	"SelectWordLeft", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f33_FieldInfo = 
{
	"SelectWordRight", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f34_FieldInfo = 
{
	"SelectToEndOfPreviousWord", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f35_FieldInfo = 
{
	"SelectToStartOfNextWord", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f36_FieldInfo = 
{
	"SelectParagraphBackward", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f37_FieldInfo = 
{
	"SelectParagraphForward", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f38_FieldInfo = 
{
	"Delete", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f39_FieldInfo = 
{
	"Backspace", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f40_FieldInfo = 
{
	"DeleteWordBack", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f41_FieldInfo = 
{
	"DeleteWordForward", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f42_FieldInfo = 
{
	"DeleteLineBack", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f43_FieldInfo = 
{
	"Cut", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f44_FieldInfo = 
{
	"Copy", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f45_FieldInfo = 
{
	"Paste", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f46_FieldInfo = 
{
	"SelectAll", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f47_FieldInfo = 
{
	"SelectNone", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f48_FieldInfo = 
{
	"ScrollStart", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f49_FieldInfo = 
{
	"ScrollEnd", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f50_FieldInfo = 
{
	"ScrollPageUp", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t552_0_0_32854;
FieldInfo t552_f51_FieldInfo = 
{
	"ScrollPageDown", &t552_0_0_32854, &t552_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t552_FIs[] =
{
	&t552_f1_FieldInfo,
	&t552_f2_FieldInfo,
	&t552_f3_FieldInfo,
	&t552_f4_FieldInfo,
	&t552_f5_FieldInfo,
	&t552_f6_FieldInfo,
	&t552_f7_FieldInfo,
	&t552_f8_FieldInfo,
	&t552_f9_FieldInfo,
	&t552_f10_FieldInfo,
	&t552_f11_FieldInfo,
	&t552_f12_FieldInfo,
	&t552_f13_FieldInfo,
	&t552_f14_FieldInfo,
	&t552_f15_FieldInfo,
	&t552_f16_FieldInfo,
	&t552_f17_FieldInfo,
	&t552_f18_FieldInfo,
	&t552_f19_FieldInfo,
	&t552_f20_FieldInfo,
	&t552_f21_FieldInfo,
	&t552_f22_FieldInfo,
	&t552_f23_FieldInfo,
	&t552_f24_FieldInfo,
	&t552_f25_FieldInfo,
	&t552_f26_FieldInfo,
	&t552_f27_FieldInfo,
	&t552_f28_FieldInfo,
	&t552_f29_FieldInfo,
	&t552_f30_FieldInfo,
	&t552_f31_FieldInfo,
	&t552_f32_FieldInfo,
	&t552_f33_FieldInfo,
	&t552_f34_FieldInfo,
	&t552_f35_FieldInfo,
	&t552_f36_FieldInfo,
	&t552_f37_FieldInfo,
	&t552_f38_FieldInfo,
	&t552_f39_FieldInfo,
	&t552_f40_FieldInfo,
	&t552_f41_FieldInfo,
	&t552_f42_FieldInfo,
	&t552_f43_FieldInfo,
	&t552_f44_FieldInfo,
	&t552_f45_FieldInfo,
	&t552_f46_FieldInfo,
	&t552_f47_FieldInfo,
	&t552_f48_FieldInfo,
	&t552_f49_FieldInfo,
	&t552_f50_FieldInfo,
	&t552_f51_FieldInfo,
	NULL
};
static const int32_t t552_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t552_f2_DefaultValue = 
{
	&t552_f2_FieldInfo, { (char*)&t552_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t552_f3_DefaultValue = 
{
	&t552_f3_FieldInfo, { (char*)&t552_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t552_f4_DefaultValue = 
{
	&t552_f4_FieldInfo, { (char*)&t552_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t552_f5_DefaultValue = 
{
	&t552_f5_FieldInfo, { (char*)&t552_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t552_f6_DefaultValue = 
{
	&t552_f6_FieldInfo, { (char*)&t552_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t552_f7_DefaultValue = 
{
	&t552_f7_FieldInfo, { (char*)&t552_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t552_f8_DefaultValue = 
{
	&t552_f8_FieldInfo, { (char*)&t552_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t552_f9_DefaultValue = 
{
	&t552_f9_FieldInfo, { (char*)&t552_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t552_f10_DefaultValue = 
{
	&t552_f10_FieldInfo, { (char*)&t552_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry t552_f11_DefaultValue = 
{
	&t552_f11_FieldInfo, { (char*)&t552_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t552_f12_DefaultValue = 
{
	&t552_f12_FieldInfo, { (char*)&t552_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry t552_f13_DefaultValue = 
{
	&t552_f13_FieldInfo, { (char*)&t552_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry t552_f14_DefaultValue = 
{
	&t552_f14_FieldInfo, { (char*)&t552_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry t552_f15_DefaultValue = 
{
	&t552_f15_FieldInfo, { (char*)&t552_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry t552_f16_DefaultValue = 
{
	&t552_f16_FieldInfo, { (char*)&t552_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry t552_f17_DefaultValue = 
{
	&t552_f17_FieldInfo, { (char*)&t552_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t552_f18_DefaultValue = 
{
	&t552_f18_FieldInfo, { (char*)&t552_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry t552_f19_DefaultValue = 
{
	&t552_f19_FieldInfo, { (char*)&t552_f19_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry t552_f20_DefaultValue = 
{
	&t552_f20_FieldInfo, { (char*)&t552_f20_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry t552_f21_DefaultValue = 
{
	&t552_f21_FieldInfo, { (char*)&t552_f21_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry t552_f22_DefaultValue = 
{
	&t552_f22_FieldInfo, { (char*)&t552_f22_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry t552_f23_DefaultValue = 
{
	&t552_f23_FieldInfo, { (char*)&t552_f23_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry t552_f24_DefaultValue = 
{
	&t552_f24_FieldInfo, { (char*)&t552_f24_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f25_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry t552_f25_DefaultValue = 
{
	&t552_f25_FieldInfo, { (char*)&t552_f25_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f26_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry t552_f26_DefaultValue = 
{
	&t552_f26_FieldInfo, { (char*)&t552_f26_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f27_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry t552_f27_DefaultValue = 
{
	&t552_f27_FieldInfo, { (char*)&t552_f27_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f28_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry t552_f28_DefaultValue = 
{
	&t552_f28_FieldInfo, { (char*)&t552_f28_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f29_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry t552_f29_DefaultValue = 
{
	&t552_f29_FieldInfo, { (char*)&t552_f29_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f30_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry t552_f30_DefaultValue = 
{
	&t552_f30_FieldInfo, { (char*)&t552_f30_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f31_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry t552_f31_DefaultValue = 
{
	&t552_f31_FieldInfo, { (char*)&t552_f31_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f32_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry t552_f32_DefaultValue = 
{
	&t552_f32_FieldInfo, { (char*)&t552_f32_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f33_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry t552_f33_DefaultValue = 
{
	&t552_f33_FieldInfo, { (char*)&t552_f33_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f34_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t552_f34_DefaultValue = 
{
	&t552_f34_FieldInfo, { (char*)&t552_f34_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f35_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry t552_f35_DefaultValue = 
{
	&t552_f35_FieldInfo, { (char*)&t552_f35_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f36_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry t552_f36_DefaultValue = 
{
	&t552_f36_FieldInfo, { (char*)&t552_f36_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f37_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry t552_f37_DefaultValue = 
{
	&t552_f37_FieldInfo, { (char*)&t552_f37_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f38_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry t552_f38_DefaultValue = 
{
	&t552_f38_FieldInfo, { (char*)&t552_f38_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f39_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry t552_f39_DefaultValue = 
{
	&t552_f39_FieldInfo, { (char*)&t552_f39_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f40_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry t552_f40_DefaultValue = 
{
	&t552_f40_FieldInfo, { (char*)&t552_f40_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f41_DefaultValueData = 39;
static Il2CppFieldDefaultValueEntry t552_f41_DefaultValue = 
{
	&t552_f41_FieldInfo, { (char*)&t552_f41_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f42_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry t552_f42_DefaultValue = 
{
	&t552_f42_FieldInfo, { (char*)&t552_f42_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f43_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry t552_f43_DefaultValue = 
{
	&t552_f43_FieldInfo, { (char*)&t552_f43_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f44_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry t552_f44_DefaultValue = 
{
	&t552_f44_FieldInfo, { (char*)&t552_f44_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f45_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry t552_f45_DefaultValue = 
{
	&t552_f45_FieldInfo, { (char*)&t552_f45_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f46_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry t552_f46_DefaultValue = 
{
	&t552_f46_FieldInfo, { (char*)&t552_f46_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f47_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry t552_f47_DefaultValue = 
{
	&t552_f47_FieldInfo, { (char*)&t552_f47_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f48_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry t552_f48_DefaultValue = 
{
	&t552_f48_FieldInfo, { (char*)&t552_f48_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f49_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry t552_f49_DefaultValue = 
{
	&t552_f49_FieldInfo, { (char*)&t552_f49_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f50_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry t552_f50_DefaultValue = 
{
	&t552_f50_FieldInfo, { (char*)&t552_f50_DefaultValueData, &t44_0_0_0 }};
static const int32_t t552_f51_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry t552_f51_DefaultValue = 
{
	&t552_f51_FieldInfo, { (char*)&t552_f51_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t552_FDVs[] = 
{
	&t552_f2_DefaultValue,
	&t552_f3_DefaultValue,
	&t552_f4_DefaultValue,
	&t552_f5_DefaultValue,
	&t552_f6_DefaultValue,
	&t552_f7_DefaultValue,
	&t552_f8_DefaultValue,
	&t552_f9_DefaultValue,
	&t552_f10_DefaultValue,
	&t552_f11_DefaultValue,
	&t552_f12_DefaultValue,
	&t552_f13_DefaultValue,
	&t552_f14_DefaultValue,
	&t552_f15_DefaultValue,
	&t552_f16_DefaultValue,
	&t552_f17_DefaultValue,
	&t552_f18_DefaultValue,
	&t552_f19_DefaultValue,
	&t552_f20_DefaultValue,
	&t552_f21_DefaultValue,
	&t552_f22_DefaultValue,
	&t552_f23_DefaultValue,
	&t552_f24_DefaultValue,
	&t552_f25_DefaultValue,
	&t552_f26_DefaultValue,
	&t552_f27_DefaultValue,
	&t552_f28_DefaultValue,
	&t552_f29_DefaultValue,
	&t552_f30_DefaultValue,
	&t552_f31_DefaultValue,
	&t552_f32_DefaultValue,
	&t552_f33_DefaultValue,
	&t552_f34_DefaultValue,
	&t552_f35_DefaultValue,
	&t552_f36_DefaultValue,
	&t552_f37_DefaultValue,
	&t552_f38_DefaultValue,
	&t552_f39_DefaultValue,
	&t552_f40_DefaultValue,
	&t552_f41_DefaultValue,
	&t552_f42_DefaultValue,
	&t552_f43_DefaultValue,
	&t552_f44_DefaultValue,
	&t552_f45_DefaultValue,
	&t552_f46_DefaultValue,
	&t552_f47_DefaultValue,
	&t552_f48_DefaultValue,
	&t552_f49_DefaultValue,
	&t552_f50_DefaultValue,
	&t552_f51_DefaultValue,
	NULL
};
static MethodInfo* t552_MIs[] =
{
	NULL
};
static MethodInfo* t552_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t552_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t552_0_0_0;
extern Il2CppType t552_1_0_0;
TypeInfo t552_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextEditOp", "", t552_MIs, NULL, t552_FIs, NULL, &t49_TI, NULL, &t377_TI, &t44_TI, NULL, t552_VT, &EmptyCustomAttributesCache, &t44_TI, &t552_0_0_0, &t552_1_0_0, t552_IOs, NULL, NULL, t552_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t552)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 259, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 51, 0, 0, 23, 0, 3};
#include "t377.h"
#ifndef _MSC_VER
#else
#endif
#include "t377MD.h"

#include "t378.h"
#include "t466.h"
#include "t263.h"
extern TypeInfo t378_TI;
extern TypeInfo t466_TI;
extern TypeInfo t164_TI;
extern TypeInfo t479_TI;
#include "t378MD.h"
#include "t466MD.h"
#include "t263MD.h"
#include "t479MD.h"
extern MethodInfo m2245_MI;
extern MethodInfo m2292_MI;
extern MethodInfo m2771_MI;
extern MethodInfo m1730_MI;
extern MethodInfo m2770_MI;
extern MethodInfo m2247_MI;
extern MethodInfo m2772_MI;
extern MethodInfo m2774_MI;
extern MethodInfo m1823_MI;
extern MethodInfo m2293_MI;
extern MethodInfo m2281_MI;
extern MethodInfo m2268_MI;
extern MethodInfo m2296_MI;
extern MethodInfo m2298_MI;
extern MethodInfo m1968_MI;
extern MethodInfo m2290_MI;
extern MethodInfo m1969_MI;
extern MethodInfo m2266_MI;
extern MethodInfo m2177_MI;
extern MethodInfo m2176_MI;
extern MethodInfo m1740_MI;
extern MethodInfo m2775_MI;
extern MethodInfo m2773_MI;


extern MethodInfo m1728_MI;
 void m1728 (t377 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t378_TI));
		t378 * L_0 = (t378 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t378_TI));
		m2245(L_0, &m2245_MI);
		__this->f3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t466_TI));
		t466 * L_1 = m2292(NULL, &m2292_MI);
		__this->f4 = L_1;
		t17  L_2 = m1394(NULL, &m1394_MI);
		__this->f10 = L_2;
		__this->f18 = (-1);
		m1331(__this, &m1331_MI);
		return;
	}
}
 void m2770 (t377 * __this, MethodInfo* method){
	{
		__this->f7 = 0;
		__this->f18 = (-1);
		return;
	}
}
extern MethodInfo m1732_MI;
 void m1732 (t377 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->f6);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->f1 = L_1;
		__this->f0 = V_0;
		goto IL_0026;
	}

IL_0020:
	{
		m2771(__this, &m2771_MI);
	}

IL_0026:
	{
		__this->f9 = 1;
		return;
	}
}
 void m2771 (t377 * __this, MethodInfo* method){
	{
		__this->f0 = 0;
		t378 * L_0 = (__this->f3);
		t7* L_1 = m1730(L_0, &m1730_MI);
		int32_t L_2 = m1715(L_1, &m1715_MI);
		__this->f1 = L_2;
		m2770(__this, &m2770_MI);
		return;
	}
}
 bool m2772 (t377 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t378 * L_0 = (__this->f3);
		t7* L_1 = m1730(L_0, &m1730_MI);
		int32_t L_2 = m1715(L_1, &m1715_MI);
		V_0 = L_2;
		int32_t L_3 = (__this->f0);
		if ((((int32_t)L_3) <= ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		__this->f0 = V_0;
	}

IL_0024:
	{
		int32_t L_4 = (__this->f1);
		if ((((int32_t)L_4) <= ((int32_t)V_0)))
		{
			goto IL_0037;
		}
	}
	{
		__this->f1 = V_0;
	}

IL_0037:
	{
		int32_t L_5 = (__this->f0);
		int32_t L_6 = (__this->f1);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		int32_t L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_00c0;
		}
	}
	{
		t378 * L_9 = (__this->f3);
		t378 * L_10 = (__this->f3);
		t7* L_11 = m1730(L_10, &m1730_MI);
		int32_t L_12 = (__this->f0);
		t7* L_13 = m1742(L_11, 0, L_12, &m1742_MI);
		t378 * L_14 = (__this->f3);
		t7* L_15 = m1730(L_14, &m1730_MI);
		int32_t L_16 = (__this->f1);
		t378 * L_17 = (__this->f3);
		t7* L_18 = m1730(L_17, &m1730_MI);
		int32_t L_19 = m1715(L_18, &m1715_MI);
		int32_t L_20 = (__this->f1);
		t7* L_21 = m1742(L_15, L_16, ((int32_t)(L_19-L_20)), &m1742_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_22 = m66(NULL, L_13, L_21, &m66_MI);
		m2247(L_9, L_22, &m2247_MI);
		int32_t L_23 = (__this->f0);
		__this->f1 = L_23;
		goto IL_0120;
	}

IL_00c0:
	{
		t378 * L_24 = (__this->f3);
		t378 * L_25 = (__this->f3);
		t7* L_26 = m1730(L_25, &m1730_MI);
		int32_t L_27 = (__this->f1);
		t7* L_28 = m1742(L_26, 0, L_27, &m1742_MI);
		t378 * L_29 = (__this->f3);
		t7* L_30 = m1730(L_29, &m1730_MI);
		int32_t L_31 = (__this->f0);
		t378 * L_32 = (__this->f3);
		t7* L_33 = m1730(L_32, &m1730_MI);
		int32_t L_34 = m1715(L_33, &m1715_MI);
		int32_t L_35 = (__this->f0);
		t7* L_36 = m1742(L_30, L_31, ((int32_t)(L_34-L_35)), &m1742_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_37 = m66(NULL, L_28, L_36, &m66_MI);
		m2247(L_24, L_37, &m2247_MI);
		int32_t L_38 = (__this->f1);
		__this->f0 = L_38;
	}

IL_0120:
	{
		m2770(__this, &m2770_MI);
		return 1;
	}
}
 void m2773 (t377 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m2772(__this, &m2772_MI);
		t378 * L_0 = (__this->f3);
		t378 * L_1 = (__this->f3);
		t7* L_2 = m1730(L_1, &m1730_MI);
		int32_t L_3 = (__this->f0);
		t7* L_4 = m1768(L_2, L_3, p0, &m1768_MI);
		m2247(L_0, L_4, &m2247_MI);
		int32_t L_5 = (__this->f0);
		int32_t L_6 = m1715(p0, &m1715_MI);
		int32_t L_7 = ((int32_t)(L_5+L_6));
		V_0 = L_7;
		__this->f0 = L_7;
		__this->f1 = V_0;
		m2770(__this, &m2770_MI);
		m2774(__this, &m2774_MI);
		__this->f11 = 1;
		return;
	}
}
 void m2774 (t377 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t164  V_1 = {0};
	t17  V_2 = {0};
	t17  V_3 = {0};
	t17 * G_B17_0 = {0};
	t17 * G_B16_0 = {0};
	float G_B18_0 = 0.0f;
	t17 * G_B18_1 = {0};
	{
		int32_t L_0 = (__this->f0);
		V_0 = L_0;
		t466 * L_1 = (__this->f4);
		t164 * L_2 = &(__this->f5);
		float L_3 = m1573(L_2, &m1573_MI);
		t164 * L_4 = &(__this->f5);
		float L_5 = m1574(L_4, &m1574_MI);
		t164  L_6 = {0};
		m1823(&L_6, (0.0f), (0.0f), L_3, L_5, &m1823_MI);
		t378 * L_7 = (__this->f3);
		t17  L_8 = m2293(L_1, L_6, L_7, V_0, &m2293_MI);
		__this->f12 = L_8;
		t466 * L_9 = (__this->f4);
		t263 * L_10 = m2281(L_9, &m2281_MI);
		t164  L_11 = (__this->f5);
		t164  L_12 = m2268(L_10, L_11, &m2268_MI);
		V_1 = L_12;
		t466 * L_13 = (__this->f4);
		t378 * L_14 = (__this->f3);
		t17  L_15 = m2296(L_13, L_14, &m2296_MI);
		V_3 = L_15;
		float L_16 = ((&V_3)->f1);
		t466 * L_17 = (__this->f4);
		t378 * L_18 = (__this->f3);
		t164 * L_19 = &(__this->f5);
		float L_20 = m1573(L_19, &m1573_MI);
		float L_21 = m2298(L_17, L_18, L_20, &m2298_MI);
		m62((&V_2), L_16, L_21, &m62_MI);
		float L_22 = ((&V_2)->f1);
		t164 * L_23 = &(__this->f5);
		float L_24 = m1573(L_23, &m1573_MI);
		if ((((float)L_22) >= ((float)L_24)))
		{
			goto IL_00c3;
		}
	}
	{
		t17 * L_25 = &(__this->f10);
		L_25->f1 = (0.0f);
		goto IL_015f;
	}

IL_00c3:
	{
		t17 * L_26 = &(__this->f12);
		float L_27 = (L_26->f1);
		t17 * L_28 = &(__this->f10);
		float L_29 = (L_28->f1);
		float L_30 = m1573((&V_1), &m1573_MI);
		if ((((float)((float)(L_27+(1.0f)))) <= ((float)((float)(L_29+L_30)))))
		{
			goto IL_010a;
		}
	}
	{
		t17 * L_31 = &(__this->f10);
		t17 * L_32 = &(__this->f12);
		float L_33 = (L_32->f1);
		float L_34 = m1573((&V_1), &m1573_MI);
		L_31->f1 = ((float)(L_33-L_34));
	}

IL_010a:
	{
		t17 * L_35 = &(__this->f12);
		float L_36 = (L_35->f1);
		t17 * L_37 = &(__this->f10);
		float L_38 = (L_37->f1);
		t466 * L_39 = (__this->f4);
		t263 * L_40 = m2281(L_39, &m2281_MI);
		int32_t L_41 = m1968(L_40, &m1968_MI);
		if ((((float)L_36) >= ((float)((float)(L_38+(((float)L_41)))))))
		{
			goto IL_015f;
		}
	}
	{
		t17 * L_42 = &(__this->f10);
		t17 * L_43 = &(__this->f12);
		float L_44 = (L_43->f1);
		t466 * L_45 = (__this->f4);
		t263 * L_46 = m2281(L_45, &m2281_MI);
		int32_t L_47 = m1968(L_46, &m1968_MI);
		L_42->f1 = ((float)(L_44-(((float)L_47))));
	}

IL_015f:
	{
		float L_48 = ((&V_2)->f2);
		float L_49 = m1574((&V_1), &m1574_MI);
		if ((((float)L_48) >= ((float)L_49)))
		{
			goto IL_0187;
		}
	}
	{
		t17 * L_50 = &(__this->f10);
		L_50->f2 = (0.0f);
		goto IL_0259;
	}

IL_0187:
	{
		t17 * L_51 = &(__this->f12);
		float L_52 = (L_51->f2);
		t466 * L_53 = (__this->f4);
		float L_54 = m2290(L_53, &m2290_MI);
		t17 * L_55 = &(__this->f10);
		float L_56 = (L_55->f2);
		float L_57 = m1574((&V_1), &m1574_MI);
		t466 * L_58 = (__this->f4);
		t263 * L_59 = m2281(L_58, &m2281_MI);
		int32_t L_60 = m1969(L_59, &m1969_MI);
		if ((((float)((float)(L_52+L_54))) <= ((float)((float)(((float)(L_56+L_57))+(((float)L_60)))))))
		{
			goto IL_0204;
		}
	}
	{
		t17 * L_61 = &(__this->f10);
		t17 * L_62 = &(__this->f12);
		float L_63 = (L_62->f2);
		float L_64 = m1574((&V_1), &m1574_MI);
		t466 * L_65 = (__this->f4);
		t263 * L_66 = m2281(L_65, &m2281_MI);
		int32_t L_67 = m1969(L_66, &m1969_MI);
		t466 * L_68 = (__this->f4);
		float L_69 = m2290(L_68, &m2290_MI);
		L_61->f2 = ((float)(((float)(((float)(L_63-L_64))-(((float)L_67))))+L_69));
	}

IL_0204:
	{
		t17 * L_70 = &(__this->f12);
		float L_71 = (L_70->f2);
		t17 * L_72 = &(__this->f10);
		float L_73 = (L_72->f2);
		t466 * L_74 = (__this->f4);
		t263 * L_75 = m2281(L_74, &m2281_MI);
		int32_t L_76 = m1969(L_75, &m1969_MI);
		if ((((float)L_71) >= ((float)((float)(L_73+(((float)L_76)))))))
		{
			goto IL_0259;
		}
	}
	{
		t17 * L_77 = &(__this->f10);
		t17 * L_78 = &(__this->f12);
		float L_79 = (L_78->f2);
		t466 * L_80 = (__this->f4);
		t263 * L_81 = m2281(L_80, &m2281_MI);
		int32_t L_82 = m1969(L_81, &m1969_MI);
		L_77->f2 = ((float)(L_79-(((float)L_82))));
	}

IL_0259:
	{
		t17 * L_83 = &(__this->f10);
		float L_84 = (L_83->f2);
		if ((((float)L_84) <= ((float)(0.0f))))
		{
			goto IL_02cb;
		}
	}
	{
		float L_85 = ((&V_2)->f2);
		t17 * L_86 = &(__this->f10);
		float L_87 = (L_86->f2);
		float L_88 = m1574((&V_1), &m1574_MI);
		if ((((float)((float)(L_85-L_87))) >= ((float)L_88)))
		{
			goto IL_02cb;
		}
	}
	{
		t17 * L_89 = &(__this->f10);
		float L_90 = ((&V_2)->f2);
		float L_91 = m1574((&V_1), &m1574_MI);
		t466 * L_92 = (__this->f4);
		t263 * L_93 = m2281(L_92, &m2281_MI);
		int32_t L_94 = m1969(L_93, &m1969_MI);
		t466 * L_95 = (__this->f4);
		t263 * L_96 = m2281(L_95, &m2281_MI);
		int32_t L_97 = m2266(L_96, &m2266_MI);
		L_89->f2 = ((float)(((float)(((float)(L_90-L_91))-(((float)L_94))))-(((float)L_97))));
	}

IL_02cb:
	{
		t17 * L_98 = &(__this->f10);
		t17 * L_99 = &(__this->f10);
		float L_100 = (L_99->f2);
		G_B16_0 = L_98;
		if ((((float)L_100) >= ((float)(0.0f))))
		{
			G_B17_0 = L_98;
			goto IL_02f0;
		}
	}
	{
		G_B18_0 = (0.0f);
		G_B18_1 = G_B16_0;
		goto IL_02fb;
	}

IL_02f0:
	{
		t17 * L_101 = &(__this->f10);
		float L_102 = (L_101->f2);
		G_B18_0 = L_102;
		G_B18_1 = G_B17_0;
	}

IL_02fb:
	{
		G_B18_1->f2 = G_B18_0;
		return;
	}
}
extern MethodInfo m1733_MI;
 void m1733 (t377 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		int32_t L_0 = (__this->f1);
		int32_t L_1 = (__this->f0);
		if ((((uint32_t)L_0) != ((uint32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = (__this->f8);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = (__this->f0);
		int32_t L_4 = (__this->f1);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0058;
		}
	}
	{
		t378 * L_5 = (__this->f3);
		t7* L_6 = m1730(L_5, &m1730_MI);
		int32_t L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		int32_t L_9 = (__this->f0);
		t7* L_10 = m1742(L_6, L_7, ((int32_t)(L_8-L_9)), &m1742_MI);
		V_0 = L_10;
		goto IL_007c;
	}

IL_0058:
	{
		t378 * L_11 = (__this->f3);
		t7* L_12 = m1730(L_11, &m1730_MI);
		int32_t L_13 = (__this->f1);
		int32_t L_14 = (__this->f0);
		int32_t L_15 = (__this->f1);
		t7* L_16 = m1742(L_12, L_13, ((int32_t)(L_14-L_15)), &m1742_MI);
		V_0 = L_16;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t479_TI));
		m2177(NULL, V_0, &m2177_MI);
		return;
	}
}
 t7* m2775 (t29 * __this, t7* p0, MethodInfo* method){
	{
		t7* L_0 = m2932(p0, (t7*) &_stringLiteral188, (t7*) &_stringLiteral79, &m2932_MI);
		p0 = L_0;
		t7* L_1 = m2933(p0, ((int32_t)10), ((int32_t)32), &m2933_MI);
		p0 = L_1;
		t7* L_2 = m2933(p0, ((int32_t)13), ((int32_t)32), &m2933_MI);
		p0 = L_2;
		return p0;
	}
}
extern MethodInfo m1729_MI;
 bool m1729 (t377 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t479_TI));
		t7* L_0 = m2176(NULL, &m2176_MI);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_1 = m1740(NULL, V_0, (((t7_SFs*)(&t7_TI)->static_fields)->f2), &m1740_MI);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		bool L_2 = (__this->f6);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		t7* L_3 = m2775(NULL, V_0, &m2775_MI);
		V_0 = L_3;
	}

IL_0028:
	{
		m2773(__this, V_0, &m2773_MI);
		return 1;
	}

IL_0031:
	{
		return 0;
	}
}
// Metadata Definition UnityEngine.TextEditor
extern Il2CppType t44_0_0_6;
FieldInfo t377_f0_FieldInfo = 
{
	"pos", &t44_0_0_6, &t377_TI, offsetof(t377, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t377_f1_FieldInfo = 
{
	"selectPos", &t44_0_0_6, &t377_TI, offsetof(t377, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t377_f2_FieldInfo = 
{
	"controlID", &t44_0_0_6, &t377_TI, offsetof(t377, f2), &EmptyCustomAttributesCache};
extern Il2CppType t378_0_0_6;
FieldInfo t377_f3_FieldInfo = 
{
	"content", &t378_0_0_6, &t377_TI, offsetof(t377, f3), &EmptyCustomAttributesCache};
extern Il2CppType t466_0_0_6;
FieldInfo t377_f4_FieldInfo = 
{
	"style", &t466_0_0_6, &t377_TI, offsetof(t377, f4), &EmptyCustomAttributesCache};
extern Il2CppType t164_0_0_6;
FieldInfo t377_f5_FieldInfo = 
{
	"position", &t164_0_0_6, &t377_TI, offsetof(t377, f5), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t377_f6_FieldInfo = 
{
	"multiline", &t40_0_0_6, &t377_TI, offsetof(t377, f6), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t377_f7_FieldInfo = 
{
	"hasHorizontalCursorPos", &t40_0_0_6, &t377_TI, offsetof(t377, f7), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t377_f8_FieldInfo = 
{
	"isPasswordField", &t40_0_0_6, &t377_TI, offsetof(t377, f8), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_3;
FieldInfo t377_f9_FieldInfo = 
{
	"m_HasFocus", &t40_0_0_3, &t377_TI, offsetof(t377, f9), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t377_f10_FieldInfo = 
{
	"scrollOffset", &t17_0_0_6, &t377_TI, offsetof(t377, f10), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t377_f11_FieldInfo = 
{
	"m_TextHeightPotentiallyChanged", &t40_0_0_1, &t377_TI, offsetof(t377, f11), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t377_f12_FieldInfo = 
{
	"graphicalCursorPos", &t17_0_0_6, &t377_TI, offsetof(t377, f12), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t377_f13_FieldInfo = 
{
	"graphicalSelectCursorPos", &t17_0_0_6, &t377_TI, offsetof(t377, f13), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t377_f14_FieldInfo = 
{
	"m_MouseDragSelectsWholeWords", &t40_0_0_1, &t377_TI, offsetof(t377, f14), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t377_f15_FieldInfo = 
{
	"m_DblClickInitPos", &t44_0_0_1, &t377_TI, offsetof(t377, f15), &EmptyCustomAttributesCache};
extern Il2CppType t551_0_0_1;
FieldInfo t377_f16_FieldInfo = 
{
	"m_DblClickSnap", &t551_0_0_1, &t377_TI, offsetof(t377, f16), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t377_f17_FieldInfo = 
{
	"m_bJustSelected", &t40_0_0_1, &t377_TI, offsetof(t377, f17), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t377_f18_FieldInfo = 
{
	"m_iAltCursorPos", &t44_0_0_1, &t377_TI, offsetof(t377, f18), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t377_f19_FieldInfo = 
{
	"oldText", &t7_0_0_1, &t377_TI, offsetof(t377, f19), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t377_f20_FieldInfo = 
{
	"oldPos", &t44_0_0_1, &t377_TI, offsetof(t377, f20), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t377_f21_FieldInfo = 
{
	"oldSelectPos", &t44_0_0_1, &t377_TI, offsetof(t377, f21), &EmptyCustomAttributesCache};
extern Il2CppType t553_0_0_17;
FieldInfo t377_f22_FieldInfo = 
{
	"s_Keyactions", &t553_0_0_17, &t377_TI, offsetof(t377_SFs, f22), &EmptyCustomAttributesCache};
static FieldInfo* t377_FIs[] =
{
	&t377_f0_FieldInfo,
	&t377_f1_FieldInfo,
	&t377_f2_FieldInfo,
	&t377_f3_FieldInfo,
	&t377_f4_FieldInfo,
	&t377_f5_FieldInfo,
	&t377_f6_FieldInfo,
	&t377_f7_FieldInfo,
	&t377_f8_FieldInfo,
	&t377_f9_FieldInfo,
	&t377_f10_FieldInfo,
	&t377_f11_FieldInfo,
	&t377_f12_FieldInfo,
	&t377_f13_FieldInfo,
	&t377_f14_FieldInfo,
	&t377_f15_FieldInfo,
	&t377_f16_FieldInfo,
	&t377_f17_FieldInfo,
	&t377_f18_FieldInfo,
	&t377_f19_FieldInfo,
	&t377_f20_FieldInfo,
	&t377_f21_FieldInfo,
	&t377_f22_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1728_MI = 
{
	".ctor", (methodPointerType)&m1728, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1064, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2770_MI = 
{
	"ClearCursorPos", (methodPointerType)&m2770, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1065, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1732_MI = 
{
	"OnFocus", (methodPointerType)&m1732, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1066, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2771_MI = 
{
	"SelectAll", (methodPointerType)&m2771, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1067, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2772_MI = 
{
	"DeleteSelection", (methodPointerType)&m2772, &t377_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1068, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t377_m2773_ParameterInfos[] = 
{
	{"replace", 0, 134218820, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2773_MI = 
{
	"ReplaceSelection", (methodPointerType)&m2773, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t377_m2773_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1069, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2774_MI = 
{
	"UpdateScrollOffset", (methodPointerType)&m2774, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1070, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1733_MI = 
{
	"Copy", (methodPointerType)&m1733, &t377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1071, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t377_m2775_ParameterInfos[] = 
{
	{"value", 0, 134218821, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2775_MI = 
{
	"ReplaceNewlinesWithSpaces", (methodPointerType)&m2775, &t377_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t377_m2775_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1072, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1729_MI = 
{
	"Paste", (methodPointerType)&m1729, &t377_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1073, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t377_MIs[] =
{
	&m1728_MI,
	&m2770_MI,
	&m1732_MI,
	&m2771_MI,
	&m2772_MI,
	&m2773_MI,
	&m2774_MI,
	&m1733_MI,
	&m2775_MI,
	&m1729_MI,
	NULL
};
extern TypeInfo t551_TI;
extern TypeInfo t552_TI;
static TypeInfo* t377_TI__nestedTypes[3] =
{
	&t551_TI,
	&t552_TI,
	NULL
};
static MethodInfo* t377_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t377_0_0_0;
extern Il2CppType t377_1_0_0;
struct t377;
TypeInfo t377_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextEditor", "UnityEngine", t377_MIs, NULL, t377_FIs, NULL, &t29_TI, t377_TI__nestedTypes, NULL, &t377_TI, NULL, t377_VT, &EmptyCustomAttributesCache, &t377_TI, &t377_0_0_0, &t377_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t377), 0, -1, sizeof(t377_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 10, 0, 23, 0, 2, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t110.h"
#include "t110MD.h"
extern MethodInfo m1582_MI;
extern MethodInfo m2776_MI;
extern MethodInfo m2777_MI;


 bool m2776 (t238 * __this, t132  p0, t132  p1, MethodInfo* method){
	t287  V_0 = {0};
	t287  V_1 = {0};
	{
		t287  L_0 = m1582(NULL, p0, &m1582_MI);
		V_0 = L_0;
		t287  L_1 = m1582(NULL, p1, &m1582_MI);
		V_1 = L_1;
		t287  L_2 = V_0;
		t29 * L_3 = Box(InitializedTypeInfo(&t287_TI), &L_2);
		t287  L_4 = V_1;
		t29 * L_5 = Box(InitializedTypeInfo(&t287_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1388_MI, L_3, L_5);
		return L_6;
	}
}
 bool m2777 (t238 * __this, t17  p0, t17  p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		float L_0 = ((&p0)->f1);
		float L_1 = ((&p1)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		bool L_2 = m1442(NULL, L_0, L_1, &m1442_MI);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ((&p0)->f2);
		float L_4 = ((&p1)->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		bool L_5 = m1442(NULL, L_3, L_4, &m1442_MI);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
 bool m2778 (t238 * __this, t238  p0, MethodInfo* method){
	int32_t G_B19_0 = 0;
	{
		t132  L_0 = (__this->f1);
		t132  L_1 = ((&p0)->f1);
		bool L_2 = m2776(__this, L_0, L_1, &m2776_MI);
		if (!L_2)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_3 = (__this->f2);
		int32_t L_4 = ((&p0)->f2);
		if ((((uint32_t)L_3) != ((uint32_t)L_4)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_5 = (__this->f8);
		int32_t L_6 = ((&p0)->f8);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_7 = (__this->f9);
		int32_t L_8 = ((&p0)->f9);
		if ((((uint32_t)L_7) != ((uint32_t)L_8)))
		{
			goto IL_015d;
		}
	}
	{
		float L_9 = (__this->f3);
		float L_10 = ((&p0)->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		bool L_11 = m1442(NULL, L_9, L_10, &m1442_MI);
		if (!L_11)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_12 = (__this->f5);
		int32_t L_13 = ((&p0)->f5);
		if ((((uint32_t)L_12) != ((uint32_t)L_13)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_14 = (__this->f4);
		bool L_15 = ((&p0)->f4);
		if ((((uint32_t)L_14) != ((uint32_t)L_15)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_16 = (__this->f6);
		int32_t L_17 = ((&p0)->f6);
		if ((((uint32_t)L_16) != ((uint32_t)L_17)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_18 = (__this->f7);
		bool L_19 = ((&p0)->f7);
		if ((((uint32_t)L_18) != ((uint32_t)L_19)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = ((&p0)->f8);
		if ((((uint32_t)L_20) != ((uint32_t)L_21)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_22 = (__this->f9);
		int32_t L_23 = ((&p0)->f9);
		if ((((uint32_t)L_22) != ((uint32_t)L_23)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_24 = (__this->f7);
		bool L_25 = ((&p0)->f7);
		if ((((uint32_t)L_24) != ((uint32_t)L_25)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_26 = (__this->f10);
		bool L_27 = ((&p0)->f10);
		if ((((uint32_t)L_26) != ((uint32_t)L_27)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_28 = (__this->f12);
		int32_t L_29 = ((&p0)->f12);
		if ((((uint32_t)L_28) != ((uint32_t)L_29)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_30 = (__this->f11);
		int32_t L_31 = ((&p0)->f11);
		if ((((uint32_t)L_30) != ((uint32_t)L_31)))
		{
			goto IL_015d;
		}
	}
	{
		t17  L_32 = (__this->f13);
		t17  L_33 = ((&p0)->f13);
		bool L_34 = m2777(__this, L_32, L_33, &m2777_MI);
		if (!L_34)
		{
			goto IL_015d;
		}
	}
	{
		t17  L_35 = (__this->f14);
		t17  L_36 = ((&p0)->f14);
		bool L_37 = m2777(__this, L_35, L_36, &m2777_MI);
		if (!L_37)
		{
			goto IL_015d;
		}
	}
	{
		t148 * L_38 = (__this->f0);
		t148 * L_39 = ((&p0)->f0);
		bool L_40 = m1297(NULL, L_38, L_39, &m1297_MI);
		G_B19_0 = ((int32_t)(L_40));
		goto IL_015e;
	}

IL_015d:
	{
		G_B19_0 = 0;
	}

IL_015e:
	{
		return G_B19_0;
	}
}
// Metadata Definition UnityEngine.TextGenerationSettings
extern Il2CppType t148_0_0_6;
FieldInfo t238_f0_FieldInfo = 
{
	"font", &t148_0_0_6, &t238_TI, offsetof(t238, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t132_0_0_6;
FieldInfo t238_f1_FieldInfo = 
{
	"color", &t132_0_0_6, &t238_TI, offsetof(t238, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t238_f2_FieldInfo = 
{
	"fontSize", &t44_0_0_6, &t238_TI, offsetof(t238, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_6;
FieldInfo t238_f3_FieldInfo = 
{
	"lineSpacing", &t22_0_0_6, &t238_TI, offsetof(t238, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t238_f4_FieldInfo = 
{
	"richText", &t40_0_0_6, &t238_TI, offsetof(t238, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t149_0_0_6;
FieldInfo t238_f5_FieldInfo = 
{
	"fontStyle", &t149_0_0_6, &t238_TI, offsetof(t238, f5) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t150_0_0_6;
FieldInfo t238_f6_FieldInfo = 
{
	"textAnchor", &t150_0_0_6, &t238_TI, offsetof(t238, f6) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t238_f7_FieldInfo = 
{
	"resizeTextForBestFit", &t40_0_0_6, &t238_TI, offsetof(t238, f7) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t238_f8_FieldInfo = 
{
	"resizeTextMinSize", &t44_0_0_6, &t238_TI, offsetof(t238, f8) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t238_f9_FieldInfo = 
{
	"resizeTextMaxSize", &t44_0_0_6, &t238_TI, offsetof(t238, f9) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t238_f10_FieldInfo = 
{
	"updateBounds", &t40_0_0_6, &t238_TI, offsetof(t238, f10) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t152_0_0_6;
FieldInfo t238_f11_FieldInfo = 
{
	"verticalOverflow", &t152_0_0_6, &t238_TI, offsetof(t238, f11) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t151_0_0_6;
FieldInfo t238_f12_FieldInfo = 
{
	"horizontalOverflow", &t151_0_0_6, &t238_TI, offsetof(t238, f12) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t238_f13_FieldInfo = 
{
	"generationExtents", &t17_0_0_6, &t238_TI, offsetof(t238, f13) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t238_f14_FieldInfo = 
{
	"pivot", &t17_0_0_6, &t238_TI, offsetof(t238, f14) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t238_f15_FieldInfo = 
{
	"generateOutOfBounds", &t40_0_0_6, &t238_TI, offsetof(t238, f15) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t238_FIs[] =
{
	&t238_f0_FieldInfo,
	&t238_f1_FieldInfo,
	&t238_f2_FieldInfo,
	&t238_f3_FieldInfo,
	&t238_f4_FieldInfo,
	&t238_f5_FieldInfo,
	&t238_f6_FieldInfo,
	&t238_f7_FieldInfo,
	&t238_f8_FieldInfo,
	&t238_f9_FieldInfo,
	&t238_f10_FieldInfo,
	&t238_f11_FieldInfo,
	&t238_f12_FieldInfo,
	&t238_f13_FieldInfo,
	&t238_f14_FieldInfo,
	&t238_f15_FieldInfo,
	NULL
};
extern Il2CppType t132_0_0_0;
extern Il2CppType t132_0_0_0;
static ParameterInfo t238_m2776_ParameterInfos[] = 
{
	{"left", 0, 134218822, &EmptyCustomAttributesCache, &t132_0_0_0},
	{"right", 1, 134218823, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t132_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m2776_MI = 
{
	"CompareColors", (methodPointerType)&m2776, &t238_TI, &t40_0_0_0, RuntimeInvoker_t40_t132_t132, t238_m2776_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 1074, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t238_m2777_ParameterInfos[] = 
{
	{"left", 0, 134218824, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"right", 1, 134218825, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t17_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m2777_MI = 
{
	"CompareVector2", (methodPointerType)&m2777, &t238_TI, &t40_0_0_0, RuntimeInvoker_t40_t17_t17, t238_m2777_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 1075, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t238_0_0_0;
static ParameterInfo t238_m2778_ParameterInfos[] = 
{
	{"other", 0, 134218826, &EmptyCustomAttributesCache, &t238_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t238 (MethodInfo* method, void* obj, void** args);
MethodInfo m2778_MI = 
{
	"Equals", (methodPointerType)&m2778, &t238_TI, &t40_0_0_0, RuntimeInvoker_t40_t238, t238_m2778_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1076, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t238_MIs[] =
{
	&m2776_MI,
	&m2777_MI,
	&m2778_MI,
	NULL
};
static MethodInfo* t238_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t238_1_0_0;
TypeInfo t238_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextGenerationSettings", "UnityEngine", t238_MIs, NULL, t238_FIs, NULL, &t110_TI, NULL, NULL, &t238_TI, NULL, t238_VT, &EmptyCustomAttributesCache, &t238_TI, &t238_0_0_0, &t238_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t238)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 16, 0, 0, 4, 0, 0};
#include "t520.h"
#ifndef _MSC_VER
#else
#endif
#include "t520MD.h"

extern MethodInfo m2781_MI;
extern MethodInfo m2948_MI;
extern MethodInfo m2949_MI;


 bool m2779 (t520 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m2781(NULL, ((t520 *)IsInst(p0, InitializedTypeInfo(&t520_TI))), __this, &m2781_MI);
		return L_0;
	}
}
 int32_t m2780 (t520 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		int32_t L_1 = m2948(NULL, L_0, &m2948_MI);
		return L_1;
	}
}
 bool m2781 (t29 * __this, t520 * p0, t520 * p1, MethodInfo* method){
	t29 * V_0 = {0};
	t29 * V_1 = {0};
	{
		V_0 = p0;
		V_1 = p1;
		if (V_1)
		{
			goto IL_0012;
		}
	}
	{
		if (V_0)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		if (V_1)
		{
			goto IL_0029;
		}
	}
	{
		t35 L_0 = (p0->f0);
		bool L_1 = m2949(NULL, L_0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2949_MI);
		return L_1;
	}

IL_0029:
	{
		if (V_0)
		{
			goto IL_0040;
		}
	}
	{
		t35 L_2 = (p1->f0);
		bool L_3 = m2949(NULL, L_2, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2949_MI);
		return L_3;
	}

IL_0040:
	{
		t35 L_4 = (p0->f0);
		t35 L_5 = (p1->f0);
		bool L_6 = m2949(NULL, L_4, L_5, &m2949_MI);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
void t520_marshal(const t520& unmarshaled, t520_marshaled& marshaled)
{
	marshaled.f0 = unmarshaled.f0;
}
void t520_marshal_back(const t520_marshaled& marshaled, t520& unmarshaled)
{
	unmarshaled.f0 = marshaled.f0;
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
void t520_marshal_cleanup(t520_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.TrackedReference
extern Il2CppType t35_0_0_3;
FieldInfo t520_f0_FieldInfo = 
{
	"m_Ptr", &t35_0_0_3, &t520_TI, offsetof(t520, f0), &EmptyCustomAttributesCache};
static FieldInfo* t520_FIs[] =
{
	&t520_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
static ParameterInfo t520_m2779_ParameterInfos[] = 
{
	{"o", 0, 134218827, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2779_MI = 
{
	"Equals", (methodPointerType)&m2779, &t520_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t520_m2779_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 1077, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2780_MI = 
{
	"GetHashCode", (methodPointerType)&m2780, &t520_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 1078, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t520_0_0_0;
extern Il2CppType t520_0_0_0;
extern Il2CppType t520_0_0_0;
static ParameterInfo t520_m2781_ParameterInfos[] = 
{
	{"x", 0, 134218828, &EmptyCustomAttributesCache, &t520_0_0_0},
	{"y", 1, 134218829, &EmptyCustomAttributesCache, &t520_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2781_MI = 
{
	"op_Equality", (methodPointerType)&m2781, &t520_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t520_m2781_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 1079, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t520_MIs[] =
{
	&m2779_MI,
	&m2780_MI,
	&m2781_MI,
	NULL
};
static MethodInfo* t520_VT[] =
{
	&m2779_MI,
	&m46_MI,
	&m2780_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t520_1_0_0;
struct t520;
TypeInfo t520_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TrackedReference", "UnityEngine", t520_MIs, NULL, t520_FIs, NULL, &t29_TI, NULL, NULL, &t520_TI, NULL, t520_VT, &EmptyCustomAttributesCache, &t520_TI, &t520_0_0_0, &t520_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t520_marshal, (methodPointerType)t520_marshal_back, (methodPointerType)t520_marshal_cleanup, sizeof (t520), 0, sizeof(t520_marshaled), 0, 0, -1, 1048585, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 4, 0, 0};
#include "t554.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t554_TI;
#include "t554MD.h"



// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern Il2CppType t44_0_0_1542;
FieldInfo t554_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t554_TI, offsetof(t554, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f2_FieldInfo = 
{
	"EventDefined", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f3_FieldInfo = 
{
	"Void", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f4_FieldInfo = 
{
	"Object", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f5_FieldInfo = 
{
	"Int", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f6_FieldInfo = 
{
	"Float", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f7_FieldInfo = 
{
	"String", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t554_0_0_32854;
FieldInfo t554_f8_FieldInfo = 
{
	"Bool", &t554_0_0_32854, &t554_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t554_FIs[] =
{
	&t554_f1_FieldInfo,
	&t554_f2_FieldInfo,
	&t554_f3_FieldInfo,
	&t554_f4_FieldInfo,
	&t554_f5_FieldInfo,
	&t554_f6_FieldInfo,
	&t554_f7_FieldInfo,
	&t554_f8_FieldInfo,
	NULL
};
static const int32_t t554_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t554_f2_DefaultValue = 
{
	&t554_f2_FieldInfo, { (char*)&t554_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t554_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t554_f3_DefaultValue = 
{
	&t554_f3_FieldInfo, { (char*)&t554_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t554_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t554_f4_DefaultValue = 
{
	&t554_f4_FieldInfo, { (char*)&t554_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t554_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t554_f5_DefaultValue = 
{
	&t554_f5_FieldInfo, { (char*)&t554_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t554_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t554_f6_DefaultValue = 
{
	&t554_f6_FieldInfo, { (char*)&t554_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t554_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t554_f7_DefaultValue = 
{
	&t554_f7_FieldInfo, { (char*)&t554_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t554_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t554_f8_DefaultValue = 
{
	&t554_f8_FieldInfo, { (char*)&t554_f8_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t554_FDVs[] = 
{
	&t554_f2_DefaultValue,
	&t554_f3_DefaultValue,
	&t554_f4_DefaultValue,
	&t554_f5_DefaultValue,
	&t554_f6_DefaultValue,
	&t554_f7_DefaultValue,
	&t554_f8_DefaultValue,
	NULL
};
static MethodInfo* t554_MIs[] =
{
	NULL
};
static MethodInfo* t554_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t554_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t554_0_0_0;
extern Il2CppType t554_1_0_0;
TypeInfo t554_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PersistentListenerMode", "UnityEngine.Events", t554_MIs, NULL, t554_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t554_VT, &EmptyCustomAttributesCache, &t44_TI, &t554_0_0_0, &t554_1_0_0, t554_IOs, NULL, NULL, t554_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t554)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 8, 0, 0, 23, 0, 3};
#include "t555.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t555_TI;
#include "t555MD.h"



extern MethodInfo m2782_MI;
 void m2782 (t555 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m2783_MI;
 t41 * m2783 (t555 * __this, MethodInfo* method){
	{
		t41 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2784_MI;
 t7* m2784 (t555 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2785_MI;
 int32_t m2785 (t555 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2786_MI;
 float m2786 (t555 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m2787_MI;
 t7* m2787 (t555 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m2788_MI;
 bool m2788 (t555 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f5);
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.ArgumentCache
extern Il2CppType t41_0_0_1;
extern CustomAttributesCache t555__CustomAttributeCache_m_ObjectArgument;
FieldInfo t555_f0_FieldInfo = 
{
	"m_ObjectArgument", &t41_0_0_1, &t555_TI, offsetof(t555, f0), &t555__CustomAttributeCache_m_ObjectArgument};
extern Il2CppType t7_0_0_1;
extern CustomAttributesCache t555__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName;
FieldInfo t555_f1_FieldInfo = 
{
	"m_ObjectArgumentAssemblyTypeName", &t7_0_0_1, &t555_TI, offsetof(t555, f1), &t555__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t555__CustomAttributeCache_m_IntArgument;
FieldInfo t555_f2_FieldInfo = 
{
	"m_IntArgument", &t44_0_0_1, &t555_TI, offsetof(t555, f2), &t555__CustomAttributeCache_m_IntArgument};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t555__CustomAttributeCache_m_FloatArgument;
FieldInfo t555_f3_FieldInfo = 
{
	"m_FloatArgument", &t22_0_0_1, &t555_TI, offsetof(t555, f3), &t555__CustomAttributeCache_m_FloatArgument};
extern Il2CppType t7_0_0_1;
extern CustomAttributesCache t555__CustomAttributeCache_m_StringArgument;
FieldInfo t555_f4_FieldInfo = 
{
	"m_StringArgument", &t7_0_0_1, &t555_TI, offsetof(t555, f4), &t555__CustomAttributeCache_m_StringArgument};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t555__CustomAttributeCache_m_BoolArgument;
FieldInfo t555_f5_FieldInfo = 
{
	"m_BoolArgument", &t40_0_0_1, &t555_TI, offsetof(t555, f5), &t555__CustomAttributeCache_m_BoolArgument};
static FieldInfo* t555_FIs[] =
{
	&t555_f0_FieldInfo,
	&t555_f1_FieldInfo,
	&t555_f2_FieldInfo,
	&t555_f3_FieldInfo,
	&t555_f4_FieldInfo,
	&t555_f5_FieldInfo,
	NULL
};
static PropertyInfo t555____unityObjectArgument_PropertyInfo = 
{
	&t555_TI, "unityObjectArgument", &m2783_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t555____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&t555_TI, "unityObjectArgumentAssemblyTypeName", &m2784_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t555____intArgument_PropertyInfo = 
{
	&t555_TI, "intArgument", &m2785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t555____floatArgument_PropertyInfo = 
{
	&t555_TI, "floatArgument", &m2786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t555____stringArgument_PropertyInfo = 
{
	&t555_TI, "stringArgument", &m2787_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t555____boolArgument_PropertyInfo = 
{
	&t555_TI, "boolArgument", &m2788_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t555_PIs[] =
{
	&t555____unityObjectArgument_PropertyInfo,
	&t555____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&t555____intArgument_PropertyInfo,
	&t555____floatArgument_PropertyInfo,
	&t555____stringArgument_PropertyInfo,
	&t555____boolArgument_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2782_MI = 
{
	".ctor", (methodPointerType)&m2782, &t555_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1080, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2783_MI = 
{
	"get_unityObjectArgument", (methodPointerType)&m2783, &t555_TI, &t41_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1081, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2784_MI = 
{
	"get_unityObjectArgumentAssemblyTypeName", (methodPointerType)&m2784, &t555_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1082, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2785_MI = 
{
	"get_intArgument", (methodPointerType)&m2785, &t555_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1083, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2786_MI = 
{
	"get_floatArgument", (methodPointerType)&m2786, &t555_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1084, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2787_MI = 
{
	"get_stringArgument", (methodPointerType)&m2787, &t555_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1085, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2788_MI = 
{
	"get_boolArgument", (methodPointerType)&m2788, &t555_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1086, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t555_MIs[] =
{
	&m2782_MI,
	&m2783_MI,
	&m2784_MI,
	&m2785_MI,
	&m2786_MI,
	&m2787_MI,
	&m2788_MI,
	NULL
};
static MethodInfo* t555_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t300_TI;
#include "t300.h"
#include "t300MD.h"
extern MethodInfo m1319_MI;
extern TypeInfo t299_TI;
#include "t299.h"
#include "t299MD.h"
extern MethodInfo m1318_MI;
void t555_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t555_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t555_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t555_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t555_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t555_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t555__CustomAttributeCache_m_ObjectArgument = {
2,
NULL,
&t555_CustomAttributesCacheGenerator_m_ObjectArgument
};
CustomAttributesCache t555__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName = {
2,
NULL,
&t555_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName
};
CustomAttributesCache t555__CustomAttributeCache_m_IntArgument = {
2,
NULL,
&t555_CustomAttributesCacheGenerator_m_IntArgument
};
CustomAttributesCache t555__CustomAttributeCache_m_FloatArgument = {
2,
NULL,
&t555_CustomAttributesCacheGenerator_m_FloatArgument
};
CustomAttributesCache t555__CustomAttributeCache_m_StringArgument = {
2,
NULL,
&t555_CustomAttributesCacheGenerator_m_StringArgument
};
CustomAttributesCache t555__CustomAttributeCache_m_BoolArgument = {
1,
NULL,
&t555_CustomAttributesCacheGenerator_m_BoolArgument
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t555_0_0_0;
extern Il2CppType t555_1_0_0;
struct t555;
extern CustomAttributesCache t555__CustomAttributeCache_m_ObjectArgument;
extern CustomAttributesCache t555__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName;
extern CustomAttributesCache t555__CustomAttributeCache_m_IntArgument;
extern CustomAttributesCache t555__CustomAttributeCache_m_FloatArgument;
extern CustomAttributesCache t555__CustomAttributeCache_m_StringArgument;
extern CustomAttributesCache t555__CustomAttributeCache_m_BoolArgument;
TypeInfo t555_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ArgumentCache", "UnityEngine.Events", t555_MIs, t555_PIs, t555_FIs, NULL, &t29_TI, NULL, NULL, &t555_TI, NULL, t555_VT, &EmptyCustomAttributesCache, &t555_TI, &t555_0_0_0, &t555_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t555), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 6, 6, 0, 0, 4, 0, 0};
#include "t556.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t556_TI;
#include "t556MD.h"

#include "t557.h"
#include "t338.h"
extern TypeInfo t338_TI;
#include "t338MD.h"
extern MethodInfo m2950_MI;
extern MethodInfo m2951_MI;
extern MethodInfo m2952_MI;
extern MethodInfo m2953_MI;


extern MethodInfo m2789_MI;
 void m2789 (t556 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m2790_MI;
 void m2790 (t556 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0017;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral189, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0017:
	{
		if (p1)
		{
			goto IL_0028;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral190, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0028:
	{
		return;
	}
}
extern MethodInfo m2791_MI;
 bool m2791 (t29 * __this, t353 * p0, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t557 * L_0 = m2951(p0, &m2951_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m2952_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		t29 * L_2 = m2953(p0, &m2953_MI);
		G_B3_0 = ((((int32_t)((((t29 *)L_2) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2789_MI = 
{
	".ctor", (methodPointerType)&m2789, &t556_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1087, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t556_m2790_ParameterInfos[] = 
{
	{"target", 0, 134218830, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"function", 1, 134218831, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2790_MI = 
{
	".ctor", (methodPointerType)&m2790, &t556_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t556_m2790_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 1088, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t556_m2954_ParameterInfos[] = 
{
	{"args", 0, 134218832, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2954_MI = 
{
	"Invoke", NULL, &t556_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t556_m2954_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, false, 1089, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t556_m2955_ParameterInfos[] = 
{
	{"arg", 0, 134218833, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m2955_IGC;
extern TypeInfo m2955_gp_T_0_TI;
Il2CppGenericParamFull m2955_gp_T_0_TI_GenericParamFull = { { &m2955_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m2955_IGPA[1] = 
{
	&m2955_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m2955_MI;
Il2CppGenericContainer m2955_IGC = { { NULL, NULL }, NULL, &m2955_MI, 1, 1, m2955_IGPA };
extern Il2CppType m2955_gp_0_0_0_0;
static Il2CppRGCTXDefinition m2955_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &m2955_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_TYPE, &m2955_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m2955_MI = 
{
	"ThrowOnInvalidArg", NULL, &t556_TI, &t21_0_0_0, NULL, t556_m2955_ParameterInfos, &EmptyCustomAttributesCache, 148, 0, 255, 1, true, false, 1090, m2955_RGCTXData, (methodPointerType)NULL, &m2955_IGC};
extern Il2CppType t353_0_0_0;
extern Il2CppType t353_0_0_0;
static ParameterInfo t556_m2791_ParameterInfos[] = 
{
	{"delegate", 0, 134218834, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2791_MI = 
{
	"AllowInvoke", (methodPointerType)&m2791, &t556_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t556_m2791_ParameterInfos, &EmptyCustomAttributesCache, 148, 0, 255, 1, false, false, 1091, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t556_m2956_ParameterInfos[] = 
{
	{"targetObj", 0, 134218835, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218836, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2956_MI = 
{
	"Find", NULL, &t556_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t556_m2956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, false, 1092, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t556_MIs[] =
{
	&m2789_MI,
	&m2790_MI,
	&m2954_MI,
	&m2955_MI,
	&m2791_MI,
	&m2956_MI,
	NULL
};
static MethodInfo* t556_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	NULL,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_1_0_0;
struct t556;
TypeInfo t556_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "BaseInvokableCall", "UnityEngine.Events", t556_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t556_TI, NULL, t556_VT, &EmptyCustomAttributesCache, &t556_TI, &t556_0_0_0, &t556_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t556), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 0, 0, 0, 0, 6, 0, 0};
#include "t558.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t558_TI;
#include "t558MD.h"

#include "t34.h"
extern TypeInfo t34_TI;
#include "t34MD.h"
extern Il2CppType t34_0_0_0;
extern MethodInfo m2957_MI;
extern MethodInfo m1564_MI;


extern MethodInfo m2792_MI;
 void m2792 (t558 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m2790(__this, p0, p1, &m2790_MI);
		t34 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t34_0_0_0), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t34 *)IsInst(L_2, InitializedTypeInfo(&t34_TI))), &m1597_MI);
		__this->f0 = ((t34 *)Castclass(L_3, InitializedTypeInfo(&t34_TI)));
		return;
	}
}
extern MethodInfo m2793_MI;
 void m2793 (t558 * __this, t34 * p0, MethodInfo* method){
	{
		m2789(__this, &m2789_MI);
		t34 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t34 *)Castclass(L_1, InitializedTypeInfo(&t34_TI)));
		return;
	}
}
extern MethodInfo m2794_MI;
 void m2794 (t558 * __this, t316* p0, MethodInfo* method){
	{
		t34 * L_0 = (__this->f0);
		bool L_1 = m2791(NULL, L_0, &m2791_MI);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		t34 * L_2 = (__this->f0);
		VirtActionInvoker0::Invoke(&m1564_MI, L_2);
	}

IL_001b:
	{
		return;
	}
}
extern MethodInfo m2795_MI;
 bool m2795 (t558 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t34 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t34 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall
extern Il2CppType t34_0_0_1;
FieldInfo t558_f0_FieldInfo = 
{
	"Delegate", &t34_0_0_1, &t558_TI, offsetof(t558, f0), &EmptyCustomAttributesCache};
static FieldInfo* t558_FIs[] =
{
	&t558_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t558_m2792_ParameterInfos[] = 
{
	{"target", 0, 134218837, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218838, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2792_MI = 
{
	".ctor", (methodPointerType)&m2792, &t558_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t558_m2792_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1093, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t34_0_0_0;
static ParameterInfo t558_m2793_ParameterInfos[] = 
{
	{"action", 0, 134218839, &EmptyCustomAttributesCache, &t34_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2793_MI = 
{
	".ctor", (methodPointerType)&m2793, &t558_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t558_m2793_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1094, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t558_m2794_ParameterInfos[] = 
{
	{"args", 0, 134218840, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2794_MI = 
{
	"Invoke", (methodPointerType)&m2794, &t558_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t558_m2794_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 1095, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t558_m2795_ParameterInfos[] = 
{
	{"targetObj", 0, 134218841, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218842, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2795_MI = 
{
	"Find", (methodPointerType)&m2795, &t558_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t558_m2795_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 1096, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t558_MIs[] =
{
	&m2792_MI,
	&m2793_MI,
	&m2794_MI,
	&m2795_MI,
	NULL
};
static MethodInfo* t558_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2794_MI,
	&m2795_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t558_0_0_0;
extern Il2CppType t558_1_0_0;
struct t558;
TypeInfo t558_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall", "UnityEngine.Events", t558_MIs, NULL, t558_FIs, NULL, &t556_TI, NULL, NULL, &t558_TI, NULL, t558_VT, &EmptyCustomAttributesCache, &t558_TI, &t558_0_0_0, &t558_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t558), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#include "t559.h"
extern Il2CppGenericContainer t559_IGC;
extern TypeInfo t559_gp_T1_0_TI;
Il2CppGenericParamFull t559_gp_T1_0_TI_GenericParamFull = { { &t559_IGC, 0}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* t559_IGPA[1] = 
{
	&t559_gp_T1_0_TI_GenericParamFull,
};
extern TypeInfo t559_TI;
Il2CppGenericContainer t559_IGC = { { NULL, NULL }, NULL, &t559_TI, 1, 0, t559_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t559_m2958_ParameterInfos[] = 
{
	{"target", 0, 134218843, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218844, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2958_MI = 
{
	".ctor", NULL, &t559_TI, &t21_0_0_0, NULL, t559_m2958_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1097, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t642_0_0_0;
extern Il2CppType t642_0_0_0;
static ParameterInfo t559_m2959_ParameterInfos[] = 
{
	{"callback", 0, 134218845, &EmptyCustomAttributesCache, &t642_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2959_MI = 
{
	".ctor", NULL, &t559_TI, &t21_0_0_0, NULL, t559_m2959_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1098, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t559_m2960_ParameterInfos[] = 
{
	{"args", 0, 134218846, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2960_MI = 
{
	"Invoke", NULL, &t559_TI, &t21_0_0_0, NULL, t559_m2960_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 1099, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t559_m2961_ParameterInfos[] = 
{
	{"targetObj", 0, 134218847, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218848, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m2961_MI = 
{
	"Find", NULL, &t559_TI, &t40_0_0_0, NULL, t559_m2961_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 1100, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t559_MIs[] =
{
	&m2958_MI,
	&m2959_MI,
	&m2960_MI,
	&m2961_MI,
	NULL
};
extern Il2CppType t642_0_0_1;
FieldInfo t559_f0_FieldInfo = 
{
	"Delegate", &t642_0_0_1, &t559_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t559_FIs[] =
{
	&t559_f0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t559_0_0_0;
extern Il2CppType t559_1_0_0;
struct t559;
TypeInfo t559_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t559_MIs, NULL, t559_FIs, NULL, NULL, NULL, NULL, &t559_TI, NULL, NULL, NULL, NULL, &t559_0_0_0, &t559_1_0_0, NULL, NULL, &t559_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 0, 0, 0};
#include "t560.h"
extern Il2CppGenericContainer t560_IGC;
extern TypeInfo t560_gp_T1_0_TI;
Il2CppGenericParamFull t560_gp_T1_0_TI_GenericParamFull = { { &t560_IGC, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t560_gp_T2_1_TI;
Il2CppGenericParamFull t560_gp_T2_1_TI_GenericParamFull = { { &t560_IGC, 1}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* t560_IGPA[2] = 
{
	&t560_gp_T1_0_TI_GenericParamFull,
	&t560_gp_T2_1_TI_GenericParamFull,
};
extern TypeInfo t560_TI;
Il2CppGenericContainer t560_IGC = { { NULL, NULL }, NULL, &t560_TI, 2, 0, t560_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t560_m2962_ParameterInfos[] = 
{
	{"target", 0, 134218849, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218850, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2962_MI = 
{
	".ctor", NULL, &t560_TI, &t21_0_0_0, NULL, t560_m2962_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1101, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t560_m2963_ParameterInfos[] = 
{
	{"args", 0, 134218851, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2963_MI = 
{
	"Invoke", NULL, &t560_TI, &t21_0_0_0, NULL, t560_m2963_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 1102, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t560_m2964_ParameterInfos[] = 
{
	{"targetObj", 0, 134218852, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218853, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m2964_MI = 
{
	"Find", NULL, &t560_TI, &t40_0_0_0, NULL, t560_m2964_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 1103, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t560_MIs[] =
{
	&m2962_MI,
	&m2963_MI,
	&m2964_MI,
	NULL
};
extern Il2CppType t645_0_0_1;
FieldInfo t560_f0_FieldInfo = 
{
	"Delegate", &t645_0_0_1, &t560_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t560_FIs[] =
{
	&t560_f0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t560_0_0_0;
extern Il2CppType t560_1_0_0;
struct t560;
TypeInfo t560_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`2", "UnityEngine.Events", t560_MIs, NULL, t560_FIs, NULL, NULL, NULL, NULL, &t560_TI, NULL, NULL, NULL, NULL, &t560_0_0_0, &t560_1_0_0, NULL, NULL, &t560_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 0, 0, 0};
#include "t561.h"
extern Il2CppGenericContainer t561_IGC;
extern TypeInfo t561_gp_T1_0_TI;
Il2CppGenericParamFull t561_gp_T1_0_TI_GenericParamFull = { { &t561_IGC, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t561_gp_T2_1_TI;
Il2CppGenericParamFull t561_gp_T2_1_TI_GenericParamFull = { { &t561_IGC, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo t561_gp_T3_2_TI;
Il2CppGenericParamFull t561_gp_T3_2_TI_GenericParamFull = { { &t561_IGC, 2}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* t561_IGPA[3] = 
{
	&t561_gp_T1_0_TI_GenericParamFull,
	&t561_gp_T2_1_TI_GenericParamFull,
	&t561_gp_T3_2_TI_GenericParamFull,
};
extern TypeInfo t561_TI;
Il2CppGenericContainer t561_IGC = { { NULL, NULL }, NULL, &t561_TI, 3, 0, t561_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t561_m2965_ParameterInfos[] = 
{
	{"target", 0, 134218854, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218855, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2965_MI = 
{
	".ctor", NULL, &t561_TI, &t21_0_0_0, NULL, t561_m2965_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1104, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t561_m2966_ParameterInfos[] = 
{
	{"args", 0, 134218856, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2966_MI = 
{
	"Invoke", NULL, &t561_TI, &t21_0_0_0, NULL, t561_m2966_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 1105, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t561_m2967_ParameterInfos[] = 
{
	{"targetObj", 0, 134218857, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218858, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m2967_MI = 
{
	"Find", NULL, &t561_TI, &t40_0_0_0, NULL, t561_m2967_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 1106, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t561_MIs[] =
{
	&m2965_MI,
	&m2966_MI,
	&m2967_MI,
	NULL
};
extern Il2CppType t649_0_0_1;
FieldInfo t561_f0_FieldInfo = 
{
	"Delegate", &t649_0_0_1, &t561_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t561_FIs[] =
{
	&t561_f0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t561_0_0_0;
extern Il2CppType t561_1_0_0;
struct t561;
TypeInfo t561_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`3", "UnityEngine.Events", t561_MIs, NULL, t561_FIs, NULL, NULL, NULL, NULL, &t561_TI, NULL, NULL, NULL, NULL, &t561_0_0_0, &t561_1_0_0, NULL, NULL, &t561_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 0, 0, 0};
#include "t562.h"
extern Il2CppGenericContainer t562_IGC;
extern TypeInfo t562_gp_T1_0_TI;
Il2CppGenericParamFull t562_gp_T1_0_TI_GenericParamFull = { { &t562_IGC, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t562_gp_T2_1_TI;
Il2CppGenericParamFull t562_gp_T2_1_TI_GenericParamFull = { { &t562_IGC, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo t562_gp_T3_2_TI;
Il2CppGenericParamFull t562_gp_T3_2_TI_GenericParamFull = { { &t562_IGC, 2}, {NULL, "T3", 0, 0, NULL} };
extern TypeInfo t562_gp_T4_3_TI;
Il2CppGenericParamFull t562_gp_T4_3_TI_GenericParamFull = { { &t562_IGC, 3}, {NULL, "T4", 0, 0, NULL} };
static Il2CppGenericParamFull* t562_IGPA[4] = 
{
	&t562_gp_T1_0_TI_GenericParamFull,
	&t562_gp_T2_1_TI_GenericParamFull,
	&t562_gp_T3_2_TI_GenericParamFull,
	&t562_gp_T4_3_TI_GenericParamFull,
};
extern TypeInfo t562_TI;
Il2CppGenericContainer t562_IGC = { { NULL, NULL }, NULL, &t562_TI, 4, 0, t562_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t562_m2968_ParameterInfos[] = 
{
	{"target", 0, 134218859, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218860, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2968_MI = 
{
	".ctor", NULL, &t562_TI, &t21_0_0_0, NULL, t562_m2968_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1107, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t562_m2969_ParameterInfos[] = 
{
	{"args", 0, 134218861, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2969_MI = 
{
	"Invoke", NULL, &t562_TI, &t21_0_0_0, NULL, t562_m2969_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 1108, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t562_m2970_ParameterInfos[] = 
{
	{"targetObj", 0, 134218862, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218863, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m2970_MI = 
{
	"Find", NULL, &t562_TI, &t40_0_0_0, NULL, t562_m2970_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 1109, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t562_MIs[] =
{
	&m2968_MI,
	&m2969_MI,
	&m2970_MI,
	NULL
};
extern Il2CppType t654_0_0_1;
FieldInfo t562_f0_FieldInfo = 
{
	"Delegate", &t654_0_0_1, &t562_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t562_FIs[] =
{
	&t562_f0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t562_0_0_0;
extern Il2CppType t562_1_0_0;
struct t562;
TypeInfo t562_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`4", "UnityEngine.Events", t562_MIs, NULL, t562_FIs, NULL, NULL, NULL, NULL, &t562_TI, NULL, NULL, NULL, NULL, &t562_0_0_0, &t562_1_0_0, NULL, NULL, &t562_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 0, 0, 0};
#include "t563.h"
extern Il2CppGenericContainer t563_IGC;
extern TypeInfo t563_gp_T_0_TI;
Il2CppGenericParamFull t563_gp_T_0_TI_GenericParamFull = { { &t563_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* t563_IGPA[1] = 
{
	&t563_gp_T_0_TI_GenericParamFull,
};
extern TypeInfo t563_TI;
Il2CppGenericContainer t563_IGC = { { NULL, NULL }, NULL, &t563_TI, 1, 0, t563_IGPA };
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t563_gp_0_0_0_0;
extern Il2CppType t563_gp_0_0_0_0;
static ParameterInfo t563_m2971_ParameterInfos[] = 
{
	{"target", 0, 134218864, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134218865, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134218866, &EmptyCustomAttributesCache, &t563_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2971_MI = 
{
	".ctor", NULL, &t563_TI, &t21_0_0_0, NULL, t563_m2971_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 1110, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t563_m2972_ParameterInfos[] = 
{
	{"args", 0, 134218867, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2972_MI = 
{
	"Invoke", NULL, &t563_TI, &t21_0_0_0, NULL, t563_m2972_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 1111, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t563_MIs[] =
{
	&m2971_MI,
	&m2972_MI,
	NULL
};
extern Il2CppType t316_0_0_33;
FieldInfo t563_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t563_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t563_FIs[] =
{
	&t563_f1_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t563_0_0_0;
extern Il2CppType t563_1_0_0;
struct t563;
TypeInfo t563_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t563_MIs, NULL, t563_FIs, NULL, NULL, NULL, NULL, &t563_TI, NULL, NULL, NULL, NULL, &t563_0_0_0, &t563_1_0_0, NULL, NULL, &t563_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 0, 0, 0};
#include "t564.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t564_TI;
#include "t564MD.h"



// Metadata Definition UnityEngine.Events.UnityEventCallState
extern Il2CppType t44_0_0_1542;
FieldInfo t564_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t564_TI, offsetof(t564, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t564_0_0_32854;
FieldInfo t564_f2_FieldInfo = 
{
	"Off", &t564_0_0_32854, &t564_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t564_0_0_32854;
FieldInfo t564_f3_FieldInfo = 
{
	"EditorAndRuntime", &t564_0_0_32854, &t564_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t564_0_0_32854;
FieldInfo t564_f4_FieldInfo = 
{
	"RuntimeOnly", &t564_0_0_32854, &t564_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t564_FIs[] =
{
	&t564_f1_FieldInfo,
	&t564_f2_FieldInfo,
	&t564_f3_FieldInfo,
	&t564_f4_FieldInfo,
	NULL
};
static const int32_t t564_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t564_f2_DefaultValue = 
{
	&t564_f2_FieldInfo, { (char*)&t564_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t564_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t564_f3_DefaultValue = 
{
	&t564_f3_FieldInfo, { (char*)&t564_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t564_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t564_f4_DefaultValue = 
{
	&t564_f4_FieldInfo, { (char*)&t564_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t564_FDVs[] = 
{
	&t564_f2_DefaultValue,
	&t564_f3_DefaultValue,
	&t564_f4_DefaultValue,
	NULL
};
static MethodInfo* t564_MIs[] =
{
	NULL
};
static MethodInfo* t564_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t564_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t564_0_0_0;
extern Il2CppType t564_1_0_0;
TypeInfo t564_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEventCallState", "UnityEngine.Events", t564_MIs, NULL, t564_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t564_VT, &EmptyCustomAttributesCache, &t44_TI, &t564_0_0_0, &t564_1_0_0, t564_IOs, NULL, NULL, t564_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t564)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t565.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t565_TI;
#include "t565MD.h"

#include "t566.h"
#include "t656.h"
#include "t657.h"
#include "t658.h"
#include "t659.h"
#include "t660.h"
extern TypeInfo t566_TI;
extern TypeInfo t557_TI;
extern TypeInfo t656_TI;
extern TypeInfo t657_TI;
extern TypeInfo t658_TI;
extern TypeInfo t659_TI;
extern TypeInfo t660_TI;
#include "t566MD.h"
#include "t656MD.h"
#include "t657MD.h"
#include "t658MD.h"
#include "t659MD.h"
#include "t660MD.h"
extern Il2CppType t563_0_0_0;
extern MethodInfo m2797_MI;
extern MethodInfo m2798_MI;
extern MethodInfo m2813_MI;
extern MethodInfo m2973_MI;
extern MethodInfo m2803_MI;
extern MethodInfo m2974_MI;
extern MethodInfo m2975_MI;
extern MethodInfo m2976_MI;
extern MethodInfo m2977_MI;
extern MethodInfo m2978_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m2980_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2982_MI;


extern MethodInfo m2796_MI;
 void m2796 (t565 * __this, MethodInfo* method){
	{
		t555 * L_0 = (t555 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t555_TI));
		m2782(L_0, &m2782_MI);
		__this->f3 = L_0;
		__this->f4 = 2;
		m1331(__this, &m1331_MI);
		return;
	}
}
 t41 * m2797 (t565 * __this, MethodInfo* method){
	{
		t41 * L_0 = (__this->f0);
		return L_0;
	}
}
 t7* m2798 (t565 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m2799_MI;
 int32_t m2799 (t565 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m2800_MI;
 t555 * m2800 (t565 * __this, MethodInfo* method){
	{
		t555 * L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m2801_MI;
 bool m2801 (t565 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t41 * L_0 = m2797(__this, &m2797_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		t7* L_2 = m2798(__this, &m2798_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_3 = m1772(NULL, L_2, &m1772_MI);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern MethodInfo m2802_MI;
 t556 * m2802 (t565 * __this, t566 * p0, MethodInfo* method){
	t557 * V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = (__this->f4);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		if (p0)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (t556 *)NULL;
	}

IL_0013:
	{
		t557 * L_1 = m2813(p0, __this, &m2813_MI);
		V_0 = L_1;
		if (V_0)
		{
			goto IL_0023;
		}
	}
	{
		return (t556 *)NULL;
	}

IL_0023:
	{
		int32_t L_2 = (__this->f2);
		V_1 = L_2;
		if (V_1 == 0)
		{
			goto IL_0051;
		}
		if (V_1 == 1)
		{
			goto IL_00d2;
		}
		if (V_1 == 2)
		{
			goto IL_005f;
		}
		if (V_1 == 3)
		{
			goto IL_008a;
		}
		if (V_1 == 4)
		{
			goto IL_0072;
		}
		if (V_1 == 5)
		{
			goto IL_00a2;
		}
		if (V_1 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		t41 * L_3 = m2797(__this, &m2797_MI);
		t556 * L_4 = (t556 *)VirtFuncInvoker2< t556 *, t29 *, t557 * >::Invoke(&m2973_MI, p0, L_3, V_0);
		return L_4;
	}

IL_005f:
	{
		t41 * L_5 = m2797(__this, &m2797_MI);
		t555 * L_6 = (__this->f3);
		t556 * L_7 = m2803(NULL, L_5, V_0, L_6, &m2803_MI);
		return L_7;
	}

IL_0072:
	{
		t41 * L_8 = m2797(__this, &m2797_MI);
		t555 * L_9 = (__this->f3);
		float L_10 = m2786(L_9, &m2786_MI);
		t656 * L_11 = (t656 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t656_TI));
		m2974(L_11, L_8, V_0, L_10, &m2974_MI);
		return L_11;
	}

IL_008a:
	{
		t41 * L_12 = m2797(__this, &m2797_MI);
		t555 * L_13 = (__this->f3);
		int32_t L_14 = m2785(L_13, &m2785_MI);
		t657 * L_15 = (t657 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t657_TI));
		m2975(L_15, L_12, V_0, L_14, &m2975_MI);
		return L_15;
	}

IL_00a2:
	{
		t41 * L_16 = m2797(__this, &m2797_MI);
		t555 * L_17 = (__this->f3);
		t7* L_18 = m2787(L_17, &m2787_MI);
		t658 * L_19 = (t658 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t658_TI));
		m2976(L_19, L_16, V_0, L_18, &m2976_MI);
		return L_19;
	}

IL_00ba:
	{
		t41 * L_20 = m2797(__this, &m2797_MI);
		t555 * L_21 = (__this->f3);
		bool L_22 = m2788(L_21, &m2788_MI);
		t659 * L_23 = (t659 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t659_TI));
		m2977(L_23, L_20, V_0, L_22, &m2977_MI);
		return L_23;
	}

IL_00d2:
	{
		t41 * L_24 = m2797(__this, &m2797_MI);
		t558 * L_25 = (t558 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t558_TI));
		m2792(L_25, L_24, V_0, &m2792_MI);
		return L_25;
	}

IL_00df:
	{
		return (t556 *)NULL;
	}
}
 t556 * m2803 (t29 * __this, t41 * p0, t557 * p1, t555 * p2, MethodInfo* method){
	t42 * V_0 = {0};
	t42 * V_1 = {0};
	t42 * V_2 = {0};
	t660 * V_3 = {0};
	t41 * V_4 = {0};
	t42 * G_B3_0 = {0};
	t42 * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t41_0_0_0), &m1554_MI);
		V_0 = L_0;
		t7* L_1 = m2784(p2, &m2784_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1772(NULL, L_1, &m1772_MI);
		if (L_2)
		{
			goto IL_0039;
		}
	}
	{
		t7* L_3 = m2784(p2, &m2784_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m2978(NULL, L_3, 0, &m2978_MI);
		t42 * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_6 = m1554(NULL, LoadTypeToken(&t41_0_0_0), &m1554_MI);
		G_B3_0 = L_6;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_7 = m1554(NULL, LoadTypeToken(&t563_0_0_0), &m1554_MI);
		V_1 = L_7;
		t537* L_8 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		ArrayElementTypeCheck (L_8, V_0);
		*((t42 **)(t42 **)SZArrayLdElema(L_8, 0)) = (t42 *)V_0;
		t42 * L_9 = (t42 *)VirtFuncInvoker1< t42 *, t537* >::Invoke(&m2979_MI, V_1, L_8);
		V_2 = L_9;
		t537* L_10 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 3));
		t42 * L_11 = m1554(NULL, LoadTypeToken(&t41_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_10, L_11);
		*((t42 **)(t42 **)SZArrayLdElema(L_10, 0)) = (t42 *)L_11;
		t537* L_12 = L_10;
		t42 * L_13 = m1554(NULL, LoadTypeToken(&t557_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_12, L_13);
		*((t42 **)(t42 **)SZArrayLdElema(L_12, 1)) = (t42 *)L_13;
		t537* L_14 = L_12;
		ArrayElementTypeCheck (L_14, V_0);
		*((t42 **)(t42 **)SZArrayLdElema(L_14, 2)) = (t42 *)V_0;
		t660 * L_15 = (t660 *)VirtFuncInvoker1< t660 *, t537* >::Invoke(&m2980_MI, V_2, L_14);
		V_3 = L_15;
		t41 * L_16 = m2783(p2, &m2783_MI);
		V_4 = L_16;
		bool L_17 = m1300(NULL, V_4, (t41 *)NULL, &m1300_MI);
		if (!L_17)
		{
			goto IL_00aa;
		}
	}
	{
		t42 * L_18 = m1430(V_4, &m1430_MI);
		bool L_19 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, V_0, L_18);
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (t41 *)NULL;
	}

IL_00aa:
	{
		t316* L_20 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 3));
		ArrayElementTypeCheck (L_20, p0);
		*((t29 **)(t29 **)SZArrayLdElema(L_20, 0)) = (t29 *)p0;
		t316* L_21 = L_20;
		ArrayElementTypeCheck (L_21, p1);
		*((t29 **)(t29 **)SZArrayLdElema(L_21, 1)) = (t29 *)p1;
		t316* L_22 = L_21;
		ArrayElementTypeCheck (L_22, V_4);
		*((t29 **)(t29 **)SZArrayLdElema(L_22, 2)) = (t29 *)V_4;
		t29 * L_23 = m2982(V_3, L_22, &m2982_MI);
		return ((t556 *)IsInst(L_23, InitializedTypeInfo(&t556_TI)));
	}
}
// Metadata Definition UnityEngine.Events.PersistentCall
extern Il2CppType t41_0_0_1;
extern CustomAttributesCache t565__CustomAttributeCache_m_Target;
FieldInfo t565_f0_FieldInfo = 
{
	"m_Target", &t41_0_0_1, &t565_TI, offsetof(t565, f0), &t565__CustomAttributeCache_m_Target};
extern Il2CppType t7_0_0_1;
extern CustomAttributesCache t565__CustomAttributeCache_m_MethodName;
FieldInfo t565_f1_FieldInfo = 
{
	"m_MethodName", &t7_0_0_1, &t565_TI, offsetof(t565, f1), &t565__CustomAttributeCache_m_MethodName};
extern Il2CppType t554_0_0_1;
extern CustomAttributesCache t565__CustomAttributeCache_m_Mode;
FieldInfo t565_f2_FieldInfo = 
{
	"m_Mode", &t554_0_0_1, &t565_TI, offsetof(t565, f2), &t565__CustomAttributeCache_m_Mode};
extern Il2CppType t555_0_0_1;
extern CustomAttributesCache t565__CustomAttributeCache_m_Arguments;
FieldInfo t565_f3_FieldInfo = 
{
	"m_Arguments", &t555_0_0_1, &t565_TI, offsetof(t565, f3), &t565__CustomAttributeCache_m_Arguments};
extern Il2CppType t564_0_0_1;
extern CustomAttributesCache t565__CustomAttributeCache_m_CallState;
FieldInfo t565_f4_FieldInfo = 
{
	"m_CallState", &t564_0_0_1, &t565_TI, offsetof(t565, f4), &t565__CustomAttributeCache_m_CallState};
static FieldInfo* t565_FIs[] =
{
	&t565_f0_FieldInfo,
	&t565_f1_FieldInfo,
	&t565_f2_FieldInfo,
	&t565_f3_FieldInfo,
	&t565_f4_FieldInfo,
	NULL
};
static PropertyInfo t565____target_PropertyInfo = 
{
	&t565_TI, "target", &m2797_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t565____methodName_PropertyInfo = 
{
	&t565_TI, "methodName", &m2798_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t565____mode_PropertyInfo = 
{
	&t565_TI, "mode", &m2799_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t565____arguments_PropertyInfo = 
{
	&t565_TI, "arguments", &m2800_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t565_PIs[] =
{
	&t565____target_PropertyInfo,
	&t565____methodName_PropertyInfo,
	&t565____mode_PropertyInfo,
	&t565____arguments_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2796_MI = 
{
	".ctor", (methodPointerType)&m2796, &t565_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1112, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2797_MI = 
{
	"get_target", (methodPointerType)&m2797, &t565_TI, &t41_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1113, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2798_MI = 
{
	"get_methodName", (methodPointerType)&m2798, &t565_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1114, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t554_0_0_0;
extern void* RuntimeInvoker_t554 (MethodInfo* method, void* obj, void** args);
MethodInfo m2799_MI = 
{
	"get_mode", (methodPointerType)&m2799, &t565_TI, &t554_0_0_0, RuntimeInvoker_t554, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1115, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t555_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2800_MI = 
{
	"get_arguments", (methodPointerType)&m2800, &t565_TI, &t555_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1116, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2801_MI = 
{
	"IsValid", (methodPointerType)&m2801, &t565_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1117, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t566_0_0_0;
extern Il2CppType t566_0_0_0;
static ParameterInfo t565_m2802_ParameterInfos[] = 
{
	{"theEvent", 0, 134218868, &EmptyCustomAttributesCache, &t566_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2802_MI = 
{
	"GetRuntimeCall", (methodPointerType)&m2802, &t565_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t565_m2802_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1118, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t555_0_0_0;
static ParameterInfo t565_m2803_ParameterInfos[] = 
{
	{"target", 0, 134218869, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"method", 1, 134218870, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"arguments", 2, 134218871, &EmptyCustomAttributesCache, &t555_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2803_MI = 
{
	"GetObjectCall", (methodPointerType)&m2803, &t565_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t565_m2803_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 3, false, false, 1119, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t565_MIs[] =
{
	&m2796_MI,
	&m2797_MI,
	&m2798_MI,
	&m2799_MI,
	&m2800_MI,
	&m2801_MI,
	&m2802_MI,
	&m2803_MI,
	NULL
};
static MethodInfo* t565_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t565_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("instance"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t565_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("methodName"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t565_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("mode"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t565_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("arguments"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t565_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("enabled"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), &m1318_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t565__CustomAttributeCache_m_Target = {
2,
NULL,
&t565_CustomAttributesCacheGenerator_m_Target
};
CustomAttributesCache t565__CustomAttributeCache_m_MethodName = {
2,
NULL,
&t565_CustomAttributesCacheGenerator_m_MethodName
};
CustomAttributesCache t565__CustomAttributeCache_m_Mode = {
2,
NULL,
&t565_CustomAttributesCacheGenerator_m_Mode
};
CustomAttributesCache t565__CustomAttributeCache_m_Arguments = {
2,
NULL,
&t565_CustomAttributesCacheGenerator_m_Arguments
};
CustomAttributesCache t565__CustomAttributeCache_m_CallState = {
3,
NULL,
&t565_CustomAttributesCacheGenerator_m_CallState
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t565_0_0_0;
extern Il2CppType t565_1_0_0;
struct t565;
extern CustomAttributesCache t565__CustomAttributeCache_m_Target;
extern CustomAttributesCache t565__CustomAttributeCache_m_MethodName;
extern CustomAttributesCache t565__CustomAttributeCache_m_Mode;
extern CustomAttributesCache t565__CustomAttributeCache_m_Arguments;
extern CustomAttributesCache t565__CustomAttributeCache_m_CallState;
TypeInfo t565_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PersistentCall", "UnityEngine.Events", t565_MIs, t565_PIs, t565_FIs, NULL, &t29_TI, NULL, NULL, &t565_TI, NULL, t565_VT, &EmptyCustomAttributesCache, &t565_TI, &t565_0_0_0, &t565_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t565), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 8, 4, 5, 0, 0, 4, 0, 0};
#include "t567.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t567_TI;
#include "t567MD.h"

#include "t568.h"
#include "t569.h"
#include "t661.h"
extern TypeInfo t568_TI;
extern TypeInfo t661_TI;
#include "t568MD.h"
#include "t661MD.h"
#include "t569MD.h"
extern MethodInfo m2983_MI;
extern MethodInfo m2984_MI;
extern MethodInfo m2985_MI;
extern MethodInfo m2807_MI;
extern MethodInfo m2986_MI;


extern MethodInfo m2804_MI;
 void m2804 (t567 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t568_TI));
		t568 * L_0 = (t568 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t568_TI));
		m2983(L_0, &m2983_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m2805_MI;
 void m2805 (t567 * __this, t569 * p0, t566 * p1, MethodInfo* method){
	t565 * V_0 = {0};
	t661  V_1 = {0};
	t556 * V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t568 * L_0 = (__this->f0);
		t661  L_1 = m2984(L_0, &m2984_MI);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			t565 * L_2 = m2985((&V_1), &m2985_MI);
			V_0 = L_2;
			bool L_3 = m2801(V_0, &m2801_MI);
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			t556 * L_4 = m2802(V_0, p1, &m2802_MI);
			V_2 = L_4;
			if (!V_2)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			m2807(p0, V_2, &m2807_MI);
		}

IL_003e:
		{
			bool L_5 = m2986((&V_1), &m2986_MI);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			// IL_004a: leave IL_005b
			leaveInstructions[0] = 0x5B; // 1
			THROW_SENTINEL(IL_005b);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_004f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_004f;
	}

IL_004f:
	{ // begin finally (depth: 1)
		t661  L_6 = V_1;
		t29 * L_7 = Box(InitializedTypeInfo(&t661_TI), &L_6);
		InterfaceActionInvoker0::Invoke(&m1428_MI, L_7);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x5B:
				goto IL_005b;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_005b:
	{
		return;
	}
}
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern Il2CppType t568_0_0_1;
extern CustomAttributesCache t567__CustomAttributeCache_m_Calls;
FieldInfo t567_f0_FieldInfo = 
{
	"m_Calls", &t568_0_0_1, &t567_TI, offsetof(t567, f0), &t567__CustomAttributeCache_m_Calls};
static FieldInfo* t567_FIs[] =
{
	&t567_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2804_MI = 
{
	".ctor", (methodPointerType)&m2804, &t567_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1120, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t569_0_0_0;
extern Il2CppType t569_0_0_0;
extern Il2CppType t566_0_0_0;
static ParameterInfo t567_m2805_ParameterInfos[] = 
{
	{"invokableList", 0, 134218872, &EmptyCustomAttributesCache, &t569_0_0_0},
	{"unityEventBase", 1, 134218873, &EmptyCustomAttributesCache, &t566_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2805_MI = 
{
	"Initialize", (methodPointerType)&m2805, &t567_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t567_m2805_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 1121, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t567_MIs[] =
{
	&m2804_MI,
	&m2805_MI,
	NULL
};
static MethodInfo* t567_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t567_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t567__CustomAttributeCache_m_Calls = {
2,
NULL,
&t567_CustomAttributesCacheGenerator_m_Calls
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t567_0_0_0;
extern Il2CppType t567_1_0_0;
struct t567;
extern CustomAttributesCache t567__CustomAttributeCache_m_Calls;
TypeInfo t567_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PersistentCallGroup", "UnityEngine.Events", t567_MIs, NULL, t567_FIs, NULL, &t29_TI, NULL, NULL, &t567_TI, NULL, t567_VT, &EmptyCustomAttributesCache, &t567_TI, &t567_0_0_0, &t567_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t567), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t569_TI;

#include "t570.h"
#include "t662.h"
extern TypeInfo t570_TI;
extern TypeInfo t662_TI;
#include "t570MD.h"
#include "t662MD.h"
extern MethodInfo m2987_MI;
extern MethodInfo m2988_MI;
extern MethodInfo m2989_MI;
extern MethodInfo m2956_MI;
extern MethodInfo m2990_MI;
extern MethodInfo m2991_MI;
extern MethodInfo m2992_MI;
extern MethodInfo m2993_MI;
extern MethodInfo m2994_MI;
extern MethodInfo m2995_MI;
extern MethodInfo m2954_MI;


extern MethodInfo m2806_MI;
 void m2806 (t569 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t570_TI));
		t570 * L_0 = (t570 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t570_TI));
		m2987(L_0, &m2987_MI);
		__this->f0 = L_0;
		t570 * L_1 = (t570 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t570_TI));
		m2987(L_1, &m2987_MI);
		__this->f1 = L_1;
		t570 * L_2 = (t570 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t570_TI));
		m2987(L_2, &m2987_MI);
		__this->f2 = L_2;
		m1331(__this, &m1331_MI);
		return;
	}
}
 void m2807 (t569 * __this, t556 * p0, MethodInfo* method){
	{
		t570 * L_0 = (__this->f0);
		VirtActionInvoker1< t556 * >::Invoke(&m2988_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m2808_MI;
 void m2808 (t569 * __this, t556 * p0, MethodInfo* method){
	{
		t570 * L_0 = (__this->f1);
		VirtActionInvoker1< t556 * >::Invoke(&m2988_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m2809_MI;
 void m2809 (t569 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	t570 * V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t570_TI));
		t570 * L_0 = (t570 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t570_TI));
		m2987(L_0, &m2987_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		t570 * L_1 = (__this->f1);
		t556 * L_2 = (t556 *)VirtFuncInvoker1< t556 *, int32_t >::Invoke(&m2989_MI, L_1, V_1);
		bool L_3 = (bool)VirtFuncInvoker2< bool, t29 *, t557 * >::Invoke(&m2956_MI, L_2, p0, p1);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		t570 * L_4 = (__this->f1);
		t556 * L_5 = (t556 *)VirtFuncInvoker1< t556 *, int32_t >::Invoke(&m2989_MI, L_4, V_1);
		VirtActionInvoker1< t556 * >::Invoke(&m2988_MI, V_0, L_5);
	}

IL_0037:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_003b:
	{
		t570 * L_6 = (__this->f1);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2990_MI, L_6);
		if ((((int32_t)V_1) < ((int32_t)L_7)))
		{
			goto IL_000d;
		}
	}
	{
		t570 * L_8 = (__this->f1);
		t570 * L_9 = V_0;
		t35 L_10 = { GetVirtualMethodInfo(L_9, &m2991_MI) };
		t662 * L_11 = (t662 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t662_TI));
		m2992(L_11, L_9, L_10, &m2992_MI);
		m2993(L_8, L_11, &m2993_MI);
		return;
	}
}
extern MethodInfo m2810_MI;
 void m2810 (t569 * __this, MethodInfo* method){
	{
		t570 * L_0 = (__this->f0);
		VirtActionInvoker0::Invoke(&m2994_MI, L_0);
		return;
	}
}
extern MethodInfo m2811_MI;
 void m2811 (t569 * __this, t316* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t570 * L_0 = (__this->f2);
		t570 * L_1 = (__this->f0);
		m2995(L_0, L_1, &m2995_MI);
		t570 * L_2 = (__this->f2);
		t570 * L_3 = (__this->f1);
		m2995(L_2, L_3, &m2995_MI);
		V_0 = 0;
		goto IL_003f;
	}

IL_0029:
	{
		t570 * L_4 = (__this->f2);
		t556 * L_5 = (t556 *)VirtFuncInvoker1< t556 *, int32_t >::Invoke(&m2989_MI, L_4, V_0);
		VirtActionInvoker1< t316* >::Invoke(&m2954_MI, L_5, p0);
		V_0 = ((int32_t)(V_0+1));
	}

IL_003f:
	{
		t570 * L_6 = (__this->f2);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2990_MI, L_6);
		if ((((int32_t)V_0) < ((int32_t)L_7)))
		{
			goto IL_0029;
		}
	}
	{
		t570 * L_8 = (__this->f2);
		VirtActionInvoker0::Invoke(&m2994_MI, L_8);
		return;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCallList
extern Il2CppType t570_0_0_33;
FieldInfo t569_f0_FieldInfo = 
{
	"m_PersistentCalls", &t570_0_0_33, &t569_TI, offsetof(t569, f0), &EmptyCustomAttributesCache};
extern Il2CppType t570_0_0_33;
FieldInfo t569_f1_FieldInfo = 
{
	"m_RuntimeCalls", &t570_0_0_33, &t569_TI, offsetof(t569, f1), &EmptyCustomAttributesCache};
extern Il2CppType t570_0_0_33;
FieldInfo t569_f2_FieldInfo = 
{
	"m_ExecutingCalls", &t570_0_0_33, &t569_TI, offsetof(t569, f2), &EmptyCustomAttributesCache};
static FieldInfo* t569_FIs[] =
{
	&t569_f0_FieldInfo,
	&t569_f1_FieldInfo,
	&t569_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2806_MI = 
{
	".ctor", (methodPointerType)&m2806, &t569_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1122, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t556_0_0_0;
static ParameterInfo t569_m2807_ParameterInfos[] = 
{
	{"call", 0, 134218874, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2807_MI = 
{
	"AddPersistentInvokableCall", (methodPointerType)&m2807, &t569_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t569_m2807_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1123, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t556_0_0_0;
static ParameterInfo t569_m2808_ParameterInfos[] = 
{
	{"call", 0, 134218875, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2808_MI = 
{
	"AddListener", (methodPointerType)&m2808, &t569_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t569_m2808_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1124, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t569_m2809_ParameterInfos[] = 
{
	{"targetObj", 0, 134218876, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218877, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2809_MI = 
{
	"RemoveListener", (methodPointerType)&m2809, &t569_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t569_m2809_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 1125, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2810_MI = 
{
	"ClearPersistent", (methodPointerType)&m2810, &t569_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1126, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t569_m2811_ParameterInfos[] = 
{
	{"parameters", 0, 134218878, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2811_MI = 
{
	"Invoke", (methodPointerType)&m2811, &t569_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t569_m2811_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1127, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t569_MIs[] =
{
	&m2806_MI,
	&m2807_MI,
	&m2808_MI,
	&m2809_MI,
	&m2810_MI,
	&m2811_MI,
	NULL
};
static MethodInfo* t569_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t569_1_0_0;
struct t569;
TypeInfo t569_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCallList", "UnityEngine.Events", t569_MIs, NULL, t569_FIs, NULL, &t29_TI, NULL, NULL, &t569_TI, NULL, t569_VT, &EmptyCustomAttributesCache, &t569_TI, &t569_0_0_0, &t569_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t569), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 0, 3, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m2996_MI;
extern MethodInfo m2815_MI;
extern MethodInfo m2814_MI;
extern MethodInfo m2997_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m2816_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m2999_MI;
extern MethodInfo m3000_MI;


extern MethodInfo m2812_MI;
 void m2812 (t566 * __this, MethodInfo* method){
	{
		__this->f3 = 1;
		m1331(__this, &m1331_MI);
		t569 * L_0 = (t569 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t569_TI));
		m2806(L_0, &m2806_MI);
		__this->f0 = L_0;
		t567 * L_1 = (t567 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t567_TI));
		m2804(L_1, &m2804_MI);
		__this->f1 = L_1;
		t42 * L_2 = m1430(__this, &m1430_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_2);
		__this->f2 = L_3;
		return;
	}
}
extern MethodInfo m1324_MI;
 void m1324 (t566 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m1325_MI;
 void m1325 (t566 * __this, MethodInfo* method){
	{
		m2815(__this, &m2815_MI);
		t42 * L_0 = m1430(__this, &m1430_MI);
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_0);
		__this->f2 = L_1;
		return;
	}
}
 t557 * m2813 (t566 * __this, t565 * p0, MethodInfo* method){
	t42 * V_0 = {0};
	t42 * G_B3_0 = {0};
	t42 * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t41_0_0_0), &m1554_MI);
		V_0 = L_0;
		t555 * L_1 = m2800(p0, &m2800_MI);
		t7* L_2 = m2784(L_1, &m2784_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_3 = m1772(NULL, L_2, &m1772_MI);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		t555 * L_4 = m2800(p0, &m2800_MI);
		t7* L_5 = m2784(L_4, &m2784_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_6 = m2978(NULL, L_5, 0, &m2978_MI);
		t42 * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t41_0_0_0), &m1554_MI);
		G_B3_0 = L_8;
	}

IL_0042:
	{
		V_0 = G_B3_0;
	}

IL_0043:
	{
		t7* L_9 = m2798(p0, &m2798_MI);
		t41 * L_10 = m2797(p0, &m2797_MI);
		int32_t L_11 = m2799(p0, &m2799_MI);
		t557 * L_12 = m2814(__this, L_9, L_10, L_11, V_0, &m2814_MI);
		return L_12;
	}
}
 t557 * m2814 (t566 * __this, t7* p0, t29 * p1, int32_t p2, t42 * p3, MethodInfo* method){
	int32_t V_0 = {0};
	t42 * G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	t537* G_B10_2 = {0};
	t537* G_B10_3 = {0};
	t7* G_B10_4 = {0};
	t29 * G_B10_5 = {0};
	t42 * G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	t537* G_B9_2 = {0};
	t537* G_B9_3 = {0};
	t7* G_B9_4 = {0};
	t29 * G_B9_5 = {0};
	{
		V_0 = p2;
		if (V_0 == 0)
		{
			goto IL_0029;
		}
		if (V_0 == 1)
		{
			goto IL_0032;
		}
		if (V_0 == 2)
		{
			goto IL_00ac;
		}
		if (V_0 == 3)
		{
			goto IL_005b;
		}
		if (V_0 == 4)
		{
			goto IL_0040;
		}
		if (V_0 == 5)
		{
			goto IL_0091;
		}
		if (V_0 == 6)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_00d0;
	}

IL_0029:
	{
		t557 * L_0 = (t557 *)VirtFuncInvoker2< t557 *, t7*, t29 * >::Invoke(&m2997_MI, __this, p0, p1);
		return L_0;
	}

IL_0032:
	{
		t557 * L_1 = m2820(NULL, p1, p0, ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 0)), &m2820_MI);
		return L_1;
	}

IL_0040:
	{
		t537* L_2 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t22_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_2, L_3);
		*((t42 **)(t42 **)SZArrayLdElema(L_2, 0)) = (t42 *)L_3;
		t557 * L_4 = m2820(NULL, p1, p0, L_2, &m2820_MI);
		return L_4;
	}

IL_005b:
	{
		t537* L_5 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_6 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_5, L_6);
		*((t42 **)(t42 **)SZArrayLdElema(L_5, 0)) = (t42 *)L_6;
		t557 * L_7 = m2820(NULL, p1, p0, L_5, &m2820_MI);
		return L_7;
	}

IL_0076:
	{
		t537* L_8 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t40_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_8, L_9);
		*((t42 **)(t42 **)SZArrayLdElema(L_8, 0)) = (t42 *)L_9;
		t557 * L_10 = m2820(NULL, p1, p0, L_8, &m2820_MI);
		return L_10;
	}

IL_0091:
	{
		t537* L_11 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_12 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_11, L_12);
		*((t42 **)(t42 **)SZArrayLdElema(L_11, 0)) = (t42 *)L_12;
		t557 * L_13 = m2820(NULL, p1, p0, L_11, &m2820_MI);
		return L_13;
	}

IL_00ac:
	{
		t537* L_14 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		t42 * L_15 = p3;
		G_B9_0 = L_15;
		G_B9_1 = 0;
		G_B9_2 = L_14;
		G_B9_3 = L_14;
		G_B9_4 = p0;
		G_B9_5 = p1;
		if (L_15)
		{
			G_B10_0 = L_15;
			G_B10_1 = 0;
			G_B10_2 = L_14;
			G_B10_3 = L_14;
			G_B10_4 = p0;
			G_B10_5 = p1;
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_16 = m1554(NULL, LoadTypeToken(&t41_0_0_0), &m1554_MI);
		G_B10_0 = L_16;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00c9:
	{
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		*((t42 **)(t42 **)SZArrayLdElema(G_B10_2, G_B10_1)) = (t42 *)G_B10_0;
		t557 * L_17 = m2820(NULL, G_B10_5, G_B10_4, G_B10_3, &m2820_MI);
		return L_17;
	}

IL_00d0:
	{
		return (t557 *)NULL;
	}
}
 void m2815 (t566 * __this, MethodInfo* method){
	{
		t569 * L_0 = (__this->f0);
		m2810(L_0, &m2810_MI);
		__this->f3 = 1;
		return;
	}
}
 void m2816 (t566 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		t567 * L_1 = (__this->f1);
		t569 * L_2 = (__this->f0);
		m2805(L_1, L_2, __this, &m2805_MI);
		__this->f3 = 0;
	}

IL_0024:
	{
		return;
	}
}
extern MethodInfo m2817_MI;
 void m2817 (t566 * __this, t556 * p0, MethodInfo* method){
	{
		t569 * L_0 = (__this->f0);
		m2808(L_0, p0, &m2808_MI);
		return;
	}
}
extern MethodInfo m2818_MI;
 void m2818 (t566 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		t569 * L_0 = (__this->f0);
		m2809(L_0, p0, p1, &m2809_MI);
		return;
	}
}
extern MethodInfo m2819_MI;
 void m2819 (t566 * __this, t316* p0, MethodInfo* method){
	{
		m2816(__this, &m2816_MI);
		t569 * L_0 = (__this->f0);
		m2811(L_0, p0, &m2811_MI);
		return;
	}
}
extern MethodInfo m1323_MI;
 t7* m1323 (t566 * __this, MethodInfo* method){
	{
		t7* L_0 = m1332(__this, &m1332_MI);
		t42 * L_1 = m1430(__this, &m1430_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1685(NULL, L_0, (t7*) &_stringLiteral79, L_2, &m1685_MI);
		return L_3;
	}
}
 t557 * m2820 (t29 * __this, t29 * p0, t7* p1, t537* p2, MethodInfo* method){
	t42 * V_0 = {0};
	t557 * V_1 = {0};
	t638* V_2 = {0};
	bool V_3 = false;
	int32_t V_4 = 0;
	t637 * V_5 = {0};
	t638* V_6 = {0};
	int32_t V_7 = 0;
	t42 * V_8 = {0};
	t42 * V_9 = {0};
	{
		t42 * L_0 = m1430(p0, &m1430_MI);
		V_0 = L_0;
		goto IL_008e;
	}

IL_000c:
	{
		t557 * L_1 = (t557 *)VirtFuncInvoker5< t557 *, t7*, int32_t, t631 *, t537*, t634* >::Invoke(&m2999_MI, V_0, p1, ((int32_t)52), (t631 *)NULL, p2, (t634*)(t634*)NULL);
		V_1 = L_1;
		if (!V_1)
		{
			goto IL_0087;
		}
	}
	{
		t638* L_2 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_1);
		V_2 = L_2;
		V_3 = 1;
		V_4 = 0;
		V_6 = V_2;
		V_7 = 0;
		goto IL_0074;
	}

IL_0036:
	{
		int32_t L_3 = V_7;
		V_5 = (*(t637 **)(t637 **)SZArrayLdElema(V_6, L_3));
		int32_t L_4 = V_4;
		V_8 = (*(t42 **)(t42 **)SZArrayLdElema(p2, L_4));
		t42 * L_5 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, V_5);
		V_9 = L_5;
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m3000_MI, V_8);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(&m3000_MI, V_9);
		V_3 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		if (V_3)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_007f;
	}

IL_0068:
	{
		V_4 = ((int32_t)(V_4+1));
		V_7 = ((int32_t)(V_7+1));
	}

IL_0074:
	{
		if ((((int32_t)V_7) < ((int32_t)(((int32_t)(((t20 *)V_6)->max_length))))))
		{
			goto IL_0036;
		}
	}

IL_007f:
	{
		if (!V_3)
		{
			goto IL_0087;
		}
	}
	{
		return V_1;
	}

IL_0087:
	{
		t42 * L_8 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_0);
		V_0 = L_8;
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)V_0) == ((t42 *)L_9)))
		{
			goto IL_00a4;
		}
	}
	{
		if (V_0)
		{
			goto IL_000c;
		}
	}

IL_00a4:
	{
		return (t557 *)NULL;
	}
}
// Metadata Definition UnityEngine.Events.UnityEventBase
extern Il2CppType t569_0_0_1;
FieldInfo t566_f0_FieldInfo = 
{
	"m_Calls", &t569_0_0_1, &t566_TI, offsetof(t566, f0), &EmptyCustomAttributesCache};
extern Il2CppType t567_0_0_1;
extern CustomAttributesCache t566__CustomAttributeCache_m_PersistentCalls;
FieldInfo t566_f1_FieldInfo = 
{
	"m_PersistentCalls", &t567_0_0_1, &t566_TI, offsetof(t566, f1), &t566__CustomAttributeCache_m_PersistentCalls};
extern Il2CppType t7_0_0_1;
extern CustomAttributesCache t566__CustomAttributeCache_m_TypeName;
FieldInfo t566_f2_FieldInfo = 
{
	"m_TypeName", &t7_0_0_1, &t566_TI, offsetof(t566, f2), &t566__CustomAttributeCache_m_TypeName};
extern Il2CppType t40_0_0_1;
FieldInfo t566_f3_FieldInfo = 
{
	"m_CallsDirty", &t40_0_0_1, &t566_TI, offsetof(t566, f3), &EmptyCustomAttributesCache};
static FieldInfo* t566_FIs[] =
{
	&t566_f0_FieldInfo,
	&t566_f1_FieldInfo,
	&t566_f2_FieldInfo,
	&t566_f3_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2812_MI = 
{
	".ctor", (methodPointerType)&m2812, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1128, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1324_MI = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize", (methodPointerType)&m1324, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, false, 1129, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1325_MI = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize", (methodPointerType)&m1325, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 5, 0, false, false, 1130, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t566_m2997_ParameterInfos[] = 
{
	{"name", 0, 134218879, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134218880, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2997_MI = 
{
	"FindMethod_Impl", NULL, &t566_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t566_m2997_ParameterInfos, &EmptyCustomAttributesCache, 1476, 0, 6, 2, false, false, 1131, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t566_m2973_ParameterInfos[] = 
{
	{"target", 0, 134218881, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218882, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2973_MI = 
{
	"GetDelegate", NULL, &t566_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t566_m2973_ParameterInfos, &EmptyCustomAttributesCache, 1475, 0, 7, 2, false, false, 1132, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t565_0_0_0;
static ParameterInfo t566_m2813_ParameterInfos[] = 
{
	{"call", 0, 134218883, &EmptyCustomAttributesCache, &t565_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2813_MI = 
{
	"FindMethod", (methodPointerType)&m2813, &t566_TI, &t557_0_0_0, RuntimeInvoker_t29_t29, t566_m2813_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 1133, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t554_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t566_m2814_ParameterInfos[] = 
{
	{"name", 0, 134218884, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"listener", 1, 134218885, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"mode", 2, 134218886, &EmptyCustomAttributesCache, &t554_0_0_0},
	{"argumentType", 3, 134218887, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2814_MI = 
{
	"FindMethod", (methodPointerType)&m2814, &t566_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29_t44_t29, t566_m2814_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 4, false, false, 1134, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2815_MI = 
{
	"DirtyPersistentCalls", (methodPointerType)&m2815, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1135, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2816_MI = 
{
	"RebuildPersistentCallsIfNeeded", (methodPointerType)&m2816, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1136, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t556_0_0_0;
static ParameterInfo t566_m2817_ParameterInfos[] = 
{
	{"call", 0, 134218888, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2817_MI = 
{
	"AddCall", (methodPointerType)&m2817, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t566_m2817_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 1137, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t566_m2818_ParameterInfos[] = 
{
	{"targetObj", 0, 134218889, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218890, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2818_MI = 
{
	"RemoveListener", (methodPointerType)&m2818, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t566_m2818_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 2, false, false, 1138, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t566_m2819_ParameterInfos[] = 
{
	{"parameters", 0, 134218891, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2819_MI = 
{
	"Invoke", (methodPointerType)&m2819, &t566_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t566_m2819_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 1139, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1323_MI = 
{
	"ToString", (methodPointerType)&m1323, &t566_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 1140, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_0_0_0;
static ParameterInfo t566_m2820_ParameterInfos[] = 
{
	{"obj", 0, 134218892, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"functionName", 1, 134218893, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"argumentTypes", 2, 134218894, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2820_MI = 
{
	"GetValidMethodInfo", (methodPointerType)&m2820, &t566_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t566_m2820_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1141, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t566_MIs[] =
{
	&m2812_MI,
	&m1324_MI,
	&m1325_MI,
	&m2997_MI,
	&m2973_MI,
	&m2813_MI,
	&m2814_MI,
	&m2815_MI,
	&m2816_MI,
	&m2817_MI,
	&m2818_MI,
	&m2819_MI,
	&m1323_MI,
	&m2820_MI,
	NULL
};
static MethodInfo* t566_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	NULL,
	NULL,
};
extern TypeInfo t301_TI;
static TypeInfo* t566_ITIs[] = 
{
	&t301_TI,
};
static Il2CppInterfaceOffsetPair t566_IOs[] = 
{
	{ &t301_TI, 4},
};
void t566_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t566_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t566__CustomAttributeCache_m_PersistentCalls = {
2,
NULL,
&t566_CustomAttributesCacheGenerator_m_PersistentCalls
};
CustomAttributesCache t566__CustomAttributeCache_m_TypeName = {
1,
NULL,
&t566_CustomAttributesCacheGenerator_m_TypeName
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t566_1_0_0;
struct t566;
extern CustomAttributesCache t566__CustomAttributeCache_m_PersistentCalls;
extern CustomAttributesCache t566__CustomAttributeCache_m_TypeName;
TypeInfo t566_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEventBase", "UnityEngine.Events", t566_MIs, NULL, t566_FIs, NULL, &t29_TI, NULL, NULL, &t566_TI, t566_ITIs, t566_VT, &EmptyCustomAttributesCache, &t566_TI, &t566_0_0_0, &t566_1_0_0, t566_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t566), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, false, false, false, 14, 0, 4, 0, 0, 8, 1, 1};
#include "t36.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t36_TI;
#include "t36MD.h"

extern MethodInfo m2821_MI;


extern MethodInfo m1512_MI;
 void m1512 (t36 * __this, MethodInfo* method){
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m53_MI;
 void m53 (t36 * __this, t34 * p0, MethodInfo* method){
	{
		t556 * L_0 = m2821(NULL, p0, &m2821_MI);
		m2817(__this, L_0, &m2817_MI);
		return;
	}
}
extern MethodInfo m1513_MI;
 t557 * m1513 (t36 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		t557 * L_0 = m2820(NULL, p1, p0, ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 0)), &m2820_MI);
		return L_0;
	}
}
extern MethodInfo m1514_MI;
 t556 * m1514 (t36 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		t558 * L_0 = (t558 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t558_TI));
		m2792(L_0, p0, p1, &m2792_MI);
		return L_0;
	}
}
 t556 * m2821 (t29 * __this, t34 * p0, MethodInfo* method){
	{
		t558 * L_0 = (t558 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t558_TI));
		m2793(L_0, p0, &m2793_MI);
		return L_0;
	}
}
extern MethodInfo m1517_MI;
 void m1517 (t36 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f4);
		m2819(__this, L_0, &m2819_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent
extern Il2CppType t316_0_0_33;
FieldInfo t36_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t36_TI, offsetof(t36, f4), &EmptyCustomAttributesCache};
static FieldInfo* t36_FIs[] =
{
	&t36_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1512_MI = 
{
	".ctor", (methodPointerType)&m1512, &t36_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1142, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t34_0_0_0;
static ParameterInfo t36_m53_ParameterInfos[] = 
{
	{"call", 0, 134218895, &EmptyCustomAttributesCache, &t34_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m53_MI = 
{
	"AddListener", (methodPointerType)&m53, &t36_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t36_m53_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1143, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t36_m1513_ParameterInfos[] = 
{
	{"name", 0, 134218896, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134218897, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1513_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m1513, &t36_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t36_m1513_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, false, 1144, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t36_m1514_ParameterInfos[] = 
{
	{"target", 0, 134218898, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218899, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1514_MI = 
{
	"GetDelegate", (methodPointerType)&m1514, &t36_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t36_m1514_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, false, 1145, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t34_0_0_0;
static ParameterInfo t36_m2821_ParameterInfos[] = 
{
	{"action", 0, 134218900, &EmptyCustomAttributesCache, &t34_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2821_MI = 
{
	"GetDelegate", (methodPointerType)&m2821, &t36_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t36_m2821_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1146, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1517_MI = 
{
	"Invoke", (methodPointerType)&m1517, &t36_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1147, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t36_MIs[] =
{
	&m1512_MI,
	&m53_MI,
	&m1513_MI,
	&m1514_MI,
	&m2821_MI,
	&m1517_MI,
	NULL
};
static MethodInfo* t36_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1513_MI,
	&m1514_MI,
};
static Il2CppInterfaceOffsetPair t36_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t36_0_0_0;
extern Il2CppType t36_1_0_0;
struct t36;
TypeInfo t36_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent", "UnityEngine.Events", t36_MIs, NULL, t36_FIs, NULL, &t566_TI, NULL, NULL, &t36_TI, NULL, t36_VT, &EmptyCustomAttributesCache, &t36_TI, &t36_0_0_0, &t36_1_0_0, t36_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t36), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 0, 1, 0, 0, 8, 0, 1};
#include "t571.h"
extern Il2CppGenericContainer t571_IGC;
extern TypeInfo t571_gp_T0_0_TI;
Il2CppGenericParamFull t571_gp_T0_0_TI_GenericParamFull = { { &t571_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* t571_IGPA[1] = 
{
	&t571_gp_T0_0_TI_GenericParamFull,
};
extern TypeInfo t571_TI;
Il2CppGenericContainer t571_IGC = { { NULL, NULL }, NULL, &t571_TI, 1, 0, t571_IGPA };
extern Il2CppType t21_0_0_0;
MethodInfo m3001_MI = 
{
	".ctor", NULL, &t571_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1148, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t664_0_0_0;
extern Il2CppType t664_0_0_0;
static ParameterInfo t571_m3002_ParameterInfos[] = 
{
	{"call", 0, 134218901, &EmptyCustomAttributesCache, &t664_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3002_MI = 
{
	"AddListener", NULL, &t571_TI, &t21_0_0_0, NULL, t571_m3002_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1149, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t664_0_0_0;
static ParameterInfo t571_m3003_ParameterInfos[] = 
{
	{"call", 0, 134218902, &EmptyCustomAttributesCache, &t664_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3003_MI = 
{
	"RemoveListener", NULL, &t571_TI, &t21_0_0_0, NULL, t571_m3003_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1150, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t571_m3004_ParameterInfos[] = 
{
	{"name", 0, 134218903, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134218904, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
MethodInfo m3004_MI = 
{
	"FindMethod_Impl", NULL, &t571_TI, &t557_0_0_0, NULL, t571_m3004_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, false, 1151, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t571_m3005_ParameterInfos[] = 
{
	{"target", 0, 134218905, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218906, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
MethodInfo m3005_MI = 
{
	"GetDelegate", NULL, &t571_TI, &t556_0_0_0, NULL, t571_m3005_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, false, 1152, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t664_0_0_0;
static ParameterInfo t571_m3006_ParameterInfos[] = 
{
	{"action", 0, 134218907, &EmptyCustomAttributesCache, &t664_0_0_0},
};
extern Il2CppType t556_0_0_0;
MethodInfo m3006_MI = 
{
	"GetDelegate", NULL, &t571_TI, &t556_0_0_0, NULL, t571_m3006_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1153, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t571_gp_0_0_0_0;
extern Il2CppType t571_gp_0_0_0_0;
static ParameterInfo t571_m3007_ParameterInfos[] = 
{
	{"arg0", 0, 134218908, &EmptyCustomAttributesCache, &t571_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3007_MI = 
{
	"Invoke", NULL, &t571_TI, &t21_0_0_0, NULL, t571_m3007_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1154, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t571_MIs[] =
{
	&m3001_MI,
	&m3002_MI,
	&m3003_MI,
	&m3004_MI,
	&m3005_MI,
	&m3006_MI,
	&m3007_MI,
	NULL
};
extern Il2CppType t316_0_0_33;
FieldInfo t571_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t571_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t571_FIs[] =
{
	&t571_f4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t571_0_0_0;
extern Il2CppType t571_1_0_0;
struct t571;
TypeInfo t571_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t571_MIs, NULL, t571_FIs, NULL, NULL, NULL, NULL, &t571_TI, NULL, NULL, NULL, NULL, &t571_0_0_0, &t571_1_0_0, NULL, NULL, &t571_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 0, 0, 0};
#include "t572.h"
extern Il2CppGenericContainer t572_IGC;
extern TypeInfo t572_gp_T0_0_TI;
Il2CppGenericParamFull t572_gp_T0_0_TI_GenericParamFull = { { &t572_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo t572_gp_T1_1_TI;
Il2CppGenericParamFull t572_gp_T1_1_TI_GenericParamFull = { { &t572_IGC, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* t572_IGPA[2] = 
{
	&t572_gp_T0_0_TI_GenericParamFull,
	&t572_gp_T1_1_TI_GenericParamFull,
};
extern TypeInfo t572_TI;
Il2CppGenericContainer t572_IGC = { { NULL, NULL }, NULL, &t572_TI, 2, 0, t572_IGPA };
extern Il2CppType t21_0_0_0;
MethodInfo m3008_MI = 
{
	".ctor", NULL, &t572_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1155, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t572_m3009_ParameterInfos[] = 
{
	{"name", 0, 134218909, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134218910, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
MethodInfo m3009_MI = 
{
	"FindMethod_Impl", NULL, &t572_TI, &t557_0_0_0, NULL, t572_m3009_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, false, 1156, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t572_m3010_ParameterInfos[] = 
{
	{"target", 0, 134218911, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218912, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
MethodInfo m3010_MI = 
{
	"GetDelegate", NULL, &t572_TI, &t556_0_0_0, NULL, t572_m3010_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, false, 1157, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t572_MIs[] =
{
	&m3008_MI,
	&m3009_MI,
	&m3010_MI,
	NULL
};
extern Il2CppType t316_0_0_33;
FieldInfo t572_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t572_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t572_FIs[] =
{
	&t572_f4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t572_0_0_0;
extern Il2CppType t572_1_0_0;
struct t572;
TypeInfo t572_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`2", "UnityEngine.Events", t572_MIs, NULL, t572_FIs, NULL, NULL, NULL, NULL, &t572_TI, NULL, NULL, NULL, NULL, &t572_0_0_0, &t572_1_0_0, NULL, NULL, &t572_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 0, 0, 0};
#include "t573.h"
extern Il2CppGenericContainer t573_IGC;
extern TypeInfo t573_gp_T0_0_TI;
Il2CppGenericParamFull t573_gp_T0_0_TI_GenericParamFull = { { &t573_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo t573_gp_T1_1_TI;
Il2CppGenericParamFull t573_gp_T1_1_TI_GenericParamFull = { { &t573_IGC, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t573_gp_T2_2_TI;
Il2CppGenericParamFull t573_gp_T2_2_TI_GenericParamFull = { { &t573_IGC, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* t573_IGPA[3] = 
{
	&t573_gp_T0_0_TI_GenericParamFull,
	&t573_gp_T1_1_TI_GenericParamFull,
	&t573_gp_T2_2_TI_GenericParamFull,
};
extern TypeInfo t573_TI;
Il2CppGenericContainer t573_IGC = { { NULL, NULL }, NULL, &t573_TI, 3, 0, t573_IGPA };
extern Il2CppType t21_0_0_0;
MethodInfo m3011_MI = 
{
	".ctor", NULL, &t573_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1158, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t573_m3012_ParameterInfos[] = 
{
	{"name", 0, 134218913, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134218914, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
MethodInfo m3012_MI = 
{
	"FindMethod_Impl", NULL, &t573_TI, &t557_0_0_0, NULL, t573_m3012_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, false, 1159, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t573_m3013_ParameterInfos[] = 
{
	{"target", 0, 134218915, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218916, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
MethodInfo m3013_MI = 
{
	"GetDelegate", NULL, &t573_TI, &t556_0_0_0, NULL, t573_m3013_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, false, 1160, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t573_MIs[] =
{
	&m3011_MI,
	&m3012_MI,
	&m3013_MI,
	NULL
};
extern Il2CppType t316_0_0_33;
FieldInfo t573_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t573_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t573_FIs[] =
{
	&t573_f4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t573_0_0_0;
extern Il2CppType t573_1_0_0;
struct t573;
TypeInfo t573_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`3", "UnityEngine.Events", t573_MIs, NULL, t573_FIs, NULL, NULL, NULL, NULL, &t573_TI, NULL, NULL, NULL, NULL, &t573_0_0_0, &t573_1_0_0, NULL, NULL, &t573_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 0, 0, 0};
#include "t574.h"
extern Il2CppGenericContainer t574_IGC;
extern TypeInfo t574_gp_T0_0_TI;
Il2CppGenericParamFull t574_gp_T0_0_TI_GenericParamFull = { { &t574_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo t574_gp_T1_1_TI;
Il2CppGenericParamFull t574_gp_T1_1_TI_GenericParamFull = { { &t574_IGC, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t574_gp_T2_2_TI;
Il2CppGenericParamFull t574_gp_T2_2_TI_GenericParamFull = { { &t574_IGC, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo t574_gp_T3_3_TI;
Il2CppGenericParamFull t574_gp_T3_3_TI_GenericParamFull = { { &t574_IGC, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* t574_IGPA[4] = 
{
	&t574_gp_T0_0_TI_GenericParamFull,
	&t574_gp_T1_1_TI_GenericParamFull,
	&t574_gp_T2_2_TI_GenericParamFull,
	&t574_gp_T3_3_TI_GenericParamFull,
};
extern TypeInfo t574_TI;
Il2CppGenericContainer t574_IGC = { { NULL, NULL }, NULL, &t574_TI, 4, 0, t574_IGPA };
extern Il2CppType t21_0_0_0;
MethodInfo m3014_MI = 
{
	".ctor", NULL, &t574_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1161, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t574_m3015_ParameterInfos[] = 
{
	{"name", 0, 134218917, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134218918, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
MethodInfo m3015_MI = 
{
	"FindMethod_Impl", NULL, &t574_TI, &t557_0_0_0, NULL, t574_m3015_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, false, 1162, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t574_m3016_ParameterInfos[] = 
{
	{"target", 0, 134218919, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134218920, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
MethodInfo m3016_MI = 
{
	"GetDelegate", NULL, &t574_TI, &t556_0_0_0, NULL, t574_m3016_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, false, 1163, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t574_MIs[] =
{
	&m3014_MI,
	&m3015_MI,
	&m3016_MI,
	NULL
};
extern Il2CppType t316_0_0_33;
FieldInfo t574_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t574_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t574_FIs[] =
{
	&t574_f4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t574_0_0_0;
extern Il2CppType t574_1_0_0;
struct t574;
TypeInfo t574_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`4", "UnityEngine.Events", t574_MIs, NULL, t574_FIs, NULL, NULL, NULL, NULL, &t574_TI, NULL, NULL, NULL, NULL, &t574_0_0_0, &t574_1_0_0, NULL, NULL, &t574_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 0, 0, 0};
#include "t575.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t575_TI;
#include "t575MD.h"

#include "t4MD.h"
extern MethodInfo m36_MI;


extern MethodInfo m2822_MI;
 void m2822 (t575 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m2823_MI;
 void m2823 (t575 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2824_MI;
 void m2824 (t575 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m2825_MI;
 void m2825 (t575 * __this, int32_t p0, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2822_MI = 
{
	".ctor", (methodPointerType)&m2822, &t575_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1164, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2823_MI = 
{
	"Start", (methodPointerType)&m2823, &t575_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1165, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2824_MI = 
{
	"OnGUI", (methodPointerType)&m2824, &t575_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1166, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t575_m2825_ParameterInfos[] = 
{
	{"windowID", 0, 134218921, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2825_MI = 
{
	"DoUserAuthorizationDialog", (methodPointerType)&m2825, &t575_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t575_m2825_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 1167, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t575_MIs[] =
{
	&m2822_MI,
	&m2823_MI,
	&m2824_MI,
	&m2825_MI,
	NULL
};
static MethodInfo* t575_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t575_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1317(tmp, il2cpp_codegen_string_new_wrapper(""), &m1317_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t575__CustomAttributeCache = {
1,
NULL,
&t575_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t575_0_0_0;
extern Il2CppType t575_1_0_0;
struct t575;
extern CustomAttributesCache t575__CustomAttributeCache;
TypeInfo t575_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UserAuthorizationDialog", "UnityEngine", t575_MIs, NULL, NULL, NULL, &t4_TI, NULL, NULL, &t575_TI, NULL, t575_VT, &t575__CustomAttributeCache, &t575_TI, &t575_0_0_0, &t575_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t575), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t490.h"
extern MethodInfo m2827_MI;


 void m2826 (t576 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
 t29 * m2827 (t576 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m2828_MI;
 bool m2828 (t576 * __this, t29 * p0, MethodInfo* method){
	t576 * V_0 = {0};
	{
		V_0 = ((t576 *)IsInst(p0, InitializedTypeInfo(&t576_TI)));
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		t29 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		t29 * L_1 = m2827(V_0, &m2827_MI);
		return ((((t29 *)L_1) == ((t29 *)NULL))? 1 : 0);
	}

IL_0024:
	{
		t29 * L_2 = (__this->f0);
		t29 * L_3 = m2827(V_0, &m2827_MI);
		bool L_4 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, L_2, L_3);
		return L_4;
	}
}
extern MethodInfo m2829_MI;
 int32_t m2829 (t576 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = m2883(__this, &m2883_MI);
		return L_1;
	}

IL_0012:
	{
		t29 * L_2 = (__this->f0);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, L_2);
		return L_3;
	}
}
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern Il2CppType t29_0_0_1;
FieldInfo t576_f0_FieldInfo = 
{
	"DefaultValue", &t29_0_0_1, &t576_TI, offsetof(t576, f0), &EmptyCustomAttributesCache};
static FieldInfo* t576_FIs[] =
{
	&t576_f0_FieldInfo,
	NULL
};
static PropertyInfo t576____Value_PropertyInfo = 
{
	&t576_TI, "Value", &m2827_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t576_PIs[] =
{
	&t576____Value_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t576_m2826_ParameterInfos[] = 
{
	{"value", 0, 134218922, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2826_MI = 
{
	".ctor", (methodPointerType)&m2826, &t576_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t576_m2826_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1168, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2827_MI = 
{
	"get_Value", (methodPointerType)&m2827, &t576_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1169, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t576_m2828_ParameterInfos[] = 
{
	{"obj", 0, 134218923, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2828_MI = 
{
	"Equals", (methodPointerType)&m2828, &t576_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t576_m2828_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 1170, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2829_MI = 
{
	"GetHashCode", (methodPointerType)&m2829, &t576_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 1171, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t576_MIs[] =
{
	&m2826_MI,
	&m2827_MI,
	&m2828_MI,
	&m2829_MI,
	NULL
};
static MethodInfo* t576_VT[] =
{
	&m2828_MI,
	&m46_MI,
	&m2829_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t576_IOs[] = 
{
	{ &t604_TI, 4},
};
void t576_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 18432, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t576__CustomAttributeCache = {
1,
NULL,
&t576_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t576_0_0_0;
extern Il2CppType t576_1_0_0;
struct t576;
extern CustomAttributesCache t576__CustomAttributeCache;
TypeInfo t576_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DefaultValueAttribute", "UnityEngine.Internal", t576_MIs, t576_PIs, t576_FIs, NULL, &t490_TI, NULL, NULL, &t576_TI, NULL, t576_VT, &t576__CustomAttributeCache, &t576_TI, &t576_0_0_0, &t576_1_0_0, t576_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t576), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 1, 0, 0, 4, 0, 1};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
