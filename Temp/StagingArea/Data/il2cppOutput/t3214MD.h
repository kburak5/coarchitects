﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3214;
struct t29;
struct t20;
#include "t811.h"

 void m17870 (t3214 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17871 (t3214 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17872 (t3214 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17873 (t3214 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17874 (t3214 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
