﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5885_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.IDeserializationCallback>
extern Il2CppType t4570_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30748_GM;
MethodInfo m30748_MI = 
{
	"GetEnumerator", NULL, &t5885_TI, &t4570_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30748_GM};
static MethodInfo* t5885_MIs[] =
{
	&m30748_MI,
	NULL
};
extern TypeInfo t603_TI;
static TypeInfo* t5885_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5885_0_0_0;
extern Il2CppType t5885_1_0_0;
struct t5885;
extern Il2CppGenericClass t5885_GC;
TypeInfo t5885_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5885_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5885_TI, t5885_ITIs, NULL, &EmptyCustomAttributesCache, &t5885_TI, &t5885_0_0_0, &t5885_1_0_0, NULL, &t5885_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4570_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.IDeserializationCallback>
extern MethodInfo m30749_MI;
static PropertyInfo t4570____Current_PropertyInfo = 
{
	&t4570_TI, "Current", &m30749_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4570_PIs[] =
{
	&t4570____Current_PropertyInfo,
	NULL
};
extern Il2CppType t918_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30749_GM;
MethodInfo m30749_MI = 
{
	"get_Current", NULL, &t4570_TI, &t918_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30749_GM};
static MethodInfo* t4570_MIs[] =
{
	&m30749_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4570_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4570_0_0_0;
extern Il2CppType t4570_1_0_0;
struct t4570;
extern Il2CppGenericClass t4570_GC;
TypeInfo t4570_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4570_MIs, t4570_PIs, NULL, NULL, NULL, NULL, NULL, &t4570_TI, t4570_ITIs, NULL, &EmptyCustomAttributesCache, &t4570_TI, &t4570_0_0_0, &t4570_1_0_0, NULL, &t4570_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3212.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3212_TI;
#include "t3212MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t918_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m17864_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m23452_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m23452(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.IDeserializationCallback>
extern Il2CppType t20_0_0_1;
FieldInfo t3212_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3212_TI, offsetof(t3212, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3212_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3212_TI, offsetof(t3212, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3212_FIs[] =
{
	&t3212_f0_FieldInfo,
	&t3212_f1_FieldInfo,
	NULL
};
extern MethodInfo m17861_MI;
static PropertyInfo t3212____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3212_TI, "System.Collections.IEnumerator.Current", &m17861_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3212____Current_PropertyInfo = 
{
	&t3212_TI, "Current", &m17864_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3212_PIs[] =
{
	&t3212____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3212____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3212_m17860_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17860_GM;
MethodInfo m17860_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3212_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3212_m17860_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17860_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17861_GM;
MethodInfo m17861_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3212_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17861_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17862_GM;
MethodInfo m17862_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3212_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17862_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17863_GM;
MethodInfo m17863_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3212_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17863_GM};
extern Il2CppType t918_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17864_GM;
MethodInfo m17864_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3212_TI, &t918_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17864_GM};
static MethodInfo* t3212_MIs[] =
{
	&m17860_MI,
	&m17861_MI,
	&m17862_MI,
	&m17863_MI,
	&m17864_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m17863_MI;
extern MethodInfo m17862_MI;
static MethodInfo* t3212_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17861_MI,
	&m17863_MI,
	&m17862_MI,
	&m17864_MI,
};
static TypeInfo* t3212_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4570_TI,
};
static Il2CppInterfaceOffsetPair t3212_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4570_TI, 7},
};
extern TypeInfo t918_TI;
static Il2CppRGCTXData t3212_RGCTXData[3] = 
{
	&m17864_MI/* Method Usage */,
	&t918_TI/* Class Usage */,
	&m23452_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3212_0_0_0;
extern Il2CppType t3212_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3212_GC;
extern TypeInfo t20_TI;
TypeInfo t3212_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3212_MIs, t3212_PIs, t3212_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3212_TI, t3212_ITIs, t3212_VT, &EmptyCustomAttributesCache, &t3212_TI, &t3212_0_0_0, &t3212_1_0_0, t3212_IOs, &t3212_GC, NULL, NULL, NULL, t3212_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3212)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5884_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.IDeserializationCallback>
extern MethodInfo m30750_MI;
extern MethodInfo m30751_MI;
static PropertyInfo t5884____Item_PropertyInfo = 
{
	&t5884_TI, "Item", &m30750_MI, &m30751_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5884_PIs[] =
{
	&t5884____Item_PropertyInfo,
	NULL
};
extern Il2CppType t918_0_0_0;
extern Il2CppType t918_0_0_0;
static ParameterInfo t5884_m30752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t918_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30752_GM;
MethodInfo m30752_MI = 
{
	"IndexOf", NULL, &t5884_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5884_m30752_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30752_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t918_0_0_0;
static ParameterInfo t5884_m30753_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t918_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30753_GM;
MethodInfo m30753_MI = 
{
	"Insert", NULL, &t5884_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5884_m30753_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30753_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5884_m30754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30754_GM;
MethodInfo m30754_MI = 
{
	"RemoveAt", NULL, &t5884_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5884_m30754_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30754_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5884_m30750_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t918_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30750_GM;
MethodInfo m30750_MI = 
{
	"get_Item", NULL, &t5884_TI, &t918_0_0_0, RuntimeInvoker_t29_t44, t5884_m30750_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30750_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t918_0_0_0;
static ParameterInfo t5884_m30751_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t918_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30751_GM;
MethodInfo m30751_MI = 
{
	"set_Item", NULL, &t5884_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5884_m30751_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30751_GM};
static MethodInfo* t5884_MIs[] =
{
	&m30752_MI,
	&m30753_MI,
	&m30754_MI,
	&m30750_MI,
	&m30751_MI,
	NULL
};
extern TypeInfo t5883_TI;
static TypeInfo* t5884_ITIs[] = 
{
	&t603_TI,
	&t5883_TI,
	&t5885_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5884_0_0_0;
extern Il2CppType t5884_1_0_0;
struct t5884;
extern Il2CppGenericClass t5884_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5884_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5884_MIs, t5884_PIs, NULL, NULL, NULL, NULL, NULL, &t5884_TI, t5884_ITIs, NULL, &t1908__CustomAttributeCache, &t5884_TI, &t5884_0_0_0, &t5884_1_0_0, NULL, &t5884_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4572_TI;

#include "t805.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
extern MethodInfo m30755_MI;
static PropertyInfo t4572____Current_PropertyInfo = 
{
	&t4572_TI, "Current", &m30755_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4572_PIs[] =
{
	&t4572____Current_PropertyInfo,
	NULL
};
extern Il2CppType t805_0_0_0;
extern void* RuntimeInvoker_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30755_GM;
MethodInfo m30755_MI = 
{
	"get_Current", NULL, &t4572_TI, &t805_0_0_0, RuntimeInvoker_t805, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30755_GM};
static MethodInfo* t4572_MIs[] =
{
	&m30755_MI,
	NULL
};
static TypeInfo* t4572_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4572_0_0_0;
extern Il2CppType t4572_1_0_0;
struct t4572;
extern Il2CppGenericClass t4572_GC;
TypeInfo t4572_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4572_MIs, t4572_PIs, NULL, NULL, NULL, NULL, NULL, &t4572_TI, t4572_ITIs, NULL, &EmptyCustomAttributesCache, &t4572_TI, &t4572_0_0_0, &t4572_1_0_0, NULL, &t4572_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3213.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3213_TI;
#include "t3213MD.h"

extern TypeInfo t805_TI;
extern MethodInfo m17869_MI;
extern MethodInfo m23463_MI;
struct t20;
 t805  m23463 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17865_MI;
 void m17865 (t3213 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17866_MI;
 t29 * m17866 (t3213 * __this, MethodInfo* method){
	{
		t805  L_0 = m17869(__this, &m17869_MI);
		t805  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t805_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17867_MI;
 void m17867 (t3213 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17868_MI;
 bool m17868 (t3213 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t805  m17869 (t3213 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t805  L_8 = m23463(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23463_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
extern Il2CppType t20_0_0_1;
FieldInfo t3213_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3213_TI, offsetof(t3213, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3213_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3213_TI, offsetof(t3213, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3213_FIs[] =
{
	&t3213_f0_FieldInfo,
	&t3213_f1_FieldInfo,
	NULL
};
static PropertyInfo t3213____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3213_TI, "System.Collections.IEnumerator.Current", &m17866_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3213____Current_PropertyInfo = 
{
	&t3213_TI, "Current", &m17869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3213_PIs[] =
{
	&t3213____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3213____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3213_m17865_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17865_GM;
MethodInfo m17865_MI = 
{
	".ctor", (methodPointerType)&m17865, &t3213_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3213_m17865_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17865_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17866_GM;
MethodInfo m17866_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17866, &t3213_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17866_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17867_GM;
MethodInfo m17867_MI = 
{
	"Dispose", (methodPointerType)&m17867, &t3213_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17867_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17868_GM;
MethodInfo m17868_MI = 
{
	"MoveNext", (methodPointerType)&m17868, &t3213_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17868_GM};
extern Il2CppType t805_0_0_0;
extern void* RuntimeInvoker_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17869_GM;
MethodInfo m17869_MI = 
{
	"get_Current", (methodPointerType)&m17869, &t3213_TI, &t805_0_0_0, RuntimeInvoker_t805, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17869_GM};
static MethodInfo* t3213_MIs[] =
{
	&m17865_MI,
	&m17866_MI,
	&m17867_MI,
	&m17868_MI,
	&m17869_MI,
	NULL
};
static MethodInfo* t3213_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17866_MI,
	&m17868_MI,
	&m17867_MI,
	&m17869_MI,
};
static TypeInfo* t3213_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4572_TI,
};
static Il2CppInterfaceOffsetPair t3213_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4572_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3213_0_0_0;
extern Il2CppType t3213_1_0_0;
extern Il2CppGenericClass t3213_GC;
TypeInfo t3213_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3213_MIs, t3213_PIs, t3213_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3213_TI, t3213_ITIs, t3213_VT, &EmptyCustomAttributesCache, &t3213_TI, &t3213_0_0_0, &t3213_1_0_0, t3213_IOs, &t3213_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3213)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5886_TI;

#include "System_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
extern MethodInfo m30756_MI;
static PropertyInfo t5886____Count_PropertyInfo = 
{
	&t5886_TI, "Count", &m30756_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30757_MI;
static PropertyInfo t5886____IsReadOnly_PropertyInfo = 
{
	&t5886_TI, "IsReadOnly", &m30757_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5886_PIs[] =
{
	&t5886____Count_PropertyInfo,
	&t5886____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30756_GM;
MethodInfo m30756_MI = 
{
	"get_Count", NULL, &t5886_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30756_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30757_GM;
MethodInfo m30757_MI = 
{
	"get_IsReadOnly", NULL, &t5886_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30757_GM};
extern Il2CppType t805_0_0_0;
extern Il2CppType t805_0_0_0;
static ParameterInfo t5886_m30758_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t805_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30758_GM;
MethodInfo m30758_MI = 
{
	"Add", NULL, &t5886_TI, &t21_0_0_0, RuntimeInvoker_t21_t805, t5886_m30758_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30758_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30759_GM;
MethodInfo m30759_MI = 
{
	"Clear", NULL, &t5886_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30759_GM};
extern Il2CppType t805_0_0_0;
static ParameterInfo t5886_m30760_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t805_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30760_GM;
MethodInfo m30760_MI = 
{
	"Contains", NULL, &t5886_TI, &t40_0_0_0, RuntimeInvoker_t40_t805, t5886_m30760_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30760_GM};
extern Il2CppType t804_0_0_0;
extern Il2CppType t804_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5886_m30761_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t804_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30761_GM;
MethodInfo m30761_MI = 
{
	"CopyTo", NULL, &t5886_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5886_m30761_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30761_GM};
extern Il2CppType t805_0_0_0;
static ParameterInfo t5886_m30762_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t805_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30762_GM;
MethodInfo m30762_MI = 
{
	"Remove", NULL, &t5886_TI, &t40_0_0_0, RuntimeInvoker_t40_t805, t5886_m30762_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30762_GM};
static MethodInfo* t5886_MIs[] =
{
	&m30756_MI,
	&m30757_MI,
	&m30758_MI,
	&m30759_MI,
	&m30760_MI,
	&m30761_MI,
	&m30762_MI,
	NULL
};
extern TypeInfo t5888_TI;
static TypeInfo* t5886_ITIs[] = 
{
	&t603_TI,
	&t5888_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5886_0_0_0;
extern Il2CppType t5886_1_0_0;
struct t5886;
extern Il2CppGenericClass t5886_GC;
TypeInfo t5886_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5886_MIs, t5886_PIs, NULL, NULL, NULL, NULL, NULL, &t5886_TI, t5886_ITIs, NULL, &EmptyCustomAttributesCache, &t5886_TI, &t5886_0_0_0, &t5886_1_0_0, NULL, &t5886_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
extern Il2CppType t4572_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30763_GM;
MethodInfo m30763_MI = 
{
	"GetEnumerator", NULL, &t5888_TI, &t4572_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30763_GM};
static MethodInfo* t5888_MIs[] =
{
	&m30763_MI,
	NULL
};
static TypeInfo* t5888_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5888_0_0_0;
extern Il2CppType t5888_1_0_0;
struct t5888;
extern Il2CppGenericClass t5888_GC;
TypeInfo t5888_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5888_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5888_TI, t5888_ITIs, NULL, &EmptyCustomAttributesCache, &t5888_TI, &t5888_0_0_0, &t5888_1_0_0, NULL, &t5888_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5887_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
extern MethodInfo m30764_MI;
extern MethodInfo m30765_MI;
static PropertyInfo t5887____Item_PropertyInfo = 
{
	&t5887_TI, "Item", &m30764_MI, &m30765_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5887_PIs[] =
{
	&t5887____Item_PropertyInfo,
	NULL
};
extern Il2CppType t805_0_0_0;
static ParameterInfo t5887_m30766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t805_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30766_GM;
MethodInfo m30766_MI = 
{
	"IndexOf", NULL, &t5887_TI, &t44_0_0_0, RuntimeInvoker_t44_t805, t5887_m30766_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30766_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t805_0_0_0;
static ParameterInfo t5887_m30767_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t805_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30767_GM;
MethodInfo m30767_MI = 
{
	"Insert", NULL, &t5887_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t805, t5887_m30767_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30767_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5887_m30768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30768_GM;
MethodInfo m30768_MI = 
{
	"RemoveAt", NULL, &t5887_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5887_m30768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30768_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5887_m30764_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t805_0_0_0;
extern void* RuntimeInvoker_t805_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30764_GM;
MethodInfo m30764_MI = 
{
	"get_Item", NULL, &t5887_TI, &t805_0_0_0, RuntimeInvoker_t805_t44, t5887_m30764_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30764_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t805_0_0_0;
static ParameterInfo t5887_m30765_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t805_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t805 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30765_GM;
MethodInfo m30765_MI = 
{
	"set_Item", NULL, &t5887_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t805, t5887_m30765_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30765_GM};
static MethodInfo* t5887_MIs[] =
{
	&m30766_MI,
	&m30767_MI,
	&m30768_MI,
	&m30764_MI,
	&m30765_MI,
	NULL
};
static TypeInfo* t5887_ITIs[] = 
{
	&t603_TI,
	&t5886_TI,
	&t5888_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5887_0_0_0;
extern Il2CppType t5887_1_0_0;
struct t5887;
extern Il2CppGenericClass t5887_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5887_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5887_MIs, t5887_PIs, NULL, NULL, NULL, NULL, NULL, &t5887_TI, t5887_ITIs, NULL, &t1908__CustomAttributeCache, &t5887_TI, &t5887_0_0_0, &t5887_1_0_0, NULL, &t5887_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4574_TI;

#include "t811.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatusFlags>
extern MethodInfo m30769_MI;
static PropertyInfo t4574____Current_PropertyInfo = 
{
	&t4574_TI, "Current", &m30769_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4574_PIs[] =
{
	&t4574____Current_PropertyInfo,
	NULL
};
extern Il2CppType t811_0_0_0;
extern void* RuntimeInvoker_t811 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30769_GM;
MethodInfo m30769_MI = 
{
	"get_Current", NULL, &t4574_TI, &t811_0_0_0, RuntimeInvoker_t811, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30769_GM};
static MethodInfo* t4574_MIs[] =
{
	&m30769_MI,
	NULL
};
static TypeInfo* t4574_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4574_0_0_0;
extern Il2CppType t4574_1_0_0;
struct t4574;
extern Il2CppGenericClass t4574_GC;
TypeInfo t4574_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4574_MIs, t4574_PIs, NULL, NULL, NULL, NULL, NULL, &t4574_TI, t4574_ITIs, NULL, &EmptyCustomAttributesCache, &t4574_TI, &t4574_0_0_0, &t4574_1_0_0, NULL, &t4574_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3214.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3214_TI;
#include "t3214MD.h"

extern TypeInfo t811_TI;
extern MethodInfo m17874_MI;
extern MethodInfo m23474_MI;
struct t20;
 int32_t m23474 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17870_MI;
 void m17870 (t3214 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17871_MI;
 t29 * m17871 (t3214 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17874(__this, &m17874_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t811_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17872_MI;
 void m17872 (t3214 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17873_MI;
 bool m17873 (t3214 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17874 (t3214 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23474(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23474_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatusFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3214_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3214_TI, offsetof(t3214, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3214_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3214_TI, offsetof(t3214, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3214_FIs[] =
{
	&t3214_f0_FieldInfo,
	&t3214_f1_FieldInfo,
	NULL
};
static PropertyInfo t3214____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3214_TI, "System.Collections.IEnumerator.Current", &m17871_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3214____Current_PropertyInfo = 
{
	&t3214_TI, "Current", &m17874_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3214_PIs[] =
{
	&t3214____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3214____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3214_m17870_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17870_GM;
MethodInfo m17870_MI = 
{
	".ctor", (methodPointerType)&m17870, &t3214_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3214_m17870_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17870_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17871_GM;
MethodInfo m17871_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17871, &t3214_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17871_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17872_GM;
MethodInfo m17872_MI = 
{
	"Dispose", (methodPointerType)&m17872, &t3214_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17872_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17873_GM;
MethodInfo m17873_MI = 
{
	"MoveNext", (methodPointerType)&m17873, &t3214_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17873_GM};
extern Il2CppType t811_0_0_0;
extern void* RuntimeInvoker_t811 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17874_GM;
MethodInfo m17874_MI = 
{
	"get_Current", (methodPointerType)&m17874, &t3214_TI, &t811_0_0_0, RuntimeInvoker_t811, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17874_GM};
static MethodInfo* t3214_MIs[] =
{
	&m17870_MI,
	&m17871_MI,
	&m17872_MI,
	&m17873_MI,
	&m17874_MI,
	NULL
};
static MethodInfo* t3214_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17871_MI,
	&m17873_MI,
	&m17872_MI,
	&m17874_MI,
};
static TypeInfo* t3214_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4574_TI,
};
static Il2CppInterfaceOffsetPair t3214_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4574_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3214_0_0_0;
extern Il2CppType t3214_1_0_0;
extern Il2CppGenericClass t3214_GC;
TypeInfo t3214_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3214_MIs, t3214_PIs, t3214_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3214_TI, t3214_ITIs, t3214_VT, &EmptyCustomAttributesCache, &t3214_TI, &t3214_0_0_0, &t3214_1_0_0, t3214_IOs, &t3214_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3214)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5889_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509ChainStatusFlags>
extern MethodInfo m30770_MI;
static PropertyInfo t5889____Count_PropertyInfo = 
{
	&t5889_TI, "Count", &m30770_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30771_MI;
static PropertyInfo t5889____IsReadOnly_PropertyInfo = 
{
	&t5889_TI, "IsReadOnly", &m30771_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5889_PIs[] =
{
	&t5889____Count_PropertyInfo,
	&t5889____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30770_GM;
MethodInfo m30770_MI = 
{
	"get_Count", NULL, &t5889_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30770_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30771_GM;
MethodInfo m30771_MI = 
{
	"get_IsReadOnly", NULL, &t5889_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30771_GM};
extern Il2CppType t811_0_0_0;
extern Il2CppType t811_0_0_0;
static ParameterInfo t5889_m30772_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t811_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30772_GM;
MethodInfo m30772_MI = 
{
	"Add", NULL, &t5889_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5889_m30772_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30772_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30773_GM;
MethodInfo m30773_MI = 
{
	"Clear", NULL, &t5889_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30773_GM};
extern Il2CppType t811_0_0_0;
static ParameterInfo t5889_m30774_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t811_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30774_GM;
MethodInfo m30774_MI = 
{
	"Contains", NULL, &t5889_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5889_m30774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30774_GM};
extern Il2CppType t3908_0_0_0;
extern Il2CppType t3908_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5889_m30775_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3908_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30775_GM;
MethodInfo m30775_MI = 
{
	"CopyTo", NULL, &t5889_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5889_m30775_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30775_GM};
extern Il2CppType t811_0_0_0;
static ParameterInfo t5889_m30776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t811_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30776_GM;
MethodInfo m30776_MI = 
{
	"Remove", NULL, &t5889_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5889_m30776_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30776_GM};
static MethodInfo* t5889_MIs[] =
{
	&m30770_MI,
	&m30771_MI,
	&m30772_MI,
	&m30773_MI,
	&m30774_MI,
	&m30775_MI,
	&m30776_MI,
	NULL
};
extern TypeInfo t5891_TI;
static TypeInfo* t5889_ITIs[] = 
{
	&t603_TI,
	&t5891_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5889_0_0_0;
extern Il2CppType t5889_1_0_0;
struct t5889;
extern Il2CppGenericClass t5889_GC;
TypeInfo t5889_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5889_MIs, t5889_PIs, NULL, NULL, NULL, NULL, NULL, &t5889_TI, t5889_ITIs, NULL, &EmptyCustomAttributesCache, &t5889_TI, &t5889_0_0_0, &t5889_1_0_0, NULL, &t5889_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509ChainStatusFlags>
extern Il2CppType t4574_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30777_GM;
MethodInfo m30777_MI = 
{
	"GetEnumerator", NULL, &t5891_TI, &t4574_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30777_GM};
static MethodInfo* t5891_MIs[] =
{
	&m30777_MI,
	NULL
};
static TypeInfo* t5891_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5891_0_0_0;
extern Il2CppType t5891_1_0_0;
struct t5891;
extern Il2CppGenericClass t5891_GC;
TypeInfo t5891_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5891_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5891_TI, t5891_ITIs, NULL, &EmptyCustomAttributesCache, &t5891_TI, &t5891_0_0_0, &t5891_1_0_0, NULL, &t5891_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5890_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509ChainStatusFlags>
extern MethodInfo m30778_MI;
extern MethodInfo m30779_MI;
static PropertyInfo t5890____Item_PropertyInfo = 
{
	&t5890_TI, "Item", &m30778_MI, &m30779_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5890_PIs[] =
{
	&t5890____Item_PropertyInfo,
	NULL
};
extern Il2CppType t811_0_0_0;
static ParameterInfo t5890_m30780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t811_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30780_GM;
MethodInfo m30780_MI = 
{
	"IndexOf", NULL, &t5890_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5890_m30780_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30780_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t811_0_0_0;
static ParameterInfo t5890_m30781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t811_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30781_GM;
MethodInfo m30781_MI = 
{
	"Insert", NULL, &t5890_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5890_m30781_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30781_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5890_m30782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30782_GM;
MethodInfo m30782_MI = 
{
	"RemoveAt", NULL, &t5890_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5890_m30782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30782_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5890_m30778_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t811_0_0_0;
extern void* RuntimeInvoker_t811_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30778_GM;
MethodInfo m30778_MI = 
{
	"get_Item", NULL, &t5890_TI, &t811_0_0_0, RuntimeInvoker_t811_t44, t5890_m30778_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30778_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t811_0_0_0;
static ParameterInfo t5890_m30779_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t811_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30779_GM;
MethodInfo m30779_MI = 
{
	"set_Item", NULL, &t5890_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5890_m30779_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30779_GM};
static MethodInfo* t5890_MIs[] =
{
	&m30780_MI,
	&m30781_MI,
	&m30782_MI,
	&m30778_MI,
	&m30779_MI,
	NULL
};
static TypeInfo* t5890_ITIs[] = 
{
	&t603_TI,
	&t5889_TI,
	&t5891_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5890_0_0_0;
extern Il2CppType t5890_1_0_0;
struct t5890;
extern Il2CppGenericClass t5890_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5890_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5890_MIs, t5890_PIs, NULL, NULL, NULL, NULL, NULL, &t5890_TI, t5890_ITIs, NULL, &t1908__CustomAttributeCache, &t5890_TI, &t5890_0_0_0, &t5890_1_0_0, NULL, &t5890_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4576_TI;

#include "t798.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509FindType>
extern MethodInfo m30783_MI;
static PropertyInfo t4576____Current_PropertyInfo = 
{
	&t4576_TI, "Current", &m30783_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4576_PIs[] =
{
	&t4576____Current_PropertyInfo,
	NULL
};
extern Il2CppType t798_0_0_0;
extern void* RuntimeInvoker_t798 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30783_GM;
MethodInfo m30783_MI = 
{
	"get_Current", NULL, &t4576_TI, &t798_0_0_0, RuntimeInvoker_t798, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30783_GM};
static MethodInfo* t4576_MIs[] =
{
	&m30783_MI,
	NULL
};
static TypeInfo* t4576_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4576_0_0_0;
extern Il2CppType t4576_1_0_0;
struct t4576;
extern Il2CppGenericClass t4576_GC;
TypeInfo t4576_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4576_MIs, t4576_PIs, NULL, NULL, NULL, NULL, NULL, &t4576_TI, t4576_ITIs, NULL, &EmptyCustomAttributesCache, &t4576_TI, &t4576_0_0_0, &t4576_1_0_0, NULL, &t4576_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3215.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3215_TI;
#include "t3215MD.h"

extern TypeInfo t798_TI;
extern MethodInfo m17879_MI;
extern MethodInfo m23485_MI;
struct t20;
 int32_t m23485 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17875_MI;
 void m17875 (t3215 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17876_MI;
 t29 * m17876 (t3215 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17879(__this, &m17879_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t798_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17877_MI;
 void m17877 (t3215 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17878_MI;
 bool m17878 (t3215 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17879 (t3215 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23485(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23485_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509FindType>
extern Il2CppType t20_0_0_1;
FieldInfo t3215_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3215_TI, offsetof(t3215, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3215_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3215_TI, offsetof(t3215, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3215_FIs[] =
{
	&t3215_f0_FieldInfo,
	&t3215_f1_FieldInfo,
	NULL
};
static PropertyInfo t3215____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3215_TI, "System.Collections.IEnumerator.Current", &m17876_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3215____Current_PropertyInfo = 
{
	&t3215_TI, "Current", &m17879_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3215_PIs[] =
{
	&t3215____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3215____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3215_m17875_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17875_GM;
MethodInfo m17875_MI = 
{
	".ctor", (methodPointerType)&m17875, &t3215_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3215_m17875_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17875_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17876_GM;
MethodInfo m17876_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17876, &t3215_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17876_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17877_GM;
MethodInfo m17877_MI = 
{
	"Dispose", (methodPointerType)&m17877, &t3215_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17877_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17878_GM;
MethodInfo m17878_MI = 
{
	"MoveNext", (methodPointerType)&m17878, &t3215_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17878_GM};
extern Il2CppType t798_0_0_0;
extern void* RuntimeInvoker_t798 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17879_GM;
MethodInfo m17879_MI = 
{
	"get_Current", (methodPointerType)&m17879, &t3215_TI, &t798_0_0_0, RuntimeInvoker_t798, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17879_GM};
static MethodInfo* t3215_MIs[] =
{
	&m17875_MI,
	&m17876_MI,
	&m17877_MI,
	&m17878_MI,
	&m17879_MI,
	NULL
};
static MethodInfo* t3215_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17876_MI,
	&m17878_MI,
	&m17877_MI,
	&m17879_MI,
};
static TypeInfo* t3215_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4576_TI,
};
static Il2CppInterfaceOffsetPair t3215_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4576_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3215_0_0_0;
extern Il2CppType t3215_1_0_0;
extern Il2CppGenericClass t3215_GC;
TypeInfo t3215_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3215_MIs, t3215_PIs, t3215_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3215_TI, t3215_ITIs, t3215_VT, &EmptyCustomAttributesCache, &t3215_TI, &t3215_0_0_0, &t3215_1_0_0, t3215_IOs, &t3215_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3215)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5892_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509FindType>
extern MethodInfo m30784_MI;
static PropertyInfo t5892____Count_PropertyInfo = 
{
	&t5892_TI, "Count", &m30784_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30785_MI;
static PropertyInfo t5892____IsReadOnly_PropertyInfo = 
{
	&t5892_TI, "IsReadOnly", &m30785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5892_PIs[] =
{
	&t5892____Count_PropertyInfo,
	&t5892____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30784_GM;
MethodInfo m30784_MI = 
{
	"get_Count", NULL, &t5892_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30784_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30785_GM;
MethodInfo m30785_MI = 
{
	"get_IsReadOnly", NULL, &t5892_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30785_GM};
extern Il2CppType t798_0_0_0;
extern Il2CppType t798_0_0_0;
static ParameterInfo t5892_m30786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t798_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30786_GM;
MethodInfo m30786_MI = 
{
	"Add", NULL, &t5892_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5892_m30786_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30786_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30787_GM;
MethodInfo m30787_MI = 
{
	"Clear", NULL, &t5892_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30787_GM};
extern Il2CppType t798_0_0_0;
static ParameterInfo t5892_m30788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t798_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30788_GM;
MethodInfo m30788_MI = 
{
	"Contains", NULL, &t5892_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5892_m30788_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30788_GM};
extern Il2CppType t3909_0_0_0;
extern Il2CppType t3909_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5892_m30789_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3909_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30789_GM;
MethodInfo m30789_MI = 
{
	"CopyTo", NULL, &t5892_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5892_m30789_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30789_GM};
extern Il2CppType t798_0_0_0;
static ParameterInfo t5892_m30790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t798_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30790_GM;
MethodInfo m30790_MI = 
{
	"Remove", NULL, &t5892_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5892_m30790_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30790_GM};
static MethodInfo* t5892_MIs[] =
{
	&m30784_MI,
	&m30785_MI,
	&m30786_MI,
	&m30787_MI,
	&m30788_MI,
	&m30789_MI,
	&m30790_MI,
	NULL
};
extern TypeInfo t5894_TI;
static TypeInfo* t5892_ITIs[] = 
{
	&t603_TI,
	&t5894_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5892_0_0_0;
extern Il2CppType t5892_1_0_0;
struct t5892;
extern Il2CppGenericClass t5892_GC;
TypeInfo t5892_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5892_MIs, t5892_PIs, NULL, NULL, NULL, NULL, NULL, &t5892_TI, t5892_ITIs, NULL, &EmptyCustomAttributesCache, &t5892_TI, &t5892_0_0_0, &t5892_1_0_0, NULL, &t5892_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509FindType>
extern Il2CppType t4576_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30791_GM;
MethodInfo m30791_MI = 
{
	"GetEnumerator", NULL, &t5894_TI, &t4576_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30791_GM};
static MethodInfo* t5894_MIs[] =
{
	&m30791_MI,
	NULL
};
static TypeInfo* t5894_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5894_0_0_0;
extern Il2CppType t5894_1_0_0;
struct t5894;
extern Il2CppGenericClass t5894_GC;
TypeInfo t5894_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5894_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5894_TI, t5894_ITIs, NULL, &EmptyCustomAttributesCache, &t5894_TI, &t5894_0_0_0, &t5894_1_0_0, NULL, &t5894_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5893_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509FindType>
extern MethodInfo m30792_MI;
extern MethodInfo m30793_MI;
static PropertyInfo t5893____Item_PropertyInfo = 
{
	&t5893_TI, "Item", &m30792_MI, &m30793_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5893_PIs[] =
{
	&t5893____Item_PropertyInfo,
	NULL
};
extern Il2CppType t798_0_0_0;
static ParameterInfo t5893_m30794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t798_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30794_GM;
MethodInfo m30794_MI = 
{
	"IndexOf", NULL, &t5893_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5893_m30794_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30794_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t798_0_0_0;
static ParameterInfo t5893_m30795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t798_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30795_GM;
MethodInfo m30795_MI = 
{
	"Insert", NULL, &t5893_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5893_m30795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30795_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5893_m30796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30796_GM;
MethodInfo m30796_MI = 
{
	"RemoveAt", NULL, &t5893_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5893_m30796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30796_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5893_m30792_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t798_0_0_0;
extern void* RuntimeInvoker_t798_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30792_GM;
MethodInfo m30792_MI = 
{
	"get_Item", NULL, &t5893_TI, &t798_0_0_0, RuntimeInvoker_t798_t44, t5893_m30792_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30792_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t798_0_0_0;
static ParameterInfo t5893_m30793_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t798_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30793_GM;
MethodInfo m30793_MI = 
{
	"set_Item", NULL, &t5893_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5893_m30793_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30793_GM};
static MethodInfo* t5893_MIs[] =
{
	&m30794_MI,
	&m30795_MI,
	&m30796_MI,
	&m30792_MI,
	&m30793_MI,
	NULL
};
static TypeInfo* t5893_ITIs[] = 
{
	&t603_TI,
	&t5892_TI,
	&t5894_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5893_0_0_0;
extern Il2CppType t5893_1_0_0;
struct t5893;
extern Il2CppGenericClass t5893_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5893_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5893_MIs, t5893_PIs, NULL, NULL, NULL, NULL, NULL, &t5893_TI, t5893_ITIs, NULL, &t1908__CustomAttributeCache, &t5893_TI, &t5893_0_0_0, &t5893_1_0_0, NULL, &t5893_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4578_TI;

#include "t821.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyUsageFlags>
extern MethodInfo m30797_MI;
static PropertyInfo t4578____Current_PropertyInfo = 
{
	&t4578_TI, "Current", &m30797_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4578_PIs[] =
{
	&t4578____Current_PropertyInfo,
	NULL
};
extern Il2CppType t821_0_0_0;
extern void* RuntimeInvoker_t821 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30797_GM;
MethodInfo m30797_MI = 
{
	"get_Current", NULL, &t4578_TI, &t821_0_0_0, RuntimeInvoker_t821, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30797_GM};
static MethodInfo* t4578_MIs[] =
{
	&m30797_MI,
	NULL
};
static TypeInfo* t4578_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4578_0_0_0;
extern Il2CppType t4578_1_0_0;
struct t4578;
extern Il2CppGenericClass t4578_GC;
TypeInfo t4578_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4578_MIs, t4578_PIs, NULL, NULL, NULL, NULL, NULL, &t4578_TI, t4578_ITIs, NULL, &EmptyCustomAttributesCache, &t4578_TI, &t4578_0_0_0, &t4578_1_0_0, NULL, &t4578_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3216.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3216_TI;
#include "t3216MD.h"

extern TypeInfo t821_TI;
extern MethodInfo m17884_MI;
extern MethodInfo m23496_MI;
struct t20;
 int32_t m23496 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17880_MI;
 void m17880 (t3216 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17881_MI;
 t29 * m17881 (t3216 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17884(__this, &m17884_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t821_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17882_MI;
 void m17882 (t3216 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17883_MI;
 bool m17883 (t3216 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17884 (t3216 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23496(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23496_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyUsageFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3216_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3216_TI, offsetof(t3216, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3216_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3216_TI, offsetof(t3216, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3216_FIs[] =
{
	&t3216_f0_FieldInfo,
	&t3216_f1_FieldInfo,
	NULL
};
static PropertyInfo t3216____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3216_TI, "System.Collections.IEnumerator.Current", &m17881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3216____Current_PropertyInfo = 
{
	&t3216_TI, "Current", &m17884_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3216_PIs[] =
{
	&t3216____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3216____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3216_m17880_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17880_GM;
MethodInfo m17880_MI = 
{
	".ctor", (methodPointerType)&m17880, &t3216_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3216_m17880_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17880_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17881_GM;
MethodInfo m17881_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17881, &t3216_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17881_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17882_GM;
MethodInfo m17882_MI = 
{
	"Dispose", (methodPointerType)&m17882, &t3216_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17882_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17883_GM;
MethodInfo m17883_MI = 
{
	"MoveNext", (methodPointerType)&m17883, &t3216_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17883_GM};
extern Il2CppType t821_0_0_0;
extern void* RuntimeInvoker_t821 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17884_GM;
MethodInfo m17884_MI = 
{
	"get_Current", (methodPointerType)&m17884, &t3216_TI, &t821_0_0_0, RuntimeInvoker_t821, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17884_GM};
static MethodInfo* t3216_MIs[] =
{
	&m17880_MI,
	&m17881_MI,
	&m17882_MI,
	&m17883_MI,
	&m17884_MI,
	NULL
};
static MethodInfo* t3216_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17881_MI,
	&m17883_MI,
	&m17882_MI,
	&m17884_MI,
};
static TypeInfo* t3216_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4578_TI,
};
static Il2CppInterfaceOffsetPair t3216_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4578_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3216_0_0_0;
extern Il2CppType t3216_1_0_0;
extern Il2CppGenericClass t3216_GC;
TypeInfo t3216_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3216_MIs, t3216_PIs, t3216_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3216_TI, t3216_ITIs, t3216_VT, &EmptyCustomAttributesCache, &t3216_TI, &t3216_0_0_0, &t3216_1_0_0, t3216_IOs, &t3216_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3216)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5895_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509KeyUsageFlags>
extern MethodInfo m30798_MI;
static PropertyInfo t5895____Count_PropertyInfo = 
{
	&t5895_TI, "Count", &m30798_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30799_MI;
static PropertyInfo t5895____IsReadOnly_PropertyInfo = 
{
	&t5895_TI, "IsReadOnly", &m30799_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5895_PIs[] =
{
	&t5895____Count_PropertyInfo,
	&t5895____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30798_GM;
MethodInfo m30798_MI = 
{
	"get_Count", NULL, &t5895_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30798_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30799_GM;
MethodInfo m30799_MI = 
{
	"get_IsReadOnly", NULL, &t5895_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30799_GM};
extern Il2CppType t821_0_0_0;
extern Il2CppType t821_0_0_0;
static ParameterInfo t5895_m30800_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t821_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30800_GM;
MethodInfo m30800_MI = 
{
	"Add", NULL, &t5895_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5895_m30800_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30800_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30801_GM;
MethodInfo m30801_MI = 
{
	"Clear", NULL, &t5895_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30801_GM};
extern Il2CppType t821_0_0_0;
static ParameterInfo t5895_m30802_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t821_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30802_GM;
MethodInfo m30802_MI = 
{
	"Contains", NULL, &t5895_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5895_m30802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30802_GM};
extern Il2CppType t3910_0_0_0;
extern Il2CppType t3910_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5895_m30803_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3910_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30803_GM;
MethodInfo m30803_MI = 
{
	"CopyTo", NULL, &t5895_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5895_m30803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30803_GM};
extern Il2CppType t821_0_0_0;
static ParameterInfo t5895_m30804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t821_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30804_GM;
MethodInfo m30804_MI = 
{
	"Remove", NULL, &t5895_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5895_m30804_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30804_GM};
static MethodInfo* t5895_MIs[] =
{
	&m30798_MI,
	&m30799_MI,
	&m30800_MI,
	&m30801_MI,
	&m30802_MI,
	&m30803_MI,
	&m30804_MI,
	NULL
};
extern TypeInfo t5897_TI;
static TypeInfo* t5895_ITIs[] = 
{
	&t603_TI,
	&t5897_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5895_0_0_0;
extern Il2CppType t5895_1_0_0;
struct t5895;
extern Il2CppGenericClass t5895_GC;
TypeInfo t5895_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5895_MIs, t5895_PIs, NULL, NULL, NULL, NULL, NULL, &t5895_TI, t5895_ITIs, NULL, &EmptyCustomAttributesCache, &t5895_TI, &t5895_0_0_0, &t5895_1_0_0, NULL, &t5895_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509KeyUsageFlags>
extern Il2CppType t4578_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30805_GM;
MethodInfo m30805_MI = 
{
	"GetEnumerator", NULL, &t5897_TI, &t4578_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30805_GM};
static MethodInfo* t5897_MIs[] =
{
	&m30805_MI,
	NULL
};
static TypeInfo* t5897_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5897_0_0_0;
extern Il2CppType t5897_1_0_0;
struct t5897;
extern Il2CppGenericClass t5897_GC;
TypeInfo t5897_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5897_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5897_TI, t5897_ITIs, NULL, &EmptyCustomAttributesCache, &t5897_TI, &t5897_0_0_0, &t5897_1_0_0, NULL, &t5897_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5896_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509KeyUsageFlags>
extern MethodInfo m30806_MI;
extern MethodInfo m30807_MI;
static PropertyInfo t5896____Item_PropertyInfo = 
{
	&t5896_TI, "Item", &m30806_MI, &m30807_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5896_PIs[] =
{
	&t5896____Item_PropertyInfo,
	NULL
};
extern Il2CppType t821_0_0_0;
static ParameterInfo t5896_m30808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t821_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30808_GM;
MethodInfo m30808_MI = 
{
	"IndexOf", NULL, &t5896_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5896_m30808_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30808_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t821_0_0_0;
static ParameterInfo t5896_m30809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t821_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30809_GM;
MethodInfo m30809_MI = 
{
	"Insert", NULL, &t5896_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5896_m30809_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30809_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5896_m30810_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30810_GM;
MethodInfo m30810_MI = 
{
	"RemoveAt", NULL, &t5896_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5896_m30810_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30810_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5896_m30806_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t821_0_0_0;
extern void* RuntimeInvoker_t821_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30806_GM;
MethodInfo m30806_MI = 
{
	"get_Item", NULL, &t5896_TI, &t821_0_0_0, RuntimeInvoker_t821_t44, t5896_m30806_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30806_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t821_0_0_0;
static ParameterInfo t5896_m30807_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t821_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30807_GM;
MethodInfo m30807_MI = 
{
	"set_Item", NULL, &t5896_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5896_m30807_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30807_GM};
static MethodInfo* t5896_MIs[] =
{
	&m30808_MI,
	&m30809_MI,
	&m30810_MI,
	&m30806_MI,
	&m30807_MI,
	NULL
};
static TypeInfo* t5896_ITIs[] = 
{
	&t603_TI,
	&t5895_TI,
	&t5897_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5896_0_0_0;
extern Il2CppType t5896_1_0_0;
struct t5896;
extern Il2CppGenericClass t5896_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5896_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5896_MIs, t5896_PIs, NULL, NULL, NULL, NULL, NULL, &t5896_TI, t5896_ITIs, NULL, &t1908__CustomAttributeCache, &t5896_TI, &t5896_0_0_0, &t5896_1_0_0, NULL, &t5896_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4580_TI;

#include "t794.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>
extern MethodInfo m30811_MI;
static PropertyInfo t4580____Current_PropertyInfo = 
{
	&t4580_TI, "Current", &m30811_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4580_PIs[] =
{
	&t4580____Current_PropertyInfo,
	NULL
};
extern Il2CppType t794_0_0_0;
extern void* RuntimeInvoker_t794 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30811_GM;
MethodInfo m30811_MI = 
{
	"get_Current", NULL, &t4580_TI, &t794_0_0_0, RuntimeInvoker_t794, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30811_GM};
static MethodInfo* t4580_MIs[] =
{
	&m30811_MI,
	NULL
};
static TypeInfo* t4580_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4580_0_0_0;
extern Il2CppType t4580_1_0_0;
struct t4580;
extern Il2CppGenericClass t4580_GC;
TypeInfo t4580_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4580_MIs, t4580_PIs, NULL, NULL, NULL, NULL, NULL, &t4580_TI, t4580_ITIs, NULL, &EmptyCustomAttributesCache, &t4580_TI, &t4580_0_0_0, &t4580_1_0_0, NULL, &t4580_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3217.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3217_TI;
#include "t3217MD.h"

extern TypeInfo t794_TI;
extern MethodInfo m17889_MI;
extern MethodInfo m23507_MI;
struct t20;
 int32_t m23507 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17885_MI;
 void m17885 (t3217 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17886_MI;
 t29 * m17886 (t3217 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17889(__this, &m17889_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t794_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17887_MI;
 void m17887 (t3217 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17888_MI;
 bool m17888 (t3217 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17889 (t3217 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23507(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23507_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>
extern Il2CppType t20_0_0_1;
FieldInfo t3217_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3217_TI, offsetof(t3217, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3217_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3217_TI, offsetof(t3217, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3217_FIs[] =
{
	&t3217_f0_FieldInfo,
	&t3217_f1_FieldInfo,
	NULL
};
static PropertyInfo t3217____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3217_TI, "System.Collections.IEnumerator.Current", &m17886_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3217____Current_PropertyInfo = 
{
	&t3217_TI, "Current", &m17889_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3217_PIs[] =
{
	&t3217____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3217____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3217_m17885_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17885_GM;
MethodInfo m17885_MI = 
{
	".ctor", (methodPointerType)&m17885, &t3217_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3217_m17885_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17885_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17886_GM;
MethodInfo m17886_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17886, &t3217_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17886_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17887_GM;
MethodInfo m17887_MI = 
{
	"Dispose", (methodPointerType)&m17887, &t3217_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17887_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17888_GM;
MethodInfo m17888_MI = 
{
	"MoveNext", (methodPointerType)&m17888, &t3217_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17888_GM};
extern Il2CppType t794_0_0_0;
extern void* RuntimeInvoker_t794 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17889_GM;
MethodInfo m17889_MI = 
{
	"get_Current", (methodPointerType)&m17889, &t3217_TI, &t794_0_0_0, RuntimeInvoker_t794, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17889_GM};
static MethodInfo* t3217_MIs[] =
{
	&m17885_MI,
	&m17886_MI,
	&m17887_MI,
	&m17888_MI,
	&m17889_MI,
	NULL
};
static MethodInfo* t3217_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17886_MI,
	&m17888_MI,
	&m17887_MI,
	&m17889_MI,
};
static TypeInfo* t3217_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4580_TI,
};
static Il2CppInterfaceOffsetPair t3217_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4580_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3217_0_0_0;
extern Il2CppType t3217_1_0_0;
extern Il2CppGenericClass t3217_GC;
TypeInfo t3217_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3217_MIs, t3217_PIs, t3217_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3217_TI, t3217_ITIs, t3217_VT, &EmptyCustomAttributesCache, &t3217_TI, &t3217_0_0_0, &t3217_1_0_0, t3217_IOs, &t3217_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3217)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5898_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509NameType>
extern MethodInfo m30812_MI;
static PropertyInfo t5898____Count_PropertyInfo = 
{
	&t5898_TI, "Count", &m30812_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30813_MI;
static PropertyInfo t5898____IsReadOnly_PropertyInfo = 
{
	&t5898_TI, "IsReadOnly", &m30813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5898_PIs[] =
{
	&t5898____Count_PropertyInfo,
	&t5898____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30812_GM;
MethodInfo m30812_MI = 
{
	"get_Count", NULL, &t5898_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30812_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30813_GM;
MethodInfo m30813_MI = 
{
	"get_IsReadOnly", NULL, &t5898_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30813_GM};
extern Il2CppType t794_0_0_0;
extern Il2CppType t794_0_0_0;
static ParameterInfo t5898_m30814_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t794_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30814_GM;
MethodInfo m30814_MI = 
{
	"Add", NULL, &t5898_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5898_m30814_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30814_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30815_GM;
MethodInfo m30815_MI = 
{
	"Clear", NULL, &t5898_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30815_GM};
extern Il2CppType t794_0_0_0;
static ParameterInfo t5898_m30816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t794_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30816_GM;
MethodInfo m30816_MI = 
{
	"Contains", NULL, &t5898_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5898_m30816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30816_GM};
extern Il2CppType t3911_0_0_0;
extern Il2CppType t3911_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5898_m30817_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3911_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30817_GM;
MethodInfo m30817_MI = 
{
	"CopyTo", NULL, &t5898_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5898_m30817_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30817_GM};
extern Il2CppType t794_0_0_0;
static ParameterInfo t5898_m30818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t794_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30818_GM;
MethodInfo m30818_MI = 
{
	"Remove", NULL, &t5898_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5898_m30818_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30818_GM};
static MethodInfo* t5898_MIs[] =
{
	&m30812_MI,
	&m30813_MI,
	&m30814_MI,
	&m30815_MI,
	&m30816_MI,
	&m30817_MI,
	&m30818_MI,
	NULL
};
extern TypeInfo t5900_TI;
static TypeInfo* t5898_ITIs[] = 
{
	&t603_TI,
	&t5900_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5898_0_0_0;
extern Il2CppType t5898_1_0_0;
struct t5898;
extern Il2CppGenericClass t5898_GC;
TypeInfo t5898_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5898_MIs, t5898_PIs, NULL, NULL, NULL, NULL, NULL, &t5898_TI, t5898_ITIs, NULL, &EmptyCustomAttributesCache, &t5898_TI, &t5898_0_0_0, &t5898_1_0_0, NULL, &t5898_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509NameType>
extern Il2CppType t4580_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30819_GM;
MethodInfo m30819_MI = 
{
	"GetEnumerator", NULL, &t5900_TI, &t4580_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30819_GM};
static MethodInfo* t5900_MIs[] =
{
	&m30819_MI,
	NULL
};
static TypeInfo* t5900_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5900_0_0_0;
extern Il2CppType t5900_1_0_0;
struct t5900;
extern Il2CppGenericClass t5900_GC;
TypeInfo t5900_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5900_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5900_TI, t5900_ITIs, NULL, &EmptyCustomAttributesCache, &t5900_TI, &t5900_0_0_0, &t5900_1_0_0, NULL, &t5900_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5899_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509NameType>
extern MethodInfo m30820_MI;
extern MethodInfo m30821_MI;
static PropertyInfo t5899____Item_PropertyInfo = 
{
	&t5899_TI, "Item", &m30820_MI, &m30821_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5899_PIs[] =
{
	&t5899____Item_PropertyInfo,
	NULL
};
extern Il2CppType t794_0_0_0;
static ParameterInfo t5899_m30822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t794_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30822_GM;
MethodInfo m30822_MI = 
{
	"IndexOf", NULL, &t5899_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5899_m30822_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30822_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t794_0_0_0;
static ParameterInfo t5899_m30823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t794_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30823_GM;
MethodInfo m30823_MI = 
{
	"Insert", NULL, &t5899_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5899_m30823_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30823_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5899_m30824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30824_GM;
MethodInfo m30824_MI = 
{
	"RemoveAt", NULL, &t5899_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5899_m30824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30824_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5899_m30820_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t794_0_0_0;
extern void* RuntimeInvoker_t794_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30820_GM;
MethodInfo m30820_MI = 
{
	"get_Item", NULL, &t5899_TI, &t794_0_0_0, RuntimeInvoker_t794_t44, t5899_m30820_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30820_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t794_0_0_0;
static ParameterInfo t5899_m30821_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t794_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30821_GM;
MethodInfo m30821_MI = 
{
	"set_Item", NULL, &t5899_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5899_m30821_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30821_GM};
static MethodInfo* t5899_MIs[] =
{
	&m30822_MI,
	&m30823_MI,
	&m30824_MI,
	&m30820_MI,
	&m30821_MI,
	NULL
};
static TypeInfo* t5899_ITIs[] = 
{
	&t603_TI,
	&t5898_TI,
	&t5900_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5899_0_0_0;
extern Il2CppType t5899_1_0_0;
struct t5899;
extern Il2CppGenericClass t5899_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5899_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5899_MIs, t5899_PIs, NULL, NULL, NULL, NULL, NULL, &t5899_TI, t5899_ITIs, NULL, &t1908__CustomAttributeCache, &t5899_TI, &t5899_0_0_0, &t5899_1_0_0, NULL, &t5899_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4582_TI;

#include "t814.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509RevocationFlag>
extern MethodInfo m30825_MI;
static PropertyInfo t4582____Current_PropertyInfo = 
{
	&t4582_TI, "Current", &m30825_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4582_PIs[] =
{
	&t4582____Current_PropertyInfo,
	NULL
};
extern Il2CppType t814_0_0_0;
extern void* RuntimeInvoker_t814 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30825_GM;
MethodInfo m30825_MI = 
{
	"get_Current", NULL, &t4582_TI, &t814_0_0_0, RuntimeInvoker_t814, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30825_GM};
static MethodInfo* t4582_MIs[] =
{
	&m30825_MI,
	NULL
};
static TypeInfo* t4582_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4582_0_0_0;
extern Il2CppType t4582_1_0_0;
struct t4582;
extern Il2CppGenericClass t4582_GC;
TypeInfo t4582_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4582_MIs, t4582_PIs, NULL, NULL, NULL, NULL, NULL, &t4582_TI, t4582_ITIs, NULL, &EmptyCustomAttributesCache, &t4582_TI, &t4582_0_0_0, &t4582_1_0_0, NULL, &t4582_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3218.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3218_TI;
#include "t3218MD.h"

extern TypeInfo t814_TI;
extern MethodInfo m17894_MI;
extern MethodInfo m23518_MI;
struct t20;
 int32_t m23518 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17890_MI;
 void m17890 (t3218 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17891_MI;
 t29 * m17891 (t3218 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17894(__this, &m17894_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t814_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17892_MI;
 void m17892 (t3218 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17893_MI;
 bool m17893 (t3218 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17894 (t3218 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23518(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23518_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509RevocationFlag>
extern Il2CppType t20_0_0_1;
FieldInfo t3218_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3218_TI, offsetof(t3218, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3218_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3218_TI, offsetof(t3218, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3218_FIs[] =
{
	&t3218_f0_FieldInfo,
	&t3218_f1_FieldInfo,
	NULL
};
static PropertyInfo t3218____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3218_TI, "System.Collections.IEnumerator.Current", &m17891_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3218____Current_PropertyInfo = 
{
	&t3218_TI, "Current", &m17894_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3218_PIs[] =
{
	&t3218____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3218____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3218_m17890_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17890_GM;
MethodInfo m17890_MI = 
{
	".ctor", (methodPointerType)&m17890, &t3218_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3218_m17890_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17890_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17891_GM;
MethodInfo m17891_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17891, &t3218_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17891_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17892_GM;
MethodInfo m17892_MI = 
{
	"Dispose", (methodPointerType)&m17892, &t3218_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17892_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17893_GM;
MethodInfo m17893_MI = 
{
	"MoveNext", (methodPointerType)&m17893, &t3218_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17893_GM};
extern Il2CppType t814_0_0_0;
extern void* RuntimeInvoker_t814 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17894_GM;
MethodInfo m17894_MI = 
{
	"get_Current", (methodPointerType)&m17894, &t3218_TI, &t814_0_0_0, RuntimeInvoker_t814, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17894_GM};
static MethodInfo* t3218_MIs[] =
{
	&m17890_MI,
	&m17891_MI,
	&m17892_MI,
	&m17893_MI,
	&m17894_MI,
	NULL
};
static MethodInfo* t3218_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17891_MI,
	&m17893_MI,
	&m17892_MI,
	&m17894_MI,
};
static TypeInfo* t3218_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4582_TI,
};
static Il2CppInterfaceOffsetPair t3218_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4582_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3218_0_0_0;
extern Il2CppType t3218_1_0_0;
extern Il2CppGenericClass t3218_GC;
TypeInfo t3218_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3218_MIs, t3218_PIs, t3218_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3218_TI, t3218_ITIs, t3218_VT, &EmptyCustomAttributesCache, &t3218_TI, &t3218_0_0_0, &t3218_1_0_0, t3218_IOs, &t3218_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3218)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5901_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509RevocationFlag>
extern MethodInfo m30826_MI;
static PropertyInfo t5901____Count_PropertyInfo = 
{
	&t5901_TI, "Count", &m30826_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30827_MI;
static PropertyInfo t5901____IsReadOnly_PropertyInfo = 
{
	&t5901_TI, "IsReadOnly", &m30827_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5901_PIs[] =
{
	&t5901____Count_PropertyInfo,
	&t5901____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30826_GM;
MethodInfo m30826_MI = 
{
	"get_Count", NULL, &t5901_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30826_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30827_GM;
MethodInfo m30827_MI = 
{
	"get_IsReadOnly", NULL, &t5901_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30827_GM};
extern Il2CppType t814_0_0_0;
extern Il2CppType t814_0_0_0;
static ParameterInfo t5901_m30828_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t814_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30828_GM;
MethodInfo m30828_MI = 
{
	"Add", NULL, &t5901_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5901_m30828_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30828_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30829_GM;
MethodInfo m30829_MI = 
{
	"Clear", NULL, &t5901_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30829_GM};
extern Il2CppType t814_0_0_0;
static ParameterInfo t5901_m30830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t814_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30830_GM;
MethodInfo m30830_MI = 
{
	"Contains", NULL, &t5901_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5901_m30830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30830_GM};
extern Il2CppType t3912_0_0_0;
extern Il2CppType t3912_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5901_m30831_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3912_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30831_GM;
MethodInfo m30831_MI = 
{
	"CopyTo", NULL, &t5901_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5901_m30831_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30831_GM};
extern Il2CppType t814_0_0_0;
static ParameterInfo t5901_m30832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t814_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30832_GM;
MethodInfo m30832_MI = 
{
	"Remove", NULL, &t5901_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5901_m30832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30832_GM};
static MethodInfo* t5901_MIs[] =
{
	&m30826_MI,
	&m30827_MI,
	&m30828_MI,
	&m30829_MI,
	&m30830_MI,
	&m30831_MI,
	&m30832_MI,
	NULL
};
extern TypeInfo t5903_TI;
static TypeInfo* t5901_ITIs[] = 
{
	&t603_TI,
	&t5903_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5901_0_0_0;
extern Il2CppType t5901_1_0_0;
struct t5901;
extern Il2CppGenericClass t5901_GC;
TypeInfo t5901_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5901_MIs, t5901_PIs, NULL, NULL, NULL, NULL, NULL, &t5901_TI, t5901_ITIs, NULL, &EmptyCustomAttributesCache, &t5901_TI, &t5901_0_0_0, &t5901_1_0_0, NULL, &t5901_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509RevocationFlag>
extern Il2CppType t4582_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30833_GM;
MethodInfo m30833_MI = 
{
	"GetEnumerator", NULL, &t5903_TI, &t4582_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30833_GM};
static MethodInfo* t5903_MIs[] =
{
	&m30833_MI,
	NULL
};
static TypeInfo* t5903_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5903_0_0_0;
extern Il2CppType t5903_1_0_0;
struct t5903;
extern Il2CppGenericClass t5903_GC;
TypeInfo t5903_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5903_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5903_TI, t5903_ITIs, NULL, &EmptyCustomAttributesCache, &t5903_TI, &t5903_0_0_0, &t5903_1_0_0, NULL, &t5903_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5902_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509RevocationFlag>
extern MethodInfo m30834_MI;
extern MethodInfo m30835_MI;
static PropertyInfo t5902____Item_PropertyInfo = 
{
	&t5902_TI, "Item", &m30834_MI, &m30835_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5902_PIs[] =
{
	&t5902____Item_PropertyInfo,
	NULL
};
extern Il2CppType t814_0_0_0;
static ParameterInfo t5902_m30836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t814_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30836_GM;
MethodInfo m30836_MI = 
{
	"IndexOf", NULL, &t5902_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5902_m30836_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30836_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t814_0_0_0;
static ParameterInfo t5902_m30837_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t814_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30837_GM;
MethodInfo m30837_MI = 
{
	"Insert", NULL, &t5902_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5902_m30837_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30837_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5902_m30838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30838_GM;
MethodInfo m30838_MI = 
{
	"RemoveAt", NULL, &t5902_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5902_m30838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30838_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5902_m30834_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t814_0_0_0;
extern void* RuntimeInvoker_t814_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30834_GM;
MethodInfo m30834_MI = 
{
	"get_Item", NULL, &t5902_TI, &t814_0_0_0, RuntimeInvoker_t814_t44, t5902_m30834_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30834_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t814_0_0_0;
static ParameterInfo t5902_m30835_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t814_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30835_GM;
MethodInfo m30835_MI = 
{
	"set_Item", NULL, &t5902_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5902_m30835_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30835_GM};
static MethodInfo* t5902_MIs[] =
{
	&m30836_MI,
	&m30837_MI,
	&m30838_MI,
	&m30834_MI,
	&m30835_MI,
	NULL
};
static TypeInfo* t5902_ITIs[] = 
{
	&t603_TI,
	&t5901_TI,
	&t5903_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5902_0_0_0;
extern Il2CppType t5902_1_0_0;
struct t5902;
extern Il2CppGenericClass t5902_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5902_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5902_MIs, t5902_PIs, NULL, NULL, NULL, NULL, NULL, &t5902_TI, t5902_ITIs, NULL, &t1908__CustomAttributeCache, &t5902_TI, &t5902_0_0_0, &t5902_1_0_0, NULL, &t5902_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4584_TI;

#include "t815.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509RevocationMode>
extern MethodInfo m30839_MI;
static PropertyInfo t4584____Current_PropertyInfo = 
{
	&t4584_TI, "Current", &m30839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4584_PIs[] =
{
	&t4584____Current_PropertyInfo,
	NULL
};
extern Il2CppType t815_0_0_0;
extern void* RuntimeInvoker_t815 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30839_GM;
MethodInfo m30839_MI = 
{
	"get_Current", NULL, &t4584_TI, &t815_0_0_0, RuntimeInvoker_t815, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30839_GM};
static MethodInfo* t4584_MIs[] =
{
	&m30839_MI,
	NULL
};
static TypeInfo* t4584_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4584_0_0_0;
extern Il2CppType t4584_1_0_0;
struct t4584;
extern Il2CppGenericClass t4584_GC;
TypeInfo t4584_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4584_MIs, t4584_PIs, NULL, NULL, NULL, NULL, NULL, &t4584_TI, t4584_ITIs, NULL, &EmptyCustomAttributesCache, &t4584_TI, &t4584_0_0_0, &t4584_1_0_0, NULL, &t4584_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3219.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3219_TI;
#include "t3219MD.h"

extern TypeInfo t815_TI;
extern MethodInfo m17899_MI;
extern MethodInfo m23529_MI;
struct t20;
 int32_t m23529 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17895_MI;
 void m17895 (t3219 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17896_MI;
 t29 * m17896 (t3219 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17899(__this, &m17899_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t815_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17897_MI;
 void m17897 (t3219 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17898_MI;
 bool m17898 (t3219 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17899 (t3219 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23529(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23529_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509RevocationMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3219_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3219_TI, offsetof(t3219, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3219_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3219_TI, offsetof(t3219, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3219_FIs[] =
{
	&t3219_f0_FieldInfo,
	&t3219_f1_FieldInfo,
	NULL
};
static PropertyInfo t3219____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3219_TI, "System.Collections.IEnumerator.Current", &m17896_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3219____Current_PropertyInfo = 
{
	&t3219_TI, "Current", &m17899_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3219_PIs[] =
{
	&t3219____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3219____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3219_m17895_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17895_GM;
MethodInfo m17895_MI = 
{
	".ctor", (methodPointerType)&m17895, &t3219_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3219_m17895_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17895_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17896_GM;
MethodInfo m17896_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17896, &t3219_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17896_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17897_GM;
MethodInfo m17897_MI = 
{
	"Dispose", (methodPointerType)&m17897, &t3219_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17897_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17898_GM;
MethodInfo m17898_MI = 
{
	"MoveNext", (methodPointerType)&m17898, &t3219_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17898_GM};
extern Il2CppType t815_0_0_0;
extern void* RuntimeInvoker_t815 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17899_GM;
MethodInfo m17899_MI = 
{
	"get_Current", (methodPointerType)&m17899, &t3219_TI, &t815_0_0_0, RuntimeInvoker_t815, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17899_GM};
static MethodInfo* t3219_MIs[] =
{
	&m17895_MI,
	&m17896_MI,
	&m17897_MI,
	&m17898_MI,
	&m17899_MI,
	NULL
};
static MethodInfo* t3219_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17896_MI,
	&m17898_MI,
	&m17897_MI,
	&m17899_MI,
};
static TypeInfo* t3219_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4584_TI,
};
static Il2CppInterfaceOffsetPair t3219_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4584_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3219_0_0_0;
extern Il2CppType t3219_1_0_0;
extern Il2CppGenericClass t3219_GC;
TypeInfo t3219_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3219_MIs, t3219_PIs, t3219_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3219_TI, t3219_ITIs, t3219_VT, &EmptyCustomAttributesCache, &t3219_TI, &t3219_0_0_0, &t3219_1_0_0, t3219_IOs, &t3219_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3219)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5904_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509RevocationMode>
extern MethodInfo m30840_MI;
static PropertyInfo t5904____Count_PropertyInfo = 
{
	&t5904_TI, "Count", &m30840_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30841_MI;
static PropertyInfo t5904____IsReadOnly_PropertyInfo = 
{
	&t5904_TI, "IsReadOnly", &m30841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5904_PIs[] =
{
	&t5904____Count_PropertyInfo,
	&t5904____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30840_GM;
MethodInfo m30840_MI = 
{
	"get_Count", NULL, &t5904_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30840_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30841_GM;
MethodInfo m30841_MI = 
{
	"get_IsReadOnly", NULL, &t5904_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30841_GM};
extern Il2CppType t815_0_0_0;
extern Il2CppType t815_0_0_0;
static ParameterInfo t5904_m30842_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t815_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30842_GM;
MethodInfo m30842_MI = 
{
	"Add", NULL, &t5904_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5904_m30842_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30842_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30843_GM;
MethodInfo m30843_MI = 
{
	"Clear", NULL, &t5904_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30843_GM};
extern Il2CppType t815_0_0_0;
static ParameterInfo t5904_m30844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t815_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30844_GM;
MethodInfo m30844_MI = 
{
	"Contains", NULL, &t5904_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5904_m30844_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30844_GM};
extern Il2CppType t3913_0_0_0;
extern Il2CppType t3913_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5904_m30845_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3913_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30845_GM;
MethodInfo m30845_MI = 
{
	"CopyTo", NULL, &t5904_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5904_m30845_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30845_GM};
extern Il2CppType t815_0_0_0;
static ParameterInfo t5904_m30846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t815_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30846_GM;
MethodInfo m30846_MI = 
{
	"Remove", NULL, &t5904_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5904_m30846_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30846_GM};
static MethodInfo* t5904_MIs[] =
{
	&m30840_MI,
	&m30841_MI,
	&m30842_MI,
	&m30843_MI,
	&m30844_MI,
	&m30845_MI,
	&m30846_MI,
	NULL
};
extern TypeInfo t5906_TI;
static TypeInfo* t5904_ITIs[] = 
{
	&t603_TI,
	&t5906_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5904_0_0_0;
extern Il2CppType t5904_1_0_0;
struct t5904;
extern Il2CppGenericClass t5904_GC;
TypeInfo t5904_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5904_MIs, t5904_PIs, NULL, NULL, NULL, NULL, NULL, &t5904_TI, t5904_ITIs, NULL, &EmptyCustomAttributesCache, &t5904_TI, &t5904_0_0_0, &t5904_1_0_0, NULL, &t5904_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509RevocationMode>
extern Il2CppType t4584_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30847_GM;
MethodInfo m30847_MI = 
{
	"GetEnumerator", NULL, &t5906_TI, &t4584_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30847_GM};
static MethodInfo* t5906_MIs[] =
{
	&m30847_MI,
	NULL
};
static TypeInfo* t5906_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5906_0_0_0;
extern Il2CppType t5906_1_0_0;
struct t5906;
extern Il2CppGenericClass t5906_GC;
TypeInfo t5906_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5906_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5906_TI, t5906_ITIs, NULL, &EmptyCustomAttributesCache, &t5906_TI, &t5906_0_0_0, &t5906_1_0_0, NULL, &t5906_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5905_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509RevocationMode>
extern MethodInfo m30848_MI;
extern MethodInfo m30849_MI;
static PropertyInfo t5905____Item_PropertyInfo = 
{
	&t5905_TI, "Item", &m30848_MI, &m30849_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5905_PIs[] =
{
	&t5905____Item_PropertyInfo,
	NULL
};
extern Il2CppType t815_0_0_0;
static ParameterInfo t5905_m30850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t815_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30850_GM;
MethodInfo m30850_MI = 
{
	"IndexOf", NULL, &t5905_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5905_m30850_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30850_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t815_0_0_0;
static ParameterInfo t5905_m30851_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t815_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30851_GM;
MethodInfo m30851_MI = 
{
	"Insert", NULL, &t5905_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5905_m30851_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30851_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5905_m30852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30852_GM;
MethodInfo m30852_MI = 
{
	"RemoveAt", NULL, &t5905_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5905_m30852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30852_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5905_m30848_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t815_0_0_0;
extern void* RuntimeInvoker_t815_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30848_GM;
MethodInfo m30848_MI = 
{
	"get_Item", NULL, &t5905_TI, &t815_0_0_0, RuntimeInvoker_t815_t44, t5905_m30848_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30848_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t815_0_0_0;
static ParameterInfo t5905_m30849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t815_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30849_GM;
MethodInfo m30849_MI = 
{
	"set_Item", NULL, &t5905_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5905_m30849_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30849_GM};
static MethodInfo* t5905_MIs[] =
{
	&m30850_MI,
	&m30851_MI,
	&m30852_MI,
	&m30848_MI,
	&m30849_MI,
	NULL
};
static TypeInfo* t5905_ITIs[] = 
{
	&t603_TI,
	&t5904_TI,
	&t5906_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5905_0_0_0;
extern Il2CppType t5905_1_0_0;
struct t5905;
extern Il2CppGenericClass t5905_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5905_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5905_MIs, t5905_PIs, NULL, NULL, NULL, NULL, NULL, &t5905_TI, t5905_ITIs, NULL, &t1908__CustomAttributeCache, &t5905_TI, &t5905_0_0_0, &t5905_1_0_0, NULL, &t5905_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4586_TI;

#include "t825.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>
extern MethodInfo m30853_MI;
static PropertyInfo t4586____Current_PropertyInfo = 
{
	&t4586_TI, "Current", &m30853_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4586_PIs[] =
{
	&t4586____Current_PropertyInfo,
	NULL
};
extern Il2CppType t825_0_0_0;
extern void* RuntimeInvoker_t825 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30853_GM;
MethodInfo m30853_MI = 
{
	"get_Current", NULL, &t4586_TI, &t825_0_0_0, RuntimeInvoker_t825, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30853_GM};
static MethodInfo* t4586_MIs[] =
{
	&m30853_MI,
	NULL
};
static TypeInfo* t4586_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4586_0_0_0;
extern Il2CppType t4586_1_0_0;
struct t4586;
extern Il2CppGenericClass t4586_GC;
TypeInfo t4586_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4586_MIs, t4586_PIs, NULL, NULL, NULL, NULL, NULL, &t4586_TI, t4586_ITIs, NULL, &EmptyCustomAttributesCache, &t4586_TI, &t4586_0_0_0, &t4586_1_0_0, NULL, &t4586_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3220.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3220_TI;
#include "t3220MD.h"

extern TypeInfo t825_TI;
extern MethodInfo m17904_MI;
extern MethodInfo m23540_MI;
struct t20;
 int32_t m23540 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17900_MI;
 void m17900 (t3220 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17901_MI;
 t29 * m17901 (t3220 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17904(__this, &m17904_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t825_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17902_MI;
 void m17902 (t3220 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17903_MI;
 bool m17903 (t3220 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17904 (t3220 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23540(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23540_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>
extern Il2CppType t20_0_0_1;
FieldInfo t3220_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3220_TI, offsetof(t3220, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3220_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3220_TI, offsetof(t3220, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3220_FIs[] =
{
	&t3220_f0_FieldInfo,
	&t3220_f1_FieldInfo,
	NULL
};
static PropertyInfo t3220____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3220_TI, "System.Collections.IEnumerator.Current", &m17901_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3220____Current_PropertyInfo = 
{
	&t3220_TI, "Current", &m17904_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3220_PIs[] =
{
	&t3220____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3220____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3220_m17900_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17900_GM;
MethodInfo m17900_MI = 
{
	".ctor", (methodPointerType)&m17900, &t3220_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3220_m17900_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17900_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17901_GM;
MethodInfo m17901_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17901, &t3220_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17901_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17902_GM;
MethodInfo m17902_MI = 
{
	"Dispose", (methodPointerType)&m17902, &t3220_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17902_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17903_GM;
MethodInfo m17903_MI = 
{
	"MoveNext", (methodPointerType)&m17903, &t3220_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17903_GM};
extern Il2CppType t825_0_0_0;
extern void* RuntimeInvoker_t825 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17904_GM;
MethodInfo m17904_MI = 
{
	"get_Current", (methodPointerType)&m17904, &t3220_TI, &t825_0_0_0, RuntimeInvoker_t825, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17904_GM};
static MethodInfo* t3220_MIs[] =
{
	&m17900_MI,
	&m17901_MI,
	&m17902_MI,
	&m17903_MI,
	&m17904_MI,
	NULL
};
static MethodInfo* t3220_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17901_MI,
	&m17903_MI,
	&m17902_MI,
	&m17904_MI,
};
static TypeInfo* t3220_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4586_TI,
};
static Il2CppInterfaceOffsetPair t3220_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4586_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3220_0_0_0;
extern Il2CppType t3220_1_0_0;
extern Il2CppGenericClass t3220_GC;
TypeInfo t3220_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3220_MIs, t3220_PIs, t3220_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3220_TI, t3220_ITIs, t3220_VT, &EmptyCustomAttributesCache, &t3220_TI, &t3220_0_0_0, &t3220_1_0_0, t3220_IOs, &t3220_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3220)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5907_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>
extern MethodInfo m30854_MI;
static PropertyInfo t5907____Count_PropertyInfo = 
{
	&t5907_TI, "Count", &m30854_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30855_MI;
static PropertyInfo t5907____IsReadOnly_PropertyInfo = 
{
	&t5907_TI, "IsReadOnly", &m30855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5907_PIs[] =
{
	&t5907____Count_PropertyInfo,
	&t5907____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30854_GM;
MethodInfo m30854_MI = 
{
	"get_Count", NULL, &t5907_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30854_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30855_GM;
MethodInfo m30855_MI = 
{
	"get_IsReadOnly", NULL, &t5907_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30855_GM};
extern Il2CppType t825_0_0_0;
extern Il2CppType t825_0_0_0;
static ParameterInfo t5907_m30856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t825_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30856_GM;
MethodInfo m30856_MI = 
{
	"Add", NULL, &t5907_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5907_m30856_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30856_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30857_GM;
MethodInfo m30857_MI = 
{
	"Clear", NULL, &t5907_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30857_GM};
extern Il2CppType t825_0_0_0;
static ParameterInfo t5907_m30858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t825_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30858_GM;
MethodInfo m30858_MI = 
{
	"Contains", NULL, &t5907_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5907_m30858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30858_GM};
extern Il2CppType t3914_0_0_0;
extern Il2CppType t3914_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5907_m30859_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3914_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30859_GM;
MethodInfo m30859_MI = 
{
	"CopyTo", NULL, &t5907_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5907_m30859_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30859_GM};
extern Il2CppType t825_0_0_0;
static ParameterInfo t5907_m30860_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t825_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30860_GM;
MethodInfo m30860_MI = 
{
	"Remove", NULL, &t5907_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5907_m30860_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30860_GM};
static MethodInfo* t5907_MIs[] =
{
	&m30854_MI,
	&m30855_MI,
	&m30856_MI,
	&m30857_MI,
	&m30858_MI,
	&m30859_MI,
	&m30860_MI,
	NULL
};
extern TypeInfo t5909_TI;
static TypeInfo* t5907_ITIs[] = 
{
	&t603_TI,
	&t5909_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5907_0_0_0;
extern Il2CppType t5907_1_0_0;
struct t5907;
extern Il2CppGenericClass t5907_GC;
TypeInfo t5907_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5907_MIs, t5907_PIs, NULL, NULL, NULL, NULL, NULL, &t5907_TI, t5907_ITIs, NULL, &EmptyCustomAttributesCache, &t5907_TI, &t5907_0_0_0, &t5907_1_0_0, NULL, &t5907_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>
extern Il2CppType t4586_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30861_GM;
MethodInfo m30861_MI = 
{
	"GetEnumerator", NULL, &t5909_TI, &t4586_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30861_GM};
static MethodInfo* t5909_MIs[] =
{
	&m30861_MI,
	NULL
};
static TypeInfo* t5909_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5909_0_0_0;
extern Il2CppType t5909_1_0_0;
struct t5909;
extern Il2CppGenericClass t5909_GC;
TypeInfo t5909_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5909_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5909_TI, t5909_ITIs, NULL, &EmptyCustomAttributesCache, &t5909_TI, &t5909_0_0_0, &t5909_1_0_0, NULL, &t5909_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5908_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>
extern MethodInfo m30862_MI;
extern MethodInfo m30863_MI;
static PropertyInfo t5908____Item_PropertyInfo = 
{
	&t5908_TI, "Item", &m30862_MI, &m30863_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5908_PIs[] =
{
	&t5908____Item_PropertyInfo,
	NULL
};
extern Il2CppType t825_0_0_0;
static ParameterInfo t5908_m30864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t825_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30864_GM;
MethodInfo m30864_MI = 
{
	"IndexOf", NULL, &t5908_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5908_m30864_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30864_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t825_0_0_0;
static ParameterInfo t5908_m30865_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t825_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30865_GM;
MethodInfo m30865_MI = 
{
	"Insert", NULL, &t5908_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5908_m30865_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30865_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5908_m30866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30866_GM;
MethodInfo m30866_MI = 
{
	"RemoveAt", NULL, &t5908_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5908_m30866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30866_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5908_m30862_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t825_0_0_0;
extern void* RuntimeInvoker_t825_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30862_GM;
MethodInfo m30862_MI = 
{
	"get_Item", NULL, &t5908_TI, &t825_0_0_0, RuntimeInvoker_t825_t44, t5908_m30862_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30862_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t825_0_0_0;
static ParameterInfo t5908_m30863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t825_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30863_GM;
MethodInfo m30863_MI = 
{
	"set_Item", NULL, &t5908_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5908_m30863_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30863_GM};
static MethodInfo* t5908_MIs[] =
{
	&m30864_MI,
	&m30865_MI,
	&m30866_MI,
	&m30862_MI,
	&m30863_MI,
	NULL
};
static TypeInfo* t5908_ITIs[] = 
{
	&t603_TI,
	&t5907_TI,
	&t5909_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5908_0_0_0;
extern Il2CppType t5908_1_0_0;
struct t5908;
extern Il2CppGenericClass t5908_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5908_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5908_MIs, t5908_PIs, NULL, NULL, NULL, NULL, NULL, &t5908_TI, t5908_ITIs, NULL, &t1908__CustomAttributeCache, &t5908_TI, &t5908_0_0_0, &t5908_1_0_0, NULL, &t5908_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4588_TI;

#include "t817.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509VerificationFlags>
extern MethodInfo m30867_MI;
static PropertyInfo t4588____Current_PropertyInfo = 
{
	&t4588_TI, "Current", &m30867_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4588_PIs[] =
{
	&t4588____Current_PropertyInfo,
	NULL
};
extern Il2CppType t817_0_0_0;
extern void* RuntimeInvoker_t817 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30867_GM;
MethodInfo m30867_MI = 
{
	"get_Current", NULL, &t4588_TI, &t817_0_0_0, RuntimeInvoker_t817, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30867_GM};
static MethodInfo* t4588_MIs[] =
{
	&m30867_MI,
	NULL
};
static TypeInfo* t4588_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4588_0_0_0;
extern Il2CppType t4588_1_0_0;
struct t4588;
extern Il2CppGenericClass t4588_GC;
TypeInfo t4588_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4588_MIs, t4588_PIs, NULL, NULL, NULL, NULL, NULL, &t4588_TI, t4588_ITIs, NULL, &EmptyCustomAttributesCache, &t4588_TI, &t4588_0_0_0, &t4588_1_0_0, NULL, &t4588_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3221.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3221_TI;
#include "t3221MD.h"

extern TypeInfo t817_TI;
extern MethodInfo m17909_MI;
extern MethodInfo m23551_MI;
struct t20;
 int32_t m23551 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17905_MI;
 void m17905 (t3221 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17906_MI;
 t29 * m17906 (t3221 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17909(__this, &m17909_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t817_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17907_MI;
 void m17907 (t3221 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17908_MI;
 bool m17908 (t3221 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17909 (t3221 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23551(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23551_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509VerificationFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3221_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3221_TI, offsetof(t3221, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3221_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3221_TI, offsetof(t3221, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3221_FIs[] =
{
	&t3221_f0_FieldInfo,
	&t3221_f1_FieldInfo,
	NULL
};
static PropertyInfo t3221____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3221_TI, "System.Collections.IEnumerator.Current", &m17906_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3221____Current_PropertyInfo = 
{
	&t3221_TI, "Current", &m17909_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3221_PIs[] =
{
	&t3221____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3221____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3221_m17905_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17905_GM;
MethodInfo m17905_MI = 
{
	".ctor", (methodPointerType)&m17905, &t3221_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3221_m17905_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17905_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17906_GM;
MethodInfo m17906_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17906, &t3221_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17906_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17907_GM;
MethodInfo m17907_MI = 
{
	"Dispose", (methodPointerType)&m17907, &t3221_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17907_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17908_GM;
MethodInfo m17908_MI = 
{
	"MoveNext", (methodPointerType)&m17908, &t3221_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17908_GM};
extern Il2CppType t817_0_0_0;
extern void* RuntimeInvoker_t817 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17909_GM;
MethodInfo m17909_MI = 
{
	"get_Current", (methodPointerType)&m17909, &t3221_TI, &t817_0_0_0, RuntimeInvoker_t817, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17909_GM};
static MethodInfo* t3221_MIs[] =
{
	&m17905_MI,
	&m17906_MI,
	&m17907_MI,
	&m17908_MI,
	&m17909_MI,
	NULL
};
static MethodInfo* t3221_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17906_MI,
	&m17908_MI,
	&m17907_MI,
	&m17909_MI,
};
static TypeInfo* t3221_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4588_TI,
};
static Il2CppInterfaceOffsetPair t3221_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4588_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3221_0_0_0;
extern Il2CppType t3221_1_0_0;
extern Il2CppGenericClass t3221_GC;
TypeInfo t3221_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3221_MIs, t3221_PIs, t3221_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3221_TI, t3221_ITIs, t3221_VT, &EmptyCustomAttributesCache, &t3221_TI, &t3221_0_0_0, &t3221_1_0_0, t3221_IOs, &t3221_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3221)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5910_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509VerificationFlags>
extern MethodInfo m30868_MI;
static PropertyInfo t5910____Count_PropertyInfo = 
{
	&t5910_TI, "Count", &m30868_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30869_MI;
static PropertyInfo t5910____IsReadOnly_PropertyInfo = 
{
	&t5910_TI, "IsReadOnly", &m30869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5910_PIs[] =
{
	&t5910____Count_PropertyInfo,
	&t5910____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30868_GM;
MethodInfo m30868_MI = 
{
	"get_Count", NULL, &t5910_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30868_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30869_GM;
MethodInfo m30869_MI = 
{
	"get_IsReadOnly", NULL, &t5910_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30869_GM};
extern Il2CppType t817_0_0_0;
extern Il2CppType t817_0_0_0;
static ParameterInfo t5910_m30870_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t817_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30870_GM;
MethodInfo m30870_MI = 
{
	"Add", NULL, &t5910_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5910_m30870_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30870_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30871_GM;
MethodInfo m30871_MI = 
{
	"Clear", NULL, &t5910_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30871_GM};
extern Il2CppType t817_0_0_0;
static ParameterInfo t5910_m30872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t817_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30872_GM;
MethodInfo m30872_MI = 
{
	"Contains", NULL, &t5910_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5910_m30872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30872_GM};
extern Il2CppType t3915_0_0_0;
extern Il2CppType t3915_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5910_m30873_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3915_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30873_GM;
MethodInfo m30873_MI = 
{
	"CopyTo", NULL, &t5910_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5910_m30873_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30873_GM};
extern Il2CppType t817_0_0_0;
static ParameterInfo t5910_m30874_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t817_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30874_GM;
MethodInfo m30874_MI = 
{
	"Remove", NULL, &t5910_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5910_m30874_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30874_GM};
static MethodInfo* t5910_MIs[] =
{
	&m30868_MI,
	&m30869_MI,
	&m30870_MI,
	&m30871_MI,
	&m30872_MI,
	&m30873_MI,
	&m30874_MI,
	NULL
};
extern TypeInfo t5912_TI;
static TypeInfo* t5910_ITIs[] = 
{
	&t603_TI,
	&t5912_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5910_0_0_0;
extern Il2CppType t5910_1_0_0;
struct t5910;
extern Il2CppGenericClass t5910_GC;
TypeInfo t5910_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5910_MIs, t5910_PIs, NULL, NULL, NULL, NULL, NULL, &t5910_TI, t5910_ITIs, NULL, &EmptyCustomAttributesCache, &t5910_TI, &t5910_0_0_0, &t5910_1_0_0, NULL, &t5910_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509VerificationFlags>
extern Il2CppType t4588_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30875_GM;
MethodInfo m30875_MI = 
{
	"GetEnumerator", NULL, &t5912_TI, &t4588_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30875_GM};
static MethodInfo* t5912_MIs[] =
{
	&m30875_MI,
	NULL
};
static TypeInfo* t5912_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5912_0_0_0;
extern Il2CppType t5912_1_0_0;
struct t5912;
extern Il2CppGenericClass t5912_GC;
TypeInfo t5912_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5912_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5912_TI, t5912_ITIs, NULL, &EmptyCustomAttributesCache, &t5912_TI, &t5912_0_0_0, &t5912_1_0_0, NULL, &t5912_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5911_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509VerificationFlags>
extern MethodInfo m30876_MI;
extern MethodInfo m30877_MI;
static PropertyInfo t5911____Item_PropertyInfo = 
{
	&t5911_TI, "Item", &m30876_MI, &m30877_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5911_PIs[] =
{
	&t5911____Item_PropertyInfo,
	NULL
};
extern Il2CppType t817_0_0_0;
static ParameterInfo t5911_m30878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t817_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30878_GM;
MethodInfo m30878_MI = 
{
	"IndexOf", NULL, &t5911_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5911_m30878_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30878_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t817_0_0_0;
static ParameterInfo t5911_m30879_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t817_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30879_GM;
MethodInfo m30879_MI = 
{
	"Insert", NULL, &t5911_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5911_m30879_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30879_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5911_m30880_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30880_GM;
MethodInfo m30880_MI = 
{
	"RemoveAt", NULL, &t5911_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5911_m30880_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30880_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5911_m30876_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t817_0_0_0;
extern void* RuntimeInvoker_t817_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30876_GM;
MethodInfo m30876_MI = 
{
	"get_Item", NULL, &t5911_TI, &t817_0_0_0, RuntimeInvoker_t817_t44, t5911_m30876_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30876_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t817_0_0_0;
static ParameterInfo t5911_m30877_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t817_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30877_GM;
MethodInfo m30877_MI = 
{
	"set_Item", NULL, &t5911_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5911_m30877_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30877_GM};
static MethodInfo* t5911_MIs[] =
{
	&m30878_MI,
	&m30879_MI,
	&m30880_MI,
	&m30876_MI,
	&m30877_MI,
	NULL
};
static TypeInfo* t5911_ITIs[] = 
{
	&t603_TI,
	&t5910_TI,
	&t5912_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5911_0_0_0;
extern Il2CppType t5911_1_0_0;
struct t5911;
extern Il2CppGenericClass t5911_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5911_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5911_MIs, t5911_PIs, NULL, NULL, NULL, NULL, NULL, &t5911_TI, t5911_ITIs, NULL, &t1908__CustomAttributeCache, &t5911_TI, &t5911_0_0_0, &t5911_1_0_0, NULL, &t5911_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4590_TI;

#include "t790.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.AsnDecodeStatus>
extern MethodInfo m30881_MI;
static PropertyInfo t4590____Current_PropertyInfo = 
{
	&t4590_TI, "Current", &m30881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4590_PIs[] =
{
	&t4590____Current_PropertyInfo,
	NULL
};
extern Il2CppType t790_0_0_0;
extern void* RuntimeInvoker_t790 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30881_GM;
MethodInfo m30881_MI = 
{
	"get_Current", NULL, &t4590_TI, &t790_0_0_0, RuntimeInvoker_t790, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30881_GM};
static MethodInfo* t4590_MIs[] =
{
	&m30881_MI,
	NULL
};
static TypeInfo* t4590_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4590_0_0_0;
extern Il2CppType t4590_1_0_0;
struct t4590;
extern Il2CppGenericClass t4590_GC;
TypeInfo t4590_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4590_MIs, t4590_PIs, NULL, NULL, NULL, NULL, NULL, &t4590_TI, t4590_ITIs, NULL, &EmptyCustomAttributesCache, &t4590_TI, &t4590_0_0_0, &t4590_1_0_0, NULL, &t4590_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3222.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3222_TI;
#include "t3222MD.h"

extern TypeInfo t790_TI;
extern MethodInfo m17914_MI;
extern MethodInfo m23562_MI;
struct t20;
 int32_t m23562 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17910_MI;
 void m17910 (t3222 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17911_MI;
 t29 * m17911 (t3222 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17914(__this, &m17914_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t790_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17912_MI;
 void m17912 (t3222 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17913_MI;
 bool m17913 (t3222 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17914 (t3222 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23562(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23562_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.AsnDecodeStatus>
extern Il2CppType t20_0_0_1;
FieldInfo t3222_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3222_TI, offsetof(t3222, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3222_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3222_TI, offsetof(t3222, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3222_FIs[] =
{
	&t3222_f0_FieldInfo,
	&t3222_f1_FieldInfo,
	NULL
};
static PropertyInfo t3222____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3222_TI, "System.Collections.IEnumerator.Current", &m17911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3222____Current_PropertyInfo = 
{
	&t3222_TI, "Current", &m17914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3222_PIs[] =
{
	&t3222____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3222____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3222_m17910_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17910_GM;
MethodInfo m17910_MI = 
{
	".ctor", (methodPointerType)&m17910, &t3222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3222_m17910_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17910_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17911_GM;
MethodInfo m17911_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17911, &t3222_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17911_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17912_GM;
MethodInfo m17912_MI = 
{
	"Dispose", (methodPointerType)&m17912, &t3222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17912_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17913_GM;
MethodInfo m17913_MI = 
{
	"MoveNext", (methodPointerType)&m17913, &t3222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17913_GM};
extern Il2CppType t790_0_0_0;
extern void* RuntimeInvoker_t790 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17914_GM;
MethodInfo m17914_MI = 
{
	"get_Current", (methodPointerType)&m17914, &t3222_TI, &t790_0_0_0, RuntimeInvoker_t790, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17914_GM};
static MethodInfo* t3222_MIs[] =
{
	&m17910_MI,
	&m17911_MI,
	&m17912_MI,
	&m17913_MI,
	&m17914_MI,
	NULL
};
static MethodInfo* t3222_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17911_MI,
	&m17913_MI,
	&m17912_MI,
	&m17914_MI,
};
static TypeInfo* t3222_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4590_TI,
};
static Il2CppInterfaceOffsetPair t3222_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4590_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3222_0_0_0;
extern Il2CppType t3222_1_0_0;
extern Il2CppGenericClass t3222_GC;
TypeInfo t3222_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3222_MIs, t3222_PIs, t3222_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3222_TI, t3222_ITIs, t3222_VT, &EmptyCustomAttributesCache, &t3222_TI, &t3222_0_0_0, &t3222_1_0_0, t3222_IOs, &t3222_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3222)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5913_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.AsnDecodeStatus>
extern MethodInfo m30882_MI;
static PropertyInfo t5913____Count_PropertyInfo = 
{
	&t5913_TI, "Count", &m30882_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30883_MI;
static PropertyInfo t5913____IsReadOnly_PropertyInfo = 
{
	&t5913_TI, "IsReadOnly", &m30883_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5913_PIs[] =
{
	&t5913____Count_PropertyInfo,
	&t5913____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30882_GM;
MethodInfo m30882_MI = 
{
	"get_Count", NULL, &t5913_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30882_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30883_GM;
MethodInfo m30883_MI = 
{
	"get_IsReadOnly", NULL, &t5913_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30883_GM};
extern Il2CppType t790_0_0_0;
extern Il2CppType t790_0_0_0;
static ParameterInfo t5913_m30884_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t790_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30884_GM;
MethodInfo m30884_MI = 
{
	"Add", NULL, &t5913_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5913_m30884_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30884_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30885_GM;
MethodInfo m30885_MI = 
{
	"Clear", NULL, &t5913_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30885_GM};
extern Il2CppType t790_0_0_0;
static ParameterInfo t5913_m30886_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t790_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30886_GM;
MethodInfo m30886_MI = 
{
	"Contains", NULL, &t5913_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5913_m30886_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30886_GM};
extern Il2CppType t3916_0_0_0;
extern Il2CppType t3916_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5913_m30887_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3916_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30887_GM;
MethodInfo m30887_MI = 
{
	"CopyTo", NULL, &t5913_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5913_m30887_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30887_GM};
extern Il2CppType t790_0_0_0;
static ParameterInfo t5913_m30888_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t790_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30888_GM;
MethodInfo m30888_MI = 
{
	"Remove", NULL, &t5913_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5913_m30888_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30888_GM};
static MethodInfo* t5913_MIs[] =
{
	&m30882_MI,
	&m30883_MI,
	&m30884_MI,
	&m30885_MI,
	&m30886_MI,
	&m30887_MI,
	&m30888_MI,
	NULL
};
extern TypeInfo t5915_TI;
static TypeInfo* t5913_ITIs[] = 
{
	&t603_TI,
	&t5915_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5913_0_0_0;
extern Il2CppType t5913_1_0_0;
struct t5913;
extern Il2CppGenericClass t5913_GC;
TypeInfo t5913_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5913_MIs, t5913_PIs, NULL, NULL, NULL, NULL, NULL, &t5913_TI, t5913_ITIs, NULL, &EmptyCustomAttributesCache, &t5913_TI, &t5913_0_0_0, &t5913_1_0_0, NULL, &t5913_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.AsnDecodeStatus>
extern Il2CppType t4590_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30889_GM;
MethodInfo m30889_MI = 
{
	"GetEnumerator", NULL, &t5915_TI, &t4590_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30889_GM};
static MethodInfo* t5915_MIs[] =
{
	&m30889_MI,
	NULL
};
static TypeInfo* t5915_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5915_0_0_0;
extern Il2CppType t5915_1_0_0;
struct t5915;
extern Il2CppGenericClass t5915_GC;
TypeInfo t5915_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5915_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5915_TI, t5915_ITIs, NULL, &EmptyCustomAttributesCache, &t5915_TI, &t5915_0_0_0, &t5915_1_0_0, NULL, &t5915_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5914_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.AsnDecodeStatus>
extern MethodInfo m30890_MI;
extern MethodInfo m30891_MI;
static PropertyInfo t5914____Item_PropertyInfo = 
{
	&t5914_TI, "Item", &m30890_MI, &m30891_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5914_PIs[] =
{
	&t5914____Item_PropertyInfo,
	NULL
};
extern Il2CppType t790_0_0_0;
static ParameterInfo t5914_m30892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t790_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30892_GM;
MethodInfo m30892_MI = 
{
	"IndexOf", NULL, &t5914_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5914_m30892_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30892_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t790_0_0_0;
static ParameterInfo t5914_m30893_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t790_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30893_GM;
MethodInfo m30893_MI = 
{
	"Insert", NULL, &t5914_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5914_m30893_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30893_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5914_m30894_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30894_GM;
MethodInfo m30894_MI = 
{
	"RemoveAt", NULL, &t5914_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5914_m30894_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30894_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5914_m30890_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t790_0_0_0;
extern void* RuntimeInvoker_t790_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30890_GM;
MethodInfo m30890_MI = 
{
	"get_Item", NULL, &t5914_TI, &t790_0_0_0, RuntimeInvoker_t790_t44, t5914_m30890_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30890_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t790_0_0_0;
static ParameterInfo t5914_m30891_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t790_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30891_GM;
MethodInfo m30891_MI = 
{
	"set_Item", NULL, &t5914_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5914_m30891_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30891_GM};
static MethodInfo* t5914_MIs[] =
{
	&m30892_MI,
	&m30893_MI,
	&m30894_MI,
	&m30890_MI,
	&m30891_MI,
	NULL
};
static TypeInfo* t5914_ITIs[] = 
{
	&t603_TI,
	&t5913_TI,
	&t5915_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5914_0_0_0;
extern Il2CppType t5914_1_0_0;
struct t5914;
extern Il2CppGenericClass t5914_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5914_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5914_MIs, t5914_PIs, NULL, NULL, NULL, NULL, NULL, &t5914_TI, t5914_ITIs, NULL, &t1908__CustomAttributeCache, &t5914_TI, &t5914_0_0_0, &t5914_1_0_0, NULL, &t5914_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4592_TI;

#include "t830.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Capture>
extern MethodInfo m30895_MI;
static PropertyInfo t4592____Current_PropertyInfo = 
{
	&t4592_TI, "Current", &m30895_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4592_PIs[] =
{
	&t4592____Current_PropertyInfo,
	NULL
};
extern Il2CppType t830_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30895_GM;
MethodInfo m30895_MI = 
{
	"get_Current", NULL, &t4592_TI, &t830_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30895_GM};
static MethodInfo* t4592_MIs[] =
{
	&m30895_MI,
	NULL
};
static TypeInfo* t4592_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4592_0_0_0;
extern Il2CppType t4592_1_0_0;
struct t4592;
extern Il2CppGenericClass t4592_GC;
TypeInfo t4592_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4592_MIs, t4592_PIs, NULL, NULL, NULL, NULL, NULL, &t4592_TI, t4592_ITIs, NULL, &EmptyCustomAttributesCache, &t4592_TI, &t4592_0_0_0, &t4592_1_0_0, NULL, &t4592_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3223.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3223_TI;
#include "t3223MD.h"

extern TypeInfo t830_TI;
extern MethodInfo m17919_MI;
extern MethodInfo m23573_MI;
struct t20;
#define m23573(__this, p0, method) (t830 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>
extern Il2CppType t20_0_0_1;
FieldInfo t3223_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3223_TI, offsetof(t3223, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3223_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3223_TI, offsetof(t3223, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3223_FIs[] =
{
	&t3223_f0_FieldInfo,
	&t3223_f1_FieldInfo,
	NULL
};
extern MethodInfo m17916_MI;
static PropertyInfo t3223____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3223_TI, "System.Collections.IEnumerator.Current", &m17916_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3223____Current_PropertyInfo = 
{
	&t3223_TI, "Current", &m17919_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3223_PIs[] =
{
	&t3223____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3223____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3223_m17915_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17915_GM;
MethodInfo m17915_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3223_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3223_m17915_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17915_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17916_GM;
MethodInfo m17916_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3223_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17916_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17917_GM;
MethodInfo m17917_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3223_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17917_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17918_GM;
MethodInfo m17918_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3223_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17918_GM};
extern Il2CppType t830_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17919_GM;
MethodInfo m17919_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3223_TI, &t830_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17919_GM};
static MethodInfo* t3223_MIs[] =
{
	&m17915_MI,
	&m17916_MI,
	&m17917_MI,
	&m17918_MI,
	&m17919_MI,
	NULL
};
extern MethodInfo m17918_MI;
extern MethodInfo m17917_MI;
static MethodInfo* t3223_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17916_MI,
	&m17918_MI,
	&m17917_MI,
	&m17919_MI,
};
static TypeInfo* t3223_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4592_TI,
};
static Il2CppInterfaceOffsetPair t3223_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4592_TI, 7},
};
extern TypeInfo t830_TI;
static Il2CppRGCTXData t3223_RGCTXData[3] = 
{
	&m17919_MI/* Method Usage */,
	&t830_TI/* Class Usage */,
	&m23573_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3223_0_0_0;
extern Il2CppType t3223_1_0_0;
extern Il2CppGenericClass t3223_GC;
TypeInfo t3223_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3223_MIs, t3223_PIs, t3223_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3223_TI, t3223_ITIs, t3223_VT, &EmptyCustomAttributesCache, &t3223_TI, &t3223_0_0_0, &t3223_1_0_0, t3223_IOs, &t3223_GC, NULL, NULL, NULL, t3223_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3223)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5916_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.Capture>
extern MethodInfo m30896_MI;
static PropertyInfo t5916____Count_PropertyInfo = 
{
	&t5916_TI, "Count", &m30896_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30897_MI;
static PropertyInfo t5916____IsReadOnly_PropertyInfo = 
{
	&t5916_TI, "IsReadOnly", &m30897_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5916_PIs[] =
{
	&t5916____Count_PropertyInfo,
	&t5916____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30896_GM;
MethodInfo m30896_MI = 
{
	"get_Count", NULL, &t5916_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30896_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30897_GM;
MethodInfo m30897_MI = 
{
	"get_IsReadOnly", NULL, &t5916_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30897_GM};
extern Il2CppType t830_0_0_0;
extern Il2CppType t830_0_0_0;
static ParameterInfo t5916_m30898_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t830_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30898_GM;
MethodInfo m30898_MI = 
{
	"Add", NULL, &t5916_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5916_m30898_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30898_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30899_GM;
MethodInfo m30899_MI = 
{
	"Clear", NULL, &t5916_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30899_GM};
extern Il2CppType t830_0_0_0;
static ParameterInfo t5916_m30900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t830_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30900_GM;
MethodInfo m30900_MI = 
{
	"Contains", NULL, &t5916_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5916_m30900_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30900_GM};
extern Il2CppType t832_0_0_0;
extern Il2CppType t832_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5916_m30901_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t832_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30901_GM;
MethodInfo m30901_MI = 
{
	"CopyTo", NULL, &t5916_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5916_m30901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30901_GM};
extern Il2CppType t830_0_0_0;
static ParameterInfo t5916_m30902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t830_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30902_GM;
MethodInfo m30902_MI = 
{
	"Remove", NULL, &t5916_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5916_m30902_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30902_GM};
static MethodInfo* t5916_MIs[] =
{
	&m30896_MI,
	&m30897_MI,
	&m30898_MI,
	&m30899_MI,
	&m30900_MI,
	&m30901_MI,
	&m30902_MI,
	NULL
};
extern TypeInfo t5918_TI;
static TypeInfo* t5916_ITIs[] = 
{
	&t603_TI,
	&t5918_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5916_0_0_0;
extern Il2CppType t5916_1_0_0;
struct t5916;
extern Il2CppGenericClass t5916_GC;
TypeInfo t5916_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5916_MIs, t5916_PIs, NULL, NULL, NULL, NULL, NULL, &t5916_TI, t5916_ITIs, NULL, &EmptyCustomAttributesCache, &t5916_TI, &t5916_0_0_0, &t5916_1_0_0, NULL, &t5916_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.Capture>
extern Il2CppType t4592_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30903_GM;
MethodInfo m30903_MI = 
{
	"GetEnumerator", NULL, &t5918_TI, &t4592_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30903_GM};
static MethodInfo* t5918_MIs[] =
{
	&m30903_MI,
	NULL
};
static TypeInfo* t5918_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5918_0_0_0;
extern Il2CppType t5918_1_0_0;
struct t5918;
extern Il2CppGenericClass t5918_GC;
TypeInfo t5918_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5918_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5918_TI, t5918_ITIs, NULL, &EmptyCustomAttributesCache, &t5918_TI, &t5918_0_0_0, &t5918_1_0_0, NULL, &t5918_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5917_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.Capture>
extern MethodInfo m30904_MI;
extern MethodInfo m30905_MI;
static PropertyInfo t5917____Item_PropertyInfo = 
{
	&t5917_TI, "Item", &m30904_MI, &m30905_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5917_PIs[] =
{
	&t5917____Item_PropertyInfo,
	NULL
};
extern Il2CppType t830_0_0_0;
static ParameterInfo t5917_m30906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t830_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30906_GM;
MethodInfo m30906_MI = 
{
	"IndexOf", NULL, &t5917_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5917_m30906_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30906_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t830_0_0_0;
static ParameterInfo t5917_m30907_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t830_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30907_GM;
MethodInfo m30907_MI = 
{
	"Insert", NULL, &t5917_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5917_m30907_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30907_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5917_m30908_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30908_GM;
MethodInfo m30908_MI = 
{
	"RemoveAt", NULL, &t5917_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5917_m30908_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30908_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5917_m30904_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t830_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30904_GM;
MethodInfo m30904_MI = 
{
	"get_Item", NULL, &t5917_TI, &t830_0_0_0, RuntimeInvoker_t29_t44, t5917_m30904_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30904_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t830_0_0_0;
static ParameterInfo t5917_m30905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t830_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30905_GM;
MethodInfo m30905_MI = 
{
	"set_Item", NULL, &t5917_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5917_m30905_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30905_GM};
static MethodInfo* t5917_MIs[] =
{
	&m30906_MI,
	&m30907_MI,
	&m30908_MI,
	&m30904_MI,
	&m30905_MI,
	NULL
};
static TypeInfo* t5917_ITIs[] = 
{
	&t603_TI,
	&t5916_TI,
	&t5918_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5917_0_0_0;
extern Il2CppType t5917_1_0_0;
struct t5917;
extern Il2CppGenericClass t5917_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5917_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5917_MIs, t5917_PIs, NULL, NULL, NULL, NULL, NULL, &t5917_TI, t5917_ITIs, NULL, &t1908__CustomAttributeCache, &t5917_TI, &t5917_0_0_0, &t5917_1_0_0, NULL, &t5917_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4594_TI;

#include "t833.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Group>
extern MethodInfo m30909_MI;
static PropertyInfo t4594____Current_PropertyInfo = 
{
	&t4594_TI, "Current", &m30909_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4594_PIs[] =
{
	&t4594____Current_PropertyInfo,
	NULL
};
extern Il2CppType t833_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30909_GM;
MethodInfo m30909_MI = 
{
	"get_Current", NULL, &t4594_TI, &t833_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30909_GM};
static MethodInfo* t4594_MIs[] =
{
	&m30909_MI,
	NULL
};
static TypeInfo* t4594_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4594_0_0_0;
extern Il2CppType t4594_1_0_0;
struct t4594;
extern Il2CppGenericClass t4594_GC;
TypeInfo t4594_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4594_MIs, t4594_PIs, NULL, NULL, NULL, NULL, NULL, &t4594_TI, t4594_ITIs, NULL, &EmptyCustomAttributesCache, &t4594_TI, &t4594_0_0_0, &t4594_1_0_0, NULL, &t4594_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3224.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3224_TI;
#include "t3224MD.h"

extern TypeInfo t833_TI;
extern MethodInfo m17924_MI;
extern MethodInfo m23584_MI;
struct t20;
#define m23584(__this, p0, method) (t833 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>
extern Il2CppType t20_0_0_1;
FieldInfo t3224_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3224_TI, offsetof(t3224, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3224_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3224_TI, offsetof(t3224, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3224_FIs[] =
{
	&t3224_f0_FieldInfo,
	&t3224_f1_FieldInfo,
	NULL
};
extern MethodInfo m17921_MI;
static PropertyInfo t3224____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3224_TI, "System.Collections.IEnumerator.Current", &m17921_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3224____Current_PropertyInfo = 
{
	&t3224_TI, "Current", &m17924_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3224_PIs[] =
{
	&t3224____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3224____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3224_m17920_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17920_GM;
MethodInfo m17920_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3224_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3224_m17920_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17920_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17921_GM;
MethodInfo m17921_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3224_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17921_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17922_GM;
MethodInfo m17922_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3224_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17922_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17923_GM;
MethodInfo m17923_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3224_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17923_GM};
extern Il2CppType t833_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17924_GM;
MethodInfo m17924_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3224_TI, &t833_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17924_GM};
static MethodInfo* t3224_MIs[] =
{
	&m17920_MI,
	&m17921_MI,
	&m17922_MI,
	&m17923_MI,
	&m17924_MI,
	NULL
};
extern MethodInfo m17923_MI;
extern MethodInfo m17922_MI;
static MethodInfo* t3224_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17921_MI,
	&m17923_MI,
	&m17922_MI,
	&m17924_MI,
};
static TypeInfo* t3224_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4594_TI,
};
static Il2CppInterfaceOffsetPair t3224_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4594_TI, 7},
};
extern TypeInfo t833_TI;
static Il2CppRGCTXData t3224_RGCTXData[3] = 
{
	&m17924_MI/* Method Usage */,
	&t833_TI/* Class Usage */,
	&m23584_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3224_0_0_0;
extern Il2CppType t3224_1_0_0;
extern Il2CppGenericClass t3224_GC;
TypeInfo t3224_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3224_MIs, t3224_PIs, t3224_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3224_TI, t3224_ITIs, t3224_VT, &EmptyCustomAttributesCache, &t3224_TI, &t3224_0_0_0, &t3224_1_0_0, t3224_IOs, &t3224_GC, NULL, NULL, NULL, t3224_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3224)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5919_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.Group>
extern MethodInfo m30910_MI;
static PropertyInfo t5919____Count_PropertyInfo = 
{
	&t5919_TI, "Count", &m30910_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30911_MI;
static PropertyInfo t5919____IsReadOnly_PropertyInfo = 
{
	&t5919_TI, "IsReadOnly", &m30911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5919_PIs[] =
{
	&t5919____Count_PropertyInfo,
	&t5919____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30910_GM;
MethodInfo m30910_MI = 
{
	"get_Count", NULL, &t5919_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30910_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30911_GM;
MethodInfo m30911_MI = 
{
	"get_IsReadOnly", NULL, &t5919_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30911_GM};
extern Il2CppType t833_0_0_0;
extern Il2CppType t833_0_0_0;
static ParameterInfo t5919_m30912_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t833_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30912_GM;
MethodInfo m30912_MI = 
{
	"Add", NULL, &t5919_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5919_m30912_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30912_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30913_GM;
MethodInfo m30913_MI = 
{
	"Clear", NULL, &t5919_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30913_GM};
extern Il2CppType t833_0_0_0;
static ParameterInfo t5919_m30914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t833_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30914_GM;
MethodInfo m30914_MI = 
{
	"Contains", NULL, &t5919_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5919_m30914_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30914_GM};
extern Il2CppType t835_0_0_0;
extern Il2CppType t835_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5919_m30915_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t835_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30915_GM;
MethodInfo m30915_MI = 
{
	"CopyTo", NULL, &t5919_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5919_m30915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30915_GM};
extern Il2CppType t833_0_0_0;
static ParameterInfo t5919_m30916_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t833_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30916_GM;
MethodInfo m30916_MI = 
{
	"Remove", NULL, &t5919_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5919_m30916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30916_GM};
static MethodInfo* t5919_MIs[] =
{
	&m30910_MI,
	&m30911_MI,
	&m30912_MI,
	&m30913_MI,
	&m30914_MI,
	&m30915_MI,
	&m30916_MI,
	NULL
};
extern TypeInfo t5921_TI;
static TypeInfo* t5919_ITIs[] = 
{
	&t603_TI,
	&t5921_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5919_0_0_0;
extern Il2CppType t5919_1_0_0;
struct t5919;
extern Il2CppGenericClass t5919_GC;
TypeInfo t5919_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5919_MIs, t5919_PIs, NULL, NULL, NULL, NULL, NULL, &t5919_TI, t5919_ITIs, NULL, &EmptyCustomAttributesCache, &t5919_TI, &t5919_0_0_0, &t5919_1_0_0, NULL, &t5919_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.Group>
extern Il2CppType t4594_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30917_GM;
MethodInfo m30917_MI = 
{
	"GetEnumerator", NULL, &t5921_TI, &t4594_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30917_GM};
static MethodInfo* t5921_MIs[] =
{
	&m30917_MI,
	NULL
};
static TypeInfo* t5921_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5921_0_0_0;
extern Il2CppType t5921_1_0_0;
struct t5921;
extern Il2CppGenericClass t5921_GC;
TypeInfo t5921_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5921_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5921_TI, t5921_ITIs, NULL, &EmptyCustomAttributesCache, &t5921_TI, &t5921_0_0_0, &t5921_1_0_0, NULL, &t5921_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5920_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.Group>
extern MethodInfo m30918_MI;
extern MethodInfo m30919_MI;
static PropertyInfo t5920____Item_PropertyInfo = 
{
	&t5920_TI, "Item", &m30918_MI, &m30919_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5920_PIs[] =
{
	&t5920____Item_PropertyInfo,
	NULL
};
extern Il2CppType t833_0_0_0;
static ParameterInfo t5920_m30920_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t833_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30920_GM;
MethodInfo m30920_MI = 
{
	"IndexOf", NULL, &t5920_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5920_m30920_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30920_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t833_0_0_0;
static ParameterInfo t5920_m30921_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t833_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30921_GM;
MethodInfo m30921_MI = 
{
	"Insert", NULL, &t5920_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5920_m30921_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30921_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5920_m30922_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30922_GM;
MethodInfo m30922_MI = 
{
	"RemoveAt", NULL, &t5920_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5920_m30922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30922_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5920_m30918_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t833_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30918_GM;
MethodInfo m30918_MI = 
{
	"get_Item", NULL, &t5920_TI, &t833_0_0_0, RuntimeInvoker_t29_t44, t5920_m30918_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30918_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t833_0_0_0;
static ParameterInfo t5920_m30919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t833_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30919_GM;
MethodInfo m30919_MI = 
{
	"set_Item", NULL, &t5920_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5920_m30919_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30919_GM};
static MethodInfo* t5920_MIs[] =
{
	&m30920_MI,
	&m30921_MI,
	&m30922_MI,
	&m30918_MI,
	&m30919_MI,
	NULL
};
static TypeInfo* t5920_ITIs[] = 
{
	&t603_TI,
	&t5919_TI,
	&t5921_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5920_0_0_0;
extern Il2CppType t5920_1_0_0;
struct t5920;
extern Il2CppGenericClass t5920_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5920_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5920_MIs, t5920_PIs, NULL, NULL, NULL, NULL, NULL, &t5920_TI, t5920_ITIs, NULL, &t1908__CustomAttributeCache, &t5920_TI, &t5920_0_0_0, &t5920_1_0_0, NULL, &t5920_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4595_TI;

#include "t842.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.RegexOptions>
extern MethodInfo m30923_MI;
static PropertyInfo t4595____Current_PropertyInfo = 
{
	&t4595_TI, "Current", &m30923_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4595_PIs[] =
{
	&t4595____Current_PropertyInfo,
	NULL
};
extern Il2CppType t842_0_0_0;
extern void* RuntimeInvoker_t842 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30923_GM;
MethodInfo m30923_MI = 
{
	"get_Current", NULL, &t4595_TI, &t842_0_0_0, RuntimeInvoker_t842, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30923_GM};
static MethodInfo* t4595_MIs[] =
{
	&m30923_MI,
	NULL
};
static TypeInfo* t4595_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4595_0_0_0;
extern Il2CppType t4595_1_0_0;
struct t4595;
extern Il2CppGenericClass t4595_GC;
TypeInfo t4595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4595_MIs, t4595_PIs, NULL, NULL, NULL, NULL, NULL, &t4595_TI, t4595_ITIs, NULL, &EmptyCustomAttributesCache, &t4595_TI, &t4595_0_0_0, &t4595_1_0_0, NULL, &t4595_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3225.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3225_TI;
#include "t3225MD.h"

extern TypeInfo t842_TI;
extern MethodInfo m17929_MI;
extern MethodInfo m23595_MI;
struct t20;
 int32_t m23595 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17925_MI;
 void m17925 (t3225 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17926_MI;
 t29 * m17926 (t3225 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17929(__this, &m17929_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t842_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17927_MI;
 void m17927 (t3225 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17928_MI;
 bool m17928 (t3225 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17929 (t3225 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23595(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23595_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>
extern Il2CppType t20_0_0_1;
FieldInfo t3225_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3225_TI, offsetof(t3225, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3225_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3225_TI, offsetof(t3225, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3225_FIs[] =
{
	&t3225_f0_FieldInfo,
	&t3225_f1_FieldInfo,
	NULL
};
static PropertyInfo t3225____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3225_TI, "System.Collections.IEnumerator.Current", &m17926_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3225____Current_PropertyInfo = 
{
	&t3225_TI, "Current", &m17929_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3225_PIs[] =
{
	&t3225____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3225____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3225_m17925_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17925_GM;
MethodInfo m17925_MI = 
{
	".ctor", (methodPointerType)&m17925, &t3225_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3225_m17925_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17925_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17926_GM;
MethodInfo m17926_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17926, &t3225_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17926_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17927_GM;
MethodInfo m17927_MI = 
{
	"Dispose", (methodPointerType)&m17927, &t3225_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17927_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17928_GM;
MethodInfo m17928_MI = 
{
	"MoveNext", (methodPointerType)&m17928, &t3225_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17928_GM};
extern Il2CppType t842_0_0_0;
extern void* RuntimeInvoker_t842 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17929_GM;
MethodInfo m17929_MI = 
{
	"get_Current", (methodPointerType)&m17929, &t3225_TI, &t842_0_0_0, RuntimeInvoker_t842, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17929_GM};
static MethodInfo* t3225_MIs[] =
{
	&m17925_MI,
	&m17926_MI,
	&m17927_MI,
	&m17928_MI,
	&m17929_MI,
	NULL
};
static MethodInfo* t3225_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17926_MI,
	&m17928_MI,
	&m17927_MI,
	&m17929_MI,
};
static TypeInfo* t3225_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4595_TI,
};
static Il2CppInterfaceOffsetPair t3225_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4595_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3225_0_0_0;
extern Il2CppType t3225_1_0_0;
extern Il2CppGenericClass t3225_GC;
TypeInfo t3225_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3225_MIs, t3225_PIs, t3225_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3225_TI, t3225_ITIs, t3225_VT, &EmptyCustomAttributesCache, &t3225_TI, &t3225_0_0_0, &t3225_1_0_0, t3225_IOs, &t3225_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3225)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5922_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.RegexOptions>
extern MethodInfo m30924_MI;
static PropertyInfo t5922____Count_PropertyInfo = 
{
	&t5922_TI, "Count", &m30924_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30925_MI;
static PropertyInfo t5922____IsReadOnly_PropertyInfo = 
{
	&t5922_TI, "IsReadOnly", &m30925_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5922_PIs[] =
{
	&t5922____Count_PropertyInfo,
	&t5922____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30924_GM;
MethodInfo m30924_MI = 
{
	"get_Count", NULL, &t5922_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30924_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30925_GM;
MethodInfo m30925_MI = 
{
	"get_IsReadOnly", NULL, &t5922_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30925_GM};
extern Il2CppType t842_0_0_0;
extern Il2CppType t842_0_0_0;
static ParameterInfo t5922_m30926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30926_GM;
MethodInfo m30926_MI = 
{
	"Add", NULL, &t5922_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5922_m30926_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30926_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30927_GM;
MethodInfo m30927_MI = 
{
	"Clear", NULL, &t5922_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30927_GM};
extern Il2CppType t842_0_0_0;
static ParameterInfo t5922_m30928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30928_GM;
MethodInfo m30928_MI = 
{
	"Contains", NULL, &t5922_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5922_m30928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30928_GM};
extern Il2CppType t3917_0_0_0;
extern Il2CppType t3917_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5922_m30929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3917_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30929_GM;
MethodInfo m30929_MI = 
{
	"CopyTo", NULL, &t5922_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5922_m30929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30929_GM};
extern Il2CppType t842_0_0_0;
static ParameterInfo t5922_m30930_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30930_GM;
MethodInfo m30930_MI = 
{
	"Remove", NULL, &t5922_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5922_m30930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30930_GM};
static MethodInfo* t5922_MIs[] =
{
	&m30924_MI,
	&m30925_MI,
	&m30926_MI,
	&m30927_MI,
	&m30928_MI,
	&m30929_MI,
	&m30930_MI,
	NULL
};
extern TypeInfo t5924_TI;
static TypeInfo* t5922_ITIs[] = 
{
	&t603_TI,
	&t5924_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5922_0_0_0;
extern Il2CppType t5922_1_0_0;
struct t5922;
extern Il2CppGenericClass t5922_GC;
TypeInfo t5922_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5922_MIs, t5922_PIs, NULL, NULL, NULL, NULL, NULL, &t5922_TI, t5922_ITIs, NULL, &EmptyCustomAttributesCache, &t5922_TI, &t5922_0_0_0, &t5922_1_0_0, NULL, &t5922_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.RegexOptions>
extern Il2CppType t4595_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30931_GM;
MethodInfo m30931_MI = 
{
	"GetEnumerator", NULL, &t5924_TI, &t4595_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30931_GM};
static MethodInfo* t5924_MIs[] =
{
	&m30931_MI,
	NULL
};
static TypeInfo* t5924_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5924_0_0_0;
extern Il2CppType t5924_1_0_0;
struct t5924;
extern Il2CppGenericClass t5924_GC;
TypeInfo t5924_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5924_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5924_TI, t5924_ITIs, NULL, &EmptyCustomAttributesCache, &t5924_TI, &t5924_0_0_0, &t5924_1_0_0, NULL, &t5924_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5923_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.RegexOptions>
extern MethodInfo m30932_MI;
extern MethodInfo m30933_MI;
static PropertyInfo t5923____Item_PropertyInfo = 
{
	&t5923_TI, "Item", &m30932_MI, &m30933_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5923_PIs[] =
{
	&t5923____Item_PropertyInfo,
	NULL
};
extern Il2CppType t842_0_0_0;
static ParameterInfo t5923_m30934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30934_GM;
MethodInfo m30934_MI = 
{
	"IndexOf", NULL, &t5923_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5923_m30934_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30934_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t842_0_0_0;
static ParameterInfo t5923_m30935_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30935_GM;
MethodInfo m30935_MI = 
{
	"Insert", NULL, &t5923_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5923_m30935_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30935_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5923_m30936_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30936_GM;
MethodInfo m30936_MI = 
{
	"RemoveAt", NULL, &t5923_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5923_m30936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30936_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5923_m30932_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t842_0_0_0;
extern void* RuntimeInvoker_t842_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30932_GM;
MethodInfo m30932_MI = 
{
	"get_Item", NULL, &t5923_TI, &t842_0_0_0, RuntimeInvoker_t842_t44, t5923_m30932_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30932_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t842_0_0_0;
static ParameterInfo t5923_m30933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30933_GM;
MethodInfo m30933_MI = 
{
	"set_Item", NULL, &t5923_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5923_m30933_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30933_GM};
static MethodInfo* t5923_MIs[] =
{
	&m30934_MI,
	&m30935_MI,
	&m30936_MI,
	&m30932_MI,
	&m30933_MI,
	NULL
};
static TypeInfo* t5923_ITIs[] = 
{
	&t603_TI,
	&t5922_TI,
	&t5924_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5923_0_0_0;
extern Il2CppType t5923_1_0_0;
struct t5923;
extern Il2CppGenericClass t5923_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5923_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5923_MIs, t5923_PIs, NULL, NULL, NULL, NULL, NULL, &t5923_TI, t5923_ITIs, NULL, &t1908__CustomAttributeCache, &t5923_TI, &t5923_0_0_0, &t5923_1_0_0, NULL, &t5923_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2082_TI;



// Metadata Definition System.Collections.Generic.IComparer`1<System.Int32>
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2082_m23606_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23606_GM;
MethodInfo m23606_MI = 
{
	"Compare", NULL, &t2082_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44, t2082_m23606_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m23606_GM};
static MethodInfo* t2082_MIs[] =
{
	&m23606_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2082_0_0_0;
extern Il2CppType t2082_1_0_0;
struct t2082;
extern Il2CppGenericClass t2082_GC;
TypeInfo t2082_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t2082_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2082_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2082_TI, &t2082_0_0_0, &t2082_1_0_0, NULL, &t2082_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3226.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3226_TI;
#include "t3226MD.h"

#include "t42.h"
#include "t43.h"
#include "t1247.h"
#include "mscorlib_ArrayTypes.h"
#include "t3227.h"
#include "t305.h"
extern TypeInfo t1706_TI;
extern TypeInfo t42_TI;
extern TypeInfo t44_TI;
extern TypeInfo t40_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3227_TI;
extern TypeInfo t305_TI;
#include "t29MD.h"
#include "t42MD.h"
#include "t931MD.h"
#include "t3227MD.h"
#include "t305MD.h"
extern Il2CppType t1706_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m1331_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m17934_MI;
extern MethodInfo m30937_MI;
extern MethodInfo m8852_MI;


extern MethodInfo m17930_MI;
 void m17930 (t3226 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m17931_MI;
 void m17931 (t29 * __this, MethodInfo* method){
	t3227 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3227 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3227_TI));
	m17934(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m17934_MI);
	((t3226_SFs*)InitializedTypeInfo(&t3226_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m17932_MI;
 int32_t m17932 (t3226 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(&m30937_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))), ((*(int32_t*)((int32_t*)UnBox (p1, InitializedTypeInfo(&t44_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
extern MethodInfo m17933_MI;
 t3226 * m17933 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3226_TI));
		return (((t3226_SFs*)InitializedTypeInfo(&t3226_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.Int32>
extern Il2CppType t3226_0_0_49;
FieldInfo t3226_f0_FieldInfo = 
{
	"_default", &t3226_0_0_49, &t3226_TI, offsetof(t3226_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3226_FIs[] =
{
	&t3226_f0_FieldInfo,
	NULL
};
static PropertyInfo t3226____Default_PropertyInfo = 
{
	&t3226_TI, "Default", &m17933_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3226_PIs[] =
{
	&t3226____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17930_GM;
MethodInfo m17930_MI = 
{
	".ctor", (methodPointerType)&m17930, &t3226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17930_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17931_GM;
MethodInfo m17931_MI = 
{
	".cctor", (methodPointerType)&m17931, &t3226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17931_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3226_m17932_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17932_GM;
MethodInfo m17932_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m17932, &t3226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3226_m17932_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17932_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3226_m30937_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30937_GM;
MethodInfo m30937_MI = 
{
	"Compare", NULL, &t3226_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44, t3226_m30937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30937_GM};
extern Il2CppType t3226_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17933_GM;
MethodInfo m17933_MI = 
{
	"get_Default", (methodPointerType)&m17933, &t3226_TI, &t3226_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17933_GM};
static MethodInfo* t3226_MIs[] =
{
	&m17930_MI,
	&m17931_MI,
	&m17932_MI,
	&m30937_MI,
	&m17933_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t3226_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m30937_MI,
	&m17932_MI,
	NULL,
};
extern TypeInfo t726_TI;
static TypeInfo* t3226_ITIs[] = 
{
	&t2082_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3226_IOs[] = 
{
	{ &t2082_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3226_0_0_0;
extern Il2CppType t3226_1_0_0;
extern TypeInfo t29_TI;
struct t3226;
extern Il2CppGenericClass t3226_GC;
TypeInfo t3226_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3226_MIs, t3226_PIs, t3226_FIs, NULL, &t29_TI, NULL, NULL, &t3226_TI, t3226_ITIs, t3226_VT, &EmptyCustomAttributesCache, &t3226_TI, &t3226_0_0_0, &t3226_1_0_0, t3226_IOs, &t3226_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3226), 0, -1, sizeof(t3226_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m26731_MI;
extern MethodInfo m9672_MI;
extern MethodInfo m1935_MI;


 void m17934 (t3227 * __this, MethodInfo* method){
	{
		m17930(__this, &m17930_MI);
		return;
	}
}
extern MethodInfo m17935_MI;
 int32_t m17935 (t3227 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		int32_t L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t44_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		int32_t L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t44_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t1706_TI))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t44_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m26731_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t1706_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		int32_t L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t44_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t44_TI), &L_13);
		int32_t L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t44_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17934_GM;
MethodInfo m17934_MI = 
{
	".ctor", (methodPointerType)&m17934, &t3227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17934_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3227_m17935_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17935_GM;
MethodInfo m17935_MI = 
{
	"Compare", (methodPointerType)&m17935, &t3227_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44, t3227_m17935_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17935_GM};
static MethodInfo* t3227_MIs[] =
{
	&m17934_MI,
	&m17935_MI,
	NULL
};
static MethodInfo* t3227_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17935_MI,
	&m17932_MI,
	&m17935_MI,
};
static Il2CppInterfaceOffsetPair t3227_IOs[] = 
{
	{ &t2082_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3227_0_0_0;
extern Il2CppType t3227_1_0_0;
struct t3227;
extern Il2CppGenericClass t3227_GC;
extern TypeInfo t1246_TI;
TypeInfo t3227_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3227_MIs, NULL, NULL, NULL, &t3226_TI, NULL, &t1246_TI, &t3227_TI, NULL, t3227_VT, &EmptyCustomAttributesCache, &t3227_TI, &t3227_0_0_0, &t3227_1_0_0, t3227_IOs, &t3227_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3227), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4597_TI;

#include "t843.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.OpCode>
extern MethodInfo m30938_MI;
static PropertyInfo t4597____Current_PropertyInfo = 
{
	&t4597_TI, "Current", &m30938_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4597_PIs[] =
{
	&t4597____Current_PropertyInfo,
	NULL
};
extern Il2CppType t843_0_0_0;
extern void* RuntimeInvoker_t843 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30938_GM;
MethodInfo m30938_MI = 
{
	"get_Current", NULL, &t4597_TI, &t843_0_0_0, RuntimeInvoker_t843, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30938_GM};
static MethodInfo* t4597_MIs[] =
{
	&m30938_MI,
	NULL
};
static TypeInfo* t4597_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4597_0_0_0;
extern Il2CppType t4597_1_0_0;
struct t4597;
extern Il2CppGenericClass t4597_GC;
TypeInfo t4597_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4597_MIs, t4597_PIs, NULL, NULL, NULL, NULL, NULL, &t4597_TI, t4597_ITIs, NULL, &EmptyCustomAttributesCache, &t4597_TI, &t4597_0_0_0, &t4597_1_0_0, NULL, &t4597_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3228.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3228_TI;
#include "t3228MD.h"

extern TypeInfo t843_TI;
extern MethodInfo m17940_MI;
extern MethodInfo m23608_MI;
struct t20;
 uint16_t m23608 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17936_MI;
 void m17936 (t3228 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17937_MI;
 t29 * m17937 (t3228 * __this, MethodInfo* method){
	{
		uint16_t L_0 = m17940(__this, &m17940_MI);
		uint16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t843_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17938_MI;
 void m17938 (t3228 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17939_MI;
 bool m17939 (t3228 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint16_t m17940 (t3228 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m23608(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23608_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.OpCode>
extern Il2CppType t20_0_0_1;
FieldInfo t3228_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3228_TI, offsetof(t3228, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3228_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3228_TI, offsetof(t3228, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3228_FIs[] =
{
	&t3228_f0_FieldInfo,
	&t3228_f1_FieldInfo,
	NULL
};
static PropertyInfo t3228____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3228_TI, "System.Collections.IEnumerator.Current", &m17937_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3228____Current_PropertyInfo = 
{
	&t3228_TI, "Current", &m17940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3228_PIs[] =
{
	&t3228____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3228____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3228_m17936_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17936_GM;
MethodInfo m17936_MI = 
{
	".ctor", (methodPointerType)&m17936, &t3228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3228_m17936_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17936_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17937_GM;
MethodInfo m17937_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17937, &t3228_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17937_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17938_GM;
MethodInfo m17938_MI = 
{
	"Dispose", (methodPointerType)&m17938, &t3228_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17938_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17939_GM;
MethodInfo m17939_MI = 
{
	"MoveNext", (methodPointerType)&m17939, &t3228_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17939_GM};
extern Il2CppType t843_0_0_0;
extern void* RuntimeInvoker_t843 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17940_GM;
MethodInfo m17940_MI = 
{
	"get_Current", (methodPointerType)&m17940, &t3228_TI, &t843_0_0_0, RuntimeInvoker_t843, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17940_GM};
static MethodInfo* t3228_MIs[] =
{
	&m17936_MI,
	&m17937_MI,
	&m17938_MI,
	&m17939_MI,
	&m17940_MI,
	NULL
};
static MethodInfo* t3228_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17937_MI,
	&m17939_MI,
	&m17938_MI,
	&m17940_MI,
};
static TypeInfo* t3228_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4597_TI,
};
static Il2CppInterfaceOffsetPair t3228_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4597_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3228_0_0_0;
extern Il2CppType t3228_1_0_0;
extern Il2CppGenericClass t3228_GC;
TypeInfo t3228_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3228_MIs, t3228_PIs, t3228_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3228_TI, t3228_ITIs, t3228_VT, &EmptyCustomAttributesCache, &t3228_TI, &t3228_0_0_0, &t3228_1_0_0, t3228_IOs, &t3228_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3228)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5925_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.OpCode>
extern MethodInfo m30939_MI;
static PropertyInfo t5925____Count_PropertyInfo = 
{
	&t5925_TI, "Count", &m30939_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30940_MI;
static PropertyInfo t5925____IsReadOnly_PropertyInfo = 
{
	&t5925_TI, "IsReadOnly", &m30940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5925_PIs[] =
{
	&t5925____Count_PropertyInfo,
	&t5925____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30939_GM;
MethodInfo m30939_MI = 
{
	"get_Count", NULL, &t5925_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30939_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30940_GM;
MethodInfo m30940_MI = 
{
	"get_IsReadOnly", NULL, &t5925_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30940_GM};
extern Il2CppType t843_0_0_0;
extern Il2CppType t843_0_0_0;
static ParameterInfo t5925_m30941_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30941_GM;
MethodInfo m30941_MI = 
{
	"Add", NULL, &t5925_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t5925_m30941_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30941_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30942_GM;
MethodInfo m30942_MI = 
{
	"Clear", NULL, &t5925_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30942_GM};
extern Il2CppType t843_0_0_0;
static ParameterInfo t5925_m30943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30943_GM;
MethodInfo m30943_MI = 
{
	"Contains", NULL, &t5925_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5925_m30943_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30943_GM};
extern Il2CppType t3918_0_0_0;
extern Il2CppType t3918_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5925_m30944_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3918_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30944_GM;
MethodInfo m30944_MI = 
{
	"CopyTo", NULL, &t5925_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5925_m30944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30944_GM};
extern Il2CppType t843_0_0_0;
static ParameterInfo t5925_m30945_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30945_GM;
MethodInfo m30945_MI = 
{
	"Remove", NULL, &t5925_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5925_m30945_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30945_GM};
static MethodInfo* t5925_MIs[] =
{
	&m30939_MI,
	&m30940_MI,
	&m30941_MI,
	&m30942_MI,
	&m30943_MI,
	&m30944_MI,
	&m30945_MI,
	NULL
};
extern TypeInfo t5927_TI;
static TypeInfo* t5925_ITIs[] = 
{
	&t603_TI,
	&t5927_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5925_0_0_0;
extern Il2CppType t5925_1_0_0;
struct t5925;
extern Il2CppGenericClass t5925_GC;
TypeInfo t5925_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5925_MIs, t5925_PIs, NULL, NULL, NULL, NULL, NULL, &t5925_TI, t5925_ITIs, NULL, &EmptyCustomAttributesCache, &t5925_TI, &t5925_0_0_0, &t5925_1_0_0, NULL, &t5925_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.OpCode>
extern Il2CppType t4597_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30946_GM;
MethodInfo m30946_MI = 
{
	"GetEnumerator", NULL, &t5927_TI, &t4597_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30946_GM};
static MethodInfo* t5927_MIs[] =
{
	&m30946_MI,
	NULL
};
static TypeInfo* t5927_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5927_0_0_0;
extern Il2CppType t5927_1_0_0;
struct t5927;
extern Il2CppGenericClass t5927_GC;
TypeInfo t5927_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5927_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5927_TI, t5927_ITIs, NULL, &EmptyCustomAttributesCache, &t5927_TI, &t5927_0_0_0, &t5927_1_0_0, NULL, &t5927_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5926_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.OpCode>
extern MethodInfo m30947_MI;
extern MethodInfo m30948_MI;
static PropertyInfo t5926____Item_PropertyInfo = 
{
	&t5926_TI, "Item", &m30947_MI, &m30948_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5926_PIs[] =
{
	&t5926____Item_PropertyInfo,
	NULL
};
extern Il2CppType t843_0_0_0;
static ParameterInfo t5926_m30949_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30949_GM;
MethodInfo m30949_MI = 
{
	"IndexOf", NULL, &t5926_TI, &t44_0_0_0, RuntimeInvoker_t44_t626, t5926_m30949_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30949_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t843_0_0_0;
static ParameterInfo t5926_m30950_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30950_GM;
MethodInfo m30950_MI = 
{
	"Insert", NULL, &t5926_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5926_m30950_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30950_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5926_m30951_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30951_GM;
MethodInfo m30951_MI = 
{
	"RemoveAt", NULL, &t5926_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5926_m30951_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30951_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5926_m30947_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t843_0_0_0;
extern void* RuntimeInvoker_t843_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30947_GM;
MethodInfo m30947_MI = 
{
	"get_Item", NULL, &t5926_TI, &t843_0_0_0, RuntimeInvoker_t843_t44, t5926_m30947_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30947_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t843_0_0_0;
static ParameterInfo t5926_m30948_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30948_GM;
MethodInfo m30948_MI = 
{
	"set_Item", NULL, &t5926_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5926_m30948_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30948_GM};
static MethodInfo* t5926_MIs[] =
{
	&m30949_MI,
	&m30950_MI,
	&m30951_MI,
	&m30947_MI,
	&m30948_MI,
	NULL
};
static TypeInfo* t5926_ITIs[] = 
{
	&t603_TI,
	&t5925_TI,
	&t5927_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5926_0_0_0;
extern Il2CppType t5926_1_0_0;
struct t5926;
extern Il2CppGenericClass t5926_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5926_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5926_MIs, t5926_PIs, NULL, NULL, NULL, NULL, NULL, &t5926_TI, t5926_ITIs, NULL, &t1908__CustomAttributeCache, &t5926_TI, &t5926_0_0_0, &t5926_1_0_0, NULL, &t5926_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4599_TI;

#include "t844.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.OpFlags>
extern MethodInfo m30952_MI;
static PropertyInfo t4599____Current_PropertyInfo = 
{
	&t4599_TI, "Current", &m30952_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4599_PIs[] =
{
	&t4599____Current_PropertyInfo,
	NULL
};
extern Il2CppType t844_0_0_0;
extern void* RuntimeInvoker_t844 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30952_GM;
MethodInfo m30952_MI = 
{
	"get_Current", NULL, &t4599_TI, &t844_0_0_0, RuntimeInvoker_t844, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30952_GM};
static MethodInfo* t4599_MIs[] =
{
	&m30952_MI,
	NULL
};
static TypeInfo* t4599_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4599_0_0_0;
extern Il2CppType t4599_1_0_0;
struct t4599;
extern Il2CppGenericClass t4599_GC;
TypeInfo t4599_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4599_MIs, t4599_PIs, NULL, NULL, NULL, NULL, NULL, &t4599_TI, t4599_ITIs, NULL, &EmptyCustomAttributesCache, &t4599_TI, &t4599_0_0_0, &t4599_1_0_0, NULL, &t4599_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3229.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3229_TI;
#include "t3229MD.h"

extern TypeInfo t844_TI;
extern MethodInfo m17945_MI;
extern MethodInfo m23619_MI;
struct t20;
 uint16_t m23619 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17941_MI;
 void m17941 (t3229 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17942_MI;
 t29 * m17942 (t3229 * __this, MethodInfo* method){
	{
		uint16_t L_0 = m17945(__this, &m17945_MI);
		uint16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t844_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17943_MI;
 void m17943 (t3229 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17944_MI;
 bool m17944 (t3229 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint16_t m17945 (t3229 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m23619(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23619_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.OpFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3229_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3229_TI, offsetof(t3229, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3229_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3229_TI, offsetof(t3229, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3229_FIs[] =
{
	&t3229_f0_FieldInfo,
	&t3229_f1_FieldInfo,
	NULL
};
static PropertyInfo t3229____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3229_TI, "System.Collections.IEnumerator.Current", &m17942_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3229____Current_PropertyInfo = 
{
	&t3229_TI, "Current", &m17945_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3229_PIs[] =
{
	&t3229____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3229____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3229_m17941_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17941_GM;
MethodInfo m17941_MI = 
{
	".ctor", (methodPointerType)&m17941, &t3229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3229_m17941_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17941_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17942_GM;
MethodInfo m17942_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17942, &t3229_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17942_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17943_GM;
MethodInfo m17943_MI = 
{
	"Dispose", (methodPointerType)&m17943, &t3229_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17943_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17944_GM;
MethodInfo m17944_MI = 
{
	"MoveNext", (methodPointerType)&m17944, &t3229_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17944_GM};
extern Il2CppType t844_0_0_0;
extern void* RuntimeInvoker_t844 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17945_GM;
MethodInfo m17945_MI = 
{
	"get_Current", (methodPointerType)&m17945, &t3229_TI, &t844_0_0_0, RuntimeInvoker_t844, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17945_GM};
static MethodInfo* t3229_MIs[] =
{
	&m17941_MI,
	&m17942_MI,
	&m17943_MI,
	&m17944_MI,
	&m17945_MI,
	NULL
};
static MethodInfo* t3229_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17942_MI,
	&m17944_MI,
	&m17943_MI,
	&m17945_MI,
};
static TypeInfo* t3229_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4599_TI,
};
static Il2CppInterfaceOffsetPair t3229_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4599_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3229_0_0_0;
extern Il2CppType t3229_1_0_0;
extern Il2CppGenericClass t3229_GC;
TypeInfo t3229_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3229_MIs, t3229_PIs, t3229_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3229_TI, t3229_ITIs, t3229_VT, &EmptyCustomAttributesCache, &t3229_TI, &t3229_0_0_0, &t3229_1_0_0, t3229_IOs, &t3229_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3229)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5928_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.OpFlags>
extern MethodInfo m30953_MI;
static PropertyInfo t5928____Count_PropertyInfo = 
{
	&t5928_TI, "Count", &m30953_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30954_MI;
static PropertyInfo t5928____IsReadOnly_PropertyInfo = 
{
	&t5928_TI, "IsReadOnly", &m30954_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5928_PIs[] =
{
	&t5928____Count_PropertyInfo,
	&t5928____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30953_GM;
MethodInfo m30953_MI = 
{
	"get_Count", NULL, &t5928_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30953_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30954_GM;
MethodInfo m30954_MI = 
{
	"get_IsReadOnly", NULL, &t5928_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30954_GM};
extern Il2CppType t844_0_0_0;
extern Il2CppType t844_0_0_0;
static ParameterInfo t5928_m30955_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30955_GM;
MethodInfo m30955_MI = 
{
	"Add", NULL, &t5928_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t5928_m30955_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30955_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30956_GM;
MethodInfo m30956_MI = 
{
	"Clear", NULL, &t5928_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30956_GM};
extern Il2CppType t844_0_0_0;
static ParameterInfo t5928_m30957_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30957_GM;
MethodInfo m30957_MI = 
{
	"Contains", NULL, &t5928_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5928_m30957_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30957_GM};
extern Il2CppType t3919_0_0_0;
extern Il2CppType t3919_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5928_m30958_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3919_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30958_GM;
MethodInfo m30958_MI = 
{
	"CopyTo", NULL, &t5928_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5928_m30958_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30958_GM};
extern Il2CppType t844_0_0_0;
static ParameterInfo t5928_m30959_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30959_GM;
MethodInfo m30959_MI = 
{
	"Remove", NULL, &t5928_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5928_m30959_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30959_GM};
static MethodInfo* t5928_MIs[] =
{
	&m30953_MI,
	&m30954_MI,
	&m30955_MI,
	&m30956_MI,
	&m30957_MI,
	&m30958_MI,
	&m30959_MI,
	NULL
};
extern TypeInfo t5930_TI;
static TypeInfo* t5928_ITIs[] = 
{
	&t603_TI,
	&t5930_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5928_0_0_0;
extern Il2CppType t5928_1_0_0;
struct t5928;
extern Il2CppGenericClass t5928_GC;
TypeInfo t5928_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5928_MIs, t5928_PIs, NULL, NULL, NULL, NULL, NULL, &t5928_TI, t5928_ITIs, NULL, &EmptyCustomAttributesCache, &t5928_TI, &t5928_0_0_0, &t5928_1_0_0, NULL, &t5928_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.OpFlags>
extern Il2CppType t4599_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30960_GM;
MethodInfo m30960_MI = 
{
	"GetEnumerator", NULL, &t5930_TI, &t4599_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30960_GM};
static MethodInfo* t5930_MIs[] =
{
	&m30960_MI,
	NULL
};
static TypeInfo* t5930_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5930_0_0_0;
extern Il2CppType t5930_1_0_0;
struct t5930;
extern Il2CppGenericClass t5930_GC;
TypeInfo t5930_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5930_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5930_TI, t5930_ITIs, NULL, &EmptyCustomAttributesCache, &t5930_TI, &t5930_0_0_0, &t5930_1_0_0, NULL, &t5930_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5929_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.OpFlags>
extern MethodInfo m30961_MI;
extern MethodInfo m30962_MI;
static PropertyInfo t5929____Item_PropertyInfo = 
{
	&t5929_TI, "Item", &m30961_MI, &m30962_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5929_PIs[] =
{
	&t5929____Item_PropertyInfo,
	NULL
};
extern Il2CppType t844_0_0_0;
static ParameterInfo t5929_m30963_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30963_GM;
MethodInfo m30963_MI = 
{
	"IndexOf", NULL, &t5929_TI, &t44_0_0_0, RuntimeInvoker_t44_t626, t5929_m30963_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30963_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t844_0_0_0;
static ParameterInfo t5929_m30964_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30964_GM;
MethodInfo m30964_MI = 
{
	"Insert", NULL, &t5929_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5929_m30964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30964_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5929_m30965_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30965_GM;
MethodInfo m30965_MI = 
{
	"RemoveAt", NULL, &t5929_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5929_m30965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30965_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5929_m30961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t844_0_0_0;
extern void* RuntimeInvoker_t844_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30961_GM;
MethodInfo m30961_MI = 
{
	"get_Item", NULL, &t5929_TI, &t844_0_0_0, RuntimeInvoker_t844_t44, t5929_m30961_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30961_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t844_0_0_0;
static ParameterInfo t5929_m30962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30962_GM;
MethodInfo m30962_MI = 
{
	"set_Item", NULL, &t5929_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5929_m30962_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30962_GM};
static MethodInfo* t5929_MIs[] =
{
	&m30963_MI,
	&m30964_MI,
	&m30965_MI,
	&m30961_MI,
	&m30962_MI,
	NULL
};
static TypeInfo* t5929_ITIs[] = 
{
	&t603_TI,
	&t5928_TI,
	&t5930_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5929_0_0_0;
extern Il2CppType t5929_1_0_0;
struct t5929;
extern Il2CppGenericClass t5929_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5929_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5929_MIs, t5929_PIs, NULL, NULL, NULL, NULL, NULL, &t5929_TI, t5929_ITIs, NULL, &t1908__CustomAttributeCache, &t5929_TI, &t5929_0_0_0, &t5929_1_0_0, NULL, &t5929_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4601_TI;

#include "t845.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Position>
extern MethodInfo m30966_MI;
static PropertyInfo t4601____Current_PropertyInfo = 
{
	&t4601_TI, "Current", &m30966_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4601_PIs[] =
{
	&t4601____Current_PropertyInfo,
	NULL
};
extern Il2CppType t845_0_0_0;
extern void* RuntimeInvoker_t845 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30966_GM;
MethodInfo m30966_MI = 
{
	"get_Current", NULL, &t4601_TI, &t845_0_0_0, RuntimeInvoker_t845, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30966_GM};
static MethodInfo* t4601_MIs[] =
{
	&m30966_MI,
	NULL
};
static TypeInfo* t4601_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4601_0_0_0;
extern Il2CppType t4601_1_0_0;
struct t4601;
extern Il2CppGenericClass t4601_GC;
TypeInfo t4601_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4601_MIs, t4601_PIs, NULL, NULL, NULL, NULL, NULL, &t4601_TI, t4601_ITIs, NULL, &EmptyCustomAttributesCache, &t4601_TI, &t4601_0_0_0, &t4601_1_0_0, NULL, &t4601_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3230.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3230_TI;
#include "t3230MD.h"

extern TypeInfo t845_TI;
extern MethodInfo m17950_MI;
extern MethodInfo m23630_MI;
struct t20;
 uint16_t m23630 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17946_MI;
 void m17946 (t3230 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17947_MI;
 t29 * m17947 (t3230 * __this, MethodInfo* method){
	{
		uint16_t L_0 = m17950(__this, &m17950_MI);
		uint16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t845_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17948_MI;
 void m17948 (t3230 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17949_MI;
 bool m17949 (t3230 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint16_t m17950 (t3230 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m23630(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23630_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Position>
extern Il2CppType t20_0_0_1;
FieldInfo t3230_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3230_TI, offsetof(t3230, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3230_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3230_TI, offsetof(t3230, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3230_FIs[] =
{
	&t3230_f0_FieldInfo,
	&t3230_f1_FieldInfo,
	NULL
};
static PropertyInfo t3230____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3230_TI, "System.Collections.IEnumerator.Current", &m17947_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3230____Current_PropertyInfo = 
{
	&t3230_TI, "Current", &m17950_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3230_PIs[] =
{
	&t3230____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3230____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3230_m17946_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17946_GM;
MethodInfo m17946_MI = 
{
	".ctor", (methodPointerType)&m17946, &t3230_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3230_m17946_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17946_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17947_GM;
MethodInfo m17947_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17947, &t3230_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17947_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17948_GM;
MethodInfo m17948_MI = 
{
	"Dispose", (methodPointerType)&m17948, &t3230_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17948_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17949_GM;
MethodInfo m17949_MI = 
{
	"MoveNext", (methodPointerType)&m17949, &t3230_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17949_GM};
extern Il2CppType t845_0_0_0;
extern void* RuntimeInvoker_t845 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17950_GM;
MethodInfo m17950_MI = 
{
	"get_Current", (methodPointerType)&m17950, &t3230_TI, &t845_0_0_0, RuntimeInvoker_t845, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17950_GM};
static MethodInfo* t3230_MIs[] =
{
	&m17946_MI,
	&m17947_MI,
	&m17948_MI,
	&m17949_MI,
	&m17950_MI,
	NULL
};
static MethodInfo* t3230_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17947_MI,
	&m17949_MI,
	&m17948_MI,
	&m17950_MI,
};
static TypeInfo* t3230_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4601_TI,
};
static Il2CppInterfaceOffsetPair t3230_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4601_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3230_0_0_0;
extern Il2CppType t3230_1_0_0;
extern Il2CppGenericClass t3230_GC;
TypeInfo t3230_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3230_MIs, t3230_PIs, t3230_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3230_TI, t3230_ITIs, t3230_VT, &EmptyCustomAttributesCache, &t3230_TI, &t3230_0_0_0, &t3230_1_0_0, t3230_IOs, &t3230_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3230)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5931_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.Position>
extern MethodInfo m30967_MI;
static PropertyInfo t5931____Count_PropertyInfo = 
{
	&t5931_TI, "Count", &m30967_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30968_MI;
static PropertyInfo t5931____IsReadOnly_PropertyInfo = 
{
	&t5931_TI, "IsReadOnly", &m30968_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5931_PIs[] =
{
	&t5931____Count_PropertyInfo,
	&t5931____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30967_GM;
MethodInfo m30967_MI = 
{
	"get_Count", NULL, &t5931_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30967_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30968_GM;
MethodInfo m30968_MI = 
{
	"get_IsReadOnly", NULL, &t5931_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30968_GM};
extern Il2CppType t845_0_0_0;
extern Il2CppType t845_0_0_0;
static ParameterInfo t5931_m30969_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30969_GM;
MethodInfo m30969_MI = 
{
	"Add", NULL, &t5931_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t5931_m30969_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30969_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30970_GM;
MethodInfo m30970_MI = 
{
	"Clear", NULL, &t5931_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30970_GM};
extern Il2CppType t845_0_0_0;
static ParameterInfo t5931_m30971_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30971_GM;
MethodInfo m30971_MI = 
{
	"Contains", NULL, &t5931_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5931_m30971_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30971_GM};
extern Il2CppType t3920_0_0_0;
extern Il2CppType t3920_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5931_m30972_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3920_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30972_GM;
MethodInfo m30972_MI = 
{
	"CopyTo", NULL, &t5931_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5931_m30972_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30972_GM};
extern Il2CppType t845_0_0_0;
static ParameterInfo t5931_m30973_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30973_GM;
MethodInfo m30973_MI = 
{
	"Remove", NULL, &t5931_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5931_m30973_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30973_GM};
static MethodInfo* t5931_MIs[] =
{
	&m30967_MI,
	&m30968_MI,
	&m30969_MI,
	&m30970_MI,
	&m30971_MI,
	&m30972_MI,
	&m30973_MI,
	NULL
};
extern TypeInfo t5933_TI;
static TypeInfo* t5931_ITIs[] = 
{
	&t603_TI,
	&t5933_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5931_0_0_0;
extern Il2CppType t5931_1_0_0;
struct t5931;
extern Il2CppGenericClass t5931_GC;
TypeInfo t5931_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5931_MIs, t5931_PIs, NULL, NULL, NULL, NULL, NULL, &t5931_TI, t5931_ITIs, NULL, &EmptyCustomAttributesCache, &t5931_TI, &t5931_0_0_0, &t5931_1_0_0, NULL, &t5931_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.Position>
extern Il2CppType t4601_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30974_GM;
MethodInfo m30974_MI = 
{
	"GetEnumerator", NULL, &t5933_TI, &t4601_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30974_GM};
static MethodInfo* t5933_MIs[] =
{
	&m30974_MI,
	NULL
};
static TypeInfo* t5933_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5933_0_0_0;
extern Il2CppType t5933_1_0_0;
struct t5933;
extern Il2CppGenericClass t5933_GC;
TypeInfo t5933_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5933_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5933_TI, t5933_ITIs, NULL, &EmptyCustomAttributesCache, &t5933_TI, &t5933_0_0_0, &t5933_1_0_0, NULL, &t5933_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5932_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.Position>
extern MethodInfo m30975_MI;
extern MethodInfo m30976_MI;
static PropertyInfo t5932____Item_PropertyInfo = 
{
	&t5932_TI, "Item", &m30975_MI, &m30976_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5932_PIs[] =
{
	&t5932____Item_PropertyInfo,
	NULL
};
extern Il2CppType t845_0_0_0;
static ParameterInfo t5932_m30977_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30977_GM;
MethodInfo m30977_MI = 
{
	"IndexOf", NULL, &t5932_TI, &t44_0_0_0, RuntimeInvoker_t44_t626, t5932_m30977_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30977_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t845_0_0_0;
static ParameterInfo t5932_m30978_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30978_GM;
MethodInfo m30978_MI = 
{
	"Insert", NULL, &t5932_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5932_m30978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30978_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5932_m30979_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30979_GM;
MethodInfo m30979_MI = 
{
	"RemoveAt", NULL, &t5932_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5932_m30979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30979_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5932_m30975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t845_0_0_0;
extern void* RuntimeInvoker_t845_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30975_GM;
MethodInfo m30975_MI = 
{
	"get_Item", NULL, &t5932_TI, &t845_0_0_0, RuntimeInvoker_t845_t44, t5932_m30975_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30975_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t845_0_0_0;
static ParameterInfo t5932_m30976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30976_GM;
MethodInfo m30976_MI = 
{
	"set_Item", NULL, &t5932_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5932_m30976_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30976_GM};
static MethodInfo* t5932_MIs[] =
{
	&m30977_MI,
	&m30978_MI,
	&m30979_MI,
	&m30975_MI,
	&m30976_MI,
	NULL
};
static TypeInfo* t5932_ITIs[] = 
{
	&t603_TI,
	&t5931_TI,
	&t5933_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5932_0_0_0;
extern Il2CppType t5932_1_0_0;
struct t5932;
extern Il2CppGenericClass t5932_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5932_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5932_MIs, t5932_PIs, NULL, NULL, NULL, NULL, NULL, &t5932_TI, t5932_ITIs, NULL, &t1908__CustomAttributeCache, &t5932_TI, &t5932_0_0_0, &t5932_1_0_0, NULL, &t5932_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4603_TI;

#include "t849.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Category>
extern MethodInfo m30980_MI;
static PropertyInfo t4603____Current_PropertyInfo = 
{
	&t4603_TI, "Current", &m30980_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4603_PIs[] =
{
	&t4603____Current_PropertyInfo,
	NULL
};
extern Il2CppType t849_0_0_0;
extern void* RuntimeInvoker_t849 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30980_GM;
MethodInfo m30980_MI = 
{
	"get_Current", NULL, &t4603_TI, &t849_0_0_0, RuntimeInvoker_t849, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30980_GM};
static MethodInfo* t4603_MIs[] =
{
	&m30980_MI,
	NULL
};
static TypeInfo* t4603_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4603_0_0_0;
extern Il2CppType t4603_1_0_0;
struct t4603;
extern Il2CppGenericClass t4603_GC;
TypeInfo t4603_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4603_MIs, t4603_PIs, NULL, NULL, NULL, NULL, NULL, &t4603_TI, t4603_ITIs, NULL, &EmptyCustomAttributesCache, &t4603_TI, &t4603_0_0_0, &t4603_1_0_0, NULL, &t4603_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3231.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3231_TI;
#include "t3231MD.h"

extern TypeInfo t849_TI;
extern MethodInfo m17955_MI;
extern MethodInfo m23641_MI;
struct t20;
 uint16_t m23641 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17951_MI;
 void m17951 (t3231 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17952_MI;
 t29 * m17952 (t3231 * __this, MethodInfo* method){
	{
		uint16_t L_0 = m17955(__this, &m17955_MI);
		uint16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t849_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17953_MI;
 void m17953 (t3231 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17954_MI;
 bool m17954 (t3231 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint16_t m17955 (t3231 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m23641(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23641_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>
extern Il2CppType t20_0_0_1;
FieldInfo t3231_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3231_TI, offsetof(t3231, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3231_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3231_TI, offsetof(t3231, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3231_FIs[] =
{
	&t3231_f0_FieldInfo,
	&t3231_f1_FieldInfo,
	NULL
};
static PropertyInfo t3231____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3231_TI, "System.Collections.IEnumerator.Current", &m17952_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3231____Current_PropertyInfo = 
{
	&t3231_TI, "Current", &m17955_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3231_PIs[] =
{
	&t3231____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3231____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3231_m17951_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17951_GM;
MethodInfo m17951_MI = 
{
	".ctor", (methodPointerType)&m17951, &t3231_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3231_m17951_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17951_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17952_GM;
MethodInfo m17952_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17952, &t3231_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17952_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17953_GM;
MethodInfo m17953_MI = 
{
	"Dispose", (methodPointerType)&m17953, &t3231_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17953_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17954_GM;
MethodInfo m17954_MI = 
{
	"MoveNext", (methodPointerType)&m17954, &t3231_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17954_GM};
extern Il2CppType t849_0_0_0;
extern void* RuntimeInvoker_t849 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17955_GM;
MethodInfo m17955_MI = 
{
	"get_Current", (methodPointerType)&m17955, &t3231_TI, &t849_0_0_0, RuntimeInvoker_t849, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17955_GM};
static MethodInfo* t3231_MIs[] =
{
	&m17951_MI,
	&m17952_MI,
	&m17953_MI,
	&m17954_MI,
	&m17955_MI,
	NULL
};
static MethodInfo* t3231_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17952_MI,
	&m17954_MI,
	&m17953_MI,
	&m17955_MI,
};
static TypeInfo* t3231_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4603_TI,
};
static Il2CppInterfaceOffsetPair t3231_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4603_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3231_0_0_0;
extern Il2CppType t3231_1_0_0;
extern Il2CppGenericClass t3231_GC;
TypeInfo t3231_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3231_MIs, t3231_PIs, t3231_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3231_TI, t3231_ITIs, t3231_VT, &EmptyCustomAttributesCache, &t3231_TI, &t3231_0_0_0, &t3231_1_0_0, t3231_IOs, &t3231_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3231)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5934_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.Category>
extern MethodInfo m30981_MI;
static PropertyInfo t5934____Count_PropertyInfo = 
{
	&t5934_TI, "Count", &m30981_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30982_MI;
static PropertyInfo t5934____IsReadOnly_PropertyInfo = 
{
	&t5934_TI, "IsReadOnly", &m30982_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5934_PIs[] =
{
	&t5934____Count_PropertyInfo,
	&t5934____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30981_GM;
MethodInfo m30981_MI = 
{
	"get_Count", NULL, &t5934_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30981_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30982_GM;
MethodInfo m30982_MI = 
{
	"get_IsReadOnly", NULL, &t5934_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30982_GM};
extern Il2CppType t849_0_0_0;
extern Il2CppType t849_0_0_0;
static ParameterInfo t5934_m30983_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t849_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30983_GM;
MethodInfo m30983_MI = 
{
	"Add", NULL, &t5934_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t5934_m30983_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30983_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30984_GM;
MethodInfo m30984_MI = 
{
	"Clear", NULL, &t5934_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30984_GM};
extern Il2CppType t849_0_0_0;
static ParameterInfo t5934_m30985_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t849_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30985_GM;
MethodInfo m30985_MI = 
{
	"Contains", NULL, &t5934_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5934_m30985_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30985_GM};
extern Il2CppType t3921_0_0_0;
extern Il2CppType t3921_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5934_m30986_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3921_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30986_GM;
MethodInfo m30986_MI = 
{
	"CopyTo", NULL, &t5934_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5934_m30986_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30986_GM};
extern Il2CppType t849_0_0_0;
static ParameterInfo t5934_m30987_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t849_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30987_GM;
MethodInfo m30987_MI = 
{
	"Remove", NULL, &t5934_TI, &t40_0_0_0, RuntimeInvoker_t40_t626, t5934_m30987_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30987_GM};
static MethodInfo* t5934_MIs[] =
{
	&m30981_MI,
	&m30982_MI,
	&m30983_MI,
	&m30984_MI,
	&m30985_MI,
	&m30986_MI,
	&m30987_MI,
	NULL
};
extern TypeInfo t5936_TI;
static TypeInfo* t5934_ITIs[] = 
{
	&t603_TI,
	&t5936_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5934_0_0_0;
extern Il2CppType t5934_1_0_0;
struct t5934;
extern Il2CppGenericClass t5934_GC;
TypeInfo t5934_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5934_MIs, t5934_PIs, NULL, NULL, NULL, NULL, NULL, &t5934_TI, t5934_ITIs, NULL, &EmptyCustomAttributesCache, &t5934_TI, &t5934_0_0_0, &t5934_1_0_0, NULL, &t5934_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.Category>
extern Il2CppType t4603_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30988_GM;
MethodInfo m30988_MI = 
{
	"GetEnumerator", NULL, &t5936_TI, &t4603_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30988_GM};
static MethodInfo* t5936_MIs[] =
{
	&m30988_MI,
	NULL
};
static TypeInfo* t5936_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5936_0_0_0;
extern Il2CppType t5936_1_0_0;
struct t5936;
extern Il2CppGenericClass t5936_GC;
TypeInfo t5936_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5936_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5936_TI, t5936_ITIs, NULL, &EmptyCustomAttributesCache, &t5936_TI, &t5936_0_0_0, &t5936_1_0_0, NULL, &t5936_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5935_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.Category>
extern MethodInfo m30989_MI;
extern MethodInfo m30990_MI;
static PropertyInfo t5935____Item_PropertyInfo = 
{
	&t5935_TI, "Item", &m30989_MI, &m30990_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5935_PIs[] =
{
	&t5935____Item_PropertyInfo,
	NULL
};
extern Il2CppType t849_0_0_0;
static ParameterInfo t5935_m30991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t849_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30991_GM;
MethodInfo m30991_MI = 
{
	"IndexOf", NULL, &t5935_TI, &t44_0_0_0, RuntimeInvoker_t44_t626, t5935_m30991_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30991_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t849_0_0_0;
static ParameterInfo t5935_m30992_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t849_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30992_GM;
MethodInfo m30992_MI = 
{
	"Insert", NULL, &t5935_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5935_m30992_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30992_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5935_m30993_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30993_GM;
MethodInfo m30993_MI = 
{
	"RemoveAt", NULL, &t5935_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5935_m30993_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30993_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5935_m30989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t849_0_0_0;
extern void* RuntimeInvoker_t849_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30989_GM;
MethodInfo m30989_MI = 
{
	"get_Item", NULL, &t5935_TI, &t849_0_0_0, RuntimeInvoker_t849_t44, t5935_m30989_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30989_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t849_0_0_0;
static ParameterInfo t5935_m30990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t849_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30990_GM;
MethodInfo m30990_MI = 
{
	"set_Item", NULL, &t5935_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t626, t5935_m30990_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30990_GM};
static MethodInfo* t5935_MIs[] =
{
	&m30991_MI,
	&m30992_MI,
	&m30993_MI,
	&m30989_MI,
	&m30990_MI,
	NULL
};
static TypeInfo* t5935_ITIs[] = 
{
	&t603_TI,
	&t5934_TI,
	&t5936_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5935_0_0_0;
extern Il2CppType t5935_1_0_0;
struct t5935;
extern Il2CppGenericClass t5935_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5935_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5935_MIs, t5935_PIs, NULL, NULL, NULL, NULL, NULL, &t5935_TI, t5935_ITIs, NULL, &t1908__CustomAttributeCache, &t5935_TI, &t5935_0_0_0, &t5935_1_0_0, NULL, &t5935_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4605_TI;

#include "t859.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Mark>
extern MethodInfo m30994_MI;
static PropertyInfo t4605____Current_PropertyInfo = 
{
	&t4605_TI, "Current", &m30994_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4605_PIs[] =
{
	&t4605____Current_PropertyInfo,
	NULL
};
extern Il2CppType t859_0_0_0;
extern void* RuntimeInvoker_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30994_GM;
MethodInfo m30994_MI = 
{
	"get_Current", NULL, &t4605_TI, &t859_0_0_0, RuntimeInvoker_t859, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30994_GM};
static MethodInfo* t4605_MIs[] =
{
	&m30994_MI,
	NULL
};
static TypeInfo* t4605_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4605_0_0_0;
extern Il2CppType t4605_1_0_0;
struct t4605;
extern Il2CppGenericClass t4605_GC;
TypeInfo t4605_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4605_MIs, t4605_PIs, NULL, NULL, NULL, NULL, NULL, &t4605_TI, t4605_ITIs, NULL, &EmptyCustomAttributesCache, &t4605_TI, &t4605_0_0_0, &t4605_1_0_0, NULL, &t4605_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3232.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3232_TI;
#include "t3232MD.h"

extern TypeInfo t859_TI;
extern MethodInfo m17960_MI;
extern MethodInfo m23652_MI;
struct t20;
 t859  m23652 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17956_MI;
 void m17956 (t3232 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17957_MI;
 t29 * m17957 (t3232 * __this, MethodInfo* method){
	{
		t859  L_0 = m17960(__this, &m17960_MI);
		t859  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t859_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17958_MI;
 void m17958 (t3232 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17959_MI;
 bool m17959 (t3232 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t859  m17960 (t3232 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t859  L_8 = m23652(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23652_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>
extern Il2CppType t20_0_0_1;
FieldInfo t3232_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3232_TI, offsetof(t3232, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3232_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3232_TI, offsetof(t3232, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3232_FIs[] =
{
	&t3232_f0_FieldInfo,
	&t3232_f1_FieldInfo,
	NULL
};
static PropertyInfo t3232____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3232_TI, "System.Collections.IEnumerator.Current", &m17957_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3232____Current_PropertyInfo = 
{
	&t3232_TI, "Current", &m17960_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3232_PIs[] =
{
	&t3232____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3232____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3232_m17956_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17956_GM;
MethodInfo m17956_MI = 
{
	".ctor", (methodPointerType)&m17956, &t3232_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3232_m17956_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17956_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17957_GM;
MethodInfo m17957_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17957, &t3232_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17957_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17958_GM;
MethodInfo m17958_MI = 
{
	"Dispose", (methodPointerType)&m17958, &t3232_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17958_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17959_GM;
MethodInfo m17959_MI = 
{
	"MoveNext", (methodPointerType)&m17959, &t3232_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17959_GM};
extern Il2CppType t859_0_0_0;
extern void* RuntimeInvoker_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17960_GM;
MethodInfo m17960_MI = 
{
	"get_Current", (methodPointerType)&m17960, &t3232_TI, &t859_0_0_0, RuntimeInvoker_t859, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17960_GM};
static MethodInfo* t3232_MIs[] =
{
	&m17956_MI,
	&m17957_MI,
	&m17958_MI,
	&m17959_MI,
	&m17960_MI,
	NULL
};
static MethodInfo* t3232_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17957_MI,
	&m17959_MI,
	&m17958_MI,
	&m17960_MI,
};
static TypeInfo* t3232_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4605_TI,
};
static Il2CppInterfaceOffsetPair t3232_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4605_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3232_0_0_0;
extern Il2CppType t3232_1_0_0;
extern Il2CppGenericClass t3232_GC;
TypeInfo t3232_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3232_MIs, t3232_PIs, t3232_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3232_TI, t3232_ITIs, t3232_VT, &EmptyCustomAttributesCache, &t3232_TI, &t3232_0_0_0, &t3232_1_0_0, t3232_IOs, &t3232_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3232)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5937_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.Mark>
extern MethodInfo m30995_MI;
static PropertyInfo t5937____Count_PropertyInfo = 
{
	&t5937_TI, "Count", &m30995_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30996_MI;
static PropertyInfo t5937____IsReadOnly_PropertyInfo = 
{
	&t5937_TI, "IsReadOnly", &m30996_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5937_PIs[] =
{
	&t5937____Count_PropertyInfo,
	&t5937____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30995_GM;
MethodInfo m30995_MI = 
{
	"get_Count", NULL, &t5937_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30995_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30996_GM;
MethodInfo m30996_MI = 
{
	"get_IsReadOnly", NULL, &t5937_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30996_GM};
extern Il2CppType t859_0_0_0;
extern Il2CppType t859_0_0_0;
static ParameterInfo t5937_m30997_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30997_GM;
MethodInfo m30997_MI = 
{
	"Add", NULL, &t5937_TI, &t21_0_0_0, RuntimeInvoker_t21_t859, t5937_m30997_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30997_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30998_GM;
MethodInfo m30998_MI = 
{
	"Clear", NULL, &t5937_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30998_GM};
extern Il2CppType t859_0_0_0;
static ParameterInfo t5937_m30999_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30999_GM;
MethodInfo m30999_MI = 
{
	"Contains", NULL, &t5937_TI, &t40_0_0_0, RuntimeInvoker_t40_t859, t5937_m30999_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30999_GM};
extern Il2CppType t865_0_0_0;
extern Il2CppType t865_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5937_m31000_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t865_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31000_GM;
MethodInfo m31000_MI = 
{
	"CopyTo", NULL, &t5937_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5937_m31000_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31000_GM};
extern Il2CppType t859_0_0_0;
static ParameterInfo t5937_m31001_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31001_GM;
MethodInfo m31001_MI = 
{
	"Remove", NULL, &t5937_TI, &t40_0_0_0, RuntimeInvoker_t40_t859, t5937_m31001_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31001_GM};
static MethodInfo* t5937_MIs[] =
{
	&m30995_MI,
	&m30996_MI,
	&m30997_MI,
	&m30998_MI,
	&m30999_MI,
	&m31000_MI,
	&m31001_MI,
	NULL
};
extern TypeInfo t5939_TI;
static TypeInfo* t5937_ITIs[] = 
{
	&t603_TI,
	&t5939_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5937_0_0_0;
extern Il2CppType t5937_1_0_0;
struct t5937;
extern Il2CppGenericClass t5937_GC;
TypeInfo t5937_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5937_MIs, t5937_PIs, NULL, NULL, NULL, NULL, NULL, &t5937_TI, t5937_ITIs, NULL, &EmptyCustomAttributesCache, &t5937_TI, &t5937_0_0_0, &t5937_1_0_0, NULL, &t5937_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.Mark>
extern Il2CppType t4605_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31002_GM;
MethodInfo m31002_MI = 
{
	"GetEnumerator", NULL, &t5939_TI, &t4605_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31002_GM};
static MethodInfo* t5939_MIs[] =
{
	&m31002_MI,
	NULL
};
static TypeInfo* t5939_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5939_0_0_0;
extern Il2CppType t5939_1_0_0;
struct t5939;
extern Il2CppGenericClass t5939_GC;
TypeInfo t5939_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5939_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5939_TI, t5939_ITIs, NULL, &EmptyCustomAttributesCache, &t5939_TI, &t5939_0_0_0, &t5939_1_0_0, NULL, &t5939_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5938_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.Mark>
extern MethodInfo m31003_MI;
extern MethodInfo m31004_MI;
static PropertyInfo t5938____Item_PropertyInfo = 
{
	&t5938_TI, "Item", &m31003_MI, &m31004_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5938_PIs[] =
{
	&t5938____Item_PropertyInfo,
	NULL
};
extern Il2CppType t859_0_0_0;
static ParameterInfo t5938_m31005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t859_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31005_GM;
MethodInfo m31005_MI = 
{
	"IndexOf", NULL, &t5938_TI, &t44_0_0_0, RuntimeInvoker_t44_t859, t5938_m31005_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31005_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t859_0_0_0;
static ParameterInfo t5938_m31006_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31006_GM;
MethodInfo m31006_MI = 
{
	"Insert", NULL, &t5938_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t859, t5938_m31006_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31006_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5938_m31007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31007_GM;
MethodInfo m31007_MI = 
{
	"RemoveAt", NULL, &t5938_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5938_m31007_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31007_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5938_m31003_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t859_0_0_0;
extern void* RuntimeInvoker_t859_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31003_GM;
MethodInfo m31003_MI = 
{
	"get_Item", NULL, &t5938_TI, &t859_0_0_0, RuntimeInvoker_t859_t44, t5938_m31003_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31003_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t859_0_0_0;
static ParameterInfo t5938_m31004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31004_GM;
MethodInfo m31004_MI = 
{
	"set_Item", NULL, &t5938_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t859, t5938_m31004_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31004_GM};
static MethodInfo* t5938_MIs[] =
{
	&m31005_MI,
	&m31006_MI,
	&m31007_MI,
	&m31003_MI,
	&m31004_MI,
	NULL
};
static TypeInfo* t5938_ITIs[] = 
{
	&t603_TI,
	&t5937_TI,
	&t5939_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5938_0_0_0;
extern Il2CppType t5938_1_0_0;
struct t5938;
extern Il2CppGenericClass t5938_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5938_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5938_MIs, t5938_PIs, NULL, NULL, NULL, NULL, NULL, &t5938_TI, t5938_ITIs, NULL, &t1908__CustomAttributeCache, &t5938_TI, &t5938_0_0_0, &t5938_1_0_0, NULL, &t5938_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4607_TI;

#include "t862.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Interpreter/Mode>
extern MethodInfo m31008_MI;
static PropertyInfo t4607____Current_PropertyInfo = 
{
	&t4607_TI, "Current", &m31008_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4607_PIs[] =
{
	&t4607____Current_PropertyInfo,
	NULL
};
extern Il2CppType t862_0_0_0;
extern void* RuntimeInvoker_t862 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31008_GM;
MethodInfo m31008_MI = 
{
	"get_Current", NULL, &t4607_TI, &t862_0_0_0, RuntimeInvoker_t862, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31008_GM};
static MethodInfo* t4607_MIs[] =
{
	&m31008_MI,
	NULL
};
static TypeInfo* t4607_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4607_0_0_0;
extern Il2CppType t4607_1_0_0;
struct t4607;
extern Il2CppGenericClass t4607_GC;
TypeInfo t4607_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4607_MIs, t4607_PIs, NULL, NULL, NULL, NULL, NULL, &t4607_TI, t4607_ITIs, NULL, &EmptyCustomAttributesCache, &t4607_TI, &t4607_0_0_0, &t4607_1_0_0, NULL, &t4607_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3233.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3233_TI;
#include "t3233MD.h"

extern TypeInfo t862_TI;
extern MethodInfo m17965_MI;
extern MethodInfo m23663_MI;
struct t20;
 int32_t m23663 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17961_MI;
 void m17961 (t3233 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17962_MI;
 t29 * m17962 (t3233 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17965(__this, &m17965_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t862_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17963_MI;
 void m17963 (t3233 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17964_MI;
 bool m17964 (t3233 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17965 (t3233 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23663(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23663_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Interpreter/Mode>
extern Il2CppType t20_0_0_1;
FieldInfo t3233_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3233_TI, offsetof(t3233, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3233_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3233_TI, offsetof(t3233, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3233_FIs[] =
{
	&t3233_f0_FieldInfo,
	&t3233_f1_FieldInfo,
	NULL
};
static PropertyInfo t3233____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3233_TI, "System.Collections.IEnumerator.Current", &m17962_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3233____Current_PropertyInfo = 
{
	&t3233_TI, "Current", &m17965_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3233_PIs[] =
{
	&t3233____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3233____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3233_m17961_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17961_GM;
MethodInfo m17961_MI = 
{
	".ctor", (methodPointerType)&m17961, &t3233_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3233_m17961_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17961_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17962_GM;
MethodInfo m17962_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17962, &t3233_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17962_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17963_GM;
MethodInfo m17963_MI = 
{
	"Dispose", (methodPointerType)&m17963, &t3233_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17963_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17964_GM;
MethodInfo m17964_MI = 
{
	"MoveNext", (methodPointerType)&m17964, &t3233_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17964_GM};
extern Il2CppType t862_0_0_0;
extern void* RuntimeInvoker_t862 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17965_GM;
MethodInfo m17965_MI = 
{
	"get_Current", (methodPointerType)&m17965, &t3233_TI, &t862_0_0_0, RuntimeInvoker_t862, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17965_GM};
static MethodInfo* t3233_MIs[] =
{
	&m17961_MI,
	&m17962_MI,
	&m17963_MI,
	&m17964_MI,
	&m17965_MI,
	NULL
};
static MethodInfo* t3233_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17962_MI,
	&m17964_MI,
	&m17963_MI,
	&m17965_MI,
};
static TypeInfo* t3233_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4607_TI,
};
static Il2CppInterfaceOffsetPair t3233_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4607_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3233_0_0_0;
extern Il2CppType t3233_1_0_0;
extern Il2CppGenericClass t3233_GC;
TypeInfo t3233_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3233_MIs, t3233_PIs, t3233_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3233_TI, t3233_ITIs, t3233_VT, &EmptyCustomAttributesCache, &t3233_TI, &t3233_0_0_0, &t3233_1_0_0, t3233_IOs, &t3233_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3233)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5940_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Text.RegularExpressions.Interpreter/Mode>
extern MethodInfo m31009_MI;
static PropertyInfo t5940____Count_PropertyInfo = 
{
	&t5940_TI, "Count", &m31009_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31010_MI;
static PropertyInfo t5940____IsReadOnly_PropertyInfo = 
{
	&t5940_TI, "IsReadOnly", &m31010_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5940_PIs[] =
{
	&t5940____Count_PropertyInfo,
	&t5940____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31009_GM;
MethodInfo m31009_MI = 
{
	"get_Count", NULL, &t5940_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31009_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31010_GM;
MethodInfo m31010_MI = 
{
	"get_IsReadOnly", NULL, &t5940_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31010_GM};
extern Il2CppType t862_0_0_0;
extern Il2CppType t862_0_0_0;
static ParameterInfo t5940_m31011_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t862_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31011_GM;
MethodInfo m31011_MI = 
{
	"Add", NULL, &t5940_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5940_m31011_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31011_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31012_GM;
MethodInfo m31012_MI = 
{
	"Clear", NULL, &t5940_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31012_GM};
extern Il2CppType t862_0_0_0;
static ParameterInfo t5940_m31013_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t862_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31013_GM;
MethodInfo m31013_MI = 
{
	"Contains", NULL, &t5940_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5940_m31013_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31013_GM};
extern Il2CppType t3922_0_0_0;
extern Il2CppType t3922_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5940_m31014_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3922_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31014_GM;
MethodInfo m31014_MI = 
{
	"CopyTo", NULL, &t5940_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5940_m31014_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31014_GM};
extern Il2CppType t862_0_0_0;
static ParameterInfo t5940_m31015_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t862_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31015_GM;
MethodInfo m31015_MI = 
{
	"Remove", NULL, &t5940_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5940_m31015_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31015_GM};
static MethodInfo* t5940_MIs[] =
{
	&m31009_MI,
	&m31010_MI,
	&m31011_MI,
	&m31012_MI,
	&m31013_MI,
	&m31014_MI,
	&m31015_MI,
	NULL
};
extern TypeInfo t5942_TI;
static TypeInfo* t5940_ITIs[] = 
{
	&t603_TI,
	&t5942_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5940_0_0_0;
extern Il2CppType t5940_1_0_0;
struct t5940;
extern Il2CppGenericClass t5940_GC;
TypeInfo t5940_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5940_MIs, t5940_PIs, NULL, NULL, NULL, NULL, NULL, &t5940_TI, t5940_ITIs, NULL, &EmptyCustomAttributesCache, &t5940_TI, &t5940_0_0_0, &t5940_1_0_0, NULL, &t5940_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
