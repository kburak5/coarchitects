﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3343;
struct t29;
struct t20;
#include "t1093.h"

 void m18587 (t3343 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18588 (t3343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18589 (t3343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18590 (t3343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18591 (t3343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
