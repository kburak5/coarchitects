﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1615;
struct t29;
struct t929;
struct t1668;
struct t66;
struct t67;
#include "t35.h"

 void m9644 (t1615 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m9645 (t1615 * __this, t29 * p0, t1668 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9646 (t1615 * __this, t29 * p0, t1668 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m9647 (t1615 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
