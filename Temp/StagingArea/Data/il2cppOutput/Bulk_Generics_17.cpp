﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5601_TI;

#include "t44.h"
#include "t40.h"
#include "t21.h"
#include "t384.h"
#include "UnityEngine_ArrayTypes.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventType>
extern MethodInfo m29322_MI;
static PropertyInfo t5601____Count_PropertyInfo = 
{
	&t5601_TI, "Count", &m29322_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29323_MI;
static PropertyInfo t5601____IsReadOnly_PropertyInfo = 
{
	&t5601_TI, "IsReadOnly", &m29323_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5601_PIs[] =
{
	&t5601____Count_PropertyInfo,
	&t5601____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29322_GM;
MethodInfo m29322_MI = 
{
	"get_Count", NULL, &t5601_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29322_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29323_GM;
MethodInfo m29323_MI = 
{
	"get_IsReadOnly", NULL, &t5601_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29323_GM};
extern Il2CppType t384_0_0_0;
extern Il2CppType t384_0_0_0;
static ParameterInfo t5601_m29324_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29324_GM;
MethodInfo m29324_MI = 
{
	"Add", NULL, &t5601_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5601_m29324_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29324_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29325_GM;
MethodInfo m29325_MI = 
{
	"Clear", NULL, &t5601_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29325_GM};
extern Il2CppType t384_0_0_0;
static ParameterInfo t5601_m29326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t384_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29326_GM;
MethodInfo m29326_MI = 
{
	"Contains", NULL, &t5601_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5601_m29326_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29326_GM};
extern Il2CppType t3749_0_0_0;
extern Il2CppType t3749_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5601_m29327_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3749_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29327_GM;
MethodInfo m29327_MI = 
{
	"CopyTo", NULL, &t5601_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5601_m29327_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29327_GM};
extern Il2CppType t384_0_0_0;
static ParameterInfo t5601_m29328_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t384_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29328_GM;
MethodInfo m29328_MI = 
{
	"Remove", NULL, &t5601_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5601_m29328_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29328_GM};
static MethodInfo* t5601_MIs[] =
{
	&m29322_MI,
	&m29323_MI,
	&m29324_MI,
	&m29325_MI,
	&m29326_MI,
	&m29327_MI,
	&m29328_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5603_TI;
static TypeInfo* t5601_ITIs[] = 
{
	&t603_TI,
	&t5603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5601_0_0_0;
extern Il2CppType t5601_1_0_0;
struct t5601;
extern Il2CppGenericClass t5601_GC;
TypeInfo t5601_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5601_MIs, t5601_PIs, NULL, NULL, NULL, NULL, NULL, &t5601_TI, t5601_ITIs, NULL, &EmptyCustomAttributesCache, &t5601_TI, &t5601_0_0_0, &t5601_1_0_0, NULL, &t5601_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventType>
extern Il2CppType t4371_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29329_GM;
MethodInfo m29329_MI = 
{
	"GetEnumerator", NULL, &t5603_TI, &t4371_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29329_GM};
static MethodInfo* t5603_MIs[] =
{
	&m29329_MI,
	NULL
};
static TypeInfo* t5603_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5603_0_0_0;
extern Il2CppType t5603_1_0_0;
struct t5603;
extern Il2CppGenericClass t5603_GC;
TypeInfo t5603_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5603_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5603_TI, t5603_ITIs, NULL, &EmptyCustomAttributesCache, &t5603_TI, &t5603_0_0_0, &t5603_1_0_0, NULL, &t5603_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5602_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventType>
extern MethodInfo m29330_MI;
extern MethodInfo m29331_MI;
static PropertyInfo t5602____Item_PropertyInfo = 
{
	&t5602_TI, "Item", &m29330_MI, &m29331_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5602_PIs[] =
{
	&t5602____Item_PropertyInfo,
	NULL
};
extern Il2CppType t384_0_0_0;
static ParameterInfo t5602_m29332_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t384_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29332_GM;
MethodInfo m29332_MI = 
{
	"IndexOf", NULL, &t5602_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5602_m29332_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29332_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t384_0_0_0;
static ParameterInfo t5602_m29333_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29333_GM;
MethodInfo m29333_MI = 
{
	"Insert", NULL, &t5602_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5602_m29333_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29333_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5602_m29334_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29334_GM;
MethodInfo m29334_MI = 
{
	"RemoveAt", NULL, &t5602_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5602_m29334_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29334_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5602_m29330_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t384_0_0_0;
extern void* RuntimeInvoker_t384_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29330_GM;
MethodInfo m29330_MI = 
{
	"get_Item", NULL, &t5602_TI, &t384_0_0_0, RuntimeInvoker_t384_t44, t5602_m29330_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29330_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t384_0_0_0;
static ParameterInfo t5602_m29331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29331_GM;
MethodInfo m29331_MI = 
{
	"set_Item", NULL, &t5602_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5602_m29331_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29331_GM};
static MethodInfo* t5602_MIs[] =
{
	&m29332_MI,
	&m29333_MI,
	&m29334_MI,
	&m29330_MI,
	&m29331_MI,
	NULL
};
static TypeInfo* t5602_ITIs[] = 
{
	&t603_TI,
	&t5601_TI,
	&t5603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5602_0_0_0;
extern Il2CppType t5602_1_0_0;
struct t5602;
extern Il2CppGenericClass t5602_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5602_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5602_MIs, t5602_PIs, NULL, NULL, NULL, NULL, NULL, &t5602_TI, t5602_ITIs, NULL, &t1908__CustomAttributeCache, &t5602_TI, &t5602_0_0_0, &t5602_1_0_0, NULL, &t5602_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4373_TI;

#include "t382.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventModifiers>
extern MethodInfo m29335_MI;
static PropertyInfo t4373____Current_PropertyInfo = 
{
	&t4373_TI, "Current", &m29335_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4373_PIs[] =
{
	&t4373____Current_PropertyInfo,
	NULL
};
extern Il2CppType t382_0_0_0;
extern void* RuntimeInvoker_t382 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29335_GM;
MethodInfo m29335_MI = 
{
	"get_Current", NULL, &t4373_TI, &t382_0_0_0, RuntimeInvoker_t382, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29335_GM};
static MethodInfo* t4373_MIs[] =
{
	&m29335_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4373_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4373_0_0_0;
extern Il2CppType t4373_1_0_0;
struct t4373;
extern Il2CppGenericClass t4373_GC;
TypeInfo t4373_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4373_MIs, t4373_PIs, NULL, NULL, NULL, NULL, NULL, &t4373_TI, t4373_ITIs, NULL, &EmptyCustomAttributesCache, &t4373_TI, &t4373_0_0_0, &t4373_1_0_0, NULL, &t4373_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2921.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2921_TI;
#include "t2921MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t382_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m16020_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m22266_MI;
struct t20;
#include "t915.h"
 int32_t m22266 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16016_MI;
 void m16016 (t2921 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16017_MI;
 t29 * m16017 (t2921 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16020(__this, &m16020_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t382_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16018_MI;
 void m16018 (t2921 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16019_MI;
 bool m16019 (t2921 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16020 (t2921 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22266(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22266_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>
extern Il2CppType t20_0_0_1;
FieldInfo t2921_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2921_TI, offsetof(t2921, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2921_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2921_TI, offsetof(t2921, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2921_FIs[] =
{
	&t2921_f0_FieldInfo,
	&t2921_f1_FieldInfo,
	NULL
};
static PropertyInfo t2921____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2921_TI, "System.Collections.IEnumerator.Current", &m16017_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2921____Current_PropertyInfo = 
{
	&t2921_TI, "Current", &m16020_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2921_PIs[] =
{
	&t2921____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2921____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2921_m16016_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16016_GM;
MethodInfo m16016_MI = 
{
	".ctor", (methodPointerType)&m16016, &t2921_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2921_m16016_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16016_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16017_GM;
MethodInfo m16017_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16017, &t2921_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16017_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16018_GM;
MethodInfo m16018_MI = 
{
	"Dispose", (methodPointerType)&m16018, &t2921_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16018_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16019_GM;
MethodInfo m16019_MI = 
{
	"MoveNext", (methodPointerType)&m16019, &t2921_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16019_GM};
extern Il2CppType t382_0_0_0;
extern void* RuntimeInvoker_t382 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16020_GM;
MethodInfo m16020_MI = 
{
	"get_Current", (methodPointerType)&m16020, &t2921_TI, &t382_0_0_0, RuntimeInvoker_t382, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16020_GM};
static MethodInfo* t2921_MIs[] =
{
	&m16016_MI,
	&m16017_MI,
	&m16018_MI,
	&m16019_MI,
	&m16020_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2921_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16017_MI,
	&m16019_MI,
	&m16018_MI,
	&m16020_MI,
};
static TypeInfo* t2921_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4373_TI,
};
static Il2CppInterfaceOffsetPair t2921_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4373_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2921_0_0_0;
extern Il2CppType t2921_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2921_GC;
extern TypeInfo t20_TI;
TypeInfo t2921_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2921_MIs, t2921_PIs, t2921_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2921_TI, t2921_ITIs, t2921_VT, &EmptyCustomAttributesCache, &t2921_TI, &t2921_0_0_0, &t2921_1_0_0, t2921_IOs, &t2921_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2921)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5604_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>
extern MethodInfo m29336_MI;
static PropertyInfo t5604____Count_PropertyInfo = 
{
	&t5604_TI, "Count", &m29336_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29337_MI;
static PropertyInfo t5604____IsReadOnly_PropertyInfo = 
{
	&t5604_TI, "IsReadOnly", &m29337_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5604_PIs[] =
{
	&t5604____Count_PropertyInfo,
	&t5604____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29336_GM;
MethodInfo m29336_MI = 
{
	"get_Count", NULL, &t5604_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29336_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29337_GM;
MethodInfo m29337_MI = 
{
	"get_IsReadOnly", NULL, &t5604_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29337_GM};
extern Il2CppType t382_0_0_0;
extern Il2CppType t382_0_0_0;
static ParameterInfo t5604_m29338_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t382_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29338_GM;
MethodInfo m29338_MI = 
{
	"Add", NULL, &t5604_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5604_m29338_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29338_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29339_GM;
MethodInfo m29339_MI = 
{
	"Clear", NULL, &t5604_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29339_GM};
extern Il2CppType t382_0_0_0;
static ParameterInfo t5604_m29340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t382_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29340_GM;
MethodInfo m29340_MI = 
{
	"Contains", NULL, &t5604_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5604_m29340_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29340_GM};
extern Il2CppType t3750_0_0_0;
extern Il2CppType t3750_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5604_m29341_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3750_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29341_GM;
MethodInfo m29341_MI = 
{
	"CopyTo", NULL, &t5604_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5604_m29341_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29341_GM};
extern Il2CppType t382_0_0_0;
static ParameterInfo t5604_m29342_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t382_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29342_GM;
MethodInfo m29342_MI = 
{
	"Remove", NULL, &t5604_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5604_m29342_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29342_GM};
static MethodInfo* t5604_MIs[] =
{
	&m29336_MI,
	&m29337_MI,
	&m29338_MI,
	&m29339_MI,
	&m29340_MI,
	&m29341_MI,
	&m29342_MI,
	NULL
};
extern TypeInfo t5606_TI;
static TypeInfo* t5604_ITIs[] = 
{
	&t603_TI,
	&t5606_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5604_0_0_0;
extern Il2CppType t5604_1_0_0;
struct t5604;
extern Il2CppGenericClass t5604_GC;
TypeInfo t5604_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5604_MIs, t5604_PIs, NULL, NULL, NULL, NULL, NULL, &t5604_TI, t5604_ITIs, NULL, &EmptyCustomAttributesCache, &t5604_TI, &t5604_0_0_0, &t5604_1_0_0, NULL, &t5604_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventModifiers>
extern Il2CppType t4373_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29343_GM;
MethodInfo m29343_MI = 
{
	"GetEnumerator", NULL, &t5606_TI, &t4373_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29343_GM};
static MethodInfo* t5606_MIs[] =
{
	&m29343_MI,
	NULL
};
static TypeInfo* t5606_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5606_0_0_0;
extern Il2CppType t5606_1_0_0;
struct t5606;
extern Il2CppGenericClass t5606_GC;
TypeInfo t5606_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5606_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5606_TI, t5606_ITIs, NULL, &EmptyCustomAttributesCache, &t5606_TI, &t5606_0_0_0, &t5606_1_0_0, NULL, &t5606_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5605_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventModifiers>
extern MethodInfo m29344_MI;
extern MethodInfo m29345_MI;
static PropertyInfo t5605____Item_PropertyInfo = 
{
	&t5605_TI, "Item", &m29344_MI, &m29345_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5605_PIs[] =
{
	&t5605____Item_PropertyInfo,
	NULL
};
extern Il2CppType t382_0_0_0;
static ParameterInfo t5605_m29346_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t382_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29346_GM;
MethodInfo m29346_MI = 
{
	"IndexOf", NULL, &t5605_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5605_m29346_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29346_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t382_0_0_0;
static ParameterInfo t5605_m29347_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t382_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29347_GM;
MethodInfo m29347_MI = 
{
	"Insert", NULL, &t5605_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5605_m29347_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29347_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5605_m29348_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29348_GM;
MethodInfo m29348_MI = 
{
	"RemoveAt", NULL, &t5605_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5605_m29348_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29348_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5605_m29344_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t382_0_0_0;
extern void* RuntimeInvoker_t382_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29344_GM;
MethodInfo m29344_MI = 
{
	"get_Item", NULL, &t5605_TI, &t382_0_0_0, RuntimeInvoker_t382_t44, t5605_m29344_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29344_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t382_0_0_0;
static ParameterInfo t5605_m29345_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t382_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29345_GM;
MethodInfo m29345_MI = 
{
	"set_Item", NULL, &t5605_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5605_m29345_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29345_GM};
static MethodInfo* t5605_MIs[] =
{
	&m29346_MI,
	&m29347_MI,
	&m29348_MI,
	&m29344_MI,
	&m29345_MI,
	NULL
};
static TypeInfo* t5605_ITIs[] = 
{
	&t603_TI,
	&t5604_TI,
	&t5606_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5605_0_0_0;
extern Il2CppType t5605_1_0_0;
struct t5605;
extern Il2CppGenericClass t5605_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5605_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5605_MIs, t5605_PIs, NULL, NULL, NULL, NULL, NULL, &t5605_TI, t5605_ITIs, NULL, &t1908__CustomAttributeCache, &t5605_TI, &t5605_0_0_0, &t5605_1_0_0, NULL, &t5605_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4375_TI;

#include "t393.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.DrivenTransformProperties>
extern MethodInfo m29349_MI;
static PropertyInfo t4375____Current_PropertyInfo = 
{
	&t4375_TI, "Current", &m29349_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4375_PIs[] =
{
	&t4375____Current_PropertyInfo,
	NULL
};
extern Il2CppType t393_0_0_0;
extern void* RuntimeInvoker_t393 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29349_GM;
MethodInfo m29349_MI = 
{
	"get_Current", NULL, &t4375_TI, &t393_0_0_0, RuntimeInvoker_t393, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29349_GM};
static MethodInfo* t4375_MIs[] =
{
	&m29349_MI,
	NULL
};
static TypeInfo* t4375_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4375_0_0_0;
extern Il2CppType t4375_1_0_0;
struct t4375;
extern Il2CppGenericClass t4375_GC;
TypeInfo t4375_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4375_MIs, t4375_PIs, NULL, NULL, NULL, NULL, NULL, &t4375_TI, t4375_ITIs, NULL, &EmptyCustomAttributesCache, &t4375_TI, &t4375_0_0_0, &t4375_1_0_0, NULL, &t4375_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2922.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2922_TI;
#include "t2922MD.h"

extern TypeInfo t393_TI;
extern MethodInfo m16025_MI;
extern MethodInfo m22277_MI;
struct t20;
 int32_t m22277 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16021_MI;
 void m16021 (t2922 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16022_MI;
 t29 * m16022 (t2922 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16025(__this, &m16025_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t393_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16023_MI;
 void m16023 (t2922 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16024_MI;
 bool m16024 (t2922 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16025 (t2922 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22277(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22277_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>
extern Il2CppType t20_0_0_1;
FieldInfo t2922_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2922_TI, offsetof(t2922, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2922_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2922_TI, offsetof(t2922, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2922_FIs[] =
{
	&t2922_f0_FieldInfo,
	&t2922_f1_FieldInfo,
	NULL
};
static PropertyInfo t2922____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2922_TI, "System.Collections.IEnumerator.Current", &m16022_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2922____Current_PropertyInfo = 
{
	&t2922_TI, "Current", &m16025_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2922_PIs[] =
{
	&t2922____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2922____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2922_m16021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16021_GM;
MethodInfo m16021_MI = 
{
	".ctor", (methodPointerType)&m16021, &t2922_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2922_m16021_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16021_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16022_GM;
MethodInfo m16022_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16022, &t2922_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16022_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16023_GM;
MethodInfo m16023_MI = 
{
	"Dispose", (methodPointerType)&m16023, &t2922_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16023_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16024_GM;
MethodInfo m16024_MI = 
{
	"MoveNext", (methodPointerType)&m16024, &t2922_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16024_GM};
extern Il2CppType t393_0_0_0;
extern void* RuntimeInvoker_t393 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16025_GM;
MethodInfo m16025_MI = 
{
	"get_Current", (methodPointerType)&m16025, &t2922_TI, &t393_0_0_0, RuntimeInvoker_t393, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16025_GM};
static MethodInfo* t2922_MIs[] =
{
	&m16021_MI,
	&m16022_MI,
	&m16023_MI,
	&m16024_MI,
	&m16025_MI,
	NULL
};
static MethodInfo* t2922_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16022_MI,
	&m16024_MI,
	&m16023_MI,
	&m16025_MI,
};
static TypeInfo* t2922_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4375_TI,
};
static Il2CppInterfaceOffsetPair t2922_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4375_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2922_0_0_0;
extern Il2CppType t2922_1_0_0;
extern Il2CppGenericClass t2922_GC;
TypeInfo t2922_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2922_MIs, t2922_PIs, t2922_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2922_TI, t2922_ITIs, t2922_VT, &EmptyCustomAttributesCache, &t2922_TI, &t2922_0_0_0, &t2922_1_0_0, t2922_IOs, &t2922_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2922)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5607_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>
extern MethodInfo m29350_MI;
static PropertyInfo t5607____Count_PropertyInfo = 
{
	&t5607_TI, "Count", &m29350_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29351_MI;
static PropertyInfo t5607____IsReadOnly_PropertyInfo = 
{
	&t5607_TI, "IsReadOnly", &m29351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5607_PIs[] =
{
	&t5607____Count_PropertyInfo,
	&t5607____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29350_GM;
MethodInfo m29350_MI = 
{
	"get_Count", NULL, &t5607_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29350_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29351_GM;
MethodInfo m29351_MI = 
{
	"get_IsReadOnly", NULL, &t5607_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29351_GM};
extern Il2CppType t393_0_0_0;
extern Il2CppType t393_0_0_0;
static ParameterInfo t5607_m29352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t393_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29352_GM;
MethodInfo m29352_MI = 
{
	"Add", NULL, &t5607_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5607_m29352_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29352_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29353_GM;
MethodInfo m29353_MI = 
{
	"Clear", NULL, &t5607_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29353_GM};
extern Il2CppType t393_0_0_0;
static ParameterInfo t5607_m29354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t393_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29354_GM;
MethodInfo m29354_MI = 
{
	"Contains", NULL, &t5607_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5607_m29354_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29354_GM};
extern Il2CppType t3751_0_0_0;
extern Il2CppType t3751_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5607_m29355_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3751_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29355_GM;
MethodInfo m29355_MI = 
{
	"CopyTo", NULL, &t5607_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5607_m29355_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29355_GM};
extern Il2CppType t393_0_0_0;
static ParameterInfo t5607_m29356_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t393_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29356_GM;
MethodInfo m29356_MI = 
{
	"Remove", NULL, &t5607_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5607_m29356_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29356_GM};
static MethodInfo* t5607_MIs[] =
{
	&m29350_MI,
	&m29351_MI,
	&m29352_MI,
	&m29353_MI,
	&m29354_MI,
	&m29355_MI,
	&m29356_MI,
	NULL
};
extern TypeInfo t5609_TI;
static TypeInfo* t5607_ITIs[] = 
{
	&t603_TI,
	&t5609_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5607_0_0_0;
extern Il2CppType t5607_1_0_0;
struct t5607;
extern Il2CppGenericClass t5607_GC;
TypeInfo t5607_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5607_MIs, t5607_PIs, NULL, NULL, NULL, NULL, NULL, &t5607_TI, t5607_ITIs, NULL, &EmptyCustomAttributesCache, &t5607_TI, &t5607_0_0_0, &t5607_1_0_0, NULL, &t5607_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.DrivenTransformProperties>
extern Il2CppType t4375_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29357_GM;
MethodInfo m29357_MI = 
{
	"GetEnumerator", NULL, &t5609_TI, &t4375_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29357_GM};
static MethodInfo* t5609_MIs[] =
{
	&m29357_MI,
	NULL
};
static TypeInfo* t5609_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5609_0_0_0;
extern Il2CppType t5609_1_0_0;
struct t5609;
extern Il2CppGenericClass t5609_GC;
TypeInfo t5609_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5609_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5609_TI, t5609_ITIs, NULL, &EmptyCustomAttributesCache, &t5609_TI, &t5609_0_0_0, &t5609_1_0_0, NULL, &t5609_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5608_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>
extern MethodInfo m29358_MI;
extern MethodInfo m29359_MI;
static PropertyInfo t5608____Item_PropertyInfo = 
{
	&t5608_TI, "Item", &m29358_MI, &m29359_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5608_PIs[] =
{
	&t5608____Item_PropertyInfo,
	NULL
};
extern Il2CppType t393_0_0_0;
static ParameterInfo t5608_m29360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t393_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29360_GM;
MethodInfo m29360_MI = 
{
	"IndexOf", NULL, &t5608_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5608_m29360_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29360_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t393_0_0_0;
static ParameterInfo t5608_m29361_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t393_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29361_GM;
MethodInfo m29361_MI = 
{
	"Insert", NULL, &t5608_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5608_m29361_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29361_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5608_m29362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29362_GM;
MethodInfo m29362_MI = 
{
	"RemoveAt", NULL, &t5608_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5608_m29362_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29362_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5608_m29358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t393_0_0_0;
extern void* RuntimeInvoker_t393_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29358_GM;
MethodInfo m29358_MI = 
{
	"get_Item", NULL, &t5608_TI, &t393_0_0_0, RuntimeInvoker_t393_t44, t5608_m29358_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29358_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t393_0_0_0;
static ParameterInfo t5608_m29359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t393_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29359_GM;
MethodInfo m29359_MI = 
{
	"set_Item", NULL, &t5608_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5608_m29359_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29359_GM};
static MethodInfo* t5608_MIs[] =
{
	&m29360_MI,
	&m29361_MI,
	&m29362_MI,
	&m29358_MI,
	&m29359_MI,
	NULL
};
static TypeInfo* t5608_ITIs[] = 
{
	&t603_TI,
	&t5607_TI,
	&t5609_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5608_0_0_0;
extern Il2CppType t5608_1_0_0;
struct t5608;
extern Il2CppGenericClass t5608_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5608_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5608_MIs, t5608_PIs, NULL, NULL, NULL, NULL, NULL, &t5608_TI, t5608_ITIs, NULL, &t1908__CustomAttributeCache, &t5608_TI, &t5608_0_0_0, &t5608_1_0_0, NULL, &t5608_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2923.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2923_TI;
#include "t2923MD.h"

#include "t41.h"
#include "t557.h"
#include "t2.h"
#include "mscorlib_ArrayTypes.h"
#include "t2924.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2_TI;
extern TypeInfo t2924_TI;
extern TypeInfo t21_TI;
#include "t2924MD.h"
extern MethodInfo m16028_MI;
extern MethodInfo m16030_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>
extern Il2CppType t316_0_0_33;
FieldInfo t2923_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2923_TI, offsetof(t2923, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2923_FIs[] =
{
	&t2923_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2923_m16026_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16026_GM;
MethodInfo m16026_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2923_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2923_m16026_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16026_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2923_m16027_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16027_GM;
MethodInfo m16027_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2923_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2923_m16027_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16027_GM};
static MethodInfo* t2923_MIs[] =
{
	&m16026_MI,
	&m16027_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m16027_MI;
extern MethodInfo m16031_MI;
static MethodInfo* t2923_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16027_MI,
	&m16031_MI,
};
extern Il2CppType t2925_0_0_0;
extern TypeInfo t2925_TI;
extern MethodInfo m22287_MI;
extern TypeInfo t2_TI;
extern MethodInfo m16033_MI;
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2923_RGCTXData[8] = 
{
	&t2925_0_0_0/* Type Usage */,
	&t2925_TI/* Class Usage */,
	&m22287_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m16033_MI/* Method Usage */,
	&m16028_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m16030_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2923_0_0_0;
extern Il2CppType t2923_1_0_0;
struct t2923;
extern Il2CppGenericClass t2923_GC;
TypeInfo t2923_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2923_MIs, NULL, t2923_FIs, NULL, &t2924_TI, NULL, NULL, &t2923_TI, NULL, t2923_VT, &EmptyCustomAttributesCache, &t2923_TI, &t2923_0_0_0, &t2923_1_0_0, NULL, &t2923_GC, NULL, NULL, NULL, t2923_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2923), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2925.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2925_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2925MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m22287(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>
extern Il2CppType t2925_0_0_1;
FieldInfo t2924_f0_FieldInfo = 
{
	"Delegate", &t2925_0_0_1, &t2924_TI, offsetof(t2924, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2924_FIs[] =
{
	&t2924_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2924_m16028_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16028_GM;
MethodInfo m16028_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2924_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2924_m16028_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16028_GM};
extern Il2CppType t2925_0_0_0;
static ParameterInfo t2924_m16029_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2925_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16029_GM;
MethodInfo m16029_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2924_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2924_m16029_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16029_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2924_m16030_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16030_GM;
MethodInfo m16030_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2924_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2924_m16030_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16030_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2924_m16031_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16031_GM;
MethodInfo m16031_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2924_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2924_m16031_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16031_GM};
static MethodInfo* t2924_MIs[] =
{
	&m16028_MI,
	&m16029_MI,
	&m16030_MI,
	&m16031_MI,
	NULL
};
static MethodInfo* t2924_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16030_MI,
	&m16031_MI,
};
extern TypeInfo t2925_TI;
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2924_RGCTXData[5] = 
{
	&t2925_0_0_0/* Type Usage */,
	&t2925_TI/* Class Usage */,
	&m22287_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m16033_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2924_0_0_0;
extern Il2CppType t2924_1_0_0;
extern TypeInfo t556_TI;
struct t2924;
extern Il2CppGenericClass t2924_GC;
TypeInfo t2924_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2924_MIs, NULL, t2924_FIs, NULL, &t556_TI, NULL, NULL, &t2924_TI, NULL, t2924_VT, &EmptyCustomAttributesCache, &t2924_TI, &t2924_0_0_0, &t2924_1_0_0, NULL, &t2924_GC, NULL, NULL, NULL, t2924_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2924), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2925_m16032_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16032_GM;
MethodInfo m16032_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2925_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2925_m16032_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16032_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2925_m16033_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16033_GM;
MethodInfo m16033_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2925_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2925_m16033_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16033_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2925_m16034_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16034_GM;
MethodInfo m16034_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2925_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2925_m16034_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16034_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2925_m16035_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16035_GM;
MethodInfo m16035_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2925_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2925_m16035_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16035_GM};
static MethodInfo* t2925_MIs[] =
{
	&m16032_MI,
	&m16033_MI,
	&m16034_MI,
	&m16035_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m16034_MI;
extern MethodInfo m16035_MI;
static MethodInfo* t2925_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16033_MI,
	&m16034_MI,
	&m16035_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2925_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2925_1_0_0;
extern TypeInfo t195_TI;
struct t2925;
extern Il2CppGenericClass t2925_GC;
TypeInfo t2925_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2925_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2925_TI, NULL, t2925_VT, &EmptyCustomAttributesCache, &t2925_TI, &t2925_0_0_0, &t2925_1_0_0, t2925_IOs, &t2925_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2925), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4377_TI;

#include "t413.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Edge>
extern MethodInfo m29363_MI;
static PropertyInfo t4377____Current_PropertyInfo = 
{
	&t4377_TI, "Current", &m29363_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4377_PIs[] =
{
	&t4377____Current_PropertyInfo,
	NULL
};
extern Il2CppType t413_0_0_0;
extern void* RuntimeInvoker_t413 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29363_GM;
MethodInfo m29363_MI = 
{
	"get_Current", NULL, &t4377_TI, &t413_0_0_0, RuntimeInvoker_t413, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29363_GM};
static MethodInfo* t4377_MIs[] =
{
	&m29363_MI,
	NULL
};
static TypeInfo* t4377_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4377_0_0_0;
extern Il2CppType t4377_1_0_0;
struct t4377;
extern Il2CppGenericClass t4377_GC;
TypeInfo t4377_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4377_MIs, t4377_PIs, NULL, NULL, NULL, NULL, NULL, &t4377_TI, t4377_ITIs, NULL, &EmptyCustomAttributesCache, &t4377_TI, &t4377_0_0_0, &t4377_1_0_0, NULL, &t4377_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2926.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2926_TI;
#include "t2926MD.h"

extern TypeInfo t413_TI;
extern MethodInfo m16040_MI;
extern MethodInfo m22289_MI;
struct t20;
 int32_t m22289 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16036_MI;
 void m16036 (t2926 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16037_MI;
 t29 * m16037 (t2926 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16040(__this, &m16040_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t413_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16038_MI;
 void m16038 (t2926 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16039_MI;
 bool m16039 (t2926 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16040 (t2926 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22289(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22289_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>
extern Il2CppType t20_0_0_1;
FieldInfo t2926_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2926_TI, offsetof(t2926, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2926_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2926_TI, offsetof(t2926, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2926_FIs[] =
{
	&t2926_f0_FieldInfo,
	&t2926_f1_FieldInfo,
	NULL
};
static PropertyInfo t2926____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2926_TI, "System.Collections.IEnumerator.Current", &m16037_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2926____Current_PropertyInfo = 
{
	&t2926_TI, "Current", &m16040_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2926_PIs[] =
{
	&t2926____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2926____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2926_m16036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16036_GM;
MethodInfo m16036_MI = 
{
	".ctor", (methodPointerType)&m16036, &t2926_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2926_m16036_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16036_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16037_GM;
MethodInfo m16037_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16037, &t2926_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16037_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16038_GM;
MethodInfo m16038_MI = 
{
	"Dispose", (methodPointerType)&m16038, &t2926_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16038_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16039_GM;
MethodInfo m16039_MI = 
{
	"MoveNext", (methodPointerType)&m16039, &t2926_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16039_GM};
extern Il2CppType t413_0_0_0;
extern void* RuntimeInvoker_t413 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16040_GM;
MethodInfo m16040_MI = 
{
	"get_Current", (methodPointerType)&m16040, &t2926_TI, &t413_0_0_0, RuntimeInvoker_t413, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16040_GM};
static MethodInfo* t2926_MIs[] =
{
	&m16036_MI,
	&m16037_MI,
	&m16038_MI,
	&m16039_MI,
	&m16040_MI,
	NULL
};
static MethodInfo* t2926_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16037_MI,
	&m16039_MI,
	&m16038_MI,
	&m16040_MI,
};
static TypeInfo* t2926_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4377_TI,
};
static Il2CppInterfaceOffsetPair t2926_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4377_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2926_0_0_0;
extern Il2CppType t2926_1_0_0;
extern Il2CppGenericClass t2926_GC;
TypeInfo t2926_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2926_MIs, t2926_PIs, t2926_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2926_TI, t2926_ITIs, t2926_VT, &EmptyCustomAttributesCache, &t2926_TI, &t2926_0_0_0, &t2926_1_0_0, t2926_IOs, &t2926_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2926)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5610_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>
extern MethodInfo m29364_MI;
static PropertyInfo t5610____Count_PropertyInfo = 
{
	&t5610_TI, "Count", &m29364_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29365_MI;
static PropertyInfo t5610____IsReadOnly_PropertyInfo = 
{
	&t5610_TI, "IsReadOnly", &m29365_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5610_PIs[] =
{
	&t5610____Count_PropertyInfo,
	&t5610____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29364_GM;
MethodInfo m29364_MI = 
{
	"get_Count", NULL, &t5610_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29364_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29365_GM;
MethodInfo m29365_MI = 
{
	"get_IsReadOnly", NULL, &t5610_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29365_GM};
extern Il2CppType t413_0_0_0;
extern Il2CppType t413_0_0_0;
static ParameterInfo t5610_m29366_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29366_GM;
MethodInfo m29366_MI = 
{
	"Add", NULL, &t5610_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5610_m29366_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29366_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29367_GM;
MethodInfo m29367_MI = 
{
	"Clear", NULL, &t5610_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29367_GM};
extern Il2CppType t413_0_0_0;
static ParameterInfo t5610_m29368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t413_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29368_GM;
MethodInfo m29368_MI = 
{
	"Contains", NULL, &t5610_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5610_m29368_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29368_GM};
extern Il2CppType t3752_0_0_0;
extern Il2CppType t3752_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5610_m29369_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3752_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29369_GM;
MethodInfo m29369_MI = 
{
	"CopyTo", NULL, &t5610_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5610_m29369_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29369_GM};
extern Il2CppType t413_0_0_0;
static ParameterInfo t5610_m29370_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t413_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29370_GM;
MethodInfo m29370_MI = 
{
	"Remove", NULL, &t5610_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5610_m29370_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29370_GM};
static MethodInfo* t5610_MIs[] =
{
	&m29364_MI,
	&m29365_MI,
	&m29366_MI,
	&m29367_MI,
	&m29368_MI,
	&m29369_MI,
	&m29370_MI,
	NULL
};
extern TypeInfo t5612_TI;
static TypeInfo* t5610_ITIs[] = 
{
	&t603_TI,
	&t5612_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5610_0_0_0;
extern Il2CppType t5610_1_0_0;
struct t5610;
extern Il2CppGenericClass t5610_GC;
TypeInfo t5610_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5610_MIs, t5610_PIs, NULL, NULL, NULL, NULL, NULL, &t5610_TI, t5610_ITIs, NULL, &EmptyCustomAttributesCache, &t5610_TI, &t5610_0_0_0, &t5610_1_0_0, NULL, &t5610_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Edge>
extern Il2CppType t4377_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29371_GM;
MethodInfo m29371_MI = 
{
	"GetEnumerator", NULL, &t5612_TI, &t4377_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29371_GM};
static MethodInfo* t5612_MIs[] =
{
	&m29371_MI,
	NULL
};
static TypeInfo* t5612_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5612_0_0_0;
extern Il2CppType t5612_1_0_0;
struct t5612;
extern Il2CppGenericClass t5612_GC;
TypeInfo t5612_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5612_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5612_TI, t5612_ITIs, NULL, &EmptyCustomAttributesCache, &t5612_TI, &t5612_0_0_0, &t5612_1_0_0, NULL, &t5612_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5611_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>
extern MethodInfo m29372_MI;
extern MethodInfo m29373_MI;
static PropertyInfo t5611____Item_PropertyInfo = 
{
	&t5611_TI, "Item", &m29372_MI, &m29373_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5611_PIs[] =
{
	&t5611____Item_PropertyInfo,
	NULL
};
extern Il2CppType t413_0_0_0;
static ParameterInfo t5611_m29374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t413_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29374_GM;
MethodInfo m29374_MI = 
{
	"IndexOf", NULL, &t5611_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5611_m29374_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29374_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t413_0_0_0;
static ParameterInfo t5611_m29375_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29375_GM;
MethodInfo m29375_MI = 
{
	"Insert", NULL, &t5611_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5611_m29375_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29375_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5611_m29376_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29376_GM;
MethodInfo m29376_MI = 
{
	"RemoveAt", NULL, &t5611_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5611_m29376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29376_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5611_m29372_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t413_0_0_0;
extern void* RuntimeInvoker_t413_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29372_GM;
MethodInfo m29372_MI = 
{
	"get_Item", NULL, &t5611_TI, &t413_0_0_0, RuntimeInvoker_t413_t44, t5611_m29372_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29372_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t413_0_0_0;
static ParameterInfo t5611_m29373_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29373_GM;
MethodInfo m29373_MI = 
{
	"set_Item", NULL, &t5611_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5611_m29373_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29373_GM};
static MethodInfo* t5611_MIs[] =
{
	&m29374_MI,
	&m29375_MI,
	&m29376_MI,
	&m29372_MI,
	&m29373_MI,
	NULL
};
static TypeInfo* t5611_ITIs[] = 
{
	&t603_TI,
	&t5610_TI,
	&t5612_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5611_0_0_0;
extern Il2CppType t5611_1_0_0;
struct t5611;
extern Il2CppGenericClass t5611_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5611_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5611_MIs, t5611_PIs, NULL, NULL, NULL, NULL, NULL, &t5611_TI, t5611_ITIs, NULL, &t1908__CustomAttributeCache, &t5611_TI, &t5611_0_0_0, &t5611_1_0_0, NULL, &t5611_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4379_TI;

#include "t408.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Axis>
extern MethodInfo m29377_MI;
static PropertyInfo t4379____Current_PropertyInfo = 
{
	&t4379_TI, "Current", &m29377_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4379_PIs[] =
{
	&t4379____Current_PropertyInfo,
	NULL
};
extern Il2CppType t408_0_0_0;
extern void* RuntimeInvoker_t408 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29377_GM;
MethodInfo m29377_MI = 
{
	"get_Current", NULL, &t4379_TI, &t408_0_0_0, RuntimeInvoker_t408, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29377_GM};
static MethodInfo* t4379_MIs[] =
{
	&m29377_MI,
	NULL
};
static TypeInfo* t4379_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4379_0_0_0;
extern Il2CppType t4379_1_0_0;
struct t4379;
extern Il2CppGenericClass t4379_GC;
TypeInfo t4379_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4379_MIs, t4379_PIs, NULL, NULL, NULL, NULL, NULL, &t4379_TI, t4379_ITIs, NULL, &EmptyCustomAttributesCache, &t4379_TI, &t4379_0_0_0, &t4379_1_0_0, NULL, &t4379_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2927.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2927_TI;
#include "t2927MD.h"

extern TypeInfo t408_TI;
extern MethodInfo m16045_MI;
extern MethodInfo m22300_MI;
struct t20;
 int32_t m22300 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16041_MI;
 void m16041 (t2927 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16042_MI;
 t29 * m16042 (t2927 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16045(__this, &m16045_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t408_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16043_MI;
 void m16043 (t2927 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16044_MI;
 bool m16044 (t2927 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16045 (t2927 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22300(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22300_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>
extern Il2CppType t20_0_0_1;
FieldInfo t2927_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2927_TI, offsetof(t2927, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2927_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2927_TI, offsetof(t2927, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2927_FIs[] =
{
	&t2927_f0_FieldInfo,
	&t2927_f1_FieldInfo,
	NULL
};
static PropertyInfo t2927____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2927_TI, "System.Collections.IEnumerator.Current", &m16042_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2927____Current_PropertyInfo = 
{
	&t2927_TI, "Current", &m16045_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2927_PIs[] =
{
	&t2927____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2927____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2927_m16041_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16041_GM;
MethodInfo m16041_MI = 
{
	".ctor", (methodPointerType)&m16041, &t2927_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2927_m16041_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16041_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16042_GM;
MethodInfo m16042_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16042, &t2927_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16042_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16043_GM;
MethodInfo m16043_MI = 
{
	"Dispose", (methodPointerType)&m16043, &t2927_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16043_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16044_GM;
MethodInfo m16044_MI = 
{
	"MoveNext", (methodPointerType)&m16044, &t2927_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16044_GM};
extern Il2CppType t408_0_0_0;
extern void* RuntimeInvoker_t408 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16045_GM;
MethodInfo m16045_MI = 
{
	"get_Current", (methodPointerType)&m16045, &t2927_TI, &t408_0_0_0, RuntimeInvoker_t408, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16045_GM};
static MethodInfo* t2927_MIs[] =
{
	&m16041_MI,
	&m16042_MI,
	&m16043_MI,
	&m16044_MI,
	&m16045_MI,
	NULL
};
static MethodInfo* t2927_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16042_MI,
	&m16044_MI,
	&m16043_MI,
	&m16045_MI,
};
static TypeInfo* t2927_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4379_TI,
};
static Il2CppInterfaceOffsetPair t2927_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4379_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2927_0_0_0;
extern Il2CppType t2927_1_0_0;
extern Il2CppGenericClass t2927_GC;
TypeInfo t2927_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2927_MIs, t2927_PIs, t2927_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2927_TI, t2927_ITIs, t2927_VT, &EmptyCustomAttributesCache, &t2927_TI, &t2927_0_0_0, &t2927_1_0_0, t2927_IOs, &t2927_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2927)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5613_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>
extern MethodInfo m29378_MI;
static PropertyInfo t5613____Count_PropertyInfo = 
{
	&t5613_TI, "Count", &m29378_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29379_MI;
static PropertyInfo t5613____IsReadOnly_PropertyInfo = 
{
	&t5613_TI, "IsReadOnly", &m29379_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5613_PIs[] =
{
	&t5613____Count_PropertyInfo,
	&t5613____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29378_GM;
MethodInfo m29378_MI = 
{
	"get_Count", NULL, &t5613_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29378_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29379_GM;
MethodInfo m29379_MI = 
{
	"get_IsReadOnly", NULL, &t5613_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29379_GM};
extern Il2CppType t408_0_0_0;
extern Il2CppType t408_0_0_0;
static ParameterInfo t5613_m29380_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29380_GM;
MethodInfo m29380_MI = 
{
	"Add", NULL, &t5613_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5613_m29380_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29380_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29381_GM;
MethodInfo m29381_MI = 
{
	"Clear", NULL, &t5613_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29381_GM};
extern Il2CppType t408_0_0_0;
static ParameterInfo t5613_m29382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t408_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29382_GM;
MethodInfo m29382_MI = 
{
	"Contains", NULL, &t5613_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5613_m29382_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29382_GM};
extern Il2CppType t3753_0_0_0;
extern Il2CppType t3753_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5613_m29383_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3753_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29383_GM;
MethodInfo m29383_MI = 
{
	"CopyTo", NULL, &t5613_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5613_m29383_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29383_GM};
extern Il2CppType t408_0_0_0;
static ParameterInfo t5613_m29384_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t408_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29384_GM;
MethodInfo m29384_MI = 
{
	"Remove", NULL, &t5613_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5613_m29384_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29384_GM};
static MethodInfo* t5613_MIs[] =
{
	&m29378_MI,
	&m29379_MI,
	&m29380_MI,
	&m29381_MI,
	&m29382_MI,
	&m29383_MI,
	&m29384_MI,
	NULL
};
extern TypeInfo t5615_TI;
static TypeInfo* t5613_ITIs[] = 
{
	&t603_TI,
	&t5615_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5613_0_0_0;
extern Il2CppType t5613_1_0_0;
struct t5613;
extern Il2CppGenericClass t5613_GC;
TypeInfo t5613_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5613_MIs, t5613_PIs, NULL, NULL, NULL, NULL, NULL, &t5613_TI, t5613_ITIs, NULL, &EmptyCustomAttributesCache, &t5613_TI, &t5613_0_0_0, &t5613_1_0_0, NULL, &t5613_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Axis>
extern Il2CppType t4379_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29385_GM;
MethodInfo m29385_MI = 
{
	"GetEnumerator", NULL, &t5615_TI, &t4379_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29385_GM};
static MethodInfo* t5615_MIs[] =
{
	&m29385_MI,
	NULL
};
static TypeInfo* t5615_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5615_0_0_0;
extern Il2CppType t5615_1_0_0;
struct t5615;
extern Il2CppGenericClass t5615_GC;
TypeInfo t5615_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5615_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5615_TI, t5615_ITIs, NULL, &EmptyCustomAttributesCache, &t5615_TI, &t5615_0_0_0, &t5615_1_0_0, NULL, &t5615_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5614_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>
extern MethodInfo m29386_MI;
extern MethodInfo m29387_MI;
static PropertyInfo t5614____Item_PropertyInfo = 
{
	&t5614_TI, "Item", &m29386_MI, &m29387_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5614_PIs[] =
{
	&t5614____Item_PropertyInfo,
	NULL
};
extern Il2CppType t408_0_0_0;
static ParameterInfo t5614_m29388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t408_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29388_GM;
MethodInfo m29388_MI = 
{
	"IndexOf", NULL, &t5614_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5614_m29388_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29388_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t408_0_0_0;
static ParameterInfo t5614_m29389_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29389_GM;
MethodInfo m29389_MI = 
{
	"Insert", NULL, &t5614_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5614_m29389_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29389_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5614_m29390_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29390_GM;
MethodInfo m29390_MI = 
{
	"RemoveAt", NULL, &t5614_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5614_m29390_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29390_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5614_m29386_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t408_0_0_0;
extern void* RuntimeInvoker_t408_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29386_GM;
MethodInfo m29386_MI = 
{
	"get_Item", NULL, &t5614_TI, &t408_0_0_0, RuntimeInvoker_t408_t44, t5614_m29386_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29386_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t408_0_0_0;
static ParameterInfo t5614_m29387_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29387_GM;
MethodInfo m29387_MI = 
{
	"set_Item", NULL, &t5614_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5614_m29387_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29387_GM};
static MethodInfo* t5614_MIs[] =
{
	&m29388_MI,
	&m29389_MI,
	&m29390_MI,
	&m29386_MI,
	&m29387_MI,
	NULL
};
static TypeInfo* t5614_ITIs[] = 
{
	&t603_TI,
	&t5613_TI,
	&t5615_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5614_0_0_0;
extern Il2CppType t5614_1_0_0;
struct t5614;
extern Il2CppGenericClass t5614_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5614_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5614_MIs, t5614_PIs, NULL, NULL, NULL, NULL, NULL, &t5614_TI, t5614_ITIs, NULL, &t1908__CustomAttributeCache, &t5614_TI, &t5614_0_0_0, &t5614_1_0_0, NULL, &t5614_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4381_TI;

#include "t489.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SerializePrivateVariables>
extern MethodInfo m29391_MI;
static PropertyInfo t4381____Current_PropertyInfo = 
{
	&t4381_TI, "Current", &m29391_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4381_PIs[] =
{
	&t4381____Current_PropertyInfo,
	NULL
};
extern Il2CppType t489_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29391_GM;
MethodInfo m29391_MI = 
{
	"get_Current", NULL, &t4381_TI, &t489_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29391_GM};
static MethodInfo* t4381_MIs[] =
{
	&m29391_MI,
	NULL
};
static TypeInfo* t4381_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4381_0_0_0;
extern Il2CppType t4381_1_0_0;
struct t4381;
extern Il2CppGenericClass t4381_GC;
TypeInfo t4381_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4381_MIs, t4381_PIs, NULL, NULL, NULL, NULL, NULL, &t4381_TI, t4381_ITIs, NULL, &EmptyCustomAttributesCache, &t4381_TI, &t4381_0_0_0, &t4381_1_0_0, NULL, &t4381_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2928.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2928_TI;
#include "t2928MD.h"

extern TypeInfo t489_TI;
extern MethodInfo m16050_MI;
extern MethodInfo m22311_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m22311(__this, p0, method) (t489 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>
extern Il2CppType t20_0_0_1;
FieldInfo t2928_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2928_TI, offsetof(t2928, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2928_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2928_TI, offsetof(t2928, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2928_FIs[] =
{
	&t2928_f0_FieldInfo,
	&t2928_f1_FieldInfo,
	NULL
};
extern MethodInfo m16047_MI;
static PropertyInfo t2928____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2928_TI, "System.Collections.IEnumerator.Current", &m16047_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2928____Current_PropertyInfo = 
{
	&t2928_TI, "Current", &m16050_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2928_PIs[] =
{
	&t2928____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2928____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2928_m16046_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16046_GM;
MethodInfo m16046_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2928_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2928_m16046_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16046_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16047_GM;
MethodInfo m16047_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2928_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16047_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16048_GM;
MethodInfo m16048_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2928_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16048_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16049_GM;
MethodInfo m16049_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2928_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16049_GM};
extern Il2CppType t489_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16050_GM;
MethodInfo m16050_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2928_TI, &t489_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16050_GM};
static MethodInfo* t2928_MIs[] =
{
	&m16046_MI,
	&m16047_MI,
	&m16048_MI,
	&m16049_MI,
	&m16050_MI,
	NULL
};
extern MethodInfo m16049_MI;
extern MethodInfo m16048_MI;
static MethodInfo* t2928_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16047_MI,
	&m16049_MI,
	&m16048_MI,
	&m16050_MI,
};
static TypeInfo* t2928_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4381_TI,
};
static Il2CppInterfaceOffsetPair t2928_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4381_TI, 7},
};
extern TypeInfo t489_TI;
static Il2CppRGCTXData t2928_RGCTXData[3] = 
{
	&m16050_MI/* Method Usage */,
	&t489_TI/* Class Usage */,
	&m22311_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2928_0_0_0;
extern Il2CppType t2928_1_0_0;
extern Il2CppGenericClass t2928_GC;
TypeInfo t2928_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2928_MIs, t2928_PIs, t2928_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2928_TI, t2928_ITIs, t2928_VT, &EmptyCustomAttributesCache, &t2928_TI, &t2928_0_0_0, &t2928_1_0_0, t2928_IOs, &t2928_GC, NULL, NULL, NULL, t2928_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2928)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5616_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>
extern MethodInfo m29392_MI;
static PropertyInfo t5616____Count_PropertyInfo = 
{
	&t5616_TI, "Count", &m29392_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29393_MI;
static PropertyInfo t5616____IsReadOnly_PropertyInfo = 
{
	&t5616_TI, "IsReadOnly", &m29393_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5616_PIs[] =
{
	&t5616____Count_PropertyInfo,
	&t5616____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29392_GM;
MethodInfo m29392_MI = 
{
	"get_Count", NULL, &t5616_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29392_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29393_GM;
MethodInfo m29393_MI = 
{
	"get_IsReadOnly", NULL, &t5616_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29393_GM};
extern Il2CppType t489_0_0_0;
extern Il2CppType t489_0_0_0;
static ParameterInfo t5616_m29394_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t489_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29394_GM;
MethodInfo m29394_MI = 
{
	"Add", NULL, &t5616_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5616_m29394_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29394_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29395_GM;
MethodInfo m29395_MI = 
{
	"Clear", NULL, &t5616_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29395_GM};
extern Il2CppType t489_0_0_0;
static ParameterInfo t5616_m29396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t489_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29396_GM;
MethodInfo m29396_MI = 
{
	"Contains", NULL, &t5616_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5616_m29396_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29396_GM};
extern Il2CppType t3754_0_0_0;
extern Il2CppType t3754_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5616_m29397_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3754_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29397_GM;
MethodInfo m29397_MI = 
{
	"CopyTo", NULL, &t5616_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5616_m29397_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29397_GM};
extern Il2CppType t489_0_0_0;
static ParameterInfo t5616_m29398_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t489_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29398_GM;
MethodInfo m29398_MI = 
{
	"Remove", NULL, &t5616_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5616_m29398_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29398_GM};
static MethodInfo* t5616_MIs[] =
{
	&m29392_MI,
	&m29393_MI,
	&m29394_MI,
	&m29395_MI,
	&m29396_MI,
	&m29397_MI,
	&m29398_MI,
	NULL
};
extern TypeInfo t5618_TI;
static TypeInfo* t5616_ITIs[] = 
{
	&t603_TI,
	&t5618_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5616_0_0_0;
extern Il2CppType t5616_1_0_0;
struct t5616;
extern Il2CppGenericClass t5616_GC;
TypeInfo t5616_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5616_MIs, t5616_PIs, NULL, NULL, NULL, NULL, NULL, &t5616_TI, t5616_ITIs, NULL, &EmptyCustomAttributesCache, &t5616_TI, &t5616_0_0_0, &t5616_1_0_0, NULL, &t5616_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SerializePrivateVariables>
extern Il2CppType t4381_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29399_GM;
MethodInfo m29399_MI = 
{
	"GetEnumerator", NULL, &t5618_TI, &t4381_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29399_GM};
static MethodInfo* t5618_MIs[] =
{
	&m29399_MI,
	NULL
};
static TypeInfo* t5618_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5618_0_0_0;
extern Il2CppType t5618_1_0_0;
struct t5618;
extern Il2CppGenericClass t5618_GC;
TypeInfo t5618_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5618_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5618_TI, t5618_ITIs, NULL, &EmptyCustomAttributesCache, &t5618_TI, &t5618_0_0_0, &t5618_1_0_0, NULL, &t5618_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5617_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>
extern MethodInfo m29400_MI;
extern MethodInfo m29401_MI;
static PropertyInfo t5617____Item_PropertyInfo = 
{
	&t5617_TI, "Item", &m29400_MI, &m29401_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5617_PIs[] =
{
	&t5617____Item_PropertyInfo,
	NULL
};
extern Il2CppType t489_0_0_0;
static ParameterInfo t5617_m29402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t489_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29402_GM;
MethodInfo m29402_MI = 
{
	"IndexOf", NULL, &t5617_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5617_m29402_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29402_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t489_0_0_0;
static ParameterInfo t5617_m29403_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t489_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29403_GM;
MethodInfo m29403_MI = 
{
	"Insert", NULL, &t5617_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5617_m29403_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29403_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5617_m29404_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29404_GM;
MethodInfo m29404_MI = 
{
	"RemoveAt", NULL, &t5617_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5617_m29404_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29404_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5617_m29400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t489_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29400_GM;
MethodInfo m29400_MI = 
{
	"get_Item", NULL, &t5617_TI, &t489_0_0_0, RuntimeInvoker_t29_t44, t5617_m29400_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29400_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t489_0_0_0;
static ParameterInfo t5617_m29401_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t489_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29401_GM;
MethodInfo m29401_MI = 
{
	"set_Item", NULL, &t5617_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5617_m29401_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29401_GM};
static MethodInfo* t5617_MIs[] =
{
	&m29402_MI,
	&m29403_MI,
	&m29404_MI,
	&m29400_MI,
	&m29401_MI,
	NULL
};
static TypeInfo* t5617_ITIs[] = 
{
	&t603_TI,
	&t5616_TI,
	&t5618_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5617_0_0_0;
extern Il2CppType t5617_1_0_0;
struct t5617;
extern Il2CppGenericClass t5617_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5617_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5617_MIs, t5617_PIs, NULL, NULL, NULL, NULL, NULL, &t5617_TI, t5617_ITIs, NULL, &t1908__CustomAttributeCache, &t5617_TI, &t5617_0_0_0, &t5617_1_0_0, NULL, &t5617_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5619_TI;

#include "t490.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Attribute>
extern MethodInfo m29405_MI;
static PropertyInfo t5619____Count_PropertyInfo = 
{
	&t5619_TI, "Count", &m29405_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29406_MI;
static PropertyInfo t5619____IsReadOnly_PropertyInfo = 
{
	&t5619_TI, "IsReadOnly", &m29406_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5619_PIs[] =
{
	&t5619____Count_PropertyInfo,
	&t5619____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29405_GM;
MethodInfo m29405_MI = 
{
	"get_Count", NULL, &t5619_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29405_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29406_GM;
MethodInfo m29406_MI = 
{
	"get_IsReadOnly", NULL, &t5619_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29406_GM};
extern Il2CppType t490_0_0_0;
extern Il2CppType t490_0_0_0;
static ParameterInfo t5619_m29407_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29407_GM;
MethodInfo m29407_MI = 
{
	"Add", NULL, &t5619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5619_m29407_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29407_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29408_GM;
MethodInfo m29408_MI = 
{
	"Clear", NULL, &t5619_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29408_GM};
extern Il2CppType t490_0_0_0;
static ParameterInfo t5619_m29409_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t490_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29409_GM;
MethodInfo m29409_MI = 
{
	"Contains", NULL, &t5619_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5619_m29409_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29409_GM};
extern Il2CppType t3542_0_0_0;
extern Il2CppType t3542_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5619_m29410_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3542_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29410_GM;
MethodInfo m29410_MI = 
{
	"CopyTo", NULL, &t5619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5619_m29410_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29410_GM};
extern Il2CppType t490_0_0_0;
static ParameterInfo t5619_m29411_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t490_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29411_GM;
MethodInfo m29411_MI = 
{
	"Remove", NULL, &t5619_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5619_m29411_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29411_GM};
static MethodInfo* t5619_MIs[] =
{
	&m29405_MI,
	&m29406_MI,
	&m29407_MI,
	&m29408_MI,
	&m29409_MI,
	&m29410_MI,
	&m29411_MI,
	NULL
};
extern TypeInfo t5621_TI;
static TypeInfo* t5619_ITIs[] = 
{
	&t603_TI,
	&t5621_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5619_0_0_0;
extern Il2CppType t5619_1_0_0;
struct t5619;
extern Il2CppGenericClass t5619_GC;
TypeInfo t5619_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5619_MIs, t5619_PIs, NULL, NULL, NULL, NULL, NULL, &t5619_TI, t5619_ITIs, NULL, &EmptyCustomAttributesCache, &t5619_TI, &t5619_0_0_0, &t5619_1_0_0, NULL, &t5619_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Attribute>
extern Il2CppType t4383_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29412_GM;
MethodInfo m29412_MI = 
{
	"GetEnumerator", NULL, &t5621_TI, &t4383_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29412_GM};
static MethodInfo* t5621_MIs[] =
{
	&m29412_MI,
	NULL
};
static TypeInfo* t5621_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5621_0_0_0;
extern Il2CppType t5621_1_0_0;
struct t5621;
extern Il2CppGenericClass t5621_GC;
TypeInfo t5621_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5621_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5621_TI, t5621_ITIs, NULL, &EmptyCustomAttributesCache, &t5621_TI, &t5621_0_0_0, &t5621_1_0_0, NULL, &t5621_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4383_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Attribute>
extern MethodInfo m29413_MI;
static PropertyInfo t4383____Current_PropertyInfo = 
{
	&t4383_TI, "Current", &m29413_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4383_PIs[] =
{
	&t4383____Current_PropertyInfo,
	NULL
};
extern Il2CppType t490_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29413_GM;
MethodInfo m29413_MI = 
{
	"get_Current", NULL, &t4383_TI, &t490_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29413_GM};
static MethodInfo* t4383_MIs[] =
{
	&m29413_MI,
	NULL
};
static TypeInfo* t4383_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4383_0_0_0;
extern Il2CppType t4383_1_0_0;
struct t4383;
extern Il2CppGenericClass t4383_GC;
TypeInfo t4383_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4383_MIs, t4383_PIs, NULL, NULL, NULL, NULL, NULL, &t4383_TI, t4383_ITIs, NULL, &EmptyCustomAttributesCache, &t4383_TI, &t4383_0_0_0, &t4383_1_0_0, NULL, &t4383_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2929.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2929_TI;
#include "t2929MD.h"

extern TypeInfo t490_TI;
extern MethodInfo m16055_MI;
extern MethodInfo m22322_MI;
struct t20;
#define m22322(__this, p0, method) (t490 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Attribute>
extern Il2CppType t20_0_0_1;
FieldInfo t2929_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2929_TI, offsetof(t2929, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2929_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2929_TI, offsetof(t2929, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2929_FIs[] =
{
	&t2929_f0_FieldInfo,
	&t2929_f1_FieldInfo,
	NULL
};
extern MethodInfo m16052_MI;
static PropertyInfo t2929____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2929_TI, "System.Collections.IEnumerator.Current", &m16052_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2929____Current_PropertyInfo = 
{
	&t2929_TI, "Current", &m16055_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2929_PIs[] =
{
	&t2929____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2929____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2929_m16051_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16051_GM;
MethodInfo m16051_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2929_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2929_m16051_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16051_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16052_GM;
MethodInfo m16052_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2929_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16052_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16053_GM;
MethodInfo m16053_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2929_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16053_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16054_GM;
MethodInfo m16054_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2929_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16054_GM};
extern Il2CppType t490_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16055_GM;
MethodInfo m16055_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2929_TI, &t490_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16055_GM};
static MethodInfo* t2929_MIs[] =
{
	&m16051_MI,
	&m16052_MI,
	&m16053_MI,
	&m16054_MI,
	&m16055_MI,
	NULL
};
extern MethodInfo m16054_MI;
extern MethodInfo m16053_MI;
static MethodInfo* t2929_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16052_MI,
	&m16054_MI,
	&m16053_MI,
	&m16055_MI,
};
static TypeInfo* t2929_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4383_TI,
};
static Il2CppInterfaceOffsetPair t2929_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4383_TI, 7},
};
extern TypeInfo t490_TI;
static Il2CppRGCTXData t2929_RGCTXData[3] = 
{
	&m16055_MI/* Method Usage */,
	&t490_TI/* Class Usage */,
	&m22322_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2929_0_0_0;
extern Il2CppType t2929_1_0_0;
extern Il2CppGenericClass t2929_GC;
TypeInfo t2929_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2929_MIs, t2929_PIs, t2929_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2929_TI, t2929_ITIs, t2929_VT, &EmptyCustomAttributesCache, &t2929_TI, &t2929_0_0_0, &t2929_1_0_0, t2929_IOs, &t2929_GC, NULL, NULL, NULL, t2929_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2929)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5620_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Attribute>
extern MethodInfo m29414_MI;
extern MethodInfo m29415_MI;
static PropertyInfo t5620____Item_PropertyInfo = 
{
	&t5620_TI, "Item", &m29414_MI, &m29415_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5620_PIs[] =
{
	&t5620____Item_PropertyInfo,
	NULL
};
extern Il2CppType t490_0_0_0;
static ParameterInfo t5620_m29416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t490_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29416_GM;
MethodInfo m29416_MI = 
{
	"IndexOf", NULL, &t5620_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5620_m29416_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29416_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t490_0_0_0;
static ParameterInfo t5620_m29417_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29417_GM;
MethodInfo m29417_MI = 
{
	"Insert", NULL, &t5620_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5620_m29417_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29417_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5620_m29418_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29418_GM;
MethodInfo m29418_MI = 
{
	"RemoveAt", NULL, &t5620_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5620_m29418_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29418_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5620_m29414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t490_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29414_GM;
MethodInfo m29414_MI = 
{
	"get_Item", NULL, &t5620_TI, &t490_0_0_0, RuntimeInvoker_t29_t44, t5620_m29414_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29414_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t490_0_0_0;
static ParameterInfo t5620_m29415_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29415_GM;
MethodInfo m29415_MI = 
{
	"set_Item", NULL, &t5620_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5620_m29415_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29415_GM};
static MethodInfo* t5620_MIs[] =
{
	&m29416_MI,
	&m29417_MI,
	&m29418_MI,
	&m29414_MI,
	&m29415_MI,
	NULL
};
static TypeInfo* t5620_ITIs[] = 
{
	&t603_TI,
	&t5619_TI,
	&t5621_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5620_0_0_0;
extern Il2CppType t5620_1_0_0;
struct t5620;
extern Il2CppGenericClass t5620_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5620_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5620_MIs, t5620_PIs, NULL, NULL, NULL, NULL, NULL, &t5620_TI, t5620_ITIs, NULL, &t1908__CustomAttributeCache, &t5620_TI, &t5620_0_0_0, &t5620_1_0_0, NULL, &t5620_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5622_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Attribute>
extern MethodInfo m29419_MI;
static PropertyInfo t5622____Count_PropertyInfo = 
{
	&t5622_TI, "Count", &m29419_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29420_MI;
static PropertyInfo t5622____IsReadOnly_PropertyInfo = 
{
	&t5622_TI, "IsReadOnly", &m29420_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5622_PIs[] =
{
	&t5622____Count_PropertyInfo,
	&t5622____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29419_GM;
MethodInfo m29419_MI = 
{
	"get_Count", NULL, &t5622_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29419_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29420_GM;
MethodInfo m29420_MI = 
{
	"get_IsReadOnly", NULL, &t5622_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29420_GM};
extern Il2CppType t604_0_0_0;
extern Il2CppType t604_0_0_0;
static ParameterInfo t5622_m29421_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t604_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29421_GM;
MethodInfo m29421_MI = 
{
	"Add", NULL, &t5622_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5622_m29421_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29421_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29422_GM;
MethodInfo m29422_MI = 
{
	"Clear", NULL, &t5622_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29422_GM};
extern Il2CppType t604_0_0_0;
static ParameterInfo t5622_m29423_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t604_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29423_GM;
MethodInfo m29423_MI = 
{
	"Contains", NULL, &t5622_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5622_m29423_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29423_GM};
extern Il2CppType t3543_0_0_0;
extern Il2CppType t3543_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5622_m29424_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3543_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29424_GM;
MethodInfo m29424_MI = 
{
	"CopyTo", NULL, &t5622_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5622_m29424_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29424_GM};
extern Il2CppType t604_0_0_0;
static ParameterInfo t5622_m29425_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t604_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29425_GM;
MethodInfo m29425_MI = 
{
	"Remove", NULL, &t5622_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5622_m29425_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29425_GM};
static MethodInfo* t5622_MIs[] =
{
	&m29419_MI,
	&m29420_MI,
	&m29421_MI,
	&m29422_MI,
	&m29423_MI,
	&m29424_MI,
	&m29425_MI,
	NULL
};
extern TypeInfo t5624_TI;
static TypeInfo* t5622_ITIs[] = 
{
	&t603_TI,
	&t5624_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5622_0_0_0;
extern Il2CppType t5622_1_0_0;
struct t5622;
extern Il2CppGenericClass t5622_GC;
TypeInfo t5622_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5622_MIs, t5622_PIs, NULL, NULL, NULL, NULL, NULL, &t5622_TI, t5622_ITIs, NULL, &EmptyCustomAttributesCache, &t5622_TI, &t5622_0_0_0, &t5622_1_0_0, NULL, &t5622_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Attribute>
extern Il2CppType t4385_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29426_GM;
MethodInfo m29426_MI = 
{
	"GetEnumerator", NULL, &t5624_TI, &t4385_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29426_GM};
static MethodInfo* t5624_MIs[] =
{
	&m29426_MI,
	NULL
};
static TypeInfo* t5624_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5624_0_0_0;
extern Il2CppType t5624_1_0_0;
struct t5624;
extern Il2CppGenericClass t5624_GC;
TypeInfo t5624_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5624_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5624_TI, t5624_ITIs, NULL, &EmptyCustomAttributesCache, &t5624_TI, &t5624_0_0_0, &t5624_1_0_0, NULL, &t5624_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4385_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Attribute>
extern MethodInfo m29427_MI;
static PropertyInfo t4385____Current_PropertyInfo = 
{
	&t4385_TI, "Current", &m29427_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4385_PIs[] =
{
	&t4385____Current_PropertyInfo,
	NULL
};
extern Il2CppType t604_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29427_GM;
MethodInfo m29427_MI = 
{
	"get_Current", NULL, &t4385_TI, &t604_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29427_GM};
static MethodInfo* t4385_MIs[] =
{
	&m29427_MI,
	NULL
};
static TypeInfo* t4385_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4385_0_0_0;
extern Il2CppType t4385_1_0_0;
struct t4385;
extern Il2CppGenericClass t4385_GC;
TypeInfo t4385_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4385_MIs, t4385_PIs, NULL, NULL, NULL, NULL, NULL, &t4385_TI, t4385_ITIs, NULL, &EmptyCustomAttributesCache, &t4385_TI, &t4385_0_0_0, &t4385_1_0_0, NULL, &t4385_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2930.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2930_TI;
#include "t2930MD.h"

extern TypeInfo t604_TI;
extern MethodInfo m16060_MI;
extern MethodInfo m22333_MI;
struct t20;
#define m22333(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Attribute>
extern Il2CppType t20_0_0_1;
FieldInfo t2930_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2930_TI, offsetof(t2930, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2930_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2930_TI, offsetof(t2930, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2930_FIs[] =
{
	&t2930_f0_FieldInfo,
	&t2930_f1_FieldInfo,
	NULL
};
extern MethodInfo m16057_MI;
static PropertyInfo t2930____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2930_TI, "System.Collections.IEnumerator.Current", &m16057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2930____Current_PropertyInfo = 
{
	&t2930_TI, "Current", &m16060_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2930_PIs[] =
{
	&t2930____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2930____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2930_m16056_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16056_GM;
MethodInfo m16056_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2930_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2930_m16056_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16056_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16057_GM;
MethodInfo m16057_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2930_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16057_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16058_GM;
MethodInfo m16058_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2930_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16058_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16059_GM;
MethodInfo m16059_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2930_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16059_GM};
extern Il2CppType t604_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16060_GM;
MethodInfo m16060_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2930_TI, &t604_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16060_GM};
static MethodInfo* t2930_MIs[] =
{
	&m16056_MI,
	&m16057_MI,
	&m16058_MI,
	&m16059_MI,
	&m16060_MI,
	NULL
};
extern MethodInfo m16059_MI;
extern MethodInfo m16058_MI;
static MethodInfo* t2930_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16057_MI,
	&m16059_MI,
	&m16058_MI,
	&m16060_MI,
};
static TypeInfo* t2930_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4385_TI,
};
static Il2CppInterfaceOffsetPair t2930_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4385_TI, 7},
};
extern TypeInfo t604_TI;
static Il2CppRGCTXData t2930_RGCTXData[3] = 
{
	&m16060_MI/* Method Usage */,
	&t604_TI/* Class Usage */,
	&m22333_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2930_0_0_0;
extern Il2CppType t2930_1_0_0;
extern Il2CppGenericClass t2930_GC;
TypeInfo t2930_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2930_MIs, t2930_PIs, t2930_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2930_TI, t2930_ITIs, t2930_VT, &EmptyCustomAttributesCache, &t2930_TI, &t2930_0_0_0, &t2930_1_0_0, t2930_IOs, &t2930_GC, NULL, NULL, NULL, t2930_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2930)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5623_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._Attribute>
extern MethodInfo m29428_MI;
extern MethodInfo m29429_MI;
static PropertyInfo t5623____Item_PropertyInfo = 
{
	&t5623_TI, "Item", &m29428_MI, &m29429_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5623_PIs[] =
{
	&t5623____Item_PropertyInfo,
	NULL
};
extern Il2CppType t604_0_0_0;
static ParameterInfo t5623_m29430_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t604_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29430_GM;
MethodInfo m29430_MI = 
{
	"IndexOf", NULL, &t5623_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5623_m29430_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29430_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t604_0_0_0;
static ParameterInfo t5623_m29431_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t604_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29431_GM;
MethodInfo m29431_MI = 
{
	"Insert", NULL, &t5623_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5623_m29431_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29431_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5623_m29432_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29432_GM;
MethodInfo m29432_MI = 
{
	"RemoveAt", NULL, &t5623_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5623_m29432_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29432_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5623_m29428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t604_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29428_GM;
MethodInfo m29428_MI = 
{
	"get_Item", NULL, &t5623_TI, &t604_0_0_0, RuntimeInvoker_t29_t44, t5623_m29428_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29428_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t604_0_0_0;
static ParameterInfo t5623_m29429_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t604_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29429_GM;
MethodInfo m29429_MI = 
{
	"set_Item", NULL, &t5623_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5623_m29429_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29429_GM};
static MethodInfo* t5623_MIs[] =
{
	&m29430_MI,
	&m29431_MI,
	&m29432_MI,
	&m29428_MI,
	&m29429_MI,
	NULL
};
static TypeInfo* t5623_ITIs[] = 
{
	&t603_TI,
	&t5622_TI,
	&t5624_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5623_0_0_0;
extern Il2CppType t5623_1_0_0;
struct t5623;
extern Il2CppGenericClass t5623_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5623_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5623_MIs, t5623_PIs, NULL, NULL, NULL, NULL, NULL, &t5623_TI, t5623_ITIs, NULL, &t1908__CustomAttributeCache, &t5623_TI, &t5623_0_0_0, &t5623_1_0_0, NULL, &t5623_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4387_TI;

#include "t300.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SerializeField>
extern MethodInfo m29433_MI;
static PropertyInfo t4387____Current_PropertyInfo = 
{
	&t4387_TI, "Current", &m29433_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4387_PIs[] =
{
	&t4387____Current_PropertyInfo,
	NULL
};
extern Il2CppType t300_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29433_GM;
MethodInfo m29433_MI = 
{
	"get_Current", NULL, &t4387_TI, &t300_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29433_GM};
static MethodInfo* t4387_MIs[] =
{
	&m29433_MI,
	NULL
};
static TypeInfo* t4387_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4387_0_0_0;
extern Il2CppType t4387_1_0_0;
struct t4387;
extern Il2CppGenericClass t4387_GC;
TypeInfo t4387_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4387_MIs, t4387_PIs, NULL, NULL, NULL, NULL, NULL, &t4387_TI, t4387_ITIs, NULL, &EmptyCustomAttributesCache, &t4387_TI, &t4387_0_0_0, &t4387_1_0_0, NULL, &t4387_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2931.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2931_TI;
#include "t2931MD.h"

extern TypeInfo t300_TI;
extern MethodInfo m16065_MI;
extern MethodInfo m22344_MI;
struct t20;
#define m22344(__this, p0, method) (t300 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SerializeField>
extern Il2CppType t20_0_0_1;
FieldInfo t2931_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2931_TI, offsetof(t2931, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2931_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2931_TI, offsetof(t2931, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2931_FIs[] =
{
	&t2931_f0_FieldInfo,
	&t2931_f1_FieldInfo,
	NULL
};
extern MethodInfo m16062_MI;
static PropertyInfo t2931____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2931_TI, "System.Collections.IEnumerator.Current", &m16062_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2931____Current_PropertyInfo = 
{
	&t2931_TI, "Current", &m16065_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2931_PIs[] =
{
	&t2931____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2931____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2931_m16061_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16061_GM;
MethodInfo m16061_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2931_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2931_m16061_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16061_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16062_GM;
MethodInfo m16062_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2931_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16062_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16063_GM;
MethodInfo m16063_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2931_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16063_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16064_GM;
MethodInfo m16064_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2931_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16064_GM};
extern Il2CppType t300_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16065_GM;
MethodInfo m16065_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2931_TI, &t300_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16065_GM};
static MethodInfo* t2931_MIs[] =
{
	&m16061_MI,
	&m16062_MI,
	&m16063_MI,
	&m16064_MI,
	&m16065_MI,
	NULL
};
extern MethodInfo m16064_MI;
extern MethodInfo m16063_MI;
static MethodInfo* t2931_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16062_MI,
	&m16064_MI,
	&m16063_MI,
	&m16065_MI,
};
static TypeInfo* t2931_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4387_TI,
};
static Il2CppInterfaceOffsetPair t2931_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4387_TI, 7},
};
extern TypeInfo t300_TI;
static Il2CppRGCTXData t2931_RGCTXData[3] = 
{
	&m16065_MI/* Method Usage */,
	&t300_TI/* Class Usage */,
	&m22344_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2931_0_0_0;
extern Il2CppType t2931_1_0_0;
extern Il2CppGenericClass t2931_GC;
TypeInfo t2931_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2931_MIs, t2931_PIs, t2931_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2931_TI, t2931_ITIs, t2931_VT, &EmptyCustomAttributesCache, &t2931_TI, &t2931_0_0_0, &t2931_1_0_0, t2931_IOs, &t2931_GC, NULL, NULL, NULL, t2931_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2931)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5625_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>
extern MethodInfo m29434_MI;
static PropertyInfo t5625____Count_PropertyInfo = 
{
	&t5625_TI, "Count", &m29434_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29435_MI;
static PropertyInfo t5625____IsReadOnly_PropertyInfo = 
{
	&t5625_TI, "IsReadOnly", &m29435_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5625_PIs[] =
{
	&t5625____Count_PropertyInfo,
	&t5625____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29434_GM;
MethodInfo m29434_MI = 
{
	"get_Count", NULL, &t5625_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29434_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29435_GM;
MethodInfo m29435_MI = 
{
	"get_IsReadOnly", NULL, &t5625_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29435_GM};
extern Il2CppType t300_0_0_0;
extern Il2CppType t300_0_0_0;
static ParameterInfo t5625_m29436_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t300_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29436_GM;
MethodInfo m29436_MI = 
{
	"Add", NULL, &t5625_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5625_m29436_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29436_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29437_GM;
MethodInfo m29437_MI = 
{
	"Clear", NULL, &t5625_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29437_GM};
extern Il2CppType t300_0_0_0;
static ParameterInfo t5625_m29438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t300_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29438_GM;
MethodInfo m29438_MI = 
{
	"Contains", NULL, &t5625_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5625_m29438_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29438_GM};
extern Il2CppType t3755_0_0_0;
extern Il2CppType t3755_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5625_m29439_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3755_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29439_GM;
MethodInfo m29439_MI = 
{
	"CopyTo", NULL, &t5625_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5625_m29439_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29439_GM};
extern Il2CppType t300_0_0_0;
static ParameterInfo t5625_m29440_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t300_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29440_GM;
MethodInfo m29440_MI = 
{
	"Remove", NULL, &t5625_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5625_m29440_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29440_GM};
static MethodInfo* t5625_MIs[] =
{
	&m29434_MI,
	&m29435_MI,
	&m29436_MI,
	&m29437_MI,
	&m29438_MI,
	&m29439_MI,
	&m29440_MI,
	NULL
};
extern TypeInfo t5627_TI;
static TypeInfo* t5625_ITIs[] = 
{
	&t603_TI,
	&t5627_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5625_0_0_0;
extern Il2CppType t5625_1_0_0;
struct t5625;
extern Il2CppGenericClass t5625_GC;
TypeInfo t5625_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5625_MIs, t5625_PIs, NULL, NULL, NULL, NULL, NULL, &t5625_TI, t5625_ITIs, NULL, &EmptyCustomAttributesCache, &t5625_TI, &t5625_0_0_0, &t5625_1_0_0, NULL, &t5625_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SerializeField>
extern Il2CppType t4387_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29441_GM;
MethodInfo m29441_MI = 
{
	"GetEnumerator", NULL, &t5627_TI, &t4387_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29441_GM};
static MethodInfo* t5627_MIs[] =
{
	&m29441_MI,
	NULL
};
static TypeInfo* t5627_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5627_0_0_0;
extern Il2CppType t5627_1_0_0;
struct t5627;
extern Il2CppGenericClass t5627_GC;
TypeInfo t5627_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5627_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5627_TI, t5627_ITIs, NULL, &EmptyCustomAttributesCache, &t5627_TI, &t5627_0_0_0, &t5627_1_0_0, NULL, &t5627_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5626_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SerializeField>
extern MethodInfo m29442_MI;
extern MethodInfo m29443_MI;
static PropertyInfo t5626____Item_PropertyInfo = 
{
	&t5626_TI, "Item", &m29442_MI, &m29443_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5626_PIs[] =
{
	&t5626____Item_PropertyInfo,
	NULL
};
extern Il2CppType t300_0_0_0;
static ParameterInfo t5626_m29444_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t300_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29444_GM;
MethodInfo m29444_MI = 
{
	"IndexOf", NULL, &t5626_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5626_m29444_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29444_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t300_0_0_0;
static ParameterInfo t5626_m29445_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t300_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29445_GM;
MethodInfo m29445_MI = 
{
	"Insert", NULL, &t5626_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5626_m29445_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29445_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5626_m29446_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29446_GM;
MethodInfo m29446_MI = 
{
	"RemoveAt", NULL, &t5626_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5626_m29446_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29446_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5626_m29442_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t300_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29442_GM;
MethodInfo m29442_MI = 
{
	"get_Item", NULL, &t5626_TI, &t300_0_0_0, RuntimeInvoker_t29_t44, t5626_m29442_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29442_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t300_0_0_0;
static ParameterInfo t5626_m29443_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t300_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29443_GM;
MethodInfo m29443_MI = 
{
	"set_Item", NULL, &t5626_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5626_m29443_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29443_GM};
static MethodInfo* t5626_MIs[] =
{
	&m29444_MI,
	&m29445_MI,
	&m29446_MI,
	&m29442_MI,
	&m29443_MI,
	NULL
};
static TypeInfo* t5626_ITIs[] = 
{
	&t603_TI,
	&t5625_TI,
	&t5627_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5626_0_0_0;
extern Il2CppType t5626_1_0_0;
struct t5626;
extern Il2CppGenericClass t5626_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5626_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5626_MIs, t5626_PIs, NULL, NULL, NULL, NULL, NULL, &t5626_TI, t5626_ITIs, NULL, &t1908__CustomAttributeCache, &t5626_TI, &t5626_0_0_0, &t5626_1_0_0, NULL, &t5626_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4389_TI;

#include "t491.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Shader>
extern MethodInfo m29447_MI;
static PropertyInfo t4389____Current_PropertyInfo = 
{
	&t4389_TI, "Current", &m29447_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4389_PIs[] =
{
	&t4389____Current_PropertyInfo,
	NULL
};
extern Il2CppType t491_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29447_GM;
MethodInfo m29447_MI = 
{
	"get_Current", NULL, &t4389_TI, &t491_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29447_GM};
static MethodInfo* t4389_MIs[] =
{
	&m29447_MI,
	NULL
};
static TypeInfo* t4389_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4389_0_0_0;
extern Il2CppType t4389_1_0_0;
struct t4389;
extern Il2CppGenericClass t4389_GC;
TypeInfo t4389_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4389_MIs, t4389_PIs, NULL, NULL, NULL, NULL, NULL, &t4389_TI, t4389_ITIs, NULL, &EmptyCustomAttributesCache, &t4389_TI, &t4389_0_0_0, &t4389_1_0_0, NULL, &t4389_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2932.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2932_TI;
#include "t2932MD.h"

extern TypeInfo t491_TI;
extern MethodInfo m16070_MI;
extern MethodInfo m22355_MI;
struct t20;
#define m22355(__this, p0, method) (t491 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Shader>
extern Il2CppType t20_0_0_1;
FieldInfo t2932_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2932_TI, offsetof(t2932, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2932_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2932_TI, offsetof(t2932, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2932_FIs[] =
{
	&t2932_f0_FieldInfo,
	&t2932_f1_FieldInfo,
	NULL
};
extern MethodInfo m16067_MI;
static PropertyInfo t2932____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2932_TI, "System.Collections.IEnumerator.Current", &m16067_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2932____Current_PropertyInfo = 
{
	&t2932_TI, "Current", &m16070_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2932_PIs[] =
{
	&t2932____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2932____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2932_m16066_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16066_GM;
MethodInfo m16066_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2932_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2932_m16066_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16066_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16067_GM;
MethodInfo m16067_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2932_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16067_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16068_GM;
MethodInfo m16068_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2932_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16068_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16069_GM;
MethodInfo m16069_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2932_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16069_GM};
extern Il2CppType t491_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16070_GM;
MethodInfo m16070_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2932_TI, &t491_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16070_GM};
static MethodInfo* t2932_MIs[] =
{
	&m16066_MI,
	&m16067_MI,
	&m16068_MI,
	&m16069_MI,
	&m16070_MI,
	NULL
};
extern MethodInfo m16069_MI;
extern MethodInfo m16068_MI;
static MethodInfo* t2932_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16067_MI,
	&m16069_MI,
	&m16068_MI,
	&m16070_MI,
};
static TypeInfo* t2932_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4389_TI,
};
static Il2CppInterfaceOffsetPair t2932_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4389_TI, 7},
};
extern TypeInfo t491_TI;
static Il2CppRGCTXData t2932_RGCTXData[3] = 
{
	&m16070_MI/* Method Usage */,
	&t491_TI/* Class Usage */,
	&m22355_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2932_0_0_0;
extern Il2CppType t2932_1_0_0;
extern Il2CppGenericClass t2932_GC;
TypeInfo t2932_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2932_MIs, t2932_PIs, t2932_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2932_TI, t2932_ITIs, t2932_VT, &EmptyCustomAttributesCache, &t2932_TI, &t2932_0_0_0, &t2932_1_0_0, t2932_IOs, &t2932_GC, NULL, NULL, NULL, t2932_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2932)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5628_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Shader>
extern MethodInfo m29448_MI;
static PropertyInfo t5628____Count_PropertyInfo = 
{
	&t5628_TI, "Count", &m29448_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29449_MI;
static PropertyInfo t5628____IsReadOnly_PropertyInfo = 
{
	&t5628_TI, "IsReadOnly", &m29449_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5628_PIs[] =
{
	&t5628____Count_PropertyInfo,
	&t5628____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29448_GM;
MethodInfo m29448_MI = 
{
	"get_Count", NULL, &t5628_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29448_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29449_GM;
MethodInfo m29449_MI = 
{
	"get_IsReadOnly", NULL, &t5628_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29449_GM};
extern Il2CppType t491_0_0_0;
extern Il2CppType t491_0_0_0;
static ParameterInfo t5628_m29450_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29450_GM;
MethodInfo m29450_MI = 
{
	"Add", NULL, &t5628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5628_m29450_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29450_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29451_GM;
MethodInfo m29451_MI = 
{
	"Clear", NULL, &t5628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29451_GM};
extern Il2CppType t491_0_0_0;
static ParameterInfo t5628_m29452_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29452_GM;
MethodInfo m29452_MI = 
{
	"Contains", NULL, &t5628_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5628_m29452_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29452_GM};
extern Il2CppType t3756_0_0_0;
extern Il2CppType t3756_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5628_m29453_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3756_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29453_GM;
MethodInfo m29453_MI = 
{
	"CopyTo", NULL, &t5628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5628_m29453_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29453_GM};
extern Il2CppType t491_0_0_0;
static ParameterInfo t5628_m29454_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29454_GM;
MethodInfo m29454_MI = 
{
	"Remove", NULL, &t5628_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5628_m29454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29454_GM};
static MethodInfo* t5628_MIs[] =
{
	&m29448_MI,
	&m29449_MI,
	&m29450_MI,
	&m29451_MI,
	&m29452_MI,
	&m29453_MI,
	&m29454_MI,
	NULL
};
extern TypeInfo t5630_TI;
static TypeInfo* t5628_ITIs[] = 
{
	&t603_TI,
	&t5630_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5628_0_0_0;
extern Il2CppType t5628_1_0_0;
struct t5628;
extern Il2CppGenericClass t5628_GC;
TypeInfo t5628_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5628_MIs, t5628_PIs, NULL, NULL, NULL, NULL, NULL, &t5628_TI, t5628_ITIs, NULL, &EmptyCustomAttributesCache, &t5628_TI, &t5628_0_0_0, &t5628_1_0_0, NULL, &t5628_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Shader>
extern Il2CppType t4389_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29455_GM;
MethodInfo m29455_MI = 
{
	"GetEnumerator", NULL, &t5630_TI, &t4389_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29455_GM};
static MethodInfo* t5630_MIs[] =
{
	&m29455_MI,
	NULL
};
static TypeInfo* t5630_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5630_0_0_0;
extern Il2CppType t5630_1_0_0;
struct t5630;
extern Il2CppGenericClass t5630_GC;
TypeInfo t5630_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5630_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5630_TI, t5630_ITIs, NULL, &EmptyCustomAttributesCache, &t5630_TI, &t5630_0_0_0, &t5630_1_0_0, NULL, &t5630_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5629_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Shader>
extern MethodInfo m29456_MI;
extern MethodInfo m29457_MI;
static PropertyInfo t5629____Item_PropertyInfo = 
{
	&t5629_TI, "Item", &m29456_MI, &m29457_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5629_PIs[] =
{
	&t5629____Item_PropertyInfo,
	NULL
};
extern Il2CppType t491_0_0_0;
static ParameterInfo t5629_m29458_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29458_GM;
MethodInfo m29458_MI = 
{
	"IndexOf", NULL, &t5629_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5629_m29458_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29458_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t491_0_0_0;
static ParameterInfo t5629_m29459_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29459_GM;
MethodInfo m29459_MI = 
{
	"Insert", NULL, &t5629_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5629_m29459_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29459_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5629_m29460_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29460_GM;
MethodInfo m29460_MI = 
{
	"RemoveAt", NULL, &t5629_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5629_m29460_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29460_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5629_m29456_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t491_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29456_GM;
MethodInfo m29456_MI = 
{
	"get_Item", NULL, &t5629_TI, &t491_0_0_0, RuntimeInvoker_t29_t44, t5629_m29456_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29456_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t491_0_0_0;
static ParameterInfo t5629_m29457_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29457_GM;
MethodInfo m29457_MI = 
{
	"set_Item", NULL, &t5629_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5629_m29457_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29457_GM};
static MethodInfo* t5629_MIs[] =
{
	&m29458_MI,
	&m29459_MI,
	&m29460_MI,
	&m29456_MI,
	&m29457_MI,
	NULL
};
static TypeInfo* t5629_ITIs[] = 
{
	&t603_TI,
	&t5628_TI,
	&t5630_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5629_0_0_0;
extern Il2CppType t5629_1_0_0;
struct t5629;
extern Il2CppGenericClass t5629_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5629_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5629_MIs, t5629_PIs, NULL, NULL, NULL, NULL, NULL, &t5629_TI, t5629_ITIs, NULL, &t1908__CustomAttributeCache, &t5629_TI, &t5629_0_0_0, &t5629_1_0_0, NULL, &t5629_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2933.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2933_TI;
#include "t2933MD.h"

#include "t2934.h"
extern TypeInfo t2934_TI;
#include "t2934MD.h"
extern MethodInfo m16073_MI;
extern MethodInfo m16075_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>
extern Il2CppType t316_0_0_33;
FieldInfo t2933_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2933_TI, offsetof(t2933, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2933_FIs[] =
{
	&t2933_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t491_0_0_0;
static ParameterInfo t2933_m16071_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16071_GM;
MethodInfo m16071_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2933_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2933_m16071_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16071_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2933_m16072_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16072_GM;
MethodInfo m16072_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2933_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2933_m16072_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16072_GM};
static MethodInfo* t2933_MIs[] =
{
	&m16071_MI,
	&m16072_MI,
	NULL
};
extern MethodInfo m16072_MI;
extern MethodInfo m16076_MI;
static MethodInfo* t2933_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16072_MI,
	&m16076_MI,
};
extern Il2CppType t2935_0_0_0;
extern TypeInfo t2935_TI;
extern MethodInfo m22365_MI;
extern TypeInfo t491_TI;
extern MethodInfo m16078_MI;
extern TypeInfo t491_TI;
static Il2CppRGCTXData t2933_RGCTXData[8] = 
{
	&t2935_0_0_0/* Type Usage */,
	&t2935_TI/* Class Usage */,
	&m22365_MI/* Method Usage */,
	&t491_TI/* Class Usage */,
	&m16078_MI/* Method Usage */,
	&m16073_MI/* Method Usage */,
	&t491_TI/* Class Usage */,
	&m16075_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2933_0_0_0;
extern Il2CppType t2933_1_0_0;
struct t2933;
extern Il2CppGenericClass t2933_GC;
TypeInfo t2933_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2933_MIs, NULL, t2933_FIs, NULL, &t2934_TI, NULL, NULL, &t2933_TI, NULL, t2933_VT, &EmptyCustomAttributesCache, &t2933_TI, &t2933_0_0_0, &t2933_1_0_0, NULL, &t2933_GC, NULL, NULL, NULL, t2933_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2933), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2935.h"
extern TypeInfo t2935_TI;
#include "t2935MD.h"
struct t556;
#define m22365(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>
extern Il2CppType t2935_0_0_1;
FieldInfo t2934_f0_FieldInfo = 
{
	"Delegate", &t2935_0_0_1, &t2934_TI, offsetof(t2934, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2934_FIs[] =
{
	&t2934_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2934_m16073_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16073_GM;
MethodInfo m16073_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2934_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2934_m16073_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16073_GM};
extern Il2CppType t2935_0_0_0;
static ParameterInfo t2934_m16074_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2935_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16074_GM;
MethodInfo m16074_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2934_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2934_m16074_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16074_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2934_m16075_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16075_GM;
MethodInfo m16075_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2934_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2934_m16075_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16075_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2934_m16076_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16076_GM;
MethodInfo m16076_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2934_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2934_m16076_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16076_GM};
static MethodInfo* t2934_MIs[] =
{
	&m16073_MI,
	&m16074_MI,
	&m16075_MI,
	&m16076_MI,
	NULL
};
static MethodInfo* t2934_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16075_MI,
	&m16076_MI,
};
extern TypeInfo t2935_TI;
extern TypeInfo t491_TI;
static Il2CppRGCTXData t2934_RGCTXData[5] = 
{
	&t2935_0_0_0/* Type Usage */,
	&t2935_TI/* Class Usage */,
	&m22365_MI/* Method Usage */,
	&t491_TI/* Class Usage */,
	&m16078_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2934_0_0_0;
extern Il2CppType t2934_1_0_0;
struct t2934;
extern Il2CppGenericClass t2934_GC;
TypeInfo t2934_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2934_MIs, NULL, t2934_FIs, NULL, &t556_TI, NULL, NULL, &t2934_TI, NULL, t2934_VT, &EmptyCustomAttributesCache, &t2934_TI, &t2934_0_0_0, &t2934_1_0_0, NULL, &t2934_GC, NULL, NULL, NULL, t2934_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2934), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Shader>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2935_m16077_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16077_GM;
MethodInfo m16077_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2935_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2935_m16077_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16077_GM};
extern Il2CppType t491_0_0_0;
static ParameterInfo t2935_m16078_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16078_GM;
MethodInfo m16078_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2935_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2935_m16078_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16078_GM};
extern Il2CppType t491_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2935_m16079_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t491_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16079_GM;
MethodInfo m16079_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2935_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2935_m16079_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16079_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2935_m16080_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16080_GM;
MethodInfo m16080_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2935_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2935_m16080_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16080_GM};
static MethodInfo* t2935_MIs[] =
{
	&m16077_MI,
	&m16078_MI,
	&m16079_MI,
	&m16080_MI,
	NULL
};
extern MethodInfo m16079_MI;
extern MethodInfo m16080_MI;
static MethodInfo* t2935_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16078_MI,
	&m16079_MI,
	&m16080_MI,
};
static Il2CppInterfaceOffsetPair t2935_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2935_1_0_0;
struct t2935;
extern Il2CppGenericClass t2935_GC;
TypeInfo t2935_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2935_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2935_TI, NULL, t2935_VT, &EmptyCustomAttributesCache, &t2935_TI, &t2935_0_0_0, &t2935_1_0_0, t2935_IOs, &t2935_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2935), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4391_TI;

#include "t156.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Material>
extern MethodInfo m29461_MI;
static PropertyInfo t4391____Current_PropertyInfo = 
{
	&t4391_TI, "Current", &m29461_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4391_PIs[] =
{
	&t4391____Current_PropertyInfo,
	NULL
};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29461_GM;
MethodInfo m29461_MI = 
{
	"get_Current", NULL, &t4391_TI, &t156_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29461_GM};
static MethodInfo* t4391_MIs[] =
{
	&m29461_MI,
	NULL
};
static TypeInfo* t4391_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4391_0_0_0;
extern Il2CppType t4391_1_0_0;
struct t4391;
extern Il2CppGenericClass t4391_GC;
TypeInfo t4391_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4391_MIs, t4391_PIs, NULL, NULL, NULL, NULL, NULL, &t4391_TI, t4391_ITIs, NULL, &EmptyCustomAttributesCache, &t4391_TI, &t4391_0_0_0, &t4391_1_0_0, NULL, &t4391_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2936.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2936_TI;
#include "t2936MD.h"

extern TypeInfo t156_TI;
extern MethodInfo m16085_MI;
extern MethodInfo m22367_MI;
struct t20;
#define m22367(__this, p0, method) (t156 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Material>
extern Il2CppType t20_0_0_1;
FieldInfo t2936_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2936_TI, offsetof(t2936, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2936_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2936_TI, offsetof(t2936, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2936_FIs[] =
{
	&t2936_f0_FieldInfo,
	&t2936_f1_FieldInfo,
	NULL
};
extern MethodInfo m16082_MI;
static PropertyInfo t2936____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2936_TI, "System.Collections.IEnumerator.Current", &m16082_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2936____Current_PropertyInfo = 
{
	&t2936_TI, "Current", &m16085_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2936_PIs[] =
{
	&t2936____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2936____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2936_m16081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16081_GM;
MethodInfo m16081_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2936_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2936_m16081_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16081_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16082_GM;
MethodInfo m16082_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2936_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16082_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16083_GM;
MethodInfo m16083_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2936_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16083_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16084_GM;
MethodInfo m16084_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2936_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16084_GM};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16085_GM;
MethodInfo m16085_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2936_TI, &t156_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16085_GM};
static MethodInfo* t2936_MIs[] =
{
	&m16081_MI,
	&m16082_MI,
	&m16083_MI,
	&m16084_MI,
	&m16085_MI,
	NULL
};
extern MethodInfo m16084_MI;
extern MethodInfo m16083_MI;
static MethodInfo* t2936_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16082_MI,
	&m16084_MI,
	&m16083_MI,
	&m16085_MI,
};
static TypeInfo* t2936_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4391_TI,
};
static Il2CppInterfaceOffsetPair t2936_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4391_TI, 7},
};
extern TypeInfo t156_TI;
static Il2CppRGCTXData t2936_RGCTXData[3] = 
{
	&m16085_MI/* Method Usage */,
	&t156_TI/* Class Usage */,
	&m22367_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2936_0_0_0;
extern Il2CppType t2936_1_0_0;
extern Il2CppGenericClass t2936_GC;
TypeInfo t2936_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2936_MIs, t2936_PIs, t2936_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2936_TI, t2936_ITIs, t2936_VT, &EmptyCustomAttributesCache, &t2936_TI, &t2936_0_0_0, &t2936_1_0_0, t2936_IOs, &t2936_GC, NULL, NULL, NULL, t2936_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2936)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5631_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Material>
extern MethodInfo m29462_MI;
static PropertyInfo t5631____Count_PropertyInfo = 
{
	&t5631_TI, "Count", &m29462_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29463_MI;
static PropertyInfo t5631____IsReadOnly_PropertyInfo = 
{
	&t5631_TI, "IsReadOnly", &m29463_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5631_PIs[] =
{
	&t5631____Count_PropertyInfo,
	&t5631____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29462_GM;
MethodInfo m29462_MI = 
{
	"get_Count", NULL, &t5631_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29462_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29463_GM;
MethodInfo m29463_MI = 
{
	"get_IsReadOnly", NULL, &t5631_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29463_GM};
extern Il2CppType t156_0_0_0;
extern Il2CppType t156_0_0_0;
static ParameterInfo t5631_m29464_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29464_GM;
MethodInfo m29464_MI = 
{
	"Add", NULL, &t5631_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5631_m29464_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29464_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29465_GM;
MethodInfo m29465_MI = 
{
	"Clear", NULL, &t5631_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29465_GM};
extern Il2CppType t156_0_0_0;
static ParameterInfo t5631_m29466_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29466_GM;
MethodInfo m29466_MI = 
{
	"Contains", NULL, &t5631_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5631_m29466_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29466_GM};
extern Il2CppType t3757_0_0_0;
extern Il2CppType t3757_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5631_m29467_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3757_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29467_GM;
MethodInfo m29467_MI = 
{
	"CopyTo", NULL, &t5631_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5631_m29467_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29467_GM};
extern Il2CppType t156_0_0_0;
static ParameterInfo t5631_m29468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29468_GM;
MethodInfo m29468_MI = 
{
	"Remove", NULL, &t5631_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5631_m29468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29468_GM};
static MethodInfo* t5631_MIs[] =
{
	&m29462_MI,
	&m29463_MI,
	&m29464_MI,
	&m29465_MI,
	&m29466_MI,
	&m29467_MI,
	&m29468_MI,
	NULL
};
extern TypeInfo t5633_TI;
static TypeInfo* t5631_ITIs[] = 
{
	&t603_TI,
	&t5633_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5631_0_0_0;
extern Il2CppType t5631_1_0_0;
struct t5631;
extern Il2CppGenericClass t5631_GC;
TypeInfo t5631_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5631_MIs, t5631_PIs, NULL, NULL, NULL, NULL, NULL, &t5631_TI, t5631_ITIs, NULL, &EmptyCustomAttributesCache, &t5631_TI, &t5631_0_0_0, &t5631_1_0_0, NULL, &t5631_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Material>
extern Il2CppType t4391_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29469_GM;
MethodInfo m29469_MI = 
{
	"GetEnumerator", NULL, &t5633_TI, &t4391_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29469_GM};
static MethodInfo* t5633_MIs[] =
{
	&m29469_MI,
	NULL
};
static TypeInfo* t5633_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5633_0_0_0;
extern Il2CppType t5633_1_0_0;
struct t5633;
extern Il2CppGenericClass t5633_GC;
TypeInfo t5633_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5633_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5633_TI, t5633_ITIs, NULL, &EmptyCustomAttributesCache, &t5633_TI, &t5633_0_0_0, &t5633_1_0_0, NULL, &t5633_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5632_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Material>
extern MethodInfo m29470_MI;
extern MethodInfo m29471_MI;
static PropertyInfo t5632____Item_PropertyInfo = 
{
	&t5632_TI, "Item", &m29470_MI, &m29471_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5632_PIs[] =
{
	&t5632____Item_PropertyInfo,
	NULL
};
extern Il2CppType t156_0_0_0;
static ParameterInfo t5632_m29472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29472_GM;
MethodInfo m29472_MI = 
{
	"IndexOf", NULL, &t5632_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5632_m29472_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29472_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t156_0_0_0;
static ParameterInfo t5632_m29473_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29473_GM;
MethodInfo m29473_MI = 
{
	"Insert", NULL, &t5632_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5632_m29473_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29473_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5632_m29474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29474_GM;
MethodInfo m29474_MI = 
{
	"RemoveAt", NULL, &t5632_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5632_m29474_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29474_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5632_m29470_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29470_GM;
MethodInfo m29470_MI = 
{
	"get_Item", NULL, &t5632_TI, &t156_0_0_0, RuntimeInvoker_t29_t44, t5632_m29470_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29470_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t156_0_0_0;
static ParameterInfo t5632_m29471_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29471_GM;
MethodInfo m29471_MI = 
{
	"set_Item", NULL, &t5632_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5632_m29471_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29471_GM};
static MethodInfo* t5632_MIs[] =
{
	&m29472_MI,
	&m29473_MI,
	&m29474_MI,
	&m29470_MI,
	&m29471_MI,
	NULL
};
static TypeInfo* t5632_ITIs[] = 
{
	&t603_TI,
	&t5631_TI,
	&t5633_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5632_0_0_0;
extern Il2CppType t5632_1_0_0;
struct t5632;
extern Il2CppGenericClass t5632_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5632_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5632_MIs, t5632_PIs, NULL, NULL, NULL, NULL, NULL, &t5632_TI, t5632_ITIs, NULL, &t1908__CustomAttributeCache, &t5632_TI, &t5632_0_0_0, &t5632_1_0_0, NULL, &t5632_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2937.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2937_TI;
#include "t2937MD.h"

#include "t2938.h"
extern TypeInfo t2938_TI;
#include "t2938MD.h"
extern MethodInfo m16088_MI;
extern MethodInfo m16090_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>
extern Il2CppType t316_0_0_33;
FieldInfo t2937_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2937_TI, offsetof(t2937, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2937_FIs[] =
{
	&t2937_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t156_0_0_0;
static ParameterInfo t2937_m16086_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16086_GM;
MethodInfo m16086_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2937_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2937_m16086_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16086_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2937_m16087_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16087_GM;
MethodInfo m16087_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2937_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2937_m16087_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16087_GM};
static MethodInfo* t2937_MIs[] =
{
	&m16086_MI,
	&m16087_MI,
	NULL
};
extern MethodInfo m16087_MI;
extern MethodInfo m16091_MI;
static MethodInfo* t2937_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16087_MI,
	&m16091_MI,
};
extern Il2CppType t2939_0_0_0;
extern TypeInfo t2939_TI;
extern MethodInfo m22377_MI;
extern TypeInfo t156_TI;
extern MethodInfo m16093_MI;
extern TypeInfo t156_TI;
static Il2CppRGCTXData t2937_RGCTXData[8] = 
{
	&t2939_0_0_0/* Type Usage */,
	&t2939_TI/* Class Usage */,
	&m22377_MI/* Method Usage */,
	&t156_TI/* Class Usage */,
	&m16093_MI/* Method Usage */,
	&m16088_MI/* Method Usage */,
	&t156_TI/* Class Usage */,
	&m16090_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2937_0_0_0;
extern Il2CppType t2937_1_0_0;
struct t2937;
extern Il2CppGenericClass t2937_GC;
TypeInfo t2937_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2937_MIs, NULL, t2937_FIs, NULL, &t2938_TI, NULL, NULL, &t2937_TI, NULL, t2937_VT, &EmptyCustomAttributesCache, &t2937_TI, &t2937_0_0_0, &t2937_1_0_0, NULL, &t2937_GC, NULL, NULL, NULL, t2937_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2937), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2939.h"
extern TypeInfo t2939_TI;
#include "t2939MD.h"
struct t556;
#define m22377(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Material>
extern Il2CppType t2939_0_0_1;
FieldInfo t2938_f0_FieldInfo = 
{
	"Delegate", &t2939_0_0_1, &t2938_TI, offsetof(t2938, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2938_FIs[] =
{
	&t2938_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2938_m16088_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16088_GM;
MethodInfo m16088_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2938_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2938_m16088_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16088_GM};
extern Il2CppType t2939_0_0_0;
static ParameterInfo t2938_m16089_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16089_GM;
MethodInfo m16089_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2938_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2938_m16089_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16089_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2938_m16090_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16090_GM;
MethodInfo m16090_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2938_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2938_m16090_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16090_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2938_m16091_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16091_GM;
MethodInfo m16091_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2938_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2938_m16091_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16091_GM};
static MethodInfo* t2938_MIs[] =
{
	&m16088_MI,
	&m16089_MI,
	&m16090_MI,
	&m16091_MI,
	NULL
};
static MethodInfo* t2938_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16090_MI,
	&m16091_MI,
};
extern TypeInfo t2939_TI;
extern TypeInfo t156_TI;
static Il2CppRGCTXData t2938_RGCTXData[5] = 
{
	&t2939_0_0_0/* Type Usage */,
	&t2939_TI/* Class Usage */,
	&m22377_MI/* Method Usage */,
	&t156_TI/* Class Usage */,
	&m16093_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2938_0_0_0;
extern Il2CppType t2938_1_0_0;
struct t2938;
extern Il2CppGenericClass t2938_GC;
TypeInfo t2938_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2938_MIs, NULL, t2938_FIs, NULL, &t556_TI, NULL, NULL, &t2938_TI, NULL, t2938_VT, &EmptyCustomAttributesCache, &t2938_TI, &t2938_0_0_0, &t2938_1_0_0, NULL, &t2938_GC, NULL, NULL, NULL, t2938_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2938), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Material>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2939_m16092_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16092_GM;
MethodInfo m16092_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2939_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2939_m16092_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16092_GM};
extern Il2CppType t156_0_0_0;
static ParameterInfo t2939_m16093_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16093_GM;
MethodInfo m16093_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2939_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2939_m16093_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16093_GM};
extern Il2CppType t156_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2939_m16094_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t156_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16094_GM;
MethodInfo m16094_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2939_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2939_m16094_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16094_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2939_m16095_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16095_GM;
MethodInfo m16095_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2939_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2939_m16095_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16095_GM};
static MethodInfo* t2939_MIs[] =
{
	&m16092_MI,
	&m16093_MI,
	&m16094_MI,
	&m16095_MI,
	NULL
};
extern MethodInfo m16094_MI;
extern MethodInfo m16095_MI;
static MethodInfo* t2939_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16093_MI,
	&m16094_MI,
	&m16095_MI,
};
static Il2CppInterfaceOffsetPair t2939_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2939_1_0_0;
struct t2939;
extern Il2CppGenericClass t2939_GC;
TypeInfo t2939_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2939_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2939_TI, NULL, t2939_VT, &EmptyCustomAttributesCache, &t2939_TI, &t2939_0_0_0, &t2939_1_0_0, t2939_IOs, &t2939_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2939), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4392_TI;

#include "t180.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>
extern MethodInfo m29475_MI;
static PropertyInfo t4392____Current_PropertyInfo = 
{
	&t4392_TI, "Current", &m29475_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4392_PIs[] =
{
	&t4392____Current_PropertyInfo,
	NULL
};
extern Il2CppType t180_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29475_GM;
MethodInfo m29475_MI = 
{
	"get_Current", NULL, &t4392_TI, &t180_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29475_GM};
static MethodInfo* t4392_MIs[] =
{
	&m29475_MI,
	NULL
};
static TypeInfo* t4392_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4392_0_0_0;
extern Il2CppType t4392_1_0_0;
struct t4392;
extern Il2CppGenericClass t4392_GC;
TypeInfo t4392_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4392_MIs, t4392_PIs, NULL, NULL, NULL, NULL, NULL, &t4392_TI, t4392_ITIs, NULL, &EmptyCustomAttributesCache, &t4392_TI, &t4392_0_0_0, &t4392_1_0_0, NULL, &t4392_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2940.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2940_TI;
#include "t2940MD.h"

extern TypeInfo t180_TI;
extern MethodInfo m16100_MI;
extern MethodInfo m22379_MI;
struct t20;
#define m22379(__this, p0, method) (t180 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Sprite>
extern Il2CppType t20_0_0_1;
FieldInfo t2940_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2940_TI, offsetof(t2940, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2940_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2940_TI, offsetof(t2940, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2940_FIs[] =
{
	&t2940_f0_FieldInfo,
	&t2940_f1_FieldInfo,
	NULL
};
extern MethodInfo m16097_MI;
static PropertyInfo t2940____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2940_TI, "System.Collections.IEnumerator.Current", &m16097_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2940____Current_PropertyInfo = 
{
	&t2940_TI, "Current", &m16100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2940_PIs[] =
{
	&t2940____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2940____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2940_m16096_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16096_GM;
MethodInfo m16096_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2940_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2940_m16096_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16096_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16097_GM;
MethodInfo m16097_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2940_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16097_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16098_GM;
MethodInfo m16098_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2940_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16098_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16099_GM;
MethodInfo m16099_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2940_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16099_GM};
extern Il2CppType t180_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16100_GM;
MethodInfo m16100_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2940_TI, &t180_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16100_GM};
static MethodInfo* t2940_MIs[] =
{
	&m16096_MI,
	&m16097_MI,
	&m16098_MI,
	&m16099_MI,
	&m16100_MI,
	NULL
};
extern MethodInfo m16099_MI;
extern MethodInfo m16098_MI;
static MethodInfo* t2940_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16097_MI,
	&m16099_MI,
	&m16098_MI,
	&m16100_MI,
};
static TypeInfo* t2940_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4392_TI,
};
static Il2CppInterfaceOffsetPair t2940_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4392_TI, 7},
};
extern TypeInfo t180_TI;
static Il2CppRGCTXData t2940_RGCTXData[3] = 
{
	&m16100_MI/* Method Usage */,
	&t180_TI/* Class Usage */,
	&m22379_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2940_0_0_0;
extern Il2CppType t2940_1_0_0;
extern Il2CppGenericClass t2940_GC;
TypeInfo t2940_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2940_MIs, t2940_PIs, t2940_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2940_TI, t2940_ITIs, t2940_VT, &EmptyCustomAttributesCache, &t2940_TI, &t2940_0_0_0, &t2940_1_0_0, t2940_IOs, &t2940_GC, NULL, NULL, NULL, t2940_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2940)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5634_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Sprite>
extern MethodInfo m29476_MI;
static PropertyInfo t5634____Count_PropertyInfo = 
{
	&t5634_TI, "Count", &m29476_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29477_MI;
static PropertyInfo t5634____IsReadOnly_PropertyInfo = 
{
	&t5634_TI, "IsReadOnly", &m29477_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5634_PIs[] =
{
	&t5634____Count_PropertyInfo,
	&t5634____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29476_GM;
MethodInfo m29476_MI = 
{
	"get_Count", NULL, &t5634_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29476_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29477_GM;
MethodInfo m29477_MI = 
{
	"get_IsReadOnly", NULL, &t5634_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29477_GM};
extern Il2CppType t180_0_0_0;
extern Il2CppType t180_0_0_0;
static ParameterInfo t5634_m29478_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29478_GM;
MethodInfo m29478_MI = 
{
	"Add", NULL, &t5634_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5634_m29478_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29478_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29479_GM;
MethodInfo m29479_MI = 
{
	"Clear", NULL, &t5634_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29479_GM};
extern Il2CppType t180_0_0_0;
static ParameterInfo t5634_m29480_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29480_GM;
MethodInfo m29480_MI = 
{
	"Contains", NULL, &t5634_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5634_m29480_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29480_GM};
extern Il2CppType t3758_0_0_0;
extern Il2CppType t3758_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5634_m29481_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3758_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29481_GM;
MethodInfo m29481_MI = 
{
	"CopyTo", NULL, &t5634_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5634_m29481_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29481_GM};
extern Il2CppType t180_0_0_0;
static ParameterInfo t5634_m29482_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29482_GM;
MethodInfo m29482_MI = 
{
	"Remove", NULL, &t5634_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5634_m29482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29482_GM};
static MethodInfo* t5634_MIs[] =
{
	&m29476_MI,
	&m29477_MI,
	&m29478_MI,
	&m29479_MI,
	&m29480_MI,
	&m29481_MI,
	&m29482_MI,
	NULL
};
extern TypeInfo t5636_TI;
static TypeInfo* t5634_ITIs[] = 
{
	&t603_TI,
	&t5636_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5634_0_0_0;
extern Il2CppType t5634_1_0_0;
struct t5634;
extern Il2CppGenericClass t5634_GC;
TypeInfo t5634_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5634_MIs, t5634_PIs, NULL, NULL, NULL, NULL, NULL, &t5634_TI, t5634_ITIs, NULL, &EmptyCustomAttributesCache, &t5634_TI, &t5634_0_0_0, &t5634_1_0_0, NULL, &t5634_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>
extern Il2CppType t4392_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29483_GM;
MethodInfo m29483_MI = 
{
	"GetEnumerator", NULL, &t5636_TI, &t4392_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29483_GM};
static MethodInfo* t5636_MIs[] =
{
	&m29483_MI,
	NULL
};
static TypeInfo* t5636_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5636_0_0_0;
extern Il2CppType t5636_1_0_0;
struct t5636;
extern Il2CppGenericClass t5636_GC;
TypeInfo t5636_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5636_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5636_TI, t5636_ITIs, NULL, &EmptyCustomAttributesCache, &t5636_TI, &t5636_0_0_0, &t5636_1_0_0, NULL, &t5636_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5635_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Sprite>
extern MethodInfo m29484_MI;
extern MethodInfo m29485_MI;
static PropertyInfo t5635____Item_PropertyInfo = 
{
	&t5635_TI, "Item", &m29484_MI, &m29485_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5635_PIs[] =
{
	&t5635____Item_PropertyInfo,
	NULL
};
extern Il2CppType t180_0_0_0;
static ParameterInfo t5635_m29486_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29486_GM;
MethodInfo m29486_MI = 
{
	"IndexOf", NULL, &t5635_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5635_m29486_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29486_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t180_0_0_0;
static ParameterInfo t5635_m29487_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29487_GM;
MethodInfo m29487_MI = 
{
	"Insert", NULL, &t5635_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5635_m29487_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29487_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5635_m29488_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29488_GM;
MethodInfo m29488_MI = 
{
	"RemoveAt", NULL, &t5635_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5635_m29488_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29488_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5635_m29484_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t180_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29484_GM;
MethodInfo m29484_MI = 
{
	"get_Item", NULL, &t5635_TI, &t180_0_0_0, RuntimeInvoker_t29_t44, t5635_m29484_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29484_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t180_0_0_0;
static ParameterInfo t5635_m29485_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29485_GM;
MethodInfo m29485_MI = 
{
	"set_Item", NULL, &t5635_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5635_m29485_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29485_GM};
static MethodInfo* t5635_MIs[] =
{
	&m29486_MI,
	&m29487_MI,
	&m29488_MI,
	&m29484_MI,
	&m29485_MI,
	NULL
};
static TypeInfo* t5635_ITIs[] = 
{
	&t603_TI,
	&t5634_TI,
	&t5636_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5635_0_0_0;
extern Il2CppType t5635_1_0_0;
struct t5635;
extern Il2CppGenericClass t5635_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5635_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5635_MIs, t5635_PIs, NULL, NULL, NULL, NULL, NULL, &t5635_TI, t5635_ITIs, NULL, &t1908__CustomAttributeCache, &t5635_TI, &t5635_0_0_0, &t5635_1_0_0, NULL, &t5635_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2941.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2941_TI;
#include "t2941MD.h"

#include "t2942.h"
extern TypeInfo t2942_TI;
#include "t2942MD.h"
extern MethodInfo m16103_MI;
extern MethodInfo m16105_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
extern Il2CppType t316_0_0_33;
FieldInfo t2941_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2941_TI, offsetof(t2941, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2941_FIs[] =
{
	&t2941_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t180_0_0_0;
static ParameterInfo t2941_m16101_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16101_GM;
MethodInfo m16101_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2941_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2941_m16101_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16101_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2941_m16102_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16102_GM;
MethodInfo m16102_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2941_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2941_m16102_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16102_GM};
static MethodInfo* t2941_MIs[] =
{
	&m16101_MI,
	&m16102_MI,
	NULL
};
extern MethodInfo m16102_MI;
extern MethodInfo m16106_MI;
static MethodInfo* t2941_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16102_MI,
	&m16106_MI,
};
extern Il2CppType t2943_0_0_0;
extern TypeInfo t2943_TI;
extern MethodInfo m22389_MI;
extern TypeInfo t180_TI;
extern MethodInfo m16108_MI;
extern TypeInfo t180_TI;
static Il2CppRGCTXData t2941_RGCTXData[8] = 
{
	&t2943_0_0_0/* Type Usage */,
	&t2943_TI/* Class Usage */,
	&m22389_MI/* Method Usage */,
	&t180_TI/* Class Usage */,
	&m16108_MI/* Method Usage */,
	&m16103_MI/* Method Usage */,
	&t180_TI/* Class Usage */,
	&m16105_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2941_0_0_0;
extern Il2CppType t2941_1_0_0;
struct t2941;
extern Il2CppGenericClass t2941_GC;
TypeInfo t2941_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2941_MIs, NULL, t2941_FIs, NULL, &t2942_TI, NULL, NULL, &t2941_TI, NULL, t2941_VT, &EmptyCustomAttributesCache, &t2941_TI, &t2941_0_0_0, &t2941_1_0_0, NULL, &t2941_GC, NULL, NULL, NULL, t2941_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2941), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2943.h"
extern TypeInfo t2943_TI;
#include "t2943MD.h"
struct t556;
#define m22389(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
extern Il2CppType t2943_0_0_1;
FieldInfo t2942_f0_FieldInfo = 
{
	"Delegate", &t2943_0_0_1, &t2942_TI, offsetof(t2942, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2942_FIs[] =
{
	&t2942_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2942_m16103_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16103_GM;
MethodInfo m16103_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2942_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2942_m16103_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16103_GM};
extern Il2CppType t2943_0_0_0;
static ParameterInfo t2942_m16104_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2943_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16104_GM;
MethodInfo m16104_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2942_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2942_m16104_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16104_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2942_m16105_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16105_GM;
MethodInfo m16105_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2942_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2942_m16105_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16105_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2942_m16106_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16106_GM;
MethodInfo m16106_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2942_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2942_m16106_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16106_GM};
static MethodInfo* t2942_MIs[] =
{
	&m16103_MI,
	&m16104_MI,
	&m16105_MI,
	&m16106_MI,
	NULL
};
static MethodInfo* t2942_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16105_MI,
	&m16106_MI,
};
extern TypeInfo t2943_TI;
extern TypeInfo t180_TI;
static Il2CppRGCTXData t2942_RGCTXData[5] = 
{
	&t2943_0_0_0/* Type Usage */,
	&t2943_TI/* Class Usage */,
	&m22389_MI/* Method Usage */,
	&t180_TI/* Class Usage */,
	&m16108_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2942_0_0_0;
extern Il2CppType t2942_1_0_0;
struct t2942;
extern Il2CppGenericClass t2942_GC;
TypeInfo t2942_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2942_MIs, NULL, t2942_FIs, NULL, &t556_TI, NULL, NULL, &t2942_TI, NULL, t2942_VT, &EmptyCustomAttributesCache, &t2942_TI, &t2942_0_0_0, &t2942_1_0_0, NULL, &t2942_GC, NULL, NULL, NULL, t2942_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2942), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2943_m16107_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16107_GM;
MethodInfo m16107_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2943_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2943_m16107_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16107_GM};
extern Il2CppType t180_0_0_0;
static ParameterInfo t2943_m16108_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16108_GM;
MethodInfo m16108_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2943_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2943_m16108_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16108_GM};
extern Il2CppType t180_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2943_m16109_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t180_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16109_GM;
MethodInfo m16109_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2943_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2943_m16109_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16109_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2943_m16110_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16110_GM;
MethodInfo m16110_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2943_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2943_m16110_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16110_GM};
static MethodInfo* t2943_MIs[] =
{
	&m16107_MI,
	&m16108_MI,
	&m16109_MI,
	&m16110_MI,
	NULL
};
extern MethodInfo m16109_MI;
extern MethodInfo m16110_MI;
static MethodInfo* t2943_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16108_MI,
	&m16109_MI,
	&m16110_MI,
};
static Il2CppInterfaceOffsetPair t2943_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2943_1_0_0;
struct t2943;
extern Il2CppGenericClass t2943_GC;
TypeInfo t2943_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2943_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2943_TI, NULL, t2943_VT, &EmptyCustomAttributesCache, &t2943_TI, &t2943_0_0_0, &t2943_1_0_0, t2943_IOs, &t2943_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2943), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4394_TI;

#include "t331.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>
extern MethodInfo m29489_MI;
static PropertyInfo t4394____Current_PropertyInfo = 
{
	&t4394_TI, "Current", &m29489_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4394_PIs[] =
{
	&t4394____Current_PropertyInfo,
	NULL
};
extern Il2CppType t331_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29489_GM;
MethodInfo m29489_MI = 
{
	"get_Current", NULL, &t4394_TI, &t331_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29489_GM};
static MethodInfo* t4394_MIs[] =
{
	&m29489_MI,
	NULL
};
static TypeInfo* t4394_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4394_0_0_0;
extern Il2CppType t4394_1_0_0;
struct t4394;
extern Il2CppGenericClass t4394_GC;
TypeInfo t4394_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4394_MIs, t4394_PIs, NULL, NULL, NULL, NULL, NULL, &t4394_TI, t4394_ITIs, NULL, &EmptyCustomAttributesCache, &t4394_TI, &t4394_0_0_0, &t4394_1_0_0, NULL, &t4394_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2944.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2944_TI;
#include "t2944MD.h"

extern TypeInfo t331_TI;
extern MethodInfo m16115_MI;
extern MethodInfo m22391_MI;
struct t20;
#define m22391(__this, p0, method) (t331 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
extern Il2CppType t20_0_0_1;
FieldInfo t2944_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2944_TI, offsetof(t2944, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2944_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2944_TI, offsetof(t2944, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2944_FIs[] =
{
	&t2944_f0_FieldInfo,
	&t2944_f1_FieldInfo,
	NULL
};
extern MethodInfo m16112_MI;
static PropertyInfo t2944____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2944_TI, "System.Collections.IEnumerator.Current", &m16112_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2944____Current_PropertyInfo = 
{
	&t2944_TI, "Current", &m16115_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2944_PIs[] =
{
	&t2944____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2944____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2944_m16111_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16111_GM;
MethodInfo m16111_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2944_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2944_m16111_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16111_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16112_GM;
MethodInfo m16112_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2944_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16112_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16113_GM;
MethodInfo m16113_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2944_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16113_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16114_GM;
MethodInfo m16114_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2944_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16114_GM};
extern Il2CppType t331_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16115_GM;
MethodInfo m16115_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2944_TI, &t331_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16115_GM};
static MethodInfo* t2944_MIs[] =
{
	&m16111_MI,
	&m16112_MI,
	&m16113_MI,
	&m16114_MI,
	&m16115_MI,
	NULL
};
extern MethodInfo m16114_MI;
extern MethodInfo m16113_MI;
static MethodInfo* t2944_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16112_MI,
	&m16114_MI,
	&m16113_MI,
	&m16115_MI,
};
static TypeInfo* t2944_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4394_TI,
};
static Il2CppInterfaceOffsetPair t2944_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4394_TI, 7},
};
extern TypeInfo t331_TI;
static Il2CppRGCTXData t2944_RGCTXData[3] = 
{
	&m16115_MI/* Method Usage */,
	&t331_TI/* Class Usage */,
	&m22391_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2944_0_0_0;
extern Il2CppType t2944_1_0_0;
extern Il2CppGenericClass t2944_GC;
TypeInfo t2944_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2944_MIs, t2944_PIs, t2944_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2944_TI, t2944_ITIs, t2944_VT, &EmptyCustomAttributesCache, &t2944_TI, &t2944_0_0_0, &t2944_1_0_0, t2944_IOs, &t2944_GC, NULL, NULL, NULL, t2944_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2944)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5637_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>
extern MethodInfo m29490_MI;
static PropertyInfo t5637____Count_PropertyInfo = 
{
	&t5637_TI, "Count", &m29490_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29491_MI;
static PropertyInfo t5637____IsReadOnly_PropertyInfo = 
{
	&t5637_TI, "IsReadOnly", &m29491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5637_PIs[] =
{
	&t5637____Count_PropertyInfo,
	&t5637____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29490_GM;
MethodInfo m29490_MI = 
{
	"get_Count", NULL, &t5637_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29490_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29491_GM;
MethodInfo m29491_MI = 
{
	"get_IsReadOnly", NULL, &t5637_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29491_GM};
extern Il2CppType t331_0_0_0;
extern Il2CppType t331_0_0_0;
static ParameterInfo t5637_m29492_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29492_GM;
MethodInfo m29492_MI = 
{
	"Add", NULL, &t5637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5637_m29492_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29492_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29493_GM;
MethodInfo m29493_MI = 
{
	"Clear", NULL, &t5637_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29493_GM};
extern Il2CppType t331_0_0_0;
static ParameterInfo t5637_m29494_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29494_GM;
MethodInfo m29494_MI = 
{
	"Contains", NULL, &t5637_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5637_m29494_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29494_GM};
extern Il2CppType t3759_0_0_0;
extern Il2CppType t3759_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5637_m29495_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3759_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29495_GM;
MethodInfo m29495_MI = 
{
	"CopyTo", NULL, &t5637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5637_m29495_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29495_GM};
extern Il2CppType t331_0_0_0;
static ParameterInfo t5637_m29496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29496_GM;
MethodInfo m29496_MI = 
{
	"Remove", NULL, &t5637_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5637_m29496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29496_GM};
static MethodInfo* t5637_MIs[] =
{
	&m29490_MI,
	&m29491_MI,
	&m29492_MI,
	&m29493_MI,
	&m29494_MI,
	&m29495_MI,
	&m29496_MI,
	NULL
};
extern TypeInfo t5639_TI;
static TypeInfo* t5637_ITIs[] = 
{
	&t603_TI,
	&t5639_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5637_0_0_0;
extern Il2CppType t5637_1_0_0;
struct t5637;
extern Il2CppGenericClass t5637_GC;
TypeInfo t5637_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5637_MIs, t5637_PIs, NULL, NULL, NULL, NULL, NULL, &t5637_TI, t5637_ITIs, NULL, &EmptyCustomAttributesCache, &t5637_TI, &t5637_0_0_0, &t5637_1_0_0, NULL, &t5637_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>
extern Il2CppType t4394_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29497_GM;
MethodInfo m29497_MI = 
{
	"GetEnumerator", NULL, &t5639_TI, &t4394_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29497_GM};
static MethodInfo* t5639_MIs[] =
{
	&m29497_MI,
	NULL
};
static TypeInfo* t5639_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5639_0_0_0;
extern Il2CppType t5639_1_0_0;
struct t5639;
extern Il2CppGenericClass t5639_GC;
TypeInfo t5639_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5639_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5639_TI, t5639_ITIs, NULL, &EmptyCustomAttributesCache, &t5639_TI, &t5639_0_0_0, &t5639_1_0_0, NULL, &t5639_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5638_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>
extern MethodInfo m29498_MI;
extern MethodInfo m29499_MI;
static PropertyInfo t5638____Item_PropertyInfo = 
{
	&t5638_TI, "Item", &m29498_MI, &m29499_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5638_PIs[] =
{
	&t5638____Item_PropertyInfo,
	NULL
};
extern Il2CppType t331_0_0_0;
static ParameterInfo t5638_m29500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29500_GM;
MethodInfo m29500_MI = 
{
	"IndexOf", NULL, &t5638_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5638_m29500_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29500_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t331_0_0_0;
static ParameterInfo t5638_m29501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29501_GM;
MethodInfo m29501_MI = 
{
	"Insert", NULL, &t5638_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5638_m29501_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29501_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5638_m29502_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29502_GM;
MethodInfo m29502_MI = 
{
	"RemoveAt", NULL, &t5638_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5638_m29502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29502_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5638_m29498_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t331_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29498_GM;
MethodInfo m29498_MI = 
{
	"get_Item", NULL, &t5638_TI, &t331_0_0_0, RuntimeInvoker_t29_t44, t5638_m29498_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29498_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t331_0_0_0;
static ParameterInfo t5638_m29499_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29499_GM;
MethodInfo m29499_MI = 
{
	"set_Item", NULL, &t5638_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5638_m29499_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29499_GM};
static MethodInfo* t5638_MIs[] =
{
	&m29500_MI,
	&m29501_MI,
	&m29502_MI,
	&m29498_MI,
	&m29499_MI,
	NULL
};
static TypeInfo* t5638_ITIs[] = 
{
	&t603_TI,
	&t5637_TI,
	&t5639_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5638_0_0_0;
extern Il2CppType t5638_1_0_0;
struct t5638;
extern Il2CppGenericClass t5638_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5638_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5638_MIs, t5638_PIs, NULL, NULL, NULL, NULL, NULL, &t5638_TI, t5638_ITIs, NULL, &t1908__CustomAttributeCache, &t5638_TI, &t5638_0_0_0, &t5638_1_0_0, NULL, &t5638_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2945.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2945_TI;
#include "t2945MD.h"

#include "t2946.h"
extern TypeInfo t2946_TI;
#include "t2946MD.h"
extern MethodInfo m16118_MI;
extern MethodInfo m16120_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
extern Il2CppType t316_0_0_33;
FieldInfo t2945_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2945_TI, offsetof(t2945, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2945_FIs[] =
{
	&t2945_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t331_0_0_0;
static ParameterInfo t2945_m16116_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16116_GM;
MethodInfo m16116_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2945_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2945_m16116_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16116_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2945_m16117_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16117_GM;
MethodInfo m16117_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2945_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2945_m16117_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16117_GM};
static MethodInfo* t2945_MIs[] =
{
	&m16116_MI,
	&m16117_MI,
	NULL
};
extern MethodInfo m16117_MI;
extern MethodInfo m16121_MI;
static MethodInfo* t2945_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16117_MI,
	&m16121_MI,
};
extern Il2CppType t2947_0_0_0;
extern TypeInfo t2947_TI;
extern MethodInfo m22401_MI;
extern TypeInfo t331_TI;
extern MethodInfo m16123_MI;
extern TypeInfo t331_TI;
static Il2CppRGCTXData t2945_RGCTXData[8] = 
{
	&t2947_0_0_0/* Type Usage */,
	&t2947_TI/* Class Usage */,
	&m22401_MI/* Method Usage */,
	&t331_TI/* Class Usage */,
	&m16123_MI/* Method Usage */,
	&m16118_MI/* Method Usage */,
	&t331_TI/* Class Usage */,
	&m16120_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2945_0_0_0;
extern Il2CppType t2945_1_0_0;
struct t2945;
extern Il2CppGenericClass t2945_GC;
TypeInfo t2945_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2945_MIs, NULL, t2945_FIs, NULL, &t2946_TI, NULL, NULL, &t2945_TI, NULL, t2945_VT, &EmptyCustomAttributesCache, &t2945_TI, &t2945_0_0_0, &t2945_1_0_0, NULL, &t2945_GC, NULL, NULL, NULL, t2945_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2945), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2947.h"
extern TypeInfo t2947_TI;
#include "t2947MD.h"
struct t556;
#define m22401(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
extern Il2CppType t2947_0_0_1;
FieldInfo t2946_f0_FieldInfo = 
{
	"Delegate", &t2947_0_0_1, &t2946_TI, offsetof(t2946, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2946_FIs[] =
{
	&t2946_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2946_m16118_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16118_GM;
MethodInfo m16118_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2946_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2946_m16118_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16118_GM};
extern Il2CppType t2947_0_0_0;
static ParameterInfo t2946_m16119_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16119_GM;
MethodInfo m16119_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2946_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2946_m16119_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16119_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2946_m16120_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16120_GM;
MethodInfo m16120_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2946_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2946_m16120_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16120_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2946_m16121_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16121_GM;
MethodInfo m16121_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2946_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2946_m16121_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16121_GM};
static MethodInfo* t2946_MIs[] =
{
	&m16118_MI,
	&m16119_MI,
	&m16120_MI,
	&m16121_MI,
	NULL
};
static MethodInfo* t2946_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16120_MI,
	&m16121_MI,
};
extern TypeInfo t2947_TI;
extern TypeInfo t331_TI;
static Il2CppRGCTXData t2946_RGCTXData[5] = 
{
	&t2947_0_0_0/* Type Usage */,
	&t2947_TI/* Class Usage */,
	&m22401_MI/* Method Usage */,
	&t331_TI/* Class Usage */,
	&m16123_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2946_0_0_0;
extern Il2CppType t2946_1_0_0;
struct t2946;
extern Il2CppGenericClass t2946_GC;
TypeInfo t2946_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2946_MIs, NULL, t2946_FIs, NULL, &t556_TI, NULL, NULL, &t2946_TI, NULL, t2946_VT, &EmptyCustomAttributesCache, &t2946_TI, &t2946_0_0_0, &t2946_1_0_0, NULL, &t2946_GC, NULL, NULL, NULL, t2946_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2946), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2947_m16122_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16122_GM;
MethodInfo m16122_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2947_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2947_m16122_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16122_GM};
extern Il2CppType t331_0_0_0;
static ParameterInfo t2947_m16123_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16123_GM;
MethodInfo m16123_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2947_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2947_m16123_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16123_GM};
extern Il2CppType t331_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2947_m16124_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t331_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16124_GM;
MethodInfo m16124_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2947_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2947_m16124_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16124_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2947_m16125_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16125_GM;
MethodInfo m16125_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2947_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2947_m16125_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16125_GM};
static MethodInfo* t2947_MIs[] =
{
	&m16122_MI,
	&m16123_MI,
	&m16124_MI,
	&m16125_MI,
	NULL
};
extern MethodInfo m16124_MI;
extern MethodInfo m16125_MI;
static MethodInfo* t2947_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16123_MI,
	&m16124_MI,
	&m16125_MI,
};
static Il2CppInterfaceOffsetPair t2947_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2947_1_0_0;
struct t2947;
extern Il2CppGenericClass t2947_GC;
TypeInfo t2947_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2947_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2947_TI, NULL, t2947_VT, &EmptyCustomAttributesCache, &t2947_TI, &t2947_0_0_0, &t2947_1_0_0, t2947_IOs, &t2947_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2947), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2948.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2948_TI;
#include "t2948MD.h"

#include "t317.h"
#include "t2949.h"
extern TypeInfo t317_TI;
extern TypeInfo t2949_TI;
#include "t2949MD.h"
extern MethodInfo m16128_MI;
extern MethodInfo m16130_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
extern Il2CppType t316_0_0_33;
FieldInfo t2948_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2948_TI, offsetof(t2948, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2948_FIs[] =
{
	&t2948_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t317_0_0_0;
extern Il2CppType t317_0_0_0;
static ParameterInfo t2948_m16126_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16126_GM;
MethodInfo m16126_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2948_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2948_m16126_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16126_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2948_m16127_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16127_GM;
MethodInfo m16127_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2948_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2948_m16127_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16127_GM};
static MethodInfo* t2948_MIs[] =
{
	&m16126_MI,
	&m16127_MI,
	NULL
};
extern MethodInfo m16127_MI;
extern MethodInfo m16131_MI;
static MethodInfo* t2948_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16127_MI,
	&m16131_MI,
};
extern Il2CppType t2950_0_0_0;
extern TypeInfo t2950_TI;
extern MethodInfo m22402_MI;
extern TypeInfo t317_TI;
extern MethodInfo m16133_MI;
extern TypeInfo t317_TI;
static Il2CppRGCTXData t2948_RGCTXData[8] = 
{
	&t2950_0_0_0/* Type Usage */,
	&t2950_TI/* Class Usage */,
	&m22402_MI/* Method Usage */,
	&t317_TI/* Class Usage */,
	&m16133_MI/* Method Usage */,
	&m16128_MI/* Method Usage */,
	&t317_TI/* Class Usage */,
	&m16130_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2948_0_0_0;
extern Il2CppType t2948_1_0_0;
struct t2948;
extern Il2CppGenericClass t2948_GC;
TypeInfo t2948_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2948_MIs, NULL, t2948_FIs, NULL, &t2949_TI, NULL, NULL, &t2948_TI, NULL, t2948_VT, &EmptyCustomAttributesCache, &t2948_TI, &t2948_0_0_0, &t2948_1_0_0, NULL, &t2948_GC, NULL, NULL, NULL, t2948_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2948), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2950.h"
extern TypeInfo t2950_TI;
#include "t2950MD.h"
struct t556;
#define m22402(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
extern Il2CppType t2950_0_0_1;
FieldInfo t2949_f0_FieldInfo = 
{
	"Delegate", &t2950_0_0_1, &t2949_TI, offsetof(t2949, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2949_FIs[] =
{
	&t2949_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2949_m16128_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16128_GM;
MethodInfo m16128_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2949_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2949_m16128_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16128_GM};
extern Il2CppType t2950_0_0_0;
static ParameterInfo t2949_m16129_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2950_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16129_GM;
MethodInfo m16129_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2949_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2949_m16129_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16129_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2949_m16130_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16130_GM;
MethodInfo m16130_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2949_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2949_m16130_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16130_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2949_m16131_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16131_GM;
MethodInfo m16131_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2949_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2949_m16131_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16131_GM};
static MethodInfo* t2949_MIs[] =
{
	&m16128_MI,
	&m16129_MI,
	&m16130_MI,
	&m16131_MI,
	NULL
};
static MethodInfo* t2949_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16130_MI,
	&m16131_MI,
};
extern TypeInfo t2950_TI;
extern TypeInfo t317_TI;
static Il2CppRGCTXData t2949_RGCTXData[5] = 
{
	&t2950_0_0_0/* Type Usage */,
	&t2950_TI/* Class Usage */,
	&m22402_MI/* Method Usage */,
	&t317_TI/* Class Usage */,
	&m16133_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2949_0_0_0;
extern Il2CppType t2949_1_0_0;
struct t2949;
extern Il2CppGenericClass t2949_GC;
TypeInfo t2949_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2949_MIs, NULL, t2949_FIs, NULL, &t556_TI, NULL, NULL, &t2949_TI, NULL, t2949_VT, &EmptyCustomAttributesCache, &t2949_TI, &t2949_0_0_0, &t2949_1_0_0, NULL, &t2949_GC, NULL, NULL, NULL, t2949_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2949), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2950_m16132_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16132_GM;
MethodInfo m16132_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2950_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2950_m16132_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16132_GM};
extern Il2CppType t317_0_0_0;
static ParameterInfo t2950_m16133_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16133_GM;
MethodInfo m16133_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2950_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2950_m16133_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16133_GM};
extern Il2CppType t317_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2950_m16134_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t317_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16134_GM;
MethodInfo m16134_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2950_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2950_m16134_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16134_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2950_m16135_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16135_GM;
MethodInfo m16135_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2950_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2950_m16135_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16135_GM};
static MethodInfo* t2950_MIs[] =
{
	&m16132_MI,
	&m16133_MI,
	&m16134_MI,
	&m16135_MI,
	NULL
};
extern MethodInfo m16134_MI;
extern MethodInfo m16135_MI;
static MethodInfo* t2950_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16133_MI,
	&m16134_MI,
	&m16135_MI,
};
static Il2CppInterfaceOffsetPair t2950_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2950_1_0_0;
struct t2950;
extern Il2CppGenericClass t2950_GC;
TypeInfo t2950_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2950_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2950_TI, NULL, t2950_VT, &EmptyCustomAttributesCache, &t2950_TI, &t2950_0_0_0, &t2950_1_0_0, t2950_IOs, &t2950_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2950), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4396_TI;

#include "t24.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Camera>
extern MethodInfo m29503_MI;
static PropertyInfo t4396____Current_PropertyInfo = 
{
	&t4396_TI, "Current", &m29503_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4396_PIs[] =
{
	&t4396____Current_PropertyInfo,
	NULL
};
extern Il2CppType t24_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29503_GM;
MethodInfo m29503_MI = 
{
	"get_Current", NULL, &t4396_TI, &t24_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29503_GM};
static MethodInfo* t4396_MIs[] =
{
	&m29503_MI,
	NULL
};
static TypeInfo* t4396_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4396_0_0_0;
extern Il2CppType t4396_1_0_0;
struct t4396;
extern Il2CppGenericClass t4396_GC;
TypeInfo t4396_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4396_MIs, t4396_PIs, NULL, NULL, NULL, NULL, NULL, &t4396_TI, t4396_ITIs, NULL, &EmptyCustomAttributesCache, &t4396_TI, &t4396_0_0_0, &t4396_1_0_0, NULL, &t4396_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2951.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2951_TI;
#include "t2951MD.h"

extern TypeInfo t24_TI;
extern MethodInfo m16140_MI;
extern MethodInfo m22404_MI;
struct t20;
#define m22404(__this, p0, method) (t24 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Camera>
extern Il2CppType t20_0_0_1;
FieldInfo t2951_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2951_TI, offsetof(t2951, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2951_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2951_TI, offsetof(t2951, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2951_FIs[] =
{
	&t2951_f0_FieldInfo,
	&t2951_f1_FieldInfo,
	NULL
};
extern MethodInfo m16137_MI;
static PropertyInfo t2951____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2951_TI, "System.Collections.IEnumerator.Current", &m16137_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2951____Current_PropertyInfo = 
{
	&t2951_TI, "Current", &m16140_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2951_PIs[] =
{
	&t2951____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2951____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2951_m16136_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16136_GM;
MethodInfo m16136_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2951_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2951_m16136_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16136_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16137_GM;
MethodInfo m16137_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2951_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16137_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16138_GM;
MethodInfo m16138_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2951_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16138_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16139_GM;
MethodInfo m16139_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2951_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16139_GM};
extern Il2CppType t24_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16140_GM;
MethodInfo m16140_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2951_TI, &t24_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16140_GM};
static MethodInfo* t2951_MIs[] =
{
	&m16136_MI,
	&m16137_MI,
	&m16138_MI,
	&m16139_MI,
	&m16140_MI,
	NULL
};
extern MethodInfo m16139_MI;
extern MethodInfo m16138_MI;
static MethodInfo* t2951_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16137_MI,
	&m16139_MI,
	&m16138_MI,
	&m16140_MI,
};
static TypeInfo* t2951_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4396_TI,
};
static Il2CppInterfaceOffsetPair t2951_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4396_TI, 7},
};
extern TypeInfo t24_TI;
static Il2CppRGCTXData t2951_RGCTXData[3] = 
{
	&m16140_MI/* Method Usage */,
	&t24_TI/* Class Usage */,
	&m22404_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2951_0_0_0;
extern Il2CppType t2951_1_0_0;
extern Il2CppGenericClass t2951_GC;
TypeInfo t2951_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2951_MIs, t2951_PIs, t2951_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2951_TI, t2951_ITIs, t2951_VT, &EmptyCustomAttributesCache, &t2951_TI, &t2951_0_0_0, &t2951_1_0_0, t2951_IOs, &t2951_GC, NULL, NULL, NULL, t2951_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2951)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5640_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Camera>
extern MethodInfo m29504_MI;
static PropertyInfo t5640____Count_PropertyInfo = 
{
	&t5640_TI, "Count", &m29504_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29505_MI;
static PropertyInfo t5640____IsReadOnly_PropertyInfo = 
{
	&t5640_TI, "IsReadOnly", &m29505_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5640_PIs[] =
{
	&t5640____Count_PropertyInfo,
	&t5640____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29504_GM;
MethodInfo m29504_MI = 
{
	"get_Count", NULL, &t5640_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29504_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29505_GM;
MethodInfo m29505_MI = 
{
	"get_IsReadOnly", NULL, &t5640_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29505_GM};
extern Il2CppType t24_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t5640_m29506_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29506_GM;
MethodInfo m29506_MI = 
{
	"Add", NULL, &t5640_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5640_m29506_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29506_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29507_GM;
MethodInfo m29507_MI = 
{
	"Clear", NULL, &t5640_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29507_GM};
extern Il2CppType t24_0_0_0;
static ParameterInfo t5640_m29508_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29508_GM;
MethodInfo m29508_MI = 
{
	"Contains", NULL, &t5640_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5640_m29508_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29508_GM};
extern Il2CppType t497_0_0_0;
extern Il2CppType t497_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5640_m29509_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t497_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29509_GM;
MethodInfo m29509_MI = 
{
	"CopyTo", NULL, &t5640_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5640_m29509_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29509_GM};
extern Il2CppType t24_0_0_0;
static ParameterInfo t5640_m29510_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29510_GM;
MethodInfo m29510_MI = 
{
	"Remove", NULL, &t5640_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5640_m29510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29510_GM};
static MethodInfo* t5640_MIs[] =
{
	&m29504_MI,
	&m29505_MI,
	&m29506_MI,
	&m29507_MI,
	&m29508_MI,
	&m29509_MI,
	&m29510_MI,
	NULL
};
extern TypeInfo t5642_TI;
static TypeInfo* t5640_ITIs[] = 
{
	&t603_TI,
	&t5642_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5640_0_0_0;
extern Il2CppType t5640_1_0_0;
struct t5640;
extern Il2CppGenericClass t5640_GC;
TypeInfo t5640_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5640_MIs, t5640_PIs, NULL, NULL, NULL, NULL, NULL, &t5640_TI, t5640_ITIs, NULL, &EmptyCustomAttributesCache, &t5640_TI, &t5640_0_0_0, &t5640_1_0_0, NULL, &t5640_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Camera>
extern Il2CppType t4396_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29511_GM;
MethodInfo m29511_MI = 
{
	"GetEnumerator", NULL, &t5642_TI, &t4396_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29511_GM};
static MethodInfo* t5642_MIs[] =
{
	&m29511_MI,
	NULL
};
static TypeInfo* t5642_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5642_0_0_0;
extern Il2CppType t5642_1_0_0;
struct t5642;
extern Il2CppGenericClass t5642_GC;
TypeInfo t5642_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5642_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5642_TI, t5642_ITIs, NULL, &EmptyCustomAttributesCache, &t5642_TI, &t5642_0_0_0, &t5642_1_0_0, NULL, &t5642_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5641_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Camera>
extern MethodInfo m29512_MI;
extern MethodInfo m29513_MI;
static PropertyInfo t5641____Item_PropertyInfo = 
{
	&t5641_TI, "Item", &m29512_MI, &m29513_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5641_PIs[] =
{
	&t5641____Item_PropertyInfo,
	NULL
};
extern Il2CppType t24_0_0_0;
static ParameterInfo t5641_m29514_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29514_GM;
MethodInfo m29514_MI = 
{
	"IndexOf", NULL, &t5641_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5641_m29514_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29514_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t5641_m29515_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29515_GM;
MethodInfo m29515_MI = 
{
	"Insert", NULL, &t5641_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5641_m29515_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29515_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5641_m29516_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29516_GM;
MethodInfo m29516_MI = 
{
	"RemoveAt", NULL, &t5641_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5641_m29516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29516_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5641_m29512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t24_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29512_GM;
MethodInfo m29512_MI = 
{
	"get_Item", NULL, &t5641_TI, &t24_0_0_0, RuntimeInvoker_t29_t44, t5641_m29512_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29512_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t5641_m29513_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29513_GM;
MethodInfo m29513_MI = 
{
	"set_Item", NULL, &t5641_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5641_m29513_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29513_GM};
static MethodInfo* t5641_MIs[] =
{
	&m29514_MI,
	&m29515_MI,
	&m29516_MI,
	&m29512_MI,
	&m29513_MI,
	NULL
};
static TypeInfo* t5641_ITIs[] = 
{
	&t603_TI,
	&t5640_TI,
	&t5642_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5641_0_0_0;
extern Il2CppType t5641_1_0_0;
struct t5641;
extern Il2CppGenericClass t5641_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5641_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5641_MIs, t5641_PIs, NULL, NULL, NULL, NULL, NULL, &t5641_TI, t5641_ITIs, NULL, &t1908__CustomAttributeCache, &t5641_TI, &t5641_0_0_0, &t5641_1_0_0, NULL, &t5641_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2952.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2952_TI;
#include "t2952MD.h"

#include "t2953.h"
extern TypeInfo t2953_TI;
#include "t2953MD.h"
extern MethodInfo m16143_MI;
extern MethodInfo m16145_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
extern Il2CppType t316_0_0_33;
FieldInfo t2952_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2952_TI, offsetof(t2952, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2952_FIs[] =
{
	&t2952_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t2952_m16141_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16141_GM;
MethodInfo m16141_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2952_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2952_m16141_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16141_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2952_m16142_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16142_GM;
MethodInfo m16142_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2952_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2952_m16142_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16142_GM};
static MethodInfo* t2952_MIs[] =
{
	&m16141_MI,
	&m16142_MI,
	NULL
};
extern MethodInfo m16142_MI;
extern MethodInfo m16146_MI;
static MethodInfo* t2952_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16142_MI,
	&m16146_MI,
};
extern Il2CppType t2954_0_0_0;
extern TypeInfo t2954_TI;
extern MethodInfo m22414_MI;
extern TypeInfo t24_TI;
extern MethodInfo m16148_MI;
extern TypeInfo t24_TI;
static Il2CppRGCTXData t2952_RGCTXData[8] = 
{
	&t2954_0_0_0/* Type Usage */,
	&t2954_TI/* Class Usage */,
	&m22414_MI/* Method Usage */,
	&t24_TI/* Class Usage */,
	&m16148_MI/* Method Usage */,
	&m16143_MI/* Method Usage */,
	&t24_TI/* Class Usage */,
	&m16145_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2952_0_0_0;
extern Il2CppType t2952_1_0_0;
struct t2952;
extern Il2CppGenericClass t2952_GC;
TypeInfo t2952_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2952_MIs, NULL, t2952_FIs, NULL, &t2953_TI, NULL, NULL, &t2952_TI, NULL, t2952_VT, &EmptyCustomAttributesCache, &t2952_TI, &t2952_0_0_0, &t2952_1_0_0, NULL, &t2952_GC, NULL, NULL, NULL, t2952_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2952), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2954.h"
extern TypeInfo t2954_TI;
#include "t2954MD.h"
struct t556;
#define m22414(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
extern Il2CppType t2954_0_0_1;
FieldInfo t2953_f0_FieldInfo = 
{
	"Delegate", &t2954_0_0_1, &t2953_TI, offsetof(t2953, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2953_FIs[] =
{
	&t2953_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2953_m16143_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16143_GM;
MethodInfo m16143_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2953_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2953_m16143_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16143_GM};
extern Il2CppType t2954_0_0_0;
static ParameterInfo t2953_m16144_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2954_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16144_GM;
MethodInfo m16144_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2953_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2953_m16144_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16144_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2953_m16145_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16145_GM;
MethodInfo m16145_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2953_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2953_m16145_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16145_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2953_m16146_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16146_GM;
MethodInfo m16146_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2953_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2953_m16146_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16146_GM};
static MethodInfo* t2953_MIs[] =
{
	&m16143_MI,
	&m16144_MI,
	&m16145_MI,
	&m16146_MI,
	NULL
};
static MethodInfo* t2953_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16145_MI,
	&m16146_MI,
};
extern TypeInfo t2954_TI;
extern TypeInfo t24_TI;
static Il2CppRGCTXData t2953_RGCTXData[5] = 
{
	&t2954_0_0_0/* Type Usage */,
	&t2954_TI/* Class Usage */,
	&m22414_MI/* Method Usage */,
	&t24_TI/* Class Usage */,
	&m16148_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2953_0_0_0;
extern Il2CppType t2953_1_0_0;
struct t2953;
extern Il2CppGenericClass t2953_GC;
TypeInfo t2953_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2953_MIs, NULL, t2953_FIs, NULL, &t556_TI, NULL, NULL, &t2953_TI, NULL, t2953_VT, &EmptyCustomAttributesCache, &t2953_TI, &t2953_0_0_0, &t2953_1_0_0, NULL, &t2953_GC, NULL, NULL, NULL, t2953_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2953), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2954_m16147_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16147_GM;
MethodInfo m16147_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2954_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2954_m16147_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16147_GM};
extern Il2CppType t24_0_0_0;
static ParameterInfo t2954_m16148_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16148_GM;
MethodInfo m16148_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2954_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2954_m16148_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16148_GM};
extern Il2CppType t24_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2954_m16149_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t24_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16149_GM;
MethodInfo m16149_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2954_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2954_m16149_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16149_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2954_m16150_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16150_GM;
MethodInfo m16150_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2954_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2954_m16150_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16150_GM};
static MethodInfo* t2954_MIs[] =
{
	&m16147_MI,
	&m16148_MI,
	&m16149_MI,
	&m16150_MI,
	NULL
};
extern MethodInfo m16149_MI;
extern MethodInfo m16150_MI;
static MethodInfo* t2954_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16148_MI,
	&m16149_MI,
	&m16150_MI,
};
static Il2CppInterfaceOffsetPair t2954_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2954_1_0_0;
struct t2954;
extern Il2CppGenericClass t2954_GC;
TypeInfo t2954_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2954_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2954_TI, NULL, t2954_VT, &EmptyCustomAttributesCache, &t2954_TI, &t2954_0_0_0, &t2954_1_0_0, t2954_IOs, &t2954_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2954), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4398_TI;

#include "t500.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Display>
extern MethodInfo m29517_MI;
static PropertyInfo t4398____Current_PropertyInfo = 
{
	&t4398_TI, "Current", &m29517_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4398_PIs[] =
{
	&t4398____Current_PropertyInfo,
	NULL
};
extern Il2CppType t500_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29517_GM;
MethodInfo m29517_MI = 
{
	"get_Current", NULL, &t4398_TI, &t500_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29517_GM};
static MethodInfo* t4398_MIs[] =
{
	&m29517_MI,
	NULL
};
static TypeInfo* t4398_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4398_0_0_0;
extern Il2CppType t4398_1_0_0;
struct t4398;
extern Il2CppGenericClass t4398_GC;
TypeInfo t4398_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4398_MIs, t4398_PIs, NULL, NULL, NULL, NULL, NULL, &t4398_TI, t4398_ITIs, NULL, &EmptyCustomAttributesCache, &t4398_TI, &t4398_0_0_0, &t4398_1_0_0, NULL, &t4398_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2955.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2955_TI;
#include "t2955MD.h"

extern TypeInfo t500_TI;
extern MethodInfo m16155_MI;
extern MethodInfo m22416_MI;
struct t20;
#define m22416(__this, p0, method) (t500 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Display>
extern Il2CppType t20_0_0_1;
FieldInfo t2955_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2955_TI, offsetof(t2955, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2955_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2955_TI, offsetof(t2955, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2955_FIs[] =
{
	&t2955_f0_FieldInfo,
	&t2955_f1_FieldInfo,
	NULL
};
extern MethodInfo m16152_MI;
static PropertyInfo t2955____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2955_TI, "System.Collections.IEnumerator.Current", &m16152_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2955____Current_PropertyInfo = 
{
	&t2955_TI, "Current", &m16155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2955_PIs[] =
{
	&t2955____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2955____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2955_m16151_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16151_GM;
MethodInfo m16151_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2955_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2955_m16151_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16151_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16152_GM;
MethodInfo m16152_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2955_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16152_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16153_GM;
MethodInfo m16153_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2955_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16153_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16154_GM;
MethodInfo m16154_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2955_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16154_GM};
extern Il2CppType t500_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16155_GM;
MethodInfo m16155_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2955_TI, &t500_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16155_GM};
static MethodInfo* t2955_MIs[] =
{
	&m16151_MI,
	&m16152_MI,
	&m16153_MI,
	&m16154_MI,
	&m16155_MI,
	NULL
};
extern MethodInfo m16154_MI;
extern MethodInfo m16153_MI;
static MethodInfo* t2955_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16152_MI,
	&m16154_MI,
	&m16153_MI,
	&m16155_MI,
};
static TypeInfo* t2955_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4398_TI,
};
static Il2CppInterfaceOffsetPair t2955_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4398_TI, 7},
};
extern TypeInfo t500_TI;
static Il2CppRGCTXData t2955_RGCTXData[3] = 
{
	&m16155_MI/* Method Usage */,
	&t500_TI/* Class Usage */,
	&m22416_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2955_0_0_0;
extern Il2CppType t2955_1_0_0;
extern Il2CppGenericClass t2955_GC;
TypeInfo t2955_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2955_MIs, t2955_PIs, t2955_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2955_TI, t2955_ITIs, t2955_VT, &EmptyCustomAttributesCache, &t2955_TI, &t2955_0_0_0, &t2955_1_0_0, t2955_IOs, &t2955_GC, NULL, NULL, NULL, t2955_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2955)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5643_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Display>
extern MethodInfo m29518_MI;
static PropertyInfo t5643____Count_PropertyInfo = 
{
	&t5643_TI, "Count", &m29518_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29519_MI;
static PropertyInfo t5643____IsReadOnly_PropertyInfo = 
{
	&t5643_TI, "IsReadOnly", &m29519_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5643_PIs[] =
{
	&t5643____Count_PropertyInfo,
	&t5643____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29518_GM;
MethodInfo m29518_MI = 
{
	"get_Count", NULL, &t5643_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29518_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29519_GM;
MethodInfo m29519_MI = 
{
	"get_IsReadOnly", NULL, &t5643_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29519_GM};
extern Il2CppType t500_0_0_0;
extern Il2CppType t500_0_0_0;
static ParameterInfo t5643_m29520_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t500_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29520_GM;
MethodInfo m29520_MI = 
{
	"Add", NULL, &t5643_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5643_m29520_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29520_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29521_GM;
MethodInfo m29521_MI = 
{
	"Clear", NULL, &t5643_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29521_GM};
extern Il2CppType t500_0_0_0;
static ParameterInfo t5643_m29522_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t500_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29522_GM;
MethodInfo m29522_MI = 
{
	"Contains", NULL, &t5643_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5643_m29522_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29522_GM};
extern Il2CppType t501_0_0_0;
extern Il2CppType t501_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5643_m29523_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t501_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29523_GM;
MethodInfo m29523_MI = 
{
	"CopyTo", NULL, &t5643_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5643_m29523_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29523_GM};
extern Il2CppType t500_0_0_0;
static ParameterInfo t5643_m29524_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t500_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29524_GM;
MethodInfo m29524_MI = 
{
	"Remove", NULL, &t5643_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5643_m29524_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29524_GM};
static MethodInfo* t5643_MIs[] =
{
	&m29518_MI,
	&m29519_MI,
	&m29520_MI,
	&m29521_MI,
	&m29522_MI,
	&m29523_MI,
	&m29524_MI,
	NULL
};
extern TypeInfo t5645_TI;
static TypeInfo* t5643_ITIs[] = 
{
	&t603_TI,
	&t5645_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5643_0_0_0;
extern Il2CppType t5643_1_0_0;
struct t5643;
extern Il2CppGenericClass t5643_GC;
TypeInfo t5643_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5643_MIs, t5643_PIs, NULL, NULL, NULL, NULL, NULL, &t5643_TI, t5643_ITIs, NULL, &EmptyCustomAttributesCache, &t5643_TI, &t5643_0_0_0, &t5643_1_0_0, NULL, &t5643_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Display>
extern Il2CppType t4398_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29525_GM;
MethodInfo m29525_MI = 
{
	"GetEnumerator", NULL, &t5645_TI, &t4398_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29525_GM};
static MethodInfo* t5645_MIs[] =
{
	&m29525_MI,
	NULL
};
static TypeInfo* t5645_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5645_0_0_0;
extern Il2CppType t5645_1_0_0;
struct t5645;
extern Il2CppGenericClass t5645_GC;
TypeInfo t5645_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5645_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5645_TI, t5645_ITIs, NULL, &EmptyCustomAttributesCache, &t5645_TI, &t5645_0_0_0, &t5645_1_0_0, NULL, &t5645_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5644_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Display>
extern MethodInfo m29526_MI;
extern MethodInfo m29527_MI;
static PropertyInfo t5644____Item_PropertyInfo = 
{
	&t5644_TI, "Item", &m29526_MI, &m29527_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5644_PIs[] =
{
	&t5644____Item_PropertyInfo,
	NULL
};
extern Il2CppType t500_0_0_0;
static ParameterInfo t5644_m29528_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t500_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29528_GM;
MethodInfo m29528_MI = 
{
	"IndexOf", NULL, &t5644_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5644_m29528_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29528_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t500_0_0_0;
static ParameterInfo t5644_m29529_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t500_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29529_GM;
MethodInfo m29529_MI = 
{
	"Insert", NULL, &t5644_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5644_m29529_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29529_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5644_m29530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29530_GM;
MethodInfo m29530_MI = 
{
	"RemoveAt", NULL, &t5644_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5644_m29530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29530_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5644_m29526_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t500_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29526_GM;
MethodInfo m29526_MI = 
{
	"get_Item", NULL, &t5644_TI, &t500_0_0_0, RuntimeInvoker_t29_t44, t5644_m29526_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29526_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t500_0_0_0;
static ParameterInfo t5644_m29527_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t500_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29527_GM;
MethodInfo m29527_MI = 
{
	"set_Item", NULL, &t5644_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5644_m29527_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29527_GM};
static MethodInfo* t5644_MIs[] =
{
	&m29528_MI,
	&m29529_MI,
	&m29530_MI,
	&m29526_MI,
	&m29527_MI,
	NULL
};
static TypeInfo* t5644_ITIs[] = 
{
	&t603_TI,
	&t5643_TI,
	&t5645_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5644_0_0_0;
extern Il2CppType t5644_1_0_0;
struct t5644;
extern Il2CppGenericClass t5644_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5644_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5644_MIs, t5644_PIs, NULL, NULL, NULL, NULL, NULL, &t5644_TI, t5644_ITIs, NULL, &t1908__CustomAttributeCache, &t5644_TI, &t5644_0_0_0, &t5644_1_0_0, NULL, &t5644_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4400_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IntPtr>
extern MethodInfo m29531_MI;
static PropertyInfo t4400____Current_PropertyInfo = 
{
	&t4400_TI, "Current", &m29531_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4400_PIs[] =
{
	&t4400____Current_PropertyInfo,
	NULL
};
extern Il2CppType t35_0_0_0;
extern void* RuntimeInvoker_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29531_GM;
MethodInfo m29531_MI = 
{
	"get_Current", NULL, &t4400_TI, &t35_0_0_0, RuntimeInvoker_t35, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29531_GM};
static MethodInfo* t4400_MIs[] =
{
	&m29531_MI,
	NULL
};
static TypeInfo* t4400_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4400_0_0_0;
extern Il2CppType t4400_1_0_0;
struct t4400;
extern Il2CppGenericClass t4400_GC;
TypeInfo t4400_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4400_MIs, t4400_PIs, NULL, NULL, NULL, NULL, NULL, &t4400_TI, t4400_ITIs, NULL, &EmptyCustomAttributesCache, &t4400_TI, &t4400_0_0_0, &t4400_1_0_0, NULL, &t4400_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2956.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2956_TI;
#include "t2956MD.h"

extern TypeInfo t35_TI;
extern MethodInfo m16160_MI;
extern MethodInfo m22427_MI;
struct t20;
 t35 m22427 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16156_MI;
 void m16156 (t2956 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16157_MI;
 t29 * m16157 (t2956 * __this, MethodInfo* method){
	{
		t35 L_0 = m16160(__this, &m16160_MI);
		t35 L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t35_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16158_MI;
 void m16158 (t2956 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16159_MI;
 bool m16159 (t2956 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t35 m16160 (t2956 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t35 L_8 = m22427(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22427_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IntPtr>
extern Il2CppType t20_0_0_1;
FieldInfo t2956_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2956_TI, offsetof(t2956, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2956_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2956_TI, offsetof(t2956, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2956_FIs[] =
{
	&t2956_f0_FieldInfo,
	&t2956_f1_FieldInfo,
	NULL
};
static PropertyInfo t2956____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2956_TI, "System.Collections.IEnumerator.Current", &m16157_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2956____Current_PropertyInfo = 
{
	&t2956_TI, "Current", &m16160_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2956_PIs[] =
{
	&t2956____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2956____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2956_m16156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16156_GM;
MethodInfo m16156_MI = 
{
	".ctor", (methodPointerType)&m16156, &t2956_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2956_m16156_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16156_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16157_GM;
MethodInfo m16157_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16157, &t2956_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16157_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16158_GM;
MethodInfo m16158_MI = 
{
	"Dispose", (methodPointerType)&m16158, &t2956_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16158_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16159_GM;
MethodInfo m16159_MI = 
{
	"MoveNext", (methodPointerType)&m16159, &t2956_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16159_GM};
extern Il2CppType t35_0_0_0;
extern void* RuntimeInvoker_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16160_GM;
MethodInfo m16160_MI = 
{
	"get_Current", (methodPointerType)&m16160, &t2956_TI, &t35_0_0_0, RuntimeInvoker_t35, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16160_GM};
static MethodInfo* t2956_MIs[] =
{
	&m16156_MI,
	&m16157_MI,
	&m16158_MI,
	&m16159_MI,
	&m16160_MI,
	NULL
};
static MethodInfo* t2956_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16157_MI,
	&m16159_MI,
	&m16158_MI,
	&m16160_MI,
};
static TypeInfo* t2956_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4400_TI,
};
static Il2CppInterfaceOffsetPair t2956_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4400_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2956_0_0_0;
extern Il2CppType t2956_1_0_0;
extern Il2CppGenericClass t2956_GC;
TypeInfo t2956_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2956_MIs, t2956_PIs, t2956_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2956_TI, t2956_ITIs, t2956_VT, &EmptyCustomAttributesCache, &t2956_TI, &t2956_0_0_0, &t2956_1_0_0, t2956_IOs, &t2956_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2956)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5646_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IntPtr>
extern MethodInfo m29532_MI;
static PropertyInfo t5646____Count_PropertyInfo = 
{
	&t5646_TI, "Count", &m29532_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29533_MI;
static PropertyInfo t5646____IsReadOnly_PropertyInfo = 
{
	&t5646_TI, "IsReadOnly", &m29533_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5646_PIs[] =
{
	&t5646____Count_PropertyInfo,
	&t5646____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29532_GM;
MethodInfo m29532_MI = 
{
	"get_Count", NULL, &t5646_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29532_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29533_GM;
MethodInfo m29533_MI = 
{
	"get_IsReadOnly", NULL, &t5646_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29533_GM};
extern Il2CppType t35_0_0_0;
static ParameterInfo t5646_m29534_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29534_GM;
MethodInfo m29534_MI = 
{
	"Add", NULL, &t5646_TI, &t21_0_0_0, RuntimeInvoker_t21_t35, t5646_m29534_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29534_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29535_GM;
MethodInfo m29535_MI = 
{
	"Clear", NULL, &t5646_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29535_GM};
extern Il2CppType t35_0_0_0;
static ParameterInfo t5646_m29536_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29536_GM;
MethodInfo m29536_MI = 
{
	"Contains", NULL, &t5646_TI, &t40_0_0_0, RuntimeInvoker_t40_t35, t5646_m29536_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29536_GM};
extern Il2CppType t502_0_0_0;
extern Il2CppType t502_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5646_m29537_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t502_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29537_GM;
MethodInfo m29537_MI = 
{
	"CopyTo", NULL, &t5646_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5646_m29537_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29537_GM};
extern Il2CppType t35_0_0_0;
static ParameterInfo t5646_m29538_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29538_GM;
MethodInfo m29538_MI = 
{
	"Remove", NULL, &t5646_TI, &t40_0_0_0, RuntimeInvoker_t40_t35, t5646_m29538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29538_GM};
static MethodInfo* t5646_MIs[] =
{
	&m29532_MI,
	&m29533_MI,
	&m29534_MI,
	&m29535_MI,
	&m29536_MI,
	&m29537_MI,
	&m29538_MI,
	NULL
};
extern TypeInfo t5648_TI;
static TypeInfo* t5646_ITIs[] = 
{
	&t603_TI,
	&t5648_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5646_0_0_0;
extern Il2CppType t5646_1_0_0;
struct t5646;
extern Il2CppGenericClass t5646_GC;
TypeInfo t5646_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5646_MIs, t5646_PIs, NULL, NULL, NULL, NULL, NULL, &t5646_TI, t5646_ITIs, NULL, &EmptyCustomAttributesCache, &t5646_TI, &t5646_0_0_0, &t5646_1_0_0, NULL, &t5646_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IntPtr>
extern Il2CppType t4400_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29539_GM;
MethodInfo m29539_MI = 
{
	"GetEnumerator", NULL, &t5648_TI, &t4400_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29539_GM};
static MethodInfo* t5648_MIs[] =
{
	&m29539_MI,
	NULL
};
static TypeInfo* t5648_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5648_0_0_0;
extern Il2CppType t5648_1_0_0;
struct t5648;
extern Il2CppGenericClass t5648_GC;
TypeInfo t5648_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5648_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5648_TI, t5648_ITIs, NULL, &EmptyCustomAttributesCache, &t5648_TI, &t5648_0_0_0, &t5648_1_0_0, NULL, &t5648_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5647_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IntPtr>
extern MethodInfo m29540_MI;
extern MethodInfo m29541_MI;
static PropertyInfo t5647____Item_PropertyInfo = 
{
	&t5647_TI, "Item", &m29540_MI, &m29541_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5647_PIs[] =
{
	&t5647____Item_PropertyInfo,
	NULL
};
extern Il2CppType t35_0_0_0;
static ParameterInfo t5647_m29542_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29542_GM;
MethodInfo m29542_MI = 
{
	"IndexOf", NULL, &t5647_TI, &t44_0_0_0, RuntimeInvoker_t44_t35, t5647_m29542_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29542_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t5647_m29543_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29543_GM;
MethodInfo m29543_MI = 
{
	"Insert", NULL, &t5647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t35, t5647_m29543_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29543_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5647_m29544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29544_GM;
MethodInfo m29544_MI = 
{
	"RemoveAt", NULL, &t5647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5647_m29544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29544_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5647_m29540_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t35_0_0_0;
extern void* RuntimeInvoker_t35_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29540_GM;
MethodInfo m29540_MI = 
{
	"get_Item", NULL, &t5647_TI, &t35_0_0_0, RuntimeInvoker_t35_t44, t5647_m29540_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29540_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t5647_m29541_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29541_GM;
MethodInfo m29541_MI = 
{
	"set_Item", NULL, &t5647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t35, t5647_m29541_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29541_GM};
static MethodInfo* t5647_MIs[] =
{
	&m29542_MI,
	&m29543_MI,
	&m29544_MI,
	&m29540_MI,
	&m29541_MI,
	NULL
};
static TypeInfo* t5647_ITIs[] = 
{
	&t603_TI,
	&t5646_TI,
	&t5648_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5647_0_0_0;
extern Il2CppType t5647_1_0_0;
struct t5647;
extern Il2CppGenericClass t5647_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5647_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5647_MIs, t5647_PIs, NULL, NULL, NULL, NULL, NULL, &t5647_TI, t5647_ITIs, NULL, &t1908__CustomAttributeCache, &t5647_TI, &t5647_0_0_0, &t5647_1_0_0, NULL, &t5647_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5649_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo m29545_MI;
static PropertyInfo t5649____Count_PropertyInfo = 
{
	&t5649_TI, "Count", &m29545_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29546_MI;
static PropertyInfo t5649____IsReadOnly_PropertyInfo = 
{
	&t5649_TI, "IsReadOnly", &m29546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5649_PIs[] =
{
	&t5649____Count_PropertyInfo,
	&t5649____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29545_GM;
MethodInfo m29545_MI = 
{
	"get_Count", NULL, &t5649_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29545_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29546_GM;
MethodInfo m29546_MI = 
{
	"get_IsReadOnly", NULL, &t5649_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29546_GM};
extern Il2CppType t374_0_0_0;
extern Il2CppType t374_0_0_0;
static ParameterInfo t5649_m29547_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t374_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29547_GM;
MethodInfo m29547_MI = 
{
	"Add", NULL, &t5649_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5649_m29547_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29547_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29548_GM;
MethodInfo m29548_MI = 
{
	"Clear", NULL, &t5649_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29548_GM};
extern Il2CppType t374_0_0_0;
static ParameterInfo t5649_m29549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t374_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29549_GM;
MethodInfo m29549_MI = 
{
	"Contains", NULL, &t5649_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5649_m29549_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29549_GM};
extern Il2CppType t3544_0_0_0;
extern Il2CppType t3544_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5649_m29550_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3544_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29550_GM;
MethodInfo m29550_MI = 
{
	"CopyTo", NULL, &t5649_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5649_m29550_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29550_GM};
extern Il2CppType t374_0_0_0;
static ParameterInfo t5649_m29551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t374_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29551_GM;
MethodInfo m29551_MI = 
{
	"Remove", NULL, &t5649_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5649_m29551_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29551_GM};
static MethodInfo* t5649_MIs[] =
{
	&m29545_MI,
	&m29546_MI,
	&m29547_MI,
	&m29548_MI,
	&m29549_MI,
	&m29550_MI,
	&m29551_MI,
	NULL
};
extern TypeInfo t5651_TI;
static TypeInfo* t5649_ITIs[] = 
{
	&t603_TI,
	&t5651_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5649_0_0_0;
extern Il2CppType t5649_1_0_0;
struct t5649;
extern Il2CppGenericClass t5649_GC;
TypeInfo t5649_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5649_MIs, t5649_PIs, NULL, NULL, NULL, NULL, NULL, &t5649_TI, t5649_ITIs, NULL, &EmptyCustomAttributesCache, &t5649_TI, &t5649_0_0_0, &t5649_1_0_0, NULL, &t5649_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
