﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2861;
struct t29;
struct t469;
struct t472;
#include "t725.h"
#include "t2859.h"

 void m15574 (t2861 * __this, t472 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15575 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15576 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15577 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15578 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15579 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2859  m15580 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15581 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15582 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15583 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15584 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15585 (t2861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
