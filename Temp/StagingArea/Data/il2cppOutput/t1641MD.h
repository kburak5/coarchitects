﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1641;
struct t29;

 void m9229 (t1641 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9230 (t1641 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9231 (t1641 * __this, int64_t p0, int64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
