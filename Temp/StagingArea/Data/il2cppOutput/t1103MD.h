﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1103;
struct t7;
struct t781;
struct t1206;

 void m8225 (t1103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8226 (t1103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8227 (t1103 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8228 (t1103 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8229 (t1103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5165 (t1103 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1206 * m8230 (t1103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8231 (t1103 * __this, t781* p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8232 (t1103 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8233 (t1103 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8234 (t1103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8235 (t1103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1103 * m5164 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1103 * m8236 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
