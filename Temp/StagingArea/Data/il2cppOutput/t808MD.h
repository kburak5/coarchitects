﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t808;
struct t949;
struct t781;
struct t7;
struct t810;
struct t780;
struct t782;
struct t783;
struct t777;
#include "t465.h"

 void m4533 (t808 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4534 (t808 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t949 * m4163 (t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4535 (t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4171 (t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4169 (t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4536 (t808 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t810 * m4167 (t808 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t810 * m4537 (t808 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4538 (t808 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4539 (t808 * __this, t782 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4540 (t808 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4166 (t808 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
