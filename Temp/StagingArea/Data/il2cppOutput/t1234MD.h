﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1234;
struct t7;
struct t1235;
struct t1236;
struct t1237;

 void m6528 (t1234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6529 (t1234 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1235 * m6530 (t1234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6531 (t1234 * __this, t1236 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6532 (t1234 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6533 (t1234 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6534 (t1234 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6535 (t1234 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6536 (t1234 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6537 (t1234 * __this, t1236 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
