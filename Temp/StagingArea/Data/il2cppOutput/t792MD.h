﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t792;
struct t29;
struct t789;
struct t7;
struct t780;
struct t20;
struct t136;
struct t819;

 void m3426 (t792 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3427 (t792 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3428 (t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3429 (t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3430 (t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3431 (t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t789 * m3432 (t792 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t819 * m3433 (t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
