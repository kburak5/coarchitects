﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t890;
struct t7;
struct t719;
struct t878;

 void m3841 (t890 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3842 (t890 * __this, t7* p0, t719 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3843 (t890 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
