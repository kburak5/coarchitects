﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3082;
struct t29;
struct t20;
#include "t632.h"

 void m17030 (t3082 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17031 (t3082 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17032 (t3082 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17033 (t3082 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t632  m17034 (t3082 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
