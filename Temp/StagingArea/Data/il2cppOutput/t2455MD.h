﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2455;
struct t29;
struct t148;
struct t350;
struct t66;
struct t67;
#include "t35.h"
#include "t725.h"

 void m13196 (t2455 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m13197 (t2455 * __this, t148 * p0, t350 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13198 (t2455 * __this, t148 * p0, t350 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m13199 (t2455 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
