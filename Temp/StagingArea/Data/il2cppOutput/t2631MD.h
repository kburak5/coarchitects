﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2631;
struct t29;
struct t20;
#include "t23.h"

 void m14147 (t2631 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14148 (t2631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14149 (t2631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14150 (t2631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m14151 (t2631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
