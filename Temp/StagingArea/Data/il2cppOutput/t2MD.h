﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2;
struct t415;
struct t223;
#include "t164.h"
#include "t17.h"
#include "t413.h"
#include "t408.h"

 void m1998 (t29 * __this, t415 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2424 (t29 * __this, t415 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2425 (t2 * __this, t164 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m1572 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2426 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2427 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1662 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1796 (t2 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2428 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2429 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1793 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1663 (t2 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2430 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2431 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1794 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1797 (t2 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2432 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2433 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m61 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m63 (t2 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2434 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2435 (t2 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1658 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1798 (t2 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2436 (t29 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2437 (t2 * __this, t223* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1860 (t2 * __this, t223* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1994 (t2 * __this, int32_t p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1946 (t2 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2438 (t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
