﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t117;
struct t6;
struct t115;
struct t7;
struct t16;
struct t53;
#include "t119.h"
#include "t112.h"
#include "t17.h"

 void m269 (t117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m270 (t117 * __this, int32_t p0, t6 ** p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m271 (t117 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m272 (t117 * __this, t119  p0, bool* p1, bool* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m273 (t117 * __this, t6 * p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m274 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t115 * m275 (t117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m276 (t117 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m277 (t29 * __this, t17  p0, t17  p1, float p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m278 (t117 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m279 (t117 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m280 (t117 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m281 (t117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m282 (t117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m283 (t117 * __this, t16 * p0, t53 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
