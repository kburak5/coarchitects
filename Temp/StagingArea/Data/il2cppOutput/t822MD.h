﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t822;
struct t945;
struct t731;
struct t7;
struct t781;
struct t780;
struct t808;

 void m4553 (t822 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t945 * m4188 (t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m4170 (t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4554 (t822 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t780 * m4555 (t822 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t808 * m4556 (t822 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4557 (t822 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t945 * m4558 (t822 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m4559 (t822 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
