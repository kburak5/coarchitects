﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t1742_TI;

#include "t7.h"
#include "t29.h"

#include "t20.h"

// Metadata Definition System.ICustomFormatter
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t1094_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1742_m9680_ParameterInfos[] = 
{
	{"format", 0, 134224081, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"arg", 1, 134224082, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"formatProvider", 2, 134224083, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9680_MI = 
{
	"Format", NULL, &t1742_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1742_m9680_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 3, false, false, 5100, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1742_MIs[] =
{
	&m9680_MI,
	NULL
};
extern TypeInfo t437_TI;
#include "t437.h"
#include "t437MD.h"
extern MethodInfo m2055_MI;
void t1742_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1742__CustomAttributeCache = {
1,
NULL,
&t1742_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1742_0_0_0;
extern Il2CppType t1742_1_0_0;
struct t1742;
extern CustomAttributesCache t1742__CustomAttributeCache;
TypeInfo t1742_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICustomFormatter", "System", t1742_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1742_TI, NULL, NULL, &t1742__CustomAttributeCache, &t1742_TI, &t1742_0_0_0, &t1742_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1094_TI;

#include "t42.h"


// Metadata Definition System.IFormatProvider
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1094_m9654_ParameterInfos[] = 
{
	{"formatType", 0, 134224084, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9654_MI = 
{
	"GetFormat", NULL, &t1094_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1094_m9654_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, false, 5101, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1094_MIs[] =
{
	&m9654_MI,
	NULL
};
void t1094_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1094__CustomAttributeCache = {
1,
NULL,
&t1094_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1094_1_0_0;
struct t1094;
extern CustomAttributesCache t1094__CustomAttributeCache;
TypeInfo t1094_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IFormatProvider", "System", t1094_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1094_TI, NULL, NULL, &t1094__CustomAttributeCache, &t1094_TI, &t1094_0_0_0, &t1094_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t595.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t595_TI;
#include "t595MD.h"

#include "t21.h"
#include "t733.h"
#include "t735.h"
#include "t1167MD.h"
#include "t956MD.h"
extern MethodInfo m6079_MI;
extern MethodInfo m4202_MI;
extern MethodInfo m9517_MI;


extern MethodInfo m9276_MI;
 void m9276 (t595 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2181, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		return;
	}
}
extern MethodInfo m2869_MI;
 void m2869 (t595 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		return;
	}
}
extern MethodInfo m9277_MI;
 void m9277 (t595 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.IndexOutOfRangeException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9276_MI = 
{
	".ctor", (methodPointerType)&m9276, &t595_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5102, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t595_m2869_ParameterInfos[] = 
{
	{"message", 0, 134224085, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2869_MI = 
{
	".ctor", (methodPointerType)&m2869, &t595_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t595_m2869_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5103, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t595_m9277_ParameterInfos[] = 
{
	{"info", 0, 134224086, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224087, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9277_MI = 
{
	".ctor", (methodPointerType)&m9277, &t595_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t595_m9277_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 5104, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t595_MIs[] =
{
	&m9276_MI,
	&m2869_MI,
	&m9277_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m2856_MI;
extern MethodInfo m2857_MI;
extern MethodInfo m2858_MI;
extern MethodInfo m1684_MI;
extern MethodInfo m2859_MI;
extern MethodInfo m2860_MI;
extern MethodInfo m2861_MI;
static MethodInfo* t595_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
extern TypeInfo t374_TI;
extern TypeInfo t590_TI;
static Il2CppInterfaceOffsetPair t595_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t595_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t595__CustomAttributeCache = {
1,
NULL,
&t595_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t595_0_0_0;
extern Il2CppType t595_1_0_0;
extern TypeInfo t956_TI;
struct t595;
extern CustomAttributesCache t595__CustomAttributeCache;
TypeInfo t595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IndexOutOfRangeException", "System", t595_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t595_TI, NULL, t595_VT, &t595__CustomAttributeCache, &t595_TI, &t595_0_0_0, &t595_1_0_0, t595_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t595), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1649.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1649_TI;
#include "t1649MD.h"

#include "t44.h"
#include "t295MD.h"
extern MethodInfo m2946_MI;


extern MethodInfo m9278_MI;
 void m9278 (t1649 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2182, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2147467262), &m2946_MI);
		return;
	}
}
extern MethodInfo m9279_MI;
 void m9279 (t1649 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2147467262), &m2946_MI);
		return;
	}
}
extern MethodInfo m9280_MI;
 void m9280 (t1649 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.InvalidCastException
extern Il2CppType t44_0_0_32849;
FieldInfo t1649_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t1649_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1649_FIs[] =
{
	&t1649_f11_FieldInfo,
	NULL
};
static const int32_t t1649_f11_DefaultValueData = -2147467262;
extern Il2CppType t44_0_0_0;
static Il2CppFieldDefaultValueEntry t1649_f11_DefaultValue = 
{
	&t1649_f11_FieldInfo, { (char*)&t1649_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1649_FDVs[] = 
{
	&t1649_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9278_MI = 
{
	".ctor", (methodPointerType)&m9278, &t1649_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5105, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1649_m9279_ParameterInfos[] = 
{
	{"message", 0, 134224088, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9279_MI = 
{
	".ctor", (methodPointerType)&m9279, &t1649_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1649_m9279_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5106, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1649_m9280_ParameterInfos[] = 
{
	{"info", 0, 134224089, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224090, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9280_MI = 
{
	".ctor", (methodPointerType)&m9280, &t1649_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1649_m9280_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5107, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1649_MIs[] =
{
	&m9278_MI,
	&m9279_MI,
	&m9280_MI,
	NULL
};
static MethodInfo* t1649_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1649_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1649_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1649__CustomAttributeCache = {
1,
NULL,
&t1649_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1649_0_0_0;
extern Il2CppType t1649_1_0_0;
struct t1649;
extern CustomAttributesCache t1649__CustomAttributeCache;
TypeInfo t1649_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InvalidCastException", "System", t1649_MIs, NULL, t1649_FIs, NULL, &t956_TI, NULL, NULL, &t1649_TI, NULL, t1649_VT, &t1649__CustomAttributeCache, &t1649_TI, &t1649_0_0_0, &t1649_1_0_0, t1649_IOs, NULL, NULL, t1649_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1649), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 11, 0, 2};
#include "t914.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t914_TI;
#include "t914MD.h"

#include "t295.h"
extern MethodInfo m9518_MI;


extern MethodInfo m3974_MI;
 void m3974 (t914 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2183, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233079), &m2946_MI);
		return;
	}
}
extern MethodInfo m3964_MI;
 void m3964 (t914 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233079), &m2946_MI);
		return;
	}
}
extern MethodInfo m9281_MI;
 void m9281 (t914 * __this, t7* p0, t295 * p1, MethodInfo* method){
	{
		m9518(__this, p0, p1, &m9518_MI);
		m2946(__this, ((int32_t)-2146233079), &m2946_MI);
		return;
	}
}
extern MethodInfo m9282_MI;
 void m9282 (t914 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.InvalidOperationException
extern Il2CppType t44_0_0_32849;
FieldInfo t914_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t914_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t914_FIs[] =
{
	&t914_f11_FieldInfo,
	NULL
};
static const int32_t t914_f11_DefaultValueData = -2146233079;
static Il2CppFieldDefaultValueEntry t914_f11_DefaultValue = 
{
	&t914_f11_FieldInfo, { (char*)&t914_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t914_FDVs[] = 
{
	&t914_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3974_MI = 
{
	".ctor", (methodPointerType)&m3974, &t914_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5108, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t914_m3964_ParameterInfos[] = 
{
	{"message", 0, 134224091, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3964_MI = 
{
	".ctor", (methodPointerType)&m3964, &t914_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t914_m3964_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5109, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t295_0_0_0;
extern Il2CppType t295_0_0_0;
static ParameterInfo t914_m9281_ParameterInfos[] = 
{
	{"message", 0, 134224092, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"innerException", 1, 134224093, &EmptyCustomAttributesCache, &t295_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9281_MI = 
{
	".ctor", (methodPointerType)&m9281, &t914_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t914_m9281_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5110, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t914_m9282_ParameterInfos[] = 
{
	{"info", 0, 134224094, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224095, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9282_MI = 
{
	".ctor", (methodPointerType)&m9282, &t914_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t914_m9282_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5111, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t914_MIs[] =
{
	&m3974_MI,
	&m3964_MI,
	&m9281_MI,
	&m9282_MI,
	NULL
};
static MethodInfo* t914_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t914_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t914_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t914__CustomAttributeCache = {
1,
NULL,
&t914_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t914_0_0_0;
extern Il2CppType t914_1_0_0;
struct t914;
extern CustomAttributesCache t914__CustomAttributeCache;
TypeInfo t914_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InvalidOperationException", "System", t914_MIs, NULL, t914_FIs, NULL, &t956_TI, NULL, NULL, &t914_TI, NULL, t914_VT, &t914__CustomAttributeCache, &t914_TI, &t914_0_0_0, &t914_1_0_0, t914_IOs, NULL, NULL, t914_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t914), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 11, 0, 2};
#include "t1620.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1620_TI;
#include "t1620MD.h"



// Metadata Definition System.LoaderOptimization
extern Il2CppType t44_0_0_1542;
FieldInfo t1620_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1620_TI, offsetof(t1620, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1620_0_0_32854;
FieldInfo t1620_f2_FieldInfo = 
{
	"NotSpecified", &t1620_0_0_32854, &t1620_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1620_0_0_32854;
FieldInfo t1620_f3_FieldInfo = 
{
	"SingleDomain", &t1620_0_0_32854, &t1620_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1620_0_0_32854;
FieldInfo t1620_f4_FieldInfo = 
{
	"MultiDomain", &t1620_0_0_32854, &t1620_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1620_0_0_32854;
FieldInfo t1620_f5_FieldInfo = 
{
	"MultiDomainHost", &t1620_0_0_32854, &t1620_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1620_0_0_32854;
extern CustomAttributesCache t1620__CustomAttributeCache_DomainMask;
FieldInfo t1620_f6_FieldInfo = 
{
	"DomainMask", &t1620_0_0_32854, &t1620_TI, 0 /*field is const -> no data*/, &t1620__CustomAttributeCache_DomainMask};
extern Il2CppType t1620_0_0_32854;
extern CustomAttributesCache t1620__CustomAttributeCache_DisallowBindings;
FieldInfo t1620_f7_FieldInfo = 
{
	"DisallowBindings", &t1620_0_0_32854, &t1620_TI, 0 /*field is const -> no data*/, &t1620__CustomAttributeCache_DisallowBindings};
static FieldInfo* t1620_FIs[] =
{
	&t1620_f1_FieldInfo,
	&t1620_f2_FieldInfo,
	&t1620_f3_FieldInfo,
	&t1620_f4_FieldInfo,
	&t1620_f5_FieldInfo,
	&t1620_f6_FieldInfo,
	&t1620_f7_FieldInfo,
	NULL
};
static const int32_t t1620_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1620_f2_DefaultValue = 
{
	&t1620_f2_FieldInfo, { (char*)&t1620_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1620_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1620_f3_DefaultValue = 
{
	&t1620_f3_FieldInfo, { (char*)&t1620_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1620_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1620_f4_DefaultValue = 
{
	&t1620_f4_FieldInfo, { (char*)&t1620_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1620_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1620_f5_DefaultValue = 
{
	&t1620_f5_FieldInfo, { (char*)&t1620_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1620_f6_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1620_f6_DefaultValue = 
{
	&t1620_f6_FieldInfo, { (char*)&t1620_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1620_f7_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1620_f7_DefaultValue = 
{
	&t1620_f7_FieldInfo, { (char*)&t1620_f7_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1620_FDVs[] = 
{
	&t1620_f2_DefaultValue,
	&t1620_f3_DefaultValue,
	&t1620_f4_DefaultValue,
	&t1620_f5_DefaultValue,
	&t1620_f6_DefaultValue,
	&t1620_f7_DefaultValue,
	NULL
};
static MethodInfo* t1620_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t1620_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t1620_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1620_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t327_TI;
#include "t327.h"
#include "t327MD.h"
extern MethodInfo m4287_MI;
void t1620_CustomAttributesCacheGenerator_DomainMask(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m4287(tmp, &m4287_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1620_CustomAttributesCacheGenerator_DisallowBindings(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m4287(tmp, &m4287_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1620__CustomAttributeCache = {
1,
NULL,
&t1620_CustomAttributesCacheGenerator
};
CustomAttributesCache t1620__CustomAttributeCache_DomainMask = {
1,
NULL,
&t1620_CustomAttributesCacheGenerator_DomainMask
};
CustomAttributesCache t1620__CustomAttributeCache_DisallowBindings = {
1,
NULL,
&t1620_CustomAttributesCacheGenerator_DisallowBindings
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1620_0_0_0;
extern Il2CppType t1620_1_0_0;
extern TypeInfo t49_TI;
extern TypeInfo t44_TI;
extern CustomAttributesCache t1620__CustomAttributeCache;
extern CustomAttributesCache t1620__CustomAttributeCache_DomainMask;
extern CustomAttributesCache t1620__CustomAttributeCache_DisallowBindings;
TypeInfo t1620_TI = 
{
	&g_mscorlib_dll_Image, NULL, "LoaderOptimization", "System", t1620_MIs, NULL, t1620_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1620_VT, &t1620__CustomAttributeCache, &t44_TI, &t1620_0_0_0, &t1620_1_0_0, t1620_IOs, NULL, NULL, t1620_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1620)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 7, 0, 0, 23, 0, 3};
#include "t602.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t602_TI;
#include "t602MD.h"

#include "t22.h"
#include "t1666.h"
#include "t919.h"
#include "t601.h"
#include "t1126.h"
#include "t40.h"
#include "t348.h"
extern TypeInfo t1666_TI;
extern TypeInfo t1126_TI;
#include "t1666MD.h"
#include "t1126MD.h"
extern MethodInfo m9492_MI;
extern MethodInfo m2878_MI;
extern MethodInfo m9287_MI;
extern MethodInfo m5728_MI;
extern MethodInfo m5755_MI;
extern MethodInfo m5696_MI;
extern MethodInfo m5779_MI;
extern MethodInfo m5757_MI;
extern MethodInfo m5756_MI;
extern MethodInfo m5697_MI;
extern MethodInfo m5778_MI;
extern MethodInfo m5780_MI;
extern MethodInfo m5754_MI;


extern MethodInfo m2875_MI;
 float m2875 (t29 * __this, float p0, MethodInfo* method){
	float G_B3_0 = 0.0f;
	{
		if ((((float)p0) >= ((float)(0.0f))))
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = ((-p0));
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = p0;
	}

IL_000d:
	{
		return G_B3_0;
	}
}
extern MethodInfo m9283_MI;
 int32_t m9283 (t29 * __this, int32_t p0, MethodInfo* method){
	int32_t G_B5_0 = 0;
	{
		if ((((uint32_t)p0) != ((uint32_t)((int32_t)-2147483648))))
		{
			goto IL_0018;
		}
	}
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2184, &m6079_MI);
		t1666 * L_1 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_1, L_0, &m9492_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		G_B5_0 = ((-p0));
		goto IL_0021;
	}

IL_0020:
	{
		G_B5_0 = p0;
	}

IL_0021:
	{
		return G_B5_0;
	}
}
extern MethodInfo m9284_MI;
 int64_t m9284 (t29 * __this, int64_t p0, MethodInfo* method){
	int64_t G_B5_0 = 0;
	{
		if ((((uint64_t)p0) != ((uint64_t)((int64_t)std::numeric_limits<int64_t>::min()))))
		{
			goto IL_001c;
		}
	}
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2184, &m6079_MI);
		t1666 * L_1 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_1, L_0, &m9492_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001c:
	{
		if ((((int64_t)p0) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_0025;
		}
	}
	{
		G_B5_0 = ((-p0));
		goto IL_0026;
	}

IL_0025:
	{
		G_B5_0 = p0;
	}

IL_0026:
	{
		return G_B5_0;
	}
}
extern MethodInfo m2880_MI;
 double m2880 (t29 * __this, double p0, MethodInfo* method){
	double V_0 = 0.0;
	{
		double L_0 = floor(p0);
		V_0 = L_0;
		if ((((double)V_0) == ((double)p0)))
		{
			goto IL_0017;
		}
	}
	{
		V_0 = ((double)(V_0+(1.0)));
	}

IL_0017:
	{
		return V_0;
	}
}
 double m2878 (t29 * __this, double p0, MethodInfo* method){
	typedef double (*m2878_ftn) (double);
	static m2878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2878_ftn)il2cpp_codegen_resolve_icall ("System.Math::Floor(System.Double)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m2877_MI;
 double m2877 (t29 * __this, double p0, double p1, MethodInfo* method){
	double V_0 = 0.0;
	double G_B3_0 = 0.0;
	{
		double L_0 = log(p0);
		double L_1 = log(p1);
		V_0 = ((double)((double)L_0/(double)L_1));
		if ((((double)V_0) != ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		G_B3_0 = (0.0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = V_0;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
extern MethodInfo m5139_MI;
 int32_t m5139 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		if ((((int32_t)p0) <= ((int32_t)p1)))
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = p0;
		goto IL_0008;
	}

IL_0007:
	{
		G_B3_0 = p1;
	}

IL_0008:
	{
		return G_B3_0;
	}
}
extern MethodInfo m9285_MI;
 int32_t m9285 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		if ((((int32_t)p0) >= ((int32_t)p1)))
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = p0;
		goto IL_0008;
	}

IL_0007:
	{
		G_B3_0 = p1;
	}

IL_0008:
	{
		return G_B3_0;
	}
}
extern MethodInfo m9286_MI;
 t1126  m9286 (t29 * __this, t1126  p0, MethodInfo* method){
	t1126  V_0 = {0};
	t1126  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1126_TI));
		t1126  L_0 = m5728(NULL, p0, &m5728_MI);
		V_0 = L_0;
		t1126  L_1 = m5755(NULL, p0, V_0, &m5755_MI);
		V_1 = L_1;
		t1126  L_2 = {0};
		m5696(&L_2, 5, 0, 0, 0, 1, &m5696_MI);
		bool L_3 = m5779(NULL, V_1, L_2, &m5779_MI);
		if (!L_3)
		{
			goto IL_006a;
		}
	}
	{
		t1126  L_4 = {0};
		m5696(&L_4, ((int32_t)20), 0, 0, 0, 1, &m5696_MI);
		t1126  L_5 = {0};
		m5696(&L_5, ((int32_t)20), 0, 0, 0, 1, &m5696_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1126_TI));
		t1126  L_6 = m5757(NULL, V_0, L_5, &m5757_MI);
		t1126  L_7 = {0};
		m5696(&L_7, ((int32_t)20), 0, 0, 0, 1, &m5696_MI);
		t1126  L_8 = m5757(NULL, V_0, L_7, &m5757_MI);
		t1126  L_9 = m5728(NULL, L_8, &m5728_MI);
		t1126  L_10 = m5755(NULL, L_6, L_9, &m5755_MI);
		t1126  L_11 = m5756(NULL, L_4, L_10, &m5756_MI);
		t1126  L_12 = {0};
		m5697(&L_12, 0, &m5697_MI);
		bool L_13 = m5778(NULL, L_11, L_12, &m5778_MI);
		if (L_13)
		{
			goto IL_007c;
		}
	}

IL_006a:
	{
		t1126  L_14 = {0};
		m5696(&L_14, 5, 0, 0, 0, 1, &m5696_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1126_TI));
		bool L_15 = m5780(NULL, V_1, L_14, &m5780_MI);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1126_TI));
		t1126  L_16 = m5754(NULL, V_0, &m5754_MI);
		V_0 = L_16;
	}

IL_0083:
	{
		return V_0;
	}
}
extern MethodInfo m2879_MI;
 double m2879 (t29 * __this, double p0, MethodInfo* method){
	typedef double (*m2879_ftn) (double);
	static m2879_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2879_ftn)il2cpp_codegen_resolve_icall ("System.Math::Round(System.Double)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m2872_MI;
 double m2872 (t29 * __this, double p0, MethodInfo* method){
	typedef double (*m2872_ftn) (double);
	static m2872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2872_ftn)il2cpp_codegen_resolve_icall ("System.Math::Sin(System.Double)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m2873_MI;
 double m2873 (t29 * __this, double p0, MethodInfo* method){
	typedef double (*m2873_ftn) (double);
	static m2873_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2873_ftn)il2cpp_codegen_resolve_icall ("System.Math::Cos(System.Double)");
	return _il2cpp_icall_func(p0);
}
 double m9287 (t29 * __this, double p0, MethodInfo* method){
	typedef double (*m9287_ftn) (double);
	static m9287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9287_ftn)il2cpp_codegen_resolve_icall ("System.Math::Log(System.Double)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m2876_MI;
 double m2876 (t29 * __this, double p0, double p1, MethodInfo* method){
	typedef double (*m2876_ftn) (double, double);
	static m2876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2876_ftn)il2cpp_codegen_resolve_icall ("System.Math::Pow(System.Double,System.Double)");
	return _il2cpp_icall_func(p0, p1);
}
extern MethodInfo m2874_MI;
 double m2874 (t29 * __this, double p0, MethodInfo* method){
	typedef double (*m2874_ftn) (double);
	static m2874_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m2874_ftn)il2cpp_codegen_resolve_icall ("System.Math::Sqrt(System.Double)");
	return _il2cpp_icall_func(p0);
}
// Metadata Definition System.Math
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t602_m2875_ParameterInfos[] = 
{
	{"value", 0, 134224096, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m2875_MI = 
{
	"Abs", (methodPointerType)&m2875, &t602_TI, &t22_0_0_0, RuntimeInvoker_t22_t22, t602_m2875_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 5112, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t602_m9283_ParameterInfos[] = 
{
	{"value", 0, 134224097, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9283_MI = 
{
	"Abs", (methodPointerType)&m9283, &t602_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t602_m9283_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 5113, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t602_m9284_ParameterInfos[] = 
{
	{"value", 0, 134224098, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9284_MI = 
{
	"Abs", (methodPointerType)&m9284, &t602_TI, &t919_0_0_0, RuntimeInvoker_t919_t919, t602_m9284_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 5114, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2880_ParameterInfos[] = 
{
	{"a", 0, 134224099, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2880_MI = 
{
	"Ceiling", (methodPointerType)&m2880, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m2880_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 5115, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2878_ParameterInfos[] = 
{
	{"d", 0, 134224100, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2878_MI = 
{
	"Floor", (methodPointerType)&m2878, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m2878_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 1, false, false, 5116, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2877_ParameterInfos[] = 
{
	{"a", 0, 134224101, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"newBase", 1, 134224102, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2877_MI = 
{
	"Log", (methodPointerType)&m2877, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601_t601, t602_m2877_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5117, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t602_m5139_ParameterInfos[] = 
{
	{"val1", 0, 134224103, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"val2", 1, 134224104, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t602__CustomAttributeCache_m5139;
MethodInfo m5139_MI = 
{
	"Max", (methodPointerType)&m5139, &t602_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44, t602_m5139_ParameterInfos, &t602__CustomAttributeCache_m5139, 150, 0, 255, 2, false, false, 5118, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t602_m9285_ParameterInfos[] = 
{
	{"val1", 0, 134224105, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"val2", 1, 134224106, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t602__CustomAttributeCache_m9285;
MethodInfo m9285_MI = 
{
	"Min", (methodPointerType)&m9285, &t602_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44, t602_m9285_ParameterInfos, &t602__CustomAttributeCache_m9285, 150, 0, 255, 2, false, false, 5119, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1126_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t602_m9286_ParameterInfos[] = 
{
	{"d", 0, 134224107, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t1126_0_0_0;
extern void* RuntimeInvoker_t1126_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m9286_MI = 
{
	"Round", (methodPointerType)&m9286, &t602_TI, &t1126_0_0_0, RuntimeInvoker_t1126_t1126, t602_m9286_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 5120, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2879_ParameterInfos[] = 
{
	{"a", 0, 134224108, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2879_MI = 
{
	"Round", (methodPointerType)&m2879, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m2879_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 1, false, false, 5121, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2872_ParameterInfos[] = 
{
	{"a", 0, 134224109, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2872_MI = 
{
	"Sin", (methodPointerType)&m2872, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m2872_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 1, false, false, 5122, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2873_ParameterInfos[] = 
{
	{"d", 0, 134224110, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2873_MI = 
{
	"Cos", (methodPointerType)&m2873, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m2873_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 1, false, false, 5123, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m9287_ParameterInfos[] = 
{
	{"d", 0, 134224111, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9287_MI = 
{
	"Log", (methodPointerType)&m9287, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m9287_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 1, false, false, 5124, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2876_ParameterInfos[] = 
{
	{"x", 0, 134224112, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"y", 1, 134224113, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m2876_MI = 
{
	"Pow", (methodPointerType)&m2876, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601_t601, t602_m2876_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 2, false, false, 5125, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t602_m2874_ParameterInfos[] = 
{
	{"d", 0, 134224114, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t602__CustomAttributeCache_m2874;
MethodInfo m2874_MI = 
{
	"Sqrt", (methodPointerType)&m2874, &t602_TI, &t601_0_0_0, RuntimeInvoker_t601_t601, t602_m2874_ParameterInfos, &t602__CustomAttributeCache_m2874, 150, 4096, 255, 1, false, false, 5126, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t602_MIs[] =
{
	&m2875_MI,
	&m9283_MI,
	&m9284_MI,
	&m2880_MI,
	&m2878_MI,
	&m2877_MI,
	&m5139_MI,
	&m9285_MI,
	&m9286_MI,
	&m2879_MI,
	&m2872_MI,
	&m2873_MI,
	&m9287_MI,
	&m2876_MI,
	&m2874_MI,
	NULL
};
extern MethodInfo m1332_MI;
static MethodInfo* t602_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t1400_TI;
#include "t1400.h"
#include "t1400MD.h"
extern MethodInfo m7732_MI;
void t602_CustomAttributesCacheGenerator_m5139(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t602_CustomAttributesCacheGenerator_m9285(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t602_CustomAttributesCacheGenerator_m2874(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t602__CustomAttributeCache_m5139 = {
1,
NULL,
&t602_CustomAttributesCacheGenerator_m5139
};
CustomAttributesCache t602__CustomAttributeCache_m9285 = {
1,
NULL,
&t602_CustomAttributesCacheGenerator_m9285
};
CustomAttributesCache t602__CustomAttributeCache_m2874 = {
1,
NULL,
&t602_CustomAttributesCacheGenerator_m2874
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t602_0_0_0;
extern Il2CppType t602_1_0_0;
extern TypeInfo t29_TI;
struct t602;
extern CustomAttributesCache t602__CustomAttributeCache_m5139;
extern CustomAttributesCache t602__CustomAttributeCache_m9285;
extern CustomAttributesCache t602__CustomAttributeCache_m2874;
TypeInfo t602_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Math", "System", t602_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t602_TI, NULL, t602_VT, &EmptyCustomAttributesCache, &t602_TI, &t602_0_0_0, &t602_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t602), 0, -1, 0, 0, -1, 1048961, 0, false, false, false, false, false, false, false, false, false, false, false, false, 15, 0, 0, 0, 0, 4, 0, 0};
#include "t1647.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1647_TI;
#include "t1647MD.h"



extern MethodInfo m9288_MI;
 void m9288 (t1647 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2185, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233062), &m2946_MI);
		return;
	}
}
extern MethodInfo m9289_MI;
 void m9289 (t1647 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233062), &m2946_MI);
		return;
	}
}
extern MethodInfo m9290_MI;
 void m9290 (t1647 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.MemberAccessException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9288_MI = 
{
	".ctor", (methodPointerType)&m9288, &t1647_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5127, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1647_m9289_ParameterInfos[] = 
{
	{"message", 0, 134224115, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9289_MI = 
{
	".ctor", (methodPointerType)&m9289, &t1647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1647_m9289_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5128, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1647_m9290_ParameterInfos[] = 
{
	{"info", 0, 134224116, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224117, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9290_MI = 
{
	".ctor", (methodPointerType)&m9290, &t1647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1647_m9290_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5129, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1647_MIs[] =
{
	&m9288_MI,
	&m9289_MI,
	&m9290_MI,
	NULL
};
static MethodInfo* t1647_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1647_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1647__CustomAttributeCache = {
1,
NULL,
&t1647_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1647_0_0_0;
extern Il2CppType t1647_1_0_0;
struct t1647;
extern CustomAttributesCache t1647__CustomAttributeCache;
TypeInfo t1647_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MemberAccessException", "System", t1647_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t1647_TI, NULL, t1647_VT, &t1647__CustomAttributeCache, &t1647_TI, &t1647_0_0_0, &t1647_1_0_0, t1647_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1647), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1650.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1650_TI;
#include "t1650MD.h"



extern MethodInfo m9291_MI;
 void m9291 (t1650 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2186, &m6079_MI);
		m9289(__this, L_0, &m9289_MI);
		m2946(__this, ((int32_t)-2146233072), &m2946_MI);
		return;
	}
}
extern MethodInfo m9292_MI;
 void m9292 (t1650 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9290(__this, p0, p1, &m9290_MI);
		return;
	}
}
// Metadata Definition System.MethodAccessException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9291_MI = 
{
	".ctor", (methodPointerType)&m9291, &t1650_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5130, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1650_m9292_ParameterInfos[] = 
{
	{"info", 0, 134224118, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224119, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9292_MI = 
{
	".ctor", (methodPointerType)&m9292, &t1650_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1650_m9292_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5131, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1650_MIs[] =
{
	&m9291_MI,
	&m9292_MI,
	NULL
};
static MethodInfo* t1650_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1650_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1650_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1650__CustomAttributeCache = {
1,
NULL,
&t1650_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1650_0_0_0;
extern Il2CppType t1650_1_0_0;
struct t1650;
extern CustomAttributesCache t1650__CustomAttributeCache;
TypeInfo t1650_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodAccessException", "System", t1650_MIs, NULL, NULL, NULL, &t1647_TI, NULL, NULL, &t1650_TI, NULL, t1650_VT, &t1650__CustomAttributeCache, &t1650_TI, &t1650_0_0_0, &t1650_1_0_0, t1650_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1650), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 11, 0, 2};
#include "t1651.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1651_TI;
#include "t1651MD.h"

#include "t1652.h"
extern TypeInfo t1652_TI;
extern TypeInfo t7_TI;
#include "t1652MD.h"
#include "t7MD.h"
extern MethodInfo m9302_MI;
extern MethodInfo m5610_MI;
extern MethodInfo m9298_MI;
extern MethodInfo m9299_MI;


extern MethodInfo m9293_MI;
 void m9293 (t1651 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2188, &m6079_MI);
		m9298(__this, L_0, &m9298_MI);
		m2946(__this, ((int32_t)-2146233071), &m2946_MI);
		return;
	}
}
extern MethodInfo m9294_MI;
 void m9294 (t1651 * __this, t7* p0, MethodInfo* method){
	{
		m9298(__this, p0, &m9298_MI);
		m2946(__this, ((int32_t)-2146233071), &m2946_MI);
		return;
	}
}
extern MethodInfo m9295_MI;
 void m9295 (t1651 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9299(__this, p0, p1, &m9299_MI);
		return;
	}
}
extern MethodInfo m9296_MI;
 t7* m9296 (t1651 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		t7* L_0 = (__this->f11);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t7* L_1 = m9302(__this, &m9302_MI);
		return L_1;
	}

IL_000f:
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral2187, &m6079_MI);
		V_0 = L_2;
		t7* L_3 = (__this->f11);
		t7* L_4 = (__this->f12);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = m5610(NULL, V_0, L_3, L_4, &m5610_MI);
		return L_5;
	}
}
// Metadata Definition System.MissingFieldException
static PropertyInfo t1651____Message_PropertyInfo = 
{
	&t1651_TI, "Message", &m9296_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1651_PIs[] =
{
	&t1651____Message_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9293_MI = 
{
	".ctor", (methodPointerType)&m9293, &t1651_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5132, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1651_m9294_ParameterInfos[] = 
{
	{"message", 0, 134224120, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9294_MI = 
{
	".ctor", (methodPointerType)&m9294, &t1651_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1651_m9294_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5133, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1651_m9295_ParameterInfos[] = 
{
	{"info", 0, 134224121, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224122, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9295_MI = 
{
	".ctor", (methodPointerType)&m9295, &t1651_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1651_m9295_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5134, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9296_MI = 
{
	"get_Message", (methodPointerType)&m9296, &t1651_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 5135, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1651_MIs[] =
{
	&m9293_MI,
	&m9294_MI,
	&m9295_MI,
	&m9296_MI,
	NULL
};
extern MethodInfo m9301_MI;
static MethodInfo* t1651_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m9301_MI,
	&m2858_MI,
	&m9296_MI,
	&m2859_MI,
	&m2860_MI,
	&m9301_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1651_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1651_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1651__CustomAttributeCache = {
1,
NULL,
&t1651_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1651_0_0_0;
extern Il2CppType t1651_1_0_0;
struct t1651;
extern CustomAttributesCache t1651__CustomAttributeCache;
TypeInfo t1651_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MissingFieldException", "System", t1651_MIs, t1651_PIs, NULL, NULL, &t1652_TI, NULL, NULL, &t1651_TI, NULL, t1651_VT, &t1651__CustomAttributeCache, &t1651_TI, &t1651_0_0_0, &t1651_1_0_0, t1651_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1651), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 0, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "mscorlib_ArrayTypes.h"
#include "t43.h"
extern TypeInfo t295_TI;
extern TypeInfo t781_TI;
extern TypeInfo t348_TI;
extern TypeInfo t42_TI;
extern TypeInfo t21_TI;
extern TypeInfo t733_TI;
extern TypeInfo t735_TI;
#include "t733MD.h"
#include "t42MD.h"
extern Il2CppType t781_0_0_0;
extern MethodInfo m3994_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m3997_MI;


extern MethodInfo m9297_MI;
 void m9297 (t1652 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2190, &m6079_MI);
		m9289(__this, L_0, &m9289_MI);
		m2946(__this, ((int32_t)-2146233070), &m2946_MI);
		return;
	}
}
 void m9298 (t1652 * __this, t7* p0, MethodInfo* method){
	{
		m9289(__this, p0, &m9289_MI);
		m2946(__this, ((int32_t)-2146233070), &m2946_MI);
		return;
	}
}
 void m9299 (t1652 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9290(__this, p0, p1, &m9290_MI);
		t7* L_0 = m3994(p0, (t7*) &_stringLiteral2191, &m3994_MI);
		__this->f11 = L_0;
		t7* L_1 = m3994(p0, (t7*) &_stringLiteral2192, &m3994_MI);
		__this->f12 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t781_0_0_0), &m1554_MI);
		t29 * L_3 = m3985(p0, (t7*) &_stringLiteral2193, L_2, &m3985_MI);
		__this->f13 = ((t781*)Castclass(L_3, InitializedTypeInfo(&t781_TI)));
		return;
	}
}
extern MethodInfo m9300_MI;
 void m9300 (t1652 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		m9288(__this, &m9288_MI);
		__this->f11 = p0;
		__this->f12 = p1;
		m2946(__this, ((int32_t)-2146233070), &m2946_MI);
		return;
	}
}
 void m9301 (t1652 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m2857(__this, p0, p1, &m2857_MI);
		t7* L_0 = (__this->f11);
		m3997(p0, (t7*) &_stringLiteral2191, L_0, &m3997_MI);
		t7* L_1 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral2192, L_1, &m3997_MI);
		t781* L_2 = (__this->f13);
		m3997(p0, (t7*) &_stringLiteral2193, (t29 *)(t29 *)L_2, &m3997_MI);
		return;
	}
}
 t7* m9302 (t1652 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		t7* L_0 = (__this->f11);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t7* L_1 = m1684(__this, &m1684_MI);
		return L_1;
	}

IL_000f:
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral2189, &m6079_MI);
		V_0 = L_2;
		t7* L_3 = (__this->f11);
		t7* L_4 = (__this->f12);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = m5610(NULL, V_0, L_3, L_4, &m5610_MI);
		return L_5;
	}
}
// Metadata Definition System.MissingMemberException
extern Il2CppType t7_0_0_4;
FieldInfo t1652_f11_FieldInfo = 
{
	"ClassName", &t7_0_0_4, &t1652_TI, offsetof(t1652, f11), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_4;
FieldInfo t1652_f12_FieldInfo = 
{
	"MemberName", &t7_0_0_4, &t1652_TI, offsetof(t1652, f12), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_4;
FieldInfo t1652_f13_FieldInfo = 
{
	"Signature", &t781_0_0_4, &t1652_TI, offsetof(t1652, f13), &EmptyCustomAttributesCache};
static FieldInfo* t1652_FIs[] =
{
	&t1652_f11_FieldInfo,
	&t1652_f12_FieldInfo,
	&t1652_f13_FieldInfo,
	NULL
};
static PropertyInfo t1652____Message_PropertyInfo = 
{
	&t1652_TI, "Message", &m9302_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1652_PIs[] =
{
	&t1652____Message_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9297_MI = 
{
	".ctor", (methodPointerType)&m9297, &t1652_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5136, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1652_m9298_ParameterInfos[] = 
{
	{"message", 0, 134224123, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9298_MI = 
{
	".ctor", (methodPointerType)&m9298, &t1652_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1652_m9298_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5137, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1652_m9299_ParameterInfos[] = 
{
	{"info", 0, 134224124, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224125, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9299_MI = 
{
	".ctor", (methodPointerType)&m9299, &t1652_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1652_m9299_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5138, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1652_m9300_ParameterInfos[] = 
{
	{"className", 0, 134224126, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"memberName", 1, 134224127, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9300_MI = 
{
	".ctor", (methodPointerType)&m9300, &t1652_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1652_m9300_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5139, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1652_m9301_ParameterInfos[] = 
{
	{"info", 0, 134224128, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224129, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9301_MI = 
{
	"GetObjectData", (methodPointerType)&m9301, &t1652_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1652_m9301_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, false, 5140, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9302_MI = 
{
	"get_Message", (methodPointerType)&m9302, &t1652_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 5141, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1652_MIs[] =
{
	&m9297_MI,
	&m9298_MI,
	&m9299_MI,
	&m9300_MI,
	&m9301_MI,
	&m9302_MI,
	NULL
};
static MethodInfo* t1652_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m9301_MI,
	&m2858_MI,
	&m9302_MI,
	&m2859_MI,
	&m2860_MI,
	&m9301_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1652_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1652_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1652__CustomAttributeCache = {
1,
NULL,
&t1652_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1652_0_0_0;
extern Il2CppType t1652_1_0_0;
struct t1652;
extern CustomAttributesCache t1652__CustomAttributeCache;
TypeInfo t1652_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MissingMemberException", "System", t1652_MIs, t1652_PIs, t1652_FIs, NULL, &t1647_TI, NULL, NULL, &t1652_TI, NULL, t1652_VT, &t1652__CustomAttributeCache, &t1652_TI, &t1652_0_0_0, &t1652_1_0_0, t1652_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1652), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 1, 3, 0, 0, 11, 0, 2};
#include "t1653.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1653_TI;
#include "t1653MD.h"



extern MethodInfo m9303_MI;
 void m9303 (t1653 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2195, &m6079_MI);
		m9298(__this, L_0, &m9298_MI);
		m2946(__this, ((int32_t)-2146233069), &m2946_MI);
		return;
	}
}
extern MethodInfo m9304_MI;
 void m9304 (t1653 * __this, t7* p0, MethodInfo* method){
	{
		m9298(__this, p0, &m9298_MI);
		m2946(__this, ((int32_t)-2146233069), &m2946_MI);
		return;
	}
}
extern MethodInfo m9305_MI;
 void m9305 (t1653 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9299(__this, p0, p1, &m9299_MI);
		return;
	}
}
extern MethodInfo m9306_MI;
 void m9306 (t1653 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		m9300(__this, p0, p1, &m9300_MI);
		m2946(__this, ((int32_t)-2146233069), &m2946_MI);
		return;
	}
}
extern MethodInfo m9307_MI;
 t7* m9307 (t1653 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		t7* L_0 = (__this->f11);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t7* L_1 = m9302(__this, &m9302_MI);
		return L_1;
	}

IL_000f:
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral2194, &m6079_MI);
		V_0 = L_2;
		t7* L_3 = (__this->f11);
		t7* L_4 = (__this->f12);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = m5610(NULL, V_0, L_3, L_4, &m5610_MI);
		return L_5;
	}
}
// Metadata Definition System.MissingMethodException
extern Il2CppType t44_0_0_32849;
FieldInfo t1653_f14_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t1653_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1653_FIs[] =
{
	&t1653_f14_FieldInfo,
	NULL
};
static const int32_t t1653_f14_DefaultValueData = -2146233069;
static Il2CppFieldDefaultValueEntry t1653_f14_DefaultValue = 
{
	&t1653_f14_FieldInfo, { (char*)&t1653_f14_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1653_FDVs[] = 
{
	&t1653_f14_DefaultValue,
	NULL
};
static PropertyInfo t1653____Message_PropertyInfo = 
{
	&t1653_TI, "Message", &m9307_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1653_PIs[] =
{
	&t1653____Message_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9303_MI = 
{
	".ctor", (methodPointerType)&m9303, &t1653_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5142, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1653_m9304_ParameterInfos[] = 
{
	{"message", 0, 134224130, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9304_MI = 
{
	".ctor", (methodPointerType)&m9304, &t1653_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1653_m9304_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5143, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1653_m9305_ParameterInfos[] = 
{
	{"info", 0, 134224131, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224132, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9305_MI = 
{
	".ctor", (methodPointerType)&m9305, &t1653_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1653_m9305_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5144, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1653_m9306_ParameterInfos[] = 
{
	{"className", 0, 134224133, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"methodName", 1, 134224134, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9306_MI = 
{
	".ctor", (methodPointerType)&m9306, &t1653_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1653_m9306_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5145, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9307_MI = 
{
	"get_Message", (methodPointerType)&m9307, &t1653_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 5146, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1653_MIs[] =
{
	&m9303_MI,
	&m9304_MI,
	&m9305_MI,
	&m9306_MI,
	&m9307_MI,
	NULL
};
static MethodInfo* t1653_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m9301_MI,
	&m2858_MI,
	&m9307_MI,
	&m2859_MI,
	&m2860_MI,
	&m9301_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1653_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1653_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1653__CustomAttributeCache = {
1,
NULL,
&t1653_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1653_0_0_0;
extern Il2CppType t1653_1_0_0;
struct t1653;
extern CustomAttributesCache t1653__CustomAttributeCache;
TypeInfo t1653_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MissingMethodException", "System", t1653_MIs, t1653_PIs, t1653_FIs, NULL, &t1652_TI, NULL, NULL, &t1653_TI, NULL, t1653_VT, &t1653__CustomAttributeCache, &t1653_TI, &t1653_0_0_0, &t1653_1_0_0, t1653_IOs, NULL, NULL, t1653_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1653), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 1, 0, 0, 11, 0, 2};
#include "t1654.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1654_TI;
#include "t1654MD.h"

#include "t29MD.h"
extern MethodInfo m1331_MI;


extern MethodInfo m9308_MI;
 void m9308 (t1654 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.MonoAsyncCall
extern Il2CppType t29_0_0_1;
FieldInfo t1654_f0_FieldInfo = 
{
	"msg", &t29_0_0_1, &t1654_TI, offsetof(t1654, f0), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_1;
FieldInfo t1654_f1_FieldInfo = 
{
	"cb_method", &t35_0_0_1, &t1654_TI, offsetof(t1654, f1), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1654_f2_FieldInfo = 
{
	"cb_target", &t29_0_0_1, &t1654_TI, offsetof(t1654, f2), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1654_f3_FieldInfo = 
{
	"state", &t29_0_0_1, &t1654_TI, offsetof(t1654, f3), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1654_f4_FieldInfo = 
{
	"res", &t29_0_0_1, &t1654_TI, offsetof(t1654, f4), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1654_f5_FieldInfo = 
{
	"out_args", &t29_0_0_1, &t1654_TI, offsetof(t1654, f5), &EmptyCustomAttributesCache};
extern Il2CppType t919_0_0_1;
FieldInfo t1654_f6_FieldInfo = 
{
	"wait_event", &t919_0_0_1, &t1654_TI, offsetof(t1654, f6), &EmptyCustomAttributesCache};
static FieldInfo* t1654_FIs[] =
{
	&t1654_f0_FieldInfo,
	&t1654_f1_FieldInfo,
	&t1654_f2_FieldInfo,
	&t1654_f3_FieldInfo,
	&t1654_f4_FieldInfo,
	&t1654_f5_FieldInfo,
	&t1654_f6_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9308_MI = 
{
	".ctor", (methodPointerType)&m9308, &t1654_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5147, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1654_MIs[] =
{
	&m9308_MI,
	NULL
};
static MethodInfo* t1654_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1654_0_0_0;
extern Il2CppType t1654_1_0_0;
struct t1654;
TypeInfo t1654_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoAsyncCall", "System", t1654_MIs, NULL, t1654_FIs, NULL, &t29_TI, NULL, NULL, &t1654_TI, NULL, t1654_VT, &EmptyCustomAttributesCache, &t1654_TI, &t1654_0_0_0, &t1654_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1654), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 7, 0, 0, 4, 0, 0};
#include "t1655.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1655_TI;
#include "t1655MD.h"

#include "t629.h"


extern MethodInfo m9309_MI;
 void m9309 (t1655 * __this, t629 * p0, int32_t p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
extern MethodInfo m9310_MI;
 t629 * m9310 (t1655 * __this, MethodInfo* method){
	{
		t629 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m9311_MI;
 int32_t m9311 (t1655 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
// Metadata Definition System.MonoCustomAttrs/AttributeInfo
extern Il2CppType t629_0_0_1;
FieldInfo t1655_f0_FieldInfo = 
{
	"_usage", &t629_0_0_1, &t1655_TI, offsetof(t1655, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1655_f1_FieldInfo = 
{
	"_inheritanceLevel", &t44_0_0_1, &t1655_TI, offsetof(t1655, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1655_FIs[] =
{
	&t1655_f0_FieldInfo,
	&t1655_f1_FieldInfo,
	NULL
};
static PropertyInfo t1655____Usage_PropertyInfo = 
{
	&t1655_TI, "Usage", &m9310_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1655____InheritanceLevel_PropertyInfo = 
{
	&t1655_TI, "InheritanceLevel", &m9311_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1655_PIs[] =
{
	&t1655____Usage_PropertyInfo,
	&t1655____InheritanceLevel_PropertyInfo,
	NULL
};
extern Il2CppType t629_0_0_0;
extern Il2CppType t629_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1655_m9309_ParameterInfos[] = 
{
	{"usage", 0, 134224159, &EmptyCustomAttributesCache, &t629_0_0_0},
	{"inheritanceLevel", 1, 134224160, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9309_MI = 
{
	".ctor", (methodPointerType)&m9309, &t1655_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t1655_m9309_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5161, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t629_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9310_MI = 
{
	"get_Usage", (methodPointerType)&m9310, &t1655_TI, &t629_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5162, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9311_MI = 
{
	"get_InheritanceLevel", (methodPointerType)&m9311, &t1655_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5163, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1655_MIs[] =
{
	&m9309_MI,
	&m9310_MI,
	&m9311_MI,
	NULL
};
static MethodInfo* t1655_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1655_0_0_0;
extern Il2CppType t1655_1_0_0;
struct t1655;
extern TypeInfo t1656_TI;
TypeInfo t1655_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AttributeInfo", "", t1655_MIs, t1655_PIs, t1655_FIs, NULL, &t29_TI, NULL, &t1656_TI, &t1655_TI, NULL, t1655_VT, &EmptyCustomAttributesCache, &t1655_TI, &t1655_0_0_0, &t1655_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1655), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 2, 2, 0, 0, 4, 0, 0};
#include "t1656.h"
#ifndef _MSC_VER
#else
#endif
#include "t1656MD.h"

#include "t1129.h"
#include "t1660.h"
#include "t1340.h"
#include "t929.h"
#include "t1377.h"
#include "t1144.h"
#include "t637.h"
#include "t490.h"
#include "t1357.h"
#include "t719.h"
#include "t731.h"
#include "t338.h"
#include "t1146.h"
#include "t557.h"
#include "t636.h"
#include "t296.h"
#include "t1383.h"
#include "t901.h"
extern TypeInfo t629_TI;
extern TypeInfo t1660_TI;
extern TypeInfo t1340_TI;
extern TypeInfo t929_TI;
extern TypeInfo t1377_TI;
extern TypeInfo t1144_TI;
extern TypeInfo t637_TI;
extern TypeInfo t40_TI;
extern TypeInfo t316_TI;
extern TypeInfo t1657_TI;
extern TypeInfo t1357_TI;
extern TypeInfo t490_TI;
extern TypeInfo t338_TI;
extern TypeInfo t719_TI;
extern TypeInfo t731_TI;
extern TypeInfo t20_TI;
extern TypeInfo t1146_TI;
extern TypeInfo t557_TI;
extern TypeInfo t636_TI;
extern TypeInfo t638_TI;
extern TypeInfo t537_TI;
extern TypeInfo t296_TI;
extern TypeInfo t1383_TI;
extern TypeInfo t901_TI;
#include "t629MD.h"
#include "t1377MD.h"
#include "t1144MD.h"
#include "t637MD.h"
#include "t20MD.h"
#include "t1357MD.h"
#include "t338MD.h"
#include "t719MD.h"
#include "t731MD.h"
#include "t1146MD.h"
#include "t636MD.h"
#include "t557MD.h"
#include "t296MD.h"
#include "t901MD.h"
extern Il2CppType t1656_0_0_0;
extern Il2CppType t490_0_0_0;
extern MethodInfo m2915_MI;
extern MethodInfo m4034_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m7638_MI;
extern MethodInfo m7535_MI;
extern MethodInfo m7708_MI;
extern MethodInfo m6046_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m9313_MI;
extern MethodInfo m9857_MI;
extern MethodInfo m9314_MI;
extern MethodInfo m9315_MI;
extern MethodInfo m5951_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m9318_MI;
extern MethodInfo m7456_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m9316_MI;
extern MethodInfo m5936_MI;
extern MethodInfo m6002_MI;
extern MethodInfo m9324_MI;
extern MethodInfo m5330_MI;
extern MethodInfo m4215_MI;
extern MethodInfo m4181_MI;
extern MethodInfo m3989_MI;
extern MethodInfo m5329_MI;
extern MethodInfo m3991_MI;
extern MethodInfo m3990_MI;
extern MethodInfo m9323_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m3976_MI;
extern MethodInfo m4177_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m9858_MI;
extern MethodInfo m9321_MI;
extern MethodInfo m9320_MI;
extern MethodInfo m10176_MI;
extern MethodInfo m7549_MI;
extern MethodInfo m10177_MI;
extern MethodInfo m10158_MI;
extern MethodInfo m10143_MI;
extern MethodInfo m2940_MI;
extern MethodInfo m2937_MI;
extern MethodInfo m2924_MI;
extern MethodInfo m10144_MI;
extern MethodInfo m6027_MI;
extern MethodInfo m6026_MI;
extern MethodInfo m2908_MI;
extern MethodInfo m9322_MI;
extern MethodInfo m4003_MI;


extern MethodInfo m9312_MI;
 void m9312 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t629_0_0_0), &m1554_MI);
		((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f1 = L_0;
		t629 * L_1 = (t629 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t629_TI));
		m2915(L_1, ((int32_t)32767), &m2915_MI);
		((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f2 = L_1;
		return;
	}
}
 bool m9313 (t29 * __this, t29 * p0, MethodInfo* method){
	t42 * V_0 = {0};
	{
		V_0 = ((t42 *)IsInst(p0, InitializedTypeInfo(&t42_TI)));
		if (((t1660 *)IsInst(V_0, InitializedTypeInfo(&t1660_TI))))
		{
			goto IL_0017;
		}
	}
	{
		if (!((t1340 *)IsInst(V_0, InitializedTypeInfo(&t1340_TI))))
		{
			goto IL_0019;
		}
	}

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		if (!((t42 *)IsInst(p0, InitializedTypeInfo(&t42_TI))))
		{
			goto IL_0023;
		}
	}
	{
		return 1;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		if ((((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f0))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t929 * L_1 = (t929 *)VirtFuncInvoker0< t929 * >::Invoke(&m4034_MI, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f0 = L_1;
	}

IL_003e:
	{
		t42 * L_2 = m1430(p0, &m1430_MI);
		t929 * L_3 = (t929 *)VirtFuncInvoker0< t929 * >::Invoke(&m4034_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		return ((((int32_t)((((t929 *)L_3) == ((t929 *)(((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 t316* m9314 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method){
	typedef t316* (*m9314_ftn) (t29 *, t42 *, bool);
	static m9314_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9314_ftn)il2cpp_codegen_resolve_icall ("System.MonoCustomAttrs::GetCustomAttributesInternal(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)");
	return _il2cpp_icall_func(p0, p1, p2);
}
 t316* m9315 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method){
	t316* V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = (t316*)NULL;
		if (!((t1377 *)IsInst(p0, InitializedTypeInfo(&t1377_TI))))
		{
			goto IL_0018;
		}
	}
	{
		t316* L_0 = m7638(((t1377 *)Castclass(p0, InitializedTypeInfo(&t1377_TI))), &m7638_MI);
		V_0 = L_0;
		goto IL_0058;
	}

IL_0018:
	{
		if (!((t1144 *)IsInst(p0, InitializedTypeInfo(&t1144_TI))))
		{
			goto IL_002e;
		}
	}
	{
		t316* L_1 = m7535(((t1144 *)Castclass(p0, InitializedTypeInfo(&t1144_TI))), &m7535_MI);
		V_0 = L_1;
		goto IL_0058;
	}

IL_002e:
	{
		if (!((t637 *)IsInst(p0, InitializedTypeInfo(&t637_TI))))
		{
			goto IL_0044;
		}
	}
	{
		t316* L_2 = m7708(((t637 *)Castclass(p0, InitializedTypeInfo(&t637_TI))), &m7708_MI);
		V_0 = L_2;
		goto IL_0058;
	}

IL_0044:
	{
		if (!((t42 *)IsInst(p0, InitializedTypeInfo(&t42_TI))))
		{
			goto IL_0058;
		}
	}
	{
		t316* L_3 = m6046(((t42 *)Castclass(p0, InitializedTypeInfo(&t42_TI))), &m6046_MI);
		V_0 = L_3;
	}

IL_0058:
	{
		if (!p1)
		{
			goto IL_0098;
		}
	}
	{
		if (!V_0)
		{
			goto IL_0098;
		}
	}
	{
		V_1 = 0;
		goto IL_008b;
	}

IL_0062:
	{
		int32_t L_4 = V_1;
		t42 * L_5 = m1430((*(t29 **)(t29 **)SZArrayLdElema(V_0, L_4)), &m1430_MI);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, L_5);
		if (!L_6)
		{
			goto IL_0087;
		}
	}
	{
		if ((((uint32_t)(((int32_t)(((t20 *)V_0)->max_length)))) != ((uint32_t)1)))
		{
			goto IL_007a;
		}
	}
	{
		return V_0;
	}

IL_007a:
	{
		t316* L_7 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		int32_t L_8 = V_1;
		ArrayElementTypeCheck (L_7, (*(t29 **)(t29 **)SZArrayLdElema(V_0, L_8)));
		*((t29 **)(t29 **)SZArrayLdElema(L_7, 0)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema(V_0, L_8));
		return L_7;
	}

IL_0087:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_008b:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0062;
		}
	}
	{
		return ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
	}

IL_0098:
	{
		return V_0;
	}
}
 t316* m9316 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method){
	t316* V_0 = {0};
	t316* V_1 = {0};
	t316* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9313(NULL, p0, &m9313_MI);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		t316* L_1 = (t316*)InterfaceFuncInvoker2< t316*, t42 *, bool >::Invoke(&m9857_MI, p0, p1, 1);
		V_0 = L_1;
		goto IL_001c;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_2 = m9314(NULL, p0, p1, 0, &m9314_MI);
		V_0 = L_2;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_3 = m9315(NULL, p0, p1, &m9315_MI);
		V_1 = L_3;
		if (!V_1)
		{
			goto IL_004e;
		}
	}
	{
		V_2 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), ((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))+(((int32_t)(((t20 *)V_1)->max_length)))))));
		m5951(NULL, (t20 *)(t20 *)V_0, (t20 *)(t20 *)V_2, (((int32_t)(((t20 *)V_0)->max_length))), &m5951_MI);
		m5952(NULL, (t20 *)(t20 *)V_1, 0, (t20 *)(t20 *)V_2, (((int32_t)(((t20 *)V_0)->max_length))), (((int32_t)(((t20 *)V_1)->max_length))), &m5952_MI);
		return V_2;
	}

IL_004e:
	{
		return V_0;
	}
}
extern MethodInfo m9317_MI;
 t490 * m9317 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method){
	t316* V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, p0, p1, p2, &m9318_MI);
		V_0 = L_0;
		if ((((int32_t)(((t20 *)V_0)->max_length))))
		{
			goto IL_0010;
		}
	}
	{
		return (t490 *)NULL;
	}

IL_0010:
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_0)->max_length)))) <= ((int32_t)1)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (t7*) &_stringLiteral2196;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_1 = m5610(NULL, V_1, p0, p1, &m5610_MI);
		V_1 = L_1;
		t1357 * L_2 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7456(L_2, V_1, &m7456_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_002c:
	{
		int32_t L_3 = 0;
		return ((t490 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_0, L_3)), InitializedTypeInfo(&t490_TI)));
	}
}
 t316* m9318 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method){
	t316* V_0 = {0};
	t316* V_1 = {0};
	t629 * V_2 = {0};
	int32_t V_3 = 0;
	t719 * V_4 = {0};
	t731 * V_5 = {0};
	t29 * V_6 = {0};
	int32_t V_7 = 0;
	t29 * V_8 = {0};
	t316* V_9 = {0};
	int32_t V_10 = 0;
	t629 * V_11 = {0};
	t42 * V_12 = {0};
	t1655 * V_13 = {0};
	t316* V_14 = {0};
	int32_t G_B23_0 = 0;
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1199, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (p1)
		{
			goto IL_001c;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral863, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t1656_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_2)))
		{
			goto IL_002e;
		}
	}
	{
		p1 = (t42 *)NULL;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_3 = m9316(NULL, p0, p1, &m9316_MI);
		V_1 = L_3;
		if (p2)
		{
			goto IL_0092;
		}
	}
	{
		if ((((uint32_t)(((int32_t)(((t20 *)V_1)->max_length)))) != ((uint32_t)1)))
		{
			goto IL_0092;
		}
	}
	{
		if (!p1)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_4 = 0;
		t42 * L_5 = m1430((*(t29 **)(t29 **)SZArrayLdElema(V_1, L_4)), &m1430_MI);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, L_5);
		if (!L_6)
		{
			goto IL_0067;
		}
	}
	{
		t20 * L_7 = m5936(NULL, p1, 1, &m5936_MI);
		V_0 = ((t316*)Castclass(L_7, InitializedTypeInfo(&t316_TI)));
		int32_t L_8 = 0;
		ArrayElementTypeCheck (V_0, (*(t29 **)(t29 **)SZArrayLdElema(V_1, L_8)));
		*((t29 **)(t29 **)SZArrayLdElema(V_0, 0)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema(V_1, L_8));
		goto IL_0074;
	}

IL_0067:
	{
		t20 * L_9 = m5936(NULL, p1, 0, &m5936_MI);
		V_0 = ((t316*)Castclass(L_9, InitializedTypeInfo(&t316_TI)));
	}

IL_0074:
	{
		goto IL_0090;
	}

IL_0076:
	{
		int32_t L_10 = 0;
		t42 * L_11 = m1430((*(t29 **)(t29 **)SZArrayLdElema(V_1, L_10)), &m1430_MI);
		t20 * L_12 = m5936(NULL, L_11, 1, &m5936_MI);
		V_0 = ((t316*)Castclass(L_12, InitializedTypeInfo(&t316_TI)));
		int32_t L_13 = 0;
		ArrayElementTypeCheck (V_0, (*(t29 **)(t29 **)SZArrayLdElema(V_1, L_13)));
		*((t29 **)(t29 **)SZArrayLdElema(V_0, 0)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema(V_1, L_13));
	}

IL_0090:
	{
		return V_0;
	}

IL_0092:
	{
		if (!p1)
		{
			goto IL_00b4;
		}
	}
	{
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6002_MI, p1);
		if (!L_14)
		{
			goto IL_00b4;
		}
	}
	{
		if (!p2)
		{
			goto IL_00b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t629 * L_15 = m9324(NULL, p1, &m9324_MI);
		V_2 = L_15;
		bool L_16 = m5330(V_2, &m5330_MI);
		if (L_16)
		{
			goto IL_00b4;
		}
	}
	{
		p2 = 0;
	}

IL_00b4:
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_1)->max_length)))) >= ((int32_t)((int32_t)16))))
		{
			goto IL_00c0;
		}
	}
	{
		G_B23_0 = (((int32_t)(((t20 *)V_1)->max_length)));
		goto IL_00c2;
	}

IL_00c0:
	{
		G_B23_0 = ((int32_t)16);
	}

IL_00c2:
	{
		V_3 = G_B23_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_17 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4215(L_17, V_3, &m4215_MI);
		V_4 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_18 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m4181(L_18, V_3, &m4181_MI);
		V_5 = L_18;
		V_6 = p0;
		V_7 = 0;
	}

IL_00d9:
	{
		V_9 = V_1;
		V_10 = 0;
		goto IL_017d;
	}

IL_00e4:
	{
		int32_t L_19 = V_10;
		V_8 = (*(t29 **)(t29 **)SZArrayLdElema(V_9, L_19));
		t42 * L_20 = m1430(V_8, &m1430_MI);
		V_12 = L_20;
		if (!p1)
		{
			goto IL_0106;
		}
	}
	{
		bool L_21 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, V_12);
		if (L_21)
		{
			goto IL_0106;
		}
	}
	{
		goto IL_0177;
	}

IL_0106:
	{
		t29 * L_22 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, V_4, V_12);
		V_13 = ((t1655 *)Castclass(L_22, InitializedTypeInfo(&t1655_TI)));
		if (!V_13)
		{
			goto IL_0125;
		}
	}
	{
		t629 * L_23 = m9310(V_13, &m9310_MI);
		V_11 = L_23;
		goto IL_012e;
	}

IL_0125:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t629 * L_24 = m9324(NULL, V_12, &m9324_MI);
		V_11 = L_24;
	}

IL_012e:
	{
		if (!V_7)
		{
			goto IL_013b;
		}
	}
	{
		bool L_25 = m5330(V_11, &m5330_MI);
		if (!L_25)
		{
			goto IL_0161;
		}
	}

IL_013b:
	{
		bool L_26 = m5329(V_11, &m5329_MI);
		if (L_26)
		{
			goto IL_0157;
		}
	}
	{
		if (!V_13)
		{
			goto IL_0157;
		}
	}
	{
		if (!V_13)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_27 = m9311(V_13, &m9311_MI);
		if ((((uint32_t)L_27) != ((uint32_t)V_7)))
		{
			goto IL_0161;
		}
	}

IL_0157:
	{
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_5, V_8);
	}

IL_0161:
	{
		if (V_13)
		{
			goto IL_0177;
		}
	}
	{
		t1655 * L_28 = (t1655 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1655_TI));
		m9309(L_28, V_11, V_7, &m9309_MI);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, V_4, V_12, L_28);
	}

IL_0177:
	{
		V_10 = ((int32_t)(V_10+1));
	}

IL_017d:
	{
		if ((((int32_t)V_10) < ((int32_t)(((int32_t)(((t20 *)V_9)->max_length))))))
		{
			goto IL_00e4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t29 * L_29 = m9323(NULL, V_6, &m9323_MI);
		t29 * L_30 = L_29;
		V_6 = L_30;
		if (!L_30)
		{
			goto IL_01a3;
		}
	}
	{
		V_7 = ((int32_t)(V_7+1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_31 = m9316(NULL, V_6, p1, &m9316_MI);
		V_1 = L_31;
	}

IL_01a3:
	{
		if (!p2)
		{
			goto IL_01ad;
		}
	}
	{
		if (V_6)
		{
			goto IL_00d9;
		}
	}

IL_01ad:
	{
		V_14 = (t316*)NULL;
		if (!p1)
		{
			goto IL_01bb;
		}
	}
	{
		bool L_32 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, p1);
		if (!L_32)
		{
			goto IL_01da;
		}
	}

IL_01bb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_33 = m1554(NULL, LoadTypeToken(&t490_0_0_0), &m1554_MI);
		int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, V_5);
		t20 * L_35 = m5936(NULL, L_33, L_34, &m5936_MI);
		V_14 = ((t316*)Castclass(L_35, InitializedTypeInfo(&t316_TI)));
		goto IL_01ee;
	}

IL_01da:
	{
		int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, V_5);
		t20 * L_37 = m5936(NULL, p1, L_36, &m5936_MI);
		V_14 = ((t316*)IsInst(L_37, InitializedTypeInfo(&t316_TI)));
	}

IL_01ee:
	{
		VirtActionInvoker2< t20 *, int32_t >::Invoke(&m4177_MI, V_5, (t20 *)(t20 *)V_14, 0);
		return V_14;
	}
}
extern MethodInfo m9319_MI;
 t316* m9319 (t29 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1199, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (p1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_1 = m9316(NULL, p0, (t42 *)NULL, &m9316_MI);
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4006_MI, L_1);
		return ((t316*)Castclass(L_2, InitializedTypeInfo(&t316_TI)));
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t1656_0_0_0), &m1554_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_4 = m9318(NULL, p0, L_3, p1, &m9318_MI);
		return L_4;
	}
}
 bool m9320 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method){
	t316* V_0 = {0};
	int32_t V_1 = 0;
	t29 * V_2 = {0};
	{
		if (p1)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral863, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_1 = m9313(NULL, p0, &m9313_MI);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, t42 *, bool >::Invoke(&m9858_MI, p0, p1, p2);
		return L_2;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_3 = m9321(NULL, p0, p1, &m9321_MI);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		return 1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_4 = m9315(NULL, p0, p1, &m9315_MI);
		V_0 = L_4;
		if (!V_0)
		{
			goto IL_0055;
		}
	}
	{
		V_1 = 0;
		goto IL_004f;
	}

IL_0039:
	{
		int32_t L_5 = V_1;
		t42 * L_6 = m1430((*(t29 **)(t29 **)SZArrayLdElema(V_0, L_5)), &m1430_MI);
		bool L_7 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, L_6);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		return 1;
	}

IL_004b:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_004f:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0039;
		}
	}

IL_0055:
	{
		if (!p2)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t29 * L_8 = m9323(NULL, p0, &m9323_MI);
		t29 * L_9 = L_8;
		V_2 = L_9;
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_10 = m9320(NULL, V_2, p1, p2, &m9320_MI);
		return L_10;
	}

IL_006b:
	{
		return 0;
	}
}
 bool m9321 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method){
	typedef bool (*m9321_ftn) (t29 *, t42 *);
	static m9321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9321_ftn)il2cpp_codegen_resolve_icall ("System.MonoCustomAttrs::IsDefinedInternal(System.Reflection.ICustomAttributeProvider,System.Type)");
	return _il2cpp_icall_func(p0, p1);
}
 t1146 * m9322 (t29 * __this, t1146 * p0, MethodInfo* method){
	t557 * V_0 = {0};
	t557 * V_1 = {0};
	t638* V_2 = {0};
	t537* V_3 = {0};
	int32_t V_4 = 0;
	{
		t557 * L_0 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10176_MI, p0, 1);
		V_0 = L_0;
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7549_MI, V_0);
		if (L_1)
		{
			goto IL_001b;
		}
	}

IL_0013:
	{
		t557 * L_2 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10177_MI, p0, 1);
		V_0 = L_2;
	}

IL_001b:
	{
		if (!V_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7549_MI, V_0);
		if (L_3)
		{
			goto IL_0028;
		}
	}

IL_0026:
	{
		return (t1146 *)NULL;
	}

IL_0028:
	{
		t557 * L_4 = (t557 *)VirtFuncInvoker0< t557 * >::Invoke(&m10158_MI, V_0);
		V_1 = L_4;
		if (!V_1)
		{
			goto IL_009f;
		}
	}
	{
		if ((((t557 *)V_1) == ((t557 *)V_0)))
		{
			goto IL_009f;
		}
	}
	{
		t638* L_5 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m10143_MI, p0);
		V_2 = L_5;
		if (!V_2)
		{
			goto IL_0087;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		V_3 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), (((int32_t)(((t20 *)V_2)->max_length)))));
		V_4 = 0;
		goto IL_0067;
	}

IL_0054:
	{
		int32_t L_6 = V_4;
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_2, L_6)));
		ArrayElementTypeCheck (V_3, L_7);
		*((t42 **)(t42 **)SZArrayLdElema(V_3, V_4)) = (t42 *)L_7;
		V_4 = ((int32_t)(V_4+1));
	}

IL_0067:
	{
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((t20 *)V_3)->max_length))))))
		{
			goto IL_0054;
		}
	}
	{
		t42 * L_8 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, V_1);
		t7* L_9 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, p0);
		t42 * L_10 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m10144_MI, p0);
		t1146 * L_11 = (t1146 *)VirtFuncInvoker3< t1146 *, t7*, t42 *, t537* >::Invoke(&m6027_MI, L_8, L_9, L_10, V_3);
		return L_11;
	}

IL_0087:
	{
		t42 * L_12 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, V_1);
		t7* L_13 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, p0);
		t42 * L_14 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m10144_MI, p0);
		t1146 * L_15 = (t1146 *)VirtFuncInvoker2< t1146 *, t7*, t42 * >::Invoke(&m6026_MI, L_12, L_13, L_14);
		return L_15;
	}

IL_009f:
	{
		return (t1146 *)NULL;
	}
}
 t29 * m9323 (t29 * __this, t29 * p0, MethodInfo* method){
	t557 * V_0 = {0};
	t557 * V_1 = {0};
	{
		if (p0)
		{
			goto IL_0005;
		}
	}
	{
		return (t29 *)NULL;
	}

IL_0005:
	{
		if (!((t42 *)IsInst(p0, InitializedTypeInfo(&t42_TI))))
		{
			goto IL_0019;
		}
	}
	{
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, ((t42 *)Castclass(p0, InitializedTypeInfo(&t42_TI))));
		return L_0;
	}

IL_0019:
	{
		V_0 = (t557 *)NULL;
		if (!((t1383 *)IsInst(p0, InitializedTypeInfo(&t1383_TI))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t1146 * L_1 = m9322(NULL, ((t1383 *)Castclass(p0, InitializedTypeInfo(&t1383_TI))), &m9322_MI);
		return L_1;
	}

IL_002f:
	{
		if (!((t1377 *)IsInst(p0, InitializedTypeInfo(&t1377_TI))))
		{
			goto IL_003e;
		}
	}
	{
		V_0 = ((t557 *)Castclass(p0, InitializedTypeInfo(&t557_TI)));
	}

IL_003e:
	{
		if (!V_0)
		{
			goto IL_0049;
		}
	}
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7549_MI, V_0);
		if (L_2)
		{
			goto IL_004b;
		}
	}

IL_0049:
	{
		return (t29 *)NULL;
	}

IL_004b:
	{
		t557 * L_3 = (t557 *)VirtFuncInvoker0< t557 * >::Invoke(&m10158_MI, V_0);
		V_1 = L_3;
		if ((((t557 *)V_1) != ((t557 *)V_0)))
		{
			goto IL_0058;
		}
	}
	{
		return (t29 *)NULL;
	}

IL_0058:
	{
		return V_1;
	}
}
 t629 * m9324 (t29 * __this, t42 * p0, MethodInfo* method){
	t629 * V_0 = {0};
	t316* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t629_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t629 * L_1 = (t629 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t629_TI));
		m2915(L_1, 4, &m2915_MI);
		return L_1;
	}

IL_0014:
	{
		V_0 = (t629 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_2 = m9318(NULL, p0, (((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f1), 0, &m9318_MI);
		V_1 = L_2;
		if ((((int32_t)(((t20 *)V_1)->max_length))))
		{
			goto IL_0047;
		}
	}
	{
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t629 * L_5 = m9324(NULL, L_4, &m9324_MI);
		V_0 = L_5;
	}

IL_003c:
	{
		if (!V_0)
		{
			goto IL_0041;
		}
	}
	{
		return V_0;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		return (((t1656_SFs*)InitializedTypeInfo(&t1656_TI)->static_fields)->f2);
	}

IL_0047:
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_1)->max_length)))) <= ((int32_t)1)))
		{
			goto IL_0058;
		}
	}
	{
		t901 * L_6 = (t901 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t901_TI));
		m4003(L_6, (t7*) &_stringLiteral2197, &m4003_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_0058:
	{
		int32_t L_7 = 0;
		return ((t629 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_1, L_7)), InitializedTypeInfo(&t629_TI)));
	}
}
// Metadata Definition System.MonoCustomAttrs
extern Il2CppType t929_0_0_17;
FieldInfo t1656_f0_FieldInfo = 
{
	"corlib", &t929_0_0_17, &t1656_TI, offsetof(t1656_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_49;
FieldInfo t1656_f1_FieldInfo = 
{
	"AttributeUsageType", &t42_0_0_49, &t1656_TI, offsetof(t1656_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t629_0_0_49;
FieldInfo t1656_f2_FieldInfo = 
{
	"DefaultAttributeUsage", &t629_0_0_49, &t1656_TI, offsetof(t1656_SFs, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1656_FIs[] =
{
	&t1656_f0_FieldInfo,
	&t1656_f1_FieldInfo,
	&t1656_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9312_MI = 
{
	".cctor", (methodPointerType)&m9312, &t1656_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 5148, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1656_m9313_ParameterInfos[] = 
{
	{"obj", 0, 134224135, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9313_MI = 
{
	"IsUserCattrProvider", (methodPointerType)&m9313, &t1656_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1656_m9313_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5149, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1656_m9314_ParameterInfos[] = 
{
	{"obj", 0, 134224136, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"attributeType", 1, 134224137, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"pseudoAttrs", 2, 134224138, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9314_MI = 
{
	"GetCustomAttributesInternal", (methodPointerType)&m9314, &t1656_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t29_t297, t1656_m9314_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 3, false, false, 5150, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1656_m9315_ParameterInfos[] = 
{
	{"obj", 0, 134224139, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"attributeType", 1, 134224140, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9315_MI = 
{
	"GetPseudoCustomAttributes", (methodPointerType)&m9315, &t1656_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t29, t1656_m9315_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 5151, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1656_m9316_ParameterInfos[] = 
{
	{"obj", 0, 134224141, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"attributeType", 1, 134224142, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9316_MI = 
{
	"GetCustomAttributesBase", (methodPointerType)&m9316, &t1656_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t29, t1656_m9316_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 5152, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1656_m9317_ParameterInfos[] = 
{
	{"obj", 0, 134224143, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"attributeType", 1, 134224144, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 2, 134224145, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t490_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9317_MI = 
{
	"GetCustomAttribute", (methodPointerType)&m9317, &t1656_TI, &t490_0_0_0, RuntimeInvoker_t29_t29_t29_t297, t1656_m9317_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 3, false, false, 5153, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1656_m9318_ParameterInfos[] = 
{
	{"obj", 0, 134224146, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"attributeType", 1, 134224147, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 2, 134224148, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9318_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m9318, &t1656_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t29_t297, t1656_m9318_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 3, false, false, 5154, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1656_m9319_ParameterInfos[] = 
{
	{"obj", 0, 134224149, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"inherit", 1, 134224150, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9319_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m9319, &t1656_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1656_m9319_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 5155, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1656_m9320_ParameterInfos[] = 
{
	{"obj", 0, 134224151, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"attributeType", 1, 134224152, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 2, 134224153, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9320_MI = 
{
	"IsDefined", (methodPointerType)&m9320, &t1656_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t297, t1656_m9320_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 3, false, false, 5156, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1656_m9321_ParameterInfos[] = 
{
	{"obj", 0, 134224154, &EmptyCustomAttributesCache, &t1657_0_0_0},
	{"AttributeType", 1, 134224155, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9321_MI = 
{
	"IsDefinedInternal", (methodPointerType)&m9321, &t1656_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1656_m9321_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 2, false, false, 5157, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1146_0_0_0;
extern Il2CppType t1146_0_0_0;
static ParameterInfo t1656_m9322_ParameterInfos[] = 
{
	{"property", 0, 134224156, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9322_MI = 
{
	"GetBasePropertyDefinition", (methodPointerType)&m9322, &t1656_TI, &t1146_0_0_0, RuntimeInvoker_t29_t29, t1656_m9322_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5158, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1657_0_0_0;
static ParameterInfo t1656_m9323_ParameterInfos[] = 
{
	{"obj", 0, 134224157, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t1657_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9323_MI = 
{
	"GetBase", (methodPointerType)&m9323, &t1656_TI, &t1657_0_0_0, RuntimeInvoker_t29_t29, t1656_m9323_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5159, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1656_m9324_ParameterInfos[] = 
{
	{"attributeType", 0, 134224158, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t629_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9324_MI = 
{
	"RetrieveAttributeUsage", (methodPointerType)&m9324, &t1656_TI, &t629_0_0_0, RuntimeInvoker_t29_t29, t1656_m9324_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5160, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1656_MIs[] =
{
	&m9312_MI,
	&m9313_MI,
	&m9314_MI,
	&m9315_MI,
	&m9316_MI,
	&m9317_MI,
	&m9318_MI,
	&m9319_MI,
	&m9320_MI,
	&m9321_MI,
	&m9322_MI,
	&m9323_MI,
	&m9324_MI,
	NULL
};
extern TypeInfo t1655_TI;
static TypeInfo* t1656_TI__nestedTypes[2] =
{
	&t1655_TI,
	NULL
};
static MethodInfo* t1656_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1656_1_0_0;
struct t1656;
TypeInfo t1656_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoCustomAttrs", "System", t1656_MIs, NULL, t1656_FIs, NULL, &t29_TI, t1656_TI__nestedTypes, NULL, &t1656_TI, NULL, t1656_VT, &EmptyCustomAttributesCache, &t1656_TI, &t1656_0_0_0, &t1656_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1656), 0, -1, sizeof(t1656_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 13, 0, 3, 0, 1, 4, 0, 0};
#include "t1658.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1658_TI;
#include "t1658MD.h"



extern MethodInfo m9325_MI;
 void m9325 (t29 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition System.MonoTouchAOTHelper
extern Il2CppType t40_0_0_19;
FieldInfo t1658_f0_FieldInfo = 
{
	"FalseFlag", &t40_0_0_19, &t1658_TI, offsetof(t1658_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1658_FIs[] =
{
	&t1658_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9325_MI = 
{
	".cctor", (methodPointerType)&m9325, &t1658_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 5164, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1658_MIs[] =
{
	&m9325_MI,
	NULL
};
static MethodInfo* t1658_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1658_0_0_0;
extern Il2CppType t1658_1_0_0;
struct t1658;
TypeInfo t1658_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoTouchAOTHelper", "System", t1658_MIs, NULL, t1658_FIs, NULL, &t29_TI, NULL, NULL, &t1658_TI, NULL, t1658_VT, &EmptyCustomAttributesCache, &t1658_TI, &t1658_0_0_0, &t1658_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1658), 0, -1, sizeof(t1658_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 1, 0, 1, 0, 0, 4, 0, 0};
#include "t1659.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1659_TI;
#include "t1659MD.h"



extern MethodInfo m9326_MI;
 void m9326 (t1659 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.MonoTypeInfo
extern Il2CppType t7_0_0_6;
FieldInfo t1659_f0_FieldInfo = 
{
	"full_name", &t7_0_0_6, &t1659_TI, offsetof(t1659, f0), &EmptyCustomAttributesCache};
extern Il2CppType t660_0_0_6;
FieldInfo t1659_f1_FieldInfo = 
{
	"default_ctor", &t660_0_0_6, &t1659_TI, offsetof(t1659, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1659_FIs[] =
{
	&t1659_f0_FieldInfo,
	&t1659_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9326_MI = 
{
	".ctor", (methodPointerType)&m9326, &t1659_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5165, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1659_MIs[] =
{
	&m9326_MI,
	NULL
};
static MethodInfo* t1659_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1659_0_0_0;
extern Il2CppType t1659_1_0_0;
struct t1659;
TypeInfo t1659_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoTypeInfo", "System", t1659_MIs, NULL, t1659_FIs, NULL, &t29_TI, NULL, NULL, &t1659_TI, NULL, t1659_VT, &EmptyCustomAttributesCache, &t1659_TI, &t1659_0_0_0, &t1659_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1659), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
#include "t1660MD.h"

#include "t1149.h"
#include "t1142.h"
#include "t1148.h"
#include "t660.h"
#include "t630.h"
#include "t631.h"
#include "t1150.h"
#include "t632.h"
#include "t1143.h"
#include "t633.h"
#include "t425.h"
#include "t305.h"
#include "t1371.h"
#include "t1353.h"
#include "t391.h"
extern TypeInfo t660_TI;
extern TypeInfo t630_TI;
extern TypeInfo t631_TI;
extern TypeInfo t1150_TI;
extern TypeInfo t634_TI;
extern TypeInfo t632_TI;
extern TypeInfo t1147_TI;
extern TypeInfo t1365_TI;
extern TypeInfo t1366_TI;
extern TypeInfo t305_TI;
extern TypeInfo t633_TI;
extern TypeInfo t446_TI;
extern TypeInfo t425_TI;
extern TypeInfo t1371_TI;
extern TypeInfo t1353_TI;
extern TypeInfo t391_TI;
#include "t631MD.h"
#include "t305MD.h"
#include "t931MD.h"
#include "t490MD.h"
#include "t425MD.h"
#include "t1371MD.h"
#include "t1678MD.h"
extern Il2CppType t425_0_0_0;
extern Il2CppType t391_0_0_0;
extern MethodInfo m9353_MI;
extern MethodInfo m9363_MI;
extern MethodInfo m9370_MI;
extern MethodInfo m6042_MI;
extern MethodInfo m9368_MI;
extern MethodInfo m6038_MI;
extern MethodInfo m5991_MI;
extern MethodInfo m9349_MI;
extern MethodInfo m6034_MI;
extern MethodInfo m9327_MI;
extern MethodInfo m9332_MI;
extern MethodInfo m7547_MI;
extern MethodInfo m7455_MI;
extern MethodInfo m9372_MI;
extern MethodInfo m7510_MI;
extern MethodInfo m10141_MI;
extern MethodInfo m9331_MI;
extern MethodInfo m9333_MI;
extern MethodInfo m9337_MI;
extern MethodInfo m7513_MI;
extern MethodInfo m9340_MI;
extern MethodInfo m10149_MI;
extern MethodInfo m9343_MI;
extern MethodInfo m9344_MI;
extern MethodInfo m9345_MI;
extern MethodInfo m6029_MI;
extern MethodInfo m6019_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m10147_MI;
extern MethodInfo m8834_MI;
extern MethodInfo m9355_MI;
extern MethodInfo m1685_MI;
extern MethodInfo m10156_MI;
extern MethodInfo m10148_MI;
extern MethodInfo m1713_MI;
extern MethodInfo m5293_MI;
extern MethodInfo m5290_MI;
extern MethodInfo m6068_MI;
extern MethodInfo m2939_MI;
extern MethodInfo m7698_MI;
extern MethodInfo m5292_MI;
extern MethodInfo m9373_MI;
extern MethodInfo m9335_MI;
extern MethodInfo m10155_MI;
extern MethodInfo m7529_MI;
extern MethodInfo m9585_MI;
extern MethodInfo m6040_MI;
extern MethodInfo m9711_MI;
extern MethodInfo m3971_MI;


 int32_t m9327 (t29 * __this, t42 * p0, MethodInfo* method){
	typedef int32_t (*m9327_ftn) (t42 *);
	static m9327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9327_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_attributes(System.Type)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m9328_MI;
 t660 * m9328 (t1660 * __this, MethodInfo* method){
	t660 * V_0 = {0};
	int32_t V_1 = {0};
	t660 * V_2 = {0};
	{
		V_0 = (t660 *)NULL;
		t1659 * L_0 = (__this->f8);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		t1659 * L_1 = (t1659 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1659_TI));
		m9326(L_1, &m9326_MI);
		__this->f8 = L_1;
	}

IL_0015:
	{
		t1659 * L_2 = (__this->f8);
		t660 * L_3 = (L_2->f1);
		t660 * L_4 = L_3;
		V_0 = L_4;
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		t1659 * L_5 = (__this->f8);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t660 * L_6 = (t660 *)VirtFuncInvoker5< t660 *, int32_t, t631 *, int32_t, t537*, t634* >::Invoke(&m6034_MI, __this, ((int32_t)52), (t631 *)NULL, 3, (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3), (t634*)(t634*)NULL);
		t660 * L_7 = L_6;
		V_2 = L_7;
		L_5->f1 = L_7;
		V_0 = V_2;
	}

IL_0043:
	{
		return V_0;
	}
}
extern MethodInfo m9329_MI;
 int32_t m9329 (t1660 * __this, MethodInfo* method){
	{
		int32_t L_0 = m9327(NULL, __this, &m9327_MI);
		return L_0;
	}
}
extern MethodInfo m9330_MI;
 t660 * m9330 (t1660 * __this, int32_t p0, t631 * p1, int32_t p2, t537* p3, t634* p4, MethodInfo* method){
	t1147* V_0 = {0};
	t660 * V_1 = {0};
	t1365* V_2 = {0};
	int32_t V_3 = 0;
	t660 * V_4 = {0};
	t1147* V_5 = {0};
	int32_t V_6 = 0;
	t660 * V_7 = {0};
	t1147* V_8 = {0};
	int32_t V_9 = 0;
	{
		if (p0)
		{
			goto IL_0009;
		}
	}
	{
		p0 = ((int32_t)20);
	}

IL_0009:
	{
		t1147* L_0 = (t1147*)VirtFuncInvoker1< t1147*, int32_t >::Invoke(&m9332_MI, __this, p0);
		V_0 = L_0;
		V_1 = (t660 *)NULL;
		V_3 = 0;
		V_5 = V_0;
		V_6 = 0;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_1 = V_6;
		V_4 = (*(t660 **)(t660 **)SZArrayLdElema(V_5, L_1));
		if ((((int32_t)p2) == ((int32_t)3)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7547_MI, V_4);
		if ((((int32_t)((int32_t)((int32_t)L_2&(int32_t)p2))) == ((int32_t)p2)))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_003d;
	}

IL_0036:
	{
		V_1 = V_4;
		V_3 = ((int32_t)(V_3+1));
	}

IL_003d:
	{
		V_6 = ((int32_t)(V_6+1));
	}

IL_0043:
	{
		if ((((int32_t)V_6) < ((int32_t)(((int32_t)(((t20 *)V_5)->max_length))))))
		{
			goto IL_001d;
		}
	}
	{
		if (V_3)
		{
			goto IL_0050;
		}
	}
	{
		return (t660 *)NULL;
	}

IL_0050:
	{
		if (p3)
		{
			goto IL_006b;
		}
	}
	{
		if ((((int32_t)V_3) <= ((int32_t)1)))
		{
			goto IL_005e;
		}
	}
	{
		t1357 * L_3 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7455(L_3, &m7455_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_005e:
	{
		t636 * L_4 = m9372(__this, V_1, &m9372_MI);
		return ((t660 *)Castclass(L_4, InitializedTypeInfo(&t660_TI)));
	}

IL_006b:
	{
		V_2 = ((t1365*)SZArrayNew(InitializedTypeInfo(&t1365_TI), V_3));
		if ((((uint32_t)V_3) != ((uint32_t)1)))
		{
			goto IL_007c;
		}
	}
	{
		ArrayElementTypeCheck (V_2, V_1);
		*((t636 **)(t636 **)SZArrayLdElema(V_2, 0)) = (t636 *)V_1;
		goto IL_00b6;
	}

IL_007c:
	{
		V_3 = 0;
		V_8 = V_0;
		V_9 = 0;
		goto IL_00ae;
	}

IL_0086:
	{
		int32_t L_5 = V_9;
		V_7 = (*(t660 **)(t660 **)SZArrayLdElema(V_8, L_5));
		if ((((int32_t)p2) == ((int32_t)3)))
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7547_MI, V_7);
		if ((((int32_t)((int32_t)((int32_t)L_6&(int32_t)p2))) == ((int32_t)p2)))
		{
			goto IL_009f;
		}
	}
	{
		goto IL_00a8;
	}

IL_009f:
	{
		int32_t L_7 = V_3;
		V_3 = ((int32_t)(L_7+1));
		ArrayElementTypeCheck (V_2, V_7);
		*((t636 **)(t636 **)SZArrayLdElema(V_2, L_7)) = (t636 *)V_7;
	}

IL_00a8:
	{
		V_9 = ((int32_t)(V_9+1));
	}

IL_00ae:
	{
		if ((((int32_t)V_9) < ((int32_t)(((int32_t)(((t20 *)V_8)->max_length))))))
		{
			goto IL_0086;
		}
	}

IL_00b6:
	{
		if (p1)
		{
			goto IL_00c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_8 = m7510(NULL, &m7510_MI);
		p1 = L_8;
	}

IL_00c2:
	{
		t636 * L_9 = (t636 *)VirtFuncInvoker4< t636 *, int32_t, t1365*, t537*, t634* >::Invoke(&m10141_MI, p1, p0, V_2, p3, p4);
		t636 * L_10 = m9372(__this, L_9, &m9372_MI);
		return ((t660 *)Castclass(L_10, InitializedTypeInfo(&t660_TI)));
	}
}
 t1147* m9331 (t1660 * __this, int32_t p0, t42 * p1, MethodInfo* method){
	typedef t1147* (*m9331_ftn) (t1660 *, int32_t, t42 *);
	static m9331_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9331_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetConstructors_internal(System.Reflection.BindingFlags,System.Type)");
	return _il2cpp_icall_func(__this, p0, p1);
}
 t1147* m9332 (t1660 * __this, int32_t p0, MethodInfo* method){
	{
		t1147* L_0 = m9331(__this, p0, __this, &m9331_MI);
		return L_0;
	}
}
 t1143 * m9333 (t1660 * __this, t7* p0, int32_t p1, MethodInfo* method){
	typedef t1143 * (*m9333_ftn) (t1660 *, t7*, int32_t);
	static m9333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9333_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::InternalGetEvent(System.String,System.Reflection.BindingFlags)");
	return _il2cpp_icall_func(__this, p0, p1);
}
extern MethodInfo m9334_MI;
 t1143 * m9334 (t1660 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral321, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		t1143 * L_1 = m9333(__this, p0, p1, &m9333_MI);
		return L_1;
	}
}
 t1144 * m9335 (t1660 * __this, t7* p0, int32_t p1, MethodInfo* method){
	typedef t1144 * (*m9335_ftn) (t1660 *, t7*, int32_t);
	static m9335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9335_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetField(System.String,System.Reflection.BindingFlags)");
	return _il2cpp_icall_func(__this, p0, p1);
}
extern MethodInfo m9336_MI;
 t537* m9336 (t1660 * __this, MethodInfo* method){
	typedef t537* (*m9336_ftn) (t1660 *);
	static m9336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9336_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetInterfaces()");
	return _il2cpp_icall_func(__this);
}
 t1145* m9337 (t1660 * __this, t7* p0, int32_t p1, bool p2, t42 * p3, MethodInfo* method){
	typedef t1145* (*m9337_ftn) (t1660 *, t7*, int32_t, bool, t42 *);
	static m9337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9337_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetMethodsByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)");
	return _il2cpp_icall_func(__this, p0, p1, p2, p3);
}
extern MethodInfo m9338_MI;
 t1145* m9338 (t1660 * __this, int32_t p0, MethodInfo* method){
	{
		t1145* L_0 = m9337(__this, (t7*)NULL, p0, 0, __this, &m9337_MI);
		return L_0;
	}
}
extern MethodInfo m9339_MI;
 t557 * m9339 (t1660 * __this, t7* p0, int32_t p1, t631 * p2, int32_t p3, t537* p4, t634* p5, MethodInfo* method){
	bool V_0 = false;
	t1145* V_1 = {0};
	t557 * V_2 = {0};
	t1365* V_3 = {0};
	int32_t V_4 = 0;
	t557 * V_5 = {0};
	t1145* V_6 = {0};
	int32_t V_7 = 0;
	t557 * V_8 = {0};
	t1145* V_9 = {0};
	int32_t V_10 = 0;
	{
		V_0 = ((((int32_t)((((int32_t)((int32_t)((int32_t)p1&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		t1145* L_0 = m9337(__this, p0, p1, V_0, __this, &m9337_MI);
		V_1 = L_0;
		V_2 = (t557 *)NULL;
		V_4 = 0;
		V_6 = V_1;
		V_7 = 0;
		goto IL_004d;
	}

IL_0022:
	{
		int32_t L_1 = V_7;
		V_5 = (*(t557 **)(t557 **)SZArrayLdElema(V_6, L_1));
		if ((((int32_t)p3) == ((int32_t)3)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7547_MI, V_5);
		if ((((int32_t)((int32_t)((int32_t)L_2&(int32_t)p3))) == ((int32_t)p3)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0047;
	}

IL_003e:
	{
		V_2 = V_5;
		V_4 = ((int32_t)(V_4+1));
	}

IL_0047:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_004d:
	{
		if ((((int32_t)V_7) < ((int32_t)(((int32_t)(((t20 *)V_6)->max_length))))))
		{
			goto IL_0022;
		}
	}
	{
		if (V_4)
		{
			goto IL_005b;
		}
	}
	{
		return (t557 *)NULL;
	}

IL_005b:
	{
		if ((((uint32_t)V_4) != ((uint32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		if (p4)
		{
			goto IL_0071;
		}
	}
	{
		t636 * L_3 = m9372(__this, V_2, &m9372_MI);
		return ((t557 *)Castclass(L_3, InitializedTypeInfo(&t557_TI)));
	}

IL_0071:
	{
		V_3 = ((t1365*)SZArrayNew(InitializedTypeInfo(&t1365_TI), V_4));
		if ((((uint32_t)V_4) != ((uint32_t)1)))
		{
			goto IL_0084;
		}
	}
	{
		ArrayElementTypeCheck (V_3, V_2);
		*((t636 **)(t636 **)SZArrayLdElema(V_3, 0)) = (t636 *)V_2;
		goto IL_00c4;
	}

IL_0084:
	{
		V_4 = 0;
		V_9 = V_1;
		V_10 = 0;
		goto IL_00bc;
	}

IL_008f:
	{
		int32_t L_4 = V_10;
		V_8 = (*(t557 **)(t557 **)SZArrayLdElema(V_9, L_4));
		if ((((int32_t)p3) == ((int32_t)3)))
		{
			goto IL_00ab;
		}
	}
	{
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7547_MI, V_8);
		if ((((int32_t)((int32_t)((int32_t)L_5&(int32_t)p3))) == ((int32_t)p3)))
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00b6;
	}

IL_00ab:
	{
		int32_t L_6 = V_4;
		V_4 = ((int32_t)(L_6+1));
		ArrayElementTypeCheck (V_3, V_8);
		*((t636 **)(t636 **)SZArrayLdElema(V_3, L_6)) = (t636 *)V_8;
	}

IL_00b6:
	{
		V_10 = ((int32_t)(V_10+1));
	}

IL_00bc:
	{
		if ((((int32_t)V_10) < ((int32_t)(((int32_t)(((t20 *)V_9)->max_length))))))
		{
			goto IL_008f;
		}
	}

IL_00c4:
	{
		if (p4)
		{
			goto IL_00da;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t636 * L_7 = m7513(NULL, V_3, &m7513_MI);
		t636 * L_8 = m9372(__this, L_7, &m9372_MI);
		return ((t557 *)Castclass(L_8, InitializedTypeInfo(&t557_TI)));
	}

IL_00da:
	{
		if (p2)
		{
			goto IL_00e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_9 = m7510(NULL, &m7510_MI);
		p2 = L_9;
	}

IL_00e6:
	{
		t636 * L_10 = (t636 *)VirtFuncInvoker4< t636 *, int32_t, t1365*, t537*, t634* >::Invoke(&m10141_MI, p2, p1, V_3, p4, p5);
		t636 * L_11 = m9372(__this, L_10, &m9372_MI);
		return ((t557 *)Castclass(L_11, InitializedTypeInfo(&t557_TI)));
	}
}
 t1366* m9340 (t1660 * __this, t7* p0, int32_t p1, bool p2, t42 * p3, MethodInfo* method){
	typedef t1366* (*m9340_ftn) (t1660 *, t7*, int32_t, bool, t42 *);
	static m9340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9340_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetPropertiesByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)");
	return _il2cpp_icall_func(__this, p0, p1, p2, p3);
}
extern MethodInfo m9341_MI;
 t1146 * m9341 (t1660 * __this, t7* p0, int32_t p1, t631 * p2, t42 * p3, t537* p4, t634* p5, MethodInfo* method){
	bool V_0 = false;
	t1366* V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = ((((int32_t)((((int32_t)((int32_t)((int32_t)p1&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		t1366* L_0 = m9340(__this, p0, p1, V_0, __this, &m9340_MI);
		V_1 = L_0;
		V_2 = (((int32_t)(((t20 *)V_1)->max_length)));
		if (V_2)
		{
			goto IL_001e;
		}
	}
	{
		return (t1146 *)NULL;
	}

IL_001e:
	{
		if ((((uint32_t)V_2) != ((uint32_t)1)))
		{
			goto IL_0040;
		}
	}
	{
		if (!p4)
		{
			goto IL_002c;
		}
	}
	{
		if ((((int32_t)(((t20 *)p4)->max_length))))
		{
			goto IL_0040;
		}
	}

IL_002c:
	{
		if (!p3)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_1 = 0;
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m10144_MI, (*(t1146 **)(t1146 **)SZArrayLdElema(V_1, L_1)));
		if ((((t42 *)p3) != ((t42 *)L_2)))
		{
			goto IL_0040;
		}
	}

IL_003c:
	{
		int32_t L_3 = 0;
		return (*(t1146 **)(t1146 **)SZArrayLdElema(V_1, L_3));
	}

IL_0040:
	{
		if (p2)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_4 = m7510(NULL, &m7510_MI);
		p2 = L_4;
	}

IL_004c:
	{
		t1146 * L_5 = (t1146 *)VirtFuncInvoker5< t1146 *, int32_t, t1366*, t42 *, t537*, t634* >::Invoke(&m10149_MI, p2, p1, V_1, p3, p4, p5);
		return L_5;
	}
}
extern MethodInfo m9342_MI;
 bool m9342 (t1660 * __this, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m9343_MI, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m9344_MI, __this);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m9345_MI, __this);
		G_B4_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B4_0 = 1;
	}

IL_0019:
	{
		return G_B4_0;
	}
}
 bool m9343 (t1660 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		bool L_0 = m6029(NULL, __this, &m6029_MI);
		return L_0;
	}
}
 bool m9344 (t1660 * __this, MethodInfo* method){
	typedef bool (*m9344_ftn) (t1660 *);
	static m9344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9344_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::IsByRefImpl()");
	return _il2cpp_icall_func(__this);
}
 bool m9345 (t1660 * __this, MethodInfo* method){
	typedef bool (*m9345_ftn) (t1660 *);
	static m9345_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9345_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::IsPointerImpl()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9346_MI;
 bool m9346 (t1660 * __this, MethodInfo* method){
	typedef bool (*m9346_ftn) (t1660 *);
	static m9346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9346_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::IsPrimitiveImpl()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9347_MI;
 bool m9347 (t1660 * __this, t42 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral896, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		bool L_1 = m6019(__this, p0, &m6019_MI);
		return L_1;
	}
}
extern MethodInfo m9348_MI;
 t29 * m9348 (t1660 * __this, t7* p0, int32_t p1, t631 * p2, t29 * p3, t316* p4, t634* p5, t633 * p6, t446* p7, MethodInfo* method){
	t7* V_0 = {0};
	t1147* V_1 = {0};
	t29 * V_2 = {0};
	t636 * V_3 = {0};
	t29 * V_4 = {0};
	t425 * V_5 = {0};
	bool V_6 = false;
	t7* V_7 = {0};
	bool V_8 = false;
	t1145* V_9 = {0};
	t29 * V_10 = {0};
	t636 * V_11 = {0};
	t638* V_12 = {0};
	int32_t V_13 = 0;
	bool V_14 = false;
	t29 * V_15 = {0};
	t1144 * V_16 = {0};
	t1144 * V_17 = {0};
	t1366* V_18 = {0};
	t29 * V_19 = {0};
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	t1365* V_22 = {0};
	t636 * V_23 = {0};
	t636 * V_24 = {0};
	t638* V_25 = {0};
	bool V_26 = false;
	t29 * V_27 = {0};
	t1366* V_28 = {0};
	t29 * V_29 = {0};
	int32_t V_30 = 0;
	int32_t V_31 = 0;
	t1365* V_32 = {0};
	t636 * V_33 = {0};
	t636 * V_34 = {0};
	t638* V_35 = {0};
	bool V_36 = false;
	t29 * V_37 = {0};
	int32_t G_B56_0 = 0;
	int32_t G_B91_0 = 0;
	int32_t G_B111_0 = 0;
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)512))))
		{
			goto IL_001f;
		}
	}
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)13312))))
		{
			goto IL_001d;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral2198, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_001d:
	{
		goto IL_002d;
	}

IL_001f:
	{
		if (p0)
		{
			goto IL_002d;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral321, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002d:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)1024))))
		{
			goto IL_004f;
		}
	}
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)2048))))
		{
			goto IL_004f;
		}
	}
	{
		t305 * L_2 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_2, (t7*) &_stringLiteral2199, (t7*) &_stringLiteral2198, &m3973_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_004f:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)4096))))
		{
			goto IL_0071;
		}
	}
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)8192))))
		{
			goto IL_0071;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_3, (t7*) &_stringLiteral2200, (t7*) &_stringLiteral2198, &m3973_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0071:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)256))))
		{
			goto IL_00ac;
		}
	}
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)2048))))
		{
			goto IL_0093;
		}
	}
	{
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, (t7*) &_stringLiteral2201, (t7*) &_stringLiteral2198, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0093:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)8192))))
		{
			goto IL_00ac;
		}
	}
	{
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, (t7*) &_stringLiteral2202, (t7*) &_stringLiteral2198, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_00ac:
	{
		if (!p7)
		{
			goto IL_00c9;
		}
	}
	{
		if (!p4)
		{
			goto IL_00be;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)p4)->max_length)))) >= ((int32_t)(((int32_t)(((t20 *)p7)->max_length))))))
		{
			goto IL_00c9;
		}
	}

IL_00be:
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral2203, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_00c9:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)16128))))
		{
			goto IL_00e2;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_7, (t7*) &_stringLiteral2204, (t7*) &_stringLiteral2198, &m3973_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_00e2:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)48))))
		{
			goto IL_00f0;
		}
	}
	{
		p1 = ((int32_t)((int32_t)p1|(int32_t)((int32_t)16)));
	}

IL_00f0:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)12))))
		{
			goto IL_00fe;
		}
	}
	{
		p1 = ((int32_t)((int32_t)p1|(int32_t)((int32_t)12)));
	}

IL_00fe:
	{
		if (p2)
		{
			goto IL_010a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_8 = m7510(NULL, &m7510_MI);
		p2 = L_8;
	}

IL_010a:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)512))))
		{
			goto IL_0184;
		}
	}
	{
		p1 = ((int32_t)((int32_t)p1|(int32_t)2));
		t1147* L_9 = (t1147*)VirtFuncInvoker1< t1147*, int32_t >::Invoke(&m9332_MI, __this, p1);
		V_1 = L_9;
		V_2 = NULL;
		t636 * L_10 = (t636 *)VirtFuncInvoker7< t636 *, int32_t, t1365*, t316**, t634*, t633 *, t446*, t29 ** >::Invoke(&m10147_MI, p2, p1, (t1365*)(t1365*)V_1, (&p4), p5, p6, p7, (&V_2));
		V_3 = L_10;
		if (V_3)
		{
			goto IL_0168;
		}
	}
	{
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, __this);
		if (!L_11)
		{
			goto IL_014d;
		}
	}
	{
		if (p4)
		{
			goto IL_014d;
		}
	}
	{
		t29 * L_12 = m8834(NULL, __this, &m8834_MI);
		return L_12;
	}

IL_014d:
	{
		t7* L_13 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m9355_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m1685(NULL, (t7*) &_stringLiteral2205, L_13, (t7*) &_stringLiteral1376, &m1685_MI);
		t1653 * L_15 = (t1653 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1653_TI));
		m9304(L_15, L_14, &m9304_MI);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0168:
	{
		t29 * L_16 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_3, p3, p1, p2, p4, p6);
		V_4 = L_16;
		VirtActionInvoker2< t316**, t29 * >::Invoke(&m10148_MI, p2, (&p4), V_2);
		return V_4;
	}

IL_0184:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_17 = m1713(NULL, p0, (((t7_SFs*)(&t7_TI)->static_fields)->f2), &m1713_MI);
		if (!L_17)
		{
			goto IL_01c5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_18 = m1554(NULL, LoadTypeToken(&t425_0_0_0), &m1554_MI);
		bool L_19 = m5293(NULL, __this, L_18, &m5293_MI);
		if (!L_19)
		{
			goto IL_01c5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_20 = m1554(NULL, LoadTypeToken(&t425_0_0_0), &m1554_MI);
		t490 * L_21 = m5290(NULL, __this, L_20, &m5290_MI);
		V_5 = ((t425 *)Castclass(L_21, InitializedTypeInfo(&t425_TI)));
		t7* L_22 = m6068(V_5, &m6068_MI);
		p0 = L_22;
	}

IL_01c5:
	{
		V_6 = ((((int32_t)((((int32_t)((int32_t)((int32_t)p1&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_7 = (t7*)NULL;
		V_8 = 0;
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)256))))
		{
			goto IL_02ea;
		}
	}
	{
		t1145* L_23 = m9337(__this, p0, p1, V_6, __this, &m9337_MI);
		V_9 = L_23;
		V_10 = NULL;
		if (p4)
		{
			goto IL_0200;
		}
	}
	{
		p4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
	}

IL_0200:
	{
		t636 * L_24 = (t636 *)VirtFuncInvoker7< t636 *, int32_t, t1365*, t316**, t634*, t633 *, t446*, t29 ** >::Invoke(&m10147_MI, p2, p1, (t1365*)(t1365*)V_9, (&p4), p5, p6, p7, (&V_10));
		V_11 = L_24;
		if (V_11)
		{
			goto IL_024b;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_9)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0234;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_25 = m1685(NULL, (t7*) &_stringLiteral2206, p0, (t7*) &_stringLiteral2207, &m1685_MI);
		V_7 = L_25;
		goto IL_0246;
	}

IL_0234:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_26 = m1685(NULL, (t7*) &_stringLiteral2208, p0, (t7*) &_stringLiteral52, &m1685_MI);
		V_7 = L_26;
	}

IL_0246:
	{
		goto IL_02ea;
	}

IL_024b:
	{
		t638* L_27 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_11);
		V_12 = L_27;
		V_13 = 0;
		goto IL_0292;
	}

IL_0259:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1371_TI));
		int32_t L_28 = V_13;
		if ((((t1371 *)(((t1371_SFs*)InitializedTypeInfo(&t1371_TI)->static_fields)->f0)) != ((t29 *)(*(t29 **)(t29 **)SZArrayLdElema(p4, L_28)))))
		{
			goto IL_028c;
		}
	}
	{
		int32_t L_29 = V_13;
		int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7698_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_12, L_29)));
		if ((((int32_t)((int32_t)((int32_t)L_30&(int32_t)((int32_t)4096)))) == ((int32_t)((int32_t)4096))))
		{
			goto IL_028c;
		}
	}
	{
		t305 * L_31 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_31, (t7*) &_stringLiteral2209, (t7*) &_stringLiteral1119, &m3973_MI);
		il2cpp_codegen_raise_exception(L_31);
	}

IL_028c:
	{
		V_13 = ((int32_t)(V_13+1));
	}

IL_0292:
	{
		if ((((int32_t)V_13) < ((int32_t)(((int32_t)(((t20 *)V_12)->max_length))))))
		{
			goto IL_0259;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_12)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_02bb;
		}
	}
	{
		int32_t L_32 = ((int32_t)((((int32_t)(((t20 *)V_12)->max_length)))-1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_33 = m1554(NULL, LoadTypeToken(&t391_0_0_0), &m1554_MI);
		bool L_34 = m5292(NULL, (*(t637 **)(t637 **)SZArrayLdElema(V_12, L_32)), L_33, &m5292_MI);
		G_B56_0 = ((int32_t)(L_34));
		goto IL_02bc;
	}

IL_02bb:
	{
		G_B56_0 = 0;
	}

IL_02bc:
	{
		V_14 = G_B56_0;
		if (!V_14)
		{
			goto IL_02cc;
		}
	}
	{
		m9373(__this, (&p4), V_11, &m9373_MI);
	}

IL_02cc:
	{
		t29 * L_35 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_11, p3, p1, p2, p4, p6);
		V_15 = L_35;
		VirtActionInvoker2< t316**, t29 * >::Invoke(&m10148_MI, p2, (&p4), V_10);
		return V_15;
	}

IL_02ea:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)1024))))
		{
			goto IL_0319;
		}
	}
	{
		t1144 * L_36 = (t1144 *)VirtFuncInvoker2< t1144 *, t7*, int32_t >::Invoke(&m9335_MI, __this, p0, p1);
		V_16 = L_36;
		if (!V_16)
		{
			goto IL_030b;
		}
	}
	{
		t29 * L_37 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m10155_MI, V_16, p3);
		return L_37;
	}

IL_030b:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)4096))))
		{
			goto IL_0317;
		}
	}
	{
		V_8 = 1;
	}

IL_0317:
	{
		goto IL_0375;
	}

IL_0319:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)2048))))
		{
			goto IL_0375;
		}
	}
	{
		t1144 * L_38 = (t1144 *)VirtFuncInvoker2< t1144 *, t7*, int32_t >::Invoke(&m9335_MI, __this, p0, p1);
		V_17 = L_38;
		if (!V_17)
		{
			goto IL_0369;
		}
	}
	{
		if (p4)
		{
			goto IL_033f;
		}
	}
	{
		t338 * L_39 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_39, (t7*) &_stringLiteral2210, &m2950_MI);
		il2cpp_codegen_raise_exception(L_39);
	}

IL_033f:
	{
		if (!p4)
		{
			goto IL_034a;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)p4)->max_length)))) == ((int32_t)1)))
		{
			goto IL_035a;
		}
	}

IL_034a:
	{
		t305 * L_40 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_40, (t7*) &_stringLiteral2211, (t7*) &_stringLiteral2198, &m3973_MI);
		il2cpp_codegen_raise_exception(L_40);
	}

IL_035a:
	{
		int32_t L_41 = 0;
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m7529_MI, V_17, p3, (*(t29 **)(t29 **)SZArrayLdElema(p4, L_41)));
		return NULL;
	}

IL_0369:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)8192))))
		{
			goto IL_0375;
		}
	}
	{
		V_8 = 1;
	}

IL_0375:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)4096))))
		{
			goto IL_0472;
		}
	}
	{
		t1366* L_42 = m9340(__this, p0, p1, V_6, __this, &m9340_MI);
		V_18 = L_42;
		V_19 = NULL;
		V_21 = 0;
		V_20 = 0;
		goto IL_03b2;
	}

IL_0399:
	{
		int32_t L_43 = V_20;
		t557 * L_44 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10176_MI, (*(t1146 **)(t1146 **)SZArrayLdElema(V_18, L_43)), 1);
		if (!L_44)
		{
			goto IL_03ac;
		}
	}
	{
		V_21 = ((int32_t)(V_21+1));
	}

IL_03ac:
	{
		V_20 = ((int32_t)(V_20+1));
	}

IL_03b2:
	{
		if ((((int32_t)V_20) < ((int32_t)(((int32_t)(((t20 *)V_18)->max_length))))))
		{
			goto IL_0399;
		}
	}
	{
		V_22 = ((t1365*)SZArrayNew(InitializedTypeInfo(&t1365_TI), V_21));
		V_21 = 0;
		V_20 = 0;
		goto IL_03ee;
	}

IL_03cb:
	{
		int32_t L_45 = V_20;
		t557 * L_46 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10176_MI, (*(t1146 **)(t1146 **)SZArrayLdElema(V_18, L_45)), 1);
		V_23 = L_46;
		if (!V_23)
		{
			goto IL_03e8;
		}
	}
	{
		int32_t L_47 = V_21;
		V_21 = ((int32_t)(L_47+1));
		ArrayElementTypeCheck (V_22, V_23);
		*((t636 **)(t636 **)SZArrayLdElema(V_22, L_47)) = (t636 *)V_23;
	}

IL_03e8:
	{
		V_20 = ((int32_t)(V_20+1));
	}

IL_03ee:
	{
		if ((((int32_t)V_20) < ((int32_t)(((int32_t)(((t20 *)V_18)->max_length))))))
		{
			goto IL_03cb;
		}
	}
	{
		t636 * L_48 = (t636 *)VirtFuncInvoker7< t636 *, int32_t, t1365*, t316**, t634*, t633 *, t446*, t29 ** >::Invoke(&m10147_MI, p2, p1, V_22, (&p4), p5, p6, p7, (&V_19));
		V_24 = L_48;
		if (V_24)
		{
			goto IL_0414;
		}
	}
	{
		V_8 = 1;
		goto IL_046d;
	}

IL_0414:
	{
		t638* L_49 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_24);
		V_25 = L_49;
		if ((((int32_t)(((int32_t)(((t20 *)V_25)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_043e;
		}
	}
	{
		int32_t L_50 = ((int32_t)((((int32_t)(((t20 *)V_25)->max_length)))-1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_51 = m1554(NULL, LoadTypeToken(&t391_0_0_0), &m1554_MI);
		bool L_52 = m5292(NULL, (*(t637 **)(t637 **)SZArrayLdElema(V_25, L_50)), L_51, &m5292_MI);
		G_B91_0 = ((int32_t)(L_52));
		goto IL_043f;
	}

IL_043e:
	{
		G_B91_0 = 0;
	}

IL_043f:
	{
		V_26 = G_B91_0;
		if (!V_26)
		{
			goto IL_044f;
		}
	}
	{
		m9373(__this, (&p4), V_24, &m9373_MI);
	}

IL_044f:
	{
		t29 * L_53 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_24, p3, p1, p2, p4, p6);
		V_27 = L_53;
		VirtActionInvoker2< t316**, t29 * >::Invoke(&m10148_MI, p2, (&p4), V_19);
		return V_27;
	}

IL_046d:
	{
		goto IL_056a;
	}

IL_0472:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)8192))))
		{
			goto IL_056a;
		}
	}
	{
		t1366* L_54 = m9340(__this, p0, p1, V_6, __this, &m9340_MI);
		V_28 = L_54;
		V_29 = NULL;
		V_31 = 0;
		V_30 = 0;
		goto IL_04af;
	}

IL_0496:
	{
		int32_t L_55 = V_30;
		t557 * L_56 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10177_MI, (*(t1146 **)(t1146 **)SZArrayLdElema(V_28, L_55)), 1);
		if (!L_56)
		{
			goto IL_04a9;
		}
	}
	{
		V_31 = ((int32_t)(V_31+1));
	}

IL_04a9:
	{
		V_30 = ((int32_t)(V_30+1));
	}

IL_04af:
	{
		if ((((int32_t)V_30) < ((int32_t)(((int32_t)(((t20 *)V_28)->max_length))))))
		{
			goto IL_0496;
		}
	}
	{
		V_32 = ((t1365*)SZArrayNew(InitializedTypeInfo(&t1365_TI), V_31));
		V_31 = 0;
		V_30 = 0;
		goto IL_04eb;
	}

IL_04c8:
	{
		int32_t L_57 = V_30;
		t557 * L_58 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10177_MI, (*(t1146 **)(t1146 **)SZArrayLdElema(V_28, L_57)), 1);
		V_33 = L_58;
		if (!V_33)
		{
			goto IL_04e5;
		}
	}
	{
		int32_t L_59 = V_31;
		V_31 = ((int32_t)(L_59+1));
		ArrayElementTypeCheck (V_32, V_33);
		*((t636 **)(t636 **)SZArrayLdElema(V_32, L_59)) = (t636 *)V_33;
	}

IL_04e5:
	{
		V_30 = ((int32_t)(V_30+1));
	}

IL_04eb:
	{
		if ((((int32_t)V_30) < ((int32_t)(((int32_t)(((t20 *)V_28)->max_length))))))
		{
			goto IL_04c8;
		}
	}
	{
		t636 * L_60 = (t636 *)VirtFuncInvoker7< t636 *, int32_t, t1365*, t316**, t634*, t633 *, t446*, t29 ** >::Invoke(&m10147_MI, p2, p1, V_32, (&p4), p5, p6, p7, (&V_29));
		V_34 = L_60;
		if (V_34)
		{
			goto IL_0511;
		}
	}
	{
		V_8 = 1;
		goto IL_056a;
	}

IL_0511:
	{
		t638* L_61 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_34);
		V_35 = L_61;
		if ((((int32_t)(((int32_t)(((t20 *)V_35)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_053b;
		}
	}
	{
		int32_t L_62 = ((int32_t)((((int32_t)(((t20 *)V_35)->max_length)))-1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_63 = m1554(NULL, LoadTypeToken(&t391_0_0_0), &m1554_MI);
		bool L_64 = m5292(NULL, (*(t637 **)(t637 **)SZArrayLdElema(V_35, L_62)), L_63, &m5292_MI);
		G_B111_0 = ((int32_t)(L_64));
		goto IL_053c;
	}

IL_053b:
	{
		G_B111_0 = 0;
	}

IL_053c:
	{
		V_36 = G_B111_0;
		if (!V_36)
		{
			goto IL_054c;
		}
	}
	{
		m9373(__this, (&p4), V_34, &m9373_MI);
	}

IL_054c:
	{
		t29 * L_65 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_34, p3, p1, p2, p4, p6);
		V_37 = L_65;
		VirtActionInvoker2< t316**, t29 * >::Invoke(&m10148_MI, p2, (&p4), V_29);
		return V_37;
	}

IL_056a:
	{
		if (!V_7)
		{
			goto IL_0576;
		}
	}
	{
		t1653 * L_66 = (t1653 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1653_TI));
		m9304(L_66, V_7, &m9304_MI);
		il2cpp_codegen_raise_exception(L_66);
	}

IL_0576:
	{
		if (!V_8)
		{
			goto IL_0590;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_67 = m1685(NULL, (t7*) &_stringLiteral2212, p0, (t7*) &_stringLiteral52, &m1685_MI);
		t1651 * L_68 = (t1651 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1651_TI));
		m9294(L_68, L_67, &m9294_MI);
		il2cpp_codegen_raise_exception(L_68);
	}

IL_0590:
	{
		return NULL;
	}
}
 t42 * m9349 (t1660 * __this, MethodInfo* method){
	typedef t42 * (*m9349_ftn) (t1660 *);
	static m9349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9349_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetElementType()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9350_MI;
 t42 * m9350 (t1660 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m9351_MI;
 t929 * m9351 (t1660 * __this, MethodInfo* method){
	typedef t929 * (*m9351_ftn) (t1660 *);
	static m9351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9351_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_Assembly()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9352_MI;
 t7* m9352 (t1660 * __this, MethodInfo* method){
	{
		t7* L_0 = m9353(__this, 1, 1, &m9353_MI);
		return L_0;
	}
}
 t7* m9353 (t1660 * __this, bool p0, bool p1, MethodInfo* method){
	typedef t7* (*m9353_ftn) (t1660 *, bool, bool);
	static m9353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9353_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::getFullName(System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(__this, p0, p1);
}
extern MethodInfo m9354_MI;
 t42 * m9354 (t1660 * __this, MethodInfo* method){
	typedef t42 * (*m9354_ftn) (t1660 *);
	static m9354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9354_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_BaseType()");
	return _il2cpp_icall_func(__this);
}
 t7* m9355 (t1660 * __this, MethodInfo* method){
	t7* V_0 = {0};
	t7* V_1 = {0};
	{
		t1659 * L_0 = (__this->f8);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		t1659 * L_1 = (t1659 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1659_TI));
		m9326(L_1, &m9326_MI);
		__this->f8 = L_1;
	}

IL_0013:
	{
		t1659 * L_2 = (__this->f8);
		t7* L_3 = (L_2->f0);
		t7* L_4 = L_3;
		V_0 = L_4;
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		t1659 * L_5 = (__this->f8);
		t7* L_6 = m9353(__this, 1, 0, &m9353_MI);
		t7* L_7 = L_6;
		V_1 = L_7;
		L_5->f0 = L_7;
		V_0 = V_1;
	}

IL_0039:
	{
		return V_0;
	}
}
extern MethodInfo m9356_MI;
 bool m9356 (t1660 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
extern MethodInfo m9357_MI;
 t316* m9357 (t1660 * __this, bool p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9319(NULL, __this, p0, &m9319_MI);
		return L_0;
	}
}
extern MethodInfo m9358_MI;
 t316* m9358 (t1660 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral863, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_1 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_1;
	}
}
extern MethodInfo m9359_MI;
 int32_t m9359 (t1660 * __this, MethodInfo* method){
	{
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9363_MI, __this);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m9370_MI, __this);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (int32_t)(((int32_t)128));
	}

IL_0016:
	{
		return (int32_t)(((int32_t)32));
	}
}
extern MethodInfo m9360_MI;
 t7* m9360 (t1660 * __this, MethodInfo* method){
	typedef t7* (*m9360_ftn) (t1660 *);
	static m9360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9360_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_Name()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9361_MI;
 t7* m9361 (t1660 * __this, MethodInfo* method){
	typedef t7* (*m9361_ftn) (t1660 *);
	static m9361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9361_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_Namespace()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9362_MI;
 t1142 * m9362 (t1660 * __this, MethodInfo* method){
	typedef t1142 * (*m9362_ftn) (t1660 *);
	static m9362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9362_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_Module()");
	return _il2cpp_icall_func(__this);
}
 t42 * m9363 (t1660 * __this, MethodInfo* method){
	typedef t42 * (*m9363_ftn) (t1660 *);
	static m9363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9363_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_DeclaringType()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9364_MI;
 t42 * m9364 (t1660 * __this, MethodInfo* method){
	{
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9363_MI, __this);
		return L_0;
	}
}
extern MethodInfo m9365_MI;
 t43  m9365 (t1660 * __this, MethodInfo* method){
	{
		t43  L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m9366_MI;
 void m9366 (t1660 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9585(NULL, __this, p0, p1, &m9585_MI);
		return;
	}
}
extern MethodInfo m9367_MI;
 t7* m9367 (t1660 * __this, MethodInfo* method){
	{
		t7* L_0 = m9353(__this, 0, 0, &m9353_MI);
		return L_0;
	}
}
 t537* m9368 (t1660 * __this, MethodInfo* method){
	typedef t537* (*m9368_ftn) (t1660 *);
	static m9368_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9368_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::GetGenericArguments()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9369_MI;
 bool m9369 (t1660 * __this, MethodInfo* method){
	t42 * V_0 = {0};
	t537* V_1 = {0};
	int32_t V_2 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m9370_MI, __this);
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 1;
	}

IL_000a:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6042_MI, __this);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		t537* L_2 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m9368_MI, __this);
		V_1 = L_2;
		V_2 = 0;
		goto IL_002f;
	}

IL_001d:
	{
		int32_t L_3 = V_2;
		V_0 = (*(t42 **)(t42 **)SZArrayLdElema(V_1, L_3));
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6038_MI, V_0);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_002f:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_001d;
		}
	}

IL_0035:
	{
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5991_MI, __this);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9349_MI, __this);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6038_MI, L_6);
		return L_7;
	}

IL_0049:
	{
		return 0;
	}
}
 bool m9370 (t1660 * __this, MethodInfo* method){
	typedef bool (*m9370_ftn) (t1660 *);
	static m9370_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9370_ftn)il2cpp_codegen_resolve_icall ("System.MonoType::get_IsGenericParameter()");
	return _il2cpp_icall_func(__this);
}
extern MethodInfo m9371_MI;
 t42 * m9371 (t1660 * __this, MethodInfo* method){
	t42 * V_0 = {0};
	{
		t42 * L_0 = m6040(__this, &m6040_MI);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0010;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0010:
	{
		return V_0;
	}
}
 t636 * m9372 (t1660 * __this, t636 * p0, MethodInfo* method){
	{
		return p0;
	}
}
 void m9373 (t1660 * __this, t316** p0, t636 * p1, MethodInfo* method){
	t638* V_0 = {0};
	t316* V_1 = {0};
	t20 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t638* L_0 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, p1);
		V_0 = L_0;
		V_1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), (((int32_t)(((t20 *)V_0)->max_length)))));
		int32_t L_1 = ((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))-1));
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_1)));
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, L_2);
		t20 * L_4 = m5936(NULL, L_3, ((int32_t)((((int32_t)(((t20 *)(*((t316**)p0)))->max_length)))-((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))-1)))), &m5936_MI);
		V_2 = L_4;
		V_3 = 0;
		V_4 = 0;
		goto IL_0062;
	}

IL_0038:
	{
		if ((((int32_t)V_4) >= ((int32_t)((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))-1)))))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_5 = V_4;
		ArrayElementTypeCheck (V_1, (*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p0)), L_5)));
		*((t29 **)(t29 **)SZArrayLdElema(V_1, V_4)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p0)), L_5));
		goto IL_005c;
	}

IL_004c:
	{
		int32_t L_6 = V_4;
		m3971(V_2, (*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p0)), L_6)), V_3, &m3971_MI);
		V_3 = ((int32_t)(V_3+1));
	}

IL_005c:
	{
		V_4 = ((int32_t)(V_4+1));
	}

IL_0062:
	{
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((t20 *)(*((t316**)p0)))->max_length))))))
		{
			goto IL_0038;
		}
	}
	{
		ArrayElementTypeCheck (V_1, V_2);
		*((t29 **)(t29 **)SZArrayLdElema(V_1, ((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))-1)))) = (t29 *)V_2;
		*((t29 **)(p0)) = (t29 *)V_1;
		return;
	}
}
// Metadata Definition System.MonoType
extern Il2CppType t1659_0_0_129;
FieldInfo t1660_f8_FieldInfo = 
{
	"type_info", &t1659_0_0_129, &t1660_TI, offsetof(t1660, f8), &EmptyCustomAttributesCache};
static FieldInfo* t1660_FIs[] =
{
	&t1660_f8_FieldInfo,
	NULL
};
static PropertyInfo t1660____UnderlyingSystemType_PropertyInfo = 
{
	&t1660_TI, "UnderlyingSystemType", &m9350_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____Assembly_PropertyInfo = 
{
	&t1660_TI, "Assembly", &m9351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____AssemblyQualifiedName_PropertyInfo = 
{
	&t1660_TI, "AssemblyQualifiedName", &m9352_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____BaseType_PropertyInfo = 
{
	&t1660_TI, "BaseType", &m9354_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____FullName_PropertyInfo = 
{
	&t1660_TI, "FullName", &m9355_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____MemberType_PropertyInfo = 
{
	&t1660_TI, "MemberType", &m9359_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____Name_PropertyInfo = 
{
	&t1660_TI, "Name", &m9360_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____Namespace_PropertyInfo = 
{
	&t1660_TI, "Namespace", &m9361_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____Module_PropertyInfo = 
{
	&t1660_TI, "Module", &m9362_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____DeclaringType_PropertyInfo = 
{
	&t1660_TI, "DeclaringType", &m9363_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____ReflectedType_PropertyInfo = 
{
	&t1660_TI, "ReflectedType", &m9364_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____TypeHandle_PropertyInfo = 
{
	&t1660_TI, "TypeHandle", &m9365_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____ContainsGenericParameters_PropertyInfo = 
{
	&t1660_TI, "ContainsGenericParameters", &m9369_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1660____IsGenericParameter_PropertyInfo = 
{
	&t1660_TI, "IsGenericParameter", &m9370_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1660_PIs[] =
{
	&t1660____UnderlyingSystemType_PropertyInfo,
	&t1660____Assembly_PropertyInfo,
	&t1660____AssemblyQualifiedName_PropertyInfo,
	&t1660____BaseType_PropertyInfo,
	&t1660____FullName_PropertyInfo,
	&t1660____MemberType_PropertyInfo,
	&t1660____Name_PropertyInfo,
	&t1660____Namespace_PropertyInfo,
	&t1660____Module_PropertyInfo,
	&t1660____DeclaringType_PropertyInfo,
	&t1660____ReflectedType_PropertyInfo,
	&t1660____TypeHandle_PropertyInfo,
	&t1660____ContainsGenericParameters_PropertyInfo,
	&t1660____IsGenericParameter_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1660_m9327_ParameterInfos[] = 
{
	{"type", 0, 134224161, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1148_0_0_0;
extern void* RuntimeInvoker_t1148_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9327_MI = 
{
	"get_attributes", (methodPointerType)&m9327, &t1660_TI, &t1148_0_0_0, RuntimeInvoker_t1148_t29, t1660_m9327_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 1, false, false, 5166, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t660_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9328_MI = 
{
	"GetDefaultConstructor", (methodPointerType)&m9328, &t1660_TI, &t660_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 5167, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1148_0_0_0;
extern void* RuntimeInvoker_t1148 (MethodInfo* method, void* obj, void** args);
MethodInfo m9329_MI = 
{
	"GetAttributeFlagsImpl", (methodPointerType)&m9329, &t1660_TI, &t1148_0_0_0, RuntimeInvoker_t1148, NULL, &EmptyCustomAttributesCache, 196, 0, 57, 0, false, false, 5168, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t1150_0_0_0;
extern Il2CppType t1150_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t1660_m9330_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224162, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 1, 134224163, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"callConvention", 2, 134224164, &EmptyCustomAttributesCache, &t1150_0_0_0},
	{"types", 3, 134224165, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 4, 134224166, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t660_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9330_MI = 
{
	"GetConstructorImpl", (methodPointerType)&m9330, &t1660_TI, &t660_0_0_0, RuntimeInvoker_t29_t44_t29_t44_t29_t29, t1660_m9330_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 56, 5, false, false, 5169, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1660_m9331_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224167, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"reflected_type", 1, 134224168, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1147_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9331_MI = 
{
	"GetConstructors_internal", (methodPointerType)&m9331, &t1660_TI, &t1147_0_0_0, RuntimeInvoker_t29_t44_t29, t1660_m9331_ParameterInfos, &EmptyCustomAttributesCache, 131, 4096, 255, 2, false, false, 5170, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
static ParameterInfo t1660_m9332_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224169, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t1147_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9332_MI = 
{
	"GetConstructors", (methodPointerType)&m9332, &t1660_TI, &t1147_0_0_0, RuntimeInvoker_t29_t44, t1660_m9332_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 69, 1, false, false, 5171, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
static ParameterInfo t1660_m9333_ParameterInfos[] = 
{
	{"name", 0, 134224170, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224171, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t1143_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9333_MI = 
{
	"InternalGetEvent", (methodPointerType)&m9333, &t1660_TI, &t1143_0_0_0, RuntimeInvoker_t29_t29_t44, t1660_m9333_ParameterInfos, &EmptyCustomAttributesCache, 129, 4096, 255, 2, false, false, 5172, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
static ParameterInfo t1660_m9334_ParameterInfos[] = 
{
	{"name", 0, 134224172, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224173, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t1143_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9334_MI = 
{
	"GetEvent", (methodPointerType)&m9334, &t1660_TI, &t1143_0_0_0, RuntimeInvoker_t29_t29_t44, t1660_m9334_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 43, 2, false, false, 5173, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
static ParameterInfo t1660_m9335_ParameterInfos[] = 
{
	{"name", 0, 134224174, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224175, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t1144_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9335_MI = 
{
	"GetField", (methodPointerType)&m9335, &t1660_TI, &t1144_0_0_0, RuntimeInvoker_t29_t29_t44, t1660_m9335_ParameterInfos, &EmptyCustomAttributesCache, 198, 4096, 44, 2, false, false, 5174, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9336_MI = 
{
	"GetInterfaces", (methodPointerType)&m9336, &t1660_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 4096, 39, 0, false, false, 5175, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1660_m9337_ParameterInfos[] = 
{
	{"name", 0, 134224176, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224177, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"ignoreCase", 2, 134224178, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reflected_type", 3, 134224179, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1145_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9337_MI = 
{
	"GetMethodsByName", (methodPointerType)&m9337, &t1660_TI, &t1145_0_0_0, RuntimeInvoker_t29_t29_t44_t297_t29, t1660_m9337_ParameterInfos, &EmptyCustomAttributesCache, 131, 4096, 255, 4, false, false, 5176, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
static ParameterInfo t1660_m9338_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224180, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t1145_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9338_MI = 
{
	"GetMethods", (methodPointerType)&m9338, &t1660_TI, &t1145_0_0_0, RuntimeInvoker_t29_t44, t1660_m9338_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 50, 1, false, false, 5177, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t1150_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t1660_m9339_ParameterInfos[] = 
{
	{"name", 0, 134224181, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224182, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134224183, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"callConvention", 3, 134224184, &EmptyCustomAttributesCache, &t1150_0_0_0},
	{"types", 4, 134224185, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 5, 134224186, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9339_MI = 
{
	"GetMethodImpl", (methodPointerType)&m9339, &t1660_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t44_t29_t29, t1660_m9339_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 49, 6, false, false, 5178, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1660_m9340_ParameterInfos[] = 
{
	{"name", 0, 134224187, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224188, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"icase", 2, 134224189, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reflected_type", 3, 134224190, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1366_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9340_MI = 
{
	"GetPropertiesByName", (methodPointerType)&m9340, &t1660_TI, &t1366_0_0_0, RuntimeInvoker_t29_t29_t44_t297_t29, t1660_m9340_ParameterInfos, &EmptyCustomAttributesCache, 131, 4096, 255, 4, false, false, 5179, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t1660_m9341_ParameterInfos[] = 
{
	{"name", 0, 134224191, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"bindingAttr", 1, 134224192, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134224193, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"returnType", 3, 134224194, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"types", 4, 134224195, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 5, 134224196, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9341_MI = 
{
	"GetPropertyImpl", (methodPointerType)&m9341, &t1660_TI, &t1146_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29_t29, t1660_m9341_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 55, 6, false, false, 5180, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9342_MI = 
{
	"HasElementTypeImpl", (methodPointerType)&m9342, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 196, 0, 58, 0, false, false, 5181, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9343_MI = 
{
	"IsArrayImpl", (methodPointerType)&m9343, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 196, 0, 59, 0, false, false, 5182, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9344_MI = 
{
	"IsByRefImpl", (methodPointerType)&m9344, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 196, 4096, 60, 0, false, false, 5183, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9345_MI = 
{
	"IsPointerImpl", (methodPointerType)&m9345, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 196, 4096, 61, 0, false, false, 5184, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9346_MI = 
{
	"IsPrimitiveImpl", (methodPointerType)&m9346, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 196, 4096, 62, 0, false, false, 5185, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1660_m9347_ParameterInfos[] = 
{
	{"type", 0, 134224197, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9347_MI = 
{
	"IsSubclassOf", (methodPointerType)&m9347, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1660_m9347_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 38, 1, false, false, 5186, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t633_0_0_0;
extern Il2CppType t633_0_0_0;
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_0_0_0;
static ParameterInfo t1660_m9348_ParameterInfos[] = 
{
	{"name", 0, 134224198, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"invokeAttr", 1, 134224199, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134224200, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"target", 3, 134224201, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"args", 4, 134224202, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"modifiers", 5, 134224203, &EmptyCustomAttributesCache, &t634_0_0_0},
	{"culture", 6, 134224204, &EmptyCustomAttributesCache, &t633_0_0_0},
	{"namedParameters", 7, 134224205, &EmptyCustomAttributesCache, &t446_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9348_MI = 
{
	"InvokeMember", (methodPointerType)&m9348, &t1660_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29_t29_t29_t29, t1660_m9348_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 70, 8, false, false, 5187, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9349_MI = 
{
	"GetElementType", (methodPointerType)&m9349, &t1660_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 4096, 42, 0, false, false, 5188, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9350_MI = 
{
	"get_UnderlyingSystemType", (methodPointerType)&m9350, &t1660_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 36, 0, false, false, 5189, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t929_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9351_MI = 
{
	"get_Assembly", (methodPointerType)&m9351, &t1660_TI, &t929_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 14, 0, false, false, 5190, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9352_MI = 
{
	"get_AssemblyQualifiedName", (methodPointerType)&m9352, &t1660_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 15, 0, false, false, 5191, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1660_m9353_ParameterInfos[] = 
{
	{"full_name", 0, 134224206, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"assembly_qualified", 1, 134224207, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9353_MI = 
{
	"getFullName", (methodPointerType)&m9353, &t1660_TI, &t7_0_0_0, RuntimeInvoker_t29_t297_t297, t1660_m9353_ParameterInfos, &EmptyCustomAttributesCache, 129, 4096, 255, 2, false, false, 5192, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9354_MI = 
{
	"get_BaseType", (methodPointerType)&m9354, &t1660_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 17, 0, false, false, 5193, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9355_MI = 
{
	"get_FullName", (methodPointerType)&m9355, &t1660_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 18, 0, false, false, 5194, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1660_m9356_ParameterInfos[] = 
{
	{"attributeType", 0, 134224208, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134224209, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9356_MI = 
{
	"IsDefined", (methodPointerType)&m9356, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1660_m9356_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 5195, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1660_m9357_ParameterInfos[] = 
{
	{"inherit", 0, 134224210, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9357_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m9357, &t1660_TI, &t316_0_0_0, RuntimeInvoker_t29_t297, t1660_m9357_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 5196, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1660_m9358_ParameterInfos[] = 
{
	{"attributeType", 0, 134224211, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134224212, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9358_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m9358, &t1660_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1660_m9358_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 13, 2, false, false, 5197, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
MethodInfo m9359_MI = 
{
	"get_MemberType", (methodPointerType)&m9359, &t1660_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2246, 0, 7, 0, false, false, 5198, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9360_MI = 
{
	"get_Name", (methodPointerType)&m9360, &t1660_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 8, 0, false, false, 5199, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9361_MI = 
{
	"get_Namespace", (methodPointerType)&m9361, &t1660_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 34, 0, false, false, 5200, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1142_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9362_MI = 
{
	"get_Module", (methodPointerType)&m9362, &t1660_TI, &t1142_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 10, 0, false, false, 5201, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9363_MI = 
{
	"get_DeclaringType", (methodPointerType)&m9363, &t1660_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 6, 0, false, false, 5202, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9364_MI = 
{
	"get_ReflectedType", (methodPointerType)&m9364, &t1660_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 9, 0, false, false, 5203, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t43_0_0_0;
extern void* RuntimeInvoker_t43 (MethodInfo* method, void* obj, void** args);
MethodInfo m9365_MI = 
{
	"get_TypeHandle", (methodPointerType)&m9365, &t1660_TI, &t43_0_0_0, RuntimeInvoker_t43, NULL, &EmptyCustomAttributesCache, 2246, 0, 35, 0, false, false, 5204, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1660_m9366_ParameterInfos[] = 
{
	{"info", 0, 134224213, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224214, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9366_MI = 
{
	"GetObjectData", (methodPointerType)&m9366, &t1660_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1660_m9366_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 78, 2, false, false, 5205, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9367_MI = 
{
	"ToString", (methodPointerType)&m9367, &t1660_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 5206, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9368_MI = 
{
	"GetGenericArguments", (methodPointerType)&m9368, &t1660_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 4096, 71, 0, false, false, 5207, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9369_MI = 
{
	"get_ContainsGenericParameters", (methodPointerType)&m9369, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 72, 0, false, false, 5208, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9370_MI = 
{
	"get_IsGenericParameter", (methodPointerType)&m9370, &t1660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 4096, 77, 0, false, false, 5209, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9371_MI = 
{
	"GetGenericTypeDefinition", (methodPointerType)&m9371, &t1660_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 74, 0, false, false, 5210, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t1660_m9372_ParameterInfos[] = 
{
	{"mb", 0, 134224215, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9372_MI = 
{
	"CheckMethodSecurity", (methodPointerType)&m9372, &t1660_TI, &t636_0_0_0, RuntimeInvoker_t29_t29, t1660_m9372_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5211, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_1_0_0;
extern Il2CppType t316_1_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t1660_m9373_ParameterInfos[] = 
{
	{"args", 0, 134224216, &EmptyCustomAttributesCache, &t316_1_0_0},
	{"method", 1, 134224217, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1704_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9373_MI = 
{
	"ReorderParamArrayArguments", (methodPointerType)&m9373, &t1660_TI, &t21_0_0_0, RuntimeInvoker_t21_t1704_t29, t1660_m9373_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5212, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1660_MIs[] =
{
	&m9327_MI,
	&m9328_MI,
	&m9329_MI,
	&m9330_MI,
	&m9331_MI,
	&m9332_MI,
	&m9333_MI,
	&m9334_MI,
	&m9335_MI,
	&m9336_MI,
	&m9337_MI,
	&m9338_MI,
	&m9339_MI,
	&m9340_MI,
	&m9341_MI,
	&m9342_MI,
	&m9343_MI,
	&m9344_MI,
	&m9345_MI,
	&m9346_MI,
	&m9347_MI,
	&m9348_MI,
	&m9349_MI,
	&m9350_MI,
	&m9351_MI,
	&m9352_MI,
	&m9353_MI,
	&m9354_MI,
	&m9355_MI,
	&m9356_MI,
	&m9357_MI,
	&m9358_MI,
	&m9359_MI,
	&m9360_MI,
	&m9361_MI,
	&m9362_MI,
	&m9363_MI,
	&m9364_MI,
	&m9365_MI,
	&m9366_MI,
	&m9367_MI,
	&m9368_MI,
	&m9369_MI,
	&m9370_MI,
	&m9371_MI,
	&m9372_MI,
	&m9373_MI,
	NULL
};
extern MethodInfo m6008_MI;
extern MethodInfo m6021_MI;
extern MethodInfo m5989_MI;
extern MethodInfo m5992_MI;
extern MethodInfo m5993_MI;
extern MethodInfo m5994_MI;
extern MethodInfo m5995_MI;
extern MethodInfo m5996_MI;
extern MethodInfo m5997_MI;
extern MethodInfo m5998_MI;
extern MethodInfo m5999_MI;
extern MethodInfo m6000_MI;
extern MethodInfo m6001_MI;
extern MethodInfo m3000_MI;
extern MethodInfo m6003_MI;
extern MethodInfo m6009_MI;
extern MethodInfo m6020_MI;
extern MethodInfo m6022_MI;
extern MethodInfo m6023_MI;
extern MethodInfo m2999_MI;
extern MethodInfo m6024_MI;
extern MethodInfo m6025_MI;
extern MethodInfo m6028_MI;
extern MethodInfo m6030_MI;
extern MethodInfo m6031_MI;
extern MethodInfo m6032_MI;
extern MethodInfo m2980_MI;
extern MethodInfo m6033_MI;
extern MethodInfo m6039_MI;
extern MethodInfo m2979_MI;
static MethodInfo* t1660_VT[] =
{
	&m6008_MI,
	&m46_MI,
	&m6021_MI,
	&m9367_MI,
	&m9358_MI,
	&m9356_MI,
	&m9363_MI,
	&m9359_MI,
	&m9360_MI,
	&m9364_MI,
	&m9362_MI,
	&m9356_MI,
	&m9357_MI,
	&m9358_MI,
	&m9351_MI,
	&m9352_MI,
	&m5989_MI,
	&m9354_MI,
	&m9355_MI,
	&m5991_MI,
	&m5992_MI,
	&m5993_MI,
	&m5994_MI,
	&m5995_MI,
	&m5996_MI,
	&m5997_MI,
	&m5998_MI,
	&m5999_MI,
	&m6000_MI,
	&m6001_MI,
	&m3000_MI,
	&m6002_MI,
	&m6003_MI,
	&m6004_MI,
	&m9361_MI,
	&m9365_MI,
	&m9350_MI,
	&m6009_MI,
	&m9347_MI,
	&m9336_MI,
	&m2981_MI,
	&m6020_MI,
	&m9349_MI,
	&m9334_MI,
	&m9335_MI,
	&m6022_MI,
	&m6023_MI,
	&m2999_MI,
	&m6024_MI,
	&m9339_MI,
	&m9338_MI,
	&m6025_MI,
	&m6026_MI,
	&m6027_MI,
	&m6028_MI,
	&m9341_MI,
	&m9330_MI,
	&m9329_MI,
	&m9342_MI,
	&m9343_MI,
	&m9344_MI,
	&m9345_MI,
	&m9346_MI,
	&m6030_MI,
	&m6031_MI,
	&m6032_MI,
	&m2980_MI,
	&m6033_MI,
	&m6034_MI,
	&m9332_MI,
	&m9348_MI,
	&m9368_MI,
	&m9369_MI,
	&m6039_MI,
	&m9371_MI,
	&m6042_MI,
	&m2979_MI,
	&m9370_MI,
	&m9366_MI,
};
static TypeInfo* t1660_ITIs[] = 
{
	&t374_TI,
};
extern TypeInfo t1913_TI;
extern TypeInfo t1914_TI;
extern TypeInfo t1915_TI;
static Il2CppInterfaceOffsetPair t1660_IOs[] = 
{
	{ &t1913_TI, 14},
	{ &t1914_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t374_TI, 78},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1660_0_0_0;
extern Il2CppType t1660_1_0_0;
struct t1660;
TypeInfo t1660_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoType", "System", t1660_MIs, t1660_PIs, t1660_FIs, NULL, &t42_TI, NULL, NULL, &t1660_TI, t1660_ITIs, t1660_VT, &EmptyCustomAttributesCache, &t1660_TI, &t1660_0_0_0, &t1660_1_0_0, t1660_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1660), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 47, 14, 1, 0, 0, 79, 1, 5};
#include "t1661.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1661_TI;
#include "t1661MD.h"



extern MethodInfo m9374_MI;
 void m9374 (t1661 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2213, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		return;
	}
}
extern MethodInfo m9375_MI;
 void m9375 (t1661 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		return;
	}
}
extern MethodInfo m9376_MI;
 void m9376 (t1661 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.MulticastNotSupportedException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9374_MI = 
{
	".ctor", (methodPointerType)&m9374, &t1661_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5213, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1661_m9375_ParameterInfos[] = 
{
	{"message", 0, 134224218, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9375_MI = 
{
	".ctor", (methodPointerType)&m9375, &t1661_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1661_m9375_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5214, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1661_m9376_ParameterInfos[] = 
{
	{"info", 0, 134224219, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224220, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9376_MI = 
{
	".ctor", (methodPointerType)&m9376, &t1661_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1661_m9376_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 5215, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1661_MIs[] =
{
	&m9374_MI,
	&m9375_MI,
	&m9376_MI,
	NULL
};
static MethodInfo* t1661_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1661_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1661__CustomAttributeCache = {
1,
NULL,
&t1661_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1661_0_0_0;
extern Il2CppType t1661_1_0_0;
struct t1661;
extern CustomAttributesCache t1661__CustomAttributeCache;
TypeInfo t1661_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MulticastNotSupportedException", "System", t1661_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t1661_TI, NULL, t1661_VT, &t1661__CustomAttributeCache, &t1661_TI, &t1661_0_0_0, &t1661_1_0_0, t1661_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1661), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1662.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1662_TI;
#include "t1662MD.h"

extern MethodInfo m2881_MI;


extern MethodInfo m9377_MI;
 void m9377 (t1662 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition System.NonSerializedAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9377_MI = 
{
	".ctor", (methodPointerType)&m9377, &t1662_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5216, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1662_MIs[] =
{
	&m9377_MI,
	NULL
};
extern MethodInfo m2882_MI;
extern MethodInfo m2883_MI;
static MethodInfo* t1662_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
extern TypeInfo t604_TI;
static Il2CppInterfaceOffsetPair t1662_IOs[] = 
{
	{ &t604_TI, 4},
};
extern MethodInfo m2916_MI;
void t1662_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1662__CustomAttributeCache = {
2,
NULL,
&t1662_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1662_0_0_0;
extern Il2CppType t1662_1_0_0;
struct t1662;
extern CustomAttributesCache t1662__CustomAttributeCache;
TypeInfo t1662_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NonSerializedAttribute", "System", t1662_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t1662_TI, NULL, t1662_VT, &t1662__CustomAttributeCache, &t1662_TI, &t1662_0_0_0, &t1662_1_0_0, t1662_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1662), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t930.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t930_TI;
#include "t930MD.h"



extern MethodInfo m9378_MI;
 void m9378 (t930 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2214, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2147467263), &m2946_MI);
		return;
	}
}
extern MethodInfo m4036_MI;
 void m4036 (t930 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2147467263), &m2946_MI);
		return;
	}
}
extern MethodInfo m9379_MI;
 void m9379 (t930 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.NotImplementedException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9378_MI = 
{
	".ctor", (methodPointerType)&m9378, &t930_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5217, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t930_m4036_ParameterInfos[] = 
{
	{"message", 0, 134224221, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4036_MI = 
{
	".ctor", (methodPointerType)&m4036, &t930_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t930_m4036_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5218, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t930_m9379_ParameterInfos[] = 
{
	{"info", 0, 134224222, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224223, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9379_MI = 
{
	".ctor", (methodPointerType)&m9379, &t930_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t930_m9379_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5219, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t930_MIs[] =
{
	&m9378_MI,
	&m4036_MI,
	&m9379_MI,
	NULL
};
static MethodInfo* t930_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t930_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t930_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t930__CustomAttributeCache = {
1,
NULL,
&t930_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t930_0_0_0;
extern Il2CppType t930_1_0_0;
struct t930;
extern CustomAttributesCache t930__CustomAttributeCache;
TypeInfo t930_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NotImplementedException", "System", t930_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t930_TI, NULL, t930_VT, &t930__CustomAttributeCache, &t930_TI, &t930_0_0_0, &t930_1_0_0, t930_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t930), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t345.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t345_TI;
#include "t345MD.h"



extern MethodInfo m1516_MI;
 void m1516 (t345 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2215, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233067), &m2946_MI);
		return;
	}
}
extern MethodInfo m3988_MI;
 void m3988 (t345 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233067), &m2946_MI);
		return;
	}
}
extern MethodInfo m9380_MI;
 void m9380 (t345 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.NotSupportedException
extern Il2CppType t44_0_0_32849;
FieldInfo t345_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t345_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t345_FIs[] =
{
	&t345_f11_FieldInfo,
	NULL
};
static const int32_t t345_f11_DefaultValueData = -2146233067;
static Il2CppFieldDefaultValueEntry t345_f11_DefaultValue = 
{
	&t345_f11_FieldInfo, { (char*)&t345_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t345_FDVs[] = 
{
	&t345_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1516_MI = 
{
	".ctor", (methodPointerType)&m1516, &t345_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5220, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t345_m3988_ParameterInfos[] = 
{
	{"message", 0, 134224224, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3988_MI = 
{
	".ctor", (methodPointerType)&m3988, &t345_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t345_m3988_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5221, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t345_m9380_ParameterInfos[] = 
{
	{"info", 0, 134224225, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224226, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9380_MI = 
{
	".ctor", (methodPointerType)&m9380, &t345_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t345_m9380_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5222, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t345_MIs[] =
{
	&m1516_MI,
	&m3988_MI,
	&m9380_MI,
	NULL
};
static MethodInfo* t345_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t345_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t345__CustomAttributeCache = {
1,
NULL,
&t345_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t345_0_0_0;
extern Il2CppType t345_1_0_0;
struct t345;
extern CustomAttributesCache t345__CustomAttributeCache;
TypeInfo t345_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NotSupportedException", "System", t345_MIs, NULL, t345_FIs, NULL, &t956_TI, NULL, NULL, &t345_TI, NULL, t345_VT, &t345__CustomAttributeCache, &t345_TI, &t345_0_0_0, &t345_1_0_0, t345_IOs, NULL, NULL, t345_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t345), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 11, 0, 2};
#include "t585.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t585_TI;
#include "t585MD.h"



extern MethodInfo m9381_MI;
 void m9381 (t585 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2216, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2147467261), &m2946_MI);
		return;
	}
}
extern MethodInfo m2837_MI;
 void m2837 (t585 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2147467261), &m2946_MI);
		return;
	}
}
extern MethodInfo m9382_MI;
 void m9382 (t585 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.NullReferenceException
extern Il2CppType t44_0_0_32849;
FieldInfo t585_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t585_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t585_FIs[] =
{
	&t585_f11_FieldInfo,
	NULL
};
static const int32_t t585_f11_DefaultValueData = -2147467261;
static Il2CppFieldDefaultValueEntry t585_f11_DefaultValue = 
{
	&t585_f11_FieldInfo, { (char*)&t585_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t585_FDVs[] = 
{
	&t585_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9381_MI = 
{
	".ctor", (methodPointerType)&m9381, &t585_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5223, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t585_m2837_ParameterInfos[] = 
{
	{"message", 0, 134224227, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2837_MI = 
{
	".ctor", (methodPointerType)&m2837, &t585_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t585_m2837_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5224, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t585_m9382_ParameterInfos[] = 
{
	{"info", 0, 134224228, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224229, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9382_MI = 
{
	".ctor", (methodPointerType)&m9382, &t585_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t585_m9382_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5225, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t585_MIs[] =
{
	&m9381_MI,
	&m2837_MI,
	&m9382_MI,
	NULL
};
static MethodInfo* t585_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t585_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t585_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t585__CustomAttributeCache = {
1,
NULL,
&t585_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t585_0_0_0;
extern Il2CppType t585_1_0_0;
struct t585;
extern CustomAttributesCache t585__CustomAttributeCache;
TypeInfo t585_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NullReferenceException", "System", t585_MIs, NULL, t585_FIs, NULL, &t956_TI, NULL, NULL, &t585_TI, NULL, t585_VT, &t585__CustomAttributeCache, &t585_TI, &t585_0_0_0, &t585_1_0_0, t585_IOs, NULL, NULL, t585_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t585), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 11, 0, 2};
#include "t1663.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1663_TI;
#include "t1663MD.h"

#include "t194.h"
#include "t1125.h"
#include "t292.h"
extern TypeInfo t841_TI;
extern TypeInfo t292_TI;
#include "t292MD.h"
#include "t1125MD.h"
extern MethodInfo m1741_MI;
extern MethodInfo m1715_MI;
extern MethodInfo m8852_MI;
extern MethodInfo m9383_MI;
extern MethodInfo m1311_MI;
extern MethodInfo m6943_MI;
extern MethodInfo m6942_MI;
extern MethodInfo m4184_MI;
extern MethodInfo m1761_MI;
extern MethodInfo m8662_MI;
extern MethodInfo m2927_MI;
extern MethodInfo m3993_MI;
extern MethodInfo m6941_MI;
extern MethodInfo m6952_MI;
extern MethodInfo m6953_MI;
extern MethodInfo m6939_MI;
extern MethodInfo m8672_MI;
extern MethodInfo m1315_MI;


 void m9383 (t1663 * __this, MethodInfo* method){
	{
		__this->f2 = (-1);
		__this->f10 = 1;
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m9384_MI;
 void m9384 (t29 * __this, t7* p0, bool* p1, bool p2, int32_t* p3, int32_t* p4, MethodInfo* method){
	t841* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	{
		V_0 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), 3));
		V_1 = 0;
		V_2 = 0;
		V_3 = 0;
		V_4 = 0;
		goto IL_007c;
	}

IL_0015:
	{
		uint16_t L_0 = m1741(p0, V_4, &m1741_MI);
		V_5 = L_0;
		if ((((int32_t)V_5) == ((int32_t)V_3)))
		{
			goto IL_0033;
		}
	}
	{
		if (V_3)
		{
			goto IL_003f;
		}
	}
	{
		if ((((int32_t)V_5) == ((int32_t)((int32_t)34))))
		{
			goto IL_0033;
		}
	}
	{
		if ((((uint32_t)V_5) != ((uint32_t)((int32_t)39))))
		{
			goto IL_003f;
		}
	}

IL_0033:
	{
		if (V_3)
		{
			goto IL_003b;
		}
	}
	{
		V_3 = V_5;
		goto IL_003d;
	}

IL_003b:
	{
		V_3 = 0;
	}

IL_003d:
	{
		goto IL_0076;
	}

IL_003f:
	{
		if (V_3)
		{
			goto IL_0076;
		}
	}
	{
		uint16_t L_1 = m1741(p0, V_4, &m1741_MI);
		if ((((uint32_t)L_1) != ((uint32_t)((int32_t)59))))
		{
			goto IL_0076;
		}
	}
	{
		if (!V_4)
		{
			goto IL_0060;
		}
	}
	{
		uint16_t L_2 = m1741(p0, ((int32_t)(V_4-1)), &m1741_MI);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)92))))
		{
			goto IL_0076;
		}
	}

IL_0060:
	{
		int32_t L_3 = V_1;
		V_1 = ((int32_t)(L_3+1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_0, L_3)) = (int32_t)((int32_t)(V_4-V_2));
		V_2 = ((int32_t)(V_4+1));
		if ((((uint32_t)V_1) != ((uint32_t)3)))
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0086;
	}

IL_0076:
	{
		V_4 = ((int32_t)(V_4+1));
	}

IL_007c:
	{
		int32_t L_4 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_4) < ((int32_t)L_4)))
		{
			goto IL_0015;
		}
	}

IL_0086:
	{
		if (V_1)
		{
			goto IL_0096;
		}
	}
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_5 = m1715(p0, &m1715_MI);
		*((int32_t*)(p4)) = (int32_t)L_5;
		return;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)1)))
		{
			goto IL_00d9;
		}
	}
	{
		if ((*((int8_t*)p1)))
		{
			goto IL_00a1;
		}
	}
	{
		if (!p2)
		{
			goto IL_00ab;
		}
	}

IL_00a1:
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_6 = 0;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_6));
		return;
	}

IL_00ab:
	{
		int32_t L_7 = 0;
		int32_t L_8 = m1715(p0, &m1715_MI);
		if ((((int32_t)((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_7))+1))) >= ((int32_t)L_8)))
		{
			goto IL_00cf;
		}
	}
	{
		*((int8_t*)(p1)) = (int8_t)1;
		int32_t L_9 = 0;
		*((int32_t*)(p3)) = (int32_t)((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_9))+1));
		int32_t L_10 = m1715(p0, &m1715_MI);
		*((int32_t*)(p4)) = (int32_t)((int32_t)(L_10-(*((int32_t*)p3))));
		return;
	}

IL_00cf:
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_11 = 0;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_11));
		return;
	}

IL_00d9:
	{
		if ((((uint32_t)V_1) != ((uint32_t)2)))
		{
			goto IL_0127;
		}
	}
	{
		if (!p2)
		{
			goto IL_00f8;
		}
	}
	{
		int32_t L_12 = 0;
		int32_t L_13 = 1;
		*((int32_t*)(p3)) = (int32_t)((int32_t)(((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_12))+(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_13))))+2));
		int32_t L_14 = m1715(p0, &m1715_MI);
		*((int32_t*)(p4)) = (int32_t)((int32_t)(L_14-(*((int32_t*)p3))));
		return;
	}

IL_00f8:
	{
		if (!(*((int8_t*)p1)))
		{
			goto IL_0106;
		}
	}
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_15 = 0;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_15));
		return;
	}

IL_0106:
	{
		int32_t L_16 = 1;
		if ((((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_16))) <= ((int32_t)0)))
		{
			goto IL_011d;
		}
	}
	{
		*((int8_t*)(p1)) = (int8_t)1;
		int32_t L_17 = 0;
		*((int32_t*)(p3)) = (int32_t)((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_17))+1));
		int32_t L_18 = 1;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_18));
		return;
	}

IL_011d:
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_19 = 0;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_19));
		return;
	}

IL_0127:
	{
		if ((((uint32_t)V_1) != ((uint32_t)3)))
		{
			goto IL_016f;
		}
	}
	{
		if (!p2)
		{
			goto IL_0140;
		}
	}
	{
		int32_t L_20 = 0;
		int32_t L_21 = 1;
		*((int32_t*)(p3)) = (int32_t)((int32_t)(((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_20))+(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_21))))+2));
		int32_t L_22 = 2;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_22));
		return;
	}

IL_0140:
	{
		if (!(*((int8_t*)p1)))
		{
			goto IL_014e;
		}
	}
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_23 = 0;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_23));
		return;
	}

IL_014e:
	{
		int32_t L_24 = 1;
		if ((((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_24))) <= ((int32_t)0)))
		{
			goto IL_0165;
		}
	}
	{
		*((int8_t*)(p1)) = (int8_t)1;
		int32_t L_25 = 0;
		*((int32_t*)(p3)) = (int32_t)((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_25))+1));
		int32_t L_26 = 1;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_26));
		return;
	}

IL_0165:
	{
		*((int32_t*)(p3)) = (int32_t)0;
		int32_t L_27 = 0;
		*((int32_t*)(p4)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_27));
		return;
	}

IL_016f:
	{
		t305 * L_28 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_28, &m8852_MI);
		il2cpp_codegen_raise_exception(L_28);
	}
}
extern MethodInfo m9385_MI;
 t1663 * m9385 (t29 * __this, t7* p0, int32_t p1, int32_t p2, t1125 * p3, MethodInfo* method){
	uint16_t V_0 = 0x0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	t1663 * V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	uint16_t V_8 = 0x0;
	uint16_t V_9 = 0x0;
	uint16_t V_10 = 0x0;
	{
		V_0 = 0;
		V_1 = 1;
		V_2 = 0;
		V_3 = 0;
		V_4 = 1;
		t1663 * L_0 = (t1663 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1663_TI));
		m9383(L_0, &m9383_MI);
		V_5 = L_0;
		V_6 = 0;
		V_7 = p1;
		goto IL_028d;
	}

IL_001d:
	{
		uint16_t L_1 = m1741(p0, V_7, &m1741_MI);
		V_8 = L_1;
		if ((((uint32_t)V_8) != ((uint32_t)V_0)))
		{
			goto IL_0037;
		}
	}
	{
		if (!V_8)
		{
			goto IL_0037;
		}
	}
	{
		V_0 = 0;
		goto IL_0287;
	}

IL_0037:
	{
		if (!V_0)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_0287;
	}

IL_003f:
	{
		if (!V_3)
		{
			goto IL_006f;
		}
	}
	{
		if (!V_8)
		{
			goto IL_006f;
		}
	}
	{
		if ((((int32_t)V_8) == ((int32_t)((int32_t)48))))
		{
			goto IL_006f;
		}
	}
	{
		if ((((int32_t)V_8) == ((int32_t)((int32_t)35))))
		{
			goto IL_006f;
		}
	}
	{
		V_3 = 0;
		int32_t L_2 = (V_5->f2);
		V_1 = ((((int32_t)L_2) < ((int32_t)0))? 1 : 0);
		V_2 = ((((int32_t)V_1) == ((int32_t)0))? 1 : 0);
		V_7 = ((int32_t)(V_7-1));
		goto IL_0287;
	}

IL_006f:
	{
		V_10 = V_8;
		if (((uint16_t)(V_10-((int32_t)34))) == 0)
		{
			goto IL_00e7;
		}
		if (((uint16_t)(V_10-((int32_t)34))) == 1)
		{
			goto IL_00fb;
		}
		if (((uint16_t)(V_10-((int32_t)34))) == 2)
		{
			goto IL_0095;
		}
		if (((uint16_t)(V_10-((int32_t)34))) == 3)
		{
			goto IL_024e;
		}
		if (((uint16_t)(V_10-((int32_t)34))) == 4)
		{
			goto IL_0095;
		}
		if (((uint16_t)(V_10-((int32_t)34))) == 5)
		{
			goto IL_00e7;
		}
	}

IL_0095:
	{
		if (((uint16_t)(V_10-((int32_t)44))) == 0)
		{
			goto IL_0270;
		}
		if (((uint16_t)(V_10-((int32_t)44))) == 1)
		{
			goto IL_00b3;
		}
		if (((uint16_t)(V_10-((int32_t)44))) == 2)
		{
			goto IL_0233;
		}
		if (((uint16_t)(V_10-((int32_t)44))) == 3)
		{
			goto IL_00b3;
		}
		if (((uint16_t)(V_10-((int32_t)44))) == 4)
		{
			goto IL_013b;
		}
	}

IL_00b3:
	{
		if ((((int32_t)V_10) == ((int32_t)((int32_t)69))))
		{
			goto IL_01be;
		}
	}
	{
		if ((((int32_t)V_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_00dc;
		}
	}
	{
		if ((((int32_t)V_10) == ((int32_t)((int32_t)101))))
		{
			goto IL_01be;
		}
	}
	{
		if ((((int32_t)V_10) == ((int32_t)((int32_t)8240))))
		{
			goto IL_025f;
		}
	}
	{
		goto IL_0285;
	}

IL_00dc:
	{
		V_7 = ((int32_t)(V_7+1));
		goto IL_0287;
	}

IL_00e7:
	{
		if ((((int32_t)V_8) == ((int32_t)((int32_t)34))))
		{
			goto IL_00f3;
		}
	}
	{
		if ((((uint32_t)V_8) != ((uint32_t)((int32_t)39))))
		{
			goto IL_00f6;
		}
	}

IL_00f3:
	{
		V_0 = V_8;
	}

IL_00f6:
	{
		goto IL_0287;
	}

IL_00fb:
	{
		if (!V_4)
		{
			goto IL_0113;
		}
	}
	{
		if (!V_1)
		{
			goto IL_0113;
		}
	}
	{
		t1663 * L_3 = V_5;
		int32_t L_4 = (L_3->f5);
		L_3->f5 = ((int32_t)(L_4+1));
		goto IL_0139;
	}

IL_0113:
	{
		if (!V_2)
		{
			goto IL_0127;
		}
	}
	{
		t1663 * L_5 = V_5;
		int32_t L_6 = (L_5->f3);
		L_5->f3 = ((int32_t)(L_6+1));
		goto IL_0139;
	}

IL_0127:
	{
		if (!V_3)
		{
			goto IL_0139;
		}
	}
	{
		t1663 * L_7 = V_5;
		int32_t L_8 = (L_7->f9);
		L_7->f9 = ((int32_t)(L_8+1));
	}

IL_0139:
	{
		goto IL_013b;
	}

IL_013b:
	{
		if ((((int32_t)V_8) == ((int32_t)((int32_t)35))))
		{
			goto IL_015c;
		}
	}
	{
		V_4 = 0;
		if (!V_2)
		{
			goto IL_0151;
		}
	}
	{
		V_5->f3 = 0;
		goto IL_015c;
	}

IL_0151:
	{
		if (!V_3)
		{
			goto IL_015c;
		}
	}
	{
		V_5->f9 = 0;
	}

IL_015c:
	{
		int32_t L_9 = (V_5->f6);
		if ((((uint32_t)L_9) != ((uint32_t)(-1))))
		{
			goto IL_016f;
		}
	}
	{
		V_5->f6 = V_7;
	}

IL_016f:
	{
		if (!V_1)
		{
			goto IL_0193;
		}
	}
	{
		t1663 * L_10 = V_5;
		int32_t L_11 = (L_10->f4);
		L_10->f4 = ((int32_t)(L_11+1));
		if ((((int32_t)V_6) <= ((int32_t)0)))
		{
			goto IL_018e;
		}
	}
	{
		V_5->f0 = 1;
	}

IL_018e:
	{
		V_6 = 0;
		goto IL_01b9;
	}

IL_0193:
	{
		if (!V_2)
		{
			goto IL_01a7;
		}
	}
	{
		t1663 * L_12 = V_5;
		int32_t L_13 = (L_12->f1);
		L_12->f1 = ((int32_t)(L_13+1));
		goto IL_01b9;
	}

IL_01a7:
	{
		if (!V_3)
		{
			goto IL_01b9;
		}
	}
	{
		t1663 * L_14 = V_5;
		int32_t L_15 = (L_14->f8);
		L_14->f8 = ((int32_t)(L_15+1));
	}

IL_01b9:
	{
		goto IL_0287;
	}

IL_01be:
	{
		bool L_16 = (V_5->f7);
		if (!L_16)
		{
			goto IL_01cc;
		}
	}
	{
		goto IL_0287;
	}

IL_01cc:
	{
		V_5->f7 = 1;
		V_1 = 0;
		V_2 = 0;
		V_3 = 1;
		if ((((int32_t)((int32_t)(((int32_t)(V_7+1))-p1))) >= ((int32_t)p2)))
		{
			goto IL_0231;
		}
	}
	{
		uint16_t L_17 = m1741(p0, ((int32_t)(V_7+1)), &m1741_MI);
		V_9 = L_17;
		if ((((uint32_t)V_9) != ((uint32_t)((int32_t)43))))
		{
			goto IL_01fd;
		}
	}
	{
		V_5->f10 = 0;
	}

IL_01fd:
	{
		if ((((int32_t)V_9) == ((int32_t)((int32_t)43))))
		{
			goto IL_0209;
		}
	}
	{
		if ((((uint32_t)V_9) != ((uint32_t)((int32_t)45))))
		{
			goto IL_0211;
		}
	}

IL_0209:
	{
		V_7 = ((int32_t)(V_7+1));
		goto IL_0231;
	}

IL_0211:
	{
		if ((((int32_t)V_9) == ((int32_t)((int32_t)48))))
		{
			goto IL_0231;
		}
	}
	{
		if ((((int32_t)V_9) == ((int32_t)((int32_t)35))))
		{
			goto IL_0231;
		}
	}
	{
		V_5->f7 = 0;
		int32_t L_18 = (V_5->f2);
		if ((((int32_t)L_18) >= ((int32_t)0)))
		{
			goto IL_0231;
		}
	}
	{
		V_1 = 1;
	}

IL_0231:
	{
		goto IL_0287;
	}

IL_0233:
	{
		V_1 = 0;
		V_2 = 1;
		V_3 = 0;
		int32_t L_19 = (V_5->f2);
		if ((((uint32_t)L_19) != ((uint32_t)(-1))))
		{
			goto IL_024c;
		}
	}
	{
		V_5->f2 = V_7;
	}

IL_024c:
	{
		goto IL_0287;
	}

IL_024e:
	{
		t1663 * L_20 = V_5;
		int32_t L_21 = (L_20->f12);
		L_20->f12 = ((int32_t)(L_21+1));
		goto IL_0287;
	}

IL_025f:
	{
		t1663 * L_22 = V_5;
		int32_t L_23 = (L_22->f13);
		L_22->f13 = ((int32_t)(L_23+1));
		goto IL_0287;
	}

IL_0270:
	{
		if (!V_1)
		{
			goto IL_0283;
		}
	}
	{
		int32_t L_24 = (V_5->f4);
		if ((((int32_t)L_24) <= ((int32_t)0)))
		{
			goto IL_0283;
		}
	}
	{
		V_6 = ((int32_t)(V_6+1));
	}

IL_0283:
	{
		goto IL_0287;
	}

IL_0285:
	{
		goto IL_0287;
	}

IL_0287:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_028d:
	{
		if ((((int32_t)((int32_t)(V_7-p1))) < ((int32_t)p2)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_25 = (V_5->f8);
		if (L_25)
		{
			goto IL_02aa;
		}
	}
	{
		V_5->f7 = 0;
		goto IL_02b2;
	}

IL_02aa:
	{
		V_5->f5 = 0;
	}

IL_02b2:
	{
		int32_t L_26 = (V_5->f1);
		if (L_26)
		{
			goto IL_02c3;
		}
	}
	{
		V_5->f2 = (-1);
	}

IL_02c3:
	{
		t1663 * L_27 = V_5;
		int32_t L_28 = (L_27->f11);
		L_27->f11 = ((int32_t)(L_28+((int32_t)((int32_t)V_6*(int32_t)3))));
		return V_5;
	}
}
extern MethodInfo m9386_MI;
 t7* m9386 (t1663 * __this, t7* p0, int32_t p1, int32_t p2, t1125 * p3, bool p4, t292 * p5, t292 * p6, t292 * p7, MethodInfo* method){
	t292 * V_0 = {0};
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t841* V_7 = {0};
	t7* V_8 = {0};
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	uint16_t V_17 = 0x0;
	bool V_18 = false;
	bool V_19 = false;
	int32_t V_20 = 0;
	uint16_t V_21 = 0x0;
	int32_t G_B10_0 = 0;
	int32_t G_B18_0 = 0;
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_0, &m1311_MI);
		V_0 = L_0;
		V_1 = 0;
		V_2 = 1;
		V_3 = 0;
		V_4 = 0;
		V_5 = 0;
		V_6 = 0;
		t841* L_1 = m6943(p3, &m6943_MI);
		V_7 = L_1;
		t7* L_2 = m6942(p3, &m6942_MI);
		V_8 = L_2;
		V_9 = 0;
		V_10 = 0;
		V_11 = 0;
		V_12 = 0;
		V_13 = 0;
		bool L_3 = (__this->f0);
		if (!L_3)
		{
			goto IL_00e7;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_7)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_4 = m4184(p5, &m4184_MI);
		V_9 = L_4;
		V_14 = 0;
		goto IL_0073;
	}

IL_0059:
	{
		int32_t L_5 = V_14;
		V_10 = ((int32_t)(V_10+(*(int32_t*)(int32_t*)SZArrayLdElema(V_7, L_5))));
		if ((((int32_t)V_10) > ((int32_t)V_9)))
		{
			goto IL_006d;
		}
	}
	{
		V_11 = V_14;
	}

IL_006d:
	{
		V_14 = ((int32_t)(V_14+1));
	}

IL_0073:
	{
		if ((((int32_t)V_14) < ((int32_t)(((int32_t)(((t20 *)V_7)->max_length))))))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_6 = V_11;
		V_13 = (*(int32_t*)(int32_t*)SZArrayLdElema(V_7, L_6));
		if ((((int32_t)V_9) <= ((int32_t)V_10)))
		{
			goto IL_008f;
		}
	}
	{
		G_B10_0 = ((int32_t)(V_9-V_10));
		goto IL_0090;
	}

IL_008f:
	{
		G_B10_0 = 0;
	}

IL_0090:
	{
		V_15 = G_B10_0;
		if (V_13)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_009e;
	}

IL_0098:
	{
		V_11 = ((int32_t)(V_11-1));
	}

IL_009e:
	{
		if ((((int32_t)V_11) < ((int32_t)0)))
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_7 = V_11;
		if (!(*(int32_t*)(int32_t*)SZArrayLdElema(V_7, L_7)))
		{
			goto IL_0098;
		}
	}

IL_00aa:
	{
		if ((((int32_t)V_15) <= ((int32_t)0)))
		{
			goto IL_00b3;
		}
	}
	{
		G_B18_0 = V_15;
		goto IL_00b8;
	}

IL_00b3:
	{
		int32_t L_8 = V_11;
		G_B18_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(V_7, L_8));
	}

IL_00b8:
	{
		V_13 = G_B18_0;
	}

IL_00ba:
	{
		if (V_15)
		{
			goto IL_00c4;
		}
	}
	{
		V_12 = V_13;
		goto IL_00e5;
	}

IL_00c4:
	{
		V_11 = ((int32_t)(V_11+((int32_t)((int32_t)V_15/(int32_t)V_13))));
		V_12 = ((int32_t)(V_15%V_13));
		if (V_12)
		{
			goto IL_00df;
		}
	}
	{
		V_12 = V_13;
		goto IL_00e5;
	}

IL_00df:
	{
		V_11 = ((int32_t)(V_11+1));
	}

IL_00e5:
	{
		goto IL_00ee;
	}

IL_00e7:
	{
		__this->f0 = 0;
	}

IL_00ee:
	{
		V_16 = p1;
		goto IL_03ce;
	}

IL_00f6:
	{
		uint16_t L_9 = m1741(p0, V_16, &m1741_MI);
		V_17 = L_9;
		if ((((uint32_t)V_17) != ((uint32_t)V_1)))
		{
			goto IL_0110;
		}
	}
	{
		if (!V_17)
		{
			goto IL_0110;
		}
	}
	{
		V_1 = 0;
		goto IL_03c8;
	}

IL_0110:
	{
		if (!V_1)
		{
			goto IL_0121;
		}
	}
	{
		m1761(V_0, V_17, &m1761_MI);
		goto IL_03c8;
	}

IL_0121:
	{
		V_21 = V_17;
		if (((uint16_t)(V_21-((int32_t)34))) == 0)
		{
			goto IL_01af;
		}
		if (((uint16_t)(V_21-((int32_t)34))) == 1)
		{
			goto IL_01c3;
		}
		if (((uint16_t)(V_21-((int32_t)34))) == 2)
		{
			goto IL_0147;
		}
		if (((uint16_t)(V_21-((int32_t)34))) == 3)
		{
			goto IL_039d;
		}
		if (((uint16_t)(V_21-((int32_t)34))) == 4)
		{
			goto IL_0147;
		}
		if (((uint16_t)(V_21-((int32_t)34))) == 5)
		{
			goto IL_01af;
		}
	}

IL_0147:
	{
		if (((uint16_t)(V_21-((int32_t)44))) == 0)
		{
			goto IL_039b;
		}
		if (((uint16_t)(V_21-((int32_t)44))) == 1)
		{
			goto IL_0165;
		}
		if (((uint16_t)(V_21-((int32_t)44))) == 2)
		{
			goto IL_0348;
		}
		if (((uint16_t)(V_21-((int32_t)44))) == 3)
		{
			goto IL_0165;
		}
		if (((uint16_t)(V_21-((int32_t)44))) == 4)
		{
			goto IL_01c5;
		}
	}

IL_0165:
	{
		if ((((int32_t)V_21) == ((int32_t)((int32_t)69))))
		{
			goto IL_0297;
		}
	}
	{
		if ((((int32_t)V_21) == ((int32_t)((int32_t)92))))
		{
			goto IL_018e;
		}
	}
	{
		if ((((int32_t)V_21) == ((int32_t)((int32_t)101))))
		{
			goto IL_0297;
		}
	}
	{
		if ((((int32_t)V_21) == ((int32_t)((int32_t)8240))))
		{
			goto IL_03ad;
		}
	}
	{
		goto IL_03bd;
	}

IL_018e:
	{
		V_16 = ((int32_t)(V_16+1));
		if ((((int32_t)((int32_t)(V_16-p1))) >= ((int32_t)p2)))
		{
			goto IL_01aa;
		}
	}
	{
		uint16_t L_10 = m1741(p0, V_16, &m1741_MI);
		m1761(V_0, L_10, &m1761_MI);
	}

IL_01aa:
	{
		goto IL_03c8;
	}

IL_01af:
	{
		if ((((int32_t)V_17) == ((int32_t)((int32_t)34))))
		{
			goto IL_01bb;
		}
	}
	{
		if ((((uint32_t)V_17) != ((uint32_t)((int32_t)39))))
		{
			goto IL_01be;
		}
	}

IL_01bb:
	{
		V_1 = V_17;
	}

IL_01be:
	{
		goto IL_03c8;
	}

IL_01c3:
	{
		goto IL_01c5;
	}

IL_01c5:
	{
		if (!V_2)
		{
			goto IL_0261;
		}
	}
	{
		V_4 = ((int32_t)(V_4+1));
		int32_t L_11 = (__this->f4);
		int32_t L_12 = m4184(p5, &m4184_MI);
		if ((((int32_t)((int32_t)(L_11-V_4))) < ((int32_t)((int32_t)(L_12+V_5)))))
		{
			goto IL_01ef;
		}
	}
	{
		if ((((uint32_t)V_17) != ((uint32_t)((int32_t)48))))
		{
			goto IL_025c;
		}
	}

IL_01ef:
	{
		goto IL_0247;
	}

IL_01f1:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)(L_13+1));
		uint16_t L_14 = m8662(p5, L_13, &m8662_MI);
		m1761(V_0, L_14, &m1761_MI);
		bool L_15 = (__this->f0);
		if (!L_15)
		{
			goto IL_0247;
		}
	}
	{
		int32_t L_16 = ((int32_t)(V_9-1));
		V_9 = L_16;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0247;
		}
	}
	{
		int32_t L_17 = ((int32_t)(V_12-1));
		V_12 = L_17;
		if (L_17)
		{
			goto IL_0247;
		}
	}
	{
		m2927(V_0, V_8, &m2927_MI);
		int32_t L_18 = ((int32_t)(V_11-1));
		V_11 = L_18;
		if ((((int32_t)L_18) >= ((int32_t)(((int32_t)(((t20 *)V_7)->max_length))))))
		{
			goto IL_0243;
		}
	}
	{
		if ((((int32_t)V_11) < ((int32_t)0)))
		{
			goto IL_0243;
		}
	}
	{
		int32_t L_19 = V_11;
		V_13 = (*(int32_t*)(int32_t*)SZArrayLdElema(V_7, L_19));
	}

IL_0243:
	{
		V_12 = V_13;
	}

IL_0247:
	{
		int32_t L_20 = (__this->f4);
		int32_t L_21 = m4184(p5, &m4184_MI);
		if ((((int32_t)((int32_t)(((int32_t)(L_20-V_4))+V_5))) < ((int32_t)L_21)))
		{
			goto IL_01f1;
		}
	}

IL_025c:
	{
		goto IL_03c8;
	}

IL_0261:
	{
		if (!V_3)
		{
			goto IL_0289;
		}
	}
	{
		int32_t L_22 = m4184(p6, &m4184_MI);
		if ((((int32_t)V_6) >= ((int32_t)L_22)))
		{
			goto IL_0284;
		}
	}
	{
		int32_t L_23 = V_6;
		V_6 = ((int32_t)(L_23+1));
		uint16_t L_24 = m8662(p6, L_23, &m8662_MI);
		m1761(V_0, L_24, &m1761_MI);
	}

IL_0284:
	{
		goto IL_03c8;
	}

IL_0289:
	{
		m1761(V_0, V_17, &m1761_MI);
		goto IL_03c8;
	}

IL_0297:
	{
		if (!p7)
		{
			goto IL_02a3;
		}
	}
	{
		bool L_25 = (__this->f7);
		if (L_25)
		{
			goto IL_02b1;
		}
	}

IL_02a3:
	{
		m1761(V_0, V_17, &m1761_MI);
		goto IL_03c8;
	}

IL_02b1:
	{
		V_18 = 1;
		V_19 = 0;
		V_20 = ((int32_t)(V_16+1));
		goto IL_0301;
	}

IL_02bf:
	{
		uint16_t L_26 = m1741(p0, V_20, &m1741_MI);
		if ((((uint32_t)L_26) != ((uint32_t)((int32_t)48))))
		{
			goto IL_02d0;
		}
	}
	{
		V_19 = 1;
		goto IL_02fb;
	}

IL_02d0:
	{
		if ((((uint32_t)V_20) != ((uint32_t)((int32_t)(V_16+1)))))
		{
			goto IL_02f2;
		}
	}
	{
		uint16_t L_27 = m1741(p0, V_20, &m1741_MI);
		if ((((int32_t)L_27) == ((int32_t)((int32_t)43))))
		{
			goto IL_02f0;
		}
	}
	{
		uint16_t L_28 = m1741(p0, V_20, &m1741_MI);
		if ((((uint32_t)L_28) != ((uint32_t)((int32_t)45))))
		{
			goto IL_02f2;
		}
	}

IL_02f0:
	{
		goto IL_02fb;
	}

IL_02f2:
	{
		if (V_19)
		{
			goto IL_02f9;
		}
	}
	{
		V_18 = 0;
	}

IL_02f9:
	{
		goto IL_0308;
	}

IL_02fb:
	{
		V_20 = ((int32_t)(V_20+1));
	}

IL_0301:
	{
		if ((((int32_t)((int32_t)(V_20-p1))) < ((int32_t)p2)))
		{
			goto IL_02bf;
		}
	}

IL_0308:
	{
		if (!V_18)
		{
			goto IL_033a;
		}
	}
	{
		V_16 = ((int32_t)(V_20-1));
		int32_t L_29 = (__this->f2);
		V_2 = ((((int32_t)L_29) < ((int32_t)0))? 1 : 0);
		V_3 = ((((int32_t)V_2) == ((int32_t)0))? 1 : 0);
		m1761(V_0, V_17, &m1761_MI);
		m3993(V_0, p7, &m3993_MI);
		p7 = (t292 *)NULL;
		goto IL_0343;
	}

IL_033a:
	{
		m1761(V_0, V_17, &m1761_MI);
	}

IL_0343:
	{
		goto IL_03c8;
	}

IL_0348:
	{
		int32_t L_30 = (__this->f2);
		if ((((uint32_t)L_30) != ((uint32_t)V_16)))
		{
			goto IL_0395;
		}
	}
	{
		int32_t L_31 = (__this->f1);
		if ((((int32_t)L_31) <= ((int32_t)0)))
		{
			goto IL_037d;
		}
	}
	{
		goto IL_0372;
	}

IL_035d:
	{
		int32_t L_32 = V_5;
		V_5 = ((int32_t)(L_32+1));
		uint16_t L_33 = m8662(p5, L_32, &m8662_MI);
		m1761(V_0, L_33, &m1761_MI);
	}

IL_0372:
	{
		int32_t L_34 = m4184(p5, &m4184_MI);
		if ((((int32_t)V_5) < ((int32_t)L_34)))
		{
			goto IL_035d;
		}
	}

IL_037d:
	{
		int32_t L_35 = m4184(p6, &m4184_MI);
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_0395;
		}
	}
	{
		t7* L_36 = m6941(p3, &m6941_MI);
		m2927(V_0, L_36, &m2927_MI);
	}

IL_0395:
	{
		V_2 = 0;
		V_3 = 1;
		goto IL_03c8;
	}

IL_039b:
	{
		goto IL_03c8;
	}

IL_039d:
	{
		t7* L_37 = m6952(p3, &m6952_MI);
		m2927(V_0, L_37, &m2927_MI);
		goto IL_03c8;
	}

IL_03ad:
	{
		t7* L_38 = m6953(p3, &m6953_MI);
		m2927(V_0, L_38, &m2927_MI);
		goto IL_03c8;
	}

IL_03bd:
	{
		m1761(V_0, V_17, &m1761_MI);
		goto IL_03c8;
	}

IL_03c8:
	{
		V_16 = ((int32_t)(V_16+1));
	}

IL_03ce:
	{
		if ((((int32_t)((int32_t)(V_16-p1))) < ((int32_t)p2)))
		{
			goto IL_00f6;
		}
	}
	{
		if (p4)
		{
			goto IL_03eb;
		}
	}
	{
		t7* L_39 = m6939(p3, &m6939_MI);
		m8672(V_0, 0, L_39, &m8672_MI);
	}

IL_03eb:
	{
		t7* L_40 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_40;
	}
}
// Metadata Definition System.NumberFormatter/CustomInfo
extern Il2CppType t40_0_0_6;
FieldInfo t1663_f0_FieldInfo = 
{
	"UseGroup", &t40_0_0_6, &t1663_TI, offsetof(t1663, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f1_FieldInfo = 
{
	"DecimalDigits", &t44_0_0_6, &t1663_TI, offsetof(t1663, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f2_FieldInfo = 
{
	"DecimalPointPos", &t44_0_0_6, &t1663_TI, offsetof(t1663, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f3_FieldInfo = 
{
	"DecimalTailSharpDigits", &t44_0_0_6, &t1663_TI, offsetof(t1663, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f4_FieldInfo = 
{
	"IntegerDigits", &t44_0_0_6, &t1663_TI, offsetof(t1663, f4), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f5_FieldInfo = 
{
	"IntegerHeadSharpDigits", &t44_0_0_6, &t1663_TI, offsetof(t1663, f5), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f6_FieldInfo = 
{
	"IntegerHeadPos", &t44_0_0_6, &t1663_TI, offsetof(t1663, f6), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t1663_f7_FieldInfo = 
{
	"UseExponent", &t40_0_0_6, &t1663_TI, offsetof(t1663, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f8_FieldInfo = 
{
	"ExponentDigits", &t44_0_0_6, &t1663_TI, offsetof(t1663, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f9_FieldInfo = 
{
	"ExponentTailSharpDigits", &t44_0_0_6, &t1663_TI, offsetof(t1663, f9), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t1663_f10_FieldInfo = 
{
	"ExponentNegativeSignOnly", &t40_0_0_6, &t1663_TI, offsetof(t1663, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f11_FieldInfo = 
{
	"DividePlaces", &t44_0_0_6, &t1663_TI, offsetof(t1663, f11), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f12_FieldInfo = 
{
	"Percents", &t44_0_0_6, &t1663_TI, offsetof(t1663, f12), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1663_f13_FieldInfo = 
{
	"Permilles", &t44_0_0_6, &t1663_TI, offsetof(t1663, f13), &EmptyCustomAttributesCache};
static FieldInfo* t1663_FIs[] =
{
	&t1663_f0_FieldInfo,
	&t1663_f1_FieldInfo,
	&t1663_f2_FieldInfo,
	&t1663_f3_FieldInfo,
	&t1663_f4_FieldInfo,
	&t1663_f5_FieldInfo,
	&t1663_f6_FieldInfo,
	&t1663_f7_FieldInfo,
	&t1663_f8_FieldInfo,
	&t1663_f9_FieldInfo,
	&t1663_f10_FieldInfo,
	&t1663_f11_FieldInfo,
	&t1663_f12_FieldInfo,
	&t1663_f13_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9383_MI = 
{
	".ctor", (methodPointerType)&m9383, &t1663_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5320, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_1_0_0;
extern Il2CppType t40_1_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_1_0_0;
static ParameterInfo t1663_m9384_ParameterInfos[] = 
{
	{"format", 0, 134224380, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"positive", 1, 134224381, &EmptyCustomAttributesCache, &t40_1_0_0},
	{"zero", 2, 134224382, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"offset", 3, 134224383, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"length", 4, 134224384, &EmptyCustomAttributesCache, &t44_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t326_t297_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m9384_MI = 
{
	"GetActiveSection", (methodPointerType)&m9384, &t1663_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t326_t297_t390_t390, t1663_m9384_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 5, false, false, 5321, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1663_m9385_ParameterInfos[] = 
{
	{"format", 0, 134224385, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"offset", 1, 134224386, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"length", 2, 134224387, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 3, 134224388, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t1663_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9385_MI = 
{
	"Parse", (methodPointerType)&m9385, &t1663_TI, &t1663_0_0_0, RuntimeInvoker_t29_t29_t44_t44_t29, t1663_m9385_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 5322, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t292_0_0_0;
extern Il2CppType t292_0_0_0;
extern Il2CppType t292_0_0_0;
extern Il2CppType t292_0_0_0;
static ParameterInfo t1663_m9386_ParameterInfos[] = 
{
	{"format", 0, 134224389, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"offset", 1, 134224390, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"length", 2, 134224391, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 3, 134224392, &EmptyCustomAttributesCache, &t1125_0_0_0},
	{"positive", 4, 134224393, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"sb_int", 5, 134224394, &EmptyCustomAttributesCache, &t292_0_0_0},
	{"sb_dec", 6, 134224395, &EmptyCustomAttributesCache, &t292_0_0_0},
	{"sb_exp", 7, 134224396, &EmptyCustomAttributesCache, &t292_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t44_t29_t297_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9386_MI = 
{
	"Format", (methodPointerType)&m9386, &t1663_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t44_t44_t29_t297_t29_t29_t29, t1663_m9386_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 8, false, false, 5323, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1663_MIs[] =
{
	&m9383_MI,
	&m9384_MI,
	&m9385_MI,
	&m9386_MI,
	NULL
};
static MethodInfo* t1663_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1663_0_0_0;
extern Il2CppType t1663_1_0_0;
struct t1663;
extern TypeInfo t1664_TI;
TypeInfo t1663_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CustomInfo", "", t1663_MIs, NULL, t1663_FIs, NULL, &t29_TI, NULL, &t1664_TI, &t1663_TI, NULL, t1663_VT, &EmptyCustomAttributesCache, &t1663_TI, &t1663_0_0_0, &t1663_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1663), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 14, 0, 0, 4, 0, 0};
#include "t1664.h"
#ifndef _MSC_VER
#else
#endif
#include "t1664MD.h"

#include "t1436.h"
#include "t1089.h"
#include "t344.h"
#include "t297.h"
#include "t626.h"
#include "t372.h"
extern TypeInfo t1125_TI;
extern TypeInfo t200_TI;
extern TypeInfo t194_TI;
extern TypeInfo t922_TI;
extern TypeInfo t1436_TI;
#include "t633MD.h"
#include "t1436MD.h"
#include "t922MD.h"
#include "t601MD.h"
#include "t22MD.h"
#include "t194MD.h"
extern MethodInfo m6856_MI;
extern MethodInfo m6854_MI;
extern MethodInfo m8799_MI;
extern MethodInfo m9416_MI;
extern MethodInfo m9389_MI;
extern MethodInfo m9394_MI;
extern MethodInfo m9395_MI;
extern MethodInfo m9392_MI;
extern MethodInfo m9396_MI;
extern MethodInfo m9397_MI;
extern MethodInfo m9390_MI;
extern MethodInfo m9401_MI;
extern MethodInfo m9398_MI;
extern MethodInfo m9402_MI;
extern MethodInfo m9403_MI;
extern MethodInfo m9391_MI;
extern MethodInfo m8872_MI;
extern MethodInfo m9399_MI;
extern MethodInfo m9400_MI;
extern MethodInfo m9428_MI;
extern MethodInfo m5719_MI;
extern MethodInfo m9393_MI;
extern MethodInfo m9411_MI;
extern MethodInfo m6958_MI;
extern MethodInfo m9424_MI;
extern MethodInfo m9426_MI;
extern MethodInfo m9425_MI;
extern MethodInfo m9427_MI;
extern MethodInfo m9429_MI;
extern MethodInfo m8791_MI;
extern MethodInfo m9387_MI;
extern MethodInfo m9430_MI;
extern MethodInfo m9404_MI;
extern MethodInfo m9451_MI;
extern MethodInfo m9431_MI;
extern MethodInfo m9405_MI;
extern MethodInfo m9407_MI;
extern MethodInfo m9406_MI;
extern MethodInfo m9408_MI;
extern MethodInfo m9415_MI;
extern MethodInfo m6937_MI;
extern MethodInfo m6954_MI;
extern MethodInfo m6938_MI;
extern MethodInfo m9458_MI;
extern MethodInfo m9452_MI;
extern MethodInfo m9457_MI;
extern MethodInfo m9409_MI;
extern MethodInfo m9437_MI;
extern MethodInfo m9450_MI;
extern MethodInfo m9438_MI;
extern MethodInfo m9439_MI;
extern MethodInfo m9440_MI;
extern MethodInfo m9459_MI;
extern MethodInfo m9410_MI;
extern MethodInfo m9414_MI;
extern MethodInfo m9475_MI;
extern MethodInfo m5555_MI;
extern MethodInfo m9453_MI;
extern MethodInfo m9454_MI;
extern MethodInfo m9462_MI;
extern MethodInfo m9456_MI;
extern MethodInfo m9460_MI;
extern MethodInfo m9461_MI;
extern MethodInfo m9455_MI;
extern MethodInfo m9464_MI;
extern MethodInfo m6928_MI;
extern MethodInfo m9423_MI;
extern MethodInfo m9417_MI;
extern MethodInfo m6933_MI;
extern MethodInfo m6934_MI;
extern MethodInfo m9412_MI;
extern MethodInfo m6932_MI;
extern MethodInfo m6931_MI;
extern MethodInfo m6930_MI;
extern MethodInfo m9472_MI;
extern MethodInfo m6929_MI;
extern MethodInfo m9471_MI;
extern MethodInfo m9476_MI;
extern MethodInfo m6940_MI;
extern MethodInfo m9469_MI;
extern MethodInfo m9480_MI;
extern MethodInfo m5688_MI;
extern MethodInfo m5660_MI;
extern MethodInfo m9419_MI;
extern MethodInfo m9422_MI;
extern MethodInfo m9463_MI;
extern MethodInfo m6944_MI;
extern MethodInfo m6946_MI;
extern MethodInfo m9478_MI;
extern MethodInfo m6951_MI;
extern MethodInfo m6950_MI;
extern MethodInfo m6949_MI;
extern MethodInfo m6948_MI;
extern MethodInfo m6947_MI;
extern MethodInfo m9474_MI;
extern MethodInfo m9473_MI;
extern MethodInfo m9420_MI;
extern MethodInfo m2923_MI;
extern MethodInfo m9479_MI;
extern MethodInfo m9467_MI;
extern MethodInfo m9421_MI;
extern MethodInfo m9468_MI;
extern MethodInfo m9418_MI;
extern MethodInfo m9470_MI;
extern MethodInfo m8674_MI;
extern MethodInfo m8673_MI;
extern MethodInfo m6955_MI;
extern MethodInfo m9466_MI;
extern MethodInfo m8665_MI;
extern MethodInfo m9465_MI;
extern MethodInfo m4222_MI;
extern MethodInfo m8668_MI;
extern MethodInfo m9477_MI;
extern MethodInfo m9413_MI;
extern MethodInfo m4285_MI;
extern MethodInfo m8663_MI;
extern MethodInfo m5283_MI;


 void m9387 (t1664 * __this, t1436 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f23 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 0));
		if (p0)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		__this->f6 = p0;
		t1436 * L_0 = (__this->f6);
		t633 * L_1 = m8799(L_0, &m8799_MI);
		m9416(__this, L_1, &m9416_MI);
		return;
	}
}
extern MethodInfo m9388_MI;
 void m9388 (t29 * __this, MethodInfo* method){
	{
		m9389(NULL, (&((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f0), (&((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f1), (&((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f2), (&((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f3), (&((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f4), (&((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5), &m9389_MI);
		return;
	}
}
 void m9389 (t29 * __this, uint64_t** p0, int32_t** p1, uint16_t** p2, uint16_t** p3, int64_t** p4, int32_t** p5, MethodInfo* method){
	typedef void (*m9389_ftn) (uint64_t**, int32_t**, uint16_t**, uint16_t**, int64_t**, int32_t**);
	static m9389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9389_ftn)il2cpp_codegen_resolve_icall ("System.NumberFormatter::GetFormatterTables(System.UInt64*&,System.Int32*&,System.Char*&,System.Char*&,System.Int64*&,System.Int32*&)");
	_il2cpp_icall_func(p0, p1, p2, p3, p4, p5);
}
 int64_t m9390 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		return (*((int64_t*)((int64_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f4)+((int32_t)((int32_t)p0*(int32_t)8))))));
	}
}
 void m9391 (t1664 * __this, uint32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if ((((uint32_t)p0) < ((uint32_t)((int32_t)100000000))))
		{
			goto IL_0029;
		}
	}
	{
		V_0 = ((uint32_t)(p0/((int32_t)100000000)));
		p0 = ((uint32_t)(p0-((int32_t)((int32_t)((int32_t)100000000)*(int32_t)V_0))));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_0 = m9394(NULL, V_0, &m9394_MI);
		__this->f20 = L_0;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_1 = m9395(NULL, p0, &m9395_MI);
		__this->f19 = L_1;
		return;
	}
}
 void m9392 (t1664 * __this, uint64_t p0, MethodInfo* method){
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	{
		if ((((uint64_t)p0) < ((uint64_t)(((int64_t)((int32_t)100000000))))))
		{
			goto IL_005b;
		}
	}
	{
		V_0 = ((uint64_t)(p0/(((int64_t)((int32_t)100000000)))));
		p0 = ((uint64_t)(p0-((int64_t)((int64_t)(((int64_t)((int32_t)100000000)))*(int64_t)V_0))));
		if ((((int64_t)V_0) < ((int64_t)(((int64_t)((int32_t)100000000))))))
		{
			goto IL_004b;
		}
	}
	{
		V_1 = (((int32_t)((int64_t)((int64_t)V_0/(int64_t)(((int64_t)((int32_t)100000000)))))));
		V_0 = ((int64_t)(V_0-((int64_t)((int64_t)(((int64_t)V_1))*(int64_t)(((int64_t)((int32_t)100000000)))))));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_0 = m9395(NULL, V_1, &m9395_MI);
		__this->f21 = L_0;
	}

IL_004b:
	{
		if (!V_0)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_1 = m9395(NULL, (((int32_t)V_0)), &m9395_MI);
		__this->f20 = L_1;
	}

IL_005b:
	{
		if (!p0)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_2 = m9395(NULL, (((int32_t)p0)), &m9395_MI);
		__this->f19 = L_2;
	}

IL_006b:
	{
		return;
	}
}
 void m9393 (t1664 * __this, uint32_t p0, uint64_t p1, MethodInfo* method){
	uint32_t V_0 = 0;
	uint64_t V_1 = 0;
	uint64_t V_2 = 0;
	uint64_t V_3 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		m9392(__this, p1, &m9392_MI);
		return;
	}

IL_000b:
	{
		V_0 = ((uint32_t)(p0/((int32_t)100000000)));
		V_1 = (((uint64_t)((uint32_t)(p0-((int32_t)((int32_t)V_0*(int32_t)((int32_t)100000000)))))));
		V_2 = ((uint64_t)(p1/(((int64_t)((int32_t)100000000)))));
		V_3 = ((int64_t)(((uint64_t)(p1-((int64_t)((int64_t)V_2*(int64_t)(((int64_t)((int32_t)100000000)))))))+((int64_t)((int64_t)V_1*(int64_t)(((int64_t)((int32_t)9551616)))))));
		p0 = V_0;
		p1 = ((int64_t)(V_2+((int64_t)((int64_t)V_1*(int64_t)((int64_t)184467440737LL)))));
		V_2 = ((uint64_t)(V_3/(((int64_t)((int32_t)100000000)))));
		V_3 = ((uint64_t)(V_3-((int64_t)((int64_t)V_2*(int64_t)(((int64_t)((int32_t)100000000)))))));
		p1 = ((int64_t)(p1+V_2));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_0 = m9395(NULL, (((int32_t)V_3)), &m9395_MI);
		__this->f19 = L_0;
		V_2 = ((uint64_t)(p1/(((int64_t)((int32_t)100000000)))));
		V_3 = ((uint64_t)(p1-((int64_t)((int64_t)V_2*(int64_t)(((int64_t)((int32_t)100000000)))))));
		p1 = V_2;
		if (!p0)
		{
			goto IL_00ce;
		}
	}
	{
		p1 = ((int64_t)(p1+((int64_t)((int64_t)(((uint64_t)p0))*(int64_t)((int64_t)184467440737LL)))));
		V_3 = ((int64_t)(V_3+((int64_t)((int64_t)(((uint64_t)p0))*(int64_t)(((int64_t)((int32_t)9551616)))))));
		V_2 = ((uint64_t)(V_3/(((int64_t)((int32_t)100000000)))));
		p1 = ((int64_t)(p1+V_2));
		V_3 = ((uint64_t)(V_3-((int64_t)((int64_t)V_2*(int64_t)(((int64_t)((int32_t)100000000)))))));
	}

IL_00ce:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_1 = m9395(NULL, (((int32_t)V_3)), &m9395_MI);
		__this->f20 = L_1;
		if ((((uint64_t)p1) < ((uint64_t)(((int64_t)((int32_t)100000000))))))
		{
			goto IL_0108;
		}
	}
	{
		V_2 = ((uint64_t)(p1/(((int64_t)((int32_t)100000000)))));
		p1 = ((uint64_t)(p1-((int64_t)((int64_t)V_2*(int64_t)(((int64_t)((int32_t)100000000)))))));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_2 = m9395(NULL, (((int32_t)V_2)), &m9395_MI);
		__this->f22 = L_2;
	}

IL_0108:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_3 = m9395(NULL, (((int32_t)p1)), &m9395_MI);
		__this->f21 = L_3;
		return;
	}
}
 uint32_t m9394 (t29 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		return (*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5)+((int32_t)((int32_t)p0*(int32_t)4))))));
	}

IL_0010:
	{
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)p0*(int32_t)((int32_t)5243)))>>(int32_t)((int32_t)19)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		return ((int32_t)((int32_t)((int32_t)((int32_t)(*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5)+((int32_t)((int32_t)V_0*(int32_t)4))))))<<(int32_t)8))|(int32_t)(*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5)+((int32_t)((int32_t)((int32_t)(p0-((int32_t)((int32_t)V_0*(int32_t)((int32_t)100)))))*(int32_t)4))))))));
	}
}
 uint32_t m9395 (t29 * __this, int32_t p0, MethodInfo* method){
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		if ((((int32_t)p0) < ((int32_t)((int32_t)10000))))
		{
			goto IL_0029;
		}
	}
	{
		V_1 = ((int32_t)((int32_t)p0/(int32_t)((int32_t)10000)));
		p0 = ((int32_t)(p0-((int32_t)((int32_t)V_1*(int32_t)((int32_t)10000)))));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_0 = m9394(NULL, V_1, &m9394_MI);
		V_0 = ((int32_t)((int32_t)L_0<<(int32_t)((int32_t)16)));
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_1 = m9394(NULL, p0, &m9394_MI);
		return ((int32_t)((int32_t)V_0|(int32_t)L_1));
	}
}
 int32_t m9396 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)256))))
		{
			goto IL_0011;
		}
	}
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)16))))
		{
			goto IL_000f;
		}
	}
	{
		return 1;
	}

IL_000f:
	{
		return 2;
	}

IL_0011:
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)4096))))
		{
			goto IL_001b;
		}
	}
	{
		return 3;
	}

IL_001b:
	{
		return 4;
	}
}
 int32_t m9397 (t29 * __this, uint32_t p0, MethodInfo* method){
	{
		if ((((uint32_t)p0) >= ((uint32_t)((int32_t)65536))))
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_0 = m9396(NULL, p0, &m9396_MI);
		return L_0;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_1 = m9396(NULL, ((int32_t)((uint32_t)p0>>((int32_t)16))), &m9396_MI);
		return ((int32_t)(4+L_1));
	}
}
 int32_t m9398 (t1664 * __this, MethodInfo* method){
	{
		uint32_t L_0 = (__this->f22);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		uint32_t L_1 = (__this->f22);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_2 = m9397(NULL, L_1, &m9397_MI);
		return ((int32_t)(L_2+((int32_t)24)));
	}

IL_0017:
	{
		uint32_t L_3 = (__this->f21);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		uint32_t L_4 = (__this->f21);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_5 = m9397(NULL, L_4, &m9397_MI);
		return ((int32_t)(L_5+((int32_t)16)));
	}

IL_002e:
	{
		uint32_t L_6 = (__this->f20);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		uint32_t L_7 = (__this->f20);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_8 = m9397(NULL, L_7, &m9397_MI);
		return ((int32_t)(L_8+8));
	}

IL_0044:
	{
		uint32_t L_9 = (__this->f19);
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		uint32_t L_10 = (__this->f19);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_11 = m9397(NULL, L_10, &m9397_MI);
		return L_11;
	}

IL_0058:
	{
		return 0;
	}
}
 int32_t m9399 (t29 * __this, int64_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)18);
		goto IL_0016;
	}

IL_0005:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int64_t L_0 = m9390(NULL, V_0, &m9390_MI);
		if ((((int64_t)p0) < ((int64_t)L_0)))
		{
			goto IL_0012;
		}
	}
	{
		return ((int32_t)(V_0+1));
	}

IL_0012:
	{
		V_0 = ((int32_t)(V_0-1));
	}

IL_0016:
	{
		if ((((int32_t)V_0) >= ((int32_t)0)))
		{
			goto IL_0005;
		}
	}
	{
		return 1;
	}
}
 int32_t m9400 (t1664 * __this, MethodInfo* method){
	{
		uint16_t L_0 = (__this->f13);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)82))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = (__this->f15);
		return ((int32_t)(L_1+2));
	}

IL_0013:
	{
		int32_t L_2 = (__this->f14);
		int32_t L_3 = (__this->f15);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = (__this->f15);
		return L_4;
	}

IL_0028:
	{
		uint16_t L_5 = (__this->f13);
		if ((((uint32_t)L_5) != ((uint32_t)((int32_t)71))))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = (__this->f15);
		int32_t L_7 = (__this->f14);
		int32_t L_8 = m9285(NULL, ((int32_t)(L_6+2)), L_7, &m9285_MI);
		return L_8;
	}

IL_0046:
	{
		uint16_t L_9 = (__this->f13);
		if ((((uint32_t)L_9) != ((uint32_t)((int32_t)69))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_10 = (__this->f15);
		int32_t L_11 = (__this->f14);
		int32_t L_12 = m9285(NULL, ((int32_t)(L_10+2)), ((int32_t)(L_11+1)), &m9285_MI);
		return L_12;
	}

IL_0066:
	{
		int32_t L_13 = (__this->f15);
		return L_13;
	}
}
 int32_t m9401 (t29 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 1;
		goto IL_002d;
	}

IL_0006:
	{
		uint16_t L_0 = m1741(p0, V_1, &m1741_MI);
		V_2 = ((uint16_t)(L_0-((int32_t)48)));
		V_0 = ((int32_t)(((int32_t)((int32_t)V_0*(int32_t)((int32_t)10)))+V_2));
		if ((((int32_t)V_2) < ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		if ((((int32_t)V_2) > ((int32_t)((int32_t)9))))
		{
			goto IL_0026;
		}
	}
	{
		if ((((int32_t)V_0) <= ((int32_t)((int32_t)99))))
		{
			goto IL_0029;
		}
	}

IL_0026:
	{
		return ((int32_t)-2);
	}

IL_0029:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_002d:
	{
		int32_t L_1 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_1) < ((int32_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		return V_0;
	}
}
 void m9402 (t1664 * __this, t7* p0, MethodInfo* method){
	uint16_t V_0 = 0x0;
	uint32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = 0;
		V_1 = L_0;
		__this->f22 = L_0;
		uint32_t L_1 = V_1;
		V_1 = L_1;
		__this->f21 = L_1;
		uint32_t L_2 = V_1;
		V_1 = L_2;
		__this->f20 = L_2;
		__this->f19 = V_1;
		__this->f17 = 0;
		int32_t L_3 = 0;
		V_2 = L_3;
		__this->f9 = L_3;
		__this->f8 = V_2;
		__this->f10 = 0;
		__this->f11 = 1;
		__this->f14 = (-1);
		if (!p0)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_4 = m1715(p0, &m1715_MI);
		if (L_4)
		{
			goto IL_0062;
		}
	}

IL_0059:
	{
		__this->f13 = ((int32_t)71);
		return;
	}

IL_0062:
	{
		uint16_t L_5 = m1741(p0, 0, &m1741_MI);
		V_0 = L_5;
		if ((((int32_t)V_0) < ((int32_t)((int32_t)97))))
		{
			goto IL_0086;
		}
	}
	{
		if ((((int32_t)V_0) > ((int32_t)((int32_t)122))))
		{
			goto IL_0086;
		}
	}
	{
		V_0 = (((uint16_t)((int32_t)(((uint16_t)(V_0-((int32_t)97)))+((int32_t)65)))));
		__this->f11 = 0;
		goto IL_00a0;
	}

IL_0086:
	{
		if ((((int32_t)V_0) < ((int32_t)((int32_t)65))))
		{
			goto IL_0090;
		}
	}
	{
		if ((((int32_t)V_0) <= ((int32_t)((int32_t)90))))
		{
			goto IL_00a0;
		}
	}

IL_0090:
	{
		__this->f10 = 1;
		__this->f13 = ((int32_t)48);
		return;
	}

IL_00a0:
	{
		__this->f13 = V_0;
		int32_t L_6 = m1715(p0, &m1715_MI);
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_00dc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_7 = m9401(NULL, p0, &m9401_MI);
		__this->f14 = L_7;
		int32_t L_8 = (__this->f14);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_00dc;
		}
	}
	{
		__this->f10 = 1;
		__this->f13 = ((int32_t)48);
		__this->f14 = (-1);
	}

IL_00dc:
	{
		return;
	}
}
 void m9403 (t1664 * __this, uint64_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f15);
		V_0 = L_0;
		if (((int32_t)(V_0-3)) == 0)
		{
			goto IL_0022;
		}
		if (((int32_t)(V_0-3)) == 1)
		{
			goto IL_001b;
		}
		if (((int32_t)(V_0-3)) == 2)
		{
			goto IL_002b;
		}
	}

IL_001b:
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)10))))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_003d;
	}

IL_0022:
	{
		p0 = (((uint64_t)(((uint8_t)p0))));
		goto IL_003d;
	}

IL_002b:
	{
		p0 = (((uint64_t)(((uint16_t)p0))));
		goto IL_003d;
	}

IL_0034:
	{
		p0 = (((uint64_t)(((uint32_t)p0))));
		goto IL_003d;
	}

IL_003d:
	{
		__this->f19 = (((uint32_t)p0));
		__this->f20 = (((uint32_t)((int64_t)((uint64_t)p0>>((int32_t)32)))));
		int32_t L_1 = m9398(__this, &m9398_MI);
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->f16 = L_2;
		__this->f18 = V_0;
		if (p0)
		{
			goto IL_006f;
		}
	}
	{
		__this->f18 = 1;
	}

IL_006f:
	{
		return;
	}
}
 void m9404 (t1664 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m9402(__this, p0, &m9402_MI);
		__this->f15 = p2;
		__this->f12 = ((((int32_t)((((int32_t)p1) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		if (!p1)
		{
			goto IL_0028;
		}
	}
	{
		uint16_t L_0 = (__this->f13);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)88))))
		{
			goto IL_0031;
		}
	}

IL_0028:
	{
		m9403(__this, (((int64_t)p1)), &m9403_MI);
		return;
	}

IL_0031:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		p1 = ((-p1));
	}

IL_003b:
	{
		m9391(__this, p1, &m9391_MI);
		int32_t L_1 = m9398(__this, &m9398_MI);
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->f16 = L_2;
		__this->f18 = V_0;
		return;
	}
}
 void m9405 (t1664 * __this, t7* p0, uint32_t p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m9402(__this, p0, &m9402_MI);
		__this->f15 = p2;
		__this->f12 = 1;
		if (!p1)
		{
			goto IL_0022;
		}
	}
	{
		uint16_t L_0 = (__this->f13);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)88))))
		{
			goto IL_002b;
		}
	}

IL_0022:
	{
		m9403(__this, (((uint64_t)p1)), &m9403_MI);
		return;
	}

IL_002b:
	{
		m9391(__this, p1, &m9391_MI);
		int32_t L_1 = m9398(__this, &m9398_MI);
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->f16 = L_2;
		__this->f18 = V_0;
		return;
	}
}
 void m9406 (t1664 * __this, t7* p0, int64_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m9402(__this, p0, &m9402_MI);
		__this->f15 = ((int32_t)19);
		__this->f12 = ((((int32_t)((((int64_t)p1) < ((int64_t)(((int64_t)0))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		if (!p1)
		{
			goto IL_002a;
		}
	}
	{
		uint16_t L_0 = (__this->f13);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)88))))
		{
			goto IL_0032;
		}
	}

IL_002a:
	{
		m9403(__this, p1, &m9403_MI);
		return;
	}

IL_0032:
	{
		if ((((int64_t)p1) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_003d;
		}
	}
	{
		p1 = ((-p1));
	}

IL_003d:
	{
		m9392(__this, p1, &m9392_MI);
		int32_t L_1 = m9398(__this, &m9398_MI);
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->f16 = L_2;
		__this->f18 = V_0;
		return;
	}
}
 void m9407 (t1664 * __this, t7* p0, uint64_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m9402(__this, p0, &m9402_MI);
		__this->f15 = ((int32_t)20);
		__this->f12 = 1;
		if (!p1)
		{
			goto IL_0023;
		}
	}
	{
		uint16_t L_0 = (__this->f13);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)88))))
		{
			goto IL_002b;
		}
	}

IL_0023:
	{
		m9403(__this, p1, &m9403_MI);
		return;
	}

IL_002b:
	{
		m9392(__this, p1, &m9392_MI);
		int32_t L_1 = m9398(__this, &m9398_MI);
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->f16 = L_2;
		__this->f18 = V_0;
		return;
	}
}
 void m9408 (t1664 * __this, t7* p0, double p1, int32_t p2, MethodInfo* method){
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint64_t V_5 = 0;
	uint64_t V_6 = 0;
	uint64_t V_7 = 0;
	uint64_t V_8 = 0;
	uint64_t V_9 = 0;
	int64_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int64_t V_13 = 0;
	{
		m9402(__this, p0, &m9402_MI);
		__this->f15 = p2;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t922_TI));
		int64_t L_0 = m8872(NULL, p1, &m8872_MI);
		V_0 = L_0;
		__this->f12 = ((((int32_t)((((int64_t)V_0) < ((int64_t)(((int64_t)0))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_0 = ((int64_t)((int64_t)V_0&(int64_t)((int64_t)std::numeric_limits<int64_t>::max())));
		if (V_0)
		{
			goto IL_0048;
		}
	}
	{
		__this->f18 = 1;
		__this->f16 = 0;
		__this->f12 = 1;
		return;
	}

IL_0048:
	{
		V_1 = (((int32_t)((int64_t)((int64_t)V_0>>(int32_t)((int32_t)52)))));
		V_2 = ((int64_t)((int64_t)V_0&(int64_t)((int64_t)4503599627370495LL)));
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)2047))))
		{
			goto IL_007c;
		}
	}
	{
		__this->f8 = ((((int32_t)((((int64_t)V_2) == ((int64_t)(((int64_t)0))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		__this->f9 = ((((int64_t)V_2) == ((int64_t)(((int64_t)0))))? 1 : 0);
		return;
	}

IL_007c:
	{
		V_3 = 0;
		if (V_1)
		{
			goto IL_00a3;
		}
	}
	{
		V_1 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_1 = m9399(NULL, V_2, &m9399_MI);
		V_4 = L_1;
		if ((((int32_t)V_4) >= ((int32_t)((int32_t)15))))
		{
			goto IL_00a1;
		}
	}
	{
		V_3 = ((int32_t)(V_4-((int32_t)15)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int64_t L_2 = m9390(NULL, ((-V_3)), &m9390_MI);
		V_2 = ((int64_t)((int64_t)V_2*(int64_t)L_2));
	}

IL_00a1:
	{
		goto IL_00b8;
	}

IL_00a3:
	{
		V_2 = ((int64_t)((int64_t)((int64_t)(((int64_t)(V_2+((int64_t)4503599627370495LL)))+(((int64_t)1))))*(int64_t)(((int64_t)((int32_t)10)))));
		V_3 = (-1);
	}

IL_00b8:
	{
		V_5 = (((uint64_t)(((uint32_t)V_2))));
		V_6 = ((int64_t)((uint64_t)V_2>>((int32_t)32)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		V_7 = (*((int64_t*)((uint64_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f0)+((int32_t)((int32_t)V_1*(int32_t)8))))));
		V_8 = ((int64_t)((uint64_t)V_7>>((int32_t)32)));
		V_7 = (((uint64_t)(((uint32_t)V_7))));
		V_9 = ((int64_t)(((int64_t)(((int64_t)((int64_t)V_6*(int64_t)V_7))+((int64_t)((int64_t)V_5*(int64_t)V_8))))+((int64_t)((uint64_t)((int64_t)((int64_t)V_5*(int64_t)V_7))>>((int32_t)32)))));
		V_10 = ((int64_t)(((int64_t)((int64_t)V_6*(int64_t)V_8))+((int64_t)((uint64_t)V_9>>((int32_t)32)))));
		goto IL_011e;
	}

IL_0101:
	{
		V_9 = ((int64_t)((int64_t)((int64_t)((int64_t)V_9&(int64_t)(((uint64_t)(((uint32_t)(-1)))))))*(int64_t)(((int64_t)((int32_t)10)))));
		V_10 = ((int64_t)(((int64_t)((int64_t)V_10*(int64_t)(((int64_t)((int32_t)10)))))+((int64_t)((uint64_t)V_9>>((int32_t)32)))));
		V_3 = ((int32_t)(V_3-1));
	}

IL_011e:
	{
		if ((((int64_t)V_10) < ((int64_t)((int64_t)10000000000000000LL))))
		{
			goto IL_0101;
		}
	}
	{
		if (!((int64_t)((int64_t)V_9&(int64_t)(((uint64_t)(((uint32_t)((int32_t)-2147483648))))))))
		{
			goto IL_013d;
		}
	}
	{
		V_10 = ((int64_t)(V_10+(((int64_t)1))));
	}

IL_013d:
	{
		V_11 = ((int32_t)17);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		__this->f18 = ((int32_t)(((int32_t)((*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f1)+((int32_t)((int32_t)V_1*(int32_t)4))))))+V_3))+V_11));
		int32_t L_3 = m9400(__this, &m9400_MI);
		V_12 = L_3;
		if ((((int32_t)V_11) <= ((int32_t)V_12)))
		{
			goto IL_0180;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int64_t L_4 = m9390(NULL, ((int32_t)(V_11-V_12)), &m9390_MI);
		V_13 = L_4;
		V_10 = ((int64_t)((int64_t)((int64_t)(V_10+((int64_t)((int64_t)V_13>>(int32_t)1))))/(int64_t)V_13));
		V_11 = V_12;
	}

IL_0180:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int64_t L_5 = m9390(NULL, V_11, &m9390_MI);
		if ((((int64_t)V_10) < ((int64_t)L_5)))
		{
			goto IL_019f;
		}
	}
	{
		V_11 = ((int32_t)(V_11+1));
		int32_t L_6 = (__this->f18);
		__this->f18 = ((int32_t)(L_6+1));
	}

IL_019f:
	{
		m9392(__this, V_10, &m9392_MI);
		int32_t L_7 = m9428(__this, &m9428_MI);
		__this->f17 = L_7;
		int32_t L_8 = (__this->f17);
		__this->f16 = ((int32_t)(V_11-L_8));
		return;
	}
}
 void m9409 (t1664 * __this, t7* p0, t1126  p1, MethodInfo* method){
	t841* V_0 = {0};
	int32_t V_1 = 0;
	{
		m9402(__this, p0, &m9402_MI);
		__this->f15 = ((int32_t)100);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1126_TI));
		t841* L_0 = m5719(NULL, p1, &m5719_MI);
		V_0 = L_0;
		int32_t L_1 = 3;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_1))&(int32_t)((int32_t)2031616)))>>(int32_t)((int32_t)16)));
		int32_t L_2 = 3;
		__this->f12 = ((((int32_t)((((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_2))) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_3 = 0;
		if ((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_3)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_4 = 1;
		if ((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_4)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_5 = 2;
		if ((*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_5)))
		{
			goto IL_0058;
		}
	}
	{
		__this->f18 = ((-V_1));
		__this->f12 = 1;
		__this->f16 = 0;
		return;
	}

IL_0058:
	{
		int32_t L_6 = 2;
		int32_t L_7 = 1;
		int32_t L_8 = 0;
		m9393(__this, (*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_6)), ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_7))))<<(int32_t)((int32_t)32)))|(int64_t)(((uint64_t)(((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_0, L_8)))))))), &m9393_MI);
		int32_t L_9 = m9398(__this, &m9398_MI);
		__this->f16 = L_9;
		int32_t L_10 = (__this->f16);
		__this->f18 = ((int32_t)(L_10-V_1));
		int32_t L_11 = (__this->f14);
		if ((((uint32_t)L_11) != ((uint32_t)(-1))))
		{
			goto IL_009a;
		}
	}
	{
		uint16_t L_12 = (__this->f13);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)71))))
		{
			goto IL_00b9;
		}
	}

IL_009a:
	{
		int32_t L_13 = m9428(__this, &m9428_MI);
		__this->f17 = L_13;
		int32_t L_14 = (__this->f16);
		int32_t L_15 = (__this->f17);
		__this->f16 = ((int32_t)(L_14-L_15));
	}

IL_00b9:
	{
		return;
	}
}
 void m9410 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f24 = 0;
		t200* L_0 = (__this->f23);
		if ((((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))) >= ((int32_t)p0)))
		{
			goto IL_001e;
		}
	}
	{
		__this->f23 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), p0));
	}

IL_001e:
	{
		return;
	}
}
 void m9411 (t1664 * __this, int32_t p0, MethodInfo* method){
	t200* V_0 = {0};
	{
		V_0 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), p0));
		t200* L_0 = (__this->f23);
		int32_t L_1 = (__this->f24);
		m5951(NULL, (t20 *)(t20 *)L_0, (t20 *)(t20 *)V_0, L_1, &m5951_MI);
		__this->f23 = V_0;
		return;
	}
}
 void m9412 (t1664 * __this, uint16_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f24);
		t200* L_1 = (__this->f23);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = (__this->f24);
		m9411(__this, ((int32_t)(L_2+((int32_t)10))), &m9411_MI);
	}

IL_001f:
	{
		t200* L_3 = (__this->f23);
		int32_t L_4 = (__this->f24);
		int32_t L_5 = L_4;
		V_0 = L_5;
		__this->f24 = ((int32_t)(L_5+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, V_0)) = (uint16_t)p0;
		return;
	}
}
 void m9413 (t1664 * __this, uint16_t p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f24);
		t200* L_1 = (__this->f23);
		if ((((int32_t)((int32_t)(L_0+p1))) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = (__this->f24);
		m9411(__this, ((int32_t)(((int32_t)(L_2+p1))+((int32_t)10))), &m9411_MI);
	}

IL_0023:
	{
		goto IL_003e;
	}

IL_0025:
	{
		t200* L_3 = (__this->f23);
		int32_t L_4 = (__this->f24);
		int32_t L_5 = L_4;
		V_0 = L_5;
		__this->f24 = ((int32_t)(L_5+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, V_0)) = (uint16_t)p0;
	}

IL_003e:
	{
		int32_t L_6 = p1;
		p1 = ((int32_t)(L_6-1));
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		return;
	}
}
 void m9414 (t1664 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		V_0 = L_0;
		int32_t L_1 = (__this->f24);
		t200* L_2 = (__this->f23);
		if ((((int32_t)((int32_t)(L_1+V_0))) <= ((int32_t)(((int32_t)(((t20 *)L_2)->max_length))))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = (__this->f24);
		m9411(__this, ((int32_t)(((int32_t)(L_3+V_0))+((int32_t)10))), &m9411_MI);
	}

IL_002a:
	{
		V_1 = 0;
		goto IL_0051;
	}

IL_002e:
	{
		t200* L_4 = (__this->f23);
		int32_t L_5 = (__this->f24);
		int32_t L_6 = L_5;
		V_2 = L_6;
		__this->f24 = ((int32_t)(L_6+1));
		uint16_t L_7 = m1741(p0, V_1, &m1741_MI);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_4, V_2)) = (uint16_t)L_7;
		V_1 = ((int32_t)(V_1+1));
	}

IL_0051:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_002e;
		}
	}
	{
		return;
	}
}
 t1125 * m9415 (t1664 * __this, t29 * p0, MethodInfo* method){
	{
		t1125 * L_0 = (__this->f7);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		if (p0)
		{
			goto IL_0012;
		}
	}
	{
		t1125 * L_1 = (__this->f7);
		return L_1;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1125_TI));
		t1125 * L_2 = m6958(NULL, p0, &m6958_MI);
		return L_2;
	}
}
 void m9416 (t1664 * __this, t633 * p0, MethodInfo* method){
	{
		if (!p0)
		{
			goto IL_0019;
		}
	}
	{
		bool L_0 = m6856(p0, &m6856_MI);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		t1125 * L_1 = (t1125 *)VirtFuncInvoker0< t1125 * >::Invoke(&m6854_MI, p0);
		__this->f7 = L_1;
		goto IL_0020;
	}

IL_0019:
	{
		__this->f7 = (t1125 *)NULL;
	}

IL_0020:
	{
		return;
	}
}
 int32_t m9417 (t1664 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f18);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = (__this->f18);
		G_B3_0 = L_1;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		return G_B3_0;
	}
}
 int32_t m9418 (t1664 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f16);
		int32_t L_1 = (__this->f18);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = (__this->f16);
		int32_t L_3 = (__this->f18);
		G_B3_0 = ((int32_t)(L_2-L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
 bool m9419 (t1664 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f15);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)15))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (__this->f15);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)7))? 1 : 0);
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
	}

IL_0016:
	{
		return G_B3_0;
	}
}
 bool m9420 (t1664 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f16);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
 bool m9421 (t1664 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f16);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (__this->f18);
		G_B3_0 = ((((int32_t)((((int32_t)L_1) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
 void m9422 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f16);
		m9424(__this, ((int32_t)(L_0-p0)), &m9424_MI);
		return;
	}
}
 bool m9423 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f16);
		int32_t L_1 = (__this->f18);
		bool L_2 = m9424(__this, ((int32_t)(((int32_t)(L_0-L_1))-p0)), &m9424_MI);
		return L_2;
	}
}
 bool m9424 (t1664 * __this, int32_t p0, MethodInfo* method){
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	{
		if ((((int32_t)p0) > ((int32_t)0)))
		{
			goto IL_0006;
		}
	}
	{
		return 0;
	}

IL_0006:
	{
		int32_t L_0 = (__this->f16);
		if ((((int32_t)p0) <= ((int32_t)L_0)))
		{
			goto IL_004e;
		}
	}
	{
		__this->f16 = 0;
		__this->f18 = 1;
		int32_t L_1 = 0;
		V_4 = L_1;
		__this->f22 = L_1;
		uint32_t L_2 = V_4;
		V_4 = L_2;
		__this->f21 = L_2;
		uint32_t L_3 = V_4;
		V_4 = L_3;
		__this->f20 = L_3;
		__this->f19 = V_4;
		__this->f12 = 1;
		return 0;
	}

IL_004e:
	{
		int32_t L_4 = (__this->f17);
		p0 = ((int32_t)(p0+L_4));
		int32_t L_5 = (__this->f16);
		int32_t L_6 = (__this->f17);
		__this->f16 = ((int32_t)(L_5+L_6));
		goto IL_00af;
	}

IL_006f:
	{
		uint32_t L_7 = (__this->f20);
		__this->f19 = L_7;
		uint32_t L_8 = (__this->f21);
		__this->f20 = L_8;
		uint32_t L_9 = (__this->f22);
		__this->f21 = L_9;
		__this->f22 = 0;
		int32_t L_10 = (__this->f16);
		__this->f16 = ((int32_t)(L_10-8));
		p0 = ((int32_t)(p0-8));
	}

IL_00af:
	{
		if ((((int32_t)p0) > ((int32_t)8)))
		{
			goto IL_006f;
		}
	}
	{
		p0 = ((int32_t)((int32_t)((int32_t)(p0-1))<<(int32_t)2));
		uint32_t L_11 = (__this->f19);
		V_0 = ((int32_t)((uint32_t)L_11>>((int32_t)((int32_t)p0&(int32_t)((int32_t)31)))));
		V_1 = ((int32_t)((int32_t)V_0&(int32_t)((int32_t)15)));
		__this->f19 = ((int32_t)((int32_t)((int32_t)((int32_t)V_0^(int32_t)V_1))<<(int32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)31)))));
		V_2 = 0;
		if ((((uint32_t)V_1) < ((uint32_t)5)))
		{
			goto IL_0134;
		}
	}
	{
		uint32_t L_12 = (__this->f19);
		__this->f19 = ((int32_t)((int32_t)L_12|(int32_t)((int32_t)((uint32_t)((int32_t)-1717986919)>>((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)28)-p0))&(int32_t)((int32_t)31)))&(int32_t)((int32_t)31)))))));
		m9426(__this, &m9426_MI);
		int32_t L_13 = m9398(__this, &m9398_MI);
		V_3 = L_13;
		int32_t L_14 = (__this->f16);
		V_2 = ((((int32_t)((((int32_t)V_3) == ((int32_t)L_14))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_15 = (__this->f18);
		int32_t L_16 = (__this->f16);
		__this->f18 = ((int32_t)(((int32_t)(L_15+V_3))-L_16));
		__this->f16 = V_3;
	}

IL_0134:
	{
		m9425(__this, &m9425_MI);
		return V_2;
	}
}
 void m9425 (t1664 * __this, MethodInfo* method){
	{
		int32_t L_0 = m9428(__this, &m9428_MI);
		__this->f17 = L_0;
		int32_t L_1 = (__this->f16);
		int32_t L_2 = (__this->f17);
		__this->f16 = ((int32_t)(L_1-L_2));
		int32_t L_3 = (__this->f16);
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		__this->f17 = 0;
		__this->f18 = 1;
		__this->f12 = 1;
	}

IL_003c:
	{
		return;
	}
}
 void m9426 (t1664 * __this, MethodInfo* method){
	{
		uint32_t L_0 = (__this->f19);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-1717986919))))
		{
			goto IL_0075;
		}
	}
	{
		__this->f19 = 0;
		uint32_t L_1 = (__this->f20);
		if ((((uint32_t)L_1) != ((uint32_t)((int32_t)-1717986919))))
		{
			goto IL_0062;
		}
	}
	{
		__this->f20 = 0;
		uint32_t L_2 = (__this->f21);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)-1717986919))))
		{
			goto IL_004f;
		}
	}
	{
		__this->f21 = 0;
		uint32_t L_3 = (__this->f22);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_4 = m9427(NULL, L_3, &m9427_MI);
		__this->f22 = L_4;
		goto IL_0060;
	}

IL_004f:
	{
		uint32_t L_5 = (__this->f21);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_6 = m9427(NULL, L_5, &m9427_MI);
		__this->f21 = L_6;
	}

IL_0060:
	{
		goto IL_0073;
	}

IL_0062:
	{
		uint32_t L_7 = (__this->f20);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_8 = m9427(NULL, L_7, &m9427_MI);
		__this->f20 = L_8;
	}

IL_0073:
	{
		goto IL_0086;
	}

IL_0075:
	{
		uint32_t L_9 = (__this->f19);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_10 = m9427(NULL, L_9, &m9427_MI);
		__this->f19 = L_10;
	}

IL_0086:
	{
		return;
	}
}
 uint32_t m9427 (t29 * __this, uint32_t p0, MethodInfo* method){
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)65535)))) != ((uint32_t)((int32_t)39321))))
		{
			goto IL_0058;
		}
	}
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)16777215)))) != ((uint32_t)((int32_t)10066329))))
		{
			goto IL_003a;
		}
	}
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)268435455)))) != ((uint32_t)((int32_t)161061273))))
		{
			goto IL_0032;
		}
	}
	{
		return ((int32_t)(p0+((int32_t)107374183)));
	}

IL_0032:
	{
		return ((int32_t)(p0+((int32_t)6710887)));
	}

IL_003a:
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)1048575)))) != ((uint32_t)((int32_t)629145))))
		{
			goto IL_0050;
		}
	}
	{
		return ((int32_t)(p0+((int32_t)419431)));
	}

IL_0050:
	{
		return ((int32_t)(p0+((int32_t)26215)));
	}

IL_0058:
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)255)))) != ((uint32_t)((int32_t)153))))
		{
			goto IL_0081;
		}
	}
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)4095)))) != ((uint32_t)((int32_t)2457))))
		{
			goto IL_007c;
		}
	}
	{
		return ((int32_t)(p0+((int32_t)1639)));
	}

IL_007c:
	{
		return ((int32_t)(p0+((int32_t)103)));
	}

IL_0081:
	{
		if ((((uint32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)15)))) != ((uint32_t)((int32_t)9))))
		{
			goto IL_008d;
		}
	}
	{
		return ((int32_t)(p0+7));
	}

IL_008d:
	{
		return ((int32_t)(p0+1));
	}
}
 int32_t m9428 (t1664 * __this, MethodInfo* method){
	{
		uint32_t L_0 = (__this->f19);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		uint32_t L_1 = (__this->f19);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_2 = m9429(NULL, L_1, &m9429_MI);
		return L_2;
	}

IL_0014:
	{
		uint32_t L_3 = (__this->f20);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		uint32_t L_4 = (__this->f20);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_5 = m9429(NULL, L_4, &m9429_MI);
		return ((int32_t)(L_5+8));
	}

IL_002a:
	{
		uint32_t L_6 = (__this->f21);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		uint32_t L_7 = (__this->f21);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_8 = m9429(NULL, L_7, &m9429_MI);
		return ((int32_t)(L_8+((int32_t)16)));
	}

IL_0041:
	{
		uint32_t L_9 = (__this->f22);
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		uint32_t L_10 = (__this->f22);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_11 = m9429(NULL, L_10, &m9429_MI);
		return ((int32_t)(L_11+((int32_t)24)));
	}

IL_0058:
	{
		int32_t L_12 = (__this->f16);
		return L_12;
	}
}
 int32_t m9429 (t29 * __this, uint32_t p0, MethodInfo* method){
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)65535))))
		{
			goto IL_002c;
		}
	}
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)16777215))))
		{
			goto IL_001f;
		}
	}
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)268435455))))
		{
			goto IL_001d;
		}
	}
	{
		return 7;
	}

IL_001d:
	{
		return 6;
	}

IL_001f:
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)1048575))))
		{
			goto IL_002a;
		}
	}
	{
		return 5;
	}

IL_002a:
	{
		return 4;
	}

IL_002c:
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)255))))
		{
			goto IL_0042;
		}
	}
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)4095))))
		{
			goto IL_0040;
		}
	}
	{
		return 3;
	}

IL_0040:
	{
		return 2;
	}

IL_0042:
	{
		if (((int32_t)((int32_t)p0&(int32_t)((int32_t)15))))
		{
			goto IL_004a;
		}
	}
	{
		return 1;
	}

IL_004a:
	{
		return 0;
	}
}
 t1664 * m9430 (t29 * __this, MethodInfo* method){
	t1664 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		V_0 = (((t1664_TSFs*)il2cpp_codegen_get_thread_static_data(InitializedTypeInfo(&t1664_TI)))->f25);
		((t1664_TSFs*)il2cpp_codegen_get_thread_static_data(InitializedTypeInfo(&t1664_TI)))->f25 = (t1664 *)NULL;
		if (V_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		t1436 * L_0 = m8791(NULL, &m8791_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_1 = (t1664 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1664_TI));
		m9387(L_1, L_0, &m9387_MI);
		return L_1;
	}

IL_001a:
	{
		return V_0;
	}
}
 void m9431 (t1664 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		((t1664_TSFs*)il2cpp_codegen_get_thread_static_data(InitializedTypeInfo(&t1664_TI)))->f25 = __this;
		return;
	}
}
extern MethodInfo m9432_MI;
 void m9432 (t29 * __this, t633 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		if (!(((t1664_TSFs*)il2cpp_codegen_get_thread_static_data(InitializedTypeInfo(&t1664_TI)))->f25))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		m9416((((t1664_TSFs*)il2cpp_codegen_get_thread_static_data(InitializedTypeInfo(&t1664_TI)))->f25), p0, &m9416_MI);
	}

IL_0012:
	{
		return;
	}
}
extern MethodInfo m9433_MI;
 t7* m9433 (t29 * __this, t7* p0, int8_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9404(V_0, p0, (((int32_t)p1)), 3, &m9404_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9434_MI;
 t7* m9434 (t29 * __this, t7* p0, uint8_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9404(V_0, p0, p1, 3, &m9404_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9435_MI;
 t7* m9435 (t29 * __this, t7* p0, uint16_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9404(V_0, p0, p1, 5, &m9404_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9436_MI;
 t7* m9436 (t29 * __this, t7* p0, int16_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9404(V_0, p0, p1, 5, &m9404_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
 t7* m9437 (t29 * __this, t7* p0, uint32_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9405(V_0, p0, p1, ((int32_t)10), &m9405_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
 t7* m9438 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9404(V_0, p0, p1, ((int32_t)10), &m9404_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
 t7* m9439 (t29 * __this, t7* p0, uint64_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9407(V_0, p0, p1, &m9407_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
 t7* m9440 (t29 * __this, t7* p0, int64_t p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9406(V_0, p0, p1, &m9406_MI);
		t7* L_1 = m9451(V_0, p0, p2, &m9451_MI);
		V_1 = L_1;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9441_MI;
 t7* m9441 (t29 * __this, t7* p0, float p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t1125 * V_1 = {0};
	t7* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9408(V_0, p0, (((double)p1)), 7, &m9408_MI);
		t1125 * L_1 = m9415(V_0, p2, &m9415_MI);
		V_1 = L_1;
		bool L_2 = (V_0->f8);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		t7* L_3 = m6937(V_1, &m6937_MI);
		V_2 = L_3;
		goto IL_0069;
	}

IL_0029:
	{
		bool L_4 = (V_0->f9);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		bool L_5 = (V_0->f12);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		t7* L_6 = m6954(V_1, &m6954_MI);
		V_2 = L_6;
		goto IL_0049;
	}

IL_0042:
	{
		t7* L_7 = m6938(V_1, &m6938_MI);
		V_2 = L_7;
	}

IL_0049:
	{
		goto IL_0069;
	}

IL_004b:
	{
		uint16_t L_8 = (V_0->f13);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)82))))
		{
			goto IL_0060;
		}
	}
	{
		t7* L_9 = m9458(V_0, p1, V_1, &m9458_MI);
		V_2 = L_9;
		goto IL_0069;
	}

IL_0060:
	{
		t7* L_10 = m9452(V_0, p0, V_1, &m9452_MI);
		V_2 = L_10;
	}

IL_0069:
	{
		m9431(V_0, &m9431_MI);
		return V_2;
	}
}
extern MethodInfo m9442_MI;
 t7* m9442 (t29 * __this, t7* p0, double p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t1125 * V_1 = {0};
	t7* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9408(V_0, p0, p1, ((int32_t)15), &m9408_MI);
		t1125 * L_1 = m9415(V_0, p2, &m9415_MI);
		V_1 = L_1;
		bool L_2 = (V_0->f8);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		t7* L_3 = m6937(V_1, &m6937_MI);
		V_2 = L_3;
		goto IL_0069;
	}

IL_0029:
	{
		bool L_4 = (V_0->f9);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		bool L_5 = (V_0->f12);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		t7* L_6 = m6954(V_1, &m6954_MI);
		V_2 = L_6;
		goto IL_0049;
	}

IL_0042:
	{
		t7* L_7 = m6938(V_1, &m6938_MI);
		V_2 = L_7;
	}

IL_0049:
	{
		goto IL_0069;
	}

IL_004b:
	{
		uint16_t L_8 = (V_0->f13);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)82))))
		{
			goto IL_0060;
		}
	}
	{
		t7* L_9 = m9457(V_0, p1, V_1, &m9457_MI);
		V_2 = L_9;
		goto IL_0069;
	}

IL_0060:
	{
		t7* L_10 = m9452(V_0, p0, V_1, &m9452_MI);
		V_2 = L_10;
	}

IL_0069:
	{
		m9431(V_0, &m9431_MI);
		return V_2;
	}
}
extern MethodInfo m9443_MI;
 t7* m9443 (t29 * __this, t7* p0, t1126  p1, t29 * p2, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9409(V_0, p0, p1, &m9409_MI);
		t1125 * L_1 = m9415(V_0, p2, &m9415_MI);
		t7* L_2 = m9452(V_0, p0, L_1, &m9452_MI);
		V_1 = L_2;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9444_MI;
 t7* m9444 (t29 * __this, uint32_t p0, t29 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		if ((((uint32_t)p0) < ((uint32_t)((int32_t)100000000))))
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t7* L_0 = m9437(NULL, (t7*)NULL, p0, p1, &m9437_MI);
		return L_0;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_1 = m9430(NULL, &m9430_MI);
		V_0 = L_1;
		t7* L_2 = m9450(V_0, p0, p1, &m9450_MI);
		V_1 = L_2;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9445_MI;
 t7* m9445 (t29 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)100000000))))
		{
			goto IL_0010;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)-100000000))))
		{
			goto IL_0019;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t7* L_0 = m9438(NULL, (t7*)NULL, p0, p1, &m9438_MI);
		return L_0;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_1 = m9430(NULL, &m9430_MI);
		V_0 = L_1;
		t7* L_2 = m9450(V_0, p0, p1, &m9450_MI);
		V_1 = L_2;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9446_MI;
 t7* m9446 (t29 * __this, uint64_t p0, t29 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		if ((((uint64_t)p0) < ((uint64_t)(((int64_t)((int32_t)100000000))))))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t7* L_0 = m9439(NULL, (t7*)NULL, p0, p1, &m9439_MI);
		return L_0;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_1 = m9430(NULL, &m9430_MI);
		V_0 = L_1;
		t7* L_2 = m9450(V_0, (((int32_t)p0)), p1, &m9450_MI);
		V_1 = L_2;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9447_MI;
 t7* m9447 (t29 * __this, int64_t p0, t29 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		if ((((int64_t)p0) >= ((int64_t)(((int64_t)((int32_t)100000000))))))
		{
			goto IL_0012;
		}
	}
	{
		if ((((int64_t)p0) > ((int64_t)(((int64_t)((int32_t)-100000000))))))
		{
			goto IL_001b;
		}
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t7* L_0 = m9440(NULL, (t7*)NULL, p0, p1, &m9440_MI);
		return L_0;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_1 = m9430(NULL, &m9430_MI);
		V_0 = L_1;
		t7* L_2 = m9450(V_0, (((int32_t)p0)), p1, &m9450_MI);
		V_1 = L_2;
		m9431(V_0, &m9431_MI);
		return V_1;
	}
}
extern MethodInfo m9448_MI;
 t7* m9448 (t29 * __this, float p0, t29 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t1125 * V_1 = {0};
	t7* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		m9408(V_0, (t7*)NULL, (((double)p0)), 7, &m9408_MI);
		t1125 * L_1 = m9415(V_0, p1, &m9415_MI);
		V_1 = L_1;
		bool L_2 = (V_0->f8);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		t7* L_3 = m6937(V_1, &m6937_MI);
		V_2 = L_3;
		goto IL_0054;
	}

IL_0029:
	{
		bool L_4 = (V_0->f9);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		bool L_5 = (V_0->f12);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		t7* L_6 = m6954(V_1, &m6954_MI);
		V_2 = L_6;
		goto IL_0049;
	}

IL_0042:
	{
		t7* L_7 = m6938(V_1, &m6938_MI);
		V_2 = L_7;
	}

IL_0049:
	{
		goto IL_0054;
	}

IL_004b:
	{
		t7* L_8 = m9459(V_0, (-1), V_1, &m9459_MI);
		V_2 = L_8;
	}

IL_0054:
	{
		m9431(V_0, &m9431_MI);
		return V_2;
	}
}
extern MethodInfo m9449_MI;
 t7* m9449 (t29 * __this, double p0, t29 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t1125 * V_1 = {0};
	t7* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		t1664 * L_0 = m9430(NULL, &m9430_MI);
		V_0 = L_0;
		t1125 * L_1 = m9415(V_0, p1, &m9415_MI);
		V_1 = L_1;
		m9408(V_0, (t7*)NULL, p0, ((int32_t)15), &m9408_MI);
		bool L_2 = (V_0->f8);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		t7* L_3 = m6937(V_1, &m6937_MI);
		V_2 = L_3;
		goto IL_0054;
	}

IL_0029:
	{
		bool L_4 = (V_0->f9);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		bool L_5 = (V_0->f12);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		t7* L_6 = m6954(V_1, &m6954_MI);
		V_2 = L_6;
		goto IL_0049;
	}

IL_0042:
	{
		t7* L_7 = m6938(V_1, &m6938_MI);
		V_2 = L_7;
	}

IL_0049:
	{
		goto IL_0054;
	}

IL_004b:
	{
		t7* L_8 = m9459(V_0, (-1), V_1, &m9459_MI);
		V_2 = L_8;
	}

IL_0054:
	{
		m9431(V_0, &m9431_MI);
		return V_2;
	}
}
 t7* m9450 (t1664 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = 0;
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		t1125 * L_0 = m9415(__this, p1, &m9415_MI);
		t7* L_1 = m6939(L_0, &m6939_MI);
		V_0 = L_1;
		int32_t L_2 = m1715(V_0, &m1715_MI);
		m9410(__this, ((int32_t)(8+L_2)), &m9410_MI);
		p0 = ((-p0));
		m9414(__this, V_0, &m9414_MI);
		goto IL_0035;
	}

IL_002e:
	{
		m9410(__this, 8, &m9410_MI);
	}

IL_0035:
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)10000))))
		{
			goto IL_005f;
		}
	}
	{
		V_1 = ((int32_t)((int32_t)p0/(int32_t)((int32_t)10000)));
		m9475(__this, V_1, 0, &m9475_MI);
		m9475(__this, ((int32_t)(p0-((int32_t)((int32_t)V_1*(int32_t)((int32_t)10000))))), 1, &m9475_MI);
		goto IL_0067;
	}

IL_005f:
	{
		m9475(__this, p0, 0, &m9475_MI);
	}

IL_0067:
	{
		t200* L_3 = (__this->f23);
		int32_t L_4 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_5 = m5627(L_5, L_3, 0, L_4, &m5555_MI);
		return L_5;
	}
}
 t7* m9451 (t1664 * __this, t7* p0, t29 * p1, MethodInfo* method){
	t1125 * V_0 = {0};
	uint16_t V_1 = 0x0;
	{
		t1125 * L_0 = m9415(__this, p1, &m9415_MI);
		V_0 = L_0;
		uint16_t L_1 = (__this->f13);
		V_1 = L_1;
		if (((uint16_t)(V_1-((int32_t)67))) == 0)
		{
			goto IL_005a;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 1)
		{
			goto IL_0068;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 2)
		{
			goto IL_0076;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 3)
		{
			goto IL_0084;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 4)
		{
			goto IL_0092;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 5)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 6)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 7)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 8)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 9)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 10)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 11)
		{
			goto IL_00b2;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 12)
		{
			goto IL_0050;
		}
		if (((uint16_t)(V_1-((int32_t)67))) == 13)
		{
			goto IL_00c0;
		}
	}

IL_0050:
	{
		if ((((int32_t)V_1) == ((int32_t)((int32_t)88))))
		{
			goto IL_00ce;
		}
	}
	{
		goto IL_00db;
	}

IL_005a:
	{
		int32_t L_2 = (__this->f14);
		t7* L_3 = m9453(__this, L_2, V_0, &m9453_MI);
		return L_3;
	}

IL_0068:
	{
		int32_t L_4 = (__this->f14);
		t7* L_5 = m9454(__this, L_4, V_0, &m9454_MI);
		return L_5;
	}

IL_0076:
	{
		int32_t L_6 = (__this->f14);
		t7* L_7 = m9462(__this, L_6, V_0, &m9462_MI);
		return L_7;
	}

IL_0084:
	{
		int32_t L_8 = (__this->f14);
		t7* L_9 = m9456(__this, L_8, V_0, &m9456_MI);
		return L_9;
	}

IL_0092:
	{
		int32_t L_10 = (__this->f14);
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_00a4;
		}
	}
	{
		t7* L_11 = m9454(__this, (-1), V_0, &m9454_MI);
		return L_11;
	}

IL_00a4:
	{
		int32_t L_12 = (__this->f14);
		t7* L_13 = m9459(__this, L_12, V_0, &m9459_MI);
		return L_13;
	}

IL_00b2:
	{
		int32_t L_14 = (__this->f14);
		t7* L_15 = m9460(__this, L_14, V_0, &m9460_MI);
		return L_15;
	}

IL_00c0:
	{
		int32_t L_16 = (__this->f14);
		t7* L_17 = m9461(__this, L_16, V_0, &m9461_MI);
		return L_17;
	}

IL_00ce:
	{
		int32_t L_18 = (__this->f14);
		t7* L_19 = m9455(__this, L_18, &m9455_MI);
		return L_19;
	}

IL_00db:
	{
		bool L_20 = (__this->f10);
		if (!L_20)
		{
			goto IL_00ec;
		}
	}
	{
		t7* L_21 = m9464(__this, p0, V_0, &m9464_MI);
		return L_21;
	}

IL_00ec:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_22 = m1685(NULL, (t7*) &_stringLiteral2217, p0, (t7*) &_stringLiteral2218, &m1685_MI);
		t901 * L_23 = (t901 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t901_TI));
		m4003(L_23, L_22, &m4003_MI);
		il2cpp_codegen_raise_exception(L_23);
	}
}
 t7* m9452 (t1664 * __this, t7* p0, t1125 * p1, MethodInfo* method){
	uint16_t V_0 = 0x0;
	{
		uint16_t L_0 = (__this->f13);
		V_0 = L_0;
		if (((uint16_t)(V_0-((int32_t)67))) == 0)
		{
			goto IL_0040;
		}
		if (((uint16_t)(V_0-((int32_t)67))) == 1)
		{
			goto IL_0024;
		}
		if (((uint16_t)(V_0-((int32_t)67))) == 2)
		{
			goto IL_004e;
		}
		if (((uint16_t)(V_0-((int32_t)67))) == 3)
		{
			goto IL_005c;
		}
		if (((uint16_t)(V_0-((int32_t)67))) == 4)
		{
			goto IL_006a;
		}
	}

IL_0024:
	{
		if (((uint16_t)(V_0-((int32_t)78))) == 0)
		{
			goto IL_0078;
		}
		if (((uint16_t)(V_0-((int32_t)78))) == 1)
		{
			goto IL_0039;
		}
		if (((uint16_t)(V_0-((int32_t)78))) == 2)
		{
			goto IL_0086;
		}
	}

IL_0039:
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)88))))
		{
			goto IL_0094;
		}
	}
	{
		goto IL_0094;
	}

IL_0040:
	{
		int32_t L_1 = (__this->f14);
		t7* L_2 = m9453(__this, L_1, p1, &m9453_MI);
		return L_2;
	}

IL_004e:
	{
		int32_t L_3 = (__this->f14);
		t7* L_4 = m9462(__this, L_3, p1, &m9462_MI);
		return L_4;
	}

IL_005c:
	{
		int32_t L_5 = (__this->f14);
		t7* L_6 = m9456(__this, L_5, p1, &m9456_MI);
		return L_6;
	}

IL_006a:
	{
		int32_t L_7 = (__this->f14);
		t7* L_8 = m9459(__this, L_7, p1, &m9459_MI);
		return L_8;
	}

IL_0078:
	{
		int32_t L_9 = (__this->f14);
		t7* L_10 = m9460(__this, L_9, p1, &m9460_MI);
		return L_10;
	}

IL_0086:
	{
		int32_t L_11 = (__this->f14);
		t7* L_12 = m9461(__this, L_11, p1, &m9461_MI);
		return L_12;
	}

IL_0094:
	{
		bool L_13 = (__this->f10);
		if (!L_13)
		{
			goto IL_00a5;
		}
	}
	{
		t7* L_14 = m9464(__this, p0, p1, &m9464_MI);
		return L_14;
	}

IL_00a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_15 = m1685(NULL, (t7*) &_stringLiteral2217, p0, (t7*) &_stringLiteral2218, &m1685_MI);
		t901 * L_16 = (t901 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t901_TI));
		m4003(L_16, L_15, &m4003_MI);
		il2cpp_codegen_raise_exception(L_16);
	}
}
 t7* m9453 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = p0;
		goto IL_000d;
	}

IL_0007:
	{
		int32_t L_0 = m6928(p1, &m6928_MI);
		G_B3_0 = L_0;
	}

IL_000d:
	{
		p0 = G_B3_0;
		m9423(__this, p0, &m9423_MI);
		int32_t L_1 = m9417(__this, &m9417_MI);
		m9410(__this, ((int32_t)(((int32_t)(((int32_t)((int32_t)L_1*(int32_t)2))+((int32_t)((int32_t)p0*(int32_t)2))))+((int32_t)16))), &m9410_MI);
		bool L_2 = (__this->f12);
		if (!L_2)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_3 = m6933(p1, &m6933_MI);
		V_0 = L_3;
		if (V_0 == 0)
		{
			goto IL_0051;
		}
		if (V_0 == 1)
		{
			goto IL_0075;
		}
		if (V_0 == 2)
		{
			goto IL_005f;
		}
	}
	{
		goto IL_0075;
	}

IL_0051:
	{
		t7* L_4 = m6934(p1, &m6934_MI);
		m9414(__this, L_4, &m9414_MI);
		goto IL_0075;
	}

IL_005f:
	{
		t7* L_5 = m6934(p1, &m6934_MI);
		m9414(__this, L_5, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		goto IL_0075;
	}

IL_0075:
	{
		goto IL_01e1;
	}

IL_007a:
	{
		int32_t L_6 = m6932(p1, &m6932_MI);
		V_0 = L_6;
		if (V_0 == 0)
		{
			goto IL_00cc;
		}
		if (V_0 == 1)
		{
			goto IL_00e5;
		}
		if (V_0 == 2)
		{
			goto IL_0102;
		}
		if (V_0 == 3)
		{
			goto IL_011f;
		}
		if (V_0 == 4)
		{
			goto IL_0130;
		}
		if (V_0 == 5)
		{
			goto IL_013d;
		}
		if (V_0 == 6)
		{
			goto IL_01e1;
		}
		if (V_0 == 7)
		{
			goto IL_01e1;
		}
		if (V_0 == 8)
		{
			goto IL_014e;
		}
		if (V_0 == 9)
		{
			goto IL_015f;
		}
		if (V_0 == 10)
		{
			goto IL_01e1;
		}
		if (V_0 == 11)
		{
			goto IL_0181;
		}
		if (V_0 == 12)
		{
			goto IL_0197;
		}
		if (V_0 == 13)
		{
			goto IL_01e1;
		}
		if (V_0 == 14)
		{
			goto IL_01b9;
		}
		if (V_0 == 15)
		{
			goto IL_01d7;
		}
	}
	{
		goto IL_01e1;
	}

IL_00cc:
	{
		m9412(__this, ((int32_t)40), &m9412_MI);
		t7* L_7 = m6934(p1, &m6934_MI);
		m9414(__this, L_7, &m9414_MI);
		goto IL_01e1;
	}

IL_00e5:
	{
		t7* L_8 = m6939(p1, &m6939_MI);
		m9414(__this, L_8, &m9414_MI);
		t7* L_9 = m6934(p1, &m6934_MI);
		m9414(__this, L_9, &m9414_MI);
		goto IL_01e1;
	}

IL_0102:
	{
		t7* L_10 = m6934(p1, &m6934_MI);
		m9414(__this, L_10, &m9414_MI);
		t7* L_11 = m6939(p1, &m6939_MI);
		m9414(__this, L_11, &m9414_MI);
		goto IL_01e1;
	}

IL_011f:
	{
		t7* L_12 = m6934(p1, &m6934_MI);
		m9414(__this, L_12, &m9414_MI);
		goto IL_01e1;
	}

IL_0130:
	{
		m9412(__this, ((int32_t)40), &m9412_MI);
		goto IL_01e1;
	}

IL_013d:
	{
		t7* L_13 = m6939(p1, &m6939_MI);
		m9414(__this, L_13, &m9414_MI);
		goto IL_01e1;
	}

IL_014e:
	{
		t7* L_14 = m6939(p1, &m6939_MI);
		m9414(__this, L_14, &m9414_MI);
		goto IL_01e1;
	}

IL_015f:
	{
		t7* L_15 = m6939(p1, &m6939_MI);
		m9414(__this, L_15, &m9414_MI);
		t7* L_16 = m6934(p1, &m6934_MI);
		m9414(__this, L_16, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		goto IL_01e1;
	}

IL_0181:
	{
		t7* L_17 = m6934(p1, &m6934_MI);
		m9414(__this, L_17, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		goto IL_01e1;
	}

IL_0197:
	{
		t7* L_18 = m6934(p1, &m6934_MI);
		m9414(__this, L_18, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_19 = m6939(p1, &m6939_MI);
		m9414(__this, L_19, &m9414_MI);
		goto IL_01e1;
	}

IL_01b9:
	{
		m9412(__this, ((int32_t)40), &m9412_MI);
		t7* L_20 = m6934(p1, &m6934_MI);
		m9414(__this, L_20, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		goto IL_01e1;
	}

IL_01d7:
	{
		m9412(__this, ((int32_t)40), &m9412_MI);
		goto IL_01e1;
	}

IL_01e1:
	{
		t841* L_21 = m6931(p1, &m6931_MI);
		t7* L_22 = m6930(p1, &m6930_MI);
		m9472(__this, L_21, L_22, &m9472_MI);
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_020a;
		}
	}
	{
		t7* L_23 = m6929(p1, &m6929_MI);
		m9414(__this, L_23, &m9414_MI);
		m9471(__this, p0, &m9471_MI);
	}

IL_020a:
	{
		bool L_24 = (__this->f12);
		if (!L_24)
		{
			goto IL_0258;
		}
	}
	{
		int32_t L_25 = m6933(p1, &m6933_MI);
		V_0 = L_25;
		if (((int32_t)(V_0-1)) == 0)
		{
			goto IL_022f;
		}
		if (((int32_t)(V_0-1)) == 1)
		{
			goto IL_0253;
		}
		if (((int32_t)(V_0-1)) == 2)
		{
			goto IL_023d;
		}
	}
	{
		goto IL_0253;
	}

IL_022f:
	{
		t7* L_26 = m6934(p1, &m6934_MI);
		m9414(__this, L_26, &m9414_MI);
		goto IL_0253;
	}

IL_023d:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_27 = m6934(p1, &m6934_MI);
		m9414(__this, L_27, &m9414_MI);
		goto IL_0253;
	}

IL_0253:
	{
		goto IL_03bf;
	}

IL_0258:
	{
		int32_t L_28 = m6932(p1, &m6932_MI);
		V_0 = L_28;
		if (V_0 == 0)
		{
			goto IL_02aa;
		}
		if (V_0 == 1)
		{
			goto IL_03bf;
		}
		if (V_0 == 2)
		{
			goto IL_03bf;
		}
		if (V_0 == 3)
		{
			goto IL_02b7;
		}
		if (V_0 == 4)
		{
			goto IL_02c8;
		}
		if (V_0 == 5)
		{
			goto IL_02e1;
		}
		if (V_0 == 6)
		{
			goto IL_02f2;
		}
		if (V_0 == 7)
		{
			goto IL_030f;
		}
		if (V_0 == 8)
		{
			goto IL_032c;
		}
		if (V_0 == 9)
		{
			goto IL_03bf;
		}
		if (V_0 == 10)
		{
			goto IL_0345;
		}
		if (V_0 == 11)
		{
			goto IL_0367;
		}
		if (V_0 == 12)
		{
			goto IL_03bf;
		}
		if (V_0 == 13)
		{
			goto IL_0375;
		}
		if (V_0 == 14)
		{
			goto IL_0397;
		}
		if (V_0 == 15)
		{
			goto IL_03a1;
		}
	}
	{
		goto IL_03bf;
	}

IL_02aa:
	{
		m9412(__this, ((int32_t)41), &m9412_MI);
		goto IL_03bf;
	}

IL_02b7:
	{
		t7* L_29 = m6939(p1, &m6939_MI);
		m9414(__this, L_29, &m9414_MI);
		goto IL_03bf;
	}

IL_02c8:
	{
		t7* L_30 = m6934(p1, &m6934_MI);
		m9414(__this, L_30, &m9414_MI);
		m9412(__this, ((int32_t)41), &m9412_MI);
		goto IL_03bf;
	}

IL_02e1:
	{
		t7* L_31 = m6934(p1, &m6934_MI);
		m9414(__this, L_31, &m9414_MI);
		goto IL_03bf;
	}

IL_02f2:
	{
		t7* L_32 = m6939(p1, &m6939_MI);
		m9414(__this, L_32, &m9414_MI);
		t7* L_33 = m6934(p1, &m6934_MI);
		m9414(__this, L_33, &m9414_MI);
		goto IL_03bf;
	}

IL_030f:
	{
		t7* L_34 = m6934(p1, &m6934_MI);
		m9414(__this, L_34, &m9414_MI);
		t7* L_35 = m6939(p1, &m6939_MI);
		m9414(__this, L_35, &m9414_MI);
		goto IL_03bf;
	}

IL_032c:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_36 = m6934(p1, &m6934_MI);
		m9414(__this, L_36, &m9414_MI);
		goto IL_03bf;
	}

IL_0345:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_37 = m6934(p1, &m6934_MI);
		m9414(__this, L_37, &m9414_MI);
		t7* L_38 = m6939(p1, &m6939_MI);
		m9414(__this, L_38, &m9414_MI);
		goto IL_03bf;
	}

IL_0367:
	{
		t7* L_39 = m6939(p1, &m6939_MI);
		m9414(__this, L_39, &m9414_MI);
		goto IL_03bf;
	}

IL_0375:
	{
		t7* L_40 = m6939(p1, &m6939_MI);
		m9414(__this, L_40, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_41 = m6934(p1, &m6934_MI);
		m9414(__this, L_41, &m9414_MI);
		goto IL_03bf;
	}

IL_0397:
	{
		m9412(__this, ((int32_t)41), &m9412_MI);
		goto IL_03bf;
	}

IL_03a1:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_42 = m6934(p1, &m6934_MI);
		m9414(__this, L_42, &m9414_MI);
		m9412(__this, ((int32_t)41), &m9412_MI);
		goto IL_03bf;
	}

IL_03bf:
	{
		t200* L_43 = (__this->f23);
		int32_t L_44 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_45 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_45 = m5627(L_45, L_43, 0, L_44, &m5555_MI);
		return L_45;
	}
}
 t7* m9454 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	{
		int32_t L_0 = (__this->f16);
		if ((((int32_t)p0) >= ((int32_t)L_0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = (__this->f16);
		p0 = L_1;
	}

IL_0013:
	{
		if (p0)
		{
			goto IL_001c;
		}
	}
	{
		return (t7*) &_stringLiteral484;
	}

IL_001c:
	{
		m9410(__this, ((int32_t)(p0+1)), &m9410_MI);
		bool L_2 = (__this->f12);
		if (L_2)
		{
			goto IL_0039;
		}
	}
	{
		t7* L_3 = m6939(p1, &m6939_MI);
		m9414(__this, L_3, &m9414_MI);
	}

IL_0039:
	{
		m9476(__this, 0, p0, &m9476_MI);
		t200* L_4 = (__this->f23);
		int32_t L_5 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_6 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_6 = m5627(L_6, L_4, 0, L_5, &m5555_MI);
		return L_6;
	}
}
 t7* m9455 (t1664 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	uint16_t* V_1 = {0};
	uint64_t V_2 = 0;
	uint16_t* G_B3_0 = {0};
	{
		int32_t L_0 = (__this->f18);
		int32_t L_1 = m5139(NULL, p0, L_0, &m5139_MI);
		V_0 = L_1;
		bool L_2 = (__this->f11);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		G_B3_0 = (((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f3);
		goto IL_0021;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		G_B3_0 = (((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f2);
	}

IL_0021:
	{
		V_1 = (uint16_t*)G_B3_0;
		m9410(__this, V_0, &m9410_MI);
		__this->f24 = V_0;
		uint32_t L_3 = (__this->f19);
		uint32_t L_4 = (__this->f20);
		V_2 = ((int64_t)((int64_t)(((uint64_t)L_3))|(int64_t)((int64_t)((int64_t)(((uint64_t)L_4))<<(int32_t)((int32_t)32)))));
		goto IL_0061;
	}

IL_0045:
	{
		t200* L_5 = (__this->f23);
		int32_t L_6 = ((int32_t)(V_0-1));
		V_0 = L_6;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_5, L_6)) = (uint16_t)(*((uint16_t*)((uint16_t*)((uint8_t*)V_1+(((uintptr_t)((int64_t)((int64_t)((int64_t)((int64_t)V_2&(int64_t)(((int64_t)((int32_t)15)))))*(int64_t)(((int64_t)2))))))))));
		V_2 = ((int64_t)((uint64_t)V_2>>4));
	}

IL_0061:
	{
		if ((((int32_t)V_0) > ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		t200* L_7 = (__this->f23);
		int32_t L_8 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_9 = m5627(L_9, L_7, 0, L_8, &m5555_MI);
		return L_9;
	}
}
 t7* m9456 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	{
		if ((((uint32_t)p0) != ((uint32_t)(-1))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_0 = m6940(p1, &m6940_MI);
		p0 = L_0;
	}

IL_000e:
	{
		m9423(__this, p0, &m9423_MI);
		int32_t L_1 = m9417(__this, &m9417_MI);
		m9410(__this, ((int32_t)(((int32_t)(L_1+p0))+2)), &m9410_MI);
		bool L_2 = (__this->f12);
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		t7* L_3 = m6939(p1, &m6939_MI);
		m9414(__this, L_3, &m9414_MI);
	}

IL_003a:
	{
		int32_t L_4 = m9417(__this, &m9417_MI);
		m9469(__this, L_4, &m9469_MI);
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		t7* L_5 = m6941(p1, &m6941_MI);
		m9414(__this, L_5, &m9414_MI);
		m9471(__this, p0, &m9471_MI);
	}

IL_005d:
	{
		t200* L_6 = (__this->f23);
		int32_t L_7 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_8 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_8 = m5627(L_8, L_6, 0, L_7, &m5555_MI);
		return L_8;
	}
}
 t7* m9457 (t1664 * __this, double p0, t1125 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		t1664 * L_0 = m9480(__this, &m9480_MI);
		V_0 = L_0;
		if ((((double)p0) < ((double)(-1.79769313486231E+308))))
		{
			goto IL_0039;
		}
	}
	{
		if ((((double)p0) > ((double)(1.79769313486231E+308))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_1 = (__this->f15);
		t7* L_2 = m9459(__this, L_1, p1, &m9459_MI);
		V_1 = L_2;
		double L_3 = m5688(NULL, V_1, p1, &m5688_MI);
		if ((((double)p0) != ((double)L_3)))
		{
			goto IL_0039;
		}
	}
	{
		return V_1;
	}

IL_0039:
	{
		int32_t L_4 = (__this->f15);
		t7* L_5 = m9459(V_0, ((int32_t)(L_4+2)), p1, &m9459_MI);
		return L_5;
	}
}
 t7* m9458 (t1664 * __this, float p0, t1125 * p1, MethodInfo* method){
	t1664 * V_0 = {0};
	t7* V_1 = {0};
	{
		t1664 * L_0 = m9480(__this, &m9480_MI);
		V_0 = L_0;
		int32_t L_1 = (__this->f15);
		t7* L_2 = m9459(__this, L_1, p1, &m9459_MI);
		V_1 = L_2;
		float L_3 = m5660(NULL, V_1, p1, &m5660_MI);
		if ((((float)p0) != ((float)L_3)))
		{
			goto IL_0021;
		}
	}
	{
		return V_1;
	}

IL_0021:
	{
		int32_t L_4 = (__this->f15);
		t7* L_5 = m9459(V_0, ((int32_t)(L_4+2)), p1, &m9459_MI);
		return L_5;
	}
}
 t7* m9459 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		if ((((uint32_t)p0) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		bool L_0 = m9419(__this, &m9419_MI);
		V_0 = L_0;
		int32_t L_1 = (__this->f15);
		p0 = L_1;
		goto IL_002d;
	}

IL_0017:
	{
		V_0 = 1;
		if (p0)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_2 = (__this->f15);
		p0 = L_2;
	}

IL_0026:
	{
		m9422(__this, p0, &m9422_MI);
	}

IL_002d:
	{
		int32_t L_3 = (__this->f18);
		V_1 = L_3;
		int32_t L_4 = (__this->f16);
		V_2 = L_4;
		V_3 = ((int32_t)(V_2-V_1));
		if ((((int32_t)V_1) > ((int32_t)p0)))
		{
			goto IL_0048;
		}
	}
	{
		if ((((int32_t)V_1) > ((int32_t)((int32_t)-4))))
		{
			goto IL_0057;
		}
	}

IL_0048:
	{
		if (!V_0)
		{
			goto IL_0057;
		}
	}
	{
		t7* L_5 = m9463(__this, ((int32_t)(V_2-1)), p1, 2, &m9463_MI);
		return L_5;
	}

IL_0057:
	{
		if ((((int32_t)V_3) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		V_3 = 0;
	}

IL_005d:
	{
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_0063;
		}
	}
	{
		V_1 = 0;
	}

IL_0063:
	{
		m9410(__this, ((int32_t)(((int32_t)(V_3+V_1))+3)), &m9410_MI);
		bool L_6 = (__this->f12);
		if (L_6)
		{
			goto IL_0082;
		}
	}
	{
		t7* L_7 = m6939(p1, &m6939_MI);
		m9414(__this, L_7, &m9414_MI);
	}

IL_0082:
	{
		if (V_1)
		{
			goto IL_008f;
		}
	}
	{
		m9412(__this, ((int32_t)48), &m9412_MI);
		goto IL_0099;
	}

IL_008f:
	{
		m9476(__this, ((int32_t)(V_2-V_1)), V_2, &m9476_MI);
	}

IL_0099:
	{
		if ((((int32_t)V_3) <= ((int32_t)0)))
		{
			goto IL_00b1;
		}
	}
	{
		t7* L_8 = m6941(p1, &m6941_MI);
		m9414(__this, L_8, &m9414_MI);
		m9476(__this, 0, V_3, &m9476_MI);
	}

IL_00b1:
	{
		t200* L_9 = (__this->f23);
		int32_t L_10 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_11 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_11 = m5627(L_11, L_9, 0, L_10, &m5555_MI);
		return L_11;
	}
}
 t7* m9460 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = p0;
		goto IL_000d;
	}

IL_0007:
	{
		int32_t L_0 = m6940(p1, &m6940_MI);
		G_B3_0 = L_0;
	}

IL_000d:
	{
		p0 = G_B3_0;
		int32_t L_1 = m9417(__this, &m9417_MI);
		m9410(__this, ((int32_t)(((int32_t)((int32_t)L_1*(int32_t)3))+p0)), &m9410_MI);
		m9423(__this, p0, &m9423_MI);
		bool L_2 = (__this->f12);
		if (L_2)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_3 = m6944(p1, &m6944_MI);
		V_0 = L_3;
		if (V_0 == 0)
		{
			goto IL_004c;
		}
		if (V_0 == 1)
		{
			goto IL_0056;
		}
		if (V_0 == 2)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_007a;
	}

IL_004c:
	{
		m9412(__this, ((int32_t)40), &m9412_MI);
		goto IL_007a;
	}

IL_0056:
	{
		t7* L_4 = m6939(p1, &m6939_MI);
		m9414(__this, L_4, &m9414_MI);
		goto IL_007a;
	}

IL_0064:
	{
		t7* L_5 = m6939(p1, &m6939_MI);
		m9414(__this, L_5, &m9414_MI);
		m9412(__this, ((int32_t)32), &m9412_MI);
		goto IL_007a;
	}

IL_007a:
	{
		t841* L_6 = m6943(p1, &m6943_MI);
		t7* L_7 = m6942(p1, &m6942_MI);
		m9472(__this, L_6, L_7, &m9472_MI);
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_00a3;
		}
	}
	{
		t7* L_8 = m6941(p1, &m6941_MI);
		m9414(__this, L_8, &m9414_MI);
		m9471(__this, p0, &m9471_MI);
	}

IL_00a3:
	{
		bool L_9 = (__this->f12);
		if (L_9)
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_10 = m6944(p1, &m6944_MI);
		V_0 = L_10;
		if (V_0 == 0)
		{
			goto IL_00ce;
		}
		if (V_0 == 1)
		{
			goto IL_00fc;
		}
		if (V_0 == 2)
		{
			goto IL_00fc;
		}
		if (V_0 == 3)
		{
			goto IL_00d8;
		}
		if (V_0 == 4)
		{
			goto IL_00e6;
		}
	}
	{
		goto IL_00fc;
	}

IL_00ce:
	{
		m9412(__this, ((int32_t)41), &m9412_MI);
		goto IL_00fc;
	}

IL_00d8:
	{
		t7* L_11 = m6939(p1, &m6939_MI);
		m9414(__this, L_11, &m9414_MI);
		goto IL_00fc;
	}

IL_00e6:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_12 = m6939(p1, &m6939_MI);
		m9414(__this, L_12, &m9414_MI);
		goto IL_00fc;
	}

IL_00fc:
	{
		t200* L_13 = (__this->f23);
		int32_t L_14 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_15 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_15 = m5627(L_15, L_13, 0, L_14, &m5555_MI);
		return L_15;
	}
}
 t7* m9461 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = p0;
		goto IL_000d;
	}

IL_0007:
	{
		int32_t L_0 = m6946(p1, &m6946_MI);
		G_B3_0 = L_0;
	}

IL_000d:
	{
		p0 = G_B3_0;
		m9478(__this, 2, &m9478_MI);
		m9423(__this, p0, &m9423_MI);
		int32_t L_1 = m9417(__this, &m9417_MI);
		m9410(__this, ((int32_t)(((int32_t)(((int32_t)((int32_t)L_1*(int32_t)2))+p0))+((int32_t)16))), &m9410_MI);
		bool L_2 = (__this->f12);
		if (!L_2)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_3 = m6951(p1, &m6951_MI);
		if ((((uint32_t)L_3) != ((uint32_t)2)))
		{
			goto IL_0050;
		}
	}
	{
		t7* L_4 = m6952(p1, &m6952_MI);
		m9414(__this, L_4, &m9414_MI);
	}

IL_0050:
	{
		goto IL_00a3;
	}

IL_0052:
	{
		int32_t L_5 = m6950(p1, &m6950_MI);
		V_0 = L_5;
		if (V_0 == 0)
		{
			goto IL_006d;
		}
		if (V_0 == 1)
		{
			goto IL_007b;
		}
		if (V_0 == 2)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a3;
	}

IL_006d:
	{
		t7* L_6 = m6939(p1, &m6939_MI);
		m9414(__this, L_6, &m9414_MI);
		goto IL_00a3;
	}

IL_007b:
	{
		t7* L_7 = m6939(p1, &m6939_MI);
		m9414(__this, L_7, &m9414_MI);
		goto IL_00a3;
	}

IL_0089:
	{
		t7* L_8 = m6939(p1, &m6939_MI);
		m9414(__this, L_8, &m9414_MI);
		t7* L_9 = m6952(p1, &m6952_MI);
		m9414(__this, L_9, &m9414_MI);
		goto IL_00a3;
	}

IL_00a3:
	{
		t841* L_10 = m6949(p1, &m6949_MI);
		t7* L_11 = m6948(p1, &m6948_MI);
		m9472(__this, L_10, L_11, &m9472_MI);
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_00cc;
		}
	}
	{
		t7* L_12 = m6947(p1, &m6947_MI);
		m9414(__this, L_12, &m9414_MI);
		m9471(__this, p0, &m9471_MI);
	}

IL_00cc:
	{
		bool L_13 = (__this->f12);
		if (!L_13)
		{
			goto IL_010a;
		}
	}
	{
		int32_t L_14 = m6951(p1, &m6951_MI);
		V_0 = L_14;
		if (!V_0)
		{
			goto IL_00e4;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)1)))
		{
			goto IL_00fa;
		}
	}
	{
		goto IL_0108;
	}

IL_00e4:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_15 = m6952(p1, &m6952_MI);
		m9414(__this, L_15, &m9414_MI);
		goto IL_0108;
	}

IL_00fa:
	{
		t7* L_16 = m6952(p1, &m6952_MI);
		m9414(__this, L_16, &m9414_MI);
		goto IL_0108;
	}

IL_0108:
	{
		goto IL_013e;
	}

IL_010a:
	{
		int32_t L_17 = m6950(p1, &m6950_MI);
		V_0 = L_17;
		if (!V_0)
		{
			goto IL_011a;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)1)))
		{
			goto IL_0130;
		}
	}
	{
		goto IL_013e;
	}

IL_011a:
	{
		m9412(__this, ((int32_t)32), &m9412_MI);
		t7* L_18 = m6952(p1, &m6952_MI);
		m9414(__this, L_18, &m9414_MI);
		goto IL_013e;
	}

IL_0130:
	{
		t7* L_19 = m6952(p1, &m6952_MI);
		m9414(__this, L_19, &m9414_MI);
		goto IL_013e;
	}

IL_013e:
	{
		t200* L_20 = (__this->f23);
		int32_t L_21 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_22 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_22 = m5627(L_22, L_20, 0, L_21, &m5555_MI);
		return L_22;
	}
}
 t7* m9462 (t1664 * __this, int32_t p0, t1125 * p1, MethodInfo* method){
	{
		if ((((uint32_t)p0) != ((uint32_t)(-1))))
		{
			goto IL_0009;
		}
	}
	{
		p0 = 6;
	}

IL_0009:
	{
		m9422(__this, ((int32_t)(p0+1)), &m9422_MI);
		t7* L_0 = m9463(__this, p0, p1, 3, &m9463_MI);
		return L_0;
	}
}
 t7* m9463 (t1664 * __this, int32_t p0, t1125 * p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = (__this->f18);
		V_0 = L_0;
		int32_t L_1 = (__this->f16);
		V_1 = L_1;
		V_2 = ((int32_t)(V_0-1));
		int32_t L_2 = 1;
		V_3 = L_2;
		__this->f18 = L_2;
		V_0 = V_3;
		m9410(__this, ((int32_t)(p0+8)), &m9410_MI);
		bool L_3 = (__this->f12);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		t7* L_4 = m6939(p1, &m6939_MI);
		m9414(__this, L_4, &m9414_MI);
	}

IL_003a:
	{
		m9474(__this, ((int32_t)(V_1-1)), &m9474_MI);
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_0066;
		}
	}
	{
		t7* L_5 = m6941(p1, &m6941_MI);
		m9414(__this, L_5, &m9414_MI);
		int32_t L_6 = (__this->f18);
		m9476(__this, ((int32_t)(((int32_t)(V_1-p0))-1)), ((int32_t)(V_1-L_6)), &m9476_MI);
	}

IL_0066:
	{
		m9473(__this, p1, V_2, p2, &m9473_MI);
		t200* L_7 = (__this->f23);
		int32_t L_8 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_9 = m5627(L_9, L_7, 0, L_8, &m5555_MI);
		return L_9;
	}
}
 t7* m9464 (t1664 * __this, t7* p0, t1125 * p1, MethodInfo* method){
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t1663 * V_3 = {0};
	t292 * V_4 = {0};
	t292 * V_5 = {0};
	t292 * V_6 = {0};
	int32_t V_7 = 0;
	bool V_8 = false;
	t7* G_B4_0 = {0};
	t292 * G_B8_0 = {0};
	t292 * G_B21_0 = {0};
	t292 * G_B20_0 = {0};
	int32_t G_B22_0 = 0;
	t292 * G_B22_1 = {0};
	{
		bool L_0 = (__this->f12);
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		bool L_1 = m9420(__this, &m9420_MI);
		m9384(NULL, p0, (&V_0), L_1, (&V_1), (&V_2), &m9384_MI);
		if (V_2)
		{
			goto IL_0036;
		}
	}
	{
		bool L_2 = (__this->f12);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B4_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		goto IL_0035;
	}

IL_002f:
	{
		t7* L_3 = m6939(p1, &m6939_MI);
		G_B4_0 = L_3;
	}

IL_0035:
	{
		return G_B4_0;
	}

IL_0036:
	{
		__this->f12 = V_0;
		t1663 * L_4 = m9385(NULL, p0, V_1, V_2, p1, &m9385_MI);
		V_3 = L_4;
		int32_t L_5 = (V_3->f4);
		t292 * L_6 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_6, ((int32_t)((int32_t)L_5*(int32_t)2)), &m2923_MI);
		V_4 = L_6;
		int32_t L_7 = (V_3->f1);
		t292 * L_8 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_8, ((int32_t)((int32_t)L_7*(int32_t)2)), &m2923_MI);
		V_5 = L_8;
		bool L_9 = (V_3->f7);
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_10 = (V_3->f8);
		t292 * L_11 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_11, ((int32_t)((int32_t)L_10*(int32_t)2)), &m2923_MI);
		G_B8_0 = L_11;
		goto IL_007d;
	}

IL_007c:
	{
		G_B8_0 = ((t292 *)(NULL));
	}

IL_007d:
	{
		V_6 = G_B8_0;
		V_7 = 0;
		int32_t L_12 = (V_3->f12);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0099;
		}
	}
	{
		int32_t L_13 = (V_3->f12);
		m9478(__this, ((int32_t)((int32_t)2*(int32_t)L_13)), &m9478_MI);
	}

IL_0099:
	{
		int32_t L_14 = (V_3->f13);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00b0;
		}
	}
	{
		int32_t L_15 = (V_3->f13);
		m9478(__this, ((int32_t)((int32_t)3*(int32_t)L_15)), &m9478_MI);
	}

IL_00b0:
	{
		int32_t L_16 = (V_3->f11);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00c5;
		}
	}
	{
		int32_t L_17 = (V_3->f11);
		m9479(__this, L_17, &m9479_MI);
	}

IL_00c5:
	{
		V_8 = 1;
		bool L_18 = (V_3->f7);
		if (!L_18)
		{
			goto IL_013a;
		}
	}
	{
		int32_t L_19 = (V_3->f1);
		if ((((int32_t)L_19) > ((int32_t)0)))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_20 = (V_3->f4);
		if ((((int32_t)L_20) <= ((int32_t)0)))
		{
			goto IL_013a;
		}
	}

IL_00e2:
	{
		bool L_21 = m9420(__this, &m9420_MI);
		if (L_21)
		{
			goto IL_011b;
		}
	}
	{
		int32_t L_22 = (V_3->f1);
		int32_t L_23 = (V_3->f4);
		m9422(__this, ((int32_t)(L_22+L_23)), &m9422_MI);
		int32_t L_24 = (__this->f18);
		int32_t L_25 = (V_3->f4);
		V_7 = ((int32_t)(V_7-((int32_t)(L_24-L_25))));
		int32_t L_26 = (V_3->f4);
		__this->f18 = L_26;
	}

IL_011b:
	{
		V_8 = ((((int32_t)((((int32_t)V_7) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B20_0 = V_6;
		if ((((int32_t)V_7) >= ((int32_t)0)))
		{
			G_B21_0 = V_6;
			goto IL_0131;
		}
	}
	{
		G_B22_0 = ((-V_7));
		G_B22_1 = G_B20_0;
		goto IL_0133;
	}

IL_0131:
	{
		G_B22_0 = V_7;
		G_B22_1 = G_B21_0;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		m9467(NULL, G_B22_1, G_B22_0, &m9467_MI);
		goto IL_0147;
	}

IL_013a:
	{
		int32_t L_27 = (V_3->f1);
		m9423(__this, L_27, &m9423_MI);
	}

IL_0147:
	{
		int32_t L_28 = (V_3->f4);
		if (L_28)
		{
			goto IL_0157;
		}
	}
	{
		bool L_29 = m9421(__this, &m9421_MI);
		if (L_29)
		{
			goto IL_0165;
		}
	}

IL_0157:
	{
		int32_t L_30 = m9417(__this, &m9417_MI);
		m9468(__this, L_30, V_4, &m9468_MI);
	}

IL_0165:
	{
		int32_t L_31 = m9418(__this, &m9418_MI);
		m9470(__this, L_31, V_5, &m9470_MI);
		bool L_32 = (V_3->f7);
		if (!L_32)
		{
			goto IL_0217;
		}
	}
	{
		int32_t L_33 = (V_3->f1);
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_0197;
		}
	}
	{
		int32_t L_34 = (V_3->f4);
		if ((((int32_t)L_34) > ((int32_t)0)))
		{
			goto IL_0197;
		}
	}
	{
		__this->f12 = 1;
	}

IL_0197:
	{
		int32_t L_35 = m4184(V_4, &m4184_MI);
		int32_t L_36 = (V_3->f4);
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_37 = (V_3->f4);
		int32_t L_38 = m4184(V_4, &m4184_MI);
		m8674(V_4, 0, (t7*) &_stringLiteral484, ((int32_t)(L_37-L_38)), &m8674_MI);
	}

IL_01c2:
	{
		goto IL_01cf;
	}

IL_01c4:
	{
		m8673(V_6, 0, ((int32_t)48), &m8673_MI);
	}

IL_01cf:
	{
		int32_t L_39 = m4184(V_6, &m4184_MI);
		int32_t L_40 = (V_3->f8);
		int32_t L_41 = (V_3->f9);
		if ((((int32_t)L_39) < ((int32_t)((int32_t)(L_40-L_41)))))
		{
			goto IL_01c4;
		}
	}
	{
		if (!V_8)
		{
			goto IL_0202;
		}
	}
	{
		bool L_42 = (V_3->f10);
		if (L_42)
		{
			goto IL_0202;
		}
	}
	{
		t7* L_43 = m6955(p1, &m6955_MI);
		m8672(V_6, 0, L_43, &m8672_MI);
		goto IL_0215;
	}

IL_0202:
	{
		if (V_8)
		{
			goto IL_0215;
		}
	}
	{
		t7* L_44 = m6939(p1, &m6939_MI);
		m8672(V_6, 0, L_44, &m8672_MI);
	}

IL_0215:
	{
		goto IL_0277;
	}

IL_0217:
	{
		int32_t L_45 = m4184(V_4, &m4184_MI);
		int32_t L_46 = (V_3->f4);
		int32_t L_47 = (V_3->f5);
		if ((((int32_t)L_45) >= ((int32_t)((int32_t)(L_46-L_47)))))
		{
			goto IL_0250;
		}
	}
	{
		int32_t L_48 = (V_3->f4);
		int32_t L_49 = (V_3->f5);
		int32_t L_50 = m4184(V_4, &m4184_MI);
		m8674(V_4, 0, (t7*) &_stringLiteral484, ((int32_t)(((int32_t)(L_48-L_49))-L_50)), &m8674_MI);
	}

IL_0250:
	{
		int32_t L_51 = (V_3->f4);
		int32_t L_52 = (V_3->f5);
		if ((((uint32_t)L_51) != ((uint32_t)L_52)))
		{
			goto IL_0277;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		bool L_53 = m9466(NULL, V_4, &m9466_MI);
		if (!L_53)
		{
			goto IL_0277;
		}
	}
	{
		int32_t L_54 = m4184(V_4, &m4184_MI);
		m8665(V_4, 0, L_54, &m8665_MI);
	}

IL_0277:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		m9465(NULL, V_5, 1, &m9465_MI);
		goto IL_028b;
	}

IL_0281:
	{
		m1761(V_5, ((int32_t)48), &m1761_MI);
	}

IL_028b:
	{
		int32_t L_55 = m4184(V_5, &m4184_MI);
		int32_t L_56 = (V_3->f1);
		int32_t L_57 = (V_3->f3);
		if ((((int32_t)L_55) < ((int32_t)((int32_t)(L_56-L_57)))))
		{
			goto IL_0281;
		}
	}
	{
		int32_t L_58 = m4184(V_5, &m4184_MI);
		int32_t L_59 = (V_3->f1);
		if ((((int32_t)L_58) <= ((int32_t)L_59)))
		{
			goto IL_02cc;
		}
	}
	{
		int32_t L_60 = (V_3->f1);
		int32_t L_61 = m4184(V_5, &m4184_MI);
		int32_t L_62 = (V_3->f1);
		m8665(V_5, L_60, ((int32_t)(L_61-L_62)), &m8665_MI);
	}

IL_02cc:
	{
		bool L_63 = (__this->f12);
		t7* L_64 = m9386(V_3, p0, V_1, V_2, p1, L_63, V_4, V_5, V_6, &m9386_MI);
		return L_64;
	}
}
 void m9465 (t29 * __this, t292 * p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B7_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = m4184(p0, &m4184_MI);
		V_1 = ((int32_t)(L_0-1));
		goto IL_0022;
	}

IL_000d:
	{
		uint16_t L_1 = m8662(p0, V_1, &m8662_MI);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)48))))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_0034;
	}

IL_001a:
	{
		V_0 = ((int32_t)(V_0+1));
		V_1 = ((int32_t)(V_1-1));
	}

IL_0022:
	{
		if (!p1)
		{
			goto IL_002e;
		}
	}
	{
		G_B7_0 = ((((int32_t)((((int32_t)V_1) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0032;
	}

IL_002e:
	{
		G_B7_0 = ((((int32_t)V_1) > ((int32_t)0))? 1 : 0);
	}

IL_0032:
	{
		if (G_B7_0)
		{
			goto IL_000d;
		}
	}

IL_0034:
	{
		if ((((int32_t)V_0) <= ((int32_t)0)))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_2 = m4184(p0, &m4184_MI);
		m8665(p0, ((int32_t)(L_2-V_0)), V_0, &m8665_MI);
	}

IL_0048:
	{
		return;
	}
}
 bool m9466 (t29 * __this, t292 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0023;
	}

IL_0004:
	{
		uint16_t L_0 = m8662(p0, V_0, &m8662_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_1 = m4222(NULL, L_0, &m4222_MI);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		uint16_t L_2 = m8662(p0, V_0, &m8662_MI);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)48))))
		{
			goto IL_001f;
		}
	}
	{
		return 0;
	}

IL_001f:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0023:
	{
		int32_t L_3 = m4184(p0, &m4184_MI);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_0004;
		}
	}
	{
		return 1;
	}
}
 void m9467 (t29 * __this, t292 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000a;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_0, &m8852_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int32_t L_1 = m9399(NULL, (((int64_t)p1)), &m9399_MI);
		V_0 = ((int32_t)(L_1-1));
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		int64_t L_2 = m9390(NULL, V_0, &m9390_MI);
		V_1 = ((int32_t)((int32_t)p1/(int32_t)(((int32_t)L_2))));
		m1761(p0, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)V_1)))), &m1761_MI);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)(L_3-1));
		int64_t L_4 = m9390(NULL, L_3, &m9390_MI);
		p1 = ((int32_t)(p1-((int32_t)((int32_t)(((int32_t)L_4))*(int32_t)V_1))));
		if ((((int32_t)V_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
 void m9468 (t1664 * __this, int32_t p0, t292 * p1, MethodInfo* method){
	{
		int32_t L_0 = (__this->f18);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		m8668(p1, ((int32_t)48), p0, &m8668_MI);
		return;
	}

IL_0014:
	{
		int32_t L_1 = (__this->f18);
		if ((((int32_t)L_1) >= ((int32_t)p0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = (__this->f18);
		m8668(p1, ((int32_t)48), ((int32_t)(p0-L_2)), &m8668_MI);
	}

IL_002e:
	{
		int32_t L_3 = (__this->f16);
		int32_t L_4 = (__this->f18);
		int32_t L_5 = (__this->f16);
		m9477(__this, ((int32_t)(L_3-L_4)), L_5, p1, &m9477_MI);
		return;
	}
}
 void m9469 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f18);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		m9413(__this, ((int32_t)48), p0, &m9413_MI);
		return;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f18);
		if ((((int32_t)L_1) >= ((int32_t)p0)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_2 = (__this->f18);
		m9413(__this, ((int32_t)48), ((int32_t)(p0-L_2)), &m9413_MI);
	}

IL_002c:
	{
		int32_t L_3 = (__this->f16);
		int32_t L_4 = (__this->f18);
		int32_t L_5 = (__this->f16);
		m9476(__this, ((int32_t)(L_3-L_4)), L_5, &m9476_MI);
		return;
	}
}
 void m9470 (t1664 * __this, int32_t p0, t292 * p1, MethodInfo* method){
	{
		int32_t L_0 = (__this->f16);
		int32_t L_1 = (__this->f18);
		int32_t L_2 = (__this->f16);
		int32_t L_3 = (__this->f18);
		m9477(__this, ((int32_t)(((int32_t)(L_0-p0))-L_1)), ((int32_t)(L_2-L_3)), p1, &m9477_MI);
		return;
	}
}
 void m9471 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f16);
		int32_t L_1 = (__this->f18);
		int32_t L_2 = (__this->f16);
		int32_t L_3 = (__this->f18);
		m9476(__this, ((int32_t)(((int32_t)(L_0-p0))-L_1)), ((int32_t)(L_2-L_3)), &m9476_MI);
		return;
	}
}
 void m9472 (t1664 * __this, t841* p0, t7* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B21_0 = 0;
	{
		bool L_0 = m9421(__this, &m9421_MI);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		m9412(__this, ((int32_t)48), &m9412_MI);
		return;
	}

IL_0011:
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		goto IL_0032;
	}

IL_0019:
	{
		int32_t L_1 = V_2;
		V_0 = ((int32_t)(V_0+(*(int32_t*)(int32_t*)SZArrayLdElema(p0, L_1))));
		int32_t L_2 = (__this->f18);
		if ((((int32_t)V_0) > ((int32_t)L_2)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = V_2;
		goto IL_002e;
	}

IL_002c:
	{
		goto IL_0038;
	}

IL_002e:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0032:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)p0)->max_length))))))
		{
			goto IL_0019;
		}
	}

IL_0038:
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_011a;
		}
	}
	{
		if ((((int32_t)V_0) <= ((int32_t)0)))
		{
			goto IL_011a;
		}
	}
	{
		int32_t L_3 = V_1;
		V_4 = (*(int32_t*)(int32_t*)SZArrayLdElema(p0, L_3));
		int32_t L_4 = (__this->f18);
		if ((((int32_t)L_4) <= ((int32_t)V_0)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_5 = (__this->f18);
		G_B13_0 = ((int32_t)(L_5-V_0));
		goto IL_0061;
	}

IL_0060:
	{
		G_B13_0 = 0;
	}

IL_0061:
	{
		V_5 = G_B13_0;
		if (V_4)
		{
			goto IL_0084;
		}
	}
	{
		goto IL_006d;
	}

IL_0069:
	{
		V_1 = ((int32_t)(V_1-1));
	}

IL_006d:
	{
		if ((((int32_t)V_1) < ((int32_t)0)))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_6 = V_1;
		if (!(*(int32_t*)(int32_t*)SZArrayLdElema(p0, L_6)))
		{
			goto IL_0069;
		}
	}

IL_0076:
	{
		if ((((int32_t)V_5) <= ((int32_t)0)))
		{
			goto IL_007f;
		}
	}
	{
		G_B21_0 = V_5;
		goto IL_0082;
	}

IL_007f:
	{
		int32_t L_7 = V_1;
		G_B21_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(p0, L_7));
	}

IL_0082:
	{
		V_4 = G_B21_0;
	}

IL_0084:
	{
		if (V_5)
		{
			goto IL_008d;
		}
	}
	{
		V_3 = V_4;
		goto IL_00a7;
	}

IL_008d:
	{
		V_1 = ((int32_t)(V_1+((int32_t)((int32_t)V_5/(int32_t)V_4))));
		V_3 = ((int32_t)(V_5%V_4));
		if (V_3)
		{
			goto IL_00a3;
		}
	}
	{
		V_3 = V_4;
		goto IL_00a7;
	}

IL_00a3:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_00a7:
	{
		V_6 = 0;
		goto IL_0116;
	}

IL_00ac:
	{
		int32_t L_8 = (__this->f18);
		if ((((int32_t)((int32_t)(L_8-V_6))) <= ((int32_t)V_3)))
		{
			goto IL_00bb;
		}
	}
	{
		if (V_3)
		{
			goto IL_00d9;
		}
	}

IL_00bb:
	{
		int32_t L_9 = (__this->f16);
		int32_t L_10 = (__this->f18);
		int32_t L_11 = (__this->f16);
		m9476(__this, ((int32_t)(L_9-L_10)), ((int32_t)(L_11-V_6)), &m9476_MI);
		goto IL_0118;
	}

IL_00d9:
	{
		int32_t L_12 = (__this->f16);
		int32_t L_13 = (__this->f16);
		m9476(__this, ((int32_t)(((int32_t)(L_12-V_6))-V_3)), ((int32_t)(L_13-V_6)), &m9476_MI);
		V_6 = ((int32_t)(V_6+V_3));
		m9414(__this, p1, &m9414_MI);
		int32_t L_14 = ((int32_t)(V_1-1));
		V_1 = L_14;
		if ((((int32_t)L_14) >= ((int32_t)(((int32_t)(((t20 *)p0)->max_length))))))
		{
			goto IL_0113;
		}
	}
	{
		if ((((int32_t)V_1) < ((int32_t)0)))
		{
			goto IL_0113;
		}
	}
	{
		int32_t L_15 = V_1;
		V_4 = (*(int32_t*)(int32_t*)SZArrayLdElema(p0, L_15));
	}

IL_0113:
	{
		V_3 = V_4;
	}

IL_0116:
	{
		goto IL_00ac;
	}

IL_0118:
	{
		goto IL_0133;
	}

IL_011a:
	{
		int32_t L_16 = (__this->f16);
		int32_t L_17 = (__this->f18);
		int32_t L_18 = (__this->f16);
		m9476(__this, ((int32_t)(L_16-L_17)), L_18, &m9476_MI);
	}

IL_0133:
	{
		return;
	}
}
 void m9473 (t1664 * __this, t1125 * p0, int32_t p1, int32_t p2, MethodInfo* method){
	uint32_t V_0 = 0;
	{
		bool L_0 = (__this->f11);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		uint16_t L_1 = (__this->f13);
		if ((((uint32_t)L_1) != ((uint32_t)((int32_t)82))))
		{
			goto IL_001c;
		}
	}

IL_0012:
	{
		m9412(__this, ((int32_t)69), &m9412_MI);
		goto IL_0024;
	}

IL_001c:
	{
		m9412(__this, ((int32_t)101), &m9412_MI);
	}

IL_0024:
	{
		if ((((int32_t)p1) < ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		t7* L_2 = m6955(p0, &m6955_MI);
		m9414(__this, L_2, &m9414_MI);
		goto IL_0048;
	}

IL_0036:
	{
		t7* L_3 = m6939(p0, &m6939_MI);
		m9414(__this, L_3, &m9414_MI);
		p1 = ((-p1));
	}

IL_0048:
	{
		if (p1)
		{
			goto IL_0056;
		}
	}
	{
		m9413(__this, ((int32_t)48), p2, &m9413_MI);
		goto IL_00ae;
	}

IL_0056:
	{
		if ((((int32_t)p1) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0073;
		}
	}
	{
		m9413(__this, ((int32_t)48), ((int32_t)(p2-1)), &m9413_MI);
		m9412(__this, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)p1)))), &m9412_MI);
		goto IL_00ae;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		uint32_t L_4 = m9394(NULL, p1, &m9394_MI);
		V_0 = L_4;
		if ((((int32_t)p1) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0083;
		}
	}
	{
		if ((((uint32_t)p2) != ((uint32_t)3)))
		{
			goto IL_0090;
		}
	}

IL_0083:
	{
		m9412(__this, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((uint32_t)V_0>>8)))))), &m9412_MI);
	}

IL_0090:
	{
		m9412(__this, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)V_0>>4))&(int32_t)((int32_t)15))))))), &m9412_MI);
		m9412(__this, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)15))))))), &m9412_MI);
	}

IL_00ae:
	{
		return;
	}
}
 void m9474 (t1664 * __this, int32_t p0, MethodInfo* method){
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->f24);
		t200* L_1 = (__this->f23);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = (__this->f24);
		m9411(__this, ((int32_t)(L_2+((int32_t)10))), &m9411_MI);
	}

IL_001f:
	{
		int32_t L_3 = (__this->f17);
		p0 = ((int32_t)(p0+L_3));
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0033;
		}
	}
	{
		V_0 = 0;
		goto IL_006c;
	}

IL_0033:
	{
		if ((((int32_t)p0) >= ((int32_t)8)))
		{
			goto IL_0040;
		}
	}
	{
		uint32_t L_4 = (__this->f19);
		V_0 = L_4;
		goto IL_006c;
	}

IL_0040:
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)16))))
		{
			goto IL_004e;
		}
	}
	{
		uint32_t L_5 = (__this->f20);
		V_0 = L_5;
		goto IL_006c;
	}

IL_004e:
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)24))))
		{
			goto IL_005c;
		}
	}
	{
		uint32_t L_6 = (__this->f21);
		V_0 = L_6;
		goto IL_006c;
	}

IL_005c:
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)32))))
		{
			goto IL_006a;
		}
	}
	{
		uint32_t L_7 = (__this->f22);
		V_0 = L_7;
		goto IL_006c;
	}

IL_006a:
	{
		V_0 = 0;
	}

IL_006c:
	{
		V_0 = ((int32_t)((uint32_t)V_0>>((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)p0&(int32_t)7))<<(int32_t)2))&(int32_t)((int32_t)31)))));
		t200* L_8 = (__this->f23);
		int32_t L_9 = (__this->f24);
		int32_t L_10 = L_9;
		V_1 = L_10;
		__this->f24 = ((int32_t)(L_10+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_8, V_1)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)15)))))));
		return;
	}
}
 void m9475 (t1664 * __this, int32_t p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = (__this->f24);
		V_0 = L_0;
		if (p1)
		{
			goto IL_000f;
		}
	}
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)100))))
		{
			goto IL_0069;
		}
	}

IL_000f:
	{
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)p0*(int32_t)((int32_t)5243)))>>(int32_t)((int32_t)19)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		V_1 = (*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5)+((int32_t)((int32_t)V_2*(int32_t)4))))));
		if (p1)
		{
			goto IL_0030;
		}
	}
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)1000))))
		{
			goto IL_0043;
		}
	}

IL_0030:
	{
		t200* L_1 = (__this->f23);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)(L_2+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_1, L_2)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_1>>(int32_t)4))))));
	}

IL_0043:
	{
		t200* L_3 = (__this->f23);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)(L_4+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, L_4)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)15)))))));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		V_1 = (*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5)+((int32_t)((int32_t)((int32_t)(p0-((int32_t)((int32_t)V_2*(int32_t)((int32_t)100)))))*(int32_t)4))))));
		goto IL_0074;
	}

IL_0069:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1664_TI));
		V_1 = (*((int32_t*)((int32_t*)((uint8_t*)(((t1664_SFs*)InitializedTypeInfo(&t1664_TI)->static_fields)->f5)+((int32_t)((int32_t)p0*(int32_t)4))))));
	}

IL_0074:
	{
		if (p1)
		{
			goto IL_007c;
		}
	}
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)10))))
		{
			goto IL_008f;
		}
	}

IL_007c:
	{
		t200* L_5 = (__this->f23);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)(L_6+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_5, L_6)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_1>>(int32_t)4))))));
	}

IL_008f:
	{
		t200* L_7 = (__this->f23);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)(L_8+1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, L_8)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)15)))))));
		__this->f24 = V_0;
		return;
	}
}
 void m9476 (t1664 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		if ((((int32_t)p0) < ((int32_t)p1)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}

IL_0005:
	{
		int32_t L_0 = (__this->f24);
		V_0 = ((int32_t)(L_0+((int32_t)(p1-p0))));
		t200* L_1 = (__this->f23);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0025;
		}
	}
	{
		m9411(__this, ((int32_t)(V_0+((int32_t)10))), &m9411_MI);
	}

IL_0025:
	{
		__this->f24 = V_0;
		int32_t L_2 = (__this->f17);
		p1 = ((int32_t)(p1+L_2));
		int32_t L_3 = (__this->f17);
		p0 = ((int32_t)(p0+L_3));
		V_1 = ((int32_t)(((int32_t)(p0+8))-((int32_t)((int32_t)p0&(int32_t)7))));
		goto IL_01a6;
	}

IL_0051:
	{
		if ((((uint32_t)V_1) != ((uint32_t)8)))
		{
			goto IL_005e;
		}
	}
	{
		uint32_t L_4 = (__this->f19);
		V_2 = L_4;
		goto IL_008a;
	}

IL_005e:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)16))))
		{
			goto IL_006c;
		}
	}
	{
		uint32_t L_5 = (__this->f20);
		V_2 = L_5;
		goto IL_008a;
	}

IL_006c:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)24))))
		{
			goto IL_007a;
		}
	}
	{
		uint32_t L_6 = (__this->f21);
		V_2 = L_6;
		goto IL_008a;
	}

IL_007a:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)32))))
		{
			goto IL_0088;
		}
	}
	{
		uint32_t L_7 = (__this->f22);
		V_2 = L_7;
		goto IL_008a;
	}

IL_0088:
	{
		V_2 = 0;
	}

IL_008a:
	{
		V_2 = ((int32_t)((uint32_t)V_2>>((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)p0&(int32_t)7))<<(int32_t)2))&(int32_t)((int32_t)31)))));
		if ((((int32_t)V_1) <= ((int32_t)p1)))
		{
			goto IL_009b;
		}
	}
	{
		V_1 = p1;
	}

IL_009b:
	{
		t200* L_8 = (__this->f23);
		int32_t L_9 = ((int32_t)(V_0-1));
		V_0 = L_9;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_8, L_9)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_2&(int32_t)((int32_t)15)))))));
		V_3 = ((int32_t)(V_1-p0));
		if (((int32_t)(V_3-1)) == 0)
		{
			goto IL_0196;
		}
		if (((int32_t)(V_3-1)) == 1)
		{
			goto IL_017c;
		}
		if (((int32_t)(V_3-1)) == 2)
		{
			goto IL_0162;
		}
		if (((int32_t)(V_3-1)) == 3)
		{
			goto IL_0148;
		}
		if (((int32_t)(V_3-1)) == 4)
		{
			goto IL_012e;
		}
		if (((int32_t)(V_3-1)) == 5)
		{
			goto IL_0114;
		}
		if (((int32_t)(V_3-1)) == 6)
		{
			goto IL_00fa;
		}
		if (((int32_t)(V_3-1)) == 7)
		{
			goto IL_00e0;
		}
	}
	{
		goto IL_019d;
	}

IL_00e0:
	{
		t200* L_10 = (__this->f23);
		int32_t L_11 = ((int32_t)(V_0-1));
		V_0 = L_11;
		int32_t L_12 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_12;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_10, L_11)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)15)))))));
		goto IL_00fa;
	}

IL_00fa:
	{
		t200* L_13 = (__this->f23);
		int32_t L_14 = ((int32_t)(V_0-1));
		V_0 = L_14;
		int32_t L_15 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_15;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_13, L_14)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_15&(int32_t)((int32_t)15)))))));
		goto IL_0114;
	}

IL_0114:
	{
		t200* L_16 = (__this->f23);
		int32_t L_17 = ((int32_t)(V_0-1));
		V_0 = L_17;
		int32_t L_18 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_18;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_16, L_17)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)15)))))));
		goto IL_012e;
	}

IL_012e:
	{
		t200* L_19 = (__this->f23);
		int32_t L_20 = ((int32_t)(V_0-1));
		V_0 = L_20;
		int32_t L_21 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_21;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_19, L_20)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_21&(int32_t)((int32_t)15)))))));
		goto IL_0148;
	}

IL_0148:
	{
		t200* L_22 = (__this->f23);
		int32_t L_23 = ((int32_t)(V_0-1));
		V_0 = L_23;
		int32_t L_24 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_24;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_22, L_23)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_24&(int32_t)((int32_t)15)))))));
		goto IL_0162;
	}

IL_0162:
	{
		t200* L_25 = (__this->f23);
		int32_t L_26 = ((int32_t)(V_0-1));
		V_0 = L_26;
		int32_t L_27 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_27;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_25, L_26)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_27&(int32_t)((int32_t)15)))))));
		goto IL_017c;
	}

IL_017c:
	{
		t200* L_28 = (__this->f23);
		int32_t L_29 = ((int32_t)(V_0-1));
		V_0 = L_29;
		int32_t L_30 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_30;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_28, L_29)) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_30&(int32_t)((int32_t)15)))))));
		goto IL_0196;
	}

IL_0196:
	{
		if ((((uint32_t)V_1) != ((uint32_t)p1)))
		{
			goto IL_019b;
		}
	}
	{
		return;
	}

IL_019b:
	{
		goto IL_019d;
	}

IL_019d:
	{
		p0 = V_1;
		V_1 = ((int32_t)(V_1+8));
	}

IL_01a6:
	{
		goto IL_0051;
	}
}
 void m9477 (t1664 * __this, int32_t p0, int32_t p1, t292 * p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		if ((((int32_t)p0) < ((int32_t)p1)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}

IL_0005:
	{
		int32_t L_0 = m4184(p2, &m4184_MI);
		V_0 = ((int32_t)(L_0+((int32_t)(p1-p0))));
		m4285(p2, V_0, &m4285_MI);
		int32_t L_1 = (__this->f17);
		p1 = ((int32_t)(p1+L_1));
		int32_t L_2 = (__this->f17);
		p0 = ((int32_t)(p0+L_2));
		V_1 = ((int32_t)(((int32_t)(p0+8))-((int32_t)((int32_t)p0&(int32_t)7))));
		goto IL_0189;
	}

IL_003c:
	{
		if ((((uint32_t)V_1) != ((uint32_t)8)))
		{
			goto IL_0049;
		}
	}
	{
		uint32_t L_3 = (__this->f19);
		V_2 = L_3;
		goto IL_0075;
	}

IL_0049:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)16))))
		{
			goto IL_0057;
		}
	}
	{
		uint32_t L_4 = (__this->f20);
		V_2 = L_4;
		goto IL_0075;
	}

IL_0057:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)24))))
		{
			goto IL_0065;
		}
	}
	{
		uint32_t L_5 = (__this->f21);
		V_2 = L_5;
		goto IL_0075;
	}

IL_0065:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)32))))
		{
			goto IL_0073;
		}
	}
	{
		uint32_t L_6 = (__this->f22);
		V_2 = L_6;
		goto IL_0075;
	}

IL_0073:
	{
		V_2 = 0;
	}

IL_0075:
	{
		V_2 = ((int32_t)((uint32_t)V_2>>((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)p0&(int32_t)7))<<(int32_t)2))&(int32_t)((int32_t)31)))));
		if ((((int32_t)V_1) <= ((int32_t)p1)))
		{
			goto IL_0086;
		}
	}
	{
		V_1 = p1;
	}

IL_0086:
	{
		int32_t L_7 = ((int32_t)(V_0-1));
		V_0 = L_7;
		m8663(p2, L_7, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)V_2&(int32_t)((int32_t)15))))))), &m8663_MI);
		V_3 = ((int32_t)(V_1-p0));
		if (((int32_t)(V_3-1)) == 0)
		{
			goto IL_0179;
		}
		if (((int32_t)(V_3-1)) == 1)
		{
			goto IL_0160;
		}
		if (((int32_t)(V_3-1)) == 2)
		{
			goto IL_0147;
		}
		if (((int32_t)(V_3-1)) == 3)
		{
			goto IL_012e;
		}
		if (((int32_t)(V_3-1)) == 4)
		{
			goto IL_0115;
		}
		if (((int32_t)(V_3-1)) == 5)
		{
			goto IL_00fc;
		}
		if (((int32_t)(V_3-1)) == 6)
		{
			goto IL_00e3;
		}
		if (((int32_t)(V_3-1)) == 7)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_0180;
	}

IL_00ca:
	{
		int32_t L_8 = ((int32_t)(V_0-1));
		V_0 = L_8;
		int32_t L_9 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_9;
		m8663(p2, L_8, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_9&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_00e3;
	}

IL_00e3:
	{
		int32_t L_10 = ((int32_t)(V_0-1));
		V_0 = L_10;
		int32_t L_11 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_11;
		m8663(p2, L_10, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_00fc;
	}

IL_00fc:
	{
		int32_t L_12 = ((int32_t)(V_0-1));
		V_0 = L_12;
		int32_t L_13 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_13;
		m8663(p2, L_12, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_13&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_0115;
	}

IL_0115:
	{
		int32_t L_14 = ((int32_t)(V_0-1));
		V_0 = L_14;
		int32_t L_15 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_15;
		m8663(p2, L_14, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_15&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_012e;
	}

IL_012e:
	{
		int32_t L_16 = ((int32_t)(V_0-1));
		V_0 = L_16;
		int32_t L_17 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_17;
		m8663(p2, L_16, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_17&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_0147;
	}

IL_0147:
	{
		int32_t L_18 = ((int32_t)(V_0-1));
		V_0 = L_18;
		int32_t L_19 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_19;
		m8663(p2, L_18, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_0160;
	}

IL_0160:
	{
		int32_t L_20 = ((int32_t)(V_0-1));
		V_0 = L_20;
		int32_t L_21 = ((int32_t)((uint32_t)V_2>>4));
		V_2 = L_21;
		m8663(p2, L_20, (((uint16_t)((int32_t)((int32_t)((int32_t)48)|(int32_t)((int32_t)((int32_t)L_21&(int32_t)((int32_t)15))))))), &m8663_MI);
		goto IL_0179;
	}

IL_0179:
	{
		if ((((uint32_t)V_1) != ((uint32_t)p1)))
		{
			goto IL_017e;
		}
	}
	{
		return;
	}

IL_017e:
	{
		goto IL_0180;
	}

IL_0180:
	{
		p0 = V_1;
		V_1 = ((int32_t)(V_1+8));
	}

IL_0189:
	{
		goto IL_003c;
	}
}
 void m9478 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_0 = (__this->f16);
		if (L_0)
		{
			goto IL_000d;
		}
	}

IL_000c:
	{
		return;
	}

IL_000d:
	{
		int32_t L_1 = (__this->f18);
		__this->f18 = ((int32_t)(L_1+p0));
		return;
	}
}
 void m9479 (t1664 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_0 = (__this->f16);
		if (L_0)
		{
			goto IL_000d;
		}
	}

IL_000c:
	{
		return;
	}

IL_000d:
	{
		int32_t L_1 = (__this->f18);
		__this->f18 = ((int32_t)(L_1-p0));
		return;
	}
}
 t1664 * m9480 (t1664 * __this, MethodInfo* method){
	{
		t29 * L_0 = m5283(__this, &m5283_MI);
		return ((t1664 *)Castclass(L_0, InitializedTypeInfo(&t1664_TI)));
	}
}
// Metadata Definition System.NumberFormatter
extern Il2CppType t2089_0_0_49;
FieldInfo t1664_f0_FieldInfo = 
{
	"MantissaBitsTable", &t2089_0_0_49, &t1664_TI, offsetof(t1664_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t2003_0_0_49;
FieldInfo t1664_f1_FieldInfo = 
{
	"TensExponentTable", &t2003_0_0_49, &t1664_TI, offsetof(t1664_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1743_0_0_49;
FieldInfo t1664_f2_FieldInfo = 
{
	"DigitLowerTable", &t1743_0_0_49, &t1664_TI, offsetof(t1664_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1743_0_0_49;
FieldInfo t1664_f3_FieldInfo = 
{
	"DigitUpperTable", &t1743_0_0_49, &t1664_TI, offsetof(t1664_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2090_0_0_49;
FieldInfo t1664_f4_FieldInfo = 
{
	"TenPowersList", &t2090_0_0_49, &t1664_TI, offsetof(t1664_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t2003_0_0_49;
FieldInfo t1664_f5_FieldInfo = 
{
	"DecHexDigits", &t2003_0_0_49, &t1664_TI, offsetof(t1664_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1436_0_0_1;
FieldInfo t1664_f6_FieldInfo = 
{
	"_thread", &t1436_0_0_1, &t1664_TI, offsetof(t1664, f6), &EmptyCustomAttributesCache};
extern Il2CppType t1125_0_0_1;
FieldInfo t1664_f7_FieldInfo = 
{
	"_nfi", &t1125_0_0_1, &t1664_TI, offsetof(t1664, f7), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1664_f8_FieldInfo = 
{
	"_NaN", &t40_0_0_1, &t1664_TI, offsetof(t1664, f8), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1664_f9_FieldInfo = 
{
	"_infinity", &t40_0_0_1, &t1664_TI, offsetof(t1664, f9), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1664_f10_FieldInfo = 
{
	"_isCustomFormat", &t40_0_0_1, &t1664_TI, offsetof(t1664, f10), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1664_f11_FieldInfo = 
{
	"_specifierIsUpper", &t40_0_0_1, &t1664_TI, offsetof(t1664, f11), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1664_f12_FieldInfo = 
{
	"_positive", &t40_0_0_1, &t1664_TI, offsetof(t1664, f12), &EmptyCustomAttributesCache};
extern Il2CppType t194_0_0_1;
FieldInfo t1664_f13_FieldInfo = 
{
	"_specifier", &t194_0_0_1, &t1664_TI, offsetof(t1664, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1664_f14_FieldInfo = 
{
	"_precision", &t44_0_0_1, &t1664_TI, offsetof(t1664, f14), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1664_f15_FieldInfo = 
{
	"_defPrecision", &t44_0_0_1, &t1664_TI, offsetof(t1664, f15), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1664_f16_FieldInfo = 
{
	"_digitsLen", &t44_0_0_1, &t1664_TI, offsetof(t1664, f16), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1664_f17_FieldInfo = 
{
	"_offset", &t44_0_0_1, &t1664_TI, offsetof(t1664, f17), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1664_f18_FieldInfo = 
{
	"_decPointPos", &t44_0_0_1, &t1664_TI, offsetof(t1664, f18), &EmptyCustomAttributesCache};
extern Il2CppType t344_0_0_1;
FieldInfo t1664_f19_FieldInfo = 
{
	"_val1", &t344_0_0_1, &t1664_TI, offsetof(t1664, f19), &EmptyCustomAttributesCache};
extern Il2CppType t344_0_0_1;
FieldInfo t1664_f20_FieldInfo = 
{
	"_val2", &t344_0_0_1, &t1664_TI, offsetof(t1664, f20), &EmptyCustomAttributesCache};
extern Il2CppType t344_0_0_1;
FieldInfo t1664_f21_FieldInfo = 
{
	"_val3", &t344_0_0_1, &t1664_TI, offsetof(t1664, f21), &EmptyCustomAttributesCache};
extern Il2CppType t344_0_0_1;
FieldInfo t1664_f22_FieldInfo = 
{
	"_val4", &t344_0_0_1, &t1664_TI, offsetof(t1664, f22), &EmptyCustomAttributesCache};
extern Il2CppType t200_0_0_1;
FieldInfo t1664_f23_FieldInfo = 
{
	"_cbuf", &t200_0_0_1, &t1664_TI, offsetof(t1664, f23), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1664_f24_FieldInfo = 
{
	"_ind", &t44_0_0_1, &t1664_TI, offsetof(t1664, f24), &EmptyCustomAttributesCache};
extern Il2CppType t1664_0_0_17;
extern CustomAttributesCache t1664__CustomAttributeCache_threadNumberFormatter;
FieldInfo t1664_f25_FieldInfo = 
{
	"threadNumberFormatter", &t1664_0_0_17, &t1664_TI, -1, &t1664__CustomAttributeCache_threadNumberFormatter};
static FieldInfo* t1664_FIs[] =
{
	&t1664_f0_FieldInfo,
	&t1664_f1_FieldInfo,
	&t1664_f2_FieldInfo,
	&t1664_f3_FieldInfo,
	&t1664_f4_FieldInfo,
	&t1664_f5_FieldInfo,
	&t1664_f6_FieldInfo,
	&t1664_f7_FieldInfo,
	&t1664_f8_FieldInfo,
	&t1664_f9_FieldInfo,
	&t1664_f10_FieldInfo,
	&t1664_f11_FieldInfo,
	&t1664_f12_FieldInfo,
	&t1664_f13_FieldInfo,
	&t1664_f14_FieldInfo,
	&t1664_f15_FieldInfo,
	&t1664_f16_FieldInfo,
	&t1664_f17_FieldInfo,
	&t1664_f18_FieldInfo,
	&t1664_f19_FieldInfo,
	&t1664_f20_FieldInfo,
	&t1664_f21_FieldInfo,
	&t1664_f22_FieldInfo,
	&t1664_f23_FieldInfo,
	&t1664_f24_FieldInfo,
	&t1664_f25_FieldInfo,
	NULL
};
static PropertyInfo t1664____CurrentCulture_PropertyInfo = 
{
	&t1664_TI, "CurrentCulture", NULL, &m9416_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1664____IntegerDigits_PropertyInfo = 
{
	&t1664_TI, "IntegerDigits", &m9417_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1664____DecimalDigits_PropertyInfo = 
{
	&t1664_TI, "DecimalDigits", &m9418_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1664____IsFloatingSource_PropertyInfo = 
{
	&t1664_TI, "IsFloatingSource", &m9419_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1664____IsZero_PropertyInfo = 
{
	&t1664_TI, "IsZero", &m9420_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1664____IsZeroInteger_PropertyInfo = 
{
	&t1664_TI, "IsZeroInteger", &m9421_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1664_PIs[] =
{
	&t1664____CurrentCulture_PropertyInfo,
	&t1664____IntegerDigits_PropertyInfo,
	&t1664____DecimalDigits_PropertyInfo,
	&t1664____IsFloatingSource_PropertyInfo,
	&t1664____IsZero_PropertyInfo,
	&t1664____IsZeroInteger_PropertyInfo,
	NULL
};
extern Il2CppType t1436_0_0_0;
extern Il2CppType t1436_0_0_0;
static ParameterInfo t1664_m9387_ParameterInfos[] = 
{
	{"current", 0, 134224230, &EmptyCustomAttributesCache, &t1436_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9387_MI = 
{
	".ctor", (methodPointerType)&m9387, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1664_m9387_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5226, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9388_MI = 
{
	".cctor", (methodPointerType)&m9388, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 5227, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2089_1_0_2;
extern Il2CppType t2089_1_0_0;
extern Il2CppType t2003_1_0_2;
extern Il2CppType t2003_1_0_0;
extern Il2CppType t1743_1_0_2;
extern Il2CppType t1743_1_0_0;
extern Il2CppType t1743_1_0_2;
extern Il2CppType t2090_1_0_2;
extern Il2CppType t2090_1_0_0;
extern Il2CppType t2003_1_0_2;
static ParameterInfo t1664_m9389_ParameterInfos[] = 
{
	{"MantissaBitsTable", 0, 134224231, &EmptyCustomAttributesCache, &t2089_1_0_2},
	{"TensExponentTable", 1, 134224232, &EmptyCustomAttributesCache, &t2003_1_0_2},
	{"DigitLowerTable", 2, 134224233, &EmptyCustomAttributesCache, &t1743_1_0_2},
	{"DigitUpperTable", 3, 134224234, &EmptyCustomAttributesCache, &t1743_1_0_2},
	{"TenPowersList", 4, 134224235, &EmptyCustomAttributesCache, &t2090_1_0_2},
	{"DecHexDigits", 5, 134224236, &EmptyCustomAttributesCache, &t2003_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2091_t2092_t2093_t2093_t2094_t2092 (MethodInfo* method, void* obj, void** args);
MethodInfo m9389_MI = 
{
	"GetFormatterTables", (methodPointerType)&m9389, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t2091_t2092_t2093_t2093_t2094_t2092, t1664_m9389_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 6, false, false, 5228, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9390_ParameterInfos[] = 
{
	{"i", 0, 134224237, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9390_MI = 
{
	"GetTenPowerOf", (methodPointerType)&m9390, &t1664_TI, &t919_0_0_0, RuntimeInvoker_t919_t44, t1664_m9390_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5229, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t344_0_0_0;
extern Il2CppType t344_0_0_0;
static ParameterInfo t1664_m9391_ParameterInfos[] = 
{
	{"value", 0, 134224238, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9391_MI = 
{
	"InitDecHexDigits", (methodPointerType)&m9391, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9391_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5230, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1089_0_0_0;
extern Il2CppType t1089_0_0_0;
static ParameterInfo t1664_m9392_ParameterInfos[] = 
{
	{"value", 0, 134224239, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9392_MI = 
{
	"InitDecHexDigits", (methodPointerType)&m9392, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t919, t1664_m9392_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5231, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t344_0_0_0;
extern Il2CppType t1089_0_0_0;
static ParameterInfo t1664_m9393_ParameterInfos[] = 
{
	{"hi", 0, 134224240, &EmptyCustomAttributesCache, &t344_0_0_0},
	{"lo", 1, 134224241, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9393_MI = 
{
	"InitDecHexDigits", (methodPointerType)&m9393, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t919, t1664_m9393_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5232, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9394_ParameterInfos[] = 
{
	{"val", 0, 134224242, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t344_0_0_0;
extern void* RuntimeInvoker_t344_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9394_MI = 
{
	"FastToDecHex", (methodPointerType)&m9394, &t1664_TI, &t344_0_0_0, RuntimeInvoker_t344_t44, t1664_m9394_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5233, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9395_ParameterInfos[] = 
{
	{"val", 0, 134224243, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t344_0_0_0;
extern void* RuntimeInvoker_t344_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9395_MI = 
{
	"ToDecHex", (methodPointerType)&m9395, &t1664_TI, &t344_0_0_0, RuntimeInvoker_t344_t44, t1664_m9395_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5234, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9396_ParameterInfos[] = 
{
	{"val", 0, 134224244, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9396_MI = 
{
	"FastDecHexLen", (methodPointerType)&m9396, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t1664_m9396_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5235, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t344_0_0_0;
static ParameterInfo t1664_m9397_ParameterInfos[] = 
{
	{"val", 0, 134224245, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9397_MI = 
{
	"DecHexLen", (methodPointerType)&m9397, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t1664_m9397_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5236, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9398_MI = 
{
	"DecHexLen", (methodPointerType)&m9398, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5237, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
static ParameterInfo t1664_m9399_ParameterInfos[] = 
{
	{"hi", 0, 134224246, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9399_MI = 
{
	"ScaleOrder", (methodPointerType)&m9399, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44_t919, t1664_m9399_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5238, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9400_MI = 
{
	"InitialFloatingPrecision", (methodPointerType)&m9400, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5239, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1664_m9401_ParameterInfos[] = 
{
	{"format", 0, 134224247, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9401_MI = 
{
	"ParsePrecision", (methodPointerType)&m9401, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1664_m9401_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5240, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1664_m9402_ParameterInfos[] = 
{
	{"format", 0, 134224248, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9402_MI = 
{
	"Init", (methodPointerType)&m9402, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1664_m9402_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5241, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1089_0_0_0;
static ParameterInfo t1664_m9403_ParameterInfos[] = 
{
	{"value", 0, 134224249, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9403_MI = 
{
	"InitHex", (methodPointerType)&m9403, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t919, t1664_m9403_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5242, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9404_ParameterInfos[] = 
{
	{"format", 0, 134224250, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224251, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"defPrecision", 2, 134224252, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9404_MI = 
{
	"Init", (methodPointerType)&m9404, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44, t1664_m9404_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 5243, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t344_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9405_ParameterInfos[] = 
{
	{"format", 0, 134224253, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224254, &EmptyCustomAttributesCache, &t344_0_0_0},
	{"defPrecision", 2, 134224255, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9405_MI = 
{
	"Init", (methodPointerType)&m9405, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44, t1664_m9405_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 5244, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t1664_m9406_ParameterInfos[] = 
{
	{"format", 0, 134224256, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224257, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9406_MI = 
{
	"Init", (methodPointerType)&m9406, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t919, t1664_m9406_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5245, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1089_0_0_0;
static ParameterInfo t1664_m9407_ParameterInfos[] = 
{
	{"format", 0, 134224258, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224259, &EmptyCustomAttributesCache, &t1089_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9407_MI = 
{
	"Init", (methodPointerType)&m9407, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t919, t1664_m9407_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5246, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t601_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9408_ParameterInfos[] = 
{
	{"format", 0, 134224260, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224261, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"defPrecision", 2, 134224262, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t601_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9408_MI = 
{
	"Init", (methodPointerType)&m9408, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t601_t44, t1664_m9408_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 5247, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t1664_m9409_ParameterInfos[] = 
{
	{"format", 0, 134224263, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224264, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m9409_MI = 
{
	"Init", (methodPointerType)&m9409, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1126, t1664_m9409_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5248, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9410_ParameterInfos[] = 
{
	{"size", 0, 134224265, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9410_MI = 
{
	"ResetCharBuf", (methodPointerType)&m9410, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9410_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5249, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9411_ParameterInfos[] = 
{
	{"len", 0, 134224266, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9411_MI = 
{
	"Resize", (methodPointerType)&m9411, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9411_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5250, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t1664_m9412_ParameterInfos[] = 
{
	{"c", 0, 134224267, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m9412_MI = 
{
	"Append", (methodPointerType)&m9412, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t1664_m9412_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5251, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9413_ParameterInfos[] = 
{
	{"c", 0, 134224268, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"cnt", 1, 134224269, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9413_MI = 
{
	"Append", (methodPointerType)&m9413, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t44, t1664_m9413_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5252, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1664_m9414_ParameterInfos[] = 
{
	{"s", 0, 134224270, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9414_MI = 
{
	"Append", (methodPointerType)&m9414, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1664_m9414_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5253, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9415_ParameterInfos[] = 
{
	{"fp", 0, 134224271, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t1125_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9415_MI = 
{
	"GetNumberFormatInstance", (methodPointerType)&m9415, &t1664_TI, &t1125_0_0_0, RuntimeInvoker_t29_t29, t1664_m9415_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5254, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t633_0_0_0;
static ParameterInfo t1664_m9416_ParameterInfos[] = 
{
	{"value", 0, 134224272, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9416_MI = 
{
	"set_CurrentCulture", (methodPointerType)&m9416, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1664_m9416_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 5255, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9417_MI = 
{
	"get_IntegerDigits", (methodPointerType)&m9417, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 5256, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9418_MI = 
{
	"get_DecimalDigits", (methodPointerType)&m9418, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 5257, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9419_MI = 
{
	"get_IsFloatingSource", (methodPointerType)&m9419, &t1664_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 5258, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9420_MI = 
{
	"get_IsZero", (methodPointerType)&m9420, &t1664_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 5259, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9421_MI = 
{
	"get_IsZeroInteger", (methodPointerType)&m9421, &t1664_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 5260, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9422_ParameterInfos[] = 
{
	{"pos", 0, 134224273, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9422_MI = 
{
	"RoundPos", (methodPointerType)&m9422, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9422_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5261, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9423_ParameterInfos[] = 
{
	{"decimals", 0, 134224274, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9423_MI = 
{
	"RoundDecimal", (methodPointerType)&m9423, &t1664_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t1664_m9423_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5262, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9424_ParameterInfos[] = 
{
	{"shift", 0, 134224275, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9424_MI = 
{
	"RoundBits", (methodPointerType)&m9424, &t1664_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t1664_m9424_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5263, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9425_MI = 
{
	"RemoveTrailingZeros", (methodPointerType)&m9425, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5264, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9426_MI = 
{
	"AddOneToDecHex", (methodPointerType)&m9426, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5265, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t344_0_0_0;
static ParameterInfo t1664_m9427_ParameterInfos[] = 
{
	{"val", 0, 134224276, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t344_0_0_0;
extern void* RuntimeInvoker_t344_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9427_MI = 
{
	"AddOneToDecHex", (methodPointerType)&m9427, &t1664_TI, &t344_0_0_0, RuntimeInvoker_t344_t44, t1664_m9427_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5266, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9428_MI = 
{
	"CountTrailingZeros", (methodPointerType)&m9428, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5267, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t344_0_0_0;
static ParameterInfo t1664_m9429_ParameterInfos[] = 
{
	{"val", 0, 134224277, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9429_MI = 
{
	"CountTrailingZeros", (methodPointerType)&m9429, &t1664_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t1664_m9429_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5268, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1664_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9430_MI = 
{
	"GetInstance", (methodPointerType)&m9430, &t1664_TI, &t1664_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 145, 0, 255, 0, false, false, 5269, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9431_MI = 
{
	"Release", (methodPointerType)&m9431, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5270, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t633_0_0_0;
static ParameterInfo t1664_m9432_ParameterInfos[] = 
{
	{"culture", 0, 134224278, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9432_MI = 
{
	"SetThreadCurrentCulture", (methodPointerType)&m9432, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1664_m9432_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 5271, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t297_0_0_0;
extern Il2CppType t297_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9433_ParameterInfos[] = 
{
	{"format", 0, 134224279, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224280, &EmptyCustomAttributesCache, &t297_0_0_0},
	{"fp", 2, 134224281, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9433_MI = 
{
	"NumberToString", (methodPointerType)&m9433, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t297_t29, t1664_m9433_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5272, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t348_0_0_0;
extern Il2CppType t348_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9434_ParameterInfos[] = 
{
	{"format", 0, 134224282, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224283, &EmptyCustomAttributesCache, &t348_0_0_0},
	{"fp", 2, 134224284, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9434_MI = 
{
	"NumberToString", (methodPointerType)&m9434, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t297_t29, t1664_m9434_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5273, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t626_0_0_0;
extern Il2CppType t626_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9435_ParameterInfos[] = 
{
	{"format", 0, 134224285, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224286, &EmptyCustomAttributesCache, &t626_0_0_0},
	{"fp", 2, 134224287, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t372_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9435_MI = 
{
	"NumberToString", (methodPointerType)&m9435, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t372_t29, t1664_m9435_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5274, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t372_0_0_0;
extern Il2CppType t372_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9436_ParameterInfos[] = 
{
	{"format", 0, 134224288, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224289, &EmptyCustomAttributesCache, &t372_0_0_0},
	{"fp", 2, 134224290, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t372_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9436_MI = 
{
	"NumberToString", (methodPointerType)&m9436, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t372_t29, t1664_m9436_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5275, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t344_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9437_ParameterInfos[] = 
{
	{"format", 0, 134224291, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224292, &EmptyCustomAttributesCache, &t344_0_0_0},
	{"fp", 2, 134224293, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9437_MI = 
{
	"NumberToString", (methodPointerType)&m9437, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t44_t29, t1664_m9437_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5276, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9438_ParameterInfos[] = 
{
	{"format", 0, 134224294, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224295, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"fp", 2, 134224296, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9438_MI = 
{
	"NumberToString", (methodPointerType)&m9438, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t44_t29, t1664_m9438_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5277, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1089_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9439_ParameterInfos[] = 
{
	{"format", 0, 134224297, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224298, &EmptyCustomAttributesCache, &t1089_0_0_0},
	{"fp", 2, 134224299, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9439_MI = 
{
	"NumberToString", (methodPointerType)&m9439, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t919_t29, t1664_m9439_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5278, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9440_ParameterInfos[] = 
{
	{"format", 0, 134224300, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224301, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"fp", 2, 134224302, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9440_MI = 
{
	"NumberToString", (methodPointerType)&m9440, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t919_t29, t1664_m9440_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5279, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9441_ParameterInfos[] = 
{
	{"format", 0, 134224303, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224304, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"fp", 2, 134224305, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9441_MI = 
{
	"NumberToString", (methodPointerType)&m9441, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t22_t29, t1664_m9441_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5280, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t601_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9442_ParameterInfos[] = 
{
	{"format", 0, 134224306, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224307, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"fp", 2, 134224308, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t601_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9442_MI = 
{
	"NumberToString", (methodPointerType)&m9442, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t601_t29, t1664_m9442_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5281, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1126_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9443_ParameterInfos[] = 
{
	{"format", 0, 134224309, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134224310, &EmptyCustomAttributesCache, &t1126_0_0_0},
	{"fp", 2, 134224311, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t1126_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9443_MI = 
{
	"NumberToString", (methodPointerType)&m9443, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t1126_t29, t1664_m9443_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5282, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t344_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9444_ParameterInfos[] = 
{
	{"value", 0, 134224312, &EmptyCustomAttributesCache, &t344_0_0_0},
	{"fp", 1, 134224313, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9444_MI = 
{
	"NumberToString", (methodPointerType)&m9444, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9444_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5283, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9445_ParameterInfos[] = 
{
	{"value", 0, 134224314, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"fp", 1, 134224315, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9445_MI = 
{
	"NumberToString", (methodPointerType)&m9445, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9445_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5284, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1089_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9446_ParameterInfos[] = 
{
	{"value", 0, 134224316, &EmptyCustomAttributesCache, &t1089_0_0_0},
	{"fp", 1, 134224317, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9446_MI = 
{
	"NumberToString", (methodPointerType)&m9446, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t919_t29, t1664_m9446_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5285, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9447_ParameterInfos[] = 
{
	{"value", 0, 134224318, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"fp", 1, 134224319, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9447_MI = 
{
	"NumberToString", (methodPointerType)&m9447, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t919_t29, t1664_m9447_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5286, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9448_ParameterInfos[] = 
{
	{"value", 0, 134224320, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"fp", 1, 134224321, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9448_MI = 
{
	"NumberToString", (methodPointerType)&m9448, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t22_t29, t1664_m9448_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5287, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9449_ParameterInfos[] = 
{
	{"value", 0, 134224322, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"fp", 1, 134224323, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t601_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9449_MI = 
{
	"NumberToString", (methodPointerType)&m9449, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t601_t29, t1664_m9449_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5288, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9450_ParameterInfos[] = 
{
	{"value", 0, 134224324, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"fp", 1, 134224325, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9450_MI = 
{
	"FastIntegerToString", (methodPointerType)&m9450, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9450_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5289, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1094_0_0_0;
static ParameterInfo t1664_m9451_ParameterInfos[] = 
{
	{"format", 0, 134224326, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"fp", 1, 134224327, &EmptyCustomAttributesCache, &t1094_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9451_MI = 
{
	"IntegerToString", (methodPointerType)&m9451, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t29, t1664_m9451_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5290, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9452_ParameterInfos[] = 
{
	{"format", 0, 134224328, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"nfi", 1, 134224329, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9452_MI = 
{
	"NumberToString", (methodPointerType)&m9452, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t29, t1664_m9452_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5291, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9453_ParameterInfos[] = 
{
	{"precision", 0, 134224330, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224331, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9453_MI = 
{
	"FormatCurrency", (methodPointerType)&m9453, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9453_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 5292, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9454_ParameterInfos[] = 
{
	{"precision", 0, 134224332, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224333, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9454_MI = 
{
	"FormatDecimal", (methodPointerType)&m9454, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9454_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5293, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9455_ParameterInfos[] = 
{
	{"precision", 0, 134224334, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9455_MI = 
{
	"FormatHexadecimal", (methodPointerType)&m9455, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t1664_m9455_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5294, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9456_ParameterInfos[] = 
{
	{"precision", 0, 134224335, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224336, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9456_MI = 
{
	"FormatFixedPoint", (methodPointerType)&m9456, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9456_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 5295, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9457_ParameterInfos[] = 
{
	{"origval", 0, 134224337, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"nfi", 1, 134224338, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t601_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9457_MI = 
{
	"FormatRoundtrip", (methodPointerType)&m9457, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t601_t29, t1664_m9457_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5296, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9458_ParameterInfos[] = 
{
	{"origval", 0, 134224339, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"nfi", 1, 134224340, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9458_MI = 
{
	"FormatRoundtrip", (methodPointerType)&m9458, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t22_t29, t1664_m9458_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5297, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9459_ParameterInfos[] = 
{
	{"precision", 0, 134224341, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224342, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9459_MI = 
{
	"FormatGeneral", (methodPointerType)&m9459, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9459_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5298, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9460_ParameterInfos[] = 
{
	{"precision", 0, 134224343, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224344, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9460_MI = 
{
	"FormatNumber", (methodPointerType)&m9460, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9460_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 5299, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9461_ParameterInfos[] = 
{
	{"precision", 0, 134224345, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224346, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9461_MI = 
{
	"FormatPercent", (methodPointerType)&m9461, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9461_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 5300, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9462_ParameterInfos[] = 
{
	{"precision", 0, 134224347, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224348, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9462_MI = 
{
	"FormatExponential", (methodPointerType)&m9462, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t1664_m9462_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 5301, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1125_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9463_ParameterInfos[] = 
{
	{"precision", 0, 134224349, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"nfi", 1, 134224350, &EmptyCustomAttributesCache, &t1125_0_0_0},
	{"expDigits", 2, 134224351, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9463_MI = 
{
	"FormatExponential", (methodPointerType)&m9463, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29_t44, t1664_m9463_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 5302, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1125_0_0_0;
static ParameterInfo t1664_m9464_ParameterInfos[] = 
{
	{"format", 0, 134224352, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"nfi", 1, 134224353, &EmptyCustomAttributesCache, &t1125_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9464_MI = 
{
	"FormatCustom", (methodPointerType)&m9464, &t1664_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t29, t1664_m9464_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 5303, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t292_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1664_m9465_ParameterInfos[] = 
{
	{"sb", 0, 134224354, &EmptyCustomAttributesCache, &t292_0_0_0},
	{"canEmpty", 1, 134224355, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9465_MI = 
{
	"ZeroTrimEnd", (methodPointerType)&m9465, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t1664_m9465_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 5304, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t292_0_0_0;
static ParameterInfo t1664_m9466_ParameterInfos[] = 
{
	{"sb", 0, 134224356, &EmptyCustomAttributesCache, &t292_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9466_MI = 
{
	"IsZeroOnly", (methodPointerType)&m9466, &t1664_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1664_m9466_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 5305, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t292_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9467_ParameterInfos[] = 
{
	{"sb", 0, 134224357, &EmptyCustomAttributesCache, &t292_0_0_0},
	{"v", 1, 134224358, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9467_MI = 
{
	"AppendNonNegativeNumber", (methodPointerType)&m9467, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t1664_m9467_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 5306, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t292_0_0_0;
static ParameterInfo t1664_m9468_ParameterInfos[] = 
{
	{"minLength", 0, 134224359, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"sb", 1, 134224360, &EmptyCustomAttributesCache, &t292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9468_MI = 
{
	"AppendIntegerString", (methodPointerType)&m9468, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1664_m9468_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5307, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9469_ParameterInfos[] = 
{
	{"minLength", 0, 134224361, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9469_MI = 
{
	"AppendIntegerString", (methodPointerType)&m9469, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9469_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5308, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t292_0_0_0;
static ParameterInfo t1664_m9470_ParameterInfos[] = 
{
	{"precision", 0, 134224362, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"sb", 1, 134224363, &EmptyCustomAttributesCache, &t292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9470_MI = 
{
	"AppendDecimalString", (methodPointerType)&m9470, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1664_m9470_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5309, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9471_ParameterInfos[] = 
{
	{"precision", 0, 134224364, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9471_MI = 
{
	"AppendDecimalString", (methodPointerType)&m9471, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9471_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5310, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1664_m9472_ParameterInfos[] = 
{
	{"groups", 0, 134224365, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"groupSeparator", 1, 134224366, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9472_MI = 
{
	"AppendIntegerStringWithGroupSeparator", (methodPointerType)&m9472, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1664_m9472_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5311, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1125_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9473_ParameterInfos[] = 
{
	{"nfi", 0, 134224367, &EmptyCustomAttributesCache, &t1125_0_0_0},
	{"exponent", 1, 134224368, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minDigits", 2, 134224369, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9473_MI = 
{
	"AppendExponent", (methodPointerType)&m9473, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44, t1664_m9473_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 5312, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9474_ParameterInfos[] = 
{
	{"start", 0, 134224370, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9474_MI = 
{
	"AppendOneDigit", (methodPointerType)&m9474, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9474_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5313, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1664_m9475_ParameterInfos[] = 
{
	{"val", 0, 134224371, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"force", 1, 134224372, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9475_MI = 
{
	"FastAppendDigits", (methodPointerType)&m9475, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t1664_m9475_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5314, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9476_ParameterInfos[] = 
{
	{"start", 0, 134224373, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"end", 1, 134224374, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9476_MI = 
{
	"AppendDigits", (methodPointerType)&m9476, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t1664_m9476_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 5315, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t292_0_0_0;
static ParameterInfo t1664_m9477_ParameterInfos[] = 
{
	{"start", 0, 134224375, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"end", 1, 134224376, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"sb", 2, 134224377, &EmptyCustomAttributesCache, &t292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9477_MI = 
{
	"AppendDigits", (methodPointerType)&m9477, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t29, t1664_m9477_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 5316, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9478_ParameterInfos[] = 
{
	{"count", 0, 134224378, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9478_MI = 
{
	"Multiply10", (methodPointerType)&m9478, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9478_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5317, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1664_m9479_ParameterInfos[] = 
{
	{"count", 0, 134224379, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9479_MI = 
{
	"Divide10", (methodPointerType)&m9479, &t1664_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1664_m9479_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5318, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1664_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9480_MI = 
{
	"GetClone", (methodPointerType)&m9480, &t1664_TI, &t1664_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 5319, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1664_MIs[] =
{
	&m9387_MI,
	&m9388_MI,
	&m9389_MI,
	&m9390_MI,
	&m9391_MI,
	&m9392_MI,
	&m9393_MI,
	&m9394_MI,
	&m9395_MI,
	&m9396_MI,
	&m9397_MI,
	&m9398_MI,
	&m9399_MI,
	&m9400_MI,
	&m9401_MI,
	&m9402_MI,
	&m9403_MI,
	&m9404_MI,
	&m9405_MI,
	&m9406_MI,
	&m9407_MI,
	&m9408_MI,
	&m9409_MI,
	&m9410_MI,
	&m9411_MI,
	&m9412_MI,
	&m9413_MI,
	&m9414_MI,
	&m9415_MI,
	&m9416_MI,
	&m9417_MI,
	&m9418_MI,
	&m9419_MI,
	&m9420_MI,
	&m9421_MI,
	&m9422_MI,
	&m9423_MI,
	&m9424_MI,
	&m9425_MI,
	&m9426_MI,
	&m9427_MI,
	&m9428_MI,
	&m9429_MI,
	&m9430_MI,
	&m9431_MI,
	&m9432_MI,
	&m9433_MI,
	&m9434_MI,
	&m9435_MI,
	&m9436_MI,
	&m9437_MI,
	&m9438_MI,
	&m9439_MI,
	&m9440_MI,
	&m9441_MI,
	&m9442_MI,
	&m9443_MI,
	&m9444_MI,
	&m9445_MI,
	&m9446_MI,
	&m9447_MI,
	&m9448_MI,
	&m9449_MI,
	&m9450_MI,
	&m9451_MI,
	&m9452_MI,
	&m9453_MI,
	&m9454_MI,
	&m9455_MI,
	&m9456_MI,
	&m9457_MI,
	&m9458_MI,
	&m9459_MI,
	&m9460_MI,
	&m9461_MI,
	&m9462_MI,
	&m9463_MI,
	&m9464_MI,
	&m9465_MI,
	&m9466_MI,
	&m9467_MI,
	&m9468_MI,
	&m9469_MI,
	&m9470_MI,
	&m9471_MI,
	&m9472_MI,
	&m9473_MI,
	&m9474_MI,
	&m9475_MI,
	&m9476_MI,
	&m9477_MI,
	&m9478_MI,
	&m9479_MI,
	&m9480_MI,
	NULL
};
extern TypeInfo t1663_TI;
static TypeInfo* t1664_TI__nestedTypes[2] =
{
	&t1663_TI,
	NULL
};
static MethodInfo* t1664_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t1671_TI;
#include "t1671.h"
#include "t1671MD.h"
extern MethodInfo m9519_MI;
void t1664_CustomAttributesCacheGenerator_threadNumberFormatter(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1671 * tmp;
		tmp = (t1671 *)il2cpp_codegen_object_new (&t1671_TI);
		m9519(tmp, &m9519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1664__CustomAttributeCache_threadNumberFormatter = {
1,
NULL,
&t1664_CustomAttributesCacheGenerator_threadNumberFormatter
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1664_0_0_0;
extern Il2CppType t1664_1_0_0;
struct t1664;
extern CustomAttributesCache t1664__CustomAttributeCache_threadNumberFormatter;
TypeInfo t1664_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NumberFormatter", "System", t1664_MIs, t1664_PIs, t1664_FIs, NULL, &t29_TI, t1664_TI__nestedTypes, NULL, &t1664_TI, NULL, t1664_VT, &EmptyCustomAttributesCache, &t1664_TI, &t1664_0_0_0, &t1664_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1664), 0, -1, sizeof(t1664_SFs), sizeof(t1664_TSFs), -1, 256, 0, false, false, false, false, false, false, false, false, false, true, false, false, 94, 6, 26, 0, 1, 4, 0, 0};
#include "t1101.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1101_TI;
#include "t1101MD.h"



extern MethodInfo m5150_MI;
 void m5150 (t1101 * __this, t7* p0, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2219, &m6079_MI);
		m3964(__this, L_0, &m3964_MI);
		__this->f12 = p0;
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral2219, &m6079_MI);
		__this->f13 = L_1;
		return;
	}
}
extern MethodInfo m9481_MI;
 void m9481 (t1101 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		m3964(__this, p1, &m3964_MI);
		__this->f12 = p0;
		__this->f13 = p1;
		return;
	}
}
extern MethodInfo m9482_MI;
 void m9482 (t1101 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9282(__this, p0, p1, &m9282_MI);
		t7* L_0 = m3994(p0, (t7*) &_stringLiteral2220, &m3994_MI);
		__this->f12 = L_0;
		return;
	}
}
extern MethodInfo m9483_MI;
 t7* m9483 (t1101 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f13);
		return L_0;
	}
}
extern MethodInfo m9484_MI;
 void m9484 (t1101 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m2857(__this, p0, p1, &m2857_MI);
		t7* L_0 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral2220, L_0, &m3997_MI);
		return;
	}
}
// Metadata Definition System.ObjectDisposedException
extern Il2CppType t7_0_0_1;
FieldInfo t1101_f12_FieldInfo = 
{
	"obj_name", &t7_0_0_1, &t1101_TI, offsetof(t1101, f12), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1101_f13_FieldInfo = 
{
	"msg", &t7_0_0_1, &t1101_TI, offsetof(t1101, f13), &EmptyCustomAttributesCache};
static FieldInfo* t1101_FIs[] =
{
	&t1101_f12_FieldInfo,
	&t1101_f13_FieldInfo,
	NULL
};
static PropertyInfo t1101____Message_PropertyInfo = 
{
	&t1101_TI, "Message", &m9483_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1101_PIs[] =
{
	&t1101____Message_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1101_m5150_ParameterInfos[] = 
{
	{"objectName", 0, 134224397, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5150_MI = 
{
	".ctor", (methodPointerType)&m5150, &t1101_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1101_m5150_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5324, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1101_m9481_ParameterInfos[] = 
{
	{"objectName", 0, 134224398, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"message", 1, 134224399, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9481_MI = 
{
	".ctor", (methodPointerType)&m9481, &t1101_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1101_m9481_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5325, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1101_m9482_ParameterInfos[] = 
{
	{"info", 0, 134224400, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224401, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9482_MI = 
{
	".ctor", (methodPointerType)&m9482, &t1101_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1101_m9482_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5326, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9483_MI = 
{
	"get_Message", (methodPointerType)&m9483, &t1101_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 5327, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1101_m9484_ParameterInfos[] = 
{
	{"info", 0, 134224402, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224403, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9484_MI = 
{
	"GetObjectData", (methodPointerType)&m9484, &t1101_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1101_m9484_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, false, 5328, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1101_MIs[] =
{
	&m5150_MI,
	&m9481_MI,
	&m9482_MI,
	&m9483_MI,
	&m9484_MI,
	NULL
};
static MethodInfo* t1101_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m9484_MI,
	&m2858_MI,
	&m9483_MI,
	&m2859_MI,
	&m2860_MI,
	&m9484_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1101_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1101_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1101__CustomAttributeCache = {
1,
NULL,
&t1101_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1101_0_0_0;
extern Il2CppType t1101_1_0_0;
struct t1101;
extern CustomAttributesCache t1101__CustomAttributeCache;
TypeInfo t1101_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObjectDisposedException", "System", t1101_MIs, t1101_PIs, t1101_FIs, NULL, &t914_TI, NULL, NULL, &t1101_TI, NULL, t1101_VT, &t1101__CustomAttributeCache, &t1101_TI, &t1101_0_0_0, &t1101_1_0_0, t1101_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1101), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 2, 0, 0, 11, 0, 2};
#include "t1643.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1643_TI;
#include "t1643MD.h"

#include "t1644.h"
#include "t760.h"
extern TypeInfo t1644_TI;
extern TypeInfo t760_TI;
#include "t760MD.h"
extern MethodInfo m9605_MI;
extern MethodInfo m9603_MI;


extern MethodInfo m9485_MI;
 void m9485 (t1643 * __this, int32_t p0, t760 * p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f2 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		m1331(__this, &m1331_MI);
		bool L_0 = m9605(NULL, p1, (t760 *)NULL, &m9605_MI);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral259, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0025:
	{
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
extern MethodInfo m9486_MI;
 int32_t m9486 (t1643 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m9487_MI;
 void m9487 (t1643 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1644_TI), &L_1);
		m3997(p0, (t7*) &_stringLiteral2221, L_2, &m3997_MI);
		t760 * L_3 = (__this->f1);
		m3997(p0, (t7*) &_stringLiteral2222, L_3, &m3997_MI);
		t7* L_4 = (__this->f2);
		m3997(p0, (t7*) &_stringLiteral2223, L_4, &m3997_MI);
		return;
	}
}
extern MethodInfo m9488_MI;
 t7* m9488 (t1643 * __this, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->f0);
		V_1 = L_0;
		if (V_1 == 0)
		{
			goto IL_003b;
		}
		if (V_1 == 1)
		{
			goto IL_0043;
		}
		if (V_1 == 2)
		{
			goto IL_0033;
		}
		if (V_1 == 3)
		{
			goto IL_004b;
		}
		if (V_1 == 4)
		{
			goto IL_0053;
		}
		if (V_1 == 5)
		{
			goto IL_005b;
		}
		if (V_1 == 6)
		{
			goto IL_0063;
		}
	}
	{
		if ((((int32_t)V_1) == ((int32_t)((int32_t)128))))
		{
			goto IL_0053;
		}
	}
	{
		goto IL_006b;
	}

IL_0033:
	{
		V_0 = (t7*) &_stringLiteral2224;
		goto IL_0078;
	}

IL_003b:
	{
		V_0 = (t7*) &_stringLiteral2225;
		goto IL_0078;
	}

IL_0043:
	{
		V_0 = (t7*) &_stringLiteral2226;
		goto IL_0078;
	}

IL_004b:
	{
		V_0 = (t7*) &_stringLiteral2227;
		goto IL_0078;
	}

IL_0053:
	{
		V_0 = (t7*) &_stringLiteral2228;
		goto IL_0078;
	}

IL_005b:
	{
		V_0 = (t7*) &_stringLiteral2229;
		goto IL_0078;
	}

IL_0063:
	{
		V_0 = (t7*) &_stringLiteral2230;
		goto IL_0078;
	}

IL_006b:
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral2231, &m6079_MI);
		V_0 = L_1;
		goto IL_0078;
	}

IL_0078:
	{
		t760 * L_2 = (__this->f1);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m9603_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m1685(NULL, V_0, (t7*) &_stringLiteral79, L_3, &m1685_MI);
		return L_4;
	}
}
// Metadata Definition System.OperatingSystem
extern Il2CppType t1644_0_0_1;
FieldInfo t1643_f0_FieldInfo = 
{
	"_platform", &t1644_0_0_1, &t1643_TI, offsetof(t1643, f0), &EmptyCustomAttributesCache};
extern Il2CppType t760_0_0_1;
FieldInfo t1643_f1_FieldInfo = 
{
	"_version", &t760_0_0_1, &t1643_TI, offsetof(t1643, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1643_f2_FieldInfo = 
{
	"_servicePack", &t7_0_0_1, &t1643_TI, offsetof(t1643, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1643_FIs[] =
{
	&t1643_f0_FieldInfo,
	&t1643_f1_FieldInfo,
	&t1643_f2_FieldInfo,
	NULL
};
static PropertyInfo t1643____Platform_PropertyInfo = 
{
	&t1643_TI, "Platform", &m9486_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1643_PIs[] =
{
	&t1643____Platform_PropertyInfo,
	NULL
};
extern Il2CppType t1644_0_0_0;
extern Il2CppType t1644_0_0_0;
extern Il2CppType t760_0_0_0;
extern Il2CppType t760_0_0_0;
static ParameterInfo t1643_m9485_ParameterInfos[] = 
{
	{"platform", 0, 134224404, &EmptyCustomAttributesCache, &t1644_0_0_0},
	{"version", 1, 134224405, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9485_MI = 
{
	".ctor", (methodPointerType)&m9485, &t1643_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1643_m9485_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5329, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1644_0_0_0;
extern void* RuntimeInvoker_t1644 (MethodInfo* method, void* obj, void** args);
MethodInfo m9486_MI = 
{
	"get_Platform", (methodPointerType)&m9486, &t1643_TI, &t1644_0_0_0, RuntimeInvoker_t1644, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5330, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1643_m9487_ParameterInfos[] = 
{
	{"info", 0, 134224406, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224407, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9487_MI = 
{
	"GetObjectData", (methodPointerType)&m9487, &t1643_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1643_m9487_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 2, false, false, 5331, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9488_MI = 
{
	"ToString", (methodPointerType)&m9488, &t1643_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 5332, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1643_MIs[] =
{
	&m9485_MI,
	&m9486_MI,
	&m9487_MI,
	&m9488_MI,
	NULL
};
static MethodInfo* t1643_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m9488_MI,
	&m9487_MI,
};
extern TypeInfo t373_TI;
static TypeInfo* t1643_ITIs[] = 
{
	&t373_TI,
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1643_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1643_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1643__CustomAttributeCache = {
1,
NULL,
&t1643_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1643_0_0_0;
extern Il2CppType t1643_1_0_0;
struct t1643;
extern CustomAttributesCache t1643__CustomAttributeCache;
TypeInfo t1643_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OperatingSystem", "System", t1643_MIs, t1643_PIs, t1643_FIs, NULL, &t29_TI, NULL, NULL, &t1643_TI, t1643_ITIs, t1643_VT, &t1643__CustomAttributeCache, &t1643_TI, &t1643_0_0_0, &t1643_1_0_0, t1643_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1643), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 3, 0, 0, 5, 2, 2};
#include "t1665.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1665_TI;
#include "t1665MD.h"



extern MethodInfo m9489_MI;
 void m9489 (t1665 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2232, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2147024882), &m2946_MI);
		return;
	}
}
extern MethodInfo m9490_MI;
 void m9490 (t1665 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.OutOfMemoryException
extern Il2CppType t44_0_0_32849;
FieldInfo t1665_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t1665_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1665_FIs[] =
{
	&t1665_f11_FieldInfo,
	NULL
};
static const int32_t t1665_f11_DefaultValueData = -2147024882;
static Il2CppFieldDefaultValueEntry t1665_f11_DefaultValue = 
{
	&t1665_f11_FieldInfo, { (char*)&t1665_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1665_FDVs[] = 
{
	&t1665_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9489_MI = 
{
	".ctor", (methodPointerType)&m9489, &t1665_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5333, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1665_m9490_ParameterInfos[] = 
{
	{"info", 0, 134224408, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224409, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9490_MI = 
{
	".ctor", (methodPointerType)&m9490, &t1665_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1665_m9490_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5334, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1665_MIs[] =
{
	&m9489_MI,
	&m9490_MI,
	NULL
};
static MethodInfo* t1665_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1665_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1665_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1665__CustomAttributeCache = {
1,
NULL,
&t1665_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1665_0_0_0;
extern Il2CppType t1665_1_0_0;
struct t1665;
extern CustomAttributesCache t1665__CustomAttributeCache;
TypeInfo t1665_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OutOfMemoryException", "System", t1665_MIs, NULL, t1665_FIs, NULL, &t956_TI, NULL, NULL, &t1665_TI, NULL, t1665_VT, &t1665__CustomAttributeCache, &t1665_TI, &t1665_0_0_0, &t1665_1_0_0, t1665_IOs, NULL, NULL, t1665_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1665), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1090MD.h"
extern MethodInfo m5101_MI;
extern MethodInfo m8865_MI;


extern MethodInfo m9491_MI;
 void m9491 (t1666 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2233, &m6079_MI);
		m5101(__this, L_0, &m5101_MI);
		m2946(__this, ((int32_t)-2146233066), &m2946_MI);
		return;
	}
}
 void m9492 (t1666 * __this, t7* p0, MethodInfo* method){
	{
		m5101(__this, p0, &m5101_MI);
		m2946(__this, ((int32_t)-2146233066), &m2946_MI);
		return;
	}
}
extern MethodInfo m9493_MI;
 void m9493 (t1666 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m8865(__this, p0, p1, &m8865_MI);
		return;
	}
}
// Metadata Definition System.OverflowException
extern Il2CppType t44_0_0_32849;
FieldInfo t1666_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t1666_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1666_FIs[] =
{
	&t1666_f11_FieldInfo,
	NULL
};
static const int32_t t1666_f11_DefaultValueData = -2146233066;
static Il2CppFieldDefaultValueEntry t1666_f11_DefaultValue = 
{
	&t1666_f11_FieldInfo, { (char*)&t1666_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1666_FDVs[] = 
{
	&t1666_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9491_MI = 
{
	".ctor", (methodPointerType)&m9491, &t1666_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5335, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1666_m9492_ParameterInfos[] = 
{
	{"message", 0, 134224410, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9492_MI = 
{
	".ctor", (methodPointerType)&m9492, &t1666_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1666_m9492_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5336, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1666_m9493_ParameterInfos[] = 
{
	{"info", 0, 134224411, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224412, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9493_MI = 
{
	".ctor", (methodPointerType)&m9493, &t1666_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1666_m9493_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5337, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1666_MIs[] =
{
	&m9491_MI,
	&m9492_MI,
	&m9493_MI,
	NULL
};
static MethodInfo* t1666_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1666_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1666__CustomAttributeCache = {
1,
NULL,
&t1666_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1666_0_0_0;
extern Il2CppType t1666_1_0_0;
extern TypeInfo t1090_TI;
struct t1666;
extern CustomAttributesCache t1666__CustomAttributeCache;
TypeInfo t1666_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OverflowException", "System", t1666_MIs, NULL, t1666_FIs, NULL, &t1090_TI, NULL, NULL, &t1666_TI, NULL, t1666_VT, &t1666__CustomAttributeCache, &t1666_TI, &t1666_0_0_0, &t1666_1_0_0, t1666_IOs, NULL, NULL, t1666_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1666), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif
#include "t1644MD.h"



// Metadata Definition System.PlatformID
extern Il2CppType t44_0_0_1542;
FieldInfo t1644_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1644_TI, offsetof(t1644, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f2_FieldInfo = 
{
	"Win32S", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f3_FieldInfo = 
{
	"Win32Windows", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f4_FieldInfo = 
{
	"Win32NT", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f5_FieldInfo = 
{
	"WinCE", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f6_FieldInfo = 
{
	"Unix", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f7_FieldInfo = 
{
	"Xbox", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1644_0_0_32854;
FieldInfo t1644_f8_FieldInfo = 
{
	"MacOSX", &t1644_0_0_32854, &t1644_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1644_FIs[] =
{
	&t1644_f1_FieldInfo,
	&t1644_f2_FieldInfo,
	&t1644_f3_FieldInfo,
	&t1644_f4_FieldInfo,
	&t1644_f5_FieldInfo,
	&t1644_f6_FieldInfo,
	&t1644_f7_FieldInfo,
	&t1644_f8_FieldInfo,
	NULL
};
static const int32_t t1644_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1644_f2_DefaultValue = 
{
	&t1644_f2_FieldInfo, { (char*)&t1644_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1644_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1644_f3_DefaultValue = 
{
	&t1644_f3_FieldInfo, { (char*)&t1644_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1644_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1644_f4_DefaultValue = 
{
	&t1644_f4_FieldInfo, { (char*)&t1644_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1644_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1644_f5_DefaultValue = 
{
	&t1644_f5_FieldInfo, { (char*)&t1644_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1644_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1644_f6_DefaultValue = 
{
	&t1644_f6_FieldInfo, { (char*)&t1644_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1644_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1644_f7_DefaultValue = 
{
	&t1644_f7_FieldInfo, { (char*)&t1644_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1644_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1644_f8_DefaultValue = 
{
	&t1644_f8_FieldInfo, { (char*)&t1644_f8_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1644_FDVs[] = 
{
	&t1644_f2_DefaultValue,
	&t1644_f3_DefaultValue,
	&t1644_f4_DefaultValue,
	&t1644_f5_DefaultValue,
	&t1644_f6_DefaultValue,
	&t1644_f7_DefaultValue,
	&t1644_f8_DefaultValue,
	NULL
};
static MethodInfo* t1644_MIs[] =
{
	NULL
};
static MethodInfo* t1644_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1644_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1644_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1644__CustomAttributeCache = {
1,
NULL,
&t1644_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1644_1_0_0;
extern CustomAttributesCache t1644__CustomAttributeCache;
TypeInfo t1644_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PlatformID", "System", t1644_MIs, NULL, t1644_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1644_VT, &t1644__CustomAttributeCache, &t44_TI, &t1644_0_0_0, &t1644_1_0_0, t1644_IOs, NULL, NULL, t1644_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1644)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 8, 0, 0, 23, 0, 3};
#include "t1667.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1667_TI;
#include "t1667MD.h"



extern MethodInfo m9494_MI;
 void m9494 (t1667 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2234, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233065), &m2946_MI);
		return;
	}
}
extern MethodInfo m9495_MI;
 void m9495 (t1667 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233065), &m2946_MI);
		return;
	}
}
extern MethodInfo m9496_MI;
 void m9496 (t1667 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.RankException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9494_MI = 
{
	".ctor", (methodPointerType)&m9494, &t1667_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5338, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1667_m9495_ParameterInfos[] = 
{
	{"message", 0, 134224413, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9495_MI = 
{
	".ctor", (methodPointerType)&m9495, &t1667_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1667_m9495_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5339, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1667_m9496_ParameterInfos[] = 
{
	{"info", 0, 134224414, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224415, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9496_MI = 
{
	".ctor", (methodPointerType)&m9496, &t1667_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1667_m9496_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5340, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1667_MIs[] =
{
	&m9494_MI,
	&m9495_MI,
	&m9496_MI,
	NULL
};
static MethodInfo* t1667_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1667_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1667__CustomAttributeCache = {
1,
NULL,
&t1667_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1667_0_0_0;
extern Il2CppType t1667_1_0_0;
struct t1667;
extern CustomAttributesCache t1667__CustomAttributeCache;
TypeInfo t1667_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RankException", "System", t1667_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t1667_TI, NULL, t1667_VT, &t1667__CustomAttributeCache, &t1667_TI, &t1667_0_0_0, &t1667_1_0_0, t1667_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1667), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1668.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1668_TI;
#include "t1668MD.h"



// Metadata Definition System.ResolveEventArgs
static MethodInfo* t1668_MIs[] =
{
	NULL
};
static MethodInfo* t1668_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1668_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1668__CustomAttributeCache = {
1,
NULL,
&t1668_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1668_0_0_0;
extern Il2CppType t1668_1_0_0;
extern TypeInfo t999_TI;
struct t1668;
extern CustomAttributesCache t1668__CustomAttributeCache;
TypeInfo t1668_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ResolveEventArgs", "System", t1668_MIs, NULL, NULL, NULL, &t999_TI, NULL, NULL, &t1668_TI, NULL, t1668_VT, &t1668__CustomAttributeCache, &t1668_TI, &t1668_0_0_0, &t1668_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1668), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1343.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1343_TI;
#include "t1343MD.h"

#include "t35.h"
#include "t917.h"
extern TypeInfo t35_TI;
extern TypeInfo t917_TI;
#include "t35MD.h"
#include "t917MD.h"
extern Il2CppType t1377_0_0_0;
extern MethodInfo m7628_MI;
extern MethodInfo m9499_MI;
extern MethodInfo m2949_MI;
extern MethodInfo m3986_MI;
extern MethodInfo m7544_MI;
extern MethodInfo m3982_MI;
extern MethodInfo m5813_MI;


extern MethodInfo m9497_MI;
 void m9497 (t1343 * __this, t35 p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m9498_MI;
 void m9498 (t1343 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t1377 * V_0 = {0};
	t1343  V_1 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t1377_0_0_0), &m1554_MI);
		t29 * L_2 = m3985(p0, (t7*) &_stringLiteral2235, L_1, &m3985_MI);
		V_0 = ((t1377 *)Castclass(L_2, InitializedTypeInfo(&t1377_TI)));
		t1343  L_3 = (t1343 )VirtFuncInvoker0< t1343  >::Invoke(&m7628_MI, V_0);
		V_1 = L_3;
		t35 L_4 = m9499((&V_1), &m9499_MI);
		__this->f0 = L_4;
		t35 L_5 = (__this->f0);
		bool L_6 = m2949(NULL, L_5, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2949_MI);
		if (!L_6)
		{
			goto IL_005f;
		}
	}
	{
		t7* L_7 = m6079(NULL, (t7*) &_stringLiteral1072, &m6079_MI);
		t917 * L_8 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_8, L_7, &m3986_MI);
		il2cpp_codegen_raise_exception(L_8);
	}

IL_005f:
	{
		return;
	}
}
 t35 m9499 (t1343 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m9500_MI;
 void m9500 (t1343 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		t35 L_1 = (__this->f0);
		bool L_2 = m2949(NULL, L_1, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2949_MI);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		t917 * L_3 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_3, (t7*) &_stringLiteral1073, &m3986_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_002b:
	{
		t636 * L_4 = m7544(NULL, (*(t1343 *)__this), &m7544_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t1377_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral2235, ((t1377 *)Castclass(L_4, InitializedTypeInfo(&t1377_TI))), L_5, &m3982_MI);
		return;
	}
}
extern MethodInfo m9501_MI;
 bool m9501 (t1343 * __this, t29 * p0, MethodInfo* method){
	t1343  V_0 = {0};
	{
		if (!p0)
		{
			goto IL_001b;
		}
	}
	{
		t1343  L_0 = (*(t1343 *)__this);
		t29 * L_1 = Box(InitializedTypeInfo(&t1343_TI), &L_0);
		t42 * L_2 = m1430(L_1, &m1430_MI);
		t42 * L_3 = m1430(p0, &m1430_MI);
		if ((((t42 *)L_2) == ((t42 *)L_3)))
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return 0;
	}

IL_001d:
	{
		t35 L_4 = (__this->f0);
		V_0 = ((*(t1343 *)((t1343 *)UnBox (p0, InitializedTypeInfo(&t1343_TI)))));
		t35 L_5 = m9499((&V_0), &m9499_MI);
		bool L_6 = m2949(NULL, L_4, L_5, &m2949_MI);
		return L_6;
	}
}
extern MethodInfo m9502_MI;
 int32_t m9502 (t1343 * __this, MethodInfo* method){
	{
		t35* L_0 = &(__this->f0);
		int32_t L_1 = m5813(L_0, &m5813_MI);
		return L_1;
	}
}
// Metadata Definition System.RuntimeMethodHandle
extern Il2CppType t35_0_0_1;
FieldInfo t1343_f0_FieldInfo = 
{
	"value", &t35_0_0_1, &t1343_TI, offsetof(t1343, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t1343_FIs[] =
{
	&t1343_f0_FieldInfo,
	NULL
};
static PropertyInfo t1343____Value_PropertyInfo = 
{
	&t1343_TI, "Value", &m9499_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1343_PIs[] =
{
	&t1343____Value_PropertyInfo,
	NULL
};
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1343_m9497_ParameterInfos[] = 
{
	{"v", 0, 134224416, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9497_MI = 
{
	".ctor", (methodPointerType)&m9497, &t1343_TI, &t21_0_0_0, RuntimeInvoker_t21_t35, t1343_m9497_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, false, 5341, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1343_m9498_ParameterInfos[] = 
{
	{"info", 0, 134224417, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224418, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9498_MI = 
{
	".ctor", (methodPointerType)&m9498, &t1343_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1343_m9498_ParameterInfos, &EmptyCustomAttributesCache, 6273, 0, 255, 2, false, false, 5342, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern void* RuntimeInvoker_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9499_MI = 
{
	"get_Value", (methodPointerType)&m9499, &t1343_TI, &t35_0_0_0, RuntimeInvoker_t35, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5343, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1343_m9500_ParameterInfos[] = 
{
	{"info", 0, 134224419, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224420, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9500_MI = 
{
	"GetObjectData", (methodPointerType)&m9500, &t1343_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1343_m9500_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 2, false, false, 5344, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1343_m9501_ParameterInfos[] = 
{
	{"obj", 0, 134224421, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1343__CustomAttributeCache_m9501;
MethodInfo m9501_MI = 
{
	"Equals", (methodPointerType)&m9501, &t1343_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1343_m9501_ParameterInfos, &t1343__CustomAttributeCache_m9501, 198, 0, 0, 1, false, false, 5345, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9502_MI = 
{
	"GetHashCode", (methodPointerType)&m9502, &t1343_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 5346, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1343_MIs[] =
{
	&m9497_MI,
	&m9498_MI,
	&m9499_MI,
	&m9500_MI,
	&m9501_MI,
	&m9502_MI,
	NULL
};
extern MethodInfo m1500_MI;
static MethodInfo* t1343_VT[] =
{
	&m9501_MI,
	&m46_MI,
	&m9502_MI,
	&m1500_MI,
	&m9500_MI,
};
static TypeInfo* t1343_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1343_IOs[] = 
{
	{ &t374_TI, 4},
};
extern TypeInfo t1168_TI;
#include "t1168.h"
#include "t1168MD.h"
extern MethodInfo m6082_MI;
void t1343_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1168 * tmp;
		tmp = (t1168 *)il2cpp_codegen_object_new (&t1168_TI);
		m6082(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), &m6082_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t1343_CustomAttributesCacheGenerator_m9501(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1343__CustomAttributeCache = {
2,
NULL,
&t1343_CustomAttributesCacheGenerator
};
CustomAttributesCache t1343__CustomAttributeCache_m9501 = {
1,
NULL,
&t1343_CustomAttributesCacheGenerator_m9501
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1343_0_0_0;
extern Il2CppType t1343_1_0_0;
extern TypeInfo t110_TI;
extern CustomAttributesCache t1343__CustomAttributeCache;
extern CustomAttributesCache t1343__CustomAttributeCache_m9501;
TypeInfo t1343_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RuntimeMethodHandle", "System", t1343_MIs, t1343_PIs, t1343_FIs, NULL, &t110_TI, NULL, NULL, &t1343_TI, t1343_ITIs, t1343_VT, &t1343__CustomAttributeCache, &t1343_TI, &t1343_0_0_0, &t1343_1_0_0, t1343_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1343)+ sizeof (Il2CppObject), 0, sizeof(t1343 ), 0, 0, -1, 1057033, 0, true, false, false, false, false, false, false, false, false, false, true, true, 6, 1, 1, 0, 0, 5, 1, 1};
#include "t591.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t591_TI;
#include "t591MD.h"

#include "t1669.h"
#include "t1670.h"
extern TypeInfo t1669_TI;
extern TypeInfo t1670_TI;
#include "t1669MD.h"
#include "t1670MD.h"
extern MethodInfo m4009_MI;
extern MethodInfo m9508_MI;
extern MethodInfo m9512_MI;
extern MethodInfo m10265_MI;
extern MethodInfo m9672_MI;
extern MethodInfo m10266_MI;
extern MethodInfo m10267_MI;


extern MethodInfo m9503_MI;
 void m9503 (t591 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m9504_MI;
 void m9504 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_0 = m4009(NULL, &m4009_MI);
		t1669 * L_1 = (t1669 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1669_TI));
		m9508(L_1, L_0, 1, &m9508_MI);
		((t591_SFs*)InitializedTypeInfo(&t591_TI)->static_fields)->f0 = L_1;
		t633 * L_2 = m4009(NULL, &m4009_MI);
		t1669 * L_3 = (t1669 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1669_TI));
		m9508(L_3, L_2, 0, &m9508_MI);
		((t591_SFs*)InitializedTypeInfo(&t591_TI)->static_fields)->f1 = L_3;
		t1670 * L_4 = (t1670 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1670_TI));
		m9512(L_4, 1, &m9512_MI);
		((t591_SFs*)InitializedTypeInfo(&t591_TI)->static_fields)->f2 = L_4;
		t1670 * L_5 = (t1670 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1670_TI));
		m9512(L_5, 0, &m9512_MI);
		((t591_SFs*)InitializedTypeInfo(&t591_TI)->static_fields)->f3 = L_5;
		return;
	}
}
extern MethodInfo m4027_MI;
 t591 * m4027 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t591_TI));
		return (((t591_SFs*)InitializedTypeInfo(&t591_TI)->static_fields)->f0);
	}
}
extern MethodInfo m2862_MI;
 t591 * m2862 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t591_TI));
		return (((t591_SFs*)InitializedTypeInfo(&t591_TI)->static_fields)->f2);
	}
}
extern MethodInfo m9505_MI;
 int32_t m9505 (t591 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	t7* V_1 = {0};
	t29 * V_2 = {0};
	{
		if ((((t29 *)p0) != ((t29 *)p1)))
		{
			goto IL_0006;
		}
	}
	{
		return 0;
	}

IL_0006:
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		V_0 = ((t7*)IsInst(p0, (&t7_TI)));
		if (!V_0)
		{
			goto IL_002d;
		}
	}
	{
		V_1 = ((t7*)IsInst(p1, (&t7_TI)));
		if (!V_1)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t7*, t7* >::Invoke(&m10265_MI, __this, V_0, V_1);
		return L_0;
	}

IL_002d:
	{
		V_2 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t290_TI)));
		if (V_2)
		{
			goto IL_003d;
		}
	}
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_003d:
	{
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, V_2, p1);
		return L_2;
	}
}
extern MethodInfo m9506_MI;
 bool m9506 (t591 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	t7* V_1 = {0};
	{
		if ((((t29 *)p0) != ((t29 *)p1)))
		{
			goto IL_0006;
		}
	}
	{
		return 1;
	}

IL_0006:
	{
		if (!p0)
		{
			goto IL_000c;
		}
	}
	{
		if (p1)
		{
			goto IL_000e;
		}
	}

IL_000c:
	{
		return 0;
	}

IL_000e:
	{
		V_0 = ((t7*)IsInst(p0, (&t7_TI)));
		if (!V_0)
		{
			goto IL_002b;
		}
	}
	{
		V_1 = ((t7*)IsInst(p1, (&t7_TI)));
		if (!V_1)
		{
			goto IL_002b;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t7*, t7* >::Invoke(&m10266_MI, __this, V_0, V_1);
		return L_0;
	}

IL_002b:
	{
		bool L_1 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, p0, p1);
		return L_1;
	}
}
extern MethodInfo m9507_MI;
 int32_t m9507 (t591 * __this, t29 * p0, MethodInfo* method){
	t7* V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1199, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		V_0 = ((t7*)IsInst(p0, (&t7_TI)));
		if (V_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, p0);
		G_B5_0 = L_1;
		goto IL_0027;
	}

IL_0020:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t7* >::Invoke(&m10267_MI, __this, V_0);
		G_B5_0 = L_2;
	}

IL_0027:
	{
		return G_B5_0;
	}
}
// Metadata Definition System.StringComparer
extern Il2CppType t591_0_0_17;
FieldInfo t591_f0_FieldInfo = 
{
	"invariantCultureIgnoreCase", &t591_0_0_17, &t591_TI, offsetof(t591_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t591_0_0_17;
FieldInfo t591_f1_FieldInfo = 
{
	"invariantCulture", &t591_0_0_17, &t591_TI, offsetof(t591_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t591_0_0_17;
FieldInfo t591_f2_FieldInfo = 
{
	"ordinalIgnoreCase", &t591_0_0_17, &t591_TI, offsetof(t591_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t591_0_0_17;
FieldInfo t591_f3_FieldInfo = 
{
	"ordinal", &t591_0_0_17, &t591_TI, offsetof(t591_SFs, f3), &EmptyCustomAttributesCache};
static FieldInfo* t591_FIs[] =
{
	&t591_f0_FieldInfo,
	&t591_f1_FieldInfo,
	&t591_f2_FieldInfo,
	&t591_f3_FieldInfo,
	NULL
};
static PropertyInfo t591____InvariantCultureIgnoreCase_PropertyInfo = 
{
	&t591_TI, "InvariantCultureIgnoreCase", &m4027_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t591____OrdinalIgnoreCase_PropertyInfo = 
{
	&t591_TI, "OrdinalIgnoreCase", &m2862_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t591_PIs[] =
{
	&t591____InvariantCultureIgnoreCase_PropertyInfo,
	&t591____OrdinalIgnoreCase_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9503_MI = 
{
	".ctor", (methodPointerType)&m9503, &t591_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 5347, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9504_MI = 
{
	".cctor", (methodPointerType)&m9504, &t591_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 5348, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t591_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4027_MI = 
{
	"get_InvariantCultureIgnoreCase", (methodPointerType)&m4027, &t591_TI, &t591_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 5349, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t591_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2862_MI = 
{
	"get_OrdinalIgnoreCase", (methodPointerType)&m2862, &t591_TI, &t591_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 5350, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t591_m9505_ParameterInfos[] = 
{
	{"x", 0, 134224422, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134224423, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9505_MI = 
{
	"Compare", (methodPointerType)&m9505, &t591_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t591_m9505_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 2, false, false, 5351, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t591_m9506_ParameterInfos[] = 
{
	{"x", 0, 134224424, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134224425, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9506_MI = 
{
	"Equals", (methodPointerType)&m9506, &t591_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t591_m9506_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 8, 2, false, false, 5352, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t591_m9507_ParameterInfos[] = 
{
	{"obj", 0, 134224426, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9507_MI = 
{
	"GetHashCode", (methodPointerType)&m9507, &t591_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t591_m9507_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 9, 1, false, false, 5353, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t591_m10265_ParameterInfos[] = 
{
	{"x", 0, 134224427, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134224428, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10265_MI = 
{
	"Compare", NULL, &t591_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t591_m10265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 10, 2, false, false, 5354, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t591_m10266_ParameterInfos[] = 
{
	{"x", 0, 134224429, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134224430, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10266_MI = 
{
	"Equals", NULL, &t591_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t591_m10266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 11, 2, false, false, 5355, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t591_m10267_ParameterInfos[] = 
{
	{"obj", 0, 134224431, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10267_MI = 
{
	"GetHashCode", NULL, &t591_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t591_m10267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 12, 1, false, false, 5356, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t591_MIs[] =
{
	&m9503_MI,
	&m9504_MI,
	&m4027_MI,
	&m2862_MI,
	&m9505_MI,
	&m9506_MI,
	&m9507_MI,
	&m10265_MI,
	&m10266_MI,
	&m10267_MI,
	NULL
};
static MethodInfo* t591_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10265_MI,
	&m10266_MI,
	&m10267_MI,
	&m9505_MI,
	&m9506_MI,
	&m9507_MI,
	NULL,
	NULL,
	NULL,
};
extern TypeInfo t2095_TI;
extern TypeInfo t2096_TI;
extern TypeInfo t726_TI;
extern TypeInfo t734_TI;
static TypeInfo* t591_ITIs[] = 
{
	&t2095_TI,
	&t2096_TI,
	&t726_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t591_IOs[] = 
{
	{ &t2095_TI, 4},
	{ &t2096_TI, 5},
	{ &t726_TI, 7},
	{ &t734_TI, 8},
};
void t591_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t591__CustomAttributeCache = {
1,
NULL,
&t591_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t591_0_0_0;
extern Il2CppType t591_1_0_0;
struct t591;
extern CustomAttributesCache t591__CustomAttributeCache;
TypeInfo t591_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringComparer", "System", t591_MIs, t591_PIs, t591_FIs, NULL, &t29_TI, NULL, NULL, &t591_TI, t591_ITIs, t591_VT, &t591__CustomAttributeCache, &t591_TI, &t591_0_0_0, &t591_1_0_0, t591_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t591), 0, -1, sizeof(t591_SFs), 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, true, false, false, 10, 2, 4, 0, 0, 13, 4, 4};
#ifndef _MSC_VER
#else
#endif

#include "t1119.h"
#include "t1120.h"
#include "t1192.h"
extern TypeInfo t1119_TI;
extern TypeInfo t1120_TI;
extern TypeInfo t1192_TI;
#include "t1119MD.h"
#include "t1192MD.h"
extern MethodInfo m5213_MI;
extern MethodInfo m5214_MI;
extern MethodInfo m9509_MI;
extern MethodInfo m6825_MI;
extern MethodInfo m6181_MI;


 void m9508 (t1669 * __this, t633 * p0, bool p1, MethodInfo* method){
	{
		m9503(__this, &m9503_MI);
		t1119 * L_0 = (t1119 *)VirtFuncInvoker0< t1119 * >::Invoke(&m5213_MI, p0);
		__this->f5 = L_0;
		__this->f4 = p1;
		return;
	}
}
 int32_t m9509 (t1669 * __this, t7* p0, t7* p1, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (__this->f4);
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_000c;
	}

IL_000b:
	{
		G_B3_0 = 0;
	}

IL_000c:
	{
		V_0 = G_B3_0;
		t1119 * L_1 = (__this->f5);
		int32_t L_2 = (int32_t)VirtFuncInvoker3< int32_t, t7*, t7*, int32_t >::Invoke(&m5214_MI, L_1, p0, p1, V_0);
		return L_2;
	}
}
extern MethodInfo m9510_MI;
 bool m9510 (t1669 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t7*, t7* >::Invoke(&m9509_MI, __this, p0, p1);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m9511_MI;
 int32_t m9511 (t1669 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral868, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		bool L_1 = (__this->f4);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_001a;
	}

IL_0019:
	{
		G_B5_0 = 0;
	}

IL_001a:
	{
		V_0 = G_B5_0;
		t1119 * L_2 = (__this->f5);
		t1192 * L_3 = (t1192 *)VirtFuncInvoker2< t1192 *, t7*, int32_t >::Invoke(&m6825_MI, L_2, p0, V_0);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6181_MI, L_3);
		return L_4;
	}
}
// Metadata Definition System.CultureAwareComparer
extern Il2CppType t40_0_0_33;
FieldInfo t1669_f4_FieldInfo = 
{
	"_ignoreCase", &t40_0_0_33, &t1669_TI, offsetof(t1669, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1119_0_0_33;
FieldInfo t1669_f5_FieldInfo = 
{
	"_compareInfo", &t1119_0_0_33, &t1669_TI, offsetof(t1669, f5), &EmptyCustomAttributesCache};
static FieldInfo* t1669_FIs[] =
{
	&t1669_f4_FieldInfo,
	&t1669_f5_FieldInfo,
	NULL
};
extern Il2CppType t633_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1669_m9508_ParameterInfos[] = 
{
	{"ci", 0, 134224432, &EmptyCustomAttributesCache, &t633_0_0_0},
	{"ignore_case", 1, 134224433, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9508_MI = 
{
	".ctor", (methodPointerType)&m9508, &t1669_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t1669_m9508_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5357, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1669_m9509_ParameterInfos[] = 
{
	{"x", 0, 134224434, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134224435, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9509_MI = 
{
	"Compare", (methodPointerType)&m9509, &t1669_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t1669_m9509_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 10, 2, false, false, 5358, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1669_m9510_ParameterInfos[] = 
{
	{"x", 0, 134224436, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134224437, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9510_MI = 
{
	"Equals", (methodPointerType)&m9510, &t1669_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1669_m9510_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 5359, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1669_m9511_ParameterInfos[] = 
{
	{"s", 0, 134224438, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9511_MI = 
{
	"GetHashCode", (methodPointerType)&m9511, &t1669_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1669_m9511_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 5360, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1669_MIs[] =
{
	&m9508_MI,
	&m9509_MI,
	&m9510_MI,
	&m9511_MI,
	NULL
};
static MethodInfo* t1669_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m9509_MI,
	&m9510_MI,
	&m9511_MI,
	&m9505_MI,
	&m9506_MI,
	&m9507_MI,
	&m9509_MI,
	&m9510_MI,
	&m9511_MI,
};
static Il2CppInterfaceOffsetPair t1669_IOs[] = 
{
	{ &t2095_TI, 4},
	{ &t2096_TI, 5},
	{ &t726_TI, 7},
	{ &t734_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1669_0_0_0;
extern Il2CppType t1669_1_0_0;
struct t1669;
TypeInfo t1669_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CultureAwareComparer", "System", t1669_MIs, NULL, t1669_FIs, NULL, &t591_TI, NULL, NULL, &t1669_TI, NULL, t1669_VT, &EmptyCustomAttributesCache, &t1669_TI, &t1669_0_0_0, &t1669_1_0_0, t1669_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1669), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 2, 0, 0, 13, 0, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m5591_MI;
extern MethodInfo m5590_MI;
extern MethodInfo m9513_MI;
extern MethodInfo m5621_MI;
extern MethodInfo m2843_MI;


 void m9512 (t1670 * __this, bool p0, MethodInfo* method){
	{
		m9503(__this, &m9503_MI);
		__this->f4 = p0;
		return;
	}
}
 int32_t m9513 (t1670 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		bool L_0 = (__this->f4);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		int32_t L_1 = m5591(NULL, p0, 0, ((int32_t)2147483647), p1, 0, ((int32_t)2147483647), &m5591_MI);
		return L_1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		int32_t L_2 = m5590(NULL, p0, 0, ((int32_t)2147483647), p1, 0, ((int32_t)2147483647), &m5590_MI);
		return L_2;
	}
}
extern MethodInfo m9514_MI;
 bool m9514 (t1670 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		bool L_0 = (__this->f4);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = (int32_t)VirtFuncInvoker2< int32_t, t7*, t7* >::Invoke(&m9513_MI, __this, p0, p1);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, p0, p1, &m1713_MI);
		return L_2;
	}
}
extern MethodInfo m9515_MI;
 int32_t m9515 (t1670 * __this, t7* p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral868, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		bool L_1 = (__this->f4);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = m5621(p0, &m5621_MI);
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, p0);
		return L_3;
	}
}
// Metadata Definition System.OrdinalComparer
extern Il2CppType t40_0_0_33;
FieldInfo t1670_f4_FieldInfo = 
{
	"_ignoreCase", &t40_0_0_33, &t1670_TI, offsetof(t1670, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1670_FIs[] =
{
	&t1670_f4_FieldInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1670_m9512_ParameterInfos[] = 
{
	{"ignoreCase", 0, 134224439, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9512_MI = 
{
	".ctor", (methodPointerType)&m9512, &t1670_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1670_m9512_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5361, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1670_m9513_ParameterInfos[] = 
{
	{"x", 0, 134224440, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134224441, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9513_MI = 
{
	"Compare", (methodPointerType)&m9513, &t1670_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t1670_m9513_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 10, 2, false, false, 5362, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1670_m9514_ParameterInfos[] = 
{
	{"x", 0, 134224442, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134224443, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9514_MI = 
{
	"Equals", (methodPointerType)&m9514, &t1670_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1670_m9514_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 5363, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1670_m9515_ParameterInfos[] = 
{
	{"s", 0, 134224444, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9515_MI = 
{
	"GetHashCode", (methodPointerType)&m9515, &t1670_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1670_m9515_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 5364, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1670_MIs[] =
{
	&m9512_MI,
	&m9513_MI,
	&m9514_MI,
	&m9515_MI,
	NULL
};
static MethodInfo* t1670_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m9513_MI,
	&m9514_MI,
	&m9515_MI,
	&m9505_MI,
	&m9506_MI,
	&m9507_MI,
	&m9513_MI,
	&m9514_MI,
	&m9515_MI,
};
static Il2CppInterfaceOffsetPair t1670_IOs[] = 
{
	{ &t2095_TI, 4},
	{ &t2096_TI, 5},
	{ &t726_TI, 7},
	{ &t734_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1670_0_0_0;
extern Il2CppType t1670_1_0_0;
struct t1670;
TypeInfo t1670_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OrdinalComparer", "System", t1670_MIs, NULL, t1670_FIs, NULL, &t591_TI, NULL, NULL, &t1670_TI, NULL, t1670_VT, &EmptyCustomAttributesCache, &t1670_TI, &t1670_0_0_0, &t1670_1_0_0, t1670_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1670), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 13, 0, 4};
#include "t947.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t947_TI;
#include "t947MD.h"



// Metadata Definition System.StringComparison
extern Il2CppType t44_0_0_1542;
FieldInfo t947_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t947_TI, offsetof(t947, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t947_0_0_32854;
FieldInfo t947_f2_FieldInfo = 
{
	"CurrentCulture", &t947_0_0_32854, &t947_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t947_0_0_32854;
FieldInfo t947_f3_FieldInfo = 
{
	"CurrentCultureIgnoreCase", &t947_0_0_32854, &t947_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t947_0_0_32854;
FieldInfo t947_f4_FieldInfo = 
{
	"InvariantCulture", &t947_0_0_32854, &t947_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t947_0_0_32854;
FieldInfo t947_f5_FieldInfo = 
{
	"InvariantCultureIgnoreCase", &t947_0_0_32854, &t947_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t947_0_0_32854;
FieldInfo t947_f6_FieldInfo = 
{
	"Ordinal", &t947_0_0_32854, &t947_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t947_0_0_32854;
FieldInfo t947_f7_FieldInfo = 
{
	"OrdinalIgnoreCase", &t947_0_0_32854, &t947_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t947_FIs[] =
{
	&t947_f1_FieldInfo,
	&t947_f2_FieldInfo,
	&t947_f3_FieldInfo,
	&t947_f4_FieldInfo,
	&t947_f5_FieldInfo,
	&t947_f6_FieldInfo,
	&t947_f7_FieldInfo,
	NULL
};
static const int32_t t947_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t947_f2_DefaultValue = 
{
	&t947_f2_FieldInfo, { (char*)&t947_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t947_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t947_f3_DefaultValue = 
{
	&t947_f3_FieldInfo, { (char*)&t947_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t947_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t947_f4_DefaultValue = 
{
	&t947_f4_FieldInfo, { (char*)&t947_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t947_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t947_f5_DefaultValue = 
{
	&t947_f5_FieldInfo, { (char*)&t947_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t947_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t947_f6_DefaultValue = 
{
	&t947_f6_FieldInfo, { (char*)&t947_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t947_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t947_f7_DefaultValue = 
{
	&t947_f7_FieldInfo, { (char*)&t947_f7_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t947_FDVs[] = 
{
	&t947_f2_DefaultValue,
	&t947_f3_DefaultValue,
	&t947_f4_DefaultValue,
	&t947_f5_DefaultValue,
	&t947_f6_DefaultValue,
	&t947_f7_DefaultValue,
	NULL
};
static MethodInfo* t947_MIs[] =
{
	NULL
};
static MethodInfo* t947_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t947_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t947_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t947__CustomAttributeCache = {
1,
NULL,
&t947_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t947_0_0_0;
extern Il2CppType t947_1_0_0;
extern CustomAttributesCache t947__CustomAttributeCache;
TypeInfo t947_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringComparison", "System", t947_MIs, NULL, t947_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t947_VT, &t947__CustomAttributeCache, &t44_TI, &t947_0_0_0, &t947_1_0_0, t947_IOs, NULL, NULL, t947_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t947)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 7, 0, 0, 23, 0, 3};
#include "t939.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t939_TI;
#include "t939MD.h"



// Metadata Definition System.StringSplitOptions
extern Il2CppType t44_0_0_1542;
FieldInfo t939_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t939_TI, offsetof(t939, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t939_0_0_32854;
FieldInfo t939_f2_FieldInfo = 
{
	"None", &t939_0_0_32854, &t939_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t939_0_0_32854;
FieldInfo t939_f3_FieldInfo = 
{
	"RemoveEmptyEntries", &t939_0_0_32854, &t939_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t939_FIs[] =
{
	&t939_f1_FieldInfo,
	&t939_f2_FieldInfo,
	&t939_f3_FieldInfo,
	NULL
};
static const int32_t t939_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t939_f2_DefaultValue = 
{
	&t939_f2_FieldInfo, { (char*)&t939_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t939_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t939_f3_DefaultValue = 
{
	&t939_f3_FieldInfo, { (char*)&t939_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t939_FDVs[] = 
{
	&t939_f2_DefaultValue,
	&t939_f3_DefaultValue,
	NULL
};
static MethodInfo* t939_MIs[] =
{
	NULL
};
static MethodInfo* t939_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t939_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern TypeInfo t291_TI;
#include "t291.h"
#include "t291MD.h"
extern MethodInfo m1270_MI;
void t939_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, false, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t939__CustomAttributeCache = {
2,
NULL,
&t939_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t939_0_0_0;
extern Il2CppType t939_1_0_0;
extern CustomAttributesCache t939__CustomAttributeCache;
TypeInfo t939_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringSplitOptions", "System", t939_MIs, NULL, t939_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t939_VT, &t939__CustomAttributeCache, &t44_TI, &t939_0_0_0, &t939_1_0_0, t939_IOs, NULL, NULL, t939_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t939)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t956.h"
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m2945_MI;
extern MethodInfo m5271_MI;
extern MethodInfo m2947_MI;


extern MethodInfo m9516_MI;
 void m9516 (t956 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2236, &m6079_MI);
		m2945(__this, L_0, &m2945_MI);
		m2946(__this, ((int32_t)-2146233087), &m2946_MI);
		return;
	}
}
 void m4202 (t956 * __this, t7* p0, MethodInfo* method){
	{
		m2945(__this, p0, &m2945_MI);
		m2946(__this, ((int32_t)-2146233087), &m2946_MI);
		return;
	}
}
 void m9517 (t956 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m5271(__this, p0, p1, &m5271_MI);
		return;
	}
}
 void m9518 (t956 * __this, t7* p0, t295 * p1, MethodInfo* method){
	{
		m2947(__this, p0, p1, &m2947_MI);
		m2946(__this, ((int32_t)-2146233087), &m2946_MI);
		return;
	}
}
// Metadata Definition System.SystemException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9516_MI = 
{
	".ctor", (methodPointerType)&m9516, &t956_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5365, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t956_m4202_ParameterInfos[] = 
{
	{"message", 0, 134224445, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4202_MI = 
{
	".ctor", (methodPointerType)&m4202, &t956_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t956_m4202_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5366, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t956_m9517_ParameterInfos[] = 
{
	{"info", 0, 134224446, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224447, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9517_MI = 
{
	".ctor", (methodPointerType)&m9517, &t956_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t956_m9517_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5367, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t295_0_0_0;
static ParameterInfo t956_m9518_ParameterInfos[] = 
{
	{"message", 0, 134224448, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"innerException", 1, 134224449, &EmptyCustomAttributesCache, &t295_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9518_MI = 
{
	".ctor", (methodPointerType)&m9518, &t956_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t956_m9518_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5368, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t956_MIs[] =
{
	&m9516_MI,
	&m4202_MI,
	&m9517_MI,
	&m9518_MI,
	NULL
};
static MethodInfo* t956_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t956_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t956_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t956__CustomAttributeCache = {
1,
NULL,
&t956_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t956_0_0_0;
extern Il2CppType t956_1_0_0;
struct t956;
extern CustomAttributesCache t956__CustomAttributeCache;
TypeInfo t956_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SystemException", "System", t956_MIs, NULL, NULL, NULL, &t295_TI, NULL, NULL, &t956_TI, NULL, t956_VT, &t956__CustomAttributeCache, &t956_TI, &t956_0_0_0, &t956_1_0_0, t956_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t956), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m9519 (t1671 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition System.ThreadStaticAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9519_MI = 
{
	".ctor", (methodPointerType)&m9519, &t1671_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5369, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1671_MIs[] =
{
	&m9519_MI,
	NULL
};
static MethodInfo* t1671_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1671_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1671__CustomAttributeCache = {
2,
NULL,
&t1671_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1671_0_0_0;
extern Il2CppType t1671_1_0_0;
struct t1671;
extern CustomAttributesCache t1671__CustomAttributeCache;
TypeInfo t1671_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ThreadStaticAttribute", "System", t1671_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t1671_TI, NULL, t1671_VT, &t1671__CustomAttributeCache, &t1671_TI, &t1671_0_0_0, &t1671_1_0_0, t1671_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1671), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t816.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t816_TI;
#include "t816MD.h"

#include "t2097.h"
#include "t2098.h"
#include "t915.h"
extern TypeInfo t2097_TI;
extern TypeInfo t2098_TI;
extern TypeInfo t915_TI;
extern TypeInfo t919_TI;
#include "t2097MD.h"
#include "t2098MD.h"
#include "t915MD.h"
#include "t919MD.h"
#include "t44MD.h"
extern MethodInfo m9524_MI;
extern MethodInfo m9520_MI;
extern MethodInfo m10268_MI;
extern MethodInfo m10269_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m9530_MI;
extern MethodInfo m9537_MI;
extern MethodInfo m9544_MI;
extern MethodInfo m5685_MI;
extern MethodInfo m5686_MI;
extern MethodInfo m5687_MI;
extern MethodInfo m5348_MI;
extern MethodInfo m9525_MI;
extern MethodInfo m4077_MI;
extern MethodInfo m9526_MI;
extern MethodInfo m4197_MI;
extern MethodInfo m9528_MI;
extern MethodInfo m9529_MI;
extern MethodInfo m9536_MI;
extern MethodInfo m9547_MI;


 void m9520 (t816 * __this, int64_t p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m9521_MI;
 void m9521 (t816 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		int64_t L_0 = m9524(NULL, 0, p0, p1, p2, 0, &m9524_MI);
		__this->f3 = L_0;
		return;
	}
}
extern MethodInfo m9522_MI;
 void m9522 (t816 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		int64_t L_0 = m9524(NULL, p0, p1, p2, p3, p4, &m9524_MI);
		__this->f3 = L_0;
		return;
	}
}
extern MethodInfo m9523_MI;
 void m9523 (t29 * __this, MethodInfo* method){
	t2097 * V_0 = {0};
	t2098 * V_1 = {0};
	{
		t816  L_0 = {0};
		m9520(&L_0, ((int64_t)std::numeric_limits<int64_t>::max()), &m9520_MI);
		((t816_SFs*)InitializedTypeInfo(&t816_TI)->static_fields)->f0 = L_0;
		t816  L_1 = {0};
		m9520(&L_1, ((int64_t)std::numeric_limits<int64_t>::min()), &m9520_MI);
		((t816_SFs*)InitializedTypeInfo(&t816_TI)->static_fields)->f1 = L_1;
		t816  L_2 = {0};
		m9520(&L_2, (((int64_t)0)), &m9520_MI);
		((t816_SFs*)InitializedTypeInfo(&t816_TI)->static_fields)->f2 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1658_TI));
		if (!(((t1658_SFs*)InitializedTypeInfo(&t1658_TI)->static_fields)->f0))
		{
			goto IL_0045;
		}
	}
	{
		t2097 * L_3 = (t2097 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2097_TI));
		m10268(L_3, &m10268_MI);
		V_0 = L_3;
		t2098 * L_4 = (t2098 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2098_TI));
		m10269(L_4, &m10269_MI);
		V_1 = L_4;
	}

IL_0045:
	{
		return;
	}
}
 int64_t m9524 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	bool V_3 = false;
	int64_t V_4 = 0;
	int64_t V_5 = 0;
	int64_t V_6 = 0;
	int64_t V_7 = 0;
	{
		V_0 = ((int32_t)((int32_t)p1*(int32_t)((int32_t)3600)));
		V_1 = ((int32_t)((int32_t)p2*(int32_t)((int32_t)60)));
		V_2 = ((int64_t)(((int64_t)((int64_t)(((int64_t)((int32_t)(((int32_t)(V_0+V_1))+p3))))*(int64_t)(((int64_t)((int32_t)1000)))))+(((int64_t)p4))));
		V_2 = ((int64_t)((int64_t)V_2*(int64_t)(((int64_t)((int32_t)10000)))));
		V_3 = 0;
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		V_4 = ((int64_t)((int64_t)((int64_t)864000000000LL)*(int64_t)(((int64_t)p0))));
		if ((((int64_t)V_2) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_0051;
		}
	}
	{
		V_5 = V_2;
		V_2 = ((int64_t)(V_2+V_4));
		V_3 = ((((int64_t)V_5) > ((int64_t)V_2))? 1 : 0);
		goto IL_005c;
	}

IL_0051:
	{
		V_2 = ((int64_t)(V_2+V_4));
		V_3 = ((((int64_t)V_2) < ((int64_t)(((int64_t)0))))? 1 : 0);
	}

IL_005c:
	{
		goto IL_0090;
	}

IL_005e:
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		V_6 = ((int64_t)((int64_t)((int64_t)864000000000LL)*(int64_t)(((int64_t)p0))));
		if ((((int64_t)V_2) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0082;
		}
	}
	{
		V_2 = ((int64_t)(V_2+V_6));
		V_3 = ((((int64_t)V_2) > ((int64_t)(((int64_t)0))))? 1 : 0);
		goto IL_0090;
	}

IL_0082:
	{
		V_7 = V_2;
		V_2 = ((int64_t)(V_2+V_6));
		V_3 = ((((int64_t)V_2) > ((int64_t)V_7))? 1 : 0);
	}

IL_0090:
	{
		if (!V_3)
		{
			goto IL_00a3;
		}
	}
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2237, &m6079_MI);
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, L_0, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_00a3:
	{
		return V_2;
	}
}
 int32_t m9525 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return (((int32_t)((int64_t)((int64_t)L_0/(int64_t)((int64_t)864000000000LL)))));
	}
}
 int32_t m9526 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return (((int32_t)((int64_t)((int64_t)((int64_t)(L_0%((int64_t)864000000000LL)))/(int64_t)((int64_t)36000000000LL)))));
	}
}
extern MethodInfo m9527_MI;
 int32_t m9527 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return (((int32_t)((int64_t)((int64_t)((int64_t)(L_0%(((int64_t)((int32_t)10000000)))))/(int64_t)(((int64_t)((int32_t)10000)))))));
	}
}
 int32_t m9528 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return (((int32_t)((int64_t)((int64_t)((int64_t)(L_0%((int64_t)36000000000LL)))/(int64_t)(((int64_t)((int32_t)600000000)))))));
	}
}
 int32_t m9529 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return (((int32_t)((int64_t)((int64_t)((int64_t)(L_0%(((int64_t)((int32_t)600000000)))))/(int64_t)(((int64_t)((int32_t)10000000)))))));
	}
}
 int64_t m9530 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m9531_MI;
 double m9531 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return ((double)((double)(((double)L_0))/(double)(864000000000.0)));
	}
}
extern MethodInfo m9532_MI;
 double m9532 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return ((double)((double)(((double)L_0))/(double)(36000000000.0)));
	}
}
extern MethodInfo m9533_MI;
 double m9533 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return ((double)((double)(((double)L_0))/(double)(10000.0)));
	}
}
extern MethodInfo m9534_MI;
 double m9534 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return ((double)((double)(((double)L_0))/(double)(600000000.0)));
	}
}
extern MethodInfo m9535_MI;
 double m9535 (t816 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->f3);
		return ((double)((double)(((double)L_0))/(double)(10000000.0)));
	}
}
 t816  m9536 (t816 * __this, t816  p0, MethodInfo* method){
	t816  V_0 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_0 = (__this->f3);
			int64_t L_1 = m9530((&p0), &m9530_MI);
			t816  L_2 = {0};
			m9520(&L_2, ((int64_t)(L_0+L_1)), &m9520_MI);
			V_0 = L_2;
			// IL_0014: leave.s IL_002b
			goto IL_002b;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_002b
			goto IL_002b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1666_TI, e.ex->object.klass))
			goto IL_0018;
		throw e;
	}

IL_0018:
	{ // begin catch(System.OverflowException)
		t7* L_3 = m6079(NULL, (t7*) &_stringLiteral2238, &m6079_MI);
		t1666 * L_4 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_4, L_3, &m9492_MI);
		il2cpp_codegen_raise_exception(L_4);
		// IL_0029: leave.s IL_002b
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		return V_0;
	}
}
 int32_t m9537 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		if ((((int64_t)L_0) >= ((int64_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		return (-1);
	}

IL_0012:
	{
		int64_t L_2 = ((&p0)->f3);
		int64_t L_3 = ((&p1)->f3);
		if ((((int64_t)L_2) <= ((int64_t)L_3)))
		{
			goto IL_0024;
		}
	}
	{
		return 1;
	}

IL_0024:
	{
		return 0;
	}
}
extern MethodInfo m9538_MI;
 int32_t m9538 (t816 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_0005;
		}
	}
	{
		return 1;
	}

IL_0005:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t816_TI))))
		{
			goto IL_0022;
		}
	}
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2239, &m6079_MI);
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_1, L_0, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		int32_t L_2 = m9537(NULL, (*(t816 *)__this), ((*(t816 *)((t816 *)UnBox (p0, InitializedTypeInfo(&t816_TI))))), &m9537_MI);
		return L_2;
	}
}
extern MethodInfo m9539_MI;
 int32_t m9539 (t816 * __this, t816  p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		int32_t L_0 = m9537(NULL, (*(t816 *)__this), p0, &m9537_MI);
		return L_0;
	}
}
extern MethodInfo m9540_MI;
 bool m9540 (t816 * __this, t816  p0, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = (__this->f3);
		return ((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m9541_MI;
 t816  m9541 (t816 * __this, MethodInfo* method){
	t816  V_0 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_0 = (__this->f3);
			int64_t L_1 = llabs(L_0);
			t816  L_2 = {0};
			m9520(&L_2, L_1, &m9520_MI);
			V_0 = L_2;
			// IL_0011: leave.s IL_0028
			goto IL_0028;
		}

IL_0013:
		{
			// IL_0013: leave.s IL_0028
			goto IL_0028;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1666_TI, e.ex->object.klass))
			goto IL_0015;
		throw e;
	}

IL_0015:
	{ // begin catch(System.OverflowException)
		t7* L_3 = m6079(NULL, (t7*) &_stringLiteral2240, &m6079_MI);
		t1666 * L_4 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_4, L_3, &m9492_MI);
		il2cpp_codegen_raise_exception(L_4);
		// IL_0026: leave.s IL_0028
		goto IL_0028;
	} // end catch (depth: 1)

IL_0028:
	{
		return V_0;
	}
}
extern MethodInfo m9542_MI;
 bool m9542 (t816 * __this, t29 * p0, MethodInfo* method){
	t816  V_0 = {0};
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t816_TI))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int64_t L_0 = (__this->f3);
		V_0 = ((*(t816 *)((t816 *)UnBox (p0, InitializedTypeInfo(&t816_TI)))));
		int64_t L_1 = ((&V_0)->f3);
		return ((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m9543_MI;
 t816  m9543 (t29 * __this, double p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		t816  L_0 = m9544(NULL, p0, (((int64_t)((int32_t)600000000))), &m9544_MI);
		return L_0;
	}
}
 t816  m9544 (t29 * __this, double p0, int64_t p1, MethodInfo* method){
	int64_t V_0 = 0;
	t816  V_1 = {0};
	t816  V_2 = {0};
	t816  V_3 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		bool L_0 = m5685(NULL, p0, &m5685_MI);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral2241, &m6079_MI);
		t305 * L_2 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_2, L_1, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001d:
	{
		bool L_3 = m5686(NULL, p0, &m5686_MI);
		if (L_3)
		{
			goto IL_004f;
		}
	}
	{
		bool L_4 = m5687(NULL, p0, &m5687_MI);
		if (L_4)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		V_1 = (((t816_SFs*)InitializedTypeInfo(&t816_TI)->static_fields)->f1);
		int64_t L_5 = m9530((&V_1), &m9530_MI);
		if ((((double)p0) < ((double)(((double)L_5)))))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		V_2 = (((t816_SFs*)InitializedTypeInfo(&t816_TI)->static_fields)->f0);
		int64_t L_6 = m9530((&V_2), &m9530_MI);
		if ((((double)p0) <= ((double)(((double)L_6)))))
		{
			goto IL_005f;
		}
	}

IL_004f:
	{
		t7* L_7 = m6079(NULL, (t7*) &_stringLiteral2242, &m6079_MI);
		t1666 * L_8 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_8, L_7, &m9492_MI);
		il2cpp_codegen_raise_exception(L_8);
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		{
			p0 = ((double)((double)p0*(double)(((double)((int64_t)((int64_t)p1/(int64_t)(((int64_t)((int32_t)10000)))))))));
			double L_9 = round(p0);
			if (L_9 > (double)(std::numeric_limits<int64_t>::max())) il2cpp_codegen_raise_exception(il2cpp_codegen_get_overflow_exception());
			V_0 = (((int64_t)L_9));
			t816  L_10 = {0};
			m9520(&L_10, ((int64_t)((int64_t)V_0*(int64_t)(((int64_t)((int32_t)10000))))), &m9520_MI);
			V_3 = L_10;
			// IL_0084: leave.s IL_009b
			goto IL_009b;
		}

IL_0086:
		{
			// IL_0086: leave.s IL_009b
			goto IL_009b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1666_TI, e.ex->object.klass))
			goto IL_0088;
		throw e;
	}

IL_0088:
	{ // begin catch(System.OverflowException)
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral2238, &m6079_MI);
		t1666 * L_12 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_12, L_11, &m9492_MI);
		il2cpp_codegen_raise_exception(L_12);
		// IL_0099: leave.s IL_009b
		goto IL_009b;
	} // end catch (depth: 1)

IL_009b:
	{
		return V_3;
	}
}
extern MethodInfo m9545_MI;
 int32_t m9545 (t816 * __this, MethodInfo* method){
	{
		int64_t* L_0 = &(__this->f3);
		int32_t L_1 = m5348(L_0, &m5348_MI);
		return L_1;
	}
}
extern MethodInfo m9546_MI;
 t816  m9546 (t816 * __this, MethodInfo* method){
	t816  V_0 = {0};
	{
		int64_t L_0 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		V_0 = (((t816_SFs*)InitializedTypeInfo(&t816_TI)->static_fields)->f1);
		int64_t L_1 = ((&V_0)->f3);
		if ((((uint64_t)L_0) != ((uint64_t)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral2243, &m6079_MI);
		t1666 * L_3 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_3, L_2, &m9492_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0025:
	{
		int64_t L_4 = (__this->f3);
		t816  L_5 = {0};
		m9520(&L_5, ((-L_4)), &m9520_MI);
		return L_5;
	}
}
 t816  m9547 (t816 * __this, t816  p0, MethodInfo* method){
	t816  V_0 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_0 = (__this->f3);
			int64_t L_1 = m9530((&p0), &m9530_MI);
			t816  L_2 = {0};
			m9520(&L_2, ((int64_t)(L_0-L_1)), &m9520_MI);
			V_0 = L_2;
			// IL_0014: leave.s IL_002b
			goto IL_002b;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_002b
			goto IL_002b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1666_TI, e.ex->object.klass))
			goto IL_0018;
		throw e;
	}

IL_0018:
	{ // begin catch(System.OverflowException)
		t7* L_3 = m6079(NULL, (t7*) &_stringLiteral2238, &m6079_MI);
		t1666 * L_4 = (t1666 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1666_TI));
		m9492(L_4, L_3, &m9492_MI);
		il2cpp_codegen_raise_exception(L_4);
		// IL_0029: leave.s IL_002b
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		return V_0;
	}
}
extern MethodInfo m9548_MI;
 t7* m9548 (t816 * __this, MethodInfo* method){
	t292 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m2923(L_0, ((int32_t)14), &m2923_MI);
		V_0 = L_0;
		int64_t L_1 = (__this->f3);
		if ((((int64_t)L_1) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_001b;
		}
	}
	{
		m1761(V_0, ((int32_t)45), &m1761_MI);
	}

IL_001b:
	{
		int32_t L_2 = m9525(__this, &m9525_MI);
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_3 = m9525(__this, &m9525_MI);
		int32_t L_4 = abs(L_3);
		m4077(V_0, L_4, &m4077_MI);
		m1761(V_0, ((int32_t)46), &m1761_MI);
	}

IL_003e:
	{
		int32_t L_5 = m9526(__this, &m9526_MI);
		int32_t L_6 = abs(L_5);
		V_2 = L_6;
		t7* L_7 = m4197((&V_2), (t7*) &_stringLiteral2244, &m4197_MI);
		m2927(V_0, L_7, &m2927_MI);
		m1761(V_0, ((int32_t)58), &m1761_MI);
		int32_t L_8 = m9528(__this, &m9528_MI);
		int32_t L_9 = abs(L_8);
		V_3 = L_9;
		t7* L_10 = m4197((&V_3), (t7*) &_stringLiteral2244, &m4197_MI);
		m2927(V_0, L_10, &m2927_MI);
		m1761(V_0, ((int32_t)58), &m1761_MI);
		int32_t L_11 = m9529(__this, &m9529_MI);
		int32_t L_12 = abs(L_11);
		V_4 = L_12;
		t7* L_13 = m4197((&V_4), (t7*) &_stringLiteral2244, &m4197_MI);
		m2927(V_0, L_13, &m2927_MI);
		int64_t L_14 = (__this->f3);
		int64_t L_15 = llabs(((int64_t)(L_14%(((int64_t)((int32_t)10000000))))));
		V_1 = (((int32_t)L_15));
		if (!V_1)
		{
			goto IL_00e1;
		}
	}
	{
		m1761(V_0, ((int32_t)46), &m1761_MI);
		t7* L_16 = m4197((&V_1), (t7*) &_stringLiteral2245, &m4197_MI);
		m2927(V_0, L_16, &m2927_MI);
	}

IL_00e1:
	{
		t7* L_17 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_17;
	}
}
extern MethodInfo m9549_MI;
 t816  m9549 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		t816  L_0 = m9536((&p0), p1, &m9536_MI);
		return L_0;
	}
}
extern MethodInfo m9550_MI;
 bool m9550 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		return ((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m9551_MI;
 bool m9551 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		return ((((int64_t)L_0) > ((int64_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m9552_MI;
 bool m9552 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		return ((((int32_t)((((int64_t)L_0) < ((int64_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m9553_MI;
 bool m9553 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		return ((((int32_t)((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m9554_MI;
 bool m9554 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		return ((((int64_t)L_0) < ((int64_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m9555_MI;
 bool m9555 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		int64_t L_0 = ((&p0)->f3);
		int64_t L_1 = ((&p1)->f3);
		return ((((int32_t)((((int64_t)L_0) > ((int64_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m9556_MI;
 t816  m9556 (t29 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		t816  L_0 = m9547((&p0), p1, &m9547_MI);
		return L_0;
	}
}
// Metadata Definition System.TimeSpan
extern Il2CppType t816_0_0_54;
FieldInfo t816_f0_FieldInfo = 
{
	"MaxValue", &t816_0_0_54, &t816_TI, offsetof(t816_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t816_0_0_54;
FieldInfo t816_f1_FieldInfo = 
{
	"MinValue", &t816_0_0_54, &t816_TI, offsetof(t816_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t816_0_0_54;
FieldInfo t816_f2_FieldInfo = 
{
	"Zero", &t816_0_0_54, &t816_TI, offsetof(t816_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t919_0_0_1;
FieldInfo t816_f3_FieldInfo = 
{
	"_ticks", &t919_0_0_1, &t816_TI, offsetof(t816, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t816_FIs[] =
{
	&t816_f0_FieldInfo,
	&t816_f1_FieldInfo,
	&t816_f2_FieldInfo,
	&t816_f3_FieldInfo,
	NULL
};
static PropertyInfo t816____Days_PropertyInfo = 
{
	&t816_TI, "Days", &m9525_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____Hours_PropertyInfo = 
{
	&t816_TI, "Hours", &m9526_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____Milliseconds_PropertyInfo = 
{
	&t816_TI, "Milliseconds", &m9527_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____Minutes_PropertyInfo = 
{
	&t816_TI, "Minutes", &m9528_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____Seconds_PropertyInfo = 
{
	&t816_TI, "Seconds", &m9529_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____Ticks_PropertyInfo = 
{
	&t816_TI, "Ticks", &m9530_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____TotalDays_PropertyInfo = 
{
	&t816_TI, "TotalDays", &m9531_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____TotalHours_PropertyInfo = 
{
	&t816_TI, "TotalHours", &m9532_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____TotalMilliseconds_PropertyInfo = 
{
	&t816_TI, "TotalMilliseconds", &m9533_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____TotalMinutes_PropertyInfo = 
{
	&t816_TI, "TotalMinutes", &m9534_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t816____TotalSeconds_PropertyInfo = 
{
	&t816_TI, "TotalSeconds", &m9535_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t816_PIs[] =
{
	&t816____Days_PropertyInfo,
	&t816____Hours_PropertyInfo,
	&t816____Milliseconds_PropertyInfo,
	&t816____Minutes_PropertyInfo,
	&t816____Seconds_PropertyInfo,
	&t816____Ticks_PropertyInfo,
	&t816____TotalDays_PropertyInfo,
	&t816____TotalHours_PropertyInfo,
	&t816____TotalMilliseconds_PropertyInfo,
	&t816____TotalMinutes_PropertyInfo,
	&t816____TotalSeconds_PropertyInfo,
	NULL
};
extern Il2CppType t919_0_0_0;
static ParameterInfo t816_m9520_ParameterInfos[] = 
{
	{"ticks", 0, 134224450, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9520_MI = 
{
	".ctor", (methodPointerType)&m9520, &t816_TI, &t21_0_0_0, RuntimeInvoker_t21_t919, t816_m9520_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5370, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t816_m9521_ParameterInfos[] = 
{
	{"hours", 0, 134224451, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minutes", 1, 134224452, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"seconds", 2, 134224453, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9521_MI = 
{
	".ctor", (methodPointerType)&m9521, &t816_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t44, t816_m9521_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 5371, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t816_m9522_ParameterInfos[] = 
{
	{"days", 0, 134224454, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hours", 1, 134224455, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minutes", 2, 134224456, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"seconds", 3, 134224457, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"milliseconds", 4, 134224458, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9522_MI = 
{
	".ctor", (methodPointerType)&m9522, &t816_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t44_t44_t44, t816_m9522_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 5, false, false, 5372, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9523_MI = 
{
	".cctor", (methodPointerType)&m9523, &t816_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 5373, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t816_m9524_ParameterInfos[] = 
{
	{"days", 0, 134224459, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hours", 1, 134224460, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minutes", 2, 134224461, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"seconds", 3, 134224462, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"milliseconds", 4, 134224463, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9524_MI = 
{
	"CalculateTicks", (methodPointerType)&m9524, &t816_TI, &t919_0_0_0, RuntimeInvoker_t919_t44_t44_t44_t44_t44, t816_m9524_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 5, false, false, 5374, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9525_MI = 
{
	"get_Days", (methodPointerType)&m9525, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5375, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9526_MI = 
{
	"get_Hours", (methodPointerType)&m9526, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5376, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9527_MI = 
{
	"get_Milliseconds", (methodPointerType)&m9527, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5377, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9528_MI = 
{
	"get_Minutes", (methodPointerType)&m9528, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5378, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9529_MI = 
{
	"get_Seconds", (methodPointerType)&m9529, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5379, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9530_MI = 
{
	"get_Ticks", (methodPointerType)&m9530, &t816_TI, &t919_0_0_0, RuntimeInvoker_t919, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5380, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9531_MI = 
{
	"get_TotalDays", (methodPointerType)&m9531, &t816_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5381, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9532_MI = 
{
	"get_TotalHours", (methodPointerType)&m9532, &t816_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5382, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9533_MI = 
{
	"get_TotalMilliseconds", (methodPointerType)&m9533, &t816_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5383, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9534_MI = 
{
	"get_TotalMinutes", (methodPointerType)&m9534, &t816_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5384, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9535_MI = 
{
	"get_TotalSeconds", (methodPointerType)&m9535, &t816_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5385, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9536_ParameterInfos[] = 
{
	{"ts", 0, 134224464, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9536_MI = 
{
	"Add", (methodPointerType)&m9536, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816_t816, t816_m9536_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 5386, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9537_ParameterInfos[] = 
{
	{"t1", 0, 134224465, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224466, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9537_MI = 
{
	"Compare", (methodPointerType)&m9537, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44_t816_t816, t816_m9537_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5387, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t816_m9538_ParameterInfos[] = 
{
	{"value", 0, 134224467, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9538_MI = 
{
	"CompareTo", (methodPointerType)&m9538, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t816_m9538_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, false, 5388, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9539_ParameterInfos[] = 
{
	{"value", 0, 134224468, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9539_MI = 
{
	"CompareTo", (methodPointerType)&m9539, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t816_m9539_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 1, false, false, 5389, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9540_ParameterInfos[] = 
{
	{"obj", 0, 134224469, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9540_MI = 
{
	"Equals", (methodPointerType)&m9540, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816, t816_m9540_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, false, 5390, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9541_MI = 
{
	"Duration", (methodPointerType)&m9541, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 5391, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t816_m9542_ParameterInfos[] = 
{
	{"value", 0, 134224470, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9542_MI = 
{
	"Equals", (methodPointerType)&m9542, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t816_m9542_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 5392, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
static ParameterInfo t816_m9543_ParameterInfos[] = 
{
	{"value", 0, 134224471, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t601 (MethodInfo* method, void* obj, void** args);
MethodInfo m9543_MI = 
{
	"FromMinutes", (methodPointerType)&m9543, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816_t601, t816_m9543_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 5393, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t601_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t816_m9544_ParameterInfos[] = 
{
	{"value", 0, 134224472, &EmptyCustomAttributesCache, &t601_0_0_0},
	{"tickMultiplicator", 1, 134224473, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t601_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9544_MI = 
{
	"From", (methodPointerType)&m9544, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816_t601_t919, t816_m9544_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 5394, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9545_MI = 
{
	"GetHashCode", (methodPointerType)&m9545, &t816_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 5395, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9546_MI = 
{
	"Negate", (methodPointerType)&m9546, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 5396, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9547_ParameterInfos[] = 
{
	{"ts", 0, 134224474, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9547_MI = 
{
	"Subtract", (methodPointerType)&m9547, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816_t816, t816_m9547_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 5397, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9548_MI = 
{
	"ToString", (methodPointerType)&m9548, &t816_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 5398, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9549_ParameterInfos[] = 
{
	{"t1", 0, 134224475, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224476, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9549_MI = 
{
	"op_Addition", (methodPointerType)&m9549, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816_t816_t816, t816_m9549_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5399, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9550_ParameterInfos[] = 
{
	{"t1", 0, 134224477, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224478, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9550_MI = 
{
	"op_Equality", (methodPointerType)&m9550, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t816_m9550_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5400, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9551_ParameterInfos[] = 
{
	{"t1", 0, 134224479, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224480, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9551_MI = 
{
	"op_GreaterThan", (methodPointerType)&m9551, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t816_m9551_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5401, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9552_ParameterInfos[] = 
{
	{"t1", 0, 134224481, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224482, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9552_MI = 
{
	"op_GreaterThanOrEqual", (methodPointerType)&m9552, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t816_m9552_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5402, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9553_ParameterInfos[] = 
{
	{"t1", 0, 134224483, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224484, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9553_MI = 
{
	"op_Inequality", (methodPointerType)&m9553, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t816_m9553_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5403, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9554_ParameterInfos[] = 
{
	{"t1", 0, 134224485, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224486, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9554_MI = 
{
	"op_LessThan", (methodPointerType)&m9554, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t816_m9554_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5404, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9555_ParameterInfos[] = 
{
	{"t1", 0, 134224487, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224488, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9555_MI = 
{
	"op_LessThanOrEqual", (methodPointerType)&m9555, &t816_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t816_m9555_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5405, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t816_m9556_ParameterInfos[] = 
{
	{"t1", 0, 134224489, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"t2", 1, 134224490, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t816_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9556_MI = 
{
	"op_Subtraction", (methodPointerType)&m9556, &t816_TI, &t816_0_0_0, RuntimeInvoker_t816_t816_t816, t816_m9556_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5406, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t816_MIs[] =
{
	&m9520_MI,
	&m9521_MI,
	&m9522_MI,
	&m9523_MI,
	&m9524_MI,
	&m9525_MI,
	&m9526_MI,
	&m9527_MI,
	&m9528_MI,
	&m9529_MI,
	&m9530_MI,
	&m9531_MI,
	&m9532_MI,
	&m9533_MI,
	&m9534_MI,
	&m9535_MI,
	&m9536_MI,
	&m9537_MI,
	&m9538_MI,
	&m9539_MI,
	&m9540_MI,
	&m9541_MI,
	&m9542_MI,
	&m9543_MI,
	&m9544_MI,
	&m9545_MI,
	&m9546_MI,
	&m9547_MI,
	&m9548_MI,
	&m9549_MI,
	&m9550_MI,
	&m9551_MI,
	&m9552_MI,
	&m9553_MI,
	&m9554_MI,
	&m9555_MI,
	&m9556_MI,
	NULL
};
static MethodInfo* t816_VT[] =
{
	&m9542_MI,
	&m46_MI,
	&m9545_MI,
	&m9548_MI,
	&m9538_MI,
	&m9539_MI,
	&m9540_MI,
};
extern TypeInfo t2099_TI;
extern TypeInfo t2100_TI;
static TypeInfo* t816_ITIs[] = 
{
	&t290_TI,
	&t2099_TI,
	&t2100_TI,
};
static Il2CppInterfaceOffsetPair t816_IOs[] = 
{
	{ &t290_TI, 4},
	{ &t2099_TI, 5},
	{ &t2100_TI, 6},
};
void t816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t816__CustomAttributeCache = {
1,
NULL,
&t816_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t816_1_0_0;
extern CustomAttributesCache t816__CustomAttributeCache;
TypeInfo t816_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TimeSpan", "System", t816_MIs, t816_PIs, t816_FIs, NULL, &t110_TI, NULL, NULL, &t816_TI, t816_ITIs, t816_VT, &t816__CustomAttributeCache, &t816_TI, &t816_0_0_0, &t816_1_0_0, t816_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t816)+ sizeof (Il2CppObject), 0, sizeof(t816 ), sizeof(t816_SFs), 0, -1, 8457, 0, true, false, false, false, false, false, false, false, false, true, true, true, 37, 11, 4, 0, 0, 7, 3, 3};
#include "t1672.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1672_TI;
#include "t1672MD.h"

#include "t1673.h"
#include "t1299.h"
#include "t465.h"
#include "t1628.h"
extern TypeInfo t465_TI;
extern TypeInfo t1673_TI;
extern TypeInfo t1299_TI;
#include "t465MD.h"
#include "t1673MD.h"
#include "t1299MD.h"
extern MethodInfo m9143_MI;
extern MethodInfo m9567_MI;
extern MethodInfo m9145_MI;
extern MethodInfo m10270_MI;
extern MethodInfo m9561_MI;
extern MethodInfo m6912_MI;
extern MethodInfo m5222_MI;
extern MethodInfo m6913_MI;
extern MethodInfo m9146_MI;
extern MethodInfo m10257_MI;
extern MethodInfo m9183_MI;
extern MethodInfo m4128_MI;
extern MethodInfo m9154_MI;
extern MethodInfo m9147_MI;
extern MethodInfo m6914_MI;
extern MethodInfo m9175_MI;
extern MethodInfo m4127_MI;
extern MethodInfo m9180_MI;
extern MethodInfo m4129_MI;
extern MethodInfo m9114_MI;
extern MethodInfo m9565_MI;
extern MethodInfo m4017_MI;


extern MethodInfo m9557_MI;
 void m9557 (t1672 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m9558_MI;
 void m9558 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		int64_t L_0 = m9143(NULL, &m9143_MI);
		t1673 * L_1 = (t1673 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1673_TI));
		m9567(L_1, L_0, &m9567_MI);
		((t1672_SFs*)InitializedTypeInfo(&t1672_TI)->static_fields)->f0 = L_1;
		return;
	}
}
extern MethodInfo m9559_MI;
 t1672 * m9559 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1672_TI));
		return (((t1672_SFs*)InitializedTypeInfo(&t1672_TI)->static_fields)->f0);
	}
}
extern MethodInfo m9560_MI;
 bool m9560 (t1672 * __this, t465  p0, MethodInfo* method){
	{
		int32_t L_0 = m9145((&p0), &m9145_MI);
		t1299 * L_1 = (t1299 *)VirtFuncInvoker1< t1299 *, int32_t >::Invoke(&m10270_MI, __this, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1672_TI));
		bool L_2 = m9561(NULL, p0, L_1, &m9561_MI);
		return L_2;
	}
}
 bool m9561 (t29 * __this, t465  p0, t1299 * p1, MethodInfo* method){
	t465  V_0 = {0};
	t465  V_1 = {0};
	t465  V_2 = {0};
	t465  V_3 = {0};
	t465  V_4 = {0};
	t465  V_5 = {0};
	t465  V_6 = {0};
	t465  V_7 = {0};
	t465  V_8 = {0};
	t465  V_9 = {0};
	{
		if (p1)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral2246, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		t465  L_1 = m6912(p1, &m6912_MI);
		V_0 = L_1;
		int64_t L_2 = m5222((&V_0), &m5222_MI);
		t465  L_3 = m6913(p1, &m6913_MI);
		V_1 = L_3;
		int64_t L_4 = m5222((&V_1), &m5222_MI);
		if ((((uint64_t)L_2) != ((uint64_t)L_4)))
		{
			goto IL_002e;
		}
	}
	{
		return 0;
	}

IL_002e:
	{
		t465  L_5 = m6912(p1, &m6912_MI);
		V_2 = L_5;
		int64_t L_6 = m5222((&V_2), &m5222_MI);
		t465  L_7 = m6913(p1, &m6913_MI);
		V_3 = L_7;
		int64_t L_8 = m5222((&V_3), &m5222_MI);
		if ((((int64_t)L_6) >= ((int64_t)L_8)))
		{
			goto IL_0080;
		}
	}
	{
		t465  L_9 = m6912(p1, &m6912_MI);
		V_4 = L_9;
		int64_t L_10 = m5222((&V_4), &m5222_MI);
		int64_t L_11 = m5222((&p0), &m5222_MI);
		if ((((int64_t)L_10) >= ((int64_t)L_11)))
		{
			goto IL_007e;
		}
	}
	{
		t465  L_12 = m6913(p1, &m6913_MI);
		V_5 = L_12;
		int64_t L_13 = m5222((&V_5), &m5222_MI);
		int64_t L_14 = m5222((&p0), &m5222_MI);
		if ((((int64_t)L_13) <= ((int64_t)L_14)))
		{
			goto IL_007e;
		}
	}
	{
		return 1;
	}

IL_007e:
	{
		goto IL_00e2;
	}

IL_0080:
	{
		int32_t L_15 = m9145((&p0), &m9145_MI);
		t465  L_16 = m6912(p1, &m6912_MI);
		V_6 = L_16;
		int32_t L_17 = m9145((&V_6), &m9145_MI);
		if ((((uint32_t)L_15) != ((uint32_t)L_17)))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_18 = m9145((&p0), &m9145_MI);
		t465  L_19 = m6913(p1, &m6913_MI);
		V_7 = L_19;
		int32_t L_20 = m9145((&V_7), &m9145_MI);
		if ((((uint32_t)L_18) != ((uint32_t)L_20)))
		{
			goto IL_00e2;
		}
	}
	{
		int64_t L_21 = m5222((&p0), &m5222_MI);
		t465  L_22 = m6913(p1, &m6913_MI);
		V_8 = L_22;
		int64_t L_23 = m5222((&V_8), &m5222_MI);
		if ((((int64_t)L_21) < ((int64_t)L_23)))
		{
			goto IL_00e0;
		}
	}
	{
		int64_t L_24 = m5222((&p0), &m5222_MI);
		t465  L_25 = m6912(p1, &m6912_MI);
		V_9 = L_25;
		int64_t L_26 = m5222((&V_9), &m5222_MI);
		if ((((int64_t)L_24) <= ((int64_t)L_26)))
		{
			goto IL_00e2;
		}
	}

IL_00e0:
	{
		return 1;
	}

IL_00e2:
	{
		return 0;
	}
}
extern MethodInfo m9562_MI;
 t465  m9562 (t1672 * __this, t465  p0, MethodInfo* method){
	t816  V_0 = {0};
	t465  V_1 = {0};
	t1299 * V_2 = {0};
	t816  V_3 = {0};
	t465  V_4 = {0};
	t816  V_5 = {0};
	t465  V_6 = {0};
	{
		int32_t L_0 = m9146((&p0), &m9146_MI);
		if ((((uint32_t)L_0) != ((uint32_t)2)))
		{
			goto IL_000c;
		}
	}
	{
		return p0;
	}

IL_000c:
	{
		t816  L_1 = (t816 )VirtFuncInvoker1< t816 , t465  >::Invoke(&m10257_MI, __this, p0);
		V_0 = L_1;
		int64_t L_2 = m9530((&V_0), &m9530_MI);
		if ((((int64_t)L_2) <= ((int64_t)(((int64_t)0)))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_3 = m9183(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f2), V_0, &m9183_MI);
		bool L_4 = m4128(NULL, L_3, p0, &m4128_MI);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_5 = m9154(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f2), 2, &m9154_MI);
		return L_5;
	}

IL_003e:
	{
		goto IL_0076;
	}

IL_0040:
	{
		int64_t L_6 = m9530((&V_0), &m9530_MI);
		if ((((int64_t)L_6) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_0076;
		}
	}
	{
		int64_t L_7 = m5222((&p0), &m5222_MI);
		int64_t L_8 = m9530((&V_0), &m9530_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		V_4 = (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f3);
		int64_t L_9 = m5222((&V_4), &m5222_MI);
		if ((((int64_t)((int64_t)(L_7+L_8))) >= ((int64_t)L_9)))
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_10 = m9154(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f3), 2, &m9154_MI);
		return L_10;
	}

IL_0076:
	{
		t465  L_11 = m9147((&p0), V_0, &m9147_MI);
		V_1 = L_11;
		int32_t L_12 = m9145((&p0), &m9145_MI);
		t1299 * L_13 = (t1299 *)VirtFuncInvoker1< t1299 *, int32_t >::Invoke(&m10270_MI, __this, L_12);
		V_2 = L_13;
		t816  L_14 = m6914(V_2, &m6914_MI);
		V_5 = L_14;
		int64_t L_15 = m9530((&V_5), &m9530_MI);
		if (L_15)
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_16 = m9154(NULL, V_1, 2, &m9154_MI);
		return L_16;
	}

IL_00a6:
	{
		t465  L_17 = m6913(V_2, &m6913_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		bool L_18 = m4128(NULL, V_1, L_17, &m4128_MI);
		if (!L_18)
		{
			goto IL_00d9;
		}
	}
	{
		t465  L_19 = m6913(V_2, &m6913_MI);
		V_6 = L_19;
		t816  L_20 = m6914(V_2, &m6914_MI);
		t465  L_21 = m9175((&V_6), L_20, &m9175_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		bool L_22 = m4127(NULL, L_21, V_1, &m4127_MI);
		if (!L_22)
		{
			goto IL_00d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_23 = m9154(NULL, V_1, 2, &m9154_MI);
		return L_23;
	}

IL_00d9:
	{
		t816  L_24 = (t816 )VirtFuncInvoker1< t816 , t465  >::Invoke(&m10257_MI, __this, V_1);
		V_3 = L_24;
		t465  L_25 = m9147((&p0), V_3, &m9147_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_26 = m9154(NULL, L_25, 2, &m9154_MI);
		return L_26;
	}
}
extern MethodInfo m9563_MI;
 t465  m9563 (t1672 * __this, t465  p0, MethodInfo* method){
	t816  V_0 = {0};
	{
		int32_t L_0 = m9146((&p0), &m9146_MI);
		if ((((uint32_t)L_0) != ((uint32_t)1)))
		{
			goto IL_000c;
		}
	}
	{
		return p0;
	}

IL_000c:
	{
		t816  L_1 = (t816 )VirtFuncInvoker1< t816 , t465  >::Invoke(&m10257_MI, __this, p0);
		V_0 = L_1;
		int64_t L_2 = m9530((&V_0), &m9530_MI);
		if ((((int64_t)L_2) >= ((int64_t)(((int64_t)0)))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_3 = m9180(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f2), V_0, &m9180_MI);
		bool L_4 = m4128(NULL, L_3, p0, &m4128_MI);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_5 = m9154(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f2), 1, &m9154_MI);
		return L_5;
	}

IL_003e:
	{
		goto IL_006a;
	}

IL_0040:
	{
		int64_t L_6 = m9530((&V_0), &m9530_MI);
		if ((((int64_t)L_6) <= ((int64_t)(((int64_t)0)))))
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_7 = m9180(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f3), V_0, &m9180_MI);
		bool L_8 = m4129(NULL, L_7, p0, &m4129_MI);
		if (!L_8)
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_9 = m9154(NULL, (((t465_SFs*)InitializedTypeInfo(&t465_TI)->static_fields)->f3), 1, &m9154_MI);
		return L_9;
	}

IL_006a:
	{
		int64_t L_10 = m5222((&p0), &m5222_MI);
		int64_t L_11 = m9530((&V_0), &m9530_MI);
		t465  L_12 = {0};
		m9114(&L_12, ((int64_t)(L_10-L_11)), &m9114_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_13 = m9154(NULL, L_12, 1, &m9154_MI);
		return L_13;
	}
}
extern MethodInfo m9564_MI;
 t816  m9564 (t1672 * __this, t465  p0, MethodInfo* method){
	{
		t816  L_0 = (t816 )VirtFuncInvoker1< t816 , t465  >::Invoke(&m10257_MI, __this, p0);
		t816  L_1 = m9565(__this, p0, L_0, &m9565_MI);
		return L_1;
	}
}
 t816  m9565 (t1672 * __this, t465  p0, t816  p1, MethodInfo* method){
	t1299 * V_0 = {0};
	t465  V_1 = {0};
	t816  V_2 = {0};
	t465  V_3 = {0};
	t465  V_4 = {0};
	{
		int32_t L_0 = m9145((&p0), &m9145_MI);
		t1299 * L_1 = (t1299 *)VirtFuncInvoker1< t1299 *, int32_t >::Invoke(&m10270_MI, __this, L_0);
		V_0 = L_1;
		t816  L_2 = m6914(V_0, &m6914_MI);
		V_2 = L_2;
		int64_t L_3 = m9530((&V_2), &m9530_MI);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		return p1;
	}

IL_0020:
	{
		t465  L_4 = m9147((&p0), p1, &m9147_MI);
		V_1 = L_4;
		t465  L_5 = m6913(V_0, &m6913_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		bool L_6 = m4128(NULL, V_1, L_5, &m4128_MI);
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		t465  L_7 = m6913(V_0, &m6913_MI);
		V_3 = L_7;
		t816  L_8 = m6914(V_0, &m6914_MI);
		t465  L_9 = m9175((&V_3), L_8, &m9175_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		bool L_10 = m4127(NULL, L_9, V_1, &m4127_MI);
		if (!L_10)
		{
			goto IL_0055;
		}
	}
	{
		return p1;
	}

IL_0055:
	{
		t465  L_11 = m6912(V_0, &m6912_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		bool L_12 = m4017(NULL, V_1, L_11, &m4017_MI);
		if (!L_12)
		{
			goto IL_008d;
		}
	}
	{
		t465  L_13 = m6912(V_0, &m6912_MI);
		V_4 = L_13;
		t816  L_14 = m6914(V_0, &m6914_MI);
		t465  L_15 = m9147((&V_4), L_14, &m9147_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		bool L_16 = m4129(NULL, L_15, V_1, &m4129_MI);
		if (!L_16)
		{
			goto IL_008d;
		}
	}
	{
		t816  L_17 = m6914(V_0, &m6914_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t816_TI));
		t816  L_18 = m9556(NULL, p1, L_17, &m9556_MI);
		return L_18;
	}

IL_008d:
	{
		t816  L_19 = (t816 )VirtFuncInvoker1< t816 , t465  >::Invoke(&m10257_MI, __this, V_1);
		return L_19;
	}
}
// Metadata Definition System.TimeZone
extern Il2CppType t1672_0_0_17;
FieldInfo t1672_f0_FieldInfo = 
{
	"currentTimeZone", &t1672_0_0_17, &t1672_TI, offsetof(t1672_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1672_FIs[] =
{
	&t1672_f0_FieldInfo,
	NULL
};
static PropertyInfo t1672____CurrentTimeZone_PropertyInfo = 
{
	&t1672_TI, "CurrentTimeZone", &m9559_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1672_PIs[] =
{
	&t1672____CurrentTimeZone_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9557_MI = 
{
	".ctor", (methodPointerType)&m9557, &t1672_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 5407, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9558_MI = 
{
	".cctor", (methodPointerType)&m9558, &t1672_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 5408, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1672_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9559_MI = 
{
	"get_CurrentTimeZone", (methodPointerType)&m9559, &t1672_TI, &t1672_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 5409, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1672_m10270_ParameterInfos[] = 
{
	{"year", 0, 134224491, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1299_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m10270_MI = 
{
	"GetDaylightChanges", NULL, &t1672_TI, &t1299_0_0_0, RuntimeInvoker_t29_t44, t1672_m10270_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, false, 5410, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t1672_m10257_ParameterInfos[] = 
{
	{"time", 0, 134224492, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m10257_MI = 
{
	"GetUtcOffset", NULL, &t1672_TI, &t816_0_0_0, RuntimeInvoker_t816_t465, t1672_m10257_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 1, false, false, 5411, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
static ParameterInfo t1672_m9560_ParameterInfos[] = 
{
	{"time", 0, 134224493, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m9560_MI = 
{
	"IsDaylightSavingTime", (methodPointerType)&m9560, &t1672_TI, &t40_0_0_0, RuntimeInvoker_t40_t465, t1672_m9560_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 6, 1, false, false, 5412, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
extern Il2CppType t1299_0_0_0;
extern Il2CppType t1299_0_0_0;
static ParameterInfo t1672_m9561_ParameterInfos[] = 
{
	{"time", 0, 134224494, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"daylightTimes", 1, 134224495, &EmptyCustomAttributesCache, &t1299_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9561_MI = 
{
	"IsDaylightSavingTime", (methodPointerType)&m9561, &t1672_TI, &t40_0_0_0, RuntimeInvoker_t40_t465_t29, t1672_m9561_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 5413, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
static ParameterInfo t1672_m9562_ParameterInfos[] = 
{
	{"time", 0, 134224496, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t465_0_0_0;
extern void* RuntimeInvoker_t465_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m9562_MI = 
{
	"ToLocalTime", (methodPointerType)&m9562, &t1672_TI, &t465_0_0_0, RuntimeInvoker_t465_t465, t1672_m9562_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 1, false, false, 5414, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
static ParameterInfo t1672_m9563_ParameterInfos[] = 
{
	{"time", 0, 134224497, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t465_0_0_0;
extern void* RuntimeInvoker_t465_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m9563_MI = 
{
	"ToUniversalTime", (methodPointerType)&m9563, &t1672_TI, &t465_0_0_0, RuntimeInvoker_t465_t465, t1672_m9563_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 8, 1, false, false, 5415, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
static ParameterInfo t1672_m9564_ParameterInfos[] = 
{
	{"time", 0, 134224498, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m9564_MI = 
{
	"GetLocalTimeDiff", (methodPointerType)&m9564, &t1672_TI, &t816_0_0_0, RuntimeInvoker_t816_t465, t1672_m9564_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 5416, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t1672_m9565_ParameterInfos[] = 
{
	{"time", 0, 134224499, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"utc_offset", 1, 134224500, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t465_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m9565_MI = 
{
	"GetLocalTimeDiff", (methodPointerType)&m9565, &t1672_TI, &t816_0_0_0, RuntimeInvoker_t816_t465_t816, t1672_m9565_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 2, false, false, 5417, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1672_MIs[] =
{
	&m9557_MI,
	&m9558_MI,
	&m9559_MI,
	&m10270_MI,
	&m10257_MI,
	&m9560_MI,
	&m9561_MI,
	&m9562_MI,
	&m9563_MI,
	&m9564_MI,
	&m9565_MI,
	NULL
};
static MethodInfo* t1672_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	NULL,
	&m9560_MI,
	&m9562_MI,
	&m9563_MI,
};
void t1672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1672__CustomAttributeCache = {
1,
NULL,
&t1672_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1672_0_0_0;
extern Il2CppType t1672_1_0_0;
struct t1672;
extern CustomAttributesCache t1672__CustomAttributeCache;
TypeInfo t1672_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TimeZone", "System", t1672_MIs, t1672_PIs, t1672_FIs, NULL, &t29_TI, NULL, NULL, &t1672_TI, NULL, t1672_VT, &t1672__CustomAttributeCache, &t1672_TI, &t1672_0_0_0, &t1672_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1672), 0, -1, sizeof(t1672_SFs), 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, true, false, false, 11, 1, 1, 0, 0, 9, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t921MD.h"
extern MethodInfo m9569_MI;
extern MethodInfo m9573_MI;
extern MethodInfo m9572_MI;
extern MethodInfo m1312_MI;
extern MethodInfo m3968_MI;
extern MethodInfo m4000_MI;
extern MethodInfo m4001_MI;
extern MethodInfo m2844_MI;
extern MethodInfo m6911_MI;


extern MethodInfo m9566_MI;
 void m9566 (t1673 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4215(L_0, 1, &m4215_MI);
		__this->f3 = L_0;
		m9557(__this, &m9557_MI);
		return;
	}
}
 void m9567 (t1673 * __this, int64_t p0, MethodInfo* method){
	t1139* V_0 = {0};
	t446* V_1 = {0};
	t465  V_2 = {0};
	t1299 * V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4215(L_0, 1, &m4215_MI);
		__this->f3 = L_0;
		m9557(__this, &m9557_MI);
		m9114((&V_2), p0, &m9114_MI);
		int32_t L_1 = m9145((&V_2), &m9145_MI);
		bool L_2 = m9569(NULL, L_1, (&V_0), (&V_1), &m9569_MI);
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		t7* L_3 = m6079(NULL, (t7*) &_stringLiteral2247, &m6079_MI);
		t345 * L_4 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_4, L_3, &m3988_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003c:
	{
		int32_t L_5 = 0;
		t7* L_6 = m6079(NULL, (*(t7**)(t7**)SZArrayLdElema(V_1, L_5)), &m6079_MI);
		__this->f1 = L_6;
		int32_t L_7 = 1;
		t7* L_8 = m6079(NULL, (*(t7**)(t7**)SZArrayLdElema(V_1, L_7)), &m6079_MI);
		__this->f2 = L_8;
		int32_t L_9 = 2;
		__this->f4 = (*(int64_t*)(int64_t*)SZArrayLdElema(V_0, L_9));
		t1299 * L_10 = m9573(__this, V_0, &m9573_MI);
		V_3 = L_10;
		t719 * L_11 = (__this->f3);
		int32_t L_12 = m9145((&V_2), &m9145_MI);
		int32_t L_13 = L_12;
		t29 * L_14 = Box(InitializedTypeInfo(&t44_TI), &L_13);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, L_11, L_14, V_3);
		m9572(__this, V_3, &m9572_MI);
		return;
	}
}
extern MethodInfo m9568_MI;
 void m9568 (t1673 * __this, t29 * p0, MethodInfo* method){
	{
		m9572(__this, (t1299 *)NULL, &m9572_MI);
		return;
	}
}
 bool m9569 (t29 * __this, int32_t p0, t1139** p1, t446** p2, MethodInfo* method){
	typedef bool (*m9569_ftn) (int32_t, t1139**, t446**);
	static m9569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m9569_ftn)il2cpp_codegen_resolve_icall ("System.CurrentSystemTimeZone::GetTimeZoneData(System.Int32,System.Int64[]&,System.String[]&)");
	return _il2cpp_icall_func(p0, p1, p2);
}
extern MethodInfo m9570_MI;
 t1299 * m9570 (t1673 * __this, int32_t p0, MethodInfo* method){
	t719 * V_0 = {0};
	t1299 * V_1 = {0};
	t1139* V_2 = {0};
	t446* V_3 = {0};
	t1299 * V_4 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if ((((int32_t)p0) < ((int32_t)1)))
		{
			goto IL_000c;
		}
	}
	{
		if ((((int32_t)p0) <= ((int32_t)((int32_t)9999))))
		{
			goto IL_002c;
		}
	}

IL_000c:
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral2249, &m6079_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1312(NULL, L_1, L_2, &m1312_MI);
		t915 * L_4 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_4, (t7*) &_stringLiteral2248, L_3, &m3968_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002c:
	{
		if ((((uint32_t)p0) != ((uint32_t)(((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f7))))
		{
			goto IL_003a;
		}
	}
	{
		return (((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f8);
	}

IL_003a:
	{
		t719 * L_5 = (__this->f3);
		V_0 = L_5;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_0047:
	try
	{ // begin try (depth: 1)
		{
			t719 * L_6 = (__this->f3);
			int32_t L_7 = p0;
			t29 * L_8 = Box(InitializedTypeInfo(&t44_TI), &L_7);
			t29 * L_9 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_6, L_8);
			V_1 = ((t1299 *)Castclass(L_9, InitializedTypeInfo(&t1299_TI)));
			if (V_1)
			{
				goto IL_00a2;
			}
		}

IL_0061:
		{
			bool L_10 = m9569(NULL, p0, (&V_2), (&V_3), &m9569_MI);
			if (L_10)
			{
				goto IL_0088;
			}
		}

IL_006d:
		{
			int32_t L_11 = p0;
			t29 * L_12 = Box(InitializedTypeInfo(&t44_TI), &L_11);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			t7* L_13 = m1312(NULL, (t7*) &_stringLiteral2250, L_12, &m1312_MI);
			t7* L_14 = m6079(NULL, L_13, &m6079_MI);
			t305 * L_15 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
			m1935(L_15, L_14, &m1935_MI);
			il2cpp_codegen_raise_exception(L_15);
		}

IL_0088:
		{
			t1299 * L_16 = m9573(__this, V_2, &m9573_MI);
			V_1 = L_16;
			t719 * L_17 = (__this->f3);
			int32_t L_18 = p0;
			t29 * L_19 = Box(InitializedTypeInfo(&t44_TI), &L_18);
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, L_17, L_19, V_1);
		}

IL_00a2:
		{
			V_4 = V_1;
			// IL_00a5: leave.s IL_00b0
			leaveInstructions[0] = 0xB0; // 1
			THROW_SENTINEL(IL_00b0);
			// finally target depth: 1
		}

IL_00a7:
		{
			// IL_00a7: leave.s IL_00b0
			leaveInstructions[0] = 0xB0; // 1
			THROW_SENTINEL(IL_00b0);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_00a9;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_00a9;
	}

IL_00a9:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xB0:
				goto IL_00b0;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00b0:
	{
		return V_4;
	}
}
extern MethodInfo m9571_MI;
 t816  m9571 (t1673 * __this, t465  p0, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t465  >::Invoke(&m9560_MI, __this, p0);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		t816  L_1 = (__this->f6);
		return L_1;
	}

IL_0010:
	{
		t816  L_2 = (__this->f5);
		return L_2;
	}
}
 void m9572 (t1673 * __this, t1299 * p0, MethodInfo* method){
	t1139* V_0 = {0};
	t446* V_1 = {0};
	t465  V_2 = {0};
	t465  V_3 = {0};
	t816  V_4 = {0};
	{
		if (p0)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_0 = m2844(NULL, &m2844_MI);
		V_2 = L_0;
		int32_t L_1 = m9145((&V_2), &m9145_MI);
		((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f7 = L_1;
		bool L_2 = m9569(NULL, (((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f7), (&V_0), (&V_1), &m9569_MI);
		if (L_2)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_3 = (((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f7);
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = m1312(NULL, (t7*) &_stringLiteral2250, L_4, &m1312_MI);
		t7* L_6 = m6079(NULL, L_5, &m6079_MI);
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, L_6, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_0044:
	{
		t1299 * L_8 = m9573(__this, V_0, &m9573_MI);
		p0 = L_8;
		goto IL_0064;
	}

IL_0051:
	{
		t465  L_9 = m6912(p0, &m6912_MI);
		V_3 = L_9;
		int32_t L_10 = m9145((&V_3), &m9145_MI);
		((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f7 = L_10;
	}

IL_0064:
	{
		int64_t L_11 = (__this->f4);
		t816  L_12 = {0};
		m9520(&L_12, L_11, &m9520_MI);
		__this->f5 = L_12;
		int64_t L_13 = (__this->f4);
		t816  L_14 = m6914(p0, &m6914_MI);
		V_4 = L_14;
		int64_t L_15 = m9530((&V_4), &m9530_MI);
		t816  L_16 = {0};
		m9520(&L_16, ((int64_t)(L_13+L_15)), &m9520_MI);
		__this->f6 = L_16;
		((t1673_SFs*)InitializedTypeInfo(&t1673_TI)->static_fields)->f8 = p0;
		return;
	}
}
 t1299 * m9573 (t1673 * __this, t1139* p0, MethodInfo* method){
	{
		int32_t L_0 = 0;
		t465  L_1 = {0};
		m9114(&L_1, (*(int64_t*)(int64_t*)SZArrayLdElema(p0, L_0)), &m9114_MI);
		int32_t L_2 = 1;
		t465  L_3 = {0};
		m9114(&L_3, (*(int64_t*)(int64_t*)SZArrayLdElema(p0, L_2)), &m9114_MI);
		int32_t L_4 = 3;
		t816  L_5 = {0};
		m9520(&L_5, (*(int64_t*)(int64_t*)SZArrayLdElema(p0, L_4)), &m9520_MI);
		t1299 * L_6 = (t1299 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1299_TI));
		m6911(L_6, L_1, L_3, L_5, &m6911_MI);
		return L_6;
	}
}
// Metadata Definition System.CurrentSystemTimeZone
extern Il2CppType t7_0_0_1;
FieldInfo t1673_f1_FieldInfo = 
{
	"m_standardName", &t7_0_0_1, &t1673_TI, offsetof(t1673, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1673_f2_FieldInfo = 
{
	"m_daylightName", &t7_0_0_1, &t1673_TI, offsetof(t1673, f2), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_1;
FieldInfo t1673_f3_FieldInfo = 
{
	"m_CachedDaylightChanges", &t719_0_0_1, &t1673_TI, offsetof(t1673, f3), &EmptyCustomAttributesCache};
extern Il2CppType t919_0_0_1;
FieldInfo t1673_f4_FieldInfo = 
{
	"m_ticksOffset", &t919_0_0_1, &t1673_TI, offsetof(t1673, f4), &EmptyCustomAttributesCache};
extern Il2CppType t816_0_0_129;
FieldInfo t1673_f5_FieldInfo = 
{
	"utcOffsetWithOutDLS", &t816_0_0_129, &t1673_TI, offsetof(t1673, f5), &EmptyCustomAttributesCache};
extern Il2CppType t816_0_0_129;
FieldInfo t1673_f6_FieldInfo = 
{
	"utcOffsetWithDLS", &t816_0_0_129, &t1673_TI, offsetof(t1673, f6), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_17;
FieldInfo t1673_f7_FieldInfo = 
{
	"this_year", &t44_0_0_17, &t1673_TI, offsetof(t1673_SFs, f7), &EmptyCustomAttributesCache};
extern Il2CppType t1299_0_0_17;
FieldInfo t1673_f8_FieldInfo = 
{
	"this_year_dlt", &t1299_0_0_17, &t1673_TI, offsetof(t1673_SFs, f8), &EmptyCustomAttributesCache};
static FieldInfo* t1673_FIs[] =
{
	&t1673_f1_FieldInfo,
	&t1673_f2_FieldInfo,
	&t1673_f3_FieldInfo,
	&t1673_f4_FieldInfo,
	&t1673_f5_FieldInfo,
	&t1673_f6_FieldInfo,
	&t1673_f7_FieldInfo,
	&t1673_f8_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9566_MI = 
{
	".ctor", (methodPointerType)&m9566, &t1673_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 5418, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
static ParameterInfo t1673_m9567_ParameterInfos[] = 
{
	{"lnow", 0, 134224501, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m9567_MI = 
{
	".ctor", (methodPointerType)&m9567, &t1673_TI, &t21_0_0_0, RuntimeInvoker_t21_t919, t1673_m9567_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, false, 5419, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1673_m9568_ParameterInfos[] = 
{
	{"sender", 0, 134224502, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9568_MI = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization", (methodPointerType)&m9568, &t1673_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1673_m9568_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 1, false, false, 5420, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1139_1_0_2;
extern Il2CppType t1139_1_0_0;
extern Il2CppType t446_1_0_2;
extern Il2CppType t446_1_0_0;
static ParameterInfo t1673_m9569_ParameterInfos[] = 
{
	{"year", 0, 134224503, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"data", 1, 134224504, &EmptyCustomAttributesCache, &t1139_1_0_2},
	{"names", 2, 134224505, &EmptyCustomAttributesCache, &t446_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t2101_t2102 (MethodInfo* method, void* obj, void** args);
MethodInfo m9569_MI = 
{
	"GetTimeZoneData", (methodPointerType)&m9569, &t1673_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t2101_t2102, t1673_m9569_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 3, false, false, 5421, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1673_m9570_ParameterInfos[] = 
{
	{"year", 0, 134224506, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1299_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9570_MI = 
{
	"GetDaylightChanges", (methodPointerType)&m9570, &t1673_TI, &t1299_0_0_0, RuntimeInvoker_t29_t44, t1673_m9570_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 5422, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t465_0_0_0;
static ParameterInfo t1673_m9571_ParameterInfos[] = 
{
	{"time", 0, 134224507, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m9571_MI = 
{
	"GetUtcOffset", (methodPointerType)&m9571, &t1673_TI, &t816_0_0_0, RuntimeInvoker_t816_t465, t1673_m9571_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 1, false, false, 5423, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1299_0_0_0;
static ParameterInfo t1673_m9572_ParameterInfos[] = 
{
	{"dlt", 0, 134224508, &EmptyCustomAttributesCache, &t1299_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9572_MI = 
{
	"OnDeserialization", (methodPointerType)&m9572, &t1673_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1673_m9572_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5424, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1139_0_0_0;
extern Il2CppType t1139_0_0_0;
static ParameterInfo t1673_m9573_ParameterInfos[] = 
{
	{"data", 0, 134224509, &EmptyCustomAttributesCache, &t1139_0_0_0},
};
extern Il2CppType t1299_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9573_MI = 
{
	"GetDaylightTimeFromData", (methodPointerType)&m9573, &t1673_TI, &t1299_0_0_0, RuntimeInvoker_t29_t29, t1673_m9573_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5425, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1673_MIs[] =
{
	&m9566_MI,
	&m9567_MI,
	&m9568_MI,
	&m9569_MI,
	&m9570_MI,
	&m9571_MI,
	&m9572_MI,
	&m9573_MI,
	NULL
};
static MethodInfo* t1673_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m9570_MI,
	&m9571_MI,
	&m9560_MI,
	&m9562_MI,
	&m9563_MI,
	&m9568_MI,
};
extern TypeInfo t918_TI;
static TypeInfo* t1673_ITIs[] = 
{
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t1673_IOs[] = 
{
	{ &t918_TI, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1673_0_0_0;
extern Il2CppType t1673_1_0_0;
struct t1673;
TypeInfo t1673_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CurrentSystemTimeZone", "System", t1673_MIs, NULL, t1673_FIs, NULL, &t1672_TI, NULL, NULL, &t1673_TI, t1673_ITIs, t1673_VT, &EmptyCustomAttributesCache, &t1673_TI, &t1673_0_0_0, &t1673_1_0_0, t1673_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1673), 0, -1, sizeof(t1673_SFs), 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 8, 0, 8, 0, 0, 10, 1, 1};
#include "t1127.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1127_TI;
#include "t1127MD.h"



// Metadata Definition System.TypeCode
extern Il2CppType t44_0_0_1542;
FieldInfo t1127_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1127_TI, offsetof(t1127, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f2_FieldInfo = 
{
	"Empty", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f3_FieldInfo = 
{
	"Object", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f4_FieldInfo = 
{
	"DBNull", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f5_FieldInfo = 
{
	"Boolean", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f6_FieldInfo = 
{
	"Char", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f7_FieldInfo = 
{
	"SByte", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f8_FieldInfo = 
{
	"Byte", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f9_FieldInfo = 
{
	"Int16", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f10_FieldInfo = 
{
	"UInt16", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f11_FieldInfo = 
{
	"Int32", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f12_FieldInfo = 
{
	"UInt32", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f13_FieldInfo = 
{
	"Int64", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f14_FieldInfo = 
{
	"UInt64", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f15_FieldInfo = 
{
	"Single", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f16_FieldInfo = 
{
	"Double", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f17_FieldInfo = 
{
	"Decimal", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f18_FieldInfo = 
{
	"DateTime", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1127_0_0_32854;
FieldInfo t1127_f19_FieldInfo = 
{
	"String", &t1127_0_0_32854, &t1127_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1127_FIs[] =
{
	&t1127_f1_FieldInfo,
	&t1127_f2_FieldInfo,
	&t1127_f3_FieldInfo,
	&t1127_f4_FieldInfo,
	&t1127_f5_FieldInfo,
	&t1127_f6_FieldInfo,
	&t1127_f7_FieldInfo,
	&t1127_f8_FieldInfo,
	&t1127_f9_FieldInfo,
	&t1127_f10_FieldInfo,
	&t1127_f11_FieldInfo,
	&t1127_f12_FieldInfo,
	&t1127_f13_FieldInfo,
	&t1127_f14_FieldInfo,
	&t1127_f15_FieldInfo,
	&t1127_f16_FieldInfo,
	&t1127_f17_FieldInfo,
	&t1127_f18_FieldInfo,
	&t1127_f19_FieldInfo,
	NULL
};
static const int32_t t1127_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1127_f2_DefaultValue = 
{
	&t1127_f2_FieldInfo, { (char*)&t1127_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1127_f3_DefaultValue = 
{
	&t1127_f3_FieldInfo, { (char*)&t1127_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1127_f4_DefaultValue = 
{
	&t1127_f4_FieldInfo, { (char*)&t1127_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1127_f5_DefaultValue = 
{
	&t1127_f5_FieldInfo, { (char*)&t1127_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1127_f6_DefaultValue = 
{
	&t1127_f6_FieldInfo, { (char*)&t1127_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1127_f7_DefaultValue = 
{
	&t1127_f7_FieldInfo, { (char*)&t1127_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1127_f8_DefaultValue = 
{
	&t1127_f8_FieldInfo, { (char*)&t1127_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1127_f9_DefaultValue = 
{
	&t1127_f9_FieldInfo, { (char*)&t1127_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1127_f10_DefaultValue = 
{
	&t1127_f10_FieldInfo, { (char*)&t1127_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry t1127_f11_DefaultValue = 
{
	&t1127_f11_FieldInfo, { (char*)&t1127_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t1127_f12_DefaultValue = 
{
	&t1127_f12_FieldInfo, { (char*)&t1127_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry t1127_f13_DefaultValue = 
{
	&t1127_f13_FieldInfo, { (char*)&t1127_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry t1127_f14_DefaultValue = 
{
	&t1127_f14_FieldInfo, { (char*)&t1127_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry t1127_f15_DefaultValue = 
{
	&t1127_f15_FieldInfo, { (char*)&t1127_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry t1127_f16_DefaultValue = 
{
	&t1127_f16_FieldInfo, { (char*)&t1127_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry t1127_f17_DefaultValue = 
{
	&t1127_f17_FieldInfo, { (char*)&t1127_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1127_f18_DefaultValue = 
{
	&t1127_f18_FieldInfo, { (char*)&t1127_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1127_f19_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry t1127_f19_DefaultValue = 
{
	&t1127_f19_FieldInfo, { (char*)&t1127_f19_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1127_FDVs[] = 
{
	&t1127_f2_DefaultValue,
	&t1127_f3_DefaultValue,
	&t1127_f4_DefaultValue,
	&t1127_f5_DefaultValue,
	&t1127_f6_DefaultValue,
	&t1127_f7_DefaultValue,
	&t1127_f8_DefaultValue,
	&t1127_f9_DefaultValue,
	&t1127_f10_DefaultValue,
	&t1127_f11_DefaultValue,
	&t1127_f12_DefaultValue,
	&t1127_f13_DefaultValue,
	&t1127_f14_DefaultValue,
	&t1127_f15_DefaultValue,
	&t1127_f16_DefaultValue,
	&t1127_f17_DefaultValue,
	&t1127_f18_DefaultValue,
	&t1127_f19_DefaultValue,
	NULL
};
static MethodInfo* t1127_MIs[] =
{
	NULL
};
static MethodInfo* t1127_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1127_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1127_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1127__CustomAttributeCache = {
1,
NULL,
&t1127_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1127_0_0_0;
extern Il2CppType t1127_1_0_0;
extern CustomAttributesCache t1127__CustomAttributeCache;
TypeInfo t1127_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeCode", "System", t1127_MIs, NULL, t1127_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1127_VT, &t1127__CustomAttributeCache, &t44_TI, &t1127_0_0_0, &t1127_1_0_0, t1127_IOs, NULL, NULL, t1127_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1127)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 19, 0, 0, 23, 0, 3};
#include "t1674.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1674_TI;
#include "t1674MD.h"



extern MethodInfo m9574_MI;
 void m9574 (t1674 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		t7* L_0 = m3994(p0, (t7*) &_stringLiteral1041, &m3994_MI);
		__this->f11 = L_0;
		return;
	}
}
extern MethodInfo m9575_MI;
 void m9575 (t1674 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m2857(__this, p0, p1, &m2857_MI);
		t7* L_0 = (__this->f11);
		m3997(p0, (t7*) &_stringLiteral1041, L_0, &m3997_MI);
		return;
	}
}
// Metadata Definition System.TypeInitializationException
extern Il2CppType t7_0_0_1;
FieldInfo t1674_f11_FieldInfo = 
{
	"type_name", &t7_0_0_1, &t1674_TI, offsetof(t1674, f11), &EmptyCustomAttributesCache};
static FieldInfo* t1674_FIs[] =
{
	&t1674_f11_FieldInfo,
	NULL
};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1674_m9574_ParameterInfos[] = 
{
	{"info", 0, 134224510, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224511, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9574_MI = 
{
	".ctor", (methodPointerType)&m9574, &t1674_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1674_m9574_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 5426, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1674_m9575_ParameterInfos[] = 
{
	{"info", 0, 134224512, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224513, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9575_MI = 
{
	"GetObjectData", (methodPointerType)&m9575, &t1674_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1674_m9575_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, false, 5427, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1674_MIs[] =
{
	&m9574_MI,
	&m9575_MI,
	NULL
};
static MethodInfo* t1674_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m9575_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m9575_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1674_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1674__CustomAttributeCache = {
1,
NULL,
&t1674_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1674_0_0_0;
extern Il2CppType t1674_1_0_0;
struct t1674;
extern CustomAttributesCache t1674__CustomAttributeCache;
TypeInfo t1674_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeInitializationException", "System", t1674_MIs, NULL, t1674_FIs, NULL, &t956_TI, NULL, NULL, &t1674_TI, NULL, t1674_VT, &t1674__CustomAttributeCache, &t1674_TI, &t1674_0_0_0, &t1674_1_0_0, t1674_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1674), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 11, 0, 2};
#include "t1636.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1636_TI;
#include "t1636MD.h"

extern MethodInfo m1740_MI;
extern MethodInfo m1535_MI;


extern MethodInfo m9576_MI;
 void m9576 (t1636 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2253, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233054), &m2946_MI);
		return;
	}
}
extern MethodInfo m9577_MI;
 void m9577 (t1636 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233054), &m2946_MI);
		return;
	}
}
extern MethodInfo m9578_MI;
 void m9578 (t1636 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		if (p0)
		{
			goto IL_0016;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0016:
	{
		t7* L_1 = m3994(p0, (t7*) &_stringLiteral2254, &m3994_MI);
		__this->f12 = L_1;
		t7* L_2 = m3994(p0, (t7*) &_stringLiteral2255, &m3994_MI);
		__this->f13 = L_2;
		return;
	}
}
extern MethodInfo m9579_MI;
 t7* m9579 (t1636 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f12);
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		t7* L_1 = (__this->f13);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		t7* L_2 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_3 = m1740(NULL, L_2, (((t7_SFs*)(&t7_TI)->static_fields)->f2), &m1740_MI);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		t7* L_4 = (__this->f12);
		t7* L_5 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_6 = m5610(NULL, (t7*) &_stringLiteral2251, L_4, L_5, &m5610_MI);
		return L_6;
	}

IL_0039:
	{
		t7* L_7 = (__this->f12);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_8 = m1535(NULL, (t7*) &_stringLiteral2252, L_7, &m1535_MI);
		return L_8;
	}

IL_004a:
	{
		t7* L_9 = m1684(__this, &m1684_MI);
		return L_9;
	}
}
extern MethodInfo m9580_MI;
 void m9580 (t1636 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		m2857(__this, p0, p1, &m2857_MI);
		t7* L_1 = (__this->f12);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral2254, L_1, L_2, &m3982_MI);
		t7* L_3 = (__this->f13);
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral2255, L_3, L_4, &m3982_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral2256, (((t7_SFs*)(&t7_TI)->static_fields)->f2), L_5, &m3982_MI);
		int32_t L_6 = 0;
		t29 * L_7 = Box(InitializedTypeInfo(&t44_TI), &L_6);
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral2257, L_7, L_8, &m3982_MI);
		return;
	}
}
// Metadata Definition System.TypeLoadException
extern Il2CppType t44_0_0_32849;
FieldInfo t1636_f11_FieldInfo = 
{
	"Result", &t44_0_0_32849, &t1636_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1636_f12_FieldInfo = 
{
	"className", &t7_0_0_1, &t1636_TI, offsetof(t1636, f12), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1636_f13_FieldInfo = 
{
	"assemblyName", &t7_0_0_1, &t1636_TI, offsetof(t1636, f13), &EmptyCustomAttributesCache};
static FieldInfo* t1636_FIs[] =
{
	&t1636_f11_FieldInfo,
	&t1636_f12_FieldInfo,
	&t1636_f13_FieldInfo,
	NULL
};
static const int32_t t1636_f11_DefaultValueData = -2146233054;
static Il2CppFieldDefaultValueEntry t1636_f11_DefaultValue = 
{
	&t1636_f11_FieldInfo, { (char*)&t1636_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1636_FDVs[] = 
{
	&t1636_f11_DefaultValue,
	NULL
};
static PropertyInfo t1636____Message_PropertyInfo = 
{
	&t1636_TI, "Message", &m9579_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1636_PIs[] =
{
	&t1636____Message_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9576_MI = 
{
	".ctor", (methodPointerType)&m9576, &t1636_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5428, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1636_m9577_ParameterInfos[] = 
{
	{"message", 0, 134224514, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9577_MI = 
{
	".ctor", (methodPointerType)&m9577, &t1636_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1636_m9577_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5429, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1636_m9578_ParameterInfos[] = 
{
	{"info", 0, 134224515, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224516, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9578_MI = 
{
	".ctor", (methodPointerType)&m9578, &t1636_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1636_m9578_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5430, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9579_MI = 
{
	"get_Message", (methodPointerType)&m9579, &t1636_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 5431, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1636_m9580_ParameterInfos[] = 
{
	{"info", 0, 134224517, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224518, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9580_MI = 
{
	"GetObjectData", (methodPointerType)&m9580, &t1636_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1636_m9580_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, false, 5432, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1636_MIs[] =
{
	&m9576_MI,
	&m9577_MI,
	&m9578_MI,
	&m9579_MI,
	&m9580_MI,
	NULL
};
static MethodInfo* t1636_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m9580_MI,
	&m2858_MI,
	&m9579_MI,
	&m2859_MI,
	&m2860_MI,
	&m9580_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1636_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1636__CustomAttributeCache = {
1,
NULL,
&t1636_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1636_0_0_0;
extern Il2CppType t1636_1_0_0;
struct t1636;
extern CustomAttributesCache t1636__CustomAttributeCache;
TypeInfo t1636_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeLoadException", "System", t1636_MIs, t1636_PIs, t1636_FIs, NULL, &t956_TI, NULL, NULL, &t1636_TI, NULL, t1636_VT, &t1636__CustomAttributeCache, &t1636_TI, &t1636_0_0_0, &t1636_1_0_0, t1636_IOs, NULL, NULL, t1636_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1636), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 3, 0, 0, 11, 0, 2};
#include "t1675.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1675_TI;
#include "t1675MD.h"



extern MethodInfo m9581_MI;
 void m9581 (t1675 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2258, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233088), &m2946_MI);
		return;
	}
}
extern MethodInfo m9582_MI;
 void m9582 (t1675 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233088), &m2946_MI);
		return;
	}
}
extern MethodInfo m9583_MI;
 void m9583 (t1675 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.UnauthorizedAccessException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9581_MI = 
{
	".ctor", (methodPointerType)&m9581, &t1675_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5433, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1675_m9582_ParameterInfos[] = 
{
	{"message", 0, 134224519, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9582_MI = 
{
	".ctor", (methodPointerType)&m9582, &t1675_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1675_m9582_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5434, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1675_m9583_ParameterInfos[] = 
{
	{"info", 0, 134224520, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224521, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9583_MI = 
{
	".ctor", (methodPointerType)&m9583, &t1675_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1675_m9583_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5435, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1675_MIs[] =
{
	&m9581_MI,
	&m9582_MI,
	&m9583_MI,
	NULL
};
static MethodInfo* t1675_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1675_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1675__CustomAttributeCache = {
1,
NULL,
&t1675_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1675_0_0_0;
extern Il2CppType t1675_1_0_0;
struct t1675;
extern CustomAttributesCache t1675__CustomAttributeCache;
TypeInfo t1675_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnauthorizedAccessException", "System", t1675_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t1675_TI, NULL, t1675_VT, &t1675__CustomAttributeCache, &t1675_TI, &t1675_0_0_0, &t1675_1_0_0, t1675_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1675), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1676.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1676_TI;
#include "t1676MD.h"



// Metadata Definition System.UnhandledExceptionEventArgs
static MethodInfo* t1676_MIs[] =
{
	NULL
};
static MethodInfo* t1676_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1676_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1676__CustomAttributeCache = {
1,
NULL,
&t1676_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1676_0_0_0;
extern Il2CppType t1676_1_0_0;
struct t1676;
extern CustomAttributesCache t1676__CustomAttributeCache;
TypeInfo t1676_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnhandledExceptionEventArgs", "System", t1676_MIs, NULL, NULL, NULL, &t999_TI, NULL, NULL, &t1676_TI, NULL, t1676_VT, &t1676__CustomAttributeCache, &t1676_TI, &t1676_0_0_0, &t1676_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1676), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1677.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1677_TI;
#include "t1677MD.h"



// Metadata Definition System.UnitySerializationHolder/UnityType
extern Il2CppType t348_0_0_1542;
FieldInfo t1677_f1_FieldInfo = 
{
	"value__", &t348_0_0_1542, &t1677_TI, offsetof(t1677, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1677_0_0_32854;
FieldInfo t1677_f2_FieldInfo = 
{
	"DBNull", &t1677_0_0_32854, &t1677_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1677_0_0_32854;
FieldInfo t1677_f3_FieldInfo = 
{
	"Type", &t1677_0_0_32854, &t1677_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1677_0_0_32854;
FieldInfo t1677_f4_FieldInfo = 
{
	"Module", &t1677_0_0_32854, &t1677_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1677_0_0_32854;
FieldInfo t1677_f5_FieldInfo = 
{
	"Assembly", &t1677_0_0_32854, &t1677_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1677_FIs[] =
{
	&t1677_f1_FieldInfo,
	&t1677_f2_FieldInfo,
	&t1677_f3_FieldInfo,
	&t1677_f4_FieldInfo,
	&t1677_f5_FieldInfo,
	NULL
};
static const uint8_t t1677_f2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1677_f2_DefaultValue = 
{
	&t1677_f2_FieldInfo, { (char*)&t1677_f2_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1677_f3_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1677_f3_DefaultValue = 
{
	&t1677_f3_FieldInfo, { (char*)&t1677_f3_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1677_f4_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1677_f4_DefaultValue = 
{
	&t1677_f4_FieldInfo, { (char*)&t1677_f4_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1677_f5_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1677_f5_DefaultValue = 
{
	&t1677_f5_FieldInfo, { (char*)&t1677_f5_DefaultValueData, &t348_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1677_FDVs[] = 
{
	&t1677_f2_DefaultValue,
	&t1677_f3_DefaultValue,
	&t1677_f4_DefaultValue,
	&t1677_f5_DefaultValue,
	NULL
};
static MethodInfo* t1677_MIs[] =
{
	NULL
};
static MethodInfo* t1677_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1677_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1677_0_0_0;
extern Il2CppType t1677_1_0_0;
extern TypeInfo t1678_TI;
TypeInfo t1677_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnityType", "", t1677_MIs, NULL, t1677_FIs, NULL, &t49_TI, NULL, &t1678_TI, &t348_TI, NULL, t1677_VT, &EmptyCustomAttributesCache, &t348_TI, &t1677_0_0_0, &t1677_1_0_0, t1677_IOs, NULL, NULL, t1677_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1677)+ sizeof (Il2CppObject), sizeof (uint8_t), sizeof(uint8_t), 0, 0, -1, 259, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t1678.h"
#ifndef _MSC_VER
#else
#endif

#include "t1626.h"
extern TypeInfo t1626_TI;
extern TypeInfo t1142_TI;
#include "t929MD.h"
#include "t1142MD.h"
#include "t1626MD.h"
extern Il2CppType t1678_0_0_0;
extern MethodInfo m3996_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m7462_MI;
extern MethodInfo m8144_MI;
extern MethodInfo m7568_MI;
extern MethodInfo m7567_MI;
extern MethodInfo m7473_MI;
extern MethodInfo m4035_MI;
extern MethodInfo m7474_MI;


extern MethodInfo m9584_MI;
 void m9584 (t1678 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t7* L_0 = m3994(p0, (t7*) &_stringLiteral1066, &m3994_MI);
		__this->f0 = L_0;
		int32_t L_1 = m3996(p0, (t7*) &_stringLiteral2259, &m3996_MI);
		__this->f1 = (((uint8_t)L_1));
		t7* L_2 = m3994(p0, (t7*) &_stringLiteral1532, &m3994_MI);
		__this->f2 = L_2;
		return;
	}
}
 void m9585 (t29 * __this, t42 * p0, t733 * p1, t735  p2, MethodInfo* method){
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, p0);
		m3997(p1, (t7*) &_stringLiteral1066, L_0, &m3997_MI);
		m3984(p1, (t7*) &_stringLiteral2259, 4, &m3984_MI);
		t929 * L_1 = (t929 *)VirtFuncInvoker0< t929 * >::Invoke(&m4034_MI, p0);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7462_MI, L_1);
		m3997(p1, (t7*) &_stringLiteral1532, L_2, &m3997_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t1678_0_0_0), &m1554_MI);
		m8144(p1, L_3, &m8144_MI);
		return;
	}
}
extern MethodInfo m9586_MI;
 void m9586 (t29 * __this, t1626 * p0, t733 * p1, t735  p2, MethodInfo* method){
	{
		m3997(p1, (t7*) &_stringLiteral1066, NULL, &m3997_MI);
		m3984(p1, (t7*) &_stringLiteral2259, 2, &m3984_MI);
		t42 * L_0 = m1430(p0, &m1430_MI);
		t929 * L_1 = (t929 *)VirtFuncInvoker0< t929 * >::Invoke(&m4034_MI, L_0);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7462_MI, L_1);
		m3997(p1, (t7*) &_stringLiteral1532, L_2, &m3997_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t1678_0_0_0), &m1554_MI);
		m8144(p1, L_3, &m8144_MI);
		return;
	}
}
extern MethodInfo m9587_MI;
 void m9587 (t29 * __this, t1142 * p0, t733 * p1, t735  p2, MethodInfo* method){
	{
		t7* L_0 = m7568(p0, &m7568_MI);
		m3997(p1, (t7*) &_stringLiteral1066, L_0, &m3997_MI);
		m3984(p1, (t7*) &_stringLiteral2259, 5, &m3984_MI);
		t929 * L_1 = m7567(p0, &m7567_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7462_MI, L_1);
		m3997(p1, (t7*) &_stringLiteral1532, L_2, &m3997_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t1678_0_0_0), &m1554_MI);
		m8144(p1, L_3, &m8144_MI);
		return;
	}
}
extern MethodInfo m9588_MI;
 void m9588 (t1678 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m9589_MI;
 t29 * m9589 (t1678 * __this, t735  p0, MethodInfo* method){
	t929 * V_0 = {0};
	t929 * V_1 = {0};
	uint8_t V_2 = {0};
	{
		uint8_t L_0 = (__this->f1);
		V_2 = L_0;
		if (((uint8_t)(V_2-2)) == 0)
		{
			goto IL_003e;
		}
		if (((uint8_t)(V_2-2)) == 1)
		{
			goto IL_0069;
		}
		if (((uint8_t)(V_2-2)) == 2)
		{
			goto IL_0025;
		}
		if (((uint8_t)(V_2-2)) == 3)
		{
			goto IL_0044;
		}
		if (((uint8_t)(V_2-2)) == 4)
		{
			goto IL_005d;
		}
	}
	{
		goto IL_0069;
	}

IL_0025:
	{
		t7* L_1 = (__this->f2);
		t929 * L_2 = m7473(NULL, L_1, &m7473_MI);
		V_0 = L_2;
		t7* L_3 = (__this->f0);
		t42 * L_4 = (t42 *)VirtFuncInvoker1< t42 *, t7* >::Invoke(&m4035_MI, V_0, L_3);
		return L_4;
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1626_TI));
		return (((t1626_SFs*)InitializedTypeInfo(&t1626_TI)->static_fields)->f0);
	}

IL_0044:
	{
		t7* L_5 = (__this->f2);
		t929 * L_6 = m7473(NULL, L_5, &m7473_MI);
		V_1 = L_6;
		t7* L_7 = (__this->f0);
		t1142 * L_8 = (t1142 *)VirtFuncInvoker1< t1142 *, t7* >::Invoke(&m7474_MI, V_1, L_7);
		return L_8;
	}

IL_005d:
	{
		t7* L_9 = (__this->f0);
		t929 * L_10 = m7473(NULL, L_9, &m7473_MI);
		return L_10;
	}

IL_0069:
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral2260, &m6079_MI);
		t345 * L_12 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_12, L_11, &m3988_MI);
		il2cpp_codegen_raise_exception(L_12);
	}
}
// Metadata Definition System.UnitySerializationHolder
extern Il2CppType t7_0_0_1;
FieldInfo t1678_f0_FieldInfo = 
{
	"_data", &t7_0_0_1, &t1678_TI, offsetof(t1678, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1677_0_0_1;
FieldInfo t1678_f1_FieldInfo = 
{
	"_unityType", &t1677_0_0_1, &t1678_TI, offsetof(t1678, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1678_f2_FieldInfo = 
{
	"_assemblyName", &t7_0_0_1, &t1678_TI, offsetof(t1678, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1678_FIs[] =
{
	&t1678_f0_FieldInfo,
	&t1678_f1_FieldInfo,
	&t1678_f2_FieldInfo,
	NULL
};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1678_m9584_ParameterInfos[] = 
{
	{"info", 0, 134224522, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"ctx", 1, 134224523, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9584_MI = 
{
	".ctor", (methodPointerType)&m9584, &t1678_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1678_m9584_ParameterInfos, &EmptyCustomAttributesCache, 6273, 0, 255, 2, false, false, 5436, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1678_m9585_ParameterInfos[] = 
{
	{"instance", 0, 134224524, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"info", 1, 134224525, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"ctx", 2, 134224526, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9585_MI = 
{
	"GetTypeData", (methodPointerType)&m9585, &t1678_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t735, t1678_m9585_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5437, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1626_0_0_0;
extern Il2CppType t1626_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1678_m9586_ParameterInfos[] = 
{
	{"instance", 0, 134224527, &EmptyCustomAttributesCache, &t1626_0_0_0},
	{"info", 1, 134224528, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"ctx", 2, 134224529, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9586_MI = 
{
	"GetDBNullData", (methodPointerType)&m9586, &t1678_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t735, t1678_m9586_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5438, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1142_0_0_0;
extern Il2CppType t1142_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1678_m9587_ParameterInfos[] = 
{
	{"instance", 0, 134224530, &EmptyCustomAttributesCache, &t1142_0_0_0},
	{"info", 1, 134224531, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"ctx", 2, 134224532, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9587_MI = 
{
	"GetModuleData", (methodPointerType)&m9587, &t1678_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t735, t1678_m9587_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 5439, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1678_m9588_ParameterInfos[] = 
{
	{"info", 0, 134224533, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224534, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9588_MI = 
{
	"GetObjectData", (methodPointerType)&m9588, &t1678_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1678_m9588_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 6, 2, false, false, 5440, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t735_0_0_0;
static ParameterInfo t1678_m9589_ParameterInfos[] = 
{
	{"context", 0, 134224535, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9589_MI = 
{
	"GetRealObject", (methodPointerType)&m9589, &t1678_TI, &t29_0_0_0, RuntimeInvoker_t29_t735, t1678_m9589_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 1, false, false, 5441, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1678_MIs[] =
{
	&m9584_MI,
	&m9585_MI,
	&m9586_MI,
	&m9587_MI,
	&m9588_MI,
	&m9589_MI,
	NULL
};
extern TypeInfo t1677_TI;
static TypeInfo* t1678_TI__nestedTypes[2] =
{
	&t1677_TI,
	NULL
};
static MethodInfo* t1678_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m9588_MI,
	&m9589_MI,
	&m9588_MI,
	&m9589_MI,
};
extern TypeInfo t2024_TI;
static TypeInfo* t1678_ITIs[] = 
{
	&t374_TI,
	&t2024_TI,
};
static Il2CppInterfaceOffsetPair t1678_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t2024_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1678_1_0_0;
struct t1678;
TypeInfo t1678_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnitySerializationHolder", "System", t1678_MIs, NULL, t1678_FIs, NULL, &t29_TI, t1678_TI__nestedTypes, NULL, &t1678_TI, t1678_ITIs, t1678_VT, &EmptyCustomAttributesCache, &t1678_TI, &t1678_0_0_0, &t1678_1_0_0, t1678_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1678), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 0, 3, 0, 1, 8, 2, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m9593_MI;
extern MethodInfo m2928_MI;
extern MethodInfo m4203_MI;
extern MethodInfo m9600_MI;
extern MethodInfo m9601_MI;
extern MethodInfo m9606_MI;
extern MethodInfo m2943_MI;
extern MethodInfo m9592_MI;
extern MethodInfo m4286_MI;


extern MethodInfo m9590_MI;
 void m9590 (t760 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m9593(__this, 2, 0, 0, (-1), (-1), &m9593_MI);
		return;
	}
}
extern MethodInfo m9591_MI;
 void m9591 (t760 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	t446* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		m1331(__this, &m1331_MI);
		V_2 = (-1);
		V_3 = (-1);
		V_4 = (-1);
		V_5 = (-1);
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral259, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_001e:
	{
		t200* L_1 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_1, 0)) = (uint16_t)((int32_t)46);
		t446* L_2 = m2928(p0, L_1, &m2928_MI);
		V_1 = L_2;
		V_0 = (((int32_t)(((t20 *)V_1)->max_length)));
		if ((((int32_t)V_0) < ((int32_t)2)))
		{
			goto IL_003c;
		}
	}
	{
		if ((((int32_t)V_0) <= ((int32_t)4)))
		{
			goto IL_004c;
		}
	}

IL_003c:
	{
		t7* L_3 = m6079(NULL, (t7*) &_stringLiteral2261, &m6079_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_4, L_3, &m1935_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_004c:
	{
		if ((((int32_t)V_0) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_5 = 0;
		int32_t L_6 = m4203(NULL, (*(t7**)(t7**)SZArrayLdElema(V_1, L_5)), &m4203_MI);
		V_2 = L_6;
	}

IL_0059:
	{
		if ((((int32_t)V_0) <= ((int32_t)1)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_7 = 1;
		int32_t L_8 = m4203(NULL, (*(t7**)(t7**)SZArrayLdElema(V_1, L_7)), &m4203_MI);
		V_3 = L_8;
	}

IL_0066:
	{
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_9 = 2;
		int32_t L_10 = m4203(NULL, (*(t7**)(t7**)SZArrayLdElema(V_1, L_9)), &m4203_MI);
		V_4 = L_10;
	}

IL_0074:
	{
		if ((((int32_t)V_0) <= ((int32_t)3)))
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_11 = 3;
		int32_t L_12 = m4203(NULL, (*(t7**)(t7**)SZArrayLdElema(V_1, L_11)), &m4203_MI);
		V_5 = L_12;
	}

IL_0082:
	{
		m9593(__this, V_0, V_2, V_3, V_4, V_5, &m9593_MI);
		return;
	}
}
extern MethodInfo m3999_MI;
 void m3999 (t760 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m9593(__this, 2, p0, p1, 0, 0, &m9593_MI);
		return;
	}
}
 void m9592 (t760 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m9593(__this, 4, p0, p1, p2, p3, &m9593_MI);
		return;
	}
}
 void m9593 (t760 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method){
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral2262, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		__this->f1 = p1;
		if ((((int32_t)p2) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral2263, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0025:
	{
		__this->f2 = p2;
		if ((((uint32_t)p0) != ((uint32_t)2)))
		{
			goto IL_003f;
		}
	}
	{
		__this->f3 = (-1);
		__this->f4 = (-1);
		return;
	}

IL_003f:
	{
		if ((((int32_t)p3) >= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		t915 * L_2 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_2, (t7*) &_stringLiteral2264, &m3975_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_004f:
	{
		__this->f3 = p3;
		if ((((uint32_t)p0) != ((uint32_t)3)))
		{
			goto IL_0063;
		}
	}
	{
		__this->f4 = (-1);
		return;
	}

IL_0063:
	{
		if ((((int32_t)p4) >= ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		t915 * L_3 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_3, (t7*) &_stringLiteral2265, &m3975_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0073:
	{
		__this->f4 = p4;
		return;
	}
}
extern MethodInfo m9594_MI;
 int32_t m9594 (t760 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m9595_MI;
 int32_t m9595 (t760 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m9596_MI;
 int32_t m9596 (t760 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m9597_MI;
 int32_t m9597 (t760 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m9598_MI;
 int32_t m9598 (t760 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_0005;
		}
	}
	{
		return 1;
	}

IL_0005:
	{
		if (((t760 *)IsInst(p0, InitializedTypeInfo(&t760_TI))))
		{
			goto IL_001d;
		}
	}
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral2266, &m6079_MI);
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, L_0, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t760 * >::Invoke(&m9600_MI, __this, ((t760 *)Castclass(p0, InitializedTypeInfo(&t760_TI))));
		return L_2;
	}
}
extern MethodInfo m9599_MI;
 bool m9599 (t760 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t760 * >::Invoke(&m9601_MI, __this, ((t760 *)IsInst(p0, InitializedTypeInfo(&t760_TI))));
		return L_0;
	}
}
 int32_t m9600 (t760 * __this, t760 * p0, MethodInfo* method){
	{
		bool L_0 = m9605(NULL, p0, (t760 *)NULL, &m9605_MI);
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 1;
	}

IL_000b:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = (p0->f1);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		return 1;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		int32_t L_4 = (p0->f1);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_002b;
		}
	}
	{
		return (-1);
	}

IL_002b:
	{
		int32_t L_5 = (__this->f2);
		int32_t L_6 = (p0->f2);
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_003b;
		}
	}
	{
		return 1;
	}

IL_003b:
	{
		int32_t L_7 = (__this->f2);
		int32_t L_8 = (p0->f2);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_004b;
		}
	}
	{
		return (-1);
	}

IL_004b:
	{
		int32_t L_9 = (__this->f3);
		int32_t L_10 = (p0->f3);
		if ((((int32_t)L_9) <= ((int32_t)L_10)))
		{
			goto IL_005b;
		}
	}
	{
		return 1;
	}

IL_005b:
	{
		int32_t L_11 = (__this->f3);
		int32_t L_12 = (p0->f3);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_006b;
		}
	}
	{
		return (-1);
	}

IL_006b:
	{
		int32_t L_13 = (__this->f4);
		int32_t L_14 = (p0->f4);
		if ((((int32_t)L_13) <= ((int32_t)L_14)))
		{
			goto IL_007b;
		}
	}
	{
		return 1;
	}

IL_007b:
	{
		int32_t L_15 = (__this->f4);
		int32_t L_16 = (p0->f4);
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_008b;
		}
	}
	{
		return (-1);
	}

IL_008b:
	{
		return 0;
	}
}
 bool m9601 (t760 * __this, t760 * p0, MethodInfo* method){
	int32_t G_B6_0 = 0;
	{
		bool L_0 = m9606(NULL, p0, (t760 *)NULL, &m9606_MI);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_1 = (p0->f1);
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_1) != ((uint32_t)L_2)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_3 = (p0->f2);
		int32_t L_4 = (__this->f2);
		if ((((uint32_t)L_3) != ((uint32_t)L_4)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_5 = (p0->f3);
		int32_t L_6 = (__this->f3);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_7 = (p0->f4);
		int32_t L_8 = (__this->f4);
		G_B6_0 = ((((int32_t)L_7) == ((int32_t)L_8))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return G_B6_0;
	}
}
extern MethodInfo m9602_MI;
 int32_t m9602 (t760 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		int32_t L_1 = (__this->f3);
		int32_t L_2 = (__this->f2);
		int32_t L_3 = (__this->f1);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_2<<(int32_t)8))))|(int32_t)L_3));
	}
}
 t7* m9603 (t760 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		int32_t* L_0 = &(__this->f1);
		t7* L_1 = m2943(L_0, &m2943_MI);
		int32_t* L_2 = &(__this->f2);
		t7* L_3 = m2943(L_2, &m2943_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m1685(NULL, L_1, (t7*) &_stringLiteral52, L_3, &m1685_MI);
		V_0 = L_4;
		int32_t L_5 = (__this->f3);
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t* L_6 = &(__this->f3);
		t7* L_7 = m2943(L_6, &m2943_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_8 = m1685(NULL, V_0, (t7*) &_stringLiteral52, L_7, &m1685_MI);
		V_0 = L_8;
	}

IL_0041:
	{
		int32_t L_9 = (__this->f4);
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_0061;
		}
	}
	{
		int32_t* L_10 = &(__this->f4);
		t7* L_11 = m2943(L_10, &m2943_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_12 = m1685(NULL, V_0, (t7*) &_stringLiteral52, L_11, &m1685_MI);
		V_0 = L_12;
	}

IL_0061:
	{
		return V_0;
	}
}
extern MethodInfo m9604_MI;
 t760 * m9604 (t29 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	uint16_t V_7 = 0x0;
	int32_t V_8 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		V_3 = 0;
		V_4 = 1;
		V_5 = (-1);
		if (p0)
		{
			goto IL_001b;
		}
	}
	{
		t760 * L_0 = (t760 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t760_TI));
		m9592(L_0, 0, 0, 0, 0, &m9592_MI);
		return L_0;
	}

IL_001b:
	{
		V_6 = 0;
		goto IL_00a1;
	}

IL_0023:
	{
		uint16_t L_1 = m1741(p0, V_6, &m1741_MI);
		V_7 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_2 = m4222(NULL, V_7, &m4222_MI);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		if ((((int32_t)V_5) >= ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		V_5 = ((uint16_t)(V_7-((int32_t)48)));
		goto IL_0051;
	}

IL_0044:
	{
		V_5 = ((int32_t)(((int32_t)((int32_t)V_5*(int32_t)((int32_t)10)))+((uint16_t)(V_7-((int32_t)48)))));
	}

IL_0051:
	{
		goto IL_0094;
	}

IL_0053:
	{
		if ((((int32_t)V_5) < ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		V_8 = V_4;
		if (((int32_t)(V_8-1)) == 0)
		{
			goto IL_0077;
		}
		if (((int32_t)(V_8-1)) == 1)
		{
			goto IL_007c;
		}
		if (((int32_t)(V_8-1)) == 2)
		{
			goto IL_0081;
		}
		if (((int32_t)(V_8-1)) == 3)
		{
			goto IL_0086;
		}
	}
	{
		goto IL_008b;
	}

IL_0077:
	{
		V_0 = V_5;
		goto IL_008b;
	}

IL_007c:
	{
		V_1 = V_5;
		goto IL_008b;
	}

IL_0081:
	{
		V_2 = V_5;
		goto IL_008b;
	}

IL_0086:
	{
		V_3 = V_5;
		goto IL_008b;
	}

IL_008b:
	{
		V_5 = (-1);
		V_4 = ((int32_t)(V_4+1));
	}

IL_0094:
	{
		if ((((uint32_t)V_4) != ((uint32_t)5)))
		{
			goto IL_009b;
		}
	}
	{
		goto IL_00ae;
	}

IL_009b:
	{
		V_6 = ((int32_t)(V_6+1));
	}

IL_00a1:
	{
		int32_t L_3 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_6) < ((int32_t)L_3)))
		{
			goto IL_0023;
		}
	}

IL_00ae:
	{
		if ((((int32_t)V_5) < ((int32_t)0)))
		{
			goto IL_00e6;
		}
	}
	{
		V_8 = V_4;
		if (((int32_t)(V_8-1)) == 0)
		{
			goto IL_00d2;
		}
		if (((int32_t)(V_8-1)) == 1)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_8-1)) == 2)
		{
			goto IL_00dc;
		}
		if (((int32_t)(V_8-1)) == 3)
		{
			goto IL_00e1;
		}
	}
	{
		goto IL_00e6;
	}

IL_00d2:
	{
		V_0 = V_5;
		goto IL_00e6;
	}

IL_00d7:
	{
		V_1 = V_5;
		goto IL_00e6;
	}

IL_00dc:
	{
		V_2 = V_5;
		goto IL_00e6;
	}

IL_00e1:
	{
		V_3 = V_5;
		goto IL_00e6;
	}

IL_00e6:
	{
		t760 * L_4 = (t760 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t760_TI));
		m9592(L_4, V_0, V_1, V_2, V_3, &m9592_MI);
		return L_4;
	}
}
 bool m9605 (t29 * __this, t760 * p0, t760 * p1, MethodInfo* method){
	{
		bool L_0 = m4286(NULL, p0, p1, &m4286_MI);
		return L_0;
	}
}
 bool m9606 (t29 * __this, t760 * p0, t760 * p1, MethodInfo* method){
	{
		bool L_0 = m4286(NULL, p0, p1, &m4286_MI);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// Metadata Definition System.Version
extern Il2CppType t44_0_0_32849;
FieldInfo t760_f0_FieldInfo = 
{
	"UNDEFINED", &t44_0_0_32849, &t760_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t760_f1_FieldInfo = 
{
	"_Major", &t44_0_0_1, &t760_TI, offsetof(t760, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t760_f2_FieldInfo = 
{
	"_Minor", &t44_0_0_1, &t760_TI, offsetof(t760, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t760_f3_FieldInfo = 
{
	"_Build", &t44_0_0_1, &t760_TI, offsetof(t760, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t760_f4_FieldInfo = 
{
	"_Revision", &t44_0_0_1, &t760_TI, offsetof(t760, f4), &EmptyCustomAttributesCache};
static FieldInfo* t760_FIs[] =
{
	&t760_f0_FieldInfo,
	&t760_f1_FieldInfo,
	&t760_f2_FieldInfo,
	&t760_f3_FieldInfo,
	&t760_f4_FieldInfo,
	NULL
};
static const int32_t t760_f0_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t760_f0_DefaultValue = 
{
	&t760_f0_FieldInfo, { (char*)&t760_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t760_FDVs[] = 
{
	&t760_f0_DefaultValue,
	NULL
};
static PropertyInfo t760____Build_PropertyInfo = 
{
	&t760_TI, "Build", &m9594_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t760____Major_PropertyInfo = 
{
	&t760_TI, "Major", &m9595_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t760____Minor_PropertyInfo = 
{
	&t760_TI, "Minor", &m9596_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t760____Revision_PropertyInfo = 
{
	&t760_TI, "Revision", &m9597_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t760_PIs[] =
{
	&t760____Build_PropertyInfo,
	&t760____Major_PropertyInfo,
	&t760____Minor_PropertyInfo,
	&t760____Revision_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9590_MI = 
{
	".ctor", (methodPointerType)&m9590, &t760_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5442, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t760_m9591_ParameterInfos[] = 
{
	{"version", 0, 134224536, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9591_MI = 
{
	".ctor", (methodPointerType)&m9591, &t760_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t760_m9591_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5443, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t760_m3999_ParameterInfos[] = 
{
	{"major", 0, 134224537, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minor", 1, 134224538, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3999_MI = 
{
	".ctor", (methodPointerType)&m3999, &t760_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t760_m3999_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5444, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t760_m9592_ParameterInfos[] = 
{
	{"major", 0, 134224539, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minor", 1, 134224540, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"build", 2, 134224541, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"revision", 3, 134224542, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9592_MI = 
{
	".ctor", (methodPointerType)&m9592, &t760_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t44_t44, t760_m9592_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 4, false, false, 5445, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t760_m9593_ParameterInfos[] = 
{
	{"defined", 0, 134224543, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"major", 1, 134224544, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minor", 2, 134224545, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"build", 3, 134224546, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"revision", 4, 134224547, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9593_MI = 
{
	"CheckedSet", (methodPointerType)&m9593, &t760_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t44_t44_t44, t760_m9593_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 5, false, false, 5446, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9594_MI = 
{
	"get_Build", (methodPointerType)&m9594, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5447, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9595_MI = 
{
	"get_Major", (methodPointerType)&m9595, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5448, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9596_MI = 
{
	"get_Minor", (methodPointerType)&m9596, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5449, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9597_MI = 
{
	"get_Revision", (methodPointerType)&m9597, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 5450, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t760_m9598_ParameterInfos[] = 
{
	{"version", 0, 134224548, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9598_MI = 
{
	"CompareTo", (methodPointerType)&m9598, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t760_m9598_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, false, 5451, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t760_m9599_ParameterInfos[] = 
{
	{"obj", 0, 134224549, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9599_MI = 
{
	"Equals", (methodPointerType)&m9599, &t760_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t760_m9599_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 5452, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t760_0_0_0;
static ParameterInfo t760_m9600_ParameterInfos[] = 
{
	{"value", 0, 134224550, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9600_MI = 
{
	"CompareTo", (methodPointerType)&m9600, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t760_m9600_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 1, false, false, 5453, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t760_0_0_0;
static ParameterInfo t760_m9601_ParameterInfos[] = 
{
	{"obj", 0, 134224551, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9601_MI = 
{
	"Equals", (methodPointerType)&m9601, &t760_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t760_m9601_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, false, 5454, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9602_MI = 
{
	"GetHashCode", (methodPointerType)&m9602, &t760_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 5455, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9603_MI = 
{
	"ToString", (methodPointerType)&m9603, &t760_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 5456, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t760_m9604_ParameterInfos[] = 
{
	{"info", 0, 134224552, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t760_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9604_MI = 
{
	"CreateFromString", (methodPointerType)&m9604, &t760_TI, &t760_0_0_0, RuntimeInvoker_t29_t29, t760_m9604_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 5457, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t760_0_0_0;
extern Il2CppType t760_0_0_0;
static ParameterInfo t760_m9605_ParameterInfos[] = 
{
	{"v1", 0, 134224553, &EmptyCustomAttributesCache, &t760_0_0_0},
	{"v2", 1, 134224554, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9605_MI = 
{
	"op_Equality", (methodPointerType)&m9605, &t760_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t760_m9605_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5458, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t760_0_0_0;
extern Il2CppType t760_0_0_0;
static ParameterInfo t760_m9606_ParameterInfos[] = 
{
	{"v1", 0, 134224555, &EmptyCustomAttributesCache, &t760_0_0_0},
	{"v2", 1, 134224556, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9606_MI = 
{
	"op_Inequality", (methodPointerType)&m9606, &t760_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t760_m9606_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 5459, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t760_MIs[] =
{
	&m9590_MI,
	&m9591_MI,
	&m3999_MI,
	&m9592_MI,
	&m9593_MI,
	&m9594_MI,
	&m9595_MI,
	&m9596_MI,
	&m9597_MI,
	&m9598_MI,
	&m9599_MI,
	&m9600_MI,
	&m9601_MI,
	&m9602_MI,
	&m9603_MI,
	&m9604_MI,
	&m9605_MI,
	&m9606_MI,
	NULL
};
static MethodInfo* t760_VT[] =
{
	&m9599_MI,
	&m46_MI,
	&m9602_MI,
	&m9603_MI,
	&m9598_MI,
	&m9600_MI,
	&m9601_MI,
};
extern TypeInfo t2103_TI;
extern TypeInfo t2104_TI;
static TypeInfo* t760_ITIs[] = 
{
	&t290_TI,
	&t373_TI,
	&t2103_TI,
	&t2104_TI,
};
static Il2CppInterfaceOffsetPair t760_IOs[] = 
{
	{ &t290_TI, 4},
	{ &t373_TI, 5},
	{ &t2103_TI, 5},
	{ &t2104_TI, 6},
};
void t760_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t760__CustomAttributeCache = {
1,
NULL,
&t760_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t760_1_0_0;
struct t760;
extern CustomAttributesCache t760__CustomAttributeCache;
TypeInfo t760_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Version", "System", t760_MIs, t760_PIs, t760_FIs, NULL, &t29_TI, NULL, NULL, &t760_TI, t760_ITIs, t760_VT, &t760__CustomAttributeCache, &t760_TI, &t760_0_0_0, &t760_1_0_0, t760_IOs, NULL, NULL, t760_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t760), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 18, 4, 5, 0, 0, 7, 4, 4};
#include "t1476.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1476_TI;
#include "t1476MD.h"

#include "t1407.h"
#include "t1408.h"
#include "t1407MD.h"
extern MethodInfo m7738_MI;
extern MethodInfo m9609_MI;
extern MethodInfo m9611_MI;
extern MethodInfo m3987_MI;
extern MethodInfo m7739_MI;
extern MethodInfo m7740_MI;
extern MethodInfo m9613_MI;
extern MethodInfo m3983_MI;
extern MethodInfo m9612_MI;


extern MethodInfo m9607_MI;
 void m9607 (t1476 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m9608_MI;
 void m9608 (t1476 * __this, t29 * p0, MethodInfo* method){
	{
		m9609(__this, p0, 0, &m9609_MI);
		return;
	}
}
 void m9609 (t1476 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p1;
		m9611(__this, p0, &m9611_MI);
		return;
	}
}
extern MethodInfo m9610_MI;
 void m9610 (t1476 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t29 * V_0 = {0};
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		bool L_1 = m3987(p0, (t7*) &_stringLiteral2267, &m3987_MI);
		__this->f0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		t29 * L_3 = m3985(p0, (t7*) &_stringLiteral2268, L_2, &m3985_MI);
		V_0 = L_3;
		m9611(__this, V_0, &m9611_MI);
		return;
	}
}
 void m9611 (t1476 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = (__this->f0);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		t1407  L_1 = m7739(NULL, p0, 1, &m7739_MI);
		__this->f1 = L_1;
		goto IL_0024;
	}

IL_0017:
	{
		t1407  L_2 = m7739(NULL, p0, 0, &m7739_MI);
		__this->f1 = L_2;
	}

IL_0024:
	{
		return;
	}
}
 t29 * m9612 (t1476 * __this, MethodInfo* method){
	{
		t1407 * L_0 = &(__this->f1);
		t29 * L_1 = m7738(L_0, &m7738_MI);
		return L_1;
	}
}
 bool m9613 (t1476 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m9614_MI;
 void m9614 (t1476 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		t1407 * L_0 = &(__this->f1);
		m7740(L_0, &m7740_MI);
		// IL_000b: leave.s IL_0014
		leaveInstructions[0] = 0x14; // 1
		THROW_SENTINEL(IL_0014);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_000d;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_000d;
	}

IL_000d:
	{ // begin finally (depth: 1)
		m46(__this, &m46_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x14:
				goto IL_0014;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0014:
	{
		return;
	}
}
extern MethodInfo m9615_MI;
 void m9615 (t1476 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m9613_MI, __this);
		m3983(p0, (t7*) &_stringLiteral2267, L_1, &m3983_MI);
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m9612_MI, __this);
		m3997(p0, (t7*) &_stringLiteral2268, L_2, &m3997_MI);
		// IL_0030: leave.s IL_0041
		goto IL_0041;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t295_TI, e.ex->object.klass))
			goto IL_0032;
		throw e;
	}

IL_0032:
	{ // begin catch(System.Exception)
		m3997(p0, (t7*) &_stringLiteral2268, NULL, &m3997_MI);
		// IL_003f: leave.s IL_0041
		goto IL_0041;
	} // end catch (depth: 1)

IL_0041:
	{
		return;
	}
}
// Metadata Definition System.WeakReference
extern Il2CppType t40_0_0_1;
FieldInfo t1476_f0_FieldInfo = 
{
	"isLongReference", &t40_0_0_1, &t1476_TI, offsetof(t1476, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1407_0_0_1;
FieldInfo t1476_f1_FieldInfo = 
{
	"gcHandle", &t1407_0_0_1, &t1476_TI, offsetof(t1476, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1476_FIs[] =
{
	&t1476_f0_FieldInfo,
	&t1476_f1_FieldInfo,
	NULL
};
static PropertyInfo t1476____Target_PropertyInfo = 
{
	&t1476_TI, "Target", &m9612_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1476____TrackResurrection_PropertyInfo = 
{
	&t1476_TI, "TrackResurrection", &m9613_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1476_PIs[] =
{
	&t1476____Target_PropertyInfo,
	&t1476____TrackResurrection_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9607_MI = 
{
	".ctor", (methodPointerType)&m9607, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 5460, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1476_m9608_ParameterInfos[] = 
{
	{"target", 0, 134224557, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9608_MI = 
{
	".ctor", (methodPointerType)&m9608, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1476_m9608_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 5461, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1476_m9609_ParameterInfos[] = 
{
	{"target", 0, 134224558, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"trackResurrection", 1, 134224559, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m9609_MI = 
{
	".ctor", (methodPointerType)&m9609, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t1476_m9609_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 5462, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1476_m9610_ParameterInfos[] = 
{
	{"info", 0, 134224560, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224561, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9610_MI = 
{
	".ctor", (methodPointerType)&m9610, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1476_m9610_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 5463, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1476_m9611_ParameterInfos[] = 
{
	{"target", 0, 134224562, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9611_MI = 
{
	"AllocateHandle", (methodPointerType)&m9611, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1476_m9611_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 5464, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9612_MI = 
{
	"get_Target", (methodPointerType)&m9612, &t1476_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 5, 0, false, false, 5465, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m9613_MI = 
{
	"get_TrackResurrection", (methodPointerType)&m9613, &t1476_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 6, 0, false, false, 5466, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m9614_MI = 
{
	"Finalize", (methodPointerType)&m9614, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 1, 0, false, false, 5467, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1476_m9615_ParameterInfos[] = 
{
	{"info", 0, 134224563, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134224564, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m9615_MI = 
{
	"GetObjectData", (methodPointerType)&m9615, &t1476_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1476_m9615_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 2, false, false, 5468, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1476_MIs[] =
{
	&m9607_MI,
	&m9608_MI,
	&m9609_MI,
	&m9610_MI,
	&m9611_MI,
	&m9612_MI,
	&m9613_MI,
	&m9614_MI,
	&m9615_MI,
	NULL
};
static MethodInfo* t1476_VT[] =
{
	&m1321_MI,
	&m9614_MI,
	&m1322_MI,
	&m1332_MI,
	&m9615_MI,
	&m9612_MI,
	&m9613_MI,
	&m9615_MI,
};
static TypeInfo* t1476_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1476_IOs[] = 
{
	{ &t374_TI, 4},
};
void t1476_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1476__CustomAttributeCache = {
1,
NULL,
&t1476_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1476_0_0_0;
extern Il2CppType t1476_1_0_0;
struct t1476;
extern CustomAttributesCache t1476__CustomAttributeCache;
TypeInfo t1476_TI = 
{
	&g_mscorlib_dll_Image, NULL, "WeakReference", "System", t1476_MIs, t1476_PIs, t1476_FIs, NULL, &t29_TI, NULL, NULL, &t1476_TI, t1476_ITIs, t1476_VT, &t1476__CustomAttributeCache, &t1476_TI, &t1476_0_0_0, &t1476_1_0_0, t1476_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1476), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, true, false, false, false, 9, 2, 2, 0, 0, 8, 1, 1};
#include "t1195.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1195_TI;
#include "t1195MD.h"

#include "t1196.h"
#include "t1197.h"
#include "t67.h"


extern MethodInfo m9616_MI;
 void m9616 (t1195 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9617_MI;
 bool m9617 (t1195 * __this, t1196 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9617((t1195 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
bool pinvoke_delegate_wrapper_t1195(Il2CppObject* delegate, t1196 * p0, int32_t p1)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(t1196 *, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t1196 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Mono.Math.BigInteger'."));

	// Marshaling of parameter 'p1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(_p0_marshaled, p1);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	return _return_value;
}
extern MethodInfo m9618_MI;
 t29 * m9618 (t1195 * __this, t1196 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t1197_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9619_MI;
 bool m9619 (t1195 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1195_m9616_ParameterInfos[] = 
{
	{"object", 0, 134224565, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224566, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9616_MI = 
{
	".ctor", (methodPointerType)&m9616, &t1195_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1195_m9616_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5469, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1196_0_0_0;
extern Il2CppType t1196_0_0_0;
extern Il2CppType t1197_0_0_0;
extern Il2CppType t1197_0_0_0;
static ParameterInfo t1195_m9617_ParameterInfos[] = 
{
	{"bi", 0, 134224567, &EmptyCustomAttributesCache, &t1196_0_0_0},
	{"confidence", 1, 134224568, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m9617_MI = 
{
	"Invoke", (methodPointerType)&m9617, &t1195_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t44, t1195_m9617_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5470, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1196_0_0_0;
extern Il2CppType t1197_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1195_m9618_ParameterInfos[] = 
{
	{"bi", 0, 134224569, &EmptyCustomAttributesCache, &t1196_0_0_0},
	{"confidence", 1, 134224570, &EmptyCustomAttributesCache, &t1197_0_0_0},
	{"callback", 2, 134224571, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224572, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9618_MI = 
{
	"BeginInvoke", (methodPointerType)&m9618, &t1195_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t1195_m9618_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5471, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t1195_m9619_ParameterInfos[] = 
{
	{"result", 0, 134224573, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9619_MI = 
{
	"EndInvoke", (methodPointerType)&m9619, &t1195_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1195_m9619_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5472, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1195_MIs[] =
{
	&m9616_MI,
	&m9617_MI,
	&m9618_MI,
	&m9619_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t1195_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9617_MI,
	&m9618_MI,
	&m9619_MI,
};
static Il2CppInterfaceOffsetPair t1195_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1195_0_0_0;
extern Il2CppType t1195_1_0_0;
extern TypeInfo t195_TI;
struct t1195;
TypeInfo t1195_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PrimalityTest", "Mono.Math.Prime", t1195_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1195_TI, NULL, t1195_VT, &EmptyCustomAttributesCache, &t1195_TI, &t1195_0_0_0, &t1195_1_0_0, t1195_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1195, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1195), 0, sizeof(methodPointerType), 0, 0, -1, 256, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1141.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1141_TI;
#include "t1141MD.h"



extern MethodInfo m9620_MI;
 void m9620 (t1141 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9621_MI;
 bool m9621 (t1141 * __this, t296 * p0, t29 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9621((t1141 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t296 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t296 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
bool pinvoke_delegate_wrapper_t1141(Il2CppObject* delegate, t296 * p0, t29 * p1)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(t296 *, t29 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t296 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Reflection.MemberInfo'."));

	// Marshaling of parameter 'p1' to native representation
	t29 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	return _return_value;
}
extern MethodInfo m9622_MI;
 t29 * m9622 (t1141 * __this, t296 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9623_MI;
 bool m9623 (t1141 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Reflection.MemberFilter
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1141_m9620_ParameterInfos[] = 
{
	{"object", 0, 134224574, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224575, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9620_MI = 
{
	".ctor", (methodPointerType)&m9620, &t1141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1141_m9620_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5473, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t296_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1141_m9621_ParameterInfos[] = 
{
	{"m", 0, 134224576, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"filterCriteria", 1, 134224577, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9621_MI = 
{
	"Invoke", (methodPointerType)&m9621, &t1141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1141_m9621_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5474, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t296_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1141_m9622_ParameterInfos[] = 
{
	{"m", 0, 134224578, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"filterCriteria", 1, 134224579, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134224580, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224581, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9622_MI = 
{
	"BeginInvoke", (methodPointerType)&m9622, &t1141_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1141_m9622_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5475, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1141_m9623_ParameterInfos[] = 
{
	{"result", 0, 134224582, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9623_MI = 
{
	"EndInvoke", (methodPointerType)&m9623, &t1141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1141_m9623_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5476, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1141_MIs[] =
{
	&m9620_MI,
	&m9621_MI,
	&m9622_MI,
	&m9623_MI,
	NULL
};
static MethodInfo* t1141_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9621_MI,
	&m9622_MI,
	&m9623_MI,
};
static Il2CppInterfaceOffsetPair t1141_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1141_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1141__CustomAttributeCache = {
1,
NULL,
&t1141_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1141_0_0_0;
extern Il2CppType t1141_1_0_0;
struct t1141;
extern CustomAttributesCache t1141__CustomAttributeCache;
TypeInfo t1141_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MemberFilter", "System.Reflection", t1141_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1141_TI, NULL, t1141_VT, &t1141__CustomAttributeCache, &t1141_TI, &t1141_0_0_0, &t1141_1_0_0, t1141_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1141, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1141), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1372.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1372_TI;
#include "t1372MD.h"



extern MethodInfo m9624_MI;
 void m9624 (t1372 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9625_MI;
 bool m9625 (t1372 * __this, t42 * p0, t29 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9625((t1372 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t42 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
bool pinvoke_delegate_wrapper_t1372(Il2CppObject* delegate, t42 * p0, t29 * p1)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(t42 *, t29 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t42 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Type'."));

	// Marshaling of parameter 'p1' to native representation
	t29 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	return _return_value;
}
extern MethodInfo m9626_MI;
 t29 * m9626 (t1372 * __this, t42 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9627_MI;
 bool m9627 (t1372 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Reflection.TypeFilter
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1372_m9624_ParameterInfos[] = 
{
	{"object", 0, 134224583, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224584, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9624_MI = 
{
	".ctor", (methodPointerType)&m9624, &t1372_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1372_m9624_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5477, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1372_m9625_ParameterInfos[] = 
{
	{"m", 0, 134224585, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"filterCriteria", 1, 134224586, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9625_MI = 
{
	"Invoke", (methodPointerType)&m9625, &t1372_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1372_m9625_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5478, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1372_m9626_ParameterInfos[] = 
{
	{"m", 0, 134224587, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"filterCriteria", 1, 134224588, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134224589, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224590, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9626_MI = 
{
	"BeginInvoke", (methodPointerType)&m9626, &t1372_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1372_m9626_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5479, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1372_m9627_ParameterInfos[] = 
{
	{"result", 0, 134224591, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9627_MI = 
{
	"EndInvoke", (methodPointerType)&m9627, &t1372_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1372_m9627_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5480, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1372_MIs[] =
{
	&m9624_MI,
	&m9625_MI,
	&m9626_MI,
	&m9627_MI,
	NULL
};
static MethodInfo* t1372_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9625_MI,
	&m9626_MI,
	&m9627_MI,
};
static Il2CppInterfaceOffsetPair t1372_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1372__CustomAttributeCache = {
1,
NULL,
&t1372_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1372_0_0_0;
extern Il2CppType t1372_1_0_0;
struct t1372;
extern CustomAttributesCache t1372__CustomAttributeCache;
TypeInfo t1372_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeFilter", "System.Reflection", t1372_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1372_TI, NULL, t1372_VT, &t1372__CustomAttributeCache, &t1372_TI, &t1372_0_0_0, &t1372_1_0_0, t1372_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1372, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1372), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1498.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1498_TI;
#include "t1498MD.h"

#include "t1448.h"


extern MethodInfo m9628_MI;
 void m9628 (t1498 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9629_MI;
 t29 * m9629 (t1498 * __this, t1451* p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9629((t1498 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, t1451* p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t1451* p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
t29 * pinvoke_delegate_wrapper_t1498(Il2CppObject* delegate, t1451* p0)
{
	typedef t29 * (STDCALL *native_function_ptr_type)(t1451*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t1451* _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Runtime.Remoting.Messaging.Header[]'."));

	// Native function invocation and marshaling of return value back from native representation
	t29 * _return_value = _il2cpp_pinvoke_func(_p0_marshaled);
	t29 * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling cleanup of parameter 'p0' native representation

	return __return_value_unmarshaled;
}
extern MethodInfo m9630_MI;
 t29 * m9630 (t1498 * __this, t1451* p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m9631_MI;
 t29 * m9631 (t1498 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Runtime.Remoting.Messaging.HeaderHandler
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1498_m9628_ParameterInfos[] = 
{
	{"object", 0, 134224592, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224593, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9628_MI = 
{
	".ctor", (methodPointerType)&m9628, &t1498_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1498_m9628_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5481, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1451_0_0_0;
extern Il2CppType t1451_0_0_0;
static ParameterInfo t1498_m9629_ParameterInfos[] = 
{
	{"headers", 0, 134224594, &EmptyCustomAttributesCache, &t1451_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9629_MI = 
{
	"Invoke", (methodPointerType)&m9629, &t1498_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1498_m9629_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 5482, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1451_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1498_m9630_ParameterInfos[] = 
{
	{"headers", 0, 134224595, &EmptyCustomAttributesCache, &t1451_0_0_0},
	{"callback", 1, 134224596, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134224597, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9630_MI = 
{
	"BeginInvoke", (methodPointerType)&m9630, &t1498_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1498_m9630_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 5483, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1498_m9631_ParameterInfos[] = 
{
	{"result", 0, 134224598, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9631_MI = 
{
	"EndInvoke", (methodPointerType)&m9631, &t1498_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1498_m9631_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5484, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1498_MIs[] =
{
	&m9628_MI,
	&m9629_MI,
	&m9630_MI,
	&m9631_MI,
	NULL
};
static MethodInfo* t1498_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9629_MI,
	&m9630_MI,
	&m9631_MI,
};
static Il2CppInterfaceOffsetPair t1498_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1498__CustomAttributeCache = {
1,
NULL,
&t1498_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1498_0_0_0;
extern Il2CppType t1498_1_0_0;
struct t1498;
extern CustomAttributesCache t1498__CustomAttributeCache;
TypeInfo t1498_TI = 
{
	&g_mscorlib_dll_Image, NULL, "HeaderHandler", "System.Runtime.Remoting.Messaging", t1498_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1498_TI, NULL, t1498_VT, &t1498__CustomAttributeCache, &t1498_TI, &t1498_0_0_0, &t1498_1_0_0, t1498_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1498, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1498), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1679.h"
extern Il2CppGenericContainer t1679_IGC;
extern TypeInfo t1679_gp_T_0_TI;
Il2CppGenericParamFull t1679_gp_T_0_TI_GenericParamFull = { { &t1679_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* t1679_IGPA[1] = 
{
	&t1679_gp_T_0_TI_GenericParamFull,
};
extern TypeInfo t1679_TI;
Il2CppGenericContainer t1679_IGC = { { NULL, NULL }, NULL, &t1679_TI, 1, 0, t1679_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1679_m10271_ParameterInfos[] = 
{
	{"object", 0, 134224599, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224600, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10271_MI = 
{
	".ctor", NULL, &t1679_TI, &t21_0_0_0, NULL, t1679_m10271_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5485, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1679_gp_0_0_0_0;
extern Il2CppType t1679_gp_0_0_0_0;
static ParameterInfo t1679_m10272_ParameterInfos[] = 
{
	{"obj", 0, 134224601, &EmptyCustomAttributesCache, &t1679_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10272_MI = 
{
	"Invoke", NULL, &t1679_TI, &t21_0_0_0, NULL, t1679_m10272_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 5486, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1679_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1679_m10273_ParameterInfos[] = 
{
	{"obj", 0, 134224602, &EmptyCustomAttributesCache, &t1679_gp_0_0_0_0},
	{"callback", 1, 134224603, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134224604, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m10273_MI = 
{
	"BeginInvoke", NULL, &t1679_TI, &t66_0_0_0, NULL, t1679_m10273_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 5487, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1679_m10274_ParameterInfos[] = 
{
	{"result", 0, 134224605, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10274_MI = 
{
	"EndInvoke", NULL, &t1679_TI, &t21_0_0_0, NULL, t1679_m10274_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5488, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1679_MIs[] =
{
	&m10271_MI,
	&m10272_MI,
	&m10273_MI,
	&m10274_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1679_0_0_0;
extern Il2CppType t1679_1_0_0;
struct t1679;
TypeInfo t1679_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Action`1", "System", t1679_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1679_TI, NULL, NULL, NULL, NULL, &t1679_0_0_0, &t1679_1_0_0, NULL, NULL, &t1679_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t1619.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1619_TI;
#include "t1619MD.h"



extern MethodInfo m9632_MI;
 void m9632 (t1619 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9633_MI;
 void m9633 (t1619 * __this, t446* p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9633((t1619 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t446* p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t446* p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
void pinvoke_delegate_wrapper_t1619(Il2CppObject* delegate, t446* p0)
{
	typedef void (STDCALL *native_function_ptr_type)(char**);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	char** _p0_marshaled = { 0 };
	_p0_marshaled = il2cpp_codegen_marshal_string_array(p0);

	// Native function invocation
	_il2cpp_pinvoke_func(_p0_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation
	if (p0 != NULL) il2cpp_codegen_marshal_free_string_array((void**)_p0_marshaled, ((Il2CppCodeGenArray*)p0)->max_length);
	_p0_marshaled = NULL;

}
extern MethodInfo m9634_MI;
 t29 * m9634 (t1619 * __this, t446* p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m9635_MI;
 void m9635 (t1619 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition System.AppDomainInitializer
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1619_m9632_ParameterInfos[] = 
{
	{"object", 0, 134224606, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224607, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9632_MI = 
{
	".ctor", (methodPointerType)&m9632, &t1619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1619_m9632_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5489, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t446_0_0_0;
static ParameterInfo t1619_m9633_ParameterInfos[] = 
{
	{"args", 0, 134224608, &EmptyCustomAttributesCache, &t446_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9633_MI = 
{
	"Invoke", (methodPointerType)&m9633, &t1619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1619_m9633_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 5490, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t446_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1619_m9634_ParameterInfos[] = 
{
	{"args", 0, 134224609, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"callback", 1, 134224610, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134224611, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9634_MI = 
{
	"BeginInvoke", (methodPointerType)&m9634, &t1619_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1619_m9634_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 5491, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1619_m9635_ParameterInfos[] = 
{
	{"result", 0, 134224612, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9635_MI = 
{
	"EndInvoke", (methodPointerType)&m9635, &t1619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1619_m9635_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5492, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1619_MIs[] =
{
	&m9632_MI,
	&m9633_MI,
	&m9634_MI,
	&m9635_MI,
	NULL
};
static MethodInfo* t1619_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9633_MI,
	&m9634_MI,
	&m9635_MI,
};
static Il2CppInterfaceOffsetPair t1619_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1619_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1619__CustomAttributeCache = {
1,
NULL,
&t1619_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1619_0_0_0;
extern Il2CppType t1619_1_0_0;
struct t1619;
extern CustomAttributesCache t1619__CustomAttributeCache;
TypeInfo t1619_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AppDomainInitializer", "System", t1619_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1619_TI, NULL, t1619_VT, &t1619__CustomAttributeCache, &t1619_TI, &t1619_0_0_0, &t1619_1_0_0, t1619_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1619, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1619), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1614.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1614_TI;
#include "t1614MD.h"

#include "t1623.h"


extern MethodInfo m9636_MI;
 void m9636 (t1614 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9637_MI;
 void m9637 (t1614 * __this, t29 * p0, t1623 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9637((t1614 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t1623 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t1623 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t1623 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
void pinvoke_delegate_wrapper_t1614(Il2CppObject* delegate, t29 * p0, t1623 * p1)
{
	typedef void (STDCALL *native_function_ptr_type)(t29 *, t1623 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling of parameter 'p1' to native representation
	t1623 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.AssemblyLoadEventArgs'."));

	// Native function invocation
	_il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

}
extern MethodInfo m9638_MI;
 t29 * m9638 (t1614 * __this, t29 * p0, t1623 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9639_MI;
 void m9639 (t1614 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition System.AssemblyLoadEventHandler
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1614_m9636_ParameterInfos[] = 
{
	{"object", 0, 134224613, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224614, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9636_MI = 
{
	".ctor", (methodPointerType)&m9636, &t1614_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1614_m9636_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5493, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1623_0_0_0;
extern Il2CppType t1623_0_0_0;
static ParameterInfo t1614_m9637_ParameterInfos[] = 
{
	{"sender", 0, 134224615, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"args", 1, 134224616, &EmptyCustomAttributesCache, &t1623_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9637_MI = 
{
	"Invoke", (methodPointerType)&m9637, &t1614_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1614_m9637_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5494, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1623_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1614_m9638_ParameterInfos[] = 
{
	{"sender", 0, 134224617, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"args", 1, 134224618, &EmptyCustomAttributesCache, &t1623_0_0_0},
	{"callback", 2, 134224619, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224620, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9638_MI = 
{
	"BeginInvoke", (methodPointerType)&m9638, &t1614_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1614_m9638_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5495, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1614_m9639_ParameterInfos[] = 
{
	{"result", 0, 134224621, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9639_MI = 
{
	"EndInvoke", (methodPointerType)&m9639, &t1614_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1614_m9639_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5496, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1614_MIs[] =
{
	&m9636_MI,
	&m9637_MI,
	&m9638_MI,
	&m9639_MI,
	NULL
};
static MethodInfo* t1614_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9637_MI,
	&m9638_MI,
	&m9639_MI,
};
static Il2CppInterfaceOffsetPair t1614_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1614_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1614__CustomAttributeCache = {
1,
NULL,
&t1614_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1614_0_0_0;
extern Il2CppType t1614_1_0_0;
struct t1614;
extern CustomAttributesCache t1614__CustomAttributeCache;
TypeInfo t1614_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyLoadEventHandler", "System", t1614_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1614_TI, NULL, t1614_VT, &t1614__CustomAttributeCache, &t1614_TI, &t1614_0_0_0, &t1614_1_0_0, t1614_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1614, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1614), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1680.h"
extern Il2CppGenericContainer t1680_IGC;
extern TypeInfo t1680_gp_T_0_TI;
Il2CppGenericParamFull t1680_gp_T_0_TI_GenericParamFull = { { &t1680_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* t1680_IGPA[1] = 
{
	&t1680_gp_T_0_TI_GenericParamFull,
};
extern TypeInfo t1680_TI;
Il2CppGenericContainer t1680_IGC = { { NULL, NULL }, NULL, &t1680_TI, 1, 0, t1680_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1680_m10275_ParameterInfos[] = 
{
	{"object", 0, 134224622, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224623, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10275_MI = 
{
	".ctor", NULL, &t1680_TI, &t21_0_0_0, NULL, t1680_m10275_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5497, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1680_gp_0_0_0_0;
extern Il2CppType t1680_gp_0_0_0_0;
extern Il2CppType t1680_gp_0_0_0_0;
static ParameterInfo t1680_m10276_ParameterInfos[] = 
{
	{"x", 0, 134224624, &EmptyCustomAttributesCache, &t1680_gp_0_0_0_0},
	{"y", 1, 134224625, &EmptyCustomAttributesCache, &t1680_gp_0_0_0_0},
};
extern Il2CppType t44_0_0_0;
MethodInfo m10276_MI = 
{
	"Invoke", NULL, &t1680_TI, &t44_0_0_0, NULL, t1680_m10276_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5498, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1680_gp_0_0_0_0;
extern Il2CppType t1680_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1680_m10277_ParameterInfos[] = 
{
	{"x", 0, 134224626, &EmptyCustomAttributesCache, &t1680_gp_0_0_0_0},
	{"y", 1, 134224627, &EmptyCustomAttributesCache, &t1680_gp_0_0_0_0},
	{"callback", 2, 134224628, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224629, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m10277_MI = 
{
	"BeginInvoke", NULL, &t1680_TI, &t66_0_0_0, NULL, t1680_m10277_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5499, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1680_m10278_ParameterInfos[] = 
{
	{"result", 0, 134224630, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
MethodInfo m10278_MI = 
{
	"EndInvoke", NULL, &t1680_TI, &t44_0_0_0, NULL, t1680_m10278_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5500, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1680_MIs[] =
{
	&m10275_MI,
	&m10276_MI,
	&m10277_MI,
	&m10278_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1680_0_0_0;
extern Il2CppType t1680_1_0_0;
struct t1680;
TypeInfo t1680_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t1680_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1680_TI, NULL, NULL, NULL, NULL, &t1680_0_0_0, &t1680_1_0_0, NULL, NULL, &t1680_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t1681.h"
extern Il2CppGenericContainer t1681_IGC;
extern TypeInfo t1681_gp_TInput_0_TI;
Il2CppGenericParamFull t1681_gp_TInput_0_TI_GenericParamFull = { { &t1681_IGC, 0}, {NULL, "TInput", 0, 0, NULL} };
extern TypeInfo t1681_gp_TOutput_1_TI;
Il2CppGenericParamFull t1681_gp_TOutput_1_TI_GenericParamFull = { { &t1681_IGC, 1}, {NULL, "TOutput", 0, 0, NULL} };
static Il2CppGenericParamFull* t1681_IGPA[2] = 
{
	&t1681_gp_TInput_0_TI_GenericParamFull,
	&t1681_gp_TOutput_1_TI_GenericParamFull,
};
extern TypeInfo t1681_TI;
Il2CppGenericContainer t1681_IGC = { { NULL, NULL }, NULL, &t1681_TI, 2, 0, t1681_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1681_m10279_ParameterInfos[] = 
{
	{"object", 0, 134224631, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224632, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10279_MI = 
{
	".ctor", NULL, &t1681_TI, &t21_0_0_0, NULL, t1681_m10279_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5501, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1681_gp_0_0_0_0;
extern Il2CppType t1681_gp_0_0_0_0;
static ParameterInfo t1681_m10280_ParameterInfos[] = 
{
	{"input", 0, 134224633, &EmptyCustomAttributesCache, &t1681_gp_0_0_0_0},
};
extern Il2CppType t1681_gp_1_0_0_0;
MethodInfo m10280_MI = 
{
	"Invoke", NULL, &t1681_TI, &t1681_gp_1_0_0_0, NULL, t1681_m10280_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 5502, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1681_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1681_m10281_ParameterInfos[] = 
{
	{"input", 0, 134224634, &EmptyCustomAttributesCache, &t1681_gp_0_0_0_0},
	{"callback", 1, 134224635, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134224636, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m10281_MI = 
{
	"BeginInvoke", NULL, &t1681_TI, &t66_0_0_0, NULL, t1681_m10281_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 5503, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1681_m10282_ParameterInfos[] = 
{
	{"result", 0, 134224637, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t1681_gp_1_0_0_0;
MethodInfo m10282_MI = 
{
	"EndInvoke", NULL, &t1681_TI, &t1681_gp_1_0_0_0, NULL, t1681_m10282_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5504, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1681_MIs[] =
{
	&m10279_MI,
	&m10280_MI,
	&m10281_MI,
	&m10282_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1681_0_0_0;
extern Il2CppType t1681_1_0_0;
struct t1681;
TypeInfo t1681_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Converter`2", "System", t1681_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1681_TI, NULL, NULL, NULL, NULL, &t1681_0_0_0, &t1681_1_0_0, NULL, NULL, &t1681_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t1616.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1616_TI;
#include "t1616MD.h"

#include "t999.h"


extern MethodInfo m9640_MI;
 void m9640 (t1616 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9641_MI;
 void m9641 (t1616 * __this, t29 * p0, t999 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9641((t1616 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t999 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t999 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t999 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
void pinvoke_delegate_wrapper_t1616(Il2CppObject* delegate, t29 * p0, t999 * p1)
{
	typedef void (STDCALL *native_function_ptr_type)(t29 *, t999 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling of parameter 'p1' to native representation
	t999 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.EventArgs'."));

	// Native function invocation
	_il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

}
extern MethodInfo m9642_MI;
 t29 * m9642 (t1616 * __this, t29 * p0, t999 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9643_MI;
 void m9643 (t1616 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition System.EventHandler
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1616_m9640_ParameterInfos[] = 
{
	{"object", 0, 134224638, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224639, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9640_MI = 
{
	".ctor", (methodPointerType)&m9640, &t1616_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1616_m9640_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5505, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t999_0_0_0;
extern Il2CppType t999_0_0_0;
static ParameterInfo t1616_m9641_ParameterInfos[] = 
{
	{"sender", 0, 134224640, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"e", 1, 134224641, &EmptyCustomAttributesCache, &t999_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9641_MI = 
{
	"Invoke", (methodPointerType)&m9641, &t1616_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1616_m9641_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5506, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t999_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1616_m9642_ParameterInfos[] = 
{
	{"sender", 0, 134224642, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"e", 1, 134224643, &EmptyCustomAttributesCache, &t999_0_0_0},
	{"callback", 2, 134224644, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224645, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9642_MI = 
{
	"BeginInvoke", (methodPointerType)&m9642, &t1616_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1616_m9642_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5507, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1616_m9643_ParameterInfos[] = 
{
	{"result", 0, 134224646, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9643_MI = 
{
	"EndInvoke", (methodPointerType)&m9643, &t1616_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1616_m9643_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5508, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1616_MIs[] =
{
	&m9640_MI,
	&m9641_MI,
	&m9642_MI,
	&m9643_MI,
	NULL
};
static MethodInfo* t1616_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9641_MI,
	&m9642_MI,
	&m9643_MI,
};
static Il2CppInterfaceOffsetPair t1616_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1616__CustomAttributeCache = {
1,
NULL,
&t1616_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1616_0_0_0;
extern Il2CppType t1616_1_0_0;
struct t1616;
extern CustomAttributesCache t1616__CustomAttributeCache;
TypeInfo t1616_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EventHandler", "System", t1616_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1616_TI, NULL, t1616_VT, &t1616__CustomAttributeCache, &t1616_TI, &t1616_0_0_0, &t1616_1_0_0, t1616_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1616, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1616), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1682.h"
extern Il2CppGenericContainer t1682_IGC;
extern TypeInfo t1682_gp_T_0_TI;
Il2CppGenericParamFull t1682_gp_T_0_TI_GenericParamFull = { { &t1682_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* t1682_IGPA[1] = 
{
	&t1682_gp_T_0_TI_GenericParamFull,
};
extern TypeInfo t1682_TI;
Il2CppGenericContainer t1682_IGC = { { NULL, NULL }, NULL, &t1682_TI, 1, 0, t1682_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1682_m10283_ParameterInfos[] = 
{
	{"object", 0, 134224647, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224648, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10283_MI = 
{
	".ctor", NULL, &t1682_TI, &t21_0_0_0, NULL, t1682_m10283_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5509, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1682_gp_0_0_0_0;
extern Il2CppType t1682_gp_0_0_0_0;
static ParameterInfo t1682_m10284_ParameterInfos[] = 
{
	{"obj", 0, 134224649, &EmptyCustomAttributesCache, &t1682_gp_0_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m10284_MI = 
{
	"Invoke", NULL, &t1682_TI, &t40_0_0_0, NULL, t1682_m10284_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 5510, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1682_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1682_m10285_ParameterInfos[] = 
{
	{"obj", 0, 134224650, &EmptyCustomAttributesCache, &t1682_gp_0_0_0_0},
	{"callback", 1, 134224651, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134224652, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m10285_MI = 
{
	"BeginInvoke", NULL, &t1682_TI, &t66_0_0_0, NULL, t1682_m10285_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 5511, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1682_m10286_ParameterInfos[] = 
{
	{"result", 0, 134224653, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m10286_MI = 
{
	"EndInvoke", NULL, &t1682_TI, &t40_0_0_0, NULL, t1682_m10286_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5512, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1682_MIs[] =
{
	&m10283_MI,
	&m10284_MI,
	&m10285_MI,
	&m10286_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1682_0_0_0;
extern Il2CppType t1682_1_0_0;
struct t1682;
TypeInfo t1682_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t1682_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1682_TI, NULL, NULL, NULL, NULL, &t1682_0_0_0, &t1682_1_0_0, NULL, NULL, &t1682_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t1615.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1615_TI;
#include "t1615MD.h"



extern MethodInfo m9644_MI;
 void m9644 (t1615 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9645_MI;
 t929 * m9645 (t1615 * __this, t29 * p0, t1668 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9645((t1615 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t929 * (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t1668 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t929 * (*FunctionPointerType) (t29 * __this, t29 * p0, t1668 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t929 * (*FunctionPointerType) (t29 * __this, t1668 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
t929 * pinvoke_delegate_wrapper_t1615(Il2CppObject* delegate, t29 * p0, t1668 * p1)
{
	typedef t929 * (STDCALL *native_function_ptr_type)(t29 *, t1668 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling of parameter 'p1' to native representation
	t1668 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.ResolveEventArgs'."));

	// Native function invocation and marshaling of return value back from native representation
	t929 * _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);
	t929 * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Reflection.Assembly'."));

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	return __return_value_unmarshaled;
}
extern MethodInfo m9646_MI;
 t29 * m9646 (t1615 * __this, t29 * p0, t1668 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9647_MI;
 t929 * m9647 (t1615 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t929 *)__result;
}
// Metadata Definition System.ResolveEventHandler
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1615_m9644_ParameterInfos[] = 
{
	{"object", 0, 134224654, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224655, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9644_MI = 
{
	".ctor", (methodPointerType)&m9644, &t1615_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1615_m9644_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5513, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1668_0_0_0;
static ParameterInfo t1615_m9645_ParameterInfos[] = 
{
	{"sender", 0, 134224656, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"args", 1, 134224657, &EmptyCustomAttributesCache, &t1668_0_0_0},
};
extern Il2CppType t929_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9645_MI = 
{
	"Invoke", (methodPointerType)&m9645, &t1615_TI, &t929_0_0_0, RuntimeInvoker_t29_t29_t29, t1615_m9645_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5514, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1668_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1615_m9646_ParameterInfos[] = 
{
	{"sender", 0, 134224658, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"args", 1, 134224659, &EmptyCustomAttributesCache, &t1668_0_0_0},
	{"callback", 2, 134224660, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224661, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9646_MI = 
{
	"BeginInvoke", (methodPointerType)&m9646, &t1615_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1615_m9646_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5515, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1615_m9647_ParameterInfos[] = 
{
	{"result", 0, 134224662, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t929_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9647_MI = 
{
	"EndInvoke", (methodPointerType)&m9647, &t1615_TI, &t929_0_0_0, RuntimeInvoker_t29_t29, t1615_m9647_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5516, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1615_MIs[] =
{
	&m9644_MI,
	&m9645_MI,
	&m9646_MI,
	&m9647_MI,
	NULL
};
static MethodInfo* t1615_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9645_MI,
	&m9646_MI,
	&m9647_MI,
};
static Il2CppInterfaceOffsetPair t1615_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1615__CustomAttributeCache = {
1,
NULL,
&t1615_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1615_0_0_0;
extern Il2CppType t1615_1_0_0;
struct t1615;
extern CustomAttributesCache t1615__CustomAttributeCache;
TypeInfo t1615_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ResolveEventHandler", "System", t1615_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1615_TI, NULL, t1615_VT, &t1615__CustomAttributeCache, &t1615_TI, &t1615_0_0_0, &t1615_1_0_0, t1615_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1615, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1615), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1617.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1617_TI;
#include "t1617MD.h"



extern MethodInfo m9648_MI;
 void m9648 (t1617 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m9649_MI;
 void m9649 (t1617 * __this, t29 * p0, t1676 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m9649((t1617 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t1676 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t1676 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t1676 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
void pinvoke_delegate_wrapper_t1617(Il2CppObject* delegate, t29 * p0, t1676 * p1)
{
	typedef void (STDCALL *native_function_ptr_type)(t29 *, t1676 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling of parameter 'p1' to native representation
	t1676 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.UnhandledExceptionEventArgs'."));

	// Native function invocation
	_il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

}
extern MethodInfo m9650_MI;
 t29 * m9650 (t1617 * __this, t29 * p0, t1676 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m9651_MI;
 void m9651 (t1617 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition System.UnhandledExceptionEventHandler
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1617_m9648_ParameterInfos[] = 
{
	{"object", 0, 134224663, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134224664, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m9648_MI = 
{
	".ctor", (methodPointerType)&m9648, &t1617_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1617_m9648_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 5517, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1676_0_0_0;
static ParameterInfo t1617_m9649_ParameterInfos[] = 
{
	{"sender", 0, 134224665, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"e", 1, 134224666, &EmptyCustomAttributesCache, &t1676_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9649_MI = 
{
	"Invoke", (methodPointerType)&m9649, &t1617_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1617_m9649_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 5518, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1676_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1617_m9650_ParameterInfos[] = 
{
	{"sender", 0, 134224667, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"e", 1, 134224668, &EmptyCustomAttributesCache, &t1676_0_0_0},
	{"callback", 2, 134224669, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134224670, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9650_MI = 
{
	"BeginInvoke", (methodPointerType)&m9650, &t1617_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1617_m9650_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 5519, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1617_m9651_ParameterInfos[] = 
{
	{"result", 0, 134224671, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m9651_MI = 
{
	"EndInvoke", (methodPointerType)&m9651, &t1617_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1617_m9651_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 5520, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1617_MIs[] =
{
	&m9648_MI,
	&m9649_MI,
	&m9650_MI,
	&m9651_MI,
	NULL
};
static MethodInfo* t1617_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m9649_MI,
	&m9650_MI,
	&m9651_MI,
};
static Il2CppInterfaceOffsetPair t1617_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1617__CustomAttributeCache = {
1,
NULL,
&t1617_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1617_0_0_0;
extern Il2CppType t1617_1_0_0;
struct t1617;
extern CustomAttributesCache t1617__CustomAttributeCache;
TypeInfo t1617_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnhandledExceptionEventHandler", "System", t1617_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1617_TI, NULL, t1617_VT, &t1617__CustomAttributeCache, &t1617_TI, &t1617_0_0_0, &t1617_1_0_0, t1617_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1617, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1617), 0, sizeof(methodPointerType), 0, 0, -1, 8449, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1683.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1683_TI;
#include "t1683MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$56
void t1683_marshal(const t1683& unmarshaled, t1683_marshaled& marshaled)
{
}
void t1683_marshal_back(const t1683_marshaled& marshaled, t1683& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$56
void t1683_marshal_cleanup(t1683_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$56
static MethodInfo* t1683_MIs[] =
{
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
static MethodInfo* t1683_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1683_0_0_0;
extern Il2CppType t1683_1_0_0;
extern TypeInfo t1703_TI;
TypeInfo t1683_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$56", "", t1683_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1683_TI, NULL, t1683_VT, &EmptyCustomAttributesCache, &t1683_TI, &t1683_0_0_0, &t1683_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1683_marshal, (methodPointerType)t1683_marshal_back, (methodPointerType)t1683_marshal_cleanup, sizeof (t1683)+ sizeof (Il2CppObject), 0, sizeof(t1683_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1684.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1684_TI;
#include "t1684MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$24
void t1684_marshal(const t1684& unmarshaled, t1684_marshaled& marshaled)
{
}
void t1684_marshal_back(const t1684_marshaled& marshaled, t1684& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$24
void t1684_marshal_cleanup(t1684_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$24
static MethodInfo* t1684_MIs[] =
{
	NULL
};
static MethodInfo* t1684_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1684_0_0_0;
extern Il2CppType t1684_1_0_0;
TypeInfo t1684_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$24", "", t1684_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1684_TI, NULL, t1684_VT, &EmptyCustomAttributesCache, &t1684_TI, &t1684_0_0_0, &t1684_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1684_marshal, (methodPointerType)t1684_marshal_back, (methodPointerType)t1684_marshal_cleanup, sizeof (t1684)+ sizeof (Il2CppObject), 0, sizeof(t1684_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1685.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1685_TI;
#include "t1685MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$16
void t1685_marshal(const t1685& unmarshaled, t1685_marshaled& marshaled)
{
}
void t1685_marshal_back(const t1685_marshaled& marshaled, t1685& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$16
void t1685_marshal_cleanup(t1685_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
static MethodInfo* t1685_MIs[] =
{
	NULL
};
static MethodInfo* t1685_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1685_0_0_0;
extern Il2CppType t1685_1_0_0;
TypeInfo t1685_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$16", "", t1685_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1685_TI, NULL, t1685_VT, &EmptyCustomAttributesCache, &t1685_TI, &t1685_0_0_0, &t1685_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1685_marshal, (methodPointerType)t1685_marshal_back, (methodPointerType)t1685_marshal_cleanup, sizeof (t1685)+ sizeof (Il2CppObject), 0, sizeof(t1685_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1686.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1686_TI;
#include "t1686MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$120
void t1686_marshal(const t1686& unmarshaled, t1686_marshaled& marshaled)
{
}
void t1686_marshal_back(const t1686_marshaled& marshaled, t1686& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$120
void t1686_marshal_cleanup(t1686_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
static MethodInfo* t1686_MIs[] =
{
	NULL
};
static MethodInfo* t1686_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1686_0_0_0;
extern Il2CppType t1686_1_0_0;
TypeInfo t1686_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$120", "", t1686_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1686_TI, NULL, t1686_VT, &EmptyCustomAttributesCache, &t1686_TI, &t1686_0_0_0, &t1686_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1686_marshal, (methodPointerType)t1686_marshal_back, (methodPointerType)t1686_marshal_cleanup, sizeof (t1686)+ sizeof (Il2CppObject), 0, sizeof(t1686_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1687.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1687_TI;
#include "t1687MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$3132
void t1687_marshal(const t1687& unmarshaled, t1687_marshaled& marshaled)
{
}
void t1687_marshal_back(const t1687_marshaled& marshaled, t1687& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$3132
void t1687_marshal_cleanup(t1687_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
static MethodInfo* t1687_MIs[] =
{
	NULL
};
static MethodInfo* t1687_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1687_0_0_0;
extern Il2CppType t1687_1_0_0;
TypeInfo t1687_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$3132", "", t1687_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1687_TI, NULL, t1687_VT, &EmptyCustomAttributesCache, &t1687_TI, &t1687_0_0_0, &t1687_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1687_marshal, (methodPointerType)t1687_marshal_back, (methodPointerType)t1687_marshal_cleanup, sizeof (t1687)+ sizeof (Il2CppObject), 0, sizeof(t1687_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1688.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1688_TI;
#include "t1688MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$20
void t1688_marshal(const t1688& unmarshaled, t1688_marshaled& marshaled)
{
}
void t1688_marshal_back(const t1688_marshaled& marshaled, t1688& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$20
void t1688_marshal_cleanup(t1688_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
static MethodInfo* t1688_MIs[] =
{
	NULL
};
static MethodInfo* t1688_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1688_0_0_0;
extern Il2CppType t1688_1_0_0;
TypeInfo t1688_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$20", "", t1688_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1688_TI, NULL, t1688_VT, &EmptyCustomAttributesCache, &t1688_TI, &t1688_0_0_0, &t1688_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1688_marshal, (methodPointerType)t1688_marshal_back, (methodPointerType)t1688_marshal_cleanup, sizeof (t1688)+ sizeof (Il2CppObject), 0, sizeof(t1688_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1689.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1689_TI;
#include "t1689MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$32
void t1689_marshal(const t1689& unmarshaled, t1689_marshaled& marshaled)
{
}
void t1689_marshal_back(const t1689_marshaled& marshaled, t1689& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$32
void t1689_marshal_cleanup(t1689_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
static MethodInfo* t1689_MIs[] =
{
	NULL
};
static MethodInfo* t1689_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1689_0_0_0;
extern Il2CppType t1689_1_0_0;
TypeInfo t1689_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$32", "", t1689_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1689_TI, NULL, t1689_VT, &EmptyCustomAttributesCache, &t1689_TI, &t1689_0_0_0, &t1689_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1689_marshal, (methodPointerType)t1689_marshal_back, (methodPointerType)t1689_marshal_cleanup, sizeof (t1689)+ sizeof (Il2CppObject), 0, sizeof(t1689_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1690.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1690_TI;
#include "t1690MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$48
void t1690_marshal(const t1690& unmarshaled, t1690_marshaled& marshaled)
{
}
void t1690_marshal_back(const t1690_marshaled& marshaled, t1690& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$48
void t1690_marshal_cleanup(t1690_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
static MethodInfo* t1690_MIs[] =
{
	NULL
};
static MethodInfo* t1690_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1690_0_0_0;
extern Il2CppType t1690_1_0_0;
TypeInfo t1690_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$48", "", t1690_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1690_TI, NULL, t1690_VT, &EmptyCustomAttributesCache, &t1690_TI, &t1690_0_0_0, &t1690_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1690_marshal, (methodPointerType)t1690_marshal_back, (methodPointerType)t1690_marshal_cleanup, sizeof (t1690)+ sizeof (Il2CppObject), 0, sizeof(t1690_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1691.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1691_TI;
#include "t1691MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$64
void t1691_marshal(const t1691& unmarshaled, t1691_marshaled& marshaled)
{
}
void t1691_marshal_back(const t1691_marshaled& marshaled, t1691& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$64
void t1691_marshal_cleanup(t1691_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
static MethodInfo* t1691_MIs[] =
{
	NULL
};
static MethodInfo* t1691_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1691_0_0_0;
extern Il2CppType t1691_1_0_0;
TypeInfo t1691_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$64", "", t1691_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1691_TI, NULL, t1691_VT, &EmptyCustomAttributesCache, &t1691_TI, &t1691_0_0_0, &t1691_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1691_marshal, (methodPointerType)t1691_marshal_back, (methodPointerType)t1691_marshal_cleanup, sizeof (t1691)+ sizeof (Il2CppObject), 0, sizeof(t1691_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1692.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1692_TI;
#include "t1692MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void t1692_marshal(const t1692& unmarshaled, t1692_marshaled& marshaled)
{
}
void t1692_marshal_back(const t1692_marshaled& marshaled, t1692& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void t1692_marshal_cleanup(t1692_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
static MethodInfo* t1692_MIs[] =
{
	NULL
};
static MethodInfo* t1692_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1692_0_0_0;
extern Il2CppType t1692_1_0_0;
TypeInfo t1692_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$12", "", t1692_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1692_TI, NULL, t1692_VT, &EmptyCustomAttributesCache, &t1692_TI, &t1692_0_0_0, &t1692_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1692_marshal, (methodPointerType)t1692_marshal_back, (methodPointerType)t1692_marshal_cleanup, sizeof (t1692)+ sizeof (Il2CppObject), 0, sizeof(t1692_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1693.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1693_TI;
#include "t1693MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void t1693_marshal(const t1693& unmarshaled, t1693_marshaled& marshaled)
{
}
void t1693_marshal_back(const t1693_marshaled& marshaled, t1693& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void t1693_marshal_cleanup(t1693_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
static MethodInfo* t1693_MIs[] =
{
	NULL
};
static MethodInfo* t1693_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1693_0_0_0;
extern Il2CppType t1693_1_0_0;
TypeInfo t1693_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$136", "", t1693_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1693_TI, NULL, t1693_VT, &EmptyCustomAttributesCache, &t1693_TI, &t1693_0_0_0, &t1693_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1693_marshal, (methodPointerType)t1693_marshal_back, (methodPointerType)t1693_marshal_cleanup, sizeof (t1693)+ sizeof (Il2CppObject), 0, sizeof(t1693_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1694.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1694_TI;
#include "t1694MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$72
void t1694_marshal(const t1694& unmarshaled, t1694_marshaled& marshaled)
{
}
void t1694_marshal_back(const t1694_marshaled& marshaled, t1694& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$72
void t1694_marshal_cleanup(t1694_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$72
static MethodInfo* t1694_MIs[] =
{
	NULL
};
static MethodInfo* t1694_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1694_0_0_0;
extern Il2CppType t1694_1_0_0;
TypeInfo t1694_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$72", "", t1694_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1694_TI, NULL, t1694_VT, &EmptyCustomAttributesCache, &t1694_TI, &t1694_0_0_0, &t1694_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1694_marshal, (methodPointerType)t1694_marshal_back, (methodPointerType)t1694_marshal_cleanup, sizeof (t1694)+ sizeof (Il2CppObject), 0, sizeof(t1694_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1695.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1695_TI;
#include "t1695MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$124
void t1695_marshal(const t1695& unmarshaled, t1695_marshaled& marshaled)
{
}
void t1695_marshal_back(const t1695_marshaled& marshaled, t1695& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$124
void t1695_marshal_cleanup(t1695_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
static MethodInfo* t1695_MIs[] =
{
	NULL
};
static MethodInfo* t1695_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1695_0_0_0;
extern Il2CppType t1695_1_0_0;
TypeInfo t1695_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$124", "", t1695_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1695_TI, NULL, t1695_VT, &EmptyCustomAttributesCache, &t1695_TI, &t1695_0_0_0, &t1695_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1695_marshal, (methodPointerType)t1695_marshal_back, (methodPointerType)t1695_marshal_cleanup, sizeof (t1695)+ sizeof (Il2CppObject), 0, sizeof(t1695_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1696.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1696_TI;
#include "t1696MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$96
void t1696_marshal(const t1696& unmarshaled, t1696_marshaled& marshaled)
{
}
void t1696_marshal_back(const t1696_marshaled& marshaled, t1696& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$96
void t1696_marshal_cleanup(t1696_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$96
static MethodInfo* t1696_MIs[] =
{
	NULL
};
static MethodInfo* t1696_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1696_0_0_0;
extern Il2CppType t1696_1_0_0;
TypeInfo t1696_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$96", "", t1696_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1696_TI, NULL, t1696_VT, &EmptyCustomAttributesCache, &t1696_TI, &t1696_0_0_0, &t1696_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1696_marshal, (methodPointerType)t1696_marshal_back, (methodPointerType)t1696_marshal_cleanup, sizeof (t1696)+ sizeof (Il2CppObject), 0, sizeof(t1696_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1697.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1697_TI;
#include "t1697MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$2048
void t1697_marshal(const t1697& unmarshaled, t1697_marshaled& marshaled)
{
}
void t1697_marshal_back(const t1697_marshaled& marshaled, t1697& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$2048
void t1697_marshal_cleanup(t1697_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$2048
static MethodInfo* t1697_MIs[] =
{
	NULL
};
static MethodInfo* t1697_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1697_0_0_0;
extern Il2CppType t1697_1_0_0;
TypeInfo t1697_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$2048", "", t1697_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1697_TI, NULL, t1697_VT, &EmptyCustomAttributesCache, &t1697_TI, &t1697_0_0_0, &t1697_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1697_marshal, (methodPointerType)t1697_marshal_back, (methodPointerType)t1697_marshal_cleanup, sizeof (t1697)+ sizeof (Il2CppObject), 0, sizeof(t1697_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1698.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1698_TI;
#include "t1698MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
void t1698_marshal(const t1698& unmarshaled, t1698_marshaled& marshaled)
{
}
void t1698_marshal_back(const t1698_marshaled& marshaled, t1698& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$256
void t1698_marshal_cleanup(t1698_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
static MethodInfo* t1698_MIs[] =
{
	NULL
};
static MethodInfo* t1698_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1698_0_0_0;
extern Il2CppType t1698_1_0_0;
TypeInfo t1698_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$256", "", t1698_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1698_TI, NULL, t1698_VT, &EmptyCustomAttributesCache, &t1698_TI, &t1698_0_0_0, &t1698_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1698_marshal, (methodPointerType)t1698_marshal_back, (methodPointerType)t1698_marshal_cleanup, sizeof (t1698)+ sizeof (Il2CppObject), 0, sizeof(t1698_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1699.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1699_TI;
#include "t1699MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
void t1699_marshal(const t1699& unmarshaled, t1699_marshaled& marshaled)
{
}
void t1699_marshal_back(const t1699_marshaled& marshaled, t1699& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
void t1699_marshal_cleanup(t1699_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
static MethodInfo* t1699_MIs[] =
{
	NULL
};
static MethodInfo* t1699_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1699_0_0_0;
extern Il2CppType t1699_1_0_0;
TypeInfo t1699_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$1024", "", t1699_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1699_TI, NULL, t1699_VT, &EmptyCustomAttributesCache, &t1699_TI, &t1699_0_0_0, &t1699_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1699_marshal, (methodPointerType)t1699_marshal_back, (methodPointerType)t1699_marshal_cleanup, sizeof (t1699)+ sizeof (Il2CppObject), 0, sizeof(t1699_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1700.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1700_TI;
#include "t1700MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$640
void t1700_marshal(const t1700& unmarshaled, t1700_marshaled& marshaled)
{
}
void t1700_marshal_back(const t1700_marshaled& marshaled, t1700& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$640
void t1700_marshal_cleanup(t1700_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$640
static MethodInfo* t1700_MIs[] =
{
	NULL
};
static MethodInfo* t1700_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1700_0_0_0;
extern Il2CppType t1700_1_0_0;
TypeInfo t1700_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$640", "", t1700_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1700_TI, NULL, t1700_VT, &EmptyCustomAttributesCache, &t1700_TI, &t1700_0_0_0, &t1700_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1700_marshal, (methodPointerType)t1700_marshal_back, (methodPointerType)t1700_marshal_cleanup, sizeof (t1700)+ sizeof (Il2CppObject), 0, sizeof(t1700_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1701.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1701_TI;
#include "t1701MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$128
void t1701_marshal(const t1701& unmarshaled, t1701_marshaled& marshaled)
{
}
void t1701_marshal_back(const t1701_marshaled& marshaled, t1701& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$128
void t1701_marshal_cleanup(t1701_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
static MethodInfo* t1701_MIs[] =
{
	NULL
};
static MethodInfo* t1701_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1701_0_0_0;
extern Il2CppType t1701_1_0_0;
TypeInfo t1701_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$128", "", t1701_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1701_TI, NULL, t1701_VT, &EmptyCustomAttributesCache, &t1701_TI, &t1701_0_0_0, &t1701_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1701_marshal, (methodPointerType)t1701_marshal_back, (methodPointerType)t1701_marshal_cleanup, sizeof (t1701)+ sizeof (Il2CppObject), 0, sizeof(t1701_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1702.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1702_TI;
#include "t1702MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$52
void t1702_marshal(const t1702& unmarshaled, t1702_marshaled& marshaled)
{
}
void t1702_marshal_back(const t1702_marshaled& marshaled, t1702& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$52
void t1702_marshal_cleanup(t1702_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$52
static MethodInfo* t1702_MIs[] =
{
	NULL
};
static MethodInfo* t1702_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1702_0_0_0;
extern Il2CppType t1702_1_0_0;
TypeInfo t1702_TI = 
{
	&g_mscorlib_dll_Image, NULL, "$ArrayType$52", "", t1702_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1703_TI, &t1702_TI, NULL, t1702_VT, &EmptyCustomAttributesCache, &t1702_TI, &t1702_0_0_0, &t1702_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1702_marshal, (methodPointerType)t1702_marshal_back, (methodPointerType)t1702_marshal_cleanup, sizeof (t1702)+ sizeof (Il2CppObject), 0, sizeof(t1702_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1703.h"
#ifndef _MSC_VER
#else
#endif
#include "t1703MD.h"



// Metadata Definition <PrivateImplementationDetails>
extern Il2CppType t1683_0_0_275;
FieldInfo t1703_f0_FieldInfo = 
{
	"$$field-0", &t1683_0_0_275, &t1703_TI, offsetof(t1703_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1684_0_0_275;
FieldInfo t1703_f1_FieldInfo = 
{
	"$$field-1", &t1684_0_0_275, &t1703_TI, offsetof(t1703_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1684_0_0_275;
FieldInfo t1703_f2_FieldInfo = 
{
	"$$field-2", &t1684_0_0_275, &t1703_TI, offsetof(t1703_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1684_0_0_275;
FieldInfo t1703_f3_FieldInfo = 
{
	"$$field-3", &t1684_0_0_275, &t1703_TI, offsetof(t1703_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1684_0_0_275;
FieldInfo t1703_f4_FieldInfo = 
{
	"$$field-4", &t1684_0_0_275, &t1703_TI, offsetof(t1703_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1685_0_0_275;
FieldInfo t1703_f5_FieldInfo = 
{
	"$$field-5", &t1685_0_0_275, &t1703_TI, offsetof(t1703_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1685_0_0_275;
FieldInfo t1703_f6_FieldInfo = 
{
	"$$field-6", &t1685_0_0_275, &t1703_TI, offsetof(t1703_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t1687_0_0_275;
FieldInfo t1703_f7_FieldInfo = 
{
	"$$field-15", &t1687_0_0_275, &t1703_TI, offsetof(t1703_SFs, f7), &EmptyCustomAttributesCache};
extern Il2CppType t1688_0_0_275;
FieldInfo t1703_f8_FieldInfo = 
{
	"$$field-16", &t1688_0_0_275, &t1703_TI, offsetof(t1703_SFs, f8), &EmptyCustomAttributesCache};
extern Il2CppType t1689_0_0_275;
FieldInfo t1703_f9_FieldInfo = 
{
	"$$field-17", &t1689_0_0_275, &t1703_TI, offsetof(t1703_SFs, f9), &EmptyCustomAttributesCache};
extern Il2CppType t1690_0_0_275;
FieldInfo t1703_f10_FieldInfo = 
{
	"$$field-18", &t1690_0_0_275, &t1703_TI, offsetof(t1703_SFs, f10), &EmptyCustomAttributesCache};
extern Il2CppType t1691_0_0_275;
FieldInfo t1703_f11_FieldInfo = 
{
	"$$field-19", &t1691_0_0_275, &t1703_TI, offsetof(t1703_SFs, f11), &EmptyCustomAttributesCache};
extern Il2CppType t1691_0_0_275;
FieldInfo t1703_f12_FieldInfo = 
{
	"$$field-20", &t1691_0_0_275, &t1703_TI, offsetof(t1703_SFs, f12), &EmptyCustomAttributesCache};
extern Il2CppType t1691_0_0_275;
FieldInfo t1703_f13_FieldInfo = 
{
	"$$field-21", &t1691_0_0_275, &t1703_TI, offsetof(t1703_SFs, f13), &EmptyCustomAttributesCache};
extern Il2CppType t1691_0_0_275;
FieldInfo t1703_f14_FieldInfo = 
{
	"$$field-22", &t1691_0_0_275, &t1703_TI, offsetof(t1703_SFs, f14), &EmptyCustomAttributesCache};
extern Il2CppType t1692_0_0_275;
FieldInfo t1703_f15_FieldInfo = 
{
	"$$field-23", &t1692_0_0_275, &t1703_TI, offsetof(t1703_SFs, f15), &EmptyCustomAttributesCache};
extern Il2CppType t1692_0_0_275;
FieldInfo t1703_f16_FieldInfo = 
{
	"$$field-24", &t1692_0_0_275, &t1703_TI, offsetof(t1703_SFs, f16), &EmptyCustomAttributesCache};
extern Il2CppType t1692_0_0_275;
FieldInfo t1703_f17_FieldInfo = 
{
	"$$field-25", &t1692_0_0_275, &t1703_TI, offsetof(t1703_SFs, f17), &EmptyCustomAttributesCache};
extern Il2CppType t1685_0_0_275;
FieldInfo t1703_f18_FieldInfo = 
{
	"$$field-26", &t1685_0_0_275, &t1703_TI, offsetof(t1703_SFs, f18), &EmptyCustomAttributesCache};
extern Il2CppType t1693_0_0_275;
FieldInfo t1703_f19_FieldInfo = 
{
	"$$field-27", &t1693_0_0_275, &t1703_TI, offsetof(t1703_SFs, f19), &EmptyCustomAttributesCache};
extern Il2CppType t1694_0_0_275;
FieldInfo t1703_f20_FieldInfo = 
{
	"$$field-30", &t1694_0_0_275, &t1703_TI, offsetof(t1703_SFs, f20), &EmptyCustomAttributesCache};
extern Il2CppType t1688_0_0_275;
FieldInfo t1703_f21_FieldInfo = 
{
	"$$field-32", &t1688_0_0_275, &t1703_TI, offsetof(t1703_SFs, f21), &EmptyCustomAttributesCache};
extern Il2CppType t1691_0_0_275;
FieldInfo t1703_f22_FieldInfo = 
{
	"$$field-33", &t1691_0_0_275, &t1703_TI, offsetof(t1703_SFs, f22), &EmptyCustomAttributesCache};
extern Il2CppType t1695_0_0_275;
FieldInfo t1703_f23_FieldInfo = 
{
	"$$field-34", &t1695_0_0_275, &t1703_TI, offsetof(t1703_SFs, f23), &EmptyCustomAttributesCache};
extern Il2CppType t1689_0_0_275;
FieldInfo t1703_f24_FieldInfo = 
{
	"$$field-35", &t1689_0_0_275, &t1703_TI, offsetof(t1703_SFs, f24), &EmptyCustomAttributesCache};
extern Il2CppType t1696_0_0_275;
FieldInfo t1703_f25_FieldInfo = 
{
	"$$field-36", &t1696_0_0_275, &t1703_TI, offsetof(t1703_SFs, f25), &EmptyCustomAttributesCache};
extern Il2CppType t1697_0_0_275;
FieldInfo t1703_f26_FieldInfo = 
{
	"$$field-37", &t1697_0_0_275, &t1703_TI, offsetof(t1703_SFs, f26), &EmptyCustomAttributesCache};
extern Il2CppType t1683_0_0_275;
FieldInfo t1703_f27_FieldInfo = 
{
	"$$field-38", &t1683_0_0_275, &t1703_TI, offsetof(t1703_SFs, f27), &EmptyCustomAttributesCache};
extern Il2CppType t1685_0_0_275;
FieldInfo t1703_f28_FieldInfo = 
{
	"$$field-39", &t1685_0_0_275, &t1703_TI, offsetof(t1703_SFs, f28), &EmptyCustomAttributesCache};
extern Il2CppType t1690_0_0_275;
FieldInfo t1703_f29_FieldInfo = 
{
	"$$field-40", &t1690_0_0_275, &t1703_TI, offsetof(t1703_SFs, f29), &EmptyCustomAttributesCache};
extern Il2CppType t1697_0_0_275;
FieldInfo t1703_f30_FieldInfo = 
{
	"$$field-41", &t1697_0_0_275, &t1703_TI, offsetof(t1703_SFs, f30), &EmptyCustomAttributesCache};
extern Il2CppType t1697_0_0_275;
FieldInfo t1703_f31_FieldInfo = 
{
	"$$field-42", &t1697_0_0_275, &t1703_TI, offsetof(t1703_SFs, f31), &EmptyCustomAttributesCache};
extern Il2CppType t1698_0_0_275;
FieldInfo t1703_f32_FieldInfo = 
{
	"$$field-43", &t1698_0_0_275, &t1703_TI, offsetof(t1703_SFs, f32), &EmptyCustomAttributesCache};
extern Il2CppType t1698_0_0_275;
FieldInfo t1703_f33_FieldInfo = 
{
	"$$field-44", &t1698_0_0_275, &t1703_TI, offsetof(t1703_SFs, f33), &EmptyCustomAttributesCache};
extern Il2CppType t1686_0_0_275;
FieldInfo t1703_f34_FieldInfo = 
{
	"$$field-45", &t1686_0_0_275, &t1703_TI, offsetof(t1703_SFs, f34), &EmptyCustomAttributesCache};
extern Il2CppType t1698_0_0_275;
FieldInfo t1703_f35_FieldInfo = 
{
	"$$field-46", &t1698_0_0_275, &t1703_TI, offsetof(t1703_SFs, f35), &EmptyCustomAttributesCache};
extern Il2CppType t1698_0_0_275;
FieldInfo t1703_f36_FieldInfo = 
{
	"$$field-47", &t1698_0_0_275, &t1703_TI, offsetof(t1703_SFs, f36), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f37_FieldInfo = 
{
	"$$field-48", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f37), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f38_FieldInfo = 
{
	"$$field-49", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f38), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f39_FieldInfo = 
{
	"$$field-50", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f39), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f40_FieldInfo = 
{
	"$$field-51", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f40), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f41_FieldInfo = 
{
	"$$field-52", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f41), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f42_FieldInfo = 
{
	"$$field-53", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f42), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f43_FieldInfo = 
{
	"$$field-54", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f43), &EmptyCustomAttributesCache};
extern Il2CppType t1699_0_0_275;
FieldInfo t1703_f44_FieldInfo = 
{
	"$$field-55", &t1699_0_0_275, &t1703_TI, offsetof(t1703_SFs, f44), &EmptyCustomAttributesCache};
extern Il2CppType t1698_0_0_275;
FieldInfo t1703_f45_FieldInfo = 
{
	"$$field-56", &t1698_0_0_275, &t1703_TI, offsetof(t1703_SFs, f45), &EmptyCustomAttributesCache};
extern Il2CppType t1700_0_0_275;
FieldInfo t1703_f46_FieldInfo = 
{
	"$$field-57", &t1700_0_0_275, &t1703_TI, offsetof(t1703_SFs, f46), &EmptyCustomAttributesCache};
extern Il2CppType t1692_0_0_275;
FieldInfo t1703_f47_FieldInfo = 
{
	"$$field-60", &t1692_0_0_275, &t1703_TI, offsetof(t1703_SFs, f47), &EmptyCustomAttributesCache};
extern Il2CppType t1701_0_0_275;
FieldInfo t1703_f48_FieldInfo = 
{
	"$$field-62", &t1701_0_0_275, &t1703_TI, offsetof(t1703_SFs, f48), &EmptyCustomAttributesCache};
extern Il2CppType t1698_0_0_275;
FieldInfo t1703_f49_FieldInfo = 
{
	"$$field-63", &t1698_0_0_275, &t1703_TI, offsetof(t1703_SFs, f49), &EmptyCustomAttributesCache};
extern Il2CppType t1702_0_0_275;
FieldInfo t1703_f50_FieldInfo = 
{
	"$$field-64", &t1702_0_0_275, &t1703_TI, offsetof(t1703_SFs, f50), &EmptyCustomAttributesCache};
extern Il2CppType t1702_0_0_275;
FieldInfo t1703_f51_FieldInfo = 
{
	"$$field-65", &t1702_0_0_275, &t1703_TI, offsetof(t1703_SFs, f51), &EmptyCustomAttributesCache};
static FieldInfo* t1703_FIs[] =
{
	&t1703_f0_FieldInfo,
	&t1703_f1_FieldInfo,
	&t1703_f2_FieldInfo,
	&t1703_f3_FieldInfo,
	&t1703_f4_FieldInfo,
	&t1703_f5_FieldInfo,
	&t1703_f6_FieldInfo,
	&t1703_f7_FieldInfo,
	&t1703_f8_FieldInfo,
	&t1703_f9_FieldInfo,
	&t1703_f10_FieldInfo,
	&t1703_f11_FieldInfo,
	&t1703_f12_FieldInfo,
	&t1703_f13_FieldInfo,
	&t1703_f14_FieldInfo,
	&t1703_f15_FieldInfo,
	&t1703_f16_FieldInfo,
	&t1703_f17_FieldInfo,
	&t1703_f18_FieldInfo,
	&t1703_f19_FieldInfo,
	&t1703_f20_FieldInfo,
	&t1703_f21_FieldInfo,
	&t1703_f22_FieldInfo,
	&t1703_f23_FieldInfo,
	&t1703_f24_FieldInfo,
	&t1703_f25_FieldInfo,
	&t1703_f26_FieldInfo,
	&t1703_f27_FieldInfo,
	&t1703_f28_FieldInfo,
	&t1703_f29_FieldInfo,
	&t1703_f30_FieldInfo,
	&t1703_f31_FieldInfo,
	&t1703_f32_FieldInfo,
	&t1703_f33_FieldInfo,
	&t1703_f34_FieldInfo,
	&t1703_f35_FieldInfo,
	&t1703_f36_FieldInfo,
	&t1703_f37_FieldInfo,
	&t1703_f38_FieldInfo,
	&t1703_f39_FieldInfo,
	&t1703_f40_FieldInfo,
	&t1703_f41_FieldInfo,
	&t1703_f42_FieldInfo,
	&t1703_f43_FieldInfo,
	&t1703_f44_FieldInfo,
	&t1703_f45_FieldInfo,
	&t1703_f46_FieldInfo,
	&t1703_f47_FieldInfo,
	&t1703_f48_FieldInfo,
	&t1703_f49_FieldInfo,
	&t1703_f50_FieldInfo,
	&t1703_f51_FieldInfo,
	NULL
};
static const uint8_t t1703_f0_DefaultValueData[] = { 0x9, 0x0, 0xA, 0x0, 0xB, 0x0, 0xC, 0x0, 0xD, 0x0, 0x85, 0x0, 0x80, 0x16, 0x28, 0x20, 0x29, 0x20, 0x20, 0x0, 0xA0, 0x0, 0x0, 0x20, 0x1, 0x20, 0x2, 0x20, 0x3, 0x20, 0x4, 0x20, 0x5, 0x20, 0x6, 0x20, 0x7, 0x20, 0x8, 0x20, 0x9, 0x20, 0xA, 0x20, 0xB, 0x20, 0x0, 0x30, 0xFF, 0xFE, 0x2F, 0x20, 0x5F, 0x20, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f0_DefaultValue = 
{
	&t1703_f0_FieldInfo, { (char*)t1703_f0_DefaultValueData, &t1683_0_0_0 }};
static const uint8_t t1703_f1_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x30, 0x0, 0x0, 0x0, 0x4E, 0x0, 0x0, 0x0, 0xAC, 0x0, 0x0, 0x0, 0xF9, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f1_DefaultValue = 
{
	&t1703_f1_FieldInfo, { (char*)t1703_f1_DefaultValueData, &t1684_0_0_0 }};
static const uint8_t t1703_f2_DefaultValueData[] = { 0x0, 0x12, 0x0, 0x0, 0x0, 0x28, 0x0, 0x0, 0x0, 0x34, 0x0, 0x0, 0x0, 0xA0, 0x0, 0x0, 0xB0, 0xD7, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f2_DefaultValue = 
{
	&t1703_f2_FieldInfo, { (char*)t1703_f2_DefaultValueData, &t1684_0_0_0 }};
static const uint8_t t1703_f3_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x30, 0x0, 0x0, 0x0, 0x4E, 0x0, 0x0, 0x0, 0xAC, 0x0, 0x0, 0x0, 0xF9, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f3_DefaultValue = 
{
	&t1703_f3_FieldInfo, { (char*)t1703_f3_DefaultValueData, &t1684_0_0_0 }};
static const uint8_t t1703_f4_DefaultValueData[] = { 0x0, 0x12, 0x0, 0x0, 0x0, 0x28, 0x0, 0x0, 0x0, 0x34, 0x0, 0x0, 0x0, 0xA0, 0x0, 0x0, 0xB0, 0xD7, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f4_DefaultValue = 
{
	&t1703_f4_FieldInfo, { (char*)t1703_f4_DefaultValueData, &t1684_0_0_0 }};
static const uint8_t t1703_f5_DefaultValueData[] = { 0x0, 0xF, 0x0, 0x0, 0x0, 0x28, 0x0, 0x0, 0x0, 0x34, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f5_DefaultValue = 
{
	&t1703_f5_FieldInfo, { (char*)t1703_f5_DefaultValueData, &t1685_0_0_0 }};
static const uint8_t t1703_f6_DefaultValueData[] = { 0x0, 0x12, 0x0, 0x0, 0x0, 0x28, 0x0, 0x0, 0x0, 0x34, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f6_DefaultValue = 
{
	&t1703_f6_FieldInfo, { (char*)t1703_f6_DefaultValueData, &t1685_0_0_0 }};
static const uint8_t t1703_f7_DefaultValueData[] = { 0x2, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0, 0x5, 0x0, 0x0, 0x0, 0x7, 0x0, 0x0, 0x0, 0xB, 0x0, 0x0, 0x0, 0xD, 0x0, 0x0, 0x0, 0x11, 0x0, 0x0, 0x0, 0x13, 0x0, 0x0, 0x0, 0x17, 0x0, 0x0, 0x0, 0x1D, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x25, 0x0, 0x0, 0x0, 0x29, 0x0, 0x0, 0x0, 0x2B, 0x0, 0x0, 0x0, 0x2F, 0x0, 0x0, 0x0, 0x35, 0x0, 0x0, 0x0, 0x3B, 0x0, 0x0, 0x0, 0x3D, 0x0, 0x0, 0x0, 0x43, 0x0, 0x0, 0x0, 0x47, 0x0, 0x0, 0x0, 0x49, 0x0, 0x0, 0x0, 0x4F, 0x0, 0x0, 0x0, 0x53, 0x0, 0x0, 0x0, 0x59, 0x0, 0x0, 0x0, 0x61, 0x0, 0x0, 0x0, 0x65, 0x0, 0x0, 0x0, 0x67, 0x0, 0x0, 0x0, 0x6B, 0x0, 0x0, 0x0, 0x6D, 0x0, 0x0, 0x0, 0x71, 0x0, 0x0, 0x0, 0x7F, 0x0, 0x0, 0x0, 0x83, 0x0, 0x0, 0x0, 0x89, 0x0, 0x0, 0x0, 0x8B, 0x0, 0x0, 0x0, 0x95, 0x0, 0x0, 0x0, 0x97, 0x0, 0x0, 0x0, 0x9D, 0x0, 0x0, 0x0, 0xA3, 0x0, 0x0, 0x0, 0xA7, 0x0, 0x0, 0x0, 0xAD, 0x0, 0x0, 0x0, 0xB3, 0x0, 0x0, 0x0, 0xB5, 0x0, 0x0, 0x0, 0xBF, 0x0, 0x0, 0x0, 0xC1, 0x0, 0x0, 0x0, 0xC5, 0x0, 0x0, 0x0, 0xC7, 0x0, 0x0, 0x0, 0xD3, 0x0, 0x0, 0x0, 0xDF, 0x0, 0x0, 0x0, 0xE3, 0x0, 0x0, 0x0, 0xE5, 0x0, 0x0, 0x0, 0xE9, 0x0, 0x0, 0x0, 0xEF, 0x0, 0x0, 0x0, 0xF1, 0x0, 0x0, 0x0, 0xFB, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x7, 0x1, 0x0, 0x0, 0xD, 0x1, 0x0, 0x0, 0xF, 0x1, 0x0, 0x0, 0x15, 0x1, 0x0, 0x0, 0x19, 0x1, 0x0, 0x0, 0x1B, 0x1, 0x0, 0x0, 0x25, 0x1, 0x0, 0x0, 0x33, 0x1, 0x0, 0x0, 0x37, 0x1, 0x0, 0x0, 0x39, 0x1, 0x0, 0x0, 0x3D, 0x1, 0x0, 0x0, 0x4B, 0x1, 0x0, 0x0, 0x51, 0x1, 0x0, 0x0, 0x5B, 0x1, 0x0, 0x0, 0x5D, 0x1, 0x0, 0x0, 0x61, 0x1, 0x0, 0x0, 0x67, 0x1, 0x0, 0x0, 0x6F, 0x1, 0x0, 0x0, 0x75, 0x1, 0x0, 0x0, 0x7B, 0x1, 0x0, 0x0, 0x7F, 0x1, 0x0, 0x0, 0x85, 0x1, 0x0, 0x0, 0x8D, 0x1, 0x0, 0x0, 0x91, 0x1, 0x0, 0x0, 0x99, 0x1, 0x0, 0x0, 0xA3, 0x1, 0x0, 0x0, 0xA5, 0x1, 0x0, 0x0, 0xAF, 0x1, 0x0, 0x0, 0xB1, 0x1, 0x0, 0x0, 0xB7, 0x1, 0x0, 0x0, 0xBB, 0x1, 0x0, 0x0, 0xC1, 0x1, 0x0, 0x0, 0xC9, 0x1, 0x0, 0x0, 0xCD, 0x1, 0x0, 0x0, 0xCF, 0x1, 0x0, 0x0, 0xD3, 0x1, 0x0, 0x0, 0xDF, 0x1, 0x0, 0x0, 0xE7, 0x1, 0x0, 0x0, 0xEB, 0x1, 0x0, 0x0, 0xF3, 0x1, 0x0, 0x0, 0xF7, 0x1, 0x0, 0x0, 0xFD, 0x1, 0x0, 0x0, 0x9, 0x2, 0x0, 0x0, 0xB, 0x2, 0x0, 0x0, 0x1D, 0x2, 0x0, 0x0, 0x23, 0x2, 0x0, 0x0, 0x2D, 0x2, 0x0, 0x0, 0x33, 0x2, 0x0, 0x0, 0x39, 0x2, 0x0, 0x0, 0x3B, 0x2, 0x0, 0x0, 0x41, 0x2, 0x0, 0x0, 0x4B, 0x2, 0x0, 0x0, 0x51, 0x2, 0x0, 0x0, 0x57, 0x2, 0x0, 0x0, 0x59, 0x2, 0x0, 0x0, 0x5F, 0x2, 0x0, 0x0, 0x65, 0x2, 0x0, 0x0, 0x69, 0x2, 0x0, 0x0, 0x6B, 0x2, 0x0, 0x0, 0x77, 0x2, 0x0, 0x0, 0x81, 0x2, 0x0, 0x0, 0x83, 0x2, 0x0, 0x0, 0x87, 0x2, 0x0, 0x0, 0x8D, 0x2, 0x0, 0x0, 0x93, 0x2, 0x0, 0x0, 0x95, 0x2, 0x0, 0x0, 0xA1, 0x2, 0x0, 0x0, 0xA5, 0x2, 0x0, 0x0, 0xAB, 0x2, 0x0, 0x0, 0xB3, 0x2, 0x0, 0x0, 0xBD, 0x2, 0x0, 0x0, 0xC5, 0x2, 0x0, 0x0, 0xCF, 0x2, 0x0, 0x0, 0xD7, 0x2, 0x0, 0x0, 0xDD, 0x2, 0x0, 0x0, 0xE3, 0x2, 0x0, 0x0, 0xE7, 0x2, 0x0, 0x0, 0xEF, 0x2, 0x0, 0x0, 0xF5, 0x2, 0x0, 0x0, 0xF9, 0x2, 0x0, 0x0, 0x1, 0x3, 0x0, 0x0, 0x5, 0x3, 0x0, 0x0, 0x13, 0x3, 0x0, 0x0, 0x1D, 0x3, 0x0, 0x0, 0x29, 0x3, 0x0, 0x0, 0x2B, 0x3, 0x0, 0x0, 0x35, 0x3, 0x0, 0x0, 0x37, 0x3, 0x0, 0x0, 0x3B, 0x3, 0x0, 0x0, 0x3D, 0x3, 0x0, 0x0, 0x47, 0x3, 0x0, 0x0, 0x55, 0x3, 0x0, 0x0, 0x59, 0x3, 0x0, 0x0, 0x5B, 0x3, 0x0, 0x0, 0x5F, 0x3, 0x0, 0x0, 0x6D, 0x3, 0x0, 0x0, 0x71, 0x3, 0x0, 0x0, 0x73, 0x3, 0x0, 0x0, 0x77, 0x3, 0x0, 0x0, 0x8B, 0x3, 0x0, 0x0, 0x8F, 0x3, 0x0, 0x0, 0x97, 0x3, 0x0, 0x0, 0xA1, 0x3, 0x0, 0x0, 0xA9, 0x3, 0x0, 0x0, 0xAD, 0x3, 0x0, 0x0, 0xB3, 0x3, 0x0, 0x0, 0xB9, 0x3, 0x0, 0x0, 0xC7, 0x3, 0x0, 0x0, 0xCB, 0x3, 0x0, 0x0, 0xD1, 0x3, 0x0, 0x0, 0xD7, 0x3, 0x0, 0x0, 0xDF, 0x3, 0x0, 0x0, 0xE5, 0x3, 0x0, 0x0, 0xF1, 0x3, 0x0, 0x0, 0xF5, 0x3, 0x0, 0x0, 0xFB, 0x3, 0x0, 0x0, 0xFD, 0x3, 0x0, 0x0, 0x7, 0x4, 0x0, 0x0, 0x9, 0x4, 0x0, 0x0, 0xF, 0x4, 0x0, 0x0, 0x19, 0x4, 0x0, 0x0, 0x1B, 0x4, 0x0, 0x0, 0x25, 0x4, 0x0, 0x0, 0x27, 0x4, 0x0, 0x0, 0x2D, 0x4, 0x0, 0x0, 0x3F, 0x4, 0x0, 0x0, 0x43, 0x4, 0x0, 0x0, 0x45, 0x4, 0x0, 0x0, 0x49, 0x4, 0x0, 0x0, 0x4F, 0x4, 0x0, 0x0, 0x55, 0x4, 0x0, 0x0, 0x5D, 0x4, 0x0, 0x0, 0x63, 0x4, 0x0, 0x0, 0x69, 0x4, 0x0, 0x0, 0x7F, 0x4, 0x0, 0x0, 0x81, 0x4, 0x0, 0x0, 0x8B, 0x4, 0x0, 0x0, 0x93, 0x4, 0x0, 0x0, 0x9D, 0x4, 0x0, 0x0, 0xA3, 0x4, 0x0, 0x0, 0xA9, 0x4, 0x0, 0x0, 0xB1, 0x4, 0x0, 0x0, 0xBD, 0x4, 0x0, 0x0, 0xC1, 0x4, 0x0, 0x0, 0xC7, 0x4, 0x0, 0x0, 0xCD, 0x4, 0x0, 0x0, 0xCF, 0x4, 0x0, 0x0, 0xD5, 0x4, 0x0, 0x0, 0xE1, 0x4, 0x0, 0x0, 0xEB, 0x4, 0x0, 0x0, 0xFD, 0x4, 0x0, 0x0, 0xFF, 0x4, 0x0, 0x0, 0x3, 0x5, 0x0, 0x0, 0x9, 0x5, 0x0, 0x0, 0xB, 0x5, 0x0, 0x0, 0x11, 0x5, 0x0, 0x0, 0x15, 0x5, 0x0, 0x0, 0x17, 0x5, 0x0, 0x0, 0x1B, 0x5, 0x0, 0x0, 0x27, 0x5, 0x0, 0x0, 0x29, 0x5, 0x0, 0x0, 0x2F, 0x5, 0x0, 0x0, 0x51, 0x5, 0x0, 0x0, 0x57, 0x5, 0x0, 0x0, 0x5D, 0x5, 0x0, 0x0, 0x65, 0x5, 0x0, 0x0, 0x77, 0x5, 0x0, 0x0, 0x81, 0x5, 0x0, 0x0, 0x8F, 0x5, 0x0, 0x0, 0x93, 0x5, 0x0, 0x0, 0x95, 0x5, 0x0, 0x0, 0x99, 0x5, 0x0, 0x0, 0x9F, 0x5, 0x0, 0x0, 0xA7, 0x5, 0x0, 0x0, 0xAB, 0x5, 0x0, 0x0, 0xAD, 0x5, 0x0, 0x0, 0xB3, 0x5, 0x0, 0x0, 0xBF, 0x5, 0x0, 0x0, 0xC9, 0x5, 0x0, 0x0, 0xCB, 0x5, 0x0, 0x0, 0xCF, 0x5, 0x0, 0x0, 0xD1, 0x5, 0x0, 0x0, 0xD5, 0x5, 0x0, 0x0, 0xDB, 0x5, 0x0, 0x0, 0xE7, 0x5, 0x0, 0x0, 0xF3, 0x5, 0x0, 0x0, 0xFB, 0x5, 0x0, 0x0, 0x7, 0x6, 0x0, 0x0, 0xD, 0x6, 0x0, 0x0, 0x11, 0x6, 0x0, 0x0, 0x17, 0x6, 0x0, 0x0, 0x1F, 0x6, 0x0, 0x0, 0x23, 0x6, 0x0, 0x0, 0x2B, 0x6, 0x0, 0x0, 0x2F, 0x6, 0x0, 0x0, 0x3D, 0x6, 0x0, 0x0, 0x41, 0x6, 0x0, 0x0, 0x47, 0x6, 0x0, 0x0, 0x49, 0x6, 0x0, 0x0, 0x4D, 0x6, 0x0, 0x0, 0x53, 0x6, 0x0, 0x0, 0x55, 0x6, 0x0, 0x0, 0x5B, 0x6, 0x0, 0x0, 0x65, 0x6, 0x0, 0x0, 0x79, 0x6, 0x0, 0x0, 0x7F, 0x6, 0x0, 0x0, 0x83, 0x6, 0x0, 0x0, 0x85, 0x6, 0x0, 0x0, 0x9D, 0x6, 0x0, 0x0, 0xA1, 0x6, 0x0, 0x0, 0xA3, 0x6, 0x0, 0x0, 0xAD, 0x6, 0x0, 0x0, 0xB9, 0x6, 0x0, 0x0, 0xBB, 0x6, 0x0, 0x0, 0xC5, 0x6, 0x0, 0x0, 0xCD, 0x6, 0x0, 0x0, 0xD3, 0x6, 0x0, 0x0, 0xD9, 0x6, 0x0, 0x0, 0xDF, 0x6, 0x0, 0x0, 0xF1, 0x6, 0x0, 0x0, 0xF7, 0x6, 0x0, 0x0, 0xFB, 0x6, 0x0, 0x0, 0xFD, 0x6, 0x0, 0x0, 0x9, 0x7, 0x0, 0x0, 0x13, 0x7, 0x0, 0x0, 0x1F, 0x7, 0x0, 0x0, 0x27, 0x7, 0x0, 0x0, 0x37, 0x7, 0x0, 0x0, 0x45, 0x7, 0x0, 0x0, 0x4B, 0x7, 0x0, 0x0, 0x4F, 0x7, 0x0, 0x0, 0x51, 0x7, 0x0, 0x0, 0x55, 0x7, 0x0, 0x0, 0x57, 0x7, 0x0, 0x0, 0x61, 0x7, 0x0, 0x0, 0x6D, 0x7, 0x0, 0x0, 0x73, 0x7, 0x0, 0x0, 0x79, 0x7, 0x0, 0x0, 0x8B, 0x7, 0x0, 0x0, 0x8D, 0x7, 0x0, 0x0, 0x9D, 0x7, 0x0, 0x0, 0x9F, 0x7, 0x0, 0x0, 0xB5, 0x7, 0x0, 0x0, 0xBB, 0x7, 0x0, 0x0, 0xC3, 0x7, 0x0, 0x0, 0xC9, 0x7, 0x0, 0x0, 0xCD, 0x7, 0x0, 0x0, 0xCF, 0x7, 0x0, 0x0, 0xD3, 0x7, 0x0, 0x0, 0xDB, 0x7, 0x0, 0x0, 0xE1, 0x7, 0x0, 0x0, 0xEB, 0x7, 0x0, 0x0, 0xED, 0x7, 0x0, 0x0, 0xF7, 0x7, 0x0, 0x0, 0x5, 0x8, 0x0, 0x0, 0xF, 0x8, 0x0, 0x0, 0x15, 0x8, 0x0, 0x0, 0x21, 0x8, 0x0, 0x0, 0x23, 0x8, 0x0, 0x0, 0x27, 0x8, 0x0, 0x0, 0x29, 0x8, 0x0, 0x0, 0x33, 0x8, 0x0, 0x0, 0x3F, 0x8, 0x0, 0x0, 0x41, 0x8, 0x0, 0x0, 0x51, 0x8, 0x0, 0x0, 0x53, 0x8, 0x0, 0x0, 0x59, 0x8, 0x0, 0x0, 0x5D, 0x8, 0x0, 0x0, 0x5F, 0x8, 0x0, 0x0, 0x69, 0x8, 0x0, 0x0, 0x71, 0x8, 0x0, 0x0, 0x83, 0x8, 0x0, 0x0, 0x9B, 0x8, 0x0, 0x0, 0x9F, 0x8, 0x0, 0x0, 0xA5, 0x8, 0x0, 0x0, 0xAD, 0x8, 0x0, 0x0, 0xBD, 0x8, 0x0, 0x0, 0xBF, 0x8, 0x0, 0x0, 0xC3, 0x8, 0x0, 0x0, 0xCB, 0x8, 0x0, 0x0, 0xDB, 0x8, 0x0, 0x0, 0xDD, 0x8, 0x0, 0x0, 0xE1, 0x8, 0x0, 0x0, 0xE9, 0x8, 0x0, 0x0, 0xEF, 0x8, 0x0, 0x0, 0xF5, 0x8, 0x0, 0x0, 0xF9, 0x8, 0x0, 0x0, 0x5, 0x9, 0x0, 0x0, 0x7, 0x9, 0x0, 0x0, 0x1D, 0x9, 0x0, 0x0, 0x23, 0x9, 0x0, 0x0, 0x25, 0x9, 0x0, 0x0, 0x2B, 0x9, 0x0, 0x0, 0x2F, 0x9, 0x0, 0x0, 0x35, 0x9, 0x0, 0x0, 0x43, 0x9, 0x0, 0x0, 0x49, 0x9, 0x0, 0x0, 0x4D, 0x9, 0x0, 0x0, 0x4F, 0x9, 0x0, 0x0, 0x55, 0x9, 0x0, 0x0, 0x59, 0x9, 0x0, 0x0, 0x5F, 0x9, 0x0, 0x0, 0x6B, 0x9, 0x0, 0x0, 0x71, 0x9, 0x0, 0x0, 0x77, 0x9, 0x0, 0x0, 0x85, 0x9, 0x0, 0x0, 0x89, 0x9, 0x0, 0x0, 0x8F, 0x9, 0x0, 0x0, 0x9B, 0x9, 0x0, 0x0, 0xA3, 0x9, 0x0, 0x0, 0xA9, 0x9, 0x0, 0x0, 0xAD, 0x9, 0x0, 0x0, 0xC7, 0x9, 0x0, 0x0, 0xD9, 0x9, 0x0, 0x0, 0xE3, 0x9, 0x0, 0x0, 0xEB, 0x9, 0x0, 0x0, 0xEF, 0x9, 0x0, 0x0, 0xF5, 0x9, 0x0, 0x0, 0xF7, 0x9, 0x0, 0x0, 0xFD, 0x9, 0x0, 0x0, 0x13, 0xA, 0x0, 0x0, 0x1F, 0xA, 0x0, 0x0, 0x21, 0xA, 0x0, 0x0, 0x31, 0xA, 0x0, 0x0, 0x39, 0xA, 0x0, 0x0, 0x3D, 0xA, 0x0, 0x0, 0x49, 0xA, 0x0, 0x0, 0x57, 0xA, 0x0, 0x0, 0x61, 0xA, 0x0, 0x0, 0x63, 0xA, 0x0, 0x0, 0x67, 0xA, 0x0, 0x0, 0x6F, 0xA, 0x0, 0x0, 0x75, 0xA, 0x0, 0x0, 0x7B, 0xA, 0x0, 0x0, 0x7F, 0xA, 0x0, 0x0, 0x81, 0xA, 0x0, 0x0, 0x85, 0xA, 0x0, 0x0, 0x8B, 0xA, 0x0, 0x0, 0x93, 0xA, 0x0, 0x0, 0x97, 0xA, 0x0, 0x0, 0x99, 0xA, 0x0, 0x0, 0x9F, 0xA, 0x0, 0x0, 0xA9, 0xA, 0x0, 0x0, 0xAB, 0xA, 0x0, 0x0, 0xB5, 0xA, 0x0, 0x0, 0xBD, 0xA, 0x0, 0x0, 0xC1, 0xA, 0x0, 0x0, 0xCF, 0xA, 0x0, 0x0, 0xD9, 0xA, 0x0, 0x0, 0xE5, 0xA, 0x0, 0x0, 0xE7, 0xA, 0x0, 0x0, 0xED, 0xA, 0x0, 0x0, 0xF1, 0xA, 0x0, 0x0, 0xF3, 0xA, 0x0, 0x0, 0x3, 0xB, 0x0, 0x0, 0x11, 0xB, 0x0, 0x0, 0x15, 0xB, 0x0, 0x0, 0x1B, 0xB, 0x0, 0x0, 0x23, 0xB, 0x0, 0x0, 0x29, 0xB, 0x0, 0x0, 0x2D, 0xB, 0x0, 0x0, 0x3F, 0xB, 0x0, 0x0, 0x47, 0xB, 0x0, 0x0, 0x51, 0xB, 0x0, 0x0, 0x57, 0xB, 0x0, 0x0, 0x5D, 0xB, 0x0, 0x0, 0x65, 0xB, 0x0, 0x0, 0x6F, 0xB, 0x0, 0x0, 0x7B, 0xB, 0x0, 0x0, 0x89, 0xB, 0x0, 0x0, 0x8D, 0xB, 0x0, 0x0, 0x93, 0xB, 0x0, 0x0, 0x99, 0xB, 0x0, 0x0, 0x9B, 0xB, 0x0, 0x0, 0xB7, 0xB, 0x0, 0x0, 0xB9, 0xB, 0x0, 0x0, 0xC3, 0xB, 0x0, 0x0, 0xCB, 0xB, 0x0, 0x0, 0xCF, 0xB, 0x0, 0x0, 0xDD, 0xB, 0x0, 0x0, 0xE1, 0xB, 0x0, 0x0, 0xE9, 0xB, 0x0, 0x0, 0xF5, 0xB, 0x0, 0x0, 0xFB, 0xB, 0x0, 0x0, 0x7, 0xC, 0x0, 0x0, 0xB, 0xC, 0x0, 0x0, 0x11, 0xC, 0x0, 0x0, 0x25, 0xC, 0x0, 0x0, 0x2F, 0xC, 0x0, 0x0, 0x31, 0xC, 0x0, 0x0, 0x41, 0xC, 0x0, 0x0, 0x5B, 0xC, 0x0, 0x0, 0x5F, 0xC, 0x0, 0x0, 0x61, 0xC, 0x0, 0x0, 0x6D, 0xC, 0x0, 0x0, 0x73, 0xC, 0x0, 0x0, 0x77, 0xC, 0x0, 0x0, 0x83, 0xC, 0x0, 0x0, 0x89, 0xC, 0x0, 0x0, 0x91, 0xC, 0x0, 0x0, 0x95, 0xC, 0x0, 0x0, 0x9D, 0xC, 0x0, 0x0, 0xB3, 0xC, 0x0, 0x0, 0xB5, 0xC, 0x0, 0x0, 0xB9, 0xC, 0x0, 0x0, 0xBB, 0xC, 0x0, 0x0, 0xC7, 0xC, 0x0, 0x0, 0xE3, 0xC, 0x0, 0x0, 0xE5, 0xC, 0x0, 0x0, 0xEB, 0xC, 0x0, 0x0, 0xF1, 0xC, 0x0, 0x0, 0xF7, 0xC, 0x0, 0x0, 0xFB, 0xC, 0x0, 0x0, 0x1, 0xD, 0x0, 0x0, 0x3, 0xD, 0x0, 0x0, 0xF, 0xD, 0x0, 0x0, 0x13, 0xD, 0x0, 0x0, 0x1F, 0xD, 0x0, 0x0, 0x21, 0xD, 0x0, 0x0, 0x2B, 0xD, 0x0, 0x0, 0x2D, 0xD, 0x0, 0x0, 0x3D, 0xD, 0x0, 0x0, 0x3F, 0xD, 0x0, 0x0, 0x4F, 0xD, 0x0, 0x0, 0x55, 0xD, 0x0, 0x0, 0x69, 0xD, 0x0, 0x0, 0x79, 0xD, 0x0, 0x0, 0x81, 0xD, 0x0, 0x0, 0x85, 0xD, 0x0, 0x0, 0x87, 0xD, 0x0, 0x0, 0x8B, 0xD, 0x0, 0x0, 0x8D, 0xD, 0x0, 0x0, 0xA3, 0xD, 0x0, 0x0, 0xAB, 0xD, 0x0, 0x0, 0xB7, 0xD, 0x0, 0x0, 0xBD, 0xD, 0x0, 0x0, 0xC7, 0xD, 0x0, 0x0, 0xC9, 0xD, 0x0, 0x0, 0xCD, 0xD, 0x0, 0x0, 0xD3, 0xD, 0x0, 0x0, 0xD5, 0xD, 0x0, 0x0, 0xDB, 0xD, 0x0, 0x0, 0xE5, 0xD, 0x0, 0x0, 0xE7, 0xD, 0x0, 0x0, 0xF3, 0xD, 0x0, 0x0, 0xFD, 0xD, 0x0, 0x0, 0xFF, 0xD, 0x0, 0x0, 0x9, 0xE, 0x0, 0x0, 0x17, 0xE, 0x0, 0x0, 0x1D, 0xE, 0x0, 0x0, 0x21, 0xE, 0x0, 0x0, 0x27, 0xE, 0x0, 0x0, 0x2F, 0xE, 0x0, 0x0, 0x35, 0xE, 0x0, 0x0, 0x3B, 0xE, 0x0, 0x0, 0x4B, 0xE, 0x0, 0x0, 0x57, 0xE, 0x0, 0x0, 0x59, 0xE, 0x0, 0x0, 0x5D, 0xE, 0x0, 0x0, 0x6B, 0xE, 0x0, 0x0, 0x71, 0xE, 0x0, 0x0, 0x75, 0xE, 0x0, 0x0, 0x7D, 0xE, 0x0, 0x0, 0x87, 0xE, 0x0, 0x0, 0x8F, 0xE, 0x0, 0x0, 0x95, 0xE, 0x0, 0x0, 0x9B, 0xE, 0x0, 0x0, 0xB1, 0xE, 0x0, 0x0, 0xB7, 0xE, 0x0, 0x0, 0xB9, 0xE, 0x0, 0x0, 0xC3, 0xE, 0x0, 0x0, 0xD1, 0xE, 0x0, 0x0, 0xD5, 0xE, 0x0, 0x0, 0xDB, 0xE, 0x0, 0x0, 0xED, 0xE, 0x0, 0x0, 0xEF, 0xE, 0x0, 0x0, 0xF9, 0xE, 0x0, 0x0, 0x7, 0xF, 0x0, 0x0, 0xB, 0xF, 0x0, 0x0, 0xD, 0xF, 0x0, 0x0, 0x17, 0xF, 0x0, 0x0, 0x25, 0xF, 0x0, 0x0, 0x29, 0xF, 0x0, 0x0, 0x31, 0xF, 0x0, 0x0, 0x43, 0xF, 0x0, 0x0, 0x47, 0xF, 0x0, 0x0, 0x4D, 0xF, 0x0, 0x0, 0x4F, 0xF, 0x0, 0x0, 0x53, 0xF, 0x0, 0x0, 0x59, 0xF, 0x0, 0x0, 0x5B, 0xF, 0x0, 0x0, 0x67, 0xF, 0x0, 0x0, 0x6B, 0xF, 0x0, 0x0, 0x7F, 0xF, 0x0, 0x0, 0x95, 0xF, 0x0, 0x0, 0xA1, 0xF, 0x0, 0x0, 0xA3, 0xF, 0x0, 0x0, 0xA7, 0xF, 0x0, 0x0, 0xAD, 0xF, 0x0, 0x0, 0xB3, 0xF, 0x0, 0x0, 0xB5, 0xF, 0x0, 0x0, 0xBB, 0xF, 0x0, 0x0, 0xD1, 0xF, 0x0, 0x0, 0xD3, 0xF, 0x0, 0x0, 0xD9, 0xF, 0x0, 0x0, 0xE9, 0xF, 0x0, 0x0, 0xEF, 0xF, 0x0, 0x0, 0xFB, 0xF, 0x0, 0x0, 0xFD, 0xF, 0x0, 0x0, 0x3, 0x10, 0x0, 0x0, 0xF, 0x10, 0x0, 0x0, 0x1F, 0x10, 0x0, 0x0, 0x21, 0x10, 0x0, 0x0, 0x25, 0x10, 0x0, 0x0, 0x2B, 0x10, 0x0, 0x0, 0x39, 0x10, 0x0, 0x0, 0x3D, 0x10, 0x0, 0x0, 0x3F, 0x10, 0x0, 0x0, 0x51, 0x10, 0x0, 0x0, 0x69, 0x10, 0x0, 0x0, 0x73, 0x10, 0x0, 0x0, 0x79, 0x10, 0x0, 0x0, 0x7B, 0x10, 0x0, 0x0, 0x85, 0x10, 0x0, 0x0, 0x87, 0x10, 0x0, 0x0, 0x91, 0x10, 0x0, 0x0, 0x93, 0x10, 0x0, 0x0, 0x9D, 0x10, 0x0, 0x0, 0xA3, 0x10, 0x0, 0x0, 0xA5, 0x10, 0x0, 0x0, 0xAF, 0x10, 0x0, 0x0, 0xB1, 0x10, 0x0, 0x0, 0xBB, 0x10, 0x0, 0x0, 0xC1, 0x10, 0x0, 0x0, 0xC9, 0x10, 0x0, 0x0, 0xE7, 0x10, 0x0, 0x0, 0xF1, 0x10, 0x0, 0x0, 0xF3, 0x10, 0x0, 0x0, 0xFD, 0x10, 0x0, 0x0, 0x5, 0x11, 0x0, 0x0, 0xB, 0x11, 0x0, 0x0, 0x15, 0x11, 0x0, 0x0, 0x27, 0x11, 0x0, 0x0, 0x2D, 0x11, 0x0, 0x0, 0x39, 0x11, 0x0, 0x0, 0x45, 0x11, 0x0, 0x0, 0x47, 0x11, 0x0, 0x0, 0x59, 0x11, 0x0, 0x0, 0x5F, 0x11, 0x0, 0x0, 0x63, 0x11, 0x0, 0x0, 0x69, 0x11, 0x0, 0x0, 0x6F, 0x11, 0x0, 0x0, 0x81, 0x11, 0x0, 0x0, 0x83, 0x11, 0x0, 0x0, 0x8D, 0x11, 0x0, 0x0, 0x9B, 0x11, 0x0, 0x0, 0xA1, 0x11, 0x0, 0x0, 0xA5, 0x11, 0x0, 0x0, 0xA7, 0x11, 0x0, 0x0, 0xAB, 0x11, 0x0, 0x0, 0xC3, 0x11, 0x0, 0x0, 0xC5, 0x11, 0x0, 0x0, 0xD1, 0x11, 0x0, 0x0, 0xD7, 0x11, 0x0, 0x0, 0xE7, 0x11, 0x0, 0x0, 0xEF, 0x11, 0x0, 0x0, 0xF5, 0x11, 0x0, 0x0, 0xFB, 0x11, 0x0, 0x0, 0xD, 0x12, 0x0, 0x0, 0x1D, 0x12, 0x0, 0x0, 0x1F, 0x12, 0x0, 0x0, 0x23, 0x12, 0x0, 0x0, 0x29, 0x12, 0x0, 0x0, 0x2B, 0x12, 0x0, 0x0, 0x31, 0x12, 0x0, 0x0, 0x37, 0x12, 0x0, 0x0, 0x41, 0x12, 0x0, 0x0, 0x47, 0x12, 0x0, 0x0, 0x53, 0x12, 0x0, 0x0, 0x5F, 0x12, 0x0, 0x0, 0x71, 0x12, 0x0, 0x0, 0x73, 0x12, 0x0, 0x0, 0x79, 0x12, 0x0, 0x0, 0x7D, 0x12, 0x0, 0x0, 0x8F, 0x12, 0x0, 0x0, 0x97, 0x12, 0x0, 0x0, 0xAF, 0x12, 0x0, 0x0, 0xB3, 0x12, 0x0, 0x0, 0xB5, 0x12, 0x0, 0x0, 0xB9, 0x12, 0x0, 0x0, 0xBF, 0x12, 0x0, 0x0, 0xC1, 0x12, 0x0, 0x0, 0xCD, 0x12, 0x0, 0x0, 0xD1, 0x12, 0x0, 0x0, 0xDF, 0x12, 0x0, 0x0, 0xFD, 0x12, 0x0, 0x0, 0x7, 0x13, 0x0, 0x0, 0xD, 0x13, 0x0, 0x0, 0x19, 0x13, 0x0, 0x0, 0x27, 0x13, 0x0, 0x0, 0x2D, 0x13, 0x0, 0x0, 0x37, 0x13, 0x0, 0x0, 0x43, 0x13, 0x0, 0x0, 0x45, 0x13, 0x0, 0x0, 0x49, 0x13, 0x0, 0x0, 0x4F, 0x13, 0x0, 0x0, 0x57, 0x13, 0x0, 0x0, 0x5D, 0x13, 0x0, 0x0, 0x67, 0x13, 0x0, 0x0, 0x69, 0x13, 0x0, 0x0, 0x6D, 0x13, 0x0, 0x0, 0x7B, 0x13, 0x0, 0x0, 0x81, 0x13, 0x0, 0x0, 0x87, 0x13, 0x0, 0x0, 0x8B, 0x13, 0x0, 0x0, 0x91, 0x13, 0x0, 0x0, 0x93, 0x13, 0x0, 0x0, 0x9D, 0x13, 0x0, 0x0, 0x9F, 0x13, 0x0, 0x0, 0xAF, 0x13, 0x0, 0x0, 0xBB, 0x13, 0x0, 0x0, 0xC3, 0x13, 0x0, 0x0, 0xD5, 0x13, 0x0, 0x0, 0xD9, 0x13, 0x0, 0x0, 0xDF, 0x13, 0x0, 0x0, 0xEB, 0x13, 0x0, 0x0, 0xED, 0x13, 0x0, 0x0, 0xF3, 0x13, 0x0, 0x0, 0xF9, 0x13, 0x0, 0x0, 0xFF, 0x13, 0x0, 0x0, 0x1B, 0x14, 0x0, 0x0, 0x21, 0x14, 0x0, 0x0, 0x2F, 0x14, 0x0, 0x0, 0x33, 0x14, 0x0, 0x0, 0x3B, 0x14, 0x0, 0x0, 0x45, 0x14, 0x0, 0x0, 0x4D, 0x14, 0x0, 0x0, 0x59, 0x14, 0x0, 0x0, 0x6B, 0x14, 0x0, 0x0, 0x6F, 0x14, 0x0, 0x0, 0x71, 0x14, 0x0, 0x0, 0x75, 0x14, 0x0, 0x0, 0x8D, 0x14, 0x0, 0x0, 0x99, 0x14, 0x0, 0x0, 0x9F, 0x14, 0x0, 0x0, 0xA1, 0x14, 0x0, 0x0, 0xB1, 0x14, 0x0, 0x0, 0xB7, 0x14, 0x0, 0x0, 0xBD, 0x14, 0x0, 0x0, 0xCB, 0x14, 0x0, 0x0, 0xD5, 0x14, 0x0, 0x0, 0xE3, 0x14, 0x0, 0x0, 0xE7, 0x14, 0x0, 0x0, 0x5, 0x15, 0x0, 0x0, 0xB, 0x15, 0x0, 0x0, 0x11, 0x15, 0x0, 0x0, 0x17, 0x15, 0x0, 0x0, 0x1F, 0x15, 0x0, 0x0, 0x25, 0x15, 0x0, 0x0, 0x29, 0x15, 0x0, 0x0, 0x2B, 0x15, 0x0, 0x0, 0x37, 0x15, 0x0, 0x0, 0x3D, 0x15, 0x0, 0x0, 0x41, 0x15, 0x0, 0x0, 0x43, 0x15, 0x0, 0x0, 0x49, 0x15, 0x0, 0x0, 0x5F, 0x15, 0x0, 0x0, 0x65, 0x15, 0x0, 0x0, 0x67, 0x15, 0x0, 0x0, 0x6B, 0x15, 0x0, 0x0, 0x7D, 0x15, 0x0, 0x0, 0x7F, 0x15, 0x0, 0x0, 0x83, 0x15, 0x0, 0x0, 0x8F, 0x15, 0x0, 0x0, 0x91, 0x15, 0x0, 0x0, 0x97, 0x15, 0x0, 0x0, 0x9B, 0x15, 0x0, 0x0, 0xB5, 0x15, 0x0, 0x0, 0xBB, 0x15, 0x0, 0x0, 0xC1, 0x15, 0x0, 0x0, 0xC5, 0x15, 0x0, 0x0, 0xCD, 0x15, 0x0, 0x0, 0xD7, 0x15, 0x0, 0x0, 0xF7, 0x15, 0x0, 0x0, 0x7, 0x16, 0x0, 0x0, 0x9, 0x16, 0x0, 0x0, 0xF, 0x16, 0x0, 0x0, 0x13, 0x16, 0x0, 0x0, 0x15, 0x16, 0x0, 0x0, 0x19, 0x16, 0x0, 0x0, 0x1B, 0x16, 0x0, 0x0, 0x25, 0x16, 0x0, 0x0, 0x33, 0x16, 0x0, 0x0, 0x39, 0x16, 0x0, 0x0, 0x3D, 0x16, 0x0, 0x0, 0x45, 0x16, 0x0, 0x0, 0x4F, 0x16, 0x0, 0x0, 0x55, 0x16, 0x0, 0x0, 0x69, 0x16, 0x0, 0x0, 0x6D, 0x16, 0x0, 0x0, 0x6F, 0x16, 0x0, 0x0, 0x75, 0x16, 0x0, 0x0, 0x93, 0x16, 0x0, 0x0, 0x97, 0x16, 0x0, 0x0, 0x9F, 0x16, 0x0, 0x0, 0xA9, 0x16, 0x0, 0x0, 0xAF, 0x16, 0x0, 0x0, 0xB5, 0x16, 0x0, 0x0, 0xBD, 0x16, 0x0, 0x0, 0xC3, 0x16, 0x0, 0x0, 0xCF, 0x16, 0x0, 0x0, 0xD3, 0x16, 0x0, 0x0, 0xD9, 0x16, 0x0, 0x0, 0xDB, 0x16, 0x0, 0x0, 0xE1, 0x16, 0x0, 0x0, 0xE5, 0x16, 0x0, 0x0, 0xEB, 0x16, 0x0, 0x0, 0xED, 0x16, 0x0, 0x0, 0xF7, 0x16, 0x0, 0x0, 0xF9, 0x16, 0x0, 0x0, 0x9, 0x17, 0x0, 0x0, 0xF, 0x17, 0x0, 0x0, 0x23, 0x17, 0x0, 0x0, 0x27, 0x17, 0x0, 0x0, 0x33, 0x17, 0x0, 0x0, 0x41, 0x17, 0x0, 0x0, 0x5D, 0x17, 0x0, 0x0, 0x63, 0x17, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f7_DefaultValue = 
{
	&t1703_f7_FieldInfo, { (char*)t1703_f7_DefaultValueData, &t1687_0_0_0 }};
static const uint8_t t1703_f8_DefaultValueData[] = { 0xDA, 0x39, 0xA3, 0xEE, 0x5E, 0x6B, 0x4B, 0xD, 0x32, 0x55, 0xBF, 0xEF, 0x95, 0x60, 0x18, 0x90, 0xAF, 0xD8, 0x7, 0x9 };
static Il2CppFieldDefaultValueEntry t1703_f8_DefaultValue = 
{
	&t1703_f8_FieldInfo, { (char*)t1703_f8_DefaultValueData, &t1688_0_0_0 }};
static const uint8_t t1703_f9_DefaultValueData[] = { 0xE3, 0xB0, 0xC4, 0x42, 0x98, 0xFC, 0x1C, 0x14, 0x9A, 0xFB, 0xF4, 0xC8, 0x99, 0x6F, 0xB9, 0x24, 0x27, 0xAE, 0x41, 0xE4, 0x64, 0x9B, 0x93, 0x4C, 0xA4, 0x95, 0x99, 0x1B, 0x78, 0x52, 0xB8, 0x55 };
static Il2CppFieldDefaultValueEntry t1703_f9_DefaultValue = 
{
	&t1703_f9_FieldInfo, { (char*)t1703_f9_DefaultValueData, &t1689_0_0_0 }};
static const uint8_t t1703_f10_DefaultValueData[] = { 0x38, 0xB0, 0x60, 0xA7, 0x51, 0xAC, 0x96, 0x38, 0x4C, 0xD9, 0x32, 0x7E, 0xB1, 0xB1, 0xE3, 0x6A, 0x21, 0xFD, 0xB7, 0x11, 0x14, 0xBE, 0x7, 0x43, 0x4C, 0xC, 0xC7, 0xBF, 0x63, 0xF6, 0xE1, 0xDA, 0x27, 0x4E, 0xDE, 0xBF, 0xE7, 0x6F, 0x65, 0xFB, 0xD5, 0x1A, 0xD2, 0xF1, 0x48, 0x98, 0xB9, 0x5B };
static Il2CppFieldDefaultValueEntry t1703_f10_DefaultValue = 
{
	&t1703_f10_FieldInfo, { (char*)t1703_f10_DefaultValueData, &t1690_0_0_0 }};
static const uint8_t t1703_f11_DefaultValueData[] = { 0xCF, 0x83, 0xE1, 0x35, 0x7E, 0xEF, 0xB8, 0xBD, 0xF1, 0x54, 0x28, 0x50, 0xD6, 0x6D, 0x80, 0x7, 0xD6, 0x20, 0xE4, 0x5, 0xB, 0x57, 0x15, 0xDC, 0x83, 0xF4, 0xA9, 0x21, 0xD3, 0x6C, 0xE9, 0xCE, 0x47, 0xD0, 0xD1, 0x3C, 0x5D, 0x85, 0xF2, 0xB0, 0xFF, 0x83, 0x18, 0xD2, 0x87, 0x7E, 0xEC, 0x2F, 0x63, 0xB9, 0x31, 0xBD, 0x47, 0x41, 0x7A, 0x81, 0xA5, 0x38, 0x32, 0x7A, 0xF9, 0x27, 0xDA, 0x3E };
static Il2CppFieldDefaultValueEntry t1703_f11_DefaultValue = 
{
	&t1703_f11_FieldInfo, { (char*)t1703_f11_DefaultValueData, &t1691_0_0_0 }};
static const uint8_t t1703_f12_DefaultValueData[] = { 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1 };
static Il2CppFieldDefaultValueEntry t1703_f12_DefaultValue = 
{
	&t1703_f12_FieldInfo, { (char*)t1703_f12_DefaultValueData, &t1691_0_0_0 }};
static const uint8_t t1703_f13_DefaultValueData[] = { 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2 };
static Il2CppFieldDefaultValueEntry t1703_f13_DefaultValue = 
{
	&t1703_f13_FieldInfo, { (char*)t1703_f13_DefaultValueData, &t1691_0_0_0 }};
static const uint8_t t1703_f14_DefaultValueData[] = { 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3 };
static Il2CppFieldDefaultValueEntry t1703_f14_DefaultValue = 
{
	&t1703_f14_FieldInfo, { (char*)t1703_f14_DefaultValueData, &t1691_0_0_0 }};
static const uint8_t t1703_f15_DefaultValueData[] = { 0x9, 0x92, 0x26, 0x89, 0x93, 0xF2, 0x2C, 0x64, 0x1, 0x19, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f15_DefaultValue = 
{
	&t1703_f15_FieldInfo, { (char*)t1703_f15_DefaultValueData, &t1692_0_0_0 }};
static const uint8_t t1703_f16_DefaultValueData[] = { 0x9, 0x92, 0x26, 0x89, 0x93, 0xF2, 0x2C, 0x64, 0x1, 0x1, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f16_DefaultValue = 
{
	&t1703_f16_FieldInfo, { (char*)t1703_f16_DefaultValueData, &t1692_0_0_0 }};
static const uint8_t t1703_f17_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x9, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f17_DefaultValue = 
{
	&t1703_f17_FieldInfo, { (char*)t1703_f17_DefaultValueData, &t1692_0_0_0 }};
static const uint8_t t1703_f18_DefaultValueData[] = { 0x2C, 0x0, 0x2B, 0x0, 0x22, 0x0, 0x5C, 0x0, 0x3C, 0x0, 0x3E, 0x0, 0x3B, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f18_DefaultValue = 
{
	&t1703_f18_FieldInfo, { (char*)t1703_f18_DefaultValueData, &t1685_0_0_0 }};
static const uint8_t t1703_f19_DefaultValueData[] = { 0xB, 0x0, 0x0, 0x0, 0x13, 0x0, 0x0, 0x0, 0x25, 0x0, 0x0, 0x0, 0x49, 0x0, 0x0, 0x0, 0x6D, 0x0, 0x0, 0x0, 0xA3, 0x0, 0x0, 0x0, 0xFB, 0x0, 0x0, 0x0, 0x6F, 0x1, 0x0, 0x0, 0x2D, 0x2, 0x0, 0x0, 0x37, 0x3, 0x0, 0x0, 0xD5, 0x4, 0x0, 0x0, 0x45, 0x7, 0x0, 0x0, 0xD9, 0xA, 0x0, 0x0, 0x51, 0x10, 0x0, 0x0, 0x67, 0x18, 0x0, 0x0, 0x9B, 0x24, 0x0, 0x0, 0xE9, 0x36, 0x0, 0x0, 0x61, 0x52, 0x0, 0x0, 0x8B, 0x7B, 0x0, 0x0, 0x47, 0xB9, 0x0, 0x0, 0xE7, 0x15, 0x1, 0x0, 0xE1, 0xA0, 0x1, 0x0, 0x49, 0x71, 0x2, 0x0, 0xE5, 0xA9, 0x3, 0x0, 0xE3, 0x7E, 0x5, 0x0, 0x39, 0x3E, 0x8, 0x0, 0x67, 0x5D, 0xC, 0x0, 0x9, 0x8C, 0x12, 0x0, 0xFF, 0xD1, 0x1B, 0x0, 0x13, 0xBB, 0x29, 0x0, 0x8B, 0x98, 0x3E, 0x0, 0xC1, 0xE4, 0x5D, 0x0, 0x21, 0xD7, 0x8C, 0x0, 0xAB, 0x42, 0xD3, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f19_DefaultValue = 
{
	&t1703_f19_FieldInfo, { (char*)t1703_f19_DefaultValueData, &t1693_0_0_0 }};
static const uint8_t t1703_f20_DefaultValueData[] = { 0x22, 0x0, 0x3C, 0x0, 0x3E, 0x0, 0x7C, 0x0, 0x0, 0x0, 0x1, 0x0, 0x2, 0x0, 0x3, 0x0, 0x4, 0x0, 0x5, 0x0, 0x6, 0x0, 0x7, 0x0, 0x8, 0x0, 0x9, 0x0, 0xA, 0x0, 0xB, 0x0, 0xC, 0x0, 0xD, 0x0, 0xE, 0x0, 0xF, 0x0, 0x10, 0x0, 0x11, 0x0, 0x12, 0x0, 0x13, 0x0, 0x14, 0x0, 0x15, 0x0, 0x16, 0x0, 0x17, 0x0, 0x18, 0x0, 0x19, 0x0, 0x1A, 0x0, 0x1B, 0x0, 0x1C, 0x0, 0x1D, 0x0, 0x1E, 0x0, 0x1F, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f20_DefaultValue = 
{
	&t1703_f20_FieldInfo, { (char*)t1703_f20_DefaultValueData, &t1694_0_0_0 }};
static const uint8_t t1703_f21_DefaultValueData[] = { 0x0, 0x1, 0x0, 0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f21_DefaultValue = 
{
	&t1703_f21_FieldInfo, { (char*)t1703_f21_DefaultValueData, &t1688_0_0_0 }};
static const uint8_t t1703_f22_DefaultValueData[] = { 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x2B, 0x2F };
static Il2CppFieldDefaultValueEntry t1703_f22_DefaultValue = 
{
	&t1703_f22_FieldInfo, { (char*)t1703_f22_DefaultValueData, &t1691_0_0_0 }};
static const uint8_t t1703_f23_DefaultValueData[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3E, 0xFF, 0xFF, 0xFF, 0x3F, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f23_DefaultValue = 
{
	&t1703_f23_FieldInfo, { (char*)t1703_f23_DefaultValueData, &t1695_0_0_0 }};
static const uint8_t t1703_f24_DefaultValueData[] = { 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1F, 0x1F, 0x1F, 0x1F, 0xF, 0xF, 0xF, 0xF, 0xE1, 0xE1, 0xE1, 0xE1, 0xF1, 0xF1, 0xF1, 0xF1, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
static Il2CppFieldDefaultValueEntry t1703_f24_DefaultValue = 
{
	&t1703_f24_FieldInfo, { (char*)t1703_f24_DefaultValueData, &t1689_0_0_0 }};
static const uint8_t t1703_f25_DefaultValueData[] = { 0x0, 0x1E, 0x0, 0x1E, 0x0, 0xE, 0x0, 0xE, 0x0, 0xE0, 0x0, 0xE0, 0x0, 0xF0, 0x0, 0xF0, 0x0, 0xFE, 0x0, 0xFE, 0x0, 0xFE, 0x0, 0xFE, 0x1E, 0x0, 0x1E, 0x0, 0xE, 0x0, 0xE, 0x0, 0x1E, 0xE0, 0x1E, 0xE0, 0xE, 0xF0, 0xE, 0xF0, 0x1E, 0xFE, 0x1E, 0xFE, 0xE, 0xFE, 0xE, 0xFE, 0xE0, 0x0, 0xE0, 0x0, 0xF0, 0x0, 0xF0, 0x0, 0xE0, 0x1E, 0xE0, 0x1E, 0xF0, 0xE, 0xF0, 0xE, 0xE0, 0xFE, 0xE0, 0xFE, 0xF0, 0xFE, 0xF0, 0xFE, 0xFE, 0x0, 0xFE, 0x0, 0xFE, 0x0, 0xFE, 0x0, 0xFE, 0x1E, 0xFE, 0x1E, 0xFE, 0xE, 0xFE, 0xE, 0xFE, 0xE0, 0xFE, 0xE0, 0xFE, 0xF0, 0xFE, 0xF0 };
static Il2CppFieldDefaultValueEntry t1703_f25_DefaultValue = 
{
	&t1703_f25_FieldInfo, { (char*)t1703_f25_DefaultValueData, &t1696_0_0_0 }};
static const uint8_t t1703_f26_DefaultValueData[] = { 0x0, 0x82, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x2, 0x82, 0x80, 0x0, 0x2, 0x80, 0x80, 0x0, 0x2, 0x82, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x82, 0x80, 0x0, 0x2, 0x82, 0x80, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x2, 0x80, 0x0, 0x2, 0x80, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x2, 0x80, 0x0, 0x0, 0x2, 0x80, 0x0, 0x0, 0x82, 0x0, 0x0, 0x0, 0x82, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x2, 0x2, 0x80, 0x0, 0x2, 0x80, 0x0, 0x0, 0x2, 0x0, 0x80, 0x0, 0x2, 0x0, 0x80, 0x0, 0x2, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x82, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x2, 0x82, 0x80, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x82, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x80, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x82, 0x0, 0x0, 0x2, 0x0, 0x80, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x2, 0x80, 0x0, 0x2, 0x82, 0x0, 0x0, 0x2, 0x82, 0x80, 0x0, 0x2, 0x80, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x2, 0x2, 0x80, 0x0, 0x2, 0x0, 0x80, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x82, 0x0, 0x0, 0x0, 0x82, 0x80, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x2, 0x80, 0x0, 0x0, 0x2, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x80, 0x0, 0x0, 0x0, 0x82, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x80, 0x80, 0x0, 0x10, 0x40, 0x8, 0x40, 0x0, 0x40, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x10, 0x40, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x8, 0x40, 0x10, 0x40, 0x0, 0x40, 0x10, 0x0, 0x0, 0x40, 0x10, 0x40, 0x8, 0x40, 0x0, 0x40, 0x8, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x8, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x8, 0x40, 0x0, 0x40, 0x8, 0x0, 0x10, 0x0, 0x8, 0x0, 0x10, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x10, 0x40, 0x8, 0x0, 0x0, 0x0, 0x8, 0x40, 0x10, 0x0, 0x8, 0x0, 0x10, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x8, 0x0, 0x10, 0x40, 0x0, 0x0, 0x0, 0x40, 0x8, 0x40, 0x0, 0x0, 0x8, 0x40, 0x10, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x40, 0x8, 0x0, 0x10, 0x0, 0x8, 0x40, 0x0, 0x0, 0x8, 0x0, 0x10, 0x40, 0x0, 0x40, 0x0, 0x0, 0x8, 0x40, 0x0, 0x40, 0x8, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x8, 0x40, 0x0, 0x40, 0x0, 0x40, 0x10, 0x0, 0x0, 0x0, 0x10, 0x40, 0x8, 0x40, 0x10, 0x40, 0x8, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x10, 0x40, 0x0, 0x0, 0x0, 0x40, 0x8, 0x40, 0x0, 0x0, 0x8, 0x0, 0x10, 0x0, 0x0, 0x40, 0x10, 0x0, 0x8, 0x0, 0x10, 0x40, 0x0, 0x40, 0x10, 0x0, 0x0, 0x40, 0x10, 0x0, 0x8, 0x0, 0x0, 0x40, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x10, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x10, 0x0, 0x8, 0x40, 0x10, 0x40, 0x8, 0x40, 0x0, 0x40, 0x8, 0x0, 0x4, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x1, 0x4, 0x0, 0x1, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x1, 0x1, 0x0, 0x0, 0x1, 0x0, 0x4, 0x4, 0x0, 0x1, 0x0, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x1, 0x0, 0x4, 0x1, 0x1, 0x4, 0x4, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x4, 0x4, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x4, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x4, 0x4, 0x0, 0x1, 0x4, 0x4, 0x1, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x4, 0x1, 0x1, 0x4, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x1, 0x1, 0x4, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x1, 0x0, 0x4, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x1, 0x4, 0x0, 0x1, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x4, 0x0, 0x1, 0x0, 0x4, 0x1, 0x1, 0x4, 0x0, 0x1, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x1, 0x4, 0x4, 0x1, 0x0, 0x4, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x1, 0x1, 0x4, 0x4, 0x0, 0x0, 0x0, 0x4, 0x1, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x1, 0x4, 0x4, 0x1, 0x0, 0x4, 0x4, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x4, 0x4, 0x1, 0x1, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x1, 0x4, 0x0, 0x1, 0x1, 0x0, 0x0, 0x10, 0x40, 0x80, 0x40, 0x10, 0x0, 0x80, 0x40, 0x10, 0x0, 0x80, 0x40, 0x0, 0x0, 0x0, 0x40, 0x10, 0x40, 0x0, 0x40, 0x0, 0x40, 0x80, 0x0, 0x0, 0x40, 0x80, 0x0, 0x10, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x40, 0x0, 0x0, 0x10, 0x40, 0x0, 0x40, 0x10, 0x40, 0x80, 0x40, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x10, 0x40, 0x80, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x10, 0x0, 0x80, 0x40, 0x10, 0x0, 0x0, 0x40, 0x0, 0x40, 0x80, 0x0, 0x0, 0x0, 0x80, 0x40, 0x10, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x10, 0x0, 0x0, 0x40, 0x10, 0x40, 0x0, 0x40, 0x10, 0x40, 0x80, 0x40, 0x0, 0x0, 0x80, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x80, 0x0, 0x10, 0x40, 0x0, 0x40, 0x10, 0x40, 0x80, 0x40, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x40, 0x0, 0x40, 0x10, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x40, 0x0, 0x40, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x10, 0x40, 0x80, 0x40, 0x10, 0x0, 0x80, 0x40, 0x10, 0x0, 0x80, 0x40, 0x0, 0x0, 0x0, 0x40, 0x10, 0x40, 0x80, 0x40, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x40, 0x80, 0x0, 0x10, 0x0, 0x80, 0x40, 0x10, 0x40, 0x0, 0x40, 0x0, 0x40, 0x80, 0x0, 0x10, 0x0, 0x80, 0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x10, 0x40, 0x80, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x10, 0x0, 0x0, 0x40, 0x10, 0x40, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x4, 0x1, 0x0, 0x0, 0x4, 0x1, 0x80, 0x0, 0x0, 0x21, 0x0, 0x0, 0x4, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x4, 0x1, 0x80, 0x0, 0x4, 0x20, 0x0, 0x0, 0x4, 0x0, 0x80, 0x0, 0x0, 0x1, 0x80, 0x0, 0x4, 0x20, 0x80, 0x0, 0x0, 0x21, 0x0, 0x0, 0x4, 0x21, 0x80, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x4, 0x20, 0x0, 0x0, 0x4, 0x20, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x20, 0x80, 0x0, 0x4, 0x21, 0x80, 0x0, 0x4, 0x21, 0x80, 0x0, 0x0, 0x1, 0x0, 0x0, 0x4, 0x21, 0x80, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x21, 0x80, 0x0, 0x4, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x21, 0x80, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x80, 0x0, 0x0, 0x21, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x4, 0x1, 0x80, 0x0, 0x0, 0x21, 0x80, 0x0, 0x4, 0x20, 0x80, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x4, 0x21, 0x80, 0x0, 0x4, 0x1, 0x80, 0x0, 0x4, 0x20, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x4, 0x21, 0x80, 0x0, 0x4, 0x21, 0x80, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x21, 0x80, 0x0, 0x4, 0x21, 0x0, 0x0, 0x4, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x20, 0x0, 0x0, 0x0, 0x21, 0x80, 0x0, 0x4, 0x0, 0x80, 0x0, 0x0, 0x1, 0x80, 0x0, 0x0, 0x20, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x20, 0x80, 0x0, 0x4, 0x1, 0x80, 0x0, 0x0, 0x20, 0x8, 0x0, 0x0, 0x10, 0x0, 0x0, 0x20, 0x10, 0x0, 0x20, 0x0, 0x0, 0x8, 0x20, 0x20, 0x10, 0x0, 0x0, 0x20, 0x10, 0x8, 0x0, 0x0, 0x0, 0x8, 0x20, 0x20, 0x10, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x10, 0x8, 0x20, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x8, 0x0, 0x0, 0x10, 0x8, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x8, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x20, 0x0, 0x8, 0x20, 0x0, 0x10, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x8, 0x20, 0x0, 0x10, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x20, 0x10, 0x8, 0x0, 0x20, 0x10, 0x0, 0x0, 0x0, 0x0, 0x8, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x10, 0x8, 0x20, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x20, 0x0, 0x10, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x20, 0x10, 0x0, 0x20, 0x20, 0x0, 0x8, 0x20, 0x20, 0x10, 0x0, 0x0, 0x20, 0x0, 0x8, 0x20, 0x0, 0x0, 0x8, 0x0, 0x0, 0x10, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x8, 0x20, 0x0, 0x0, 0x8, 0x0, 0x0, 0x10, 0x8, 0x20, 0x20, 0x10, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x20, 0x10, 0x8, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x10, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x20, 0x10, 0x8, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x10, 0x8, 0x20, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x8, 0x0, 0x20, 0x0, 0x8, 0x20, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x10, 0x0, 0x0, 0x0, 0x10, 0x8, 0x0, 0x20, 0x0, 0x8, 0x20, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x1, 0x0, 0x10, 0x2, 0x1, 0x4, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x1, 0x4, 0x0, 0x2, 0x1, 0x4, 0x10, 0x0, 0x0, 0x4, 0x10, 0x2, 0x1, 0x4, 0x10, 0x2, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x2, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x1, 0x0, 0x10, 0x2, 0x1, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x2, 0x1, 0x4, 0x10, 0x0, 0x1, 0x0, 0x10, 0x0, 0x0, 0x4, 0x0, 0x2, 0x1, 0x0, 0x0, 0x2, 0x0, 0x0, 0x10, 0x2, 0x0, 0x4, 0x10, 0x2, 0x1, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x2, 0x0, 0x4, 0x0, 0x0, 0x1, 0x4, 0x0, 0x0, 0x1, 0x4, 0x10, 0x2, 0x0, 0x4, 0x10, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x4, 0x10, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x4, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x1, 0x4, 0x0, 0x2, 0x1, 0x4, 0x0, 0x2, 0x1, 0x0, 0x10, 0x2, 0x1, 0x0, 0x10, 0x2, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x4, 0x0, 0x2, 0x0, 0x0, 0x10, 0x0, 0x0, 0x4, 0x10, 0x2, 0x1, 0x4, 0x0, 0x0, 0x1, 0x4, 0x10, 0x0, 0x0, 0x4, 0x10, 0x2, 0x1, 0x4, 0x0, 0x0, 0x1, 0x0, 0x0, 0x2, 0x1, 0x4, 0x10, 0x2, 0x0, 0x0, 0x10, 0x2, 0x0, 0x4, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x4, 0x10, 0x2, 0x0, 0x0, 0x0, 0x0, 0x1, 0x4, 0x10, 0x0, 0x0, 0x0, 0x10, 0x2, 0x0, 0x4, 0x0, 0x0, 0x1, 0x0, 0x0, 0x2, 0x0, 0x4, 0x0, 0x2, 0x0, 0x4, 0x0, 0x0, 0x1, 0x0, 0x10, 0x0, 0x20, 0x8, 0x0, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x20, 0x8, 0x2, 0x8, 0x0, 0x0, 0x0, 0x8, 0x20, 0x8, 0x0, 0x8, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x20, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x8, 0x20, 0x8, 0x2, 0x8, 0x0, 0x8, 0x2, 0x0, 0x0, 0x8, 0x2, 0x8, 0x20, 0x8, 0x2, 0x0, 0x0, 0x8, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x8, 0x20, 0x0, 0x0, 0x8, 0x0, 0x8, 0x0, 0x8, 0x20, 0x8, 0x0, 0x0, 0x0, 0x8, 0x2, 0x0, 0x20, 0x0, 0x2, 0x0, 0x20, 0x0, 0x2, 0x8, 0x0, 0x8, 0x2, 0x8, 0x20, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x2, 0x8, 0x20, 0x0, 0x0, 0x8, 0x0, 0x8, 0x0, 0x8, 0x20, 0x8, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x20, 0x8, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x8, 0x2, 0x8, 0x0, 0x8, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x2, 0x8, 0x0, 0x8, 0x0, 0x0, 0x20, 0x8, 0x2, 0x0, 0x0, 0x8, 0x0, 0x8, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x8, 0x0, 0x0, 0x2, 0x8, 0x20, 0x0, 0x2, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x2, 0x0, 0x20, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x20, 0x8, 0x2, 0x8, 0x20, 0x0, 0x2, 0x0, 0x20, 0x0, 0x0, 0x8, 0x0, 0x0, 0x2, 0x8, 0x0, 0x8, 0x0, 0x8, 0x20, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x20, 0x8, 0x2, 0x8, 0x0, 0x8, 0x2, 0x0, 0x0, 0x8, 0x2, 0x0, 0x20, 0x8, 0x0, 0x0, 0x20, 0x8, 0x0, 0x0, 0x20, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x2, 0x8 };
static Il2CppFieldDefaultValueEntry t1703_f26_DefaultValue = 
{
	&t1703_f26_FieldInfo, { (char*)t1703_f26_DefaultValueData, &t1697_0_0_0 }};
static const uint8_t t1703_f27_DefaultValueData[] = { 0x38, 0x30, 0x28, 0x20, 0x18, 0x10, 0x8, 0x0, 0x39, 0x31, 0x29, 0x21, 0x19, 0x11, 0x9, 0x1, 0x3A, 0x32, 0x2A, 0x22, 0x1A, 0x12, 0xA, 0x2, 0x3B, 0x33, 0x2B, 0x23, 0x3E, 0x36, 0x2E, 0x26, 0x1E, 0x16, 0xE, 0x6, 0x3D, 0x35, 0x2D, 0x25, 0x1D, 0x15, 0xD, 0x5, 0x3C, 0x34, 0x2C, 0x24, 0x1C, 0x14, 0xC, 0x4, 0x1B, 0x13, 0xB, 0x3 };
static Il2CppFieldDefaultValueEntry t1703_f27_DefaultValue = 
{
	&t1703_f27_FieldInfo, { (char*)t1703_f27_DefaultValueData, &t1683_0_0_0 }};
static const uint8_t t1703_f28_DefaultValueData[] = { 0x1, 0x2, 0x4, 0x6, 0x8, 0xA, 0xC, 0xE, 0xF, 0x11, 0x13, 0x15, 0x17, 0x19, 0x1B, 0x1C };
static Il2CppFieldDefaultValueEntry t1703_f28_DefaultValue = 
{
	&t1703_f28_FieldInfo, { (char*)t1703_f28_DefaultValueData, &t1685_0_0_0 }};
static const uint8_t t1703_f29_DefaultValueData[] = { 0xD, 0x10, 0xA, 0x17, 0x0, 0x4, 0x2, 0x1B, 0xE, 0x5, 0x14, 0x9, 0x16, 0x12, 0xB, 0x3, 0x19, 0x7, 0xF, 0x6, 0x1A, 0x13, 0xC, 0x1, 0x28, 0x33, 0x1E, 0x24, 0x2E, 0x36, 0x1D, 0x27, 0x32, 0x2C, 0x20, 0x2F, 0x2B, 0x30, 0x26, 0x37, 0x21, 0x34, 0x2D, 0x29, 0x31, 0x23, 0x1C, 0x1F };
static Il2CppFieldDefaultValueEntry t1703_f29_DefaultValue = 
{
	&t1703_f29_FieldInfo, { (char*)t1703_f29_DefaultValueData, &t1690_0_0_0 }};
static const uint8_t t1703_f30_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x80, 0x80 };
static Il2CppFieldDefaultValueEntry t1703_f30_DefaultValue = 
{
	&t1703_f30_FieldInfo, { (char*)t1703_f30_DefaultValueData, &t1697_0_0_0 }};
static const uint8_t t1703_f31_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x40, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x10, 0x10, 0x10, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x0, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x4, 0x4, 0x4, 0x4, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x80, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x0, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x20, 0x20, 0x20, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x0, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x8, 0x8, 0x8, 0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0, 0x2, 0x2, 0x2, 0x2, 0x0, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f31_DefaultValue = 
{
	&t1703_f31_FieldInfo, { (char*)t1703_f31_DefaultValueData, &t1697_0_0_0 }};
static const uint8_t t1703_f32_DefaultValueData[] = { 0x78, 0xA4, 0x6A, 0xD7, 0x56, 0xB7, 0xC7, 0xE8, 0xDB, 0x70, 0x20, 0x24, 0xEE, 0xCE, 0xBD, 0xC1, 0xAF, 0xF, 0x7C, 0xF5, 0x2A, 0xC6, 0x87, 0x47, 0x13, 0x46, 0x30, 0xA8, 0x1, 0x95, 0x46, 0xFD, 0xD8, 0x98, 0x80, 0x69, 0xAF, 0xF7, 0x44, 0x8B, 0xB1, 0x5B, 0xFF, 0xFF, 0xBE, 0xD7, 0x5C, 0x89, 0x22, 0x11, 0x90, 0x6B, 0x93, 0x71, 0x98, 0xFD, 0x8E, 0x43, 0x79, 0xA6, 0x21, 0x8, 0xB4, 0x49, 0x62, 0x25, 0x1E, 0xF6, 0x40, 0xB3, 0x40, 0xC0, 0x51, 0x5A, 0x5E, 0x26, 0xAA, 0xC7, 0xB6, 0xE9, 0x5D, 0x10, 0x2F, 0xD6, 0x53, 0x14, 0x44, 0x2, 0x81, 0xE6, 0xA1, 0xD8, 0xC8, 0xFB, 0xD3, 0xE7, 0xE6, 0xCD, 0xE1, 0x21, 0xD6, 0x7, 0x37, 0xC3, 0x87, 0xD, 0xD5, 0xF4, 0xED, 0x14, 0x5A, 0x45, 0x5, 0xE9, 0xE3, 0xA9, 0xF8, 0xA3, 0xEF, 0xFC, 0xD9, 0x2, 0x6F, 0x67, 0x8A, 0x4C, 0x2A, 0x8D, 0x42, 0x39, 0xFA, 0xFF, 0x81, 0xF6, 0x71, 0x87, 0x22, 0x61, 0x9D, 0x6D, 0xC, 0x38, 0xE5, 0xFD, 0x44, 0xEA, 0xBE, 0xA4, 0xA9, 0xCF, 0xDE, 0x4B, 0x60, 0x4B, 0xBB, 0xF6, 0x70, 0xBC, 0xBF, 0xBE, 0xC6, 0x7E, 0x9B, 0x28, 0xFA, 0x27, 0xA1, 0xEA, 0x85, 0x30, 0xEF, 0xD4, 0x5, 0x1D, 0x88, 0x4, 0x39, 0xD0, 0xD4, 0xD9, 0xE5, 0x99, 0xDB, 0xE6, 0xF8, 0x7C, 0xA2, 0x1F, 0x65, 0x56, 0xAC, 0xC4, 0x44, 0x22, 0x29, 0xF4, 0x97, 0xFF, 0x2A, 0x43, 0xA7, 0x23, 0x94, 0xAB, 0x39, 0xA0, 0x93, 0xFC, 0xC3, 0x59, 0x5B, 0x65, 0x92, 0xCC, 0xC, 0x8F, 0x7D, 0xF4, 0xEF, 0xFF, 0xD1, 0x5D, 0x84, 0x85, 0x4F, 0x7E, 0xA8, 0x6F, 0xE0, 0xE6, 0x2C, 0xFE, 0x14, 0x43, 0x1, 0xA3, 0xA1, 0x11, 0x8, 0x4E, 0x82, 0x7E, 0x53, 0xF7, 0x35, 0xF2, 0x3A, 0xBD, 0xBB, 0xD2, 0xD7, 0x2A, 0x91, 0xD3, 0x86, 0xEB };
static Il2CppFieldDefaultValueEntry t1703_f32_DefaultValue = 
{
	&t1703_f32_FieldInfo, { (char*)t1703_f32_DefaultValueData, &t1698_0_0_0 }};
static const uint8_t t1703_f33_DefaultValueData[] = { 0xD9, 0x78, 0xF9, 0xC4, 0x19, 0xDD, 0xB5, 0xED, 0x28, 0xE9, 0xFD, 0x79, 0x4A, 0xA0, 0xD8, 0x9D, 0xC6, 0x7E, 0x37, 0x83, 0x2B, 0x76, 0x53, 0x8E, 0x62, 0x4C, 0x64, 0x88, 0x44, 0x8B, 0xFB, 0xA2, 0x17, 0x9A, 0x59, 0xF5, 0x87, 0xB3, 0x4F, 0x13, 0x61, 0x45, 0x6D, 0x8D, 0x9, 0x81, 0x7D, 0x32, 0xBD, 0x8F, 0x40, 0xEB, 0x86, 0xB7, 0x7B, 0xB, 0xF0, 0x95, 0x21, 0x22, 0x5C, 0x6B, 0x4E, 0x82, 0x54, 0xD6, 0x65, 0x93, 0xCE, 0x60, 0xB2, 0x1C, 0x73, 0x56, 0xC0, 0x14, 0xA7, 0x8C, 0xF1, 0xDC, 0x12, 0x75, 0xCA, 0x1F, 0x3B, 0xBE, 0xE4, 0xD1, 0x42, 0x3D, 0xD4, 0x30, 0xA3, 0x3C, 0xB6, 0x26, 0x6F, 0xBF, 0xE, 0xDA, 0x46, 0x69, 0x7, 0x57, 0x27, 0xF2, 0x1D, 0x9B, 0xBC, 0x94, 0x43, 0x3, 0xF8, 0x11, 0xC7, 0xF6, 0x90, 0xEF, 0x3E, 0xE7, 0x6, 0xC3, 0xD5, 0x2F, 0xC8, 0x66, 0x1E, 0xD7, 0x8, 0xE8, 0xEA, 0xDE, 0x80, 0x52, 0xEE, 0xF7, 0x84, 0xAA, 0x72, 0xAC, 0x35, 0x4D, 0x6A, 0x2A, 0x96, 0x1A, 0xD2, 0x71, 0x5A, 0x15, 0x49, 0x74, 0x4B, 0x9F, 0xD0, 0x5E, 0x4, 0x18, 0xA4, 0xEC, 0xC2, 0xE0, 0x41, 0x6E, 0xF, 0x51, 0xCB, 0xCC, 0x24, 0x91, 0xAF, 0x50, 0xA1, 0xF4, 0x70, 0x39, 0x99, 0x7C, 0x3A, 0x85, 0x23, 0xB8, 0xB4, 0x7A, 0xFC, 0x2, 0x36, 0x5B, 0x25, 0x55, 0x97, 0x31, 0x2D, 0x5D, 0xFA, 0x98, 0xE3, 0x8A, 0x92, 0xAE, 0x5, 0xDF, 0x29, 0x10, 0x67, 0x6C, 0xBA, 0xC9, 0xD3, 0x0, 0xE6, 0xCF, 0xE1, 0x9E, 0xA8, 0x2C, 0x63, 0x16, 0x1, 0x3F, 0x58, 0xE2, 0x89, 0xA9, 0xD, 0x38, 0x34, 0x1B, 0xAB, 0x33, 0xFF, 0xB0, 0xBB, 0x48, 0xC, 0x5F, 0xB9, 0xB1, 0xCD, 0x2E, 0xC5, 0xF3, 0xDB, 0x47, 0xE5, 0xA5, 0x9C, 0x77, 0xA, 0xA6, 0x20, 0x68, 0xFE, 0x7F, 0xC1, 0xAD };
static Il2CppFieldDefaultValueEntry t1703_f33_DefaultValue = 
{
	&t1703_f33_FieldInfo, { (char*)t1703_f33_DefaultValueData, &t1698_0_0_0 }};
static const uint8_t t1703_f34_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x4, 0x0, 0x0, 0x0, 0x8, 0x0, 0x0, 0x0, 0x10, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x40, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x1B, 0x0, 0x0, 0x0, 0x36, 0x0, 0x0, 0x0, 0x6C, 0x0, 0x0, 0x0, 0xD8, 0x0, 0x0, 0x0, 0xAB, 0x0, 0x0, 0x0, 0x4D, 0x0, 0x0, 0x0, 0x9A, 0x0, 0x0, 0x0, 0x2F, 0x0, 0x0, 0x0, 0x5E, 0x0, 0x0, 0x0, 0xBC, 0x0, 0x0, 0x0, 0x63, 0x0, 0x0, 0x0, 0xC6, 0x0, 0x0, 0x0, 0x97, 0x0, 0x0, 0x0, 0x35, 0x0, 0x0, 0x0, 0x6A, 0x0, 0x0, 0x0, 0xD4, 0x0, 0x0, 0x0, 0xB3, 0x0, 0x0, 0x0, 0x7D, 0x0, 0x0, 0x0, 0xFA, 0x0, 0x0, 0x0, 0xEF, 0x0, 0x0, 0x0, 0xC5 };
static Il2CppFieldDefaultValueEntry t1703_f34_DefaultValue = 
{
	&t1703_f34_FieldInfo, { (char*)t1703_f34_DefaultValueData, &t1686_0_0_0 }};
static const uint8_t t1703_f35_DefaultValueData[] = { 0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x1, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76, 0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0, 0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15, 0x4, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x5, 0x9A, 0x7, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75, 0x9, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84, 0x53, 0xD1, 0x0, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF, 0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x2, 0x7F, 0x50, 0x3C, 0x9F, 0xA8, 0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2, 0xCD, 0xC, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73, 0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0xB, 0xDB, 0xE0, 0x32, 0x3A, 0xA, 0x49, 0x6, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79, 0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x8, 0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A, 0x70, 0x3E, 0xB5, 0x66, 0x48, 0x3, 0xF6, 0xE, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E, 0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF, 0x8C, 0xA1, 0x89, 0xD, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0xF, 0xB0, 0x54, 0xBB, 0x16 };
static Il2CppFieldDefaultValueEntry t1703_f35_DefaultValue = 
{
	&t1703_f35_FieldInfo, { (char*)t1703_f35_DefaultValueData, &t1698_0_0_0 }};
static const uint8_t t1703_f36_DefaultValueData[] = { 0x52, 0x9, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB, 0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB, 0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0xB, 0x42, 0xFA, 0xC3, 0x4E, 0x8, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25, 0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92, 0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84, 0x90, 0xD8, 0xAB, 0x0, 0x8C, 0xBC, 0xD3, 0xA, 0xF7, 0xE4, 0x58, 0x5, 0xB8, 0xB3, 0x45, 0x6, 0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0xF, 0x2, 0xC1, 0xAF, 0xBD, 0x3, 0x1, 0x13, 0x8A, 0x6B, 0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73, 0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E, 0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0xE, 0xAA, 0x18, 0xBE, 0x1B, 0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4, 0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x7, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F, 0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0xD, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF, 0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61, 0x17, 0x2B, 0x4, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0xC, 0x7D };
static Il2CppFieldDefaultValueEntry t1703_f36_DefaultValue = 
{
	&t1703_f36_FieldInfo, { (char*)t1703_f36_DefaultValueData, &t1698_0_0_0 }};
static const uint8_t t1703_f37_DefaultValueData[] = { 0xA5, 0x63, 0x63, 0xC6, 0x84, 0x7C, 0x7C, 0xF8, 0x99, 0x77, 0x77, 0xEE, 0x8D, 0x7B, 0x7B, 0xF6, 0xD, 0xF2, 0xF2, 0xFF, 0xBD, 0x6B, 0x6B, 0xD6, 0xB1, 0x6F, 0x6F, 0xDE, 0x54, 0xC5, 0xC5, 0x91, 0x50, 0x30, 0x30, 0x60, 0x3, 0x1, 0x1, 0x2, 0xA9, 0x67, 0x67, 0xCE, 0x7D, 0x2B, 0x2B, 0x56, 0x19, 0xFE, 0xFE, 0xE7, 0x62, 0xD7, 0xD7, 0xB5, 0xE6, 0xAB, 0xAB, 0x4D, 0x9A, 0x76, 0x76, 0xEC, 0x45, 0xCA, 0xCA, 0x8F, 0x9D, 0x82, 0x82, 0x1F, 0x40, 0xC9, 0xC9, 0x89, 0x87, 0x7D, 0x7D, 0xFA, 0x15, 0xFA, 0xFA, 0xEF, 0xEB, 0x59, 0x59, 0xB2, 0xC9, 0x47, 0x47, 0x8E, 0xB, 0xF0, 0xF0, 0xFB, 0xEC, 0xAD, 0xAD, 0x41, 0x67, 0xD4, 0xD4, 0xB3, 0xFD, 0xA2, 0xA2, 0x5F, 0xEA, 0xAF, 0xAF, 0x45, 0xBF, 0x9C, 0x9C, 0x23, 0xF7, 0xA4, 0xA4, 0x53, 0x96, 0x72, 0x72, 0xE4, 0x5B, 0xC0, 0xC0, 0x9B, 0xC2, 0xB7, 0xB7, 0x75, 0x1C, 0xFD, 0xFD, 0xE1, 0xAE, 0x93, 0x93, 0x3D, 0x6A, 0x26, 0x26, 0x4C, 0x5A, 0x36, 0x36, 0x6C, 0x41, 0x3F, 0x3F, 0x7E, 0x2, 0xF7, 0xF7, 0xF5, 0x4F, 0xCC, 0xCC, 0x83, 0x5C, 0x34, 0x34, 0x68, 0xF4, 0xA5, 0xA5, 0x51, 0x34, 0xE5, 0xE5, 0xD1, 0x8, 0xF1, 0xF1, 0xF9, 0x93, 0x71, 0x71, 0xE2, 0x73, 0xD8, 0xD8, 0xAB, 0x53, 0x31, 0x31, 0x62, 0x3F, 0x15, 0x15, 0x2A, 0xC, 0x4, 0x4, 0x8, 0x52, 0xC7, 0xC7, 0x95, 0x65, 0x23, 0x23, 0x46, 0x5E, 0xC3, 0xC3, 0x9D, 0x28, 0x18, 0x18, 0x30, 0xA1, 0x96, 0x96, 0x37, 0xF, 0x5, 0x5, 0xA, 0xB5, 0x9A, 0x9A, 0x2F, 0x9, 0x7, 0x7, 0xE, 0x36, 0x12, 0x12, 0x24, 0x9B, 0x80, 0x80, 0x1B, 0x3D, 0xE2, 0xE2, 0xDF, 0x26, 0xEB, 0xEB, 0xCD, 0x69, 0x27, 0x27, 0x4E, 0xCD, 0xB2, 0xB2, 0x7F, 0x9F, 0x75, 0x75, 0xEA, 0x1B, 0x9, 0x9, 0x12, 0x9E, 0x83, 0x83, 0x1D, 0x74, 0x2C, 0x2C, 0x58, 0x2E, 0x1A, 0x1A, 0x34, 0x2D, 0x1B, 0x1B, 0x36, 0xB2, 0x6E, 0x6E, 0xDC, 0xEE, 0x5A, 0x5A, 0xB4, 0xFB, 0xA0, 0xA0, 0x5B, 0xF6, 0x52, 0x52, 0xA4, 0x4D, 0x3B, 0x3B, 0x76, 0x61, 0xD6, 0xD6, 0xB7, 0xCE, 0xB3, 0xB3, 0x7D, 0x7B, 0x29, 0x29, 0x52, 0x3E, 0xE3, 0xE3, 0xDD, 0x71, 0x2F, 0x2F, 0x5E, 0x97, 0x84, 0x84, 0x13, 0xF5, 0x53, 0x53, 0xA6, 0x68, 0xD1, 0xD1, 0xB9, 0x0, 0x0, 0x0, 0x0, 0x2C, 0xED, 0xED, 0xC1, 0x60, 0x20, 0x20, 0x40, 0x1F, 0xFC, 0xFC, 0xE3, 0xC8, 0xB1, 0xB1, 0x79, 0xED, 0x5B, 0x5B, 0xB6, 0xBE, 0x6A, 0x6A, 0xD4, 0x46, 0xCB, 0xCB, 0x8D, 0xD9, 0xBE, 0xBE, 0x67, 0x4B, 0x39, 0x39, 0x72, 0xDE, 0x4A, 0x4A, 0x94, 0xD4, 0x4C, 0x4C, 0x98, 0xE8, 0x58, 0x58, 0xB0, 0x4A, 0xCF, 0xCF, 0x85, 0x6B, 0xD0, 0xD0, 0xBB, 0x2A, 0xEF, 0xEF, 0xC5, 0xE5, 0xAA, 0xAA, 0x4F, 0x16, 0xFB, 0xFB, 0xED, 0xC5, 0x43, 0x43, 0x86, 0xD7, 0x4D, 0x4D, 0x9A, 0x55, 0x33, 0x33, 0x66, 0x94, 0x85, 0x85, 0x11, 0xCF, 0x45, 0x45, 0x8A, 0x10, 0xF9, 0xF9, 0xE9, 0x6, 0x2, 0x2, 0x4, 0x81, 0x7F, 0x7F, 0xFE, 0xF0, 0x50, 0x50, 0xA0, 0x44, 0x3C, 0x3C, 0x78, 0xBA, 0x9F, 0x9F, 0x25, 0xE3, 0xA8, 0xA8, 0x4B, 0xF3, 0x51, 0x51, 0xA2, 0xFE, 0xA3, 0xA3, 0x5D, 0xC0, 0x40, 0x40, 0x80, 0x8A, 0x8F, 0x8F, 0x5, 0xAD, 0x92, 0x92, 0x3F, 0xBC, 0x9D, 0x9D, 0x21, 0x48, 0x38, 0x38, 0x70, 0x4, 0xF5, 0xF5, 0xF1, 0xDF, 0xBC, 0xBC, 0x63, 0xC1, 0xB6, 0xB6, 0x77, 0x75, 0xDA, 0xDA, 0xAF, 0x63, 0x21, 0x21, 0x42, 0x30, 0x10, 0x10, 0x20, 0x1A, 0xFF, 0xFF, 0xE5, 0xE, 0xF3, 0xF3, 0xFD, 0x6D, 0xD2, 0xD2, 0xBF, 0x4C, 0xCD, 0xCD, 0x81, 0x14, 0xC, 0xC, 0x18, 0x35, 0x13, 0x13, 0x26, 0x2F, 0xEC, 0xEC, 0xC3, 0xE1, 0x5F, 0x5F, 0xBE, 0xA2, 0x97, 0x97, 0x35, 0xCC, 0x44, 0x44, 0x88, 0x39, 0x17, 0x17, 0x2E, 0x57, 0xC4, 0xC4, 0x93, 0xF2, 0xA7, 0xA7, 0x55, 0x82, 0x7E, 0x7E, 0xFC, 0x47, 0x3D, 0x3D, 0x7A, 0xAC, 0x64, 0x64, 0xC8, 0xE7, 0x5D, 0x5D, 0xBA, 0x2B, 0x19, 0x19, 0x32, 0x95, 0x73, 0x73, 0xE6, 0xA0, 0x60, 0x60, 0xC0, 0x98, 0x81, 0x81, 0x19, 0xD1, 0x4F, 0x4F, 0x9E, 0x7F, 0xDC, 0xDC, 0xA3, 0x66, 0x22, 0x22, 0x44, 0x7E, 0x2A, 0x2A, 0x54, 0xAB, 0x90, 0x90, 0x3B, 0x83, 0x88, 0x88, 0xB, 0xCA, 0x46, 0x46, 0x8C, 0x29, 0xEE, 0xEE, 0xC7, 0xD3, 0xB8, 0xB8, 0x6B, 0x3C, 0x14, 0x14, 0x28, 0x79, 0xDE, 0xDE, 0xA7, 0xE2, 0x5E, 0x5E, 0xBC, 0x1D, 0xB, 0xB, 0x16, 0x76, 0xDB, 0xDB, 0xAD, 0x3B, 0xE0, 0xE0, 0xDB, 0x56, 0x32, 0x32, 0x64, 0x4E, 0x3A, 0x3A, 0x74, 0x1E, 0xA, 0xA, 0x14, 0xDB, 0x49, 0x49, 0x92, 0xA, 0x6, 0x6, 0xC, 0x6C, 0x24, 0x24, 0x48, 0xE4, 0x5C, 0x5C, 0xB8, 0x5D, 0xC2, 0xC2, 0x9F, 0x6E, 0xD3, 0xD3, 0xBD, 0xEF, 0xAC, 0xAC, 0x43, 0xA6, 0x62, 0x62, 0xC4, 0xA8, 0x91, 0x91, 0x39, 0xA4, 0x95, 0x95, 0x31, 0x37, 0xE4, 0xE4, 0xD3, 0x8B, 0x79, 0x79, 0xF2, 0x32, 0xE7, 0xE7, 0xD5, 0x43, 0xC8, 0xC8, 0x8B, 0x59, 0x37, 0x37, 0x6E, 0xB7, 0x6D, 0x6D, 0xDA, 0x8C, 0x8D, 0x8D, 0x1, 0x64, 0xD5, 0xD5, 0xB1, 0xD2, 0x4E, 0x4E, 0x9C, 0xE0, 0xA9, 0xA9, 0x49, 0xB4, 0x6C, 0x6C, 0xD8, 0xFA, 0x56, 0x56, 0xAC, 0x7, 0xF4, 0xF4, 0xF3, 0x25, 0xEA, 0xEA, 0xCF, 0xAF, 0x65, 0x65, 0xCA, 0x8E, 0x7A, 0x7A, 0xF4, 0xE9, 0xAE, 0xAE, 0x47, 0x18, 0x8, 0x8, 0x10, 0xD5, 0xBA, 0xBA, 0x6F, 0x88, 0x78, 0x78, 0xF0, 0x6F, 0x25, 0x25, 0x4A, 0x72, 0x2E, 0x2E, 0x5C, 0x24, 0x1C, 0x1C, 0x38, 0xF1, 0xA6, 0xA6, 0x57, 0xC7, 0xB4, 0xB4, 0x73, 0x51, 0xC6, 0xC6, 0x97, 0x23, 0xE8, 0xE8, 0xCB, 0x7C, 0xDD, 0xDD, 0xA1, 0x9C, 0x74, 0x74, 0xE8, 0x21, 0x1F, 0x1F, 0x3E, 0xDD, 0x4B, 0x4B, 0x96, 0xDC, 0xBD, 0xBD, 0x61, 0x86, 0x8B, 0x8B, 0xD, 0x85, 0x8A, 0x8A, 0xF, 0x90, 0x70, 0x70, 0xE0, 0x42, 0x3E, 0x3E, 0x7C, 0xC4, 0xB5, 0xB5, 0x71, 0xAA, 0x66, 0x66, 0xCC, 0xD8, 0x48, 0x48, 0x90, 0x5, 0x3, 0x3, 0x6, 0x1, 0xF6, 0xF6, 0xF7, 0x12, 0xE, 0xE, 0x1C, 0xA3, 0x61, 0x61, 0xC2, 0x5F, 0x35, 0x35, 0x6A, 0xF9, 0x57, 0x57, 0xAE, 0xD0, 0xB9, 0xB9, 0x69, 0x91, 0x86, 0x86, 0x17, 0x58, 0xC1, 0xC1, 0x99, 0x27, 0x1D, 0x1D, 0x3A, 0xB9, 0x9E, 0x9E, 0x27, 0x38, 0xE1, 0xE1, 0xD9, 0x13, 0xF8, 0xF8, 0xEB, 0xB3, 0x98, 0x98, 0x2B, 0x33, 0x11, 0x11, 0x22, 0xBB, 0x69, 0x69, 0xD2, 0x70, 0xD9, 0xD9, 0xA9, 0x89, 0x8E, 0x8E, 0x7, 0xA7, 0x94, 0x94, 0x33, 0xB6, 0x9B, 0x9B, 0x2D, 0x22, 0x1E, 0x1E, 0x3C, 0x92, 0x87, 0x87, 0x15, 0x20, 0xE9, 0xE9, 0xC9, 0x49, 0xCE, 0xCE, 0x87, 0xFF, 0x55, 0x55, 0xAA, 0x78, 0x28, 0x28, 0x50, 0x7A, 0xDF, 0xDF, 0xA5, 0x8F, 0x8C, 0x8C, 0x3, 0xF8, 0xA1, 0xA1, 0x59, 0x80, 0x89, 0x89, 0x9, 0x17, 0xD, 0xD, 0x1A, 0xDA, 0xBF, 0xBF, 0x65, 0x31, 0xE6, 0xE6, 0xD7, 0xC6, 0x42, 0x42, 0x84, 0xB8, 0x68, 0x68, 0xD0, 0xC3, 0x41, 0x41, 0x82, 0xB0, 0x99, 0x99, 0x29, 0x77, 0x2D, 0x2D, 0x5A, 0x11, 0xF, 0xF, 0x1E, 0xCB, 0xB0, 0xB0, 0x7B, 0xFC, 0x54, 0x54, 0xA8, 0xD6, 0xBB, 0xBB, 0x6D, 0x3A, 0x16, 0x16, 0x2C };
static Il2CppFieldDefaultValueEntry t1703_f37_DefaultValue = 
{
	&t1703_f37_FieldInfo, { (char*)t1703_f37_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f38_DefaultValueData[] = { 0x63, 0x63, 0xC6, 0xA5, 0x7C, 0x7C, 0xF8, 0x84, 0x77, 0x77, 0xEE, 0x99, 0x7B, 0x7B, 0xF6, 0x8D, 0xF2, 0xF2, 0xFF, 0xD, 0x6B, 0x6B, 0xD6, 0xBD, 0x6F, 0x6F, 0xDE, 0xB1, 0xC5, 0xC5, 0x91, 0x54, 0x30, 0x30, 0x60, 0x50, 0x1, 0x1, 0x2, 0x3, 0x67, 0x67, 0xCE, 0xA9, 0x2B, 0x2B, 0x56, 0x7D, 0xFE, 0xFE, 0xE7, 0x19, 0xD7, 0xD7, 0xB5, 0x62, 0xAB, 0xAB, 0x4D, 0xE6, 0x76, 0x76, 0xEC, 0x9A, 0xCA, 0xCA, 0x8F, 0x45, 0x82, 0x82, 0x1F, 0x9D, 0xC9, 0xC9, 0x89, 0x40, 0x7D, 0x7D, 0xFA, 0x87, 0xFA, 0xFA, 0xEF, 0x15, 0x59, 0x59, 0xB2, 0xEB, 0x47, 0x47, 0x8E, 0xC9, 0xF0, 0xF0, 0xFB, 0xB, 0xAD, 0xAD, 0x41, 0xEC, 0xD4, 0xD4, 0xB3, 0x67, 0xA2, 0xA2, 0x5F, 0xFD, 0xAF, 0xAF, 0x45, 0xEA, 0x9C, 0x9C, 0x23, 0xBF, 0xA4, 0xA4, 0x53, 0xF7, 0x72, 0x72, 0xE4, 0x96, 0xC0, 0xC0, 0x9B, 0x5B, 0xB7, 0xB7, 0x75, 0xC2, 0xFD, 0xFD, 0xE1, 0x1C, 0x93, 0x93, 0x3D, 0xAE, 0x26, 0x26, 0x4C, 0x6A, 0x36, 0x36, 0x6C, 0x5A, 0x3F, 0x3F, 0x7E, 0x41, 0xF7, 0xF7, 0xF5, 0x2, 0xCC, 0xCC, 0x83, 0x4F, 0x34, 0x34, 0x68, 0x5C, 0xA5, 0xA5, 0x51, 0xF4, 0xE5, 0xE5, 0xD1, 0x34, 0xF1, 0xF1, 0xF9, 0x8, 0x71, 0x71, 0xE2, 0x93, 0xD8, 0xD8, 0xAB, 0x73, 0x31, 0x31, 0x62, 0x53, 0x15, 0x15, 0x2A, 0x3F, 0x4, 0x4, 0x8, 0xC, 0xC7, 0xC7, 0x95, 0x52, 0x23, 0x23, 0x46, 0x65, 0xC3, 0xC3, 0x9D, 0x5E, 0x18, 0x18, 0x30, 0x28, 0x96, 0x96, 0x37, 0xA1, 0x5, 0x5, 0xA, 0xF, 0x9A, 0x9A, 0x2F, 0xB5, 0x7, 0x7, 0xE, 0x9, 0x12, 0x12, 0x24, 0x36, 0x80, 0x80, 0x1B, 0x9B, 0xE2, 0xE2, 0xDF, 0x3D, 0xEB, 0xEB, 0xCD, 0x26, 0x27, 0x27, 0x4E, 0x69, 0xB2, 0xB2, 0x7F, 0xCD, 0x75, 0x75, 0xEA, 0x9F, 0x9, 0x9, 0x12, 0x1B, 0x83, 0x83, 0x1D, 0x9E, 0x2C, 0x2C, 0x58, 0x74, 0x1A, 0x1A, 0x34, 0x2E, 0x1B, 0x1B, 0x36, 0x2D, 0x6E, 0x6E, 0xDC, 0xB2, 0x5A, 0x5A, 0xB4, 0xEE, 0xA0, 0xA0, 0x5B, 0xFB, 0x52, 0x52, 0xA4, 0xF6, 0x3B, 0x3B, 0x76, 0x4D, 0xD6, 0xD6, 0xB7, 0x61, 0xB3, 0xB3, 0x7D, 0xCE, 0x29, 0x29, 0x52, 0x7B, 0xE3, 0xE3, 0xDD, 0x3E, 0x2F, 0x2F, 0x5E, 0x71, 0x84, 0x84, 0x13, 0x97, 0x53, 0x53, 0xA6, 0xF5, 0xD1, 0xD1, 0xB9, 0x68, 0x0, 0x0, 0x0, 0x0, 0xED, 0xED, 0xC1, 0x2C, 0x20, 0x20, 0x40, 0x60, 0xFC, 0xFC, 0xE3, 0x1F, 0xB1, 0xB1, 0x79, 0xC8, 0x5B, 0x5B, 0xB6, 0xED, 0x6A, 0x6A, 0xD4, 0xBE, 0xCB, 0xCB, 0x8D, 0x46, 0xBE, 0xBE, 0x67, 0xD9, 0x39, 0x39, 0x72, 0x4B, 0x4A, 0x4A, 0x94, 0xDE, 0x4C, 0x4C, 0x98, 0xD4, 0x58, 0x58, 0xB0, 0xE8, 0xCF, 0xCF, 0x85, 0x4A, 0xD0, 0xD0, 0xBB, 0x6B, 0xEF, 0xEF, 0xC5, 0x2A, 0xAA, 0xAA, 0x4F, 0xE5, 0xFB, 0xFB, 0xED, 0x16, 0x43, 0x43, 0x86, 0xC5, 0x4D, 0x4D, 0x9A, 0xD7, 0x33, 0x33, 0x66, 0x55, 0x85, 0x85, 0x11, 0x94, 0x45, 0x45, 0x8A, 0xCF, 0xF9, 0xF9, 0xE9, 0x10, 0x2, 0x2, 0x4, 0x6, 0x7F, 0x7F, 0xFE, 0x81, 0x50, 0x50, 0xA0, 0xF0, 0x3C, 0x3C, 0x78, 0x44, 0x9F, 0x9F, 0x25, 0xBA, 0xA8, 0xA8, 0x4B, 0xE3, 0x51, 0x51, 0xA2, 0xF3, 0xA3, 0xA3, 0x5D, 0xFE, 0x40, 0x40, 0x80, 0xC0, 0x8F, 0x8F, 0x5, 0x8A, 0x92, 0x92, 0x3F, 0xAD, 0x9D, 0x9D, 0x21, 0xBC, 0x38, 0x38, 0x70, 0x48, 0xF5, 0xF5, 0xF1, 0x4, 0xBC, 0xBC, 0x63, 0xDF, 0xB6, 0xB6, 0x77, 0xC1, 0xDA, 0xDA, 0xAF, 0x75, 0x21, 0x21, 0x42, 0x63, 0x10, 0x10, 0x20, 0x30, 0xFF, 0xFF, 0xE5, 0x1A, 0xF3, 0xF3, 0xFD, 0xE, 0xD2, 0xD2, 0xBF, 0x6D, 0xCD, 0xCD, 0x81, 0x4C, 0xC, 0xC, 0x18, 0x14, 0x13, 0x13, 0x26, 0x35, 0xEC, 0xEC, 0xC3, 0x2F, 0x5F, 0x5F, 0xBE, 0xE1, 0x97, 0x97, 0x35, 0xA2, 0x44, 0x44, 0x88, 0xCC, 0x17, 0x17, 0x2E, 0x39, 0xC4, 0xC4, 0x93, 0x57, 0xA7, 0xA7, 0x55, 0xF2, 0x7E, 0x7E, 0xFC, 0x82, 0x3D, 0x3D, 0x7A, 0x47, 0x64, 0x64, 0xC8, 0xAC, 0x5D, 0x5D, 0xBA, 0xE7, 0x19, 0x19, 0x32, 0x2B, 0x73, 0x73, 0xE6, 0x95, 0x60, 0x60, 0xC0, 0xA0, 0x81, 0x81, 0x19, 0x98, 0x4F, 0x4F, 0x9E, 0xD1, 0xDC, 0xDC, 0xA3, 0x7F, 0x22, 0x22, 0x44, 0x66, 0x2A, 0x2A, 0x54, 0x7E, 0x90, 0x90, 0x3B, 0xAB, 0x88, 0x88, 0xB, 0x83, 0x46, 0x46, 0x8C, 0xCA, 0xEE, 0xEE, 0xC7, 0x29, 0xB8, 0xB8, 0x6B, 0xD3, 0x14, 0x14, 0x28, 0x3C, 0xDE, 0xDE, 0xA7, 0x79, 0x5E, 0x5E, 0xBC, 0xE2, 0xB, 0xB, 0x16, 0x1D, 0xDB, 0xDB, 0xAD, 0x76, 0xE0, 0xE0, 0xDB, 0x3B, 0x32, 0x32, 0x64, 0x56, 0x3A, 0x3A, 0x74, 0x4E, 0xA, 0xA, 0x14, 0x1E, 0x49, 0x49, 0x92, 0xDB, 0x6, 0x6, 0xC, 0xA, 0x24, 0x24, 0x48, 0x6C, 0x5C, 0x5C, 0xB8, 0xE4, 0xC2, 0xC2, 0x9F, 0x5D, 0xD3, 0xD3, 0xBD, 0x6E, 0xAC, 0xAC, 0x43, 0xEF, 0x62, 0x62, 0xC4, 0xA6, 0x91, 0x91, 0x39, 0xA8, 0x95, 0x95, 0x31, 0xA4, 0xE4, 0xE4, 0xD3, 0x37, 0x79, 0x79, 0xF2, 0x8B, 0xE7, 0xE7, 0xD5, 0x32, 0xC8, 0xC8, 0x8B, 0x43, 0x37, 0x37, 0x6E, 0x59, 0x6D, 0x6D, 0xDA, 0xB7, 0x8D, 0x8D, 0x1, 0x8C, 0xD5, 0xD5, 0xB1, 0x64, 0x4E, 0x4E, 0x9C, 0xD2, 0xA9, 0xA9, 0x49, 0xE0, 0x6C, 0x6C, 0xD8, 0xB4, 0x56, 0x56, 0xAC, 0xFA, 0xF4, 0xF4, 0xF3, 0x7, 0xEA, 0xEA, 0xCF, 0x25, 0x65, 0x65, 0xCA, 0xAF, 0x7A, 0x7A, 0xF4, 0x8E, 0xAE, 0xAE, 0x47, 0xE9, 0x8, 0x8, 0x10, 0x18, 0xBA, 0xBA, 0x6F, 0xD5, 0x78, 0x78, 0xF0, 0x88, 0x25, 0x25, 0x4A, 0x6F, 0x2E, 0x2E, 0x5C, 0x72, 0x1C, 0x1C, 0x38, 0x24, 0xA6, 0xA6, 0x57, 0xF1, 0xB4, 0xB4, 0x73, 0xC7, 0xC6, 0xC6, 0x97, 0x51, 0xE8, 0xE8, 0xCB, 0x23, 0xDD, 0xDD, 0xA1, 0x7C, 0x74, 0x74, 0xE8, 0x9C, 0x1F, 0x1F, 0x3E, 0x21, 0x4B, 0x4B, 0x96, 0xDD, 0xBD, 0xBD, 0x61, 0xDC, 0x8B, 0x8B, 0xD, 0x86, 0x8A, 0x8A, 0xF, 0x85, 0x70, 0x70, 0xE0, 0x90, 0x3E, 0x3E, 0x7C, 0x42, 0xB5, 0xB5, 0x71, 0xC4, 0x66, 0x66, 0xCC, 0xAA, 0x48, 0x48, 0x90, 0xD8, 0x3, 0x3, 0x6, 0x5, 0xF6, 0xF6, 0xF7, 0x1, 0xE, 0xE, 0x1C, 0x12, 0x61, 0x61, 0xC2, 0xA3, 0x35, 0x35, 0x6A, 0x5F, 0x57, 0x57, 0xAE, 0xF9, 0xB9, 0xB9, 0x69, 0xD0, 0x86, 0x86, 0x17, 0x91, 0xC1, 0xC1, 0x99, 0x58, 0x1D, 0x1D, 0x3A, 0x27, 0x9E, 0x9E, 0x27, 0xB9, 0xE1, 0xE1, 0xD9, 0x38, 0xF8, 0xF8, 0xEB, 0x13, 0x98, 0x98, 0x2B, 0xB3, 0x11, 0x11, 0x22, 0x33, 0x69, 0x69, 0xD2, 0xBB, 0xD9, 0xD9, 0xA9, 0x70, 0x8E, 0x8E, 0x7, 0x89, 0x94, 0x94, 0x33, 0xA7, 0x9B, 0x9B, 0x2D, 0xB6, 0x1E, 0x1E, 0x3C, 0x22, 0x87, 0x87, 0x15, 0x92, 0xE9, 0xE9, 0xC9, 0x20, 0xCE, 0xCE, 0x87, 0x49, 0x55, 0x55, 0xAA, 0xFF, 0x28, 0x28, 0x50, 0x78, 0xDF, 0xDF, 0xA5, 0x7A, 0x8C, 0x8C, 0x3, 0x8F, 0xA1, 0xA1, 0x59, 0xF8, 0x89, 0x89, 0x9, 0x80, 0xD, 0xD, 0x1A, 0x17, 0xBF, 0xBF, 0x65, 0xDA, 0xE6, 0xE6, 0xD7, 0x31, 0x42, 0x42, 0x84, 0xC6, 0x68, 0x68, 0xD0, 0xB8, 0x41, 0x41, 0x82, 0xC3, 0x99, 0x99, 0x29, 0xB0, 0x2D, 0x2D, 0x5A, 0x77, 0xF, 0xF, 0x1E, 0x11, 0xB0, 0xB0, 0x7B, 0xCB, 0x54, 0x54, 0xA8, 0xFC, 0xBB, 0xBB, 0x6D, 0xD6, 0x16, 0x16, 0x2C, 0x3A };
static Il2CppFieldDefaultValueEntry t1703_f38_DefaultValue = 
{
	&t1703_f38_FieldInfo, { (char*)t1703_f38_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f39_DefaultValueData[] = { 0x63, 0xC6, 0xA5, 0x63, 0x7C, 0xF8, 0x84, 0x7C, 0x77, 0xEE, 0x99, 0x77, 0x7B, 0xF6, 0x8D, 0x7B, 0xF2, 0xFF, 0xD, 0xF2, 0x6B, 0xD6, 0xBD, 0x6B, 0x6F, 0xDE, 0xB1, 0x6F, 0xC5, 0x91, 0x54, 0xC5, 0x30, 0x60, 0x50, 0x30, 0x1, 0x2, 0x3, 0x1, 0x67, 0xCE, 0xA9, 0x67, 0x2B, 0x56, 0x7D, 0x2B, 0xFE, 0xE7, 0x19, 0xFE, 0xD7, 0xB5, 0x62, 0xD7, 0xAB, 0x4D, 0xE6, 0xAB, 0x76, 0xEC, 0x9A, 0x76, 0xCA, 0x8F, 0x45, 0xCA, 0x82, 0x1F, 0x9D, 0x82, 0xC9, 0x89, 0x40, 0xC9, 0x7D, 0xFA, 0x87, 0x7D, 0xFA, 0xEF, 0x15, 0xFA, 0x59, 0xB2, 0xEB, 0x59, 0x47, 0x8E, 0xC9, 0x47, 0xF0, 0xFB, 0xB, 0xF0, 0xAD, 0x41, 0xEC, 0xAD, 0xD4, 0xB3, 0x67, 0xD4, 0xA2, 0x5F, 0xFD, 0xA2, 0xAF, 0x45, 0xEA, 0xAF, 0x9C, 0x23, 0xBF, 0x9C, 0xA4, 0x53, 0xF7, 0xA4, 0x72, 0xE4, 0x96, 0x72, 0xC0, 0x9B, 0x5B, 0xC0, 0xB7, 0x75, 0xC2, 0xB7, 0xFD, 0xE1, 0x1C, 0xFD, 0x93, 0x3D, 0xAE, 0x93, 0x26, 0x4C, 0x6A, 0x26, 0x36, 0x6C, 0x5A, 0x36, 0x3F, 0x7E, 0x41, 0x3F, 0xF7, 0xF5, 0x2, 0xF7, 0xCC, 0x83, 0x4F, 0xCC, 0x34, 0x68, 0x5C, 0x34, 0xA5, 0x51, 0xF4, 0xA5, 0xE5, 0xD1, 0x34, 0xE5, 0xF1, 0xF9, 0x8, 0xF1, 0x71, 0xE2, 0x93, 0x71, 0xD8, 0xAB, 0x73, 0xD8, 0x31, 0x62, 0x53, 0x31, 0x15, 0x2A, 0x3F, 0x15, 0x4, 0x8, 0xC, 0x4, 0xC7, 0x95, 0x52, 0xC7, 0x23, 0x46, 0x65, 0x23, 0xC3, 0x9D, 0x5E, 0xC3, 0x18, 0x30, 0x28, 0x18, 0x96, 0x37, 0xA1, 0x96, 0x5, 0xA, 0xF, 0x5, 0x9A, 0x2F, 0xB5, 0x9A, 0x7, 0xE, 0x9, 0x7, 0x12, 0x24, 0x36, 0x12, 0x80, 0x1B, 0x9B, 0x80, 0xE2, 0xDF, 0x3D, 0xE2, 0xEB, 0xCD, 0x26, 0xEB, 0x27, 0x4E, 0x69, 0x27, 0xB2, 0x7F, 0xCD, 0xB2, 0x75, 0xEA, 0x9F, 0x75, 0x9, 0x12, 0x1B, 0x9, 0x83, 0x1D, 0x9E, 0x83, 0x2C, 0x58, 0x74, 0x2C, 0x1A, 0x34, 0x2E, 0x1A, 0x1B, 0x36, 0x2D, 0x1B, 0x6E, 0xDC, 0xB2, 0x6E, 0x5A, 0xB4, 0xEE, 0x5A, 0xA0, 0x5B, 0xFB, 0xA0, 0x52, 0xA4, 0xF6, 0x52, 0x3B, 0x76, 0x4D, 0x3B, 0xD6, 0xB7, 0x61, 0xD6, 0xB3, 0x7D, 0xCE, 0xB3, 0x29, 0x52, 0x7B, 0x29, 0xE3, 0xDD, 0x3E, 0xE3, 0x2F, 0x5E, 0x71, 0x2F, 0x84, 0x13, 0x97, 0x84, 0x53, 0xA6, 0xF5, 0x53, 0xD1, 0xB9, 0x68, 0xD1, 0x0, 0x0, 0x0, 0x0, 0xED, 0xC1, 0x2C, 0xED, 0x20, 0x40, 0x60, 0x20, 0xFC, 0xE3, 0x1F, 0xFC, 0xB1, 0x79, 0xC8, 0xB1, 0x5B, 0xB6, 0xED, 0x5B, 0x6A, 0xD4, 0xBE, 0x6A, 0xCB, 0x8D, 0x46, 0xCB, 0xBE, 0x67, 0xD9, 0xBE, 0x39, 0x72, 0x4B, 0x39, 0x4A, 0x94, 0xDE, 0x4A, 0x4C, 0x98, 0xD4, 0x4C, 0x58, 0xB0, 0xE8, 0x58, 0xCF, 0x85, 0x4A, 0xCF, 0xD0, 0xBB, 0x6B, 0xD0, 0xEF, 0xC5, 0x2A, 0xEF, 0xAA, 0x4F, 0xE5, 0xAA, 0xFB, 0xED, 0x16, 0xFB, 0x43, 0x86, 0xC5, 0x43, 0x4D, 0x9A, 0xD7, 0x4D, 0x33, 0x66, 0x55, 0x33, 0x85, 0x11, 0x94, 0x85, 0x45, 0x8A, 0xCF, 0x45, 0xF9, 0xE9, 0x10, 0xF9, 0x2, 0x4, 0x6, 0x2, 0x7F, 0xFE, 0x81, 0x7F, 0x50, 0xA0, 0xF0, 0x50, 0x3C, 0x78, 0x44, 0x3C, 0x9F, 0x25, 0xBA, 0x9F, 0xA8, 0x4B, 0xE3, 0xA8, 0x51, 0xA2, 0xF3, 0x51, 0xA3, 0x5D, 0xFE, 0xA3, 0x40, 0x80, 0xC0, 0x40, 0x8F, 0x5, 0x8A, 0x8F, 0x92, 0x3F, 0xAD, 0x92, 0x9D, 0x21, 0xBC, 0x9D, 0x38, 0x70, 0x48, 0x38, 0xF5, 0xF1, 0x4, 0xF5, 0xBC, 0x63, 0xDF, 0xBC, 0xB6, 0x77, 0xC1, 0xB6, 0xDA, 0xAF, 0x75, 0xDA, 0x21, 0x42, 0x63, 0x21, 0x10, 0x20, 0x30, 0x10, 0xFF, 0xE5, 0x1A, 0xFF, 0xF3, 0xFD, 0xE, 0xF3, 0xD2, 0xBF, 0x6D, 0xD2, 0xCD, 0x81, 0x4C, 0xCD, 0xC, 0x18, 0x14, 0xC, 0x13, 0x26, 0x35, 0x13, 0xEC, 0xC3, 0x2F, 0xEC, 0x5F, 0xBE, 0xE1, 0x5F, 0x97, 0x35, 0xA2, 0x97, 0x44, 0x88, 0xCC, 0x44, 0x17, 0x2E, 0x39, 0x17, 0xC4, 0x93, 0x57, 0xC4, 0xA7, 0x55, 0xF2, 0xA7, 0x7E, 0xFC, 0x82, 0x7E, 0x3D, 0x7A, 0x47, 0x3D, 0x64, 0xC8, 0xAC, 0x64, 0x5D, 0xBA, 0xE7, 0x5D, 0x19, 0x32, 0x2B, 0x19, 0x73, 0xE6, 0x95, 0x73, 0x60, 0xC0, 0xA0, 0x60, 0x81, 0x19, 0x98, 0x81, 0x4F, 0x9E, 0xD1, 0x4F, 0xDC, 0xA3, 0x7F, 0xDC, 0x22, 0x44, 0x66, 0x22, 0x2A, 0x54, 0x7E, 0x2A, 0x90, 0x3B, 0xAB, 0x90, 0x88, 0xB, 0x83, 0x88, 0x46, 0x8C, 0xCA, 0x46, 0xEE, 0xC7, 0x29, 0xEE, 0xB8, 0x6B, 0xD3, 0xB8, 0x14, 0x28, 0x3C, 0x14, 0xDE, 0xA7, 0x79, 0xDE, 0x5E, 0xBC, 0xE2, 0x5E, 0xB, 0x16, 0x1D, 0xB, 0xDB, 0xAD, 0x76, 0xDB, 0xE0, 0xDB, 0x3B, 0xE0, 0x32, 0x64, 0x56, 0x32, 0x3A, 0x74, 0x4E, 0x3A, 0xA, 0x14, 0x1E, 0xA, 0x49, 0x92, 0xDB, 0x49, 0x6, 0xC, 0xA, 0x6, 0x24, 0x48, 0x6C, 0x24, 0x5C, 0xB8, 0xE4, 0x5C, 0xC2, 0x9F, 0x5D, 0xC2, 0xD3, 0xBD, 0x6E, 0xD3, 0xAC, 0x43, 0xEF, 0xAC, 0x62, 0xC4, 0xA6, 0x62, 0x91, 0x39, 0xA8, 0x91, 0x95, 0x31, 0xA4, 0x95, 0xE4, 0xD3, 0x37, 0xE4, 0x79, 0xF2, 0x8B, 0x79, 0xE7, 0xD5, 0x32, 0xE7, 0xC8, 0x8B, 0x43, 0xC8, 0x37, 0x6E, 0x59, 0x37, 0x6D, 0xDA, 0xB7, 0x6D, 0x8D, 0x1, 0x8C, 0x8D, 0xD5, 0xB1, 0x64, 0xD5, 0x4E, 0x9C, 0xD2, 0x4E, 0xA9, 0x49, 0xE0, 0xA9, 0x6C, 0xD8, 0xB4, 0x6C, 0x56, 0xAC, 0xFA, 0x56, 0xF4, 0xF3, 0x7, 0xF4, 0xEA, 0xCF, 0x25, 0xEA, 0x65, 0xCA, 0xAF, 0x65, 0x7A, 0xF4, 0x8E, 0x7A, 0xAE, 0x47, 0xE9, 0xAE, 0x8, 0x10, 0x18, 0x8, 0xBA, 0x6F, 0xD5, 0xBA, 0x78, 0xF0, 0x88, 0x78, 0x25, 0x4A, 0x6F, 0x25, 0x2E, 0x5C, 0x72, 0x2E, 0x1C, 0x38, 0x24, 0x1C, 0xA6, 0x57, 0xF1, 0xA6, 0xB4, 0x73, 0xC7, 0xB4, 0xC6, 0x97, 0x51, 0xC6, 0xE8, 0xCB, 0x23, 0xE8, 0xDD, 0xA1, 0x7C, 0xDD, 0x74, 0xE8, 0x9C, 0x74, 0x1F, 0x3E, 0x21, 0x1F, 0x4B, 0x96, 0xDD, 0x4B, 0xBD, 0x61, 0xDC, 0xBD, 0x8B, 0xD, 0x86, 0x8B, 0x8A, 0xF, 0x85, 0x8A, 0x70, 0xE0, 0x90, 0x70, 0x3E, 0x7C, 0x42, 0x3E, 0xB5, 0x71, 0xC4, 0xB5, 0x66, 0xCC, 0xAA, 0x66, 0x48, 0x90, 0xD8, 0x48, 0x3, 0x6, 0x5, 0x3, 0xF6, 0xF7, 0x1, 0xF6, 0xE, 0x1C, 0x12, 0xE, 0x61, 0xC2, 0xA3, 0x61, 0x35, 0x6A, 0x5F, 0x35, 0x57, 0xAE, 0xF9, 0x57, 0xB9, 0x69, 0xD0, 0xB9, 0x86, 0x17, 0x91, 0x86, 0xC1, 0x99, 0x58, 0xC1, 0x1D, 0x3A, 0x27, 0x1D, 0x9E, 0x27, 0xB9, 0x9E, 0xE1, 0xD9, 0x38, 0xE1, 0xF8, 0xEB, 0x13, 0xF8, 0x98, 0x2B, 0xB3, 0x98, 0x11, 0x22, 0x33, 0x11, 0x69, 0xD2, 0xBB, 0x69, 0xD9, 0xA9, 0x70, 0xD9, 0x8E, 0x7, 0x89, 0x8E, 0x94, 0x33, 0xA7, 0x94, 0x9B, 0x2D, 0xB6, 0x9B, 0x1E, 0x3C, 0x22, 0x1E, 0x87, 0x15, 0x92, 0x87, 0xE9, 0xC9, 0x20, 0xE9, 0xCE, 0x87, 0x49, 0xCE, 0x55, 0xAA, 0xFF, 0x55, 0x28, 0x50, 0x78, 0x28, 0xDF, 0xA5, 0x7A, 0xDF, 0x8C, 0x3, 0x8F, 0x8C, 0xA1, 0x59, 0xF8, 0xA1, 0x89, 0x9, 0x80, 0x89, 0xD, 0x1A, 0x17, 0xD, 0xBF, 0x65, 0xDA, 0xBF, 0xE6, 0xD7, 0x31, 0xE6, 0x42, 0x84, 0xC6, 0x42, 0x68, 0xD0, 0xB8, 0x68, 0x41, 0x82, 0xC3, 0x41, 0x99, 0x29, 0xB0, 0x99, 0x2D, 0x5A, 0x77, 0x2D, 0xF, 0x1E, 0x11, 0xF, 0xB0, 0x7B, 0xCB, 0xB0, 0x54, 0xA8, 0xFC, 0x54, 0xBB, 0x6D, 0xD6, 0xBB, 0x16, 0x2C, 0x3A, 0x16 };
static Il2CppFieldDefaultValueEntry t1703_f39_DefaultValue = 
{
	&t1703_f39_FieldInfo, { (char*)t1703_f39_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f40_DefaultValueData[] = { 0xC6, 0xA5, 0x63, 0x63, 0xF8, 0x84, 0x7C, 0x7C, 0xEE, 0x99, 0x77, 0x77, 0xF6, 0x8D, 0x7B, 0x7B, 0xFF, 0xD, 0xF2, 0xF2, 0xD6, 0xBD, 0x6B, 0x6B, 0xDE, 0xB1, 0x6F, 0x6F, 0x91, 0x54, 0xC5, 0xC5, 0x60, 0x50, 0x30, 0x30, 0x2, 0x3, 0x1, 0x1, 0xCE, 0xA9, 0x67, 0x67, 0x56, 0x7D, 0x2B, 0x2B, 0xE7, 0x19, 0xFE, 0xFE, 0xB5, 0x62, 0xD7, 0xD7, 0x4D, 0xE6, 0xAB, 0xAB, 0xEC, 0x9A, 0x76, 0x76, 0x8F, 0x45, 0xCA, 0xCA, 0x1F, 0x9D, 0x82, 0x82, 0x89, 0x40, 0xC9, 0xC9, 0xFA, 0x87, 0x7D, 0x7D, 0xEF, 0x15, 0xFA, 0xFA, 0xB2, 0xEB, 0x59, 0x59, 0x8E, 0xC9, 0x47, 0x47, 0xFB, 0xB, 0xF0, 0xF0, 0x41, 0xEC, 0xAD, 0xAD, 0xB3, 0x67, 0xD4, 0xD4, 0x5F, 0xFD, 0xA2, 0xA2, 0x45, 0xEA, 0xAF, 0xAF, 0x23, 0xBF, 0x9C, 0x9C, 0x53, 0xF7, 0xA4, 0xA4, 0xE4, 0x96, 0x72, 0x72, 0x9B, 0x5B, 0xC0, 0xC0, 0x75, 0xC2, 0xB7, 0xB7, 0xE1, 0x1C, 0xFD, 0xFD, 0x3D, 0xAE, 0x93, 0x93, 0x4C, 0x6A, 0x26, 0x26, 0x6C, 0x5A, 0x36, 0x36, 0x7E, 0x41, 0x3F, 0x3F, 0xF5, 0x2, 0xF7, 0xF7, 0x83, 0x4F, 0xCC, 0xCC, 0x68, 0x5C, 0x34, 0x34, 0x51, 0xF4, 0xA5, 0xA5, 0xD1, 0x34, 0xE5, 0xE5, 0xF9, 0x8, 0xF1, 0xF1, 0xE2, 0x93, 0x71, 0x71, 0xAB, 0x73, 0xD8, 0xD8, 0x62, 0x53, 0x31, 0x31, 0x2A, 0x3F, 0x15, 0x15, 0x8, 0xC, 0x4, 0x4, 0x95, 0x52, 0xC7, 0xC7, 0x46, 0x65, 0x23, 0x23, 0x9D, 0x5E, 0xC3, 0xC3, 0x30, 0x28, 0x18, 0x18, 0x37, 0xA1, 0x96, 0x96, 0xA, 0xF, 0x5, 0x5, 0x2F, 0xB5, 0x9A, 0x9A, 0xE, 0x9, 0x7, 0x7, 0x24, 0x36, 0x12, 0x12, 0x1B, 0x9B, 0x80, 0x80, 0xDF, 0x3D, 0xE2, 0xE2, 0xCD, 0x26, 0xEB, 0xEB, 0x4E, 0x69, 0x27, 0x27, 0x7F, 0xCD, 0xB2, 0xB2, 0xEA, 0x9F, 0x75, 0x75, 0x12, 0x1B, 0x9, 0x9, 0x1D, 0x9E, 0x83, 0x83, 0x58, 0x74, 0x2C, 0x2C, 0x34, 0x2E, 0x1A, 0x1A, 0x36, 0x2D, 0x1B, 0x1B, 0xDC, 0xB2, 0x6E, 0x6E, 0xB4, 0xEE, 0x5A, 0x5A, 0x5B, 0xFB, 0xA0, 0xA0, 0xA4, 0xF6, 0x52, 0x52, 0x76, 0x4D, 0x3B, 0x3B, 0xB7, 0x61, 0xD6, 0xD6, 0x7D, 0xCE, 0xB3, 0xB3, 0x52, 0x7B, 0x29, 0x29, 0xDD, 0x3E, 0xE3, 0xE3, 0x5E, 0x71, 0x2F, 0x2F, 0x13, 0x97, 0x84, 0x84, 0xA6, 0xF5, 0x53, 0x53, 0xB9, 0x68, 0xD1, 0xD1, 0x0, 0x0, 0x0, 0x0, 0xC1, 0x2C, 0xED, 0xED, 0x40, 0x60, 0x20, 0x20, 0xE3, 0x1F, 0xFC, 0xFC, 0x79, 0xC8, 0xB1, 0xB1, 0xB6, 0xED, 0x5B, 0x5B, 0xD4, 0xBE, 0x6A, 0x6A, 0x8D, 0x46, 0xCB, 0xCB, 0x67, 0xD9, 0xBE, 0xBE, 0x72, 0x4B, 0x39, 0x39, 0x94, 0xDE, 0x4A, 0x4A, 0x98, 0xD4, 0x4C, 0x4C, 0xB0, 0xE8, 0x58, 0x58, 0x85, 0x4A, 0xCF, 0xCF, 0xBB, 0x6B, 0xD0, 0xD0, 0xC5, 0x2A, 0xEF, 0xEF, 0x4F, 0xE5, 0xAA, 0xAA, 0xED, 0x16, 0xFB, 0xFB, 0x86, 0xC5, 0x43, 0x43, 0x9A, 0xD7, 0x4D, 0x4D, 0x66, 0x55, 0x33, 0x33, 0x11, 0x94, 0x85, 0x85, 0x8A, 0xCF, 0x45, 0x45, 0xE9, 0x10, 0xF9, 0xF9, 0x4, 0x6, 0x2, 0x2, 0xFE, 0x81, 0x7F, 0x7F, 0xA0, 0xF0, 0x50, 0x50, 0x78, 0x44, 0x3C, 0x3C, 0x25, 0xBA, 0x9F, 0x9F, 0x4B, 0xE3, 0xA8, 0xA8, 0xA2, 0xF3, 0x51, 0x51, 0x5D, 0xFE, 0xA3, 0xA3, 0x80, 0xC0, 0x40, 0x40, 0x5, 0x8A, 0x8F, 0x8F, 0x3F, 0xAD, 0x92, 0x92, 0x21, 0xBC, 0x9D, 0x9D, 0x70, 0x48, 0x38, 0x38, 0xF1, 0x4, 0xF5, 0xF5, 0x63, 0xDF, 0xBC, 0xBC, 0x77, 0xC1, 0xB6, 0xB6, 0xAF, 0x75, 0xDA, 0xDA, 0x42, 0x63, 0x21, 0x21, 0x20, 0x30, 0x10, 0x10, 0xE5, 0x1A, 0xFF, 0xFF, 0xFD, 0xE, 0xF3, 0xF3, 0xBF, 0x6D, 0xD2, 0xD2, 0x81, 0x4C, 0xCD, 0xCD, 0x18, 0x14, 0xC, 0xC, 0x26, 0x35, 0x13, 0x13, 0xC3, 0x2F, 0xEC, 0xEC, 0xBE, 0xE1, 0x5F, 0x5F, 0x35, 0xA2, 0x97, 0x97, 0x88, 0xCC, 0x44, 0x44, 0x2E, 0x39, 0x17, 0x17, 0x93, 0x57, 0xC4, 0xC4, 0x55, 0xF2, 0xA7, 0xA7, 0xFC, 0x82, 0x7E, 0x7E, 0x7A, 0x47, 0x3D, 0x3D, 0xC8, 0xAC, 0x64, 0x64, 0xBA, 0xE7, 0x5D, 0x5D, 0x32, 0x2B, 0x19, 0x19, 0xE6, 0x95, 0x73, 0x73, 0xC0, 0xA0, 0x60, 0x60, 0x19, 0x98, 0x81, 0x81, 0x9E, 0xD1, 0x4F, 0x4F, 0xA3, 0x7F, 0xDC, 0xDC, 0x44, 0x66, 0x22, 0x22, 0x54, 0x7E, 0x2A, 0x2A, 0x3B, 0xAB, 0x90, 0x90, 0xB, 0x83, 0x88, 0x88, 0x8C, 0xCA, 0x46, 0x46, 0xC7, 0x29, 0xEE, 0xEE, 0x6B, 0xD3, 0xB8, 0xB8, 0x28, 0x3C, 0x14, 0x14, 0xA7, 0x79, 0xDE, 0xDE, 0xBC, 0xE2, 0x5E, 0x5E, 0x16, 0x1D, 0xB, 0xB, 0xAD, 0x76, 0xDB, 0xDB, 0xDB, 0x3B, 0xE0, 0xE0, 0x64, 0x56, 0x32, 0x32, 0x74, 0x4E, 0x3A, 0x3A, 0x14, 0x1E, 0xA, 0xA, 0x92, 0xDB, 0x49, 0x49, 0xC, 0xA, 0x6, 0x6, 0x48, 0x6C, 0x24, 0x24, 0xB8, 0xE4, 0x5C, 0x5C, 0x9F, 0x5D, 0xC2, 0xC2, 0xBD, 0x6E, 0xD3, 0xD3, 0x43, 0xEF, 0xAC, 0xAC, 0xC4, 0xA6, 0x62, 0x62, 0x39, 0xA8, 0x91, 0x91, 0x31, 0xA4, 0x95, 0x95, 0xD3, 0x37, 0xE4, 0xE4, 0xF2, 0x8B, 0x79, 0x79, 0xD5, 0x32, 0xE7, 0xE7, 0x8B, 0x43, 0xC8, 0xC8, 0x6E, 0x59, 0x37, 0x37, 0xDA, 0xB7, 0x6D, 0x6D, 0x1, 0x8C, 0x8D, 0x8D, 0xB1, 0x64, 0xD5, 0xD5, 0x9C, 0xD2, 0x4E, 0x4E, 0x49, 0xE0, 0xA9, 0xA9, 0xD8, 0xB4, 0x6C, 0x6C, 0xAC, 0xFA, 0x56, 0x56, 0xF3, 0x7, 0xF4, 0xF4, 0xCF, 0x25, 0xEA, 0xEA, 0xCA, 0xAF, 0x65, 0x65, 0xF4, 0x8E, 0x7A, 0x7A, 0x47, 0xE9, 0xAE, 0xAE, 0x10, 0x18, 0x8, 0x8, 0x6F, 0xD5, 0xBA, 0xBA, 0xF0, 0x88, 0x78, 0x78, 0x4A, 0x6F, 0x25, 0x25, 0x5C, 0x72, 0x2E, 0x2E, 0x38, 0x24, 0x1C, 0x1C, 0x57, 0xF1, 0xA6, 0xA6, 0x73, 0xC7, 0xB4, 0xB4, 0x97, 0x51, 0xC6, 0xC6, 0xCB, 0x23, 0xE8, 0xE8, 0xA1, 0x7C, 0xDD, 0xDD, 0xE8, 0x9C, 0x74, 0x74, 0x3E, 0x21, 0x1F, 0x1F, 0x96, 0xDD, 0x4B, 0x4B, 0x61, 0xDC, 0xBD, 0xBD, 0xD, 0x86, 0x8B, 0x8B, 0xF, 0x85, 0x8A, 0x8A, 0xE0, 0x90, 0x70, 0x70, 0x7C, 0x42, 0x3E, 0x3E, 0x71, 0xC4, 0xB5, 0xB5, 0xCC, 0xAA, 0x66, 0x66, 0x90, 0xD8, 0x48, 0x48, 0x6, 0x5, 0x3, 0x3, 0xF7, 0x1, 0xF6, 0xF6, 0x1C, 0x12, 0xE, 0xE, 0xC2, 0xA3, 0x61, 0x61, 0x6A, 0x5F, 0x35, 0x35, 0xAE, 0xF9, 0x57, 0x57, 0x69, 0xD0, 0xB9, 0xB9, 0x17, 0x91, 0x86, 0x86, 0x99, 0x58, 0xC1, 0xC1, 0x3A, 0x27, 0x1D, 0x1D, 0x27, 0xB9, 0x9E, 0x9E, 0xD9, 0x38, 0xE1, 0xE1, 0xEB, 0x13, 0xF8, 0xF8, 0x2B, 0xB3, 0x98, 0x98, 0x22, 0x33, 0x11, 0x11, 0xD2, 0xBB, 0x69, 0x69, 0xA9, 0x70, 0xD9, 0xD9, 0x7, 0x89, 0x8E, 0x8E, 0x33, 0xA7, 0x94, 0x94, 0x2D, 0xB6, 0x9B, 0x9B, 0x3C, 0x22, 0x1E, 0x1E, 0x15, 0x92, 0x87, 0x87, 0xC9, 0x20, 0xE9, 0xE9, 0x87, 0x49, 0xCE, 0xCE, 0xAA, 0xFF, 0x55, 0x55, 0x50, 0x78, 0x28, 0x28, 0xA5, 0x7A, 0xDF, 0xDF, 0x3, 0x8F, 0x8C, 0x8C, 0x59, 0xF8, 0xA1, 0xA1, 0x9, 0x80, 0x89, 0x89, 0x1A, 0x17, 0xD, 0xD, 0x65, 0xDA, 0xBF, 0xBF, 0xD7, 0x31, 0xE6, 0xE6, 0x84, 0xC6, 0x42, 0x42, 0xD0, 0xB8, 0x68, 0x68, 0x82, 0xC3, 0x41, 0x41, 0x29, 0xB0, 0x99, 0x99, 0x5A, 0x77, 0x2D, 0x2D, 0x1E, 0x11, 0xF, 0xF, 0x7B, 0xCB, 0xB0, 0xB0, 0xA8, 0xFC, 0x54, 0x54, 0x6D, 0xD6, 0xBB, 0xBB, 0x2C, 0x3A, 0x16, 0x16 };
static Il2CppFieldDefaultValueEntry t1703_f40_DefaultValue = 
{
	&t1703_f40_FieldInfo, { (char*)t1703_f40_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f41_DefaultValueData[] = { 0x50, 0xA7, 0xF4, 0x51, 0x53, 0x65, 0x41, 0x7E, 0xC3, 0xA4, 0x17, 0x1A, 0x96, 0x5E, 0x27, 0x3A, 0xCB, 0x6B, 0xAB, 0x3B, 0xF1, 0x45, 0x9D, 0x1F, 0xAB, 0x58, 0xFA, 0xAC, 0x93, 0x3, 0xE3, 0x4B, 0x55, 0xFA, 0x30, 0x20, 0xF6, 0x6D, 0x76, 0xAD, 0x91, 0x76, 0xCC, 0x88, 0x25, 0x4C, 0x2, 0xF5, 0xFC, 0xD7, 0xE5, 0x4F, 0xD7, 0xCB, 0x2A, 0xC5, 0x80, 0x44, 0x35, 0x26, 0x8F, 0xA3, 0x62, 0xB5, 0x49, 0x5A, 0xB1, 0xDE, 0x67, 0x1B, 0xBA, 0x25, 0x98, 0xE, 0xEA, 0x45, 0xE1, 0xC0, 0xFE, 0x5D, 0x2, 0x75, 0x2F, 0xC3, 0x12, 0xF0, 0x4C, 0x81, 0xA3, 0x97, 0x46, 0x8D, 0xC6, 0xF9, 0xD3, 0x6B, 0xE7, 0x5F, 0x8F, 0x3, 0x95, 0x9C, 0x92, 0x15, 0xEB, 0x7A, 0x6D, 0xBF, 0xDA, 0x59, 0x52, 0x95, 0x2D, 0x83, 0xBE, 0xD4, 0xD3, 0x21, 0x74, 0x58, 0x29, 0x69, 0xE0, 0x49, 0x44, 0xC8, 0xC9, 0x8E, 0x6A, 0x89, 0xC2, 0x75, 0x78, 0x79, 0x8E, 0xF4, 0x6B, 0x3E, 0x58, 0x99, 0xDD, 0x71, 0xB9, 0x27, 0xB6, 0x4F, 0xE1, 0xBE, 0x17, 0xAD, 0x88, 0xF0, 0x66, 0xAC, 0x20, 0xC9, 0xB4, 0x3A, 0xCE, 0x7D, 0x18, 0x4A, 0xDF, 0x63, 0x82, 0x31, 0x1A, 0xE5, 0x60, 0x33, 0x51, 0x97, 0x45, 0x7F, 0x53, 0x62, 0xE0, 0x77, 0x64, 0xB1, 0x84, 0xAE, 0x6B, 0xBB, 0x1C, 0xA0, 0x81, 0xFE, 0x94, 0x2B, 0x8, 0xF9, 0x58, 0x68, 0x48, 0x70, 0x19, 0xFD, 0x45, 0x8F, 0x87, 0x6C, 0xDE, 0x94, 0xB7, 0xF8, 0x7B, 0x52, 0x23, 0xD3, 0x73, 0xAB, 0xE2, 0x2, 0x4B, 0x72, 0x57, 0x8F, 0x1F, 0xE3, 0x2A, 0xAB, 0x55, 0x66, 0x7, 0x28, 0xEB, 0xB2, 0x3, 0xC2, 0xB5, 0x2F, 0x9A, 0x7B, 0xC5, 0x86, 0xA5, 0x8, 0x37, 0xD3, 0xF2, 0x87, 0x28, 0x30, 0xB2, 0xA5, 0xBF, 0x23, 0xBA, 0x6A, 0x3, 0x2, 0x5C, 0x82, 0x16, 0xED, 0x2B, 0x1C, 0xCF, 0x8A, 0x92, 0xB4, 0x79, 0xA7, 0xF0, 0xF2, 0x7, 0xF3, 0xA1, 0xE2, 0x69, 0x4E, 0xCD, 0xF4, 0xDA, 0x65, 0xD5, 0xBE, 0x5, 0x6, 0x1F, 0x62, 0x34, 0xD1, 0x8A, 0xFE, 0xA6, 0xC4, 0x9D, 0x53, 0x2E, 0x34, 0xA0, 0x55, 0xF3, 0xA2, 0x32, 0xE1, 0x8A, 0x5, 0x75, 0xEB, 0xF6, 0xA4, 0x39, 0xEC, 0x83, 0xB, 0xAA, 0xEF, 0x60, 0x40, 0x6, 0x9F, 0x71, 0x5E, 0x51, 0x10, 0x6E, 0xBD, 0xF9, 0x8A, 0x21, 0x3E, 0x3D, 0x6, 0xDD, 0x96, 0xAE, 0x5, 0x3E, 0xDD, 0x46, 0xBD, 0xE6, 0x4D, 0xB5, 0x8D, 0x54, 0x91, 0x5, 0x5D, 0xC4, 0x71, 0x6F, 0xD4, 0x6, 0x4, 0xFF, 0x15, 0x50, 0x60, 0x24, 0xFB, 0x98, 0x19, 0x97, 0xE9, 0xBD, 0xD6, 0xCC, 0x43, 0x40, 0x89, 0x77, 0x9E, 0xD9, 0x67, 0xBD, 0x42, 0xE8, 0xB0, 0x88, 0x8B, 0x89, 0x7, 0x38, 0x5B, 0x19, 0xE7, 0xDB, 0xEE, 0xC8, 0x79, 0x47, 0xA, 0x7C, 0xA1, 0xE9, 0xF, 0x42, 0x7C, 0xC9, 0x1E, 0x84, 0xF8, 0x0, 0x0, 0x0, 0x0, 0x83, 0x86, 0x80, 0x9, 0x48, 0xED, 0x2B, 0x32, 0xAC, 0x70, 0x11, 0x1E, 0x4E, 0x72, 0x5A, 0x6C, 0xFB, 0xFF, 0xE, 0xFD, 0x56, 0x38, 0x85, 0xF, 0x1E, 0xD5, 0xAE, 0x3D, 0x27, 0x39, 0x2D, 0x36, 0x64, 0xD9, 0xF, 0xA, 0x21, 0xA6, 0x5C, 0x68, 0xD1, 0x54, 0x5B, 0x9B, 0x3A, 0x2E, 0x36, 0x24, 0xB1, 0x67, 0xA, 0xC, 0xF, 0xE7, 0x57, 0x93, 0xD2, 0x96, 0xEE, 0xB4, 0x9E, 0x91, 0x9B, 0x1B, 0x4F, 0xC5, 0xC0, 0x80, 0xA2, 0x20, 0xDC, 0x61, 0x69, 0x4B, 0x77, 0x5A, 0x16, 0x1A, 0x12, 0x1C, 0xA, 0xBA, 0x93, 0xE2, 0xE5, 0x2A, 0xA0, 0xC0, 0x43, 0xE0, 0x22, 0x3C, 0x1D, 0x17, 0x1B, 0x12, 0xB, 0xD, 0x9, 0xE, 0xAD, 0xC7, 0x8B, 0xF2, 0xB9, 0xA8, 0xB6, 0x2D, 0xC8, 0xA9, 0x1E, 0x14, 0x85, 0x19, 0xF1, 0x57, 0x4C, 0x7, 0x75, 0xAF, 0xBB, 0xDD, 0x99, 0xEE, 0xFD, 0x60, 0x7F, 0xA3, 0x9F, 0x26, 0x1, 0xF7, 0xBC, 0xF5, 0x72, 0x5C, 0xC5, 0x3B, 0x66, 0x44, 0x34, 0x7E, 0xFB, 0x5B, 0x76, 0x29, 0x43, 0x8B, 0xDC, 0xC6, 0x23, 0xCB, 0x68, 0xFC, 0xED, 0xB6, 0x63, 0xF1, 0xE4, 0xB8, 0xCA, 0xDC, 0x31, 0xD7, 0x10, 0x85, 0x63, 0x42, 0x40, 0x22, 0x97, 0x13, 0x20, 0x11, 0xC6, 0x84, 0x7D, 0x24, 0x4A, 0x85, 0xF8, 0x3D, 0xBB, 0xD2, 0x11, 0x32, 0xF9, 0xAE, 0x6D, 0xA1, 0x29, 0xC7, 0x4B, 0x2F, 0x9E, 0x1D, 0xF3, 0x30, 0xB2, 0xDC, 0xEC, 0x52, 0x86, 0xD, 0xD0, 0xE3, 0xC1, 0x77, 0x6C, 0x16, 0xB3, 0x2B, 0x99, 0xB9, 0x70, 0xA9, 0xFA, 0x48, 0x94, 0x11, 0x22, 0x64, 0xE9, 0x47, 0xC4, 0x8C, 0xFC, 0xA8, 0x1A, 0x3F, 0xF0, 0xA0, 0xD8, 0x2C, 0x7D, 0x56, 0xEF, 0x90, 0x33, 0x22, 0xC7, 0x4E, 0x49, 0x87, 0xC1, 0xD1, 0x38, 0xD9, 0xFE, 0xA2, 0xCA, 0x8C, 0x36, 0xB, 0xD4, 0x98, 0xCF, 0x81, 0xF5, 0xA6, 0x28, 0xDE, 0x7A, 0xA5, 0x26, 0x8E, 0xB7, 0xDA, 0xA4, 0xBF, 0xAD, 0x3F, 0xE4, 0x9D, 0x3A, 0x2C, 0xD, 0x92, 0x78, 0x50, 0x9B, 0xCC, 0x5F, 0x6A, 0x62, 0x46, 0x7E, 0x54, 0xC2, 0x13, 0x8D, 0xF6, 0xE8, 0xB8, 0xD8, 0x90, 0x5E, 0xF7, 0x39, 0x2E, 0xF5, 0xAF, 0xC3, 0x82, 0xBE, 0x80, 0x5D, 0x9F, 0x7C, 0x93, 0xD0, 0x69, 0xA9, 0x2D, 0xD5, 0x6F, 0xB3, 0x12, 0x25, 0xCF, 0x3B, 0x99, 0xAC, 0xC8, 0xA7, 0x7D, 0x18, 0x10, 0x6E, 0x63, 0x9C, 0xE8, 0x7B, 0xBB, 0x3B, 0xDB, 0x9, 0x78, 0x26, 0xCD, 0xF4, 0x18, 0x59, 0x6E, 0x1, 0xB7, 0x9A, 0xEC, 0xA8, 0x9A, 0x4F, 0x83, 0x65, 0x6E, 0x95, 0xE6, 0x7E, 0xE6, 0xFF, 0xAA, 0x8, 0xCF, 0xBC, 0x21, 0xE6, 0xE8, 0x15, 0xEF, 0xD9, 0x9B, 0xE7, 0xBA, 0xCE, 0x36, 0x6F, 0x4A, 0xD4, 0x9, 0x9F, 0xEA, 0xD6, 0x7C, 0xB0, 0x29, 0xAF, 0xB2, 0xA4, 0x31, 0x31, 0x23, 0x3F, 0x2A, 0x30, 0x94, 0xA5, 0xC6, 0xC0, 0x66, 0xA2, 0x35, 0x37, 0xBC, 0x4E, 0x74, 0xA6, 0xCA, 0x82, 0xFC, 0xB0, 0xD0, 0x90, 0xE0, 0x15, 0xD8, 0xA7, 0x33, 0x4A, 0x98, 0x4, 0xF1, 0xF7, 0xDA, 0xEC, 0x41, 0xE, 0x50, 0xCD, 0x7F, 0x2F, 0xF6, 0x91, 0x17, 0x8D, 0xD6, 0x4D, 0x76, 0x4D, 0xB0, 0xEF, 0x43, 0x54, 0x4D, 0xAA, 0xCC, 0xDF, 0x4, 0x96, 0xE4, 0xE3, 0xB5, 0xD1, 0x9E, 0x1B, 0x88, 0x6A, 0x4C, 0xB8, 0x1F, 0x2C, 0xC1, 0x7F, 0x51, 0x65, 0x46, 0x4, 0xEA, 0x5E, 0x9D, 0x5D, 0x35, 0x8C, 0x1, 0x73, 0x74, 0x87, 0xFA, 0x2E, 0x41, 0xB, 0xFB, 0x5A, 0x1D, 0x67, 0xB3, 0x52, 0xD2, 0xDB, 0x92, 0x33, 0x56, 0x10, 0xE9, 0x13, 0x47, 0xD6, 0x6D, 0x8C, 0x61, 0xD7, 0x9A, 0x7A, 0xC, 0xA1, 0x37, 0x8E, 0x14, 0xF8, 0x59, 0x89, 0x3C, 0x13, 0xEB, 0xEE, 0x27, 0xA9, 0xCE, 0x35, 0xC9, 0x61, 0xB7, 0xED, 0xE5, 0x1C, 0xE1, 0x3C, 0xB1, 0x47, 0x7A, 0x59, 0xDF, 0xD2, 0x9C, 0x3F, 0x73, 0xF2, 0x55, 0x79, 0xCE, 0x14, 0x18, 0xBF, 0x37, 0xC7, 0x73, 0xEA, 0xCD, 0xF7, 0x53, 0x5B, 0xAA, 0xFD, 0x5F, 0x14, 0x6F, 0x3D, 0xDF, 0x86, 0xDB, 0x44, 0x78, 0x81, 0xF3, 0xAF, 0xCA, 0x3E, 0xC4, 0x68, 0xB9, 0x2C, 0x34, 0x24, 0x38, 0x5F, 0x40, 0xA3, 0xC2, 0x72, 0xC3, 0x1D, 0x16, 0xC, 0x25, 0xE2, 0xBC, 0x8B, 0x49, 0x3C, 0x28, 0x41, 0x95, 0xD, 0xFF, 0x71, 0x1, 0xA8, 0x39, 0xDE, 0xB3, 0xC, 0x8, 0x9C, 0xE4, 0xB4, 0xD8, 0x90, 0xC1, 0x56, 0x64, 0x61, 0x84, 0xCB, 0x7B, 0x70, 0xB6, 0x32, 0xD5, 0x74, 0x5C, 0x6C, 0x48, 0x42, 0x57, 0xB8, 0xD0 };
static Il2CppFieldDefaultValueEntry t1703_f41_DefaultValue = 
{
	&t1703_f41_FieldInfo, { (char*)t1703_f41_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f42_DefaultValueData[] = { 0xA7, 0xF4, 0x51, 0x50, 0x65, 0x41, 0x7E, 0x53, 0xA4, 0x17, 0x1A, 0xC3, 0x5E, 0x27, 0x3A, 0x96, 0x6B, 0xAB, 0x3B, 0xCB, 0x45, 0x9D, 0x1F, 0xF1, 0x58, 0xFA, 0xAC, 0xAB, 0x3, 0xE3, 0x4B, 0x93, 0xFA, 0x30, 0x20, 0x55, 0x6D, 0x76, 0xAD, 0xF6, 0x76, 0xCC, 0x88, 0x91, 0x4C, 0x2, 0xF5, 0x25, 0xD7, 0xE5, 0x4F, 0xFC, 0xCB, 0x2A, 0xC5, 0xD7, 0x44, 0x35, 0x26, 0x80, 0xA3, 0x62, 0xB5, 0x8F, 0x5A, 0xB1, 0xDE, 0x49, 0x1B, 0xBA, 0x25, 0x67, 0xE, 0xEA, 0x45, 0x98, 0xC0, 0xFE, 0x5D, 0xE1, 0x75, 0x2F, 0xC3, 0x2, 0xF0, 0x4C, 0x81, 0x12, 0x97, 0x46, 0x8D, 0xA3, 0xF9, 0xD3, 0x6B, 0xC6, 0x5F, 0x8F, 0x3, 0xE7, 0x9C, 0x92, 0x15, 0x95, 0x7A, 0x6D, 0xBF, 0xEB, 0x59, 0x52, 0x95, 0xDA, 0x83, 0xBE, 0xD4, 0x2D, 0x21, 0x74, 0x58, 0xD3, 0x69, 0xE0, 0x49, 0x29, 0xC8, 0xC9, 0x8E, 0x44, 0x89, 0xC2, 0x75, 0x6A, 0x79, 0x8E, 0xF4, 0x78, 0x3E, 0x58, 0x99, 0x6B, 0x71, 0xB9, 0x27, 0xDD, 0x4F, 0xE1, 0xBE, 0xB6, 0xAD, 0x88, 0xF0, 0x17, 0xAC, 0x20, 0xC9, 0x66, 0x3A, 0xCE, 0x7D, 0xB4, 0x4A, 0xDF, 0x63, 0x18, 0x31, 0x1A, 0xE5, 0x82, 0x33, 0x51, 0x97, 0x60, 0x7F, 0x53, 0x62, 0x45, 0x77, 0x64, 0xB1, 0xE0, 0xAE, 0x6B, 0xBB, 0x84, 0xA0, 0x81, 0xFE, 0x1C, 0x2B, 0x8, 0xF9, 0x94, 0x68, 0x48, 0x70, 0x58, 0xFD, 0x45, 0x8F, 0x19, 0x6C, 0xDE, 0x94, 0x87, 0xF8, 0x7B, 0x52, 0xB7, 0xD3, 0x73, 0xAB, 0x23, 0x2, 0x4B, 0x72, 0xE2, 0x8F, 0x1F, 0xE3, 0x57, 0xAB, 0x55, 0x66, 0x2A, 0x28, 0xEB, 0xB2, 0x7, 0xC2, 0xB5, 0x2F, 0x3, 0x7B, 0xC5, 0x86, 0x9A, 0x8, 0x37, 0xD3, 0xA5, 0x87, 0x28, 0x30, 0xF2, 0xA5, 0xBF, 0x23, 0xB2, 0x6A, 0x3, 0x2, 0xBA, 0x82, 0x16, 0xED, 0x5C, 0x1C, 0xCF, 0x8A, 0x2B, 0xB4, 0x79, 0xA7, 0x92, 0xF2, 0x7, 0xF3, 0xF0, 0xE2, 0x69, 0x4E, 0xA1, 0xF4, 0xDA, 0x65, 0xCD, 0xBE, 0x5, 0x6, 0xD5, 0x62, 0x34, 0xD1, 0x1F, 0xFE, 0xA6, 0xC4, 0x8A, 0x53, 0x2E, 0x34, 0x9D, 0x55, 0xF3, 0xA2, 0xA0, 0xE1, 0x8A, 0x5, 0x32, 0xEB, 0xF6, 0xA4, 0x75, 0xEC, 0x83, 0xB, 0x39, 0xEF, 0x60, 0x40, 0xAA, 0x9F, 0x71, 0x5E, 0x6, 0x10, 0x6E, 0xBD, 0x51, 0x8A, 0x21, 0x3E, 0xF9, 0x6, 0xDD, 0x96, 0x3D, 0x5, 0x3E, 0xDD, 0xAE, 0xBD, 0xE6, 0x4D, 0x46, 0x8D, 0x54, 0x91, 0xB5, 0x5D, 0xC4, 0x71, 0x5, 0xD4, 0x6, 0x4, 0x6F, 0x15, 0x50, 0x60, 0xFF, 0xFB, 0x98, 0x19, 0x24, 0xE9, 0xBD, 0xD6, 0x97, 0x43, 0x40, 0x89, 0xCC, 0x9E, 0xD9, 0x67, 0x77, 0x42, 0xE8, 0xB0, 0xBD, 0x8B, 0x89, 0x7, 0x88, 0x5B, 0x19, 0xE7, 0x38, 0xEE, 0xC8, 0x79, 0xDB, 0xA, 0x7C, 0xA1, 0x47, 0xF, 0x42, 0x7C, 0xE9, 0x1E, 0x84, 0xF8, 0xC9, 0x0, 0x0, 0x0, 0x0, 0x86, 0x80, 0x9, 0x83, 0xED, 0x2B, 0x32, 0x48, 0x70, 0x11, 0x1E, 0xAC, 0x72, 0x5A, 0x6C, 0x4E, 0xFF, 0xE, 0xFD, 0xFB, 0x38, 0x85, 0xF, 0x56, 0xD5, 0xAE, 0x3D, 0x1E, 0x39, 0x2D, 0x36, 0x27, 0xD9, 0xF, 0xA, 0x64, 0xA6, 0x5C, 0x68, 0x21, 0x54, 0x5B, 0x9B, 0xD1, 0x2E, 0x36, 0x24, 0x3A, 0x67, 0xA, 0xC, 0xB1, 0xE7, 0x57, 0x93, 0xF, 0x96, 0xEE, 0xB4, 0xD2, 0x91, 0x9B, 0x1B, 0x9E, 0xC5, 0xC0, 0x80, 0x4F, 0x20, 0xDC, 0x61, 0xA2, 0x4B, 0x77, 0x5A, 0x69, 0x1A, 0x12, 0x1C, 0x16, 0xBA, 0x93, 0xE2, 0xA, 0x2A, 0xA0, 0xC0, 0xE5, 0xE0, 0x22, 0x3C, 0x43, 0x17, 0x1B, 0x12, 0x1D, 0xD, 0x9, 0xE, 0xB, 0xC7, 0x8B, 0xF2, 0xAD, 0xA8, 0xB6, 0x2D, 0xB9, 0xA9, 0x1E, 0x14, 0xC8, 0x19, 0xF1, 0x57, 0x85, 0x7, 0x75, 0xAF, 0x4C, 0xDD, 0x99, 0xEE, 0xBB, 0x60, 0x7F, 0xA3, 0xFD, 0x26, 0x1, 0xF7, 0x9F, 0xF5, 0x72, 0x5C, 0xBC, 0x3B, 0x66, 0x44, 0xC5, 0x7E, 0xFB, 0x5B, 0x34, 0x29, 0x43, 0x8B, 0x76, 0xC6, 0x23, 0xCB, 0xDC, 0xFC, 0xED, 0xB6, 0x68, 0xF1, 0xE4, 0xB8, 0x63, 0xDC, 0x31, 0xD7, 0xCA, 0x85, 0x63, 0x42, 0x10, 0x22, 0x97, 0x13, 0x40, 0x11, 0xC6, 0x84, 0x20, 0x24, 0x4A, 0x85, 0x7D, 0x3D, 0xBB, 0xD2, 0xF8, 0x32, 0xF9, 0xAE, 0x11, 0xA1, 0x29, 0xC7, 0x6D, 0x2F, 0x9E, 0x1D, 0x4B, 0x30, 0xB2, 0xDC, 0xF3, 0x52, 0x86, 0xD, 0xEC, 0xE3, 0xC1, 0x77, 0xD0, 0x16, 0xB3, 0x2B, 0x6C, 0xB9, 0x70, 0xA9, 0x99, 0x48, 0x94, 0x11, 0xFA, 0x64, 0xE9, 0x47, 0x22, 0x8C, 0xFC, 0xA8, 0xC4, 0x3F, 0xF0, 0xA0, 0x1A, 0x2C, 0x7D, 0x56, 0xD8, 0x90, 0x33, 0x22, 0xEF, 0x4E, 0x49, 0x87, 0xC7, 0xD1, 0x38, 0xD9, 0xC1, 0xA2, 0xCA, 0x8C, 0xFE, 0xB, 0xD4, 0x98, 0x36, 0x81, 0xF5, 0xA6, 0xCF, 0xDE, 0x7A, 0xA5, 0x28, 0x8E, 0xB7, 0xDA, 0x26, 0xBF, 0xAD, 0x3F, 0xA4, 0x9D, 0x3A, 0x2C, 0xE4, 0x92, 0x78, 0x50, 0xD, 0xCC, 0x5F, 0x6A, 0x9B, 0x46, 0x7E, 0x54, 0x62, 0x13, 0x8D, 0xF6, 0xC2, 0xB8, 0xD8, 0x90, 0xE8, 0xF7, 0x39, 0x2E, 0x5E, 0xAF, 0xC3, 0x82, 0xF5, 0x80, 0x5D, 0x9F, 0xBE, 0x93, 0xD0, 0x69, 0x7C, 0x2D, 0xD5, 0x6F, 0xA9, 0x12, 0x25, 0xCF, 0xB3, 0x99, 0xAC, 0xC8, 0x3B, 0x7D, 0x18, 0x10, 0xA7, 0x63, 0x9C, 0xE8, 0x6E, 0xBB, 0x3B, 0xDB, 0x7B, 0x78, 0x26, 0xCD, 0x9, 0x18, 0x59, 0x6E, 0xF4, 0xB7, 0x9A, 0xEC, 0x1, 0x9A, 0x4F, 0x83, 0xA8, 0x6E, 0x95, 0xE6, 0x65, 0xE6, 0xFF, 0xAA, 0x7E, 0xCF, 0xBC, 0x21, 0x8, 0xE8, 0x15, 0xEF, 0xE6, 0x9B, 0xE7, 0xBA, 0xD9, 0x36, 0x6F, 0x4A, 0xCE, 0x9, 0x9F, 0xEA, 0xD4, 0x7C, 0xB0, 0x29, 0xD6, 0xB2, 0xA4, 0x31, 0xAF, 0x23, 0x3F, 0x2A, 0x31, 0x94, 0xA5, 0xC6, 0x30, 0x66, 0xA2, 0x35, 0xC0, 0xBC, 0x4E, 0x74, 0x37, 0xCA, 0x82, 0xFC, 0xA6, 0xD0, 0x90, 0xE0, 0xB0, 0xD8, 0xA7, 0x33, 0x15, 0x98, 0x4, 0xF1, 0x4A, 0xDA, 0xEC, 0x41, 0xF7, 0x50, 0xCD, 0x7F, 0xE, 0xF6, 0x91, 0x17, 0x2F, 0xD6, 0x4D, 0x76, 0x8D, 0xB0, 0xEF, 0x43, 0x4D, 0x4D, 0xAA, 0xCC, 0x54, 0x4, 0x96, 0xE4, 0xDF, 0xB5, 0xD1, 0x9E, 0xE3, 0x88, 0x6A, 0x4C, 0x1B, 0x1F, 0x2C, 0xC1, 0xB8, 0x51, 0x65, 0x46, 0x7F, 0xEA, 0x5E, 0x9D, 0x4, 0x35, 0x8C, 0x1, 0x5D, 0x74, 0x87, 0xFA, 0x73, 0x41, 0xB, 0xFB, 0x2E, 0x1D, 0x67, 0xB3, 0x5A, 0xD2, 0xDB, 0x92, 0x52, 0x56, 0x10, 0xE9, 0x33, 0x47, 0xD6, 0x6D, 0x13, 0x61, 0xD7, 0x9A, 0x8C, 0xC, 0xA1, 0x37, 0x7A, 0x14, 0xF8, 0x59, 0x8E, 0x3C, 0x13, 0xEB, 0x89, 0x27, 0xA9, 0xCE, 0xEE, 0xC9, 0x61, 0xB7, 0x35, 0xE5, 0x1C, 0xE1, 0xED, 0xB1, 0x47, 0x7A, 0x3C, 0xDF, 0xD2, 0x9C, 0x59, 0x73, 0xF2, 0x55, 0x3F, 0xCE, 0x14, 0x18, 0x79, 0x37, 0xC7, 0x73, 0xBF, 0xCD, 0xF7, 0x53, 0xEA, 0xAA, 0xFD, 0x5F, 0x5B, 0x6F, 0x3D, 0xDF, 0x14, 0xDB, 0x44, 0x78, 0x86, 0xF3, 0xAF, 0xCA, 0x81, 0xC4, 0x68, 0xB9, 0x3E, 0x34, 0x24, 0x38, 0x2C, 0x40, 0xA3, 0xC2, 0x5F, 0xC3, 0x1D, 0x16, 0x72, 0x25, 0xE2, 0xBC, 0xC, 0x49, 0x3C, 0x28, 0x8B, 0x95, 0xD, 0xFF, 0x41, 0x1, 0xA8, 0x39, 0x71, 0xB3, 0xC, 0x8, 0xDE, 0xE4, 0xB4, 0xD8, 0x9C, 0xC1, 0x56, 0x64, 0x90, 0x84, 0xCB, 0x7B, 0x61, 0xB6, 0x32, 0xD5, 0x70, 0x5C, 0x6C, 0x48, 0x74, 0x57, 0xB8, 0xD0, 0x42 };
static Il2CppFieldDefaultValueEntry t1703_f42_DefaultValue = 
{
	&t1703_f42_FieldInfo, { (char*)t1703_f42_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f43_DefaultValueData[] = { 0xF4, 0x51, 0x50, 0xA7, 0x41, 0x7E, 0x53, 0x65, 0x17, 0x1A, 0xC3, 0xA4, 0x27, 0x3A, 0x96, 0x5E, 0xAB, 0x3B, 0xCB, 0x6B, 0x9D, 0x1F, 0xF1, 0x45, 0xFA, 0xAC, 0xAB, 0x58, 0xE3, 0x4B, 0x93, 0x3, 0x30, 0x20, 0x55, 0xFA, 0x76, 0xAD, 0xF6, 0x6D, 0xCC, 0x88, 0x91, 0x76, 0x2, 0xF5, 0x25, 0x4C, 0xE5, 0x4F, 0xFC, 0xD7, 0x2A, 0xC5, 0xD7, 0xCB, 0x35, 0x26, 0x80, 0x44, 0x62, 0xB5, 0x8F, 0xA3, 0xB1, 0xDE, 0x49, 0x5A, 0xBA, 0x25, 0x67, 0x1B, 0xEA, 0x45, 0x98, 0xE, 0xFE, 0x5D, 0xE1, 0xC0, 0x2F, 0xC3, 0x2, 0x75, 0x4C, 0x81, 0x12, 0xF0, 0x46, 0x8D, 0xA3, 0x97, 0xD3, 0x6B, 0xC6, 0xF9, 0x8F, 0x3, 0xE7, 0x5F, 0x92, 0x15, 0x95, 0x9C, 0x6D, 0xBF, 0xEB, 0x7A, 0x52, 0x95, 0xDA, 0x59, 0xBE, 0xD4, 0x2D, 0x83, 0x74, 0x58, 0xD3, 0x21, 0xE0, 0x49, 0x29, 0x69, 0xC9, 0x8E, 0x44, 0xC8, 0xC2, 0x75, 0x6A, 0x89, 0x8E, 0xF4, 0x78, 0x79, 0x58, 0x99, 0x6B, 0x3E, 0xB9, 0x27, 0xDD, 0x71, 0xE1, 0xBE, 0xB6, 0x4F, 0x88, 0xF0, 0x17, 0xAD, 0x20, 0xC9, 0x66, 0xAC, 0xCE, 0x7D, 0xB4, 0x3A, 0xDF, 0x63, 0x18, 0x4A, 0x1A, 0xE5, 0x82, 0x31, 0x51, 0x97, 0x60, 0x33, 0x53, 0x62, 0x45, 0x7F, 0x64, 0xB1, 0xE0, 0x77, 0x6B, 0xBB, 0x84, 0xAE, 0x81, 0xFE, 0x1C, 0xA0, 0x8, 0xF9, 0x94, 0x2B, 0x48, 0x70, 0x58, 0x68, 0x45, 0x8F, 0x19, 0xFD, 0xDE, 0x94, 0x87, 0x6C, 0x7B, 0x52, 0xB7, 0xF8, 0x73, 0xAB, 0x23, 0xD3, 0x4B, 0x72, 0xE2, 0x2, 0x1F, 0xE3, 0x57, 0x8F, 0x55, 0x66, 0x2A, 0xAB, 0xEB, 0xB2, 0x7, 0x28, 0xB5, 0x2F, 0x3, 0xC2, 0xC5, 0x86, 0x9A, 0x7B, 0x37, 0xD3, 0xA5, 0x8, 0x28, 0x30, 0xF2, 0x87, 0xBF, 0x23, 0xB2, 0xA5, 0x3, 0x2, 0xBA, 0x6A, 0x16, 0xED, 0x5C, 0x82, 0xCF, 0x8A, 0x2B, 0x1C, 0x79, 0xA7, 0x92, 0xB4, 0x7, 0xF3, 0xF0, 0xF2, 0x69, 0x4E, 0xA1, 0xE2, 0xDA, 0x65, 0xCD, 0xF4, 0x5, 0x6, 0xD5, 0xBE, 0x34, 0xD1, 0x1F, 0x62, 0xA6, 0xC4, 0x8A, 0xFE, 0x2E, 0x34, 0x9D, 0x53, 0xF3, 0xA2, 0xA0, 0x55, 0x8A, 0x5, 0x32, 0xE1, 0xF6, 0xA4, 0x75, 0xEB, 0x83, 0xB, 0x39, 0xEC, 0x60, 0x40, 0xAA, 0xEF, 0x71, 0x5E, 0x6, 0x9F, 0x6E, 0xBD, 0x51, 0x10, 0x21, 0x3E, 0xF9, 0x8A, 0xDD, 0x96, 0x3D, 0x6, 0x3E, 0xDD, 0xAE, 0x5, 0xE6, 0x4D, 0x46, 0xBD, 0x54, 0x91, 0xB5, 0x8D, 0xC4, 0x71, 0x5, 0x5D, 0x6, 0x4, 0x6F, 0xD4, 0x50, 0x60, 0xFF, 0x15, 0x98, 0x19, 0x24, 0xFB, 0xBD, 0xD6, 0x97, 0xE9, 0x40, 0x89, 0xCC, 0x43, 0xD9, 0x67, 0x77, 0x9E, 0xE8, 0xB0, 0xBD, 0x42, 0x89, 0x7, 0x88, 0x8B, 0x19, 0xE7, 0x38, 0x5B, 0xC8, 0x79, 0xDB, 0xEE, 0x7C, 0xA1, 0x47, 0xA, 0x42, 0x7C, 0xE9, 0xF, 0x84, 0xF8, 0xC9, 0x1E, 0x0, 0x0, 0x0, 0x0, 0x80, 0x9, 0x83, 0x86, 0x2B, 0x32, 0x48, 0xED, 0x11, 0x1E, 0xAC, 0x70, 0x5A, 0x6C, 0x4E, 0x72, 0xE, 0xFD, 0xFB, 0xFF, 0x85, 0xF, 0x56, 0x38, 0xAE, 0x3D, 0x1E, 0xD5, 0x2D, 0x36, 0x27, 0x39, 0xF, 0xA, 0x64, 0xD9, 0x5C, 0x68, 0x21, 0xA6, 0x5B, 0x9B, 0xD1, 0x54, 0x36, 0x24, 0x3A, 0x2E, 0xA, 0xC, 0xB1, 0x67, 0x57, 0x93, 0xF, 0xE7, 0xEE, 0xB4, 0xD2, 0x96, 0x9B, 0x1B, 0x9E, 0x91, 0xC0, 0x80, 0x4F, 0xC5, 0xDC, 0x61, 0xA2, 0x20, 0x77, 0x5A, 0x69, 0x4B, 0x12, 0x1C, 0x16, 0x1A, 0x93, 0xE2, 0xA, 0xBA, 0xA0, 0xC0, 0xE5, 0x2A, 0x22, 0x3C, 0x43, 0xE0, 0x1B, 0x12, 0x1D, 0x17, 0x9, 0xE, 0xB, 0xD, 0x8B, 0xF2, 0xAD, 0xC7, 0xB6, 0x2D, 0xB9, 0xA8, 0x1E, 0x14, 0xC8, 0xA9, 0xF1, 0x57, 0x85, 0x19, 0x75, 0xAF, 0x4C, 0x7, 0x99, 0xEE, 0xBB, 0xDD, 0x7F, 0xA3, 0xFD, 0x60, 0x1, 0xF7, 0x9F, 0x26, 0x72, 0x5C, 0xBC, 0xF5, 0x66, 0x44, 0xC5, 0x3B, 0xFB, 0x5B, 0x34, 0x7E, 0x43, 0x8B, 0x76, 0x29, 0x23, 0xCB, 0xDC, 0xC6, 0xED, 0xB6, 0x68, 0xFC, 0xE4, 0xB8, 0x63, 0xF1, 0x31, 0xD7, 0xCA, 0xDC, 0x63, 0x42, 0x10, 0x85, 0x97, 0x13, 0x40, 0x22, 0xC6, 0x84, 0x20, 0x11, 0x4A, 0x85, 0x7D, 0x24, 0xBB, 0xD2, 0xF8, 0x3D, 0xF9, 0xAE, 0x11, 0x32, 0x29, 0xC7, 0x6D, 0xA1, 0x9E, 0x1D, 0x4B, 0x2F, 0xB2, 0xDC, 0xF3, 0x30, 0x86, 0xD, 0xEC, 0x52, 0xC1, 0x77, 0xD0, 0xE3, 0xB3, 0x2B, 0x6C, 0x16, 0x70, 0xA9, 0x99, 0xB9, 0x94, 0x11, 0xFA, 0x48, 0xE9, 0x47, 0x22, 0x64, 0xFC, 0xA8, 0xC4, 0x8C, 0xF0, 0xA0, 0x1A, 0x3F, 0x7D, 0x56, 0xD8, 0x2C, 0x33, 0x22, 0xEF, 0x90, 0x49, 0x87, 0xC7, 0x4E, 0x38, 0xD9, 0xC1, 0xD1, 0xCA, 0x8C, 0xFE, 0xA2, 0xD4, 0x98, 0x36, 0xB, 0xF5, 0xA6, 0xCF, 0x81, 0x7A, 0xA5, 0x28, 0xDE, 0xB7, 0xDA, 0x26, 0x8E, 0xAD, 0x3F, 0xA4, 0xBF, 0x3A, 0x2C, 0xE4, 0x9D, 0x78, 0x50, 0xD, 0x92, 0x5F, 0x6A, 0x9B, 0xCC, 0x7E, 0x54, 0x62, 0x46, 0x8D, 0xF6, 0xC2, 0x13, 0xD8, 0x90, 0xE8, 0xB8, 0x39, 0x2E, 0x5E, 0xF7, 0xC3, 0x82, 0xF5, 0xAF, 0x5D, 0x9F, 0xBE, 0x80, 0xD0, 0x69, 0x7C, 0x93, 0xD5, 0x6F, 0xA9, 0x2D, 0x25, 0xCF, 0xB3, 0x12, 0xAC, 0xC8, 0x3B, 0x99, 0x18, 0x10, 0xA7, 0x7D, 0x9C, 0xE8, 0x6E, 0x63, 0x3B, 0xDB, 0x7B, 0xBB, 0x26, 0xCD, 0x9, 0x78, 0x59, 0x6E, 0xF4, 0x18, 0x9A, 0xEC, 0x1, 0xB7, 0x4F, 0x83, 0xA8, 0x9A, 0x95, 0xE6, 0x65, 0x6E, 0xFF, 0xAA, 0x7E, 0xE6, 0xBC, 0x21, 0x8, 0xCF, 0x15, 0xEF, 0xE6, 0xE8, 0xE7, 0xBA, 0xD9, 0x9B, 0x6F, 0x4A, 0xCE, 0x36, 0x9F, 0xEA, 0xD4, 0x9, 0xB0, 0x29, 0xD6, 0x7C, 0xA4, 0x31, 0xAF, 0xB2, 0x3F, 0x2A, 0x31, 0x23, 0xA5, 0xC6, 0x30, 0x94, 0xA2, 0x35, 0xC0, 0x66, 0x4E, 0x74, 0x37, 0xBC, 0x82, 0xFC, 0xA6, 0xCA, 0x90, 0xE0, 0xB0, 0xD0, 0xA7, 0x33, 0x15, 0xD8, 0x4, 0xF1, 0x4A, 0x98, 0xEC, 0x41, 0xF7, 0xDA, 0xCD, 0x7F, 0xE, 0x50, 0x91, 0x17, 0x2F, 0xF6, 0x4D, 0x76, 0x8D, 0xD6, 0xEF, 0x43, 0x4D, 0xB0, 0xAA, 0xCC, 0x54, 0x4D, 0x96, 0xE4, 0xDF, 0x4, 0xD1, 0x9E, 0xE3, 0xB5, 0x6A, 0x4C, 0x1B, 0x88, 0x2C, 0xC1, 0xB8, 0x1F, 0x65, 0x46, 0x7F, 0x51, 0x5E, 0x9D, 0x4, 0xEA, 0x8C, 0x1, 0x5D, 0x35, 0x87, 0xFA, 0x73, 0x74, 0xB, 0xFB, 0x2E, 0x41, 0x67, 0xB3, 0x5A, 0x1D, 0xDB, 0x92, 0x52, 0xD2, 0x10, 0xE9, 0x33, 0x56, 0xD6, 0x6D, 0x13, 0x47, 0xD7, 0x9A, 0x8C, 0x61, 0xA1, 0x37, 0x7A, 0xC, 0xF8, 0x59, 0x8E, 0x14, 0x13, 0xEB, 0x89, 0x3C, 0xA9, 0xCE, 0xEE, 0x27, 0x61, 0xB7, 0x35, 0xC9, 0x1C, 0xE1, 0xED, 0xE5, 0x47, 0x7A, 0x3C, 0xB1, 0xD2, 0x9C, 0x59, 0xDF, 0xF2, 0x55, 0x3F, 0x73, 0x14, 0x18, 0x79, 0xCE, 0xC7, 0x73, 0xBF, 0x37, 0xF7, 0x53, 0xEA, 0xCD, 0xFD, 0x5F, 0x5B, 0xAA, 0x3D, 0xDF, 0x14, 0x6F, 0x44, 0x78, 0x86, 0xDB, 0xAF, 0xCA, 0x81, 0xF3, 0x68, 0xB9, 0x3E, 0xC4, 0x24, 0x38, 0x2C, 0x34, 0xA3, 0xC2, 0x5F, 0x40, 0x1D, 0x16, 0x72, 0xC3, 0xE2, 0xBC, 0xC, 0x25, 0x3C, 0x28, 0x8B, 0x49, 0xD, 0xFF, 0x41, 0x95, 0xA8, 0x39, 0x71, 0x1, 0xC, 0x8, 0xDE, 0xB3, 0xB4, 0xD8, 0x9C, 0xE4, 0x56, 0x64, 0x90, 0xC1, 0xCB, 0x7B, 0x61, 0x84, 0x32, 0xD5, 0x70, 0xB6, 0x6C, 0x48, 0x74, 0x5C, 0xB8, 0xD0, 0x42, 0x57 };
static Il2CppFieldDefaultValueEntry t1703_f43_DefaultValue = 
{
	&t1703_f43_FieldInfo, { (char*)t1703_f43_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f44_DefaultValueData[] = { 0x51, 0x50, 0xA7, 0xF4, 0x7E, 0x53, 0x65, 0x41, 0x1A, 0xC3, 0xA4, 0x17, 0x3A, 0x96, 0x5E, 0x27, 0x3B, 0xCB, 0x6B, 0xAB, 0x1F, 0xF1, 0x45, 0x9D, 0xAC, 0xAB, 0x58, 0xFA, 0x4B, 0x93, 0x3, 0xE3, 0x20, 0x55, 0xFA, 0x30, 0xAD, 0xF6, 0x6D, 0x76, 0x88, 0x91, 0x76, 0xCC, 0xF5, 0x25, 0x4C, 0x2, 0x4F, 0xFC, 0xD7, 0xE5, 0xC5, 0xD7, 0xCB, 0x2A, 0x26, 0x80, 0x44, 0x35, 0xB5, 0x8F, 0xA3, 0x62, 0xDE, 0x49, 0x5A, 0xB1, 0x25, 0x67, 0x1B, 0xBA, 0x45, 0x98, 0xE, 0xEA, 0x5D, 0xE1, 0xC0, 0xFE, 0xC3, 0x2, 0x75, 0x2F, 0x81, 0x12, 0xF0, 0x4C, 0x8D, 0xA3, 0x97, 0x46, 0x6B, 0xC6, 0xF9, 0xD3, 0x3, 0xE7, 0x5F, 0x8F, 0x15, 0x95, 0x9C, 0x92, 0xBF, 0xEB, 0x7A, 0x6D, 0x95, 0xDA, 0x59, 0x52, 0xD4, 0x2D, 0x83, 0xBE, 0x58, 0xD3, 0x21, 0x74, 0x49, 0x29, 0x69, 0xE0, 0x8E, 0x44, 0xC8, 0xC9, 0x75, 0x6A, 0x89, 0xC2, 0xF4, 0x78, 0x79, 0x8E, 0x99, 0x6B, 0x3E, 0x58, 0x27, 0xDD, 0x71, 0xB9, 0xBE, 0xB6, 0x4F, 0xE1, 0xF0, 0x17, 0xAD, 0x88, 0xC9, 0x66, 0xAC, 0x20, 0x7D, 0xB4, 0x3A, 0xCE, 0x63, 0x18, 0x4A, 0xDF, 0xE5, 0x82, 0x31, 0x1A, 0x97, 0x60, 0x33, 0x51, 0x62, 0x45, 0x7F, 0x53, 0xB1, 0xE0, 0x77, 0x64, 0xBB, 0x84, 0xAE, 0x6B, 0xFE, 0x1C, 0xA0, 0x81, 0xF9, 0x94, 0x2B, 0x8, 0x70, 0x58, 0x68, 0x48, 0x8F, 0x19, 0xFD, 0x45, 0x94, 0x87, 0x6C, 0xDE, 0x52, 0xB7, 0xF8, 0x7B, 0xAB, 0x23, 0xD3, 0x73, 0x72, 0xE2, 0x2, 0x4B, 0xE3, 0x57, 0x8F, 0x1F, 0x66, 0x2A, 0xAB, 0x55, 0xB2, 0x7, 0x28, 0xEB, 0x2F, 0x3, 0xC2, 0xB5, 0x86, 0x9A, 0x7B, 0xC5, 0xD3, 0xA5, 0x8, 0x37, 0x30, 0xF2, 0x87, 0x28, 0x23, 0xB2, 0xA5, 0xBF, 0x2, 0xBA, 0x6A, 0x3, 0xED, 0x5C, 0x82, 0x16, 0x8A, 0x2B, 0x1C, 0xCF, 0xA7, 0x92, 0xB4, 0x79, 0xF3, 0xF0, 0xF2, 0x7, 0x4E, 0xA1, 0xE2, 0x69, 0x65, 0xCD, 0xF4, 0xDA, 0x6, 0xD5, 0xBE, 0x5, 0xD1, 0x1F, 0x62, 0x34, 0xC4, 0x8A, 0xFE, 0xA6, 0x34, 0x9D, 0x53, 0x2E, 0xA2, 0xA0, 0x55, 0xF3, 0x5, 0x32, 0xE1, 0x8A, 0xA4, 0x75, 0xEB, 0xF6, 0xB, 0x39, 0xEC, 0x83, 0x40, 0xAA, 0xEF, 0x60, 0x5E, 0x6, 0x9F, 0x71, 0xBD, 0x51, 0x10, 0x6E, 0x3E, 0xF9, 0x8A, 0x21, 0x96, 0x3D, 0x6, 0xDD, 0xDD, 0xAE, 0x5, 0x3E, 0x4D, 0x46, 0xBD, 0xE6, 0x91, 0xB5, 0x8D, 0x54, 0x71, 0x5, 0x5D, 0xC4, 0x4, 0x6F, 0xD4, 0x6, 0x60, 0xFF, 0x15, 0x50, 0x19, 0x24, 0xFB, 0x98, 0xD6, 0x97, 0xE9, 0xBD, 0x89, 0xCC, 0x43, 0x40, 0x67, 0x77, 0x9E, 0xD9, 0xB0, 0xBD, 0x42, 0xE8, 0x7, 0x88, 0x8B, 0x89, 0xE7, 0x38, 0x5B, 0x19, 0x79, 0xDB, 0xEE, 0xC8, 0xA1, 0x47, 0xA, 0x7C, 0x7C, 0xE9, 0xF, 0x42, 0xF8, 0xC9, 0x1E, 0x84, 0x0, 0x0, 0x0, 0x0, 0x9, 0x83, 0x86, 0x80, 0x32, 0x48, 0xED, 0x2B, 0x1E, 0xAC, 0x70, 0x11, 0x6C, 0x4E, 0x72, 0x5A, 0xFD, 0xFB, 0xFF, 0xE, 0xF, 0x56, 0x38, 0x85, 0x3D, 0x1E, 0xD5, 0xAE, 0x36, 0x27, 0x39, 0x2D, 0xA, 0x64, 0xD9, 0xF, 0x68, 0x21, 0xA6, 0x5C, 0x9B, 0xD1, 0x54, 0x5B, 0x24, 0x3A, 0x2E, 0x36, 0xC, 0xB1, 0x67, 0xA, 0x93, 0xF, 0xE7, 0x57, 0xB4, 0xD2, 0x96, 0xEE, 0x1B, 0x9E, 0x91, 0x9B, 0x80, 0x4F, 0xC5, 0xC0, 0x61, 0xA2, 0x20, 0xDC, 0x5A, 0x69, 0x4B, 0x77, 0x1C, 0x16, 0x1A, 0x12, 0xE2, 0xA, 0xBA, 0x93, 0xC0, 0xE5, 0x2A, 0xA0, 0x3C, 0x43, 0xE0, 0x22, 0x12, 0x1D, 0x17, 0x1B, 0xE, 0xB, 0xD, 0x9, 0xF2, 0xAD, 0xC7, 0x8B, 0x2D, 0xB9, 0xA8, 0xB6, 0x14, 0xC8, 0xA9, 0x1E, 0x57, 0x85, 0x19, 0xF1, 0xAF, 0x4C, 0x7, 0x75, 0xEE, 0xBB, 0xDD, 0x99, 0xA3, 0xFD, 0x60, 0x7F, 0xF7, 0x9F, 0x26, 0x1, 0x5C, 0xBC, 0xF5, 0x72, 0x44, 0xC5, 0x3B, 0x66, 0x5B, 0x34, 0x7E, 0xFB, 0x8B, 0x76, 0x29, 0x43, 0xCB, 0xDC, 0xC6, 0x23, 0xB6, 0x68, 0xFC, 0xED, 0xB8, 0x63, 0xF1, 0xE4, 0xD7, 0xCA, 0xDC, 0x31, 0x42, 0x10, 0x85, 0x63, 0x13, 0x40, 0x22, 0x97, 0x84, 0x20, 0x11, 0xC6, 0x85, 0x7D, 0x24, 0x4A, 0xD2, 0xF8, 0x3D, 0xBB, 0xAE, 0x11, 0x32, 0xF9, 0xC7, 0x6D, 0xA1, 0x29, 0x1D, 0x4B, 0x2F, 0x9E, 0xDC, 0xF3, 0x30, 0xB2, 0xD, 0xEC, 0x52, 0x86, 0x77, 0xD0, 0xE3, 0xC1, 0x2B, 0x6C, 0x16, 0xB3, 0xA9, 0x99, 0xB9, 0x70, 0x11, 0xFA, 0x48, 0x94, 0x47, 0x22, 0x64, 0xE9, 0xA8, 0xC4, 0x8C, 0xFC, 0xA0, 0x1A, 0x3F, 0xF0, 0x56, 0xD8, 0x2C, 0x7D, 0x22, 0xEF, 0x90, 0x33, 0x87, 0xC7, 0x4E, 0x49, 0xD9, 0xC1, 0xD1, 0x38, 0x8C, 0xFE, 0xA2, 0xCA, 0x98, 0x36, 0xB, 0xD4, 0xA6, 0xCF, 0x81, 0xF5, 0xA5, 0x28, 0xDE, 0x7A, 0xDA, 0x26, 0x8E, 0xB7, 0x3F, 0xA4, 0xBF, 0xAD, 0x2C, 0xE4, 0x9D, 0x3A, 0x50, 0xD, 0x92, 0x78, 0x6A, 0x9B, 0xCC, 0x5F, 0x54, 0x62, 0x46, 0x7E, 0xF6, 0xC2, 0x13, 0x8D, 0x90, 0xE8, 0xB8, 0xD8, 0x2E, 0x5E, 0xF7, 0x39, 0x82, 0xF5, 0xAF, 0xC3, 0x9F, 0xBE, 0x80, 0x5D, 0x69, 0x7C, 0x93, 0xD0, 0x6F, 0xA9, 0x2D, 0xD5, 0xCF, 0xB3, 0x12, 0x25, 0xC8, 0x3B, 0x99, 0xAC, 0x10, 0xA7, 0x7D, 0x18, 0xE8, 0x6E, 0x63, 0x9C, 0xDB, 0x7B, 0xBB, 0x3B, 0xCD, 0x9, 0x78, 0x26, 0x6E, 0xF4, 0x18, 0x59, 0xEC, 0x1, 0xB7, 0x9A, 0x83, 0xA8, 0x9A, 0x4F, 0xE6, 0x65, 0x6E, 0x95, 0xAA, 0x7E, 0xE6, 0xFF, 0x21, 0x8, 0xCF, 0xBC, 0xEF, 0xE6, 0xE8, 0x15, 0xBA, 0xD9, 0x9B, 0xE7, 0x4A, 0xCE, 0x36, 0x6F, 0xEA, 0xD4, 0x9, 0x9F, 0x29, 0xD6, 0x7C, 0xB0, 0x31, 0xAF, 0xB2, 0xA4, 0x2A, 0x31, 0x23, 0x3F, 0xC6, 0x30, 0x94, 0xA5, 0x35, 0xC0, 0x66, 0xA2, 0x74, 0x37, 0xBC, 0x4E, 0xFC, 0xA6, 0xCA, 0x82, 0xE0, 0xB0, 0xD0, 0x90, 0x33, 0x15, 0xD8, 0xA7, 0xF1, 0x4A, 0x98, 0x4, 0x41, 0xF7, 0xDA, 0xEC, 0x7F, 0xE, 0x50, 0xCD, 0x17, 0x2F, 0xF6, 0x91, 0x76, 0x8D, 0xD6, 0x4D, 0x43, 0x4D, 0xB0, 0xEF, 0xCC, 0x54, 0x4D, 0xAA, 0xE4, 0xDF, 0x4, 0x96, 0x9E, 0xE3, 0xB5, 0xD1, 0x4C, 0x1B, 0x88, 0x6A, 0xC1, 0xB8, 0x1F, 0x2C, 0x46, 0x7F, 0x51, 0x65, 0x9D, 0x4, 0xEA, 0x5E, 0x1, 0x5D, 0x35, 0x8C, 0xFA, 0x73, 0x74, 0x87, 0xFB, 0x2E, 0x41, 0xB, 0xB3, 0x5A, 0x1D, 0x67, 0x92, 0x52, 0xD2, 0xDB, 0xE9, 0x33, 0x56, 0x10, 0x6D, 0x13, 0x47, 0xD6, 0x9A, 0x8C, 0x61, 0xD7, 0x37, 0x7A, 0xC, 0xA1, 0x59, 0x8E, 0x14, 0xF8, 0xEB, 0x89, 0x3C, 0x13, 0xCE, 0xEE, 0x27, 0xA9, 0xB7, 0x35, 0xC9, 0x61, 0xE1, 0xED, 0xE5, 0x1C, 0x7A, 0x3C, 0xB1, 0x47, 0x9C, 0x59, 0xDF, 0xD2, 0x55, 0x3F, 0x73, 0xF2, 0x18, 0x79, 0xCE, 0x14, 0x73, 0xBF, 0x37, 0xC7, 0x53, 0xEA, 0xCD, 0xF7, 0x5F, 0x5B, 0xAA, 0xFD, 0xDF, 0x14, 0x6F, 0x3D, 0x78, 0x86, 0xDB, 0x44, 0xCA, 0x81, 0xF3, 0xAF, 0xB9, 0x3E, 0xC4, 0x68, 0x38, 0x2C, 0x34, 0x24, 0xC2, 0x5F, 0x40, 0xA3, 0x16, 0x72, 0xC3, 0x1D, 0xBC, 0xC, 0x25, 0xE2, 0x28, 0x8B, 0x49, 0x3C, 0xFF, 0x41, 0x95, 0xD, 0x39, 0x71, 0x1, 0xA8, 0x8, 0xDE, 0xB3, 0xC, 0xD8, 0x9C, 0xE4, 0xB4, 0x64, 0x90, 0xC1, 0x56, 0x7B, 0x61, 0x84, 0xCB, 0xD5, 0x70, 0xB6, 0x32, 0x48, 0x74, 0x5C, 0x6C, 0xD0, 0x42, 0x57, 0xB8 };
static Il2CppFieldDefaultValueEntry t1703_f44_DefaultValue = 
{
	&t1703_f44_FieldInfo, { (char*)t1703_f44_DefaultValueData, &t1699_0_0_0 }};
static const uint8_t t1703_f45_DefaultValueData[] = { 0x98, 0x2F, 0x8A, 0x42, 0x91, 0x44, 0x37, 0x71, 0xCF, 0xFB, 0xC0, 0xB5, 0xA5, 0xDB, 0xB5, 0xE9, 0x5B, 0xC2, 0x56, 0x39, 0xF1, 0x11, 0xF1, 0x59, 0xA4, 0x82, 0x3F, 0x92, 0xD5, 0x5E, 0x1C, 0xAB, 0x98, 0xAA, 0x7, 0xD8, 0x1, 0x5B, 0x83, 0x12, 0xBE, 0x85, 0x31, 0x24, 0xC3, 0x7D, 0xC, 0x55, 0x74, 0x5D, 0xBE, 0x72, 0xFE, 0xB1, 0xDE, 0x80, 0xA7, 0x6, 0xDC, 0x9B, 0x74, 0xF1, 0x9B, 0xC1, 0xC1, 0x69, 0x9B, 0xE4, 0x86, 0x47, 0xBE, 0xEF, 0xC6, 0x9D, 0xC1, 0xF, 0xCC, 0xA1, 0xC, 0x24, 0x6F, 0x2C, 0xE9, 0x2D, 0xAA, 0x84, 0x74, 0x4A, 0xDC, 0xA9, 0xB0, 0x5C, 0xDA, 0x88, 0xF9, 0x76, 0x52, 0x51, 0x3E, 0x98, 0x6D, 0xC6, 0x31, 0xA8, 0xC8, 0x27, 0x3, 0xB0, 0xC7, 0x7F, 0x59, 0xBF, 0xF3, 0xB, 0xE0, 0xC6, 0x47, 0x91, 0xA7, 0xD5, 0x51, 0x63, 0xCA, 0x6, 0x67, 0x29, 0x29, 0x14, 0x85, 0xA, 0xB7, 0x27, 0x38, 0x21, 0x1B, 0x2E, 0xFC, 0x6D, 0x2C, 0x4D, 0x13, 0xD, 0x38, 0x53, 0x54, 0x73, 0xA, 0x65, 0xBB, 0xA, 0x6A, 0x76, 0x2E, 0xC9, 0xC2, 0x81, 0x85, 0x2C, 0x72, 0x92, 0xA1, 0xE8, 0xBF, 0xA2, 0x4B, 0x66, 0x1A, 0xA8, 0x70, 0x8B, 0x4B, 0xC2, 0xA3, 0x51, 0x6C, 0xC7, 0x19, 0xE8, 0x92, 0xD1, 0x24, 0x6, 0x99, 0xD6, 0x85, 0x35, 0xE, 0xF4, 0x70, 0xA0, 0x6A, 0x10, 0x16, 0xC1, 0xA4, 0x19, 0x8, 0x6C, 0x37, 0x1E, 0x4C, 0x77, 0x48, 0x27, 0xB5, 0xBC, 0xB0, 0x34, 0xB3, 0xC, 0x1C, 0x39, 0x4A, 0xAA, 0xD8, 0x4E, 0x4F, 0xCA, 0x9C, 0x5B, 0xF3, 0x6F, 0x2E, 0x68, 0xEE, 0x82, 0x8F, 0x74, 0x6F, 0x63, 0xA5, 0x78, 0x14, 0x78, 0xC8, 0x84, 0x8, 0x2, 0xC7, 0x8C, 0xFA, 0xFF, 0xBE, 0x90, 0xEB, 0x6C, 0x50, 0xA4, 0xF7, 0xA3, 0xF9, 0xBE, 0xF2, 0x78, 0x71, 0xC6 };
static Il2CppFieldDefaultValueEntry t1703_f45_DefaultValue = 
{
	&t1703_f45_FieldInfo, { (char*)t1703_f45_DefaultValueData, &t1698_0_0_0 }};
static const uint8_t t1703_f46_DefaultValueData[] = { 0x22, 0xAE, 0x28, 0xD7, 0x98, 0x2F, 0x8A, 0x42, 0xCD, 0x65, 0xEF, 0x23, 0x91, 0x44, 0x37, 0x71, 0x2F, 0x3B, 0x4D, 0xEC, 0xCF, 0xFB, 0xC0, 0xB5, 0xBC, 0xDB, 0x89, 0x81, 0xA5, 0xDB, 0xB5, 0xE9, 0x38, 0xB5, 0x48, 0xF3, 0x5B, 0xC2, 0x56, 0x39, 0x19, 0xD0, 0x5, 0xB6, 0xF1, 0x11, 0xF1, 0x59, 0x9B, 0x4F, 0x19, 0xAF, 0xA4, 0x82, 0x3F, 0x92, 0x18, 0x81, 0x6D, 0xDA, 0xD5, 0x5E, 0x1C, 0xAB, 0x42, 0x2, 0x3, 0xA3, 0x98, 0xAA, 0x7, 0xD8, 0xBE, 0x6F, 0x70, 0x45, 0x1, 0x5B, 0x83, 0x12, 0x8C, 0xB2, 0xE4, 0x4E, 0xBE, 0x85, 0x31, 0x24, 0xE2, 0xB4, 0xFF, 0xD5, 0xC3, 0x7D, 0xC, 0x55, 0x6F, 0x89, 0x7B, 0xF2, 0x74, 0x5D, 0xBE, 0x72, 0xB1, 0x96, 0x16, 0x3B, 0xFE, 0xB1, 0xDE, 0x80, 0x35, 0x12, 0xC7, 0x25, 0xA7, 0x6, 0xDC, 0x9B, 0x94, 0x26, 0x69, 0xCF, 0x74, 0xF1, 0x9B, 0xC1, 0xD2, 0x4A, 0xF1, 0x9E, 0xC1, 0x69, 0x9B, 0xE4, 0xE3, 0x25, 0x4F, 0x38, 0x86, 0x47, 0xBE, 0xEF, 0xB5, 0xD5, 0x8C, 0x8B, 0xC6, 0x9D, 0xC1, 0xF, 0x65, 0x9C, 0xAC, 0x77, 0xCC, 0xA1, 0xC, 0x24, 0x75, 0x2, 0x2B, 0x59, 0x6F, 0x2C, 0xE9, 0x2D, 0x83, 0xE4, 0xA6, 0x6E, 0xAA, 0x84, 0x74, 0x4A, 0xD4, 0xFB, 0x41, 0xBD, 0xDC, 0xA9, 0xB0, 0x5C, 0xB5, 0x53, 0x11, 0x83, 0xDA, 0x88, 0xF9, 0x76, 0xAB, 0xDF, 0x66, 0xEE, 0x52, 0x51, 0x3E, 0x98, 0x10, 0x32, 0xB4, 0x2D, 0x6D, 0xC6, 0x31, 0xA8, 0x3F, 0x21, 0xFB, 0x98, 0xC8, 0x27, 0x3, 0xB0, 0xE4, 0xE, 0xEF, 0xBE, 0xC7, 0x7F, 0x59, 0xBF, 0xC2, 0x8F, 0xA8, 0x3D, 0xF3, 0xB, 0xE0, 0xC6, 0x25, 0xA7, 0xA, 0x93, 0x47, 0x91, 0xA7, 0xD5, 0x6F, 0x82, 0x3, 0xE0, 0x51, 0x63, 0xCA, 0x6, 0x70, 0x6E, 0xE, 0xA, 0x67, 0x29, 0x29, 0x14, 0xFC, 0x2F, 0xD2, 0x46, 0x85, 0xA, 0xB7, 0x27, 0x26, 0xC9, 0x26, 0x5C, 0x38, 0x21, 0x1B, 0x2E, 0xED, 0x2A, 0xC4, 0x5A, 0xFC, 0x6D, 0x2C, 0x4D, 0xDF, 0xB3, 0x95, 0x9D, 0x13, 0xD, 0x38, 0x53, 0xDE, 0x63, 0xAF, 0x8B, 0x54, 0x73, 0xA, 0x65, 0xA8, 0xB2, 0x77, 0x3C, 0xBB, 0xA, 0x6A, 0x76, 0xE6, 0xAE, 0xED, 0x47, 0x2E, 0xC9, 0xC2, 0x81, 0x3B, 0x35, 0x82, 0x14, 0x85, 0x2C, 0x72, 0x92, 0x64, 0x3, 0xF1, 0x4C, 0xA1, 0xE8, 0xBF, 0xA2, 0x1, 0x30, 0x42, 0xBC, 0x4B, 0x66, 0x1A, 0xA8, 0x91, 0x97, 0xF8, 0xD0, 0x70, 0x8B, 0x4B, 0xC2, 0x30, 0xBE, 0x54, 0x6, 0xA3, 0x51, 0x6C, 0xC7, 0x18, 0x52, 0xEF, 0xD6, 0x19, 0xE8, 0x92, 0xD1, 0x10, 0xA9, 0x65, 0x55, 0x24, 0x6, 0x99, 0xD6, 0x2A, 0x20, 0x71, 0x57, 0x85, 0x35, 0xE, 0xF4, 0xB8, 0xD1, 0xBB, 0x32, 0x70, 0xA0, 0x6A, 0x10, 0xC8, 0xD0, 0xD2, 0xB8, 0x16, 0xC1, 0xA4, 0x19, 0x53, 0xAB, 0x41, 0x51, 0x8, 0x6C, 0x37, 0x1E, 0x99, 0xEB, 0x8E, 0xDF, 0x4C, 0x77, 0x48, 0x27, 0xA8, 0x48, 0x9B, 0xE1, 0xB5, 0xBC, 0xB0, 0x34, 0x63, 0x5A, 0xC9, 0xC5, 0xB3, 0xC, 0x1C, 0x39, 0xCB, 0x8A, 0x41, 0xE3, 0x4A, 0xAA, 0xD8, 0x4E, 0x73, 0xE3, 0x63, 0x77, 0x4F, 0xCA, 0x9C, 0x5B, 0xA3, 0xB8, 0xB2, 0xD6, 0xF3, 0x6F, 0x2E, 0x68, 0xFC, 0xB2, 0xEF, 0x5D, 0xEE, 0x82, 0x8F, 0x74, 0x60, 0x2F, 0x17, 0x43, 0x6F, 0x63, 0xA5, 0x78, 0x72, 0xAB, 0xF0, 0xA1, 0x14, 0x78, 0xC8, 0x84, 0xEC, 0x39, 0x64, 0x1A, 0x8, 0x2, 0xC7, 0x8C, 0x28, 0x1E, 0x63, 0x23, 0xFA, 0xFF, 0xBE, 0x90, 0xE9, 0xBD, 0x82, 0xDE, 0xEB, 0x6C, 0x50, 0xA4, 0x15, 0x79, 0xC6, 0xB2, 0xF7, 0xA3, 0xF9, 0xBE, 0x2B, 0x53, 0x72, 0xE3, 0xF2, 0x78, 0x71, 0xC6, 0x9C, 0x61, 0x26, 0xEA, 0xCE, 0x3E, 0x27, 0xCA, 0x7, 0xC2, 0xC0, 0x21, 0xC7, 0xB8, 0x86, 0xD1, 0x1E, 0xEB, 0xE0, 0xCD, 0xD6, 0x7D, 0xDA, 0xEA, 0x78, 0xD1, 0x6E, 0xEE, 0x7F, 0x4F, 0x7D, 0xF5, 0xBA, 0x6F, 0x17, 0x72, 0xAA, 0x67, 0xF0, 0x6, 0xA6, 0x98, 0xC8, 0xA2, 0xC5, 0x7D, 0x63, 0xA, 0xAE, 0xD, 0xF9, 0xBE, 0x4, 0x98, 0x3F, 0x11, 0x1B, 0x47, 0x1C, 0x13, 0x35, 0xB, 0x71, 0x1B, 0x84, 0x7D, 0x4, 0x23, 0xF5, 0x77, 0xDB, 0x28, 0x93, 0x24, 0xC7, 0x40, 0x7B, 0xAB, 0xCA, 0x32, 0xBC, 0xBE, 0xC9, 0x15, 0xA, 0xBE, 0x9E, 0x3C, 0x4C, 0xD, 0x10, 0x9C, 0xC4, 0x67, 0x1D, 0x43, 0xB6, 0x42, 0x3E, 0xCB, 0xBE, 0xD4, 0xC5, 0x4C, 0x2A, 0x7E, 0x65, 0xFC, 0x9C, 0x29, 0x7F, 0x59, 0xEC, 0xFA, 0xD6, 0x3A, 0xAB, 0x6F, 0xCB, 0x5F, 0x17, 0x58, 0x47, 0x4A, 0x8C, 0x19, 0x44, 0x6C };
static Il2CppFieldDefaultValueEntry t1703_f46_DefaultValue = 
{
	&t1703_f46_FieldInfo, { (char*)t1703_f46_DefaultValueData, &t1700_0_0_0 }};
static const uint8_t t1703_f47_DefaultValueData[] = { 0x3C, 0x0, 0x3E, 0x0, 0x22, 0x0, 0x27, 0x0, 0x26, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f47_DefaultValue = 
{
	&t1703_f47_FieldInfo, { (char*)t1703_f47_DefaultValueData, &t1692_0_0_0 }};
static const uint8_t t1703_f48_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x1, 0x1, 0x1, 0x2, 0x3, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2, 0x2, 0x1, 0x2, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x2, 0x0, 0x2, 0x2, 0x2, 0x2, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f48_DefaultValue = 
{
	&t1703_f48_FieldInfo, { (char*)t1703_f48_DefaultValueData, &t1701_0_0_0 }};
static const uint8_t t1703_f49_DefaultValueData[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3E, 0xFF, 0xFF, 0xFF, 0x3F, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
static Il2CppFieldDefaultValueEntry t1703_f49_DefaultValue = 
{
	&t1703_f49_FieldInfo, { (char*)t1703_f49_DefaultValueData, &t1698_0_0_0 }};
static const uint8_t t1703_f50_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1C, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f50_DefaultValue = 
{
	&t1703_f50_FieldInfo, { (char*)t1703_f50_DefaultValueData, &t1702_0_0_0 }};
static const uint8_t t1703_f51_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1D, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x1E, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1703_f51_DefaultValue = 
{
	&t1703_f51_FieldInfo, { (char*)t1703_f51_DefaultValueData, &t1702_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1703_FDVs[] = 
{
	&t1703_f0_DefaultValue,
	&t1703_f1_DefaultValue,
	&t1703_f2_DefaultValue,
	&t1703_f3_DefaultValue,
	&t1703_f4_DefaultValue,
	&t1703_f5_DefaultValue,
	&t1703_f6_DefaultValue,
	&t1703_f7_DefaultValue,
	&t1703_f8_DefaultValue,
	&t1703_f9_DefaultValue,
	&t1703_f10_DefaultValue,
	&t1703_f11_DefaultValue,
	&t1703_f12_DefaultValue,
	&t1703_f13_DefaultValue,
	&t1703_f14_DefaultValue,
	&t1703_f15_DefaultValue,
	&t1703_f16_DefaultValue,
	&t1703_f17_DefaultValue,
	&t1703_f18_DefaultValue,
	&t1703_f19_DefaultValue,
	&t1703_f20_DefaultValue,
	&t1703_f21_DefaultValue,
	&t1703_f22_DefaultValue,
	&t1703_f23_DefaultValue,
	&t1703_f24_DefaultValue,
	&t1703_f25_DefaultValue,
	&t1703_f26_DefaultValue,
	&t1703_f27_DefaultValue,
	&t1703_f28_DefaultValue,
	&t1703_f29_DefaultValue,
	&t1703_f30_DefaultValue,
	&t1703_f31_DefaultValue,
	&t1703_f32_DefaultValue,
	&t1703_f33_DefaultValue,
	&t1703_f34_DefaultValue,
	&t1703_f35_DefaultValue,
	&t1703_f36_DefaultValue,
	&t1703_f37_DefaultValue,
	&t1703_f38_DefaultValue,
	&t1703_f39_DefaultValue,
	&t1703_f40_DefaultValue,
	&t1703_f41_DefaultValue,
	&t1703_f42_DefaultValue,
	&t1703_f43_DefaultValue,
	&t1703_f44_DefaultValue,
	&t1703_f45_DefaultValue,
	&t1703_f46_DefaultValue,
	&t1703_f47_DefaultValue,
	&t1703_f48_DefaultValue,
	&t1703_f49_DefaultValue,
	&t1703_f50_DefaultValue,
	&t1703_f51_DefaultValue,
	NULL
};
static MethodInfo* t1703_MIs[] =
{
	NULL
};
extern TypeInfo t1683_TI;
extern TypeInfo t1684_TI;
extern TypeInfo t1685_TI;
extern TypeInfo t1686_TI;
extern TypeInfo t1687_TI;
extern TypeInfo t1688_TI;
extern TypeInfo t1689_TI;
extern TypeInfo t1690_TI;
extern TypeInfo t1691_TI;
extern TypeInfo t1692_TI;
extern TypeInfo t1693_TI;
extern TypeInfo t1694_TI;
extern TypeInfo t1695_TI;
extern TypeInfo t1696_TI;
extern TypeInfo t1697_TI;
extern TypeInfo t1698_TI;
extern TypeInfo t1699_TI;
extern TypeInfo t1700_TI;
extern TypeInfo t1701_TI;
extern TypeInfo t1702_TI;
static TypeInfo* t1703_TI__nestedTypes[21] =
{
	&t1683_TI,
	&t1684_TI,
	&t1685_TI,
	&t1686_TI,
	&t1687_TI,
	&t1688_TI,
	&t1689_TI,
	&t1690_TI,
	&t1691_TI,
	&t1692_TI,
	&t1693_TI,
	&t1694_TI,
	&t1695_TI,
	&t1696_TI,
	&t1697_TI,
	&t1698_TI,
	&t1699_TI,
	&t1700_TI,
	&t1701_TI,
	&t1702_TI,
	NULL
};
static MethodInfo* t1703_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t1703_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1703__CustomAttributeCache = {
1,
NULL,
&t1703_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1703_0_0_0;
extern Il2CppType t1703_1_0_0;
struct t1703;
extern CustomAttributesCache t1703__CustomAttributeCache;
TypeInfo t1703_TI = 
{
	&g_mscorlib_dll_Image, NULL, "<PrivateImplementationDetails>", "", t1703_MIs, NULL, t1703_FIs, NULL, &t29_TI, t1703_TI__nestedTypes, NULL, &t1703_TI, NULL, t1703_VT, &t1703__CustomAttributeCache, &t1703_TI, &t1703_0_0_0, &t1703_1_0_0, NULL, NULL, NULL, t1703_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1703), 0, -1, sizeof(t1703_SFs), 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 52, 0, 20, 4, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
