﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2464;
struct t29;
struct t2461;
struct t346;
struct t20;
struct t136;
struct t316;
#include "t2469.h"

 void m12928_gshared (t2464 * __this, t2461 * p0, MethodInfo* method);
#define m12928(__this, p0, method) (void)m12928_gshared((t2464 *)__this, (t2461 *)p0, method)
 void m12929_gshared (t2464 * __this, t29 * p0, MethodInfo* method);
#define m12929(__this, p0, method) (void)m12929_gshared((t2464 *)__this, (t29 *)p0, method)
 void m12930_gshared (t2464 * __this, MethodInfo* method);
#define m12930(__this, method) (void)m12930_gshared((t2464 *)__this, method)
 bool m12931_gshared (t2464 * __this, t29 * p0, MethodInfo* method);
#define m12931(__this, p0, method) (bool)m12931_gshared((t2464 *)__this, (t29 *)p0, method)
 bool m12932_gshared (t2464 * __this, t29 * p0, MethodInfo* method);
#define m12932(__this, p0, method) (bool)m12932_gshared((t2464 *)__this, (t29 *)p0, method)
 t29* m12933_gshared (t2464 * __this, MethodInfo* method);
#define m12933(__this, method) (t29*)m12933_gshared((t2464 *)__this, method)
 void m12934_gshared (t2464 * __this, t20 * p0, int32_t p1, MethodInfo* method);
#define m12934(__this, p0, p1, method) (void)m12934_gshared((t2464 *)__this, (t20 *)p0, (int32_t)p1, method)
 t29 * m12935_gshared (t2464 * __this, MethodInfo* method);
#define m12935(__this, method) (t29 *)m12935_gshared((t2464 *)__this, method)
 bool m12936_gshared (t2464 * __this, MethodInfo* method);
#define m12936(__this, method) (bool)m12936_gshared((t2464 *)__this, method)
 bool m12937_gshared (t2464 * __this, MethodInfo* method);
#define m12937(__this, method) (bool)m12937_gshared((t2464 *)__this, method)
 t29 * m12938_gshared (t2464 * __this, MethodInfo* method);
#define m12938(__this, method) (t29 *)m12938_gshared((t2464 *)__this, method)
 void m12939_gshared (t2464 * __this, t316* p0, int32_t p1, MethodInfo* method);
#define m12939(__this, p0, p1, method) (void)m12939_gshared((t2464 *)__this, (t316*)p0, (int32_t)p1, method)
 t2469  m12940 (t2464 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12941_gshared (t2464 * __this, MethodInfo* method);
#define m12941(__this, method) (int32_t)m12941_gshared((t2464 *)__this, method)
