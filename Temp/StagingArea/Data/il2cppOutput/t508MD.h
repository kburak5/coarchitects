﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t508;
struct t29;
struct t509;
struct t66;
struct t67;
#include "t35.h"

 void m2608 (t508 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2609 (t508 * __this, t509* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2610 (t508 * __this, t509* p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2611 (t508 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
