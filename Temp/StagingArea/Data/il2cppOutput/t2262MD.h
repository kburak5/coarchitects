﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2262;
#include "t57.h"

 void m11385 (t2262 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11386 (t2262 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11387 (t2262 * __this, t57  p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
