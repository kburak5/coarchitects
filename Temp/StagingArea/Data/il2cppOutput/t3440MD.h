﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3440;
struct t29;
struct t20;
#include "t1126.h"

 void m19070 (t3440 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19071 (t3440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19072 (t3440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19073 (t3440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m19074 (t3440 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
