﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1694;
struct t1694_marshaled;

void t1694_marshal(const t1694& unmarshaled, t1694_marshaled& marshaled);
void t1694_marshal_back(const t1694_marshaled& marshaled, t1694& unmarshaled);
void t1694_marshal_cleanup(t1694_marshaled& marshaled);
