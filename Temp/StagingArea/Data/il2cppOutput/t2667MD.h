﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2667;
struct t29;
struct t20;
#include "t225.h"

 void m14461 (t2667 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14462 (t2667 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14463 (t2667 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14464 (t2667 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14465 (t2667 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
