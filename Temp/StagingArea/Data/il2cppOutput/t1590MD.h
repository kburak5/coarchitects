﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1590;
struct t200;
struct t7;
struct t781;
struct t1575;

 void m8643 (t1590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8644 (t1590 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8645 (t1590 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8646 (t1590 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8647 (t1590 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, t1575 ** p5, t200** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8648 (t1590 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8649 (t1590 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, t1575 ** p5, t200** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8650 (t1590 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8651 (t1590 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8652 (t1590 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8653 (t1590 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8654 (t1590 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8655 (t1590 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
