﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1586;
struct t7;

 void m8594 (t1586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8595 (t1586 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8596 (t1586 * __this, uint16_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8597 (t1586 * __this, uint16_t p0, uint16_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
