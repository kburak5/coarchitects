﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1027;
struct t7;
struct t781;
#include "t1018.h"
#include "t1021.h"
#include "t1022.h"
#include "t1024.h"

 void m4952 (t1027 * __this, int16_t p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, bool p5, bool p6, uint8_t p7, uint8_t p8, int16_t p9, uint8_t p10, uint8_t p11, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4953 (t1027 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4954 (t1027 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4955 (t1027 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4956 (t1027 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
