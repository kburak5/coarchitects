﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3205;
struct t29;
struct t20;

 void m17825 (t3205 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17826 (t3205 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17827 (t3205 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17828 (t3205 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m17829 (t3205 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
