﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2525;
struct t29;

 void m13409 (t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13410 (t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13411 (t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13412 (t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13413 (t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13414 (t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
