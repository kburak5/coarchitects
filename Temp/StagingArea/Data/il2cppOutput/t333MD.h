﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t333;
struct t335;
#include "t17.h"
#include "t330.h"

 void m2601 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2602 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, float p4, float p5, t330 * p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2603 (t29 * __this, t17 * p0, t17 * p1, float p2, int32_t p3, float p4, float p5, t330 * p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t330  m1615 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t330  m2604 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, float p4, float p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t335* m1469 (t29 * __this, t17  p0, t17  p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t335* m2605 (t29 * __this, t17 * p0, t17 * p1, float p2, int32_t p3, float p4, float p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
