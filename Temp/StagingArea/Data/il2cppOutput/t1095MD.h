﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1095;
struct t781;
struct t7;
struct t1094;
struct t29;
struct t42;
#include "t1126.h"
#include "t465.h"

 void m8893 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8894 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5178 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5151 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8895 (t29 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8896 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8897 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8898 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8899 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8900 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8901 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8902 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8903 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8904 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8905 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8906 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8907 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8908 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8909 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8910 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8911 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8912 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8913 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8914 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8915 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8916 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8917 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8918 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8919 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8920 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8921 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8922 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8923 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m8924 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5180 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8925 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8926 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8927 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8928 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8929 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8930 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8931 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8932 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8933 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8934 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8935 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8936 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8937 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8938 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8939 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8940 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8941 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8942 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8943 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m8944 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8945 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8946 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8947 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8948 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8949 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8950 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8951 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8952 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8953 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8954 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8955 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8956 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m8957 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8958 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8959 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8960 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8961 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8962 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8963 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8964 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8965 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8966 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8967 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8968 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8969 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8970 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m8971 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8972 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8973 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8974 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8975 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8976 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8977 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8978 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8979 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8980 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8981 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5109 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8982 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8983 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8984 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8985 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8986 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8987 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8988 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8989 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8990 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8991 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8992 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8993 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8994 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8995 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8996 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8997 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8998 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8999 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9000 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5192 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9001 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9002 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9003 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9004 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9005 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9006 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9007 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9008 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9009 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9010 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9011 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9012 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9013 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9014 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9015 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9016 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9017 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9018 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9019 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9020 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9021 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9022 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9023 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9024 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9025 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9026 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9027 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9028 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9029 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9030 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9031 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9032 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9033 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9034 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9035 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9036 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9037 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9038 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9039 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9040 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9041 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9042 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9043 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9044 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9045 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9046 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9047 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9048 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9049 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9050 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9051 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9052 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9053 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9054 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9055 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9056 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9057 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9058 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9059 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9060 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9061 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9062 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9063 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9064 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9065 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9066 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9067 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9068 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9069 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9070 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9071 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9072 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9073 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9074 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9075 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9076 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9077 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9078 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9079 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9080 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9081 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9082 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9083 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9084 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9085 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9086 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9087 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9088 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9089 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9090 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9091 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9092 (t29 * __this, t29 * p0, t42 * p1, t29 * p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
