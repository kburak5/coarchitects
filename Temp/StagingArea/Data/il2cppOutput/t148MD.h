﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t148;
struct t156;
struct t351;

 void m1546 (t29 * __this, t351 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2691 (t29 * __this, t351 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m1914 (t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1758 (t148 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2692 (t29 * __this, t148 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1916 (t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1918 (t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
