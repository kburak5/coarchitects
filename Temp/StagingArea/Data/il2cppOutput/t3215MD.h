﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3215;
struct t29;
struct t20;
#include "t798.h"

 void m17875 (t3215 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17876 (t3215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17877 (t3215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17878 (t3215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17879 (t3215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
