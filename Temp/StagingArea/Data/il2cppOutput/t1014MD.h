﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1014;
struct t781;
struct t783;

 void m4600 (t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4601 (t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4602 (t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4603 (t1014 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4604 (t1014 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4605 (t1014 * __this, t783 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
