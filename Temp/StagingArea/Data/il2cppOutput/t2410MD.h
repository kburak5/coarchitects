﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2410;
struct t29;
struct t20;
#include "t140.h"

 void m12451 (t2410 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12452 (t2410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12453 (t2410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12454 (t2410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12455 (t2410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
