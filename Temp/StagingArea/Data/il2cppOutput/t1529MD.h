﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1529;
struct t781;
struct t7;
struct t777;

 void m8221 (t1529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8222 (t1529 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8223 (t1529 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8224 (t1529 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
