﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1240;
struct t200;
struct t7;

 void m7208 (t1240 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7209 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7210 (t1240 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7211 (t1240 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7212 (t1240 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7213 (t1240 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7214 (t1240 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7215 (t1240 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7216 (t1240 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1240 * m7217 (t29 * __this, t1240 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
