﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t933;
struct t781;
struct t7;
struct t972;
#include "t934.h"

 void m4451 (t933 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4452 (t933 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4453 (t933 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4454 (t933 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4455 (t933 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4044 (t933 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4456 (t933 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4457 (t933 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t934  m4458 (t933 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4459 (t933 * __this, t934  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4460 (t933 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4461 (t933 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4462 (t933 * __this, t972 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
