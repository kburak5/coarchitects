﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3086;
struct t29;
struct t20;
#include "t542.h"

 void m17050 (t3086 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17051 (t3086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17052 (t3086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17053 (t3086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t542  m17054 (t3086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
