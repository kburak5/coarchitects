﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t163;
struct t29;
struct t2512;
struct t20;
struct t136;
struct t404;
struct t2513;
struct t2514;
struct t201;
struct t2515;
struct t2516;
#include "t184.h"
#include "t2517.h"

 void m1735 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2903 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13257 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m13258 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13259 (t163 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13260 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13261 (t163 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13262 (t163 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13263 (t163 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13264 (t163 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13265 (t163 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13266 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13267 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13268 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13269 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13270 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13271 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13272 (t163 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1584 (t163 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13273 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13274 (t163 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13275 (t163 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13276 (t163 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2514 * m13277 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1601 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13278 (t163 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13279 (t163 * __this, t201* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13280 (t163 * __this, t2515 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13281 (t29 * __this, t2515 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13282 (t163 * __this, int32_t p0, int32_t p1, t2515 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2517  m13283 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13284 (t163 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13285 (t163 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13286 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13287 (t163 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13288 (t163 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13289 (t163 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13290 (t163 * __this, t2515 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13291 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13292 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13293 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13294 (t163 * __this, t2516 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t201* m1787 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13295 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1599 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1600 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1786 (t163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m2044 (t163 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2045 (t163 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
