﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3246;
struct t29;
struct t20;
#include "t1007.h"

 void m18026 (t3246 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18027 (t3246 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18028 (t3246 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18029 (t3246 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18030 (t3246 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
