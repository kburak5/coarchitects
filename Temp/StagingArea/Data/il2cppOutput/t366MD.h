﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t366;
struct t155;
struct t136;
struct t2546;
struct t2545;
struct t2549;
struct t168;

#include "t2414MD.h"
#define m1636(__this, method) (void)m12456_gshared((t2414 *)__this, method)
#define m13762(__this, method) (t29 *)m12458_gshared((t2414 *)__this, method)
#define m1635(__this, p0, method) (void)m12459_gshared((t2414 *)__this, (t29 *)p0, method)
#define m1638(__this, p0, method) (bool)m12460_gshared((t2414 *)__this, (t29 *)p0, method)
#define m13763(__this, method) (t29*)m12462_gshared((t2414 *)__this, method)
#define m13764(__this, method) (void)m12463_gshared((t2414 *)__this, method)
#define m13765(__this, p0, method) (bool)m12465_gshared((t2414 *)__this, (t29 *)p0, method)
#define m13766(__this, p0, p1, method) (void)m12467_gshared((t2414 *)__this, (t316*)p0, (int32_t)p1, method)
#define m13767(__this, method) (int32_t)m12468_gshared((t2414 *)__this, method)
#define m13768(__this, method) (bool)m12470_gshared((t2414 *)__this, method)
#define m13769(__this, p0, method) (int32_t)m12472_gshared((t2414 *)__this, (t29 *)p0, method)
#define m13770(__this, p0, p1, method) (void)m12474_gshared((t2414 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13771(__this, p0, method) (void)m12476_gshared((t2414 *)__this, (int32_t)p0, method)
#define m13772(__this, p0, method) (t155 *)m12477_gshared((t2414 *)__this, (int32_t)p0, method)
#define m13773(__this, p0, p1, method) (void)m12479_gshared((t2414 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13774(__this, p0, method) (void)m12480_gshared((t2414 *)__this, (t2180 *)p0, method)
#define m13775(__this, p0, method) (void)m12481_gshared((t2414 *)__this, (t2181 *)p0, method)
