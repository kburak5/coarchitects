﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t470;
struct t263;
struct t473;
struct t466;
struct t7;

 void m2159 (t470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t263 * m2160 (t470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2161 (t470 * __this, t473* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2162 (t470 * __this, t466 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2163 (t470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2164 (t470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2165 (t470 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2166 (t470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2167 (t470 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2168 (t470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
