﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2418;
struct t29;
struct t2415;
struct t2424;
struct t20;
struct t136;
struct t841;
#include "t2425.h"

 void m12536 (t2418 * __this, t2415 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12537 (t2418 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12538 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12539 (t2418 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12540 (t2418 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m12541 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12542 (t2418 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12543 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12544 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12545 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12546 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12547 (t2418 * __this, t841* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2425  m12548 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12549 (t2418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
