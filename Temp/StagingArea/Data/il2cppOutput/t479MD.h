﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t479;
struct t7;
struct t463;
struct t295;

 void m2175 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2176 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2177 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t463 * m2178 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t463 * m2179 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2180 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2181 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2182 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2183 (t29 * __this, t295 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2184 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2185 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
