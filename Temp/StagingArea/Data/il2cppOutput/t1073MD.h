﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1073;
struct t1020;
struct t781;
struct t780;
struct t945;
struct t7;

 void m5049 (t1073 * __this, t1020 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5050 (t1073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5051 (t1073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5052 (t1073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5053 (t1073 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5054 (t1073 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5055 (t1073 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5056 (t1073 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5057 (t29 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
