﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t628;
struct t29;
struct t42;
struct t3067;
struct t20;
struct t136;
struct t3069;
struct t3070;
struct t3071;
struct t537;
struct t3072;
struct t3073;
#include "t3074.h"

#include "t294MD.h"
#define m2912(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m16885(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m16886(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m16887(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m16888(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m16889(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m16890(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m16891(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m16892(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m16893(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m16894(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m16895(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m16896(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m16897(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m16898(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m16899(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m16900(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m16901(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m2913(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m16902(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m16903(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m16904(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m16905(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m16906(__this, method) (t3071 *)m10583_gshared((t294 *)__this, method)
#define m16907(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m16908(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m16909(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m16910(__this, p0, method) (t42 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m16911(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m16912(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t3074  m16913 (t628 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m16914(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m16915(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m16916(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m16917(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m16918(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m16919(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m16920(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m16921(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m16922(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m16923(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m16924(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m2914(__this, method) (t537*)m10619_gshared((t294 *)__this, method)
#define m16925(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m16926(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m16927(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m16928(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m16929(__this, p0, method) (t42 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m16930(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
