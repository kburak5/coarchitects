﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5833_TI;

#include "t740.h"
#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo m30499_MI;
extern MethodInfo m30500_MI;
static PropertyInfo t5833____Item_PropertyInfo = 
{
	&t5833_TI, "Item", &m30499_MI, &m30500_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5833_PIs[] =
{
	&t5833____Item_PropertyInfo,
	NULL
};
extern Il2CppType t740_0_0_0;
extern Il2CppType t740_0_0_0;
static ParameterInfo t5833_m30501_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t740_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30501_GM;
MethodInfo m30501_MI = 
{
	"IndexOf", NULL, &t5833_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5833_m30501_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30501_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t740_0_0_0;
static ParameterInfo t5833_m30502_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t740_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30502_GM;
MethodInfo m30502_MI = 
{
	"Insert", NULL, &t5833_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5833_m30502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30502_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5833_m30503_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30503_GM;
MethodInfo m30503_MI = 
{
	"RemoveAt", NULL, &t5833_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5833_m30503_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30503_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5833_m30499_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t740_0_0_0;
extern void* RuntimeInvoker_t740_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30499_GM;
MethodInfo m30499_MI = 
{
	"get_Item", NULL, &t5833_TI, &t740_0_0_0, RuntimeInvoker_t740_t44, t5833_m30499_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30499_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t740_0_0_0;
static ParameterInfo t5833_m30500_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t740_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30500_GM;
MethodInfo m30500_MI = 
{
	"set_Item", NULL, &t5833_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5833_m30500_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30500_GM};
static MethodInfo* t5833_MIs[] =
{
	&m30501_MI,
	&m30502_MI,
	&m30503_MI,
	&m30499_MI,
	&m30500_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5832_TI;
extern TypeInfo t5834_TI;
static TypeInfo* t5833_ITIs[] = 
{
	&t603_TI,
	&t5832_TI,
	&t5834_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5833_0_0_0;
extern Il2CppType t5833_1_0_0;
struct t5833;
extern Il2CppGenericClass t5833_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5833_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5833_MIs, t5833_PIs, NULL, NULL, NULL, NULL, NULL, &t5833_TI, t5833_ITIs, NULL, &t1908__CustomAttributeCache, &t5833_TI, &t5833_0_0_0, &t5833_1_0_0, NULL, &t5833_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4541_TI;

#include "t741.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo m30504_MI;
static PropertyInfo t4541____Current_PropertyInfo = 
{
	&t4541_TI, "Current", &m30504_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4541_PIs[] =
{
	&t4541____Current_PropertyInfo,
	NULL
};
extern Il2CppType t741_0_0_0;
extern void* RuntimeInvoker_t741 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30504_GM;
MethodInfo m30504_MI = 
{
	"get_Current", NULL, &t4541_TI, &t741_0_0_0, RuntimeInvoker_t741, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30504_GM};
static MethodInfo* t4541_MIs[] =
{
	&m30504_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4541_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4541_0_0_0;
extern Il2CppType t4541_1_0_0;
struct t4541;
extern Il2CppGenericClass t4541_GC;
TypeInfo t4541_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4541_MIs, t4541_PIs, NULL, NULL, NULL, NULL, NULL, &t4541_TI, t4541_ITIs, NULL, &EmptyCustomAttributesCache, &t4541_TI, &t4541_0_0_0, &t4541_1_0_0, NULL, &t4541_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3182.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3182_TI;
#include "t3182MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t741_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m17677_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m23269_MI;
struct t20;
#include "t915.h"
 int32_t m23269 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17673_MI;
 void m17673 (t3182 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17674_MI;
 t29 * m17674 (t3182 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17677(__this, &m17677_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t741_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17675_MI;
 void m17675 (t3182 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17676_MI;
 bool m17676 (t3182 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17677 (t3182 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23269(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23269_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
extern Il2CppType t20_0_0_1;
FieldInfo t3182_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3182_TI, offsetof(t3182, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3182_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3182_TI, offsetof(t3182, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3182_FIs[] =
{
	&t3182_f0_FieldInfo,
	&t3182_f1_FieldInfo,
	NULL
};
static PropertyInfo t3182____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3182_TI, "System.Collections.IEnumerator.Current", &m17674_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3182____Current_PropertyInfo = 
{
	&t3182_TI, "Current", &m17677_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3182_PIs[] =
{
	&t3182____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3182____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3182_m17673_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17673_GM;
MethodInfo m17673_MI = 
{
	".ctor", (methodPointerType)&m17673, &t3182_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3182_m17673_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17673_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17674_GM;
MethodInfo m17674_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17674, &t3182_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17674_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17675_GM;
MethodInfo m17675_MI = 
{
	"Dispose", (methodPointerType)&m17675, &t3182_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17675_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17676_GM;
MethodInfo m17676_MI = 
{
	"MoveNext", (methodPointerType)&m17676, &t3182_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17676_GM};
extern Il2CppType t741_0_0_0;
extern void* RuntimeInvoker_t741 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17677_GM;
MethodInfo m17677_MI = 
{
	"get_Current", (methodPointerType)&m17677, &t3182_TI, &t741_0_0_0, RuntimeInvoker_t741, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17677_GM};
static MethodInfo* t3182_MIs[] =
{
	&m17673_MI,
	&m17674_MI,
	&m17675_MI,
	&m17676_MI,
	&m17677_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3182_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17674_MI,
	&m17676_MI,
	&m17675_MI,
	&m17677_MI,
};
static TypeInfo* t3182_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4541_TI,
};
static Il2CppInterfaceOffsetPair t3182_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4541_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3182_0_0_0;
extern Il2CppType t3182_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3182_GC;
extern TypeInfo t20_TI;
TypeInfo t3182_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3182_MIs, t3182_PIs, t3182_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3182_TI, t3182_ITIs, t3182_VT, &EmptyCustomAttributesCache, &t3182_TI, &t3182_0_0_0, &t3182_1_0_0, t3182_IOs, &t3182_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3182)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5835_TI;

#include "System_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo m30505_MI;
static PropertyInfo t5835____Count_PropertyInfo = 
{
	&t5835_TI, "Count", &m30505_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30506_MI;
static PropertyInfo t5835____IsReadOnly_PropertyInfo = 
{
	&t5835_TI, "IsReadOnly", &m30506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5835_PIs[] =
{
	&t5835____Count_PropertyInfo,
	&t5835____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30505_GM;
MethodInfo m30505_MI = 
{
	"get_Count", NULL, &t5835_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30505_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30506_GM;
MethodInfo m30506_MI = 
{
	"get_IsReadOnly", NULL, &t5835_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30506_GM};
extern Il2CppType t741_0_0_0;
extern Il2CppType t741_0_0_0;
static ParameterInfo t5835_m30507_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30507_GM;
MethodInfo m30507_MI = 
{
	"Add", NULL, &t5835_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5835_m30507_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30507_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30508_GM;
MethodInfo m30508_MI = 
{
	"Clear", NULL, &t5835_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30508_GM};
extern Il2CppType t741_0_0_0;
static ParameterInfo t5835_m30509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30509_GM;
MethodInfo m30509_MI = 
{
	"Contains", NULL, &t5835_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5835_m30509_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30509_GM};
extern Il2CppType t3901_0_0_0;
extern Il2CppType t3901_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5835_m30510_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3901_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30510_GM;
MethodInfo m30510_MI = 
{
	"CopyTo", NULL, &t5835_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5835_m30510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30510_GM};
extern Il2CppType t741_0_0_0;
static ParameterInfo t5835_m30511_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30511_GM;
MethodInfo m30511_MI = 
{
	"Remove", NULL, &t5835_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5835_m30511_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30511_GM};
static MethodInfo* t5835_MIs[] =
{
	&m30505_MI,
	&m30506_MI,
	&m30507_MI,
	&m30508_MI,
	&m30509_MI,
	&m30510_MI,
	&m30511_MI,
	NULL
};
extern TypeInfo t5837_TI;
static TypeInfo* t5835_ITIs[] = 
{
	&t603_TI,
	&t5837_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5835_0_0_0;
extern Il2CppType t5835_1_0_0;
struct t5835;
extern Il2CppGenericClass t5835_GC;
TypeInfo t5835_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5835_MIs, t5835_PIs, NULL, NULL, NULL, NULL, NULL, &t5835_TI, t5835_ITIs, NULL, &EmptyCustomAttributesCache, &t5835_TI, &t5835_0_0_0, &t5835_1_0_0, NULL, &t5835_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>
extern Il2CppType t4541_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30512_GM;
MethodInfo m30512_MI = 
{
	"GetEnumerator", NULL, &t5837_TI, &t4541_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30512_GM};
static MethodInfo* t5837_MIs[] =
{
	&m30512_MI,
	NULL
};
static TypeInfo* t5837_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5837_0_0_0;
extern Il2CppType t5837_1_0_0;
struct t5837;
extern Il2CppGenericClass t5837_GC;
TypeInfo t5837_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5837_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5837_TI, t5837_ITIs, NULL, &EmptyCustomAttributesCache, &t5837_TI, &t5837_0_0_0, &t5837_1_0_0, NULL, &t5837_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5836_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo m30513_MI;
extern MethodInfo m30514_MI;
static PropertyInfo t5836____Item_PropertyInfo = 
{
	&t5836_TI, "Item", &m30513_MI, &m30514_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5836_PIs[] =
{
	&t5836____Item_PropertyInfo,
	NULL
};
extern Il2CppType t741_0_0_0;
static ParameterInfo t5836_m30515_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30515_GM;
MethodInfo m30515_MI = 
{
	"IndexOf", NULL, &t5836_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5836_m30515_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30515_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t741_0_0_0;
static ParameterInfo t5836_m30516_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30516_GM;
MethodInfo m30516_MI = 
{
	"Insert", NULL, &t5836_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5836_m30516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30516_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5836_m30517_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30517_GM;
MethodInfo m30517_MI = 
{
	"RemoveAt", NULL, &t5836_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5836_m30517_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30517_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5836_m30513_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t741_0_0_0;
extern void* RuntimeInvoker_t741_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30513_GM;
MethodInfo m30513_MI = 
{
	"get_Item", NULL, &t5836_TI, &t741_0_0_0, RuntimeInvoker_t741_t44, t5836_m30513_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30513_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t741_0_0_0;
static ParameterInfo t5836_m30514_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30514_GM;
MethodInfo m30514_MI = 
{
	"set_Item", NULL, &t5836_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5836_m30514_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30514_GM};
static MethodInfo* t5836_MIs[] =
{
	&m30515_MI,
	&m30516_MI,
	&m30517_MI,
	&m30513_MI,
	&m30514_MI,
	NULL
};
static TypeInfo* t5836_ITIs[] = 
{
	&t603_TI,
	&t5835_TI,
	&t5837_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5836_0_0_0;
extern Il2CppType t5836_1_0_0;
struct t5836;
extern Il2CppGenericClass t5836_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5836_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5836_MIs, t5836_PIs, NULL, NULL, NULL, NULL, NULL, &t5836_TI, t5836_ITIs, NULL, &t1908__CustomAttributeCache, &t5836_TI, &t5836_0_0_0, &t5836_1_0_0, NULL, &t5836_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4543_TI;

#include "t742.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Sockets.AddressFamily>
extern MethodInfo m30518_MI;
static PropertyInfo t4543____Current_PropertyInfo = 
{
	&t4543_TI, "Current", &m30518_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4543_PIs[] =
{
	&t4543____Current_PropertyInfo,
	NULL
};
extern Il2CppType t742_0_0_0;
extern void* RuntimeInvoker_t742 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30518_GM;
MethodInfo m30518_MI = 
{
	"get_Current", NULL, &t4543_TI, &t742_0_0_0, RuntimeInvoker_t742, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30518_GM};
static MethodInfo* t4543_MIs[] =
{
	&m30518_MI,
	NULL
};
static TypeInfo* t4543_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4543_0_0_0;
extern Il2CppType t4543_1_0_0;
struct t4543;
extern Il2CppGenericClass t4543_GC;
TypeInfo t4543_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4543_MIs, t4543_PIs, NULL, NULL, NULL, NULL, NULL, &t4543_TI, t4543_ITIs, NULL, &EmptyCustomAttributesCache, &t4543_TI, &t4543_0_0_0, &t4543_1_0_0, NULL, &t4543_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3183.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3183_TI;
#include "t3183MD.h"

extern TypeInfo t742_TI;
extern MethodInfo m17682_MI;
extern MethodInfo m23280_MI;
struct t20;
 int32_t m23280 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17678_MI;
 void m17678 (t3183 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17679_MI;
 t29 * m17679 (t3183 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17682(__this, &m17682_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t742_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17680_MI;
 void m17680 (t3183 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17681_MI;
 bool m17681 (t3183 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17682 (t3183 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23280(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23280_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>
extern Il2CppType t20_0_0_1;
FieldInfo t3183_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3183_TI, offsetof(t3183, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3183_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3183_TI, offsetof(t3183, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3183_FIs[] =
{
	&t3183_f0_FieldInfo,
	&t3183_f1_FieldInfo,
	NULL
};
static PropertyInfo t3183____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3183_TI, "System.Collections.IEnumerator.Current", &m17679_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3183____Current_PropertyInfo = 
{
	&t3183_TI, "Current", &m17682_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3183_PIs[] =
{
	&t3183____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3183____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3183_m17678_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17678_GM;
MethodInfo m17678_MI = 
{
	".ctor", (methodPointerType)&m17678, &t3183_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3183_m17678_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17678_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17679_GM;
MethodInfo m17679_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17679, &t3183_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17679_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17680_GM;
MethodInfo m17680_MI = 
{
	"Dispose", (methodPointerType)&m17680, &t3183_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17680_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17681_GM;
MethodInfo m17681_MI = 
{
	"MoveNext", (methodPointerType)&m17681, &t3183_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17681_GM};
extern Il2CppType t742_0_0_0;
extern void* RuntimeInvoker_t742 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17682_GM;
MethodInfo m17682_MI = 
{
	"get_Current", (methodPointerType)&m17682, &t3183_TI, &t742_0_0_0, RuntimeInvoker_t742, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17682_GM};
static MethodInfo* t3183_MIs[] =
{
	&m17678_MI,
	&m17679_MI,
	&m17680_MI,
	&m17681_MI,
	&m17682_MI,
	NULL
};
static MethodInfo* t3183_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17679_MI,
	&m17681_MI,
	&m17680_MI,
	&m17682_MI,
};
static TypeInfo* t3183_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4543_TI,
};
static Il2CppInterfaceOffsetPair t3183_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4543_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3183_0_0_0;
extern Il2CppType t3183_1_0_0;
extern Il2CppGenericClass t3183_GC;
TypeInfo t3183_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3183_MIs, t3183_PIs, t3183_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3183_TI, t3183_ITIs, t3183_VT, &EmptyCustomAttributesCache, &t3183_TI, &t3183_0_0_0, &t3183_1_0_0, t3183_IOs, &t3183_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3183)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5838_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>
extern MethodInfo m30519_MI;
static PropertyInfo t5838____Count_PropertyInfo = 
{
	&t5838_TI, "Count", &m30519_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30520_MI;
static PropertyInfo t5838____IsReadOnly_PropertyInfo = 
{
	&t5838_TI, "IsReadOnly", &m30520_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5838_PIs[] =
{
	&t5838____Count_PropertyInfo,
	&t5838____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30519_GM;
MethodInfo m30519_MI = 
{
	"get_Count", NULL, &t5838_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30519_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30520_GM;
MethodInfo m30520_MI = 
{
	"get_IsReadOnly", NULL, &t5838_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30520_GM};
extern Il2CppType t742_0_0_0;
extern Il2CppType t742_0_0_0;
static ParameterInfo t5838_m30521_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t742_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30521_GM;
MethodInfo m30521_MI = 
{
	"Add", NULL, &t5838_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5838_m30521_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30521_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30522_GM;
MethodInfo m30522_MI = 
{
	"Clear", NULL, &t5838_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30522_GM};
extern Il2CppType t742_0_0_0;
static ParameterInfo t5838_m30523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t742_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30523_GM;
MethodInfo m30523_MI = 
{
	"Contains", NULL, &t5838_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5838_m30523_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30523_GM};
extern Il2CppType t3902_0_0_0;
extern Il2CppType t3902_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5838_m30524_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3902_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30524_GM;
MethodInfo m30524_MI = 
{
	"CopyTo", NULL, &t5838_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5838_m30524_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30524_GM};
extern Il2CppType t742_0_0_0;
static ParameterInfo t5838_m30525_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t742_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30525_GM;
MethodInfo m30525_MI = 
{
	"Remove", NULL, &t5838_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5838_m30525_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30525_GM};
static MethodInfo* t5838_MIs[] =
{
	&m30519_MI,
	&m30520_MI,
	&m30521_MI,
	&m30522_MI,
	&m30523_MI,
	&m30524_MI,
	&m30525_MI,
	NULL
};
extern TypeInfo t5840_TI;
static TypeInfo* t5838_ITIs[] = 
{
	&t603_TI,
	&t5840_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5838_0_0_0;
extern Il2CppType t5838_1_0_0;
struct t5838;
extern Il2CppGenericClass t5838_GC;
TypeInfo t5838_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5838_MIs, t5838_PIs, NULL, NULL, NULL, NULL, NULL, &t5838_TI, t5838_ITIs, NULL, &EmptyCustomAttributesCache, &t5838_TI, &t5838_0_0_0, &t5838_1_0_0, NULL, &t5838_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Sockets.AddressFamily>
extern Il2CppType t4543_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30526_GM;
MethodInfo m30526_MI = 
{
	"GetEnumerator", NULL, &t5840_TI, &t4543_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30526_GM};
static MethodInfo* t5840_MIs[] =
{
	&m30526_MI,
	NULL
};
static TypeInfo* t5840_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5840_0_0_0;
extern Il2CppType t5840_1_0_0;
struct t5840;
extern Il2CppGenericClass t5840_GC;
TypeInfo t5840_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5840_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5840_TI, t5840_ITIs, NULL, &EmptyCustomAttributesCache, &t5840_TI, &t5840_0_0_0, &t5840_1_0_0, NULL, &t5840_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5839_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>
extern MethodInfo m30527_MI;
extern MethodInfo m30528_MI;
static PropertyInfo t5839____Item_PropertyInfo = 
{
	&t5839_TI, "Item", &m30527_MI, &m30528_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5839_PIs[] =
{
	&t5839____Item_PropertyInfo,
	NULL
};
extern Il2CppType t742_0_0_0;
static ParameterInfo t5839_m30529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t742_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30529_GM;
MethodInfo m30529_MI = 
{
	"IndexOf", NULL, &t5839_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5839_m30529_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30529_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t742_0_0_0;
static ParameterInfo t5839_m30530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t742_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30530_GM;
MethodInfo m30530_MI = 
{
	"Insert", NULL, &t5839_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5839_m30530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30530_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5839_m30531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30531_GM;
MethodInfo m30531_MI = 
{
	"RemoveAt", NULL, &t5839_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5839_m30531_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30531_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5839_m30527_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t742_0_0_0;
extern void* RuntimeInvoker_t742_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30527_GM;
MethodInfo m30527_MI = 
{
	"get_Item", NULL, &t5839_TI, &t742_0_0_0, RuntimeInvoker_t742_t44, t5839_m30527_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30527_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t742_0_0_0;
static ParameterInfo t5839_m30528_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t742_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30528_GM;
MethodInfo m30528_MI = 
{
	"set_Item", NULL, &t5839_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5839_m30528_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30528_GM};
static MethodInfo* t5839_MIs[] =
{
	&m30529_MI,
	&m30530_MI,
	&m30531_MI,
	&m30527_MI,
	&m30528_MI,
	NULL
};
static TypeInfo* t5839_ITIs[] = 
{
	&t603_TI,
	&t5838_TI,
	&t5840_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5839_0_0_0;
extern Il2CppType t5839_1_0_0;
struct t5839;
extern Il2CppGenericClass t5839_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5839_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5839_MIs, t5839_PIs, NULL, NULL, NULL, NULL, NULL, &t5839_TI, t5839_ITIs, NULL, &t1908__CustomAttributeCache, &t5839_TI, &t5839_0_0_0, &t5839_1_0_0, NULL, &t5839_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4545_TI;

#include "t751.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileAccess>
extern MethodInfo m30532_MI;
static PropertyInfo t4545____Current_PropertyInfo = 
{
	&t4545_TI, "Current", &m30532_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4545_PIs[] =
{
	&t4545____Current_PropertyInfo,
	NULL
};
extern Il2CppType t751_0_0_0;
extern void* RuntimeInvoker_t751 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30532_GM;
MethodInfo m30532_MI = 
{
	"get_Current", NULL, &t4545_TI, &t751_0_0_0, RuntimeInvoker_t751, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30532_GM};
static MethodInfo* t4545_MIs[] =
{
	&m30532_MI,
	NULL
};
static TypeInfo* t4545_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4545_0_0_0;
extern Il2CppType t4545_1_0_0;
struct t4545;
extern Il2CppGenericClass t4545_GC;
TypeInfo t4545_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4545_MIs, t4545_PIs, NULL, NULL, NULL, NULL, NULL, &t4545_TI, t4545_ITIs, NULL, &EmptyCustomAttributesCache, &t4545_TI, &t4545_0_0_0, &t4545_1_0_0, NULL, &t4545_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3184.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3184_TI;
#include "t3184MD.h"

extern TypeInfo t751_TI;
extern MethodInfo m17687_MI;
extern MethodInfo m23291_MI;
struct t20;
 int32_t m23291 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17683_MI;
 void m17683 (t3184 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17684_MI;
 t29 * m17684 (t3184 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17687(__this, &m17687_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t751_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17685_MI;
 void m17685 (t3184 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17686_MI;
 bool m17686 (t3184 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17687 (t3184 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23291(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23291_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileAccess>
extern Il2CppType t20_0_0_1;
FieldInfo t3184_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3184_TI, offsetof(t3184, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3184_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3184_TI, offsetof(t3184, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3184_FIs[] =
{
	&t3184_f0_FieldInfo,
	&t3184_f1_FieldInfo,
	NULL
};
static PropertyInfo t3184____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3184_TI, "System.Collections.IEnumerator.Current", &m17684_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3184____Current_PropertyInfo = 
{
	&t3184_TI, "Current", &m17687_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3184_PIs[] =
{
	&t3184____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3184____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3184_m17683_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17683_GM;
MethodInfo m17683_MI = 
{
	".ctor", (methodPointerType)&m17683, &t3184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3184_m17683_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17683_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17684_GM;
MethodInfo m17684_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17684, &t3184_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17684_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17685_GM;
MethodInfo m17685_MI = 
{
	"Dispose", (methodPointerType)&m17685, &t3184_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17685_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17686_GM;
MethodInfo m17686_MI = 
{
	"MoveNext", (methodPointerType)&m17686, &t3184_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17686_GM};
extern Il2CppType t751_0_0_0;
extern void* RuntimeInvoker_t751 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17687_GM;
MethodInfo m17687_MI = 
{
	"get_Current", (methodPointerType)&m17687, &t3184_TI, &t751_0_0_0, RuntimeInvoker_t751, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17687_GM};
static MethodInfo* t3184_MIs[] =
{
	&m17683_MI,
	&m17684_MI,
	&m17685_MI,
	&m17686_MI,
	&m17687_MI,
	NULL
};
static MethodInfo* t3184_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17684_MI,
	&m17686_MI,
	&m17685_MI,
	&m17687_MI,
};
static TypeInfo* t3184_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4545_TI,
};
static Il2CppInterfaceOffsetPair t3184_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4545_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3184_0_0_0;
extern Il2CppType t3184_1_0_0;
extern Il2CppGenericClass t3184_GC;
TypeInfo t3184_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3184_MIs, t3184_PIs, t3184_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3184_TI, t3184_ITIs, t3184_VT, &EmptyCustomAttributesCache, &t3184_TI, &t3184_0_0_0, &t3184_1_0_0, t3184_IOs, &t3184_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3184)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5841_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileAccess>
extern MethodInfo m30533_MI;
static PropertyInfo t5841____Count_PropertyInfo = 
{
	&t5841_TI, "Count", &m30533_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30534_MI;
static PropertyInfo t5841____IsReadOnly_PropertyInfo = 
{
	&t5841_TI, "IsReadOnly", &m30534_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5841_PIs[] =
{
	&t5841____Count_PropertyInfo,
	&t5841____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30533_GM;
MethodInfo m30533_MI = 
{
	"get_Count", NULL, &t5841_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30533_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30534_GM;
MethodInfo m30534_MI = 
{
	"get_IsReadOnly", NULL, &t5841_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30534_GM};
extern Il2CppType t751_0_0_0;
extern Il2CppType t751_0_0_0;
static ParameterInfo t5841_m30535_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t751_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30535_GM;
MethodInfo m30535_MI = 
{
	"Add", NULL, &t5841_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5841_m30535_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30535_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30536_GM;
MethodInfo m30536_MI = 
{
	"Clear", NULL, &t5841_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30536_GM};
extern Il2CppType t751_0_0_0;
static ParameterInfo t5841_m30537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t751_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30537_GM;
MethodInfo m30537_MI = 
{
	"Contains", NULL, &t5841_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5841_m30537_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30537_GM};
extern Il2CppType t3550_0_0_0;
extern Il2CppType t3550_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5841_m30538_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3550_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30538_GM;
MethodInfo m30538_MI = 
{
	"CopyTo", NULL, &t5841_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5841_m30538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30538_GM};
extern Il2CppType t751_0_0_0;
static ParameterInfo t5841_m30539_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t751_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30539_GM;
MethodInfo m30539_MI = 
{
	"Remove", NULL, &t5841_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5841_m30539_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30539_GM};
static MethodInfo* t5841_MIs[] =
{
	&m30533_MI,
	&m30534_MI,
	&m30535_MI,
	&m30536_MI,
	&m30537_MI,
	&m30538_MI,
	&m30539_MI,
	NULL
};
extern TypeInfo t5843_TI;
static TypeInfo* t5841_ITIs[] = 
{
	&t603_TI,
	&t5843_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5841_0_0_0;
extern Il2CppType t5841_1_0_0;
struct t5841;
extern Il2CppGenericClass t5841_GC;
TypeInfo t5841_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5841_MIs, t5841_PIs, NULL, NULL, NULL, NULL, NULL, &t5841_TI, t5841_ITIs, NULL, &EmptyCustomAttributesCache, &t5841_TI, &t5841_0_0_0, &t5841_1_0_0, NULL, &t5841_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileAccess>
extern Il2CppType t4545_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30540_GM;
MethodInfo m30540_MI = 
{
	"GetEnumerator", NULL, &t5843_TI, &t4545_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30540_GM};
static MethodInfo* t5843_MIs[] =
{
	&m30540_MI,
	NULL
};
static TypeInfo* t5843_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5843_0_0_0;
extern Il2CppType t5843_1_0_0;
struct t5843;
extern Il2CppGenericClass t5843_GC;
TypeInfo t5843_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5843_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5843_TI, t5843_ITIs, NULL, &EmptyCustomAttributesCache, &t5843_TI, &t5843_0_0_0, &t5843_1_0_0, NULL, &t5843_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5842_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileAccess>
extern MethodInfo m30541_MI;
extern MethodInfo m30542_MI;
static PropertyInfo t5842____Item_PropertyInfo = 
{
	&t5842_TI, "Item", &m30541_MI, &m30542_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5842_PIs[] =
{
	&t5842____Item_PropertyInfo,
	NULL
};
extern Il2CppType t751_0_0_0;
static ParameterInfo t5842_m30543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t751_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30543_GM;
MethodInfo m30543_MI = 
{
	"IndexOf", NULL, &t5842_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5842_m30543_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30543_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t751_0_0_0;
static ParameterInfo t5842_m30544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t751_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30544_GM;
MethodInfo m30544_MI = 
{
	"Insert", NULL, &t5842_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5842_m30544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30544_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5842_m30545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30545_GM;
MethodInfo m30545_MI = 
{
	"RemoveAt", NULL, &t5842_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5842_m30545_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30545_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5842_m30541_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t751_0_0_0;
extern void* RuntimeInvoker_t751_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30541_GM;
MethodInfo m30541_MI = 
{
	"get_Item", NULL, &t5842_TI, &t751_0_0_0, RuntimeInvoker_t751_t44, t5842_m30541_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30541_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t751_0_0_0;
static ParameterInfo t5842_m30542_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t751_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30542_GM;
MethodInfo m30542_MI = 
{
	"set_Item", NULL, &t5842_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5842_m30542_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30542_GM};
static MethodInfo* t5842_MIs[] =
{
	&m30543_MI,
	&m30544_MI,
	&m30545_MI,
	&m30541_MI,
	&m30542_MI,
	NULL
};
static TypeInfo* t5842_ITIs[] = 
{
	&t603_TI,
	&t5841_TI,
	&t5843_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5842_0_0_0;
extern Il2CppType t5842_1_0_0;
struct t5842;
extern Il2CppGenericClass t5842_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5842_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5842_MIs, t5842_PIs, NULL, NULL, NULL, NULL, NULL, &t5842_TI, t5842_ITIs, NULL, &t1908__CustomAttributeCache, &t5842_TI, &t5842_0_0_0, &t5842_1_0_0, NULL, &t5842_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4546_TI;

#include "t626.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UInt16>
extern MethodInfo m30546_MI;
static PropertyInfo t4546____Current_PropertyInfo = 
{
	&t4546_TI, "Current", &m30546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4546_PIs[] =
{
	&t4546____Current_PropertyInfo,
	NULL
};
extern Il2CppType t626_0_0_0;
extern void* RuntimeInvoker_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30546_GM;
MethodInfo m30546_MI = 
{
	"get_Current", NULL, &t4546_TI, &t626_0_0_0, RuntimeInvoker_t626, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30546_GM};
static MethodInfo* t4546_MIs[] =
{
	&m30546_MI,
	NULL
};
static TypeInfo* t4546_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4546_0_0_0;
extern Il2CppType t4546_1_0_0;
struct t4546;
extern Il2CppGenericClass t4546_GC;
TypeInfo t4546_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4546_MIs, t4546_PIs, NULL, NULL, NULL, NULL, NULL, &t4546_TI, t4546_ITIs, NULL, &EmptyCustomAttributesCache, &t4546_TI, &t4546_0_0_0, &t4546_1_0_0, NULL, &t4546_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3185.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3185_TI;
#include "t3185MD.h"

extern TypeInfo t626_TI;
extern MethodInfo m17692_MI;
extern MethodInfo m23302_MI;
struct t20;
 uint16_t m23302 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17688_MI;
 void m17688 (t3185 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17689_MI;
 t29 * m17689 (t3185 * __this, MethodInfo* method){
	{
		uint16_t L_0 = m17692(__this, &m17692_MI);
		uint16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t626_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17690_MI;
 void m17690 (t3185 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17691_MI;
 bool m17691 (t3185 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint16_t m17692 (t3185 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m23302(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23302_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UInt16>
extern Il2CppType t20_0_0_1;
FieldInfo t3185_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3185_TI, offsetof(t3185, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3185_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3185_TI, offsetof(t3185, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3185_FIs[] =
{
	&t3185_f0_FieldInfo,
	&t3185_f1_FieldInfo,
	NULL
};
static PropertyInfo t3185____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3185_TI, "System.Collections.IEnumerator.Current", &m17689_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3185____Current_PropertyInfo = 
{
	&t3185_TI, "Current", &m17692_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3185_PIs[] =
{
	&t3185____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3185____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3185_m17688_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17688_GM;
MethodInfo m17688_MI = 
{
	".ctor", (methodPointerType)&m17688, &t3185_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3185_m17688_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17688_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17689_GM;
MethodInfo m17689_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17689, &t3185_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17689_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17690_GM;
MethodInfo m17690_MI = 
{
	"Dispose", (methodPointerType)&m17690, &t3185_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17690_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17691_GM;
MethodInfo m17691_MI = 
{
	"MoveNext", (methodPointerType)&m17691, &t3185_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17691_GM};
extern Il2CppType t626_0_0_0;
extern void* RuntimeInvoker_t626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17692_GM;
MethodInfo m17692_MI = 
{
	"get_Current", (methodPointerType)&m17692, &t3185_TI, &t626_0_0_0, RuntimeInvoker_t626, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17692_GM};
static MethodInfo* t3185_MIs[] =
{
	&m17688_MI,
	&m17689_MI,
	&m17690_MI,
	&m17691_MI,
	&m17692_MI,
	NULL
};
static MethodInfo* t3185_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17689_MI,
	&m17691_MI,
	&m17690_MI,
	&m17692_MI,
};
static TypeInfo* t3185_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4546_TI,
};
static Il2CppInterfaceOffsetPair t3185_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4546_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3185_0_0_0;
extern Il2CppType t3185_1_0_0;
extern Il2CppGenericClass t3185_GC;
TypeInfo t3185_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3185_MIs, t3185_PIs, t3185_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3185_TI, t3185_ITIs, t3185_VT, &EmptyCustomAttributesCache, &t3185_TI, &t3185_0_0_0, &t3185_1_0_0, t3185_IOs, &t3185_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3185)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5844_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UInt16>
extern MethodInfo m30547_MI;
static PropertyInfo t5844____Count_PropertyInfo = 
{
	&t5844_TI, "Count", &m30547_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30548_MI;
static PropertyInfo t5844____IsReadOnly_PropertyInfo = 
{
	&t5844_TI, "IsReadOnly", &m30548_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5844_PIs[] =
{
	&t5844____Count_PropertyInfo,
	&t5844____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30547_GM;
MethodInfo m30547_MI = 
{
	"get_Count", NULL, &t5844_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30547_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30548_GM;
MethodInfo m30548_MI = 
{
	"get_IsReadOnly", NULL, &t5844_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30548_GM};
extern Il2CppType t626_0_0_0;
extern Il2CppType t626_0_0_0;
static ParameterInfo t5844_m30549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30549_GM;
MethodInfo m30549_MI = 
{
	"Add", NULL, &t5844_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t5844_m30549_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30549_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30550_GM;
MethodInfo m30550_MI = 
{
	"Clear", NULL, &t5844_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30550_GM};
extern Il2CppType t626_0_0_0;
static ParameterInfo t5844_m30551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30551_GM;
MethodInfo m30551_MI = 
{
	"Contains", NULL, &t5844_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t5844_m30551_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30551_GM};
extern Il2CppType t764_0_0_0;
extern Il2CppType t764_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5844_m30552_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t764_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30552_GM;
MethodInfo m30552_MI = 
{
	"CopyTo", NULL, &t5844_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5844_m30552_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30552_GM};
extern Il2CppType t626_0_0_0;
static ParameterInfo t5844_m30553_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30553_GM;
MethodInfo m30553_MI = 
{
	"Remove", NULL, &t5844_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t5844_m30553_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30553_GM};
static MethodInfo* t5844_MIs[] =
{
	&m30547_MI,
	&m30548_MI,
	&m30549_MI,
	&m30550_MI,
	&m30551_MI,
	&m30552_MI,
	&m30553_MI,
	NULL
};
extern TypeInfo t5846_TI;
static TypeInfo* t5844_ITIs[] = 
{
	&t603_TI,
	&t5846_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5844_0_0_0;
extern Il2CppType t5844_1_0_0;
struct t5844;
extern Il2CppGenericClass t5844_GC;
TypeInfo t5844_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5844_MIs, t5844_PIs, NULL, NULL, NULL, NULL, NULL, &t5844_TI, t5844_ITIs, NULL, &EmptyCustomAttributesCache, &t5844_TI, &t5844_0_0_0, &t5844_1_0_0, NULL, &t5844_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UInt16>
extern Il2CppType t4546_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30554_GM;
MethodInfo m30554_MI = 
{
	"GetEnumerator", NULL, &t5846_TI, &t4546_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30554_GM};
static MethodInfo* t5846_MIs[] =
{
	&m30554_MI,
	NULL
};
static TypeInfo* t5846_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5846_0_0_0;
extern Il2CppType t5846_1_0_0;
struct t5846;
extern Il2CppGenericClass t5846_GC;
TypeInfo t5846_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5846_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5846_TI, t5846_ITIs, NULL, &EmptyCustomAttributesCache, &t5846_TI, &t5846_0_0_0, &t5846_1_0_0, NULL, &t5846_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5845_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UInt16>
extern MethodInfo m30555_MI;
extern MethodInfo m30556_MI;
static PropertyInfo t5845____Item_PropertyInfo = 
{
	&t5845_TI, "Item", &m30555_MI, &m30556_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5845_PIs[] =
{
	&t5845____Item_PropertyInfo,
	NULL
};
extern Il2CppType t626_0_0_0;
static ParameterInfo t5845_m30557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30557_GM;
MethodInfo m30557_MI = 
{
	"IndexOf", NULL, &t5845_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t5845_m30557_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30557_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t626_0_0_0;
static ParameterInfo t5845_m30558_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30558_GM;
MethodInfo m30558_MI = 
{
	"Insert", NULL, &t5845_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t372, t5845_m30558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30558_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5845_m30559_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30559_GM;
MethodInfo m30559_MI = 
{
	"RemoveAt", NULL, &t5845_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5845_m30559_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30559_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5845_m30555_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t626_0_0_0;
extern void* RuntimeInvoker_t626_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30555_GM;
MethodInfo m30555_MI = 
{
	"get_Item", NULL, &t5845_TI, &t626_0_0_0, RuntimeInvoker_t626_t44, t5845_m30555_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30555_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t626_0_0_0;
static ParameterInfo t5845_m30556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30556_GM;
MethodInfo m30556_MI = 
{
	"set_Item", NULL, &t5845_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t372, t5845_m30556_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30556_GM};
static MethodInfo* t5845_MIs[] =
{
	&m30557_MI,
	&m30558_MI,
	&m30559_MI,
	&m30555_MI,
	&m30556_MI,
	NULL
};
static TypeInfo* t5845_ITIs[] = 
{
	&t603_TI,
	&t5844_TI,
	&t5846_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5845_0_0_0;
extern Il2CppType t5845_1_0_0;
struct t5845;
extern Il2CppGenericClass t5845_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5845_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5845_MIs, t5845_PIs, NULL, NULL, NULL, NULL, NULL, &t5845_TI, t5845_ITIs, NULL, &t1908__CustomAttributeCache, &t5845_TI, &t5845_0_0_0, &t5845_1_0_0, NULL, &t5845_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5847_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.UInt16>>
extern MethodInfo m30560_MI;
static PropertyInfo t5847____Count_PropertyInfo = 
{
	&t5847_TI, "Count", &m30560_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30561_MI;
static PropertyInfo t5847____IsReadOnly_PropertyInfo = 
{
	&t5847_TI, "IsReadOnly", &m30561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5847_PIs[] =
{
	&t5847____Count_PropertyInfo,
	&t5847____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30560_GM;
MethodInfo m30560_MI = 
{
	"get_Count", NULL, &t5847_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30560_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30561_GM;
MethodInfo m30561_MI = 
{
	"get_IsReadOnly", NULL, &t5847_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30561_GM};
extern Il2CppType t1730_0_0_0;
extern Il2CppType t1730_0_0_0;
static ParameterInfo t5847_m30562_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1730_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30562_GM;
MethodInfo m30562_MI = 
{
	"Add", NULL, &t5847_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5847_m30562_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30562_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30563_GM;
MethodInfo m30563_MI = 
{
	"Clear", NULL, &t5847_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30563_GM};
extern Il2CppType t1730_0_0_0;
static ParameterInfo t5847_m30564_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1730_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30564_GM;
MethodInfo m30564_MI = 
{
	"Contains", NULL, &t5847_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5847_m30564_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30564_GM};
extern Il2CppType t3551_0_0_0;
extern Il2CppType t3551_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5847_m30565_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3551_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30565_GM;
MethodInfo m30565_MI = 
{
	"CopyTo", NULL, &t5847_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5847_m30565_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30565_GM};
extern Il2CppType t1730_0_0_0;
static ParameterInfo t5847_m30566_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1730_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30566_GM;
MethodInfo m30566_MI = 
{
	"Remove", NULL, &t5847_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5847_m30566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30566_GM};
static MethodInfo* t5847_MIs[] =
{
	&m30560_MI,
	&m30561_MI,
	&m30562_MI,
	&m30563_MI,
	&m30564_MI,
	&m30565_MI,
	&m30566_MI,
	NULL
};
extern TypeInfo t5849_TI;
static TypeInfo* t5847_ITIs[] = 
{
	&t603_TI,
	&t5849_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5847_0_0_0;
extern Il2CppType t5847_1_0_0;
struct t5847;
extern Il2CppGenericClass t5847_GC;
TypeInfo t5847_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5847_MIs, t5847_PIs, NULL, NULL, NULL, NULL, NULL, &t5847_TI, t5847_ITIs, NULL, &EmptyCustomAttributesCache, &t5847_TI, &t5847_0_0_0, &t5847_1_0_0, NULL, &t5847_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.UInt16>>
extern Il2CppType t4548_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30567_GM;
MethodInfo m30567_MI = 
{
	"GetEnumerator", NULL, &t5849_TI, &t4548_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30567_GM};
static MethodInfo* t5849_MIs[] =
{
	&m30567_MI,
	NULL
};
static TypeInfo* t5849_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5849_0_0_0;
extern Il2CppType t5849_1_0_0;
struct t5849;
extern Il2CppGenericClass t5849_GC;
TypeInfo t5849_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5849_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5849_TI, t5849_ITIs, NULL, &EmptyCustomAttributesCache, &t5849_TI, &t5849_0_0_0, &t5849_1_0_0, NULL, &t5849_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4548_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.UInt16>>
extern MethodInfo m30568_MI;
static PropertyInfo t4548____Current_PropertyInfo = 
{
	&t4548_TI, "Current", &m30568_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4548_PIs[] =
{
	&t4548____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1730_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30568_GM;
MethodInfo m30568_MI = 
{
	"get_Current", NULL, &t4548_TI, &t1730_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30568_GM};
static MethodInfo* t4548_MIs[] =
{
	&m30568_MI,
	NULL
};
static TypeInfo* t4548_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4548_0_0_0;
extern Il2CppType t4548_1_0_0;
struct t4548;
extern Il2CppGenericClass t4548_GC;
TypeInfo t4548_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4548_MIs, t4548_PIs, NULL, NULL, NULL, NULL, NULL, &t4548_TI, t4548_ITIs, NULL, &EmptyCustomAttributesCache, &t4548_TI, &t4548_0_0_0, &t4548_1_0_0, NULL, &t4548_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1730_TI;



// Metadata Definition System.IComparable`1<System.UInt16>
extern Il2CppType t626_0_0_0;
static ParameterInfo t1730_m30569_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30569_GM;
MethodInfo m30569_MI = 
{
	"CompareTo", NULL, &t1730_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t1730_m30569_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30569_GM};
static MethodInfo* t1730_MIs[] =
{
	&m30569_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1730_1_0_0;
struct t1730;
extern Il2CppGenericClass t1730_GC;
TypeInfo t1730_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1730_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1730_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1730_TI, &t1730_0_0_0, &t1730_1_0_0, NULL, &t1730_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3186.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3186_TI;
#include "t3186MD.h"

extern MethodInfo m17697_MI;
extern MethodInfo m23313_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m23313(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.UInt16>>
extern Il2CppType t20_0_0_1;
FieldInfo t3186_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3186_TI, offsetof(t3186, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3186_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3186_TI, offsetof(t3186, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3186_FIs[] =
{
	&t3186_f0_FieldInfo,
	&t3186_f1_FieldInfo,
	NULL
};
extern MethodInfo m17694_MI;
static PropertyInfo t3186____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3186_TI, "System.Collections.IEnumerator.Current", &m17694_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3186____Current_PropertyInfo = 
{
	&t3186_TI, "Current", &m17697_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3186_PIs[] =
{
	&t3186____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3186____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3186_m17693_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17693_GM;
MethodInfo m17693_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3186_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3186_m17693_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17693_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17694_GM;
MethodInfo m17694_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3186_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17694_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17695_GM;
MethodInfo m17695_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3186_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17695_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17696_GM;
MethodInfo m17696_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3186_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17696_GM};
extern Il2CppType t1730_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17697_GM;
MethodInfo m17697_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3186_TI, &t1730_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17697_GM};
static MethodInfo* t3186_MIs[] =
{
	&m17693_MI,
	&m17694_MI,
	&m17695_MI,
	&m17696_MI,
	&m17697_MI,
	NULL
};
extern MethodInfo m17696_MI;
extern MethodInfo m17695_MI;
static MethodInfo* t3186_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17694_MI,
	&m17696_MI,
	&m17695_MI,
	&m17697_MI,
};
static TypeInfo* t3186_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4548_TI,
};
static Il2CppInterfaceOffsetPair t3186_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4548_TI, 7},
};
extern TypeInfo t1730_TI;
static Il2CppRGCTXData t3186_RGCTXData[3] = 
{
	&m17697_MI/* Method Usage */,
	&t1730_TI/* Class Usage */,
	&m23313_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3186_0_0_0;
extern Il2CppType t3186_1_0_0;
extern Il2CppGenericClass t3186_GC;
TypeInfo t3186_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3186_MIs, t3186_PIs, t3186_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3186_TI, t3186_ITIs, t3186_VT, &EmptyCustomAttributesCache, &t3186_TI, &t3186_0_0_0, &t3186_1_0_0, t3186_IOs, &t3186_GC, NULL, NULL, NULL, t3186_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3186)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5848_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.UInt16>>
extern MethodInfo m30570_MI;
extern MethodInfo m30571_MI;
static PropertyInfo t5848____Item_PropertyInfo = 
{
	&t5848_TI, "Item", &m30570_MI, &m30571_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5848_PIs[] =
{
	&t5848____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1730_0_0_0;
static ParameterInfo t5848_m30572_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1730_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30572_GM;
MethodInfo m30572_MI = 
{
	"IndexOf", NULL, &t5848_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5848_m30572_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30572_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1730_0_0_0;
static ParameterInfo t5848_m30573_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1730_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30573_GM;
MethodInfo m30573_MI = 
{
	"Insert", NULL, &t5848_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5848_m30573_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30573_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5848_m30574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30574_GM;
MethodInfo m30574_MI = 
{
	"RemoveAt", NULL, &t5848_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5848_m30574_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30574_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5848_m30570_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1730_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30570_GM;
MethodInfo m30570_MI = 
{
	"get_Item", NULL, &t5848_TI, &t1730_0_0_0, RuntimeInvoker_t29_t44, t5848_m30570_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30570_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1730_0_0_0;
static ParameterInfo t5848_m30571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1730_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30571_GM;
MethodInfo m30571_MI = 
{
	"set_Item", NULL, &t5848_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5848_m30571_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30571_GM};
static MethodInfo* t5848_MIs[] =
{
	&m30572_MI,
	&m30573_MI,
	&m30574_MI,
	&m30570_MI,
	&m30571_MI,
	NULL
};
static TypeInfo* t5848_ITIs[] = 
{
	&t603_TI,
	&t5847_TI,
	&t5849_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5848_0_0_0;
extern Il2CppType t5848_1_0_0;
struct t5848;
extern Il2CppGenericClass t5848_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5848_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5848_MIs, t5848_PIs, NULL, NULL, NULL, NULL, NULL, &t5848_TI, t5848_ITIs, NULL, &t1908__CustomAttributeCache, &t5848_TI, &t5848_0_0_0, &t5848_1_0_0, NULL, &t5848_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5850_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.UInt16>>
extern MethodInfo m30575_MI;
static PropertyInfo t5850____Count_PropertyInfo = 
{
	&t5850_TI, "Count", &m30575_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30576_MI;
static PropertyInfo t5850____IsReadOnly_PropertyInfo = 
{
	&t5850_TI, "IsReadOnly", &m30576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5850_PIs[] =
{
	&t5850____Count_PropertyInfo,
	&t5850____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30575_GM;
MethodInfo m30575_MI = 
{
	"get_Count", NULL, &t5850_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30575_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30576_GM;
MethodInfo m30576_MI = 
{
	"get_IsReadOnly", NULL, &t5850_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30576_GM};
extern Il2CppType t1731_0_0_0;
extern Il2CppType t1731_0_0_0;
static ParameterInfo t5850_m30577_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1731_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30577_GM;
MethodInfo m30577_MI = 
{
	"Add", NULL, &t5850_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5850_m30577_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30577_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30578_GM;
MethodInfo m30578_MI = 
{
	"Clear", NULL, &t5850_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30578_GM};
extern Il2CppType t1731_0_0_0;
static ParameterInfo t5850_m30579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1731_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30579_GM;
MethodInfo m30579_MI = 
{
	"Contains", NULL, &t5850_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5850_m30579_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30579_GM};
extern Il2CppType t3552_0_0_0;
extern Il2CppType t3552_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5850_m30580_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3552_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30580_GM;
MethodInfo m30580_MI = 
{
	"CopyTo", NULL, &t5850_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5850_m30580_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30580_GM};
extern Il2CppType t1731_0_0_0;
static ParameterInfo t5850_m30581_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1731_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30581_GM;
MethodInfo m30581_MI = 
{
	"Remove", NULL, &t5850_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5850_m30581_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30581_GM};
static MethodInfo* t5850_MIs[] =
{
	&m30575_MI,
	&m30576_MI,
	&m30577_MI,
	&m30578_MI,
	&m30579_MI,
	&m30580_MI,
	&m30581_MI,
	NULL
};
extern TypeInfo t5852_TI;
static TypeInfo* t5850_ITIs[] = 
{
	&t603_TI,
	&t5852_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5850_0_0_0;
extern Il2CppType t5850_1_0_0;
struct t5850;
extern Il2CppGenericClass t5850_GC;
TypeInfo t5850_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5850_MIs, t5850_PIs, NULL, NULL, NULL, NULL, NULL, &t5850_TI, t5850_ITIs, NULL, &EmptyCustomAttributesCache, &t5850_TI, &t5850_0_0_0, &t5850_1_0_0, NULL, &t5850_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.UInt16>>
extern Il2CppType t4550_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30582_GM;
MethodInfo m30582_MI = 
{
	"GetEnumerator", NULL, &t5852_TI, &t4550_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30582_GM};
static MethodInfo* t5852_MIs[] =
{
	&m30582_MI,
	NULL
};
static TypeInfo* t5852_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5852_0_0_0;
extern Il2CppType t5852_1_0_0;
struct t5852;
extern Il2CppGenericClass t5852_GC;
TypeInfo t5852_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5852_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5852_TI, t5852_ITIs, NULL, &EmptyCustomAttributesCache, &t5852_TI, &t5852_0_0_0, &t5852_1_0_0, NULL, &t5852_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4550_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.UInt16>>
extern MethodInfo m30583_MI;
static PropertyInfo t4550____Current_PropertyInfo = 
{
	&t4550_TI, "Current", &m30583_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4550_PIs[] =
{
	&t4550____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1731_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30583_GM;
MethodInfo m30583_MI = 
{
	"get_Current", NULL, &t4550_TI, &t1731_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30583_GM};
static MethodInfo* t4550_MIs[] =
{
	&m30583_MI,
	NULL
};
static TypeInfo* t4550_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4550_0_0_0;
extern Il2CppType t4550_1_0_0;
struct t4550;
extern Il2CppGenericClass t4550_GC;
TypeInfo t4550_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4550_MIs, t4550_PIs, NULL, NULL, NULL, NULL, NULL, &t4550_TI, t4550_ITIs, NULL, &EmptyCustomAttributesCache, &t4550_TI, &t4550_0_0_0, &t4550_1_0_0, NULL, &t4550_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1731_TI;



// Metadata Definition System.IEquatable`1<System.UInt16>
extern Il2CppType t626_0_0_0;
static ParameterInfo t1731_m30584_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30584_GM;
MethodInfo m30584_MI = 
{
	"Equals", NULL, &t1731_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t1731_m30584_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30584_GM};
static MethodInfo* t1731_MIs[] =
{
	&m30584_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1731_1_0_0;
struct t1731;
extern Il2CppGenericClass t1731_GC;
TypeInfo t1731_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1731_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1731_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1731_TI, &t1731_0_0_0, &t1731_1_0_0, NULL, &t1731_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3187.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3187_TI;
#include "t3187MD.h"

extern MethodInfo m17702_MI;
extern MethodInfo m23324_MI;
struct t20;
#define m23324(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.UInt16>>
extern Il2CppType t20_0_0_1;
FieldInfo t3187_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3187_TI, offsetof(t3187, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3187_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3187_TI, offsetof(t3187, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3187_FIs[] =
{
	&t3187_f0_FieldInfo,
	&t3187_f1_FieldInfo,
	NULL
};
extern MethodInfo m17699_MI;
static PropertyInfo t3187____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3187_TI, "System.Collections.IEnumerator.Current", &m17699_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3187____Current_PropertyInfo = 
{
	&t3187_TI, "Current", &m17702_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3187_PIs[] =
{
	&t3187____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3187____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3187_m17698_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17698_GM;
MethodInfo m17698_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3187_m17698_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17698_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17699_GM;
MethodInfo m17699_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3187_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17699_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17700_GM;
MethodInfo m17700_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3187_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17700_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17701_GM;
MethodInfo m17701_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3187_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17701_GM};
extern Il2CppType t1731_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17702_GM;
MethodInfo m17702_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3187_TI, &t1731_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17702_GM};
static MethodInfo* t3187_MIs[] =
{
	&m17698_MI,
	&m17699_MI,
	&m17700_MI,
	&m17701_MI,
	&m17702_MI,
	NULL
};
extern MethodInfo m17701_MI;
extern MethodInfo m17700_MI;
static MethodInfo* t3187_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17699_MI,
	&m17701_MI,
	&m17700_MI,
	&m17702_MI,
};
static TypeInfo* t3187_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4550_TI,
};
static Il2CppInterfaceOffsetPair t3187_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4550_TI, 7},
};
extern TypeInfo t1731_TI;
static Il2CppRGCTXData t3187_RGCTXData[3] = 
{
	&m17702_MI/* Method Usage */,
	&t1731_TI/* Class Usage */,
	&m23324_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3187_0_0_0;
extern Il2CppType t3187_1_0_0;
extern Il2CppGenericClass t3187_GC;
TypeInfo t3187_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3187_MIs, t3187_PIs, t3187_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3187_TI, t3187_ITIs, t3187_VT, &EmptyCustomAttributesCache, &t3187_TI, &t3187_0_0_0, &t3187_1_0_0, t3187_IOs, &t3187_GC, NULL, NULL, NULL, t3187_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3187)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5851_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.UInt16>>
extern MethodInfo m30585_MI;
extern MethodInfo m30586_MI;
static PropertyInfo t5851____Item_PropertyInfo = 
{
	&t5851_TI, "Item", &m30585_MI, &m30586_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5851_PIs[] =
{
	&t5851____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1731_0_0_0;
static ParameterInfo t5851_m30587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1731_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30587_GM;
MethodInfo m30587_MI = 
{
	"IndexOf", NULL, &t5851_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5851_m30587_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30587_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1731_0_0_0;
static ParameterInfo t5851_m30588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1731_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30588_GM;
MethodInfo m30588_MI = 
{
	"Insert", NULL, &t5851_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5851_m30588_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30588_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5851_m30589_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30589_GM;
MethodInfo m30589_MI = 
{
	"RemoveAt", NULL, &t5851_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5851_m30589_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30589_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5851_m30585_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1731_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30585_GM;
MethodInfo m30585_MI = 
{
	"get_Item", NULL, &t5851_TI, &t1731_0_0_0, RuntimeInvoker_t29_t44, t5851_m30585_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30585_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1731_0_0_0;
static ParameterInfo t5851_m30586_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1731_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30586_GM;
MethodInfo m30586_MI = 
{
	"set_Item", NULL, &t5851_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5851_m30586_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30586_GM};
static MethodInfo* t5851_MIs[] =
{
	&m30587_MI,
	&m30588_MI,
	&m30589_MI,
	&m30585_MI,
	&m30586_MI,
	NULL
};
static TypeInfo* t5851_ITIs[] = 
{
	&t603_TI,
	&t5850_TI,
	&t5852_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5851_0_0_0;
extern Il2CppType t5851_1_0_0;
struct t5851;
extern Il2CppGenericClass t5851_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5851_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5851_MIs, t5851_PIs, NULL, NULL, NULL, NULL, NULL, &t5851_TI, t5851_ITIs, NULL, &t1908__CustomAttributeCache, &t5851_TI, &t5851_0_0_0, &t5851_1_0_0, NULL, &t5851_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4552_TI;

#include "t766.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.SecurityProtocolType>
extern MethodInfo m30590_MI;
static PropertyInfo t4552____Current_PropertyInfo = 
{
	&t4552_TI, "Current", &m30590_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4552_PIs[] =
{
	&t4552____Current_PropertyInfo,
	NULL
};
extern Il2CppType t766_0_0_0;
extern void* RuntimeInvoker_t766 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30590_GM;
MethodInfo m30590_MI = 
{
	"get_Current", NULL, &t4552_TI, &t766_0_0_0, RuntimeInvoker_t766, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30590_GM};
static MethodInfo* t4552_MIs[] =
{
	&m30590_MI,
	NULL
};
static TypeInfo* t4552_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4552_0_0_0;
extern Il2CppType t4552_1_0_0;
struct t4552;
extern Il2CppGenericClass t4552_GC;
TypeInfo t4552_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4552_MIs, t4552_PIs, NULL, NULL, NULL, NULL, NULL, &t4552_TI, t4552_ITIs, NULL, &EmptyCustomAttributesCache, &t4552_TI, &t4552_0_0_0, &t4552_1_0_0, NULL, &t4552_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3188.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3188_TI;
#include "t3188MD.h"

extern TypeInfo t766_TI;
extern MethodInfo m17707_MI;
extern MethodInfo m23335_MI;
struct t20;
 int32_t m23335 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17703_MI;
 void m17703 (t3188 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17704_MI;
 t29 * m17704 (t3188 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17707(__this, &m17707_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t766_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17705_MI;
 void m17705 (t3188 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17706_MI;
 bool m17706 (t3188 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17707 (t3188 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23335(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23335_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>
extern Il2CppType t20_0_0_1;
FieldInfo t3188_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3188_TI, offsetof(t3188, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3188_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3188_TI, offsetof(t3188, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3188_FIs[] =
{
	&t3188_f0_FieldInfo,
	&t3188_f1_FieldInfo,
	NULL
};
static PropertyInfo t3188____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3188_TI, "System.Collections.IEnumerator.Current", &m17704_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3188____Current_PropertyInfo = 
{
	&t3188_TI, "Current", &m17707_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3188_PIs[] =
{
	&t3188____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3188____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3188_m17703_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17703_GM;
MethodInfo m17703_MI = 
{
	".ctor", (methodPointerType)&m17703, &t3188_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3188_m17703_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17703_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17704_GM;
MethodInfo m17704_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17704, &t3188_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17704_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17705_GM;
MethodInfo m17705_MI = 
{
	"Dispose", (methodPointerType)&m17705, &t3188_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17705_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17706_GM;
MethodInfo m17706_MI = 
{
	"MoveNext", (methodPointerType)&m17706, &t3188_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17706_GM};
extern Il2CppType t766_0_0_0;
extern void* RuntimeInvoker_t766 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17707_GM;
MethodInfo m17707_MI = 
{
	"get_Current", (methodPointerType)&m17707, &t3188_TI, &t766_0_0_0, RuntimeInvoker_t766, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17707_GM};
static MethodInfo* t3188_MIs[] =
{
	&m17703_MI,
	&m17704_MI,
	&m17705_MI,
	&m17706_MI,
	&m17707_MI,
	NULL
};
static MethodInfo* t3188_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17704_MI,
	&m17706_MI,
	&m17705_MI,
	&m17707_MI,
};
static TypeInfo* t3188_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4552_TI,
};
static Il2CppInterfaceOffsetPair t3188_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4552_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3188_0_0_0;
extern Il2CppType t3188_1_0_0;
extern Il2CppGenericClass t3188_GC;
TypeInfo t3188_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3188_MIs, t3188_PIs, t3188_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3188_TI, t3188_ITIs, t3188_VT, &EmptyCustomAttributesCache, &t3188_TI, &t3188_0_0_0, &t3188_1_0_0, t3188_IOs, &t3188_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3188)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5853_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.SecurityProtocolType>
extern MethodInfo m30591_MI;
static PropertyInfo t5853____Count_PropertyInfo = 
{
	&t5853_TI, "Count", &m30591_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30592_MI;
static PropertyInfo t5853____IsReadOnly_PropertyInfo = 
{
	&t5853_TI, "IsReadOnly", &m30592_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5853_PIs[] =
{
	&t5853____Count_PropertyInfo,
	&t5853____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30591_GM;
MethodInfo m30591_MI = 
{
	"get_Count", NULL, &t5853_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30591_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30592_GM;
MethodInfo m30592_MI = 
{
	"get_IsReadOnly", NULL, &t5853_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30592_GM};
extern Il2CppType t766_0_0_0;
extern Il2CppType t766_0_0_0;
static ParameterInfo t5853_m30593_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t766_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30593_GM;
MethodInfo m30593_MI = 
{
	"Add", NULL, &t5853_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5853_m30593_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30593_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30594_GM;
MethodInfo m30594_MI = 
{
	"Clear", NULL, &t5853_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30594_GM};
extern Il2CppType t766_0_0_0;
static ParameterInfo t5853_m30595_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t766_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30595_GM;
MethodInfo m30595_MI = 
{
	"Contains", NULL, &t5853_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5853_m30595_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30595_GM};
extern Il2CppType t3903_0_0_0;
extern Il2CppType t3903_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5853_m30596_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3903_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30596_GM;
MethodInfo m30596_MI = 
{
	"CopyTo", NULL, &t5853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5853_m30596_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30596_GM};
extern Il2CppType t766_0_0_0;
static ParameterInfo t5853_m30597_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t766_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30597_GM;
MethodInfo m30597_MI = 
{
	"Remove", NULL, &t5853_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5853_m30597_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30597_GM};
static MethodInfo* t5853_MIs[] =
{
	&m30591_MI,
	&m30592_MI,
	&m30593_MI,
	&m30594_MI,
	&m30595_MI,
	&m30596_MI,
	&m30597_MI,
	NULL
};
extern TypeInfo t5855_TI;
static TypeInfo* t5853_ITIs[] = 
{
	&t603_TI,
	&t5855_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5853_0_0_0;
extern Il2CppType t5853_1_0_0;
struct t5853;
extern Il2CppGenericClass t5853_GC;
TypeInfo t5853_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5853_MIs, t5853_PIs, NULL, NULL, NULL, NULL, NULL, &t5853_TI, t5853_ITIs, NULL, &EmptyCustomAttributesCache, &t5853_TI, &t5853_0_0_0, &t5853_1_0_0, NULL, &t5853_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.SecurityProtocolType>
extern Il2CppType t4552_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30598_GM;
MethodInfo m30598_MI = 
{
	"GetEnumerator", NULL, &t5855_TI, &t4552_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30598_GM};
static MethodInfo* t5855_MIs[] =
{
	&m30598_MI,
	NULL
};
static TypeInfo* t5855_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5855_0_0_0;
extern Il2CppType t5855_1_0_0;
struct t5855;
extern Il2CppGenericClass t5855_GC;
TypeInfo t5855_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5855_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5855_TI, t5855_ITIs, NULL, &EmptyCustomAttributesCache, &t5855_TI, &t5855_0_0_0, &t5855_1_0_0, NULL, &t5855_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5854_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Net.SecurityProtocolType>
extern MethodInfo m30599_MI;
extern MethodInfo m30600_MI;
static PropertyInfo t5854____Item_PropertyInfo = 
{
	&t5854_TI, "Item", &m30599_MI, &m30600_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5854_PIs[] =
{
	&t5854____Item_PropertyInfo,
	NULL
};
extern Il2CppType t766_0_0_0;
static ParameterInfo t5854_m30601_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t766_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30601_GM;
MethodInfo m30601_MI = 
{
	"IndexOf", NULL, &t5854_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5854_m30601_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30601_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t766_0_0_0;
static ParameterInfo t5854_m30602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t766_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30602_GM;
MethodInfo m30602_MI = 
{
	"Insert", NULL, &t5854_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5854_m30602_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30602_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5854_m30603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30603_GM;
MethodInfo m30603_MI = 
{
	"RemoveAt", NULL, &t5854_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5854_m30603_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30603_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5854_m30599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t766_0_0_0;
extern void* RuntimeInvoker_t766_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30599_GM;
MethodInfo m30599_MI = 
{
	"get_Item", NULL, &t5854_TI, &t766_0_0_0, RuntimeInvoker_t766_t44, t5854_m30599_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30599_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t766_0_0_0;
static ParameterInfo t5854_m30600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t766_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30600_GM;
MethodInfo m30600_MI = 
{
	"set_Item", NULL, &t5854_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5854_m30600_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30600_GM};
static MethodInfo* t5854_MIs[] =
{
	&m30601_MI,
	&m30602_MI,
	&m30603_MI,
	&m30599_MI,
	&m30600_MI,
	NULL
};
static TypeInfo* t5854_ITIs[] = 
{
	&t603_TI,
	&t5853_TI,
	&t5855_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5854_0_0_0;
extern Il2CppType t5854_1_0_0;
struct t5854;
extern Il2CppGenericClass t5854_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5854_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5854_MIs, t5854_PIs, NULL, NULL, NULL, NULL, NULL, &t5854_TI, t5854_ITIs, NULL, &t1908__CustomAttributeCache, &t5854_TI, &t5854_0_0_0, &t5854_1_0_0, NULL, &t5854_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t770.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t770_TI;
#include "t770MD.h"

#include "t338.h"
#include "t1248.h"
#include "t1258.h"
#include "t3190.h"
#include "t733.h"
#include "t735.h"
#include "t3192.h"
#include "t725.h"
#include "t3189.h"
#include "t35.h"
#include "t3199.h"
#include "t3194.h"
#include "t3200.h"
#include "t2901.h"
#include "t305.h"
#include "t3201.h"
#include "t42.h"
#include "t43.h"
extern TypeInfo t7_TI;
extern TypeInfo t40_TI;
extern TypeInfo t21_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2096_TI;
extern TypeInfo t44_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t3190_TI;
extern TypeInfo t3191_TI;
extern TypeInfo t3192_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t725_TI;
extern TypeInfo t3189_TI;
extern TypeInfo t3199_TI;
extern TypeInfo t3194_TI;
extern TypeInfo t3200_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2901_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t446_TI;
extern TypeInfo t771_TI;
extern TypeInfo t305_TI;
extern TypeInfo t719_TI;
extern TypeInfo t3201_TI;
extern TypeInfo t6755_TI;
extern TypeInfo t42_TI;
#include "t338MD.h"
#include "t1258MD.h"
#include "t3190MD.h"
#include "t29MD.h"
#include "t3192MD.h"
#include "t3189MD.h"
#include "t3199MD.h"
#include "t3194MD.h"
#include "t3200MD.h"
#include "t915MD.h"
#include "t2901MD.h"
#include "t305MD.h"
#include "t719MD.h"
#include "t3201MD.h"
#include "t733MD.h"
#include "t42MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t2096_0_0_0;
extern Il2CppType t3191_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern MethodInfo m17737_MI;
extern MethodInfo m17744_MI;
extern MethodInfo m17727_MI;
extern MethodInfo m17745_MI;
extern MethodInfo m17728_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m29233_MI;
extern MethodInfo m29234_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m17735_MI;
extern MethodInfo m17760_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m17729_MI;
extern MethodInfo m4029_MI;
extern MethodInfo m17741_MI;
extern MethodInfo m17750_MI;
extern MethodInfo m17752_MI;
extern MethodInfo m17746_MI;
extern MethodInfo m17734_MI;
extern MethodInfo m17731_MI;
extern MethodInfo m17748_MI;
extern MethodInfo m17795_MI;
extern MethodInfo m23359_MI;
extern MethodInfo m17732_MI;
extern MethodInfo m17799_MI;
extern MethodInfo m23361_MI;
extern MethodInfo m17779_MI;
extern MethodInfo m17803_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m15881_MI;
extern MethodInfo m17730_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m17726_MI;
extern MethodInfo m17749_MI;
extern MethodInfo m23362_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m17813_MI;
extern MethodInfo m30604_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m17742_MI;
extern MethodInfo m30605_MI;
extern MethodInfo m3965_MI;
struct t770;
 void m23359 (t770 * __this, t3541* p0, int32_t p1, t3189 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t770;
#include "t295.h"
 void m23361 (t770 * __this, t20 * p0, int32_t p1, t3199 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t770;
 void m23362 (t770 * __this, t3191* p0, int32_t p1, t3199 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17708_MI;
 void m17708 (t770 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m17729(__this, ((int32_t)10), (t29*)NULL, &m17729_MI);
		return;
	}
}
extern MethodInfo m4028_MI;
 void m4028 (t770 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m17729(__this, ((int32_t)10), p0, &m17729_MI);
		return;
	}
}
extern MethodInfo m17709_MI;
 void m17709 (t770 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m17729(__this, p0, (t29*)NULL, &m17729_MI);
		return;
	}
}
extern MethodInfo m17710_MI;
 void m17710 (t770 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m17711_MI;
 t29 * m17711 (t770 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t7*)IsInst(p0, (&t7_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m17737_MI, __this, ((t7*)Castclass(p0, (&t7_TI))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t7* L_1 = m17744(__this, p0, &m17744_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m17727_MI, __this, L_1);
		bool L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t40_TI), &L_3);
		return L_4;
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m17712_MI;
 void m17712 (t770 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t7* L_0 = m17744(__this, p0, &m17744_MI);
		bool L_1 = m17745(__this, p1, &m17745_MI);
		VirtActionInvoker2< t7*, bool >::Invoke(&m17728_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m17713_MI;
 void m17713 (t770 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t7* L_0 = m17744(__this, p0, &m17744_MI);
		bool L_1 = m17745(__this, p1, &m17745_MI);
		VirtActionInvoker2< t7*, bool >::Invoke(&m4029_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m17714_MI;
 void m17714 (t770 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t7*)IsInst(p0, (&t7_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t7* >::Invoke(&m17741_MI, __this, ((t7*)Castclass(p0, (&t7_TI))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m17715_MI;
 bool m17715 (t770 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m17716_MI;
 t29 * m17716 (t770 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m17717_MI;
 bool m17717 (t770 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m17718_MI;
 void m17718 (t770 * __this, t3192  p0, MethodInfo* method){
	{
		t7* L_0 = m17750((&p0), &m17750_MI);
		bool L_1 = m17752((&p0), &m17752_MI);
		VirtActionInvoker2< t7*, bool >::Invoke(&m4029_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m17719_MI;
 bool m17719 (t770 * __this, t3192  p0, MethodInfo* method){
	{
		bool L_0 = m17746(__this, p0, &m17746_MI);
		return L_0;
	}
}
extern MethodInfo m17720_MI;
 void m17720 (t770 * __this, t3191* p0, int32_t p1, MethodInfo* method){
	{
		m17734(__this, p0, p1, &m17734_MI);
		return;
	}
}
extern MethodInfo m17721_MI;
 bool m17721 (t770 * __this, t3192  p0, MethodInfo* method){
	{
		bool L_0 = m17746(__this, p0, &m17746_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t7* L_1 = m17750((&p0), &m17750_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m17741_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m17722_MI;
 void m17722 (t770 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t3191* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t770 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t770 * G_B4_2 = {0};
	{
		V_0 = ((t3191*)IsInst(p0, InitializedTypeInfo(&t3191_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m17734(__this, V_0, p1, &m17734_MI);
		return;
	}

IL_0013:
	{
		m17731(__this, p0, p1, &m17731_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t770 *)(__this));
		if ((((t770_SFs*)InitializedTypeInfo(&t770_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t770 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m17748_MI };
		t3189 * L_1 = (t3189 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3189_TI));
		m17795(L_1, NULL, L_0, &m17795_MI);
		((t770_SFs*)InitializedTypeInfo(&t770_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t770 *)(G_B4_2));
	}

IL_0040:
	{
		m23359(G_B5_2, G_B5_1, G_B5_0, (((t770_SFs*)InitializedTypeInfo(&t770_TI)->static_fields)->f15), &m23359_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m17732_MI };
		t3199 * L_3 = (t3199 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3199_TI));
		m17799(L_3, NULL, L_2, &m17799_MI);
		m23361(__this, p0, p1, L_3, &m23361_MI);
		return;
	}
}
extern MethodInfo m17723_MI;
 t29 * m17723 (t770 * __this, MethodInfo* method){
	{
		t3194  L_0 = {0};
		m17779(&L_0, __this, &m17779_MI);
		t3194  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3194_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m17724_MI;
 t29* m17724 (t770 * __this, MethodInfo* method){
	{
		t3194  L_0 = {0};
		m17779(&L_0, __this, &m17779_MI);
		t3194  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3194_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m17725_MI;
 t29 * m17725 (t770 * __this, MethodInfo* method){
	{
		t3200 * L_0 = (t3200 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3200_TI));
		m17803(L_0, __this, &m17803_MI);
		return L_0;
	}
}
 int32_t m17726 (t770 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 bool m17727 (t770 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t771* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(bool*)(bool*)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m17728 (t770 * __this, t7* p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		m17735(__this, &m17735_MI);
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t446* L_29 = (__this->f6);
		*((t7**)(t7**)SZArrayLdElema(L_29, V_2)) = (t7*)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t771* L_37 = (__this->f7);
		*((bool*)(bool*)SZArrayLdElema(L_37, V_2)) = (bool)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m17729 (t770 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t770 * G_B4_0 = {0};
	t770 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t770 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t770 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t770 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t770 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2901_TI));
		t2901 * L_1 = m15881(NULL, &m15881_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t770 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m17730(__this, p0, &m17730_MI);
		__this->f14 = 0;
		return;
	}
}
 void m17730 (t770 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), p0));
		__this->f7 = ((t771*)SZArrayNew(InitializedTypeInfo(&t771_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m17731 (t770 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m17726_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t3192  m17732 (t29 * __this, t7* p0, bool p1, MethodInfo* method){
	{
		t3192  L_0 = {0};
		m17749(&L_0, p0, p1, &m17749_MI);
		return L_0;
	}
}
extern MethodInfo m17733_MI;
 bool m17733 (t29 * __this, t7* p0, bool p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m17734 (t770 * __this, t3191* p0, int32_t p1, MethodInfo* method){
	{
		m17731(__this, (t20 *)(t20 *)p0, p1, &m17731_MI);
		t35 L_0 = { &m17732_MI };
		t3199 * L_1 = (t3199 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3199_TI));
		m17799(L_1, NULL, L_0, &m17799_MI);
		m23362(__this, p0, p1, L_1, &m23362_MI);
		return;
	}
}
 void m17735 (t770 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t446* V_7 = {0};
	t771* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t446* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_4, (*(t7**)(t7**)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), V_0));
		V_8 = ((t771*)SZArrayNew(InitializedTypeInfo(&t771_TI), V_0));
		t446* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t771* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m4029 (t770 * __this, t7* p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		m17735(__this, &m17735_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t446* L_30 = (__this->f6);
		*((t7**)(t7**)SZArrayLdElema(L_30, V_2)) = (t7*)p0;
		t771* L_31 = (__this->f7);
		*((bool*)(bool*)SZArrayLdElema(L_31, V_2)) = (bool)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
extern MethodInfo m17736_MI;
 void m17736 (t770 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t446* L_2 = (__this->f6);
		t446* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t771* L_4 = (__this->f7);
		t771* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m17737 (t770 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m17738_MI;
 bool m17738 (t770 * __this, bool p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3201_TI));
		t3201 * L_0 = m17813(NULL, &m17813_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t771* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, bool, bool >::Invoke(&m30604_MI, V_0, (*(bool*)(bool*)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m17739_MI;
 void m17739 (t770 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t3191* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t3191*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t3191*)SZArrayNew(InitializedTypeInfo(&t3191_TI), L_4));
		m17734(__this, V_0, 0, &m17734_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m17740_MI;
 void m17740 (t770 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t3191* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2096_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2096_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t3191_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t3191*)Castclass(L_10, InitializedTypeInfo(&t3191_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m17730(__this, V_0, &m17730_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t7* L_11 = m17750(((t3192 *)(t3192 *)SZArrayLdElema(V_1, V_2)), &m17750_MI);
		bool L_12 = m17752(((t3192 *)(t3192 *)SZArrayLdElema(V_1, V_2)), &m17752_MI);
		VirtActionInvoker2< t7*, bool >::Invoke(&m4029_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m17741 (t770 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t7* V_4 = {0};
	bool V_5 = false;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t446* L_25 = (__this->f6);
		Initobj (&t7_TI, (&V_4));
		*((t7**)(t7**)SZArrayLdElema(L_25, V_2)) = (t7*)V_4;
		t771* L_26 = (__this->f7);
		Initobj (&t40_TI, (&V_5));
		*((bool*)(bool*)SZArrayLdElema(L_26, V_2)) = (bool)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m17742 (t770 * __this, t7* p0, bool* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t771* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(bool*)(bool*)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t40_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m17743_MI;
 t3190 * m17743 (t770 * __this, MethodInfo* method){
	{
		t3190 * L_0 = (t3190 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3190_TI));
		m17760(L_0, __this, &m17760_MI);
		return L_0;
	}
}
 t7* m17744 (t770 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t7*)IsInst(p0, (&t7_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t7*)Castclass(p0, (&t7_TI)));
	}
}
 bool m17745 (t770 * __this, t29 * p0, MethodInfo* method){
	bool V_0 = false;
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t40_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t40_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t40_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t40_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((*(bool*)((bool*)UnBox (p0, InitializedTypeInfo(&t40_TI)))));
	}
}
 bool m17746 (t770 * __this, t3192  p0, MethodInfo* method){
	bool V_0 = false;
	{
		t7* L_0 = m17750((&p0), &m17750_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, bool* >::Invoke(&m17742_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3201_TI));
		t3201 * L_2 = m17813(NULL, &m17813_MI);
		bool L_3 = m17752((&p0), &m17752_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, bool, bool >::Invoke(&m30605_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m17747_MI;
 t3194  m17747 (t770 * __this, MethodInfo* method){
	{
		t3194  L_0 = {0};
		m17779(&L_0, __this, &m17779_MI);
		return L_0;
	}
}
 t725  m17748 (t29 * __this, t7* p0, bool p1, MethodInfo* method){
	{
		t7* L_0 = p0;
		bool L_1 = p1;
		t29 * L_2 = Box(InitializedTypeInfo(&t40_TI), &L_1);
		t725  L_3 = {0};
		m3965(&L_3, ((t7*)L_0), L_2, &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
extern Il2CppType t44_0_0_32849;
FieldInfo t770_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t770_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t770_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t770_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t770_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t770_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t770_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t770_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t770_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t770_TI, offsetof(t770, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t770_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t770_TI, offsetof(t770, f5), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t770_f6_FieldInfo = 
{
	"keySlots", &t446_0_0_1, &t770_TI, offsetof(t770, f6), &EmptyCustomAttributesCache};
extern Il2CppType t771_0_0_1;
FieldInfo t770_f7_FieldInfo = 
{
	"valueSlots", &t771_0_0_1, &t770_TI, offsetof(t770, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t770_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t770_TI, offsetof(t770, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t770_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t770_TI, offsetof(t770, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t770_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t770_TI, offsetof(t770, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t770_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t770_TI, offsetof(t770, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2096_0_0_1;
FieldInfo t770_f12_FieldInfo = 
{
	"hcp", &t2096_0_0_1, &t770_TI, offsetof(t770, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t770_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t770_TI, offsetof(t770, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t770_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t770_TI, offsetof(t770, f14), &EmptyCustomAttributesCache};
extern Il2CppType t3189_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t770_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t3189_0_0_17, &t770_TI, offsetof(t770_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t770_FIs[] =
{
	&t770_f0_FieldInfo,
	&t770_f1_FieldInfo,
	&t770_f2_FieldInfo,
	&t770_f3_FieldInfo,
	&t770_f4_FieldInfo,
	&t770_f5_FieldInfo,
	&t770_f6_FieldInfo,
	&t770_f7_FieldInfo,
	&t770_f8_FieldInfo,
	&t770_f9_FieldInfo,
	&t770_f10_FieldInfo,
	&t770_f11_FieldInfo,
	&t770_f12_FieldInfo,
	&t770_f13_FieldInfo,
	&t770_f14_FieldInfo,
	&t770_f15_FieldInfo,
	NULL
};
static const int32_t t770_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t770_f0_DefaultValue = 
{
	&t770_f0_FieldInfo, { (char*)&t770_f0_DefaultValueData, &t44_0_0_0 }};
static const float t770_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t770_f1_DefaultValue = 
{
	&t770_f1_FieldInfo, { (char*)&t770_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t770_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t770_f2_DefaultValue = 
{
	&t770_f2_FieldInfo, { (char*)&t770_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t770_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t770_f3_DefaultValue = 
{
	&t770_f3_FieldInfo, { (char*)&t770_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t770_FDVs[] = 
{
	&t770_f0_DefaultValue,
	&t770_f1_DefaultValue,
	&t770_f2_DefaultValue,
	&t770_f3_DefaultValue,
	NULL
};
static PropertyInfo t770____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t770_TI, "System.Collections.IDictionary.Item", &m17711_MI, &m17712_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t770____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t770_TI, "System.Collections.ICollection.IsSynchronized", &m17715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t770____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t770_TI, "System.Collections.ICollection.SyncRoot", &m17716_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t770____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t770_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m17717_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t770____Count_PropertyInfo = 
{
	&t770_TI, "Count", &m17726_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t770____Item_PropertyInfo = 
{
	&t770_TI, "Item", &m17727_MI, &m17728_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t770____Values_PropertyInfo = 
{
	&t770_TI, "Values", &m17743_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t770_PIs[] =
{
	&t770____System_Collections_IDictionary_Item_PropertyInfo,
	&t770____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t770____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t770____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t770____Count_PropertyInfo,
	&t770____Item_PropertyInfo,
	&t770____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17708_GM;
MethodInfo m17708_MI = 
{
	".ctor", (methodPointerType)&m17708, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17708_GM};
extern Il2CppType t2096_0_0_0;
static ParameterInfo t770_m4028_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2096_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m4028_GM;
MethodInfo m4028_MI = 
{
	".ctor", (methodPointerType)&m4028, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t770_m4028_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m4028_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t770_m17709_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17709_GM;
MethodInfo m17709_MI = 
{
	".ctor", (methodPointerType)&m17709, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t770_m17709_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17709_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t770_m17710_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17710_GM;
MethodInfo m17710_MI = 
{
	".ctor", (methodPointerType)&m17710, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t770_m17710_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17710_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17711_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17711_GM;
MethodInfo m17711_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m17711, &t770_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t770_m17711_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17711_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17712_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17712_GM;
MethodInfo m17712_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m17712, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t770_m17712_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17712_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17713_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17713_GM;
MethodInfo m17713_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m17713, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t770_m17713_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17713_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17714_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17714_GM;
MethodInfo m17714_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m17714, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t770_m17714_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17714_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17715_GM;
MethodInfo m17715_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m17715, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17715_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17716_GM;
MethodInfo m17716_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m17716, &t770_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17716_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17717_GM;
MethodInfo m17717_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m17717, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17717_GM};
extern Il2CppType t3192_0_0_0;
extern Il2CppType t3192_0_0_0;
static ParameterInfo t770_m17718_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17718_GM;
MethodInfo m17718_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m17718, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t3192, t770_m17718_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17718_GM};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t770_m17719_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17719_GM;
MethodInfo m17719_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m17719, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t3192, t770_m17719_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17719_GM};
extern Il2CppType t3191_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t770_m17720_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3191_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17720_GM;
MethodInfo m17720_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m17720, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t770_m17720_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17720_GM};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t770_m17721_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17721_GM;
MethodInfo m17721_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m17721, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t3192, t770_m17721_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17721_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t770_m17722_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17722_GM;
MethodInfo m17722_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m17722, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t770_m17722_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17722_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17723_GM;
MethodInfo m17723_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m17723, &t770_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17723_GM};
extern Il2CppType t3193_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17724_GM;
MethodInfo m17724_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m17724, &t770_TI, &t3193_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17724_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17725_GM;
MethodInfo m17725_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m17725, &t770_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17725_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17726_GM;
MethodInfo m17726_MI = 
{
	"get_Count", (methodPointerType)&m17726, &t770_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17726_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t770_m17727_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17727_GM;
MethodInfo m17727_MI = 
{
	"get_Item", (methodPointerType)&m17727, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t770_m17727_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17727_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t770_m17728_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17728_GM;
MethodInfo m17728_MI = 
{
	"set_Item", (methodPointerType)&m17728, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t770_m17728_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17728_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2096_0_0_0;
static ParameterInfo t770_m17729_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2096_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17729_GM;
MethodInfo m17729_MI = 
{
	"Init", (methodPointerType)&m17729, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t770_m17729_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17729_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t770_m17730_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17730_GM;
MethodInfo m17730_MI = 
{
	"InitArrays", (methodPointerType)&m17730, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t770_m17730_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17730_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t770_m17731_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17731_GM;
MethodInfo m17731_MI = 
{
	"CopyToCheck", (methodPointerType)&m17731, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t770_m17731_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17731_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6756_0_0_0;
extern Il2CppType t6756_0_0_0;
static ParameterInfo t770_m30606_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6756_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m30606_IGC;
extern TypeInfo m30606_gp_TRet_0_TI;
Il2CppGenericParamFull m30606_gp_TRet_0_TI_GenericParamFull = { { &m30606_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m30606_gp_TElem_1_TI;
Il2CppGenericParamFull m30606_gp_TElem_1_TI_GenericParamFull = { { &m30606_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m30606_IGPA[2] = 
{
	&m30606_gp_TRet_0_TI_GenericParamFull,
	&m30606_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m30606_MI;
Il2CppGenericContainer m30606_IGC = { { NULL, NULL }, NULL, &m30606_MI, 2, 1, m30606_IGPA };
extern Il2CppGenericMethod m30607_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m30606_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m30607_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m30606_GM;
MethodInfo m30606_MI = 
{
	"Do_CopyTo", NULL, &t770_TI, &t21_0_0_0, NULL, t770_m30606_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m30606_RGCTXData, (methodPointerType)NULL, &m30606_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t770_m17732_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17732_GM;
MethodInfo m17732_MI = 
{
	"make_pair", (methodPointerType)&m17732, &t770_TI, &t3192_0_0_0, RuntimeInvoker_t3192_t29_t297, t770_m17732_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17732_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t770_m17733_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17733_GM;
MethodInfo m17733_MI = 
{
	"pick_value", (methodPointerType)&m17733, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t770_m17733_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17733_GM};
extern Il2CppType t3191_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t770_m17734_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3191_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17734_GM;
MethodInfo m17734_MI = 
{
	"CopyTo", (methodPointerType)&m17734, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t770_m17734_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17734_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6759_0_0_0;
extern Il2CppType t6759_0_0_0;
static ParameterInfo t770_m30608_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6759_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m30608_IGC;
extern TypeInfo m30608_gp_TRet_0_TI;
Il2CppGenericParamFull m30608_gp_TRet_0_TI_GenericParamFull = { { &m30608_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m30608_IGPA[1] = 
{
	&m30608_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m30608_MI;
Il2CppGenericContainer m30608_IGC = { { NULL, NULL }, NULL, &m30608_MI, 1, 1, m30608_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m30609_GM;
static Il2CppRGCTXDefinition m30608_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m30609_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m30608_GM;
MethodInfo m30608_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t770_TI, &t21_0_0_0, NULL, t770_m30608_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m30608_RGCTXData, (methodPointerType)NULL, &m30608_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17735_GM;
MethodInfo m17735_MI = 
{
	"Resize", (methodPointerType)&m17735, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17735_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t770_m4029_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m4029_GM;
MethodInfo m4029_MI = 
{
	"Add", (methodPointerType)&m4029, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t770_m4029_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m4029_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17736_GM;
MethodInfo m17736_MI = 
{
	"Clear", (methodPointerType)&m17736, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17736_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t770_m17737_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17737_GM;
MethodInfo m17737_MI = 
{
	"ContainsKey", (methodPointerType)&m17737, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t770_m17737_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17737_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t770_m17738_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17738_GM;
MethodInfo m17738_MI = 
{
	"ContainsValue", (methodPointerType)&m17738, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t770_m17738_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17738_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t770_m17739_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17739_GM;
MethodInfo m17739_MI = 
{
	"GetObjectData", (methodPointerType)&m17739, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t770_m17739_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17739_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17740_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17740_GM;
MethodInfo m17740_MI = 
{
	"OnDeserialization", (methodPointerType)&m17740, &t770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t770_m17740_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17740_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t770_m17741_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17741_GM;
MethodInfo m17741_MI = 
{
	"Remove", (methodPointerType)&m17741, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t770_m17741_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17741_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_1_0_2;
extern Il2CppType t40_1_0_0;
static ParameterInfo t770_m17742_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t326 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17742_GM;
MethodInfo m17742_MI = 
{
	"TryGetValue", (methodPointerType)&m17742, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t326, t770_m17742_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17742_GM};
extern Il2CppType t3190_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17743_GM;
MethodInfo m17743_MI = 
{
	"get_Values", (methodPointerType)&m17743, &t770_TI, &t3190_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17743_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17744_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17744_GM;
MethodInfo m17744_MI = 
{
	"ToTKey", (methodPointerType)&m17744, &t770_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t770_m17744_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17744_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t770_m17745_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17745_GM;
MethodInfo m17745_MI = 
{
	"ToTValue", (methodPointerType)&m17745, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t770_m17745_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17745_GM};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t770_m17746_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17746_GM;
MethodInfo m17746_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m17746, &t770_TI, &t40_0_0_0, RuntimeInvoker_t40_t3192, t770_m17746_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17746_GM};
extern Il2CppType t3194_0_0_0;
extern void* RuntimeInvoker_t3194 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17747_GM;
MethodInfo m17747_MI = 
{
	"GetEnumerator", (methodPointerType)&m17747, &t770_TI, &t3194_0_0_0, RuntimeInvoker_t3194, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17747_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t770_m17748_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m17748_GM;
MethodInfo m17748_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m17748, &t770_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t297, t770_m17748_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17748_GM};
static MethodInfo* t770_MIs[] =
{
	&m17708_MI,
	&m4028_MI,
	&m17709_MI,
	&m17710_MI,
	&m17711_MI,
	&m17712_MI,
	&m17713_MI,
	&m17714_MI,
	&m17715_MI,
	&m17716_MI,
	&m17717_MI,
	&m17718_MI,
	&m17719_MI,
	&m17720_MI,
	&m17721_MI,
	&m17722_MI,
	&m17723_MI,
	&m17724_MI,
	&m17725_MI,
	&m17726_MI,
	&m17727_MI,
	&m17728_MI,
	&m17729_MI,
	&m17730_MI,
	&m17731_MI,
	&m30606_MI,
	&m17732_MI,
	&m17733_MI,
	&m17734_MI,
	&m30608_MI,
	&m17735_MI,
	&m4029_MI,
	&m17736_MI,
	&m17737_MI,
	&m17738_MI,
	&m17739_MI,
	&m17740_MI,
	&m17741_MI,
	&m17742_MI,
	&m17743_MI,
	&m17744_MI,
	&m17745_MI,
	&m17746_MI,
	&m17747_MI,
	&m17748_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t770_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17723_MI,
	&m17739_MI,
	&m17726_MI,
	&m17715_MI,
	&m17716_MI,
	&m17722_MI,
	&m17726_MI,
	&m17717_MI,
	&m17718_MI,
	&m17736_MI,
	&m17719_MI,
	&m17720_MI,
	&m17721_MI,
	&m17724_MI,
	&m17741_MI,
	&m17711_MI,
	&m17712_MI,
	&m17713_MI,
	&m17725_MI,
	&m17714_MI,
	&m17740_MI,
	&m17727_MI,
	&m17728_MI,
	&m4029_MI,
	&m17737_MI,
	&m17739_MI,
	&m17740_MI,
	&m17742_MI,
};
extern TypeInfo t374_TI;
extern TypeInfo t674_TI;
extern TypeInfo t5856_TI;
extern TypeInfo t5858_TI;
extern TypeInfo t6761_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t770_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5856_TI,
	&t5858_TI,
	&t6761_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t770_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5856_TI, 10},
	{ &t5858_TI, 17},
	{ &t6761_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t770_0_0_0;
extern Il2CppType t770_1_0_0;
extern TypeInfo t29_TI;
struct t770;
extern Il2CppGenericClass t770_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t770_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t770_MIs, t770_PIs, t770_FIs, NULL, &t29_TI, NULL, NULL, &t770_TI, t770_ITIs, t770_VT, &t1254__CustomAttributeCache, &t770_TI, &t770_0_0_0, &t770_1_0_0, t770_IOs, &t770_GC, NULL, t770_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t770), 0, -1, sizeof(t770_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
extern MethodInfo m30610_MI;
static PropertyInfo t5856____Count_PropertyInfo = 
{
	&t5856_TI, "Count", &m30610_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30611_MI;
static PropertyInfo t5856____IsReadOnly_PropertyInfo = 
{
	&t5856_TI, "IsReadOnly", &m30611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5856_PIs[] =
{
	&t5856____Count_PropertyInfo,
	&t5856____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30610_GM;
MethodInfo m30610_MI = 
{
	"get_Count", NULL, &t5856_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30610_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30611_GM;
MethodInfo m30611_MI = 
{
	"get_IsReadOnly", NULL, &t5856_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30611_GM};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t5856_m30612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30612_GM;
MethodInfo m30612_MI = 
{
	"Add", NULL, &t5856_TI, &t21_0_0_0, RuntimeInvoker_t21_t3192, t5856_m30612_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30612_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30613_GM;
MethodInfo m30613_MI = 
{
	"Clear", NULL, &t5856_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30613_GM};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t5856_m30614_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30614_GM;
MethodInfo m30614_MI = 
{
	"Contains", NULL, &t5856_TI, &t40_0_0_0, RuntimeInvoker_t40_t3192, t5856_m30614_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30614_GM};
extern Il2CppType t3191_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5856_m30615_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3191_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30615_GM;
MethodInfo m30615_MI = 
{
	"CopyTo", NULL, &t5856_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5856_m30615_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30615_GM};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t5856_m30616_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30616_GM;
MethodInfo m30616_MI = 
{
	"Remove", NULL, &t5856_TI, &t40_0_0_0, RuntimeInvoker_t40_t3192, t5856_m30616_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30616_GM};
static MethodInfo* t5856_MIs[] =
{
	&m30610_MI,
	&m30611_MI,
	&m30612_MI,
	&m30613_MI,
	&m30614_MI,
	&m30615_MI,
	&m30616_MI,
	NULL
};
static TypeInfo* t5856_ITIs[] = 
{
	&t603_TI,
	&t5858_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5856_0_0_0;
extern Il2CppType t5856_1_0_0;
struct t5856;
extern Il2CppGenericClass t5856_GC;
TypeInfo t5856_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5856_MIs, t5856_PIs, NULL, NULL, NULL, NULL, NULL, &t5856_TI, t5856_ITIs, NULL, &EmptyCustomAttributesCache, &t5856_TI, &t5856_0_0_0, &t5856_1_0_0, NULL, &t5856_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
extern Il2CppType t3193_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30617_GM;
MethodInfo m30617_MI = 
{
	"GetEnumerator", NULL, &t5858_TI, &t3193_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30617_GM};
static MethodInfo* t5858_MIs[] =
{
	&m30617_MI,
	NULL
};
static TypeInfo* t5858_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5858_0_0_0;
extern Il2CppType t5858_1_0_0;
struct t5858;
extern Il2CppGenericClass t5858_GC;
TypeInfo t5858_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5858_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5858_TI, t5858_ITIs, NULL, &EmptyCustomAttributesCache, &t5858_TI, &t5858_0_0_0, &t5858_1_0_0, NULL, &t5858_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3193_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
extern MethodInfo m30618_MI;
static PropertyInfo t3193____Current_PropertyInfo = 
{
	&t3193_TI, "Current", &m30618_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3193_PIs[] =
{
	&t3193____Current_PropertyInfo,
	NULL
};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30618_GM;
MethodInfo m30618_MI = 
{
	"get_Current", NULL, &t3193_TI, &t3192_0_0_0, RuntimeInvoker_t3192, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30618_GM};
static MethodInfo* t3193_MIs[] =
{
	&m30618_MI,
	NULL
};
static TypeInfo* t3193_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3193_0_0_0;
extern Il2CppType t3193_1_0_0;
struct t3193;
extern Il2CppGenericClass t3193_GC;
TypeInfo t3193_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3193_MIs, t3193_PIs, NULL, NULL, NULL, NULL, NULL, &t3193_TI, t3193_ITIs, NULL, &EmptyCustomAttributesCache, &t3193_TI, &t3193_0_0_0, &t3193_1_0_0, NULL, &t3193_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17751_MI;
extern MethodInfo m17753_MI;
extern MethodInfo m4008_MI;


 void m17749 (t3192 * __this, t7* p0, bool p1, MethodInfo* method){
	{
		m17751(__this, p0, &m17751_MI);
		m17753(__this, p1, &m17753_MI);
		return;
	}
}
 t7* m17750 (t3192 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
 void m17751 (t3192 * __this, t7* p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 bool m17752 (t3192 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f1);
		return L_0;
	}
}
 void m17753 (t3192 * __this, bool p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m17754_MI;
 t7* m17754 (t3192 * __this, MethodInfo* method){
	t7* V_0 = {0};
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t7* L_2 = m17750(__this, &m17750_MI);
		t7* L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t7*)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t7* L_4 = m17750(__this, &m17750_MI);
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		bool L_8 = m17752(__this, &m17752_MI);
		bool L_9 = L_8;
		t29 * L_10 = Box(InitializedTypeInfo(&t40_TI), &L_9);
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_10)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		bool L_11 = m17752(__this, &m17752_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t40_TI), &(*(&V_1))));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
extern Il2CppType t7_0_0_1;
FieldInfo t3192_f0_FieldInfo = 
{
	"key", &t7_0_0_1, &t3192_TI, offsetof(t3192, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t3192_f1_FieldInfo = 
{
	"value", &t40_0_0_1, &t3192_TI, offsetof(t3192, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3192_FIs[] =
{
	&t3192_f0_FieldInfo,
	&t3192_f1_FieldInfo,
	NULL
};
static PropertyInfo t3192____Key_PropertyInfo = 
{
	&t3192_TI, "Key", &m17750_MI, &m17751_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3192____Value_PropertyInfo = 
{
	&t3192_TI, "Value", &m17752_MI, &m17753_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3192_PIs[] =
{
	&t3192____Key_PropertyInfo,
	&t3192____Value_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3192_m17749_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17749_GM;
MethodInfo m17749_MI = 
{
	".ctor", (methodPointerType)&m17749, &t3192_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t3192_m17749_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17749_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17750_GM;
MethodInfo m17750_MI = 
{
	"get_Key", (methodPointerType)&m17750, &t3192_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17750_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3192_m17751_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17751_GM;
MethodInfo m17751_MI = 
{
	"set_Key", (methodPointerType)&m17751, &t3192_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3192_m17751_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17751_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17752_GM;
MethodInfo m17752_MI = 
{
	"get_Value", (methodPointerType)&m17752, &t3192_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17752_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3192_m17753_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17753_GM;
MethodInfo m17753_MI = 
{
	"set_Value", (methodPointerType)&m17753, &t3192_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t3192_m17753_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17753_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17754_GM;
MethodInfo m17754_MI = 
{
	"ToString", (methodPointerType)&m17754, &t3192_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17754_GM};
static MethodInfo* t3192_MIs[] =
{
	&m17749_MI,
	&m17750_MI,
	&m17751_MI,
	&m17752_MI,
	&m17753_MI,
	&m17754_MI,
	NULL
};
static MethodInfo* t3192_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m17754_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3192_1_0_0;
extern Il2CppGenericClass t3192_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t3192_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t3192_MIs, t3192_PIs, t3192_FIs, NULL, &t110_TI, NULL, NULL, &t3192_TI, NULL, t3192_VT, &t1259__CustomAttributeCache, &t3192_TI, &t3192_0_0_0, &t3192_1_0_0, NULL, &t3192_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3192)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t3195.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3195_TI;
#include "t3195MD.h"

extern MethodInfo m17759_MI;
extern MethodInfo m23346_MI;
struct t20;
 t3192  m23346 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17755_MI;
 void m17755 (t3195 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17756_MI;
 t29 * m17756 (t3195 * __this, MethodInfo* method){
	{
		t3192  L_0 = m17759(__this, &m17759_MI);
		t3192  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3192_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17757_MI;
 void m17757 (t3195 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17758_MI;
 bool m17758 (t3195 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t3192  m17759 (t3195 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t3192  L_8 = m23346(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23346_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
extern Il2CppType t20_0_0_1;
FieldInfo t3195_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3195_TI, offsetof(t3195, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3195_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3195_TI, offsetof(t3195, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3195_FIs[] =
{
	&t3195_f0_FieldInfo,
	&t3195_f1_FieldInfo,
	NULL
};
static PropertyInfo t3195____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3195_TI, "System.Collections.IEnumerator.Current", &m17756_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3195____Current_PropertyInfo = 
{
	&t3195_TI, "Current", &m17759_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3195_PIs[] =
{
	&t3195____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3195____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3195_m17755_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17755_GM;
MethodInfo m17755_MI = 
{
	".ctor", (methodPointerType)&m17755, &t3195_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3195_m17755_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17755_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17756_GM;
MethodInfo m17756_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17756, &t3195_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17756_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17757_GM;
MethodInfo m17757_MI = 
{
	"Dispose", (methodPointerType)&m17757, &t3195_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17757_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17758_GM;
MethodInfo m17758_MI = 
{
	"MoveNext", (methodPointerType)&m17758, &t3195_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17758_GM};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17759_GM;
MethodInfo m17759_MI = 
{
	"get_Current", (methodPointerType)&m17759, &t3195_TI, &t3192_0_0_0, RuntimeInvoker_t3192, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17759_GM};
static MethodInfo* t3195_MIs[] =
{
	&m17755_MI,
	&m17756_MI,
	&m17757_MI,
	&m17758_MI,
	&m17759_MI,
	NULL
};
static MethodInfo* t3195_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17756_MI,
	&m17758_MI,
	&m17757_MI,
	&m17759_MI,
};
static TypeInfo* t3195_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3193_TI,
};
static Il2CppInterfaceOffsetPair t3195_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3193_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3195_0_0_0;
extern Il2CppType t3195_1_0_0;
extern Il2CppGenericClass t3195_GC;
TypeInfo t3195_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3195_MIs, t3195_PIs, t3195_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3195_TI, t3195_ITIs, t3195_VT, &EmptyCustomAttributesCache, &t3195_TI, &t3195_0_0_0, &t3195_1_0_0, t3195_IOs, &t3195_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3195)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5857_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
extern MethodInfo m30619_MI;
extern MethodInfo m30620_MI;
static PropertyInfo t5857____Item_PropertyInfo = 
{
	&t5857_TI, "Item", &m30619_MI, &m30620_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5857_PIs[] =
{
	&t5857____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3192_0_0_0;
static ParameterInfo t5857_m30621_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30621_GM;
MethodInfo m30621_MI = 
{
	"IndexOf", NULL, &t5857_TI, &t44_0_0_0, RuntimeInvoker_t44_t3192, t5857_m30621_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30621_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3192_0_0_0;
static ParameterInfo t5857_m30622_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30622_GM;
MethodInfo m30622_MI = 
{
	"Insert", NULL, &t5857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t3192, t5857_m30622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30622_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5857_m30623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30623_GM;
MethodInfo m30623_MI = 
{
	"RemoveAt", NULL, &t5857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5857_m30623_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30623_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5857_m30619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30619_GM;
MethodInfo m30619_MI = 
{
	"get_Item", NULL, &t5857_TI, &t3192_0_0_0, RuntimeInvoker_t3192_t44, t5857_m30619_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30619_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3192_0_0_0;
static ParameterInfo t5857_m30620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t3192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30620_GM;
MethodInfo m30620_MI = 
{
	"set_Item", NULL, &t5857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t3192, t5857_m30620_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30620_GM};
static MethodInfo* t5857_MIs[] =
{
	&m30621_MI,
	&m30622_MI,
	&m30623_MI,
	&m30619_MI,
	&m30620_MI,
	NULL
};
static TypeInfo* t5857_ITIs[] = 
{
	&t603_TI,
	&t5856_TI,
	&t5858_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5857_0_0_0;
extern Il2CppType t5857_1_0_0;
struct t5857;
extern Il2CppGenericClass t5857_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5857_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5857_MIs, t5857_PIs, NULL, NULL, NULL, NULL, NULL, &t5857_TI, t5857_ITIs, NULL, &t1908__CustomAttributeCache, &t5857_TI, &t5857_0_0_0, &t5857_1_0_0, NULL, &t5857_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.String,System.Boolean>
extern Il2CppType t7_0_0_0;
static ParameterInfo t6761_m30624_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30624_GM;
MethodInfo m30624_MI = 
{
	"Remove", NULL, &t6761_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6761_m30624_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30624_GM};
static MethodInfo* t6761_MIs[] =
{
	&m30624_MI,
	NULL
};
static TypeInfo* t6761_ITIs[] = 
{
	&t603_TI,
	&t5856_TI,
	&t5858_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6761_0_0_0;
extern Il2CppType t6761_1_0_0;
struct t6761;
extern Il2CppGenericClass t6761_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6761_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6761_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6761_TI, t6761_ITIs, NULL, &t1975__CustomAttributeCache, &t6761_TI, &t6761_0_0_0, &t6761_1_0_0, NULL, &t6761_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
#include "t3197.h"
#include "t3198.h"
extern TypeInfo t345_TI;
extern TypeInfo t3197_TI;
extern TypeInfo t3198_TI;
#include "t345MD.h"
#include "t3198MD.h"
#include "t3197MD.h"
extern MethodInfo m9816_MI;
extern MethodInfo m3988_MI;
extern MethodInfo m17772_MI;
extern MethodInfo m17771_MI;
extern MethodInfo m17791_MI;
extern MethodInfo m23357_MI;
extern MethodInfo m23358_MI;
extern MethodInfo m17774_MI;
struct t770;
 void m23357 (t770 * __this, t20 * p0, int32_t p1, t3198 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t770;
 void m23358 (t770 * __this, t771* p0, int32_t p1, t3198 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m17760 (t3190 * __this, t770 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m17761_MI;
 void m17761 (t3190 * __this, bool p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m17762_MI;
 void m17762 (t3190 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m17763_MI;
 bool m17763 (t3190 * __this, bool p0, MethodInfo* method){
	{
		t770 * L_0 = (__this->f0);
		bool L_1 = m17738(L_0, p0, &m17738_MI);
		return L_1;
	}
}
extern MethodInfo m17764_MI;
 bool m17764 (t3190 * __this, bool p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m17765_MI;
 t29* m17765 (t3190 * __this, MethodInfo* method){
	{
		t3197  L_0 = m17772(__this, &m17772_MI);
		t3197  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3197_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m17766_MI;
 void m17766 (t3190 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t771* V_0 = {0};
	{
		V_0 = ((t771*)IsInst(p0, InitializedTypeInfo(&t771_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t771*, int32_t >::Invoke(&m17771_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t770 * L_0 = (__this->f0);
		m17731(L_0, p0, p1, &m17731_MI);
		t770 * L_1 = (__this->f0);
		t35 L_2 = { &m17733_MI };
		t3198 * L_3 = (t3198 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3198_TI));
		m17791(L_3, NULL, L_2, &m17791_MI);
		m23357(L_1, p0, p1, L_3, &m23357_MI);
		return;
	}
}
extern MethodInfo m17767_MI;
 t29 * m17767 (t3190 * __this, MethodInfo* method){
	{
		t3197  L_0 = m17772(__this, &m17772_MI);
		t3197  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3197_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m17768_MI;
 bool m17768 (t3190 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m17769_MI;
 bool m17769 (t3190 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m17770_MI;
 t29 * m17770 (t3190 * __this, MethodInfo* method){
	{
		t770 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m17771 (t3190 * __this, t771* p0, int32_t p1, MethodInfo* method){
	{
		t770 * L_0 = (__this->f0);
		m17731(L_0, (t20 *)(t20 *)p0, p1, &m17731_MI);
		t770 * L_1 = (__this->f0);
		t35 L_2 = { &m17733_MI };
		t3198 * L_3 = (t3198 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t3198_TI));
		m17791(L_3, NULL, L_2, &m17791_MI);
		m23358(L_1, p0, p1, L_3, &m23358_MI);
		return;
	}
}
 t3197  m17772 (t3190 * __this, MethodInfo* method){
	{
		t770 * L_0 = (__this->f0);
		t3197  L_1 = {0};
		m17774(&L_1, L_0, &m17774_MI);
		return L_1;
	}
}
extern MethodInfo m17773_MI;
 int32_t m17773 (t3190 * __this, MethodInfo* method){
	{
		t770 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m17726_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Boolean>
extern Il2CppType t770_0_0_1;
FieldInfo t3190_f0_FieldInfo = 
{
	"dictionary", &t770_0_0_1, &t3190_TI, offsetof(t3190, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3190_FIs[] =
{
	&t3190_f0_FieldInfo,
	NULL
};
static PropertyInfo t3190____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t3190_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m17768_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3190____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3190_TI, "System.Collections.ICollection.IsSynchronized", &m17769_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3190____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3190_TI, "System.Collections.ICollection.SyncRoot", &m17770_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3190____Count_PropertyInfo = 
{
	&t3190_TI, "Count", &m17773_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3190_PIs[] =
{
	&t3190____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t3190____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3190____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3190____Count_PropertyInfo,
	NULL
};
extern Il2CppType t770_0_0_0;
static ParameterInfo t3190_m17760_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t770_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17760_GM;
MethodInfo m17760_MI = 
{
	".ctor", (methodPointerType)&m17760, &t3190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3190_m17760_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17760_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3190_m17761_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17761_GM;
MethodInfo m17761_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m17761, &t3190_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t3190_m17761_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17761_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17762_GM;
MethodInfo m17762_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m17762, &t3190_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17762_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3190_m17763_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17763_GM;
MethodInfo m17763_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m17763, &t3190_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t3190_m17763_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17763_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3190_m17764_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17764_GM;
MethodInfo m17764_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m17764, &t3190_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t3190_m17764_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17764_GM};
extern Il2CppType t3196_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17765_GM;
MethodInfo m17765_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m17765, &t3190_TI, &t3196_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17765_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3190_m17766_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17766_GM;
MethodInfo m17766_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m17766, &t3190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3190_m17766_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17766_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17767_GM;
MethodInfo m17767_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m17767, &t3190_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17767_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17768_GM;
MethodInfo m17768_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m17768, &t3190_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17768_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17769_GM;
MethodInfo m17769_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m17769, &t3190_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17769_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17770_GM;
MethodInfo m17770_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m17770, &t3190_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17770_GM};
extern Il2CppType t771_0_0_0;
extern Il2CppType t771_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3190_m17771_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t771_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17771_GM;
MethodInfo m17771_MI = 
{
	"CopyTo", (methodPointerType)&m17771, &t3190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3190_m17771_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17771_GM};
extern Il2CppType t3197_0_0_0;
extern void* RuntimeInvoker_t3197 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17772_GM;
MethodInfo m17772_MI = 
{
	"GetEnumerator", (methodPointerType)&m17772, &t3190_TI, &t3197_0_0_0, RuntimeInvoker_t3197, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17772_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17773_GM;
MethodInfo m17773_MI = 
{
	"get_Count", (methodPointerType)&m17773, &t3190_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17773_GM};
static MethodInfo* t3190_MIs[] =
{
	&m17760_MI,
	&m17761_MI,
	&m17762_MI,
	&m17763_MI,
	&m17764_MI,
	&m17765_MI,
	&m17766_MI,
	&m17767_MI,
	&m17768_MI,
	&m17769_MI,
	&m17770_MI,
	&m17771_MI,
	&m17772_MI,
	&m17773_MI,
	NULL
};
static MethodInfo* t3190_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17767_MI,
	&m17773_MI,
	&m17769_MI,
	&m17770_MI,
	&m17766_MI,
	&m17773_MI,
	&m17768_MI,
	&m17761_MI,
	&m17762_MI,
	&m17763_MI,
	&m17771_MI,
	&m17764_MI,
	&m17765_MI,
};
extern TypeInfo t5790_TI;
extern TypeInfo t5792_TI;
static TypeInfo* t3190_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5790_TI,
	&t5792_TI,
};
static Il2CppInterfaceOffsetPair t3190_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5790_TI, 9},
	{ &t5792_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3190_0_0_0;
extern Il2CppType t3190_1_0_0;
struct t3190;
extern Il2CppGenericClass t3190_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t3190_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t3190_MIs, t3190_PIs, t3190_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t3190_TI, t3190_ITIs, t3190_VT, &t1252__CustomAttributeCache, &t3190_TI, &t3190_0_0_0, &t3190_1_0_0, t3190_IOs, &t3190_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3190), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17787_MI;
extern MethodInfo m17790_MI;
extern MethodInfo m17784_MI;


 void m17774 (t3197 * __this, t770 * p0, MethodInfo* method){
	{
		t3194  L_0 = m17747(p0, &m17747_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m17775_MI;
 t29 * m17775 (t3197 * __this, MethodInfo* method){
	{
		t3194 * L_0 = &(__this->f0);
		bool L_1 = m17787(L_0, &m17787_MI);
		bool L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t40_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m17776_MI;
 void m17776 (t3197 * __this, MethodInfo* method){
	{
		t3194 * L_0 = &(__this->f0);
		m17790(L_0, &m17790_MI);
		return;
	}
}
extern MethodInfo m17777_MI;
 bool m17777 (t3197 * __this, MethodInfo* method){
	{
		t3194 * L_0 = &(__this->f0);
		bool L_1 = m17784(L_0, &m17784_MI);
		return L_1;
	}
}
extern MethodInfo m17778_MI;
 bool m17778 (t3197 * __this, MethodInfo* method){
	{
		t3194 * L_0 = &(__this->f0);
		t3192 * L_1 = &(L_0->f3);
		bool L_2 = m17752(L_1, &m17752_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Boolean>
extern Il2CppType t3194_0_0_1;
FieldInfo t3197_f0_FieldInfo = 
{
	"host_enumerator", &t3194_0_0_1, &t3197_TI, offsetof(t3197, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3197_FIs[] =
{
	&t3197_f0_FieldInfo,
	NULL
};
static PropertyInfo t3197____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3197_TI, "System.Collections.IEnumerator.Current", &m17775_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3197____Current_PropertyInfo = 
{
	&t3197_TI, "Current", &m17778_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3197_PIs[] =
{
	&t3197____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3197____Current_PropertyInfo,
	NULL
};
extern Il2CppType t770_0_0_0;
static ParameterInfo t3197_m17774_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t770_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17774_GM;
MethodInfo m17774_MI = 
{
	".ctor", (methodPointerType)&m17774, &t3197_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3197_m17774_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17774_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17775_GM;
MethodInfo m17775_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17775, &t3197_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17775_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17776_GM;
MethodInfo m17776_MI = 
{
	"Dispose", (methodPointerType)&m17776, &t3197_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17776_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17777_GM;
MethodInfo m17777_MI = 
{
	"MoveNext", (methodPointerType)&m17777, &t3197_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17777_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17778_GM;
MethodInfo m17778_MI = 
{
	"get_Current", (methodPointerType)&m17778, &t3197_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17778_GM};
static MethodInfo* t3197_MIs[] =
{
	&m17774_MI,
	&m17775_MI,
	&m17776_MI,
	&m17777_MI,
	&m17778_MI,
	NULL
};
static MethodInfo* t3197_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17775_MI,
	&m17777_MI,
	&m17776_MI,
	&m17778_MI,
};
extern TypeInfo t3196_TI;
static TypeInfo* t3197_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3196_TI,
};
static Il2CppInterfaceOffsetPair t3197_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3196_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3197_0_0_0;
extern Il2CppType t3197_1_0_0;
extern Il2CppGenericClass t3197_GC;
extern TypeInfo t1252_TI;
TypeInfo t3197_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3197_MIs, t3197_PIs, t3197_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t3197_TI, t3197_ITIs, t3197_VT, &EmptyCustomAttributesCache, &t3197_TI, &t3197_0_0_0, &t3197_1_0_0, t3197_IOs, &t3197_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3197)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m17789_MI;
extern MethodInfo m17786_MI;
extern MethodInfo m17788_MI;
extern MethodInfo m5150_MI;


 void m17779 (t3194 * __this, t770 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m17780_MI;
 t29 * m17780 (t3194 * __this, MethodInfo* method){
	{
		m17789(__this, &m17789_MI);
		t3192  L_0 = (__this->f3);
		t3192  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3192_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17781_MI;
 t725  m17781 (t3194 * __this, MethodInfo* method){
	{
		m17789(__this, &m17789_MI);
		t3192 * L_0 = &(__this->f3);
		t7* L_1 = m17750(L_0, &m17750_MI);
		t7* L_2 = L_1;
		t3192 * L_3 = &(__this->f3);
		bool L_4 = m17752(L_3, &m17752_MI);
		bool L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t40_TI), &L_5);
		t725  L_7 = {0};
		m3965(&L_7, ((t7*)L_2), L_6, &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m17782_MI;
 t29 * m17782 (t3194 * __this, MethodInfo* method){
	{
		t7* L_0 = m17786(__this, &m17786_MI);
		t7* L_1 = L_0;
		return ((t7*)L_1);
	}
}
extern MethodInfo m17783_MI;
 t29 * m17783 (t3194 * __this, MethodInfo* method){
	{
		bool L_0 = m17787(__this, &m17787_MI);
		bool L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t40_TI), &L_1);
		return L_2;
	}
}
 bool m17784 (t3194 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m17788(__this, &m17788_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t770 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t770 * L_6 = (__this->f0);
		t446* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t770 * L_9 = (__this->f0);
		t771* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t3192  L_12 = {0};
		m17749(&L_12, (*(t7**)(t7**)SZArrayLdElema(L_7, L_8)), (*(bool*)(bool*)SZArrayLdElema(L_10, L_11)), &m17749_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t770 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m17785_MI;
 t3192  m17785 (t3194 * __this, MethodInfo* method){
	{
		t3192  L_0 = (__this->f3);
		return L_0;
	}
}
 t7* m17786 (t3194 * __this, MethodInfo* method){
	{
		m17789(__this, &m17789_MI);
		t3192 * L_0 = &(__this->f3);
		t7* L_1 = m17750(L_0, &m17750_MI);
		return L_1;
	}
}
 bool m17787 (t3194 * __this, MethodInfo* method){
	{
		m17789(__this, &m17789_MI);
		t3192 * L_0 = &(__this->f3);
		bool L_1 = m17752(L_0, &m17752_MI);
		return L_1;
	}
}
 void m17788 (t3194 * __this, MethodInfo* method){
	{
		t770 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t770 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m17789 (t3194 * __this, MethodInfo* method){
	{
		m17788(__this, &m17788_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m17790 (t3194 * __this, MethodInfo* method){
	{
		__this->f0 = (t770 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
extern Il2CppType t770_0_0_1;
FieldInfo t3194_f0_FieldInfo = 
{
	"dictionary", &t770_0_0_1, &t3194_TI, offsetof(t3194, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3194_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3194_TI, offsetof(t3194, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3194_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t3194_TI, offsetof(t3194, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t3192_0_0_3;
FieldInfo t3194_f3_FieldInfo = 
{
	"current", &t3192_0_0_3, &t3194_TI, offsetof(t3194, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3194_FIs[] =
{
	&t3194_f0_FieldInfo,
	&t3194_f1_FieldInfo,
	&t3194_f2_FieldInfo,
	&t3194_f3_FieldInfo,
	NULL
};
static PropertyInfo t3194____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3194_TI, "System.Collections.IEnumerator.Current", &m17780_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3194____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t3194_TI, "System.Collections.IDictionaryEnumerator.Entry", &m17781_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3194____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t3194_TI, "System.Collections.IDictionaryEnumerator.Key", &m17782_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3194____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t3194_TI, "System.Collections.IDictionaryEnumerator.Value", &m17783_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3194____Current_PropertyInfo = 
{
	&t3194_TI, "Current", &m17785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3194____CurrentKey_PropertyInfo = 
{
	&t3194_TI, "CurrentKey", &m17786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3194____CurrentValue_PropertyInfo = 
{
	&t3194_TI, "CurrentValue", &m17787_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3194_PIs[] =
{
	&t3194____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3194____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t3194____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t3194____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t3194____Current_PropertyInfo,
	&t3194____CurrentKey_PropertyInfo,
	&t3194____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t770_0_0_0;
static ParameterInfo t3194_m17779_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t770_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17779_GM;
MethodInfo m17779_MI = 
{
	".ctor", (methodPointerType)&m17779, &t3194_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3194_m17779_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17779_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17780_GM;
MethodInfo m17780_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17780, &t3194_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17780_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17781_GM;
MethodInfo m17781_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m17781, &t3194_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17781_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17782_GM;
MethodInfo m17782_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m17782, &t3194_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17782_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17783_GM;
MethodInfo m17783_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m17783, &t3194_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17783_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17784_GM;
MethodInfo m17784_MI = 
{
	"MoveNext", (methodPointerType)&m17784, &t3194_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17784_GM};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17785_GM;
MethodInfo m17785_MI = 
{
	"get_Current", (methodPointerType)&m17785, &t3194_TI, &t3192_0_0_0, RuntimeInvoker_t3192, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17785_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17786_GM;
MethodInfo m17786_MI = 
{
	"get_CurrentKey", (methodPointerType)&m17786, &t3194_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17786_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17787_GM;
MethodInfo m17787_MI = 
{
	"get_CurrentValue", (methodPointerType)&m17787, &t3194_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17787_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17788_GM;
MethodInfo m17788_MI = 
{
	"VerifyState", (methodPointerType)&m17788, &t3194_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17788_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17789_GM;
MethodInfo m17789_MI = 
{
	"VerifyCurrent", (methodPointerType)&m17789, &t3194_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17789_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17790_GM;
MethodInfo m17790_MI = 
{
	"Dispose", (methodPointerType)&m17790, &t3194_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17790_GM};
static MethodInfo* t3194_MIs[] =
{
	&m17779_MI,
	&m17780_MI,
	&m17781_MI,
	&m17782_MI,
	&m17783_MI,
	&m17784_MI,
	&m17785_MI,
	&m17786_MI,
	&m17787_MI,
	&m17788_MI,
	&m17789_MI,
	&m17790_MI,
	NULL
};
static MethodInfo* t3194_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17780_MI,
	&m17784_MI,
	&m17790_MI,
	&m17785_MI,
	&m17781_MI,
	&m17782_MI,
	&m17783_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t3194_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3193_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t3194_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3193_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3194_0_0_0;
extern Il2CppType t3194_1_0_0;
extern Il2CppGenericClass t3194_GC;
TypeInfo t3194_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3194_MIs, t3194_PIs, t3194_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t3194_TI, t3194_ITIs, t3194_VT, &EmptyCustomAttributesCache, &t3194_TI, &t3194_0_0_0, &t3194_1_0_0, t3194_IOs, &t3194_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3194)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif

#include "t67.h"


 void m17791 (t3198 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17792_MI;
 bool m17792 (t3198 * __this, t7* p0, bool p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17792((t3198 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t7* p0, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17793_MI;
 t29 * m17793 (t3198 * __this, t7* p0, bool p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t40_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17794_MI;
 bool m17794 (t3198 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Boolean>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3198_m17791_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17791_GM;
MethodInfo m17791_MI = 
{
	".ctor", (methodPointerType)&m17791, &t3198_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3198_m17791_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17791_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3198_m17792_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17792_GM;
MethodInfo m17792_MI = 
{
	"Invoke", (methodPointerType)&m17792, &t3198_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t3198_m17792_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17792_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3198_m17793_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17793_GM;
MethodInfo m17793_MI = 
{
	"BeginInvoke", (methodPointerType)&m17793, &t3198_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t297_t29_t29, t3198_m17793_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17793_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3198_m17794_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17794_GM;
MethodInfo m17794_MI = 
{
	"EndInvoke", (methodPointerType)&m17794, &t3198_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3198_m17794_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17794_GM};
static MethodInfo* t3198_MIs[] =
{
	&m17791_MI,
	&m17792_MI,
	&m17793_MI,
	&m17794_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t3198_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17792_MI,
	&m17793_MI,
	&m17794_MI,
};
extern TypeInfo t373_TI;
static Il2CppInterfaceOffsetPair t3198_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3198_0_0_0;
extern Il2CppType t3198_1_0_0;
extern TypeInfo t195_TI;
struct t3198;
extern Il2CppGenericClass t3198_GC;
TypeInfo t3198_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t3198_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t3198_TI, NULL, t3198_VT, &EmptyCustomAttributesCache, &t3198_TI, &t3198_0_0_0, &t3198_1_0_0, t3198_IOs, &t3198_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3198), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m17795 (t3189 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17796_MI;
 t725  m17796 (t3189 * __this, t7* p0, bool p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17796((t3189 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t7* p0, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17797_MI;
 t29 * m17797 (t3189 * __this, t7* p0, bool p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t40_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17798_MI;
 t725  m17798 (t3189 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3189_m17795_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17795_GM;
MethodInfo m17795_MI = 
{
	".ctor", (methodPointerType)&m17795, &t3189_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3189_m17795_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17795_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3189_m17796_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17796_GM;
MethodInfo m17796_MI = 
{
	"Invoke", (methodPointerType)&m17796, &t3189_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t297, t3189_m17796_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17796_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3189_m17797_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17797_GM;
MethodInfo m17797_MI = 
{
	"BeginInvoke", (methodPointerType)&m17797, &t3189_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t297_t29_t29, t3189_m17797_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17797_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3189_m17798_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17798_GM;
MethodInfo m17798_MI = 
{
	"EndInvoke", (methodPointerType)&m17798, &t3189_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t3189_m17798_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17798_GM};
static MethodInfo* t3189_MIs[] =
{
	&m17795_MI,
	&m17796_MI,
	&m17797_MI,
	&m17798_MI,
	NULL
};
static MethodInfo* t3189_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17796_MI,
	&m17797_MI,
	&m17798_MI,
};
static Il2CppInterfaceOffsetPair t3189_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3189_0_0_0;
extern Il2CppType t3189_1_0_0;
struct t3189;
extern Il2CppGenericClass t3189_GC;
TypeInfo t3189_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t3189_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t3189_TI, NULL, t3189_VT, &EmptyCustomAttributesCache, &t3189_TI, &t3189_0_0_0, &t3189_1_0_0, t3189_IOs, &t3189_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3189), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m17799 (t3199 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17800_MI;
 t3192  m17800 (t3199 * __this, t7* p0, bool p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m17800((t3199 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t3192  (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t3192  (*FunctionPointerType) (t29 * __this, t7* p0, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t3192  (*FunctionPointerType) (t29 * __this, bool p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17801_MI;
 t29 * m17801 (t3199 * __this, t7* p0, bool p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t40_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m17802_MI;
 t3192  m17802 (t3199 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t3192 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3199_m17799_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17799_GM;
MethodInfo m17799_MI = 
{
	".ctor", (methodPointerType)&m17799, &t3199_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3199_m17799_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17799_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3199_m17800_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192_t29_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17800_GM;
MethodInfo m17800_MI = 
{
	"Invoke", (methodPointerType)&m17800, &t3199_TI, &t3192_0_0_0, RuntimeInvoker_t3192_t29_t297, t3199_m17800_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17800_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3199_m17801_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17801_GM;
MethodInfo m17801_MI = 
{
	"BeginInvoke", (methodPointerType)&m17801, &t3199_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t297_t29_t29, t3199_m17801_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17801_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3199_m17802_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t3192_0_0_0;
extern void* RuntimeInvoker_t3192_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17802_GM;
MethodInfo m17802_MI = 
{
	"EndInvoke", (methodPointerType)&m17802, &t3199_TI, &t3192_0_0_0, RuntimeInvoker_t3192_t29, t3199_m17802_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17802_GM};
static MethodInfo* t3199_MIs[] =
{
	&m17799_MI,
	&m17800_MI,
	&m17801_MI,
	&m17802_MI,
	NULL
};
static MethodInfo* t3199_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17800_MI,
	&m17801_MI,
	&m17802_MI,
};
static Il2CppInterfaceOffsetPair t3199_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3199_0_0_0;
extern Il2CppType t3199_1_0_0;
struct t3199;
extern Il2CppGenericClass t3199_GC;
TypeInfo t3199_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t3199_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t3199_TI, NULL, t3199_VT, &EmptyCustomAttributesCache, &t3199_TI, &t3199_0_0_0, &t3199_1_0_0, t3199_IOs, &t3199_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3199), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10122_MI;
extern MethodInfo m17805_MI;


 void m17803 (t3200 * __this, t770 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t3194  L_0 = m17747(p0, &m17747_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m17804_MI;
 bool m17804 (t3200 * __this, MethodInfo* method){
	{
		t3194 * L_0 = &(__this->f0);
		bool L_1 = m17784(L_0, &m17784_MI);
		return L_1;
	}
}
 t725  m17805 (t3200 * __this, MethodInfo* method){
	{
		t3194  L_0 = (__this->f0);
		t3194  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3194_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m17806_MI;
 t29 * m17806 (t3200 * __this, MethodInfo* method){
	t3192  V_0 = {0};
	{
		t3194 * L_0 = &(__this->f0);
		t3192  L_1 = m17785(L_0, &m17785_MI);
		V_0 = L_1;
		t7* L_2 = m17750((&V_0), &m17750_MI);
		t7* L_3 = L_2;
		return ((t7*)L_3);
	}
}
extern MethodInfo m17807_MI;
 t29 * m17807 (t3200 * __this, MethodInfo* method){
	t3192  V_0 = {0};
	{
		t3194 * L_0 = &(__this->f0);
		t3192  L_1 = m17785(L_0, &m17785_MI);
		V_0 = L_1;
		bool L_2 = m17752((&V_0), &m17752_MI);
		bool L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t40_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m17808_MI;
 t29 * m17808 (t3200 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m17805_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>
extern Il2CppType t3194_0_0_1;
FieldInfo t3200_f0_FieldInfo = 
{
	"host_enumerator", &t3194_0_0_1, &t3200_TI, offsetof(t3200, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3200_FIs[] =
{
	&t3200_f0_FieldInfo,
	NULL
};
static PropertyInfo t3200____Entry_PropertyInfo = 
{
	&t3200_TI, "Entry", &m17805_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3200____Key_PropertyInfo = 
{
	&t3200_TI, "Key", &m17806_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3200____Value_PropertyInfo = 
{
	&t3200_TI, "Value", &m17807_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3200____Current_PropertyInfo = 
{
	&t3200_TI, "Current", &m17808_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3200_PIs[] =
{
	&t3200____Entry_PropertyInfo,
	&t3200____Key_PropertyInfo,
	&t3200____Value_PropertyInfo,
	&t3200____Current_PropertyInfo,
	NULL
};
extern Il2CppType t770_0_0_0;
static ParameterInfo t3200_m17803_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t770_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17803_GM;
MethodInfo m17803_MI = 
{
	".ctor", (methodPointerType)&m17803, &t3200_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3200_m17803_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17803_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17804_GM;
MethodInfo m17804_MI = 
{
	"MoveNext", (methodPointerType)&m17804, &t3200_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17804_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17805_GM;
MethodInfo m17805_MI = 
{
	"get_Entry", (methodPointerType)&m17805, &t3200_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17805_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17806_GM;
MethodInfo m17806_MI = 
{
	"get_Key", (methodPointerType)&m17806, &t3200_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17806_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17807_GM;
MethodInfo m17807_MI = 
{
	"get_Value", (methodPointerType)&m17807, &t3200_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17807_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17808_GM;
MethodInfo m17808_MI = 
{
	"get_Current", (methodPointerType)&m17808, &t3200_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17808_GM};
static MethodInfo* t3200_MIs[] =
{
	&m17803_MI,
	&m17804_MI,
	&m17805_MI,
	&m17806_MI,
	&m17807_MI,
	&m17808_MI,
	NULL
};
static MethodInfo* t3200_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17808_MI,
	&m17804_MI,
	&m17805_MI,
	&m17806_MI,
	&m17807_MI,
};
static TypeInfo* t3200_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t3200_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3200_0_0_0;
extern Il2CppType t3200_1_0_0;
struct t3200;
extern Il2CppGenericClass t3200_GC;
TypeInfo t3200_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t3200_MIs, t3200_PIs, t3200_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t3200_TI, t3200_ITIs, t3200_VT, &EmptyCustomAttributesCache, &t3200_TI, &t3200_0_0_0, &t3200_1_0_0, t3200_IOs, &t3200_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3200), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Boolean>
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t6755_m30604_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30604_GM;
MethodInfo m30604_MI = 
{
	"Equals", NULL, &t6755_TI, &t40_0_0_0, RuntimeInvoker_t40_t297_t297, t6755_m30604_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30604_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t6755_m30625_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30625_GM;
MethodInfo m30625_MI = 
{
	"GetHashCode", NULL, &t6755_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t6755_m30625_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30625_GM};
static MethodInfo* t6755_MIs[] =
{
	&m30604_MI,
	&m30625_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6755_0_0_0;
extern Il2CppType t6755_1_0_0;
struct t6755;
extern Il2CppGenericClass t6755_GC;
TypeInfo t6755_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6755_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6755_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6755_TI, &t6755_0_0_0, &t6755_1_0_0, NULL, &t6755_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1257.h"
#include "t3203.h"
extern TypeInfo t1760_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3203_TI;
#include "t931MD.h"
#include "t3203MD.h"
extern Il2CppType t1760_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m17817_MI;
extern MethodInfo m30626_MI;


extern MethodInfo m17809_MI;
 void m17809 (t3201 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m17810_MI;
 void m17810 (t29 * __this, MethodInfo* method){
	t3203 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3203 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3203_TI));
	m17817(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m17817_MI);
	((t3201_SFs*)InitializedTypeInfo(&t3201_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m17811_MI;
 int32_t m17811 (t3201 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, bool >::Invoke(&m30626_MI, __this, ((*(bool*)((bool*)UnBox (p0, InitializedTypeInfo(&t40_TI))))));
		return L_0;
	}
}
extern MethodInfo m17812_MI;
 bool m17812 (t3201 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, bool, bool >::Invoke(&m30605_MI, __this, ((*(bool*)((bool*)UnBox (p0, InitializedTypeInfo(&t40_TI))))), ((*(bool*)((bool*)UnBox (p1, InitializedTypeInfo(&t40_TI))))));
		return L_0;
	}
}
 t3201 * m17813 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3201_TI));
		return (((t3201_SFs*)InitializedTypeInfo(&t3201_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Boolean>
extern Il2CppType t3201_0_0_49;
FieldInfo t3201_f0_FieldInfo = 
{
	"_default", &t3201_0_0_49, &t3201_TI, offsetof(t3201_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3201_FIs[] =
{
	&t3201_f0_FieldInfo,
	NULL
};
static PropertyInfo t3201____Default_PropertyInfo = 
{
	&t3201_TI, "Default", &m17813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3201_PIs[] =
{
	&t3201____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17809_GM;
MethodInfo m17809_MI = 
{
	".ctor", (methodPointerType)&m17809, &t3201_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17809_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17810_GM;
MethodInfo m17810_MI = 
{
	".cctor", (methodPointerType)&m17810, &t3201_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17810_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3201_m17811_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17811_GM;
MethodInfo m17811_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m17811, &t3201_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3201_m17811_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17811_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3201_m17812_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17812_GM;
MethodInfo m17812_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m17812, &t3201_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3201_m17812_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17812_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3201_m30626_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30626_GM;
MethodInfo m30626_MI = 
{
	"GetHashCode", NULL, &t3201_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t3201_m30626_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30626_GM};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3201_m30605_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30605_GM;
MethodInfo m30605_MI = 
{
	"Equals", NULL, &t3201_TI, &t40_0_0_0, RuntimeInvoker_t40_t297_t297, t3201_m30605_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30605_GM};
extern Il2CppType t3201_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17813_GM;
MethodInfo m17813_MI = 
{
	"get_Default", (methodPointerType)&m17813, &t3201_TI, &t3201_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17813_GM};
static MethodInfo* t3201_MIs[] =
{
	&m17809_MI,
	&m17810_MI,
	&m17811_MI,
	&m17812_MI,
	&m30626_MI,
	&m30605_MI,
	&m17813_MI,
	NULL
};
static MethodInfo* t3201_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m30605_MI,
	&m30626_MI,
	&m17812_MI,
	&m17811_MI,
	NULL,
	NULL,
};
extern TypeInfo t734_TI;
static TypeInfo* t3201_ITIs[] = 
{
	&t6755_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3201_IOs[] = 
{
	{ &t6755_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3201_0_0_0;
extern Il2CppType t3201_1_0_0;
struct t3201;
extern Il2CppGenericClass t3201_GC;
TypeInfo t3201_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3201_MIs, t3201_PIs, t3201_FIs, NULL, &t29_TI, NULL, NULL, &t3201_TI, t3201_ITIs, t3201_VT, &EmptyCustomAttributesCache, &t3201_TI, &t3201_0_0_0, &t3201_1_0_0, t3201_IOs, &t3201_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3201), 0, -1, sizeof(t3201_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#include "t3202.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3202_TI;
#include "t3202MD.h"

extern MethodInfo m30330_MI;


extern MethodInfo m17814_MI;
 void m17814 (t3202 * __this, MethodInfo* method){
	{
		m17809(__this, &m17809_MI);
		return;
	}
}
extern MethodInfo m17815_MI;
 int32_t m17815 (t3202 * __this, bool p0, MethodInfo* method){
	{
		bool L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t40_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t40_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m17816_MI;
 bool m17816 (t3202 * __this, bool p0, bool p1, MethodInfo* method){
	{
		bool L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t40_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		bool L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t40_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, bool >::Invoke(&m30330_MI, Box(InitializedTypeInfo(&t40_TI), &(*(&p0))), p1);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17814_GM;
MethodInfo m17814_MI = 
{
	".ctor", (methodPointerType)&m17814, &t3202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17814_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3202_m17815_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17815_GM;
MethodInfo m17815_MI = 
{
	"GetHashCode", (methodPointerType)&m17815, &t3202_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t3202_m17815_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17815_GM};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3202_m17816_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17816_GM;
MethodInfo m17816_MI = 
{
	"Equals", (methodPointerType)&m17816, &t3202_TI, &t40_0_0_0, RuntimeInvoker_t40_t297_t297, t3202_m17816_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17816_GM};
static MethodInfo* t3202_MIs[] =
{
	&m17814_MI,
	&m17815_MI,
	&m17816_MI,
	NULL
};
static MethodInfo* t3202_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17816_MI,
	&m17815_MI,
	&m17812_MI,
	&m17811_MI,
	&m17815_MI,
	&m17816_MI,
};
static Il2CppInterfaceOffsetPair t3202_IOs[] = 
{
	{ &t6755_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3202_0_0_0;
extern Il2CppType t3202_1_0_0;
struct t3202;
extern Il2CppGenericClass t3202_GC;
TypeInfo t3202_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t3202_MIs, NULL, NULL, NULL, &t3201_TI, NULL, NULL, &t3202_TI, NULL, t3202_VT, &EmptyCustomAttributesCache, &t3202_TI, &t3202_0_0_0, &t3202_1_0_0, t3202_IOs, &t3202_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3202), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m17817 (t3203 * __this, MethodInfo* method){
	{
		m17809(__this, &m17809_MI);
		return;
	}
}
extern MethodInfo m17818_MI;
 int32_t m17818 (t3203 * __this, bool p0, MethodInfo* method){
	{
		bool L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t40_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t40_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m17819_MI;
 bool m17819 (t3203 * __this, bool p0, bool p1, MethodInfo* method){
	{
		bool L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t40_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		bool L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t40_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t40_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t40_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17817_GM;
MethodInfo m17817_MI = 
{
	".ctor", (methodPointerType)&m17817, &t3203_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17817_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t3203_m17818_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17818_GM;
MethodInfo m17818_MI = 
{
	"GetHashCode", (methodPointerType)&m17818, &t3203_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t3203_m17818_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17818_GM};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t3203_m17819_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17819_GM;
MethodInfo m17819_MI = 
{
	"Equals", (methodPointerType)&m17819, &t3203_TI, &t40_0_0_0, RuntimeInvoker_t40_t297_t297, t3203_m17819_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17819_GM};
static MethodInfo* t3203_MIs[] =
{
	&m17817_MI,
	&m17818_MI,
	&m17819_MI,
	NULL
};
static MethodInfo* t3203_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17819_MI,
	&m17818_MI,
	&m17812_MI,
	&m17811_MI,
	&m17818_MI,
	&m17819_MI,
};
static Il2CppInterfaceOffsetPair t3203_IOs[] = 
{
	{ &t6755_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3203_0_0_0;
extern Il2CppType t3203_1_0_0;
struct t3203;
extern Il2CppGenericClass t3203_GC;
extern TypeInfo t1256_TI;
TypeInfo t3203_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3203_MIs, NULL, NULL, NULL, &t3201_TI, NULL, &t1256_TI, &t3203_TI, NULL, t3203_VT, &EmptyCustomAttributesCache, &t3203_TI, &t3203_0_0_0, &t3203_1_0_0, t3203_IOs, &t3203_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3203), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4555_TI;

#include "t775.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>
extern MethodInfo m30627_MI;
static PropertyInfo t4555____Current_PropertyInfo = 
{
	&t4555_TI, "Current", &m30627_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4555_PIs[] =
{
	&t4555____Current_PropertyInfo,
	NULL
};
extern Il2CppType t775_0_0_0;
extern void* RuntimeInvoker_t775 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30627_GM;
MethodInfo m30627_MI = 
{
	"get_Current", NULL, &t4555_TI, &t775_0_0_0, RuntimeInvoker_t775, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30627_GM};
static MethodInfo* t4555_MIs[] =
{
	&m30627_MI,
	NULL
};
static TypeInfo* t4555_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4555_0_0_0;
extern Il2CppType t4555_1_0_0;
struct t4555;
extern Il2CppGenericClass t4555_GC;
TypeInfo t4555_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4555_MIs, t4555_PIs, NULL, NULL, NULL, NULL, NULL, &t4555_TI, t4555_ITIs, NULL, &EmptyCustomAttributesCache, &t4555_TI, &t4555_0_0_0, &t4555_1_0_0, NULL, &t4555_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3204.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3204_TI;
#include "t3204MD.h"

extern TypeInfo t775_TI;
extern MethodInfo m17824_MI;
extern MethodInfo m23364_MI;
struct t20;
 int32_t m23364 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17820_MI;
 void m17820 (t3204 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17821_MI;
 t29 * m17821 (t3204 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17824(__this, &m17824_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t775_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17822_MI;
 void m17822 (t3204 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17823_MI;
 bool m17823 (t3204 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17824 (t3204 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23364(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23364_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3204_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3204_TI, offsetof(t3204, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3204_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3204_TI, offsetof(t3204, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3204_FIs[] =
{
	&t3204_f0_FieldInfo,
	&t3204_f1_FieldInfo,
	NULL
};
static PropertyInfo t3204____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3204_TI, "System.Collections.IEnumerator.Current", &m17821_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3204____Current_PropertyInfo = 
{
	&t3204_TI, "Current", &m17824_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3204_PIs[] =
{
	&t3204____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3204____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3204_m17820_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17820_GM;
MethodInfo m17820_MI = 
{
	".ctor", (methodPointerType)&m17820, &t3204_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3204_m17820_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17820_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17821_GM;
MethodInfo m17821_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17821, &t3204_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17821_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17822_GM;
MethodInfo m17822_MI = 
{
	"Dispose", (methodPointerType)&m17822, &t3204_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17822_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17823_GM;
MethodInfo m17823_MI = 
{
	"MoveNext", (methodPointerType)&m17823, &t3204_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17823_GM};
extern Il2CppType t775_0_0_0;
extern void* RuntimeInvoker_t775 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17824_GM;
MethodInfo m17824_MI = 
{
	"get_Current", (methodPointerType)&m17824, &t3204_TI, &t775_0_0_0, RuntimeInvoker_t775, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17824_GM};
static MethodInfo* t3204_MIs[] =
{
	&m17820_MI,
	&m17821_MI,
	&m17822_MI,
	&m17823_MI,
	&m17824_MI,
	NULL
};
static MethodInfo* t3204_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17821_MI,
	&m17823_MI,
	&m17822_MI,
	&m17824_MI,
};
static TypeInfo* t3204_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4555_TI,
};
static Il2CppInterfaceOffsetPair t3204_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4555_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3204_0_0_0;
extern Il2CppType t3204_1_0_0;
extern Il2CppGenericClass t3204_GC;
TypeInfo t3204_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3204_MIs, t3204_PIs, t3204_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3204_TI, t3204_ITIs, t3204_VT, &EmptyCustomAttributesCache, &t3204_TI, &t3204_0_0_0, &t3204_1_0_0, t3204_IOs, &t3204_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3204)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5859_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.OpenFlags>
extern MethodInfo m30628_MI;
static PropertyInfo t5859____Count_PropertyInfo = 
{
	&t5859_TI, "Count", &m30628_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30629_MI;
static PropertyInfo t5859____IsReadOnly_PropertyInfo = 
{
	&t5859_TI, "IsReadOnly", &m30629_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5859_PIs[] =
{
	&t5859____Count_PropertyInfo,
	&t5859____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30628_GM;
MethodInfo m30628_MI = 
{
	"get_Count", NULL, &t5859_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30628_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30629_GM;
MethodInfo m30629_MI = 
{
	"get_IsReadOnly", NULL, &t5859_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30629_GM};
extern Il2CppType t775_0_0_0;
extern Il2CppType t775_0_0_0;
static ParameterInfo t5859_m30630_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t775_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30630_GM;
MethodInfo m30630_MI = 
{
	"Add", NULL, &t5859_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5859_m30630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30630_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30631_GM;
MethodInfo m30631_MI = 
{
	"Clear", NULL, &t5859_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30631_GM};
extern Il2CppType t775_0_0_0;
static ParameterInfo t5859_m30632_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t775_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30632_GM;
MethodInfo m30632_MI = 
{
	"Contains", NULL, &t5859_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5859_m30632_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30632_GM};
extern Il2CppType t3904_0_0_0;
extern Il2CppType t3904_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5859_m30633_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3904_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30633_GM;
MethodInfo m30633_MI = 
{
	"CopyTo", NULL, &t5859_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5859_m30633_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30633_GM};
extern Il2CppType t775_0_0_0;
static ParameterInfo t5859_m30634_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t775_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30634_GM;
MethodInfo m30634_MI = 
{
	"Remove", NULL, &t5859_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5859_m30634_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30634_GM};
static MethodInfo* t5859_MIs[] =
{
	&m30628_MI,
	&m30629_MI,
	&m30630_MI,
	&m30631_MI,
	&m30632_MI,
	&m30633_MI,
	&m30634_MI,
	NULL
};
extern TypeInfo t5861_TI;
static TypeInfo* t5859_ITIs[] = 
{
	&t603_TI,
	&t5861_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5859_0_0_0;
extern Il2CppType t5859_1_0_0;
struct t5859;
extern Il2CppGenericClass t5859_GC;
TypeInfo t5859_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5859_MIs, t5859_PIs, NULL, NULL, NULL, NULL, NULL, &t5859_TI, t5859_ITIs, NULL, &EmptyCustomAttributesCache, &t5859_TI, &t5859_0_0_0, &t5859_1_0_0, NULL, &t5859_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.OpenFlags>
extern Il2CppType t4555_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30635_GM;
MethodInfo m30635_MI = 
{
	"GetEnumerator", NULL, &t5861_TI, &t4555_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30635_GM};
static MethodInfo* t5861_MIs[] =
{
	&m30635_MI,
	NULL
};
static TypeInfo* t5861_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5861_0_0_0;
extern Il2CppType t5861_1_0_0;
struct t5861;
extern Il2CppGenericClass t5861_GC;
TypeInfo t5861_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5861_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5861_TI, t5861_ITIs, NULL, &EmptyCustomAttributesCache, &t5861_TI, &t5861_0_0_0, &t5861_1_0_0, NULL, &t5861_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5860_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.OpenFlags>
extern MethodInfo m30636_MI;
extern MethodInfo m30637_MI;
static PropertyInfo t5860____Item_PropertyInfo = 
{
	&t5860_TI, "Item", &m30636_MI, &m30637_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5860_PIs[] =
{
	&t5860____Item_PropertyInfo,
	NULL
};
extern Il2CppType t775_0_0_0;
static ParameterInfo t5860_m30638_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t775_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30638_GM;
MethodInfo m30638_MI = 
{
	"IndexOf", NULL, &t5860_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5860_m30638_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30638_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t775_0_0_0;
static ParameterInfo t5860_m30639_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t775_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30639_GM;
MethodInfo m30639_MI = 
{
	"Insert", NULL, &t5860_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5860_m30639_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30639_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5860_m30640_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30640_GM;
MethodInfo m30640_MI = 
{
	"RemoveAt", NULL, &t5860_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5860_m30640_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30640_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5860_m30636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t775_0_0_0;
extern void* RuntimeInvoker_t775_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30636_GM;
MethodInfo m30636_MI = 
{
	"get_Item", NULL, &t5860_TI, &t775_0_0_0, RuntimeInvoker_t775_t44, t5860_m30636_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30636_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t775_0_0_0;
static ParameterInfo t5860_m30637_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t775_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30637_GM;
MethodInfo m30637_MI = 
{
	"set_Item", NULL, &t5860_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5860_m30637_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30637_GM};
static MethodInfo* t5860_MIs[] =
{
	&m30638_MI,
	&m30639_MI,
	&m30640_MI,
	&m30636_MI,
	&m30637_MI,
	NULL
};
static TypeInfo* t5860_ITIs[] = 
{
	&t603_TI,
	&t5859_TI,
	&t5861_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5860_0_0_0;
extern Il2CppType t5860_1_0_0;
struct t5860;
extern Il2CppGenericClass t5860_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5860_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5860_MIs, t5860_PIs, NULL, NULL, NULL, NULL, NULL, &t5860_TI, t5860_ITIs, NULL, &t1908__CustomAttributeCache, &t5860_TI, &t5860_0_0_0, &t5860_1_0_0, NULL, &t5860_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4556_TI;

#include "t348.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Byte>
extern MethodInfo m30641_MI;
static PropertyInfo t4556____Current_PropertyInfo = 
{
	&t4556_TI, "Current", &m30641_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4556_PIs[] =
{
	&t4556____Current_PropertyInfo,
	NULL
};
extern Il2CppType t348_0_0_0;
extern void* RuntimeInvoker_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30641_GM;
MethodInfo m30641_MI = 
{
	"get_Current", NULL, &t4556_TI, &t348_0_0_0, RuntimeInvoker_t348, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30641_GM};
static MethodInfo* t4556_MIs[] =
{
	&m30641_MI,
	NULL
};
static TypeInfo* t4556_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4556_0_0_0;
extern Il2CppType t4556_1_0_0;
struct t4556;
extern Il2CppGenericClass t4556_GC;
TypeInfo t4556_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4556_MIs, t4556_PIs, NULL, NULL, NULL, NULL, NULL, &t4556_TI, t4556_ITIs, NULL, &EmptyCustomAttributesCache, &t4556_TI, &t4556_0_0_0, &t4556_1_0_0, NULL, &t4556_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3205.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3205_TI;
#include "t3205MD.h"

extern TypeInfo t348_TI;
extern MethodInfo m17829_MI;
extern MethodInfo m23375_MI;
struct t20;
 uint8_t m23375 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17825_MI;
 void m17825 (t3205 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17826_MI;
 t29 * m17826 (t3205 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m17829(__this, &m17829_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t348_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17827_MI;
 void m17827 (t3205 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17828_MI;
 bool m17828 (t3205 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m17829 (t3205 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m23375(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23375_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Byte>
extern Il2CppType t20_0_0_1;
FieldInfo t3205_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3205_TI, offsetof(t3205, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3205_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3205_TI, offsetof(t3205, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3205_FIs[] =
{
	&t3205_f0_FieldInfo,
	&t3205_f1_FieldInfo,
	NULL
};
static PropertyInfo t3205____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3205_TI, "System.Collections.IEnumerator.Current", &m17826_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3205____Current_PropertyInfo = 
{
	&t3205_TI, "Current", &m17829_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3205_PIs[] =
{
	&t3205____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3205____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3205_m17825_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17825_GM;
MethodInfo m17825_MI = 
{
	".ctor", (methodPointerType)&m17825, &t3205_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3205_m17825_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17825_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17826_GM;
MethodInfo m17826_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17826, &t3205_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17826_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17827_GM;
MethodInfo m17827_MI = 
{
	"Dispose", (methodPointerType)&m17827, &t3205_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17827_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17828_GM;
MethodInfo m17828_MI = 
{
	"MoveNext", (methodPointerType)&m17828, &t3205_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17828_GM};
extern Il2CppType t348_0_0_0;
extern void* RuntimeInvoker_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17829_GM;
MethodInfo m17829_MI = 
{
	"get_Current", (methodPointerType)&m17829, &t3205_TI, &t348_0_0_0, RuntimeInvoker_t348, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17829_GM};
static MethodInfo* t3205_MIs[] =
{
	&m17825_MI,
	&m17826_MI,
	&m17827_MI,
	&m17828_MI,
	&m17829_MI,
	NULL
};
static MethodInfo* t3205_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17826_MI,
	&m17828_MI,
	&m17827_MI,
	&m17829_MI,
};
static TypeInfo* t3205_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4556_TI,
};
static Il2CppInterfaceOffsetPair t3205_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4556_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3205_0_0_0;
extern Il2CppType t3205_1_0_0;
extern Il2CppGenericClass t3205_GC;
TypeInfo t3205_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3205_MIs, t3205_PIs, t3205_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3205_TI, t3205_ITIs, t3205_VT, &EmptyCustomAttributesCache, &t3205_TI, &t3205_0_0_0, &t3205_1_0_0, t3205_IOs, &t3205_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3205)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5862_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Byte>
extern MethodInfo m30642_MI;
static PropertyInfo t5862____Count_PropertyInfo = 
{
	&t5862_TI, "Count", &m30642_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30643_MI;
static PropertyInfo t5862____IsReadOnly_PropertyInfo = 
{
	&t5862_TI, "IsReadOnly", &m30643_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5862_PIs[] =
{
	&t5862____Count_PropertyInfo,
	&t5862____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30642_GM;
MethodInfo m30642_MI = 
{
	"get_Count", NULL, &t5862_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30642_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30643_GM;
MethodInfo m30643_MI = 
{
	"get_IsReadOnly", NULL, &t5862_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30643_GM};
extern Il2CppType t348_0_0_0;
extern Il2CppType t348_0_0_0;
static ParameterInfo t5862_m30644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30644_GM;
MethodInfo m30644_MI = 
{
	"Add", NULL, &t5862_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t5862_m30644_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30644_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30645_GM;
MethodInfo m30645_MI = 
{
	"Clear", NULL, &t5862_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30645_GM};
extern Il2CppType t348_0_0_0;
static ParameterInfo t5862_m30646_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30646_GM;
MethodInfo m30646_MI = 
{
	"Contains", NULL, &t5862_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t5862_m30646_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30646_GM};
extern Il2CppType t781_0_0_0;
extern Il2CppType t781_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5862_m30647_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30647_GM;
MethodInfo m30647_MI = 
{
	"CopyTo", NULL, &t5862_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5862_m30647_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30647_GM};
extern Il2CppType t348_0_0_0;
static ParameterInfo t5862_m30648_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30648_GM;
MethodInfo m30648_MI = 
{
	"Remove", NULL, &t5862_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t5862_m30648_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30648_GM};
static MethodInfo* t5862_MIs[] =
{
	&m30642_MI,
	&m30643_MI,
	&m30644_MI,
	&m30645_MI,
	&m30646_MI,
	&m30647_MI,
	&m30648_MI,
	NULL
};
extern TypeInfo t5864_TI;
static TypeInfo* t5862_ITIs[] = 
{
	&t603_TI,
	&t5864_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5862_0_0_0;
extern Il2CppType t5862_1_0_0;
struct t5862;
extern Il2CppGenericClass t5862_GC;
TypeInfo t5862_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5862_MIs, t5862_PIs, NULL, NULL, NULL, NULL, NULL, &t5862_TI, t5862_ITIs, NULL, &EmptyCustomAttributesCache, &t5862_TI, &t5862_0_0_0, &t5862_1_0_0, NULL, &t5862_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Byte>
extern Il2CppType t4556_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30649_GM;
MethodInfo m30649_MI = 
{
	"GetEnumerator", NULL, &t5864_TI, &t4556_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30649_GM};
static MethodInfo* t5864_MIs[] =
{
	&m30649_MI,
	NULL
};
static TypeInfo* t5864_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5864_0_0_0;
extern Il2CppType t5864_1_0_0;
struct t5864;
extern Il2CppGenericClass t5864_GC;
TypeInfo t5864_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5864_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5864_TI, t5864_ITIs, NULL, &EmptyCustomAttributesCache, &t5864_TI, &t5864_0_0_0, &t5864_1_0_0, NULL, &t5864_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5863_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Byte>
extern MethodInfo m30650_MI;
extern MethodInfo m30651_MI;
static PropertyInfo t5863____Item_PropertyInfo = 
{
	&t5863_TI, "Item", &m30650_MI, &m30651_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5863_PIs[] =
{
	&t5863____Item_PropertyInfo,
	NULL
};
extern Il2CppType t348_0_0_0;
static ParameterInfo t5863_m30652_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30652_GM;
MethodInfo m30652_MI = 
{
	"IndexOf", NULL, &t5863_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t5863_m30652_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30652_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t348_0_0_0;
static ParameterInfo t5863_m30653_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30653_GM;
MethodInfo m30653_MI = 
{
	"Insert", NULL, &t5863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t5863_m30653_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30653_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5863_m30654_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30654_GM;
MethodInfo m30654_MI = 
{
	"RemoveAt", NULL, &t5863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5863_m30654_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30654_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5863_m30650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t348_0_0_0;
extern void* RuntimeInvoker_t348_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30650_GM;
MethodInfo m30650_MI = 
{
	"get_Item", NULL, &t5863_TI, &t348_0_0_0, RuntimeInvoker_t348_t44, t5863_m30650_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30650_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t348_0_0_0;
static ParameterInfo t5863_m30651_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30651_GM;
MethodInfo m30651_MI = 
{
	"set_Item", NULL, &t5863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t5863_m30651_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30651_GM};
static MethodInfo* t5863_MIs[] =
{
	&m30652_MI,
	&m30653_MI,
	&m30654_MI,
	&m30650_MI,
	&m30651_MI,
	NULL
};
static TypeInfo* t5863_ITIs[] = 
{
	&t603_TI,
	&t5862_TI,
	&t5864_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5863_0_0_0;
extern Il2CppType t5863_1_0_0;
struct t5863;
extern Il2CppGenericClass t5863_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5863_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5863_MIs, t5863_PIs, NULL, NULL, NULL, NULL, NULL, &t5863_TI, t5863_ITIs, NULL, &t1908__CustomAttributeCache, &t5863_TI, &t5863_0_0_0, &t5863_1_0_0, NULL, &t5863_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5865_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Byte>>
extern MethodInfo m30655_MI;
static PropertyInfo t5865____Count_PropertyInfo = 
{
	&t5865_TI, "Count", &m30655_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30656_MI;
static PropertyInfo t5865____IsReadOnly_PropertyInfo = 
{
	&t5865_TI, "IsReadOnly", &m30656_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5865_PIs[] =
{
	&t5865____Count_PropertyInfo,
	&t5865____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30655_GM;
MethodInfo m30655_MI = 
{
	"get_Count", NULL, &t5865_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30655_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30656_GM;
MethodInfo m30656_MI = 
{
	"get_IsReadOnly", NULL, &t5865_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30656_GM};
extern Il2CppType t1721_0_0_0;
extern Il2CppType t1721_0_0_0;
static ParameterInfo t5865_m30657_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1721_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30657_GM;
MethodInfo m30657_MI = 
{
	"Add", NULL, &t5865_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5865_m30657_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30657_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30658_GM;
MethodInfo m30658_MI = 
{
	"Clear", NULL, &t5865_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30658_GM};
extern Il2CppType t1721_0_0_0;
static ParameterInfo t5865_m30659_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1721_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30659_GM;
MethodInfo m30659_MI = 
{
	"Contains", NULL, &t5865_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5865_m30659_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30659_GM};
extern Il2CppType t3553_0_0_0;
extern Il2CppType t3553_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5865_m30660_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3553_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30660_GM;
MethodInfo m30660_MI = 
{
	"CopyTo", NULL, &t5865_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5865_m30660_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30660_GM};
extern Il2CppType t1721_0_0_0;
static ParameterInfo t5865_m30661_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1721_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30661_GM;
MethodInfo m30661_MI = 
{
	"Remove", NULL, &t5865_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5865_m30661_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30661_GM};
static MethodInfo* t5865_MIs[] =
{
	&m30655_MI,
	&m30656_MI,
	&m30657_MI,
	&m30658_MI,
	&m30659_MI,
	&m30660_MI,
	&m30661_MI,
	NULL
};
extern TypeInfo t5867_TI;
static TypeInfo* t5865_ITIs[] = 
{
	&t603_TI,
	&t5867_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5865_0_0_0;
extern Il2CppType t5865_1_0_0;
struct t5865;
extern Il2CppGenericClass t5865_GC;
TypeInfo t5865_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5865_MIs, t5865_PIs, NULL, NULL, NULL, NULL, NULL, &t5865_TI, t5865_ITIs, NULL, &EmptyCustomAttributesCache, &t5865_TI, &t5865_0_0_0, &t5865_1_0_0, NULL, &t5865_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Byte>>
extern Il2CppType t4558_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30662_GM;
MethodInfo m30662_MI = 
{
	"GetEnumerator", NULL, &t5867_TI, &t4558_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30662_GM};
static MethodInfo* t5867_MIs[] =
{
	&m30662_MI,
	NULL
};
static TypeInfo* t5867_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5867_0_0_0;
extern Il2CppType t5867_1_0_0;
struct t5867;
extern Il2CppGenericClass t5867_GC;
TypeInfo t5867_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5867_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5867_TI, t5867_ITIs, NULL, &EmptyCustomAttributesCache, &t5867_TI, &t5867_0_0_0, &t5867_1_0_0, NULL, &t5867_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4558_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Byte>>
extern MethodInfo m30663_MI;
static PropertyInfo t4558____Current_PropertyInfo = 
{
	&t4558_TI, "Current", &m30663_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4558_PIs[] =
{
	&t4558____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30663_GM;
MethodInfo m30663_MI = 
{
	"get_Current", NULL, &t4558_TI, &t1721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30663_GM};
static MethodInfo* t4558_MIs[] =
{
	&m30663_MI,
	NULL
};
static TypeInfo* t4558_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4558_0_0_0;
extern Il2CppType t4558_1_0_0;
struct t4558;
extern Il2CppGenericClass t4558_GC;
TypeInfo t4558_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4558_MIs, t4558_PIs, NULL, NULL, NULL, NULL, NULL, &t4558_TI, t4558_ITIs, NULL, &EmptyCustomAttributesCache, &t4558_TI, &t4558_0_0_0, &t4558_1_0_0, NULL, &t4558_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1721_TI;



// Metadata Definition System.IComparable`1<System.Byte>
extern Il2CppType t348_0_0_0;
static ParameterInfo t1721_m30664_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30664_GM;
MethodInfo m30664_MI = 
{
	"CompareTo", NULL, &t1721_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t1721_m30664_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30664_GM};
static MethodInfo* t1721_MIs[] =
{
	&m30664_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1721_1_0_0;
struct t1721;
extern Il2CppGenericClass t1721_GC;
TypeInfo t1721_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1721_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1721_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1721_TI, &t1721_0_0_0, &t1721_1_0_0, NULL, &t1721_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3206.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3206_TI;
#include "t3206MD.h"

extern MethodInfo m17834_MI;
extern MethodInfo m23386_MI;
struct t20;
#define m23386(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Byte>>
extern Il2CppType t20_0_0_1;
FieldInfo t3206_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3206_TI, offsetof(t3206, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3206_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3206_TI, offsetof(t3206, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3206_FIs[] =
{
	&t3206_f0_FieldInfo,
	&t3206_f1_FieldInfo,
	NULL
};
extern MethodInfo m17831_MI;
static PropertyInfo t3206____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3206_TI, "System.Collections.IEnumerator.Current", &m17831_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3206____Current_PropertyInfo = 
{
	&t3206_TI, "Current", &m17834_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3206_PIs[] =
{
	&t3206____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3206____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3206_m17830_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17830_GM;
MethodInfo m17830_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3206_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3206_m17830_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17830_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17831_GM;
MethodInfo m17831_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3206_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17831_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17832_GM;
MethodInfo m17832_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3206_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17832_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17833_GM;
MethodInfo m17833_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3206_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17833_GM};
extern Il2CppType t1721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17834_GM;
MethodInfo m17834_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3206_TI, &t1721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17834_GM};
static MethodInfo* t3206_MIs[] =
{
	&m17830_MI,
	&m17831_MI,
	&m17832_MI,
	&m17833_MI,
	&m17834_MI,
	NULL
};
extern MethodInfo m17833_MI;
extern MethodInfo m17832_MI;
static MethodInfo* t3206_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17831_MI,
	&m17833_MI,
	&m17832_MI,
	&m17834_MI,
};
static TypeInfo* t3206_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4558_TI,
};
static Il2CppInterfaceOffsetPair t3206_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4558_TI, 7},
};
extern TypeInfo t1721_TI;
static Il2CppRGCTXData t3206_RGCTXData[3] = 
{
	&m17834_MI/* Method Usage */,
	&t1721_TI/* Class Usage */,
	&m23386_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3206_0_0_0;
extern Il2CppType t3206_1_0_0;
extern Il2CppGenericClass t3206_GC;
TypeInfo t3206_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3206_MIs, t3206_PIs, t3206_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3206_TI, t3206_ITIs, t3206_VT, &EmptyCustomAttributesCache, &t3206_TI, &t3206_0_0_0, &t3206_1_0_0, t3206_IOs, &t3206_GC, NULL, NULL, NULL, t3206_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3206)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5866_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Byte>>
extern MethodInfo m30665_MI;
extern MethodInfo m30666_MI;
static PropertyInfo t5866____Item_PropertyInfo = 
{
	&t5866_TI, "Item", &m30665_MI, &m30666_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5866_PIs[] =
{
	&t5866____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1721_0_0_0;
static ParameterInfo t5866_m30667_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1721_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30667_GM;
MethodInfo m30667_MI = 
{
	"IndexOf", NULL, &t5866_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5866_m30667_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30667_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1721_0_0_0;
static ParameterInfo t5866_m30668_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1721_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30668_GM;
MethodInfo m30668_MI = 
{
	"Insert", NULL, &t5866_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5866_m30668_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30668_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5866_m30669_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30669_GM;
MethodInfo m30669_MI = 
{
	"RemoveAt", NULL, &t5866_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5866_m30669_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30669_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5866_m30665_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1721_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30665_GM;
MethodInfo m30665_MI = 
{
	"get_Item", NULL, &t5866_TI, &t1721_0_0_0, RuntimeInvoker_t29_t44, t5866_m30665_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30665_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1721_0_0_0;
static ParameterInfo t5866_m30666_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1721_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30666_GM;
MethodInfo m30666_MI = 
{
	"set_Item", NULL, &t5866_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5866_m30666_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30666_GM};
static MethodInfo* t5866_MIs[] =
{
	&m30667_MI,
	&m30668_MI,
	&m30669_MI,
	&m30665_MI,
	&m30666_MI,
	NULL
};
static TypeInfo* t5866_ITIs[] = 
{
	&t603_TI,
	&t5865_TI,
	&t5867_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5866_0_0_0;
extern Il2CppType t5866_1_0_0;
struct t5866;
extern Il2CppGenericClass t5866_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5866_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5866_MIs, t5866_PIs, NULL, NULL, NULL, NULL, NULL, &t5866_TI, t5866_ITIs, NULL, &t1908__CustomAttributeCache, &t5866_TI, &t5866_0_0_0, &t5866_1_0_0, NULL, &t5866_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5868_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Byte>>
extern MethodInfo m30670_MI;
static PropertyInfo t5868____Count_PropertyInfo = 
{
	&t5868_TI, "Count", &m30670_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30671_MI;
static PropertyInfo t5868____IsReadOnly_PropertyInfo = 
{
	&t5868_TI, "IsReadOnly", &m30671_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5868_PIs[] =
{
	&t5868____Count_PropertyInfo,
	&t5868____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30670_GM;
MethodInfo m30670_MI = 
{
	"get_Count", NULL, &t5868_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30670_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30671_GM;
MethodInfo m30671_MI = 
{
	"get_IsReadOnly", NULL, &t5868_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30671_GM};
extern Il2CppType t1722_0_0_0;
extern Il2CppType t1722_0_0_0;
static ParameterInfo t5868_m30672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1722_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30672_GM;
MethodInfo m30672_MI = 
{
	"Add", NULL, &t5868_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5868_m30672_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30672_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30673_GM;
MethodInfo m30673_MI = 
{
	"Clear", NULL, &t5868_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30673_GM};
extern Il2CppType t1722_0_0_0;
static ParameterInfo t5868_m30674_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1722_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30674_GM;
MethodInfo m30674_MI = 
{
	"Contains", NULL, &t5868_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5868_m30674_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30674_GM};
extern Il2CppType t3554_0_0_0;
extern Il2CppType t3554_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5868_m30675_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3554_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30675_GM;
MethodInfo m30675_MI = 
{
	"CopyTo", NULL, &t5868_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5868_m30675_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30675_GM};
extern Il2CppType t1722_0_0_0;
static ParameterInfo t5868_m30676_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1722_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30676_GM;
MethodInfo m30676_MI = 
{
	"Remove", NULL, &t5868_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5868_m30676_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30676_GM};
static MethodInfo* t5868_MIs[] =
{
	&m30670_MI,
	&m30671_MI,
	&m30672_MI,
	&m30673_MI,
	&m30674_MI,
	&m30675_MI,
	&m30676_MI,
	NULL
};
extern TypeInfo t5870_TI;
static TypeInfo* t5868_ITIs[] = 
{
	&t603_TI,
	&t5870_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5868_0_0_0;
extern Il2CppType t5868_1_0_0;
struct t5868;
extern Il2CppGenericClass t5868_GC;
TypeInfo t5868_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5868_MIs, t5868_PIs, NULL, NULL, NULL, NULL, NULL, &t5868_TI, t5868_ITIs, NULL, &EmptyCustomAttributesCache, &t5868_TI, &t5868_0_0_0, &t5868_1_0_0, NULL, &t5868_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Byte>>
extern Il2CppType t4560_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30677_GM;
MethodInfo m30677_MI = 
{
	"GetEnumerator", NULL, &t5870_TI, &t4560_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30677_GM};
static MethodInfo* t5870_MIs[] =
{
	&m30677_MI,
	NULL
};
static TypeInfo* t5870_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5870_0_0_0;
extern Il2CppType t5870_1_0_0;
struct t5870;
extern Il2CppGenericClass t5870_GC;
TypeInfo t5870_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5870_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5870_TI, t5870_ITIs, NULL, &EmptyCustomAttributesCache, &t5870_TI, &t5870_0_0_0, &t5870_1_0_0, NULL, &t5870_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4560_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Byte>>
extern MethodInfo m30678_MI;
static PropertyInfo t4560____Current_PropertyInfo = 
{
	&t4560_TI, "Current", &m30678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4560_PIs[] =
{
	&t4560____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30678_GM;
MethodInfo m30678_MI = 
{
	"get_Current", NULL, &t4560_TI, &t1722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30678_GM};
static MethodInfo* t4560_MIs[] =
{
	&m30678_MI,
	NULL
};
static TypeInfo* t4560_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4560_0_0_0;
extern Il2CppType t4560_1_0_0;
struct t4560;
extern Il2CppGenericClass t4560_GC;
TypeInfo t4560_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4560_MIs, t4560_PIs, NULL, NULL, NULL, NULL, NULL, &t4560_TI, t4560_ITIs, NULL, &EmptyCustomAttributesCache, &t4560_TI, &t4560_0_0_0, &t4560_1_0_0, NULL, &t4560_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1722_TI;



// Metadata Definition System.IEquatable`1<System.Byte>
extern Il2CppType t348_0_0_0;
static ParameterInfo t1722_m30679_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t348_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30679_GM;
MethodInfo m30679_MI = 
{
	"Equals", NULL, &t1722_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t1722_m30679_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30679_GM};
static MethodInfo* t1722_MIs[] =
{
	&m30679_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1722_1_0_0;
struct t1722;
extern Il2CppGenericClass t1722_GC;
TypeInfo t1722_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1722_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1722_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1722_TI, &t1722_0_0_0, &t1722_1_0_0, NULL, &t1722_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3207.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3207_TI;
#include "t3207MD.h"

extern MethodInfo m17839_MI;
extern MethodInfo m23397_MI;
struct t20;
#define m23397(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Byte>>
extern Il2CppType t20_0_0_1;
FieldInfo t3207_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3207_TI, offsetof(t3207, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3207_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3207_TI, offsetof(t3207, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3207_FIs[] =
{
	&t3207_f0_FieldInfo,
	&t3207_f1_FieldInfo,
	NULL
};
extern MethodInfo m17836_MI;
static PropertyInfo t3207____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3207_TI, "System.Collections.IEnumerator.Current", &m17836_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3207____Current_PropertyInfo = 
{
	&t3207_TI, "Current", &m17839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3207_PIs[] =
{
	&t3207____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3207____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3207_m17835_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17835_GM;
MethodInfo m17835_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3207_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3207_m17835_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17835_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17836_GM;
MethodInfo m17836_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3207_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17836_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17837_GM;
MethodInfo m17837_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3207_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17837_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17838_GM;
MethodInfo m17838_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3207_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17838_GM};
extern Il2CppType t1722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17839_GM;
MethodInfo m17839_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3207_TI, &t1722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17839_GM};
static MethodInfo* t3207_MIs[] =
{
	&m17835_MI,
	&m17836_MI,
	&m17837_MI,
	&m17838_MI,
	&m17839_MI,
	NULL
};
extern MethodInfo m17838_MI;
extern MethodInfo m17837_MI;
static MethodInfo* t3207_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17836_MI,
	&m17838_MI,
	&m17837_MI,
	&m17839_MI,
};
static TypeInfo* t3207_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4560_TI,
};
static Il2CppInterfaceOffsetPair t3207_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4560_TI, 7},
};
extern TypeInfo t1722_TI;
static Il2CppRGCTXData t3207_RGCTXData[3] = 
{
	&m17839_MI/* Method Usage */,
	&t1722_TI/* Class Usage */,
	&m23397_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3207_0_0_0;
extern Il2CppType t3207_1_0_0;
extern Il2CppGenericClass t3207_GC;
TypeInfo t3207_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3207_MIs, t3207_PIs, t3207_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3207_TI, t3207_ITIs, t3207_VT, &EmptyCustomAttributesCache, &t3207_TI, &t3207_0_0_0, &t3207_1_0_0, t3207_IOs, &t3207_GC, NULL, NULL, NULL, t3207_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3207)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5869_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Byte>>
extern MethodInfo m30680_MI;
extern MethodInfo m30681_MI;
static PropertyInfo t5869____Item_PropertyInfo = 
{
	&t5869_TI, "Item", &m30680_MI, &m30681_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5869_PIs[] =
{
	&t5869____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1722_0_0_0;
static ParameterInfo t5869_m30682_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1722_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30682_GM;
MethodInfo m30682_MI = 
{
	"IndexOf", NULL, &t5869_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5869_m30682_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30682_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1722_0_0_0;
static ParameterInfo t5869_m30683_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1722_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30683_GM;
MethodInfo m30683_MI = 
{
	"Insert", NULL, &t5869_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5869_m30683_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30683_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5869_m30684_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30684_GM;
MethodInfo m30684_MI = 
{
	"RemoveAt", NULL, &t5869_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5869_m30684_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30684_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5869_m30680_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1722_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30680_GM;
MethodInfo m30680_MI = 
{
	"get_Item", NULL, &t5869_TI, &t1722_0_0_0, RuntimeInvoker_t29_t44, t5869_m30680_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30680_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1722_0_0_0;
static ParameterInfo t5869_m30681_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1722_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30681_GM;
MethodInfo m30681_MI = 
{
	"set_Item", NULL, &t5869_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5869_m30681_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30681_GM};
static MethodInfo* t5869_MIs[] =
{
	&m30682_MI,
	&m30683_MI,
	&m30684_MI,
	&m30680_MI,
	&m30681_MI,
	NULL
};
static TypeInfo* t5869_ITIs[] = 
{
	&t603_TI,
	&t5868_TI,
	&t5870_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5869_0_0_0;
extern Il2CppType t5869_1_0_0;
struct t5869;
extern Il2CppGenericClass t5869_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5869_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5869_MIs, t5869_PIs, NULL, NULL, NULL, NULL, NULL, &t5869_TI, t5869_ITIs, NULL, &t1908__CustomAttributeCache, &t5869_TI, &t5869_0_0_0, &t5869_1_0_0, NULL, &t5869_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4562_TI;

#include "t784.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.StoreLocation>
extern MethodInfo m30685_MI;
static PropertyInfo t4562____Current_PropertyInfo = 
{
	&t4562_TI, "Current", &m30685_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4562_PIs[] =
{
	&t4562____Current_PropertyInfo,
	NULL
};
extern Il2CppType t784_0_0_0;
extern void* RuntimeInvoker_t784 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30685_GM;
MethodInfo m30685_MI = 
{
	"get_Current", NULL, &t4562_TI, &t784_0_0_0, RuntimeInvoker_t784, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30685_GM};
static MethodInfo* t4562_MIs[] =
{
	&m30685_MI,
	NULL
};
static TypeInfo* t4562_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4562_0_0_0;
extern Il2CppType t4562_1_0_0;
struct t4562;
extern Il2CppGenericClass t4562_GC;
TypeInfo t4562_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4562_MIs, t4562_PIs, NULL, NULL, NULL, NULL, NULL, &t4562_TI, t4562_ITIs, NULL, &EmptyCustomAttributesCache, &t4562_TI, &t4562_0_0_0, &t4562_1_0_0, NULL, &t4562_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3208.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3208_TI;
#include "t3208MD.h"

extern TypeInfo t784_TI;
extern MethodInfo m17844_MI;
extern MethodInfo m23408_MI;
struct t20;
 int32_t m23408 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17840_MI;
 void m17840 (t3208 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17841_MI;
 t29 * m17841 (t3208 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17844(__this, &m17844_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t784_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17842_MI;
 void m17842 (t3208 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17843_MI;
 bool m17843 (t3208 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17844 (t3208 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23408(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23408_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreLocation>
extern Il2CppType t20_0_0_1;
FieldInfo t3208_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3208_TI, offsetof(t3208, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3208_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3208_TI, offsetof(t3208, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3208_FIs[] =
{
	&t3208_f0_FieldInfo,
	&t3208_f1_FieldInfo,
	NULL
};
static PropertyInfo t3208____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3208_TI, "System.Collections.IEnumerator.Current", &m17841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3208____Current_PropertyInfo = 
{
	&t3208_TI, "Current", &m17844_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3208_PIs[] =
{
	&t3208____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3208____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3208_m17840_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17840_GM;
MethodInfo m17840_MI = 
{
	".ctor", (methodPointerType)&m17840, &t3208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3208_m17840_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17840_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17841_GM;
MethodInfo m17841_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17841, &t3208_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17841_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17842_GM;
MethodInfo m17842_MI = 
{
	"Dispose", (methodPointerType)&m17842, &t3208_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17842_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17843_GM;
MethodInfo m17843_MI = 
{
	"MoveNext", (methodPointerType)&m17843, &t3208_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17843_GM};
extern Il2CppType t784_0_0_0;
extern void* RuntimeInvoker_t784 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17844_GM;
MethodInfo m17844_MI = 
{
	"get_Current", (methodPointerType)&m17844, &t3208_TI, &t784_0_0_0, RuntimeInvoker_t784, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17844_GM};
static MethodInfo* t3208_MIs[] =
{
	&m17840_MI,
	&m17841_MI,
	&m17842_MI,
	&m17843_MI,
	&m17844_MI,
	NULL
};
static MethodInfo* t3208_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17841_MI,
	&m17843_MI,
	&m17842_MI,
	&m17844_MI,
};
static TypeInfo* t3208_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4562_TI,
};
static Il2CppInterfaceOffsetPair t3208_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4562_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3208_0_0_0;
extern Il2CppType t3208_1_0_0;
extern Il2CppGenericClass t3208_GC;
TypeInfo t3208_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3208_MIs, t3208_PIs, t3208_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3208_TI, t3208_ITIs, t3208_VT, &EmptyCustomAttributesCache, &t3208_TI, &t3208_0_0_0, &t3208_1_0_0, t3208_IOs, &t3208_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3208)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5871_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.StoreLocation>
extern MethodInfo m30686_MI;
static PropertyInfo t5871____Count_PropertyInfo = 
{
	&t5871_TI, "Count", &m30686_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30687_MI;
static PropertyInfo t5871____IsReadOnly_PropertyInfo = 
{
	&t5871_TI, "IsReadOnly", &m30687_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5871_PIs[] =
{
	&t5871____Count_PropertyInfo,
	&t5871____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30686_GM;
MethodInfo m30686_MI = 
{
	"get_Count", NULL, &t5871_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30686_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30687_GM;
MethodInfo m30687_MI = 
{
	"get_IsReadOnly", NULL, &t5871_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30687_GM};
extern Il2CppType t784_0_0_0;
extern Il2CppType t784_0_0_0;
static ParameterInfo t5871_m30688_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t784_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30688_GM;
MethodInfo m30688_MI = 
{
	"Add", NULL, &t5871_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5871_m30688_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30688_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30689_GM;
MethodInfo m30689_MI = 
{
	"Clear", NULL, &t5871_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30689_GM};
extern Il2CppType t784_0_0_0;
static ParameterInfo t5871_m30690_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t784_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30690_GM;
MethodInfo m30690_MI = 
{
	"Contains", NULL, &t5871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5871_m30690_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30690_GM};
extern Il2CppType t3905_0_0_0;
extern Il2CppType t3905_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5871_m30691_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3905_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30691_GM;
MethodInfo m30691_MI = 
{
	"CopyTo", NULL, &t5871_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5871_m30691_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30691_GM};
extern Il2CppType t784_0_0_0;
static ParameterInfo t5871_m30692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t784_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30692_GM;
MethodInfo m30692_MI = 
{
	"Remove", NULL, &t5871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5871_m30692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30692_GM};
static MethodInfo* t5871_MIs[] =
{
	&m30686_MI,
	&m30687_MI,
	&m30688_MI,
	&m30689_MI,
	&m30690_MI,
	&m30691_MI,
	&m30692_MI,
	NULL
};
extern TypeInfo t5873_TI;
static TypeInfo* t5871_ITIs[] = 
{
	&t603_TI,
	&t5873_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5871_0_0_0;
extern Il2CppType t5871_1_0_0;
struct t5871;
extern Il2CppGenericClass t5871_GC;
TypeInfo t5871_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5871_MIs, t5871_PIs, NULL, NULL, NULL, NULL, NULL, &t5871_TI, t5871_ITIs, NULL, &EmptyCustomAttributesCache, &t5871_TI, &t5871_0_0_0, &t5871_1_0_0, NULL, &t5871_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.StoreLocation>
extern Il2CppType t4562_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30693_GM;
MethodInfo m30693_MI = 
{
	"GetEnumerator", NULL, &t5873_TI, &t4562_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30693_GM};
static MethodInfo* t5873_MIs[] =
{
	&m30693_MI,
	NULL
};
static TypeInfo* t5873_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5873_0_0_0;
extern Il2CppType t5873_1_0_0;
struct t5873;
extern Il2CppGenericClass t5873_GC;
TypeInfo t5873_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5873_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5873_TI, t5873_ITIs, NULL, &EmptyCustomAttributesCache, &t5873_TI, &t5873_0_0_0, &t5873_1_0_0, NULL, &t5873_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5872_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.StoreLocation>
extern MethodInfo m30694_MI;
extern MethodInfo m30695_MI;
static PropertyInfo t5872____Item_PropertyInfo = 
{
	&t5872_TI, "Item", &m30694_MI, &m30695_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5872_PIs[] =
{
	&t5872____Item_PropertyInfo,
	NULL
};
extern Il2CppType t784_0_0_0;
static ParameterInfo t5872_m30696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t784_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30696_GM;
MethodInfo m30696_MI = 
{
	"IndexOf", NULL, &t5872_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5872_m30696_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30696_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t784_0_0_0;
static ParameterInfo t5872_m30697_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t784_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30697_GM;
MethodInfo m30697_MI = 
{
	"Insert", NULL, &t5872_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5872_m30697_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30697_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5872_m30698_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30698_GM;
MethodInfo m30698_MI = 
{
	"RemoveAt", NULL, &t5872_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5872_m30698_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30698_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5872_m30694_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t784_0_0_0;
extern void* RuntimeInvoker_t784_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30694_GM;
MethodInfo m30694_MI = 
{
	"get_Item", NULL, &t5872_TI, &t784_0_0_0, RuntimeInvoker_t784_t44, t5872_m30694_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30694_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t784_0_0_0;
static ParameterInfo t5872_m30695_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t784_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30695_GM;
MethodInfo m30695_MI = 
{
	"set_Item", NULL, &t5872_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5872_m30695_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30695_GM};
static MethodInfo* t5872_MIs[] =
{
	&m30696_MI,
	&m30697_MI,
	&m30698_MI,
	&m30694_MI,
	&m30695_MI,
	NULL
};
static TypeInfo* t5872_ITIs[] = 
{
	&t603_TI,
	&t5871_TI,
	&t5873_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5872_0_0_0;
extern Il2CppType t5872_1_0_0;
struct t5872;
extern Il2CppGenericClass t5872_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5872_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5872_MIs, t5872_PIs, NULL, NULL, NULL, NULL, NULL, &t5872_TI, t5872_ITIs, NULL, &t1908__CustomAttributeCache, &t5872_TI, &t5872_0_0_0, &t5872_1_0_0, NULL, &t5872_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4564_TI;

#include "t785.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>
extern MethodInfo m30699_MI;
static PropertyInfo t4564____Current_PropertyInfo = 
{
	&t4564_TI, "Current", &m30699_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4564_PIs[] =
{
	&t4564____Current_PropertyInfo,
	NULL
};
extern Il2CppType t785_0_0_0;
extern void* RuntimeInvoker_t785 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30699_GM;
MethodInfo m30699_MI = 
{
	"get_Current", NULL, &t4564_TI, &t785_0_0_0, RuntimeInvoker_t785, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30699_GM};
static MethodInfo* t4564_MIs[] =
{
	&m30699_MI,
	NULL
};
static TypeInfo* t4564_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4564_0_0_0;
extern Il2CppType t4564_1_0_0;
struct t4564;
extern Il2CppGenericClass t4564_GC;
TypeInfo t4564_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4564_MIs, t4564_PIs, NULL, NULL, NULL, NULL, NULL, &t4564_TI, t4564_ITIs, NULL, &EmptyCustomAttributesCache, &t4564_TI, &t4564_0_0_0, &t4564_1_0_0, NULL, &t4564_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3209.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3209_TI;
#include "t3209MD.h"

extern TypeInfo t785_TI;
extern MethodInfo m17849_MI;
extern MethodInfo m23419_MI;
struct t20;
 int32_t m23419 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17845_MI;
 void m17845 (t3209 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17846_MI;
 t29 * m17846 (t3209 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17849(__this, &m17849_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t785_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17847_MI;
 void m17847 (t3209 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17848_MI;
 bool m17848 (t3209 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17849 (t3209 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23419(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23419_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>
extern Il2CppType t20_0_0_1;
FieldInfo t3209_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3209_TI, offsetof(t3209, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3209_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3209_TI, offsetof(t3209, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3209_FIs[] =
{
	&t3209_f0_FieldInfo,
	&t3209_f1_FieldInfo,
	NULL
};
static PropertyInfo t3209____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3209_TI, "System.Collections.IEnumerator.Current", &m17846_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3209____Current_PropertyInfo = 
{
	&t3209_TI, "Current", &m17849_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3209_PIs[] =
{
	&t3209____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3209____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3209_m17845_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17845_GM;
MethodInfo m17845_MI = 
{
	".ctor", (methodPointerType)&m17845, &t3209_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3209_m17845_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17845_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17846_GM;
MethodInfo m17846_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17846, &t3209_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17846_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17847_GM;
MethodInfo m17847_MI = 
{
	"Dispose", (methodPointerType)&m17847, &t3209_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17847_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17848_GM;
MethodInfo m17848_MI = 
{
	"MoveNext", (methodPointerType)&m17848, &t3209_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17848_GM};
extern Il2CppType t785_0_0_0;
extern void* RuntimeInvoker_t785 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17849_GM;
MethodInfo m17849_MI = 
{
	"get_Current", (methodPointerType)&m17849, &t3209_TI, &t785_0_0_0, RuntimeInvoker_t785, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17849_GM};
static MethodInfo* t3209_MIs[] =
{
	&m17845_MI,
	&m17846_MI,
	&m17847_MI,
	&m17848_MI,
	&m17849_MI,
	NULL
};
static MethodInfo* t3209_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17846_MI,
	&m17848_MI,
	&m17847_MI,
	&m17849_MI,
};
static TypeInfo* t3209_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4564_TI,
};
static Il2CppInterfaceOffsetPair t3209_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4564_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3209_0_0_0;
extern Il2CppType t3209_1_0_0;
extern Il2CppGenericClass t3209_GC;
TypeInfo t3209_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3209_MIs, t3209_PIs, t3209_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3209_TI, t3209_ITIs, t3209_VT, &EmptyCustomAttributesCache, &t3209_TI, &t3209_0_0_0, &t3209_1_0_0, t3209_IOs, &t3209_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3209)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5874_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.StoreName>
extern MethodInfo m30700_MI;
static PropertyInfo t5874____Count_PropertyInfo = 
{
	&t5874_TI, "Count", &m30700_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30701_MI;
static PropertyInfo t5874____IsReadOnly_PropertyInfo = 
{
	&t5874_TI, "IsReadOnly", &m30701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5874_PIs[] =
{
	&t5874____Count_PropertyInfo,
	&t5874____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30700_GM;
MethodInfo m30700_MI = 
{
	"get_Count", NULL, &t5874_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30700_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30701_GM;
MethodInfo m30701_MI = 
{
	"get_IsReadOnly", NULL, &t5874_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30701_GM};
extern Il2CppType t785_0_0_0;
extern Il2CppType t785_0_0_0;
static ParameterInfo t5874_m30702_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t785_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30702_GM;
MethodInfo m30702_MI = 
{
	"Add", NULL, &t5874_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5874_m30702_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30702_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30703_GM;
MethodInfo m30703_MI = 
{
	"Clear", NULL, &t5874_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30703_GM};
extern Il2CppType t785_0_0_0;
static ParameterInfo t5874_m30704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t785_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30704_GM;
MethodInfo m30704_MI = 
{
	"Contains", NULL, &t5874_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5874_m30704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30704_GM};
extern Il2CppType t3906_0_0_0;
extern Il2CppType t3906_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5874_m30705_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3906_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30705_GM;
MethodInfo m30705_MI = 
{
	"CopyTo", NULL, &t5874_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5874_m30705_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30705_GM};
extern Il2CppType t785_0_0_0;
static ParameterInfo t5874_m30706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t785_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30706_GM;
MethodInfo m30706_MI = 
{
	"Remove", NULL, &t5874_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5874_m30706_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30706_GM};
static MethodInfo* t5874_MIs[] =
{
	&m30700_MI,
	&m30701_MI,
	&m30702_MI,
	&m30703_MI,
	&m30704_MI,
	&m30705_MI,
	&m30706_MI,
	NULL
};
extern TypeInfo t5876_TI;
static TypeInfo* t5874_ITIs[] = 
{
	&t603_TI,
	&t5876_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5874_0_0_0;
extern Il2CppType t5874_1_0_0;
struct t5874;
extern Il2CppGenericClass t5874_GC;
TypeInfo t5874_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5874_MIs, t5874_PIs, NULL, NULL, NULL, NULL, NULL, &t5874_TI, t5874_ITIs, NULL, &EmptyCustomAttributesCache, &t5874_TI, &t5874_0_0_0, &t5874_1_0_0, NULL, &t5874_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.StoreName>
extern Il2CppType t4564_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30707_GM;
MethodInfo m30707_MI = 
{
	"GetEnumerator", NULL, &t5876_TI, &t4564_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30707_GM};
static MethodInfo* t5876_MIs[] =
{
	&m30707_MI,
	NULL
};
static TypeInfo* t5876_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5876_0_0_0;
extern Il2CppType t5876_1_0_0;
struct t5876;
extern Il2CppGenericClass t5876_GC;
TypeInfo t5876_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5876_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5876_TI, t5876_ITIs, NULL, &EmptyCustomAttributesCache, &t5876_TI, &t5876_0_0_0, &t5876_1_0_0, NULL, &t5876_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5875_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.StoreName>
extern MethodInfo m30708_MI;
extern MethodInfo m30709_MI;
static PropertyInfo t5875____Item_PropertyInfo = 
{
	&t5875_TI, "Item", &m30708_MI, &m30709_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5875_PIs[] =
{
	&t5875____Item_PropertyInfo,
	NULL
};
extern Il2CppType t785_0_0_0;
static ParameterInfo t5875_m30710_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t785_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30710_GM;
MethodInfo m30710_MI = 
{
	"IndexOf", NULL, &t5875_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5875_m30710_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30710_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t785_0_0_0;
static ParameterInfo t5875_m30711_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t785_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30711_GM;
MethodInfo m30711_MI = 
{
	"Insert", NULL, &t5875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5875_m30711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30711_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5875_m30712_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30712_GM;
MethodInfo m30712_MI = 
{
	"RemoveAt", NULL, &t5875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5875_m30712_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30712_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5875_m30708_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t785_0_0_0;
extern void* RuntimeInvoker_t785_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30708_GM;
MethodInfo m30708_MI = 
{
	"get_Item", NULL, &t5875_TI, &t785_0_0_0, RuntimeInvoker_t785_t44, t5875_m30708_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30708_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t785_0_0_0;
static ParameterInfo t5875_m30709_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t785_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30709_GM;
MethodInfo m30709_MI = 
{
	"set_Item", NULL, &t5875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5875_m30709_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30709_GM};
static MethodInfo* t5875_MIs[] =
{
	&m30710_MI,
	&m30711_MI,
	&m30712_MI,
	&m30708_MI,
	&m30709_MI,
	NULL
};
static TypeInfo* t5875_ITIs[] = 
{
	&t603_TI,
	&t5874_TI,
	&t5876_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5875_0_0_0;
extern Il2CppType t5875_1_0_0;
struct t5875;
extern Il2CppGenericClass t5875_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5875_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5875_MIs, t5875_PIs, NULL, NULL, NULL, NULL, NULL, &t5875_TI, t5875_ITIs, NULL, &t1908__CustomAttributeCache, &t5875_TI, &t5875_0_0_0, &t5875_1_0_0, NULL, &t5875_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4566_TI;

#include "t787.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags>
extern MethodInfo m30713_MI;
static PropertyInfo t4566____Current_PropertyInfo = 
{
	&t4566_TI, "Current", &m30713_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4566_PIs[] =
{
	&t4566____Current_PropertyInfo,
	NULL
};
extern Il2CppType t787_0_0_0;
extern void* RuntimeInvoker_t787 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30713_GM;
MethodInfo m30713_MI = 
{
	"get_Current", NULL, &t4566_TI, &t787_0_0_0, RuntimeInvoker_t787, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30713_GM};
static MethodInfo* t4566_MIs[] =
{
	&m30713_MI,
	NULL
};
static TypeInfo* t4566_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4566_0_0_0;
extern Il2CppType t4566_1_0_0;
struct t4566;
extern Il2CppGenericClass t4566_GC;
TypeInfo t4566_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4566_MIs, t4566_PIs, NULL, NULL, NULL, NULL, NULL, &t4566_TI, t4566_ITIs, NULL, &EmptyCustomAttributesCache, &t4566_TI, &t4566_0_0_0, &t4566_1_0_0, NULL, &t4566_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3210.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3210_TI;
#include "t3210MD.h"

extern TypeInfo t787_TI;
extern MethodInfo m17854_MI;
extern MethodInfo m23430_MI;
struct t20;
 int32_t m23430 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17850_MI;
 void m17850 (t3210 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17851_MI;
 t29 * m17851 (t3210 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17854(__this, &m17854_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t787_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17852_MI;
 void m17852 (t3210 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17853_MI;
 bool m17853 (t3210 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17854 (t3210 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23430(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23430_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3210_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3210_TI, offsetof(t3210, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3210_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3210_TI, offsetof(t3210, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3210_FIs[] =
{
	&t3210_f0_FieldInfo,
	&t3210_f1_FieldInfo,
	NULL
};
static PropertyInfo t3210____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3210_TI, "System.Collections.IEnumerator.Current", &m17851_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3210____Current_PropertyInfo = 
{
	&t3210_TI, "Current", &m17854_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3210_PIs[] =
{
	&t3210____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3210____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3210_m17850_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17850_GM;
MethodInfo m17850_MI = 
{
	".ctor", (methodPointerType)&m17850, &t3210_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3210_m17850_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17850_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17851_GM;
MethodInfo m17851_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17851, &t3210_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17851_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17852_GM;
MethodInfo m17852_MI = 
{
	"Dispose", (methodPointerType)&m17852, &t3210_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17852_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17853_GM;
MethodInfo m17853_MI = 
{
	"MoveNext", (methodPointerType)&m17853, &t3210_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17853_GM};
extern Il2CppType t787_0_0_0;
extern void* RuntimeInvoker_t787 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17854_GM;
MethodInfo m17854_MI = 
{
	"get_Current", (methodPointerType)&m17854, &t3210_TI, &t787_0_0_0, RuntimeInvoker_t787, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17854_GM};
static MethodInfo* t3210_MIs[] =
{
	&m17850_MI,
	&m17851_MI,
	&m17852_MI,
	&m17853_MI,
	&m17854_MI,
	NULL
};
static MethodInfo* t3210_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17851_MI,
	&m17853_MI,
	&m17852_MI,
	&m17854_MI,
};
static TypeInfo* t3210_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4566_TI,
};
static Il2CppInterfaceOffsetPair t3210_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4566_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3210_0_0_0;
extern Il2CppType t3210_1_0_0;
extern Il2CppGenericClass t3210_GC;
TypeInfo t3210_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3210_MIs, t3210_PIs, t3210_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3210_TI, t3210_ITIs, t3210_VT, &EmptyCustomAttributesCache, &t3210_TI, &t3210_0_0_0, &t3210_1_0_0, t3210_IOs, &t3210_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3210)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5877_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags>
extern MethodInfo m30714_MI;
static PropertyInfo t5877____Count_PropertyInfo = 
{
	&t5877_TI, "Count", &m30714_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30715_MI;
static PropertyInfo t5877____IsReadOnly_PropertyInfo = 
{
	&t5877_TI, "IsReadOnly", &m30715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5877_PIs[] =
{
	&t5877____Count_PropertyInfo,
	&t5877____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30714_GM;
MethodInfo m30714_MI = 
{
	"get_Count", NULL, &t5877_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30714_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30715_GM;
MethodInfo m30715_MI = 
{
	"get_IsReadOnly", NULL, &t5877_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30715_GM};
extern Il2CppType t787_0_0_0;
extern Il2CppType t787_0_0_0;
static ParameterInfo t5877_m30716_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t787_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30716_GM;
MethodInfo m30716_MI = 
{
	"Add", NULL, &t5877_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5877_m30716_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30716_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30717_GM;
MethodInfo m30717_MI = 
{
	"Clear", NULL, &t5877_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30717_GM};
extern Il2CppType t787_0_0_0;
static ParameterInfo t5877_m30718_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t787_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30718_GM;
MethodInfo m30718_MI = 
{
	"Contains", NULL, &t5877_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5877_m30718_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30718_GM};
extern Il2CppType t3907_0_0_0;
extern Il2CppType t3907_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5877_m30719_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3907_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30719_GM;
MethodInfo m30719_MI = 
{
	"CopyTo", NULL, &t5877_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5877_m30719_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30719_GM};
extern Il2CppType t787_0_0_0;
static ParameterInfo t5877_m30720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t787_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30720_GM;
MethodInfo m30720_MI = 
{
	"Remove", NULL, &t5877_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5877_m30720_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30720_GM};
static MethodInfo* t5877_MIs[] =
{
	&m30714_MI,
	&m30715_MI,
	&m30716_MI,
	&m30717_MI,
	&m30718_MI,
	&m30719_MI,
	&m30720_MI,
	NULL
};
extern TypeInfo t5879_TI;
static TypeInfo* t5877_ITIs[] = 
{
	&t603_TI,
	&t5879_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5877_0_0_0;
extern Il2CppType t5877_1_0_0;
struct t5877;
extern Il2CppGenericClass t5877_GC;
TypeInfo t5877_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5877_MIs, t5877_PIs, NULL, NULL, NULL, NULL, NULL, &t5877_TI, t5877_ITIs, NULL, &EmptyCustomAttributesCache, &t5877_TI, &t5877_0_0_0, &t5877_1_0_0, NULL, &t5877_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags>
extern Il2CppType t4566_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30721_GM;
MethodInfo m30721_MI = 
{
	"GetEnumerator", NULL, &t5879_TI, &t4566_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30721_GM};
static MethodInfo* t5879_MIs[] =
{
	&m30721_MI,
	NULL
};
static TypeInfo* t5879_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5879_0_0_0;
extern Il2CppType t5879_1_0_0;
struct t5879;
extern Il2CppGenericClass t5879_GC;
TypeInfo t5879_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5879_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5879_TI, t5879_ITIs, NULL, &EmptyCustomAttributesCache, &t5879_TI, &t5879_0_0_0, &t5879_1_0_0, NULL, &t5879_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5878_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags>
extern MethodInfo m30722_MI;
extern MethodInfo m30723_MI;
static PropertyInfo t5878____Item_PropertyInfo = 
{
	&t5878_TI, "Item", &m30722_MI, &m30723_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5878_PIs[] =
{
	&t5878____Item_PropertyInfo,
	NULL
};
extern Il2CppType t787_0_0_0;
static ParameterInfo t5878_m30724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t787_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30724_GM;
MethodInfo m30724_MI = 
{
	"IndexOf", NULL, &t5878_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5878_m30724_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30724_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t787_0_0_0;
static ParameterInfo t5878_m30725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t787_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30725_GM;
MethodInfo m30725_MI = 
{
	"Insert", NULL, &t5878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5878_m30725_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30725_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5878_m30726_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30726_GM;
MethodInfo m30726_MI = 
{
	"RemoveAt", NULL, &t5878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5878_m30726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30726_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5878_m30722_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t787_0_0_0;
extern void* RuntimeInvoker_t787_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30722_GM;
MethodInfo m30722_MI = 
{
	"get_Item", NULL, &t5878_TI, &t787_0_0_0, RuntimeInvoker_t787_t44, t5878_m30722_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30722_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t787_0_0_0;
static ParameterInfo t5878_m30723_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t787_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30723_GM;
MethodInfo m30723_MI = 
{
	"set_Item", NULL, &t5878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5878_m30723_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30723_GM};
static MethodInfo* t5878_MIs[] =
{
	&m30724_MI,
	&m30725_MI,
	&m30726_MI,
	&m30722_MI,
	&m30723_MI,
	NULL
};
static TypeInfo* t5878_ITIs[] = 
{
	&t603_TI,
	&t5877_TI,
	&t5879_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5878_0_0_0;
extern Il2CppType t5878_1_0_0;
struct t5878;
extern Il2CppGenericClass t5878_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5878_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5878_MIs, t5878_PIs, NULL, NULL, NULL, NULL, NULL, &t5878_TI, t5878_ITIs, NULL, &t1908__CustomAttributeCache, &t5878_TI, &t5878_0_0_0, &t5878_1_0_0, NULL, &t5878_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4568_TI;

#include "t745.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509Certificate>
extern MethodInfo m30727_MI;
static PropertyInfo t4568____Current_PropertyInfo = 
{
	&t4568_TI, "Current", &m30727_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4568_PIs[] =
{
	&t4568____Current_PropertyInfo,
	NULL
};
extern Il2CppType t745_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30727_GM;
MethodInfo m30727_MI = 
{
	"get_Current", NULL, &t4568_TI, &t745_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30727_GM};
static MethodInfo* t4568_MIs[] =
{
	&m30727_MI,
	NULL
};
static TypeInfo* t4568_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4568_0_0_0;
extern Il2CppType t4568_1_0_0;
struct t4568;
extern Il2CppGenericClass t4568_GC;
TypeInfo t4568_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4568_MIs, t4568_PIs, NULL, NULL, NULL, NULL, NULL, &t4568_TI, t4568_ITIs, NULL, &EmptyCustomAttributesCache, &t4568_TI, &t4568_0_0_0, &t4568_1_0_0, NULL, &t4568_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3211.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3211_TI;
#include "t3211MD.h"

extern TypeInfo t745_TI;
extern MethodInfo m17859_MI;
extern MethodInfo m23441_MI;
struct t20;
#define m23441(__this, p0, method) (t745 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509Certificate>
extern Il2CppType t20_0_0_1;
FieldInfo t3211_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3211_TI, offsetof(t3211, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3211_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3211_TI, offsetof(t3211, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3211_FIs[] =
{
	&t3211_f0_FieldInfo,
	&t3211_f1_FieldInfo,
	NULL
};
extern MethodInfo m17856_MI;
static PropertyInfo t3211____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3211_TI, "System.Collections.IEnumerator.Current", &m17856_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3211____Current_PropertyInfo = 
{
	&t3211_TI, "Current", &m17859_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3211_PIs[] =
{
	&t3211____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3211____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3211_m17855_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17855_GM;
MethodInfo m17855_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3211_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3211_m17855_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17855_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17856_GM;
MethodInfo m17856_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3211_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17856_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17857_GM;
MethodInfo m17857_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3211_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17857_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17858_GM;
MethodInfo m17858_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3211_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17858_GM};
extern Il2CppType t745_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17859_GM;
MethodInfo m17859_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3211_TI, &t745_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17859_GM};
static MethodInfo* t3211_MIs[] =
{
	&m17855_MI,
	&m17856_MI,
	&m17857_MI,
	&m17858_MI,
	&m17859_MI,
	NULL
};
extern MethodInfo m17858_MI;
extern MethodInfo m17857_MI;
static MethodInfo* t3211_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17856_MI,
	&m17858_MI,
	&m17857_MI,
	&m17859_MI,
};
static TypeInfo* t3211_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4568_TI,
};
static Il2CppInterfaceOffsetPair t3211_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4568_TI, 7},
};
extern TypeInfo t745_TI;
static Il2CppRGCTXData t3211_RGCTXData[3] = 
{
	&m17859_MI/* Method Usage */,
	&t745_TI/* Class Usage */,
	&m23441_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3211_0_0_0;
extern Il2CppType t3211_1_0_0;
extern Il2CppGenericClass t3211_GC;
TypeInfo t3211_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3211_MIs, t3211_PIs, t3211_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3211_TI, t3211_ITIs, t3211_VT, &EmptyCustomAttributesCache, &t3211_TI, &t3211_0_0_0, &t3211_1_0_0, t3211_IOs, &t3211_GC, NULL, NULL, NULL, t3211_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3211)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5880_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509Certificate>
extern MethodInfo m30728_MI;
static PropertyInfo t5880____Count_PropertyInfo = 
{
	&t5880_TI, "Count", &m30728_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30729_MI;
static PropertyInfo t5880____IsReadOnly_PropertyInfo = 
{
	&t5880_TI, "IsReadOnly", &m30729_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5880_PIs[] =
{
	&t5880____Count_PropertyInfo,
	&t5880____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30728_GM;
MethodInfo m30728_MI = 
{
	"get_Count", NULL, &t5880_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30728_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30729_GM;
MethodInfo m30729_MI = 
{
	"get_IsReadOnly", NULL, &t5880_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30729_GM};
extern Il2CppType t745_0_0_0;
extern Il2CppType t745_0_0_0;
static ParameterInfo t5880_m30730_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30730_GM;
MethodInfo m30730_MI = 
{
	"Add", NULL, &t5880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5880_m30730_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30730_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30731_GM;
MethodInfo m30731_MI = 
{
	"Clear", NULL, &t5880_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30731_GM};
extern Il2CppType t745_0_0_0;
static ParameterInfo t5880_m30732_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t745_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30732_GM;
MethodInfo m30732_MI = 
{
	"Contains", NULL, &t5880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5880_m30732_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30732_GM};
extern Il2CppType t801_0_0_0;
extern Il2CppType t801_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5880_m30733_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t801_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30733_GM;
MethodInfo m30733_MI = 
{
	"CopyTo", NULL, &t5880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5880_m30733_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30733_GM};
extern Il2CppType t745_0_0_0;
static ParameterInfo t5880_m30734_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t745_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30734_GM;
MethodInfo m30734_MI = 
{
	"Remove", NULL, &t5880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5880_m30734_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30734_GM};
static MethodInfo* t5880_MIs[] =
{
	&m30728_MI,
	&m30729_MI,
	&m30730_MI,
	&m30731_MI,
	&m30732_MI,
	&m30733_MI,
	&m30734_MI,
	NULL
};
extern TypeInfo t5882_TI;
static TypeInfo* t5880_ITIs[] = 
{
	&t603_TI,
	&t5882_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5880_0_0_0;
extern Il2CppType t5880_1_0_0;
struct t5880;
extern Il2CppGenericClass t5880_GC;
TypeInfo t5880_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5880_MIs, t5880_PIs, NULL, NULL, NULL, NULL, NULL, &t5880_TI, t5880_ITIs, NULL, &EmptyCustomAttributesCache, &t5880_TI, &t5880_0_0_0, &t5880_1_0_0, NULL, &t5880_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509Certificate>
extern Il2CppType t4568_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30735_GM;
MethodInfo m30735_MI = 
{
	"GetEnumerator", NULL, &t5882_TI, &t4568_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30735_GM};
static MethodInfo* t5882_MIs[] =
{
	&m30735_MI,
	NULL
};
static TypeInfo* t5882_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5882_0_0_0;
extern Il2CppType t5882_1_0_0;
struct t5882;
extern Il2CppGenericClass t5882_GC;
TypeInfo t5882_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5882_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5882_TI, t5882_ITIs, NULL, &EmptyCustomAttributesCache, &t5882_TI, &t5882_0_0_0, &t5882_1_0_0, NULL, &t5882_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5881_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509Certificate>
extern MethodInfo m30736_MI;
extern MethodInfo m30737_MI;
static PropertyInfo t5881____Item_PropertyInfo = 
{
	&t5881_TI, "Item", &m30736_MI, &m30737_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5881_PIs[] =
{
	&t5881____Item_PropertyInfo,
	NULL
};
extern Il2CppType t745_0_0_0;
static ParameterInfo t5881_m30738_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t745_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30738_GM;
MethodInfo m30738_MI = 
{
	"IndexOf", NULL, &t5881_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5881_m30738_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30738_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t745_0_0_0;
static ParameterInfo t5881_m30739_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30739_GM;
MethodInfo m30739_MI = 
{
	"Insert", NULL, &t5881_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5881_m30739_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30739_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5881_m30740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30740_GM;
MethodInfo m30740_MI = 
{
	"RemoveAt", NULL, &t5881_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5881_m30740_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30740_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5881_m30736_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t745_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30736_GM;
MethodInfo m30736_MI = 
{
	"get_Item", NULL, &t5881_TI, &t745_0_0_0, RuntimeInvoker_t29_t44, t5881_m30736_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30736_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t745_0_0_0;
static ParameterInfo t5881_m30737_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30737_GM;
MethodInfo m30737_MI = 
{
	"set_Item", NULL, &t5881_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5881_m30737_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30737_GM};
static MethodInfo* t5881_MIs[] =
{
	&m30738_MI,
	&m30739_MI,
	&m30740_MI,
	&m30736_MI,
	&m30737_MI,
	NULL
};
static TypeInfo* t5881_ITIs[] = 
{
	&t603_TI,
	&t5880_TI,
	&t5882_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5881_0_0_0;
extern Il2CppType t5881_1_0_0;
struct t5881;
extern Il2CppGenericClass t5881_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5881_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5881_MIs, t5881_PIs, NULL, NULL, NULL, NULL, NULL, &t5881_TI, t5881_ITIs, NULL, &t1908__CustomAttributeCache, &t5881_TI, &t5881_0_0_0, &t5881_1_0_0, NULL, &t5881_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5883_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.IDeserializationCallback>
extern MethodInfo m30741_MI;
static PropertyInfo t5883____Count_PropertyInfo = 
{
	&t5883_TI, "Count", &m30741_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30742_MI;
static PropertyInfo t5883____IsReadOnly_PropertyInfo = 
{
	&t5883_TI, "IsReadOnly", &m30742_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5883_PIs[] =
{
	&t5883____Count_PropertyInfo,
	&t5883____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30741_GM;
MethodInfo m30741_MI = 
{
	"get_Count", NULL, &t5883_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30741_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30742_GM;
MethodInfo m30742_MI = 
{
	"get_IsReadOnly", NULL, &t5883_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30742_GM};
extern Il2CppType t918_0_0_0;
extern Il2CppType t918_0_0_0;
static ParameterInfo t5883_m30743_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t918_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30743_GM;
MethodInfo m30743_MI = 
{
	"Add", NULL, &t5883_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5883_m30743_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30743_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30744_GM;
MethodInfo m30744_MI = 
{
	"Clear", NULL, &t5883_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30744_GM};
extern Il2CppType t918_0_0_0;
static ParameterInfo t5883_m30745_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t918_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30745_GM;
MethodInfo m30745_MI = 
{
	"Contains", NULL, &t5883_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5883_m30745_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30745_GM};
extern Il2CppType t3555_0_0_0;
extern Il2CppType t3555_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5883_m30746_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3555_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30746_GM;
MethodInfo m30746_MI = 
{
	"CopyTo", NULL, &t5883_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5883_m30746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30746_GM};
extern Il2CppType t918_0_0_0;
static ParameterInfo t5883_m30747_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t918_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30747_GM;
MethodInfo m30747_MI = 
{
	"Remove", NULL, &t5883_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5883_m30747_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30747_GM};
static MethodInfo* t5883_MIs[] =
{
	&m30741_MI,
	&m30742_MI,
	&m30743_MI,
	&m30744_MI,
	&m30745_MI,
	&m30746_MI,
	&m30747_MI,
	NULL
};
extern TypeInfo t5885_TI;
static TypeInfo* t5883_ITIs[] = 
{
	&t603_TI,
	&t5885_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5883_0_0_0;
extern Il2CppType t5883_1_0_0;
struct t5883;
extern Il2CppGenericClass t5883_GC;
TypeInfo t5883_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5883_MIs, t5883_PIs, NULL, NULL, NULL, NULL, NULL, &t5883_TI, t5883_ITIs, NULL, &EmptyCustomAttributesCache, &t5883_TI, &t5883_0_0_0, &t5883_1_0_0, NULL, &t5883_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
