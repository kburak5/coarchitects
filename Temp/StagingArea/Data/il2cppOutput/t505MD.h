﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t505;
#include "t23.h"
#include "t132.h"

 t23  m2580 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2581 (t505 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2582 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2583 (t505 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2584 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2585 (t505 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2586 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2587 (t505 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2588 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2589 (t505 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2590 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2591 (t505 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2592 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2593 (t505 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m2594 (t505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2595 (t505 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
