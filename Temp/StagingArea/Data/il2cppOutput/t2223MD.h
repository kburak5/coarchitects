﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2223;
struct t29;
struct t31;

#include "t2001MD.h"
#define m11029(__this, method) (void)m10703_gshared((t2001 *)__this, method)
#define m11030(__this, method) (void)m10704_gshared((t29 *)__this, method)
#define m11031(__this, p0, method) (int32_t)m10705_gshared((t2001 *)__this, (t29 *)p0, method)
#define m11032(__this, p0, p1, method) (bool)m10706_gshared((t2001 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m11033(__this, method) (t2223 *)m10707_gshared((t29 *)__this, method)
