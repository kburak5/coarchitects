﻿#pragma once
#include <stdint.h>
#include "t110.h"
#include "t164.h"
#include "t149.h"
struct t526 
{
	int32_t f0;
	t164  f1;
	t164  f2;
	float f3;
	int32_t f4;
	int32_t f5;
	bool f6;
	int32_t f7;
};
// Native definition for marshalling of: UnityEngine.CharacterInfo
struct t526_marshaled
{
	int32_t f0;
	t164  f1;
	t164  f2;
	float f3;
	int32_t f4;
	int32_t f5;
	int32_t f6;
	int32_t f7;
};
