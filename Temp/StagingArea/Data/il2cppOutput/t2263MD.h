﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2263;
struct t29;
#include "t57.h"

 void m11392 (t2263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11393 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11394 (t2263 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2263 * m11395 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
