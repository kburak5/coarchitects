﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1193;
struct t7;
struct t781;
struct t1192;
#include "t1120.h"

 void m6183 (t1193 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6184 (t1193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6185 (t1193 * __this, int32_t p0, int32_t p1, t7* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6186 (t1193 * __this, uint8_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6187 (t1193 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, bool p4, uint8_t p5, bool p6, bool p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6188 (t1193 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6189 (t1193 * __this, uint8_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6190 (t1193 * __this, uint8_t p0, t781** p1, int32_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1192 * m6191 (t1193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6192 (t1193 * __this, t781* p0, int32_t p1, uint8_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1192 * m6193 (t1193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
