﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t724;
struct t29;
struct t723;
struct t720;
#include "t725.h"

 void m3087 (t724 * __this, t720 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3088 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3089 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3090 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3091 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t723 * m3092 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m3093 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3094 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3095 (t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
