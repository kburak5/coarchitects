﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1219;
struct t781;
struct t7;

 void m6484 (t1219 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6485 (t1219 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6486 (t1219 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6487 (t1219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6488 (t1219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6489 (t1219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6490 (t1219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6491 (t1219 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6492 (t1219 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6493 (t1219 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6494 (t1219 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6495 (t1219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6496 (t1219 * __this, t781* p0, int32_t* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6497 (t1219 * __this, t781* p0, int32_t* p1, uint8_t* p2, int32_t* p3, t781** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6498 (t1219 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6499 (t1219 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6500 (t1219 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
