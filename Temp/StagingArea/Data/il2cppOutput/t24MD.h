﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t24;
struct t453;
struct t497;
struct t16;
#include "t164.h"
#include "t498.h"
#include "t23.h"
#include "t329.h"

 float m1466 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1465 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1302 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1480 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2484 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2485 (t24 * __this, t164 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m2486 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t453 * m2487 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2488 (t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m41 (t24 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2489 (t29 * __this, t24 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1613 (t24 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2490 (t29 * __this, t24 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t329  m1464 (t24 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t329  m2491 (t29 * __this, t24 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t24 * m38 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2492 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2493 (t29 * __this, t497* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2494 (t29 * __this, t24 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2495 (t29 * __this, t24 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2496 (t29 * __this, t24 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m2497 (t24 * __this, t329  p0, float p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m2498 (t29 * __this, t24 * p0, t329 * p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m2499 (t24 * __this, t329  p0, float p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m2500 (t29 * __this, t24 * p0, t329 * p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
