﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t922;
struct t781;
struct t7;

 void m8869 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8870 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8871 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m8872 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8873 (t29 * __this, uint8_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8874 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8875 (t29 * __this, uint8_t* p0, t781* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m8876 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5220 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8877 (t29 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
