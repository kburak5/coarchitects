﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2866;
struct t29;
struct t469;
struct t66;
struct t67;
#include "t35.h"

 void m15586 (t2866 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15587 (t2866 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15588 (t2866 * __this, int32_t p0, t469 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15589 (t2866 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
