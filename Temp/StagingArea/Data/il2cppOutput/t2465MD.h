﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2465;
struct t29;
struct t7;

 void m12917_gshared (t2465 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12917(__this, p0, p1, method) (void)m12917_gshared((t2465 *)__this, (t29 *)p0, (t29 *)p1, method)
 t29 * m12918_gshared (t2465 * __this, MethodInfo* method);
#define m12918(__this, method) (t29 *)m12918_gshared((t2465 *)__this, method)
 void m12919_gshared (t2465 * __this, t29 * p0, MethodInfo* method);
#define m12919(__this, p0, method) (void)m12919_gshared((t2465 *)__this, (t29 *)p0, method)
 t29 * m12920_gshared (t2465 * __this, MethodInfo* method);
#define m12920(__this, method) (t29 *)m12920_gshared((t2465 *)__this, method)
 void m12921_gshared (t2465 * __this, t29 * p0, MethodInfo* method);
#define m12921(__this, p0, method) (void)m12921_gshared((t2465 *)__this, (t29 *)p0, method)
 t7* m12922_gshared (t2465 * __this, MethodInfo* method);
#define m12922(__this, method) (t7*)m12922_gshared((t2465 *)__this, method)
