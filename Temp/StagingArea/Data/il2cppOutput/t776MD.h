﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t776;
struct t778;
struct t777;
struct t779;
struct t780;
struct t781;
struct t782;
struct t783;

 void m3279 (t776 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t778 * m3280 (t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t778 * m3281 (t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m3282 (t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t779 * m3283 (t776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m3284 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m3285 (t29 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m3286 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
