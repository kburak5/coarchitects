﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1599;
struct t200;
struct t7;
struct t781;
struct t1305;
struct t29;

 void m8751 (t1599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8752 (t1599 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8753 (t1599 * __this, bool p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8754 (t1599 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8755 (t1599 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8756 (t1599 * __this, uint16_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8757 (t1599 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8758 (t1599 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8759 (t1599 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8760 (t1599 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8761 (t1599 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8762 (t1599 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8763 (t1599 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8764 (t1599 * __this, uint8_t* p0, int32_t p1, uint16_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8765 (t1599 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8766 (t1599 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1305 * m8767 (t1599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8768 (t1599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8769 (t1599 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8770 (t1599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8771 (t29 * __this, uint8_t* p0, uint8_t* p1, int32_t p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
