﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1013;
struct t781;

 void m5194 (t1013 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5195 (t1013 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8258 (t1013 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8259 (t1013 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5196 (t1013 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8260 (t1013 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
