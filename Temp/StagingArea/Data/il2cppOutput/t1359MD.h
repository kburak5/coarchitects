﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1359;
struct t29;
struct t731;
struct t20;
struct t136;

 void m8475 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8476 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8477 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8478 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m8479 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m8480 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8481 (t1359 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8482 (t1359 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8483 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8484 (t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
