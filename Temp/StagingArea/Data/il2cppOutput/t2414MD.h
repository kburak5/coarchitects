﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2414;
struct t29;
struct t136;
struct t346;
struct t316;
struct t2180;
struct t2181;

 void m12456_gshared (t2414 * __this, MethodInfo* method);
#define m12456(__this, method) (void)m12456_gshared((t2414 *)__this, method)
 t29 * m12458_gshared (t2414 * __this, MethodInfo* method);
#define m12458(__this, method) (t29 *)m12458_gshared((t2414 *)__this, method)
 void m12459_gshared (t2414 * __this, t29 * p0, MethodInfo* method);
#define m12459(__this, p0, method) (void)m12459_gshared((t2414 *)__this, (t29 *)p0, method)
 bool m12460_gshared (t2414 * __this, t29 * p0, MethodInfo* method);
#define m12460(__this, p0, method) (bool)m12460_gshared((t2414 *)__this, (t29 *)p0, method)
 t29* m12462_gshared (t2414 * __this, MethodInfo* method);
#define m12462(__this, method) (t29*)m12462_gshared((t2414 *)__this, method)
 void m12463_gshared (t2414 * __this, MethodInfo* method);
#define m12463(__this, method) (void)m12463_gshared((t2414 *)__this, method)
 bool m12465_gshared (t2414 * __this, t29 * p0, MethodInfo* method);
#define m12465(__this, p0, method) (bool)m12465_gshared((t2414 *)__this, (t29 *)p0, method)
 void m12467_gshared (t2414 * __this, t316* p0, int32_t p1, MethodInfo* method);
#define m12467(__this, p0, p1, method) (void)m12467_gshared((t2414 *)__this, (t316*)p0, (int32_t)p1, method)
 int32_t m12468_gshared (t2414 * __this, MethodInfo* method);
#define m12468(__this, method) (int32_t)m12468_gshared((t2414 *)__this, method)
 bool m12470_gshared (t2414 * __this, MethodInfo* method);
#define m12470(__this, method) (bool)m12470_gshared((t2414 *)__this, method)
 int32_t m12472_gshared (t2414 * __this, t29 * p0, MethodInfo* method);
#define m12472(__this, p0, method) (int32_t)m12472_gshared((t2414 *)__this, (t29 *)p0, method)
 void m12474_gshared (t2414 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m12474(__this, p0, p1, method) (void)m12474_gshared((t2414 *)__this, (int32_t)p0, (t29 *)p1, method)
 void m12476_gshared (t2414 * __this, int32_t p0, MethodInfo* method);
#define m12476(__this, p0, method) (void)m12476_gshared((t2414 *)__this, (int32_t)p0, method)
 t29 * m12477_gshared (t2414 * __this, int32_t p0, MethodInfo* method);
#define m12477(__this, p0, method) (t29 *)m12477_gshared((t2414 *)__this, (int32_t)p0, method)
 void m12479_gshared (t2414 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m12479(__this, p0, p1, method) (void)m12479_gshared((t2414 *)__this, (int32_t)p0, (t29 *)p1, method)
 void m12480_gshared (t2414 * __this, t2180 * p0, MethodInfo* method);
#define m12480(__this, p0, method) (void)m12480_gshared((t2414 *)__this, (t2180 *)p0, method)
 void m12481_gshared (t2414 * __this, t2181 * p0, MethodInfo* method);
#define m12481(__this, p0, method) (void)m12481_gshared((t2414 *)__this, (t2181 *)p0, method)
