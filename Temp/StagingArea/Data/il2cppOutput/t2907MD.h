﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2907;
struct t29;
struct t20;
#include "t205.h"

 void m15901 (t2907 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15902 (t2907 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15903 (t2907 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15904 (t2907 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15905 (t2907 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
