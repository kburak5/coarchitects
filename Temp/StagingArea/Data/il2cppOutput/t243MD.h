﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t243;
struct t29;
struct t39;
struct t2705;
struct t20;
struct t136;
struct t2706;
struct t246;
struct t2707;
struct t2704;
struct t244;
struct t2708;
#include "t2709.h"

#include "t294MD.h"
#define m1932(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m14695(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m14696(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m14697(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m14698(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m14699(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m14700(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m14701(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m14702(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m14703(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14704(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m14705(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m14706(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m14707(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m14708(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m14709(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m14710(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m14711(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1939(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m14712(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m14713(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m14714(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m14715(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m14716(__this, method) (t2707 *)m10583_gshared((t294 *)__this, method)
#define m14717(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m1933(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m14718(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m1941(__this, p0, method) (t39 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14719(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m14720(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2709  m14721 (t243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m14722(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m14723(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m14724(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m14725(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14726(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m1938(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m14727(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14728(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m14729(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m14730(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m14731(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m14732(__this, method) (t2704*)m10619_gshared((t294 *)__this, method)
#define m14733(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m14734(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m14735(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1937(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1936(__this, p0, method) (t39 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m14736(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
