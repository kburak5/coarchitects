﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t885;
struct t881;
struct t876;
struct t886;
struct t878;

 void m3806 (t885 * __this, t886 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3807 (t885 * __this, t881 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3808 (t885 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3809 (t885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t876 * m3810 (t885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
