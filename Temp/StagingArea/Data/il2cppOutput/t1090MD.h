﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1090;
struct t7;
struct t733;
#include "t735.h"

 void m8864 (t1090 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5101 (t1090 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8865 (t1090 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
