﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t245;
struct t29;
struct t39;
struct t66;
struct t67;
#include "t35.h"

 void m1942 (t245 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14830 (t245 * __this, t39 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14831 (t245 * __this, t39 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14832 (t245 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
