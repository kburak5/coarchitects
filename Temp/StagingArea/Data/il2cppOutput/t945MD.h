﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t945;
struct t780;
struct t136;
struct t951;
struct t781;

 void m4511 (t945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4512 (t945 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4513 (t945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t780 * m4097 (t945 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4514 (t945 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4515 (t945 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4516 (t945 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t951 * m4189 (t945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4517 (t945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4518 (t945 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4519 (t945 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4520 (t945 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
