﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2595;
struct t29;
struct t20;
#include "t174.h"

 void m13986 (t2595 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13987 (t2595 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13988 (t2595 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13989 (t2595 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13990 (t2595 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
