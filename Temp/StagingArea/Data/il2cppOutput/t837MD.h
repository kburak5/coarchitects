﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t837;
struct t29;
struct t838;

 void m3539 (t837 * __this, t838 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3540 (t837 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3541 (t837 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
