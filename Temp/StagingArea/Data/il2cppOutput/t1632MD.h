﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1632;
struct t353;
struct t7;
struct t733;

 void m9208 (t1632 * __this, t353 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m9209 (t1632 * __this, t733 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
