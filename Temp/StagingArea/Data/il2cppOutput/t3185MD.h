﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3185;
struct t29;
struct t20;

 void m17688 (t3185 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17689 (t3185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17690 (t3185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17691 (t3185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m17692 (t3185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
