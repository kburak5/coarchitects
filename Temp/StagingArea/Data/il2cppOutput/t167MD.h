﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t167;
struct t29;
struct t155;
struct t2546;
struct t20;
struct t136;
struct t363;
struct t2547;
struct t2548;
struct t2545;
struct t2549;
struct t168;
#include "t2550.h"

#include "t294MD.h"
#define m1610(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m13575(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m13576(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m13577(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m13578(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m13579(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m13580(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m13581(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m13582(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m13583(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13584(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m13585(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m13586(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m13587(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m13588(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m13589(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m13590(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m13591(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1629(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m13592(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m13593(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m13594(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m13595(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m13596(__this, method) (t2548 *)m10583_gshared((t294 *)__this, method)
#define m1617(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m13597(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m13598(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m13599(__this, p0, method) (t155 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m13600(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m13601(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2550  m13602 (t167 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13603(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m13604(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m13605(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m13606(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13607(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m13608(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m13609(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m13610(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m13611(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m13612(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m1632(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m13613(__this, method) (t2545*)m10619_gshared((t294 *)__this, method)
#define m13614(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m13615(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m13616(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1626(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1618(__this, p0, method) (t155 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m13617(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
