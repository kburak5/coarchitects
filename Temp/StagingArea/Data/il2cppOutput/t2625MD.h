﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2625;
struct t29;
struct t20;
#include "t212.h"

 void m14122 (t2625 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14123 (t2625 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14124 (t2625 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14125 (t2625 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14126 (t2625 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
