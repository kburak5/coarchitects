﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3048;
struct t29;
#include "t380.h"

 void m16791 (t3048 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16792 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16793 (t3048 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16794 (t3048 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3048 * m16795 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
