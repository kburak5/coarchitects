﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3113;
struct t29;
struct t553;
#include "t552.h"

 void m17193 (t3113 * __this, t553 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17194 (t3113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17195 (t3113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17196 (t3113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17197 (t3113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
