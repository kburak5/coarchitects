﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1019;
struct t988;
struct t1013;
struct t7;
struct t1020;
struct t781;
#include "t1018.h"
#include "t1021.h"
#include "t1022.h"
#include "t1023.h"
#include "t1024.h"

 void m4614 (t1019 * __this, int16_t p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, bool p5, bool p6, uint8_t p7, uint8_t p8, int16_t p9, uint8_t p10, uint8_t p11, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4615 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4616 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4617 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1013 * m4618 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1013 * m4619 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4620 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4621 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4622 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4623 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4624 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4625 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m4626 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4627 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4628 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4629 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4630 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4631 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m4632 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4633 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1020 * m4634 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4635 (t1019 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4636 (t1019 * __this, t781* p0, int32_t p1, int16_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4637 (t1019 * __this, t781* p0, int32_t p1, uint64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4638 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4639 (t1019 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4640 (t1019 * __this, t781* p0, t781** p1, t781** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4641 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4642 (t1019 * __this, t781* p0, t7* p1, t781* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4643 (t1019 * __this, t7* p0, t781* p1, t781* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4644 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4645 (t1019 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
