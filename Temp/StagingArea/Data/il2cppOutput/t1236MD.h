﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1236;
struct t295;
struct t7;
struct t1240;
struct t1239;
struct t1238;

 void m6544 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m6545 (t1236 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m6546 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6547 (t1236 * __this, uint16_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6548 (t1236 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6549 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6550 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6551 (t1236 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6552 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6553 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6554 (t1236 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6555 (t1236 * __this, uint16_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6556 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6557 (t1236 * __this, t1240 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6558 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6559 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6560 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6561 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6562 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6563 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6564 (t1236 * __this, t1238 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6565 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6566 (t1236 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
