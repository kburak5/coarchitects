﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3183;
struct t29;
struct t20;
#include "t742.h"

 void m17678 (t3183 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17679 (t3183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17680 (t3183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17681 (t3183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17682 (t3183 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
