﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1065;
struct t1017;
struct t7;
struct t733;
#include "t735.h"
#include "t1015.h"
#include "t1016.h"

 void m4965 (t1065 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4966 (t1065 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4967 (t1065 * __this, uint8_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4968 (t1065 * __this, uint8_t p0, uint8_t p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4969 (t1065 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4970 (t1065 * __this, uint8_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1017 * m4971 (t1065 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
