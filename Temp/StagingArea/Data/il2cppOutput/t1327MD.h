﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1327;
struct t1034;
struct t943;
struct t200;
struct t7;

 void m7181 (t1327 * __this, t1034 * p0, t943 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7182 (t1327 * __this, t1034 * p0, t943 * p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7183 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7184 (t1327 * __this, t943 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7185 (t1327 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7186 (t1327 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7187 (t1327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7188 (t1327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7189 (t1327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7190 (t1327 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7191 (t1327 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7192 (t1327 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7193 (t1327 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7194 (t1327 * __this, t200* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7195 (t1327 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7196 (t1327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7197 (t1327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
