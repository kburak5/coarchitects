﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1689;
struct t1689_marshaled;

void t1689_marshal(const t1689& unmarshaled, t1689_marshaled& marshaled);
void t1689_marshal_back(const t1689_marshaled& marshaled, t1689& unmarshaled);
void t1689_marshal_cleanup(t1689_marshaled& marshaled);
