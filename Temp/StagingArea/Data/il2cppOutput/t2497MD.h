﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2497;
struct t29;
struct t350;
struct t148;
struct t66;
struct t67;
#include "t35.h"

#include "t2470MD.h"
#define m13192(__this, p0, p1, method) (void)m12959_gshared((t2470 *)__this, (t29 *)p0, (t35)p1, method)
#define m13193(__this, p0, p1, method) (t350 *)m12960_gshared((t2470 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13194(__this, p0, p1, p2, p3, method) (t29 *)m12961_gshared((t2470 *)__this, (t29 *)p0, (t29 *)p1, (t67 *)p2, (t29 *)p3, method)
#define m13195(__this, p0, method) (t350 *)m12962_gshared((t2470 *)__this, (t29 *)p0, method)
