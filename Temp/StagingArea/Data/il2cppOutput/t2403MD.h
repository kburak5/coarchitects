﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2403;
struct t29;
struct t557;
struct t133;
struct t316;

 void m12422 (t2403 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12423 (t2403 * __this, t133 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12424 (t2403 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12425 (t2403 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
