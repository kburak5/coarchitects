﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t320;
struct t29;
struct t6;
struct t118;

 void m12130 (t320 * __this, t118 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12131 (t320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12132 (t320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1427 (t320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m1426 (t320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
