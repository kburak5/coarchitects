﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "mscorlib_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t3682_TI;


#include "t20.h"

// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType[]
static MethodInfo* t3682_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m4200_MI;
extern MethodInfo m5904_MI;
extern MethodInfo m5920_MI;
extern MethodInfo m5921_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5922_MI;
extern MethodInfo m5923_MI;
extern MethodInfo m5895_MI;
extern MethodInfo m5896_MI;
extern MethodInfo m5897_MI;
extern MethodInfo m5898_MI;
extern MethodInfo m5899_MI;
extern MethodInfo m5900_MI;
extern MethodInfo m5901_MI;
extern MethodInfo m5902_MI;
extern MethodInfo m5903_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m5905_MI;
extern MethodInfo m5906_MI;
extern MethodInfo m25656_MI;
extern MethodInfo m5907_MI;
extern MethodInfo m25657_MI;
extern MethodInfo m25658_MI;
extern MethodInfo m25659_MI;
extern MethodInfo m25660_MI;
extern MethodInfo m25661_MI;
extern MethodInfo m5908_MI;
extern MethodInfo m25655_MI;
extern MethodInfo m25663_MI;
extern MethodInfo m25664_MI;
extern MethodInfo m19711_MI;
extern MethodInfo m19712_MI;
extern MethodInfo m19713_MI;
extern MethodInfo m19714_MI;
extern MethodInfo m19715_MI;
extern MethodInfo m19716_MI;
extern MethodInfo m19710_MI;
extern MethodInfo m19718_MI;
extern MethodInfo m19719_MI;
extern MethodInfo m19722_MI;
extern MethodInfo m19723_MI;
extern MethodInfo m19724_MI;
extern MethodInfo m19725_MI;
extern MethodInfo m19726_MI;
extern MethodInfo m19727_MI;
extern MethodInfo m19721_MI;
extern MethodInfo m19729_MI;
extern MethodInfo m19730_MI;
extern MethodInfo m19733_MI;
extern MethodInfo m19734_MI;
extern MethodInfo m19735_MI;
extern MethodInfo m19736_MI;
extern MethodInfo m19737_MI;
extern MethodInfo m19738_MI;
extern MethodInfo m19732_MI;
extern MethodInfo m19740_MI;
extern MethodInfo m19741_MI;
extern MethodInfo m19744_MI;
extern MethodInfo m19745_MI;
extern MethodInfo m19746_MI;
extern MethodInfo m19747_MI;
extern MethodInfo m19748_MI;
extern MethodInfo m19749_MI;
extern MethodInfo m19743_MI;
extern MethodInfo m19751_MI;
extern MethodInfo m19752_MI;
extern MethodInfo m19755_MI;
extern MethodInfo m19756_MI;
extern MethodInfo m19757_MI;
extern MethodInfo m19758_MI;
extern MethodInfo m19759_MI;
extern MethodInfo m19760_MI;
extern MethodInfo m19754_MI;
extern MethodInfo m19762_MI;
extern MethodInfo m19763_MI;
extern MethodInfo m19494_MI;
extern MethodInfo m19496_MI;
extern MethodInfo m19498_MI;
extern MethodInfo m19499_MI;
extern MethodInfo m19500_MI;
extern MethodInfo m19501_MI;
extern MethodInfo m19490_MI;
extern MethodInfo m19503_MI;
extern MethodInfo m19504_MI;
static MethodInfo* t3682_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25656_MI,
	&m5907_MI,
	&m25657_MI,
	&m25658_MI,
	&m25659_MI,
	&m25660_MI,
	&m25661_MI,
	&m5908_MI,
	&m25655_MI,
	&m25663_MI,
	&m25664_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6471_TI;
extern TypeInfo t6472_TI;
extern TypeInfo t6473_TI;
extern TypeInfo t5104_TI;
extern TypeInfo t5105_TI;
extern TypeInfo t5106_TI;
extern TypeInfo t5107_TI;
extern TypeInfo t5108_TI;
extern TypeInfo t5109_TI;
extern TypeInfo t5110_TI;
extern TypeInfo t5111_TI;
extern TypeInfo t5112_TI;
extern TypeInfo t5113_TI;
extern TypeInfo t5114_TI;
extern TypeInfo t5115_TI;
extern TypeInfo t5116_TI;
extern TypeInfo t5117_TI;
extern TypeInfo t5118_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2186_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t3682_ITIs[] = 
{
	&t6471_TI,
	&t6472_TI,
	&t6473_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static Il2CppInterfaceOffsetPair t3682_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6471_TI, 21},
	{ &t6472_TI, 28},
	{ &t6473_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3682_0_0_0;
extern Il2CppType t3682_1_0_0;
extern TypeInfo t20_TI;
#include "t1437.h"
extern TypeInfo t1437_TI;
extern TypeInfo t348_TI;
TypeInfo t3682_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ArgInfoType[]", "System.Runtime.Remoting.Messaging", t3682_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1437_TI, t3682_ITIs, t3682_VT, &EmptyCustomAttributesCache, &t348_TI, &t3682_0_0_0, &t3682_1_0_0, t3682_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1451_TI;



// Metadata Definition System.Runtime.Remoting.Messaging.Header[]
static MethodInfo* t1451_MIs[] =
{
	NULL
};
extern MethodInfo m25667_MI;
extern MethodInfo m25668_MI;
extern MethodInfo m25669_MI;
extern MethodInfo m25670_MI;
extern MethodInfo m25671_MI;
extern MethodInfo m25672_MI;
extern MethodInfo m25666_MI;
extern MethodInfo m25674_MI;
extern MethodInfo m25675_MI;
static MethodInfo* t1451_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25667_MI,
	&m5907_MI,
	&m25668_MI,
	&m25669_MI,
	&m25670_MI,
	&m25671_MI,
	&m25672_MI,
	&m5908_MI,
	&m25666_MI,
	&m25674_MI,
	&m25675_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6474_TI;
extern TypeInfo t6475_TI;
extern TypeInfo t6476_TI;
static TypeInfo* t1451_ITIs[] = 
{
	&t6474_TI,
	&t6475_TI,
	&t6476_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1451_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6474_TI, 21},
	{ &t6475_TI, 28},
	{ &t6476_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1451_0_0_0;
extern Il2CppType t1451_1_0_0;
struct t1448;
extern TypeInfo t1448_TI;
extern CustomAttributesCache t1448__CustomAttributeCache;
TypeInfo t1451_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Header[]", "System.Runtime.Remoting.Messaging", t1451_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1448_TI, t1451_ITIs, t1451_VT, &EmptyCustomAttributesCache, &t1451_TI, &t1451_0_0_0, &t1451_1_0_0, t1451_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1448 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3683_TI;



// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute[]
static MethodInfo* t3683_MIs[] =
{
	NULL
};
extern MethodInfo m25678_MI;
extern MethodInfo m25679_MI;
extern MethodInfo m25680_MI;
extern MethodInfo m25681_MI;
extern MethodInfo m25682_MI;
extern MethodInfo m25683_MI;
extern MethodInfo m25677_MI;
extern MethodInfo m25685_MI;
extern MethodInfo m25686_MI;
extern MethodInfo m25601_MI;
extern MethodInfo m25602_MI;
extern MethodInfo m25603_MI;
extern MethodInfo m25604_MI;
extern MethodInfo m25605_MI;
extern MethodInfo m25606_MI;
extern MethodInfo m25600_MI;
extern MethodInfo m25608_MI;
extern MethodInfo m25609_MI;
extern MethodInfo m22323_MI;
extern MethodInfo m22324_MI;
extern MethodInfo m22325_MI;
extern MethodInfo m22326_MI;
extern MethodInfo m22327_MI;
extern MethodInfo m22328_MI;
extern MethodInfo m22322_MI;
extern MethodInfo m22330_MI;
extern MethodInfo m22331_MI;
extern MethodInfo m22334_MI;
extern MethodInfo m22335_MI;
extern MethodInfo m22336_MI;
extern MethodInfo m22337_MI;
extern MethodInfo m22338_MI;
extern MethodInfo m22339_MI;
extern MethodInfo m22333_MI;
extern MethodInfo m22341_MI;
extern MethodInfo m22342_MI;
static MethodInfo* t3683_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25678_MI,
	&m5907_MI,
	&m25679_MI,
	&m25680_MI,
	&m25681_MI,
	&m25682_MI,
	&m25683_MI,
	&m5908_MI,
	&m25677_MI,
	&m25685_MI,
	&m25686_MI,
	&m5905_MI,
	&m5906_MI,
	&m25601_MI,
	&m5907_MI,
	&m25602_MI,
	&m25603_MI,
	&m25604_MI,
	&m25605_MI,
	&m25606_MI,
	&m5908_MI,
	&m25600_MI,
	&m25608_MI,
	&m25609_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6477_TI;
extern TypeInfo t6478_TI;
extern TypeInfo t6479_TI;
extern TypeInfo t6456_TI;
extern TypeInfo t6457_TI;
extern TypeInfo t6458_TI;
extern TypeInfo t5619_TI;
extern TypeInfo t5620_TI;
extern TypeInfo t5621_TI;
extern TypeInfo t5622_TI;
extern TypeInfo t5623_TI;
extern TypeInfo t5624_TI;
static TypeInfo* t3683_ITIs[] = 
{
	&t6477_TI,
	&t6478_TI,
	&t6479_TI,
	&t6456_TI,
	&t6457_TI,
	&t6458_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3683_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6477_TI, 21},
	{ &t6478_TI, 28},
	{ &t6479_TI, 33},
	{ &t6456_TI, 34},
	{ &t6457_TI, 41},
	{ &t6458_TI, 46},
	{ &t5619_TI, 47},
	{ &t5620_TI, 54},
	{ &t5621_TI, 59},
	{ &t5622_TI, 60},
	{ &t5623_TI, 67},
	{ &t5624_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3683_0_0_0;
extern Il2CppType t3683_1_0_0;
struct t1464;
extern TypeInfo t1464_TI;
extern CustomAttributesCache t1464__CustomAttributeCache;
extern CustomAttributesCache t1464__CustomAttributeCache_m7945;
extern CustomAttributesCache t1464__CustomAttributeCache_m7946;
TypeInfo t3683_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ProxyAttribute[]", "System.Runtime.Remoting.Proxies", t3683_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1464_TI, t3683_ITIs, t3683_VT, &EmptyCustomAttributesCache, &t3683_TI, &t3683_0_0_0, &t3683_1_0_0, t3683_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1464 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2052_TI;



// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler[]
static MethodInfo* t2052_MIs[] =
{
	NULL
};
extern MethodInfo m25689_MI;
extern MethodInfo m25690_MI;
extern MethodInfo m25691_MI;
extern MethodInfo m25692_MI;
extern MethodInfo m25693_MI;
extern MethodInfo m25694_MI;
extern MethodInfo m25688_MI;
extern MethodInfo m25696_MI;
extern MethodInfo m25697_MI;
static MethodInfo* t2052_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25689_MI,
	&m5907_MI,
	&m25690_MI,
	&m25691_MI,
	&m25692_MI,
	&m25693_MI,
	&m25694_MI,
	&m5908_MI,
	&m25688_MI,
	&m25696_MI,
	&m25697_MI,
};
extern TypeInfo t6480_TI;
extern TypeInfo t6481_TI;
extern TypeInfo t6482_TI;
static TypeInfo* t2052_ITIs[] = 
{
	&t6480_TI,
	&t6481_TI,
	&t6482_TI,
};
static Il2CppInterfaceOffsetPair t2052_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6480_TI, 21},
	{ &t6481_TI, 28},
	{ &t6482_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2052_0_0_0;
extern Il2CppType t2052_1_0_0;
struct t2051;
extern TypeInfo t2051_TI;
extern CustomAttributesCache t2051__CustomAttributeCache;
TypeInfo t2052_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ITrackingHandler[]", "System.Runtime.Remoting.Services", t2052_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2051_TI, t2052_ITIs, t2052_VT, &EmptyCustomAttributesCache, &t2052_TI, &t2052_0_0_0, &t2052_1_0_0, t2052_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3684_TI;



// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode[]
static MethodInfo* t3684_MIs[] =
{
	NULL
};
extern MethodInfo m25700_MI;
extern MethodInfo m25701_MI;
extern MethodInfo m25702_MI;
extern MethodInfo m25703_MI;
extern MethodInfo m25704_MI;
extern MethodInfo m25705_MI;
extern MethodInfo m25699_MI;
extern MethodInfo m25707_MI;
extern MethodInfo m25708_MI;
static MethodInfo* t3684_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25700_MI,
	&m5907_MI,
	&m25701_MI,
	&m25702_MI,
	&m25703_MI,
	&m25704_MI,
	&m25705_MI,
	&m5908_MI,
	&m25699_MI,
	&m25707_MI,
	&m25708_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6483_TI;
extern TypeInfo t6484_TI;
extern TypeInfo t6485_TI;
static TypeInfo* t3684_ITIs[] = 
{
	&t6483_TI,
	&t6484_TI,
	&t6485_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3684_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6483_TI, 21},
	{ &t6484_TI, 28},
	{ &t6485_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3684_0_0_0;
extern Il2CppType t3684_1_0_0;
#include "t1484.h"
extern TypeInfo t1484_TI;
extern TypeInfo t44_TI;
extern CustomAttributesCache t1484__CustomAttributeCache;
TypeInfo t3684_TI = 
{
	&g_mscorlib_dll_Image, NULL, "WellKnownObjectMode[]", "System.Runtime.Remoting", t3684_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1484_TI, t3684_ITIs, t3684_VT, &EmptyCustomAttributesCache, &t44_TI, &t3684_0_0_0, &t3684_1_0_0, t3684_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3685_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement[]
static MethodInfo* t3685_MIs[] =
{
	NULL
};
extern MethodInfo m25711_MI;
extern MethodInfo m25712_MI;
extern MethodInfo m25713_MI;
extern MethodInfo m25714_MI;
extern MethodInfo m25715_MI;
extern MethodInfo m25716_MI;
extern MethodInfo m25710_MI;
extern MethodInfo m25718_MI;
extern MethodInfo m25719_MI;
static MethodInfo* t3685_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25711_MI,
	&m5907_MI,
	&m25712_MI,
	&m25713_MI,
	&m25714_MI,
	&m25715_MI,
	&m25716_MI,
	&m5908_MI,
	&m25710_MI,
	&m25718_MI,
	&m25719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6486_TI;
extern TypeInfo t6487_TI;
extern TypeInfo t6488_TI;
static TypeInfo* t3685_ITIs[] = 
{
	&t6486_TI,
	&t6487_TI,
	&t6488_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3685_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6486_TI, 21},
	{ &t6487_TI, 28},
	{ &t6488_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3685_0_0_0;
extern Il2CppType t3685_1_0_0;
#include "t1490.h"
extern TypeInfo t1490_TI;
TypeInfo t3685_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BinaryElement[]", "System.Runtime.Serialization.Formatters.Binary", t3685_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1490_TI, t3685_ITIs, t3685_VT, &EmptyCustomAttributesCache, &t348_TI, &t3685_0_0_0, &t3685_1_0_0, t3685_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2056_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag[]
static MethodInfo* t2056_MIs[] =
{
	NULL
};
extern MethodInfo m25722_MI;
extern MethodInfo m25723_MI;
extern MethodInfo m25724_MI;
extern MethodInfo m25725_MI;
extern MethodInfo m25726_MI;
extern MethodInfo m25727_MI;
extern MethodInfo m25721_MI;
extern MethodInfo m25729_MI;
extern MethodInfo m25730_MI;
static MethodInfo* t2056_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25722_MI,
	&m5907_MI,
	&m25723_MI,
	&m25724_MI,
	&m25725_MI,
	&m25726_MI,
	&m25727_MI,
	&m5908_MI,
	&m25721_MI,
	&m25729_MI,
	&m25730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6489_TI;
extern TypeInfo t6490_TI;
extern TypeInfo t6491_TI;
static TypeInfo* t2056_ITIs[] = 
{
	&t6489_TI,
	&t6490_TI,
	&t6491_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2056_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6489_TI, 21},
	{ &t6490_TI, 28},
	{ &t6491_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2056_0_0_0;
extern Il2CppType t2056_1_0_0;
#include "t1491.h"
extern TypeInfo t1491_TI;
TypeInfo t2056_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeTag[]", "System.Runtime.Serialization.Formatters.Binary", t2056_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1491_TI, t2056_ITIs, t2056_VT, &EmptyCustomAttributesCache, &t348_TI, &t2056_0_0_0, &t2056_1_0_0, t2056_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3686_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags[]
static MethodInfo* t3686_MIs[] =
{
	NULL
};
extern MethodInfo m25733_MI;
extern MethodInfo m25734_MI;
extern MethodInfo m25735_MI;
extern MethodInfo m25736_MI;
extern MethodInfo m25737_MI;
extern MethodInfo m25738_MI;
extern MethodInfo m25732_MI;
extern MethodInfo m25740_MI;
extern MethodInfo m25741_MI;
static MethodInfo* t3686_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25733_MI,
	&m5907_MI,
	&m25734_MI,
	&m25735_MI,
	&m25736_MI,
	&m25737_MI,
	&m25738_MI,
	&m5908_MI,
	&m25732_MI,
	&m25740_MI,
	&m25741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6492_TI;
extern TypeInfo t6493_TI;
extern TypeInfo t6494_TI;
static TypeInfo* t3686_ITIs[] = 
{
	&t6492_TI,
	&t6493_TI,
	&t6494_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3686_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6492_TI, 21},
	{ &t6493_TI, 28},
	{ &t6494_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3686_0_0_0;
extern Il2CppType t3686_1_0_0;
#include "t1492.h"
extern TypeInfo t1492_TI;
TypeInfo t3686_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodFlags[]", "System.Runtime.Serialization.Formatters.Binary", t3686_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1492_TI, t3686_ITIs, t3686_VT, &EmptyCustomAttributesCache, &t44_TI, &t3686_0_0_0, &t3686_1_0_0, t3686_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3687_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag[]
static MethodInfo* t3687_MIs[] =
{
	NULL
};
extern MethodInfo m25744_MI;
extern MethodInfo m25745_MI;
extern MethodInfo m25746_MI;
extern MethodInfo m25747_MI;
extern MethodInfo m25748_MI;
extern MethodInfo m25749_MI;
extern MethodInfo m25743_MI;
extern MethodInfo m25751_MI;
extern MethodInfo m25752_MI;
static MethodInfo* t3687_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25744_MI,
	&m5907_MI,
	&m25745_MI,
	&m25746_MI,
	&m25747_MI,
	&m25748_MI,
	&m25749_MI,
	&m5908_MI,
	&m25743_MI,
	&m25751_MI,
	&m25752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6495_TI;
extern TypeInfo t6496_TI;
extern TypeInfo t6497_TI;
static TypeInfo* t3687_ITIs[] = 
{
	&t6495_TI,
	&t6496_TI,
	&t6497_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3687_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6495_TI, 21},
	{ &t6496_TI, 28},
	{ &t6497_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3687_0_0_0;
extern Il2CppType t3687_1_0_0;
#include "t1493.h"
extern TypeInfo t1493_TI;
TypeInfo t3687_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReturnTypeTag[]", "System.Runtime.Serialization.Formatters.Binary", t3687_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1493_TI, t3687_ITIs, t3687_VT, &EmptyCustomAttributesCache, &t348_TI, &t3687_0_0_0, &t3687_1_0_0, t3687_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2053_TI;



// Metadata Definition System.DateTime[]
static MethodInfo* t2053_MIs[] =
{
	NULL
};
extern MethodInfo m25755_MI;
extern MethodInfo m25756_MI;
extern MethodInfo m25757_MI;
extern MethodInfo m25758_MI;
extern MethodInfo m25759_MI;
extern MethodInfo m25760_MI;
extern MethodInfo m25754_MI;
extern MethodInfo m25762_MI;
extern MethodInfo m25763_MI;
extern MethodInfo m25766_MI;
extern MethodInfo m25767_MI;
extern MethodInfo m25768_MI;
extern MethodInfo m25769_MI;
extern MethodInfo m25770_MI;
extern MethodInfo m25771_MI;
extern MethodInfo m25765_MI;
extern MethodInfo m25773_MI;
extern MethodInfo m25774_MI;
extern MethodInfo m25777_MI;
extern MethodInfo m25778_MI;
extern MethodInfo m25779_MI;
extern MethodInfo m25780_MI;
extern MethodInfo m25781_MI;
extern MethodInfo m25782_MI;
extern MethodInfo m25776_MI;
extern MethodInfo m25784_MI;
extern MethodInfo m25785_MI;
static MethodInfo* t2053_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25755_MI,
	&m5907_MI,
	&m25756_MI,
	&m25757_MI,
	&m25758_MI,
	&m25759_MI,
	&m25760_MI,
	&m5908_MI,
	&m25754_MI,
	&m25762_MI,
	&m25763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m25766_MI,
	&m5907_MI,
	&m25767_MI,
	&m25768_MI,
	&m25769_MI,
	&m25770_MI,
	&m25771_MI,
	&m5908_MI,
	&m25765_MI,
	&m25773_MI,
	&m25774_MI,
	&m5905_MI,
	&m5906_MI,
	&m25777_MI,
	&m5907_MI,
	&m25778_MI,
	&m25779_MI,
	&m25780_MI,
	&m25781_MI,
	&m25782_MI,
	&m5908_MI,
	&m25776_MI,
	&m25784_MI,
	&m25785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6498_TI;
extern TypeInfo t6499_TI;
extern TypeInfo t6500_TI;
extern TypeInfo t6501_TI;
extern TypeInfo t6502_TI;
extern TypeInfo t6503_TI;
extern TypeInfo t6504_TI;
extern TypeInfo t6505_TI;
extern TypeInfo t6506_TI;
static TypeInfo* t2053_ITIs[] = 
{
	&t6498_TI,
	&t6499_TI,
	&t6500_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6501_TI,
	&t6502_TI,
	&t6503_TI,
	&t6504_TI,
	&t6505_TI,
	&t6506_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2053_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6498_TI, 21},
	{ &t6499_TI, 28},
	{ &t6500_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t6501_TI, 73},
	{ &t6502_TI, 80},
	{ &t6503_TI, 85},
	{ &t6504_TI, 86},
	{ &t6505_TI, 93},
	{ &t6506_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2053_0_0_0;
extern Il2CppType t2053_1_0_0;
#include "t465.h"
extern TypeInfo t465_TI;
TypeInfo t2053_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DateTime[]", "System", t2053_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t465_TI, t2053_ITIs, t2053_VT, &EmptyCustomAttributesCache, &t2053_TI, &t2053_0_0_0, &t2053_1_0_0, t2053_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t465 ), -1, sizeof(t2053_SFs), 0, -1, 8449, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3688_TI;



// Metadata Definition System.IComparable`1<System.DateTime>[]
static MethodInfo* t3688_MIs[] =
{
	NULL
};
static MethodInfo* t3688_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25766_MI,
	&m5907_MI,
	&m25767_MI,
	&m25768_MI,
	&m25769_MI,
	&m25770_MI,
	&m25771_MI,
	&m5908_MI,
	&m25765_MI,
	&m25773_MI,
	&m25774_MI,
};
static TypeInfo* t3688_ITIs[] = 
{
	&t6501_TI,
	&t6502_TI,
	&t6503_TI,
};
static Il2CppInterfaceOffsetPair t3688_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6501_TI, 21},
	{ &t6502_TI, 28},
	{ &t6503_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3688_0_0_0;
extern Il2CppType t3688_1_0_0;
struct t2074;
extern TypeInfo t2074_TI;
TypeInfo t3688_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3688_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2074_TI, t3688_ITIs, t3688_VT, &EmptyCustomAttributesCache, &t3688_TI, &t3688_0_0_0, &t3688_1_0_0, t3688_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3689_TI;



// Metadata Definition System.IEquatable`1<System.DateTime>[]
static MethodInfo* t3689_MIs[] =
{
	NULL
};
static MethodInfo* t3689_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25777_MI,
	&m5907_MI,
	&m25778_MI,
	&m25779_MI,
	&m25780_MI,
	&m25781_MI,
	&m25782_MI,
	&m5908_MI,
	&m25776_MI,
	&m25784_MI,
	&m25785_MI,
};
static TypeInfo* t3689_ITIs[] = 
{
	&t6504_TI,
	&t6505_TI,
	&t6506_TI,
};
static Il2CppInterfaceOffsetPair t3689_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6504_TI, 21},
	{ &t6505_TI, 28},
	{ &t6506_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3689_0_0_0;
extern Il2CppType t3689_1_0_0;
struct t2075;
extern TypeInfo t2075_TI;
TypeInfo t3689_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3689_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2075_TI, t3689_ITIs, t3689_VT, &EmptyCustomAttributesCache, &t3689_TI, &t3689_0_0_0, &t3689_1_0_0, t3689_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2054_TI;



// Metadata Definition System.Decimal[]
static MethodInfo* t2054_MIs[] =
{
	NULL
};
extern MethodInfo m25788_MI;
extern MethodInfo m25789_MI;
extern MethodInfo m25790_MI;
extern MethodInfo m25791_MI;
extern MethodInfo m25792_MI;
extern MethodInfo m25793_MI;
extern MethodInfo m25787_MI;
extern MethodInfo m25795_MI;
extern MethodInfo m25796_MI;
extern MethodInfo m25799_MI;
extern MethodInfo m25800_MI;
extern MethodInfo m25801_MI;
extern MethodInfo m25802_MI;
extern MethodInfo m25803_MI;
extern MethodInfo m25804_MI;
extern MethodInfo m25798_MI;
extern MethodInfo m25806_MI;
extern MethodInfo m25807_MI;
extern MethodInfo m25810_MI;
extern MethodInfo m25811_MI;
extern MethodInfo m25812_MI;
extern MethodInfo m25813_MI;
extern MethodInfo m25814_MI;
extern MethodInfo m25815_MI;
extern MethodInfo m25809_MI;
extern MethodInfo m25817_MI;
extern MethodInfo m25818_MI;
static MethodInfo* t2054_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25788_MI,
	&m5907_MI,
	&m25789_MI,
	&m25790_MI,
	&m25791_MI,
	&m25792_MI,
	&m25793_MI,
	&m5908_MI,
	&m25787_MI,
	&m25795_MI,
	&m25796_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m25799_MI,
	&m5907_MI,
	&m25800_MI,
	&m25801_MI,
	&m25802_MI,
	&m25803_MI,
	&m25804_MI,
	&m5908_MI,
	&m25798_MI,
	&m25806_MI,
	&m25807_MI,
	&m5905_MI,
	&m5906_MI,
	&m25810_MI,
	&m5907_MI,
	&m25811_MI,
	&m25812_MI,
	&m25813_MI,
	&m25814_MI,
	&m25815_MI,
	&m5908_MI,
	&m25809_MI,
	&m25817_MI,
	&m25818_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6507_TI;
extern TypeInfo t6508_TI;
extern TypeInfo t6509_TI;
extern TypeInfo t6510_TI;
extern TypeInfo t6511_TI;
extern TypeInfo t6512_TI;
extern TypeInfo t6513_TI;
extern TypeInfo t6514_TI;
extern TypeInfo t6515_TI;
static TypeInfo* t2054_ITIs[] = 
{
	&t6507_TI,
	&t6508_TI,
	&t6509_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6510_TI,
	&t6511_TI,
	&t6512_TI,
	&t6513_TI,
	&t6514_TI,
	&t6515_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2054_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6507_TI, 21},
	{ &t6508_TI, 28},
	{ &t6509_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t6510_TI, 73},
	{ &t6511_TI, 80},
	{ &t6512_TI, 85},
	{ &t6513_TI, 86},
	{ &t6514_TI, 93},
	{ &t6515_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2054_0_0_0;
extern Il2CppType t2054_1_0_0;
#include "t1126.h"
extern TypeInfo t1126_TI;
extern CustomAttributesCache t1126__CustomAttributeCache;
extern CustomAttributesCache t1126__CustomAttributeCache_MinValue;
extern CustomAttributesCache t1126__CustomAttributeCache_MaxValue;
extern CustomAttributesCache t1126__CustomAttributeCache_MinusOne;
extern CustomAttributesCache t1126__CustomAttributeCache_One;
extern CustomAttributesCache t1126__CustomAttributeCache_m5698;
extern CustomAttributesCache t1126__CustomAttributeCache_m5700;
extern CustomAttributesCache t1126__CustomAttributeCache_m5731;
extern CustomAttributesCache t1126__CustomAttributeCache_m5759;
extern CustomAttributesCache t1126__CustomAttributeCache_m5761;
extern CustomAttributesCache t1126__CustomAttributeCache_m5763;
extern CustomAttributesCache t1126__CustomAttributeCache_m5765;
extern CustomAttributesCache t1126__CustomAttributeCache_m5767;
extern CustomAttributesCache t1126__CustomAttributeCache_m5769;
extern CustomAttributesCache t1126__CustomAttributeCache_m5771;
extern CustomAttributesCache t1126__CustomAttributeCache_m5773;
TypeInfo t2054_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Decimal[]", "System", t2054_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1126_TI, t2054_ITIs, t2054_VT, &EmptyCustomAttributesCache, &t2054_TI, &t2054_0_0_0, &t2054_1_0_0, t2054_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1126 ), -1, sizeof(t2054_SFs), 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, true, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3690_TI;



// Metadata Definition System.IComparable`1<System.Decimal>[]
static MethodInfo* t3690_MIs[] =
{
	NULL
};
static MethodInfo* t3690_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25799_MI,
	&m5907_MI,
	&m25800_MI,
	&m25801_MI,
	&m25802_MI,
	&m25803_MI,
	&m25804_MI,
	&m5908_MI,
	&m25798_MI,
	&m25806_MI,
	&m25807_MI,
};
static TypeInfo* t3690_ITIs[] = 
{
	&t6510_TI,
	&t6511_TI,
	&t6512_TI,
};
static Il2CppInterfaceOffsetPair t3690_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6510_TI, 21},
	{ &t6511_TI, 28},
	{ &t6512_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3690_0_0_0;
extern Il2CppType t3690_1_0_0;
struct t1757;
extern TypeInfo t1757_TI;
TypeInfo t3690_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3690_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1757_TI, t3690_ITIs, t3690_VT, &EmptyCustomAttributesCache, &t3690_TI, &t3690_0_0_0, &t3690_1_0_0, t3690_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3691_TI;



// Metadata Definition System.IEquatable`1<System.Decimal>[]
static MethodInfo* t3691_MIs[] =
{
	NULL
};
static MethodInfo* t3691_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25810_MI,
	&m5907_MI,
	&m25811_MI,
	&m25812_MI,
	&m25813_MI,
	&m25814_MI,
	&m25815_MI,
	&m5908_MI,
	&m25809_MI,
	&m25817_MI,
	&m25818_MI,
};
static TypeInfo* t3691_ITIs[] = 
{
	&t6513_TI,
	&t6514_TI,
	&t6515_TI,
};
static Il2CppInterfaceOffsetPair t3691_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6513_TI, 21},
	{ &t6514_TI, 28},
	{ &t6515_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3691_0_0_0;
extern Il2CppType t3691_1_0_0;
struct t1758;
extern TypeInfo t1758_TI;
TypeInfo t3691_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3691_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1758_TI, t3691_ITIs, t3691_VT, &EmptyCustomAttributesCache, &t3691_TI, &t3691_0_0_0, &t3691_1_0_0, t3691_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2055_TI;



// Metadata Definition System.TimeSpan[]
static MethodInfo* t2055_MIs[] =
{
	NULL
};
extern MethodInfo m25821_MI;
extern MethodInfo m25822_MI;
extern MethodInfo m25823_MI;
extern MethodInfo m25824_MI;
extern MethodInfo m25825_MI;
extern MethodInfo m25826_MI;
extern MethodInfo m25820_MI;
extern MethodInfo m25828_MI;
extern MethodInfo m25829_MI;
extern MethodInfo m25832_MI;
extern MethodInfo m25833_MI;
extern MethodInfo m25834_MI;
extern MethodInfo m25835_MI;
extern MethodInfo m25836_MI;
extern MethodInfo m25837_MI;
extern MethodInfo m25831_MI;
extern MethodInfo m25839_MI;
extern MethodInfo m25840_MI;
extern MethodInfo m25843_MI;
extern MethodInfo m25844_MI;
extern MethodInfo m25845_MI;
extern MethodInfo m25846_MI;
extern MethodInfo m25847_MI;
extern MethodInfo m25848_MI;
extern MethodInfo m25842_MI;
extern MethodInfo m25850_MI;
extern MethodInfo m25851_MI;
static MethodInfo* t2055_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25821_MI,
	&m5907_MI,
	&m25822_MI,
	&m25823_MI,
	&m25824_MI,
	&m25825_MI,
	&m25826_MI,
	&m5908_MI,
	&m25820_MI,
	&m25828_MI,
	&m25829_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m25832_MI,
	&m5907_MI,
	&m25833_MI,
	&m25834_MI,
	&m25835_MI,
	&m25836_MI,
	&m25837_MI,
	&m5908_MI,
	&m25831_MI,
	&m25839_MI,
	&m25840_MI,
	&m5905_MI,
	&m5906_MI,
	&m25843_MI,
	&m5907_MI,
	&m25844_MI,
	&m25845_MI,
	&m25846_MI,
	&m25847_MI,
	&m25848_MI,
	&m5908_MI,
	&m25842_MI,
	&m25850_MI,
	&m25851_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6516_TI;
extern TypeInfo t6517_TI;
extern TypeInfo t6518_TI;
extern TypeInfo t6519_TI;
extern TypeInfo t6520_TI;
extern TypeInfo t6521_TI;
extern TypeInfo t6522_TI;
extern TypeInfo t6523_TI;
extern TypeInfo t6524_TI;
static TypeInfo* t2055_ITIs[] = 
{
	&t6516_TI,
	&t6517_TI,
	&t6518_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6519_TI,
	&t6520_TI,
	&t6521_TI,
	&t6522_TI,
	&t6523_TI,
	&t6524_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2055_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6516_TI, 21},
	{ &t6517_TI, 28},
	{ &t6518_TI, 33},
	{ &t5113_TI, 34},
	{ &t5114_TI, 41},
	{ &t5115_TI, 46},
	{ &t6519_TI, 47},
	{ &t6520_TI, 54},
	{ &t6521_TI, 59},
	{ &t6522_TI, 60},
	{ &t6523_TI, 67},
	{ &t6524_TI, 72},
	{ &t5116_TI, 73},
	{ &t5117_TI, 80},
	{ &t5118_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2055_0_0_0;
extern Il2CppType t2055_1_0_0;
#include "t816.h"
extern TypeInfo t816_TI;
extern CustomAttributesCache t816__CustomAttributeCache;
TypeInfo t2055_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TimeSpan[]", "System", t2055_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t816_TI, t2055_ITIs, t2055_VT, &EmptyCustomAttributesCache, &t2055_TI, &t2055_0_0_0, &t2055_1_0_0, t2055_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t816 ), -1, sizeof(t2055_SFs), 0, -1, 8457, 1, false, false, false, false, false, true, false, false, false, true, false, true, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3692_TI;



// Metadata Definition System.IComparable`1<System.TimeSpan>[]
static MethodInfo* t3692_MIs[] =
{
	NULL
};
static MethodInfo* t3692_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25832_MI,
	&m5907_MI,
	&m25833_MI,
	&m25834_MI,
	&m25835_MI,
	&m25836_MI,
	&m25837_MI,
	&m5908_MI,
	&m25831_MI,
	&m25839_MI,
	&m25840_MI,
};
static TypeInfo* t3692_ITIs[] = 
{
	&t6519_TI,
	&t6520_TI,
	&t6521_TI,
};
static Il2CppInterfaceOffsetPair t3692_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6519_TI, 21},
	{ &t6520_TI, 28},
	{ &t6521_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3692_0_0_0;
extern Il2CppType t3692_1_0_0;
struct t2099;
extern TypeInfo t2099_TI;
TypeInfo t3692_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3692_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2099_TI, t3692_ITIs, t3692_VT, &EmptyCustomAttributesCache, &t3692_TI, &t3692_0_0_0, &t3692_1_0_0, t3692_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3693_TI;



// Metadata Definition System.IEquatable`1<System.TimeSpan>[]
static MethodInfo* t3693_MIs[] =
{
	NULL
};
static MethodInfo* t3693_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25843_MI,
	&m5907_MI,
	&m25844_MI,
	&m25845_MI,
	&m25846_MI,
	&m25847_MI,
	&m25848_MI,
	&m5908_MI,
	&m25842_MI,
	&m25850_MI,
	&m25851_MI,
};
static TypeInfo* t3693_ITIs[] = 
{
	&t6522_TI,
	&t6523_TI,
	&t6524_TI,
};
static Il2CppInterfaceOffsetPair t3693_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6522_TI, 21},
	{ &t6523_TI, 28},
	{ &t6524_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3693_0_0_0;
extern Il2CppType t3693_1_0_0;
struct t2100;
extern TypeInfo t2100_TI;
TypeInfo t3693_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3693_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2100_TI, t3693_ITIs, t3693_VT, &EmptyCustomAttributesCache, &t3693_TI, &t3693_0_0_0, &t3693_1_0_0, t3693_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3694_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle[]
static MethodInfo* t3694_MIs[] =
{
	NULL
};
extern MethodInfo m25854_MI;
extern MethodInfo m25855_MI;
extern MethodInfo m25856_MI;
extern MethodInfo m25857_MI;
extern MethodInfo m25858_MI;
extern MethodInfo m25859_MI;
extern MethodInfo m25853_MI;
extern MethodInfo m25861_MI;
extern MethodInfo m25862_MI;
static MethodInfo* t3694_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25854_MI,
	&m5907_MI,
	&m25855_MI,
	&m25856_MI,
	&m25857_MI,
	&m25858_MI,
	&m25859_MI,
	&m5908_MI,
	&m25853_MI,
	&m25861_MI,
	&m25862_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6525_TI;
extern TypeInfo t6526_TI;
extern TypeInfo t6527_TI;
static TypeInfo* t3694_ITIs[] = 
{
	&t6525_TI,
	&t6526_TI,
	&t6527_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3694_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6525_TI, 21},
	{ &t6526_TI, 28},
	{ &t6527_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3694_0_0_0;
extern Il2CppType t3694_1_0_0;
#include "t1495.h"
extern TypeInfo t1495_TI;
extern CustomAttributesCache t1495__CustomAttributeCache;
TypeInfo t3694_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FormatterAssemblyStyle[]", "System.Runtime.Serialization.Formatters", t3694_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1495_TI, t3694_ITIs, t3694_VT, &EmptyCustomAttributesCache, &t44_TI, &t3694_0_0_0, &t3694_1_0_0, t3694_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3695_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle[]
static MethodInfo* t3695_MIs[] =
{
	NULL
};
extern MethodInfo m25865_MI;
extern MethodInfo m25866_MI;
extern MethodInfo m25867_MI;
extern MethodInfo m25868_MI;
extern MethodInfo m25869_MI;
extern MethodInfo m25870_MI;
extern MethodInfo m25864_MI;
extern MethodInfo m25872_MI;
extern MethodInfo m25873_MI;
static MethodInfo* t3695_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25865_MI,
	&m5907_MI,
	&m25866_MI,
	&m25867_MI,
	&m25868_MI,
	&m25869_MI,
	&m25870_MI,
	&m5908_MI,
	&m25864_MI,
	&m25872_MI,
	&m25873_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6528_TI;
extern TypeInfo t6529_TI;
extern TypeInfo t6530_TI;
static TypeInfo* t3695_ITIs[] = 
{
	&t6528_TI,
	&t6529_TI,
	&t6530_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3695_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6528_TI, 21},
	{ &t6529_TI, 28},
	{ &t6530_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3695_0_0_0;
extern Il2CppType t3695_1_0_0;
#include "t1496.h"
extern TypeInfo t1496_TI;
extern CustomAttributesCache t1496__CustomAttributeCache;
TypeInfo t3695_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FormatterTypeStyle[]", "System.Runtime.Serialization.Formatters", t3695_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1496_TI, t3695_ITIs, t3695_VT, &EmptyCustomAttributesCache, &t44_TI, &t3695_0_0_0, &t3695_1_0_0, t3695_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3696_TI;



// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel[]
static MethodInfo* t3696_MIs[] =
{
	NULL
};
extern MethodInfo m25876_MI;
extern MethodInfo m25877_MI;
extern MethodInfo m25878_MI;
extern MethodInfo m25879_MI;
extern MethodInfo m25880_MI;
extern MethodInfo m25881_MI;
extern MethodInfo m25875_MI;
extern MethodInfo m25883_MI;
extern MethodInfo m25884_MI;
static MethodInfo* t3696_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25876_MI,
	&m5907_MI,
	&m25877_MI,
	&m25878_MI,
	&m25879_MI,
	&m25880_MI,
	&m25881_MI,
	&m5908_MI,
	&m25875_MI,
	&m25883_MI,
	&m25884_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6531_TI;
extern TypeInfo t6532_TI;
extern TypeInfo t6533_TI;
static TypeInfo* t3696_ITIs[] = 
{
	&t6531_TI,
	&t6532_TI,
	&t6533_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3696_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6531_TI, 21},
	{ &t6532_TI, 28},
	{ &t6533_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3696_0_0_0;
extern Il2CppType t3696_1_0_0;
#include "t1497.h"
extern TypeInfo t1497_TI;
extern CustomAttributesCache t1497__CustomAttributeCache;
TypeInfo t3696_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeFilterLevel[]", "System.Runtime.Serialization.Formatters", t3696_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1497_TI, t3696_ITIs, t3696_VT, &EmptyCustomAttributesCache, &t44_TI, &t3696_0_0_0, &t3696_1_0_0, t3696_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3697_TI;



// Metadata Definition System.Runtime.Serialization.ObjectRecordStatus[]
static MethodInfo* t3697_MIs[] =
{
	NULL
};
extern MethodInfo m25887_MI;
extern MethodInfo m25888_MI;
extern MethodInfo m25889_MI;
extern MethodInfo m25890_MI;
extern MethodInfo m25891_MI;
extern MethodInfo m25892_MI;
extern MethodInfo m25886_MI;
extern MethodInfo m25894_MI;
extern MethodInfo m25895_MI;
static MethodInfo* t3697_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25887_MI,
	&m5907_MI,
	&m25888_MI,
	&m25889_MI,
	&m25890_MI,
	&m25891_MI,
	&m25892_MI,
	&m5908_MI,
	&m25886_MI,
	&m25894_MI,
	&m25895_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6534_TI;
extern TypeInfo t6535_TI;
extern TypeInfo t6536_TI;
static TypeInfo* t3697_ITIs[] = 
{
	&t6534_TI,
	&t6535_TI,
	&t6536_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3697_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6534_TI, 21},
	{ &t6535_TI, 28},
	{ &t6536_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3697_0_0_0;
extern Il2CppType t3697_1_0_0;
#include "t1513.h"
extern TypeInfo t1513_TI;
TypeInfo t3697_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObjectRecordStatus[]", "System.Runtime.Serialization", t3697_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1513_TI, t3697_ITIs, t3697_VT, &EmptyCustomAttributesCache, &t348_TI, &t3697_0_0_0, &t3697_1_0_0, t3697_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3698_TI;



// Metadata Definition System.Runtime.Serialization.OnDeserializedAttribute[]
static MethodInfo* t3698_MIs[] =
{
	NULL
};
extern MethodInfo m25898_MI;
extern MethodInfo m25899_MI;
extern MethodInfo m25900_MI;
extern MethodInfo m25901_MI;
extern MethodInfo m25902_MI;
extern MethodInfo m25903_MI;
extern MethodInfo m25897_MI;
extern MethodInfo m25905_MI;
extern MethodInfo m25906_MI;
static MethodInfo* t3698_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25898_MI,
	&m5907_MI,
	&m25899_MI,
	&m25900_MI,
	&m25901_MI,
	&m25902_MI,
	&m25903_MI,
	&m5908_MI,
	&m25897_MI,
	&m25905_MI,
	&m25906_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6537_TI;
extern TypeInfo t6538_TI;
extern TypeInfo t6539_TI;
static TypeInfo* t3698_ITIs[] = 
{
	&t6537_TI,
	&t6538_TI,
	&t6539_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3698_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6537_TI, 21},
	{ &t6538_TI, 28},
	{ &t6539_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3698_0_0_0;
extern Il2CppType t3698_1_0_0;
struct t1514;
extern TypeInfo t1514_TI;
extern CustomAttributesCache t1514__CustomAttributeCache;
TypeInfo t3698_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OnDeserializedAttribute[]", "System.Runtime.Serialization", t3698_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1514_TI, t3698_ITIs, t3698_VT, &EmptyCustomAttributesCache, &t3698_TI, &t3698_0_0_0, &t3698_1_0_0, t3698_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1514 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3699_TI;



// Metadata Definition System.Runtime.Serialization.OnDeserializingAttribute[]
static MethodInfo* t3699_MIs[] =
{
	NULL
};
extern MethodInfo m25909_MI;
extern MethodInfo m25910_MI;
extern MethodInfo m25911_MI;
extern MethodInfo m25912_MI;
extern MethodInfo m25913_MI;
extern MethodInfo m25914_MI;
extern MethodInfo m25908_MI;
extern MethodInfo m25916_MI;
extern MethodInfo m25917_MI;
static MethodInfo* t3699_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25909_MI,
	&m5907_MI,
	&m25910_MI,
	&m25911_MI,
	&m25912_MI,
	&m25913_MI,
	&m25914_MI,
	&m5908_MI,
	&m25908_MI,
	&m25916_MI,
	&m25917_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6540_TI;
extern TypeInfo t6541_TI;
extern TypeInfo t6542_TI;
static TypeInfo* t3699_ITIs[] = 
{
	&t6540_TI,
	&t6541_TI,
	&t6542_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3699_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6540_TI, 21},
	{ &t6541_TI, 28},
	{ &t6542_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3699_0_0_0;
extern Il2CppType t3699_1_0_0;
struct t1515;
extern TypeInfo t1515_TI;
extern CustomAttributesCache t1515__CustomAttributeCache;
TypeInfo t3699_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OnDeserializingAttribute[]", "System.Runtime.Serialization", t3699_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1515_TI, t3699_ITIs, t3699_VT, &EmptyCustomAttributesCache, &t3699_TI, &t3699_0_0_0, &t3699_1_0_0, t3699_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1515 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3700_TI;



// Metadata Definition System.Runtime.Serialization.OnSerializedAttribute[]
static MethodInfo* t3700_MIs[] =
{
	NULL
};
extern MethodInfo m25920_MI;
extern MethodInfo m25921_MI;
extern MethodInfo m25922_MI;
extern MethodInfo m25923_MI;
extern MethodInfo m25924_MI;
extern MethodInfo m25925_MI;
extern MethodInfo m25919_MI;
extern MethodInfo m25927_MI;
extern MethodInfo m25928_MI;
static MethodInfo* t3700_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25920_MI,
	&m5907_MI,
	&m25921_MI,
	&m25922_MI,
	&m25923_MI,
	&m25924_MI,
	&m25925_MI,
	&m5908_MI,
	&m25919_MI,
	&m25927_MI,
	&m25928_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6543_TI;
extern TypeInfo t6544_TI;
extern TypeInfo t6545_TI;
static TypeInfo* t3700_ITIs[] = 
{
	&t6543_TI,
	&t6544_TI,
	&t6545_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3700_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6543_TI, 21},
	{ &t6544_TI, 28},
	{ &t6545_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3700_0_0_0;
extern Il2CppType t3700_1_0_0;
struct t1516;
extern TypeInfo t1516_TI;
extern CustomAttributesCache t1516__CustomAttributeCache;
TypeInfo t3700_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OnSerializedAttribute[]", "System.Runtime.Serialization", t3700_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1516_TI, t3700_ITIs, t3700_VT, &EmptyCustomAttributesCache, &t3700_TI, &t3700_0_0_0, &t3700_1_0_0, t3700_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1516 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3701_TI;



// Metadata Definition System.Runtime.Serialization.OnSerializingAttribute[]
static MethodInfo* t3701_MIs[] =
{
	NULL
};
extern MethodInfo m25931_MI;
extern MethodInfo m25932_MI;
extern MethodInfo m25933_MI;
extern MethodInfo m25934_MI;
extern MethodInfo m25935_MI;
extern MethodInfo m25936_MI;
extern MethodInfo m25930_MI;
extern MethodInfo m25938_MI;
extern MethodInfo m25939_MI;
static MethodInfo* t3701_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25931_MI,
	&m5907_MI,
	&m25932_MI,
	&m25933_MI,
	&m25934_MI,
	&m25935_MI,
	&m25936_MI,
	&m5908_MI,
	&m25930_MI,
	&m25938_MI,
	&m25939_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6546_TI;
extern TypeInfo t6547_TI;
extern TypeInfo t6548_TI;
static TypeInfo* t3701_ITIs[] = 
{
	&t6546_TI,
	&t6547_TI,
	&t6548_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3701_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6546_TI, 21},
	{ &t6547_TI, 28},
	{ &t6548_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3701_0_0_0;
extern Il2CppType t3701_1_0_0;
struct t1517;
extern TypeInfo t1517_TI;
extern CustomAttributesCache t1517__CustomAttributeCache;
TypeInfo t3701_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OnSerializingAttribute[]", "System.Runtime.Serialization", t3701_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1517_TI, t3701_ITIs, t3701_VT, &EmptyCustomAttributesCache, &t3701_TI, &t3701_0_0_0, &t3701_1_0_0, t3701_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1517 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3702_TI;



// Metadata Definition System.Runtime.Serialization.StreamingContextStates[]
static MethodInfo* t3702_MIs[] =
{
	NULL
};
extern MethodInfo m25942_MI;
extern MethodInfo m25943_MI;
extern MethodInfo m25944_MI;
extern MethodInfo m25945_MI;
extern MethodInfo m25946_MI;
extern MethodInfo m25947_MI;
extern MethodInfo m25941_MI;
extern MethodInfo m25949_MI;
extern MethodInfo m25950_MI;
static MethodInfo* t3702_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25942_MI,
	&m5907_MI,
	&m25943_MI,
	&m25944_MI,
	&m25945_MI,
	&m25946_MI,
	&m25947_MI,
	&m5908_MI,
	&m25941_MI,
	&m25949_MI,
	&m25950_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6549_TI;
extern TypeInfo t6550_TI;
extern TypeInfo t6551_TI;
static TypeInfo* t3702_ITIs[] = 
{
	&t6549_TI,
	&t6550_TI,
	&t6551_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3702_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6549_TI, 21},
	{ &t6550_TI, 28},
	{ &t6551_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3702_0_0_0;
extern Il2CppType t3702_1_0_0;
#include "t1523.h"
extern TypeInfo t1523_TI;
extern CustomAttributesCache t1523__CustomAttributeCache;
TypeInfo t3702_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StreamingContextStates[]", "System.Runtime.Serialization", t3702_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1523_TI, t3702_ITIs, t3702_VT, &EmptyCustomAttributesCache, &t44_TI, &t3702_0_0_0, &t3702_1_0_0, t3702_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3703_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyStorageFlags[]
static MethodInfo* t3703_MIs[] =
{
	NULL
};
extern MethodInfo m25953_MI;
extern MethodInfo m25954_MI;
extern MethodInfo m25955_MI;
extern MethodInfo m25956_MI;
extern MethodInfo m25957_MI;
extern MethodInfo m25958_MI;
extern MethodInfo m25952_MI;
extern MethodInfo m25960_MI;
extern MethodInfo m25961_MI;
static MethodInfo* t3703_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25953_MI,
	&m5907_MI,
	&m25954_MI,
	&m25955_MI,
	&m25956_MI,
	&m25957_MI,
	&m25958_MI,
	&m5908_MI,
	&m25952_MI,
	&m25960_MI,
	&m25961_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6552_TI;
extern TypeInfo t6553_TI;
extern TypeInfo t6554_TI;
static TypeInfo* t3703_ITIs[] = 
{
	&t6552_TI,
	&t6553_TI,
	&t6554_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3703_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6552_TI, 21},
	{ &t6553_TI, 28},
	{ &t6554_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3703_0_0_0;
extern Il2CppType t3703_1_0_0;
#include "t795.h"
extern TypeInfo t795_TI;
extern CustomAttributesCache t795__CustomAttributeCache;
TypeInfo t3703_TI = 
{
	&g_mscorlib_dll_Image, NULL, "X509KeyStorageFlags[]", "System.Security.Cryptography.X509Certificates", t3703_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t795_TI, t3703_ITIs, t3703_VT, &EmptyCustomAttributesCache, &t44_TI, &t3703_0_0_0, &t3703_1_0_0, t3703_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3704_TI;



// Metadata Definition System.Security.Cryptography.CipherMode[]
static MethodInfo* t3704_MIs[] =
{
	NULL
};
extern MethodInfo m25964_MI;
extern MethodInfo m25965_MI;
extern MethodInfo m25966_MI;
extern MethodInfo m25967_MI;
extern MethodInfo m25968_MI;
extern MethodInfo m25969_MI;
extern MethodInfo m25963_MI;
extern MethodInfo m25971_MI;
extern MethodInfo m25972_MI;
static MethodInfo* t3704_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25964_MI,
	&m5907_MI,
	&m25965_MI,
	&m25966_MI,
	&m25967_MI,
	&m25968_MI,
	&m25969_MI,
	&m5908_MI,
	&m25963_MI,
	&m25971_MI,
	&m25972_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6555_TI;
extern TypeInfo t6556_TI;
extern TypeInfo t6557_TI;
static TypeInfo* t3704_ITIs[] = 
{
	&t6555_TI,
	&t6556_TI,
	&t6557_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3704_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6555_TI, 21},
	{ &t6556_TI, 28},
	{ &t6557_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3704_0_0_0;
extern Il2CppType t3704_1_0_0;
#include "t1023.h"
extern TypeInfo t1023_TI;
extern CustomAttributesCache t1023__CustomAttributeCache;
TypeInfo t3704_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CipherMode[]", "System.Security.Cryptography", t3704_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1023_TI, t3704_ITIs, t3704_VT, &EmptyCustomAttributesCache, &t44_TI, &t3704_0_0_0, &t3704_1_0_0, t3704_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3705_TI;



// Metadata Definition System.Security.Cryptography.CspProviderFlags[]
static MethodInfo* t3705_MIs[] =
{
	NULL
};
extern MethodInfo m25975_MI;
extern MethodInfo m25976_MI;
extern MethodInfo m25977_MI;
extern MethodInfo m25978_MI;
extern MethodInfo m25979_MI;
extern MethodInfo m25980_MI;
extern MethodInfo m25974_MI;
extern MethodInfo m25982_MI;
extern MethodInfo m25983_MI;
static MethodInfo* t3705_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25975_MI,
	&m5907_MI,
	&m25976_MI,
	&m25977_MI,
	&m25978_MI,
	&m25979_MI,
	&m25980_MI,
	&m5908_MI,
	&m25974_MI,
	&m25982_MI,
	&m25983_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6558_TI;
extern TypeInfo t6559_TI;
extern TypeInfo t6560_TI;
static TypeInfo* t3705_ITIs[] = 
{
	&t6558_TI,
	&t6559_TI,
	&t6560_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3705_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6558_TI, 21},
	{ &t6559_TI, 28},
	{ &t6560_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3705_0_0_0;
extern Il2CppType t3705_1_0_0;
#include "t1099.h"
extern TypeInfo t1099_TI;
extern CustomAttributesCache t1099__CustomAttributeCache;
TypeInfo t3705_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CspProviderFlags[]", "System.Security.Cryptography", t3705_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1099_TI, t3705_ITIs, t3705_VT, &EmptyCustomAttributesCache, &t44_TI, &t3705_0_0_0, &t3705_1_0_0, t3705_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1526_TI;



// Metadata Definition System.Byte[,]
static MethodInfo* t1526_MIs[] =
{
	NULL
};
extern MethodInfo m23376_MI;
extern MethodInfo m23377_MI;
extern MethodInfo m23378_MI;
extern MethodInfo m23379_MI;
extern MethodInfo m23380_MI;
extern MethodInfo m23381_MI;
extern MethodInfo m23375_MI;
extern MethodInfo m23383_MI;
extern MethodInfo m23384_MI;
extern MethodInfo m23387_MI;
extern MethodInfo m23388_MI;
extern MethodInfo m23389_MI;
extern MethodInfo m23390_MI;
extern MethodInfo m23391_MI;
extern MethodInfo m23392_MI;
extern MethodInfo m23386_MI;
extern MethodInfo m23394_MI;
extern MethodInfo m23395_MI;
extern MethodInfo m23398_MI;
extern MethodInfo m23399_MI;
extern MethodInfo m23400_MI;
extern MethodInfo m23401_MI;
extern MethodInfo m23402_MI;
extern MethodInfo m23403_MI;
extern MethodInfo m23397_MI;
extern MethodInfo m23405_MI;
extern MethodInfo m23406_MI;
static MethodInfo* t1526_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23376_MI,
	&m5907_MI,
	&m23377_MI,
	&m23378_MI,
	&m23379_MI,
	&m23380_MI,
	&m23381_MI,
	&m5908_MI,
	&m23375_MI,
	&m23383_MI,
	&m23384_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m23387_MI,
	&m5907_MI,
	&m23388_MI,
	&m23389_MI,
	&m23390_MI,
	&m23391_MI,
	&m23392_MI,
	&m5908_MI,
	&m23386_MI,
	&m23394_MI,
	&m23395_MI,
	&m5905_MI,
	&m5906_MI,
	&m23398_MI,
	&m5907_MI,
	&m23399_MI,
	&m23400_MI,
	&m23401_MI,
	&m23402_MI,
	&m23403_MI,
	&m5908_MI,
	&m23397_MI,
	&m23405_MI,
	&m23406_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5862_TI;
extern TypeInfo t5863_TI;
extern TypeInfo t5864_TI;
extern TypeInfo t5865_TI;
extern TypeInfo t5866_TI;
extern TypeInfo t5867_TI;
extern TypeInfo t5868_TI;
extern TypeInfo t5869_TI;
extern TypeInfo t5870_TI;
static TypeInfo* t1526_ITIs[] = 
{
	&t5862_TI,
	&t5863_TI,
	&t5864_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5865_TI,
	&t5866_TI,
	&t5867_TI,
	&t5868_TI,
	&t5869_TI,
	&t5870_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1526_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5862_TI, 21},
	{ &t5863_TI, 28},
	{ &t5864_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5865_TI, 73},
	{ &t5866_TI, 80},
	{ &t5867_TI, 85},
	{ &t5868_TI, 86},
	{ &t5869_TI, 93},
	{ &t5870_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1526_0_0_0;
extern Il2CppType t1526_1_0_0;
#include "t348.h"
extern CustomAttributesCache t348__CustomAttributeCache;
TypeInfo t1526_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Byte[,]", "System", t1526_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t348_TI, t1526_ITIs, t1526_VT, &EmptyCustomAttributesCache, &t1526_TI, &t1526_0_0_0, &t1526_1_0_0, t1526_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 1057033, 2, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3706_TI;



// Metadata Definition System.Security.Cryptography.PaddingMode[]
static MethodInfo* t3706_MIs[] =
{
	NULL
};
extern MethodInfo m25986_MI;
extern MethodInfo m25987_MI;
extern MethodInfo m25988_MI;
extern MethodInfo m25989_MI;
extern MethodInfo m25990_MI;
extern MethodInfo m25991_MI;
extern MethodInfo m25985_MI;
extern MethodInfo m25993_MI;
extern MethodInfo m25994_MI;
static MethodInfo* t3706_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25986_MI,
	&m5907_MI,
	&m25987_MI,
	&m25988_MI,
	&m25989_MI,
	&m25990_MI,
	&m25991_MI,
	&m5908_MI,
	&m25985_MI,
	&m25993_MI,
	&m25994_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6561_TI;
extern TypeInfo t6562_TI;
extern TypeInfo t6563_TI;
static TypeInfo* t3706_ITIs[] = 
{
	&t6561_TI,
	&t6562_TI,
	&t6563_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3706_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6561_TI, 21},
	{ &t6562_TI, 28},
	{ &t6563_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3706_0_0_0;
extern Il2CppType t3706_1_0_0;
#include "t1117.h"
extern TypeInfo t1117_TI;
extern CustomAttributesCache t1117__CustomAttributeCache;
TypeInfo t3706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PaddingMode[]", "System.Security.Cryptography", t3706_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1117_TI, t3706_ITIs, t3706_VT, &EmptyCustomAttributesCache, &t44_TI, &t3706_0_0_0, &t3706_1_0_0, t3706_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3462_TI;



// Metadata Definition System.Security.Policy.StrongName[]
static MethodInfo* t3462_MIs[] =
{
	NULL
};
extern MethodInfo m25997_MI;
extern MethodInfo m25998_MI;
extern MethodInfo m25999_MI;
extern MethodInfo m26000_MI;
extern MethodInfo m26001_MI;
extern MethodInfo m26002_MI;
extern MethodInfo m25996_MI;
extern MethodInfo m26004_MI;
extern MethodInfo m26005_MI;
extern MethodInfo m26008_MI;
extern MethodInfo m26009_MI;
extern MethodInfo m26010_MI;
extern MethodInfo m26011_MI;
extern MethodInfo m26012_MI;
extern MethodInfo m26013_MI;
extern MethodInfo m26007_MI;
extern MethodInfo m26015_MI;
extern MethodInfo m26016_MI;
extern MethodInfo m26019_MI;
extern MethodInfo m26020_MI;
extern MethodInfo m26021_MI;
extern MethodInfo m26022_MI;
extern MethodInfo m26023_MI;
extern MethodInfo m26024_MI;
extern MethodInfo m26018_MI;
extern MethodInfo m26026_MI;
extern MethodInfo m26027_MI;
static MethodInfo* t3462_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m25997_MI,
	&m5907_MI,
	&m25998_MI,
	&m25999_MI,
	&m26000_MI,
	&m26001_MI,
	&m26002_MI,
	&m5908_MI,
	&m25996_MI,
	&m26004_MI,
	&m26005_MI,
	&m5905_MI,
	&m5906_MI,
	&m26008_MI,
	&m5907_MI,
	&m26009_MI,
	&m26010_MI,
	&m26011_MI,
	&m26012_MI,
	&m26013_MI,
	&m5908_MI,
	&m26007_MI,
	&m26015_MI,
	&m26016_MI,
	&m5905_MI,
	&m5906_MI,
	&m26019_MI,
	&m5907_MI,
	&m26020_MI,
	&m26021_MI,
	&m26022_MI,
	&m26023_MI,
	&m26024_MI,
	&m5908_MI,
	&m26018_MI,
	&m26026_MI,
	&m26027_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t3464_TI;
extern TypeInfo t1564_TI;
extern TypeInfo t3465_TI;
extern TypeInfo t6564_TI;
extern TypeInfo t6565_TI;
extern TypeInfo t6566_TI;
extern TypeInfo t6567_TI;
extern TypeInfo t6568_TI;
extern TypeInfo t6569_TI;
static TypeInfo* t3462_ITIs[] = 
{
	&t3464_TI,
	&t1564_TI,
	&t3465_TI,
	&t6564_TI,
	&t6565_TI,
	&t6566_TI,
	&t6567_TI,
	&t6568_TI,
	&t6569_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3462_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3464_TI, 21},
	{ &t1564_TI, 28},
	{ &t3465_TI, 33},
	{ &t6564_TI, 34},
	{ &t6565_TI, 41},
	{ &t6566_TI, 46},
	{ &t6567_TI, 47},
	{ &t6568_TI, 54},
	{ &t6569_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t3462_1_0_0;
struct t1567;
extern TypeInfo t1567_TI;
extern CustomAttributesCache t1567__CustomAttributeCache;
TypeInfo t3462_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StrongName[]", "System.Security.Policy", t3462_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1567_TI, t3462_ITIs, t3462_VT, &EmptyCustomAttributesCache, &t3462_TI, &t3462_0_0_0, &t3462_1_0_0, t3462_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1567 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3707_TI;



// Metadata Definition System.Security.Policy.IBuiltInEvidence[]
static MethodInfo* t3707_MIs[] =
{
	NULL
};
static MethodInfo* t3707_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26008_MI,
	&m5907_MI,
	&m26009_MI,
	&m26010_MI,
	&m26011_MI,
	&m26012_MI,
	&m26013_MI,
	&m5908_MI,
	&m26007_MI,
	&m26015_MI,
	&m26016_MI,
};
static TypeInfo* t3707_ITIs[] = 
{
	&t6564_TI,
	&t6565_TI,
	&t6566_TI,
};
static Il2CppInterfaceOffsetPair t3707_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6564_TI, 21},
	{ &t6565_TI, 28},
	{ &t6566_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3707_0_0_0;
extern Il2CppType t3707_1_0_0;
struct t2063;
extern TypeInfo t2063_TI;
TypeInfo t3707_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IBuiltInEvidence[]", "System.Security.Policy", t3707_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2063_TI, t3707_ITIs, t3707_VT, &EmptyCustomAttributesCache, &t3707_TI, &t3707_0_0_0, &t3707_1_0_0, t3707_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 160, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3708_TI;



// Metadata Definition System.Security.Policy.IIdentityPermissionFactory[]
static MethodInfo* t3708_MIs[] =
{
	NULL
};
static MethodInfo* t3708_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26019_MI,
	&m5907_MI,
	&m26020_MI,
	&m26021_MI,
	&m26022_MI,
	&m26023_MI,
	&m26024_MI,
	&m5908_MI,
	&m26018_MI,
	&m26026_MI,
	&m26027_MI,
};
static TypeInfo* t3708_ITIs[] = 
{
	&t6567_TI,
	&t6568_TI,
	&t6569_TI,
};
static Il2CppInterfaceOffsetPair t3708_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6567_TI, 21},
	{ &t6568_TI, 28},
	{ &t6569_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3708_0_0_0;
extern Il2CppType t3708_1_0_0;
struct t2064;
extern TypeInfo t2064_TI;
extern CustomAttributesCache t2064__CustomAttributeCache;
TypeInfo t3708_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IIdentityPermissionFactory[]", "System.Security.Policy", t3708_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2064_TI, t3708_ITIs, t3708_VT, &EmptyCustomAttributesCache, &t3708_TI, &t3708_0_0_0, &t3708_1_0_0, t3708_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3709_TI;



// Metadata Definition System.Security.Principal.PrincipalPolicy[]
static MethodInfo* t3709_MIs[] =
{
	NULL
};
extern MethodInfo m26045_MI;
extern MethodInfo m26046_MI;
extern MethodInfo m26047_MI;
extern MethodInfo m26048_MI;
extern MethodInfo m26049_MI;
extern MethodInfo m26050_MI;
extern MethodInfo m26044_MI;
extern MethodInfo m26052_MI;
extern MethodInfo m26053_MI;
static MethodInfo* t3709_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26045_MI,
	&m5907_MI,
	&m26046_MI,
	&m26047_MI,
	&m26048_MI,
	&m26049_MI,
	&m26050_MI,
	&m5908_MI,
	&m26044_MI,
	&m26052_MI,
	&m26053_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6570_TI;
extern TypeInfo t6571_TI;
extern TypeInfo t6572_TI;
static TypeInfo* t3709_ITIs[] = 
{
	&t6570_TI,
	&t6571_TI,
	&t6572_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3709_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6570_TI, 21},
	{ &t6571_TI, 28},
	{ &t6572_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3709_0_0_0;
extern Il2CppType t3709_1_0_0;
#include "t1568.h"
extern TypeInfo t1568_TI;
extern CustomAttributesCache t1568__CustomAttributeCache;
TypeInfo t3709_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PrincipalPolicy[]", "System.Security.Principal", t3709_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1568_TI, t3709_ITIs, t3709_VT, &EmptyCustomAttributesCache, &t44_TI, &t3709_0_0_0, &t3709_1_0_0, t3709_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3710_TI;



// Metadata Definition System.Security.SecuritySafeCriticalAttribute[]
static MethodInfo* t3710_MIs[] =
{
	NULL
};
extern MethodInfo m26056_MI;
extern MethodInfo m26057_MI;
extern MethodInfo m26058_MI;
extern MethodInfo m26059_MI;
extern MethodInfo m26060_MI;
extern MethodInfo m26061_MI;
extern MethodInfo m26055_MI;
extern MethodInfo m26063_MI;
extern MethodInfo m26064_MI;
static MethodInfo* t3710_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26056_MI,
	&m5907_MI,
	&m26057_MI,
	&m26058_MI,
	&m26059_MI,
	&m26060_MI,
	&m26061_MI,
	&m5908_MI,
	&m26055_MI,
	&m26063_MI,
	&m26064_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6573_TI;
extern TypeInfo t6574_TI;
extern TypeInfo t6575_TI;
static TypeInfo* t3710_ITIs[] = 
{
	&t6573_TI,
	&t6574_TI,
	&t6575_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3710_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6573_TI, 21},
	{ &t6574_TI, 28},
	{ &t6575_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3710_0_0_0;
extern Il2CppType t3710_1_0_0;
struct t615;
extern TypeInfo t615_TI;
extern CustomAttributesCache t615__CustomAttributeCache;
TypeInfo t3710_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SecuritySafeCriticalAttribute[]", "System.Security", t3710_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t615_TI, t3710_ITIs, t3710_VT, &EmptyCustomAttributesCache, &t3710_TI, &t3710_0_0_0, &t3710_1_0_0, t3710_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t615 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3711_TI;



// Metadata Definition System.Security.SuppressUnmanagedCodeSecurityAttribute[]
static MethodInfo* t3711_MIs[] =
{
	NULL
};
extern MethodInfo m26067_MI;
extern MethodInfo m26068_MI;
extern MethodInfo m26069_MI;
extern MethodInfo m26070_MI;
extern MethodInfo m26071_MI;
extern MethodInfo m26072_MI;
extern MethodInfo m26066_MI;
extern MethodInfo m26074_MI;
extern MethodInfo m26075_MI;
static MethodInfo* t3711_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26067_MI,
	&m5907_MI,
	&m26068_MI,
	&m26069_MI,
	&m26070_MI,
	&m26071_MI,
	&m26072_MI,
	&m5908_MI,
	&m26066_MI,
	&m26074_MI,
	&m26075_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6576_TI;
extern TypeInfo t6577_TI;
extern TypeInfo t6578_TI;
static TypeInfo* t3711_ITIs[] = 
{
	&t6576_TI,
	&t6577_TI,
	&t6578_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3711_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6576_TI, 21},
	{ &t6577_TI, 28},
	{ &t6578_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3711_0_0_0;
extern Il2CppType t3711_1_0_0;
struct t1572;
extern TypeInfo t1572_TI;
extern CustomAttributesCache t1572__CustomAttributeCache;
TypeInfo t3711_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SuppressUnmanagedCodeSecurityAttribute[]", "System.Security", t3711_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1572_TI, t3711_ITIs, t3711_VT, &EmptyCustomAttributesCache, &t3711_TI, &t3711_0_0_0, &t3711_1_0_0, t3711_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1572 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3712_TI;



// Metadata Definition System.Security.UnverifiableCodeAttribute[]
static MethodInfo* t3712_MIs[] =
{
	NULL
};
extern MethodInfo m26078_MI;
extern MethodInfo m26079_MI;
extern MethodInfo m26080_MI;
extern MethodInfo m26081_MI;
extern MethodInfo m26082_MI;
extern MethodInfo m26083_MI;
extern MethodInfo m26077_MI;
extern MethodInfo m26085_MI;
extern MethodInfo m26086_MI;
static MethodInfo* t3712_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26078_MI,
	&m5907_MI,
	&m26079_MI,
	&m26080_MI,
	&m26081_MI,
	&m26082_MI,
	&m26083_MI,
	&m5908_MI,
	&m26077_MI,
	&m26085_MI,
	&m26086_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6579_TI;
extern TypeInfo t6580_TI;
extern TypeInfo t6581_TI;
static TypeInfo* t3712_ITIs[] = 
{
	&t6579_TI,
	&t6580_TI,
	&t6581_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3712_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6579_TI, 21},
	{ &t6580_TI, 28},
	{ &t6581_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3712_0_0_0;
extern Il2CppType t3712_1_0_0;
struct t1573;
extern TypeInfo t1573_TI;
extern CustomAttributesCache t1573__CustomAttributeCache;
TypeInfo t3712_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnverifiableCodeAttribute[]", "System.Security", t3712_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1573_TI, t3712_ITIs, t3712_VT, &EmptyCustomAttributesCache, &t3712_TI, &t3712_0_0_0, &t3712_1_0_0, t3712_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1573 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3713_TI;



// Metadata Definition System.Threading.EventResetMode[]
static MethodInfo* t3713_MIs[] =
{
	NULL
};
extern MethodInfo m26089_MI;
extern MethodInfo m26090_MI;
extern MethodInfo m26091_MI;
extern MethodInfo m26092_MI;
extern MethodInfo m26093_MI;
extern MethodInfo m26094_MI;
extern MethodInfo m26088_MI;
extern MethodInfo m26096_MI;
extern MethodInfo m26097_MI;
static MethodInfo* t3713_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26089_MI,
	&m5907_MI,
	&m26090_MI,
	&m26091_MI,
	&m26092_MI,
	&m26093_MI,
	&m26094_MI,
	&m5908_MI,
	&m26088_MI,
	&m26096_MI,
	&m26097_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6582_TI;
extern TypeInfo t6583_TI;
extern TypeInfo t6584_TI;
static TypeInfo* t3713_ITIs[] = 
{
	&t6582_TI,
	&t6583_TI,
	&t6584_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3713_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6582_TI, 21},
	{ &t6583_TI, 28},
	{ &t6584_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3713_0_0_0;
extern Il2CppType t3713_1_0_0;
#include "t1600.h"
extern TypeInfo t1600_TI;
extern CustomAttributesCache t1600__CustomAttributeCache;
TypeInfo t3713_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EventResetMode[]", "System.Threading", t3713_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1600_TI, t3713_ITIs, t3713_VT, &EmptyCustomAttributesCache, &t44_TI, &t3713_0_0_0, &t3713_1_0_0, t3713_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3714_TI;



// Metadata Definition System.Threading.ThreadState[]
static MethodInfo* t3714_MIs[] =
{
	NULL
};
extern MethodInfo m26100_MI;
extern MethodInfo m26101_MI;
extern MethodInfo m26102_MI;
extern MethodInfo m26103_MI;
extern MethodInfo m26104_MI;
extern MethodInfo m26105_MI;
extern MethodInfo m26099_MI;
extern MethodInfo m26107_MI;
extern MethodInfo m26108_MI;
static MethodInfo* t3714_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26100_MI,
	&m5907_MI,
	&m26101_MI,
	&m26102_MI,
	&m26103_MI,
	&m26104_MI,
	&m26105_MI,
	&m5908_MI,
	&m26099_MI,
	&m26107_MI,
	&m26108_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6585_TI;
extern TypeInfo t6586_TI;
extern TypeInfo t6587_TI;
static TypeInfo* t3714_ITIs[] = 
{
	&t6585_TI,
	&t6586_TI,
	&t6587_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3714_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6585_TI, 21},
	{ &t6586_TI, 28},
	{ &t6587_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3714_0_0_0;
extern Il2CppType t3714_1_0_0;
#include "t1605.h"
extern TypeInfo t1605_TI;
extern CustomAttributesCache t1605__CustomAttributeCache;
TypeInfo t3714_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ThreadState[]", "System.Threading", t3714_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1605_TI, t3714_ITIs, t3714_VT, &EmptyCustomAttributesCache, &t44_TI, &t3714_0_0_0, &t3714_1_0_0, t3714_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3715_TI;



// Metadata Definition System.AttributeTargets[]
static MethodInfo* t3715_MIs[] =
{
	NULL
};
extern MethodInfo m26111_MI;
extern MethodInfo m26112_MI;
extern MethodInfo m26113_MI;
extern MethodInfo m26114_MI;
extern MethodInfo m26115_MI;
extern MethodInfo m26116_MI;
extern MethodInfo m26110_MI;
extern MethodInfo m26118_MI;
extern MethodInfo m26119_MI;
static MethodInfo* t3715_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26111_MI,
	&m5907_MI,
	&m26112_MI,
	&m26113_MI,
	&m26114_MI,
	&m26115_MI,
	&m26116_MI,
	&m5908_MI,
	&m26110_MI,
	&m26118_MI,
	&m26119_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6588_TI;
extern TypeInfo t6589_TI;
extern TypeInfo t6590_TI;
static TypeInfo* t3715_ITIs[] = 
{
	&t6588_TI,
	&t6589_TI,
	&t6590_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3715_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6588_TI, 21},
	{ &t6589_TI, 28},
	{ &t6590_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3715_0_0_0;
extern Il2CppType t3715_1_0_0;
#include "t1129.h"
extern TypeInfo t1129_TI;
extern CustomAttributesCache t1129__CustomAttributeCache;
TypeInfo t3715_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AttributeTargets[]", "System", t3715_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1129_TI, t3715_ITIs, t3715_VT, &EmptyCustomAttributesCache, &t44_TI, &t3715_0_0_0, &t3715_1_0_0, t3715_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3716_TI;



// Metadata Definition System.DateTime/Which[]
static MethodInfo* t3716_MIs[] =
{
	NULL
};
extern MethodInfo m26122_MI;
extern MethodInfo m26123_MI;
extern MethodInfo m26124_MI;
extern MethodInfo m26125_MI;
extern MethodInfo m26126_MI;
extern MethodInfo m26127_MI;
extern MethodInfo m26121_MI;
extern MethodInfo m26129_MI;
extern MethodInfo m26130_MI;
static MethodInfo* t3716_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26122_MI,
	&m5907_MI,
	&m26123_MI,
	&m26124_MI,
	&m26125_MI,
	&m26126_MI,
	&m26127_MI,
	&m5908_MI,
	&m26121_MI,
	&m26129_MI,
	&m26130_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6591_TI;
extern TypeInfo t6592_TI;
extern TypeInfo t6593_TI;
static TypeInfo* t3716_ITIs[] = 
{
	&t6591_TI,
	&t6592_TI,
	&t6593_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3716_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6591_TI, 21},
	{ &t6592_TI, 28},
	{ &t6593_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3716_0_0_0;
extern Il2CppType t3716_1_0_0;
#include "t1627.h"
extern TypeInfo t1627_TI;
TypeInfo t3716_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Which[]", "", t3716_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1627_TI, t3716_ITIs, t3716_VT, &EmptyCustomAttributesCache, &t44_TI, &t3716_0_0_0, &t3716_1_0_0, t3716_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3717_TI;



// Metadata Definition System.DateTimeKind[]
static MethodInfo* t3717_MIs[] =
{
	NULL
};
extern MethodInfo m26133_MI;
extern MethodInfo m26134_MI;
extern MethodInfo m26135_MI;
extern MethodInfo m26136_MI;
extern MethodInfo m26137_MI;
extern MethodInfo m26138_MI;
extern MethodInfo m26132_MI;
extern MethodInfo m26140_MI;
extern MethodInfo m26141_MI;
static MethodInfo* t3717_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26133_MI,
	&m5907_MI,
	&m26134_MI,
	&m26135_MI,
	&m26136_MI,
	&m26137_MI,
	&m26138_MI,
	&m5908_MI,
	&m26132_MI,
	&m26140_MI,
	&m26141_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6594_TI;
extern TypeInfo t6595_TI;
extern TypeInfo t6596_TI;
static TypeInfo* t3717_ITIs[] = 
{
	&t6594_TI,
	&t6595_TI,
	&t6596_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3717_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6594_TI, 21},
	{ &t6595_TI, 28},
	{ &t6596_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3717_0_0_0;
extern Il2CppType t3717_1_0_0;
#include "t1628.h"
extern TypeInfo t1628_TI;
extern CustomAttributesCache t1628__CustomAttributeCache;
TypeInfo t3717_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DateTimeKind[]", "System", t3717_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1628_TI, t3717_ITIs, t3717_VT, &EmptyCustomAttributesCache, &t44_TI, &t3717_0_0_0, &t3717_1_0_0, t3717_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3718_TI;



// Metadata Definition System.DayOfWeek[]
static MethodInfo* t3718_MIs[] =
{
	NULL
};
extern MethodInfo m26144_MI;
extern MethodInfo m26145_MI;
extern MethodInfo m26146_MI;
extern MethodInfo m26147_MI;
extern MethodInfo m26148_MI;
extern MethodInfo m26149_MI;
extern MethodInfo m26143_MI;
extern MethodInfo m26151_MI;
extern MethodInfo m26152_MI;
static MethodInfo* t3718_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26144_MI,
	&m5907_MI,
	&m26145_MI,
	&m26146_MI,
	&m26147_MI,
	&m26148_MI,
	&m26149_MI,
	&m5908_MI,
	&m26143_MI,
	&m26151_MI,
	&m26152_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6597_TI;
extern TypeInfo t6598_TI;
extern TypeInfo t6599_TI;
static TypeInfo* t3718_ITIs[] = 
{
	&t6597_TI,
	&t6598_TI,
	&t6599_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3718_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6597_TI, 21},
	{ &t6598_TI, 28},
	{ &t6599_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3718_0_0_0;
extern Il2CppType t3718_1_0_0;
#include "t1292.h"
extern TypeInfo t1292_TI;
extern CustomAttributesCache t1292__CustomAttributeCache;
TypeInfo t3718_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DayOfWeek[]", "System", t3718_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1292_TI, t3718_ITIs, t3718_VT, &EmptyCustomAttributesCache, &t44_TI, &t3718_0_0_0, &t3718_1_0_0, t3718_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3719_TI;



// Metadata Definition System.Environment/SpecialFolder[]
static MethodInfo* t3719_MIs[] =
{
	NULL
};
extern MethodInfo m26155_MI;
extern MethodInfo m26156_MI;
extern MethodInfo m26157_MI;
extern MethodInfo m26158_MI;
extern MethodInfo m26159_MI;
extern MethodInfo m26160_MI;
extern MethodInfo m26154_MI;
extern MethodInfo m26162_MI;
extern MethodInfo m26163_MI;
static MethodInfo* t3719_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26155_MI,
	&m5907_MI,
	&m26156_MI,
	&m26157_MI,
	&m26158_MI,
	&m26159_MI,
	&m26160_MI,
	&m5908_MI,
	&m26154_MI,
	&m26162_MI,
	&m26163_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6600_TI;
extern TypeInfo t6601_TI;
extern TypeInfo t6602_TI;
static TypeInfo* t3719_ITIs[] = 
{
	&t6600_TI,
	&t6601_TI,
	&t6602_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3719_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6600_TI, 21},
	{ &t6601_TI, 28},
	{ &t6602_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3719_0_0_0;
extern Il2CppType t3719_1_0_0;
#include "t1112.h"
extern TypeInfo t1112_TI;
extern CustomAttributesCache t1112__CustomAttributeCache;
TypeInfo t3719_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SpecialFolder[]", "", t3719_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1112_TI, t3719_ITIs, t3719_VT, &EmptyCustomAttributesCache, &t44_TI, &t3719_0_0_0, &t3719_1_0_0, t3719_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3720_TI;



// Metadata Definition System.LoaderOptimization[]
static MethodInfo* t3720_MIs[] =
{
	NULL
};
extern MethodInfo m26166_MI;
extern MethodInfo m26167_MI;
extern MethodInfo m26168_MI;
extern MethodInfo m26169_MI;
extern MethodInfo m26170_MI;
extern MethodInfo m26171_MI;
extern MethodInfo m26165_MI;
extern MethodInfo m26173_MI;
extern MethodInfo m26174_MI;
static MethodInfo* t3720_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26166_MI,
	&m5907_MI,
	&m26167_MI,
	&m26168_MI,
	&m26169_MI,
	&m26170_MI,
	&m26171_MI,
	&m5908_MI,
	&m26165_MI,
	&m26173_MI,
	&m26174_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6603_TI;
extern TypeInfo t6604_TI;
extern TypeInfo t6605_TI;
static TypeInfo* t3720_ITIs[] = 
{
	&t6603_TI,
	&t6604_TI,
	&t6605_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3720_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6603_TI, 21},
	{ &t6604_TI, 28},
	{ &t6605_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3720_0_0_0;
extern Il2CppType t3720_1_0_0;
#include "t1620.h"
extern TypeInfo t1620_TI;
extern CustomAttributesCache t1620__CustomAttributeCache;
extern CustomAttributesCache t1620__CustomAttributeCache_DomainMask;
extern CustomAttributesCache t1620__CustomAttributeCache_DisallowBindings;
TypeInfo t3720_TI = 
{
	&g_mscorlib_dll_Image, NULL, "LoaderOptimization[]", "System", t3720_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1620_TI, t3720_ITIs, t3720_VT, &EmptyCustomAttributesCache, &t44_TI, &t3720_0_0_0, &t3720_1_0_0, t3720_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3721_TI;



// Metadata Definition System.NonSerializedAttribute[]
static MethodInfo* t3721_MIs[] =
{
	NULL
};
extern MethodInfo m26177_MI;
extern MethodInfo m26178_MI;
extern MethodInfo m26179_MI;
extern MethodInfo m26180_MI;
extern MethodInfo m26181_MI;
extern MethodInfo m26182_MI;
extern MethodInfo m26176_MI;
extern MethodInfo m26184_MI;
extern MethodInfo m26185_MI;
static MethodInfo* t3721_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26177_MI,
	&m5907_MI,
	&m26178_MI,
	&m26179_MI,
	&m26180_MI,
	&m26181_MI,
	&m26182_MI,
	&m5908_MI,
	&m26176_MI,
	&m26184_MI,
	&m26185_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6606_TI;
extern TypeInfo t6607_TI;
extern TypeInfo t6608_TI;
static TypeInfo* t3721_ITIs[] = 
{
	&t6606_TI,
	&t6607_TI,
	&t6608_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3721_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6606_TI, 21},
	{ &t6607_TI, 28},
	{ &t6608_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3721_0_0_0;
extern Il2CppType t3721_1_0_0;
struct t1662;
extern TypeInfo t1662_TI;
extern CustomAttributesCache t1662__CustomAttributeCache;
TypeInfo t3721_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NonSerializedAttribute[]", "System", t3721_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1662_TI, t3721_ITIs, t3721_VT, &EmptyCustomAttributesCache, &t3721_TI, &t3721_0_0_0, &t3721_1_0_0, t3721_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1662 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3722_TI;



// Metadata Definition System.PlatformID[]
static MethodInfo* t3722_MIs[] =
{
	NULL
};
extern MethodInfo m26188_MI;
extern MethodInfo m26189_MI;
extern MethodInfo m26190_MI;
extern MethodInfo m26191_MI;
extern MethodInfo m26192_MI;
extern MethodInfo m26193_MI;
extern MethodInfo m26187_MI;
extern MethodInfo m26195_MI;
extern MethodInfo m26196_MI;
static MethodInfo* t3722_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26188_MI,
	&m5907_MI,
	&m26189_MI,
	&m26190_MI,
	&m26191_MI,
	&m26192_MI,
	&m26193_MI,
	&m5908_MI,
	&m26187_MI,
	&m26195_MI,
	&m26196_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6609_TI;
extern TypeInfo t6610_TI;
extern TypeInfo t6611_TI;
static TypeInfo* t3722_ITIs[] = 
{
	&t6609_TI,
	&t6610_TI,
	&t6611_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3722_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6609_TI, 21},
	{ &t6610_TI, 28},
	{ &t6611_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3722_0_0_0;
extern Il2CppType t3722_1_0_0;
#include "t1644.h"
extern TypeInfo t1644_TI;
extern CustomAttributesCache t1644__CustomAttributeCache;
TypeInfo t3722_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PlatformID[]", "System", t3722_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1644_TI, t3722_ITIs, t3722_VT, &EmptyCustomAttributesCache, &t44_TI, &t3722_0_0_0, &t3722_1_0_0, t3722_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3723_TI;



// Metadata Definition System.StringComparison[]
static MethodInfo* t3723_MIs[] =
{
	NULL
};
extern MethodInfo m26199_MI;
extern MethodInfo m26200_MI;
extern MethodInfo m26201_MI;
extern MethodInfo m26202_MI;
extern MethodInfo m26203_MI;
extern MethodInfo m26204_MI;
extern MethodInfo m26198_MI;
extern MethodInfo m26206_MI;
extern MethodInfo m26207_MI;
static MethodInfo* t3723_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26199_MI,
	&m5907_MI,
	&m26200_MI,
	&m26201_MI,
	&m26202_MI,
	&m26203_MI,
	&m26204_MI,
	&m5908_MI,
	&m26198_MI,
	&m26206_MI,
	&m26207_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6612_TI;
extern TypeInfo t6613_TI;
extern TypeInfo t6614_TI;
static TypeInfo* t3723_ITIs[] = 
{
	&t6612_TI,
	&t6613_TI,
	&t6614_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3723_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6612_TI, 21},
	{ &t6613_TI, 28},
	{ &t6614_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3723_0_0_0;
extern Il2CppType t3723_1_0_0;
#include "t947.h"
extern TypeInfo t947_TI;
extern CustomAttributesCache t947__CustomAttributeCache;
TypeInfo t3723_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringComparison[]", "System", t3723_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t947_TI, t3723_ITIs, t3723_VT, &EmptyCustomAttributesCache, &t44_TI, &t3723_0_0_0, &t3723_1_0_0, t3723_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3724_TI;



// Metadata Definition System.StringSplitOptions[]
static MethodInfo* t3724_MIs[] =
{
	NULL
};
extern MethodInfo m26210_MI;
extern MethodInfo m26211_MI;
extern MethodInfo m26212_MI;
extern MethodInfo m26213_MI;
extern MethodInfo m26214_MI;
extern MethodInfo m26215_MI;
extern MethodInfo m26209_MI;
extern MethodInfo m26217_MI;
extern MethodInfo m26218_MI;
static MethodInfo* t3724_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26210_MI,
	&m5907_MI,
	&m26211_MI,
	&m26212_MI,
	&m26213_MI,
	&m26214_MI,
	&m26215_MI,
	&m5908_MI,
	&m26209_MI,
	&m26217_MI,
	&m26218_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6615_TI;
extern TypeInfo t6616_TI;
extern TypeInfo t6617_TI;
static TypeInfo* t3724_ITIs[] = 
{
	&t6615_TI,
	&t6616_TI,
	&t6617_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3724_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6615_TI, 21},
	{ &t6616_TI, 28},
	{ &t6617_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3724_0_0_0;
extern Il2CppType t3724_1_0_0;
#include "t939.h"
extern TypeInfo t939_TI;
extern CustomAttributesCache t939__CustomAttributeCache;
TypeInfo t3724_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringSplitOptions[]", "System", t3724_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t939_TI, t3724_ITIs, t3724_VT, &EmptyCustomAttributesCache, &t44_TI, &t3724_0_0_0, &t3724_1_0_0, t3724_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3725_TI;



// Metadata Definition System.ThreadStaticAttribute[]
static MethodInfo* t3725_MIs[] =
{
	NULL
};
extern MethodInfo m26221_MI;
extern MethodInfo m26222_MI;
extern MethodInfo m26223_MI;
extern MethodInfo m26224_MI;
extern MethodInfo m26225_MI;
extern MethodInfo m26226_MI;
extern MethodInfo m26220_MI;
extern MethodInfo m26228_MI;
extern MethodInfo m26229_MI;
static MethodInfo* t3725_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26221_MI,
	&m5907_MI,
	&m26222_MI,
	&m26223_MI,
	&m26224_MI,
	&m26225_MI,
	&m26226_MI,
	&m5908_MI,
	&m26220_MI,
	&m26228_MI,
	&m26229_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6618_TI;
extern TypeInfo t6619_TI;
extern TypeInfo t6620_TI;
static TypeInfo* t3725_ITIs[] = 
{
	&t6618_TI,
	&t6619_TI,
	&t6620_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3725_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6618_TI, 21},
	{ &t6619_TI, 28},
	{ &t6620_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3725_0_0_0;
extern Il2CppType t3725_1_0_0;
struct t1671;
extern TypeInfo t1671_TI;
extern CustomAttributesCache t1671__CustomAttributeCache;
TypeInfo t3725_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ThreadStaticAttribute[]", "System", t3725_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1671_TI, t3725_ITIs, t3725_VT, &EmptyCustomAttributesCache, &t3725_TI, &t3725_0_0_0, &t3725_1_0_0, t3725_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1671 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3726_TI;



// Metadata Definition System.TypeCode[]
static MethodInfo* t3726_MIs[] =
{
	NULL
};
extern MethodInfo m26232_MI;
extern MethodInfo m26233_MI;
extern MethodInfo m26234_MI;
extern MethodInfo m26235_MI;
extern MethodInfo m26236_MI;
extern MethodInfo m26237_MI;
extern MethodInfo m26231_MI;
extern MethodInfo m26239_MI;
extern MethodInfo m26240_MI;
static MethodInfo* t3726_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26232_MI,
	&m5907_MI,
	&m26233_MI,
	&m26234_MI,
	&m26235_MI,
	&m26236_MI,
	&m26237_MI,
	&m5908_MI,
	&m26231_MI,
	&m26239_MI,
	&m26240_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6621_TI;
extern TypeInfo t6622_TI;
extern TypeInfo t6623_TI;
static TypeInfo* t3726_ITIs[] = 
{
	&t6621_TI,
	&t6622_TI,
	&t6623_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3726_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6621_TI, 21},
	{ &t6622_TI, 28},
	{ &t6623_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3726_0_0_0;
extern Il2CppType t3726_1_0_0;
#include "t1127.h"
extern TypeInfo t1127_TI;
extern CustomAttributesCache t1127__CustomAttributeCache;
TypeInfo t3726_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeCode[]", "System", t3726_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1127_TI, t3726_ITIs, t3726_VT, &EmptyCustomAttributesCache, &t44_TI, &t3726_0_0_0, &t3726_1_0_0, t3726_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3727_TI;



// Metadata Definition System.UnitySerializationHolder/UnityType[]
static MethodInfo* t3727_MIs[] =
{
	NULL
};
extern MethodInfo m26243_MI;
extern MethodInfo m26244_MI;
extern MethodInfo m26245_MI;
extern MethodInfo m26246_MI;
extern MethodInfo m26247_MI;
extern MethodInfo m26248_MI;
extern MethodInfo m26242_MI;
extern MethodInfo m26250_MI;
extern MethodInfo m26251_MI;
static MethodInfo* t3727_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m26243_MI,
	&m5907_MI,
	&m26244_MI,
	&m26245_MI,
	&m26246_MI,
	&m26247_MI,
	&m26248_MI,
	&m5908_MI,
	&m26242_MI,
	&m26250_MI,
	&m26251_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6624_TI;
extern TypeInfo t6625_TI;
extern TypeInfo t6626_TI;
static TypeInfo* t3727_ITIs[] = 
{
	&t6624_TI,
	&t6625_TI,
	&t6626_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3727_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6624_TI, 21},
	{ &t6625_TI, 28},
	{ &t6626_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3727_0_0_0;
extern Il2CppType t3727_1_0_0;
#include "t1677.h"
extern TypeInfo t1677_TI;
TypeInfo t3727_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnityType[]", "", t3727_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1677_TI, t3727_ITIs, t3727_VT, &EmptyCustomAttributesCache, &t348_TI, &t3727_0_0_0, &t3727_1_0_0, t3727_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
