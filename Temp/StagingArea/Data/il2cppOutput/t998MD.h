﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t998;
struct t29;
struct t999;
struct t66;
struct t67;
#include "t35.h"

 void m4447 (t998 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4448 (t998 * __this, t29 * p0, t999 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4449 (t998 * __this, t29 * p0, t999 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4450 (t998 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
