﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3492;
struct t29;
struct t20;
#include "t1292.h"

 void m19405 (t3492 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19406 (t3492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19407 (t3492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19408 (t3492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19409 (t3492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
