﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3448;
struct t29;
struct t20;
#include "t1497.h"

 void m19110 (t3448 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19111 (t3448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19112 (t3448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19113 (t3448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19114 (t3448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
