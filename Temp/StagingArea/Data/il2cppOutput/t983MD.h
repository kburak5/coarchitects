﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t983;
struct t793;
struct t7;
struct t781;

 void m4380 (t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4381 (t983 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4382 (t983 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4383 (t983 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4384 (t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4385 (t983 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4386 (t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
