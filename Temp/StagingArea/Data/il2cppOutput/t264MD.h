﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t264;
struct t29;
struct t2;
struct t2744;
struct t20;
struct t136;
struct t2745;
struct t2746;
struct t2747;
struct t2743;
struct t2748;
struct t2749;
#include "t2750.h"

#include "t294MD.h"
#define m1989(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m14956(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m14957(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m14958(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m14959(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m14960(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m14961(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m14962(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m14963(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m14964(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14965(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m14966(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m14967(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m14968(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m14969(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m14970(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m14971(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m14972(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1992(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m14973(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m14974(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m14975(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m14976(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m14977(__this, method) (t2747 *)m10583_gshared((t294 *)__this, method)
#define m1990(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m14978(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m14979(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m14980(__this, p0, method) (t2 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14981(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m14982(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2750  m14983 (t264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m14984(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m14985(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m14986(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m14987(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14988(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m14989(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m14990(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14991(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m14992(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m14993(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m14994(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m14995(__this, method) (t2743*)m10619_gshared((t294 *)__this, method)
#define m14996(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m14997(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m14998(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1959(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1964(__this, p0, method) (t2 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m14999(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
