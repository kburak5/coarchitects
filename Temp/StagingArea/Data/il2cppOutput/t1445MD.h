﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1445;
struct t1424;
struct t29;
struct t7;

 void m7846 (t1445 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7847 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7848 (t1445 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7849 (t1445 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
