﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t770;
struct t29;
struct t7;
struct t3190;
struct t2096;
struct t733;
struct t3191;
struct t20;
struct t136;
struct t3193;
struct t722;
#include "t735.h"
#include "t3192.h"
#include "t3194.h"
#include "t725.h"

 void m17708 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4028 (t770 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17709 (t770 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17710 (t770 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17711 (t770 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17712 (t770 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17713 (t770 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17714 (t770 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17715 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17716 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17717 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17718 (t770 * __this, t3192  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17719 (t770 * __this, t3192  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17720 (t770 * __this, t3191* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17721 (t770 * __this, t3192  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17722 (t770 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17723 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m17724 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17725 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17726 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17727 (t770 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17728 (t770 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17729 (t770 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17730 (t770 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17731 (t770 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3192  m17732 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17733 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17734 (t770 * __this, t3191* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17735 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4029 (t770 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17736 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17737 (t770 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17738 (t770 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17739 (t770 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17740 (t770 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17741 (t770 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17742 (t770 * __this, t7* p0, bool* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3190 * m17743 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m17744 (t770 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17745 (t770 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17746 (t770 * __this, t3192  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3194  m17747 (t770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17748 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
