﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1142;
struct t929;
struct t7;
struct t316;
struct t42;
struct t733;
struct t29;
#include "t735.h"

 void m7565 (t1142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7566 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m7567 (t1142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7568 (t1142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7569 (t1142 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7570 (t1142 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7571 (t1142 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7572 (t1142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7573 (t1142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7574 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7575 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
