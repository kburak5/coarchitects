﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3049;
#include "t380.h"

 void m16796 (t3049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16797 (t3049 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16798 (t3049 * __this, t380  p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
