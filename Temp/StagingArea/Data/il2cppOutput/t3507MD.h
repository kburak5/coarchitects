﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3507;
#include "t816.h"

 void m19476 (t3507 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19477 (t3507 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19478 (t3507 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
