﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t893;
struct t7;
struct t748;
struct t900;
struct t719;

 void m3923 (t893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3924 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3925 (t893 * __this, t748 * p0, t900 ** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3926 (t893 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3927 (t893 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3928 (t893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3929 (t893 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3930 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3931 (t29 * __this, t719 * p0, t893 * p1, t7* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t893 * m3932 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
