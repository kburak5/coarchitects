﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t884;
struct t875;
struct t878;
struct t879;

 void m3793 (t884 * __this, int32_t p0, int32_t p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3794 (t884 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3795 (t884 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3796 (t884 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3797 (t884 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3798 (t884 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t879 * m3799 (t884 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
