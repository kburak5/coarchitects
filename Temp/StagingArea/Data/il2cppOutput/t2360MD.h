﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2360;
struct t29;
struct t118;
#include "t725.h"

 void m12160 (t2360 * __this, t118 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12161 (t2360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12162 (t2360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12163 (t2360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12164 (t2360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12165 (t2360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
