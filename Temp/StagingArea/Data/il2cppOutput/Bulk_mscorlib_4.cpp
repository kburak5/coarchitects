﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t1419.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t1419_TI;
#include "t1419MD.h"

#include "t21.h"
#include "t29MD.h"
extern MethodInfo m1331_MI;

#include "t20.h"

extern MethodInfo m7769_MI;
 void m7769 (t1419 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7769_MI = 
{
	".ctor", (methodPointerType)&m7769, &t1419_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3325, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1419_MIs[] =
{
	&m7769_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t1419_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t1416_TI;
static TypeInfo* t1419_ITIs[] = 
{
	&t1416_TI,
};
static Il2CppInterfaceOffsetPair t1419_IOs[] = 
{
	{ &t1416_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1419_0_0_0;
extern Il2CppType t1419_1_0_0;
extern TypeInfo t29_TI;
struct t1419;
TypeInfo t1419_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConstructionLevelActivator", "System.Runtime.Remoting.Activation", t1419_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1419_TI, t1419_ITIs, t1419_VT, &EmptyCustomAttributesCache, &t1419_TI, &t1419_0_0_0, &t1419_1_0_0, t1419_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1419), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 1, 1};
#include "t1420.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1420_TI;
#include "t1420MD.h"



extern MethodInfo m7770_MI;
 void m7770 (t1420 * __this, t29 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern Il2CppType t1416_0_0_1;
FieldInfo t1420_f0_FieldInfo = 
{
	"m_NextActivator", &t1416_0_0_1, &t1420_TI, offsetof(t1420, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1420_FIs[] =
{
	&t1420_f0_FieldInfo,
	NULL
};
extern Il2CppType t1416_0_0_0;
extern Il2CppType t1416_0_0_0;
static ParameterInfo t1420_m7770_ParameterInfos[] = 
{
	{"next", 0, 134221838, &EmptyCustomAttributesCache, &t1416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7770_MI = 
{
	".ctor", (methodPointerType)&m7770, &t1420_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1420_m7770_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3326, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1420_MIs[] =
{
	&m7770_MI,
	NULL
};
static MethodInfo* t1420_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
static TypeInfo* t1420_ITIs[] = 
{
	&t1416_TI,
};
static Il2CppInterfaceOffsetPair t1420_IOs[] = 
{
	{ &t1416_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1420_0_0_0;
extern Il2CppType t1420_1_0_0;
struct t1420;
TypeInfo t1420_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ContextLevelActivator", "System.Runtime.Remoting.Activation", t1420_MIs, NULL, t1420_FIs, NULL, &t29_TI, NULL, NULL, &t1420_TI, t1420_ITIs, t1420_VT, &EmptyCustomAttributesCache, &t1420_TI, &t1420_0_0_0, &t1420_1_0_0, t1420_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1420), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 1, 1};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Activation.IActivator
static MethodInfo* t1416_MIs[] =
{
	NULL
};
extern TypeInfo t437_TI;
#include "t437.h"
#include "t437MD.h"
extern MethodInfo m2055_MI;
void t1416_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1416__CustomAttributeCache = {
1,
NULL,
&t1416_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1416_1_0_0;
struct t1416;
extern CustomAttributesCache t1416__CustomAttributeCache;
TypeInfo t1416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IActivator", "System.Runtime.Remoting.Activation", t1416_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1416_TI, NULL, NULL, &t1416__CustomAttributeCache, &t1416_TI, &t1416_0_0_0, &t1416_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1424_TI;

#include "t42.h"
#include "t7.h"
#include "mscorlib_ArrayTypes.h"
#include "t29.h"


// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern MethodInfo m10182_MI;
static PropertyInfo t1424____ActivationType_PropertyInfo = 
{
	&t1424_TI, "ActivationType", &m10182_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10183_MI;
static PropertyInfo t1424____ActivationTypeName_PropertyInfo = 
{
	&t1424_TI, "ActivationTypeName", &m10183_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10184_MI;
extern MethodInfo m10185_MI;
static PropertyInfo t1424____Activator_PropertyInfo = 
{
	&t1424_TI, "Activator", &m10184_MI, &m10185_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10186_MI;
static PropertyInfo t1424____CallSiteActivationAttributes_PropertyInfo = 
{
	&t1424_TI, "CallSiteActivationAttributes", &m10186_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10187_MI;
static PropertyInfo t1424____ContextProperties_PropertyInfo = 
{
	&t1424_TI, "ContextProperties", &m10187_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1424_PIs[] =
{
	&t1424____ActivationType_PropertyInfo,
	&t1424____ActivationTypeName_PropertyInfo,
	&t1424____Activator_PropertyInfo,
	&t1424____CallSiteActivationAttributes_PropertyInfo,
	&t1424____ContextProperties_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10182_MI = 
{
	"get_ActivationType", NULL, &t1424_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3327, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10183_MI = 
{
	"get_ActivationTypeName", NULL, &t1424_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, false, 3328, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1416_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10184_MI = 
{
	"get_Activator", NULL, &t1424_TI, &t1416_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 2, 0, false, false, 3329, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1416_0_0_0;
static ParameterInfo t1424_m10185_ParameterInfos[] = 
{
	{"value", 0, 134221839, &EmptyCustomAttributesCache, &t1416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10185_MI = 
{
	"set_Activator", NULL, &t1424_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1424_m10185_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, false, 3330, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10186_MI = 
{
	"get_CallSiteActivationAttributes", NULL, &t1424_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 4, 0, false, false, 3331, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t868_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10187_MI = 
{
	"get_ContextProperties", NULL, &t1424_TI, &t868_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 5, 0, false, false, 3332, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1424_MIs[] =
{
	&m10182_MI,
	&m10183_MI,
	&m10184_MI,
	&m10185_MI,
	&m10186_MI,
	&m10187_MI,
	NULL
};
extern TypeInfo t1443_TI;
extern TypeInfo t1463_TI;
extern TypeInfo t1453_TI;
static TypeInfo* t1424_ITIs[] = 
{
	&t1443_TI,
	&t1463_TI,
	&t1453_TI,
};
void t1424_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1424__CustomAttributeCache = {
1,
NULL,
&t1424_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1424_0_0_0;
extern Il2CppType t1424_1_0_0;
struct t1424;
extern CustomAttributesCache t1424__CustomAttributeCache;
TypeInfo t1424_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IConstructionCallMessage", "System.Runtime.Remoting.Activation", t1424_MIs, t1424_PIs, NULL, NULL, NULL, NULL, NULL, &t1424_TI, t1424_ITIs, NULL, &t1424__CustomAttributeCache, &t1424_TI, &t1424_0_0_0, &t1424_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 6, 5, 0, 0, 0, 0, 3, 0};
#include "t1421.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1421_TI;
#include "t1421MD.h"



// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
static MethodInfo* t1421_MIs[] =
{
	NULL
};
static MethodInfo* t1421_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
static TypeInfo* t1421_ITIs[] = 
{
	&t1416_TI,
};
static Il2CppInterfaceOffsetPair t1421_IOs[] = 
{
	{ &t1416_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1421_0_0_0;
extern Il2CppType t1421_1_0_0;
extern TypeInfo t774_TI;
struct t1421;
TypeInfo t1421_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemoteActivator", "System.Runtime.Remoting.Activation", t1421_MIs, NULL, NULL, NULL, &t774_TI, NULL, NULL, &t1421_TI, t1421_ITIs, t1421_VT, &EmptyCustomAttributesCache, &t1421_TI, &t1421_0_0_0, &t1421_1_0_0, t1421_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1421), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 1, 1};
#include "t1422.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1422_TI;
#include "t1422MD.h"

#include "t40.h"
#include "t44.h"
#include "t1425.h"
extern TypeInfo t7_TI;
extern TypeInfo t44_TI;
#include "t7MD.h"
extern MethodInfo m7771_MI;
extern MethodInfo m1713_MI;
extern MethodInfo m2843_MI;


 t7* m7771 (t1422 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m7772_MI;
 bool m7772 (t1422 * __this, t29 * p0, MethodInfo* method){
	{
		if (((t1422 *)IsInst(p0, InitializedTypeInfo(&t1422_TI))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		t7* L_0 = m7771(((t1422 *)Castclass(p0, InitializedTypeInfo(&t1422_TI))), &m7771_MI);
		t7* L_1 = (__this->f1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, L_0, L_1, &m1713_MI);
		return L_2;
	}
}
extern MethodInfo m7773_MI;
 int32_t m7773 (t1422 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m7774_MI;
 void m7774 (t1422 * __this, t29 * p0, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m7775_MI;
 bool m7775 (t1422 * __this, t1425 * p0, t29 * p1, MethodInfo* method){
	{
		return 1;
	}
}
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t1422_f1_FieldInfo = 
{
	"url", &t7_0_0_1, &t1422_TI, offsetof(t1422, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1422_FIs[] =
{
	&t1422_f1_FieldInfo,
	NULL
};
static PropertyInfo t1422____UrlValue_PropertyInfo = 
{
	&t1422_TI, "UrlValue", &m7771_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1422_PIs[] =
{
	&t1422____UrlValue_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7771_MI = 
{
	"get_UrlValue", (methodPointerType)&m7771, &t1422_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3333, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1422_m7772_ParameterInfos[] = 
{
	{"o", 0, 134221840, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7772_MI = 
{
	"Equals", (methodPointerType)&m7772, &t1422_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1422_m7772_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 3334, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7773_MI = 
{
	"GetHashCode", (methodPointerType)&m7773, &t1422_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 3335, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1422_m7774_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221841, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1422__CustomAttributeCache_m7774;
MethodInfo m7774_MI = 
{
	"GetPropertiesForNewContext", (methodPointerType)&m7774, &t1422_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1422_m7774_ParameterInfos, &t1422__CustomAttributeCache_m7774, 198, 0, 8, 1, false, false, 3336, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1425_0_0_0;
extern Il2CppType t1425_0_0_0;
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1422_m7775_ParameterInfos[] = 
{
	{"ctx", 0, 134221842, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"msg", 1, 134221843, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1422__CustomAttributeCache_m7775;
MethodInfo m7775_MI = 
{
	"IsContextOK", (methodPointerType)&m7775, &t1422_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1422_m7775_ParameterInfos, &t1422__CustomAttributeCache_m7775, 198, 0, 9, 2, false, false, 3337, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1422_MIs[] =
{
	&m7771_MI,
	&m7772_MI,
	&m7773_MI,
	&m7774_MI,
	&m7775_MI,
	NULL
};
extern MethodInfo m7799_MI;
static MethodInfo* t1422_VT[] =
{
	&m7772_MI,
	&m46_MI,
	&m7773_MI,
	&m1332_MI,
	&m7774_MI,
	&m7775_MI,
	&m7799_MI,
	&m7799_MI,
	&m7774_MI,
	&m7775_MI,
};
extern TypeInfo t2040_TI;
extern TypeInfo t1433_TI;
extern TypeInfo t604_TI;
static Il2CppInterfaceOffsetPair t1422_IOs[] = 
{
	{ &t2040_TI, 4},
	{ &t1433_TI, 6},
	{ &t604_TI, 4},
};
void t1422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1422_CustomAttributesCacheGenerator_m7774(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1422_CustomAttributesCacheGenerator_m7775(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1422__CustomAttributeCache = {
1,
NULL,
&t1422_CustomAttributesCacheGenerator
};
CustomAttributesCache t1422__CustomAttributeCache_m7774 = {
1,
NULL,
&t1422_CustomAttributesCacheGenerator_m7774
};
CustomAttributesCache t1422__CustomAttributeCache_m7775 = {
1,
NULL,
&t1422_CustomAttributesCacheGenerator_m7775
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1422_0_0_0;
extern Il2CppType t1422_1_0_0;
extern TypeInfo t1423_TI;
struct t1422;
extern CustomAttributesCache t1422__CustomAttributeCache;
extern CustomAttributesCache t1422__CustomAttributeCache_m7774;
extern CustomAttributesCache t1422__CustomAttributeCache_m7775;
TypeInfo t1422_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UrlAttribute", "System.Runtime.Remoting.Activation", t1422_MIs, t1422_PIs, t1422_FIs, NULL, &t1423_TI, NULL, NULL, &t1422_TI, NULL, t1422_VT, &t1422__CustomAttributeCache, &t1422_TI, &t1422_0_0_0, &t1422_1_0_0, t1422_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1422), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 1, 0, 0, 10, 0, 3};
#include "t1426.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1426_TI;
#include "t1426MD.h"

extern TypeInfo t1427_TI;
#include "t1427MD.h"
extern MethodInfo m7781_MI;


extern MethodInfo m7776_MI;
 void m7776 (t1426 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		t316* L_0 = m7781(NULL, &m7781_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m7777_MI;
 t316* m7777 (t1426 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f0);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern Il2CppType t316_0_0_1;
FieldInfo t1426_f0_FieldInfo = 
{
	"channelData", &t316_0_0_1, &t1426_TI, offsetof(t1426, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1426_FIs[] =
{
	&t1426_f0_FieldInfo,
	NULL
};
static PropertyInfo t1426____ChannelData_PropertyInfo = 
{
	&t1426_TI, "ChannelData", &m7777_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1426_PIs[] =
{
	&t1426____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7776_MI = 
{
	".ctor", (methodPointerType)&m7776, &t1426_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3338, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7777_MI = 
{
	"get_ChannelData", (methodPointerType)&m7777, &t1426_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 3339, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1426_MIs[] =
{
	&m7776_MI,
	&m7777_MI,
	NULL
};
static MethodInfo* t1426_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7777_MI,
};
extern TypeInfo t1477_TI;
static TypeInfo* t1426_ITIs[] = 
{
	&t1477_TI,
};
static Il2CppInterfaceOffsetPair t1426_IOs[] = 
{
	{ &t1477_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1426_0_0_0;
extern Il2CppType t1426_1_0_0;
struct t1426;
TypeInfo t1426_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ChannelInfo", "System.Runtime.Remoting", t1426_MIs, t1426_PIs, t1426_FIs, NULL, &t29_TI, NULL, NULL, &t1426_TI, t1426_ITIs, t1426_VT, &EmptyCustomAttributesCache, &t1426_TI, &t1426_0_0_0, &t1426_1_0_0, t1426_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1426), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 1, 1, 0, 0, 5, 1, 1};
#include "t1427.h"
#ifndef _MSC_VER
#else
#endif

#include "t731.h"
#include "t1428.h"
#include "t338.h"
#include "t1481.h"
extern TypeInfo t731_TI;
extern TypeInfo t1428_TI;
extern TypeInfo t446_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2041_TI;
extern TypeInfo t1429_TI;
extern TypeInfo t1481_TI;
extern TypeInfo t21_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2042_TI;
extern TypeInfo t42_TI;
extern TypeInfo t868_TI;
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
extern TypeInfo t316_TI;
#include "t731MD.h"
#include "t1428MD.h"
#include "t338MD.h"
#include "t1481MD.h"
#include "t921MD.h"
#include "t42MD.h"
extern MethodInfo m3980_MI;
extern MethodInfo m7804_MI;
extern MethodInfo m7780_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m10188_MI;
extern MethodInfo m1535_MI;
extern MethodInfo m7999_MI;
extern MethodInfo m10189_MI;
extern MethodInfo m4176_MI;
extern MethodInfo m4000_MI;
extern MethodInfo m3978_MI;
extern MethodInfo m1740_MI;
extern MethodInfo m1685_MI;
extern MethodInfo m10190_MI;
extern MethodInfo m3976_MI;
extern MethodInfo m4157_MI;
extern MethodInfo m3991_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m9683_MI;
extern MethodInfo m10191_MI;
extern MethodInfo m4001_MI;
extern MethodInfo m3981_MI;
extern MethodInfo m3970_MI;
extern MethodInfo m10192_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m6654_MI;


extern MethodInfo m7778_MI;
 void m7778 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0 = L_0;
		t731 * L_1 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_1, &m3980_MI);
		((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f1 = L_1;
		t1428 * L_2 = (t1428 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1428_TI));
		m7804(L_2, &m7804_MI);
		((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f2 = L_2;
		((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f3 = (t7*) &_stringLiteral1577;
		t446* L_3 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 2));
		ArrayElementTypeCheck (L_3, (t7*) &_stringLiteral1578);
		*((t7**)(t7**)SZArrayLdElema(L_3, 0)) = (t7*)(t7*) &_stringLiteral1578;
		t446* L_4 = L_3;
		ArrayElementTypeCheck (L_4, (t7*) &_stringLiteral1579);
		*((t7**)(t7**)SZArrayLdElema(L_4, 1)) = (t7*)(t7*) &_stringLiteral1579;
		((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f4 = (t29 *)L_4;
		return;
	}
}
extern MethodInfo m7779_MI;
 void m7779 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		m7780(NULL, p0, 0, &m7780_MI);
		return;
	}
}
 void m7780 (t29 * __this, t29 * p0, bool p1, MethodInfo* method){
	t29 * V_0 = {0};
	t29 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t29 * V_4 = {0};
	t29 * V_5 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1580, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!p1)
		{
			goto IL_0038;
		}
	}
	{
		V_1 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t2041_TI)));
		if (V_1)
		{
			goto IL_0031;
		}
	}
	{
		t7* L_1 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10188_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m1535(NULL, (t7*) &_stringLiteral1581, L_1, &m1535_MI);
		t1481 * L_3 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_3, L_2, &m7999_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		InterfaceActionInvoker1< bool >::Invoke(&m10189_MI, V_1, 1);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		t29 * L_4 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4176_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0));
		V_0 = L_4;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			V_2 = (-1);
			V_3 = 0;
			goto IL_00bc;
		}

IL_004f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
			t29 * L_5 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0), V_3);
			V_4 = ((t29 *)Castclass(L_5, InitializedTypeInfo(&t1429_TI)));
			t7* L_6 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10188_MI, V_4);
			t7* L_7 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10188_MI, p0);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			bool L_8 = m1713(NULL, L_6, L_7, &m1713_MI);
			if (!L_8)
			{
				goto IL_00a3;
			}
		}

IL_0075:
		{
			t7* L_9 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10188_MI, p0);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			bool L_10 = m1740(NULL, L_9, (((t7_SFs*)(&t7_TI)->static_fields)->f2), &m1740_MI);
			if (!L_10)
			{
				goto IL_00a3;
			}
		}

IL_0087:
		{
			t7* L_11 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10188_MI, V_4);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			t7* L_12 = m1685(NULL, (t7*) &_stringLiteral1582, L_11, (t7*) &_stringLiteral1583, &m1685_MI);
			t1481 * L_13 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
			m7999(L_13, L_12, &m7999_MI);
			il2cpp_codegen_raise_exception(L_13);
		}

IL_00a3:
		{
			int32_t L_14 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m10190_MI, V_4);
			int32_t L_15 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m10190_MI, p0);
			if ((((int32_t)L_14) >= ((int32_t)L_15)))
			{
				goto IL_00b8;
			}
		}

IL_00b2:
		{
			if ((((uint32_t)V_2) != ((uint32_t)(-1))))
			{
				goto IL_00b8;
			}
		}

IL_00b6:
		{
			V_2 = V_3;
		}

IL_00b8:
		{
			V_3 = ((int32_t)(V_3+1));
		}

IL_00bc:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
			int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0));
			if ((((int32_t)V_3) < ((int32_t)L_16)))
			{
				goto IL_004f;
			}
		}

IL_00c9:
		{
			if ((((int32_t)V_2) == ((int32_t)(-1))))
			{
				goto IL_00db;
			}
		}

IL_00cd:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
			VirtActionInvoker2< int32_t, t29 * >::Invoke(&m4157_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0), V_2, p0);
			goto IL_00e7;
		}

IL_00db:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
			VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0), p0);
		}

IL_00e7:
		{
			V_5 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t2042_TI)));
			if (!V_5)
			{
				goto IL_0112;
			}
		}

IL_00f3:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
			t42 * L_17 = m1430(p0, &m1430_MI);
			t7* L_18 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_17);
			bool L_19 = (bool)InterfaceFuncInvoker1< bool, t29 * >::Invoke(&m9683_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f4), L_18);
			if (!L_19)
			{
				goto IL_0112;
			}
		}

IL_010a:
		{
			InterfaceActionInvoker1< t29 * >::Invoke(&m10191_MI, V_5, NULL);
		}

IL_0112:
		{
			// IL_0112: leave.s IL_011b
			leaveInstructions[0] = 0x11B; // 1
			THROW_SENTINEL(IL_011b);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0114;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0114;
	}

IL_0114:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x11B:
				goto IL_011b;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_011b:
	{
		return;
	}
}
 t316* m7781 (t29 * __this, MethodInfo* method){
	t731 * V_0 = {0};
	t29 * V_1 = {0};
	t29 * V_2 = {0};
	t29 * V_3 = {0};
	t29 * V_4 = {0};
	t29 * V_5 = {0};
	t29 * V_6 = {0};
	int32_t leaveInstructions[2] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4176_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0));
		V_1 = L_1;
		m4000(NULL, V_1, &m4000_MI);
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
			t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f0));
			V_3 = L_2;
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			{
				goto IL_004d;
			}

IL_0024:
			{
				t29 * L_3 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_3);
				V_2 = L_3;
				V_4 = ((t29 *)IsInst(V_2, InitializedTypeInfo(&t2042_TI)));
				if (!V_4)
				{
					goto IL_004d;
				}
			}

IL_0037:
			{
				t29 * L_4 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10192_MI, V_4);
				V_5 = L_4;
				if (!V_5)
				{
					goto IL_004d;
				}
			}

IL_0044:
			{
				VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_0, V_5);
			}

IL_004d:
			{
				bool L_5 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_3);
				if (L_5)
				{
					goto IL_0024;
				}
			}

IL_0055:
			{
				// IL_0055: leave.s IL_006c
				leaveInstructions[1] = 0x6C; // 2
				THROW_SENTINEL(IL_006c);
				// finally target depth: 2
			}
		} // end try (depth: 2)
		catch(Il2CppFinallySentinel& e)
		{
			goto IL_0057;
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (t295 *)e.ex;
			goto IL_0057;
		}

IL_0057:
		{ // begin finally (depth: 2)
			{
				V_6 = ((t29 *)IsInst(V_3, InitializedTypeInfo(&t324_TI)));
				if (V_6)
				{
					goto IL_0064;
				}
			}

IL_0063:
			{
				// finally node depth: 2
				switch (leaveInstructions[1])
				{
					case 0x6C:
						goto IL_006c;
					default:
					{
						#if IL2CPP_DEBUG
						assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
						#endif
						t295 * _tmp_exception_local = __last_unhandled_exception;
						__last_unhandled_exception = 0;
						il2cpp_codegen_raise_exception(_tmp_exception_local);
					}
				}
			}

IL_0064:
			{
				InterfaceActionInvoker0::Invoke(&m1428_MI, V_6);
				// finally node depth: 2
				switch (leaveInstructions[1])
				{
					case 0x6C:
						goto IL_006c;
					default:
					{
						#if IL2CPP_DEBUG
						assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
						#endif
						t295 * _tmp_exception_local = __last_unhandled_exception;
						__last_unhandled_exception = 0;
						il2cpp_codegen_raise_exception(_tmp_exception_local);
					}
				}
			}
		} // end finally (depth: 2)

IL_006c:
		{
			// IL_006c: leave.s IL_0075
			leaveInstructions[0] = 0x75; // 1
			THROW_SENTINEL(IL_0075);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_006e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_006e;
	}

IL_006e:
	{ // begin finally (depth: 1)
		m4001(NULL, V_1, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x75:
				goto IL_0075;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0075:
	{
		t316* L_6 = (t316*)VirtFuncInvoker0< t316* >::Invoke(&m6654_MI, V_0);
		return L_6;
	}
}
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern Il2CppType t731_0_0_17;
FieldInfo t1427_f0_FieldInfo = 
{
	"registeredChannels", &t731_0_0_17, &t1427_TI, offsetof(t1427_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t731_0_0_17;
FieldInfo t1427_f1_FieldInfo = 
{
	"delayedClientChannels", &t731_0_0_17, &t1427_TI, offsetof(t1427_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1428_0_0_17;
FieldInfo t1427_f2_FieldInfo = 
{
	"_crossContextSink", &t1428_0_0_17, &t1427_TI, offsetof(t1427_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_19;
FieldInfo t1427_f3_FieldInfo = 
{
	"CrossContextUrl", &t7_0_0_19, &t1427_TI, offsetof(t1427_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t868_0_0_17;
FieldInfo t1427_f4_FieldInfo = 
{
	"oldStartModeTypes", &t868_0_0_17, &t1427_TI, offsetof(t1427_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1427_FIs[] =
{
	&t1427_f0_FieldInfo,
	&t1427_f1_FieldInfo,
	&t1427_f2_FieldInfo,
	&t1427_f3_FieldInfo,
	&t1427_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7778_MI = 
{
	".cctor", (methodPointerType)&m7778, &t1427_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3340, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1429_0_0_0;
extern Il2CppType t1429_0_0_0;
static ParameterInfo t1427_m7779_ParameterInfos[] = 
{
	{"chnl", 0, 134221844, &EmptyCustomAttributesCache, &t1429_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1427__CustomAttributeCache_m7779;
MethodInfo m7779_MI = 
{
	"RegisterChannel", (methodPointerType)&m7779, &t1427_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1427_m7779_ParameterInfos, &t1427__CustomAttributeCache_m7779, 150, 0, 255, 1, false, false, 3341, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1429_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1427_m7780_ParameterInfos[] = 
{
	{"chnl", 0, 134221845, &EmptyCustomAttributesCache, &t1429_0_0_0},
	{"ensureSecurity", 1, 134221846, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7780_MI = 
{
	"RegisterChannel", (methodPointerType)&m7780, &t1427_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t1427_m7780_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 3342, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7781_MI = 
{
	"GetCurrentChannelInfo", (methodPointerType)&m7781, &t1427_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 147, 0, 255, 0, false, false, 3343, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1427_MIs[] =
{
	&m7778_MI,
	&m7779_MI,
	&m7780_MI,
	&m7781_MI,
	NULL
};
static MethodInfo* t1427_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t327_TI;
#include "t327.h"
#include "t327MD.h"
extern MethodInfo m2838_MI;
void t1427_CustomAttributesCacheGenerator_m7779(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m2838(tmp, il2cpp_codegen_string_new_wrapper("Use RegisterChannel(IChannel,Boolean)"), &m2838_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1427__CustomAttributeCache = {
1,
NULL,
&t1427_CustomAttributesCacheGenerator
};
CustomAttributesCache t1427__CustomAttributeCache_m7779 = {
1,
NULL,
&t1427_CustomAttributesCacheGenerator_m7779
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1427_0_0_0;
extern Il2CppType t1427_1_0_0;
struct t1427;
extern CustomAttributesCache t1427__CustomAttributeCache;
extern CustomAttributesCache t1427__CustomAttributeCache_m7779;
TypeInfo t1427_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ChannelServices", "System.Runtime.Remoting.Channels", t1427_MIs, NULL, t1427_FIs, NULL, &t29_TI, NULL, NULL, &t1427_TI, NULL, t1427_VT, &t1427__CustomAttributeCache, &t1427_TI, &t1427_0_0_0, &t1427_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1427), 0, -1, sizeof(t1427_SFs), 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 0, 5, 0, 0, 4, 0, 0};
#include "t1430.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1430_TI;
#include "t1430MD.h"

extern TypeInfo t1480_TI;
#include "t1480MD.h"
extern MethodInfo m7996_MI;


extern MethodInfo m7782_MI;
 void m7782 (t1430 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		int32_t L_0 = 0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		__this->f0 = L_1;
		__this->f1 = p0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		t7* L_2 = m7996(NULL, &m7996_MI);
		__this->f2 = L_2;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern Il2CppType t29_0_0_1;
FieldInfo t1430_f0_FieldInfo = 
{
	"_ContextID", &t29_0_0_1, &t1430_TI, offsetof(t1430, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1430_f1_FieldInfo = 
{
	"_DomainID", &t44_0_0_1, &t1430_TI, offsetof(t1430, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1430_f2_FieldInfo = 
{
	"_processGuid", &t7_0_0_1, &t1430_TI, offsetof(t1430, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1430_FIs[] =
{
	&t1430_f0_FieldInfo,
	&t1430_f1_FieldInfo,
	&t1430_f2_FieldInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1430_m7782_ParameterInfos[] = 
{
	{"domainId", 0, 134221847, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7782_MI = 
{
	".ctor", (methodPointerType)&m7782, &t1430_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1430_m7782_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, false, 3344, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1430_MIs[] =
{
	&m7782_MI,
	NULL
};
static MethodInfo* t1430_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1430_0_0_0;
extern Il2CppType t1430_1_0_0;
struct t1430;
TypeInfo t1430_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CrossAppDomainData", "System.Runtime.Remoting.Channels", t1430_MIs, NULL, t1430_FIs, NULL, &t29_TI, NULL, NULL, &t1430_TI, NULL, t1430_VT, &EmptyCustomAttributesCache, &t1430_TI, &t1430_0_0_0, &t1430_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1430), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 3, 0, 0, 4, 0, 0};
#include "t1431.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1431_TI;
#include "t1431MD.h"

extern TypeInfo t1436_TI;
#include "t1436MD.h"
extern MethodInfo m8792_MI;
extern MethodInfo m7783_MI;


 void m7783 (t1431 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7784_MI;
 void m7784 (t29 * __this, MethodInfo* method){
	{
		t29 * L_0 = (t29 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t29_TI));
		m1331(L_0, &m1331_MI);
		((t1431_SFs*)InitializedTypeInfo(&t1431_TI)->static_fields)->f0 = L_0;
		return;
	}
}
extern MethodInfo m7785_MI;
 void m7785 (t29 * __this, MethodInfo* method){
	t29 * V_0 = {0};
	t1431 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1431_TI));
		V_0 = (((t1431_SFs*)InitializedTypeInfo(&t1431_TI)->static_fields)->f0);
		m4000(NULL, V_0, &m4000_MI);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1431_TI));
		t1431 * L_0 = (t1431 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1431_TI));
		m7783(L_0, &m7783_MI);
		V_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		m7779(NULL, V_1, &m7779_MI);
		// IL_0018: leave.s IL_0021
		leaveInstructions[0] = 0x21; // 1
		THROW_SENTINEL(IL_0021);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_001a;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_001a;
	}

IL_001a:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x21:
				goto IL_0021;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0021:
	{
		return;
	}
}
extern MethodInfo m7786_MI;
 t7* m7786 (t1431 * __this, MethodInfo* method){
	{
		return (t7*) &_stringLiteral1584;
	}
}
extern MethodInfo m7787_MI;
 int32_t m7787 (t1431 * __this, MethodInfo* method){
	{
		return ((int32_t)100);
	}
}
extern MethodInfo m7788_MI;
 t29 * m7788 (t1431 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		int32_t L_0 = m8792(NULL, &m8792_MI);
		t1430 * L_1 = (t1430 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1430_TI));
		m7782(L_1, L_0, &m7782_MI);
		return L_1;
	}
}
extern MethodInfo m7789_MI;
 void m7789 (t1431 * __this, t29 * p0, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern Il2CppType t29_0_0_17;
FieldInfo t1431_f0_FieldInfo = 
{
	"s_lock", &t29_0_0_17, &t1431_TI, offsetof(t1431_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1431_FIs[] =
{
	&t1431_f0_FieldInfo,
	NULL
};
static PropertyInfo t1431____ChannelName_PropertyInfo = 
{
	&t1431_TI, "ChannelName", &m7786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1431____ChannelPriority_PropertyInfo = 
{
	&t1431_TI, "ChannelPriority", &m7787_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1431____ChannelData_PropertyInfo = 
{
	&t1431_TI, "ChannelData", &m7788_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1431_PIs[] =
{
	&t1431____ChannelName_PropertyInfo,
	&t1431____ChannelPriority_PropertyInfo,
	&t1431____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7783_MI = 
{
	".ctor", (methodPointerType)&m7783, &t1431_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3345, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7784_MI = 
{
	".cctor", (methodPointerType)&m7784, &t1431_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3346, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7785_MI = 
{
	"RegisterCrossAppDomainChannel", (methodPointerType)&m7785, &t1431_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 147, 0, 255, 0, false, false, 3347, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7786_MI = 
{
	"get_ChannelName", (methodPointerType)&m7786, &t1431_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 8, 0, false, false, 3348, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7787_MI = 
{
	"get_ChannelPriority", (methodPointerType)&m7787, &t1431_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2502, 0, 9, 0, false, false, 3349, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7788_MI = 
{
	"get_ChannelData", (methodPointerType)&m7788, &t1431_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 10, 0, false, false, 3350, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1431_m7789_ParameterInfos[] = 
{
	{"data", 0, 134221848, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7789_MI = 
{
	"StartListening", (methodPointerType)&m7789, &t1431_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1431_m7789_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 11, 1, false, false, 3351, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1431_MIs[] =
{
	&m7783_MI,
	&m7784_MI,
	&m7785_MI,
	&m7786_MI,
	&m7787_MI,
	&m7788_MI,
	&m7789_MI,
	NULL
};
static MethodInfo* t1431_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7786_MI,
	&m7787_MI,
	&m7788_MI,
	&m7789_MI,
	&m7786_MI,
	&m7787_MI,
	&m7788_MI,
	&m7789_MI,
};
extern TypeInfo t2043_TI;
static TypeInfo* t1431_ITIs[] = 
{
	&t1429_TI,
	&t2042_TI,
	&t2043_TI,
};
static Il2CppInterfaceOffsetPair t1431_IOs[] = 
{
	{ &t1429_TI, 4},
	{ &t2042_TI, 6},
	{ &t2043_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1431_0_0_0;
extern Il2CppType t1431_1_0_0;
struct t1431;
TypeInfo t1431_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CrossAppDomainChannel", "System.Runtime.Remoting.Channels", t1431_MIs, t1431_PIs, t1431_FIs, NULL, &t29_TI, NULL, NULL, &t1431_TI, t1431_ITIs, t1431_VT, &EmptyCustomAttributesCache, &t1431_TI, &t1431_0_0_0, &t1431_1_0_0, t1431_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1431), 0, -1, sizeof(t1431_SFs), 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, true, false, false, 7, 3, 1, 0, 0, 12, 3, 3};
#include "t1432.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1432_TI;
#include "t1432MD.h"

#include "t719.h"
#include "t43.h"
#include "t557.h"
#include "t630.h"
extern TypeInfo t719_TI;
extern TypeInfo t557_TI;
extern TypeInfo t630_TI;
#include "t719MD.h"
extern Il2CppType t1432_0_0_0;
extern MethodInfo m4209_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6023_MI;


extern MethodInfo m7790_MI;
 void m7790 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		((t1432_SFs*)InitializedTypeInfo(&t1432_TI)->static_fields)->f0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t1432_0_0_0), &m1554_MI);
		t557 * L_2 = (t557 *)VirtFuncInvoker2< t557 *, t7*, int32_t >::Invoke(&m6023_MI, L_1, (t7*) &_stringLiteral1585, ((int32_t)40));
		((t1432_SFs*)InitializedTypeInfo(&t1432_TI)->static_fields)->f1 = L_2;
		return;
	}
}
extern MethodInfo m7791_MI;
 int32_t m7791 (t1432 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern Il2CppType t719_0_0_17;
FieldInfo t1432_f0_FieldInfo = 
{
	"s_sinks", &t719_0_0_17, &t1432_TI, offsetof(t1432_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_17;
FieldInfo t1432_f1_FieldInfo = 
{
	"processMessageMethod", &t557_0_0_17, &t1432_TI, offsetof(t1432_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1432_f2_FieldInfo = 
{
	"_domainID", &t44_0_0_1, &t1432_TI, offsetof(t1432, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1432_FIs[] =
{
	&t1432_f0_FieldInfo,
	&t1432_f1_FieldInfo,
	&t1432_f2_FieldInfo,
	NULL
};
static PropertyInfo t1432____TargetDomainId_PropertyInfo = 
{
	&t1432_TI, "TargetDomainId", &m7791_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1432_PIs[] =
{
	&t1432____TargetDomainId_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7790_MI = 
{
	".cctor", (methodPointerType)&m7790, &t1432_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3352, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7791_MI = 
{
	"get_TargetDomainId", (methodPointerType)&m7791, &t1432_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 3353, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1432_MIs[] =
{
	&m7790_MI,
	&m7791_MI,
	NULL
};
static MethodInfo* t1432_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t967_TI;
static TypeInfo* t1432_ITIs[] = 
{
	&t967_TI,
};
static Il2CppInterfaceOffsetPair t1432_IOs[] = 
{
	{ &t967_TI, 4},
};
extern TypeInfo t1168_TI;
#include "t1168.h"
#include "t1168MD.h"
extern MethodInfo m6082_MI;
void t1432_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1168 * tmp;
		tmp = (t1168 *)il2cpp_codegen_object_new (&t1168_TI);
		m6082(tmp, il2cpp_codegen_string_new_wrapper("Handle domain unloading?"), &m6082_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1432__CustomAttributeCache = {
1,
NULL,
&t1432_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1432_1_0_0;
struct t1432;
extern CustomAttributesCache t1432__CustomAttributeCache;
TypeInfo t1432_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CrossAppDomainSink", "System.Runtime.Remoting.Channels", t1432_MIs, t1432_PIs, t1432_FIs, NULL, &t29_TI, NULL, NULL, &t1432_TI, t1432_ITIs, t1432_VT, &t1432__CustomAttributeCache, &t1432_TI, &t1432_0_0_0, &t1432_1_0_0, t1432_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1432), 0, -1, sizeof(t1432_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 2, 1, 3, 0, 0, 4, 1, 1};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Channels.IChannel
static PropertyInfo t1429____ChannelName_PropertyInfo = 
{
	&t1429_TI, "ChannelName", &m10188_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1429____ChannelPriority_PropertyInfo = 
{
	&t1429_TI, "ChannelPriority", &m10190_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1429_PIs[] =
{
	&t1429____ChannelName_PropertyInfo,
	&t1429____ChannelPriority_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10188_MI = 
{
	"get_ChannelName", NULL, &t1429_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3354, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m10190_MI = 
{
	"get_ChannelPriority", NULL, &t1429_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, false, 3355, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1429_MIs[] =
{
	&m10188_MI,
	&m10190_MI,
	NULL
};
void t1429_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1429__CustomAttributeCache = {
1,
NULL,
&t1429_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1429_1_0_0;
struct t1429;
extern CustomAttributesCache t1429__CustomAttributeCache;
TypeInfo t1429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IChannel", "System.Runtime.Remoting.Channels", t1429_MIs, t1429_PIs, NULL, NULL, NULL, NULL, NULL, &t1429_TI, NULL, NULL, &t1429__CustomAttributeCache, &t1429_TI, &t1429_0_0_0, &t1429_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 2, 2, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
static PropertyInfo t2042____ChannelData_PropertyInfo = 
{
	&t2042_TI, "ChannelData", &m10192_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2042_PIs[] =
{
	&t2042____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10192_MI = 
{
	"get_ChannelData", NULL, &t2042_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3356, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2042_m10191_ParameterInfos[] = 
{
	{"data", 0, 134221849, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10191_MI = 
{
	"StartListening", NULL, &t2042_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2042_m10191_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, false, 3357, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t2042_MIs[] =
{
	&m10192_MI,
	&m10191_MI,
	NULL
};
static TypeInfo* t2042_ITIs[] = 
{
	&t1429_TI,
};
void t2042_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2042__CustomAttributeCache = {
1,
NULL,
&t2042_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2042_0_0_0;
extern Il2CppType t2042_1_0_0;
struct t2042;
extern CustomAttributesCache t2042__CustomAttributeCache;
TypeInfo t2042_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IChannelReceiver", "System.Runtime.Remoting.Channels", t2042_MIs, t2042_PIs, NULL, NULL, NULL, NULL, NULL, &t2042_TI, t2042_ITIs, NULL, &t2042__CustomAttributeCache, &t2042_TI, &t2042_0_0_0, &t2042_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 2, 1, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
static MethodInfo* t2043_MIs[] =
{
	NULL
};
static TypeInfo* t2043_ITIs[] = 
{
	&t1429_TI,
};
void t2043_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2043__CustomAttributeCache = {
1,
NULL,
&t2043_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2043_0_0_0;
extern Il2CppType t2043_1_0_0;
struct t2043;
extern CustomAttributesCache t2043__CustomAttributeCache;
TypeInfo t2043_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IChannelSender", "System.Runtime.Remoting.Channels", t2043_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2043_TI, t2043_ITIs, NULL, &t2043__CustomAttributeCache, &t2043_TI, &t2043_0_0_0, &t2043_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
static PropertyInfo t2041____IsSecured_PropertyInfo = 
{
	&t2041_TI, "IsSecured", NULL, &m10189_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2041_PIs[] =
{
	&t2041____IsSecured_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
static ParameterInfo t2041_m10189_ParameterInfos[] = 
{
	{"value", 0, 134221850, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m10189_MI = 
{
	"set_IsSecured", NULL, &t2041_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t2041_m10189_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 0, 1, false, false, 3358, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t2041_MIs[] =
{
	&m10189_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2041_0_0_0;
extern Il2CppType t2041_1_0_0;
struct t2041;
TypeInfo t2041_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ISecurableChannel", "System.Runtime.Remoting.Channels", t2041_MIs, t2041_PIs, NULL, NULL, NULL, NULL, NULL, &t2041_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2041_TI, &t2041_0_0_0, &t2041_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1425_TI;
#include "t1425MD.h"

#include "t1611MD.h"
extern MethodInfo m8842_MI;
extern MethodInfo m10193_MI;
extern MethodInfo m1312_MI;


extern MethodInfo m7792_MI;
 void m7792 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		((t1425_SFs*)InitializedTypeInfo(&t1425_TI)->static_fields)->f2 = L_0;
		return;
	}
}
extern MethodInfo m7793_MI;
 void m7793 (t1425 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		// IL_0000: leave.s IL_0009
		leaveInstructions[0] = 0x9; // 1
		THROW_SENTINEL(IL_0009);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0002;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0002;
	}

IL_0002:
	{ // begin finally (depth: 1)
		m46(__this, &m46_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x9:
				goto IL_0009;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0009:
	{
		return;
	}
}
extern MethodInfo m7794_MI;
 t1425 * m7794 (t29 * __this, MethodInfo* method){
	{
		t1425 * L_0 = m8842(NULL, &m8842_MI);
		return L_0;
	}
}
extern MethodInfo m7795_MI;
 bool m7795 (t1425 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m7796_MI;
 t29 * m7796 (t1425 * __this, t7* p0, MethodInfo* method){
	t29 * V_0 = {0};
	t29 * V_1 = {0};
	t29 * V_2 = {0};
	t29 * V_3 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t731 * L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (t29 *)NULL;
	}

IL_000a:
	{
		t731 * L_1 = (__this->f1);
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, L_1);
		V_1 = L_2;
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0036;
		}

IL_0018:
		{
			t29 * L_3 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_1);
			V_0 = ((t29 *)Castclass(L_3, InitializedTypeInfo(&t1433_TI)));
			t7* L_4 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10193_MI, V_0);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			bool L_5 = m1713(NULL, L_4, p0, &m1713_MI);
			if (!L_5)
			{
				goto IL_0036;
			}
		}

IL_0032:
		{
			V_2 = V_0;
			// IL_0034: leave.s IL_0054
			leaveInstructions[0] = 0x54; // 1
			THROW_SENTINEL(IL_0054);
			// finally target depth: 1
		}

IL_0036:
		{
			bool L_6 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_6)
			{
				goto IL_0018;
			}
		}

IL_003e:
		{
			// IL_003e: leave.s IL_0052
			leaveInstructions[0] = 0x52; // 1
			THROW_SENTINEL(IL_0052);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0040;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0040;
	}

IL_0040:
	{ // begin finally (depth: 1)
		{
			V_3 = ((t29 *)IsInst(V_1, InitializedTypeInfo(&t324_TI)));
			if (V_3)
			{
				goto IL_004b;
			}
		}

IL_004a:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x54:
					goto IL_0054;
				case 0x52:
					goto IL_0052;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_004b:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_3);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x54:
					goto IL_0054;
				case 0x52:
					goto IL_0052;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0052:
	{
		return (t29 *)NULL;
	}

IL_0054:
	{
		return V_2;
	}
}
extern MethodInfo m7797_MI;
 t7* m7797 (t1425 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1312(NULL, (t7*) &_stringLiteral1586, L_2, &m1312_MI);
		return L_3;
	}
}
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern Il2CppType t44_0_0_1;
FieldInfo t1425_f0_FieldInfo = 
{
	"context_id", &t44_0_0_1, &t1425_TI, offsetof(t1425, f0), &EmptyCustomAttributesCache};
extern Il2CppType t731_0_0_1;
FieldInfo t1425_f1_FieldInfo = 
{
	"context_properties", &t731_0_0_1, &t1425_TI, offsetof(t1425, f1), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1425_f2_FieldInfo = 
{
	"namedSlots", &t719_0_0_17, &t1425_TI, offsetof(t1425_SFs, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1425_FIs[] =
{
	&t1425_f0_FieldInfo,
	&t1425_f1_FieldInfo,
	&t1425_f2_FieldInfo,
	NULL
};
static PropertyInfo t1425____DefaultContext_PropertyInfo = 
{
	&t1425_TI, "DefaultContext", &m7794_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1425____IsDefaultContext_PropertyInfo = 
{
	&t1425_TI, "IsDefaultContext", &m7795_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1425_PIs[] =
{
	&t1425____DefaultContext_PropertyInfo,
	&t1425____IsDefaultContext_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7792_MI = 
{
	".cctor", (methodPointerType)&m7792, &t1425_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3359, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7793_MI = 
{
	"Finalize", (methodPointerType)&m7793, &t1425_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 1, 0, false, false, 3360, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1425_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7794_MI = 
{
	"get_DefaultContext", (methodPointerType)&m7794, &t1425_TI, &t1425_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 3361, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7795_MI = 
{
	"get_IsDefaultContext", (methodPointerType)&m7795, &t1425_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 3362, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1425_m7796_ParameterInfos[] = 
{
	{"name", 0, 134221851, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t1433_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7796_MI = 
{
	"GetProperty", (methodPointerType)&m7796, &t1425_TI, &t1433_0_0_0, RuntimeInvoker_t29_t29, t1425_m7796_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 4, 1, false, false, 3363, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7797_MI = 
{
	"ToString", (methodPointerType)&m7797, &t1425_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3364, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1425_MIs[] =
{
	&m7792_MI,
	&m7793_MI,
	&m7794_MI,
	&m7795_MI,
	&m7796_MI,
	&m7797_MI,
	NULL
};
static MethodInfo* t1425_VT[] =
{
	&m1321_MI,
	&m7793_MI,
	&m1322_MI,
	&m7797_MI,
	&m7796_MI,
};
void t1425_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1425__CustomAttributeCache = {
1,
NULL,
&t1425_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1425_1_0_0;
struct t1425;
extern CustomAttributesCache t1425__CustomAttributeCache;
TypeInfo t1425_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Context", "System.Runtime.Remoting.Contexts", t1425_MIs, t1425_PIs, t1425_FIs, NULL, &t29_TI, NULL, NULL, &t1425_TI, NULL, t1425_VT, &t1425__CustomAttributeCache, &t1425_TI, &t1425_0_0_0, &t1425_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1425), 0, -1, sizeof(t1425_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, true, true, false, false, 6, 2, 3, 0, 0, 5, 0, 0};
#include "t1423.h"
#ifndef _MSC_VER
#else
#endif
#include "t1423MD.h"

#include "t490MD.h"
extern MethodInfo m2881_MI;
extern MethodInfo m4270_MI;
extern MethodInfo m5996_MI;


extern MethodInfo m7798_MI;
 void m7798 (t1423 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
 t7* m7799 (t1423 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m7800_MI;
 bool m7800 (t1423 * __this, t29 * p0, MethodInfo* method){
	t1423 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_0005;
		}
	}
	{
		return 0;
	}

IL_0005:
	{
		if (((t1423 *)IsInst(p0, InitializedTypeInfo(&t1423_TI))))
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		V_0 = ((t1423 *)Castclass(p0, InitializedTypeInfo(&t1423_TI)));
		t7* L_0 = (V_0->f0);
		t7* L_1 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1740(NULL, L_0, L_1, &m1740_MI);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 0;
	}

IL_002b:
	{
		return 1;
	}
}
extern MethodInfo m7801_MI;
 int32_t m7801 (t1423 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		t7* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_1);
		return L_2;
	}
}
extern MethodInfo m7802_MI;
 void m7802 (t1423 * __this, t29 * p0, MethodInfo* method){
	t29 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1587, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10187_MI, p0);
		V_0 = L_1;
		InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m4270_MI, V_0, __this);
		return;
	}
}
extern MethodInfo m7803_MI;
 bool m7803 (t1423 * __this, t1425 * p0, t29 * p1, MethodInfo* method){
	t29 * V_0 = {0};
	{
		if (p1)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1587, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (p0)
		{
			goto IL_001c;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral1588, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001c:
	{
		t42 * L_2 = (t42 *)InterfaceFuncInvoker0< t42 * >::Invoke(&m10182_MI, p1);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5996_MI, L_2);
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		t7* L_4 = (__this->f0);
		t29 * L_5 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7796_MI, p0, L_4);
		V_0 = L_5;
		if (V_0)
		{
			goto IL_003d;
		}
	}
	{
		return 0;
	}

IL_003d:
	{
		if ((((t1423 *)__this) == ((t29 *)V_0)))
		{
			goto IL_0043;
		}
	}
	{
		return 0;
	}

IL_0043:
	{
		return 1;
	}
}
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern Il2CppType t7_0_0_4;
FieldInfo t1423_f0_FieldInfo = 
{
	"AttributeName", &t7_0_0_4, &t1423_TI, offsetof(t1423, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1423_FIs[] =
{
	&t1423_f0_FieldInfo,
	NULL
};
static PropertyInfo t1423____Name_PropertyInfo = 
{
	&t1423_TI, "Name", &m7799_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1423_PIs[] =
{
	&t1423____Name_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1423_m7798_ParameterInfos[] = 
{
	{"name", 0, 134221852, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7798_MI = 
{
	".ctor", (methodPointerType)&m7798, &t1423_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1423_m7798_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3365, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7799_MI = 
{
	"get_Name", (methodPointerType)&m7799, &t1423_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 7, 0, false, false, 3366, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1423_m7800_ParameterInfos[] = 
{
	{"o", 0, 134221853, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7800_MI = 
{
	"Equals", (methodPointerType)&m7800, &t1423_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1423_m7800_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 3367, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7801_MI = 
{
	"GetHashCode", (methodPointerType)&m7801, &t1423_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 3368, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1423_m7802_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221854, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7802_MI = 
{
	"GetPropertiesForNewContext", (methodPointerType)&m7802, &t1423_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1423_m7802_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 8, 1, false, false, 3369, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1425_0_0_0;
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1423_m7803_ParameterInfos[] = 
{
	{"ctx", 0, 134221855, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"ctorMsg", 1, 134221856, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7803_MI = 
{
	"IsContextOK", (methodPointerType)&m7803, &t1423_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1423_m7803_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 9, 2, false, false, 3370, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1423_MIs[] =
{
	&m7798_MI,
	&m7799_MI,
	&m7800_MI,
	&m7801_MI,
	&m7802_MI,
	&m7803_MI,
	NULL
};
static MethodInfo* t1423_VT[] =
{
	&m7800_MI,
	&m46_MI,
	&m7801_MI,
	&m1332_MI,
	&m7802_MI,
	&m7803_MI,
	&m7799_MI,
	&m7799_MI,
	&m7802_MI,
	&m7803_MI,
};
static TypeInfo* t1423_ITIs[] = 
{
	&t2040_TI,
	&t1433_TI,
};
static Il2CppInterfaceOffsetPair t1423_IOs[] = 
{
	{ &t604_TI, 4},
	{ &t2040_TI, 4},
	{ &t1433_TI, 6},
};
extern TypeInfo t629_TI;
#include "t629.h"
#include "t629MD.h"
extern MethodInfo m2915_MI;
void t1423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1423__CustomAttributeCache = {
2,
NULL,
&t1423_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1423_0_0_0;
extern Il2CppType t1423_1_0_0;
extern TypeInfo t490_TI;
struct t1423;
extern CustomAttributesCache t1423__CustomAttributeCache;
TypeInfo t1423_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ContextAttribute", "System.Runtime.Remoting.Contexts", t1423_MIs, t1423_PIs, t1423_FIs, NULL, &t490_TI, NULL, NULL, &t1423_TI, t1423_ITIs, t1423_VT, &t1423__CustomAttributeCache, &t1423_TI, &t1423_0_0_0, &t1423_1_0_0, t1423_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1423), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 1, 1, 0, 0, 10, 2, 3};
#ifndef _MSC_VER
#else
#endif



 void m7804 (t1428 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7804_MI = 
{
	".ctor", (methodPointerType)&m7804, &t1428_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3371, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1428_MIs[] =
{
	&m7804_MI,
	NULL
};
static MethodInfo* t1428_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
static TypeInfo* t1428_ITIs[] = 
{
	&t967_TI,
};
static Il2CppInterfaceOffsetPair t1428_IOs[] = 
{
	{ &t967_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1428_0_0_0;
extern Il2CppType t1428_1_0_0;
struct t1428;
TypeInfo t1428_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CrossContextChannel", "System.Runtime.Remoting.Contexts", t1428_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1428_TI, t1428_ITIs, t1428_VT, &EmptyCustomAttributesCache, &t1428_TI, &t1428_0_0_0, &t1428_1_0_0, t1428_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1428), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 1, 1};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern Il2CppType t1424_0_0_0;
static ParameterInfo t2040_m10181_ParameterInfos[] = 
{
	{"msg", 0, 134221857, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10181_MI = 
{
	"GetPropertiesForNewContext", NULL, &t2040_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2040_m10181_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, false, 3372, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1425_0_0_0;
extern Il2CppType t1424_0_0_0;
static ParameterInfo t2040_m10180_ParameterInfos[] = 
{
	{"ctx", 0, 134221858, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"msg", 1, 134221859, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10180_MI = 
{
	"IsContextOK", NULL, &t2040_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2040_m10180_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, false, 3373, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t2040_MIs[] =
{
	&m10181_MI,
	&m10180_MI,
	NULL
};
void t2040_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2040__CustomAttributeCache = {
1,
NULL,
&t2040_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2040_0_0_0;
extern Il2CppType t2040_1_0_0;
struct t2040;
extern CustomAttributesCache t2040__CustomAttributeCache;
TypeInfo t2040_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContextAttribute", "System.Runtime.Remoting.Contexts", t2040_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2040_TI, NULL, NULL, &t2040__CustomAttributeCache, &t2040_TI, &t2040_0_0_0, &t2040_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
static PropertyInfo t1433____Name_PropertyInfo = 
{
	&t1433_TI, "Name", &m10193_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1433_PIs[] =
{
	&t1433____Name_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10193_MI = 
{
	"get_Name", NULL, &t1433_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3374, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1433_MIs[] =
{
	&m10193_MI,
	NULL
};
void t1433_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1433__CustomAttributeCache = {
1,
NULL,
&t1433_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1433_0_0_0;
extern Il2CppType t1433_1_0_0;
struct t1433;
extern CustomAttributesCache t1433__CustomAttributeCache;
TypeInfo t1433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContextProperty", "System.Runtime.Remoting.Contexts", t1433_MIs, t1433_PIs, NULL, NULL, NULL, NULL, NULL, &t1433_TI, NULL, NULL, &t1433__CustomAttributeCache, &t1433_TI, &t1433_0_0_0, &t1433_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2044_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
static MethodInfo* t2044_MIs[] =
{
	NULL
};
void t2044_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2044__CustomAttributeCache = {
1,
NULL,
&t2044_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2044_0_0_0;
extern Il2CppType t2044_1_0_0;
struct t2044;
extern CustomAttributesCache t2044__CustomAttributeCache;
TypeInfo t2044_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContributeClientContextSink", "System.Runtime.Remoting.Contexts", t2044_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2044_TI, NULL, NULL, &t2044__CustomAttributeCache, &t2044_TI, &t2044_0_0_0, &t2044_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2045_TI;



// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
static MethodInfo* t2045_MIs[] =
{
	NULL
};
void t2045_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2045__CustomAttributeCache = {
1,
NULL,
&t2045_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2045_0_0_0;
extern Il2CppType t2045_1_0_0;
struct t2045;
extern CustomAttributesCache t2045__CustomAttributeCache;
TypeInfo t2045_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IContributeServerContextSink", "System.Runtime.Remoting.Contexts", t2045_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2045_TI, NULL, NULL, &t2045__CustomAttributeCache, &t2045_TI, &t2045_0_0_0, &t2045_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#include "t1434.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1434_TI;
#include "t1434MD.h"

#include "t1435.h"
#include "t1050.h"
#include "t1436.h"
#include "t305.h"
extern TypeInfo t1050_TI;
extern TypeInfo t1435_TI;
extern TypeInfo t305_TI;
#include "t1050MD.h"
#include "t1435MD.h"
#include "t305MD.h"
extern MethodInfo m5238_MI;
extern MethodInfo m7808_MI;
extern MethodInfo m8791_MI;
extern MethodInfo m8781_MI;
extern MethodInfo m7806_MI;
extern MethodInfo m8778_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m8789_MI;
extern MethodInfo m7807_MI;


extern MethodInfo m7805_MI;
 void m7805 (t1434 * __this, MethodInfo* method){
	{
		m7806(__this, 8, 0, &m7806_MI);
		return;
	}
}
 void m7806 (t1434 * __this, int32_t p0, bool p1, MethodInfo* method){
	{
		t1435 * L_0 = (t1435 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1435_TI));
		m8778(L_0, 0, &m8778_MI);
		__this->f4 = L_0;
		m7798(__this, (t7*) &_stringLiteral1589, &m7798_MI);
		if ((((int32_t)p0) == ((int32_t)1)))
		{
			goto IL_0032;
		}
	}
	{
		if ((((int32_t)p0) == ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		if ((((int32_t)p0) == ((int32_t)8)))
		{
			goto IL_0032;
		}
	}
	{
		if ((((int32_t)p0) == ((int32_t)2)))
		{
			goto IL_0032;
		}
	}
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral353, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0032:
	{
		__this->f1 = p1;
		__this->f2 = p0;
		return;
	}
}
 void m7807 (t1434 * __this, bool p0, MethodInfo* method){
	t1434 * V_0 = {0};
	t1434 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if (!p0)
		{
			goto IL_004a;
		}
	}
	{
		t1435 * L_0 = (__this->f4);
		VirtFuncInvoker0< bool >::Invoke(&m5238_MI, L_0);
		V_0 = __this;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = (__this->f3);
			__this->f3 = ((int32_t)(L_1+1));
			int32_t L_2 = (__this->f3);
			if ((((int32_t)L_2) <= ((int32_t)1)))
			{
				goto IL_0034;
			}
		}

IL_002e:
		{
			m7808(__this, &m7808_MI);
		}

IL_0034:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
			t1436 * L_3 = m8791(NULL, &m8791_MI);
			__this->f5 = L_3;
			// IL_003f: leave.s IL_0048
			leaveInstructions[0] = 0x48; // 1
			THROW_SENTINEL(IL_0048);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0041;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0041;
	}

IL_0041:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x48:
				goto IL_0048;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0048:
	{
		goto IL_0093;
	}

IL_004a:
	{
		V_1 = __this;
		m4000(NULL, V_1, &m4000_MI);
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0074;
		}

IL_0054:
		{
			int32_t L_4 = (__this->f3);
			__this->f3 = ((int32_t)(L_4-1));
			t1435 * L_5 = (__this->f4);
			m8781(L_5, &m8781_MI);
			__this->f5 = (t1436 *)NULL;
		}

IL_0074:
		{
			int32_t L_6 = (__this->f3);
			if ((((int32_t)L_6) <= ((int32_t)0)))
			{
				goto IL_008a;
			}
		}

IL_007d:
		{
			t1436 * L_7 = (__this->f5);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
			t1436 * L_8 = m8791(NULL, &m8791_MI);
			if ((((t1436 *)L_7) == ((t1436 *)L_8)))
			{
				goto IL_0054;
			}
		}

IL_008a:
		{
			// IL_008a: leave.s IL_0093
			leaveInstructions[0] = 0x93; // 1
			THROW_SENTINEL(IL_0093);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_008c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_008c;
	}

IL_008c:
	{ // begin finally (depth: 1)
		m4001(NULL, V_1, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x93:
				goto IL_0093;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0093:
	{
		return;
	}
}
 void m7808 (t1434 * __this, MethodInfo* method){
	t1434 * V_0 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		V_0 = __this;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (__this->f3);
			if ((((int32_t)L_0) <= ((int32_t)0)))
			{
				goto IL_003e;
			}
		}

IL_0011:
		{
			t1436 * L_1 = (__this->f5);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
			t1436 * L_2 = m8791(NULL, &m8791_MI);
			if ((((t1436 *)L_1) != ((t1436 *)L_2)))
			{
				goto IL_003e;
			}
		}

IL_001e:
		{
			int32_t L_3 = (__this->f3);
			__this->f3 = ((int32_t)(L_3-1));
			t1435 * L_4 = (__this->f4);
			m8781(L_4, &m8781_MI);
			__this->f5 = (t1436 *)NULL;
		}

IL_003e:
		{
			// IL_003e: leave.s IL_0047
			leaveInstructions[0] = 0x47; // 1
			THROW_SENTINEL(IL_0047);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0040;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0040;
	}

IL_0040:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x47:
				goto IL_0047;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0047:
	{
		return;
	}
}
extern MethodInfo m7809_MI;
 void m7809 (t1434 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10187_MI, p0);
		InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m4270_MI, L_1, __this);
	}

IL_0016:
	{
		return;
	}
}
extern MethodInfo m7810_MI;
 bool m7810 (t1434 * __this, t1425 * p0, t29 * p1, MethodInfo* method){
	t1434 * V_0 = {0};
	int32_t V_1 = 0;
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7796_MI, p0, (t7*) &_stringLiteral1589);
		V_0 = ((t1434 *)IsInst(L_0, InitializedTypeInfo(&t1434_TI)));
		int32_t L_1 = (__this->f2);
		V_1 = L_1;
		if (((int32_t)(V_1-1)) == 0)
		{
			goto IL_0042;
		}
		if (((int32_t)(V_1-1)) == 1)
		{
			goto IL_0051;
		}
		if (((int32_t)(V_1-1)) == 2)
		{
			goto IL_0053;
		}
		if (((int32_t)(V_1-1)) == 3)
		{
			goto IL_0047;
		}
		if (((int32_t)(V_1-1)) == 4)
		{
			goto IL_0053;
		}
		if (((int32_t)(V_1-1)) == 5)
		{
			goto IL_0053;
		}
		if (((int32_t)(V_1-1)) == 6)
		{
			goto IL_0053;
		}
		if (((int32_t)(V_1-1)) == 7)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_0053;
	}

IL_0042:
	{
		return ((((t1434 *)V_0) == ((t29 *)NULL))? 1 : 0);
	}

IL_0047:
	{
		return ((((int32_t)((((t1434 *)V_0) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_004f:
	{
		return 0;
	}

IL_0051:
	{
		return 1;
	}

IL_0053:
	{
		return 0;
	}
}
extern MethodInfo m7811_MI;
 void m7811 (t29 * __this, MethodInfo* method){
	t1434 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		t1425 * L_0 = m8789(NULL, &m8789_MI);
		bool L_1 = m7795(L_0, &m7795_MI);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		t1425 * L_2 = m8789(NULL, &m8789_MI);
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7796_MI, L_2, (t7*) &_stringLiteral1589);
		V_0 = ((t1434 *)IsInst(L_3, InitializedTypeInfo(&t1434_TI)));
		if (V_0)
		{
			goto IL_0026;
		}
	}
	{
		return;
	}

IL_0026:
	{
		VirtActionInvoker1< bool >::Invoke(&m7807_MI, V_0, 0);
		return;
	}
}
extern MethodInfo m7812_MI;
 void m7812 (t29 * __this, MethodInfo* method){
	t1434 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		t1425 * L_0 = m8789(NULL, &m8789_MI);
		bool L_1 = m7795(L_0, &m7795_MI);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		t1425 * L_2 = m8789(NULL, &m8789_MI);
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7796_MI, L_2, (t7*) &_stringLiteral1589);
		V_0 = ((t1434 *)IsInst(L_3, InitializedTypeInfo(&t1434_TI)));
		if (V_0)
		{
			goto IL_0026;
		}
	}
	{
		return;
	}

IL_0026:
	{
		VirtActionInvoker1< bool >::Invoke(&m7807_MI, V_0, 1);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern Il2CppType t40_0_0_1;
FieldInfo t1434_f1_FieldInfo = 
{
	"_bReEntrant", &t40_0_0_1, &t1434_TI, offsetof(t1434, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1434_f2_FieldInfo = 
{
	"_flavor", &t44_0_0_1, &t1434_TI, offsetof(t1434, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_129;
FieldInfo t1434_f3_FieldInfo = 
{
	"_lockCount", &t44_0_0_129, &t1434_TI, offsetof(t1434, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1435_0_0_129;
FieldInfo t1434_f4_FieldInfo = 
{
	"_mutex", &t1435_0_0_129, &t1434_TI, offsetof(t1434, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1436_0_0_129;
FieldInfo t1434_f5_FieldInfo = 
{
	"_ownerThread", &t1436_0_0_129, &t1434_TI, offsetof(t1434, f5), &EmptyCustomAttributesCache};
static FieldInfo* t1434_FIs[] =
{
	&t1434_f1_FieldInfo,
	&t1434_f2_FieldInfo,
	&t1434_f3_FieldInfo,
	&t1434_f4_FieldInfo,
	&t1434_f5_FieldInfo,
	NULL
};
static PropertyInfo t1434____Locked_PropertyInfo = 
{
	&t1434_TI, "Locked", NULL, &m7807_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1434_PIs[] =
{
	&t1434____Locked_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7805_MI = 
{
	".ctor", (methodPointerType)&m7805, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3375, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1434_m7806_ParameterInfos[] = 
{
	{"flag", 0, 134221860, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"reEntrant", 1, 134221861, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7806_MI = 
{
	".ctor", (methodPointerType)&m7806, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t1434_m7806_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3376, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1434_m7807_ParameterInfos[] = 
{
	{"value", 0, 134221862, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7807_MI = 
{
	"set_Locked", (methodPointerType)&m7807, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1434_m7807_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 10, 1, false, false, 3377, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7808_MI = 
{
	"ReleaseLock", (methodPointerType)&m7808, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 3378, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1434_m7809_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221863, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1434__CustomAttributeCache_m7809;
MethodInfo m7809_MI = 
{
	"GetPropertiesForNewContext", (methodPointerType)&m7809, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1434_m7809_ParameterInfos, &t1434__CustomAttributeCache_m7809, 198, 0, 8, 1, false, false, 3379, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1425_0_0_0;
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1434_m7810_ParameterInfos[] = 
{
	{"ctx", 0, 134221864, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"msg", 1, 134221865, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1434__CustomAttributeCache_m7810;
MethodInfo m7810_MI = 
{
	"IsContextOK", (methodPointerType)&m7810, &t1434_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1434_m7810_ParameterInfos, &t1434__CustomAttributeCache_m7810, 198, 0, 9, 2, false, false, 3380, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7811_MI = 
{
	"ExitContext", (methodPointerType)&m7811, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 147, 0, 255, 0, false, false, 3381, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7812_MI = 
{
	"EnterContext", (methodPointerType)&m7812, &t1434_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 147, 0, 255, 0, false, false, 3382, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1434_MIs[] =
{
	&m7805_MI,
	&m7806_MI,
	&m7807_MI,
	&m7808_MI,
	&m7809_MI,
	&m7810_MI,
	&m7811_MI,
	&m7812_MI,
	NULL
};
static MethodInfo* t1434_VT[] =
{
	&m7800_MI,
	&m46_MI,
	&m7801_MI,
	&m1332_MI,
	&m7809_MI,
	&m7810_MI,
	&m7799_MI,
	&m7799_MI,
	&m7809_MI,
	&m7810_MI,
	&m7807_MI,
};
static TypeInfo* t1434_ITIs[] = 
{
	&t2044_TI,
	&t2045_TI,
};
static Il2CppInterfaceOffsetPair t1434_IOs[] = 
{
	{ &t2040_TI, 4},
	{ &t1433_TI, 6},
	{ &t604_TI, 4},
	{ &t2044_TI, 10},
	{ &t2045_TI, 10},
};
void t1434_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t1434_CustomAttributesCacheGenerator_m7809(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1434_CustomAttributesCacheGenerator_m7810(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1434__CustomAttributeCache = {
2,
NULL,
&t1434_CustomAttributesCacheGenerator
};
CustomAttributesCache t1434__CustomAttributeCache_m7809 = {
1,
NULL,
&t1434_CustomAttributesCacheGenerator_m7809
};
CustomAttributesCache t1434__CustomAttributeCache_m7810 = {
1,
NULL,
&t1434_CustomAttributesCacheGenerator_m7810
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1434_0_0_0;
extern Il2CppType t1434_1_0_0;
struct t1434;
extern CustomAttributesCache t1434__CustomAttributeCache;
extern CustomAttributesCache t1434__CustomAttributeCache_m7809;
extern CustomAttributesCache t1434__CustomAttributeCache_m7810;
TypeInfo t1434_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SynchronizationAttribute", "System.Runtime.Remoting.Contexts", t1434_MIs, t1434_PIs, t1434_FIs, NULL, &t1423_TI, NULL, NULL, &t1434_TI, t1434_ITIs, t1434_VT, &t1434__CustomAttributeCache, &t1434_TI, &t1434_0_0_0, &t1434_1_0_0, t1434_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1434), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 8, 1, 5, 0, 0, 11, 2, 5};
#include "t1437.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1437_TI;
#include "t1437MD.h"



// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern Il2CppType t348_0_0_1542;
FieldInfo t1437_f1_FieldInfo = 
{
	"value__", &t348_0_0_1542, &t1437_TI, offsetof(t1437, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1437_0_0_32854;
FieldInfo t1437_f2_FieldInfo = 
{
	"In", &t1437_0_0_32854, &t1437_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1437_0_0_32854;
FieldInfo t1437_f3_FieldInfo = 
{
	"Out", &t1437_0_0_32854, &t1437_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1437_FIs[] =
{
	&t1437_f1_FieldInfo,
	&t1437_f2_FieldInfo,
	&t1437_f3_FieldInfo,
	NULL
};
static const uint8_t t1437_f2_DefaultValueData = 0;
extern Il2CppType t348_0_0_0;
static Il2CppFieldDefaultValueEntry t1437_f2_DefaultValue = 
{
	&t1437_f2_FieldInfo, { (char*)&t1437_f2_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1437_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1437_f3_DefaultValue = 
{
	&t1437_f3_FieldInfo, { (char*)&t1437_f3_DefaultValueData, &t348_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1437_FDVs[] = 
{
	&t1437_f2_DefaultValue,
	&t1437_f3_DefaultValue,
	NULL
};
static MethodInfo* t1437_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t1437_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t1437_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1437_0_0_0;
extern Il2CppType t1437_1_0_0;
extern TypeInfo t49_TI;
#include "t348.h"
extern TypeInfo t348_TI;
TypeInfo t1437_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ArgInfoType", "System.Runtime.Remoting.Messaging", t1437_MIs, NULL, t1437_FIs, NULL, &t49_TI, NULL, NULL, &t348_TI, NULL, t1437_VT, &EmptyCustomAttributesCache, &t348_TI, &t1437_0_0_0, &t1437_1_0_0, t1437_IOs, NULL, NULL, t1437_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1437)+ sizeof (Il2CppObject), sizeof (uint8_t), sizeof(uint8_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t1438.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1438_TI;
#include "t1438MD.h"

#include "t636.h"
#include "t637.h"
extern TypeInfo t636_TI;
extern TypeInfo t638_TI;
extern TypeInfo t637_TI;
extern TypeInfo t841_TI;
#include "t636MD.h"
#include "t637MD.h"
extern MethodInfo m2939_MI;
extern MethodInfo m2940_MI;
extern MethodInfo m5994_MI;
extern MethodInfo m7701_MI;


extern MethodInfo m7813_MI;
 void m7813 (t1438 * __this, t636 * p0, uint8_t p1, MethodInfo* method){
	t638* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		m1331(__this, &m1331_MI);
		__this->f2 = p0;
		t636 * L_0 = (__this->f2);
		t638* L_1 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, L_0);
		V_0 = L_1;
		__this->f0 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), (((int32_t)(((t20 *)V_0)->max_length)))));
		__this->f1 = 0;
		if (p1)
		{
			goto IL_0069;
		}
	}
	{
		V_1 = 0;
		goto IL_0061;
	}

IL_0035:
	{
		int32_t L_2 = V_1;
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_2)));
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, L_3);
		if (L_4)
		{
			goto IL_005d;
		}
	}
	{
		t841* L_5 = (__this->f0);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_3 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_5, V_3)) = (int32_t)V_1;
	}

IL_005d:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0061:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0035;
		}
	}
	{
		goto IL_00a9;
	}

IL_0069:
	{
		V_2 = 0;
		goto IL_00a3;
	}

IL_006d:
	{
		int32_t L_8 = V_2;
		t42 * L_9 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_8)));
		bool L_10 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, L_9);
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		int32_t L_11 = V_2;
		bool L_12 = m7701((*(t637 **)(t637 **)SZArrayLdElema(V_0, L_11)), &m7701_MI);
		if (!L_12)
		{
			goto IL_009f;
		}
	}

IL_0086:
	{
		t841* L_13 = (__this->f0);
		int32_t L_14 = (__this->f1);
		int32_t L_15 = L_14;
		V_3 = L_15;
		__this->f1 = ((int32_t)(L_15+1));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_13, V_3)) = (int32_t)V_2;
	}

IL_009f:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_00a3:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_006d;
		}
	}

IL_00a9:
	{
		return;
	}
}
extern MethodInfo m7814_MI;
 t316* m7814 (t1438 * __this, t316* p0, MethodInfo* method){
	t316* V_0 = {0};
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->f1);
		V_0 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), L_0));
		V_1 = 0;
		goto IL_0021;
	}

IL_0010:
	{
		t841* L_1 = (__this->f0);
		int32_t L_2 = V_1;
		int32_t L_3 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2));
		ArrayElementTypeCheck (V_0, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_3)));
		*((t29 **)(t29 **)SZArrayLdElema(V_0, V_1)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema(p0, L_3));
		V_1 = ((int32_t)(V_1+1));
	}

IL_0021:
	{
		int32_t L_4 = (__this->f1);
		if ((((int32_t)V_1) < ((int32_t)L_4)))
		{
			goto IL_0010;
		}
	}
	{
		return V_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern Il2CppType t841_0_0_1;
FieldInfo t1438_f0_FieldInfo = 
{
	"_paramMap", &t841_0_0_1, &t1438_TI, offsetof(t1438, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1438_f1_FieldInfo = 
{
	"_inoutArgCount", &t44_0_0_1, &t1438_TI, offsetof(t1438, f1), &EmptyCustomAttributesCache};
extern Il2CppType t636_0_0_1;
FieldInfo t1438_f2_FieldInfo = 
{
	"_method", &t636_0_0_1, &t1438_TI, offsetof(t1438, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1438_FIs[] =
{
	&t1438_f0_FieldInfo,
	&t1438_f1_FieldInfo,
	&t1438_f2_FieldInfo,
	NULL
};
extern Il2CppType t636_0_0_0;
extern Il2CppType t636_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t1438_m7813_ParameterInfos[] = 
{
	{"method", 0, 134221866, &EmptyCustomAttributesCache, &t636_0_0_0},
	{"type", 1, 134221867, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m7813_MI = 
{
	".ctor", (methodPointerType)&m7813, &t1438_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t348, t1438_m7813_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3383, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1438_m7814_ParameterInfos[] = 
{
	{"args", 0, 134221868, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7814_MI = 
{
	"GetInOutArgs", (methodPointerType)&m7814, &t1438_TI, &t316_0_0_0, RuntimeInvoker_t29_t29, t1438_m7814_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 3384, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1438_MIs[] =
{
	&m7813_MI,
	&m7814_MI,
	NULL
};
static MethodInfo* t1438_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1438_0_0_0;
extern Il2CppType t1438_1_0_0;
struct t1438;
TypeInfo t1438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ArgInfo", "System.Runtime.Remoting.Messaging", t1438_MIs, NULL, t1438_FIs, NULL, &t29_TI, NULL, NULL, &t1438_TI, NULL, t1438_VT, &EmptyCustomAttributesCache, &t1438_TI, &t1438_0_0_0, &t1438_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1438), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 3, 0, 0, 4, 0, 0};
#include "t1439.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1439_TI;
#include "t1439MD.h"

#include "t1049.h"
#include "t1441.h"
#include "t345.h"
#include "t67.h"
extern TypeInfo t1049_TI;
extern TypeInfo t345_TI;
extern TypeInfo t67_TI;
extern TypeInfo t66_TI;
#include "t1049MD.h"
#include "t345MD.h"
#include "t1121MD.h"
#include "t67MD.h"
extern MethodInfo m5227_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m7817_MI;
extern MethodInfo m5228_MI;
extern MethodInfo m6071_MI;


extern MethodInfo m7815_MI;
 void m7815 (t1439 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7816_MI;
 t29 * m7816 (t1439 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
 t1050 * m7817 (t1439 * __this, MethodInfo* method){
	t1439 * V_0 = {0};
	t1050 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		V_0 = __this;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			t1050 * L_0 = (__this->f1);
			if (L_0)
			{
				goto IL_0021;
			}
		}

IL_0010:
		{
			bool L_1 = (__this->f6);
			t1049 * L_2 = (t1049 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1049_TI));
			m5227(L_2, L_1, &m5227_MI);
			__this->f1 = L_2;
		}

IL_0021:
		{
			t1050 * L_3 = (__this->f1);
			V_1 = L_3;
			// IL_0028: leave.s IL_0033
			leaveInstructions[0] = 0x33; // 1
			THROW_SENTINEL(IL_0033);
			// finally target depth: 1
		}

IL_002a:
		{
			// IL_002a: leave.s IL_0033
			leaveInstructions[0] = 0x33; // 1
			THROW_SENTINEL(IL_0033);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_002c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_002c;
	}

IL_002c:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x33:
				goto IL_0033;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0033:
	{
		return V_1;
	}
}
extern MethodInfo m7818_MI;
 bool m7818 (t1439 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m7819_MI;
 bool m7819 (t1439 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m7820_MI;
 bool m7820 (t1439 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m7821_MI;
 void m7821 (t1439 * __this, bool p0, MethodInfo* method){
	{
		__this->f7 = p0;
		return;
	}
}
extern MethodInfo m7822_MI;
 t29 * m7822 (t1439 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m7823_MI;
 t29 * m7823 (t1439 * __this, MethodInfo* method){
	{
		return (t29 *)NULL;
	}
}
extern MethodInfo m7824_MI;
 t29 * m7824 (t1439 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m7825_MI;
 t29 * m7825 (t1439 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f14);
		return L_0;
	}
}
extern MethodInfo m7826_MI;
 void m7826 (t1439 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m7827_MI;
 void m7827 (t1439 * __this, bool p0, MethodInfo* method){
	{
		__this->f5 = p0;
		return;
	}
}
extern MethodInfo m7828_MI;
 t29 * m7828 (t1439 * __this, MethodInfo* method){
	t1439 * V_0 = {0};
	t29 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		V_0 = __this;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (__this->f6);
			if (!L_0)
			{
				goto IL_0019;
			}
		}

IL_0010:
		{
			t29 * L_1 = (__this->f14);
			V_1 = L_1;
			// IL_0017: leave.s IL_0035
			leaveInstructions[0] = 0x35; // 1
			THROW_SENTINEL(IL_0035);
			// finally target depth: 1
		}

IL_0019:
		{
			// IL_0019: leave.s IL_0022
			leaveInstructions[0] = 0x22; // 1
			THROW_SENTINEL(IL_0022);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_001b;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_001b;
	}

IL_001b:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x35:
				goto IL_0035;
			case 0x22:
				goto IL_0022;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0022:
	{
		t1050 * L_2 = (t1050 *)VirtFuncInvoker0< t1050 * >::Invoke(&m7817_MI, __this);
		VirtFuncInvoker0< bool >::Invoke(&m5238_MI, L_2);
		t29 * L_3 = (__this->f14);
		return L_3;
	}

IL_0035:
	{
		return V_1;
	}
}
extern MethodInfo m7829_MI;
 t29 * m7829 (t1439 * __this, t29 * p0, MethodInfo* method){
	t1439 * V_0 = {0};
	t67 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		__this->f14 = p0;
		V_0 = __this;
		m4000(NULL, V_0, &m4000_MI);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			__this->f6 = 1;
			t1050 * L_0 = (__this->f1);
			if (!L_0)
			{
				goto IL_002f;
			}
		}

IL_001e:
		{
			t1050 * L_1 = (t1050 *)VirtFuncInvoker0< t1050 * >::Invoke(&m7817_MI, __this);
			m5228(((t1049 *)Castclass(L_1, InitializedTypeInfo(&t1049_TI))), &m5228_MI);
		}

IL_002f:
		{
			// IL_002f: leave.s IL_0038
			leaveInstructions[0] = 0x38; // 1
			THROW_SENTINEL(IL_0038);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0031;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0031;
	}

IL_0031:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x38:
				goto IL_0038;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0038:
	{
		t29 * L_2 = (__this->f8);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		t29 * L_3 = (__this->f8);
		V_1 = ((t67 *)Castclass(L_3, InitializedTypeInfo(&t67_TI)));
		VirtActionInvoker1< t29 * >::Invoke(&m6071_MI, V_1, __this);
	}

IL_0053:
	{
		return (t29 *)NULL;
	}
}
extern MethodInfo m7830_MI;
 t1441 * m7830 (t1439 * __this, MethodInfo* method){
	{
		t1441 * L_0 = (__this->f12);
		return L_0;
	}
}
extern MethodInfo m7831_MI;
 void m7831 (t1439 * __this, t1441 * p0, MethodInfo* method){
	{
		__this->f12 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern Il2CppType t29_0_0_1;
FieldInfo t1439_f0_FieldInfo = 
{
	"async_state", &t29_0_0_1, &t1439_TI, offsetof(t1439, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1050_0_0_1;
FieldInfo t1439_f1_FieldInfo = 
{
	"handle", &t1050_0_0_1, &t1439_TI, offsetof(t1439, f1), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1439_f2_FieldInfo = 
{
	"async_delegate", &t29_0_0_1, &t1439_TI, offsetof(t1439, f2), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_1;
FieldInfo t1439_f3_FieldInfo = 
{
	"data", &t35_0_0_1, &t1439_TI, offsetof(t1439, f3), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1439_f4_FieldInfo = 
{
	"object_data", &t29_0_0_1, &t1439_TI, offsetof(t1439, f4), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1439_f5_FieldInfo = 
{
	"sync_completed", &t40_0_0_1, &t1439_TI, offsetof(t1439, f5), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1439_f6_FieldInfo = 
{
	"completed", &t40_0_0_1, &t1439_TI, offsetof(t1439, f6), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1439_f7_FieldInfo = 
{
	"endinvoke_called", &t40_0_0_1, &t1439_TI, offsetof(t1439, f7), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1439_f8_FieldInfo = 
{
	"async_callback", &t29_0_0_1, &t1439_TI, offsetof(t1439, f8), &EmptyCustomAttributesCache};
extern Il2CppType t1440_0_0_1;
FieldInfo t1439_f9_FieldInfo = 
{
	"current", &t1440_0_0_1, &t1439_TI, offsetof(t1439, f9), &EmptyCustomAttributesCache};
extern Il2CppType t1440_0_0_1;
FieldInfo t1439_f10_FieldInfo = 
{
	"original", &t1440_0_0_1, &t1439_TI, offsetof(t1439, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1439_f11_FieldInfo = 
{
	"gchandle", &t44_0_0_1, &t1439_TI, offsetof(t1439, f11), &EmptyCustomAttributesCache};
extern Il2CppType t1441_0_0_1;
FieldInfo t1439_f12_FieldInfo = 
{
	"call_message", &t1441_0_0_1, &t1439_TI, offsetof(t1439, f12), &EmptyCustomAttributesCache};
extern Il2CppType t1442_0_0_1;
FieldInfo t1439_f13_FieldInfo = 
{
	"message_ctrl", &t1442_0_0_1, &t1439_TI, offsetof(t1439, f13), &EmptyCustomAttributesCache};
extern Il2CppType t1443_0_0_1;
FieldInfo t1439_f14_FieldInfo = 
{
	"reply_message", &t1443_0_0_1, &t1439_TI, offsetof(t1439, f14), &EmptyCustomAttributesCache};
static FieldInfo* t1439_FIs[] =
{
	&t1439_f0_FieldInfo,
	&t1439_f1_FieldInfo,
	&t1439_f2_FieldInfo,
	&t1439_f3_FieldInfo,
	&t1439_f4_FieldInfo,
	&t1439_f5_FieldInfo,
	&t1439_f6_FieldInfo,
	&t1439_f7_FieldInfo,
	&t1439_f8_FieldInfo,
	&t1439_f9_FieldInfo,
	&t1439_f10_FieldInfo,
	&t1439_f11_FieldInfo,
	&t1439_f12_FieldInfo,
	&t1439_f13_FieldInfo,
	&t1439_f14_FieldInfo,
	NULL
};
static PropertyInfo t1439____AsyncState_PropertyInfo = 
{
	&t1439_TI, "AsyncState", &m7816_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____AsyncWaitHandle_PropertyInfo = 
{
	&t1439_TI, "AsyncWaitHandle", &m7817_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____CompletedSynchronously_PropertyInfo = 
{
	&t1439_TI, "CompletedSynchronously", &m7818_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____IsCompleted_PropertyInfo = 
{
	&t1439_TI, "IsCompleted", &m7819_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____EndInvokeCalled_PropertyInfo = 
{
	&t1439_TI, "EndInvokeCalled", &m7820_MI, &m7821_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____AsyncDelegate_PropertyInfo = 
{
	&t1439_TI, "AsyncDelegate", &m7822_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____NextSink_PropertyInfo = 
{
	&t1439_TI, "NextSink", &m7823_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1439____CallMessage_PropertyInfo = 
{
	&t1439_TI, "CallMessage", &m7830_MI, &m7831_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1439_PIs[] =
{
	&t1439____AsyncState_PropertyInfo,
	&t1439____AsyncWaitHandle_PropertyInfo,
	&t1439____CompletedSynchronously_PropertyInfo,
	&t1439____IsCompleted_PropertyInfo,
	&t1439____EndInvokeCalled_PropertyInfo,
	&t1439____AsyncDelegate_PropertyInfo,
	&t1439____NextSink_PropertyInfo,
	&t1439____CallMessage_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7815_MI = 
{
	".ctor", (methodPointerType)&m7815, &t1439_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3385, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7816_MI = 
{
	"get_AsyncState", (methodPointerType)&m7816, &t1439_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 7, 0, false, false, 3386, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1050_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7817_MI = 
{
	"get_AsyncWaitHandle", (methodPointerType)&m7817, &t1439_TI, &t1050_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 8, 0, false, false, 3387, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7818_MI = 
{
	"get_CompletedSynchronously", (methodPointerType)&m7818, &t1439_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 9, 0, false, false, 3388, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7819_MI = 
{
	"get_IsCompleted", (methodPointerType)&m7819, &t1439_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 10, 0, false, false, 3389, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7820_MI = 
{
	"get_EndInvokeCalled", (methodPointerType)&m7820, &t1439_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3390, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1439_m7821_ParameterInfos[] = 
{
	{"value", 0, 134221869, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7821_MI = 
{
	"set_EndInvokeCalled", (methodPointerType)&m7821, &t1439_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1439_m7821_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 3391, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7822_MI = 
{
	"get_AsyncDelegate", (methodPointerType)&m7822, &t1439_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 11, 0, false, false, 3392, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t967_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7823_MI = 
{
	"get_NextSink", (methodPointerType)&m7823, &t1439_TI, &t967_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 12, 0, false, false, 3393, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1443_0_0_0;
extern Il2CppType t1443_0_0_0;
extern Il2CppType t967_0_0_0;
extern Il2CppType t967_0_0_0;
static ParameterInfo t1439_m7824_ParameterInfos[] = 
{
	{"msg", 0, 134221870, &EmptyCustomAttributesCache, &t1443_0_0_0},
	{"replySink", 1, 134221871, &EmptyCustomAttributesCache, &t967_0_0_0},
};
extern Il2CppType t1442_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7824_MI = 
{
	"AsyncProcessMessage", (methodPointerType)&m7824, &t1439_TI, &t1442_0_0_0, RuntimeInvoker_t29_t29_t29, t1439_m7824_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 13, 2, false, false, 3394, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1443_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7825_MI = 
{
	"GetReplyMessage", (methodPointerType)&m7825, &t1439_TI, &t1443_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 14, 0, false, false, 3395, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1442_0_0_0;
extern Il2CppType t1442_0_0_0;
static ParameterInfo t1439_m7826_ParameterInfos[] = 
{
	{"mc", 0, 134221872, &EmptyCustomAttributesCache, &t1442_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7826_MI = 
{
	"SetMessageCtrl", (methodPointerType)&m7826, &t1439_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1439_m7826_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 15, 1, false, false, 3396, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1439_m7827_ParameterInfos[] = 
{
	{"completed", 0, 134221873, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7827_MI = 
{
	"SetCompletedSynchronously", (methodPointerType)&m7827, &t1439_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1439_m7827_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 3397, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1443_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7828_MI = 
{
	"EndInvoke", (methodPointerType)&m7828, &t1439_TI, &t1443_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 3398, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1443_0_0_0;
static ParameterInfo t1439_m7829_ParameterInfos[] = 
{
	{"msg", 0, 134221874, &EmptyCustomAttributesCache, &t1443_0_0_0},
};
extern Il2CppType t1443_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7829_MI = 
{
	"SyncProcessMessage", (methodPointerType)&m7829, &t1439_TI, &t1443_0_0_0, RuntimeInvoker_t29_t29, t1439_m7829_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 16, 1, false, false, 3399, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1441_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7830_MI = 
{
	"get_CallMessage", (methodPointerType)&m7830, &t1439_TI, &t1441_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 3400, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1441_0_0_0;
extern Il2CppType t1441_0_0_0;
static ParameterInfo t1439_m7831_ParameterInfos[] = 
{
	{"value", 0, 134221875, &EmptyCustomAttributesCache, &t1441_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7831_MI = 
{
	"set_CallMessage", (methodPointerType)&m7831, &t1439_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1439_m7831_ParameterInfos, &EmptyCustomAttributesCache, 2179, 0, 255, 1, false, false, 3401, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1439_MIs[] =
{
	&m7815_MI,
	&m7816_MI,
	&m7817_MI,
	&m7818_MI,
	&m7819_MI,
	&m7820_MI,
	&m7821_MI,
	&m7822_MI,
	&m7823_MI,
	&m7824_MI,
	&m7825_MI,
	&m7826_MI,
	&m7827_MI,
	&m7828_MI,
	&m7829_MI,
	&m7830_MI,
	&m7831_MI,
	NULL
};
static MethodInfo* t1439_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7816_MI,
	&m7817_MI,
	&m7819_MI,
	&m7816_MI,
	&m7817_MI,
	&m7818_MI,
	&m7819_MI,
	&m7822_MI,
	&m7823_MI,
	&m7824_MI,
	&m7825_MI,
	&m7826_MI,
	&m7829_MI,
};
static TypeInfo* t1439_ITIs[] = 
{
	&t66_TI,
	&t967_TI,
};
static Il2CppInterfaceOffsetPair t1439_IOs[] = 
{
	{ &t66_TI, 4},
	{ &t967_TI, 7},
};
void t1439_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1439__CustomAttributeCache = {
1,
NULL,
&t1439_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1439_0_0_0;
extern Il2CppType t1439_1_0_0;
struct t1439;
extern CustomAttributesCache t1439__CustomAttributeCache;
TypeInfo t1439_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AsyncResult", "System.Runtime.Remoting.Messaging", t1439_MIs, t1439_PIs, t1439_FIs, NULL, &t29_TI, NULL, NULL, &t1439_TI, t1439_ITIs, t1439_VT, &t1439__CustomAttributeCache, &t1439_TI, &t1439_0_0_0, &t1439_1_0_0, t1439_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1439), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 17, 8, 15, 0, 0, 17, 2, 2};
#include "t1417.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1417_TI;
#include "t1417MD.h"

#include "t1444.h"
#include "t733.h"
#include "t735.h"
#include "t1445.h"
#include "t485.h"
extern TypeInfo t1444_TI;
extern TypeInfo t721_TI;
extern TypeInfo t1445_TI;
extern TypeInfo t485_TI;
extern TypeInfo t733_TI;
extern TypeInfo t735_TI;
extern TypeInfo t674_TI;
#include "t1444MD.h"
#include "t1445MD.h"
#include "t1446MD.h"
#include "t485MD.h"
#include "t733MD.h"
extern MethodInfo m6013_MI;
extern MethodInfo m7871_MI;
extern MethodInfo m7862_MI;
extern MethodInfo m2996_MI;
extern MethodInfo m7861_MI;
extern MethodInfo m7846_MI;
extern MethodInfo m7893_MI;
extern MethodInfo m4038_MI;
extern MethodInfo m4039_MI;
extern MethodInfo m4040_MI;
extern MethodInfo m7864_MI;
extern MethodInfo m7865_MI;
extern MethodInfo m3953_MI;
extern MethodInfo m3997_MI;


extern MethodInfo m7832_MI;
 void m7832 (t1417 * __this, t42 * p0, MethodInfo* method){
	{
		m7862(__this, &m7862_MI);
		__this->f14 = p0;
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, p0);
		__this->f15 = L_0;
		__this->f16 = 1;
		return;
	}
}
extern MethodInfo m7833_MI;
 void m7833 (t1417 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m7861(__this, p0, p1, &m7861_MI);
		return;
	}
}
extern MethodInfo m7834_MI;
 void m7834 (t1417 * __this, MethodInfo* method){
	t1445 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		t1445 * L_0 = (t1445 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1445_TI));
		m7846(L_0, __this, &m7846_MI);
		V_0 = L_0;
		__this->f8 = V_0;
		t29 * L_1 = m7893(V_0, &m7893_MI);
		__this->f9 = L_1;
		return;
	}
}
extern MethodInfo m7835_MI;
 void m7835 (t1417 * __this, bool p0, MethodInfo* method){
	{
		__this->f16 = p0;
		return;
	}
}
extern MethodInfo m7836_MI;
 t42 * m7836 (t1417 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f14);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = (__this->f15);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m6013(NULL, L_1, &m6013_MI);
		__this->f14 = L_2;
	}

IL_0019:
	{
		t42 * L_3 = (__this->f14);
		return L_3;
	}
}
extern MethodInfo m7837_MI;
 t7* m7837 (t1417 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f15);
		return L_0;
	}
}
extern MethodInfo m7838_MI;
 t29 * m7838 (t1417 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f11);
		return L_0;
	}
}
extern MethodInfo m7839_MI;
 void m7839 (t1417 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f11 = p0;
		return;
	}
}
extern MethodInfo m7840_MI;
 t316* m7840 (t1417 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f12);
		return L_0;
	}
}
extern MethodInfo m7841_MI;
 void m7841 (t1417 * __this, t316* p0, MethodInfo* method){
	{
		__this->f12 = p0;
		return;
	}
}
extern MethodInfo m7842_MI;
 t29 * m7842 (t1417 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_1 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_1, &m3980_MI);
		__this->f13 = L_1;
	}

IL_0013:
	{
		t29 * L_2 = (__this->f13);
		return L_2;
	}
}
extern MethodInfo m7843_MI;
 void m7843 (t1417 * __this, t7* p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_00c4;
		}
	}
	{
		if ((((t1417_SFs*)InitializedTypeInfo(&t1417_TI)->static_fields)->f17))
		{
			goto IL_0058;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 5, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1590, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1591, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1592, 2);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1593, 3);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1594, 4);
		((t1417_SFs*)InitializedTypeInfo(&t1417_TI)->static_fields)->f17 = V_1;
	}

IL_0058:
	{
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1417_SFs*)InitializedTypeInfo(&t1417_TI)->static_fields)->f17), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_00c4;
		}
	}
	{
		if (V_2 == 0)
		{
			goto IL_0083;
		}
		if (V_2 == 1)
		{
			goto IL_0090;
		}
		if (V_2 == 2)
		{
			goto IL_009d;
		}
		if (V_2 == 3)
		{
			goto IL_00aa;
		}
		if (V_2 == 4)
		{
			goto IL_00b7;
		}
	}
	{
		goto IL_00c4;
	}

IL_0083:
	{
		__this->f11 = ((t29 *)Castclass(p1, InitializedTypeInfo(&t1416_TI)));
		return;
	}

IL_0090:
	{
		__this->f12 = ((t316*)Castclass(p1, InitializedTypeInfo(&t316_TI)));
		return;
	}

IL_009d:
	{
		__this->f14 = ((t42 *)Castclass(p1, InitializedTypeInfo(&t42_TI)));
		return;
	}

IL_00aa:
	{
		__this->f13 = ((t29 *)Castclass(p1, InitializedTypeInfo(&t868_TI)));
		return;
	}

IL_00b7:
	{
		__this->f15 = ((t7*)Castclass(p1, (&t7_TI)));
		return;
	}

IL_00c4:
	{
		m7864(__this, p0, p1, &m7864_MI);
		return;
	}
}
extern MethodInfo m7844_MI;
 void m7844 (t1417 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t29 * V_0 = {0};
	{
		m7865(__this, p0, p1, &m7865_MI);
		t29 * L_0 = (__this->f13);
		V_0 = L_0;
		if (!V_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m3953_MI, V_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		V_0 = (t29 *)NULL;
	}

IL_001c:
	{
		t29 * L_2 = (__this->f11);
		m3997(p0, (t7*) &_stringLiteral1590, L_2, &m3997_MI);
		t316* L_3 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral1591, (t29 *)(t29 *)L_3, &m3997_MI);
		m3997(p0, (t7*) &_stringLiteral1592, NULL, &m3997_MI);
		m3997(p0, (t7*) &_stringLiteral1593, V_0, &m3997_MI);
		t7* L_4 = (__this->f15);
		m3997(p0, (t7*) &_stringLiteral1594, L_4, &m3997_MI);
		return;
	}
}
extern MethodInfo m7845_MI;
 t29 * m7845 (t1417 * __this, MethodInfo* method){
	{
		t29 * L_0 = m7871(__this, &m7871_MI);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern Il2CppType t1416_0_0_1;
FieldInfo t1417_f11_FieldInfo = 
{
	"_activator", &t1416_0_0_1, &t1417_TI, offsetof(t1417, f11), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t1417_f12_FieldInfo = 
{
	"_activationAttributes", &t316_0_0_1, &t1417_TI, offsetof(t1417, f12), &EmptyCustomAttributesCache};
extern Il2CppType t868_0_0_1;
FieldInfo t1417_f13_FieldInfo = 
{
	"_contextProperties", &t868_0_0_1, &t1417_TI, offsetof(t1417, f13), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1417_f14_FieldInfo = 
{
	"_activationType", &t42_0_0_1, &t1417_TI, offsetof(t1417, f14), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1417_f15_FieldInfo = 
{
	"_activationTypeName", &t7_0_0_1, &t1417_TI, offsetof(t1417, f15), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1417_f16_FieldInfo = 
{
	"_isContextOk", &t40_0_0_1, &t1417_TI, offsetof(t1417, f16), &EmptyCustomAttributesCache};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1417__CustomAttributeCache_U3CU3Ef__switch$map20;
FieldInfo t1417_f17_FieldInfo = 
{
	"<>f__switch$map20", &t485_0_0_17, &t1417_TI, offsetof(t1417_SFs, f17), &t1417__CustomAttributeCache_U3CU3Ef__switch$map20};
static FieldInfo* t1417_FIs[] =
{
	&t1417_f11_FieldInfo,
	&t1417_f12_FieldInfo,
	&t1417_f13_FieldInfo,
	&t1417_f14_FieldInfo,
	&t1417_f15_FieldInfo,
	&t1417_f16_FieldInfo,
	&t1417_f17_FieldInfo,
	NULL
};
static PropertyInfo t1417____IsContextOk_PropertyInfo = 
{
	&t1417_TI, "IsContextOk", NULL, &m7835_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1417____ActivationType_PropertyInfo = 
{
	&t1417_TI, "ActivationType", &m7836_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1417____ActivationTypeName_PropertyInfo = 
{
	&t1417_TI, "ActivationTypeName", &m7837_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1417____Activator_PropertyInfo = 
{
	&t1417_TI, "Activator", &m7838_MI, &m7839_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1417____CallSiteActivationAttributes_PropertyInfo = 
{
	&t1417_TI, "CallSiteActivationAttributes", &m7840_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1417____ContextProperties_PropertyInfo = 
{
	&t1417_TI, "ContextProperties", &m7842_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1417____Properties_PropertyInfo = 
{
	&t1417_TI, "Properties", &m7845_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1417_PIs[] =
{
	&t1417____IsContextOk_PropertyInfo,
	&t1417____ActivationType_PropertyInfo,
	&t1417____ActivationTypeName_PropertyInfo,
	&t1417____Activator_PropertyInfo,
	&t1417____CallSiteActivationAttributes_PropertyInfo,
	&t1417____ContextProperties_PropertyInfo,
	&t1417____Properties_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1417_m7832_ParameterInfos[] = 
{
	{"type", 0, 134221876, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7832_MI = 
{
	".ctor", (methodPointerType)&m7832, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1417_m7832_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, false, 3402, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1417_m7833_ParameterInfos[] = 
{
	{"info", 0, 134221877, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221878, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7833_MI = 
{
	".ctor", (methodPointerType)&m7833, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1417_m7833_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3403, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7834_MI = 
{
	"InitDictionary", (methodPointerType)&m7834, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 195, 0, 16, 0, false, false, 3404, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1417_m7835_ParameterInfos[] = 
{
	{"value", 0, 134221879, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7835_MI = 
{
	"set_IsContextOk", (methodPointerType)&m7835, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1417_m7835_ParameterInfos, &EmptyCustomAttributesCache, 2179, 0, 255, 1, false, false, 3405, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7836_MI = 
{
	"get_ActivationType", (methodPointerType)&m7836, &t1417_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 19, 0, false, false, 3406, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7837_MI = 
{
	"get_ActivationTypeName", (methodPointerType)&m7837, &t1417_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, false, 3407, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1416_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7838_MI = 
{
	"get_Activator", (methodPointerType)&m7838, &t1417_TI, &t1416_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 21, 0, false, false, 3408, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1416_0_0_0;
static ParameterInfo t1417_m7839_ParameterInfos[] = 
{
	{"value", 0, 134221880, &EmptyCustomAttributesCache, &t1416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7839_MI = 
{
	"set_Activator", (methodPointerType)&m7839, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1417_m7839_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 22, 1, false, false, 3409, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7840_MI = 
{
	"get_CallSiteActivationAttributes", (methodPointerType)&m7840, &t1417_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 23, 0, false, false, 3410, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t1417_m7841_ParameterInfos[] = 
{
	{"attributes", 0, 134221881, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7841_MI = 
{
	"SetActivationAttributes", (methodPointerType)&m7841, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1417_m7841_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 3411, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t868_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7842_MI = 
{
	"get_ContextProperties", (methodPointerType)&m7842, &t1417_TI, &t868_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 24, 0, false, false, 3412, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1417_m7843_ParameterInfos[] = 
{
	{"key", 0, 134221882, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134221883, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7843_MI = 
{
	"InitMethodProperty", (methodPointerType)&m7843, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1417_m7843_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 13, 2, false, false, 3413, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1417_m7844_ParameterInfos[] = 
{
	{"info", 0, 134221884, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221885, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7844_MI = 
{
	"GetObjectData", (methodPointerType)&m7844, &t1417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1417_m7844_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 14, 2, false, false, 3414, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7845_MI = 
{
	"get_Properties", (methodPointerType)&m7845, &t1417_TI, &t721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 15, 0, false, false, 3415, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1417_MIs[] =
{
	&m7832_MI,
	&m7833_MI,
	&m7834_MI,
	&m7835_MI,
	&m7836_MI,
	&m7837_MI,
	&m7838_MI,
	&m7839_MI,
	&m7840_MI,
	&m7841_MI,
	&m7842_MI,
	&m7843_MI,
	&m7844_MI,
	&m7845_MI,
	NULL
};
extern MethodInfo m7863_MI;
extern MethodInfo m7866_MI;
extern MethodInfo m7867_MI;
extern MethodInfo m7868_MI;
extern MethodInfo m7869_MI;
extern MethodInfo m7870_MI;
extern MethodInfo m7873_MI;
extern MethodInfo m7874_MI;
extern MethodInfo m7875_MI;
extern MethodInfo m7876_MI;
static MethodInfo* t1417_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7844_MI,
	&m7863_MI,
	&m7866_MI,
	&m7867_MI,
	&m7868_MI,
	&m7869_MI,
	&m7870_MI,
	&m7873_MI,
	&m7874_MI,
	&m7843_MI,
	&m7844_MI,
	&m7845_MI,
	&m7834_MI,
	&m7875_MI,
	&m7876_MI,
	&m7836_MI,
	&m7837_MI,
	&m7838_MI,
	&m7839_MI,
	&m7840_MI,
	&m7842_MI,
};
static TypeInfo* t1417_ITIs[] = 
{
	&t1424_TI,
	&t1443_TI,
	&t1463_TI,
	&t1453_TI,
};
extern TypeInfo t374_TI;
extern TypeInfo t2046_TI;
extern TypeInfo t2047_TI;
static Il2CppInterfaceOffsetPair t1417_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t2046_TI, 5},
	{ &t1443_TI, 6},
	{ &t1463_TI, 6},
	{ &t1453_TI, 6},
	{ &t2047_TI, 13},
	{ &t1424_TI, 19},
};
extern TypeInfo t707_TI;
#include "t707.h"
#include "t707MD.h"
extern MethodInfo m3062_MI;
void t1417_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t1417_CustomAttributesCacheGenerator_U3CU3Ef__switch$map20(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1417__CustomAttributeCache = {
2,
NULL,
&t1417_CustomAttributesCacheGenerator
};
CustomAttributesCache t1417__CustomAttributeCache_U3CU3Ef__switch$map20 = {
1,
NULL,
&t1417_CustomAttributesCacheGenerator_U3CU3Ef__switch$map20
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1417_0_0_0;
extern Il2CppType t1417_1_0_0;
struct t1417;
extern CustomAttributesCache t1417__CustomAttributeCache;
extern CustomAttributesCache t1417__CustomAttributeCache_U3CU3Ef__switch$map20;
TypeInfo t1417_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConstructionCall", "System.Runtime.Remoting.Messaging", t1417_MIs, t1417_PIs, t1417_FIs, NULL, &t1444_TI, NULL, NULL, &t1417_TI, t1417_ITIs, t1417_VT, &t1417__CustomAttributeCache, &t1417_TI, &t1417_0_0_0, &t1417_1_0_0, t1417_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1417), 0, -1, sizeof(t1417_SFs), 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 14, 7, 7, 0, 0, 25, 4, 7};
#ifndef _MSC_VER
#else
#endif

#include "t1446.h"
extern TypeInfo t1446_TI;
extern MethodInfo m7889_MI;
extern MethodInfo m7891_MI;
extern MethodInfo m7897_MI;
extern MethodInfo m7898_MI;


 void m7846 (t1445 * __this, t29 * p0, MethodInfo* method){
	{
		m7889(__this, p0, &m7889_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		m7891(__this, (((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f6), &m7891_MI);
		return;
	}
}
extern MethodInfo m7847_MI;
 void m7847 (t29 * __this, MethodInfo* method){
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), ((int32_t)11)));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral1595);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral1595;
		t446* L_1 = L_0;
		ArrayElementTypeCheck (L_1, (t7*) &_stringLiteral1596);
		*((t7**)(t7**)SZArrayLdElema(L_1, 1)) = (t7*)(t7*) &_stringLiteral1596;
		t446* L_2 = L_1;
		ArrayElementTypeCheck (L_2, (t7*) &_stringLiteral1597);
		*((t7**)(t7**)SZArrayLdElema(L_2, 2)) = (t7*)(t7*) &_stringLiteral1597;
		t446* L_3 = L_2;
		ArrayElementTypeCheck (L_3, (t7*) &_stringLiteral1598);
		*((t7**)(t7**)SZArrayLdElema(L_3, 3)) = (t7*)(t7*) &_stringLiteral1598;
		t446* L_4 = L_3;
		ArrayElementTypeCheck (L_4, (t7*) &_stringLiteral1599);
		*((t7**)(t7**)SZArrayLdElema(L_4, 4)) = (t7*)(t7*) &_stringLiteral1599;
		t446* L_5 = L_4;
		ArrayElementTypeCheck (L_5, (t7*) &_stringLiteral1600);
		*((t7**)(t7**)SZArrayLdElema(L_5, 5)) = (t7*)(t7*) &_stringLiteral1600;
		t446* L_6 = L_5;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral1591);
		*((t7**)(t7**)SZArrayLdElema(L_6, 6)) = (t7*)(t7*) &_stringLiteral1591;
		t446* L_7 = L_6;
		ArrayElementTypeCheck (L_7, (t7*) &_stringLiteral1592);
		*((t7**)(t7**)SZArrayLdElema(L_7, 7)) = (t7*)(t7*) &_stringLiteral1592;
		t446* L_8 = L_7;
		ArrayElementTypeCheck (L_8, (t7*) &_stringLiteral1593);
		*((t7**)(t7**)SZArrayLdElema(L_8, 8)) = (t7*)(t7*) &_stringLiteral1593;
		t446* L_9 = L_8;
		ArrayElementTypeCheck (L_9, (t7*) &_stringLiteral1590);
		*((t7**)(t7**)SZArrayLdElema(L_9, ((int32_t)9))) = (t7*)(t7*) &_stringLiteral1590;
		t446* L_10 = L_9;
		ArrayElementTypeCheck (L_10, (t7*) &_stringLiteral1594);
		*((t7**)(t7**)SZArrayLdElema(L_10, ((int32_t)10))) = (t7*)(t7*) &_stringLiteral1594;
		((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f6 = L_10;
		return;
	}
}
extern MethodInfo m7848_MI;
 t29 * m7848 (t1445 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		if ((((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f7))
		{
			goto IL_0058;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 5, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1590, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1591, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1592, 2);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1593, 3);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1594, 4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f7 = V_1;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f7), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_00d8;
		}
	}
	{
		if (V_2 == 0)
		{
			goto IL_0083;
		}
		if (V_2 == 1)
		{
			goto IL_0094;
		}
		if (V_2 == 2)
		{
			goto IL_00a5;
		}
		if (V_2 == 3)
		{
			goto IL_00b6;
		}
		if (V_2 == 4)
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_00d8;
	}

IL_0083:
	{
		t29 * L_2 = (__this->f1);
		t29 * L_3 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10184_MI, ((t29 *)Castclass(L_2, InitializedTypeInfo(&t1424_TI))));
		return L_3;
	}

IL_0094:
	{
		t29 * L_4 = (__this->f1);
		t316* L_5 = (t316*)InterfaceFuncInvoker0< t316* >::Invoke(&m10186_MI, ((t29 *)Castclass(L_4, InitializedTypeInfo(&t1424_TI))));
		return (t29 *)L_5;
	}

IL_00a5:
	{
		t29 * L_6 = (__this->f1);
		t42 * L_7 = (t42 *)InterfaceFuncInvoker0< t42 * >::Invoke(&m10182_MI, ((t29 *)Castclass(L_6, InitializedTypeInfo(&t1424_TI))));
		return L_7;
	}

IL_00b6:
	{
		t29 * L_8 = (__this->f1);
		t29 * L_9 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10187_MI, ((t29 *)Castclass(L_8, InitializedTypeInfo(&t1424_TI))));
		return L_9;
	}

IL_00c7:
	{
		t29 * L_10 = (__this->f1);
		t7* L_11 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10183_MI, ((t29 *)Castclass(L_10, InitializedTypeInfo(&t1424_TI))));
		return L_11;
	}

IL_00d8:
	{
		t29 * L_12 = m7897(__this, p0, &m7897_MI);
		return L_12;
	}
}
extern MethodInfo m7849_MI;
 void m7849 (t1445 * __this, t7* p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_0093;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		if ((((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f8))
		{
			goto IL_0058;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 5, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1590, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1591, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1592, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1593, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1594, 1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f8 = V_1;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1445_TI));
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1445_SFs*)InitializedTypeInfo(&t1445_TI)->static_fields)->f8), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		if (!V_2)
		{
			goto IL_0070;
		}
	}
	{
		if ((((int32_t)V_2) == ((int32_t)1)))
		{
			goto IL_0088;
		}
	}
	{
		goto IL_0093;
	}

IL_0070:
	{
		t29 * L_2 = (__this->f1);
		InterfaceActionInvoker1< t29 * >::Invoke(&m10185_MI, ((t29 *)Castclass(L_2, InitializedTypeInfo(&t1424_TI))), ((t29 *)Castclass(p1, InitializedTypeInfo(&t1416_TI))));
		goto IL_009d;
	}

IL_0088:
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1601, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0093:
	{
		m7898(__this, p0, p1, &m7898_MI);
		goto IL_009d;
	}

IL_009d:
	{
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern Il2CppType t446_0_0_22;
FieldInfo t1445_f6_FieldInfo = 
{
	"InternalKeys", &t446_0_0_22, &t1445_TI, offsetof(t1445_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1445__CustomAttributeCache_U3CU3Ef__switch$map23;
FieldInfo t1445_f7_FieldInfo = 
{
	"<>f__switch$map23", &t485_0_0_17, &t1445_TI, offsetof(t1445_SFs, f7), &t1445__CustomAttributeCache_U3CU3Ef__switch$map23};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1445__CustomAttributeCache_U3CU3Ef__switch$map24;
FieldInfo t1445_f8_FieldInfo = 
{
	"<>f__switch$map24", &t485_0_0_17, &t1445_TI, offsetof(t1445_SFs, f8), &t1445__CustomAttributeCache_U3CU3Ef__switch$map24};
static FieldInfo* t1445_FIs[] =
{
	&t1445_f6_FieldInfo,
	&t1445_f7_FieldInfo,
	&t1445_f8_FieldInfo,
	NULL
};
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1445_m7846_ParameterInfos[] = 
{
	{"message", 0, 134221886, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7846_MI = 
{
	".ctor", (methodPointerType)&m7846, &t1445_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1445_m7846_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3416, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7847_MI = 
{
	".cctor", (methodPointerType)&m7847, &t1445_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3417, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1445_m7848_ParameterInfos[] = 
{
	{"key", 0, 134221887, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7848_MI = 
{
	"GetMethodProperty", (methodPointerType)&m7848, &t1445_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1445_m7848_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 15, 1, false, false, 3418, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1445_m7849_ParameterInfos[] = 
{
	{"key", 0, 134221888, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134221889, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7849_MI = 
{
	"SetMethodProperty", (methodPointerType)&m7849, &t1445_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1445_m7849_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 16, 2, false, false, 3419, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1445_MIs[] =
{
	&m7846_MI,
	&m7847_MI,
	&m7848_MI,
	&m7849_MI,
	NULL
};
extern MethodInfo m7890_MI;
extern MethodInfo m7902_MI;
extern MethodInfo m7903_MI;
extern MethodInfo m7904_MI;
extern MethodInfo m7905_MI;
extern MethodInfo m7895_MI;
extern MethodInfo m7896_MI;
extern MethodInfo m7900_MI;
extern MethodInfo m7906_MI;
extern MethodInfo m7901_MI;
extern MethodInfo m7892_MI;
extern MethodInfo m7899_MI;
static MethodInfo* t1445_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7890_MI,
	&m7902_MI,
	&m7903_MI,
	&m7904_MI,
	&m7905_MI,
	&m7895_MI,
	&m7896_MI,
	&m7900_MI,
	&m7906_MI,
	&m7901_MI,
	&m7892_MI,
	&m7848_MI,
	&m7849_MI,
	&m7899_MI,
};
extern TypeInfo t603_TI;
static Il2CppInterfaceOffsetPair t1445_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t721_TI, 9},
};
void t1445_CustomAttributesCacheGenerator_U3CU3Ef__switch$map23(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1445_CustomAttributesCacheGenerator_U3CU3Ef__switch$map24(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1445__CustomAttributeCache_U3CU3Ef__switch$map23 = {
1,
NULL,
&t1445_CustomAttributesCacheGenerator_U3CU3Ef__switch$map23
};
CustomAttributesCache t1445__CustomAttributeCache_U3CU3Ef__switch$map24 = {
1,
NULL,
&t1445_CustomAttributesCacheGenerator_U3CU3Ef__switch$map24
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1445_0_0_0;
extern Il2CppType t1445_1_0_0;
struct t1445;
extern CustomAttributesCache t1445__CustomAttributeCache_U3CU3Ef__switch$map23;
extern CustomAttributesCache t1445__CustomAttributeCache_U3CU3Ef__switch$map24;
TypeInfo t1445_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConstructionCallDictionary", "System.Runtime.Remoting.Messaging", t1445_MIs, NULL, t1445_FIs, NULL, &t1446_TI, NULL, NULL, &t1445_TI, NULL, t1445_VT, &EmptyCustomAttributesCache, &t1445_TI, &t1445_0_0_0, &t1445_1_0_0, t1445_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1445), 0, -1, sizeof(t1445_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 0, 3, 0, 0, 18, 0, 3};
#include "t1447.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1447_TI;
#include "t1447MD.h"

extern MethodInfo m7850_MI;


 void m7850 (t1447 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7851_MI;
 void m7851 (t29 * __this, MethodInfo* method){
	{
		t1447 * L_0 = (t1447 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1447_TI));
		m7850(L_0, &m7850_MI);
		((t1447_SFs*)InitializedTypeInfo(&t1447_TI)->static_fields)->f0 = L_0;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern Il2CppType t1447_0_0_22;
FieldInfo t1447_f0_FieldInfo = 
{
	"Instance", &t1447_0_0_22, &t1447_TI, offsetof(t1447_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1447_FIs[] =
{
	&t1447_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7850_MI = 
{
	".ctor", (methodPointerType)&m7850, &t1447_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3420, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7851_MI = 
{
	".cctor", (methodPointerType)&m7851, &t1447_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3421, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1447_MIs[] =
{
	&m7850_MI,
	&m7851_MI,
	NULL
};
static MethodInfo* t1447_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
static TypeInfo* t1447_ITIs[] = 
{
	&t967_TI,
};
static Il2CppInterfaceOffsetPair t1447_IOs[] = 
{
	{ &t967_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1447_0_0_0;
extern Il2CppType t1447_1_0_0;
struct t1447;
TypeInfo t1447_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EnvoyTerminatorSink", "System.Runtime.Remoting.Messaging", t1447_MIs, NULL, t1447_FIs, NULL, &t29_TI, NULL, NULL, &t1447_TI, t1447_ITIs, t1447_VT, &EmptyCustomAttributesCache, &t1447_TI, &t1447_0_0_0, &t1447_1_0_0, t1447_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1447), 0, -1, sizeof(t1447_SFs), 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, true, false, false, 2, 0, 1, 0, 0, 4, 1, 1};
#include "t1448.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1448_TI;
#include "t1448MD.h"

extern MethodInfo m7853_MI;
extern MethodInfo m7854_MI;


extern MethodInfo m7852_MI;
 void m7852 (t1448 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		m7853(__this, p0, p1, 1, &m7853_MI);
		return;
	}
}
 void m7853 (t1448 * __this, t7* p0, t29 * p1, bool p2, MethodInfo* method){
	{
		m7854(__this, p0, p1, p2, (t7*)NULL, &m7854_MI);
		return;
	}
}
 void m7854 (t1448 * __this, t7* p0, t29 * p1, bool p2, t7* p3, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f2 = p0;
		__this->f3 = p1;
		__this->f1 = p2;
		__this->f0 = p3;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern Il2CppType t7_0_0_6;
FieldInfo t1448_f0_FieldInfo = 
{
	"HeaderNamespace", &t7_0_0_6, &t1448_TI, offsetof(t1448, f0), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t1448_f1_FieldInfo = 
{
	"MustUnderstand", &t40_0_0_6, &t1448_TI, offsetof(t1448, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_6;
FieldInfo t1448_f2_FieldInfo = 
{
	"Name", &t7_0_0_6, &t1448_TI, offsetof(t1448, f2), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_6;
FieldInfo t1448_f3_FieldInfo = 
{
	"Value", &t29_0_0_6, &t1448_TI, offsetof(t1448, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1448_FIs[] =
{
	&t1448_f0_FieldInfo,
	&t1448_f1_FieldInfo,
	&t1448_f2_FieldInfo,
	&t1448_f3_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1448_m7852_ParameterInfos[] = 
{
	{"_Name", 0, 134221890, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"_Value", 1, 134221891, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7852_MI = 
{
	".ctor", (methodPointerType)&m7852, &t1448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1448_m7852_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3422, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1448_m7853_ParameterInfos[] = 
{
	{"_Name", 0, 134221892, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"_Value", 1, 134221893, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"_MustUnderstand", 2, 134221894, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7853_MI = 
{
	".ctor", (methodPointerType)&m7853, &t1448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t297, t1448_m7853_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 3423, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1448_m7854_ParameterInfos[] = 
{
	{"_Name", 0, 134221895, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"_Value", 1, 134221896, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"_MustUnderstand", 2, 134221897, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"_HeaderNamespace", 3, 134221898, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7854_MI = 
{
	".ctor", (methodPointerType)&m7854, &t1448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t297_t29, t1448_m7854_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 4, false, false, 3424, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1448_MIs[] =
{
	&m7852_MI,
	&m7853_MI,
	&m7854_MI,
	NULL
};
static MethodInfo* t1448_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1448_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1448__CustomAttributeCache = {
1,
NULL,
&t1448_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1448_0_0_0;
extern Il2CppType t1448_1_0_0;
struct t1448;
extern CustomAttributesCache t1448__CustomAttributeCache;
TypeInfo t1448_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Header", "System.Runtime.Remoting.Messaging", t1448_MIs, NULL, t1448_FIs, NULL, &t29_TI, NULL, NULL, &t1448_TI, NULL, t1448_VT, &t1448__CustomAttributeCache, &t1448_TI, &t1448_0_0_0, &t1448_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1448), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 4, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern MethodInfo m10194_MI;
static PropertyInfo t2046____Uri_PropertyInfo = 
{
	&t2046_TI, "Uri", NULL, &m10194_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2046_PIs[] =
{
	&t2046____Uri_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2046_m10194_ParameterInfos[] = 
{
	{"value", 0, 134221899, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10194_MI = 
{
	"set_Uri", NULL, &t2046_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2046_m10194_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 0, 1, false, false, 3425, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t2046_MIs[] =
{
	&m10194_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2046_0_0_0;
extern Il2CppType t2046_1_0_0;
struct t2046;
TypeInfo t2046_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IInternalMessage", "System.Runtime.Remoting.Messaging", t2046_MIs, t2046_PIs, NULL, NULL, NULL, NULL, NULL, &t2046_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2046_TI, &t2046_0_0_0, &t2046_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 160, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
static MethodInfo* t1443_MIs[] =
{
	NULL
};
void t1443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1443__CustomAttributeCache = {
1,
NULL,
&t1443_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1443_1_0_0;
struct t1443;
extern CustomAttributesCache t1443__CustomAttributeCache;
TypeInfo t1443_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IMessage", "System.Runtime.Remoting.Messaging", t1443_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1443_TI, NULL, NULL, &t1443__CustomAttributeCache, &t1443_TI, &t1443_0_0_0, &t1443_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1442_TI;



// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
static MethodInfo* t1442_MIs[] =
{
	NULL
};
void t1442_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1442__CustomAttributeCache = {
1,
NULL,
&t1442_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1442_1_0_0;
struct t1442;
extern CustomAttributesCache t1442__CustomAttributeCache;
TypeInfo t1442_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IMessageCtrl", "System.Runtime.Remoting.Messaging", t1442_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1442_TI, NULL, NULL, &t1442__CustomAttributeCache, &t1442_TI, &t1442_0_0_0, &t1442_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
static MethodInfo* t967_MIs[] =
{
	NULL
};
void t967_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t967__CustomAttributeCache = {
1,
NULL,
&t967_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t967_1_0_0;
struct t967;
extern CustomAttributesCache t967__CustomAttributeCache;
TypeInfo t967_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IMessageSink", "System.Runtime.Remoting.Messaging", t967_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t967_TI, NULL, NULL, &t967__CustomAttributeCache, &t967_TI, &t967_0_0_0, &t967_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
static MethodInfo* t1463_MIs[] =
{
	NULL
};
static TypeInfo* t1463_ITIs[] = 
{
	&t1443_TI,
	&t1453_TI,
};
void t1463_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1463__CustomAttributeCache = {
1,
NULL,
&t1463_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1463_0_0_0;
extern Il2CppType t1463_1_0_0;
struct t1463;
extern CustomAttributesCache t1463__CustomAttributeCache;
TypeInfo t1463_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IMethodCallMessage", "System.Runtime.Remoting.Messaging", t1463_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1463_TI, t1463_ITIs, NULL, &t1463__CustomAttributeCache, &t1463_TI, &t1463_0_0_0, &t1463_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1449.h"


// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern MethodInfo m10195_MI;
static PropertyInfo t1453____Args_PropertyInfo = 
{
	&t1453_TI, "Args", &m10195_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10196_MI;
static PropertyInfo t1453____LogicalCallContext_PropertyInfo = 
{
	&t1453_TI, "LogicalCallContext", &m10196_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10197_MI;
static PropertyInfo t1453____MethodBase_PropertyInfo = 
{
	&t1453_TI, "MethodBase", &m10197_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10198_MI;
static PropertyInfo t1453____MethodName_PropertyInfo = 
{
	&t1453_TI, "MethodName", &m10198_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10199_MI;
static PropertyInfo t1453____MethodSignature_PropertyInfo = 
{
	&t1453_TI, "MethodSignature", &m10199_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10200_MI;
static PropertyInfo t1453____TypeName_PropertyInfo = 
{
	&t1453_TI, "TypeName", &m10200_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10201_MI;
static PropertyInfo t1453____Uri_PropertyInfo = 
{
	&t1453_TI, "Uri", &m10201_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1453_PIs[] =
{
	&t1453____Args_PropertyInfo,
	&t1453____LogicalCallContext_PropertyInfo,
	&t1453____MethodBase_PropertyInfo,
	&t1453____MethodName_PropertyInfo,
	&t1453____MethodSignature_PropertyInfo,
	&t1453____TypeName_PropertyInfo,
	&t1453____Uri_PropertyInfo,
	NULL
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10195_MI = 
{
	"get_Args", NULL, &t1453_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3426, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1449_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10196_MI = 
{
	"get_LogicalCallContext", NULL, &t1453_TI, &t1449_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, false, 3427, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10197_MI = 
{
	"get_MethodBase", NULL, &t1453_TI, &t636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 2, 0, false, false, 3428, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10198_MI = 
{
	"get_MethodName", NULL, &t1453_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 3, 0, false, false, 3429, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10199_MI = 
{
	"get_MethodSignature", NULL, &t1453_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 4, 0, false, false, 3430, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10200_MI = 
{
	"get_TypeName", NULL, &t1453_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 5, 0, false, false, 3431, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10201_MI = 
{
	"get_Uri", NULL, &t1453_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 6, 0, false, false, 3432, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1453_MIs[] =
{
	&m10195_MI,
	&m10196_MI,
	&m10197_MI,
	&m10198_MI,
	&m10199_MI,
	&m10200_MI,
	&m10201_MI,
	NULL
};
static TypeInfo* t1453_ITIs[] = 
{
	&t1443_TI,
};
void t1453_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1453__CustomAttributeCache = {
1,
NULL,
&t1453_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1453_0_0_0;
extern Il2CppType t1453_1_0_0;
struct t1453;
extern CustomAttributesCache t1453__CustomAttributeCache;
TypeInfo t1453_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IMethodMessage", "System.Runtime.Remoting.Messaging", t1453_MIs, t1453_PIs, NULL, NULL, NULL, NULL, NULL, &t1453_TI, t1453_ITIs, NULL, &t1453__CustomAttributeCache, &t1453_TI, &t1453_0_0_0, &t1453_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 7, 7, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1456_TI;

#include "t295.h"


// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern MethodInfo m10202_MI;
static PropertyInfo t1456____Exception_PropertyInfo = 
{
	&t1456_TI, "Exception", &m10202_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10203_MI;
static PropertyInfo t1456____OutArgs_PropertyInfo = 
{
	&t1456_TI, "OutArgs", &m10203_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10204_MI;
static PropertyInfo t1456____ReturnValue_PropertyInfo = 
{
	&t1456_TI, "ReturnValue", &m10204_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1456_PIs[] =
{
	&t1456____Exception_PropertyInfo,
	&t1456____OutArgs_PropertyInfo,
	&t1456____ReturnValue_PropertyInfo,
	NULL
};
extern Il2CppType t295_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10202_MI = 
{
	"get_Exception", NULL, &t1456_TI, &t295_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3433, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10203_MI = 
{
	"get_OutArgs", NULL, &t1456_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, false, 3434, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10204_MI = 
{
	"get_ReturnValue", NULL, &t1456_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 2, 0, false, false, 3435, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1456_MIs[] =
{
	&m10202_MI,
	&m10203_MI,
	&m10204_MI,
	NULL
};
static TypeInfo* t1456_ITIs[] = 
{
	&t1443_TI,
	&t1453_TI,
};
void t1456_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1456__CustomAttributeCache = {
1,
NULL,
&t1456_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1456_0_0_0;
extern Il2CppType t1456_1_0_0;
struct t1456;
extern CustomAttributesCache t1456__CustomAttributeCache;
TypeInfo t1456_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IMethodReturnMessage", "System.Runtime.Remoting.Messaging", t1456_MIs, t1456_PIs, NULL, NULL, NULL, NULL, NULL, &t1456_TI, t1456_ITIs, NULL, &t1456__CustomAttributeCache, &t1456_TI, &t1456_0_0_0, &t1456_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 3, 3, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2048_TI;



// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
static MethodInfo* t2048_MIs[] =
{
	NULL
};
extern TypeInfo t2049_TI;
static TypeInfo* t2048_ITIs[] = 
{
	&t2049_TI,
};
void t2048_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2048__CustomAttributeCache = {
1,
NULL,
&t2048_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2048_0_0_0;
extern Il2CppType t2048_1_0_0;
struct t2048;
extern CustomAttributesCache t2048__CustomAttributeCache;
TypeInfo t2048_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IRemotingFormatter", "System.Runtime.Remoting.Messaging", t2048_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2048_TI, t2048_ITIs, NULL, &t2048__CustomAttributeCache, &t2048_TI, &t2048_0_0_0, &t2048_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
static MethodInfo* t2047_MIs[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2047_0_0_0;
extern Il2CppType t2047_1_0_0;
struct t2047;
TypeInfo t2047_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ISerializationRootObject", "System.Runtime.Remoting.Messaging", t2047_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2047_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2047_TI, &t2047_0_0_0, &t2047_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 160, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1449_TI;
#include "t1449MD.h"

#include "t1450.h"
#include "t1520.h"
#include "t1522.h"
#include "t725.h"
extern TypeInfo t1450_TI;
extern TypeInfo t1522_TI;
extern TypeInfo t722_TI;
extern TypeInfo t725_TI;
#include "t1450MD.h"
#include "t1522MD.h"
#include "t1520MD.h"
#include "t725MD.h"
extern MethodInfo m7859_MI;
extern MethodInfo m8145_MI;
extern MethodInfo m8153_MI;
extern MethodInfo m8139_MI;
extern MethodInfo m8140_MI;
extern MethodInfo m7858_MI;
extern MethodInfo m8156_MI;
extern MethodInfo m6722_MI;
extern MethodInfo m6684_MI;
extern MethodInfo m6685_MI;
extern MethodInfo m4216_MI;


extern MethodInfo m7855_MI;
 void m7855 (t1449 * __this, MethodInfo* method){
	{
		t1450 * L_0 = (t1450 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1450_TI));
		m7859(L_0, &m7859_MI);
		__this->f1 = L_0;
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7856_MI;
 void m7856 (t1449 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t1520  V_0 = {0};
	t1522 * V_1 = {0};
	{
		t1450 * L_0 = (t1450 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1450_TI));
		m7859(L_0, &m7859_MI);
		__this->f1 = L_0;
		m1331(__this, &m1331_MI);
		t1522 * L_1 = m8145(p0, &m8145_MI);
		V_1 = L_1;
		goto IL_005c;
	}

IL_001a:
	{
		t1520  L_2 = m8153(V_1, &m8153_MI);
		V_0 = L_2;
		t7* L_3 = m8139((&V_0), &m8139_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1713(NULL, L_3, (t7*) &_stringLiteral1602, &m1713_MI);
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		t29 * L_5 = m8140((&V_0), &m8140_MI);
		__this->f1 = ((t1450 *)Castclass(L_5, InitializedTypeInfo(&t1450_TI)));
		goto IL_005c;
	}

IL_0048:
	{
		t7* L_6 = m8139((&V_0), &m8139_MI);
		t29 * L_7 = m8140((&V_0), &m8140_MI);
		m7858(__this, L_6, L_7, &m7858_MI);
	}

IL_005c:
	{
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(&m8156_MI, V_1);
		if (L_8)
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
extern MethodInfo m7857_MI;
 void m7857 (t1449 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t725  V_0 = {0};
	t29 * V_1 = {0};
	t29 * V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t1450 * L_0 = (__this->f1);
		m3997(p0, (t7*) &_stringLiteral1602, L_0, &m3997_MI);
		t719 * L_1 = (__this->f0);
		if (!L_1)
		{
			goto IL_0068;
		}
	}
	{
		t719 * L_2 = (__this->f0);
		t29 * L_3 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m6722_MI, L_2);
		V_1 = L_3;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004c;
		}

IL_0027:
		{
			t29 * L_4 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_1);
			V_0 = ((*(t725 *)((t725 *)UnBox (L_4, InitializedTypeInfo(&t725_TI)))));
			t29 * L_5 = m6684((&V_0), &m6684_MI);
			t29 * L_6 = m6685((&V_0), &m6685_MI);
			m3997(p0, ((t7*)Castclass(L_5, (&t7_TI))), L_6, &m3997_MI);
		}

IL_004c:
		{
			bool L_7 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_7)
			{
				goto IL_0027;
			}
		}

IL_0054:
		{
			// IL_0054: leave.s IL_0068
			leaveInstructions[0] = 0x68; // 1
			THROW_SENTINEL(IL_0068);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0056;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0056;
	}

IL_0056:
	{ // begin finally (depth: 1)
		{
			V_2 = ((t29 *)IsInst(V_1, InitializedTypeInfo(&t324_TI)));
			if (V_2)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x68:
					goto IL_0068;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0061:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_2);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x68:
					goto IL_0068;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0068:
	{
		return;
	}
}
 void m7858 (t1449 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		t719 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_1 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_1, &m4209_MI);
		__this->f0 = L_1;
	}

IL_0013:
	{
		t719 * L_2 = (__this->f0);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, L_2, p0, p1);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern Il2CppType t719_0_0_1;
FieldInfo t1449_f0_FieldInfo = 
{
	"_data", &t719_0_0_1, &t1449_TI, offsetof(t1449, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1450_0_0_1;
FieldInfo t1449_f1_FieldInfo = 
{
	"_remotingData", &t1450_0_0_1, &t1449_TI, offsetof(t1449, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1449_FIs[] =
{
	&t1449_f0_FieldInfo,
	&t1449_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7855_MI = 
{
	".ctor", (methodPointerType)&m7855, &t1449_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3436, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1449_m7856_ParameterInfos[] = 
{
	{"info", 0, 134221900, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221901, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7856_MI = 
{
	".ctor", (methodPointerType)&m7856, &t1449_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1449_m7856_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3437, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1449_m7857_ParameterInfos[] = 
{
	{"info", 0, 134221902, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221903, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7857_MI = 
{
	"GetObjectData", (methodPointerType)&m7857, &t1449_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1449_m7857_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 2, false, false, 3438, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1449_m7858_ParameterInfos[] = 
{
	{"name", 0, 134221904, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"data", 1, 134221905, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7858_MI = 
{
	"SetData", (methodPointerType)&m7858, &t1449_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1449_m7858_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 3439, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1449_MIs[] =
{
	&m7855_MI,
	&m7856_MI,
	&m7857_MI,
	&m7858_MI,
	NULL
};
static MethodInfo* t1449_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7857_MI,
};
extern TypeInfo t373_TI;
static TypeInfo* t1449_ITIs[] = 
{
	&t373_TI,
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1449_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
void t1449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1449__CustomAttributeCache = {
1,
NULL,
&t1449_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1449_0_0_0;
extern Il2CppType t1449_1_0_0;
struct t1449;
extern CustomAttributesCache t1449__CustomAttributeCache;
TypeInfo t1449_TI = 
{
	&g_mscorlib_dll_Image, NULL, "LogicalCallContext", "System.Runtime.Remoting.Messaging", t1449_MIs, NULL, t1449_FIs, NULL, &t29_TI, NULL, NULL, &t1449_TI, t1449_ITIs, t1449_VT, &t1449__CustomAttributeCache, &t1449_TI, &t1449_0_0_0, &t1449_1_0_0, t1449_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1449), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 2, 0, 0, 5, 2, 2};
#ifndef _MSC_VER
#else
#endif



 void m7859 (t1450 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7859_MI = 
{
	".ctor", (methodPointerType)&m7859, &t1450_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3440, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1450_MIs[] =
{
	&m7859_MI,
	NULL
};
static MethodInfo* t1450_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
static TypeInfo* t1450_ITIs[] = 
{
	&t373_TI,
};
static Il2CppInterfaceOffsetPair t1450_IOs[] = 
{
	{ &t373_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1450_0_0_0;
extern Il2CppType t1450_1_0_0;
struct t1450;
TypeInfo t1450_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CallContextRemotingData", "System.Runtime.Remoting.Messaging", t1450_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1450_TI, t1450_ITIs, t1450_VT, &EmptyCustomAttributesCache, &t1450_TI, &t1450_0_0_0, &t1450_1_0_0, t1450_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1450), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 1, 1};
#ifndef _MSC_VER
#else
#endif

#include "t296.h"
#include "t1452.h"
#include "t194.h"
extern TypeInfo t296_TI;
extern TypeInfo t537_TI;
extern TypeInfo t1452_TI;
extern TypeInfo t1482_TI;
#include "t296MD.h"
#include "t1452MD.h"
#include "t1482MD.h"
#include "t557MD.h"
extern MethodInfo m7877_MI;
extern MethodInfo m2924_MI;
extern MethodInfo m7872_MI;
extern MethodInfo m2937_MI;
extern MethodInfo m7550_MI;
extern MethodInfo m3955_MI;
extern MethodInfo m3961_MI;
extern MethodInfo m7881_MI;
extern MethodInfo m8004_MI;
extern MethodInfo m2926_MI;
extern MethodInfo m7878_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m4008_MI;
extern MethodInfo m8009_MI;
extern MethodInfo m1387_MI;
extern MethodInfo m5999_MI;
extern MethodInfo m8002_MI;
extern MethodInfo m8008_MI;
extern MethodInfo m7553_MI;
extern MethodInfo m7551_MI;
extern MethodInfo m7880_MI;
extern MethodInfo m7557_MI;
extern MethodInfo m7879_MI;
extern MethodInfo m2908_MI;
extern MethodInfo m9850_MI;
extern MethodInfo m2929_MI;
extern MethodInfo m4282_MI;
extern MethodInfo m1742_MI;
extern MethodInfo m2925_MI;


extern MethodInfo m7860_MI;
 void m7860 (t1444 * __this, t1451* p0, MethodInfo* method){
	t1448 * V_0 = {0};
	t1451* V_1 = {0};
	int32_t V_2 = 0;
	{
		m1331(__this, &m1331_MI);
		VirtActionInvoker0::Invoke(&m7876_MI, __this);
		if (!p0)
		{
			goto IL_0014;
		}
	}
	{
		if ((((int32_t)(((t20 *)p0)->max_length))))
		{
			goto IL_0015;
		}
	}

IL_0014:
	{
		return;
	}

IL_0015:
	{
		V_1 = p0;
		V_2 = 0;
		goto IL_0035;
	}

IL_001b:
	{
		int32_t L_0 = V_2;
		V_0 = (*(t1448 **)(t1448 **)SZArrayLdElema(V_1, L_0));
		t7* L_1 = (V_0->f2);
		t29 * L_2 = (V_0->f3);
		VirtActionInvoker2< t7*, t29 * >::Invoke(&m7864_MI, __this, L_1, L_2);
		V_2 = ((int32_t)(V_2+1));
	}

IL_0035:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_001b;
		}
	}
	{
		m7877(__this, &m7877_MI);
		return;
	}
}
 void m7861 (t1444 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t1520  V_0 = {0};
	t1522 * V_1 = {0};
	{
		m1331(__this, &m1331_MI);
		VirtActionInvoker0::Invoke(&m7876_MI, __this);
		t1522 * L_0 = m8145(p0, &m8145_MI);
		V_1 = L_0;
		goto IL_0030;
	}

IL_0015:
	{
		t1520  L_1 = m8153(V_1, &m8153_MI);
		V_0 = L_1;
		t7* L_2 = m8139((&V_0), &m8139_MI);
		t29 * L_3 = m8140((&V_0), &m8140_MI);
		VirtActionInvoker2< t7*, t29 * >::Invoke(&m7864_MI, __this, L_2, L_3);
	}

IL_0030:
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m8156_MI, V_1);
		if (L_4)
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
 void m7862 (t1444 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
 void m7863 (t1444 * __this, t7* p0, MethodInfo* method){
	{
		VirtActionInvoker1< t7* >::Invoke(&m7875_MI, __this, p0);
		return;
	}
}
 void m7864 (t1444 * __this, t7* p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_0101;
		}
	}
	{
		if ((((t1444_SFs*)InitializedTypeInfo(&t1444_TI)->static_fields)->f10))
		{
			goto IL_0070;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 7, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1597, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1596, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1598, 2);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1599, 3);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1600, 4);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1595, 5);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1603, 6);
		((t1444_SFs*)InitializedTypeInfo(&t1444_TI)->static_fields)->f10 = V_1;
	}

IL_0070:
	{
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1444_SFs*)InitializedTypeInfo(&t1444_TI)->static_fields)->f10), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_0101;
		}
	}
	{
		if (V_2 == 0)
		{
			goto IL_00a6;
		}
		if (V_2 == 1)
		{
			goto IL_00b3;
		}
		if (V_2 == 2)
		{
			goto IL_00c0;
		}
		if (V_2 == 3)
		{
			goto IL_00cd;
		}
		if (V_2 == 4)
		{
			goto IL_00da;
		}
		if (V_2 == 5)
		{
			goto IL_00e7;
		}
		if (V_2 == 6)
		{
			goto IL_00f4;
		}
	}
	{
		goto IL_0101;
	}

IL_00a6:
	{
		__this->f1 = ((t7*)Castclass(p1, (&t7_TI)));
		return;
	}

IL_00b3:
	{
		__this->f2 = ((t7*)Castclass(p1, (&t7_TI)));
		return;
	}

IL_00c0:
	{
		__this->f4 = ((t537*)Castclass(p1, InitializedTypeInfo(&t537_TI)));
		return;
	}

IL_00cd:
	{
		__this->f3 = ((t316*)Castclass(p1, InitializedTypeInfo(&t316_TI)));
		return;
	}

IL_00da:
	{
		__this->f6 = ((t1449 *)Castclass(p1, InitializedTypeInfo(&t1449_TI)));
		return;
	}

IL_00e7:
	{
		__this->f0 = ((t7*)Castclass(p1, (&t7_TI)));
		return;
	}

IL_00f4:
	{
		__this->f7 = ((t537*)Castclass(p1, InitializedTypeInfo(&t537_TI)));
		return;
	}

IL_0101:
	{
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7871_MI, __this);
		InterfaceActionInvoker2< t29 *, t29 * >::Invoke(&m3955_MI, L_2, p0, p1);
		return;
	}
}
 void m7865 (t1444 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t725  V_0 = {0};
	t29 * V_1 = {0};
	t29 * V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t7* L_0 = (__this->f1);
		m3997(p0, (t7*) &_stringLiteral1597, L_0, &m3997_MI);
		t7* L_1 = (__this->f2);
		m3997(p0, (t7*) &_stringLiteral1596, L_1, &m3997_MI);
		t537* L_2 = (__this->f4);
		m3997(p0, (t7*) &_stringLiteral1598, (t29 *)(t29 *)L_2, &m3997_MI);
		t316* L_3 = (__this->f3);
		m3997(p0, (t7*) &_stringLiteral1599, (t29 *)(t29 *)L_3, &m3997_MI);
		t1449 * L_4 = (__this->f6);
		m3997(p0, (t7*) &_stringLiteral1600, L_4, &m3997_MI);
		t7* L_5 = (__this->f0);
		m3997(p0, (t7*) &_stringLiteral1595, L_5, &m3997_MI);
		t537* L_6 = (__this->f7);
		m3997(p0, (t7*) &_stringLiteral1603, (t29 *)(t29 *)L_6, &m3997_MI);
		t29 * L_7 = (__this->f9);
		if (!L_7)
		{
			goto IL_00ce;
		}
	}
	{
		t29 * L_8 = (__this->f9);
		t29 * L_9 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3961_MI, L_8);
		V_1 = L_9;
	}

IL_008b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b2;
		}

IL_008d:
		{
			t29 * L_10 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_1);
			V_0 = ((*(t725 *)((t725 *)UnBox (L_10, InitializedTypeInfo(&t725_TI)))));
			t29 * L_11 = m6684((&V_0), &m6684_MI);
			t29 * L_12 = m6685((&V_0), &m6685_MI);
			m3997(p0, ((t7*)Castclass(L_11, (&t7_TI))), L_12, &m3997_MI);
		}

IL_00b2:
		{
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_13)
			{
				goto IL_008d;
			}
		}

IL_00ba:
		{
			// IL_00ba: leave.s IL_00ce
			leaveInstructions[0] = 0xCE; // 1
			THROW_SENTINEL(IL_00ce);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_00bc;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_00bc;
	}

IL_00bc:
	{ // begin finally (depth: 1)
		{
			V_2 = ((t29 *)IsInst(V_1, InitializedTypeInfo(&t324_TI)));
			if (V_2)
			{
				goto IL_00c7;
			}
		}

IL_00c6:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xCE:
					goto IL_00ce;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_00c7:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_2);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xCE:
					goto IL_00ce;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_00ce:
	{
		return;
	}
}
 t316* m7866 (t1444 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f3);
		return L_0;
	}
}
 t1449 * m7867 (t1444 * __this, MethodInfo* method){
	{
		t1449 * L_0 = (__this->f6);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		t1449 * L_1 = (t1449 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1449_TI));
		m7855(L_1, &m7855_MI);
		__this->f6 = L_1;
	}

IL_0013:
	{
		t1449 * L_2 = (__this->f6);
		return L_2;
	}
}
 t636 * m7868 (t1444 * __this, MethodInfo* method){
	{
		t636 * L_0 = (__this->f5);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		m7877(__this, &m7877_MI);
	}

IL_000e:
	{
		t636 * L_1 = (__this->f5);
		return L_1;
	}
}
 t7* m7869 (t1444 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		t636 * L_1 = (__this->f5);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, L_1);
		__this->f2 = L_2;
	}

IL_0019:
	{
		t7* L_3 = (__this->f2);
		return L_3;
	}
}
 t29 * m7870 (t1444 * __this, MethodInfo* method){
	t638* V_0 = {0};
	int32_t V_1 = 0;
	{
		t537* L_0 = (__this->f4);
		if (L_0)
		{
			goto IL_0048;
		}
	}
	{
		t636 * L_1 = (__this->f5);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		t636 * L_2 = (__this->f5);
		t638* L_3 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, L_2);
		V_0 = L_3;
		__this->f4 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), (((int32_t)(((t20 *)V_0)->max_length)))));
		V_1 = 0;
		goto IL_0042;
	}

IL_002e:
	{
		t537* L_4 = (__this->f4);
		int32_t L_5 = V_1;
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_5)));
		ArrayElementTypeCheck (L_4, L_6);
		*((t42 **)(t42 **)SZArrayLdElema(L_4, V_1)) = (t42 *)L_6;
		V_1 = ((int32_t)(V_1+1));
	}

IL_0042:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_002e;
		}
	}

IL_0048:
	{
		t537* L_7 = (__this->f4);
		return (t29 *)L_7;
	}
}
 t29 * m7871 (t1444 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f8);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		VirtActionInvoker0::Invoke(&m7872_MI, __this);
	}

IL_000e:
	{
		t29 * L_1 = (__this->f8);
		return L_1;
	}
}
 void m7872 (t1444 * __this, MethodInfo* method){
	t1452 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1452_TI));
		t1452 * L_0 = (t1452 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1452_TI));
		m7881(L_0, __this, &m7881_MI);
		V_0 = L_0;
		__this->f8 = V_0;
		t29 * L_1 = m7893(V_0, &m7893_MI);
		__this->f9 = L_1;
		return;
	}
}
 t7* m7873 (t1444 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		t636 * L_1 = (__this->f5);
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, L_1);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_2);
		__this->f1 = L_3;
	}

IL_001e:
	{
		t7* L_4 = (__this->f1);
		return L_4;
	}
}
 t7* m7874 (t1444 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
 void m7875 (t1444 * __this, t7* p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 void m7876 (t1444 * __this, MethodInfo* method){
	{
		return;
	}
}
 void m7877 (t1444 * __this, MethodInfo* method){
	t42 * V_0 = {0};
	t7* V_1 = {0};
	t42 * V_2 = {0};
	t7* G_B5_0 = {0};
	{
		t7* L_0 = (__this->f0);
		if (!L_0)
		{
			goto IL_0153;
		}
	}
	{
		t7* L_1 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t42 * L_2 = m8004(NULL, L_1, &m8004_MI);
		V_0 = L_2;
		if (V_0)
		{
			goto IL_005b;
		}
	}
	{
		t7* L_3 = (__this->f1);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		t7* L_4 = (__this->f1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = m1685(NULL, (t7*) &_stringLiteral59, L_4, (t7*) &_stringLiteral60, &m1685_MI);
		G_B5_0 = L_5;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B5_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_003e:
	{
		V_1 = G_B5_0;
		t7* L_6 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_7 = m2926(NULL, (t7*) &_stringLiteral1604, V_1, (t7*) &_stringLiteral1605, L_6, &m2926_MI);
		t1481 * L_8 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_8, L_7, &m7999_MI);
		il2cpp_codegen_raise_exception(L_8);
	}

IL_005b:
	{
		t7* L_9 = (__this->f1);
		t42 * L_10 = m7878(__this, L_9, V_0, &m7878_MI);
		V_2 = L_10;
		if (V_2)
		{
			goto IL_00a7;
		}
	}
	{
		t446* L_11 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_11, (t7*) &_stringLiteral1606);
		*((t7**)(t7**)SZArrayLdElema(L_11, 0)) = (t7*)(t7*) &_stringLiteral1606;
		t446* L_12 = L_11;
		t7* L_13 = (__this->f1);
		ArrayElementTypeCheck (L_12, L_13);
		*((t7**)(t7**)SZArrayLdElema(L_12, 1)) = (t7*)L_13;
		t446* L_14 = L_12;
		ArrayElementTypeCheck (L_14, (t7*) &_stringLiteral1607);
		*((t7**)(t7**)SZArrayLdElema(L_14, 2)) = (t7*)(t7*) &_stringLiteral1607;
		t446* L_15 = L_14;
		t7* L_16 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, V_0);
		ArrayElementTypeCheck (L_15, L_16);
		*((t7**)(t7**)SZArrayLdElema(L_15, 3)) = (t7*)L_16;
		t446* L_17 = L_15;
		ArrayElementTypeCheck (L_17, (t7*) &_stringLiteral53);
		*((t7**)(t7**)SZArrayLdElema(L_17, 4)) = (t7*)(t7*) &_stringLiteral53;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_18 = m4008(NULL, L_17, &m4008_MI);
		t1481 * L_19 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_19, L_18, &m7999_MI);
		il2cpp_codegen_raise_exception(L_19);
	}

IL_00a7:
	{
		t7* L_20 = (__this->f2);
		t537* L_21 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t636 * L_22 = m8009(NULL, V_2, L_20, L_21, &m8009_MI);
		__this->f5 = L_22;
		t636 * L_23 = (__this->f5);
		if (L_23)
		{
			goto IL_00f5;
		}
	}
	{
		t316* L_24 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 4));
		ArrayElementTypeCheck (L_24, (t7*) &_stringLiteral1608);
		*((t29 **)(t29 **)SZArrayLdElema(L_24, 0)) = (t29 *)(t7*) &_stringLiteral1608;
		t316* L_25 = L_24;
		t7* L_26 = (__this->f2);
		ArrayElementTypeCheck (L_25, L_26);
		*((t29 **)(t29 **)SZArrayLdElema(L_25, 1)) = (t29 *)L_26;
		t316* L_27 = L_25;
		ArrayElementTypeCheck (L_27, (t7*) &_stringLiteral1609);
		*((t29 **)(t29 **)SZArrayLdElema(L_27, 2)) = (t29 *)(t7*) &_stringLiteral1609;
		t316* L_28 = L_27;
		ArrayElementTypeCheck (L_28, V_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_28, 3)) = (t29 *)V_2;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_29 = m1387(NULL, L_28, &m1387_MI);
		t1481 * L_30 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_30, L_29, &m7999_MI);
		il2cpp_codegen_raise_exception(L_30);
	}

IL_00f5:
	{
		if ((((t42 *)V_2) == ((t42 *)V_0)))
		{
			goto IL_0151;
		}
	}
	{
		bool L_31 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, V_2);
		if (!L_31)
		{
			goto IL_0151;
		}
	}
	{
		bool L_32 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, V_0);
		if (L_32)
		{
			goto IL_0151;
		}
	}
	{
		t636 * L_33 = (__this->f5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t636 * L_34 = m8002(NULL, V_0, L_33, &m8002_MI);
		__this->f5 = L_34;
		t636 * L_35 = (__this->f5);
		if (L_35)
		{
			goto IL_0151;
		}
	}
	{
		t316* L_36 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 4));
		ArrayElementTypeCheck (L_36, (t7*) &_stringLiteral1608);
		*((t29 **)(t29 **)SZArrayLdElema(L_36, 0)) = (t29 *)(t7*) &_stringLiteral1608;
		t316* L_37 = L_36;
		t7* L_38 = (__this->f2);
		ArrayElementTypeCheck (L_37, L_38);
		*((t29 **)(t29 **)SZArrayLdElema(L_37, 1)) = (t29 *)L_38;
		t316* L_39 = L_37;
		ArrayElementTypeCheck (L_39, (t7*) &_stringLiteral1609);
		*((t29 **)(t29 **)SZArrayLdElema(L_39, 2)) = (t29 *)(t7*) &_stringLiteral1609;
		t316* L_40 = L_39;
		ArrayElementTypeCheck (L_40, V_0);
		*((t29 **)(t29 **)SZArrayLdElema(L_40, 3)) = (t29 *)V_0;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_41 = m1387(NULL, L_40, &m1387_MI);
		t1481 * L_42 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_42, L_41, &m7999_MI);
		il2cpp_codegen_raise_exception(L_42);
	}

IL_0151:
	{
		goto IL_0188;
	}

IL_0153:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t636 * L_43 = m8008(NULL, __this, &m8008_MI);
		__this->f5 = L_43;
		t636 * L_44 = (__this->f5);
		if (L_44)
		{
			goto IL_0188;
		}
	}
	{
		t7* L_45 = (__this->f2);
		t7* L_46 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7873_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_47 = m2926(NULL, (t7*) &_stringLiteral1608, L_45, (t7*) &_stringLiteral1609, L_46, &m2926_MI);
		t1481 * L_48 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_48, L_47, &m7999_MI);
		il2cpp_codegen_raise_exception(L_48);
	}

IL_0188:
	{
		t636 * L_49 = (__this->f5);
		bool L_50 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7553_MI, L_49);
		if (!L_50)
		{
			goto IL_01d1;
		}
	}
	{
		t636 * L_51 = (__this->f5);
		bool L_52 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7551_MI, L_51);
		if (!L_52)
		{
			goto IL_01d1;
		}
	}
	{
		t537* L_53 = m7880(__this, &m7880_MI);
		if (L_53)
		{
			goto IL_01b5;
		}
	}
	{
		t1481 * L_54 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_54, (t7*) &_stringLiteral1610, &m7999_MI);
		il2cpp_codegen_raise_exception(L_54);
	}

IL_01b5:
	{
		t636 * L_55 = (__this->f5);
		t537* L_56 = m7880(__this, &m7880_MI);
		t557 * L_57 = (t557 *)VirtFuncInvoker1< t557 *, t537* >::Invoke(&m7557_MI, ((t557 *)Castclass(L_55, InitializedTypeInfo(&t557_TI))), L_56);
		__this->f5 = L_57;
	}

IL_01d1:
	{
		return;
	}
}
 t42 * m7878 (t1444 * __this, t7* p0, t42 * p1, MethodInfo* method){
	t42 * V_0 = {0};
	t537* V_1 = {0};
	t42 * V_2 = {0};
	t537* V_3 = {0};
	int32_t V_4 = 0;
	{
		t7* L_0 = m7879(NULL, p0, &m7879_MI);
		p0 = L_0;
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, p1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, p0, L_1, &m1713_MI);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return p1;
	}

IL_001a:
	{
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p1);
		V_0 = L_3;
		goto IL_003a;
	}

IL_0023:
	{
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, V_0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_5 = m1713(NULL, p0, L_4, &m1713_MI);
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		return V_0;
	}

IL_0033:
	{
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_0);
		V_0 = L_6;
	}

IL_003a:
	{
		if (V_0)
		{
			goto IL_0023;
		}
	}
	{
		t537* L_7 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m9850_MI, p1);
		V_1 = L_7;
		V_3 = V_1;
		V_4 = 0;
		goto IL_0066;
	}

IL_004b:
	{
		int32_t L_8 = V_4;
		V_2 = (*(t42 **)(t42 **)SZArrayLdElema(V_3, L_8));
		t7* L_9 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, V_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_10 = m1713(NULL, p0, L_9, &m1713_MI);
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		return V_2;
	}

IL_0060:
	{
		V_4 = ((int32_t)(V_4+1));
	}

IL_0066:
	{
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((t20 *)V_3)->max_length))))))
		{
			goto IL_004b;
		}
	}
	{
		return (t42 *)NULL;
	}
}
 t7* m7879 (t29 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	t7* G_B2_1 = {0};
	int32_t G_B1_0 = 0;
	t7* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	t7* G_B3_2 = {0};
	{
		int32_t L_0 = m2929(p0, (t7*) &_stringLiteral1611, &m2929_MI);
		V_0 = L_0;
		G_B1_0 = ((int32_t)44);
		G_B1_1 = p0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			G_B2_0 = ((int32_t)44);
			G_B2_1 = p0;
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0019;
	}

IL_0016:
	{
		G_B3_0 = ((int32_t)(V_0+2));
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0019:
	{
		int32_t L_1 = m4282(G_B3_2, G_B3_1, G_B3_0, &m4282_MI);
		V_1 = L_1;
		if ((((int32_t)V_1) == ((int32_t)(-1))))
		{
			goto IL_0034;
		}
	}
	{
		t7* L_2 = m1742(p0, 0, V_1, &m1742_MI);
		t7* L_3 = m2925(L_2, &m2925_MI);
		p0 = L_3;
	}

IL_0034:
	{
		return p0;
	}
}
 t537* m7880 (t1444 * __this, MethodInfo* method){
	t537* V_0 = {0};
	{
		t537* L_0 = (__this->f7);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		t537* L_1 = (__this->f7);
		return L_1;
	}

IL_000f:
	{
		t636 * L_2 = (t636 *)VirtFuncInvoker0< t636 * >::Invoke(&m7868_MI, __this);
		t537* L_3 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m7550_MI, L_2);
		t537* L_4 = L_3;
		V_0 = L_4;
		__this->f7 = L_4;
		return V_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern Il2CppType t7_0_0_1;
FieldInfo t1444_f0_FieldInfo = 
{
	"_uri", &t7_0_0_1, &t1444_TI, offsetof(t1444, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1444_f1_FieldInfo = 
{
	"_typeName", &t7_0_0_1, &t1444_TI, offsetof(t1444, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1444_f2_FieldInfo = 
{
	"_methodName", &t7_0_0_1, &t1444_TI, offsetof(t1444, f2), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t1444_f3_FieldInfo = 
{
	"_args", &t316_0_0_1, &t1444_TI, offsetof(t1444, f3), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_1;
FieldInfo t1444_f4_FieldInfo = 
{
	"_methodSignature", &t537_0_0_1, &t1444_TI, offsetof(t1444, f4), &EmptyCustomAttributesCache};
extern Il2CppType t636_0_0_1;
FieldInfo t1444_f5_FieldInfo = 
{
	"_methodBase", &t636_0_0_1, &t1444_TI, offsetof(t1444, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1449_0_0_1;
FieldInfo t1444_f6_FieldInfo = 
{
	"_callContext", &t1449_0_0_1, &t1444_TI, offsetof(t1444, f6), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_1;
FieldInfo t1444_f7_FieldInfo = 
{
	"_genericArguments", &t537_0_0_1, &t1444_TI, offsetof(t1444, f7), &EmptyCustomAttributesCache};
extern Il2CppType t721_0_0_4;
FieldInfo t1444_f8_FieldInfo = 
{
	"ExternalProperties", &t721_0_0_4, &t1444_TI, offsetof(t1444, f8), &EmptyCustomAttributesCache};
extern Il2CppType t721_0_0_4;
FieldInfo t1444_f9_FieldInfo = 
{
	"InternalProperties", &t721_0_0_4, &t1444_TI, offsetof(t1444, f9), &EmptyCustomAttributesCache};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1444__CustomAttributeCache_U3CU3Ef__switch$map1F;
FieldInfo t1444_f10_FieldInfo = 
{
	"<>f__switch$map1F", &t485_0_0_17, &t1444_TI, offsetof(t1444_SFs, f10), &t1444__CustomAttributeCache_U3CU3Ef__switch$map1F};
static FieldInfo* t1444_FIs[] =
{
	&t1444_f0_FieldInfo,
	&t1444_f1_FieldInfo,
	&t1444_f2_FieldInfo,
	&t1444_f3_FieldInfo,
	&t1444_f4_FieldInfo,
	&t1444_f5_FieldInfo,
	&t1444_f6_FieldInfo,
	&t1444_f7_FieldInfo,
	&t1444_f8_FieldInfo,
	&t1444_f9_FieldInfo,
	&t1444_f10_FieldInfo,
	NULL
};
static PropertyInfo t1444____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&t1444_TI, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri", NULL, &m7863_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____Args_PropertyInfo = 
{
	&t1444_TI, "Args", &m7866_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____LogicalCallContext_PropertyInfo = 
{
	&t1444_TI, "LogicalCallContext", &m7867_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____MethodBase_PropertyInfo = 
{
	&t1444_TI, "MethodBase", &m7868_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____MethodName_PropertyInfo = 
{
	&t1444_TI, "MethodName", &m7869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____MethodSignature_PropertyInfo = 
{
	&t1444_TI, "MethodSignature", &m7870_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____Properties_PropertyInfo = 
{
	&t1444_TI, "Properties", &m7871_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____TypeName_PropertyInfo = 
{
	&t1444_TI, "TypeName", &m7873_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____Uri_PropertyInfo = 
{
	&t1444_TI, "Uri", &m7874_MI, &m7875_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1444____GenericArguments_PropertyInfo = 
{
	&t1444_TI, "GenericArguments", &m7880_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1444_PIs[] =
{
	&t1444____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&t1444____Args_PropertyInfo,
	&t1444____LogicalCallContext_PropertyInfo,
	&t1444____MethodBase_PropertyInfo,
	&t1444____MethodName_PropertyInfo,
	&t1444____MethodSignature_PropertyInfo,
	&t1444____Properties_PropertyInfo,
	&t1444____TypeName_PropertyInfo,
	&t1444____Uri_PropertyInfo,
	&t1444____GenericArguments_PropertyInfo,
	NULL
};
extern Il2CppType t1451_0_0_0;
extern Il2CppType t1451_0_0_0;
static ParameterInfo t1444_m7860_ParameterInfos[] = 
{
	{"h1", 0, 134221906, &EmptyCustomAttributesCache, &t1451_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7860_MI = 
{
	".ctor", (methodPointerType)&m7860, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1444_m7860_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3441, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1444_m7861_ParameterInfos[] = 
{
	{"info", 0, 134221907, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221908, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7861_MI = 
{
	".ctor", (methodPointerType)&m7861, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1444_m7861_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3442, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7862_MI = 
{
	".ctor", (methodPointerType)&m7862, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3443, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1444_m7863_ParameterInfos[] = 
{
	{"value", 0, 134221909, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7863_MI = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri", (methodPointerType)&m7863, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1444_m7863_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 5, 1, false, false, 3444, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1444_m7864_ParameterInfos[] = 
{
	{"key", 0, 134221910, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134221911, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7864_MI = 
{
	"InitMethodProperty", (methodPointerType)&m7864, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1444_m7864_ParameterInfos, &EmptyCustomAttributesCache, 451, 0, 13, 2, false, false, 3445, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1444_m7865_ParameterInfos[] = 
{
	{"info", 0, 134221912, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221913, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7865_MI = 
{
	"GetObjectData", (methodPointerType)&m7865, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1444_m7865_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 14, 2, false, false, 3446, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7866_MI = 
{
	"get_Args", (methodPointerType)&m7866, &t1444_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3447, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1449_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7867_MI = 
{
	"get_LogicalCallContext", (methodPointerType)&m7867, &t1444_TI, &t1449_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, false, 3448, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7868_MI = 
{
	"get_MethodBase", (methodPointerType)&m7868, &t1444_TI, &t636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, false, 3449, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7869_MI = 
{
	"get_MethodName", (methodPointerType)&m7869, &t1444_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, false, 3450, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7870_MI = 
{
	"get_MethodSignature", (methodPointerType)&m7870, &t1444_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, false, 3451, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7871_MI = 
{
	"get_Properties", (methodPointerType)&m7871, &t1444_TI, &t721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 15, 0, false, false, 3452, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7872_MI = 
{
	"InitDictionary", (methodPointerType)&m7872, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 451, 0, 16, 0, false, false, 3453, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7873_MI = 
{
	"get_TypeName", (methodPointerType)&m7873, &t1444_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 11, 0, false, false, 3454, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7874_MI = 
{
	"get_Uri", (methodPointerType)&m7874, &t1444_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 12, 0, false, false, 3455, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1444_m7875_ParameterInfos[] = 
{
	{"value", 0, 134221914, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7875_MI = 
{
	"set_Uri", (methodPointerType)&m7875, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1444_m7875_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 17, 1, false, false, 3456, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7876_MI = 
{
	"Init", (methodPointerType)&m7876, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 18, 0, false, false, 3457, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7877_MI = 
{
	"ResolveMethod", (methodPointerType)&m7877, &t1444_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 3458, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1444_m7878_ParameterInfos[] = 
{
	{"clientType", 0, 134221915, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"serverType", 1, 134221916, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7878_MI = 
{
	"CastTo", (methodPointerType)&m7878, &t1444_TI, &t42_0_0_0, RuntimeInvoker_t29_t29_t29, t1444_m7878_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3459, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1444_m7879_ParameterInfos[] = 
{
	{"aqname", 0, 134221917, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7879_MI = 
{
	"GetTypeNameFromAssemblyQualifiedName", (methodPointerType)&m7879, &t1444_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1444_m7879_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 3460, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7880_MI = 
{
	"get_GenericArguments", (methodPointerType)&m7880, &t1444_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 3461, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1444_MIs[] =
{
	&m7860_MI,
	&m7861_MI,
	&m7862_MI,
	&m7863_MI,
	&m7864_MI,
	&m7865_MI,
	&m7866_MI,
	&m7867_MI,
	&m7868_MI,
	&m7869_MI,
	&m7870_MI,
	&m7871_MI,
	&m7872_MI,
	&m7873_MI,
	&m7874_MI,
	&m7875_MI,
	&m7876_MI,
	&m7877_MI,
	&m7878_MI,
	&m7879_MI,
	&m7880_MI,
	NULL
};
static MethodInfo* t1444_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7865_MI,
	&m7863_MI,
	&m7866_MI,
	&m7867_MI,
	&m7868_MI,
	&m7869_MI,
	&m7870_MI,
	&m7873_MI,
	&m7874_MI,
	&m7864_MI,
	&m7865_MI,
	&m7871_MI,
	&m7872_MI,
	&m7875_MI,
	&m7876_MI,
};
static TypeInfo* t1444_ITIs[] = 
{
	&t374_TI,
	&t2046_TI,
	&t1443_TI,
	&t1463_TI,
	&t1453_TI,
	&t2047_TI,
};
static Il2CppInterfaceOffsetPair t1444_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t2046_TI, 5},
	{ &t1443_TI, 6},
	{ &t1463_TI, 6},
	{ &t1453_TI, 6},
	{ &t2047_TI, 13},
};
void t1444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t1444_CustomAttributesCacheGenerator_U3CU3Ef__switch$map1F(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1444__CustomAttributeCache = {
2,
NULL,
&t1444_CustomAttributesCacheGenerator
};
CustomAttributesCache t1444__CustomAttributeCache_U3CU3Ef__switch$map1F = {
1,
NULL,
&t1444_CustomAttributesCacheGenerator_U3CU3Ef__switch$map1F
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1444_0_0_0;
extern Il2CppType t1444_1_0_0;
struct t1444;
extern CustomAttributesCache t1444__CustomAttributeCache;
extern CustomAttributesCache t1444__CustomAttributeCache_U3CU3Ef__switch$map1F;
TypeInfo t1444_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodCall", "System.Runtime.Remoting.Messaging", t1444_MIs, t1444_PIs, t1444_FIs, NULL, &t29_TI, NULL, NULL, &t1444_TI, t1444_ITIs, t1444_VT, &t1444__CustomAttributeCache, &t1444_TI, &t1444_0_0_0, &t1444_1_0_0, t1444_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1444), 0, -1, sizeof(t1444_SFs), 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 21, 10, 11, 0, 0, 19, 6, 6};
#ifndef _MSC_VER
#else
#endif



 void m7881 (t1452 * __this, t29 * p0, MethodInfo* method){
	{
		m7889(__this, p0, &m7889_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1452_TI));
		m7891(__this, (((t1452_SFs*)InitializedTypeInfo(&t1452_TI)->static_fields)->f6), &m7891_MI);
		return;
	}
}
extern MethodInfo m7882_MI;
 void m7882 (t29 * __this, MethodInfo* method){
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 6));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral1595);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral1595;
		t446* L_1 = L_0;
		ArrayElementTypeCheck (L_1, (t7*) &_stringLiteral1596);
		*((t7**)(t7**)SZArrayLdElema(L_1, 1)) = (t7*)(t7*) &_stringLiteral1596;
		t446* L_2 = L_1;
		ArrayElementTypeCheck (L_2, (t7*) &_stringLiteral1597);
		*((t7**)(t7**)SZArrayLdElema(L_2, 2)) = (t7*)(t7*) &_stringLiteral1597;
		t446* L_3 = L_2;
		ArrayElementTypeCheck (L_3, (t7*) &_stringLiteral1598);
		*((t7**)(t7**)SZArrayLdElema(L_3, 3)) = (t7*)(t7*) &_stringLiteral1598;
		t446* L_4 = L_3;
		ArrayElementTypeCheck (L_4, (t7*) &_stringLiteral1599);
		*((t7**)(t7**)SZArrayLdElema(L_4, 4)) = (t7*)(t7*) &_stringLiteral1599;
		t446* L_5 = L_4;
		ArrayElementTypeCheck (L_5, (t7*) &_stringLiteral1600);
		*((t7**)(t7**)SZArrayLdElema(L_5, 5)) = (t7*)(t7*) &_stringLiteral1600;
		((t1452_SFs*)InitializedTypeInfo(&t1452_TI)->static_fields)->f6 = L_5;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern Il2CppType t446_0_0_22;
FieldInfo t1452_f6_FieldInfo = 
{
	"InternalKeys", &t446_0_0_22, &t1452_TI, offsetof(t1452_SFs, f6), &EmptyCustomAttributesCache};
static FieldInfo* t1452_FIs[] =
{
	&t1452_f6_FieldInfo,
	NULL
};
extern Il2CppType t1453_0_0_0;
static ParameterInfo t1452_m7881_ParameterInfos[] = 
{
	{"message", 0, 134221918, &EmptyCustomAttributesCache, &t1453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7881_MI = 
{
	".ctor", (methodPointerType)&m7881, &t1452_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1452_m7881_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3462, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7882_MI = 
{
	".cctor", (methodPointerType)&m7882, &t1452_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3463, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1452_MIs[] =
{
	&m7881_MI,
	&m7882_MI,
	NULL
};
static MethodInfo* t1452_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7890_MI,
	&m7902_MI,
	&m7903_MI,
	&m7904_MI,
	&m7905_MI,
	&m7895_MI,
	&m7896_MI,
	&m7900_MI,
	&m7906_MI,
	&m7901_MI,
	&m7892_MI,
	&m7897_MI,
	&m7898_MI,
	&m7899_MI,
};
static Il2CppInterfaceOffsetPair t1452_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t721_TI, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1452_0_0_0;
extern Il2CppType t1452_1_0_0;
struct t1452;
TypeInfo t1452_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodCallDictionary", "System.Runtime.Remoting.Messaging", t1452_MIs, NULL, t1452_FIs, NULL, &t1446_TI, NULL, NULL, &t1452_TI, NULL, t1452_VT, &EmptyCustomAttributesCache, &t1452_TI, &t1452_0_0_0, &t1452_1_0_0, t1452_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1452), 0, -1, sizeof(t1452_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 2, 0, 1, 0, 0, 18, 0, 3};
#include "t1454.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1454_TI;
#include "t1454MD.h"

#include "t914.h"
extern TypeInfo t914_TI;
#include "t914MD.h"
extern MethodInfo m7886_MI;
extern MethodInfo m3965_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m10122_MI;
extern MethodInfo m4019_MI;
extern MethodInfo m7894_MI;


extern MethodInfo m7883_MI;
 void m7883 (t1454 * __this, t1446 * p0, MethodInfo* method){
	t29 * V_0 = {0};
	t1454 * G_B2_0 = {0};
	t1454 * G_B1_0 = {0};
	t29 * G_B3_0 = {0};
	t1454 * G_B3_1 = {0};
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		t1446 * L_0 = (__this->f0);
		t29 * L_1 = (L_0->f0);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_002f;
		}
	}
	{
		t1446 * L_2 = (__this->f0);
		t29 * L_3 = (L_2->f0);
		t29 * L_4 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3961_MI, L_3);
		V_0 = L_4;
		G_B3_0 = V_0;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = ((t29 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		G_B3_1->f1 = G_B3_0;
		__this->f2 = (-1);
		return;
	}
}
extern MethodInfo m7884_MI;
 t29 * m7884 (t1454 * __this, MethodInfo* method){
	t725  V_0 = {0};
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m7886_MI, __this);
		V_0 = L_0;
		t29 * L_1 = m6685((&V_0), &m6685_MI);
		return L_1;
	}
}
extern MethodInfo m7885_MI;
 bool m7885 (t1454 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_1 = (__this->f2);
		__this->f2 = ((int32_t)(L_1+1));
		int32_t L_2 = (__this->f2);
		t1446 * L_3 = (__this->f0);
		t446* L_4 = (L_3->f2);
		if ((((int32_t)L_2) >= ((int32_t)(((int32_t)(((t20 *)L_4)->max_length))))))
		{
			goto IL_002f;
		}
	}
	{
		return 1;
	}

IL_002f:
	{
		__this->f2 = ((int32_t)-2);
	}

IL_0037:
	{
		t29 * L_5 = (__this->f1);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		return 0;
	}

IL_0041:
	{
		goto IL_0062;
	}

IL_0043:
	{
		t1446 * L_6 = (__this->f0);
		t29 * L_7 = (__this->f1);
		t29 * L_8 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4019_MI, L_7);
		bool L_9 = m7894(L_6, ((t7*)Castclass(L_8, (&t7_TI))), &m7894_MI);
		if (L_9)
		{
			goto IL_0062;
		}
	}
	{
		return 1;
	}

IL_0062:
	{
		t29 * L_10 = (__this->f1);
		bool L_11 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, L_10);
		if (L_11)
		{
			goto IL_0043;
		}
	}
	{
		return 0;
	}
}
 t725  m7886 (t1454 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		t1446 * L_1 = (__this->f0);
		t446* L_2 = (L_1->f2);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = L_3;
		t1446 * L_5 = (__this->f0);
		t1446 * L_6 = (__this->f0);
		t446* L_7 = (L_6->f2);
		int32_t L_8 = (__this->f2);
		int32_t L_9 = L_8;
		t29 * L_10 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7897_MI, L_5, (*(t7**)(t7**)SZArrayLdElema(L_7, L_9)));
		t725  L_11 = {0};
		m3965(&L_11, (*(t7**)(t7**)SZArrayLdElema(L_2, L_4)), L_10, &m3965_MI);
		return L_11;
	}

IL_003e:
	{
		int32_t L_12 = (__this->f2);
		if ((((int32_t)L_12) == ((int32_t)(-1))))
		{
			goto IL_004f;
		}
	}
	{
		t29 * L_13 = (__this->f1);
		if (L_13)
		{
			goto IL_005a;
		}
	}

IL_004f:
	{
		t914 * L_14 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_14, (t7*) &_stringLiteral1614, &m3964_MI);
		il2cpp_codegen_raise_exception(L_14);
	}

IL_005a:
	{
		t29 * L_15 = (__this->f1);
		t725  L_16 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_15);
		return L_16;
	}
}
extern MethodInfo m7887_MI;
 t29 * m7887 (t1454 * __this, MethodInfo* method){
	t725  V_0 = {0};
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m7886_MI, __this);
		V_0 = L_0;
		t29 * L_1 = m6684((&V_0), &m6684_MI);
		return L_1;
	}
}
extern MethodInfo m7888_MI;
 t29 * m7888 (t1454 * __this, MethodInfo* method){
	t725  V_0 = {0};
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m7886_MI, __this);
		V_0 = L_0;
		t29 * L_1 = m6685((&V_0), &m6685_MI);
		return L_1;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern Il2CppType t1446_0_0_1;
FieldInfo t1454_f0_FieldInfo = 
{
	"_methodDictionary", &t1446_0_0_1, &t1454_TI, offsetof(t1454, f0), &EmptyCustomAttributesCache};
extern Il2CppType t722_0_0_1;
FieldInfo t1454_f1_FieldInfo = 
{
	"_hashtableEnum", &t722_0_0_1, &t1454_TI, offsetof(t1454, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1454_f2_FieldInfo = 
{
	"_posMethod", &t44_0_0_1, &t1454_TI, offsetof(t1454, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1454_FIs[] =
{
	&t1454_f0_FieldInfo,
	&t1454_f1_FieldInfo,
	&t1454_f2_FieldInfo,
	NULL
};
static PropertyInfo t1454____Current_PropertyInfo = 
{
	&t1454_TI, "Current", &m7884_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1454____Entry_PropertyInfo = 
{
	&t1454_TI, "Entry", &m7886_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1454____Key_PropertyInfo = 
{
	&t1454_TI, "Key", &m7887_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1454____Value_PropertyInfo = 
{
	&t1454_TI, "Value", &m7888_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1454_PIs[] =
{
	&t1454____Current_PropertyInfo,
	&t1454____Entry_PropertyInfo,
	&t1454____Key_PropertyInfo,
	&t1454____Value_PropertyInfo,
	NULL
};
extern Il2CppType t1446_0_0_0;
extern Il2CppType t1446_0_0_0;
static ParameterInfo t1454_m7883_ParameterInfos[] = 
{
	{"methodDictionary", 0, 134221933, &EmptyCustomAttributesCache, &t1446_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7883_MI = 
{
	".ctor", (methodPointerType)&m7883, &t1454_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1454_m7883_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3482, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7884_MI = 
{
	"get_Current", (methodPointerType)&m7884, &t1454_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 3483, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7885_MI = 
{
	"MoveNext", (methodPointerType)&m7885, &t1454_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, false, 3484, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
MethodInfo m7886_MI = 
{
	"get_Entry", (methodPointerType)&m7886, &t1454_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3485, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7887_MI = 
{
	"get_Key", (methodPointerType)&m7887, &t1454_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, false, 3486, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7888_MI = 
{
	"get_Value", (methodPointerType)&m7888, &t1454_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, false, 3487, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1454_MIs[] =
{
	&m7883_MI,
	&m7884_MI,
	&m7885_MI,
	&m7886_MI,
	&m7887_MI,
	&m7888_MI,
	NULL
};
static MethodInfo* t1454_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7884_MI,
	&m7885_MI,
	&m7886_MI,
	&m7887_MI,
	&m7888_MI,
};
static TypeInfo* t1454_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t1454_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1454_0_0_0;
extern Il2CppType t1454_1_0_0;
struct t1454;
TypeInfo t1454_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DictionaryEnumerator", "", t1454_MIs, t1454_PIs, t1454_FIs, NULL, &t29_TI, NULL, &t1446_TI, &t1454_TI, t1454_ITIs, t1454_VT, &EmptyCustomAttributesCache, &t1454_TI, &t1454_0_0_0, &t1454_1_0_0, t1454_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1454), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 4, 3, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t20_TI;
extern MethodInfo m3954_MI;
extern MethodInfo m3962_MI;
extern MethodInfo m3960_MI;


 void m7889 (t1446 * __this, t29 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f1 = p0;
		return;
	}
}
 t29 * m7890 (t1446 * __this, MethodInfo* method){
	{
		t1454 * L_0 = (t1454 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1454_TI));
		m7883(L_0, __this, &m7883_MI);
		return L_0;
	}
}
 void m7891 (t1446 * __this, t446* p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 t29 * m7892 (t1446 * __this, MethodInfo* method){
	{
		__this->f3 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		return L_0;
	}
}
 t29 * m7893 (t1446 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7892_MI, __this);
		__this->f0 = L_1;
	}

IL_0014:
	{
		t29 * L_2 = (__this->f0);
		return L_2;
	}
}
 bool m7894 (t1446 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	t446* V_1 = {0};
	int32_t V_2 = 0;
	{
		bool L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		t446* L_1 = (__this->f2);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		int32_t L_2 = V_2;
		V_0 = (*(t7**)(t7**)SZArrayLdElema(V_1, L_2));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_3 = m1713(NULL, p0, V_0, &m1713_MI);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		return 1;
	}

IL_0024:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0028:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}
}
 t29 * m7895 (t1446 * __this, t29 * p0, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = ((t7*)Castclass(p0, (&t7_TI)));
		V_1 = 0;
		goto IL_0027;
	}

IL_000b:
	{
		t446* L_0 = (__this->f2);
		int32_t L_1 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, (*(t7**)(t7**)SZArrayLdElema(L_0, L_1)), V_0, &m1713_MI);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7897_MI, __this, V_0);
		return L_3;
	}

IL_0023:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0027:
	{
		t446* L_4 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_4)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		t29 * L_5 = (__this->f0);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		t29 * L_6 = (__this->f0);
		t29 * L_7 = (t29 *)InterfaceFuncInvoker1< t29 *, t29 * >::Invoke(&m3954_MI, L_6, p0);
		return L_7;
	}

IL_0047:
	{
		return NULL;
	}
}
 void m7896 (t1446 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m7900_MI, __this, p0, p1);
		return;
	}
}
 t29 * m7897 (t1446 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_0120;
		}
	}
	{
		if ((((t1446_SFs*)InitializedTypeInfo(&t1446_TI)->static_fields)->f4))
		{
			goto IL_007c;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 8, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1595, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1596, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1597, 2);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1598, 3);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1600, 4);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1599, 5);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1612, 6);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1613, 7);
		((t1446_SFs*)InitializedTypeInfo(&t1446_TI)->static_fields)->f4 = V_1;
	}

IL_007c:
	{
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1446_SFs*)InitializedTypeInfo(&t1446_TI)->static_fields)->f4), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_0120;
		}
	}
	{
		if (V_2 == 0)
		{
			goto IL_00b6;
		}
		if (V_2 == 1)
		{
			goto IL_00c2;
		}
		if (V_2 == 2)
		{
			goto IL_00ce;
		}
		if (V_2 == 3)
		{
			goto IL_00da;
		}
		if (V_2 == 4)
		{
			goto IL_00e6;
		}
		if (V_2 == 5)
		{
			goto IL_00f2;
		}
		if (V_2 == 6)
		{
			goto IL_00fe;
		}
		if (V_2 == 7)
		{
			goto IL_010f;
		}
	}
	{
		goto IL_0120;
	}

IL_00b6:
	{
		t29 * L_2 = (__this->f1);
		t7* L_3 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10201_MI, L_2);
		return L_3;
	}

IL_00c2:
	{
		t29 * L_4 = (__this->f1);
		t7* L_5 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10198_MI, L_4);
		return L_5;
	}

IL_00ce:
	{
		t29 * L_6 = (__this->f1);
		t7* L_7 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10200_MI, L_6);
		return L_7;
	}

IL_00da:
	{
		t29 * L_8 = (__this->f1);
		t29 * L_9 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10199_MI, L_8);
		return L_9;
	}

IL_00e6:
	{
		t29 * L_10 = (__this->f1);
		t1449 * L_11 = (t1449 *)InterfaceFuncInvoker0< t1449 * >::Invoke(&m10196_MI, L_10);
		return L_11;
	}

IL_00f2:
	{
		t29 * L_12 = (__this->f1);
		t316* L_13 = (t316*)InterfaceFuncInvoker0< t316* >::Invoke(&m10195_MI, L_12);
		return (t29 *)L_13;
	}

IL_00fe:
	{
		t29 * L_14 = (__this->f1);
		t316* L_15 = (t316*)InterfaceFuncInvoker0< t316* >::Invoke(&m10203_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t1456_TI))));
		return (t29 *)L_15;
	}

IL_010f:
	{
		t29 * L_16 = (__this->f1);
		t29 * L_17 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10204_MI, ((t29 *)Castclass(L_16, InitializedTypeInfo(&t1456_TI))));
		return L_17;
	}

IL_0120:
	{
		return NULL;
	}
}
 void m7898 (t1446 * __this, t7* p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_00c2;
		}
	}
	{
		if ((((t1446_SFs*)InitializedTypeInfo(&t1446_TI)->static_fields)->f5))
		{
			goto IL_007c;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 8, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1600, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1612, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1613, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1596, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1597, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1598, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1599, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral1595, 2);
		((t1446_SFs*)InitializedTypeInfo(&t1446_TI)->static_fields)->f5 = V_1;
	}

IL_007c:
	{
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1446_SFs*)InitializedTypeInfo(&t1446_TI)->static_fields)->f5), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_00c2;
		}
	}
	{
		if (V_2 == 0)
		{
			goto IL_009f;
		}
		if (V_2 == 1)
		{
			goto IL_00a0;
		}
		if (V_2 == 2)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00c2;
	}

IL_009f:
	{
		return;
	}

IL_00a0:
	{
		t305 * L_2 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_2, (t7*) &_stringLiteral1601, &m1935_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_00ab:
	{
		t29 * L_3 = (__this->f1);
		InterfaceActionInvoker1< t7* >::Invoke(&m10194_MI, ((t29 *)Castclass(L_3, InitializedTypeInfo(&t2046_TI))), ((t7*)Castclass(p1, (&t7_TI))));
		return;
	}

IL_00c2:
	{
		return;
	}
}
 t29 * m7899 (t1446 * __this, MethodInfo* method){
	t731 * V_0 = {0};
	int32_t V_1 = 0;
	t725  V_2 = {0};
	t29 * V_3 = {0};
	t29 * V_4 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0023;
	}

IL_000a:
	{
		t446* L_1 = (__this->f2);
		int32_t L_2 = V_1;
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7897_MI, __this, (*(t7**)(t7**)SZArrayLdElema(L_1, L_2)));
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_0, L_3);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0023:
	{
		t446* L_4 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_4)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		t29 * L_5 = (__this->f0);
		if (!L_5)
		{
			goto IL_0091;
		}
	}
	{
		t29 * L_6 = (__this->f0);
		t29 * L_7 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3961_MI, L_6);
		V_3 = L_7;
	}

IL_0042:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0072;
		}

IL_0044:
		{
			t29 * L_8 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_3);
			V_2 = ((*(t725 *)((t725 *)UnBox (L_8, InitializedTypeInfo(&t725_TI)))));
			t29 * L_9 = m6684((&V_2), &m6684_MI);
			bool L_10 = m7894(__this, ((t7*)Castclass(L_9, (&t7_TI))), &m7894_MI);
			if (L_10)
			{
				goto IL_0072;
			}
		}

IL_0064:
		{
			t29 * L_11 = m6685((&V_2), &m6685_MI);
			VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_0, L_11);
		}

IL_0072:
		{
			bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_3);
			if (L_12)
			{
				goto IL_0044;
			}
		}

IL_007a:
		{
			// IL_007a: leave.s IL_0091
			leaveInstructions[0] = 0x91; // 1
			THROW_SENTINEL(IL_0091);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_007c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_007c;
	}

IL_007c:
	{ // begin finally (depth: 1)
		{
			V_4 = ((t29 *)IsInst(V_3, InitializedTypeInfo(&t324_TI)));
			if (V_4)
			{
				goto IL_0089;
			}
		}

IL_0088:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x91:
					goto IL_0091;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0089:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_4);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x91:
					goto IL_0091;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0091:
	{
		return V_0;
	}
}
 void m7900 (t1446 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = ((t7*)Castclass(p0, (&t7_TI)));
		V_1 = 0;
		goto IL_0028;
	}

IL_000b:
	{
		t446* L_0 = (__this->f2);
		int32_t L_1 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, (*(t7**)(t7**)SZArrayLdElema(L_0, L_1)), V_0, &m1713_MI);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		VirtActionInvoker2< t7*, t29 * >::Invoke(&m7898_MI, __this, V_0, p1);
		return;
	}

IL_0024:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0028:
	{
		t446* L_3 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_3)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		t29 * L_4 = (__this->f0);
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		t29 * L_5 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7892_MI, __this);
		__this->f0 = L_5;
	}

IL_0047:
	{
		t29 * L_6 = (__this->f0);
		InterfaceActionInvoker2< t29 *, t29 * >::Invoke(&m3955_MI, L_6, p0, p1);
		return;
	}
}
 void m7901 (t1446 * __this, t29 * p0, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = ((t7*)Castclass(p0, (&t7_TI)));
		V_1 = 0;
		goto IL_002a;
	}

IL_000b:
	{
		t446* L_0 = (__this->f2);
		int32_t L_1 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1713(NULL, (*(t7**)(t7**)SZArrayLdElema(L_0, L_1)), V_0, &m1713_MI);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1601, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0026:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_002a:
	{
		t446* L_4 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_4)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		t29 * L_5 = (__this->f0);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		t29 * L_6 = (__this->f0);
		InterfaceActionInvoker1< t29 * >::Invoke(&m3962_MI, L_6, p0);
	}

IL_0049:
	{
		return;
	}
}
 int32_t m7902 (t1446 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		t29 * L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m3953_MI, L_1);
		t446* L_3 = (__this->f2);
		return ((int32_t)(L_2+(((int32_t)(((t20 *)L_3)->max_length)))));
	}

IL_001d:
	{
		t446* L_4 = (__this->f2);
		return (((int32_t)(((t20 *)L_4)->max_length)));
	}
}
 bool m7903 (t1446 * __this, MethodInfo* method){
	{
		return 0;
	}
}
 t29 * m7904 (t1446 * __this, MethodInfo* method){
	{
		return __this;
	}
}
 void m7905 (t1446 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7899_MI, __this);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, L_0, p0, p1);
		return;
	}
}
 t29 * m7906 (t1446 * __this, MethodInfo* method){
	{
		t1454 * L_0 = (t1454 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1454_TI));
		m7883(L_0, __this, &m7883_MI);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
extern Il2CppType t721_0_0_1;
FieldInfo t1446_f0_FieldInfo = 
{
	"_internalProperties", &t721_0_0_1, &t1446_TI, offsetof(t1446, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1453_0_0_4;
FieldInfo t1446_f1_FieldInfo = 
{
	"_message", &t1453_0_0_4, &t1446_TI, offsetof(t1446, f1), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t1446_f2_FieldInfo = 
{
	"_methodKeys", &t446_0_0_1, &t1446_TI, offsetof(t1446, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1446_f3_FieldInfo = 
{
	"_ownProperties", &t40_0_0_1, &t1446_TI, offsetof(t1446, f3), &EmptyCustomAttributesCache};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1446__CustomAttributeCache_U3CU3Ef__switch$map21;
FieldInfo t1446_f4_FieldInfo = 
{
	"<>f__switch$map21", &t485_0_0_17, &t1446_TI, offsetof(t1446_SFs, f4), &t1446__CustomAttributeCache_U3CU3Ef__switch$map21};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1446__CustomAttributeCache_U3CU3Ef__switch$map22;
FieldInfo t1446_f5_FieldInfo = 
{
	"<>f__switch$map22", &t485_0_0_17, &t1446_TI, offsetof(t1446_SFs, f5), &t1446__CustomAttributeCache_U3CU3Ef__switch$map22};
static FieldInfo* t1446_FIs[] =
{
	&t1446_f0_FieldInfo,
	&t1446_f1_FieldInfo,
	&t1446_f2_FieldInfo,
	&t1446_f3_FieldInfo,
	&t1446_f4_FieldInfo,
	&t1446_f5_FieldInfo,
	NULL
};
static PropertyInfo t1446____MethodKeys_PropertyInfo = 
{
	&t1446_TI, "MethodKeys", NULL, &m7891_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1446____Item_PropertyInfo = 
{
	&t1446_TI, "Item", &m7895_MI, &m7896_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1446____Values_PropertyInfo = 
{
	&t1446_TI, "Values", &m7899_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1446____Count_PropertyInfo = 
{
	&t1446_TI, "Count", &m7902_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1446____IsSynchronized_PropertyInfo = 
{
	&t1446_TI, "IsSynchronized", &m7903_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1446____SyncRoot_PropertyInfo = 
{
	&t1446_TI, "SyncRoot", &m7904_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1446_PIs[] =
{
	&t1446____MethodKeys_PropertyInfo,
	&t1446____Item_PropertyInfo,
	&t1446____Values_PropertyInfo,
	&t1446____Count_PropertyInfo,
	&t1446____IsSynchronized_PropertyInfo,
	&t1446____SyncRoot_PropertyInfo,
	NULL
};
extern Il2CppType t1453_0_0_0;
static ParameterInfo t1446_m7889_ParameterInfos[] = 
{
	{"message", 0, 134221919, &EmptyCustomAttributesCache, &t1453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7889_MI = 
{
	".ctor", (methodPointerType)&m7889, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1446_m7889_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3464, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7890_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m7890, &t1446_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, false, 3465, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_0_0_0;
static ParameterInfo t1446_m7891_ParameterInfos[] = 
{
	{"value", 0, 134221920, &EmptyCustomAttributesCache, &t446_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7891_MI = 
{
	"set_MethodKeys", (methodPointerType)&m7891, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1446_m7891_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 3466, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7892_MI = 
{
	"AllocInternalProperties", (methodPointerType)&m7892, &t1446_TI, &t721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 452, 0, 14, 0, false, false, 3467, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7893_MI = 
{
	"GetInternalProperties", (methodPointerType)&m7893, &t1446_TI, &t721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 3468, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1446_m7894_ParameterInfos[] = 
{
	{"key", 0, 134221921, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7894_MI = 
{
	"IsOverridenKey", (methodPointerType)&m7894, &t1446_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1446_m7894_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 3469, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1446_m7895_ParameterInfos[] = 
{
	{"key", 0, 134221922, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7895_MI = 
{
	"get_Item", (methodPointerType)&m7895, &t1446_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1446_m7895_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 9, 1, false, false, 3470, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1446_m7896_ParameterInfos[] = 
{
	{"key", 0, 134221923, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221924, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7896_MI = 
{
	"set_Item", (methodPointerType)&m7896, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1446_m7896_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 10, 2, false, false, 3471, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1446_m7897_ParameterInfos[] = 
{
	{"key", 0, 134221925, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7897_MI = 
{
	"GetMethodProperty", (methodPointerType)&m7897, &t1446_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1446_m7897_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 15, 1, false, false, 3472, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1446_m7898_ParameterInfos[] = 
{
	{"key", 0, 134221926, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134221927, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7898_MI = 
{
	"SetMethodProperty", (methodPointerType)&m7898, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1446_m7898_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 16, 2, false, false, 3473, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t674_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7899_MI = 
{
	"get_Values", (methodPointerType)&m7899, &t1446_TI, &t674_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 17, 0, false, false, 3474, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1446_m7900_ParameterInfos[] = 
{
	{"key", 0, 134221928, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221929, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7900_MI = 
{
	"Add", (methodPointerType)&m7900, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1446_m7900_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 11, 2, false, false, 3475, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1446_m7901_ParameterInfos[] = 
{
	{"key", 0, 134221930, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7901_MI = 
{
	"Remove", (methodPointerType)&m7901, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1446_m7901_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 13, 1, false, false, 3476, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7902_MI = 
{
	"get_Count", (methodPointerType)&m7902, &t1446_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 3477, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7903_MI = 
{
	"get_IsSynchronized", (methodPointerType)&m7903, &t1446_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3478, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7904_MI = 
{
	"get_SyncRoot", (methodPointerType)&m7904, &t1446_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, false, 3479, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1446_m7905_ParameterInfos[] = 
{
	{"array", 0, 134221931, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134221932, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7905_MI = 
{
	"CopyTo", (methodPointerType)&m7905, &t1446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t1446_m7905_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 8, 2, false, false, 3480, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7906_MI = 
{
	"GetEnumerator", (methodPointerType)&m7906, &t1446_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 12, 0, false, false, 3481, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1446_MIs[] =
{
	&m7889_MI,
	&m7890_MI,
	&m7891_MI,
	&m7892_MI,
	&m7893_MI,
	&m7894_MI,
	&m7895_MI,
	&m7896_MI,
	&m7897_MI,
	&m7898_MI,
	&m7899_MI,
	&m7900_MI,
	&m7901_MI,
	&m7902_MI,
	&m7903_MI,
	&m7904_MI,
	&m7905_MI,
	&m7906_MI,
	NULL
};
extern TypeInfo t1454_TI;
static TypeInfo* t1446_TI__nestedTypes[2] =
{
	&t1454_TI,
	NULL
};
static MethodInfo* t1446_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7890_MI,
	&m7902_MI,
	&m7903_MI,
	&m7904_MI,
	&m7905_MI,
	&m7895_MI,
	&m7896_MI,
	&m7900_MI,
	&m7906_MI,
	&m7901_MI,
	&m7892_MI,
	&m7897_MI,
	&m7898_MI,
	&m7899_MI,
};
static TypeInfo* t1446_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t721_TI,
};
static Il2CppInterfaceOffsetPair t1446_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t721_TI, 9},
};
extern TypeInfo t425_TI;
#include "t425.h"
#include "t425MD.h"
extern MethodInfo m2025_MI;
void t1446_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t425 * tmp;
		tmp = (t425 *)il2cpp_codegen_object_new (&t425_TI);
		m2025(tmp, il2cpp_codegen_string_new_wrapper("Item"), &m2025_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1446_CustomAttributesCacheGenerator_U3CU3Ef__switch$map21(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1446_CustomAttributesCacheGenerator_U3CU3Ef__switch$map22(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1446__CustomAttributeCache = {
1,
NULL,
&t1446_CustomAttributesCacheGenerator
};
CustomAttributesCache t1446__CustomAttributeCache_U3CU3Ef__switch$map21 = {
1,
NULL,
&t1446_CustomAttributesCacheGenerator_U3CU3Ef__switch$map21
};
CustomAttributesCache t1446__CustomAttributeCache_U3CU3Ef__switch$map22 = {
1,
NULL,
&t1446_CustomAttributesCacheGenerator_U3CU3Ef__switch$map22
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1446_1_0_0;
struct t1446;
extern CustomAttributesCache t1446__CustomAttributeCache;
extern CustomAttributesCache t1446__CustomAttributeCache_U3CU3Ef__switch$map21;
extern CustomAttributesCache t1446__CustomAttributeCache_U3CU3Ef__switch$map22;
TypeInfo t1446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodDictionary", "System.Runtime.Remoting.Messaging", t1446_MIs, t1446_PIs, t1446_FIs, NULL, &t29_TI, t1446_TI__nestedTypes, NULL, &t1446_TI, t1446_ITIs, t1446_VT, &t1446__CustomAttributeCache, &t1446_TI, &t1446_0_0_0, &t1446_1_0_0, t1446_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1446), 0, -1, sizeof(t1446_SFs), 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 18, 6, 6, 0, 1, 18, 3, 3};
#include "t1455.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1455_TI;
#include "t1455MD.h"

extern TypeInfo t295_TI;


extern MethodInfo m7907_MI;
 void m7907 (t1455 * __this, t29 * p0, MethodInfo* method){
	{
		m7889(__this, p0, &m7889_MI);
		t295 * L_0 = (t295 *)InterfaceFuncInvoker0< t295 * >::Invoke(&m10202_MI, p0);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1455_TI));
		m7891(__this, (((t1455_SFs*)InitializedTypeInfo(&t1455_TI)->static_fields)->f6), &m7891_MI);
		goto IL_0027;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1455_TI));
		m7891(__this, (((t1455_SFs*)InitializedTypeInfo(&t1455_TI)->static_fields)->f7), &m7891_MI);
	}

IL_0027:
	{
		return;
	}
}
extern MethodInfo m7908_MI;
 void m7908 (t29 * __this, MethodInfo* method){
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 7));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral1595);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral1595;
		t446* L_1 = L_0;
		ArrayElementTypeCheck (L_1, (t7*) &_stringLiteral1596);
		*((t7**)(t7**)SZArrayLdElema(L_1, 1)) = (t7*)(t7*) &_stringLiteral1596;
		t446* L_2 = L_1;
		ArrayElementTypeCheck (L_2, (t7*) &_stringLiteral1597);
		*((t7**)(t7**)SZArrayLdElema(L_2, 2)) = (t7*)(t7*) &_stringLiteral1597;
		t446* L_3 = L_2;
		ArrayElementTypeCheck (L_3, (t7*) &_stringLiteral1598);
		*((t7**)(t7**)SZArrayLdElema(L_3, 3)) = (t7*)(t7*) &_stringLiteral1598;
		t446* L_4 = L_3;
		ArrayElementTypeCheck (L_4, (t7*) &_stringLiteral1612);
		*((t7**)(t7**)SZArrayLdElema(L_4, 4)) = (t7*)(t7*) &_stringLiteral1612;
		t446* L_5 = L_4;
		ArrayElementTypeCheck (L_5, (t7*) &_stringLiteral1613);
		*((t7**)(t7**)SZArrayLdElema(L_5, 5)) = (t7*)(t7*) &_stringLiteral1613;
		t446* L_6 = L_5;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral1600);
		*((t7**)(t7**)SZArrayLdElema(L_6, 6)) = (t7*)(t7*) &_stringLiteral1600;
		((t1455_SFs*)InitializedTypeInfo(&t1455_TI)->static_fields)->f6 = L_6;
		t446* L_7 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 1));
		ArrayElementTypeCheck (L_7, (t7*) &_stringLiteral1600);
		*((t7**)(t7**)SZArrayLdElema(L_7, 0)) = (t7*)(t7*) &_stringLiteral1600;
		((t1455_SFs*)InitializedTypeInfo(&t1455_TI)->static_fields)->f7 = L_7;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern Il2CppType t446_0_0_22;
FieldInfo t1455_f6_FieldInfo = 
{
	"InternalReturnKeys", &t446_0_0_22, &t1455_TI, offsetof(t1455_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_22;
FieldInfo t1455_f7_FieldInfo = 
{
	"InternalExceptionKeys", &t446_0_0_22, &t1455_TI, offsetof(t1455_SFs, f7), &EmptyCustomAttributesCache};
static FieldInfo* t1455_FIs[] =
{
	&t1455_f6_FieldInfo,
	&t1455_f7_FieldInfo,
	NULL
};
extern Il2CppType t1456_0_0_0;
static ParameterInfo t1455_m7907_ParameterInfos[] = 
{
	{"message", 0, 134221934, &EmptyCustomAttributesCache, &t1456_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7907_MI = 
{
	".ctor", (methodPointerType)&m7907, &t1455_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1455_m7907_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3488, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7908_MI = 
{
	".cctor", (methodPointerType)&m7908, &t1455_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3489, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1455_MIs[] =
{
	&m7907_MI,
	&m7908_MI,
	NULL
};
static MethodInfo* t1455_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7890_MI,
	&m7902_MI,
	&m7903_MI,
	&m7904_MI,
	&m7905_MI,
	&m7895_MI,
	&m7896_MI,
	&m7900_MI,
	&m7906_MI,
	&m7901_MI,
	&m7892_MI,
	&m7897_MI,
	&m7898_MI,
	&m7899_MI,
};
static Il2CppInterfaceOffsetPair t1455_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t721_TI, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1455_0_0_0;
extern Il2CppType t1455_1_0_0;
struct t1455;
TypeInfo t1455_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodReturnDictionary", "System.Runtime.Remoting.Messaging", t1455_MIs, NULL, t1455_FIs, NULL, &t1446_TI, NULL, NULL, &t1455_TI, NULL, t1455_VT, &EmptyCustomAttributesCache, &t1455_TI, &t1455_0_0_0, &t1455_1_0_0, t1455_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1455), 0, -1, sizeof(t1455_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 2, 0, 2, 0, 0, 18, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1441_TI;
#include "t1441MD.h"

#include "t1377.h"
extern TypeInfo t1377_TI;
#include "t1377MD.h"
extern MethodInfo m7633_MI;
extern MethodInfo m7625_MI;
extern MethodInfo m7632_MI;
extern MethodInfo m7918_MI;


extern MethodInfo m7909_MI;
 t316* m7909 (t1441 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m7910_MI;
 t1449 * m7910 (t1441 * __this, MethodInfo* method){
	{
		t1449 * L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m7911_MI;
 t636 * m7911 (t1441 * __this, MethodInfo* method){
	{
		t1377 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m7912_MI;
 t7* m7912 (t1441 * __this, MethodInfo* method){
	{
		t1377 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_000e:
	{
		t1377 * L_1 = (__this->f0);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7633_MI, L_1);
		return L_2;
	}
}
extern MethodInfo m7913_MI;
 t29 * m7913 (t1441 * __this, MethodInfo* method){
	t638* V_0 = {0};
	int32_t V_1 = 0;
	{
		t537* L_0 = (__this->f7);
		if (L_0)
		{
			goto IL_0040;
		}
	}
	{
		t1377 * L_1 = (__this->f0);
		t638* L_2 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m7625_MI, L_1);
		V_0 = L_2;
		__this->f7 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), (((int32_t)(((t20 *)V_0)->max_length)))));
		V_1 = 0;
		goto IL_003a;
	}

IL_0026:
	{
		t537* L_3 = (__this->f7);
		int32_t L_4 = V_1;
		t42 * L_5 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_4)));
		ArrayElementTypeCheck (L_3, L_5);
		*((t42 **)(t42 **)SZArrayLdElema(L_3, V_1)) = (t42 *)L_5;
		V_1 = ((int32_t)(V_1+1));
	}

IL_003a:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0026;
		}
	}

IL_0040:
	{
		t537* L_6 = (__this->f7);
		return (t29 *)L_6;
	}
}
extern MethodInfo m7914_MI;
 t7* m7914 (t1441 * __this, MethodInfo* method){
	{
		t1377 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_000e:
	{
		t1377 * L_1 = (__this->f0);
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7632_MI, L_1);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m7915_MI;
 t7* m7915 (t1441 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m7916_MI;
 void m7916 (t1441 * __this, t7* p0, MethodInfo* method){
	{
		__this->f6 = p0;
		return;
	}
}
extern MethodInfo m7917_MI;
 t295 * m7917 (t1441 * __this, MethodInfo* method){
	{
		t295 * L_0 = (__this->f5);
		return L_0;
	}
}
 int32_t m7918 (t1441 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	uint8_t V_1 = 0x0;
	t781* V_2 = {0};
	int32_t V_3 = 0;
	{
		t316* L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		V_0 = 0;
		t781* L_1 = (__this->f2);
		V_2 = L_1;
		V_3 = 0;
		goto IL_0028;
	}

IL_0017:
	{
		int32_t L_2 = V_3;
		V_1 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(V_2, L_2));
		if (!((int32_t)((int32_t)V_1&(int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0024:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0028:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_2)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		return V_0;
	}
}
extern MethodInfo m7919_MI;
 t316* m7919 (t1441 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t316* V_3 = {0};
	uint8_t V_4 = 0x0;
	t781* V_5 = {0};
	int32_t V_6 = 0;
	{
		t316* L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (t316*)NULL;
	}

IL_000a:
	{
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7918_MI, __this);
		V_2 = L_1;
		V_3 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), V_2));
		int32_t L_2 = 0;
		V_1 = L_2;
		V_0 = L_2;
		t781* L_3 = (__this->f2);
		V_5 = L_3;
		V_6 = 0;
		goto IL_004f;
	}

IL_0029:
	{
		int32_t L_4 = V_6;
		V_4 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(V_5, L_4));
		if (!((int32_t)((int32_t)V_4&(int32_t)2)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_5 = V_1;
		V_1 = ((int32_t)(L_5+1));
		t316* L_6 = (__this->f1);
		int32_t L_7 = V_0;
		ArrayElementTypeCheck (V_3, (*(t29 **)(t29 **)SZArrayLdElema(L_6, L_7)));
		*((t29 **)(t29 **)SZArrayLdElema(V_3, L_5)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema(L_6, L_7));
	}

IL_0045:
	{
		V_0 = ((int32_t)(V_0+1));
		V_6 = ((int32_t)(V_6+1));
	}

IL_004f:
	{
		if ((((int32_t)V_6) < ((int32_t)(((int32_t)(((t20 *)V_5)->max_length))))))
		{
			goto IL_0029;
		}
	}
	{
		return V_3;
	}
}
extern MethodInfo m7920_MI;
 t29 * m7920 (t1441 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f4);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern Il2CppType t1377_0_0_1;
FieldInfo t1441_f0_FieldInfo = 
{
	"method", &t1377_0_0_1, &t1441_TI, offsetof(t1441, f0), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t1441_f1_FieldInfo = 
{
	"args", &t316_0_0_1, &t1441_TI, offsetof(t1441, f1), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t1441_f2_FieldInfo = 
{
	"arg_types", &t781_0_0_1, &t1441_TI, offsetof(t1441, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1449_0_0_6;
FieldInfo t1441_f3_FieldInfo = 
{
	"ctx", &t1449_0_0_6, &t1441_TI, offsetof(t1441, f3), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_6;
FieldInfo t1441_f4_FieldInfo = 
{
	"rval", &t29_0_0_6, &t1441_TI, offsetof(t1441, f4), &EmptyCustomAttributesCache};
extern Il2CppType t295_0_0_6;
FieldInfo t1441_f5_FieldInfo = 
{
	"exc", &t295_0_0_6, &t1441_TI, offsetof(t1441, f5), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1441_f6_FieldInfo = 
{
	"uri", &t7_0_0_1, &t1441_TI, offsetof(t1441, f6), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_1;
FieldInfo t1441_f7_FieldInfo = 
{
	"methodSignature", &t537_0_0_1, &t1441_TI, offsetof(t1441, f7), &EmptyCustomAttributesCache};
static FieldInfo* t1441_FIs[] =
{
	&t1441_f0_FieldInfo,
	&t1441_f1_FieldInfo,
	&t1441_f2_FieldInfo,
	&t1441_f3_FieldInfo,
	&t1441_f4_FieldInfo,
	&t1441_f5_FieldInfo,
	&t1441_f6_FieldInfo,
	&t1441_f7_FieldInfo,
	NULL
};
static PropertyInfo t1441____Args_PropertyInfo = 
{
	&t1441_TI, "Args", &m7909_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____LogicalCallContext_PropertyInfo = 
{
	&t1441_TI, "LogicalCallContext", &m7910_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____MethodBase_PropertyInfo = 
{
	&t1441_TI, "MethodBase", &m7911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____MethodName_PropertyInfo = 
{
	&t1441_TI, "MethodName", &m7912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____MethodSignature_PropertyInfo = 
{
	&t1441_TI, "MethodSignature", &m7913_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____TypeName_PropertyInfo = 
{
	&t1441_TI, "TypeName", &m7914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____Uri_PropertyInfo = 
{
	&t1441_TI, "Uri", &m7915_MI, &m7916_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____Exception_PropertyInfo = 
{
	&t1441_TI, "Exception", &m7917_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____OutArgCount_PropertyInfo = 
{
	&t1441_TI, "OutArgCount", &m7918_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____OutArgs_PropertyInfo = 
{
	&t1441_TI, "OutArgs", &m7919_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1441____ReturnValue_PropertyInfo = 
{
	&t1441_TI, "ReturnValue", &m7920_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1441_PIs[] =
{
	&t1441____Args_PropertyInfo,
	&t1441____LogicalCallContext_PropertyInfo,
	&t1441____MethodBase_PropertyInfo,
	&t1441____MethodName_PropertyInfo,
	&t1441____MethodSignature_PropertyInfo,
	&t1441____TypeName_PropertyInfo,
	&t1441____Uri_PropertyInfo,
	&t1441____Exception_PropertyInfo,
	&t1441____OutArgCount_PropertyInfo,
	&t1441____OutArgs_PropertyInfo,
	&t1441____ReturnValue_PropertyInfo,
	NULL
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7909_MI = 
{
	"get_Args", (methodPointerType)&m7909, &t1441_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 3490, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1449_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7910_MI = 
{
	"get_LogicalCallContext", (methodPointerType)&m7910, &t1441_TI, &t1449_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3491, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7911_MI = 
{
	"get_MethodBase", (methodPointerType)&m7911, &t1441_TI, &t636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, false, 3492, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7912_MI = 
{
	"get_MethodName", (methodPointerType)&m7912, &t1441_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, false, 3493, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7913_MI = 
{
	"get_MethodSignature", (methodPointerType)&m7913, &t1441_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, false, 3494, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7914_MI = 
{
	"get_TypeName", (methodPointerType)&m7914, &t1441_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, false, 3495, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7915_MI = 
{
	"get_Uri", (methodPointerType)&m7915, &t1441_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 11, 0, false, false, 3496, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1441_m7916_ParameterInfos[] = 
{
	{"value", 0, 134221935, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7916_MI = 
{
	"set_Uri", (methodPointerType)&m7916, &t1441_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1441_m7916_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 4, 1, false, false, 3497, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t295_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7917_MI = 
{
	"get_Exception", (methodPointerType)&m7917, &t1441_TI, &t295_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 12, 0, false, false, 3498, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7918_MI = 
{
	"get_OutArgCount", (methodPointerType)&m7918, &t1441_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 15, 0, false, false, 3499, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7919_MI = 
{
	"get_OutArgs", (methodPointerType)&m7919, &t1441_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 13, 0, false, false, 3500, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7920_MI = 
{
	"get_ReturnValue", (methodPointerType)&m7920, &t1441_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 14, 0, false, false, 3501, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1441_MIs[] =
{
	&m7909_MI,
	&m7910_MI,
	&m7911_MI,
	&m7912_MI,
	&m7913_MI,
	&m7914_MI,
	&m7915_MI,
	&m7916_MI,
	&m7917_MI,
	&m7918_MI,
	&m7919_MI,
	&m7920_MI,
	NULL
};
static MethodInfo* t1441_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7916_MI,
	&m7909_MI,
	&m7910_MI,
	&m7911_MI,
	&m7912_MI,
	&m7913_MI,
	&m7914_MI,
	&m7915_MI,
	&m7917_MI,
	&m7919_MI,
	&m7920_MI,
	&m7918_MI,
};
static TypeInfo* t1441_ITIs[] = 
{
	&t2046_TI,
	&t1443_TI,
	&t1463_TI,
	&t1453_TI,
	&t1456_TI,
};
static Il2CppInterfaceOffsetPair t1441_IOs[] = 
{
	{ &t2046_TI, 4},
	{ &t1443_TI, 5},
	{ &t1463_TI, 5},
	{ &t1453_TI, 5},
	{ &t1456_TI, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1441_1_0_0;
struct t1441;
TypeInfo t1441_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoMethodMessage", "System.Runtime.Remoting.Messaging", t1441_MIs, t1441_PIs, t1441_FIs, NULL, &t29_TI, NULL, NULL, &t1441_TI, t1441_ITIs, t1441_VT, &EmptyCustomAttributesCache, &t1441_TI, &t1441_0_0_0, &t1441_1_0_0, t1441_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1441), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 12, 11, 8, 0, 0, 16, 5, 5};
#include "t1457.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1457_TI;
#include "t1457MD.h"



extern MethodInfo m7921_MI;
 void m7921 (t1457 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7922_MI;
 t29 * m7922 (t1457 * __this, t29 * p0, t733 * p1, t735  p2, t29 * p3, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7921_MI = 
{
	".ctor", (methodPointerType)&m7921, &t1457_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3502, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t1458_0_0_0;
extern Il2CppType t1458_0_0_0;
static ParameterInfo t1457_m7922_ParameterInfos[] = 
{
	{"obj", 0, 134221936, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"si", 1, 134221937, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"sc", 2, 134221938, &EmptyCustomAttributesCache, &t735_0_0_0},
	{"selector", 3, 134221939, &EmptyCustomAttributesCache, &t1458_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t735_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7922_MI = 
{
	"SetObjectData", (methodPointerType)&m7922, &t1457_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t735_t29, t1457_m7922_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 5, 4, false, false, 3503, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1457_MIs[] =
{
	&m7921_MI,
	&m7922_MI,
	NULL
};
static MethodInfo* t1457_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7922_MI,
	&m7922_MI,
};
extern TypeInfo t1461_TI;
static TypeInfo* t1457_ITIs[] = 
{
	&t1461_TI,
};
static Il2CppInterfaceOffsetPair t1457_IOs[] = 
{
	{ &t1461_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1457_0_0_0;
extern Il2CppType t1457_1_0_0;
struct t1457;
TypeInfo t1457_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemotingSurrogate", "System.Runtime.Remoting.Messaging", t1457_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1457_TI, t1457_ITIs, t1457_VT, &EmptyCustomAttributesCache, &t1457_TI, &t1457_0_0_0, &t1457_1_0_0, t1457_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1457), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 6, 1, 1};
#include "t1459.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1459_TI;
#include "t1459MD.h"

extern MethodInfo m3988_MI;


extern MethodInfo m7923_MI;
 void m7923 (t1459 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7924_MI;
 t29 * m7924 (t1459 * __this, t29 * p0, t733 * p1, t735  p2, t29 * p3, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1615, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7923_MI = 
{
	".ctor", (methodPointerType)&m7923, &t1459_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3504, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t1458_0_0_0;
static ParameterInfo t1459_m7924_ParameterInfos[] = 
{
	{"obj", 0, 134221940, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"si", 1, 134221941, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"sc", 2, 134221942, &EmptyCustomAttributesCache, &t735_0_0_0},
	{"selector", 3, 134221943, &EmptyCustomAttributesCache, &t1458_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t735_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7924_MI = 
{
	"SetObjectData", (methodPointerType)&m7924, &t1459_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t735_t29, t1459_m7924_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 5, 4, false, false, 3505, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1459_MIs[] =
{
	&m7923_MI,
	&m7924_MI,
	NULL
};
static MethodInfo* t1459_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7924_MI,
	&m7924_MI,
};
static TypeInfo* t1459_ITIs[] = 
{
	&t1461_TI,
};
static Il2CppInterfaceOffsetPair t1459_IOs[] = 
{
	{ &t1461_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1459_0_0_0;
extern Il2CppType t1459_1_0_0;
struct t1459;
TypeInfo t1459_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObjRefSurrogate", "System.Runtime.Remoting.Messaging", t1459_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1459_TI, t1459_ITIs, t1459_VT, &EmptyCustomAttributesCache, &t1459_TI, &t1459_0_0_0, &t1459_1_0_0, t1459_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1459), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 6, 1, 1};
#include "t1460.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1460_TI;
#include "t1460MD.h"

#include "t1466.h"
extern TypeInfo t1466_TI;
extern TypeInfo t1458_TI;
extern Il2CppType t1466_0_0_0;
extern MethodInfo m6000_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m10205_MI;


extern MethodInfo m7925_MI;
 void m7925 (t1460 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7926_MI;
 void m7926 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t1466_0_0_0), &m1554_MI);
		((t1460_SFs*)InitializedTypeInfo(&t1460_TI)->static_fields)->f0 = L_0;
		t1459 * L_1 = (t1459 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1459_TI));
		m7923(L_1, &m7923_MI);
		((t1460_SFs*)InitializedTypeInfo(&t1460_TI)->static_fields)->f1 = L_1;
		t1457 * L_2 = (t1457 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1457_TI));
		m7921(L_2, &m7921_MI);
		((t1460_SFs*)InitializedTypeInfo(&t1460_TI)->static_fields)->f2 = L_2;
		return;
	}
}
extern MethodInfo m7927_MI;
 t29 * m7927 (t1460 * __this, t42 * p0, t735  p1, t29 ** p2, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6000_MI, p0);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)__this;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1460_TI));
		return (((t1460_SFs*)InitializedTypeInfo(&t1460_TI)->static_fields)->f2);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1460_TI));
		bool L_1 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, (((t1460_SFs*)InitializedTypeInfo(&t1460_TI)->static_fields)->f0), p0);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)__this;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1460_TI));
		return (((t1460_SFs*)InitializedTypeInfo(&t1460_TI)->static_fields)->f1);
	}

IL_0027:
	{
		t29 * L_2 = (__this->f3);
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		t29 * L_3 = (__this->f3);
		t29 * L_4 = (t29 *)InterfaceFuncInvoker3< t29 *, t42 *, t735 , t29 ** >::Invoke(&m10205_MI, L_3, p0, p1, p2);
		return L_4;
	}

IL_003e:
	{
		*((t29 **)(p2)) = (t29 *)NULL;
		return (t29 *)NULL;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern Il2CppType t42_0_0_17;
FieldInfo t1460_f0_FieldInfo = 
{
	"s_cachedTypeObjRef", &t42_0_0_17, &t1460_TI, offsetof(t1460_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1459_0_0_17;
FieldInfo t1460_f1_FieldInfo = 
{
	"_objRefSurrogate", &t1459_0_0_17, &t1460_TI, offsetof(t1460_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1457_0_0_17;
FieldInfo t1460_f2_FieldInfo = 
{
	"_objRemotingSurrogate", &t1457_0_0_17, &t1460_TI, offsetof(t1460_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1458_0_0_1;
FieldInfo t1460_f3_FieldInfo = 
{
	"_next", &t1458_0_0_1, &t1460_TI, offsetof(t1460, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1460_FIs[] =
{
	&t1460_f0_FieldInfo,
	&t1460_f1_FieldInfo,
	&t1460_f2_FieldInfo,
	&t1460_f3_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7925_MI = 
{
	".ctor", (methodPointerType)&m7925, &t1460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3506, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7926_MI = 
{
	".cctor", (methodPointerType)&m7926, &t1460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3507, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t1458_1_0_2;
extern Il2CppType t1458_1_0_0;
static ParameterInfo t1460_m7927_ParameterInfos[] = 
{
	{"type", 0, 134221944, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"context", 1, 134221945, &EmptyCustomAttributesCache, &t735_0_0_0},
	{"ssout", 2, 134221946, &EmptyCustomAttributesCache, &t1458_1_0_2},
};
extern Il2CppType t1461_0_0_0;
extern void* RuntimeInvoker_t29_t29_t735_t2050 (MethodInfo* method, void* obj, void** args);
MethodInfo m7927_MI = 
{
	"GetSurrogate", (methodPointerType)&m7927, &t1460_TI, &t1461_0_0_0, RuntimeInvoker_t29_t29_t735_t2050, t1460_m7927_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 5, 3, false, false, 3508, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1460_MIs[] =
{
	&m7925_MI,
	&m7926_MI,
	&m7927_MI,
	NULL
};
static MethodInfo* t1460_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7927_MI,
	&m7927_MI,
};
static TypeInfo* t1460_ITIs[] = 
{
	&t1458_TI,
};
static Il2CppInterfaceOffsetPair t1460_IOs[] = 
{
	{ &t1458_TI, 4},
};
void t1460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1460__CustomAttributeCache = {
1,
NULL,
&t1460_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1460_0_0_0;
extern Il2CppType t1460_1_0_0;
struct t1460;
extern CustomAttributesCache t1460__CustomAttributeCache;
TypeInfo t1460_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemotingSurrogateSelector", "System.Runtime.Remoting.Messaging", t1460_MIs, NULL, t1460_FIs, NULL, &t29_TI, NULL, NULL, &t1460_TI, t1460_ITIs, t1460_VT, &t1460__CustomAttributeCache, &t1460_TI, &t1460_0_0_0, &t1460_1_0_0, t1460_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1460), 0, -1, sizeof(t1460_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, true, false, false, 3, 0, 4, 0, 0, 6, 1, 1};
#include "t1462.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1462_TI;
#include "t1462MD.h"

extern MethodInfo m7939_MI;
extern MethodInfo m7933_MI;


extern MethodInfo m7928_MI;
 void m7928 (t1462 * __this, t29 * p0, t316* p1, int32_t p2, t1449 * p3, t29 * p4, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f4 = p0;
		__this->f1 = p1;
		__this->f2 = p2;
		__this->f3 = p3;
		if (!p4)
		{
			goto IL_0041;
		}
	}
	{
		t7* L_0 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10201_MI, p4);
		__this->f5 = L_0;
		t636 * L_1 = (t636 *)InterfaceFuncInvoker0< t636 * >::Invoke(&m10197_MI, p4);
		__this->f7 = L_1;
	}

IL_0041:
	{
		t316* L_2 = (__this->f1);
		if (L_2)
		{
			goto IL_0055;
		}
	}
	{
		__this->f1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), p2));
	}

IL_0055:
	{
		return;
	}
}
extern MethodInfo m7929_MI;
 void m7929 (t1462 * __this, t295 * p0, t29 * p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f6 = p0;
		if (!p1)
		{
			goto IL_0028;
		}
	}
	{
		t636 * L_0 = (t636 *)InterfaceFuncInvoker0< t636 * >::Invoke(&m10197_MI, p1);
		__this->f7 = L_0;
		t1449 * L_1 = (t1449 *)InterfaceFuncInvoker0< t1449 * >::Invoke(&m10196_MI, p1);
		__this->f3 = L_1;
	}

IL_0028:
	{
		__this->f1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
		return;
	}
}
extern MethodInfo m7930_MI;
 void m7930 (t1462 * __this, t7* p0, MethodInfo* method){
	{
		VirtActionInvoker1< t7* >::Invoke(&m7939_MI, __this, p0);
		return;
	}
}
extern MethodInfo m7931_MI;
 t316* m7931 (t1462 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m7932_MI;
 t1449 * m7932 (t1462 * __this, MethodInfo* method){
	{
		t1449 * L_0 = (__this->f3);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		t1449 * L_1 = (t1449 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1449_TI));
		m7855(L_1, &m7855_MI);
		__this->f3 = L_1;
	}

IL_0013:
	{
		t1449 * L_2 = (__this->f3);
		return L_2;
	}
}
 t636 * m7933 (t1462 * __this, MethodInfo* method){
	{
		t636 * L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m7934_MI;
 t7* m7934 (t1462 * __this, MethodInfo* method){
	{
		t636 * L_0 = (__this->f7);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		t7* L_1 = (__this->f8);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		t636 * L_2 = (__this->f7);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, L_2);
		__this->f8 = L_3;
	}

IL_0021:
	{
		t7* L_4 = (__this->f8);
		return L_4;
	}
}
extern MethodInfo m7935_MI;
 t29 * m7935 (t1462 * __this, MethodInfo* method){
	t638* V_0 = {0};
	int32_t V_1 = 0;
	{
		t636 * L_0 = (__this->f7);
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		t537* L_1 = (__this->f9);
		if (L_1)
		{
			goto IL_0048;
		}
	}
	{
		t636 * L_2 = (__this->f7);
		t638* L_3 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, L_2);
		V_0 = L_3;
		__this->f9 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), (((int32_t)(((t20 *)V_0)->max_length)))));
		V_1 = 0;
		goto IL_0042;
	}

IL_002e:
	{
		t537* L_4 = (__this->f9);
		int32_t L_5 = V_1;
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_5)));
		ArrayElementTypeCheck (L_4, L_6);
		*((t42 **)(t42 **)SZArrayLdElema(L_4, V_1)) = (t42 *)L_6;
		V_1 = ((int32_t)(V_1+1));
	}

IL_0042:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_002e;
		}
	}

IL_0048:
	{
		t537* L_7 = (__this->f9);
		return (t29 *)L_7;
	}
}
extern MethodInfo m7936_MI;
 t29 * m7936 (t1462 * __this, MethodInfo* method){
	{
		t1455 * L_0 = (__this->f11);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1455_TI));
		t1455 * L_1 = (t1455 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1455_TI));
		m7907(L_1, __this, &m7907_MI);
		__this->f11 = L_1;
	}

IL_0014:
	{
		t1455 * L_2 = (__this->f11);
		return L_2;
	}
}
extern MethodInfo m7937_MI;
 t7* m7937 (t1462 * __this, MethodInfo* method){
	{
		t636 * L_0 = (__this->f7);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		t7* L_1 = (__this->f10);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		t636 * L_2 = (__this->f7);
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, L_2);
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_3);
		__this->f10 = L_4;
	}

IL_0026:
	{
		t7* L_5 = (__this->f10);
		return L_5;
	}
}
extern MethodInfo m7938_MI;
 t7* m7938 (t1462 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f5);
		return L_0;
	}
}
 void m7939 (t1462 * __this, t7* p0, MethodInfo* method){
	{
		__this->f5 = p0;
		return;
	}
}
extern MethodInfo m7940_MI;
 t295 * m7940 (t1462 * __this, MethodInfo* method){
	{
		t295 * L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m7941_MI;
 t316* m7941 (t1462 * __this, MethodInfo* method){
	{
		t316* L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		t316* L_1 = (__this->f1);
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		t1438 * L_2 = (__this->f12);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		t636 * L_3 = (t636 *)VirtFuncInvoker0< t636 * >::Invoke(&m7933_MI, __this);
		t1438 * L_4 = (t1438 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1438_TI));
		m7813(L_4, L_3, 1, &m7813_MI);
		__this->f12 = L_4;
	}

IL_002a:
	{
		t1438 * L_5 = (__this->f12);
		t316* L_6 = (__this->f1);
		t316* L_7 = m7814(L_5, L_6, &m7814_MI);
		__this->f0 = L_7;
	}

IL_0041:
	{
		t316* L_8 = (__this->f0);
		return L_8;
	}
}
extern MethodInfo m7942_MI;
 t29 * m7942 (t1462 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f4);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern Il2CppType t316_0_0_1;
FieldInfo t1462_f0_FieldInfo = 
{
	"_outArgs", &t316_0_0_1, &t1462_TI, offsetof(t1462, f0), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t1462_f1_FieldInfo = 
{
	"_args", &t316_0_0_1, &t1462_TI, offsetof(t1462, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1462_f2_FieldInfo = 
{
	"_outArgsCount", &t44_0_0_1, &t1462_TI, offsetof(t1462, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1449_0_0_1;
FieldInfo t1462_f3_FieldInfo = 
{
	"_callCtx", &t1449_0_0_1, &t1462_TI, offsetof(t1462, f3), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1462_f4_FieldInfo = 
{
	"_returnValue", &t29_0_0_1, &t1462_TI, offsetof(t1462, f4), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1462_f5_FieldInfo = 
{
	"_uri", &t7_0_0_1, &t1462_TI, offsetof(t1462, f5), &EmptyCustomAttributesCache};
extern Il2CppType t295_0_0_1;
FieldInfo t1462_f6_FieldInfo = 
{
	"_exception", &t295_0_0_1, &t1462_TI, offsetof(t1462, f6), &EmptyCustomAttributesCache};
extern Il2CppType t636_0_0_1;
FieldInfo t1462_f7_FieldInfo = 
{
	"_methodBase", &t636_0_0_1, &t1462_TI, offsetof(t1462, f7), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1462_f8_FieldInfo = 
{
	"_methodName", &t7_0_0_1, &t1462_TI, offsetof(t1462, f8), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_1;
FieldInfo t1462_f9_FieldInfo = 
{
	"_methodSignature", &t537_0_0_1, &t1462_TI, offsetof(t1462, f9), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1462_f10_FieldInfo = 
{
	"_typeName", &t7_0_0_1, &t1462_TI, offsetof(t1462, f10), &EmptyCustomAttributesCache};
extern Il2CppType t1455_0_0_1;
FieldInfo t1462_f11_FieldInfo = 
{
	"_properties", &t1455_0_0_1, &t1462_TI, offsetof(t1462, f11), &EmptyCustomAttributesCache};
extern Il2CppType t1438_0_0_1;
FieldInfo t1462_f12_FieldInfo = 
{
	"_inArgInfo", &t1438_0_0_1, &t1462_TI, offsetof(t1462, f12), &EmptyCustomAttributesCache};
static FieldInfo* t1462_FIs[] =
{
	&t1462_f0_FieldInfo,
	&t1462_f1_FieldInfo,
	&t1462_f2_FieldInfo,
	&t1462_f3_FieldInfo,
	&t1462_f4_FieldInfo,
	&t1462_f5_FieldInfo,
	&t1462_f6_FieldInfo,
	&t1462_f7_FieldInfo,
	&t1462_f8_FieldInfo,
	&t1462_f9_FieldInfo,
	&t1462_f10_FieldInfo,
	&t1462_f11_FieldInfo,
	&t1462_f12_FieldInfo,
	NULL
};
static PropertyInfo t1462____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&t1462_TI, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri", NULL, &m7930_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____Args_PropertyInfo = 
{
	&t1462_TI, "Args", &m7931_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____LogicalCallContext_PropertyInfo = 
{
	&t1462_TI, "LogicalCallContext", &m7932_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____MethodBase_PropertyInfo = 
{
	&t1462_TI, "MethodBase", &m7933_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____MethodName_PropertyInfo = 
{
	&t1462_TI, "MethodName", &m7934_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____MethodSignature_PropertyInfo = 
{
	&t1462_TI, "MethodSignature", &m7935_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____Properties_PropertyInfo = 
{
	&t1462_TI, "Properties", &m7936_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____TypeName_PropertyInfo = 
{
	&t1462_TI, "TypeName", &m7937_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____Uri_PropertyInfo = 
{
	&t1462_TI, "Uri", &m7938_MI, &m7939_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____Exception_PropertyInfo = 
{
	&t1462_TI, "Exception", &m7940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____OutArgs_PropertyInfo = 
{
	&t1462_TI, "OutArgs", &m7941_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1462____ReturnValue_PropertyInfo = 
{
	&t1462_TI, "ReturnValue", &m7942_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1462_PIs[] =
{
	&t1462____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&t1462____Args_PropertyInfo,
	&t1462____LogicalCallContext_PropertyInfo,
	&t1462____MethodBase_PropertyInfo,
	&t1462____MethodName_PropertyInfo,
	&t1462____MethodSignature_PropertyInfo,
	&t1462____Properties_PropertyInfo,
	&t1462____TypeName_PropertyInfo,
	&t1462____Uri_PropertyInfo,
	&t1462____Exception_PropertyInfo,
	&t1462____OutArgs_PropertyInfo,
	&t1462____ReturnValue_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1449_0_0_0;
extern Il2CppType t1463_0_0_0;
static ParameterInfo t1462_m7928_ParameterInfos[] = 
{
	{"ret", 0, 134221947, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"outArgs", 1, 134221948, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"outArgsCount", 2, 134221949, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callCtx", 3, 134221950, &EmptyCustomAttributesCache, &t1449_0_0_0},
	{"mcm", 4, 134221951, &EmptyCustomAttributesCache, &t1463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7928_MI = 
{
	".ctor", (methodPointerType)&m7928, &t1462_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t29_t29, t1462_m7928_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 5, false, false, 3509, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t295_0_0_0;
extern Il2CppType t295_0_0_0;
extern Il2CppType t1463_0_0_0;
static ParameterInfo t1462_m7929_ParameterInfos[] = 
{
	{"e", 0, 134221952, &EmptyCustomAttributesCache, &t295_0_0_0},
	{"mcm", 1, 134221953, &EmptyCustomAttributesCache, &t1463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7929_MI = 
{
	".ctor", (methodPointerType)&m7929, &t1462_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1462_m7929_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3510, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1462_m7930_ParameterInfos[] = 
{
	{"value", 0, 134221954, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7930_MI = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri", (methodPointerType)&m7930, &t1462_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1462_m7930_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 4, 1, false, false, 3511, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7931_MI = 
{
	"get_Args", (methodPointerType)&m7931, &t1462_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 3512, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1449_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7932_MI = 
{
	"get_LogicalCallContext", (methodPointerType)&m7932, &t1462_TI, &t1449_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3513, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7933_MI = 
{
	"get_MethodBase", (methodPointerType)&m7933, &t1462_TI, &t636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, false, 3514, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7934_MI = 
{
	"get_MethodName", (methodPointerType)&m7934, &t1462_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, false, 3515, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7935_MI = 
{
	"get_MethodSignature", (methodPointerType)&m7935, &t1462_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, false, 3516, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7936_MI = 
{
	"get_Properties", (methodPointerType)&m7936, &t1462_TI, &t721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 15, 0, false, false, 3517, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7937_MI = 
{
	"get_TypeName", (methodPointerType)&m7937, &t1462_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, false, 3518, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7938_MI = 
{
	"get_Uri", (methodPointerType)&m7938, &t1462_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 11, 0, false, false, 3519, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1462_m7939_ParameterInfos[] = 
{
	{"value", 0, 134221955, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7939_MI = 
{
	"set_Uri", (methodPointerType)&m7939, &t1462_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1462_m7939_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 16, 1, false, false, 3520, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t295_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7940_MI = 
{
	"get_Exception", (methodPointerType)&m7940, &t1462_TI, &t295_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 12, 0, false, false, 3521, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7941_MI = 
{
	"get_OutArgs", (methodPointerType)&m7941, &t1462_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 13, 0, false, false, 3522, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7942_MI = 
{
	"get_ReturnValue", (methodPointerType)&m7942, &t1462_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 17, 0, false, false, 3523, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1462_MIs[] =
{
	&m7928_MI,
	&m7929_MI,
	&m7930_MI,
	&m7931_MI,
	&m7932_MI,
	&m7933_MI,
	&m7934_MI,
	&m7935_MI,
	&m7936_MI,
	&m7937_MI,
	&m7938_MI,
	&m7939_MI,
	&m7940_MI,
	&m7941_MI,
	&m7942_MI,
	NULL
};
static MethodInfo* t1462_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7930_MI,
	&m7931_MI,
	&m7932_MI,
	&m7933_MI,
	&m7934_MI,
	&m7935_MI,
	&m7937_MI,
	&m7938_MI,
	&m7940_MI,
	&m7941_MI,
	&m7942_MI,
	&m7936_MI,
	&m7939_MI,
	&m7942_MI,
};
static TypeInfo* t1462_ITIs[] = 
{
	&t2046_TI,
	&t1443_TI,
	&t1453_TI,
	&t1456_TI,
};
static Il2CppInterfaceOffsetPair t1462_IOs[] = 
{
	{ &t2046_TI, 4},
	{ &t1443_TI, 5},
	{ &t1453_TI, 5},
	{ &t1456_TI, 12},
};
void t1462_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1462__CustomAttributeCache = {
1,
NULL,
&t1462_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1462_0_0_0;
extern Il2CppType t1462_1_0_0;
struct t1462;
extern CustomAttributesCache t1462__CustomAttributeCache;
TypeInfo t1462_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReturnMessage", "System.Runtime.Remoting.Messaging", t1462_MIs, t1462_PIs, t1462_FIs, NULL, &t29_TI, NULL, NULL, &t1462_TI, t1462_ITIs, t1462_VT, &t1462__CustomAttributeCache, &t1462_TI, &t1462_0_0_0, &t1462_1_0_0, t1462_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1462), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 15, 12, 13, 0, 0, 18, 4, 4};
#include "t1464.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1464_TI;
#include "t1464MD.h"

#include "t774.h"
#include "t1470.h"
#include "t1465.h"
extern TypeInfo t1470_TI;
extern TypeInfo t1465_TI;
#include "t1470MD.h"
#include "t1465MD.h"
extern MethodInfo m7956_MI;
extern MethodInfo m7953_MI;
extern MethodInfo m8018_MI;
extern MethodInfo m8007_MI;


extern MethodInfo m7943_MI;
 t774 * m7943 (t1464 * __this, t42 * p0, MethodInfo* method){
	t1470 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1470_TI));
		t1470 * L_0 = (t1470 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1470_TI));
		m7956(L_0, p0, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f3), (t316*)(t316*)NULL, &m7956_MI);
		V_0 = L_0;
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7953_MI, V_0);
		return ((t774 *)Castclass(L_1, InitializedTypeInfo(&t774_TI)));
	}
}
extern MethodInfo m7944_MI;
 t1465 * m7944 (t1464 * __this, t1466 * p0, t42 * p1, t29 * p2, t1425 * p3, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_0 = m8018(NULL, p0, p1, &m8018_MI);
		t1465 * L_1 = m8007(NULL, L_0, &m8007_MI);
		return L_1;
	}
}
extern MethodInfo m7945_MI;
 void m7945 (t1464 * __this, t29 * p0, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m7946_MI;
 bool m7946 (t1464 * __this, t1425 * p0, t29 * p1, MethodInfo* method){
	{
		return 1;
	}
}
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern Il2CppType t42_0_0_0;
static ParameterInfo t1464_m7943_ParameterInfos[] = 
{
	{"serverType", 0, 134221956, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t774_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7943_MI = 
{
	"CreateInstance", (methodPointerType)&m7943, &t1464_TI, &t774_0_0_0, RuntimeInvoker_t29_t29, t1464_m7943_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 6, 1, false, false, 3524, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1466_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t1425_0_0_0;
static ParameterInfo t1464_m7944_ParameterInfos[] = 
{
	{"objRef", 0, 134221957, &EmptyCustomAttributesCache, &t1466_0_0_0},
	{"serverType", 1, 134221958, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"serverObject", 2, 134221959, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"serverContext", 3, 134221960, &EmptyCustomAttributesCache, &t1425_0_0_0},
};
extern Il2CppType t1465_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7944_MI = 
{
	"CreateProxy", (methodPointerType)&m7944, &t1464_TI, &t1465_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1464_m7944_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 4, false, false, 3525, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1464_m7945_ParameterInfos[] = 
{
	{"msg", 0, 134221961, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1464__CustomAttributeCache_m7945;
MethodInfo m7945_MI = 
{
	"GetPropertiesForNewContext", (methodPointerType)&m7945, &t1464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1464_m7945_ParameterInfos, &t1464__CustomAttributeCache_m7945, 486, 0, 4, 1, false, false, 3526, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1425_0_0_0;
extern Il2CppType t1424_0_0_0;
static ParameterInfo t1464_m7946_ParameterInfos[] = 
{
	{"ctx", 0, 134221962, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"msg", 1, 134221963, &EmptyCustomAttributesCache, &t1424_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1464__CustomAttributeCache_m7946;
MethodInfo m7946_MI = 
{
	"IsContextOK", (methodPointerType)&m7946, &t1464_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1464_m7946_ParameterInfos, &t1464__CustomAttributeCache_m7946, 486, 0, 5, 2, false, false, 3527, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1464_MIs[] =
{
	&m7943_MI,
	&m7944_MI,
	&m7945_MI,
	&m7946_MI,
	NULL
};
extern MethodInfo m2882_MI;
extern MethodInfo m2883_MI;
static MethodInfo* t1464_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
	&m7945_MI,
	&m7946_MI,
	&m7943_MI,
	&m7944_MI,
};
static TypeInfo* t1464_ITIs[] = 
{
	&t2040_TI,
};
static Il2CppInterfaceOffsetPair t1464_IOs[] = 
{
	{ &t604_TI, 4},
	{ &t2040_TI, 4},
};
void t1464_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t1464_CustomAttributesCacheGenerator_m7945(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1464_CustomAttributesCacheGenerator_m7946(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1464__CustomAttributeCache = {
2,
NULL,
&t1464_CustomAttributesCacheGenerator
};
CustomAttributesCache t1464__CustomAttributeCache_m7945 = {
1,
NULL,
&t1464_CustomAttributesCacheGenerator_m7945
};
CustomAttributesCache t1464__CustomAttributeCache_m7946 = {
1,
NULL,
&t1464_CustomAttributesCacheGenerator_m7946
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1464_0_0_0;
extern Il2CppType t1464_1_0_0;
struct t1464;
extern CustomAttributesCache t1464__CustomAttributeCache;
extern CustomAttributesCache t1464__CustomAttributeCache_m7945;
extern CustomAttributesCache t1464__CustomAttributeCache_m7946;
TypeInfo t1464_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ProxyAttribute", "System.Runtime.Remoting.Proxies", t1464_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t1464_TI, t1464_ITIs, t1464_VT, &t1464__CustomAttributeCache, &t1464_TI, &t1464_0_0_0, &t1464_1_0_0, t1464_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1464), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 8, 1, 2};
#include "t1467.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1467_TI;
#include "t1467MD.h"



// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern Il2CppType t1465_0_0_6;
FieldInfo t1467_f0_FieldInfo = 
{
	"_rp", &t1465_0_0_6, &t1467_TI, offsetof(t1467, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1467_FIs[] =
{
	&t1467_f0_FieldInfo,
	NULL
};
static MethodInfo* t1467_MIs[] =
{
	NULL
};
static MethodInfo* t1467_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1467_0_0_0;
extern Il2CppType t1467_1_0_0;
struct t1467;
TypeInfo t1467_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TransparentProxy", "System.Runtime.Remoting.Proxies", t1467_MIs, NULL, t1467_FIs, NULL, &t29_TI, NULL, NULL, &t1467_TI, NULL, t1467_VT, &EmptyCustomAttributesCache, &t1467_TI, &t1467_0_0_0, &t1467_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1467), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t1469.h"
#include "t1468.h"
extern TypeInfo t35_TI;
extern TypeInfo t1478_TI;
#include "t35MD.h"
extern Il2CppType t774_0_0_0;
extern MethodInfo m7949_MI;
extern MethodInfo m2888_MI;
extern MethodInfo m7950_MI;
extern MethodInfo m10206_MI;
extern MethodInfo m7952_MI;


extern MethodInfo m7947_MI;
 void m7947 (t1465 * __this, t42 * p0, MethodInfo* method){
	{
		m7949(__this, p0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), NULL, &m7949_MI);
		return;
	}
}
extern MethodInfo m7948_MI;
 void m7948 (t1465 * __this, t42 * p0, t1469 * p1, MethodInfo* method){
	{
		m7949(__this, p0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), NULL, &m7949_MI);
		__this->f3 = p1;
		return;
	}
}
 void m7949 (t1465 * __this, t42 * p0, t35 p1, t29 * p2, MethodInfo* method){
	{
		__this->f1 = (-1);
		m1331(__this, &m1331_MI);
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6000_MI, p0);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, p0);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		t305 * L_2 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_2, (t7*) &_stringLiteral1616, &m1935_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0028:
	{
		__this->f0 = p0;
		bool L_3 = m2888(NULL, p1, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2888_MI);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		t345 * L_4 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_4, (t7*) &_stringLiteral1617, &m3988_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0047:
	{
		return;
	}
}
 t42 * m7950 (t29 * __this, t29 * p0, MethodInfo* method){
	typedef t42 * (*m7950_ftn) (t29 *);
	static m7950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7950_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.Remoting.Proxies.RealProxy::InternalGetProxyType(System.Object)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m7951_MI;
 t42 * m7951 (t1465 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f4);
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		t42 * L_1 = (__this->f0);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, L_1);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t774_0_0_0), &m1554_MI);
		return L_3;
	}

IL_0020:
	{
		t42 * L_4 = (__this->f0);
		return L_4;
	}

IL_0027:
	{
		t29 * L_5 = (__this->f4);
		t42 * L_6 = m7950(NULL, L_5, &m7950_MI);
		return L_6;
	}
}
 t29 * m7952 (t1465 * __this, t7* p0, MethodInfo* method){
	typedef t29 * (*m7952_ftn) (t1465 *, t7*);
	static m7952_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7952_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.Remoting.Proxies.RealProxy::InternalGetTransparentProxy(System.String)");
	return _il2cpp_icall_func(__this, p0);
}
 t29 * m7953 (t1465 * __this, MethodInfo* method){
	t7* V_0 = {0};
	t29 * V_1 = {0};
	{
		t29 * L_0 = (__this->f4);
		if (L_0)
		{
			goto IL_005a;
		}
	}
	{
		V_1 = ((t29 *)IsInst(__this, InitializedTypeInfo(&t1478_TI)));
		if (!V_1)
		{
			goto IL_0041;
		}
	}
	{
		t7* L_1 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10206_MI, V_1);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t774_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1713(NULL, V_0, L_3, &m1713_MI);
		if (!L_4)
		{
			goto IL_003f;
		}
	}

IL_0033:
	{
		t42 * L_5 = (__this->f0);
		t7* L_6 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_5);
		V_0 = L_6;
	}

IL_003f:
	{
		goto IL_004d;
	}

IL_0041:
	{
		t42 * L_7 = (__this->f0);
		t7* L_8 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_7);
		V_0 = L_8;
	}

IL_004d:
	{
		t29 * L_9 = (t29 *)VirtFuncInvoker1< t29 *, t7* >::Invoke(&m7952_MI, __this, V_0);
		__this->f4 = L_9;
	}

IL_005a:
	{
		t29 * L_10 = (__this->f4);
		return L_10;
	}
}
extern MethodInfo m7954_MI;
 void m7954 (t1465 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern Il2CppType t42_0_0_1;
FieldInfo t1465_f0_FieldInfo = 
{
	"class_to_proxy", &t42_0_0_1, &t1465_TI, offsetof(t1465, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1465_f1_FieldInfo = 
{
	"_targetDomainId", &t44_0_0_1, &t1465_TI, offsetof(t1465, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t1465_f2_FieldInfo = 
{
	"_targetUri", &t7_0_0_3, &t1465_TI, offsetof(t1465, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1468_0_0_3;
FieldInfo t1465_f3_FieldInfo = 
{
	"_objectIdentity", &t1468_0_0_3, &t1465_TI, offsetof(t1465, f3), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1465_f4_FieldInfo = 
{
	"_objTP", &t29_0_0_1, &t1465_TI, offsetof(t1465, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1465_FIs[] =
{
	&t1465_f0_FieldInfo,
	&t1465_f1_FieldInfo,
	&t1465_f2_FieldInfo,
	&t1465_f3_FieldInfo,
	&t1465_f4_FieldInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1465_m7947_ParameterInfos[] = 
{
	{"classToProxy", 0, 134221964, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7947_MI = 
{
	".ctor", (methodPointerType)&m7947, &t1465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1465_m7947_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 1, false, false, 3528, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t1469_0_0_0;
extern Il2CppType t1469_0_0_0;
static ParameterInfo t1465_m7948_ParameterInfos[] = 
{
	{"classToProxy", 0, 134221965, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"identity", 1, 134221966, &EmptyCustomAttributesCache, &t1469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7948_MI = 
{
	".ctor", (methodPointerType)&m7948, &t1465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1465_m7948_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3529, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1465_m7949_ParameterInfos[] = 
{
	{"classToProxy", 0, 134221967, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"stub", 1, 134221968, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"stubData", 2, 134221969, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7949_MI = 
{
	".ctor", (methodPointerType)&m7949, &t1465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35_t29, t1465_m7949_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 3, false, false, 3530, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1465_m7950_ParameterInfos[] = 
{
	{"transparentProxy", 0, 134221970, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7950_MI = 
{
	"InternalGetProxyType", (methodPointerType)&m7950, &t1465_TI, &t42_0_0_0, RuntimeInvoker_t29_t29, t1465_m7950_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 1, false, false, 3531, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7951_MI = 
{
	"GetProxiedType", (methodPointerType)&m7951, &t1465_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 3532, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1465_m7952_ParameterInfos[] = 
{
	{"className", 0, 134221971, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7952_MI = 
{
	"InternalGetTransparentProxy", (methodPointerType)&m7952, &t1465_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1465_m7952_ParameterInfos, &EmptyCustomAttributesCache, 451, 4096, 4, 1, false, false, 3533, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7953_MI = 
{
	"GetTransparentProxy", (methodPointerType)&m7953, &t1465_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 5, 0, false, false, 3534, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1465_m7954_ParameterInfos[] = 
{
	{"domainId", 0, 134221972, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7954_MI = 
{
	"SetTargetDomain", (methodPointerType)&m7954, &t1465_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1465_m7954_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 3535, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1465_MIs[] =
{
	&m7947_MI,
	&m7948_MI,
	&m7949_MI,
	&m7950_MI,
	&m7951_MI,
	&m7952_MI,
	&m7953_MI,
	&m7954_MI,
	NULL
};
static MethodInfo* t1465_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7952_MI,
	&m7953_MI,
};
void t1465_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1465__CustomAttributeCache = {
1,
NULL,
&t1465_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1465_0_0_0;
extern Il2CppType t1465_1_0_0;
struct t1465;
extern CustomAttributesCache t1465__CustomAttributeCache;
TypeInfo t1465_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RealProxy", "System.Runtime.Remoting.Proxies", t1465_MIs, NULL, t1465_FIs, NULL, &t29_TI, NULL, NULL, &t1465_TI, NULL, t1465_VT, &t1465__CustomAttributeCache, &t1465_TI, &t1465_0_0_0, &t1465_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1465), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, false, false, false, 8, 0, 5, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1485.h"
extern TypeInfo t1469_TI;
extern TypeInfo t1468_TI;
extern TypeInfo t1485_TI;
#include "t1468MD.h"
#include "t1466MD.h"
#include "t1469MD.h"
#include "t1415MD.h"
extern MethodInfo m10207_MI;
extern MethodInfo m7986_MI;
extern MethodInfo m7969_MI;
extern MethodInfo m7978_MI;
extern MethodInfo m7766_MI;
extern MethodInfo m6022_MI;
extern MethodInfo m8021_MI;


extern MethodInfo m7955_MI;
 void m7955 (t1470 * __this, t42 * p0, t1469 * p1, MethodInfo* method){
	{
		m7948(__this, p0, p1, &m7948_MI);
		t29 * L_0 = m7969(p1, &m7969_MI);
		__this->f7 = L_0;
		__this->f8 = 0;
		t7* L_1 = m7978(p1, &m7978_MI);
		__this->f2 = L_1;
		return;
	}
}
 void m7956 (t1470 * __this, t42 * p0, t7* p1, t316* p2, MethodInfo* method){
	{
		m7947(__this, p0, &m7947_MI);
		__this->f8 = 0;
		t1417 * L_0 = m7766(NULL, p0, p1, p2, &m7766_MI);
		__this->f9 = L_0;
		return;
	}
}
extern MethodInfo m7957_MI;
 void m7957 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		t557 * L_1 = (t557 *)VirtFuncInvoker1< t557 *, t7* >::Invoke(&m6022_MI, L_0, (t7*) &_stringLiteral1618);
		((t1470_SFs*)InitializedTypeInfo(&t1470_TI)->static_fields)->f5 = L_1;
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		t557 * L_3 = (t557 *)VirtFuncInvoker1< t557 *, t7* >::Invoke(&m6022_MI, L_2, (t7*) &_stringLiteral1619);
		((t1470_SFs*)InitializedTypeInfo(&t1470_TI)->static_fields)->f6 = L_3;
		return;
	}
}
extern MethodInfo m7958_MI;
 t7* m7958 (t1470 * __this, MethodInfo* method){
	t1466 * V_0 = {0};
	{
		t1468 * L_0 = (__this->f3);
		if (!((t1469 *)IsInst(L_0, InitializedTypeInfo(&t1469_TI))))
		{
			goto IL_002e;
		}
	}
	{
		t1468 * L_1 = (__this->f3);
		t1466 * L_2 = (t1466 *)VirtFuncInvoker1< t1466 *, t42 * >::Invoke(&m10207_MI, L_1, (t42 *)NULL);
		V_0 = L_2;
		t29 * L_3 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7986_MI, V_0);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		t29 * L_4 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7986_MI, V_0);
		t7* L_5 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10206_MI, L_4);
		return L_5;
	}

IL_002e:
	{
		t42 * L_6 = m7951(__this, &m7951_MI);
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_6);
		return L_7;
	}
}
extern MethodInfo m7959_MI;
 void m7959 (t1470 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			t1468 * L_0 = (__this->f3);
			if (!L_0)
			{
				goto IL_0020;
			}
		}

IL_0008:
		{
			t1468 * L_1 = (__this->f3);
			if (((t1485 *)IsInst(L_1, InitializedTypeInfo(&t1485_TI))))
			{
				goto IL_0020;
			}
		}

IL_0015:
		{
			t1468 * L_2 = (__this->f3);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			m8021(NULL, L_2, &m8021_MI);
		}

IL_0020:
		{
			// IL_0020: leave.s IL_0029
			leaveInstructions[0] = 0x29; // 1
			THROW_SENTINEL(IL_0029);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0022;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0022;
	}

IL_0022:
	{ // begin finally (depth: 1)
		m46(__this, &m46_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x29:
				goto IL_0029;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0029:
	{
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern Il2CppType t557_0_0_17;
FieldInfo t1470_f5_FieldInfo = 
{
	"_cache_GetTypeMethod", &t557_0_0_17, &t1470_TI, offsetof(t1470_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_17;
FieldInfo t1470_f6_FieldInfo = 
{
	"_cache_GetHashCodeMethod", &t557_0_0_17, &t1470_TI, offsetof(t1470_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t967_0_0_1;
FieldInfo t1470_f7_FieldInfo = 
{
	"_sink", &t967_0_0_1, &t1470_TI, offsetof(t1470, f7), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1470_f8_FieldInfo = 
{
	"_hasEnvoySink", &t40_0_0_1, &t1470_TI, offsetof(t1470, f8), &EmptyCustomAttributesCache};
extern Il2CppType t1417_0_0_1;
FieldInfo t1470_f9_FieldInfo = 
{
	"_ctorCall", &t1417_0_0_1, &t1470_TI, offsetof(t1470, f9), &EmptyCustomAttributesCache};
static FieldInfo* t1470_FIs[] =
{
	&t1470_f5_FieldInfo,
	&t1470_f6_FieldInfo,
	&t1470_f7_FieldInfo,
	&t1470_f8_FieldInfo,
	&t1470_f9_FieldInfo,
	NULL
};
static PropertyInfo t1470____TypeName_PropertyInfo = 
{
	&t1470_TI, "TypeName", &m7958_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1470_PIs[] =
{
	&t1470____TypeName_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
extern Il2CppType t1469_0_0_0;
static ParameterInfo t1470_m7955_ParameterInfos[] = 
{
	{"type", 0, 134221973, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"identity", 1, 134221974, &EmptyCustomAttributesCache, &t1469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7955_MI = 
{
	".ctor", (methodPointerType)&m7955, &t1470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1470_m7955_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3536, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1470_m7956_ParameterInfos[] = 
{
	{"type", 0, 134221975, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"activationUrl", 1, 134221976, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"activationAttributes", 2, 134221977, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7956_MI = 
{
	".ctor", (methodPointerType)&m7956, &t1470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1470_m7956_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 3, false, false, 3537, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7957_MI = 
{
	".cctor", (methodPointerType)&m7957, &t1470_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3538, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7958_MI = 
{
	"get_TypeName", (methodPointerType)&m7958, &t1470_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3539, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7959_MI = 
{
	"Finalize", (methodPointerType)&m7959, &t1470_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 1, 0, false, false, 3540, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1470_MIs[] =
{
	&m7955_MI,
	&m7956_MI,
	&m7957_MI,
	&m7958_MI,
	&m7959_MI,
	NULL
};
static MethodInfo* t1470_VT[] =
{
	&m1321_MI,
	&m7959_MI,
	&m1322_MI,
	&m1332_MI,
	&m7952_MI,
	&m7953_MI,
	&m7958_MI,
};
static TypeInfo* t1470_ITIs[] = 
{
	&t1478_TI,
};
static Il2CppInterfaceOffsetPair t1470_IOs[] = 
{
	{ &t1478_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1470_0_0_0;
extern Il2CppType t1470_1_0_0;
struct t1470;
TypeInfo t1470_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemotingProxy", "System.Runtime.Remoting.Proxies", t1470_MIs, t1470_PIs, t1470_FIs, NULL, &t1465_TI, NULL, NULL, &t1470_TI, t1470_ITIs, t1470_VT, &EmptyCustomAttributesCache, &t1470_TI, &t1470_0_0_0, &t1470_1_0_0, t1470_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1470), 0, -1, sizeof(t1470_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, true, true, false, false, 5, 1, 5, 0, 0, 7, 1, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2051_TI;



// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern Il2CppType t29_0_0_0;
extern Il2CppType t1466_0_0_0;
static ParameterInfo t2051_m10208_ParameterInfos[] = 
{
	{"obj", 0, 134221978, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"or", 1, 134221979, &EmptyCustomAttributesCache, &t1466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10208_MI = 
{
	"UnmarshaledObject", NULL, &t2051_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2051_m10208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, false, 3541, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t2051_MIs[] =
{
	&m10208_MI,
	NULL
};
void t2051_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2051__CustomAttributeCache = {
1,
NULL,
&t2051_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2051_0_0_0;
extern Il2CppType t2051_1_0_0;
struct t2051;
extern CustomAttributesCache t2051__CustomAttributeCache;
TypeInfo t2051_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ITrackingHandler", "System.Runtime.Remoting.Services", t2051_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2051_TI, NULL, NULL, &t2051__CustomAttributeCache, &t2051_TI, &t2051_0_0_0, &t2051_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t1471.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1471_TI;
#include "t1471MD.h"

extern TypeInfo t2052_TI;
extern MethodInfo m4158_MI;
extern MethodInfo m10208_MI;


extern MethodInfo m7960_MI;
 void m7960 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		((t1471_SFs*)InitializedTypeInfo(&t1471_TI)->static_fields)->f0 = L_0;
		return;
	}
}
extern MethodInfo m7961_MI;
 void m7961 (t29 * __this, t29 * p0, t1466 * p1, MethodInfo* method){
	t2052* V_0 = {0};
	t29 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1471_TI));
		t29 * L_0 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4176_MI, (((t1471_SFs*)InitializedTypeInfo(&t1471_TI)->static_fields)->f0));
		V_1 = L_0;
		m4000(NULL, V_1, &m4000_MI);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1471_TI));
			int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, (((t1471_SFs*)InitializedTypeInfo(&t1471_TI)->static_fields)->f0));
			if (L_1)
			{
				goto IL_001f;
			}
		}

IL_001d:
		{
			// IL_001d: leave.s IL_005a
			leaveInstructions[0] = 0x5A; // 1
			THROW_SENTINEL(IL_005a);
			// finally target depth: 1
		}

IL_001f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1471_TI));
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
			t42 * L_2 = m1554(NULL, LoadTypeToken(&t2051_0_0_0), &m1554_MI);
			t20 * L_3 = (t20 *)VirtFuncInvoker1< t20 *, t42 * >::Invoke(&m4158_MI, (((t1471_SFs*)InitializedTypeInfo(&t1471_TI)->static_fields)->f0), L_2);
			V_0 = ((t2052*)Castclass(L_3, InitializedTypeInfo(&t2052_TI)));
			// IL_0039: leave.s IL_0042
			leaveInstructions[0] = 0x42; // 1
			THROW_SENTINEL(IL_0042);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_003b;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_003b;
	}

IL_003b:
	{ // begin finally (depth: 1)
		m4001(NULL, V_1, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x5A:
				goto IL_005a;
			case 0x42:
				goto IL_0042;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0042:
	{
		V_2 = 0;
		goto IL_0054;
	}

IL_0046:
	{
		int32_t L_4 = V_2;
		InterfaceActionInvoker2< t29 *, t1466 * >::Invoke(&m10208_MI, (*(t29 **)(t29 **)SZArrayLdElema(V_0, L_4)), p0, p1);
		V_2 = ((int32_t)(V_2+1));
	}

IL_0054:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0046;
		}
	}

IL_005a:
	{
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern Il2CppType t731_0_0_17;
FieldInfo t1471_f0_FieldInfo = 
{
	"_handlers", &t731_0_0_17, &t1471_TI, offsetof(t1471_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1471_FIs[] =
{
	&t1471_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7960_MI = 
{
	".cctor", (methodPointerType)&m7960, &t1471_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3542, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1466_0_0_0;
static ParameterInfo t1471_m7961_ParameterInfos[] = 
{
	{"obj", 0, 134221980, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"or", 1, 134221981, &EmptyCustomAttributesCache, &t1466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7961_MI = 
{
	"NotifyUnmarshaledObject", (methodPointerType)&m7961, &t1471_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1471_m7961_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 3543, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1471_MIs[] =
{
	&m7960_MI,
	&m7961_MI,
	NULL
};
static MethodInfo* t1471_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1471_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1471__CustomAttributeCache = {
1,
NULL,
&t1471_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1471_0_0_0;
extern Il2CppType t1471_1_0_0;
struct t1471;
extern CustomAttributesCache t1471__CustomAttributeCache;
TypeInfo t1471_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TrackingServices", "System.Runtime.Remoting.Services", t1471_MIs, NULL, t1471_FIs, NULL, &t29_TI, NULL, NULL, &t1471_TI, NULL, t1471_VT, &t1471__CustomAttributeCache, &t1471_TI, &t1471_0_0_0, &t1471_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1471), 0, -1, sizeof(t1471_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, true, false, false, 2, 0, 1, 0, 0, 4, 0, 0};
#include "t1472.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1472_TI;
#include "t1472MD.h"

#include "t1473MD.h"
extern MethodInfo m8030_MI;
extern MethodInfo m8029_MI;
extern MethodInfo m7962_MI;


 t7* m7962 (t1472 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m7963_MI;
 t1474* m7963 (t1472 * __this, MethodInfo* method){
	{
		return (t1474*)NULL;
	}
}
extern MethodInfo m7964_MI;
 t42 * m7964 (t1472 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m7965_MI;
 t7* m7965 (t1472 * __this, MethodInfo* method){
	{
		t7* L_0 = m8030(__this, &m8030_MI);
		t7* L_1 = m8029(__this, &m8029_MI);
		t7* L_2 = m7962(__this, &m7962_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1685(NULL, L_0, L_1, L_2, &m1685_MI);
		return L_3;
	}
}
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern Il2CppType t7_0_0_1;
FieldInfo t1472_f2_FieldInfo = 
{
	"applicationUrl", &t7_0_0_1, &t1472_TI, offsetof(t1472, f2), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1472_f3_FieldInfo = 
{
	"obj_type", &t42_0_0_1, &t1472_TI, offsetof(t1472, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1472_FIs[] =
{
	&t1472_f2_FieldInfo,
	&t1472_f3_FieldInfo,
	NULL
};
static PropertyInfo t1472____ApplicationUrl_PropertyInfo = 
{
	&t1472_TI, "ApplicationUrl", &m7962_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1472____ContextAttributes_PropertyInfo = 
{
	&t1472_TI, "ContextAttributes", &m7963_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1472____ObjectType_PropertyInfo = 
{
	&t1472_TI, "ObjectType", &m7964_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1472_PIs[] =
{
	&t1472____ApplicationUrl_PropertyInfo,
	&t1472____ContextAttributes_PropertyInfo,
	&t1472____ObjectType_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7962_MI = 
{
	"get_ApplicationUrl", (methodPointerType)&m7962, &t1472_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3544, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7963_MI = 
{
	"get_ContextAttributes", (methodPointerType)&m7963, &t1472_TI, &t1474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3545, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7964_MI = 
{
	"get_ObjectType", (methodPointerType)&m7964, &t1472_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3546, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7965_MI = 
{
	"ToString", (methodPointerType)&m7965, &t1472_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3547, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1472_MIs[] =
{
	&m7962_MI,
	&m7963_MI,
	&m7964_MI,
	&m7965_MI,
	NULL
};
static MethodInfo* t1472_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7965_MI,
};
void t1472_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1472__CustomAttributeCache = {
1,
NULL,
&t1472_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1472_0_0_0;
extern Il2CppType t1472_1_0_0;
extern TypeInfo t1473_TI;
struct t1472;
extern CustomAttributesCache t1472__CustomAttributeCache;
TypeInfo t1472_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ActivatedClientTypeEntry", "System.Runtime.Remoting", t1472_MIs, t1472_PIs, t1472_FIs, NULL, &t1473_TI, NULL, NULL, &t1472_TI, NULL, t1472_VT, &t1472__CustomAttributeCache, &t1472_TI, &t1472_0_0_0, &t1472_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1472), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 3, 2, 0, 0, 4, 0, 0};
#include "t1475.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1475_TI;
#include "t1475MD.h"



extern MethodInfo m7966_MI;
 void m7966 (t1475 * __this, t29 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m7967_MI;
 t29 * m7967 (t1475 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern Il2CppType t967_0_0_1;
FieldInfo t1475_f0_FieldInfo = 
{
	"envoySinks", &t967_0_0_1, &t1475_TI, offsetof(t1475, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1475_FIs[] =
{
	&t1475_f0_FieldInfo,
	NULL
};
static PropertyInfo t1475____EnvoySinks_PropertyInfo = 
{
	&t1475_TI, "EnvoySinks", &m7967_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1475_PIs[] =
{
	&t1475____EnvoySinks_PropertyInfo,
	NULL
};
extern Il2CppType t967_0_0_0;
static ParameterInfo t1475_m7966_ParameterInfos[] = 
{
	{"sinks", 0, 134221982, &EmptyCustomAttributesCache, &t967_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7966_MI = 
{
	".ctor", (methodPointerType)&m7966, &t1475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1475_m7966_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3548, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t967_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7967_MI = 
{
	"get_EnvoySinks", (methodPointerType)&m7967, &t1475_TI, &t967_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 3549, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1475_MIs[] =
{
	&m7966_MI,
	&m7967_MI,
	NULL
};
static MethodInfo* t1475_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7967_MI,
};
extern TypeInfo t1479_TI;
static TypeInfo* t1475_ITIs[] = 
{
	&t1479_TI,
};
static Il2CppInterfaceOffsetPair t1475_IOs[] = 
{
	{ &t1479_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1475_0_0_0;
extern Il2CppType t1475_1_0_0;
struct t1475;
TypeInfo t1475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EnvoyInfo", "System.Runtime.Remoting", t1475_MIs, t1475_PIs, t1475_FIs, NULL, &t29_TI, NULL, NULL, &t1475_TI, t1475_ITIs, t1475_VT, &EmptyCustomAttributesCache, &t1475_TI, &t1475_0_0_0, &t1475_1_0_0, t1475_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1475), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 1, 1, 0, 0, 5, 1, 1};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern MethodInfo m10209_MI;
static PropertyInfo t1477____ChannelData_PropertyInfo = 
{
	&t1477_TI, "ChannelData", &m10209_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1477_PIs[] =
{
	&t1477____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10209_MI = 
{
	"get_ChannelData", NULL, &t1477_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3550, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1477_MIs[] =
{
	&m10209_MI,
	NULL
};
void t1477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1477__CustomAttributeCache = {
1,
NULL,
&t1477_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1477_0_0_0;
extern Il2CppType t1477_1_0_0;
struct t1477;
extern CustomAttributesCache t1477__CustomAttributeCache;
TypeInfo t1477_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IChannelInfo", "System.Runtime.Remoting", t1477_MIs, t1477_PIs, NULL, NULL, NULL, NULL, NULL, &t1477_TI, NULL, NULL, &t1477__CustomAttributeCache, &t1477_TI, &t1477_0_0_0, &t1477_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern MethodInfo m10210_MI;
static PropertyInfo t1479____EnvoySinks_PropertyInfo = 
{
	&t1479_TI, "EnvoySinks", &m10210_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1479_PIs[] =
{
	&t1479____EnvoySinks_PropertyInfo,
	NULL
};
extern Il2CppType t967_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10210_MI = 
{
	"get_EnvoySinks", NULL, &t1479_TI, &t967_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3551, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1479_MIs[] =
{
	&m10210_MI,
	NULL
};
void t1479_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1479__CustomAttributeCache = {
1,
NULL,
&t1479_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1479_0_0_0;
extern Il2CppType t1479_1_0_0;
struct t1479;
extern CustomAttributesCache t1479__CustomAttributeCache;
TypeInfo t1479_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnvoyInfo", "System.Runtime.Remoting", t1479_MIs, t1479_PIs, NULL, NULL, NULL, NULL, NULL, &t1479_TI, NULL, NULL, &t1479__CustomAttributeCache, &t1479_TI, &t1479_0_0_0, &t1479_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
static PropertyInfo t1478____TypeName_PropertyInfo = 
{
	&t1478_TI, "TypeName", &m10206_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1478_PIs[] =
{
	&t1478____TypeName_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10206_MI = 
{
	"get_TypeName", NULL, &t1478_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 3552, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1478_MIs[] =
{
	&m10206_MI,
	NULL
};
void t1478_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1478__CustomAttributeCache = {
1,
NULL,
&t1478_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1478_0_0_0;
extern Il2CppType t1478_1_0_0;
struct t1478;
extern CustomAttributesCache t1478__CustomAttributeCache;
TypeInfo t1478_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IRemotingTypeInfo", "System.Runtime.Remoting", t1478_MIs, t1478_PIs, NULL, NULL, NULL, NULL, NULL, &t1478_TI, NULL, NULL, &t1478__CustomAttributeCache, &t1478_TI, &t1478_0_0_0, &t1478_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m7968_MI;
 void m7968 (t1468 * __this, t7* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		return;
	}
}
 t29 * m7969 (t1468 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m7970_MI;
 void m7970 (t1468 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m7971_MI;
 t7* m7971 (t1468 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m7972_MI;
 bool m7972 (t1468 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m7973_MI;
 void m7973 (t1468 * __this, bool p0, MethodInfo* method){
	{
		__this->f4 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Identity
extern Il2CppType t7_0_0_4;
FieldInfo t1468_f0_FieldInfo = 
{
	"_objectUri", &t7_0_0_4, &t1468_TI, offsetof(t1468, f0), &EmptyCustomAttributesCache};
extern Il2CppType t967_0_0_4;
FieldInfo t1468_f1_FieldInfo = 
{
	"_channelSink", &t967_0_0_4, &t1468_TI, offsetof(t1468, f1), &EmptyCustomAttributesCache};
extern Il2CppType t967_0_0_4;
FieldInfo t1468_f2_FieldInfo = 
{
	"_envoySink", &t967_0_0_4, &t1468_TI, offsetof(t1468, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1466_0_0_4;
FieldInfo t1468_f3_FieldInfo = 
{
	"_objRef", &t1466_0_0_4, &t1468_TI, offsetof(t1468, f3), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1468_f4_FieldInfo = 
{
	"_disposed", &t40_0_0_1, &t1468_TI, offsetof(t1468, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1468_FIs[] =
{
	&t1468_f0_FieldInfo,
	&t1468_f1_FieldInfo,
	&t1468_f2_FieldInfo,
	&t1468_f3_FieldInfo,
	&t1468_f4_FieldInfo,
	NULL
};
static PropertyInfo t1468____ChannelSink_PropertyInfo = 
{
	&t1468_TI, "ChannelSink", &m7969_MI, &m7970_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1468____ObjectUri_PropertyInfo = 
{
	&t1468_TI, "ObjectUri", &m7971_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1468____Disposed_PropertyInfo = 
{
	&t1468_TI, "Disposed", &m7972_MI, &m7973_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1468_PIs[] =
{
	&t1468____ChannelSink_PropertyInfo,
	&t1468____ObjectUri_PropertyInfo,
	&t1468____Disposed_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1468_m7968_ParameterInfos[] = 
{
	{"objectUri", 0, 134221983, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7968_MI = 
{
	".ctor", (methodPointerType)&m7968, &t1468_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1468_m7968_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3553, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1468_m10207_ParameterInfos[] = 
{
	{"requestedType", 0, 134221984, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1466_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10207_MI = 
{
	"CreateObjRef", NULL, &t1468_TI, &t1466_0_0_0, RuntimeInvoker_t29_t29, t1468_m10207_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, false, 3554, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t967_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7969_MI = 
{
	"get_ChannelSink", (methodPointerType)&m7969, &t1468_TI, &t967_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3555, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t967_0_0_0;
static ParameterInfo t1468_m7970_ParameterInfos[] = 
{
	{"value", 0, 134221985, &EmptyCustomAttributesCache, &t967_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7970_MI = 
{
	"set_ChannelSink", (methodPointerType)&m7970, &t1468_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1468_m7970_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 3556, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7971_MI = 
{
	"get_ObjectUri", (methodPointerType)&m7971, &t1468_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3557, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7972_MI = 
{
	"get_Disposed", (methodPointerType)&m7972, &t1468_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3558, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1468_m7973_ParameterInfos[] = 
{
	{"value", 0, 134221986, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7973_MI = 
{
	"set_Disposed", (methodPointerType)&m7973, &t1468_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1468_m7973_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 3559, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1468_MIs[] =
{
	&m7968_MI,
	&m10207_MI,
	&m7969_MI,
	&m7970_MI,
	&m7971_MI,
	&m7972_MI,
	&m7973_MI,
	NULL
};
static MethodInfo* t1468_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1468_0_0_0;
extern Il2CppType t1468_1_0_0;
struct t1468;
TypeInfo t1468_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Identity", "System.Runtime.Remoting", t1468_MIs, t1468_PIs, t1468_FIs, NULL, &t29_TI, NULL, NULL, &t1468_TI, NULL, t1468_VT, &EmptyCustomAttributesCache, &t1468_TI, &t1468_0_0_0, &t1468_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1468), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 3, 5, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1476.h"
extern TypeInfo t1476_TI;
#include "t1476MD.h"
extern MethodInfo m9612_MI;
extern MethodInfo m9608_MI;
extern MethodInfo m7988_MI;
extern MethodInfo m7984_MI;


extern MethodInfo m7974_MI;
 void m7974 (t1469 * __this, t7* p0, t1466 * p1, MethodInfo* method){
	t29 * V_0 = {0};
	t1469 * G_B2_0 = {0};
	t1469 * G_B1_0 = {0};
	t29 * G_B3_0 = {0};
	t1469 * G_B3_1 = {0};
	{
		m7968(__this, p0, &m7968_MI);
		__this->f3 = p1;
		t1466 * L_0 = (__this->f3);
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7984_MI, L_0);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_0030;
		}
	}
	{
		t1466 * L_2 = (__this->f3);
		t29 * L_3 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7984_MI, L_2);
		t29 * L_4 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10210_MI, L_3);
		V_0 = L_4;
		G_B3_0 = V_0;
		G_B3_1 = G_B1_0;
		goto IL_0031;
	}

IL_0030:
	{
		G_B3_0 = ((t29 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_0031:
	{
		G_B3_1->f2 = G_B3_0;
		return;
	}
}
extern MethodInfo m7975_MI;
 t774 * m7975 (t1469 * __this, MethodInfo* method){
	{
		t1476 * L_0 = (__this->f5);
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m9612_MI, L_0);
		return ((t774 *)Castclass(L_1, InitializedTypeInfo(&t774_TI)));
	}
}
extern MethodInfo m7976_MI;
 void m7976 (t1469 * __this, t774 * p0, MethodInfo* method){
	{
		t1476 * L_0 = (t1476 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1476_TI));
		m9608(L_0, p0, &m9608_MI);
		__this->f5 = L_0;
		return;
	}
}
extern MethodInfo m7977_MI;
 t1466 * m7977 (t1469 * __this, t42 * p0, MethodInfo* method){
	{
		t1466 * L_0 = (__this->f3);
		return L_0;
	}
}
 t7* m7978 (t1469 * __this, MethodInfo* method){
	{
		t1466 * L_0 = (__this->f3);
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7988_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern Il2CppType t1476_0_0_1;
FieldInfo t1469_f5_FieldInfo = 
{
	"_proxyReference", &t1476_0_0_1, &t1469_TI, offsetof(t1469, f5), &EmptyCustomAttributesCache};
static FieldInfo* t1469_FIs[] =
{
	&t1469_f5_FieldInfo,
	NULL
};
static PropertyInfo t1469____ClientProxy_PropertyInfo = 
{
	&t1469_TI, "ClientProxy", &m7975_MI, &m7976_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1469____TargetUri_PropertyInfo = 
{
	&t1469_TI, "TargetUri", &m7978_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1469_PIs[] =
{
	&t1469____ClientProxy_PropertyInfo,
	&t1469____TargetUri_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1466_0_0_0;
static ParameterInfo t1469_m7974_ParameterInfos[] = 
{
	{"objectUri", 0, 134221987, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"objRef", 1, 134221988, &EmptyCustomAttributesCache, &t1466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7974_MI = 
{
	".ctor", (methodPointerType)&m7974, &t1469_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1469_m7974_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3560, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t774_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7975_MI = 
{
	"get_ClientProxy", (methodPointerType)&m7975, &t1469_TI, &t774_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3561, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t774_0_0_0;
static ParameterInfo t1469_m7976_ParameterInfos[] = 
{
	{"value", 0, 134221989, &EmptyCustomAttributesCache, &t774_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7976_MI = 
{
	"set_ClientProxy", (methodPointerType)&m7976, &t1469_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1469_m7976_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 3562, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1469_m7977_ParameterInfos[] = 
{
	{"requestedType", 0, 134221990, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1466_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7977_MI = 
{
	"CreateObjRef", (methodPointerType)&m7977, &t1469_TI, &t1466_0_0_0, RuntimeInvoker_t29_t29, t1469_m7977_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 3563, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7978_MI = 
{
	"get_TargetUri", (methodPointerType)&m7978, &t1469_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3564, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1469_MIs[] =
{
	&m7974_MI,
	&m7975_MI,
	&m7976_MI,
	&m7977_MI,
	&m7978_MI,
	NULL
};
static MethodInfo* t1469_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7977_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1469_1_0_0;
struct t1469;
TypeInfo t1469_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ClientIdentity", "System.Runtime.Remoting", t1469_MIs, t1469_PIs, t1469_FIs, NULL, &t1468_TI, NULL, NULL, &t1469_TI, NULL, t1469_VT, &EmptyCustomAttributesCache, &t1469_TI, &t1469_0_0_0, &t1469_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1469), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t1094_TI;
extern TypeInfo t1095_TI;
#include "t1095MD.h"
extern MethodInfo m7992_MI;
extern MethodInfo m8154_MI;
extern MethodInfo m8155_MI;
extern MethodInfo m9663_MI;
extern MethodInfo m9000_MI;
extern MethodInfo m8144_MI;
extern MethodInfo m3982_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m8005_MI;


extern MethodInfo m7979_MI;
 void m7979 (t1466 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m7992(__this, &m7992_MI);
		return;
	}
}
extern MethodInfo m7980_MI;
 void m7980 (t1466 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t1522 * V_0 = {0};
	bool V_1 = false;
	int32_t V_2 = 0;
	t29 * V_3 = {0};
	t7* V_4 = {0};
	t485 * V_5 = {0};
	int32_t V_6 = 0;
	{
		m1331(__this, &m1331_MI);
		t1522 * L_0 = m8145(p0, &m8145_MI);
		V_0 = L_0;
		V_1 = 1;
		goto IL_0155;
	}

IL_0014:
	{
		t7* L_1 = m8154(V_0, &m8154_MI);
		V_4 = L_1;
		if (!V_4)
		{
			goto IL_014f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		if ((((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f8))
		{
			goto IL_0087;
		}
	}
	{
		t485 * L_2 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_2, 6, &m4038_MI);
		V_5 = L_2;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral226, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral1620, 1);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral1621, 2);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral1622, 3);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral1623, 4);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral1624, 5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f8 = V_5;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		bool L_3 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f8), V_4, (&V_6));
		if (!L_3)
		{
			goto IL_014f;
		}
	}
	{
		if (V_6 == 0)
		{
			goto IL_00be;
		}
		if (V_6 == 1)
		{
			goto IL_00d4;
		}
		if (V_6 == 2)
		{
			goto IL_00ea;
		}
		if (V_6 == 3)
		{
			goto IL_00fd;
		}
		if (V_6 == 4)
		{
			goto IL_0110;
		}
		if (V_6 == 5)
		{
			goto IL_013c;
		}
	}
	{
		goto IL_014f;
	}

IL_00be:
	{
		t29 * L_4 = m8155(V_0, &m8155_MI);
		__this->f1 = ((t7*)Castclass(L_4, (&t7_TI)));
		goto IL_0155;
	}

IL_00d4:
	{
		t29 * L_5 = m8155(V_0, &m8155_MI);
		__this->f2 = ((t29 *)Castclass(L_5, InitializedTypeInfo(&t1478_TI)));
		goto IL_0155;
	}

IL_00ea:
	{
		t29 * L_6 = m8155(V_0, &m8155_MI);
		__this->f0 = ((t29 *)Castclass(L_6, InitializedTypeInfo(&t1477_TI)));
		goto IL_0155;
	}

IL_00fd:
	{
		t29 * L_7 = m8155(V_0, &m8155_MI);
		__this->f3 = ((t29 *)Castclass(L_7, InitializedTypeInfo(&t1479_TI)));
		goto IL_0155;
	}

IL_0110:
	{
		t29 * L_8 = m8155(V_0, &m8155_MI);
		V_3 = L_8;
		if (!((t7*)IsInst(V_3, (&t7_TI))))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_9 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9663_MI, ((t29 *)Castclass(V_3, InitializedTypeInfo(&t289_TI))), (t29 *)NULL);
		V_2 = L_9;
		goto IL_0135;
	}

IL_012e:
	{
		V_2 = ((*(int32_t*)((int32_t*)UnBox (V_3, InitializedTypeInfo(&t44_TI)))));
	}

IL_0135:
	{
		if (V_2)
		{
			goto IL_013a;
		}
	}
	{
		V_1 = 0;
	}

IL_013a:
	{
		goto IL_0155;
	}

IL_013c:
	{
		t29 * L_10 = m8155(V_0, &m8155_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		int32_t L_11 = m9000(NULL, L_10, &m9000_MI);
		__this->f4 = L_11;
		goto IL_0155;
	}

IL_014f:
	{
		t345 * L_12 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_12, &m1516_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_0155:
	{
		bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(&m8156_MI, V_0);
		if (L_13)
		{
			goto IL_0014;
		}
	}
	{
		if (!V_1)
		{
			goto IL_0175;
		}
	}
	{
		int32_t L_14 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		__this->f4 = ((int32_t)((int32_t)L_14|(int32_t)(((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f6)));
	}

IL_0175:
	{
		return;
	}
}
extern MethodInfo m7981_MI;
 void m7981 (t29 * __this, MethodInfo* method){
	{
		((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f6 = 1;
		((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f7 = 2;
		return;
	}
}
extern MethodInfo m7982_MI;
 bool m7982 (t1466 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)(((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f7)))) > ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m7983_MI;
 t29 * m7983 (t1466 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
 t29 * m7984 (t1466 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m7985_MI;
 void m7985 (t1466 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
 t29 * m7986 (t1466 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m7987_MI;
 void m7987 (t1466 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 t7* m7988 (t1466 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m7989_MI;
 void m7989 (t1466 * __this, t7* p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m7990_MI;
 void m7990 (t1466 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t42 * L_0 = m1430(__this, &m1430_MI);
		m8144(p0, L_0, &m8144_MI);
		t7* L_1 = (__this->f1);
		m3997(p0, (t7*) &_stringLiteral226, L_1, &m3997_MI);
		t29 * L_2 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t1478_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1620, L_2, L_3, &m3982_MI);
		t29 * L_4 = (__this->f3);
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t1479_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1622, L_4, L_5, &m3982_MI);
		t29 * L_6 = (__this->f0);
		t42 * L_7 = m1554(NULL, LoadTypeToken(&t1477_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1621, L_6, L_7, &m3982_MI);
		int32_t L_8 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1624, L_8, &m3984_MI);
		return;
	}
}
extern MethodInfo m7991_MI;
 t29 * m7991 (t1466 * __this, t735  p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		if ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)(((t1466_SFs*)InitializedTypeInfo(&t1466_TI)->static_fields)->f6)))) <= ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_1 = m8005(NULL, __this, &m8005_MI);
		return L_1;
	}

IL_0016:
	{
		return __this;
	}
}
 void m7992 (t1466 * __this, MethodInfo* method){
	{
		t1426 * L_0 = (t1426 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1426_TI));
		m7776(L_0, &m7776_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m7993_MI;
 t42 * m7993 (t1466 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f5);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		t29 * L_1 = (__this->f2);
		t7* L_2 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10206_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m6013(NULL, L_2, &m6013_MI);
		__this->f5 = L_3;
	}

IL_001e:
	{
		t42 * L_4 = (__this->f5);
		return L_4;
	}
}
// Metadata Definition System.Runtime.Remoting.ObjRef
extern Il2CppType t1477_0_0_1;
FieldInfo t1466_f0_FieldInfo = 
{
	"channel_info", &t1477_0_0_1, &t1466_TI, offsetof(t1466, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1466_f1_FieldInfo = 
{
	"uri", &t7_0_0_1, &t1466_TI, offsetof(t1466, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1478_0_0_1;
FieldInfo t1466_f2_FieldInfo = 
{
	"typeInfo", &t1478_0_0_1, &t1466_TI, offsetof(t1466, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1479_0_0_1;
FieldInfo t1466_f3_FieldInfo = 
{
	"envoyInfo", &t1479_0_0_1, &t1466_TI, offsetof(t1466, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1466_f4_FieldInfo = 
{
	"flags", &t44_0_0_1, &t1466_TI, offsetof(t1466, f4), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1466_f5_FieldInfo = 
{
	"_serverType", &t42_0_0_1, &t1466_TI, offsetof(t1466, f5), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_17;
FieldInfo t1466_f6_FieldInfo = 
{
	"MarshalledObjectRef", &t44_0_0_17, &t1466_TI, offsetof(t1466_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_17;
FieldInfo t1466_f7_FieldInfo = 
{
	"WellKnowObjectRef", &t44_0_0_17, &t1466_TI, offsetof(t1466_SFs, f7), &EmptyCustomAttributesCache};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t1466__CustomAttributeCache_U3CU3Ef__switch$map26;
FieldInfo t1466_f8_FieldInfo = 
{
	"<>f__switch$map26", &t485_0_0_17, &t1466_TI, offsetof(t1466_SFs, f8), &t1466__CustomAttributeCache_U3CU3Ef__switch$map26};
static FieldInfo* t1466_FIs[] =
{
	&t1466_f0_FieldInfo,
	&t1466_f1_FieldInfo,
	&t1466_f2_FieldInfo,
	&t1466_f3_FieldInfo,
	&t1466_f4_FieldInfo,
	&t1466_f5_FieldInfo,
	&t1466_f6_FieldInfo,
	&t1466_f7_FieldInfo,
	&t1466_f8_FieldInfo,
	NULL
};
static PropertyInfo t1466____IsReferenceToWellKnow_PropertyInfo = 
{
	&t1466_TI, "IsReferenceToWellKnow", &m7982_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1466____ChannelInfo_PropertyInfo = 
{
	&t1466_TI, "ChannelInfo", &m7983_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1466____EnvoyInfo_PropertyInfo = 
{
	&t1466_TI, "EnvoyInfo", &m7984_MI, &m7985_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1466____TypeInfo_PropertyInfo = 
{
	&t1466_TI, "TypeInfo", &m7986_MI, &m7987_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1466____URI_PropertyInfo = 
{
	&t1466_TI, "URI", &m7988_MI, &m7989_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1466____ServerType_PropertyInfo = 
{
	&t1466_TI, "ServerType", &m7993_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1466_PIs[] =
{
	&t1466____IsReferenceToWellKnow_PropertyInfo,
	&t1466____ChannelInfo_PropertyInfo,
	&t1466____EnvoyInfo_PropertyInfo,
	&t1466____TypeInfo_PropertyInfo,
	&t1466____URI_PropertyInfo,
	&t1466____ServerType_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7979_MI = 
{
	".ctor", (methodPointerType)&m7979, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3565, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1466_m7980_ParameterInfos[] = 
{
	{"info", 0, 134221991, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221992, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7980_MI = 
{
	".ctor", (methodPointerType)&m7980, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1466_m7980_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 3566, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7981_MI = 
{
	".cctor", (methodPointerType)&m7981, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3567, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7982_MI = 
{
	"get_IsReferenceToWellKnow", (methodPointerType)&m7982, &t1466_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 3568, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1477_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1466__CustomAttributeCache_m7983;
MethodInfo m7983_MI = 
{
	"get_ChannelInfo", (methodPointerType)&m7983, &t1466_TI, &t1477_0_0_0, RuntimeInvoker_t29, NULL, &t1466__CustomAttributeCache_m7983, 2502, 0, 6, 0, false, false, 3569, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1479_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7984_MI = 
{
	"get_EnvoyInfo", (methodPointerType)&m7984, &t1466_TI, &t1479_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 7, 0, false, false, 3570, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1479_0_0_0;
static ParameterInfo t1466_m7985_ParameterInfos[] = 
{
	{"value", 0, 134221993, &EmptyCustomAttributesCache, &t1479_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7985_MI = 
{
	"set_EnvoyInfo", (methodPointerType)&m7985, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1466_m7985_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 8, 1, false, false, 3571, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1478_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7986_MI = 
{
	"get_TypeInfo", (methodPointerType)&m7986, &t1466_TI, &t1478_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 9, 0, false, false, 3572, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1478_0_0_0;
static ParameterInfo t1466_m7987_ParameterInfos[] = 
{
	{"value", 0, 134221994, &EmptyCustomAttributesCache, &t1478_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7987_MI = 
{
	"set_TypeInfo", (methodPointerType)&m7987, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1466_m7987_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 10, 1, false, false, 3573, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7988_MI = 
{
	"get_URI", (methodPointerType)&m7988, &t1466_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 11, 0, false, false, 3574, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1466_m7989_ParameterInfos[] = 
{
	{"value", 0, 134221995, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7989_MI = 
{
	"set_URI", (methodPointerType)&m7989, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1466_m7989_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 12, 1, false, false, 3575, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1466_m7990_ParameterInfos[] = 
{
	{"info", 0, 134221996, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221997, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7990_MI = 
{
	"GetObjectData", (methodPointerType)&m7990, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1466_m7990_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 13, 2, false, false, 3576, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t735_0_0_0;
static ParameterInfo t1466_m7991_ParameterInfos[] = 
{
	{"context", 0, 134221998, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7991_MI = 
{
	"GetRealObject", (methodPointerType)&m7991, &t1466_TI, &t29_0_0_0, RuntimeInvoker_t29_t735, t1466_m7991_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 14, 1, false, false, 3577, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7992_MI = 
{
	"UpdateChannelInfo", (methodPointerType)&m7992, &t1466_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 3578, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7993_MI = 
{
	"get_ServerType", (methodPointerType)&m7993, &t1466_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 3579, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1466_MIs[] =
{
	&m7979_MI,
	&m7980_MI,
	&m7981_MI,
	&m7982_MI,
	&m7983_MI,
	&m7984_MI,
	&m7985_MI,
	&m7986_MI,
	&m7987_MI,
	&m7988_MI,
	&m7989_MI,
	&m7990_MI,
	&m7991_MI,
	&m7992_MI,
	&m7993_MI,
	NULL
};
static MethodInfo* t1466_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7990_MI,
	&m7991_MI,
	&m7983_MI,
	&m7984_MI,
	&m7985_MI,
	&m7986_MI,
	&m7987_MI,
	&m7988_MI,
	&m7989_MI,
	&m7990_MI,
	&m7991_MI,
};
extern TypeInfo t2024_TI;
static TypeInfo* t1466_ITIs[] = 
{
	&t374_TI,
	&t2024_TI,
};
static Il2CppInterfaceOffsetPair t1466_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t2024_TI, 5},
};
void t1466_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1466_CustomAttributesCacheGenerator_U3CU3Ef__switch$map26(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t1400_TI;
#include "t1400.h"
#include "t1400MD.h"
extern MethodInfo m7732_MI;
void t1466_CustomAttributesCacheGenerator_m7983(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1466__CustomAttributeCache = {
1,
NULL,
&t1466_CustomAttributesCacheGenerator
};
CustomAttributesCache t1466__CustomAttributeCache_U3CU3Ef__switch$map26 = {
1,
NULL,
&t1466_CustomAttributesCacheGenerator_U3CU3Ef__switch$map26
};
CustomAttributesCache t1466__CustomAttributeCache_m7983 = {
1,
NULL,
&t1466_CustomAttributesCacheGenerator_m7983
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1466_1_0_0;
struct t1466;
extern CustomAttributesCache t1466__CustomAttributeCache;
extern CustomAttributesCache t1466__CustomAttributeCache_U3CU3Ef__switch$map26;
extern CustomAttributesCache t1466__CustomAttributeCache_m7983;
TypeInfo t1466_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObjRef", "System.Runtime.Remoting", t1466_MIs, t1466_PIs, t1466_FIs, NULL, &t29_TI, NULL, NULL, &t1466_TI, t1466_ITIs, t1466_VT, &t1466__CustomAttributeCache, &t1466_TI, &t1466_0_0_0, &t1466_1_0_0, t1466_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1466), 0, -1, sizeof(t1466_SFs), 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, true, false, false, 15, 6, 9, 0, 0, 15, 2, 2};
#include "t1480.h"
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m8844_MI;
extern MethodInfo m3989_MI;


extern MethodInfo m7994_MI;
 void m7994 (t29 * __this, MethodInfo* method){
	{
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f0 = (t7*)NULL;
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f1 = (t7*)NULL;
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f2 = (t7*)NULL;
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f3 = 0;
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f4 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f5 = L_0;
		t719 * L_1 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_1, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f6 = L_1;
		t719 * L_2 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_2, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f7 = L_2;
		t719 * L_3 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_3, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f8 = L_3;
		t719 * L_4 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_4, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f9 = L_4;
		t719 * L_5 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_5, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f10 = L_5;
		t719 * L_6 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_6, &m4209_MI);
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f11 = L_6;
		return;
	}
}
extern MethodInfo m7995_MI;
 t7* m7995 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		return (((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f1);
	}
}
 t7* m7996 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		if ((((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f2))
		{
			goto IL_0011;
		}
	}
	{
		t7* L_0 = m8844(NULL, &m8844_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f2 = L_0;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		return (((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f2);
	}
}
extern MethodInfo m7997_MI;
 t1472 * m7997 (t29 * __this, t42 * p0, MethodInfo* method){
	t719 * V_0 = {0};
	t1472 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		V_0 = (((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f9);
		m4000(NULL, V_0, &m4000_MI);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
			t29 * L_0 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, (((t1480_SFs*)InitializedTypeInfo(&t1480_TI)->static_fields)->f6), p0);
			V_1 = ((t1472 *)IsInst(L_0, InitializedTypeInfo(&t1472_TI)));
			// IL_001d: leave.s IL_0028
			leaveInstructions[0] = 0x28; // 1
			THROW_SENTINEL(IL_0028);
			// finally target depth: 1
		}

IL_001f:
		{
			// IL_001f: leave.s IL_0028
			leaveInstructions[0] = 0x28; // 1
			THROW_SENTINEL(IL_0028);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0021;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0021;
	}

IL_0021:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x28:
				goto IL_0028;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0028:
	{
		return V_1;
	}
}
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern Il2CppType t7_0_0_17;
FieldInfo t1480_f0_FieldInfo = 
{
	"applicationID", &t7_0_0_17, &t1480_TI, offsetof(t1480_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_17;
FieldInfo t1480_f1_FieldInfo = 
{
	"applicationName", &t7_0_0_17, &t1480_TI, offsetof(t1480_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_17;
FieldInfo t1480_f2_FieldInfo = 
{
	"processGuid", &t7_0_0_17, &t1480_TI, offsetof(t1480_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_17;
FieldInfo t1480_f3_FieldInfo = 
{
	"defaultConfigRead", &t40_0_0_17, &t1480_TI, offsetof(t1480_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_17;
FieldInfo t1480_f4_FieldInfo = 
{
	"defaultDelayedConfigRead", &t40_0_0_17, &t1480_TI, offsetof(t1480_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f5_FieldInfo = 
{
	"wellKnownClientEntries", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f6_FieldInfo = 
{
	"activatedClientEntries", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f7_FieldInfo = 
{
	"wellKnownServiceEntries", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f7), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f8_FieldInfo = 
{
	"activatedServiceEntries", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f8), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f9_FieldInfo = 
{
	"channelTemplates", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f9), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f10_FieldInfo = 
{
	"clientProviderTemplates", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f10), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t1480_f11_FieldInfo = 
{
	"serverProviderTemplates", &t719_0_0_17, &t1480_TI, offsetof(t1480_SFs, f11), &EmptyCustomAttributesCache};
static FieldInfo* t1480_FIs[] =
{
	&t1480_f0_FieldInfo,
	&t1480_f1_FieldInfo,
	&t1480_f2_FieldInfo,
	&t1480_f3_FieldInfo,
	&t1480_f4_FieldInfo,
	&t1480_f5_FieldInfo,
	&t1480_f6_FieldInfo,
	&t1480_f7_FieldInfo,
	&t1480_f8_FieldInfo,
	&t1480_f9_FieldInfo,
	&t1480_f10_FieldInfo,
	&t1480_f11_FieldInfo,
	NULL
};
static PropertyInfo t1480____ApplicationName_PropertyInfo = 
{
	&t1480_TI, "ApplicationName", &m7995_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1480____ProcessId_PropertyInfo = 
{
	&t1480_TI, "ProcessId", &m7996_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1480_PIs[] =
{
	&t1480____ApplicationName_PropertyInfo,
	&t1480____ProcessId_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7994_MI = 
{
	".cctor", (methodPointerType)&m7994, &t1480_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3580, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7995_MI = 
{
	"get_ApplicationName", (methodPointerType)&m7995, &t1480_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 3581, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7996_MI = 
{
	"get_ProcessId", (methodPointerType)&m7996, &t1480_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 3582, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1480_m7997_ParameterInfos[] = 
{
	{"svrType", 0, 134221999, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1472_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7997_MI = 
{
	"IsRemotelyActivatedClientType", (methodPointerType)&m7997, &t1480_TI, &t1472_0_0_0, RuntimeInvoker_t29_t29, t1480_m7997_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3583, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1480_MIs[] =
{
	&m7994_MI,
	&m7995_MI,
	&m7996_MI,
	&m7997_MI,
	NULL
};
static MethodInfo* t1480_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1480_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1480__CustomAttributeCache = {
1,
NULL,
&t1480_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1480_0_0_0;
extern Il2CppType t1480_1_0_0;
struct t1480;
extern CustomAttributesCache t1480__CustomAttributeCache;
TypeInfo t1480_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemotingConfiguration", "System.Runtime.Remoting", t1480_MIs, t1480_PIs, t1480_FIs, NULL, &t29_TI, NULL, NULL, &t1480_TI, NULL, t1480_VT, &t1480__CustomAttributeCache, &t1480_TI, &t1480_0_0_0, &t1480_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1480), 0, -1, sizeof(t1480_SFs), 0, -1, 1048961, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 2, 12, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t956MD.h"
extern MethodInfo m9516_MI;
extern MethodInfo m4202_MI;
extern MethodInfo m9517_MI;


extern MethodInfo m7998_MI;
 void m7998 (t1481 * __this, MethodInfo* method){
	{
		m9516(__this, &m9516_MI);
		return;
	}
}
 void m7999 (t1481 * __this, t7* p0, MethodInfo* method){
	{
		m4202(__this, p0, &m4202_MI);
		return;
	}
}
extern MethodInfo m8000_MI;
 void m8000 (t1481 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.RemotingException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7998_MI = 
{
	".ctor", (methodPointerType)&m7998, &t1481_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3584, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1481_m7999_ParameterInfos[] = 
{
	{"message", 0, 134222000, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7999_MI = 
{
	".ctor", (methodPointerType)&m7999, &t1481_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1481_m7999_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3585, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1481_m8000_ParameterInfos[] = 
{
	{"info", 0, 134222001, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134222002, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m8000_MI = 
{
	".ctor", (methodPointerType)&m8000, &t1481_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1481_m8000_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 3586, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1481_MIs[] =
{
	&m7998_MI,
	&m7999_MI,
	&m8000_MI,
	NULL
};
extern MethodInfo m2856_MI;
extern MethodInfo m2857_MI;
extern MethodInfo m2858_MI;
extern MethodInfo m1684_MI;
extern MethodInfo m2859_MI;
extern MethodInfo m2860_MI;
extern MethodInfo m2861_MI;
static MethodInfo* t1481_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
extern TypeInfo t590_TI;
static Il2CppInterfaceOffsetPair t1481_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1481__CustomAttributeCache = {
1,
NULL,
&t1481_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1481_0_0_0;
extern Il2CppType t1481_1_0_0;
extern TypeInfo t956_TI;
struct t1481;
extern CustomAttributesCache t1481__CustomAttributeCache;
TypeInfo t1481_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemotingException", "System.Runtime.Remoting", t1481_MIs, NULL, NULL, NULL, &t956_TI, NULL, NULL, &t1481_TI, NULL, t1481_VT, &t1481__CustomAttributeCache, &t1481_TI, &t1481_0_0_0, &t1481_1_0_0, t1481_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1481), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1482.h"
#ifndef _MSC_VER
#else
#endif

#include "t1648.h"
#include "t1523.h"
#include "t1483.h"
#include "t1495.h"
#include "t1165.h"
#include "t1484.h"
#include "t490.h"
#include "t631.h"
#include "t632.h"
#include "t660.h"
#include "t1487.h"
#include "t1486.h"
extern TypeInfo t1483_TI;
extern TypeInfo t1648_TI;
extern TypeInfo t1165_TI;
extern TypeInfo t631_TI;
extern TypeInfo t634_TI;
extern TypeInfo t632_TI;
extern TypeInfo t660_TI;
extern TypeInfo t1487_TI;
extern TypeInfo t1486_TI;
#include "t735MD.h"
#include "t1483MD.h"
#include "t1648MD.h"
#include "t1165MD.h"
#include "t966MD.h"
#include "t1487MD.h"
#include "t1486MD.h"
#include "t1485MD.h"
extern MethodInfo m8158_MI;
extern MethodInfo m8038_MI;
extern MethodInfo m8040_MI;
extern MethodInfo m8020_MI;
extern MethodInfo m9268_MI;
extern MethodInfo m9273_MI;
extern MethodInfo m2933_MI;
extern MethodInfo m66_MI;
extern MethodInfo m8016_MI;
extern MethodInfo m8013_MI;
extern MethodInfo m8024_MI;
extern MethodInfo m8006_MI;
extern MethodInfo m8019_MI;
extern MethodInfo m5291_MI;
extern MethodInfo m8003_MI;
extern MethodInfo m8010_MI;
extern MethodInfo m2999_MI;
extern MethodInfo m6033_MI;
extern MethodInfo m4124_MI;
extern MethodInfo m4292_MI;
extern MethodInfo m8022_MI;
extern MethodInfo m8014_MI;
extern MethodInfo m2922_MI;
extern MethodInfo m1715_MI;
extern MethodInfo m1770_MI;
extern MethodInfo m4291_MI;
extern MethodInfo m8028_MI;
extern MethodInfo m8027_MI;
extern MethodInfo m8017_MI;
extern MethodInfo m4030_MI;
extern MethodInfo m8026_MI;
extern MethodInfo m8015_MI;
extern MethodInfo m4217_MI;


extern MethodInfo m8001_MI;
 void m8001 (t29 * __this, MethodInfo* method){
	t1460 * V_0 = {0};
	t735  V_1 = {0};
	t1648  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0 = L_0;
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f4 = 1;
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5 = ((int32_t)52);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1460_TI));
		t1460 * L_1 = (t1460 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1460_TI));
		m7925(L_1, &m7925_MI);
		V_0 = L_1;
		m8158((&V_1), ((int32_t)16), NULL, &m8158_MI);
		t1483 * L_2 = (t1483 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1483_TI));
		m8038(L_2, V_0, V_1, &m8038_MI);
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f1 = L_2;
		t1483 * L_3 = (t1483 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1483_TI));
		m8038(L_3, (t29 *)NULL, V_1, &m8038_MI);
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f2 = L_3;
		m8040((((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f1), 1, &m8040_MI);
		m8040((((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f2), 1, &m8040_MI);
		m8020(NULL, &m8020_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1648_TI));
		t1648  L_4 = m9268(NULL, &m9268_MI);
		V_2 = L_4;
		t7* L_5 = m9273((&V_2), &m9273_MI);
		t7* L_6 = m2933(L_5, ((int32_t)45), ((int32_t)95), &m2933_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_7 = m66(NULL, L_6, (t7*) &_stringLiteral534, &m66_MI);
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f3 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t1421_0_0_0), &m1554_MI);
		m8016(NULL, L_8, (t7*) &_stringLiteral1625, 1, &m8016_MI);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		t557 * L_10 = (t557 *)VirtFuncInvoker2< t557 *, t7*, int32_t >::Invoke(&m6023_MI, L_9, (t7*) &_stringLiteral1626, ((int32_t)36));
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f6 = L_10;
		t42 * L_11 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		t557 * L_12 = (t557 *)VirtFuncInvoker2< t557 *, t7*, int32_t >::Invoke(&m6023_MI, L_11, (t7*) &_stringLiteral1627, ((int32_t)36));
		((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f7 = L_12;
		return;
	}
}
 t636 * m8002 (t29 * __this, t42 * p0, t636 * p1, MethodInfo* method){
	typedef t636 * (*m8002_ftn) (t42 *, t636 *);
	static m8002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m8002_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.Remoting.RemotingServices::GetVirtualMethod(System.Type,System.Reflection.MethodBase)");
	return _il2cpp_icall_func(p0, p1);
}
 bool m8003 (t29 * __this, t29 * p0, MethodInfo* method){
	typedef bool (*m8003_ftn) (t29 *);
	static m8003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m8003_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.Remoting.RemotingServices::IsTransparentProxy(System.Object)");
	return _il2cpp_icall_func(p0);
}
 t42 * m8004 (t29 * __this, t7* p0, MethodInfo* method){
	t1165 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t1468 * L_0 = m8013(NULL, p0, &m8013_MI);
		V_0 = ((t1165 *)IsInst(L_0, InitializedTypeInfo(&t1165_TI)));
		if (V_0)
		{
			goto IL_0011;
		}
	}
	{
		return (t42 *)NULL;
	}

IL_0011:
	{
		t42 * L_1 = m8024(V_0, &m8024_MI);
		return L_1;
	}
}
 t29 * m8005 (t29 * __this, t1466 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_0 = m8006(NULL, p0, 1, &m8006_MI);
		return L_0;
	}
}
 t29 * m8006 (t29 * __this, t1466 * p0, bool p1, MethodInfo* method){
	t42 * V_0 = {0};
	t29 * V_1 = {0};
	t29 * V_2 = {0};
	t1464 * V_3 = {0};
	t42 * G_B3_0 = {0};
	{
		if (!p1)
		{
			goto IL_000b;
		}
	}
	{
		t42 * L_0 = m7993(p0, &m7993_MI);
		G_B3_0 = L_0;
		goto IL_0015;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t774_0_0_0), &m1554_MI);
		G_B3_0 = L_1;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		if (V_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t774_0_0_0), &m1554_MI);
		V_0 = L_2;
	}

IL_0024:
	{
		bool L_3 = m7982(p0, &m7982_MI);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_4 = m8019(NULL, p0, V_0, &m8019_MI);
		V_1 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1471_TI));
		m7961(NULL, V_1, p0, &m7961_MI);
		return V_1;
	}

IL_003d:
	{
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5996_MI, V_0);
		if (!L_5)
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_6 = m1554(NULL, LoadTypeToken(&t1464_0_0_0), &m1554_MI);
		t490 * L_7 = m5291(NULL, V_0, L_6, 1, &m5291_MI);
		V_3 = ((t1464 *)Castclass(L_7, InitializedTypeInfo(&t1464_TI)));
		if (!V_3)
		{
			goto IL_0078;
		}
	}
	{
		t1465 * L_8 = (t1465 *)VirtFuncInvoker4< t1465 *, t1466 *, t42 *, t29 *, t1425 * >::Invoke(&m7944_MI, V_3, p0, V_0, NULL, (t1425 *)NULL);
		t29 * L_9 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7953_MI, L_8);
		V_2 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1471_TI));
		m7961(NULL, V_2, p0, &m7961_MI);
		return V_2;
	}

IL_0078:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_10 = m8018(NULL, p0, V_0, &m8018_MI);
		V_2 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1471_TI));
		m7961(NULL, V_2, p0, &m7961_MI);
		return V_2;
	}
}
 t1465 * m8007 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		bool L_0 = m8003(NULL, p0, &m8003_MI);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		t1481 * L_1 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_1, (t7*) &_stringLiteral1628, &m7999_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t1465 * L_2 = (((t1467 *)Castclass(p0, InitializedTypeInfo(&t1467_TI)))->f0);
		return L_2;
	}
}
 t636 * m8008 (t29 * __this, t29 * p0, MethodInfo* method){
	t42 * V_0 = {0};
	{
		t7* L_0 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10200_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m6013(NULL, L_0, &m6013_MI);
		V_0 = L_1;
		if (V_0)
		{
			goto IL_002a;
		}
	}
	{
		t7* L_2 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10200_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1685(NULL, (t7*) &_stringLiteral1629, L_2, (t7*) &_stringLiteral1376, &m1685_MI);
		t1481 * L_4 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_4, L_3, &m7999_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002a:
	{
		t7* L_5 = (t7*)InterfaceFuncInvoker0< t7* >::Invoke(&m10198_MI, p0);
		t29 * L_6 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m10199_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t636 * L_7 = m8009(NULL, V_0, L_5, ((t537*)Castclass(L_6, InitializedTypeInfo(&t537_TI))), &m8009_MI);
		return L_7;
	}
}
 t636 * m8009 (t29 * __this, t42 * p0, t7* p1, t537* p2, MethodInfo* method){
	t636 * V_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, p0);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t636 * L_1 = m8010(NULL, p0, p1, p2, &m8010_MI);
		return L_1;
	}

IL_0011:
	{
		V_0 = (t636 *)NULL;
		if (p2)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t557 * L_2 = (t557 *)VirtFuncInvoker2< t557 *, t7*, int32_t >::Invoke(&m6023_MI, p0, p1, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5));
		V_0 = L_2;
		goto IL_0035;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t557 * L_3 = (t557 *)VirtFuncInvoker5< t557 *, t7*, int32_t, t631 *, t537*, t634* >::Invoke(&m2999_MI, p0, p1, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5), (t631 *)NULL, p2, (t634*)(t634*)NULL);
		V_0 = L_3;
	}

IL_0035:
	{
		if (!V_0)
		{
			goto IL_003a;
		}
	}
	{
		return V_0;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1713(NULL, p1, (t7*) &_stringLiteral1626, &m1713_MI);
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		return (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f6);
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_5 = m1713(NULL, p1, (t7*) &_stringLiteral1627, &m1713_MI);
		if (!L_5)
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		return (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f7);
	}

IL_0060:
	{
		if (p2)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t660 * L_6 = (t660 *)VirtFuncInvoker4< t660 *, int32_t, t631 *, t537*, t634* >::Invoke(&m6033_MI, p0, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5), (t631 *)NULL, (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3), (t634*)(t634*)NULL);
		return L_6;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t660 * L_7 = (t660 *)VirtFuncInvoker4< t660 *, int32_t, t631 *, t537*, t634* >::Invoke(&m6033_MI, p0, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5), (t631 *)NULL, p2, (t634*)(t634*)NULL);
		return L_7;
	}
}
 t636 * m8010 (t29 * __this, t42 * p0, t7* p1, t537* p2, MethodInfo* method){
	t636 * V_0 = {0};
	t42 * V_1 = {0};
	t537* V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = (t636 *)NULL;
		if (p2)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t557 * L_0 = (t557 *)VirtFuncInvoker2< t557 *, t7*, int32_t >::Invoke(&m6023_MI, p0, p1, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5));
		V_0 = L_0;
		goto IL_0024;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t557 * L_1 = (t557 *)VirtFuncInvoker5< t557 *, t7*, int32_t, t631 *, t537*, t634* >::Invoke(&m2999_MI, p0, p1, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f5), (t631 *)NULL, p2, (t634*)(t634*)NULL);
		V_0 = L_1;
	}

IL_0024:
	{
		if (!V_0)
		{
			goto IL_0029;
		}
	}
	{
		return V_0;
	}

IL_0029:
	{
		t537* L_2 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m9850_MI, p0);
		V_2 = L_2;
		V_3 = 0;
		goto IL_004a;
	}

IL_0034:
	{
		int32_t L_3 = V_3;
		V_1 = (*(t42 **)(t42 **)SZArrayLdElema(V_2, L_3));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t636 * L_4 = m8010(NULL, V_1, p1, p2, &m8010_MI);
		V_0 = L_4;
		if (!V_0)
		{
			goto IL_0046;
		}
	}
	{
		return V_0;
	}

IL_0046:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_004a:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_2)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		return (t636 *)NULL;
	}
}
extern MethodInfo m8011_MI;
 t29 * m8011 (t29 * __this, t1472 * p0, t316* p1, MethodInfo* method){
	t731 * V_0 = {0};
	{
		t1474* L_0 = m7963(p0, &m7963_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		if (!p1)
		{
			goto IL_0047;
		}
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_1 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_1, &m3980_MI);
		V_0 = L_1;
		t1474* L_2 = m7963(p0, &m7963_MI);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		t1474* L_3 = m7963(p0, &m7963_MI);
		VirtActionInvoker1< t29 * >::Invoke(&m4124_MI, V_0, (t29 *)(t29 *)L_3);
	}

IL_0025:
	{
		if (!p1)
		{
			goto IL_002f;
		}
	}
	{
		VirtActionInvoker1< t29 * >::Invoke(&m4124_MI, V_0, (t29 *)(t29 *)p1);
	}

IL_002f:
	{
		t42 * L_4 = m7964(p0, &m7964_MI);
		t7* L_5 = m7962(p0, &m7962_MI);
		t316* L_6 = (t316*)VirtFuncInvoker0< t316* >::Invoke(&m6654_MI, V_0);
		t29 * L_7 = m4292(NULL, L_4, L_5, L_6, &m4292_MI);
		return L_7;
	}

IL_0047:
	{
		t42 * L_8 = m7964(p0, &m7964_MI);
		t7* L_9 = m7962(p0, &m7962_MI);
		t29 * L_10 = m4292(NULL, L_8, L_9, (t316*)(t316*)NULL, &m4292_MI);
		return L_10;
	}
}
extern MethodInfo m8012_MI;
 t29 * m8012 (t29 * __this, t42 * p0, t316* p1, MethodInfo* method){
	t1464 * V_0 = {0};
	t1470 * V_1 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5996_MI, p0);
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t1464_0_0_0), &m1554_MI);
		t490 * L_2 = m5291(NULL, p0, L_1, 1, &m5291_MI);
		V_0 = ((t1464 *)Castclass(L_2, InitializedTypeInfo(&t1464_TI)));
		if (!V_0)
		{
			goto IL_002a;
		}
	}
	{
		t774 * L_3 = (t774 *)VirtFuncInvoker1< t774 *, t42 * >::Invoke(&m7943_MI, V_0, p0);
		return L_3;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1470_TI));
		t1470 * L_4 = (t1470 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1470_TI));
		m7956(L_4, p0, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f3), p1, &m7956_MI);
		V_1 = L_4;
		t29 * L_5 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7953_MI, V_1);
		return L_5;
	}
}
 t1468 * m8013 (t29 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	t719 * V_1 = {0};
	t1468 * V_2 = {0};
	t1468 * V_3 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t7* L_0 = m8022(NULL, p0, &m8022_MI);
		V_0 = L_0;
		V_1 = (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0);
		m4000(NULL, V_1, &m4000_MI);
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t29 * L_1 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), V_0);
			V_2 = ((t1468 *)Castclass(L_1, InitializedTypeInfo(&t1468_TI)));
			if (V_2)
			{
				goto IL_0042;
			}
		}

IL_0027:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t7* L_2 = m8014(NULL, p0, &m8014_MI);
			V_0 = L_2;
			if (!V_0)
			{
				goto IL_0042;
			}
		}

IL_0031:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), V_0);
			V_2 = ((t1468 *)Castclass(L_3, InitializedTypeInfo(&t1468_TI)));
		}

IL_0042:
		{
			V_3 = V_2;
			// IL_0044: leave.s IL_004f
			leaveInstructions[0] = 0x4F; // 1
			THROW_SENTINEL(IL_004f);
			// finally target depth: 1
		}

IL_0046:
		{
			// IL_0046: leave.s IL_004f
			leaveInstructions[0] = 0x4F; // 1
			THROW_SENTINEL(IL_004f);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0048;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0048;
	}

IL_0048:
	{ // begin finally (depth: 1)
		m4001(NULL, V_1, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x4F:
				goto IL_004f;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_004f:
	{
		return V_3;
	}
}
 t7* m8014 (t29 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		t7* L_0 = m7995(NULL, &m7995_MI);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000b;
		}
	}
	{
		return (t7*)NULL;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_1 = m1685(NULL, (t7*) &_stringLiteral534, V_0, (t7*) &_stringLiteral534, &m1685_MI);
		V_0 = L_1;
		bool L_2 = m2922(p0, V_0, &m2922_MI);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_3 = m1715(V_0, &m1715_MI);
		t7* L_4 = m1770(p0, L_3, &m1770_MI);
		return L_4;
	}

IL_0032:
	{
		return (t7*)NULL;
	}
}
 t1469 * m8015 (t29 * __this, t1466 * p0, t42 * p1, t29 ** p2, MethodInfo* method){
	t29 * V_0 = {0};
	t7* V_1 = {0};
	t29 * V_2 = {0};
	t719 * V_3 = {0};
	t7* V_4 = {0};
	t1469 * V_5 = {0};
	t1470 * V_6 = {0};
	t1432 * V_7 = {0};
	t1469 * V_8 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	t316* G_B3_0 = {0};
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7983_MI, p0);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7983_MI, p0);
		t316* L_2 = (t316*)InterfaceFuncInvoker0< t316* >::Invoke(&m10209_MI, L_1);
		G_B3_0 = L_2;
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = ((t316*)(NULL));
	}

IL_0016:
	{
		V_0 = (t29 *)G_B3_0;
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7988_MI, p0);
		t29 * L_4 = m4291(NULL, L_3, V_0, (&V_1), &m4291_MI);
		V_2 = L_4;
		if (V_1)
		{
			goto IL_0030;
		}
	}
	{
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7988_MI, p0);
		V_1 = L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		V_3 = (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0);
		m4000(NULL, V_3, &m4000_MI);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			*((t29 **)(p2)) = (t29 *)NULL;
			t7* L_6 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7988_MI, p0);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t7* L_7 = m8022(NULL, L_6, &m8022_MI);
			V_4 = L_7;
			t29 * L_8 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), V_4);
			V_5 = ((t1469 *)IsInst(L_8, InitializedTypeInfo(&t1469_TI)));
			if (!V_5)
			{
				goto IL_007d;
			}
		}

IL_0063:
		{
			t774 * L_9 = m7975(V_5, &m7975_MI);
			*((t29 **)(p2)) = (t29 *)L_9;
			if (!(*((t29 **)p2)))
			{
				goto IL_0076;
			}
		}

IL_0070:
		{
			V_8 = V_5;
			// IL_0074: leave.s IL_00e9
			leaveInstructions[0] = 0xE9; // 1
			THROW_SENTINEL(IL_00e9);
			// finally target depth: 1
		}

IL_0076:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			m8021(NULL, V_5, &m8021_MI);
		}

IL_007d:
		{
			t1469 * L_10 = (t1469 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1469_TI));
			m7974(L_10, V_1, p0, &m7974_MI);
			V_5 = L_10;
			m7970(V_5, V_2, &m7970_MI);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), V_4, V_5);
			if (!p1)
			{
				goto IL_00da;
			}
		}

IL_009f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1470_TI));
			t1470 * L_11 = (t1470 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1470_TI));
			m7955(L_11, p1, V_5, &m7955_MI);
			V_6 = L_11;
			V_7 = ((t1432 *)IsInst(V_2, InitializedTypeInfo(&t1432_TI)));
			if (!V_7)
			{
				goto IL_00c3;
			}
		}

IL_00b5:
		{
			int32_t L_12 = m7791(V_7, &m7791_MI);
			m7954(V_6, L_12, &m7954_MI);
		}

IL_00c3:
		{
			t29 * L_13 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7953_MI, V_6);
			*((t29 **)(p2)) = (t29 *)L_13;
			m7976(V_5, ((t774 *)Castclass((*((t29 **)p2)), InitializedTypeInfo(&t774_TI))), &m7976_MI);
		}

IL_00da:
		{
			V_8 = V_5;
			// IL_00de: leave.s IL_00e9
			leaveInstructions[0] = 0xE9; // 1
			THROW_SENTINEL(IL_00e9);
			// finally target depth: 1
		}

IL_00e0:
		{
			// IL_00e0: leave.s IL_00e9
			leaveInstructions[0] = 0xE9; // 1
			THROW_SENTINEL(IL_00e9);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_00e2;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_00e2;
	}

IL_00e2:
	{ // begin finally (depth: 1)
		m4001(NULL, V_3, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xE9:
				goto IL_00e9;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00e9:
	{
		return V_8;
	}
}
 t1165 * m8016 (t29 * __this, t42 * p0, t7* p1, int32_t p2, MethodInfo* method){
	t1165 * V_0 = {0};
	{
		if ((((uint32_t)p2) != ((uint32_t)2)))
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1425_TI));
		t1425 * L_0 = m7794(NULL, &m7794_MI);
		t1487 * L_1 = (t1487 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1487_TI));
		m8028(L_1, p1, L_0, p0, &m8028_MI);
		V_0 = L_1;
		goto IL_0020;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1425_TI));
		t1425 * L_2 = m7794(NULL, &m7794_MI);
		t1486 * L_3 = (t1486 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1486_TI));
		m8027(L_3, p1, L_2, p0, &m8027_MI);
		V_0 = L_3;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		m8017(NULL, V_0, &m8017_MI);
		return V_0;
	}
}
 void m8017 (t29 * __this, t1165 * p0, MethodInfo* method){
	t719 * V_0 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		V_0 = (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0);
		m4000(NULL, V_0, &m4000_MI);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t7* L_0 = m7971(p0, &m7971_MI);
			bool L_1 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m4030_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), L_0);
			if (!L_1)
			{
				goto IL_0039;
			}
		}

IL_001e:
		{
			t7* L_2 = m7971(p0, &m7971_MI);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			t7* L_3 = m1685(NULL, (t7*) &_stringLiteral1630, L_2, (t7*) &_stringLiteral52, &m1685_MI);
			t1481 * L_4 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
			m7999(L_4, L_3, &m7999_MI);
			il2cpp_codegen_raise_exception(L_4);
		}

IL_0039:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t7* L_5 = m7971(p0, &m7971_MI);
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), L_5, p0);
			// IL_004a: leave.s IL_0053
			leaveInstructions[0] = 0x53; // 1
			THROW_SENTINEL(IL_0053);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_004c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_004c;
	}

IL_004c:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x53:
				goto IL_0053;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0053:
	{
		return;
	}
}
 t29 * m8018 (t29 * __this, t1466 * p0, t42 * p1, MethodInfo* method){
	t1485 * V_0 = {0};
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7988_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t1468 * L_1 = m8013(NULL, L_0, &m8013_MI);
		V_0 = ((t1485 *)IsInst(L_1, InitializedTypeInfo(&t1485_TI)));
		if (!V_0)
		{
			goto IL_001b;
		}
	}
	{
		t774 * L_2 = m8026(V_0, &m8026_MI);
		return L_2;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_3 = m8019(NULL, p0, p1, &m8019_MI);
		return L_3;
	}
}
 t29 * m8019 (t29 * __this, t1466 * p0, t42 * p1, MethodInfo* method){
	t29 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		m8015(NULL, p0, p1, (&V_0), &m8015_MI);
		return V_0;
	}
}
 void m8020 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1431_TI));
		m7785(NULL, &m7785_MI);
		return;
	}
}
 void m8021 (t29 * __this, t1468 * p0, MethodInfo* method){
	t719 * V_0 = {0};
	t1469 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		V_0 = (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0);
		m4000(NULL, V_0, &m4000_MI);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = m7972(p0, &m7972_MI);
			if (L_0)
			{
				goto IL_004c;
			}
		}

IL_0014:
		{
			V_1 = ((t1469 *)IsInst(p0, InitializedTypeInfo(&t1469_TI)));
			if (!V_1)
			{
				goto IL_0035;
			}
		}

IL_001e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t7* L_1 = m7978(V_1, &m7978_MI);
			t7* L_2 = m8022(NULL, L_1, &m8022_MI);
			VirtActionInvoker1< t29 * >::Invoke(&m4217_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), L_2);
			goto IL_0045;
		}

IL_0035:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
			t7* L_3 = m7971(p0, &m7971_MI);
			VirtActionInvoker1< t29 * >::Invoke(&m4217_MI, (((t1482_SFs*)InitializedTypeInfo(&t1482_TI)->static_fields)->f0), L_3);
		}

IL_0045:
		{
			m7973(p0, 1, &m7973_MI);
		}

IL_004c:
		{
			// IL_004c: leave.s IL_0055
			leaveInstructions[0] = 0x55; // 1
			THROW_SENTINEL(IL_0055);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_004e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_004e;
	}

IL_004e:
	{ // begin finally (depth: 1)
		m4001(NULL, V_0, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x55:
				goto IL_0055;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0055:
	{
		return;
	}
}
 t7* m8022 (t29 * __this, t7* p0, MethodInfo* method){
	{
		bool L_0 = m2922(p0, (t7*) &_stringLiteral534, &m2922_MI);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		t7* L_1 = m1770(p0, 1, &m1770_MI);
		return L_1;
	}

IL_0015:
	{
		return p0;
	}
}
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern Il2CppType t719_0_0_17;
FieldInfo t1482_f0_FieldInfo = 
{
	"uri_hash", &t719_0_0_17, &t1482_TI, offsetof(t1482_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1483_0_0_17;
FieldInfo t1482_f1_FieldInfo = 
{
	"_serializationFormatter", &t1483_0_0_17, &t1482_TI, offsetof(t1482_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1483_0_0_17;
FieldInfo t1482_f2_FieldInfo = 
{
	"_deserializationFormatter", &t1483_0_0_17, &t1482_TI, offsetof(t1482_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_19;
FieldInfo t1482_f3_FieldInfo = 
{
	"app_id", &t7_0_0_19, &t1482_TI, offsetof(t1482_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_17;
FieldInfo t1482_f4_FieldInfo = 
{
	"next_id", &t44_0_0_17, &t1482_TI, offsetof(t1482_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_49;
FieldInfo t1482_f5_FieldInfo = 
{
	"methodBindings", &t630_0_0_49, &t1482_TI, offsetof(t1482_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_49;
FieldInfo t1482_f6_FieldInfo = 
{
	"FieldSetterMethod", &t557_0_0_49, &t1482_TI, offsetof(t1482_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_49;
FieldInfo t1482_f7_FieldInfo = 
{
	"FieldGetterMethod", &t557_0_0_49, &t1482_TI, offsetof(t1482_SFs, f7), &EmptyCustomAttributesCache};
static FieldInfo* t1482_FIs[] =
{
	&t1482_f0_FieldInfo,
	&t1482_f1_FieldInfo,
	&t1482_f2_FieldInfo,
	&t1482_f3_FieldInfo,
	&t1482_f4_FieldInfo,
	&t1482_f5_FieldInfo,
	&t1482_f6_FieldInfo,
	&t1482_f7_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8001_MI = 
{
	".cctor", (methodPointerType)&m8001, &t1482_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3587, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t1482_m8002_ParameterInfos[] = 
{
	{"type", 0, 134222003, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"method", 1, 134222004, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8002_MI = 
{
	"GetVirtualMethod", (methodPointerType)&m8002, &t1482_TI, &t636_0_0_0, RuntimeInvoker_t29_t29_t29, t1482_m8002_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 2, false, false, 3588, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1482_m8003_ParameterInfos[] = 
{
	{"proxy", 0, 134222005, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1482__CustomAttributeCache_m8003;
MethodInfo m8003_MI = 
{
	"IsTransparentProxy", (methodPointerType)&m8003, &t1482_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1482_m8003_ParameterInfos, &t1482__CustomAttributeCache_m8003, 150, 4096, 255, 1, false, false, 3589, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1482_m8004_ParameterInfos[] = 
{
	{"URI", 0, 134222006, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8004_MI = 
{
	"GetServerTypeForUri", (methodPointerType)&m8004, &t1482_TI, &t42_0_0_0, RuntimeInvoker_t29_t29, t1482_m8004_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3590, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1466_0_0_0;
static ParameterInfo t1482_m8005_ParameterInfos[] = 
{
	{"objectRef", 0, 134222007, &EmptyCustomAttributesCache, &t1466_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8005_MI = 
{
	"Unmarshal", (methodPointerType)&m8005, &t1482_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1482_m8005_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3591, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1466_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1482_m8006_ParameterInfos[] = 
{
	{"objectRef", 0, 134222008, &EmptyCustomAttributesCache, &t1466_0_0_0},
	{"fRefine", 1, 134222009, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m8006_MI = 
{
	"Unmarshal", (methodPointerType)&m8006, &t1482_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t297, t1482_m8006_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 3592, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1482_m8007_ParameterInfos[] = 
{
	{"proxy", 0, 134222010, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t1465_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1482__CustomAttributeCache_m8007;
MethodInfo m8007_MI = 
{
	"GetRealProxy", (methodPointerType)&m8007, &t1482_TI, &t1465_0_0_0, RuntimeInvoker_t29_t29, t1482_m8007_ParameterInfos, &t1482__CustomAttributeCache_m8007, 150, 0, 255, 1, false, false, 3593, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1453_0_0_0;
static ParameterInfo t1482_m8008_ParameterInfos[] = 
{
	{"msg", 0, 134222011, &EmptyCustomAttributesCache, &t1453_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8008_MI = 
{
	"GetMethodBaseFromMethodMessage", (methodPointerType)&m8008, &t1482_TI, &t636_0_0_0, RuntimeInvoker_t29_t29, t1482_m8008_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3594, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_0_0_0;
static ParameterInfo t1482_m8009_ParameterInfos[] = 
{
	{"type", 0, 134222012, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"methodName", 1, 134222013, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"signature", 2, 134222014, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8009_MI = 
{
	"GetMethodBaseFromName", (methodPointerType)&m8009, &t1482_TI, &t636_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1482_m8009_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 3, false, false, 3595, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t537_0_0_0;
static ParameterInfo t1482_m8010_ParameterInfos[] = 
{
	{"type", 0, 134222015, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"methodName", 1, 134222016, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"signature", 2, 134222017, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8010_MI = 
{
	"FindInterfaceMethod", (methodPointerType)&m8010, &t1482_TI, &t636_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1482_m8010_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 3, false, false, 3596, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1472_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1482_m8011_ParameterInfos[] = 
{
	{"entry", 0, 134222018, &EmptyCustomAttributesCache, &t1472_0_0_0},
	{"activationAttributes", 1, 134222019, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8011_MI = 
{
	"CreateClientProxy", (methodPointerType)&m8011, &t1482_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1482_m8011_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 3597, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1482_m8012_ParameterInfos[] = 
{
	{"type", 0, 134222020, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"activationAttributes", 1, 134222021, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8012_MI = 
{
	"CreateClientProxyForContextBound", (methodPointerType)&m8012, &t1482_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1482_m8012_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 3598, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1482_m8013_ParameterInfos[] = 
{
	{"uri", 0, 134222022, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t1468_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8013_MI = 
{
	"GetIdentityForUri", (methodPointerType)&m8013, &t1482_TI, &t1468_0_0_0, RuntimeInvoker_t29_t29, t1482_m8013_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3599, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1482_m8014_ParameterInfos[] = 
{
	{"uri", 0, 134222023, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8014_MI = 
{
	"RemoveAppNameFromUri", (methodPointerType)&m8014, &t1482_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1482_m8014_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 3600, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1466_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t29_1_0_0;
static ParameterInfo t1482_m8015_ParameterInfos[] = 
{
	{"objRef", 0, 134222024, &EmptyCustomAttributesCache, &t1466_0_0_0},
	{"proxyType", 1, 134222025, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"clientProxy", 2, 134222026, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t1469_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8015_MI = 
{
	"GetOrCreateClientIdentity", (methodPointerType)&m8015, &t1482_TI, &t1469_0_0_0, RuntimeInvoker_t29_t29_t29_t2022, t1482_m8015_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 3, false, false, 3601, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t1484_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t1482_m8016_ParameterInfos[] = 
{
	{"objectType", 0, 134222027, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"objectUri", 1, 134222028, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"mode", 2, 134222029, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t1165_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m8016_MI = 
{
	"CreateWellKnownServerIdentity", (methodPointerType)&m8016, &t1482_TI, &t1165_0_0_0, RuntimeInvoker_t29_t29_t29_t44, t1482_m8016_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 3, false, false, 3602, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1165_0_0_0;
extern Il2CppType t1165_0_0_0;
static ParameterInfo t1482_m8017_ParameterInfos[] = 
{
	{"identity", 0, 134222030, &EmptyCustomAttributesCache, &t1165_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8017_MI = 
{
	"RegisterServerIdentity", (methodPointerType)&m8017, &t1482_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1482_m8017_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 3603, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1466_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1482_m8018_ParameterInfos[] = 
{
	{"objref", 0, 134222031, &EmptyCustomAttributesCache, &t1466_0_0_0},
	{"classToProxy", 1, 134222032, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8018_MI = 
{
	"GetProxyForRemoteObject", (methodPointerType)&m8018, &t1482_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1482_m8018_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 3604, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1466_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1482_m8019_ParameterInfos[] = 
{
	{"objRef", 0, 134222033, &EmptyCustomAttributesCache, &t1466_0_0_0},
	{"proxyType", 1, 134222034, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8019_MI = 
{
	"GetRemoteObject", (methodPointerType)&m8019, &t1482_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1482_m8019_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 3605, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8020_MI = 
{
	"RegisterInternalChannels", (methodPointerType)&m8020, &t1482_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 145, 0, 255, 0, false, false, 3606, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1468_0_0_0;
static ParameterInfo t1482_m8021_ParameterInfos[] = 
{
	{"ident", 0, 134222035, &EmptyCustomAttributesCache, &t1468_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8021_MI = 
{
	"DisposeIdentity", (methodPointerType)&m8021, &t1482_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1482_m8021_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3607, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1482_m8022_ParameterInfos[] = 
{
	{"uri", 0, 134222036, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8022_MI = 
{
	"GetNormalizedUri", (methodPointerType)&m8022, &t1482_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1482_m8022_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 3608, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1482_MIs[] =
{
	&m8001_MI,
	&m8002_MI,
	&m8003_MI,
	&m8004_MI,
	&m8005_MI,
	&m8006_MI,
	&m8007_MI,
	&m8008_MI,
	&m8009_MI,
	&m8010_MI,
	&m8011_MI,
	&m8012_MI,
	&m8013_MI,
	&m8014_MI,
	&m8015_MI,
	&m8016_MI,
	&m8017_MI,
	&m8018_MI,
	&m8019_MI,
	&m8020_MI,
	&m8021_MI,
	&m8022_MI,
	NULL
};
static MethodInfo* t1482_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1482_CustomAttributesCacheGenerator_m8003(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1482_CustomAttributesCacheGenerator_m8007(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1482__CustomAttributeCache = {
1,
NULL,
&t1482_CustomAttributesCacheGenerator
};
CustomAttributesCache t1482__CustomAttributeCache_m8003 = {
1,
NULL,
&t1482_CustomAttributesCacheGenerator_m8003
};
CustomAttributesCache t1482__CustomAttributeCache_m8007 = {
1,
NULL,
&t1482_CustomAttributesCacheGenerator_m8007
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1482_0_0_0;
extern Il2CppType t1482_1_0_0;
struct t1482;
extern CustomAttributesCache t1482__CustomAttributeCache;
extern CustomAttributesCache t1482__CustomAttributeCache_m8003;
extern CustomAttributesCache t1482__CustomAttributeCache_m8007;
TypeInfo t1482_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RemotingServices", "System.Runtime.Remoting", t1482_MIs, NULL, t1482_FIs, NULL, &t29_TI, NULL, NULL, &t1482_TI, NULL, t1482_VT, &t1482__CustomAttributeCache, &t1482_TI, &t1482_0_0_0, &t1482_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1482), 0, -1, sizeof(t1482_SFs), 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, true, false, false, 22, 0, 8, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1488.h"
extern TypeInfo t1488_TI;
#include "t1488MD.h"
extern MethodInfo m8031_MI;


extern MethodInfo m8023_MI;
 void m8023 (t1165 * __this, t7* p0, t1425 * p1, t42 * p2, MethodInfo* method){
	{
		m7968(__this, p0, &m7968_MI);
		__this->f5 = p2;
		__this->f7 = p1;
		return;
	}
}
 t42 * m8024 (t1165 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m8025_MI;
 t1466 * m8025 (t1165 * __this, t42 * p0, MethodInfo* method){
	{
		t1466 * L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t1466 * L_1 = (__this->f3);
		m7992(L_1, &m7992_MI);
		t1466 * L_2 = (__this->f3);
		return L_2;
	}

IL_001a:
	{
		if (p0)
		{
			goto IL_0027;
		}
	}
	{
		t42 * L_3 = (__this->f5);
		p0 = L_3;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1466_TI));
		t1466 * L_4 = (t1466 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1466_TI));
		m7979(L_4, &m7979_MI);
		__this->f3 = L_4;
		t1466 * L_5 = (__this->f3);
		t1488 * L_6 = (t1488 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1488_TI));
		m8031(L_6, p0, &m8031_MI);
		VirtActionInvoker1< t29 * >::Invoke(&m7987_MI, L_5, L_6);
		t1466 * L_7 = (__this->f3);
		t7* L_8 = (__this->f0);
		VirtActionInvoker1< t7* >::Invoke(&m7989_MI, L_7, L_8);
		t29 * L_9 = (__this->f2);
		if (!L_9)
		{
			goto IL_007f;
		}
	}
	{
		t29 * L_10 = (__this->f2);
		if (((t1447 *)IsInst(L_10, InitializedTypeInfo(&t1447_TI))))
		{
			goto IL_007f;
		}
	}
	{
		t1466 * L_11 = (__this->f3);
		t29 * L_12 = (__this->f2);
		t1475 * L_13 = (t1475 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1475_TI));
		m7966(L_13, L_12, &m7966_MI);
		VirtActionInvoker1< t29 * >::Invoke(&m7985_MI, L_11, L_13);
	}

IL_007f:
	{
		t1466 * L_14 = (__this->f3);
		return L_14;
	}
}
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern Il2CppType t42_0_0_4;
FieldInfo t1165_f5_FieldInfo = 
{
	"_objectType", &t42_0_0_4, &t1165_TI, offsetof(t1165, f5), &EmptyCustomAttributesCache};
extern Il2CppType t774_0_0_4;
FieldInfo t1165_f6_FieldInfo = 
{
	"_serverObject", &t774_0_0_4, &t1165_TI, offsetof(t1165, f6), &EmptyCustomAttributesCache};
extern Il2CppType t1425_0_0_4;
FieldInfo t1165_f7_FieldInfo = 
{
	"_context", &t1425_0_0_4, &t1165_TI, offsetof(t1165, f7), &EmptyCustomAttributesCache};
static FieldInfo* t1165_FIs[] =
{
	&t1165_f5_FieldInfo,
	&t1165_f6_FieldInfo,
	&t1165_f7_FieldInfo,
	NULL
};
static PropertyInfo t1165____ObjectType_PropertyInfo = 
{
	&t1165_TI, "ObjectType", &m8024_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1165_PIs[] =
{
	&t1165____ObjectType_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1425_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1165_m8023_ParameterInfos[] = 
{
	{"objectUri", 0, 134222037, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"context", 1, 134222038, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"objectType", 2, 134222039, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8023_MI = 
{
	".ctor", (methodPointerType)&m8023, &t1165_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1165_m8023_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 3609, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8024_MI = 
{
	"get_ObjectType", (methodPointerType)&m8024, &t1165_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3610, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1165_m8025_ParameterInfos[] = 
{
	{"requestedType", 0, 134222040, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t1466_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8025_MI = 
{
	"CreateObjRef", (methodPointerType)&m8025, &t1165_TI, &t1466_0_0_0, RuntimeInvoker_t29_t29, t1165_m8025_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, false, 3611, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1165_MIs[] =
{
	&m8023_MI,
	&m8024_MI,
	&m8025_MI,
	NULL
};
static MethodInfo* t1165_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8025_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1165_1_0_0;
struct t1165;
TypeInfo t1165_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ServerIdentity", "System.Runtime.Remoting", t1165_MIs, t1165_PIs, t1165_FIs, NULL, &t1468_TI, NULL, NULL, &t1165_TI, NULL, t1165_VT, &EmptyCustomAttributesCache, &t1165_TI, &t1165_0_0_0, &t1165_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1165), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 1, 3, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif



 t774 * m8026 (t1485 * __this, MethodInfo* method){
	{
		t774 * L_0 = (__this->f6);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern Il2CppType t774_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8026_MI = 
{
	"GetServerObject", (methodPointerType)&m8026, &t1485_TI, &t774_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 3612, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1485_MIs[] =
{
	&m8026_MI,
	NULL
};
static MethodInfo* t1485_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8025_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1485_0_0_0;
extern Il2CppType t1485_1_0_0;
struct t1485;
TypeInfo t1485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ClientActivatedIdentity", "System.Runtime.Remoting", t1485_MIs, NULL, NULL, NULL, &t1165_TI, NULL, NULL, &t1485_TI, NULL, t1485_VT, &EmptyCustomAttributesCache, &t1485_TI, &t1485_0_0_0, &t1485_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1485), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m8027 (t1486 * __this, t7* p0, t1425 * p1, t42 * p2, MethodInfo* method){
	{
		m8023(__this, p0, p1, p2, &m8023_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern Il2CppType t7_0_0_0;
extern Il2CppType t1425_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1486_m8027_ParameterInfos[] = 
{
	{"objectUri", 0, 134222041, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"context", 1, 134222042, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"objectType", 2, 134222043, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8027_MI = 
{
	".ctor", (methodPointerType)&m8027, &t1486_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1486_m8027_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 3613, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1486_MIs[] =
{
	&m8027_MI,
	NULL
};
static MethodInfo* t1486_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8025_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1486_0_0_0;
extern Il2CppType t1486_1_0_0;
struct t1486;
TypeInfo t1486_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SingletonIdentity", "System.Runtime.Remoting", t1486_MIs, NULL, NULL, NULL, &t1165_TI, NULL, NULL, &t1486_TI, NULL, t1486_VT, &EmptyCustomAttributesCache, &t1486_TI, &t1486_0_0_0, &t1486_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1486), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m8028 (t1487 * __this, t7* p0, t1425 * p1, t42 * p2, MethodInfo* method){
	{
		m8023(__this, p0, p1, p2, &m8023_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern Il2CppType t7_0_0_0;
extern Il2CppType t1425_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1487_m8028_ParameterInfos[] = 
{
	{"objectUri", 0, 134222044, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"context", 1, 134222045, &EmptyCustomAttributesCache, &t1425_0_0_0},
	{"objectType", 2, 134222046, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8028_MI = 
{
	".ctor", (methodPointerType)&m8028, &t1487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1487_m8028_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 3614, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1487_MIs[] =
{
	&m8028_MI,
	NULL
};
static MethodInfo* t1487_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8025_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1487_0_0_0;
extern Il2CppType t1487_1_0_0;
struct t1487;
TypeInfo t1487_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SingleCallIdentity", "System.Runtime.Remoting", t1487_MIs, NULL, NULL, NULL, &t1165_TI, NULL, NULL, &t1487_TI, NULL, t1487_VT, &EmptyCustomAttributesCache, &t1487_TI, &t1487_0_0_0, &t1487_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1487), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 5, 0, 0};
#include "t1473.h"
#ifndef _MSC_VER
#else
#endif



 t7* m8029 (t1473 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
 t7* m8030 (t1473 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern Il2CppType t7_0_0_1;
FieldInfo t1473_f0_FieldInfo = 
{
	"assembly_name", &t7_0_0_1, &t1473_TI, offsetof(t1473, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1473_f1_FieldInfo = 
{
	"type_name", &t7_0_0_1, &t1473_TI, offsetof(t1473, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1473_FIs[] =
{
	&t1473_f0_FieldInfo,
	&t1473_f1_FieldInfo,
	NULL
};
static PropertyInfo t1473____AssemblyName_PropertyInfo = 
{
	&t1473_TI, "AssemblyName", &m8029_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1473____TypeName_PropertyInfo = 
{
	&t1473_TI, "TypeName", &m8030_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1473_PIs[] =
{
	&t1473____AssemblyName_PropertyInfo,
	&t1473____TypeName_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8029_MI = 
{
	"get_AssemblyName", (methodPointerType)&m8029, &t1473_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3615, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8030_MI = 
{
	"get_TypeName", (methodPointerType)&m8030, &t1473_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3616, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1473_MIs[] =
{
	&m8029_MI,
	&m8030_MI,
	NULL
};
static MethodInfo* t1473_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1473_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1473__CustomAttributeCache = {
1,
NULL,
&t1473_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1473_0_0_0;
extern Il2CppType t1473_1_0_0;
struct t1473;
extern CustomAttributesCache t1473__CustomAttributeCache;
TypeInfo t1473_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeEntry", "System.Runtime.Remoting", t1473_MIs, t1473_PIs, t1473_FIs, NULL, &t29_TI, NULL, NULL, &t1473_TI, NULL, t1473_VT, &t1473__CustomAttributeCache, &t1473_TI, &t1473_0_0_0, &t1473_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1473), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 2, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m8031 (t1488 * __this, t42 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t42 * V_1 = {0};
	int32_t V_2 = 0;
	t537* V_3 = {0};
	int32_t V_4 = 0;
	{
		m1331(__this, &m1331_MI);
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, p0);
		if (!L_0)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t774_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, L_1);
		__this->f0 = L_2;
		__this->f1 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 0));
		t446* L_3 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 1));
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, p0);
		ArrayElementTypeCheck (L_3, L_4);
		*((t7**)(t7**)SZArrayLdElema(L_3, 0)) = (t7*)L_4;
		__this->f2 = L_3;
		goto IL_00f2;
	}

IL_0049:
	{
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, p0);
		__this->f0 = L_5;
		V_0 = 0;
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		V_1 = L_6;
		goto IL_006b;
	}

IL_0060:
	{
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_1);
		V_1 = L_7;
		V_0 = ((int32_t)(V_0+1));
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t774_0_0_0), &m1554_MI);
		if ((((t42 *)V_1) == ((t42 *)L_8)))
		{
			goto IL_0085;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)V_1) != ((t42 *)L_9)))
		{
			goto IL_0060;
		}
	}

IL_0085:
	{
		__this->f1 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), V_0));
		t42 * L_10 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, p0);
		V_1 = L_10;
		V_2 = 0;
		goto IL_00b5;
	}

IL_009c:
	{
		t446* L_11 = (__this->f1);
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, V_1);
		ArrayElementTypeCheck (L_11, L_12);
		*((t7**)(t7**)SZArrayLdElema(L_11, V_2)) = (t7*)L_12;
		t42 * L_13 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_1);
		V_1 = L_13;
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b5:
	{
		if ((((int32_t)V_2) < ((int32_t)V_0)))
		{
			goto IL_009c;
		}
	}
	{
		t537* L_14 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m9850_MI, p0);
		V_3 = L_14;
		__this->f2 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), (((int32_t)(((t20 *)V_3)->max_length)))));
		V_4 = 0;
		goto IL_00eb;
	}

IL_00d3:
	{
		t446* L_15 = (__this->f2);
		int32_t L_16 = V_4;
		t7* L_17 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2996_MI, (*(t42 **)(t42 **)SZArrayLdElema(V_3, L_16)));
		ArrayElementTypeCheck (L_15, L_17);
		*((t7**)(t7**)SZArrayLdElema(L_15, V_4)) = (t7*)L_17;
		V_4 = ((int32_t)(V_4+1));
	}

IL_00eb:
	{
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((t20 *)V_3)->max_length))))))
		{
			goto IL_00d3;
		}
	}

IL_00f2:
	{
		return;
	}
}
extern MethodInfo m8032_MI;
 t7* m8032 (t1488 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern Il2CppType t7_0_0_1;
FieldInfo t1488_f0_FieldInfo = 
{
	"serverType", &t7_0_0_1, &t1488_TI, offsetof(t1488, f0), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t1488_f1_FieldInfo = 
{
	"serverHierarchy", &t446_0_0_1, &t1488_TI, offsetof(t1488, f1), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t1488_f2_FieldInfo = 
{
	"interfacesImplemented", &t446_0_0_1, &t1488_TI, offsetof(t1488, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1488_FIs[] =
{
	&t1488_f0_FieldInfo,
	&t1488_f1_FieldInfo,
	&t1488_f2_FieldInfo,
	NULL
};
static PropertyInfo t1488____TypeName_PropertyInfo = 
{
	&t1488_TI, "TypeName", &m8032_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1488_PIs[] =
{
	&t1488____TypeName_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1488_m8031_ParameterInfos[] = 
{
	{"type", 0, 134222047, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8031_MI = 
{
	".ctor", (methodPointerType)&m8031, &t1488_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1488_m8031_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3617, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8032_MI = 
{
	"get_TypeName", (methodPointerType)&m8032, &t1488_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 3618, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1488_MIs[] =
{
	&m8031_MI,
	&m8032_MI,
	NULL
};
static MethodInfo* t1488_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8032_MI,
};
static TypeInfo* t1488_ITIs[] = 
{
	&t1478_TI,
};
static Il2CppInterfaceOffsetPair t1488_IOs[] = 
{
	{ &t1478_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1488_0_0_0;
extern Il2CppType t1488_1_0_0;
struct t1488;
TypeInfo t1488_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeInfo", "System.Runtime.Remoting", t1488_MIs, t1488_PIs, t1488_FIs, NULL, &t29_TI, NULL, NULL, &t1488_TI, t1488_ITIs, t1488_VT, &EmptyCustomAttributesCache, &t1488_TI, &t1488_0_0_0, &t1488_1_0_0, t1488_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1488), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 1, 3, 0, 0, 5, 1, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1484_TI;
#include "t1484MD.h"



// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern Il2CppType t44_0_0_1542;
FieldInfo t1484_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1484_TI, offsetof(t1484, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1484_0_0_32854;
FieldInfo t1484_f2_FieldInfo = 
{
	"Singleton", &t1484_0_0_32854, &t1484_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1484_0_0_32854;
FieldInfo t1484_f3_FieldInfo = 
{
	"SingleCall", &t1484_0_0_32854, &t1484_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1484_FIs[] =
{
	&t1484_f1_FieldInfo,
	&t1484_f2_FieldInfo,
	&t1484_f3_FieldInfo,
	NULL
};
static const int32_t t1484_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1484_f2_DefaultValue = 
{
	&t1484_f2_FieldInfo, { (char*)&t1484_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1484_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1484_f3_DefaultValue = 
{
	&t1484_f3_FieldInfo, { (char*)&t1484_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1484_FDVs[] = 
{
	&t1484_f2_DefaultValue,
	&t1484_f3_DefaultValue,
	NULL
};
static MethodInfo* t1484_MIs[] =
{
	NULL
};
static MethodInfo* t1484_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1484_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1484__CustomAttributeCache = {
1,
NULL,
&t1484_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1484_1_0_0;
extern CustomAttributesCache t1484__CustomAttributeCache;
TypeInfo t1484_TI = 
{
	&g_mscorlib_dll_Image, NULL, "WellKnownObjectMode", "System.Runtime.Remoting", t1484_MIs, NULL, t1484_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1484_VT, &t1484__CustomAttributeCache, &t44_TI, &t1484_0_0_0, &t1484_1_0_0, t1484_IOs, NULL, NULL, t1484_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1484)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t1489.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1489_TI;
#include "t1489MD.h"

#include "t1703.h"
#include "t1688.h"
#include "t927.h"
#include "t816.h"
#include "t465.h"
#include "t1126.h"
#include "t601.h"
#include "t372.h"
#include "t919.h"
#include "t297.h"
#include "t22.h"
#include "t626.h"
#include "t344.h"
#include "t1089.h"
extern TypeInfo t781_TI;
extern TypeInfo t1703_TI;
extern TypeInfo t194_TI;
extern TypeInfo t816_TI;
extern TypeInfo t465_TI;
extern TypeInfo t1126_TI;
extern TypeInfo t601_TI;
extern TypeInfo t372_TI;
extern TypeInfo t919_TI;
extern TypeInfo t297_TI;
extern TypeInfo t22_TI;
extern TypeInfo t626_TI;
extern TypeInfo t344_TI;
extern TypeInfo t1089_TI;
#include "t1703MD.h"
#include "t928MD.h"
#include "t941MD.h"
extern Il2CppType t194_0_0_0;
extern Il2CppType t816_0_0_0;
extern Il2CppType t465_0_0_0;
extern Il2CppType t1126_0_0_0;
extern Il2CppType t601_0_0_0;
extern Il2CppType t372_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t297_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t626_0_0_0;
extern Il2CppType t344_0_0_0;
extern Il2CppType t1089_0_0_0;
extern MethodInfo m4025_MI;
extern MethodInfo m5218_MI;
extern MethodInfo m3000_MI;
extern FieldInfo t1703_f21_FieldInfo;


extern MethodInfo m8033_MI;
 void m8033 (t29 * __this, MethodInfo* method){
	t7* V_0 = {0};
	{
		t781* L_0 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), ((int32_t)17)));
		m4025(NULL, (t20 *)(t20 *)L_0, LoadFieldToken(&t1703_f21_FieldInfo), &m4025_MI);
		((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f0 = L_0;
		((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f3 = 0;
		((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), ((int32_t)19)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t40_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_1);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 1)) = (t42 *)L_1;
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t348_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_2);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 2)) = (t42 *)L_2;
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t194_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_3);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 3)) = (t42 *)L_3;
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t816_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_4);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)12))) = (t42 *)L_4;
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t465_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_5);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)13))) = (t42 *)L_5;
		t42 * L_6 = m1554(NULL, LoadTypeToken(&t1126_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_6);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 5)) = (t42 *)L_6;
		t42 * L_7 = m1554(NULL, LoadTypeToken(&t601_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_7);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 6)) = (t42 *)L_7;
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t372_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_8);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 7)) = (t42 *)L_8;
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_9);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), 8)) = (t42 *)L_9;
		t42 * L_10 = m1554(NULL, LoadTypeToken(&t919_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_10);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)9))) = (t42 *)L_10;
		t42 * L_11 = m1554(NULL, LoadTypeToken(&t297_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_11);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)10))) = (t42 *)L_11;
		t42 * L_12 = m1554(NULL, LoadTypeToken(&t22_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_12);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)11))) = (t42 *)L_12;
		t42 * L_13 = m1554(NULL, LoadTypeToken(&t626_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_13);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)14))) = (t42 *)L_13;
		t42 * L_14 = m1554(NULL, LoadTypeToken(&t344_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_14);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)15))) = (t42 *)L_14;
		t42 * L_15 = m1554(NULL, LoadTypeToken(&t1089_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_15);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)16))) = (t42 *)L_15;
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), NULL);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)17))) = (t42 *)NULL;
		t42 * L_16 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		ArrayElementTypeCheck ((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_16);
		*((t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), ((int32_t)18))) = (t42 *)L_16;
		((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), ((int32_t)30)));
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), 3)) = (uint8_t)1;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), 6)) = (uint8_t)2;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), 4)) = (uint8_t)3;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)16))) = (uint8_t)((int32_t)13);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)15))) = (uint8_t)5;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)14))) = (uint8_t)6;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), 7)) = (uint8_t)7;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)9))) = (uint8_t)8;
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)11))) = (uint8_t)((int32_t)9);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), 5)) = (uint8_t)((int32_t)10);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)13))) = (uint8_t)((int32_t)11);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), 8)) = (uint8_t)((int32_t)14);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)10))) = (uint8_t)((int32_t)15);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)12))) = (uint8_t)((int32_t)16);
		*((uint8_t*)(uint8_t*)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f2), ((int32_t)18))) = (uint8_t)((int32_t)18);
		t7* L_17 = m5218(NULL, (t7*) &_stringLiteral1631, &m5218_MI);
		V_0 = L_17;
		if (V_0)
		{
			goto IL_01f4;
		}
	}
	{
		V_0 = (t7*) &_stringLiteral1632;
	}

IL_01f4:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_18 = m1740(NULL, V_0, (t7*) &_stringLiteral1632, &m1740_MI);
		((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f3 = L_18;
		return;
	}
}
extern MethodInfo m8034_MI;
 bool m8034 (t29 * __this, t42 * p0, MethodInfo* method){
	int32_t G_B6_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m3000_MI, p0);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t35_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_1)))
		{
			goto IL_003e;
		}
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t465_0_0_0), &m1554_MI);
		if ((((t42 *)p0) == ((t42 *)L_2)))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t816_0_0_0), &m1554_MI);
		if ((((t42 *)p0) == ((t42 *)L_3)))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t1126_0_0_0), &m1554_MI);
		G_B6_0 = ((((t42 *)p0) == ((t42 *)L_4))? 1 : 0);
		goto IL_003f;
	}

IL_003e:
	{
		G_B6_0 = 1;
	}

IL_003f:
	{
		return G_B6_0;
	}
}
extern MethodInfo m8035_MI;
 t42 * m8035 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		int32_t L_0 = p0;
		return (*(t42 **)(t42 **)SZArrayLdElema((((t1489_SFs*)InitializedTypeInfo(&t1489_TI)->static_fields)->f1), L_0));
	}
}
extern MethodInfo m8036_MI;
 void m8036 (t29 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method){
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		if ((((uint32_t)p2) != ((uint32_t)8)))
		{
			goto IL_0066;
		}
	}
	{
		V_1 = 0;
		goto IL_0060;
	}

IL_0008:
	{
		int32_t L_0 = V_1;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_0));
		int32_t L_1 = ((int32_t)(V_1+7));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, V_1)) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_1));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+7)))) = (uint8_t)V_0;
		int32_t L_2 = ((int32_t)(V_1+1));
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_2));
		int32_t L_3 = ((int32_t)(V_1+6));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+1)))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_3));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+6)))) = (uint8_t)V_0;
		int32_t L_4 = ((int32_t)(V_1+2));
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_4));
		int32_t L_5 = ((int32_t)(V_1+5));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+2)))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_5));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+5)))) = (uint8_t)V_0;
		int32_t L_6 = ((int32_t)(V_1+3));
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_6));
		int32_t L_7 = ((int32_t)(V_1+4));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+3)))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_7));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_1+4)))) = (uint8_t)V_0;
		V_1 = ((int32_t)(V_1+8));
	}

IL_0060:
	{
		if ((((int32_t)V_1) < ((int32_t)p1)))
		{
			goto IL_0008;
		}
	}
	{
		goto IL_00c2;
	}

IL_0066:
	{
		if ((((uint32_t)p2) != ((uint32_t)4)))
		{
			goto IL_00a0;
		}
	}
	{
		V_2 = 0;
		goto IL_009a;
	}

IL_006e:
	{
		int32_t L_8 = V_2;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_8));
		int32_t L_9 = ((int32_t)(V_2+3));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, V_2)) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_9));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_2+3)))) = (uint8_t)V_0;
		int32_t L_10 = ((int32_t)(V_2+1));
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_10));
		int32_t L_11 = ((int32_t)(V_2+2));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_2+1)))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_11));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_2+2)))) = (uint8_t)V_0;
		V_2 = ((int32_t)(V_2+4));
	}

IL_009a:
	{
		if ((((int32_t)V_2) < ((int32_t)p1)))
		{
			goto IL_006e;
		}
	}
	{
		goto IL_00c2;
	}

IL_00a0:
	{
		if ((((uint32_t)p2) != ((uint32_t)2)))
		{
			goto IL_00c2;
		}
	}
	{
		V_3 = 0;
		goto IL_00be;
	}

IL_00a8:
	{
		int32_t L_12 = V_3;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_12));
		int32_t L_13 = ((int32_t)(V_3+1));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, V_3)) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(p0, L_13));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(p0, ((int32_t)(V_3+1)))) = (uint8_t)V_0;
		V_3 = ((int32_t)(V_3+2));
	}

IL_00be:
	{
		if ((((int32_t)V_3) < ((int32_t)p1)))
		{
			goto IL_00a8;
		}
	}

IL_00c2:
	{
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern Il2CppType t781_0_0_22;
FieldInfo t1489_f0_FieldInfo = 
{
	"BinaryHeader", &t781_0_0_22, &t1489_TI, offsetof(t1489_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_17;
FieldInfo t1489_f1_FieldInfo = 
{
	"_typeCodesToType", &t537_0_0_17, &t1489_TI, offsetof(t1489_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_17;
FieldInfo t1489_f2_FieldInfo = 
{
	"_typeCodeMap", &t781_0_0_17, &t1489_TI, offsetof(t1489_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_22;
FieldInfo t1489_f3_FieldInfo = 
{
	"UseReflectionSerialization", &t40_0_0_22, &t1489_TI, offsetof(t1489_SFs, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1489_FIs[] =
{
	&t1489_f0_FieldInfo,
	&t1489_f1_FieldInfo,
	&t1489_f2_FieldInfo,
	&t1489_f3_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8033_MI = 
{
	".cctor", (methodPointerType)&m8033, &t1489_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3619, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1489_m8034_ParameterInfos[] = 
{
	{"type", 0, 134222048, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8034_MI = 
{
	"IsPrimitive", (methodPointerType)&m8034, &t1489_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1489_m8034_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3620, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1489_m8035_ParameterInfos[] = 
{
	{"code", 0, 134222049, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m8035_MI = 
{
	"GetTypeFromCode", (methodPointerType)&m8035, &t1489_TI, &t42_0_0_0, RuntimeInvoker_t29_t44, t1489_m8035_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3621, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t781_0_0_0;
extern Il2CppType t781_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1489_m8036_ParameterInfos[] = 
{
	{"byteArray", 0, 134222050, &EmptyCustomAttributesCache, &t781_0_0_0},
	{"size", 1, 134222051, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"dataSize", 2, 134222052, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m8036_MI = 
{
	"SwapBytes", (methodPointerType)&m8036, &t1489_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44, t1489_m8036_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 3622, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1489_MIs[] =
{
	&m8033_MI,
	&m8034_MI,
	&m8035_MI,
	&m8036_MI,
	NULL
};
static MethodInfo* t1489_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1489_0_0_0;
extern Il2CppType t1489_1_0_0;
struct t1489;
TypeInfo t1489_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BinaryCommon", "System.Runtime.Serialization.Formatters.Binary", t1489_MIs, NULL, t1489_FIs, NULL, &t29_TI, NULL, NULL, &t1489_TI, NULL, t1489_VT, &EmptyCustomAttributesCache, &t1489_TI, &t1489_0_0_0, &t1489_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1489), 0, -1, sizeof(t1489_SFs), 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 0, 4, 0, 0, 4, 0, 0};
#include "t1490.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1490_TI;
#include "t1490MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern Il2CppType t348_0_0_1542;
FieldInfo t1490_f1_FieldInfo = 
{
	"value__", &t348_0_0_1542, &t1490_TI, offsetof(t1490, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f2_FieldInfo = 
{
	"Header", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f3_FieldInfo = 
{
	"RefTypeObject", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f4_FieldInfo = 
{
	"UntypedRuntimeObject", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f5_FieldInfo = 
{
	"UntypedExternalObject", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f6_FieldInfo = 
{
	"RuntimeObject", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f7_FieldInfo = 
{
	"ExternalObject", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f8_FieldInfo = 
{
	"String", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f9_FieldInfo = 
{
	"GenericArray", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f10_FieldInfo = 
{
	"BoxedPrimitiveTypeValue", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f11_FieldInfo = 
{
	"ObjectReference", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f12_FieldInfo = 
{
	"NullValue", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f13_FieldInfo = 
{
	"End", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f14_FieldInfo = 
{
	"Assembly", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f15_FieldInfo = 
{
	"ArrayFiller8b", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f16_FieldInfo = 
{
	"ArrayFiller32b", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f17_FieldInfo = 
{
	"ArrayOfPrimitiveType", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f18_FieldInfo = 
{
	"ArrayOfObject", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f19_FieldInfo = 
{
	"ArrayOfString", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f20_FieldInfo = 
{
	"Method", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f21_FieldInfo = 
{
	"_Unknown4", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f22_FieldInfo = 
{
	"_Unknown5", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f23_FieldInfo = 
{
	"MethodCall", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1490_0_0_32854;
FieldInfo t1490_f24_FieldInfo = 
{
	"MethodResponse", &t1490_0_0_32854, &t1490_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1490_FIs[] =
{
	&t1490_f1_FieldInfo,
	&t1490_f2_FieldInfo,
	&t1490_f3_FieldInfo,
	&t1490_f4_FieldInfo,
	&t1490_f5_FieldInfo,
	&t1490_f6_FieldInfo,
	&t1490_f7_FieldInfo,
	&t1490_f8_FieldInfo,
	&t1490_f9_FieldInfo,
	&t1490_f10_FieldInfo,
	&t1490_f11_FieldInfo,
	&t1490_f12_FieldInfo,
	&t1490_f13_FieldInfo,
	&t1490_f14_FieldInfo,
	&t1490_f15_FieldInfo,
	&t1490_f16_FieldInfo,
	&t1490_f17_FieldInfo,
	&t1490_f18_FieldInfo,
	&t1490_f19_FieldInfo,
	&t1490_f20_FieldInfo,
	&t1490_f21_FieldInfo,
	&t1490_f22_FieldInfo,
	&t1490_f23_FieldInfo,
	&t1490_f24_FieldInfo,
	NULL
};
static const uint8_t t1490_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1490_f2_DefaultValue = 
{
	&t1490_f2_FieldInfo, { (char*)&t1490_f2_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1490_f3_DefaultValue = 
{
	&t1490_f3_FieldInfo, { (char*)&t1490_f3_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1490_f4_DefaultValue = 
{
	&t1490_f4_FieldInfo, { (char*)&t1490_f4_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1490_f5_DefaultValue = 
{
	&t1490_f5_FieldInfo, { (char*)&t1490_f5_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1490_f6_DefaultValue = 
{
	&t1490_f6_FieldInfo, { (char*)&t1490_f6_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1490_f7_DefaultValue = 
{
	&t1490_f7_FieldInfo, { (char*)&t1490_f7_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1490_f8_DefaultValue = 
{
	&t1490_f8_FieldInfo, { (char*)&t1490_f8_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1490_f9_DefaultValue = 
{
	&t1490_f9_FieldInfo, { (char*)&t1490_f9_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1490_f10_DefaultValue = 
{
	&t1490_f10_FieldInfo, { (char*)&t1490_f10_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry t1490_f11_DefaultValue = 
{
	&t1490_f11_FieldInfo, { (char*)&t1490_f11_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t1490_f12_DefaultValue = 
{
	&t1490_f12_FieldInfo, { (char*)&t1490_f12_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry t1490_f13_DefaultValue = 
{
	&t1490_f13_FieldInfo, { (char*)&t1490_f13_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry t1490_f14_DefaultValue = 
{
	&t1490_f14_FieldInfo, { (char*)&t1490_f14_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry t1490_f15_DefaultValue = 
{
	&t1490_f15_FieldInfo, { (char*)&t1490_f15_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry t1490_f16_DefaultValue = 
{
	&t1490_f16_FieldInfo, { (char*)&t1490_f16_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry t1490_f17_DefaultValue = 
{
	&t1490_f17_FieldInfo, { (char*)&t1490_f17_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1490_f18_DefaultValue = 
{
	&t1490_f18_FieldInfo, { (char*)&t1490_f18_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry t1490_f19_DefaultValue = 
{
	&t1490_f19_FieldInfo, { (char*)&t1490_f19_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry t1490_f20_DefaultValue = 
{
	&t1490_f20_FieldInfo, { (char*)&t1490_f20_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry t1490_f21_DefaultValue = 
{
	&t1490_f21_FieldInfo, { (char*)&t1490_f21_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry t1490_f22_DefaultValue = 
{
	&t1490_f22_FieldInfo, { (char*)&t1490_f22_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry t1490_f23_DefaultValue = 
{
	&t1490_f23_FieldInfo, { (char*)&t1490_f23_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1490_f24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry t1490_f24_DefaultValue = 
{
	&t1490_f24_FieldInfo, { (char*)&t1490_f24_DefaultValueData, &t348_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1490_FDVs[] = 
{
	&t1490_f2_DefaultValue,
	&t1490_f3_DefaultValue,
	&t1490_f4_DefaultValue,
	&t1490_f5_DefaultValue,
	&t1490_f6_DefaultValue,
	&t1490_f7_DefaultValue,
	&t1490_f8_DefaultValue,
	&t1490_f9_DefaultValue,
	&t1490_f10_DefaultValue,
	&t1490_f11_DefaultValue,
	&t1490_f12_DefaultValue,
	&t1490_f13_DefaultValue,
	&t1490_f14_DefaultValue,
	&t1490_f15_DefaultValue,
	&t1490_f16_DefaultValue,
	&t1490_f17_DefaultValue,
	&t1490_f18_DefaultValue,
	&t1490_f19_DefaultValue,
	&t1490_f20_DefaultValue,
	&t1490_f21_DefaultValue,
	&t1490_f22_DefaultValue,
	&t1490_f23_DefaultValue,
	&t1490_f24_DefaultValue,
	NULL
};
static MethodInfo* t1490_MIs[] =
{
	NULL
};
static MethodInfo* t1490_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1490_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1490_1_0_0;
TypeInfo t1490_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BinaryElement", "System.Runtime.Serialization.Formatters.Binary", t1490_MIs, NULL, t1490_FIs, NULL, &t49_TI, NULL, NULL, &t348_TI, NULL, t1490_VT, &EmptyCustomAttributesCache, &t348_TI, &t1490_0_0_0, &t1490_1_0_0, t1490_IOs, NULL, NULL, t1490_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1490)+ sizeof (Il2CppObject), sizeof (uint8_t), sizeof(uint8_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 24, 0, 0, 23, 0, 3};
#include "t1491.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1491_TI;
#include "t1491MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern Il2CppType t348_0_0_1542;
FieldInfo t1491_f1_FieldInfo = 
{
	"value__", &t348_0_0_1542, &t1491_TI, offsetof(t1491, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f2_FieldInfo = 
{
	"PrimitiveType", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f3_FieldInfo = 
{
	"String", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f4_FieldInfo = 
{
	"ObjectType", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f5_FieldInfo = 
{
	"RuntimeType", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f6_FieldInfo = 
{
	"GenericType", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f7_FieldInfo = 
{
	"ArrayOfObject", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f8_FieldInfo = 
{
	"ArrayOfString", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1491_0_0_32854;
FieldInfo t1491_f9_FieldInfo = 
{
	"ArrayOfPrimitiveType", &t1491_0_0_32854, &t1491_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1491_FIs[] =
{
	&t1491_f1_FieldInfo,
	&t1491_f2_FieldInfo,
	&t1491_f3_FieldInfo,
	&t1491_f4_FieldInfo,
	&t1491_f5_FieldInfo,
	&t1491_f6_FieldInfo,
	&t1491_f7_FieldInfo,
	&t1491_f8_FieldInfo,
	&t1491_f9_FieldInfo,
	NULL
};
static const uint8_t t1491_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1491_f2_DefaultValue = 
{
	&t1491_f2_FieldInfo, { (char*)&t1491_f2_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1491_f3_DefaultValue = 
{
	&t1491_f3_FieldInfo, { (char*)&t1491_f3_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1491_f4_DefaultValue = 
{
	&t1491_f4_FieldInfo, { (char*)&t1491_f4_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1491_f5_DefaultValue = 
{
	&t1491_f5_FieldInfo, { (char*)&t1491_f5_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1491_f6_DefaultValue = 
{
	&t1491_f6_FieldInfo, { (char*)&t1491_f6_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1491_f7_DefaultValue = 
{
	&t1491_f7_FieldInfo, { (char*)&t1491_f7_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1491_f8_DefaultValue = 
{
	&t1491_f8_FieldInfo, { (char*)&t1491_f8_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1491_f9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1491_f9_DefaultValue = 
{
	&t1491_f9_FieldInfo, { (char*)&t1491_f9_DefaultValueData, &t348_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1491_FDVs[] = 
{
	&t1491_f2_DefaultValue,
	&t1491_f3_DefaultValue,
	&t1491_f4_DefaultValue,
	&t1491_f5_DefaultValue,
	&t1491_f6_DefaultValue,
	&t1491_f7_DefaultValue,
	&t1491_f8_DefaultValue,
	&t1491_f9_DefaultValue,
	NULL
};
static MethodInfo* t1491_MIs[] =
{
	NULL
};
static MethodInfo* t1491_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1491_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1491_0_0_0;
extern Il2CppType t1491_1_0_0;
TypeInfo t1491_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeTag", "System.Runtime.Serialization.Formatters.Binary", t1491_MIs, NULL, t1491_FIs, NULL, &t49_TI, NULL, NULL, &t348_TI, NULL, t1491_VT, &EmptyCustomAttributesCache, &t348_TI, &t1491_0_0_0, &t1491_1_0_0, t1491_IOs, NULL, NULL, t1491_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1491)+ sizeof (Il2CppObject), sizeof (uint8_t), sizeof(uint8_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 9, 0, 0, 23, 0, 3};
#include "t1492.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1492_TI;
#include "t1492MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern Il2CppType t44_0_0_1542;
FieldInfo t1492_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1492_TI, offsetof(t1492, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f2_FieldInfo = 
{
	"NoArguments", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f3_FieldInfo = 
{
	"PrimitiveArguments", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f4_FieldInfo = 
{
	"ArgumentsInSimpleArray", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f5_FieldInfo = 
{
	"ArgumentsInMultiArray", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f6_FieldInfo = 
{
	"ExcludeLogicalCallContext", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f7_FieldInfo = 
{
	"IncludesLogicalCallContext", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f8_FieldInfo = 
{
	"IncludesSignature", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f9_FieldInfo = 
{
	"FormatMask", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f10_FieldInfo = 
{
	"GenericArguments", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1492_0_0_32854;
FieldInfo t1492_f11_FieldInfo = 
{
	"NeedsInfoArrayMask", &t1492_0_0_32854, &t1492_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1492_FIs[] =
{
	&t1492_f1_FieldInfo,
	&t1492_f2_FieldInfo,
	&t1492_f3_FieldInfo,
	&t1492_f4_FieldInfo,
	&t1492_f5_FieldInfo,
	&t1492_f6_FieldInfo,
	&t1492_f7_FieldInfo,
	&t1492_f8_FieldInfo,
	&t1492_f9_FieldInfo,
	&t1492_f10_FieldInfo,
	&t1492_f11_FieldInfo,
	NULL
};
static const int32_t t1492_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1492_f2_DefaultValue = 
{
	&t1492_f2_FieldInfo, { (char*)&t1492_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1492_f3_DefaultValue = 
{
	&t1492_f3_FieldInfo, { (char*)&t1492_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f4_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1492_f4_DefaultValue = 
{
	&t1492_f4_FieldInfo, { (char*)&t1492_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f5_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1492_f5_DefaultValue = 
{
	&t1492_f5_FieldInfo, { (char*)&t1492_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f6_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1492_f6_DefaultValue = 
{
	&t1492_f6_FieldInfo, { (char*)&t1492_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f7_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t1492_f7_DefaultValue = 
{
	&t1492_f7_FieldInfo, { (char*)&t1492_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f8_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t1492_f8_DefaultValue = 
{
	&t1492_f8_FieldInfo, { (char*)&t1492_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f9_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry t1492_f9_DefaultValue = 
{
	&t1492_f9_FieldInfo, { (char*)&t1492_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f10_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t1492_f10_DefaultValue = 
{
	&t1492_f10_FieldInfo, { (char*)&t1492_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1492_f11_DefaultValueData = 32972;
static Il2CppFieldDefaultValueEntry t1492_f11_DefaultValue = 
{
	&t1492_f11_FieldInfo, { (char*)&t1492_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1492_FDVs[] = 
{
	&t1492_f2_DefaultValue,
	&t1492_f3_DefaultValue,
	&t1492_f4_DefaultValue,
	&t1492_f5_DefaultValue,
	&t1492_f6_DefaultValue,
	&t1492_f7_DefaultValue,
	&t1492_f8_DefaultValue,
	&t1492_f9_DefaultValue,
	&t1492_f10_DefaultValue,
	&t1492_f11_DefaultValue,
	NULL
};
static MethodInfo* t1492_MIs[] =
{
	NULL
};
static MethodInfo* t1492_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1492_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1492_0_0_0;
extern Il2CppType t1492_1_0_0;
TypeInfo t1492_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodFlags", "System.Runtime.Serialization.Formatters.Binary", t1492_MIs, NULL, t1492_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1492_VT, &EmptyCustomAttributesCache, &t44_TI, &t1492_0_0_0, &t1492_1_0_0, t1492_IOs, NULL, NULL, t1492_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1492)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 11, 0, 0, 23, 0, 3};
#include "t1493.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1493_TI;
#include "t1493MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern Il2CppType t348_0_0_1542;
FieldInfo t1493_f1_FieldInfo = 
{
	"value__", &t348_0_0_1542, &t1493_TI, offsetof(t1493, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1493_0_0_32854;
FieldInfo t1493_f2_FieldInfo = 
{
	"Null", &t1493_0_0_32854, &t1493_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1493_0_0_32854;
FieldInfo t1493_f3_FieldInfo = 
{
	"PrimitiveType", &t1493_0_0_32854, &t1493_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1493_0_0_32854;
FieldInfo t1493_f4_FieldInfo = 
{
	"ObjectType", &t1493_0_0_32854, &t1493_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1493_0_0_32854;
FieldInfo t1493_f5_FieldInfo = 
{
	"Exception", &t1493_0_0_32854, &t1493_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1493_FIs[] =
{
	&t1493_f1_FieldInfo,
	&t1493_f2_FieldInfo,
	&t1493_f3_FieldInfo,
	&t1493_f4_FieldInfo,
	&t1493_f5_FieldInfo,
	NULL
};
static const uint8_t t1493_f2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1493_f2_DefaultValue = 
{
	&t1493_f2_FieldInfo, { (char*)&t1493_f2_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1493_f3_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1493_f3_DefaultValue = 
{
	&t1493_f3_FieldInfo, { (char*)&t1493_f3_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1493_f4_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1493_f4_DefaultValue = 
{
	&t1493_f4_FieldInfo, { (char*)&t1493_f4_DefaultValueData, &t348_0_0_0 }};
static const uint8_t t1493_f5_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1493_f5_DefaultValue = 
{
	&t1493_f5_FieldInfo, { (char*)&t1493_f5_DefaultValueData, &t348_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1493_FDVs[] = 
{
	&t1493_f2_DefaultValue,
	&t1493_f3_DefaultValue,
	&t1493_f4_DefaultValue,
	&t1493_f5_DefaultValue,
	NULL
};
static MethodInfo* t1493_MIs[] =
{
	NULL
};
static MethodInfo* t1493_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1493_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1493_0_0_0;
extern Il2CppType t1493_1_0_0;
TypeInfo t1493_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReturnTypeTag", "System.Runtime.Serialization.Formatters.Binary", t1493_MIs, NULL, t1493_FIs, NULL, &t49_TI, NULL, NULL, &t348_TI, NULL, t1493_VT, &EmptyCustomAttributesCache, &t348_TI, &t1493_0_0_0, &t1493_1_0_0, t1493_IOs, NULL, NULL, t1493_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1493)+ sizeof (Il2CppObject), sizeof (uint8_t), sizeof(uint8_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1494.h"
#include "t1497.h"
#include "t1496.h"
#include "t1034.h"
#include "t1498.h"
#include "t1304.h"
#include "t1503.h"
#include "t917.h"
extern TypeInfo t1034_TI;
extern TypeInfo t917_TI;
extern TypeInfo t1304_TI;
extern TypeInfo t1503_TI;
extern TypeInfo t1498_TI;
extern TypeInfo t1451_TI;
#include "t1034MD.h"
#include "t917MD.h"
#include "t1304MD.h"
#include "t1499MD.h"
#include "t1503MD.h"
#include "t1498MD.h"
extern MethodInfo m8039_MI;
extern MethodInfo m8157_MI;
extern MethodInfo m8046_MI;
extern MethodInfo m5235_MI;
extern MethodInfo m5240_MI;
extern MethodInfo m3986_MI;
extern MethodInfo m6971_MI;
extern MethodInfo m8047_MI;
extern MethodInfo m6976_MI;
extern MethodInfo m8048_MI;
extern MethodInfo m8049_MI;
extern MethodInfo m8052_MI;
extern MethodInfo m8054_MI;
extern MethodInfo m9629_MI;
extern MethodInfo m6982_MI;
extern MethodInfo m6987_MI;


extern MethodInfo m8037_MI;
 void m8037 (t1483 * __this, MethodInfo* method){
	{
		__this->f4 = 1;
		__this->f5 = 3;
		m1331(__this, &m1331_MI);
		t29 * L_0 = m8039(NULL, &m8039_MI);
		__this->f3 = L_0;
		t735  L_1 = {0};
		m8157(&L_1, ((int32_t)255), &m8157_MI);
		__this->f2 = L_1;
		return;
	}
}
 void m8038 (t1483 * __this, t29 * p0, t735  p1, MethodInfo* method){
	{
		__this->f4 = 1;
		__this->f5 = 3;
		m1331(__this, &m1331_MI);
		__this->f3 = p0;
		__this->f2 = p1;
		return;
	}
}
 t29 * m8039 (t29 * __this, MethodInfo* method){
	{
		return (((t1483_SFs*)InitializedTypeInfo(&t1483_TI)->static_fields)->f6);
	}
}
 void m8040 (t1483 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m8041_MI;
 t1494 * m8041 (t1483 * __this, MethodInfo* method){
	{
		t1494 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m8042_MI;
 t735  m8042 (t1483 * __this, MethodInfo* method){
	{
		t735  L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m8043_MI;
 t29 * m8043 (t1483 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m8044_MI;
 int32_t m8044 (t1483 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m8045_MI;
 t29 * m8045 (t1483 * __this, t1034 * p0, MethodInfo* method){
	{
		t29 * L_0 = m8046(__this, p0, (t1498 *)NULL, &m8046_MI);
		return L_0;
	}
}
 t29 * m8046 (t1483 * __this, t1034 * p0, t1498 * p1, MethodInfo* method){
	t1304 * V_0 = {0};
	bool V_1 = false;
	uint8_t V_2 = {0};
	t1503 * V_3 = {0};
	t29 * V_4 = {0};
	t1451* V_5 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1633, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5235_MI, p0);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int64_t L_2 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m5240_MI, p0);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		t917 * L_3 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_3, (t7*) &_stringLiteral1634, &m3986_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t1304 * L_4 = (t1304 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1304_TI));
		m6971(L_4, p0, &m6971_MI);
		V_0 = L_4;
		m8047(__this, V_0, (&V_1), &m8047_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6976_MI, V_0);
		V_2 = (((uint8_t)L_5));
		if ((((uint32_t)V_2) != ((uint32_t)((int32_t)21))))
		{
			goto IL_0051;
		}
	}
	{
		t29 * L_6 = m8048(NULL, V_2, V_0, V_1, p1, __this, &m8048_MI);
		return L_6;
	}

IL_0051:
	{
		if ((((uint32_t)V_2) != ((uint32_t)((int32_t)22))))
		{
			goto IL_0062;
		}
	}
	{
		t29 * L_7 = m8049(NULL, V_2, V_0, V_1, p1, (t29 *)NULL, __this, &m8049_MI);
		return L_7;
	}

IL_0062:
	{
		t1503 * L_8 = (t1503 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1503_TI));
		m8052(L_8, __this, &m8052_MI);
		V_3 = L_8;
		m8054(V_3, V_2, V_0, V_1, (&V_4), (&V_5), &m8054_MI);
		if (!p1)
		{
			goto IL_0082;
		}
	}
	{
		VirtFuncInvoker1< t29 *, t1451* >::Invoke(&m9629_MI, p1, V_5);
	}

IL_0082:
	{
		return V_4;
	}
}
 void m8047 (t1483 * __this, t1304 * p0, bool* p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		V_0 = L_0;
		*((int8_t*)(p1)) = (int8_t)((((int32_t)V_0) == ((int32_t)2))? 1 : 0);
		VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern Il2CppType t1495_0_0_1;
FieldInfo t1483_f0_FieldInfo = 
{
	"assembly_format", &t1495_0_0_1, &t1483_TI, offsetof(t1483, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1494_0_0_1;
FieldInfo t1483_f1_FieldInfo = 
{
	"binder", &t1494_0_0_1, &t1483_TI, offsetof(t1483, f1), &EmptyCustomAttributesCache};
extern Il2CppType t735_0_0_1;
FieldInfo t1483_f2_FieldInfo = 
{
	"context", &t735_0_0_1, &t1483_TI, offsetof(t1483, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1458_0_0_1;
FieldInfo t1483_f3_FieldInfo = 
{
	"surrogate_selector", &t1458_0_0_1, &t1483_TI, offsetof(t1483, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1496_0_0_1;
FieldInfo t1483_f4_FieldInfo = 
{
	"type_format", &t1496_0_0_1, &t1483_TI, offsetof(t1483, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1497_0_0_1;
FieldInfo t1483_f5_FieldInfo = 
{
	"filter_level", &t1497_0_0_1, &t1483_TI, offsetof(t1483, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1458_0_0_17;
extern CustomAttributesCache t1483__CustomAttributeCache_U3CDefaultSurrogateSelectorU3Ek__BackingField;
FieldInfo t1483_f6_FieldInfo = 
{
	"<DefaultSurrogateSelector>k__BackingField", &t1458_0_0_17, &t1483_TI, offsetof(t1483_SFs, f6), &t1483__CustomAttributeCache_U3CDefaultSurrogateSelectorU3Ek__BackingField};
static FieldInfo* t1483_FIs[] =
{
	&t1483_f0_FieldInfo,
	&t1483_f1_FieldInfo,
	&t1483_f2_FieldInfo,
	&t1483_f3_FieldInfo,
	&t1483_f4_FieldInfo,
	&t1483_f5_FieldInfo,
	&t1483_f6_FieldInfo,
	NULL
};
static PropertyInfo t1483____DefaultSurrogateSelector_PropertyInfo = 
{
	&t1483_TI, "DefaultSurrogateSelector", &m8039_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1483____AssemblyFormat_PropertyInfo = 
{
	&t1483_TI, "AssemblyFormat", NULL, &m8040_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1483____Binder_PropertyInfo = 
{
	&t1483_TI, "Binder", &m8041_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1483____Context_PropertyInfo = 
{
	&t1483_TI, "Context", &m8042_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1483____SurrogateSelector_PropertyInfo = 
{
	&t1483_TI, "SurrogateSelector", &m8043_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1483____FilterLevel_PropertyInfo = 
{
	&t1483_TI, "FilterLevel", &m8044_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1483_PIs[] =
{
	&t1483____DefaultSurrogateSelector_PropertyInfo,
	&t1483____AssemblyFormat_PropertyInfo,
	&t1483____Binder_PropertyInfo,
	&t1483____Context_PropertyInfo,
	&t1483____SurrogateSelector_PropertyInfo,
	&t1483____FilterLevel_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8037_MI = 
{
	".ctor", (methodPointerType)&m8037, &t1483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3623, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1458_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1483_m8038_ParameterInfos[] = 
{
	{"selector", 0, 134222053, &EmptyCustomAttributesCache, &t1458_0_0_0},
	{"context", 1, 134222054, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m8038_MI = 
{
	".ctor", (methodPointerType)&m8038, &t1483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1483_m8038_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3624, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1458_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1483__CustomAttributeCache_m8039;
MethodInfo m8039_MI = 
{
	"get_DefaultSurrogateSelector", (methodPointerType)&m8039, &t1483_TI, &t1458_0_0_0, RuntimeInvoker_t29, NULL, &t1483__CustomAttributeCache_m8039, 2198, 0, 255, 0, false, false, 3625, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1495_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t1483_m8040_ParameterInfos[] = 
{
	{"value", 0, 134222055, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m8040_MI = 
{
	"set_AssemblyFormat", (methodPointerType)&m8040, &t1483_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1483_m8040_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 3626, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1494_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8041_MI = 
{
	"get_Binder", (methodPointerType)&m8041, &t1483_TI, &t1494_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 3627, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t735_0_0_0;
extern void* RuntimeInvoker_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m8042_MI = 
{
	"get_Context", (methodPointerType)&m8042, &t1483_TI, &t735_0_0_0, RuntimeInvoker_t735, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 3628, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1458_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8043_MI = 
{
	"get_SurrogateSelector", (methodPointerType)&m8043, &t1483_TI, &t1458_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 3629, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1497_0_0_0;
extern void* RuntimeInvoker_t1497 (MethodInfo* method, void* obj, void** args);
MethodInfo m8044_MI = 
{
	"get_FilterLevel", (methodPointerType)&m8044, &t1483_TI, &t1497_0_0_0, RuntimeInvoker_t1497, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3630, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1034_0_0_0;
extern Il2CppType t1034_0_0_0;
static ParameterInfo t1483_m8045_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222056, &EmptyCustomAttributesCache, &t1034_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8045_MI = 
{
	"Deserialize", (methodPointerType)&m8045, &t1483_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1483_m8045_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 1, false, false, 3631, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1034_0_0_0;
extern Il2CppType t1498_0_0_0;
extern Il2CppType t1498_0_0_0;
static ParameterInfo t1483_m8046_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222057, &EmptyCustomAttributesCache, &t1034_0_0_0},
	{"handler", 1, 134222058, &EmptyCustomAttributesCache, &t1498_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8046_MI = 
{
	"NoCheckDeserialize", (methodPointerType)&m8046, &t1483_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1483_m8046_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3632, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_1_0_2;
extern Il2CppType t40_1_0_0;
static ParameterInfo t1483_m8047_ParameterInfos[] = 
{
	{"reader", 0, 134222059, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"hasHeaders", 1, 134222060, &EmptyCustomAttributesCache, &t40_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t326 (MethodInfo* method, void* obj, void** args);
MethodInfo m8047_MI = 
{
	"ReadBinaryHeader", (methodPointerType)&m8047, &t1483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t326, t1483_m8047_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3633, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1483_MIs[] =
{
	&m8037_MI,
	&m8038_MI,
	&m8039_MI,
	&m8040_MI,
	&m8041_MI,
	&m8042_MI,
	&m8043_MI,
	&m8044_MI,
	&m8045_MI,
	&m8046_MI,
	&m8047_MI,
	NULL
};
static MethodInfo* t1483_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8041_MI,
	&m8042_MI,
	&m8043_MI,
	&m8045_MI,
};
static TypeInfo* t1483_ITIs[] = 
{
	&t2048_TI,
	&t2049_TI,
};
static Il2CppInterfaceOffsetPair t1483_IOs[] = 
{
	{ &t2048_TI, 4},
	{ &t2049_TI, 4},
};
void t1483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1483_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1483_CustomAttributesCacheGenerator_m8039(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1483__CustomAttributeCache = {
1,
NULL,
&t1483_CustomAttributesCacheGenerator
};
CustomAttributesCache t1483__CustomAttributeCache_U3CDefaultSurrogateSelectorU3Ek__BackingField = {
1,
NULL,
&t1483_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField
};
CustomAttributesCache t1483__CustomAttributeCache_m8039 = {
1,
NULL,
&t1483_CustomAttributesCacheGenerator_m8039
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1483_0_0_0;
extern Il2CppType t1483_1_0_0;
struct t1483;
extern CustomAttributesCache t1483__CustomAttributeCache;
extern CustomAttributesCache t1483__CustomAttributeCache_U3CDefaultSurrogateSelectorU3Ek__BackingField;
extern CustomAttributesCache t1483__CustomAttributeCache_m8039;
TypeInfo t1483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BinaryFormatter", "System.Runtime.Serialization.Formatters.Binary", t1483_MIs, t1483_PIs, t1483_FIs, NULL, &t29_TI, NULL, NULL, &t1483_TI, t1483_ITIs, t1483_VT, &t1483__CustomAttributeCache, &t1483_TI, &t1483_0_0_0, &t1483_1_0_0, t1483_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1483), 0, -1, sizeof(t1483_SFs), 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 11, 6, 7, 0, 0, 8, 2, 2};
#include "t1499.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1499_TI;

extern MethodInfo m6990_MI;
extern MethodInfo m6993_MI;
extern MethodInfo m8078_MI;
extern MethodInfo m8053_MI;


 t29 * m8048 (t29 * __this, uint8_t p0, t1304 * p1, bool p2, t1498 * p3, t1483 * p4, MethodInfo* method){
	int32_t V_0 = {0};
	t7* V_1 = {0};
	t7* V_2 = {0};
	t316* V_3 = {0};
	t29 * V_4 = {0};
	t29 * V_5 = {0};
	t316* V_6 = {0};
	t1451* V_7 = {0};
	t537* V_8 = {0};
	uint32_t V_9 = 0;
	int32_t V_10 = 0;
	t42 * V_11 = {0};
	t1503 * V_12 = {0};
	t29 * V_13 = {0};
	t316* V_14 = {0};
	int32_t V_15 = 0;
	t7* V_16 = {0};
	t1451* V_17 = {0};
	t1444 * V_18 = {0};
	t725  V_19 = {0};
	t316* V_20 = {0};
	int32_t V_21 = 0;
	{
		if ((((int32_t)p0) == ((int32_t)((int32_t)21))))
		{
			goto IL_001b;
		}
	}
	{
		uint8_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1490_TI), &L_0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m1312(NULL, (t7*) &_stringLiteral1635, L_1, &m1312_MI);
		t917 * L_3 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_3, L_2, &m3986_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001b:
	{
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p1);
		V_0 = L_4;
		uint8_t L_5 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)18))))
		{
			goto IL_0037;
		}
	}
	{
		t917 * L_6 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_6, (t7*) &_stringLiteral1636, &m3986_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_0037:
	{
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p1);
		V_1 = L_7;
		uint8_t L_8 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)18))))
		{
			goto IL_0053;
		}
	}
	{
		t917 * L_9 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_9, (t7*) &_stringLiteral1636, &m3986_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0053:
	{
		t7* L_10 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p1);
		V_2 = L_10;
		V_3 = (t316*)NULL;
		V_4 = NULL;
		V_5 = NULL;
		V_6 = (t316*)NULL;
		V_7 = (t1451*)NULL;
		V_8 = (t537*)NULL;
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)2))) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		uint32_t L_11 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p1);
		V_9 = L_11;
		V_3 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), (((uintptr_t)V_9))));
		V_10 = 0;
		goto IL_00a6;
	}

IL_0087:
	{
		uint8_t L_12 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		t42 * L_13 = m8035(NULL, L_12, &m8035_MI);
		V_11 = L_13;
		t29 * L_14 = m8078(NULL, p1, V_11, &m8078_MI);
		ArrayElementTypeCheck (V_3, L_14);
		*((t29 **)(t29 **)SZArrayLdElema(V_3, V_10)) = (t29 *)L_14;
		V_10 = ((int32_t)(V_10+1));
	}

IL_00a6:
	{
		if ((((int64_t)(((int64_t)V_10))) < ((int64_t)(((uint64_t)V_9)))))
		{
			goto IL_0087;
		}
	}

IL_00ae:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)32972)))) <= ((int32_t)0)))
		{
			goto IL_016b;
		}
	}
	{
		t1503 * L_15 = (t1503 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1503_TI));
		m8052(L_15, p4, &m8052_MI);
		V_12 = L_15;
		m8053(V_12, p1, p2, (&V_13), (&V_7), &m8053_MI);
		V_14 = ((t316*)Castclass(V_13, InitializedTypeInfo(&t316_TI)));
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)4))) <= ((int32_t)0)))
		{
			goto IL_00e8;
		}
	}
	{
		V_3 = V_14;
		goto IL_0169;
	}

IL_00e8:
	{
		V_15 = 0;
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)8))) <= ((int32_t)0)))
		{
			goto IL_0111;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_14)->max_length)))) <= ((int32_t)1)))
		{
			goto IL_010a;
		}
	}
	{
		int32_t L_16 = V_15;
		V_15 = ((int32_t)(L_16+1));
		int32_t L_17 = L_16;
		V_3 = ((t316*)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_14, L_17)), InitializedTypeInfo(&t316_TI)));
		goto IL_0111;
	}

IL_010a:
	{
		V_3 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
	}

IL_0111:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)32768)))) <= ((int32_t)0)))
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_18 = V_15;
		V_15 = ((int32_t)(L_18+1));
		int32_t L_19 = L_18;
		V_8 = ((t537*)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_14, L_19)), InitializedTypeInfo(&t537_TI)));
	}

IL_012c:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)128)))) <= ((int32_t)0)))
		{
			goto IL_0142;
		}
	}
	{
		int32_t L_20 = V_15;
		V_15 = ((int32_t)(L_20+1));
		int32_t L_21 = L_20;
		V_4 = (*(t29 **)(t29 **)SZArrayLdElema(V_14, L_21));
	}

IL_0142:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)64)))) <= ((int32_t)0)))
		{
			goto IL_0155;
		}
	}
	{
		int32_t L_22 = V_15;
		V_15 = ((int32_t)(L_22+1));
		int32_t L_23 = L_22;
		V_5 = (*(t29 **)(t29 **)SZArrayLdElema(V_14, L_23));
	}

IL_0155:
	{
		if ((((int32_t)V_15) >= ((int32_t)(((int32_t)(((t20 *)V_14)->max_length))))))
		{
			goto IL_0169;
		}
	}
	{
		int32_t L_24 = V_15;
		V_6 = ((t316*)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_14, L_24)), InitializedTypeInfo(&t316_TI)));
	}

IL_0169:
	{
		goto IL_0172;
	}

IL_016b:
	{
		VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
	}

IL_0172:
	{
		if (V_3)
		{
			goto IL_017c;
		}
	}
	{
		V_3 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
	}

IL_017c:
	{
		V_16 = (t7*)NULL;
		if (!p3)
		{
			goto IL_0191;
		}
	}
	{
		t29 * L_25 = (t29 *)VirtFuncInvoker1< t29 *, t1451* >::Invoke(&m9629_MI, p3, V_7);
		V_16 = ((t7*)IsInst(L_25, (&t7_TI)));
	}

IL_0191:
	{
		V_17 = ((t1451*)SZArrayNew(InitializedTypeInfo(&t1451_TI), 7));
		t1448 * L_26 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_26, (t7*) &_stringLiteral1596, V_1, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_26);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 0)) = (t1448 *)L_26;
		t1448 * L_27 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_27, (t7*) &_stringLiteral1598, V_4, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_27);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 1)) = (t1448 *)L_27;
		t1448 * L_28 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_28, (t7*) &_stringLiteral1597, V_2, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_28);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 2)) = (t1448 *)L_28;
		t1448 * L_29 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_29, (t7*) &_stringLiteral1599, (t29 *)(t29 *)V_3, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_29);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 3)) = (t1448 *)L_29;
		t1448 * L_30 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_30, (t7*) &_stringLiteral1600, V_5, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_30);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 4)) = (t1448 *)L_30;
		t1448 * L_31 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_31, (t7*) &_stringLiteral1595, V_16, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_31);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 5)) = (t1448 *)L_31;
		t1448 * L_32 = (t1448 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1448_TI));
		m7852(L_32, (t7*) &_stringLiteral1603, (t29 *)(t29 *)V_8, &m7852_MI);
		ArrayElementTypeCheck (V_17, L_32);
		*((t1448 **)(t1448 **)SZArrayLdElema(V_17, 6)) = (t1448 *)L_32;
		t1444 * L_33 = (t1444 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1444_TI));
		m7860(L_33, V_17, &m7860_MI);
		V_18 = L_33;
		if (!V_6)
		{
			goto IL_0255;
		}
	}
	{
		V_20 = V_6;
		V_21 = 0;
		goto IL_024d;
	}

IL_021c:
	{
		int32_t L_34 = V_21;
		V_19 = ((*(t725 *)((t725 *)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(V_20, L_34)), InitializedTypeInfo(&t725_TI)))));
		t29 * L_35 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7871_MI, V_18);
		t29 * L_36 = m6684((&V_19), &m6684_MI);
		t29 * L_37 = m6685((&V_19), &m6685_MI);
		InterfaceActionInvoker2< t29 *, t29 * >::Invoke(&m3955_MI, L_35, ((t7*)Castclass(L_36, (&t7_TI))), L_37);
		V_21 = ((int32_t)(V_21+1));
	}

IL_024d:
	{
		if ((((int32_t)V_21) < ((int32_t)(((int32_t)(((t20 *)V_20)->max_length))))))
		{
			goto IL_021c;
		}
	}

IL_0255:
	{
		return V_18;
	}
}
 t29 * m8049 (t29 * __this, uint8_t p0, t1304 * p1, bool p2, t1498 * p3, t29 * p4, t1483 * p5, MethodInfo* method){
	int32_t V_0 = {0};
	uint8_t V_1 = {0};
	bool V_2 = false;
	t29 * V_3 = {0};
	t316* V_4 = {0};
	t1449 * V_5 = {0};
	t295 * V_6 = {0};
	t316* V_7 = {0};
	t1451* V_8 = {0};
	t42 * V_9 = {0};
	uint32_t V_10 = 0;
	int32_t V_11 = 0;
	t42 * V_12 = {0};
	t1503 * V_13 = {0};
	t29 * V_14 = {0};
	t316* V_15 = {0};
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	t1462 * V_19 = {0};
	t725  V_20 = {0};
	t316* V_21 = {0};
	int32_t V_22 = 0;
	int32_t G_B43_0 = 0;
	{
		if ((((int32_t)p0) == ((int32_t)((int32_t)22))))
		{
			goto IL_001b;
		}
	}
	{
		uint8_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1490_TI), &L_0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m1312(NULL, (t7*) &_stringLiteral1637, L_1, &m1312_MI);
		t917 * L_3 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_3, L_2, &m3986_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001b:
	{
		uint8_t L_4 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		V_0 = L_4;
		uint8_t L_5 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		V_1 = L_5;
		V_2 = ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)64)))) > ((int32_t)0))? 1 : 0);
		VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		V_3 = NULL;
		V_4 = (t316*)NULL;
		V_5 = (t1449 *)NULL;
		V_6 = (t295 *)NULL;
		V_7 = (t316*)NULL;
		V_8 = (t1451*)NULL;
		if ((((int32_t)(((uint8_t)((int32_t)((int32_t)V_1&(int32_t)8))))) <= ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		uint8_t L_6 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		t42 * L_7 = m8035(NULL, L_6, &m8035_MI);
		V_9 = L_7;
		t29 * L_8 = m8078(NULL, p1, V_9, &m8078_MI);
		V_3 = L_8;
	}

IL_006d:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)2))) <= ((int32_t)0)))
		{
			goto IL_00b2;
		}
	}
	{
		uint32_t L_9 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p1);
		V_10 = L_9;
		V_4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), (((uintptr_t)V_10))));
		V_11 = 0;
		goto IL_00aa;
	}

IL_008a:
	{
		uint8_t L_10 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		t42 * L_11 = m8035(NULL, L_10, &m8035_MI);
		V_12 = L_11;
		t29 * L_12 = m8078(NULL, p1, V_12, &m8078_MI);
		ArrayElementTypeCheck (V_4, L_12);
		*((t29 **)(t29 **)SZArrayLdElema(V_4, V_11)) = (t29 *)L_12;
		V_11 = ((int32_t)(V_11+1));
	}

IL_00aa:
	{
		if ((((int64_t)(((int64_t)V_11))) < ((int64_t)(((uint64_t)V_10)))))
		{
			goto IL_008a;
		}
	}

IL_00b2:
	{
		if (V_2)
		{
			goto IL_00d4;
		}
	}
	{
		if ((((int32_t)(((uint8_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)16)))))) > ((int32_t)0)))
		{
			goto IL_00d4;
		}
	}
	{
		if ((((int32_t)(((uint8_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)32)))))) > ((int32_t)0)))
		{
			goto IL_00d4;
		}
	}
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)4))) > ((int32_t)0)))
		{
			goto IL_00d4;
		}
	}
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)8))) <= ((int32_t)0)))
		{
			goto IL_01c2;
		}
	}

IL_00d4:
	{
		t1503 * L_13 = (t1503 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1503_TI));
		m8052(L_13, p5, &m8052_MI);
		V_13 = L_13;
		m8053(V_13, p1, p2, (&V_14), (&V_8), &m8053_MI);
		V_15 = ((t316*)Castclass(V_14, InitializedTypeInfo(&t316_TI)));
		if ((((int32_t)(((uint8_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)32)))))) <= ((int32_t)0)))
		{
			goto IL_0119;
		}
	}
	{
		int32_t L_14 = 0;
		V_6 = ((t295 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_14)), InitializedTypeInfo(&t295_TI)));
		if (!V_2)
		{
			goto IL_0114;
		}
	}
	{
		int32_t L_15 = 1;
		V_5 = ((t1449 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_15)), InitializedTypeInfo(&t1449_TI)));
	}

IL_0114:
	{
		goto IL_01c0;
	}

IL_0119:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)1))) > ((int32_t)0)))
		{
			goto IL_0125;
		}
	}
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)2))) <= ((int32_t)0)))
		{
			goto IL_0165;
		}
	}

IL_0125:
	{
		V_16 = 0;
		if ((((int32_t)(((uint8_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)16)))))) <= ((int32_t)0)))
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_16 = V_16;
		V_16 = ((int32_t)(L_16+1));
		int32_t L_17 = L_16;
		V_3 = (*(t29 **)(t29 **)SZArrayLdElema(V_15, L_17));
	}

IL_013b:
	{
		if (!V_2)
		{
			goto IL_014f;
		}
	}
	{
		int32_t L_18 = V_16;
		V_16 = ((int32_t)(L_18+1));
		int32_t L_19 = L_18;
		V_5 = ((t1449 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_19)), InitializedTypeInfo(&t1449_TI)));
	}

IL_014f:
	{
		if ((((int32_t)V_16) >= ((int32_t)(((int32_t)(((t20 *)V_15)->max_length))))))
		{
			goto IL_0163;
		}
	}
	{
		int32_t L_20 = V_16;
		V_7 = ((t316*)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_20)), InitializedTypeInfo(&t316_TI)));
	}

IL_0163:
	{
		goto IL_01c0;
	}

IL_0165:
	{
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)4))) <= ((int32_t)0)))
		{
			goto IL_0171;
		}
	}
	{
		V_4 = V_15;
		goto IL_01c0;
	}

IL_0171:
	{
		V_17 = 0;
		int32_t L_21 = V_17;
		V_17 = ((int32_t)(L_21+1));
		int32_t L_22 = L_21;
		V_4 = ((t316*)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_22)), InitializedTypeInfo(&t316_TI)));
		if ((((int32_t)(((uint8_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)16)))))) <= ((int32_t)0)))
		{
			goto IL_0198;
		}
	}
	{
		int32_t L_23 = V_17;
		V_17 = ((int32_t)(L_23+1));
		int32_t L_24 = L_23;
		V_3 = (*(t29 **)(t29 **)SZArrayLdElema(V_15, L_24));
	}

IL_0198:
	{
		if (!V_2)
		{
			goto IL_01ac;
		}
	}
	{
		int32_t L_25 = V_17;
		V_17 = ((int32_t)(L_25+1));
		int32_t L_26 = L_25;
		V_5 = ((t1449 *)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_26)), InitializedTypeInfo(&t1449_TI)));
	}

IL_01ac:
	{
		if ((((int32_t)V_17) >= ((int32_t)(((int32_t)(((t20 *)V_15)->max_length))))))
		{
			goto IL_01c0;
		}
	}
	{
		int32_t L_27 = V_17;
		V_7 = ((t316*)Castclass((*(t29 **)(t29 **)SZArrayLdElema(V_15, L_27)), InitializedTypeInfo(&t316_TI)));
	}

IL_01c0:
	{
		goto IL_01c9;
	}

IL_01c2:
	{
		VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
	}

IL_01c9:
	{
		if (!p3)
		{
			goto IL_01d5;
		}
	}
	{
		VirtFuncInvoker1< t29 *, t1451* >::Invoke(&m9629_MI, p3, V_8);
	}

IL_01d5:
	{
		if (!V_6)
		{
			goto IL_01e3;
		}
	}
	{
		t1462 * L_28 = (t1462 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1462_TI));
		m7929(L_28, V_6, p4, &m7929_MI);
		return L_28;
	}

IL_01e3:
	{
		if (!V_4)
		{
			goto IL_01ed;
		}
	}
	{
		G_B43_0 = (((int32_t)(((t20 *)V_4)->max_length)));
		goto IL_01ee;
	}

IL_01ed:
	{
		G_B43_0 = 0;
	}

IL_01ee:
	{
		V_18 = G_B43_0;
		t1462 * L_29 = (t1462 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1462_TI));
		m7928(L_29, V_3, V_4, V_18, V_5, p4, &m7928_MI);
		V_19 = L_29;
		if (!V_7)
		{
			goto IL_0246;
		}
	}
	{
		V_21 = V_7;
		V_22 = 0;
		goto IL_023e;
	}

IL_020d:
	{
		int32_t L_30 = V_22;
		V_20 = ((*(t725 *)((t725 *)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(V_21, L_30)), InitializedTypeInfo(&t725_TI)))));
		t29 * L_31 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m7936_MI, V_19);
		t29 * L_32 = m6684((&V_20), &m6684_MI);
		t29 * L_33 = m6685((&V_20), &m6685_MI);
		InterfaceActionInvoker2< t29 *, t29 * >::Invoke(&m3955_MI, L_31, ((t7*)Castclass(L_32, (&t7_TI))), L_33);
		V_22 = ((int32_t)(V_22+1));
	}

IL_023e:
	{
		if ((((int32_t)V_22) < ((int32_t)(((int32_t)(((t20 *)V_21)->max_length))))))
		{
			goto IL_020d;
		}
	}

IL_0246:
	{
		return V_19;
	}
}
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t1498_0_0_0;
extern Il2CppType t1483_0_0_0;
static ParameterInfo t1499_m8048_ParameterInfos[] = 
{
	{"elem", 0, 134222061, &EmptyCustomAttributesCache, &t1490_0_0_0},
	{"reader", 1, 134222062, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"hasHeaders", 2, 134222063, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"headerHandler", 3, 134222064, &EmptyCustomAttributesCache, &t1498_0_0_0},
	{"formatter", 4, 134222065, &EmptyCustomAttributesCache, &t1483_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t348_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8048_MI = 
{
	"ReadMethodCall", (methodPointerType)&m8048, &t1499_TI, &t29_0_0_0, RuntimeInvoker_t29_t348_t29_t297_t29_t29, t1499_m8048_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 5, false, false, 3634, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t1498_0_0_0;
extern Il2CppType t1463_0_0_0;
extern Il2CppType t1483_0_0_0;
static ParameterInfo t1499_m8049_ParameterInfos[] = 
{
	{"elem", 0, 134222066, &EmptyCustomAttributesCache, &t1490_0_0_0},
	{"reader", 1, 134222067, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"hasHeaders", 2, 134222068, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"headerHandler", 3, 134222069, &EmptyCustomAttributesCache, &t1498_0_0_0},
	{"methodCallMessage", 4, 134222070, &EmptyCustomAttributesCache, &t1463_0_0_0},
	{"formatter", 5, 134222071, &EmptyCustomAttributesCache, &t1483_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t348_t29_t297_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8049_MI = 
{
	"ReadMethodResponse", (methodPointerType)&m8049, &t1499_TI, &t29_0_0_0, RuntimeInvoker_t29_t348_t29_t297_t29_t29_t29, t1499_m8049_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 6, false, false, 3635, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1499_MIs[] =
{
	&m8048_MI,
	&m8049_MI,
	NULL
};
static MethodInfo* t1499_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1499_0_0_0;
extern Il2CppType t1499_1_0_0;
struct t1499;
TypeInfo t1499_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MessageFormatter", "System.Runtime.Serialization.Formatters.Binary", t1499_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1499_TI, NULL, t1499_VT, &EmptyCustomAttributesCache, &t1499_TI, &t1499_0_0_0, &t1499_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1499), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 4, 0, 0};
#include "t1500.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1500_TI;
#include "t1500MD.h"



extern MethodInfo m8050_MI;
 void m8050 (t1500 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern Il2CppType t42_0_0_6;
FieldInfo t1500_f0_FieldInfo = 
{
	"Type", &t42_0_0_6, &t1500_TI, offsetof(t1500, f0), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_6;
FieldInfo t1500_f1_FieldInfo = 
{
	"MemberTypes", &t537_0_0_6, &t1500_TI, offsetof(t1500, f1), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_6;
FieldInfo t1500_f2_FieldInfo = 
{
	"MemberNames", &t446_0_0_6, &t1500_TI, offsetof(t1500, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1501_0_0_6;
FieldInfo t1500_f3_FieldInfo = 
{
	"MemberInfos", &t1501_0_0_6, &t1500_TI, offsetof(t1500, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t1500_f4_FieldInfo = 
{
	"FieldCount", &t44_0_0_6, &t1500_TI, offsetof(t1500, f4), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t1500_f5_FieldInfo = 
{
	"NeedsSerializationInfo", &t40_0_0_6, &t1500_TI, offsetof(t1500, f5), &EmptyCustomAttributesCache};
static FieldInfo* t1500_FIs[] =
{
	&t1500_f0_FieldInfo,
	&t1500_f1_FieldInfo,
	&t1500_f2_FieldInfo,
	&t1500_f3_FieldInfo,
	&t1500_f4_FieldInfo,
	&t1500_f5_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8050_MI = 
{
	".ctor", (methodPointerType)&m8050, &t1500_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3663, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1500_MIs[] =
{
	&m8050_MI,
	NULL
};
static MethodInfo* t1500_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1500_0_0_0;
extern Il2CppType t1500_1_0_0;
struct t1500;
TypeInfo t1500_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeMetadata", "", t1500_MIs, NULL, t1500_FIs, NULL, &t29_TI, NULL, &t1503_TI, &t1500_TI, NULL, t1500_VT, &EmptyCustomAttributesCache, &t1500_TI, &t1500_0_0_0, &t1500_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1500), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 6, 0, 0, 4, 0, 0};
#include "t1502.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1502_TI;
#include "t1502MD.h"



extern MethodInfo m8051_MI;
 void m8051 (t1502 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern Il2CppType t44_0_0_6;
FieldInfo t1502_f0_FieldInfo = 
{
	"NullCount", &t44_0_0_6, &t1502_TI, offsetof(t1502, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1502_FIs[] =
{
	&t1502_f0_FieldInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1502_m8051_ParameterInfos[] = 
{
	{"count", 0, 134222166, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m8051_MI = 
{
	".ctor", (methodPointerType)&m8051, &t1502_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1502_m8051_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3664, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1502_MIs[] =
{
	&m8051_MI,
	NULL
};
static MethodInfo* t1502_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1502_0_0_0;
extern Il2CppType t1502_1_0_0;
struct t1502;
TypeInfo t1502_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ArrayNullFiller", "", t1502_MIs, NULL, t1502_FIs, NULL, &t29_TI, NULL, &t1503_TI, &t1502_TI, NULL, t1502_VT, &EmptyCustomAttributesCache, &t1502_TI, &t1502_0_0_0, &t1502_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1502), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1504.h"
#include "t1505.h"
#include "t1127.h"
#include "t922.h"
#include "t1144.h"
#include "t1146.h"
#include "t929.h"
#include "t1660.h"
#include "t633.h"
extern TypeInfo t1494_TI;
extern TypeInfo t1504_TI;
extern TypeInfo t1505_TI;
extern TypeInfo t771_TI;
extern TypeInfo t200_TI;
extern TypeInfo t2053_TI;
extern TypeInfo t2054_TI;
extern TypeInfo t1140_TI;
extern TypeInfo t1763_TI;
extern TypeInfo t1139_TI;
extern TypeInfo t1595_TI;
extern TypeInfo t509_TI;
extern TypeInfo t764_TI;
extern TypeInfo t975_TI;
extern TypeInfo t1552_TI;
extern TypeInfo t2055_TI;
extern TypeInfo t922_TI;
extern TypeInfo t2056_TI;
extern TypeInfo t1501_TI;
extern TypeInfo t1144_TI;
extern TypeInfo t1146_TI;
extern TypeInfo t929_TI;
extern TypeInfo t1660_TI;
extern TypeInfo t2057_TI;
extern TypeInfo t633_TI;
#include "t1504MD.h"
#include "t1506MD.h"
#include "t1505MD.h"
#include "t20MD.h"
#include "t465MD.h"
#include "t816MD.h"
#include "t938MD.h"
#include "t922MD.h"
#include "t1144MD.h"
#include "t1146MD.h"
#include "t1494MD.h"
#include "t929MD.h"
#include "t633MD.h"
#include "t1126MD.h"
extern Il2CppType t374_0_0_0;
extern Il2CppType t1660_0_0_0;
extern Il2CppType t2057_0_0_0;
extern MethodInfo m8088_MI;
extern MethodInfo m8055_MI;
extern MethodInfo m8057_MI;
extern MethodInfo m8056_MI;
extern MethodInfo m8091_MI;
extern MethodInfo m8089_MI;
extern MethodInfo m8092_MI;
extern MethodInfo m8058_MI;
extern MethodInfo m8063_MI;
extern MethodInfo m8061_MI;
extern MethodInfo m8060_MI;
extern MethodInfo m8064_MI;
extern MethodInfo m8065_MI;
extern MethodInfo m8066_MI;
extern MethodInfo m8059_MI;
extern MethodInfo m8067_MI;
extern MethodInfo m8069_MI;
extern MethodInfo m8070_MI;
extern MethodInfo m8072_MI;
extern MethodInfo m8062_MI;
extern MethodInfo m8087_MI;
extern MethodInfo m8086_MI;
extern MethodInfo m8093_MI;
extern MethodInfo m8079_MI;
extern MethodInfo m8143_MI;
extern MethodInfo m8073_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m8101_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m8077_MI;
extern MethodInfo m5939_MI;
extern MethodInfo m5913_MI;
extern MethodInfo m3977_MI;
extern MethodInfo m5924_MI;
extern MethodInfo m6015_MI;
extern MethodInfo m6981_MI;
extern MethodInfo m6977_MI;
extern MethodInfo m6978_MI;
extern MethodInfo m6988_MI;
extern MethodInfo m9153_MI;
extern MethodInfo m6984_MI;
extern MethodInfo m8068_MI;
extern MethodInfo m6985_MI;
extern MethodInfo m6986_MI;
extern MethodInfo m6989_MI;
extern MethodInfo m6991_MI;
extern MethodInfo m6992_MI;
extern MethodInfo m6994_MI;
extern MethodInfo m9520_MI;
extern MethodInfo m8878_MI;
extern MethodInfo m4055_MI;
extern MethodInfo m8071_MI;
extern MethodInfo m5936_MI;
extern MethodInfo m8076_MI;
extern MethodInfo m2978_MI;
extern MethodInfo m6003_MI;
extern MethodInfo m1810_MI;
extern MethodInfo m9852_MI;
extern MethodInfo m10154_MI;
extern MethodInfo m8074_MI;
extern MethodInfo m8075_MI;
extern MethodInfo m10211_MI;
extern MethodInfo m5915_MI;
extern MethodInfo m7529_MI;
extern MethodInfo m7714_MI;
extern MethodInfo m8098_MI;
extern MethodInfo m8096_MI;
extern MethodInfo m8097_MI;
extern MethodInfo m8099_MI;
extern MethodInfo m10212_MI;
extern MethodInfo m7473_MI;
extern MethodInfo m7467_MI;
extern MethodInfo m8159_MI;
extern MethodInfo m6983_MI;
extern MethodInfo m4009_MI;
extern MethodInfo m5735_MI;


 void m8052 (t1503 * __this, t1483 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		__this->f5 = L_0;
		t719 * L_1 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_1, &m4209_MI);
		__this->f6 = L_1;
		__this->f11 = ((int32_t)4096);
		m1331(__this, &m1331_MI);
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m8043_MI, p0);
		__this->f0 = L_2;
		t735  L_3 = (t735 )VirtFuncInvoker0< t735  >::Invoke(&m8042_MI, p0);
		__this->f1 = L_3;
		t1494 * L_4 = (t1494 *)VirtFuncInvoker0< t1494 * >::Invoke(&m8041_MI, p0);
		__this->f2 = L_4;
		t29 * L_5 = (__this->f0);
		t735  L_6 = (__this->f1);
		t1504 * L_7 = (t1504 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1504_TI));
		m8088(L_7, L_5, L_6, &m8088_MI);
		__this->f4 = L_7;
		int32_t L_8 = m8044(p0, &m8044_MI);
		__this->f3 = L_8;
		return;
	}
}
 void m8053 (t1503 * __this, t1304 * p0, bool p1, t29 ** p2, t1451** p3, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		uint8_t L_0 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		V_0 = L_0;
		m8054(__this, V_0, p0, p1, p2, p3, &m8054_MI);
		return;
	}
}
 void m8054 (t1503 * __this, uint8_t p0, t1304 * p1, bool p2, t29 ** p3, t1451** p4, MethodInfo* method){
	bool V_0 = false;
	{
		*((t29 **)(p4)) = (t29 *)NULL;
		bool L_0 = m8055(__this, p0, p1, &m8055_MI);
		V_0 = L_0;
		if (!V_0)
		{
			goto IL_0045;
		}
	}

IL_0010:
	{
		if (!p2)
		{
			goto IL_0028;
		}
	}
	{
		if ((*((t1451**)p4)))
		{
			goto IL_0028;
		}
	}
	{
		t29 * L_1 = m8057(__this, &m8057_MI);
		*((t29 **)(p4)) = (t29 *)((t1451*)Castclass(L_1, InitializedTypeInfo(&t1451_TI)));
		goto IL_003c;
	}

IL_0028:
	{
		int64_t L_2 = (__this->f9);
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		int64_t L_3 = (__this->f8);
		__this->f9 = L_3;
	}

IL_003c:
	{
		bool L_4 = m8056(__this, p1, &m8056_MI);
		if (L_4)
		{
			goto IL_0010;
		}
	}

IL_0045:
	{
		t1504 * L_5 = (__this->f4);
		int64_t L_6 = (__this->f9);
		t29 * L_7 = (t29 *)VirtFuncInvoker1< t29 *, int64_t >::Invoke(&m8091_MI, L_5, L_6);
		*((t29 **)(p3)) = (t29 *)L_7;
		return;
	}
}
 bool m8055 (t1503 * __this, uint8_t p0, t1304 * p1, MethodInfo* method){
	t733 * V_0 = {0};
	int64_t V_1 = 0;
	{
		if ((((uint32_t)p0) != ((uint32_t)((int32_t)11))))
		{
			goto IL_001d;
		}
	}
	{
		t1504 * L_0 = (__this->f4);
		VirtActionInvoker0::Invoke(&m8089_MI, L_0);
		t1504 * L_1 = (__this->f4);
		VirtActionInvoker0::Invoke(&m8092_MI, L_1);
		return 0;
	}

IL_001d:
	{
		t29 ** L_2 = &(__this->f7);
		m8058(__this, p0, p1, (&V_1), L_2, (&V_0), &m8058_MI);
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		t29 * L_3 = (__this->f7);
		m8063(__this, V_1, L_3, V_0, (((int64_t)0)), (t296 *)NULL, (t841*)(t841*)NULL, &m8063_MI);
		__this->f8 = V_1;
	}

IL_004b:
	{
		return 1;
	}
}
 bool m8056 (t1503 * __this, t1304 * p0, MethodInfo* method){
	uint8_t V_0 = {0};
	t733 * V_1 = {0};
	int64_t V_2 = 0;
	{
		uint8_t L_0 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)((int32_t)11))))
		{
			goto IL_0024;
		}
	}
	{
		t1504 * L_1 = (__this->f4);
		VirtActionInvoker0::Invoke(&m8089_MI, L_1);
		t1504 * L_2 = (__this->f4);
		VirtActionInvoker0::Invoke(&m8092_MI, L_2);
		return 0;
	}

IL_0024:
	{
		t29 ** L_3 = &(__this->f7);
		m8058(__this, V_0, p0, (&V_2), L_3, (&V_1), &m8058_MI);
		if (!V_2)
		{
			goto IL_0052;
		}
	}
	{
		t29 * L_4 = (__this->f7);
		m8063(__this, V_2, L_4, V_1, (((int64_t)0)), (t296 *)NULL, (t841*)(t841*)NULL, &m8063_MI);
		__this->f8 = V_2;
	}

IL_0052:
	{
		return 1;
	}
}
 t29 * m8057 (t1503 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f7);
		return L_0;
	}
}
 void m8058 (t1503 * __this, uint8_t p0, t1304 * p1, int64_t* p2, t29 ** p3, t733 ** p4, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		V_0 = p0;
		if (((uint8_t)(V_0-1)) == 0)
		{
			goto IL_0053;
		}
		if (((uint8_t)(V_0-1)) == 1)
		{
			goto IL_0064;
		}
		if (((uint8_t)(V_0-1)) == 2)
		{
			goto IL_0077;
		}
		if (((uint8_t)(V_0-1)) == 3)
		{
			goto IL_008a;
		}
		if (((uint8_t)(V_0-1)) == 4)
		{
			goto IL_009d;
		}
		if (((uint8_t)(V_0-1)) == 5)
		{
			goto IL_00b0;
		}
		if (((uint8_t)(V_0-1)) == 6)
		{
			goto IL_00c3;
		}
		if (((uint8_t)(V_0-1)) == 7)
		{
			goto IL_00d6;
		}
		if (((uint8_t)(V_0-1)) == 8)
		{
			goto IL_017c;
		}
		if (((uint8_t)(V_0-1)) == 9)
		{
			goto IL_00ed;
		}
		if (((uint8_t)(V_0-1)) == 10)
		{
			goto IL_017c;
		}
		if (((uint8_t)(V_0-1)) == 11)
		{
			goto IL_00fe;
		}
		if (((uint8_t)(V_0-1)) == 12)
		{
			goto IL_011c;
		}
		if (((uint8_t)(V_0-1)) == 13)
		{
			goto IL_0134;
		}
		if (((uint8_t)(V_0-1)) == 14)
		{
			goto IL_014c;
		}
		if (((uint8_t)(V_0-1)) == 15)
		{
			goto IL_015c;
		}
		if (((uint8_t)(V_0-1)) == 16)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_017c;
	}

IL_0053:
	{
		m8061(__this, p1, p2, p3, p4, &m8061_MI);
		goto IL_0192;
	}

IL_0064:
	{
		m8060(__this, p1, 1, 0, p2, p3, p4, &m8060_MI);
		goto IL_0192;
	}

IL_0077:
	{
		m8060(__this, p1, 0, 0, p2, p3, p4, &m8060_MI);
		goto IL_0192;
	}

IL_008a:
	{
		m8060(__this, p1, 1, 1, p2, p3, p4, &m8060_MI);
		goto IL_0192;
	}

IL_009d:
	{
		m8060(__this, p1, 0, 1, p2, p3, p4, &m8060_MI);
		goto IL_0192;
	}

IL_00b0:
	{
		*((t29 **)(p4)) = (t29 *)NULL;
		m8064(__this, p1, p2, p3, &m8064_MI);
		goto IL_0192;
	}

IL_00c3:
	{
		*((t29 **)(p4)) = (t29 *)NULL;
		m8065(__this, p1, p2, p3, &m8065_MI);
		goto IL_0192;
	}

IL_00d6:
	{
		t29 * L_0 = m8066(__this, p1, &m8066_MI);
		*((t29 **)(p3)) = (t29 *)L_0;
		*((int64_t*)(p2)) = (int64_t)(((int64_t)0));
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_00ed:
	{
		*((t29 **)(p3)) = (t29 *)NULL;
		*((int64_t*)(p2)) = (int64_t)(((int64_t)0));
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_00fe:
	{
		m8059(__this, p1, &m8059_MI);
		uint8_t L_1 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		m8058(__this, L_1, p1, p2, p3, p4, &m8058_MI);
		goto IL_0192;
	}

IL_011c:
	{
		uint8_t L_2 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p1);
		t1502 * L_3 = (t1502 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1502_TI));
		m8051(L_3, L_2, &m8051_MI);
		*((t29 **)(p3)) = (t29 *)L_3;
		*((int64_t*)(p2)) = (int64_t)(((int64_t)0));
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_0134:
	{
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p1);
		t1502 * L_5 = (t1502 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1502_TI));
		m8051(L_5, L_4, &m8051_MI);
		*((t29 **)(p3)) = (t29 *)L_5;
		*((int64_t*)(p2)) = (int64_t)(((int64_t)0));
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_014c:
	{
		m8067(__this, p1, p2, p3, &m8067_MI);
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_015c:
	{
		m8069(__this, p1, p2, p3, &m8069_MI);
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_016c:
	{
		m8070(__this, p1, p2, p3, &m8070_MI);
		*((t29 **)(p4)) = (t29 *)NULL;
		goto IL_0192;
	}

IL_017c:
	{
		int32_t L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t44_TI), &L_6);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_8 = m1312(NULL, (t7*) &_stringLiteral1638, L_7, &m1312_MI);
		t917 * L_9 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_9, L_8, &m3986_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0192:
	{
		return;
	}
}
 void m8059 (t1503 * __this, t1304 * p0, MethodInfo* method){
	int64_t V_0 = 0;
	t7* V_1 = {0};
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		V_0 = (((uint64_t)L_0));
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		V_1 = L_1;
		t719 * L_2 = (__this->f5);
		int64_t L_3 = V_0;
		t29 * L_4 = Box(InitializedTypeInfo(&t919_TI), &L_3);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, L_2, L_4, V_1);
		return;
	}
}
 void m8060 (t1503 * __this, t1304 * p0, bool p1, bool p2, int64_t* p3, t29 ** p4, t733 ** p5, MethodInfo* method){
	t1500 * V_0 = {0};
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((int64_t*)(p3)) = (int64_t)(((uint64_t)L_0));
		t1500 * L_1 = m8072(__this, p0, p1, p2, &m8072_MI);
		V_0 = L_1;
		m8062(__this, p0, V_0, (*((int64_t*)p3)), p4, p5, &m8062_MI);
		return;
	}
}
 void m8061 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, t733 ** p3, MethodInfo* method){
	int64_t V_0 = 0;
	t29 * V_1 = {0};
	t1500 * V_2 = {0};
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((int64_t*)(p1)) = (int64_t)(((uint64_t)L_0));
		uint32_t L_1 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		V_0 = (((uint64_t)L_1));
		t1504 * L_2 = (__this->f4);
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, int64_t >::Invoke(&m8091_MI, L_2, V_0);
		V_1 = L_3;
		if (V_1)
		{
			goto IL_002c;
		}
	}
	{
		t917 * L_4 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_4, (t7*) &_stringLiteral1639, &m3986_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002c:
	{
		t719 * L_5 = (__this->f6);
		t42 * L_6 = m1430(V_1, &m1430_MI);
		t29 * L_7 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_5, L_6);
		V_2 = ((t1500 *)Castclass(L_7, InitializedTypeInfo(&t1500_TI)));
		m8062(__this, p0, V_2, (*((int64_t*)p1)), p2, p3, &m8062_MI);
		return;
	}
}
 void m8062 (t1503 * __this, t1304 * p0, t1500 * p1, int64_t p2, t29 ** p3, t733 ** p4, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t733 ** G_B5_0 = {0};
	t733 ** G_B4_0 = {0};
	t733 * G_B6_0 = {0};
	t733 ** G_B6_1 = {0};
	{
		int32_t L_0 = (__this->f3);
		if ((((uint32_t)L_0) != ((uint32_t)2)))
		{
			goto IL_0019;
		}
	}
	{
		t42 * L_1 = (p1->f0);
		t29 * L_2 = m8087(NULL, L_1, &m8087_MI);
		*((t29 **)(p3)) = (t29 *)L_2;
		goto IL_0027;
	}

IL_0019:
	{
		t42 * L_3 = (p1->f0);
		t29 * L_4 = m8086(NULL, L_3, &m8086_MI);
		*((t29 **)(p3)) = (t29 *)L_4;
	}

IL_0027:
	{
		t1504 * L_5 = (__this->f4);
		m8093(L_5, (*((t29 **)p3)), &m8093_MI);
		bool L_6 = (p1->f5);
		G_B4_0 = p4;
		if (!L_6)
		{
			G_B5_0 = p4;
			goto IL_0051;
		}
	}
	{
		t42 * L_7 = (p1->f0);
		t1505 * L_8 = (t1505 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1505_TI));
		m8079(L_8, &m8079_MI);
		t733 * L_9 = (t733 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t733_TI));
		m8143(L_9, L_7, L_8, &m8143_MI);
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		goto IL_0052;
	}

IL_0051:
	{
		G_B6_0 = ((t733 *)(NULL));
		G_B6_1 = G_B5_0;
	}

IL_0052:
	{
		*((t29 **)(G_B6_1)) = (t29 *)G_B6_0;
		t446* L_10 = (p1->f2);
		if (!L_10)
		{
			goto IL_008e;
		}
	}
	{
		V_0 = 0;
		goto IL_0083;
	}

IL_005f:
	{
		t537* L_11 = (p1->f1);
		int32_t L_12 = V_0;
		t446* L_13 = (p1->f2);
		int32_t L_14 = V_0;
		m8073(__this, p0, (*((t29 **)p3)), p2, (*((t733 **)p4)), (*(t42 **)(t42 **)SZArrayLdElema(L_11, L_12)), (*(t7**)(t7**)SZArrayLdElema(L_13, L_14)), (t296 *)NULL, (t841*)(t841*)NULL, &m8073_MI);
		V_0 = ((int32_t)(V_0+1));
	}

IL_0083:
	{
		int32_t L_15 = (p1->f4);
		if ((((int32_t)V_0) < ((int32_t)L_15)))
		{
			goto IL_005f;
		}
	}
	{
		goto IL_00cb;
	}

IL_008e:
	{
		V_1 = 0;
		goto IL_00c2;
	}

IL_0092:
	{
		t537* L_16 = (p1->f1);
		int32_t L_17 = V_1;
		t1501* L_18 = (p1->f3);
		int32_t L_19 = V_1;
		t7* L_20 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, (*(t296 **)(t296 **)SZArrayLdElema(L_18, L_19)));
		t1501* L_21 = (p1->f3);
		int32_t L_22 = V_1;
		m8073(__this, p0, (*((t29 **)p3)), p2, (*((t733 **)p4)), (*(t42 **)(t42 **)SZArrayLdElema(L_16, L_17)), L_20, (*(t296 **)(t296 **)SZArrayLdElema(L_21, L_22)), (t841*)(t841*)NULL, &m8073_MI);
		V_1 = ((int32_t)(V_1+1));
	}

IL_00c2:
	{
		int32_t L_23 = (p1->f4);
		if ((((int32_t)V_1) < ((int32_t)L_23)))
		{
			goto IL_0092;
		}
	}

IL_00cb:
	{
		return;
	}
}
 void m8063 (t1503 * __this, int64_t p0, t29 * p1, t733 * p2, int64_t p3, t296 * p4, t841* p5, MethodInfo* method){
	{
		if (p3)
		{
			goto IL_0009;
		}
	}
	{
		p5 = (t841*)NULL;
	}

IL_0009:
	{
		t42 * L_0 = m1430(p1, &m1430_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		if (p3)
		{
			goto IL_002e;
		}
	}

IL_001a:
	{
		t1504 * L_2 = (__this->f4);
		m8101(L_2, p1, p0, p2, (((int64_t)0)), (t296 *)NULL, (t841*)(t841*)NULL, &m8101_MI);
		goto IL_0056;
	}

IL_002e:
	{
		if (!p5)
		{
			goto IL_0042;
		}
	}
	{
		t29 * L_3 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4006_MI, p5);
		p5 = ((t841*)Castclass(L_3, InitializedTypeInfo(&t841_TI)));
	}

IL_0042:
	{
		t1504 * L_4 = (__this->f4);
		m8101(L_4, p1, p0, p2, p3, p4, p5, &m8101_MI);
	}

IL_0056:
	{
		return;
	}
}
 void m8064 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method){
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((int64_t*)(p1)) = (int64_t)(((uint64_t)L_0));
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		*((t29 **)(p2)) = (t29 *)L_1;
		return;
	}
}
 void m8065 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method){
	int32_t V_0 = 0;
	bool V_1 = false;
	t841* V_2 = {0};
	int32_t V_3 = 0;
	uint8_t V_4 = {0};
	t42 * V_5 = {0};
	t20 * V_6 = {0};
	t841* V_7 = {0};
	int32_t V_8 = 0;
	bool V_9 = false;
	int32_t V_10 = 0;
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((int64_t*)(p1)) = (int64_t)(((uint64_t)L_0));
		VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		V_0 = L_1;
		V_1 = 0;
		V_2 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_3 = 0;
		goto IL_0038;
	}

IL_0024:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		*((int32_t*)(int32_t*)SZArrayLdElema(V_2, V_3)) = (int32_t)L_2;
		int32_t L_3 = V_3;
		if ((*(int32_t*)(int32_t*)SZArrayLdElema(V_2, L_3)))
		{
			goto IL_0034;
		}
	}
	{
		V_1 = 1;
	}

IL_0034:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0038:
	{
		if ((((int32_t)V_3) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		uint8_t L_4 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		V_4 = L_4;
		t42 * L_5 = m8077(__this, p0, V_4, &m8077_MI);
		V_5 = L_5;
		t20 * L_6 = m5939(NULL, V_5, V_2, &m5939_MI);
		V_6 = L_6;
		if (!V_1)
		{
			goto IL_0061;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_6;
		return;
	}

IL_0061:
	{
		V_7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_8 = ((int32_t)(V_0-1));
		goto IL_0084;
	}

IL_0070:
	{
		int32_t L_7 = m5913(V_6, V_8, &m5913_MI);
		*((int32_t*)(int32_t*)SZArrayLdElema(V_7, V_8)) = (int32_t)L_7;
		V_8 = ((int32_t)(V_8-1));
	}

IL_0084:
	{
		if ((((int32_t)V_8) >= ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		V_9 = 0;
		goto IL_00f0;
	}

IL_008e:
	{
		m8073(__this, p0, V_6, (*((int64_t*)p1)), (t733 *)NULL, V_5, (t7*)NULL, (t296 *)NULL, V_7, &m8073_MI);
		int32_t L_8 = m3977(V_6, &m3977_MI);
		V_10 = ((int32_t)(L_8-1));
		goto IL_00eb;
	}

IL_00ad:
	{
		int32_t* L_9 = ((int32_t*)(int32_t*)SZArrayLdElema(V_7, V_10));
		*((int32_t*)(L_9)) = (int32_t)((int32_t)((*((int32_t*)L_9))+1));
		int32_t L_10 = V_10;
		int32_t L_11 = m5924(V_6, V_10, &m5924_MI);
		if ((((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(V_7, L_10))) <= ((int32_t)L_11)))
		{
			goto IL_00e3;
		}
	}
	{
		if ((((int32_t)V_10) <= ((int32_t)0)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_12 = m5913(V_6, V_10, &m5913_MI);
		*((int32_t*)(int32_t*)SZArrayLdElema(V_7, V_10)) = (int32_t)L_12;
		goto IL_00e5;
	}

IL_00e0:
	{
		V_9 = 1;
	}

IL_00e3:
	{
		goto IL_00f0;
	}

IL_00e5:
	{
		V_10 = ((int32_t)(V_10-1));
	}

IL_00eb:
	{
		if ((((int32_t)V_10) >= ((int32_t)0)))
		{
			goto IL_00ad;
		}
	}

IL_00f0:
	{
		if (!V_9)
		{
			goto IL_008e;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_6;
		return;
	}
}
 t29 * m8066 (t1503 * __this, t1304 * p0, MethodInfo* method){
	t42 * V_0 = {0};
	{
		t42 * L_0 = m8077(__this, p0, 0, &m8077_MI);
		V_0 = L_0;
		t29 * L_1 = m8078(NULL, p0, V_0, &m8078_MI);
		return L_1;
	}
}
 void m8067 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method){
	int32_t V_0 = 0;
	t42 * V_1 = {0};
	t771* V_2 = {0};
	int32_t V_3 = 0;
	t781* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t200* V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	t2053* V_10 = {0};
	int32_t V_11 = 0;
	t2054* V_12 = {0};
	int32_t V_13 = 0;
	t1140* V_14 = {0};
	int32_t V_15 = 0;
	t1763* V_16 = {0};
	int32_t V_17 = 0;
	t841* V_18 = {0};
	int32_t V_19 = 0;
	t1139* V_20 = {0};
	int32_t V_21 = 0;
	t1595* V_22 = {0};
	int32_t V_23 = 0;
	t509* V_24 = {0};
	int32_t V_25 = 0;
	t764* V_26 = {0};
	int32_t V_27 = 0;
	t975* V_28 = {0};
	int32_t V_29 = 0;
	t1552* V_30 = {0};
	int32_t V_31 = 0;
	t446* V_32 = {0};
	int32_t V_33 = 0;
	t2055* V_34 = {0};
	int32_t V_35 = 0;
	int32_t V_36 = {0};
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((int64_t*)(p1)) = (int64_t)(((uint64_t)L_0));
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		V_0 = L_1;
		t42 * L_2 = m8077(__this, p0, 0, &m8077_MI);
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		int32_t L_3 = m6015(NULL, V_1, &m6015_MI);
		V_36 = L_3;
		if (((int32_t)(V_36-3)) == 0)
		{
			goto IL_006f;
		}
		if (((int32_t)(V_36-3)) == 1)
		{
			goto IL_00cb;
		}
		if (((int32_t)(V_36-3)) == 2)
		{
			goto IL_0262;
		}
		if (((int32_t)(V_36-3)) == 3)
		{
			goto IL_0093;
		}
		if (((int32_t)(V_36-3)) == 4)
		{
			goto IL_01ae;
		}
		if (((int32_t)(V_36-3)) == 5)
		{
			goto IL_02da;
		}
		if (((int32_t)(V_36-3)) == 6)
		{
			goto IL_01ea;
		}
		if (((int32_t)(V_36-3)) == 7)
		{
			goto IL_0316;
		}
		if (((int32_t)(V_36-3)) == 8)
		{
			goto IL_0226;
		}
		if (((int32_t)(V_36-3)) == 9)
		{
			goto IL_0352;
		}
		if (((int32_t)(V_36-3)) == 10)
		{
			goto IL_029e;
		}
		if (((int32_t)(V_36-3)) == 11)
		{
			goto IL_0172;
		}
		if (((int32_t)(V_36-3)) == 12)
		{
			goto IL_013d;
		}
		if (((int32_t)(V_36-3)) == 13)
		{
			goto IL_0103;
		}
		if (((int32_t)(V_36-3)) == 14)
		{
			goto IL_03b7;
		}
		if (((int32_t)(V_36-3)) == 15)
		{
			goto IL_038e;
		}
	}
	{
		goto IL_03b7;
	}

IL_006f:
	{
		V_2 = ((t771*)SZArrayNew(InitializedTypeInfo(&t771_TI), V_0));
		V_3 = 0;
		goto IL_0087;
	}

IL_007a:
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6981_MI, p0);
		*((bool*)(bool*)SZArrayLdElema(V_2, V_3)) = (bool)L_4;
		V_3 = ((int32_t)(V_3+1));
	}

IL_0087:
	{
		if ((((int32_t)V_3) < ((int32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_2;
		goto IL_0413;
	}

IL_0093:
	{
		V_4 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), V_0));
		V_5 = 0;
		goto IL_00bd;
	}

IL_00a0:
	{
		int32_t L_5 = (int32_t)VirtFuncInvoker3< int32_t, t781*, int32_t, int32_t >::Invoke(&m6977_MI, p0, V_4, V_5, ((int32_t)(V_0-V_5)));
		V_6 = L_5;
		if (V_6)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00c2;
	}

IL_00b6:
	{
		V_5 = ((int32_t)(V_5+V_6));
	}

IL_00bd:
	{
		if ((((int32_t)V_5) < ((int32_t)V_0)))
		{
			goto IL_00a0;
		}
	}

IL_00c2:
	{
		*((t29 **)(p2)) = (t29 *)V_4;
		goto IL_0413;
	}

IL_00cb:
	{
		V_7 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), V_0));
		V_8 = 0;
		goto IL_00f5;
	}

IL_00d8:
	{
		int32_t L_6 = (int32_t)VirtFuncInvoker3< int32_t, t200*, int32_t, int32_t >::Invoke(&m6978_MI, p0, V_7, V_8, ((int32_t)(V_0-V_8)));
		V_9 = L_6;
		if (V_9)
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_00fa;
	}

IL_00ee:
	{
		V_8 = ((int32_t)(V_8+V_9));
	}

IL_00f5:
	{
		if ((((int32_t)V_8) < ((int32_t)V_0)))
		{
			goto IL_00d8;
		}
	}

IL_00fa:
	{
		*((t29 **)(p2)) = (t29 *)V_7;
		goto IL_0413;
	}

IL_0103:
	{
		V_10 = ((t2053*)SZArrayNew(InitializedTypeInfo(&t2053_TI), V_0));
		V_11 = 0;
		goto IL_012f;
	}

IL_0110:
	{
		int64_t L_7 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m6988_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_8 = m9153(NULL, L_7, &m9153_MI);
		*((t465 *)(t465 *)SZArrayLdElema(V_10, V_11)) = L_8;
		V_11 = ((int32_t)(V_11+1));
	}

IL_012f:
	{
		if ((((int32_t)V_11) < ((int32_t)V_0)))
		{
			goto IL_0110;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_10;
		goto IL_0413;
	}

IL_013d:
	{
		V_12 = ((t2054*)SZArrayNew(InitializedTypeInfo(&t2054_TI), V_0));
		V_13 = 0;
		goto IL_0164;
	}

IL_014a:
	{
		t1126  L_9 = (t1126 )VirtFuncInvoker0< t1126  >::Invoke(&m6984_MI, p0);
		*((t1126 *)(t1126 *)SZArrayLdElema(V_12, V_13)) = L_9;
		V_13 = ((int32_t)(V_13+1));
	}

IL_0164:
	{
		if ((((int32_t)V_13) < ((int32_t)V_0)))
		{
			goto IL_014a;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_12;
		goto IL_0413;
	}

IL_0172:
	{
		V_14 = ((t1140*)SZArrayNew(InitializedTypeInfo(&t1140_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_018a;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_14, 8, &m8068_MI);
		goto IL_01a5;
	}

IL_018a:
	{
		V_15 = 0;
		goto IL_01a0;
	}

IL_018f:
	{
		double L_10 = (double)VirtFuncInvoker0< double >::Invoke(&m6985_MI, p0);
		*((double*)(double*)SZArrayLdElema(V_14, V_15)) = (double)L_10;
		V_15 = ((int32_t)(V_15+1));
	}

IL_01a0:
	{
		if ((((int32_t)V_15) < ((int32_t)V_0)))
		{
			goto IL_018f;
		}
	}

IL_01a5:
	{
		*((t29 **)(p2)) = (t29 *)V_14;
		goto IL_0413;
	}

IL_01ae:
	{
		V_16 = ((t1763*)SZArrayNew(InitializedTypeInfo(&t1763_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_01c6;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_16, 2, &m8068_MI);
		goto IL_01e1;
	}

IL_01c6:
	{
		V_17 = 0;
		goto IL_01dc;
	}

IL_01cb:
	{
		int16_t L_11 = (int16_t)VirtFuncInvoker0< int16_t >::Invoke(&m6986_MI, p0);
		*((int16_t*)(int16_t*)SZArrayLdElema(V_16, V_17)) = (int16_t)L_11;
		V_17 = ((int32_t)(V_17+1));
	}

IL_01dc:
	{
		if ((((int32_t)V_17) < ((int32_t)V_0)))
		{
			goto IL_01cb;
		}
	}

IL_01e1:
	{
		*((t29 **)(p2)) = (t29 *)V_16;
		goto IL_0413;
	}

IL_01ea:
	{
		V_18 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_0202;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_18, 4, &m8068_MI);
		goto IL_021d;
	}

IL_0202:
	{
		V_19 = 0;
		goto IL_0218;
	}

IL_0207:
	{
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		*((int32_t*)(int32_t*)SZArrayLdElema(V_18, V_19)) = (int32_t)L_12;
		V_19 = ((int32_t)(V_19+1));
	}

IL_0218:
	{
		if ((((int32_t)V_19) < ((int32_t)V_0)))
		{
			goto IL_0207;
		}
	}

IL_021d:
	{
		*((t29 **)(p2)) = (t29 *)V_18;
		goto IL_0413;
	}

IL_0226:
	{
		V_20 = ((t1139*)SZArrayNew(InitializedTypeInfo(&t1139_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_023e;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_20, 8, &m8068_MI);
		goto IL_0259;
	}

IL_023e:
	{
		V_21 = 0;
		goto IL_0254;
	}

IL_0243:
	{
		int64_t L_13 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m6988_MI, p0);
		*((int64_t*)(int64_t*)SZArrayLdElema(V_20, V_21)) = (int64_t)L_13;
		V_21 = ((int32_t)(V_21+1));
	}

IL_0254:
	{
		if ((((int32_t)V_21) < ((int32_t)V_0)))
		{
			goto IL_0243;
		}
	}

IL_0259:
	{
		*((t29 **)(p2)) = (t29 *)V_20;
		goto IL_0413;
	}

IL_0262:
	{
		V_22 = ((t1595*)SZArrayNew(InitializedTypeInfo(&t1595_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_027a;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_22, 1, &m8068_MI);
		goto IL_0295;
	}

IL_027a:
	{
		V_23 = 0;
		goto IL_0290;
	}

IL_027f:
	{
		int8_t L_14 = (int8_t)VirtFuncInvoker0< int8_t >::Invoke(&m6989_MI, p0);
		*((int8_t*)(int8_t*)SZArrayLdElema(V_22, V_23)) = (int8_t)L_14;
		V_23 = ((int32_t)(V_23+1));
	}

IL_0290:
	{
		if ((((int32_t)V_23) < ((int32_t)V_0)))
		{
			goto IL_027f;
		}
	}

IL_0295:
	{
		*((t29 **)(p2)) = (t29 *)V_22;
		goto IL_0413;
	}

IL_029e:
	{
		V_24 = ((t509*)SZArrayNew(InitializedTypeInfo(&t509_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_02b6;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_24, 4, &m8068_MI);
		goto IL_02d1;
	}

IL_02b6:
	{
		V_25 = 0;
		goto IL_02cc;
	}

IL_02bb:
	{
		float L_15 = (float)VirtFuncInvoker0< float >::Invoke(&m6991_MI, p0);
		*((float*)(float*)SZArrayLdElema(V_24, V_25)) = (float)L_15;
		V_25 = ((int32_t)(V_25+1));
	}

IL_02cc:
	{
		if ((((int32_t)V_25) < ((int32_t)V_0)))
		{
			goto IL_02bb;
		}
	}

IL_02d1:
	{
		*((t29 **)(p2)) = (t29 *)V_24;
		goto IL_0413;
	}

IL_02da:
	{
		V_26 = ((t764*)SZArrayNew(InitializedTypeInfo(&t764_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_02f2;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_26, 2, &m8068_MI);
		goto IL_030d;
	}

IL_02f2:
	{
		V_27 = 0;
		goto IL_0308;
	}

IL_02f7:
	{
		uint16_t L_16 = (uint16_t)VirtFuncInvoker0< uint16_t >::Invoke(&m6992_MI, p0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_26, V_27)) = (uint16_t)L_16;
		V_27 = ((int32_t)(V_27+1));
	}

IL_0308:
	{
		if ((((int32_t)V_27) < ((int32_t)V_0)))
		{
			goto IL_02f7;
		}
	}

IL_030d:
	{
		*((t29 **)(p2)) = (t29 *)V_26;
		goto IL_0413;
	}

IL_0316:
	{
		V_28 = ((t975*)SZArrayNew(InitializedTypeInfo(&t975_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_032e;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_28, 4, &m8068_MI);
		goto IL_0349;
	}

IL_032e:
	{
		V_29 = 0;
		goto IL_0344;
	}

IL_0333:
	{
		uint32_t L_17 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(V_28, V_29)) = (uint32_t)L_17;
		V_29 = ((int32_t)(V_29+1));
	}

IL_0344:
	{
		if ((((int32_t)V_29) < ((int32_t)V_0)))
		{
			goto IL_0333;
		}
	}

IL_0349:
	{
		*((t29 **)(p2)) = (t29 *)V_28;
		goto IL_0413;
	}

IL_0352:
	{
		V_30 = ((t1552*)SZArrayNew(InitializedTypeInfo(&t1552_TI), V_0));
		if ((((int32_t)V_0) <= ((int32_t)2)))
		{
			goto IL_036a;
		}
	}
	{
		m8068(__this, p0, (t20 *)(t20 *)V_30, 8, &m8068_MI);
		goto IL_0385;
	}

IL_036a:
	{
		V_31 = 0;
		goto IL_0380;
	}

IL_036f:
	{
		uint64_t L_18 = (uint64_t)VirtFuncInvoker0< uint64_t >::Invoke(&m6994_MI, p0);
		*((uint64_t*)(uint64_t*)SZArrayLdElema(V_30, V_31)) = (uint64_t)L_18;
		V_31 = ((int32_t)(V_31+1));
	}

IL_0380:
	{
		if ((((int32_t)V_31) < ((int32_t)V_0)))
		{
			goto IL_036f;
		}
	}

IL_0385:
	{
		*((t29 **)(p2)) = (t29 *)V_30;
		goto IL_0413;
	}

IL_038e:
	{
		V_32 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), V_0));
		V_33 = 0;
		goto IL_03ac;
	}

IL_039b:
	{
		t7* L_19 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		ArrayElementTypeCheck (V_32, L_19);
		*((t7**)(t7**)SZArrayLdElema(V_32, V_33)) = (t7*)L_19;
		V_33 = ((int32_t)(V_33+1));
	}

IL_03ac:
	{
		if ((((int32_t)V_33) < ((int32_t)V_0)))
		{
			goto IL_039b;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_32;
		goto IL_0413;
	}

IL_03b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_20 = m1554(NULL, LoadTypeToken(&t816_0_0_0), &m1554_MI);
		if ((((t42 *)V_1) != ((t42 *)L_20)))
		{
			goto IL_03fb;
		}
	}
	{
		V_34 = ((t2055*)SZArrayNew(InitializedTypeInfo(&t2055_TI), V_0));
		V_35 = 0;
		goto IL_03f0;
	}

IL_03d1:
	{
		int64_t L_21 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m6988_MI, p0);
		t816  L_22 = {0};
		m9520(&L_22, L_21, &m9520_MI);
		*((t816 *)(t816 *)SZArrayLdElema(V_34, V_35)) = L_22;
		V_35 = ((int32_t)(V_35+1));
	}

IL_03f0:
	{
		if ((((int32_t)V_35) < ((int32_t)V_0)))
		{
			goto IL_03d1;
		}
	}
	{
		*((t29 **)(p2)) = (t29 *)V_34;
		goto IL_0411;
	}

IL_03fb:
	{
		t7* L_23 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, V_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_24 = m66(NULL, (t7*) &_stringLiteral1640, L_23, &m66_MI);
		t345 * L_25 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_25, L_24, &m3988_MI);
		il2cpp_codegen_raise_exception(L_25);
	}

IL_0411:
	{
		goto IL_0413;
	}

IL_0413:
	{
		return;
	}
}
 void m8068 (t1503 * __this, t1304 * p0, t20 * p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	t1503 * G_B5_0 = {0};
	t1503 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	t1503 * G_B6_1 = {0};
	int32_t G_B11_0 = 0;
	{
		int32_t L_0 = m8878(NULL, p1, &m8878_MI);
		V_0 = L_0;
		t781* L_1 = (__this->f10);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		t781* L_2 = (__this->f10);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_2)->max_length))))))
		{
			goto IL_0047;
		}
	}
	{
		t781* L_3 = (__this->f10);
		int32_t L_4 = (__this->f11);
		if ((((int32_t)(((int32_t)(((t20 *)L_3)->max_length)))) == ((int32_t)L_4)))
		{
			goto IL_0047;
		}
	}

IL_002a:
	{
		int32_t L_5 = (__this->f11);
		G_B4_0 = __this;
		if ((((int32_t)V_0) > ((int32_t)L_5)))
		{
			G_B5_0 = __this;
			goto IL_0037;
		}
	}
	{
		G_B6_0 = V_0;
		G_B6_1 = G_B4_0;
		goto IL_003d;
	}

IL_0037:
	{
		int32_t L_6 = (__this->f11);
		G_B6_0 = L_6;
		G_B6_1 = G_B5_0;
	}

IL_003d:
	{
		G_B6_1->f10 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), G_B6_0));
	}

IL_0047:
	{
		V_1 = 0;
		goto IL_00b4;
	}

IL_004b:
	{
		t781* L_7 = (__this->f10);
		if ((((int32_t)V_0) >= ((int32_t)(((int32_t)(((t20 *)L_7)->max_length))))))
		{
			goto IL_0059;
		}
	}
	{
		G_B11_0 = V_0;
		goto IL_0061;
	}

IL_0059:
	{
		t781* L_8 = (__this->f10);
		G_B11_0 = (((int32_t)(((t20 *)L_8)->max_length)));
	}

IL_0061:
	{
		V_2 = G_B11_0;
		V_3 = 0;
	}

IL_0064:
	{
		t781* L_9 = (__this->f10);
		int32_t L_10 = (int32_t)VirtFuncInvoker3< int32_t, t781*, int32_t, int32_t >::Invoke(&m6977_MI, p0, L_9, V_3, ((int32_t)(V_2-V_3)));
		V_4 = L_10;
		if (V_4)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0085;
	}

IL_007c:
	{
		V_3 = ((int32_t)(V_3+V_4));
		if ((((int32_t)V_3) < ((int32_t)V_2)))
		{
			goto IL_0064;
		}
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t922_TI));
		if ((((t922_SFs*)InitializedTypeInfo(&t922_TI)->static_fields)->f1))
		{
			goto IL_009d;
		}
	}
	{
		if ((((int32_t)p2) <= ((int32_t)1)))
		{
			goto IL_009d;
		}
	}
	{
		t781* L_11 = (__this->f10);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		m8036(NULL, L_11, V_2, p2, &m8036_MI);
	}

IL_009d:
	{
		t781* L_12 = (__this->f10);
		m4055(NULL, (t20 *)(t20 *)L_12, 0, p1, V_1, V_2, &m4055_MI);
		V_0 = ((int32_t)(V_0-V_2));
		V_1 = ((int32_t)(V_1+V_2));
	}

IL_00b4:
	{
		if ((((int32_t)V_0) > ((int32_t)0)))
		{
			goto IL_004b;
		}
	}
	{
		return;
	}
}
 void m8069 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		m8071(__this, p0, L_0, p1, p2, &m8071_MI);
		return;
	}
}
 void m8070 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m8071(__this, p0, L_0, p1, p2, &m8071_MI);
		return;
	}
}
 void m8071 (t1503 * __this, t1304 * p0, t42 * p1, int64_t* p2, t29 ** p3, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t20 * V_2 = {0};
	int32_t V_3 = 0;
	{
		uint32_t L_0 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		*((int64_t*)(p2)) = (int64_t)(((uint64_t)L_0));
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), 1));
		t20 * L_2 = m5936(NULL, p1, V_0, &m5936_MI);
		V_2 = L_2;
		V_3 = 0;
		goto IL_003e;
	}

IL_0023:
	{
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, 0)) = (int32_t)V_3;
		m8073(__this, p0, V_2, (*((int64_t*)p2)), (t733 *)NULL, p1, (t7*)NULL, (t296 *)NULL, V_1, &m8073_MI);
		int32_t L_3 = 0;
		V_3 = (*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_3));
		V_3 = ((int32_t)(V_3+1));
	}

IL_003e:
	{
		if ((((int32_t)V_3) < ((int32_t)V_0)))
		{
			goto IL_0023;
		}
	}
	{
		*((t29 **)(p3)) = (t29 *)V_2;
		return;
	}
}
 t1500 * m8072 (t1503 * __this, t1304 * p0, bool p1, bool p2, MethodInfo* method){
	t1500 * V_0 = {0};
	t7* V_1 = {0};
	int32_t V_2 = 0;
	t537* V_3 = {0};
	t446* V_4 = {0};
	int32_t V_5 = 0;
	t2056* V_6 = {0};
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int64_t V_9 = 0;
	t29 * V_10 = {0};
	t29 * V_11 = {0};
	int32_t V_12 = 0;
	t1144 * V_13 = {0};
	t7* V_14 = {0};
	int32_t V_15 = 0;
	t7* V_16 = {0};
	t42 * V_17 = {0};
	{
		t1500 * L_0 = (t1500 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1500_TI));
		m8050(L_0, &m8050_MI);
		V_0 = L_0;
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		V_1 = L_1;
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		V_2 = L_2;
		V_3 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), V_2));
		V_4 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), V_2));
		V_5 = 0;
		goto IL_0039;
	}

IL_0028:
	{
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		ArrayElementTypeCheck (V_4, L_3);
		*((t7**)(t7**)SZArrayLdElema(V_4, V_5)) = (t7*)L_3;
		V_5 = ((int32_t)(V_5+1));
	}

IL_0039:
	{
		if ((((int32_t)V_5) < ((int32_t)V_2)))
		{
			goto IL_0028;
		}
	}
	{
		if (!p2)
		{
			goto IL_0084;
		}
	}
	{
		V_6 = ((t2056*)SZArrayNew(InitializedTypeInfo(&t2056_TI), V_2));
		V_7 = 0;
		goto IL_005f;
	}

IL_004e:
	{
		uint8_t L_4 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(V_6, V_7)) = (uint8_t)L_4;
		V_7 = ((int32_t)(V_7+1));
	}

IL_005f:
	{
		if ((((int32_t)V_7) < ((int32_t)V_2)))
		{
			goto IL_004e;
		}
	}
	{
		V_8 = 0;
		goto IL_007f;
	}

IL_0069:
	{
		int32_t L_5 = V_8;
		t42 * L_6 = m8077(__this, p0, (*(uint8_t*)(uint8_t*)SZArrayLdElema(V_6, L_5)), &m8077_MI);
		ArrayElementTypeCheck (V_3, L_6);
		*((t42 **)(t42 **)SZArrayLdElema(V_3, V_8)) = (t42 *)L_6;
		V_8 = ((int32_t)(V_8+1));
	}

IL_007f:
	{
		if ((((int32_t)V_8) < ((int32_t)V_2)))
		{
			goto IL_0069;
		}
	}

IL_0084:
	{
		if (p1)
		{
			goto IL_00a1;
		}
	}
	{
		uint32_t L_7 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		V_9 = (((uint64_t)L_7));
		t42 * L_8 = m8076(__this, V_9, V_1, &m8076_MI);
		V_0->f0 = L_8;
		goto IL_00ae;
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_9 = m2978(NULL, V_1, 1, &m2978_MI);
		V_0->f0 = L_9;
	}

IL_00ae:
	{
		V_0->f1 = V_3;
		V_0->f2 = V_4;
		V_0->f4 = (((int32_t)(((t20 *)V_4)->max_length)));
		t29 * L_10 = (__this->f0);
		if (!L_10)
		{
			goto IL_00f8;
		}
	}
	{
		t29 * L_11 = (__this->f0);
		t42 * L_12 = (V_0->f0);
		t735  L_13 = (__this->f1);
		t29 * L_14 = (t29 *)InterfaceFuncInvoker3< t29 *, t42 *, t735 , t29 ** >::Invoke(&m10205_MI, L_11, L_12, L_13, (&V_10));
		V_11 = L_14;
		V_0->f5 = ((((int32_t)((((t29 *)V_11) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00f8:
	{
		bool L_15 = (V_0->f5);
		if (L_15)
		{
			goto IL_0233;
		}
	}
	{
		t42 * L_16 = (V_0->f0);
		bool L_17 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6003_MI, L_16);
		if (L_17)
		{
			goto IL_011b;
		}
	}
	{
		t917 * L_18 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_18, (t7*) &_stringLiteral1641, &m3986_MI);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_011b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_19 = m1554(NULL, LoadTypeToken(&t374_0_0_0), &m1554_MI);
		t42 * L_20 = (V_0->f0);
		bool L_21 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, L_19, L_20);
		V_0->f5 = L_21;
		bool L_22 = (V_0->f5);
		if (L_22)
		{
			goto IL_0233;
		}
	}
	{
		V_0->f3 = ((t1501*)SZArrayNew(InitializedTypeInfo(&t1501_TI), V_2));
		V_12 = 0;
		goto IL_0224;
	}

IL_0155:
	{
		V_13 = (t1144 *)NULL;
		int32_t L_23 = V_12;
		V_14 = (*(t7**)(t7**)SZArrayLdElema(V_4, L_23));
		int32_t L_24 = m1810(V_14, ((int32_t)43), &m1810_MI);
		V_15 = L_24;
		if ((((int32_t)V_15) == ((int32_t)(-1))))
		{
			goto IL_01cb;
		}
	}
	{
		int32_t L_25 = V_12;
		t7* L_26 = m1742((*(t7**)(t7**)SZArrayLdElema(V_4, L_25)), 0, V_15, &m1742_MI);
		V_16 = L_26;
		int32_t L_27 = V_12;
		t7* L_28 = m1770((*(t7**)(t7**)SZArrayLdElema(V_4, L_27)), ((int32_t)(V_15+1)), &m1770_MI);
		V_14 = L_28;
		t42 * L_29 = (V_0->f0);
		t42 * L_30 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, L_29);
		V_17 = L_30;
		goto IL_01c5;
	}

IL_019d:
	{
		t7* L_31 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, V_17);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_32 = m1713(NULL, L_31, V_16, &m1713_MI);
		if (!L_32)
		{
			goto IL_01bc;
		}
	}
	{
		t1144 * L_33 = (t1144 *)VirtFuncInvoker2< t1144 *, t7*, int32_t >::Invoke(&m9852_MI, V_17, V_14, ((int32_t)52));
		V_13 = L_33;
		goto IL_01c9;
	}

IL_01bc:
	{
		t42 * L_34 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_17);
		V_17 = L_34;
	}

IL_01c5:
	{
		if (V_17)
		{
			goto IL_019d;
		}
	}

IL_01c9:
	{
		goto IL_01dc;
	}

IL_01cb:
	{
		t42 * L_35 = (V_0->f0);
		t1144 * L_36 = (t1144 *)VirtFuncInvoker2< t1144 *, t7*, int32_t >::Invoke(&m9852_MI, L_35, V_14, ((int32_t)52));
		V_13 = L_36;
	}

IL_01dc:
	{
		if (V_13)
		{
			goto IL_0205;
		}
	}
	{
		int32_t L_37 = V_12;
		t42 * L_38 = (V_0->f0);
		t7* L_39 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_38);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_40 = m2926(NULL, (t7*) &_stringLiteral1642, (*(t7**)(t7**)SZArrayLdElema(V_4, L_37)), (t7*) &_stringLiteral1643, L_39, &m2926_MI);
		t917 * L_41 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_41, L_40, &m3986_MI);
		il2cpp_codegen_raise_exception(L_41);
	}

IL_0205:
	{
		t1501* L_42 = (V_0->f3);
		ArrayElementTypeCheck (L_42, V_13);
		*((t296 **)(t296 **)SZArrayLdElema(L_42, V_12)) = (t296 *)V_13;
		if (p2)
		{
			goto IL_021e;
		}
	}
	{
		t42 * L_43 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m10154_MI, V_13);
		ArrayElementTypeCheck (V_3, L_43);
		*((t42 **)(t42 **)SZArrayLdElema(V_3, V_12)) = (t42 *)L_43;
	}

IL_021e:
	{
		V_12 = ((int32_t)(V_12+1));
	}

IL_0224:
	{
		if ((((int32_t)V_12) < ((int32_t)V_2)))
		{
			goto IL_0155;
		}
	}
	{
		V_0->f2 = (t446*)NULL;
	}

IL_0233:
	{
		t719 * L_44 = (__this->f6);
		t42 * L_45 = (V_0->f0);
		bool L_46 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m4030_MI, L_44, L_45);
		if (L_46)
		{
			goto IL_0258;
		}
	}
	{
		t719 * L_47 = (__this->f6);
		t42 * L_48 = (V_0->f0);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, L_47, L_48, V_0);
	}

IL_0258:
	{
		return V_0;
	}
}
 void m8073 (t1503 * __this, t1304 * p0, t29 * p1, int64_t p2, t733 * p3, t42 * p4, t7* p5, t296 * p6, t841* p7, MethodInfo* method){
	t29 * V_0 = {0};
	uint8_t V_1 = {0};
	int64_t V_2 = 0;
	int64_t V_3 = 0;
	t733 * V_4 = {0};
	bool V_5 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		bool L_0 = m8034(NULL, p4, &m8034_MI);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		t29 * L_1 = m8078(NULL, p0, p4, &m8078_MI);
		V_0 = L_1;
		m8074(__this, p1, p5, p6, p3, V_0, p4, p7, &m8074_MI);
		return;
	}

IL_0025:
	{
		uint8_t L_2 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		V_1 = L_2;
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)9))))
		{
			goto IL_004b;
		}
	}
	{
		uint32_t L_3 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		V_2 = (((uint64_t)L_3));
		m8075(__this, p2, V_2, p1, p3, p5, p6, p7, &m8075_MI);
		return;
	}

IL_004b:
	{
		m8058(__this, V_1, p0, (&V_3), (&V_0), (&V_4), &m8058_MI);
		V_5 = 0;
		if (!V_3)
		{
			goto IL_00aa;
		}
	}
	{
		t42 * L_4 = m1430(V_0, &m1430_MI);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_4);
		if (!L_5)
		{
			goto IL_0080;
		}
	}
	{
		m8075(__this, p2, V_3, p1, p3, p5, p6, p7, &m8075_MI);
		V_5 = 1;
	}

IL_0080:
	{
		if (p3)
		{
			goto IL_009c;
		}
	}
	{
		if (((t20 *)IsInst(p1, InitializedTypeInfo(&t20_TI))))
		{
			goto IL_009c;
		}
	}
	{
		m8063(__this, V_3, V_0, V_4, p2, p6, (t841*)(t841*)NULL, &m8063_MI);
		goto IL_00aa;
	}

IL_009c:
	{
		m8063(__this, V_3, V_0, V_4, p2, (t296 *)NULL, p7, &m8063_MI);
	}

IL_00aa:
	{
		if (V_5)
		{
			goto IL_00c0;
		}
	}
	{
		m8074(__this, p1, p5, p6, p3, V_0, p4, p7, &m8074_MI);
	}

IL_00c0:
	{
		return;
	}
}
 void m8074 (t1503 * __this, t29 * p0, t7* p1, t296 * p2, t733 * p3, t29 * p4, t42 * p5, t841* p6, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if (!((t29 *)IsInst(p4, InitializedTypeInfo(&t2024_TI))))
		{
			goto IL_001f;
		}
	}
	{
		t735  L_0 = (__this->f1);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker1< t29 *, t735  >::Invoke(&m10211_MI, ((t29 *)Castclass(p4, InitializedTypeInfo(&t2024_TI))), L_0);
		p4 = L_1;
	}

IL_001f:
	{
		if (!((t20 *)IsInst(p0, InitializedTypeInfo(&t20_TI))))
		{
			goto IL_005f;
		}
	}
	{
		if (!((t1502 *)IsInst(p4, InitializedTypeInfo(&t1502_TI))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_2 = (((t1502 *)Castclass(p4, InitializedTypeInfo(&t1502_TI)))->f0);
		V_0 = L_2;
		int32_t* L_3 = ((int32_t*)(int32_t*)SZArrayLdElema(p6, 0));
		*((int32_t*)(L_3)) = (int32_t)((int32_t)((*((int32_t*)L_3))+((int32_t)(V_0-1))));
		goto IL_005d;
	}

IL_004e:
	{
		m5915(((t20 *)Castclass(p0, InitializedTypeInfo(&t20_TI))), p4, p6, &m5915_MI);
	}

IL_005d:
	{
		goto IL_0098;
	}

IL_005f:
	{
		if (!p3)
		{
			goto IL_0071;
		}
	}
	{
		m3982(p3, p1, p4, p5, &m3982_MI);
		goto IL_0098;
	}

IL_0071:
	{
		if (!((t1144 *)IsInst(p2, InitializedTypeInfo(&t1144_TI))))
		{
			goto IL_0089;
		}
	}
	{
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m7529_MI, ((t1144 *)Castclass(p2, InitializedTypeInfo(&t1144_TI))), p0, p4);
		goto IL_0098;
	}

IL_0089:
	{
		VirtActionInvoker3< t29 *, t29 *, t316* >::Invoke(&m7714_MI, ((t1146 *)Castclass(p2, InitializedTypeInfo(&t1146_TI))), p0, p4, (t316*)(t316*)NULL);
	}

IL_0098:
	{
		return;
	}
}
 void m8075 (t1503 * __this, int64_t p0, int64_t p1, t29 * p2, t733 * p3, t7* p4, t296 * p5, t841* p6, MethodInfo* method){
	{
		if (!p3)
		{
			goto IL_0015;
		}
	}
	{
		t1504 * L_0 = (__this->f4);
		VirtActionInvoker3< int64_t, t7*, int64_t >::Invoke(&m8098_MI, L_0, p0, p4, p1);
		goto IL_0061;
	}

IL_0015:
	{
		if (!((t20 *)IsInst(p2, InitializedTypeInfo(&t20_TI))))
		{
			goto IL_0052;
		}
	}
	{
		if ((((uint32_t)(((int32_t)(((t20 *)p6)->max_length)))) != ((uint32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		t1504 * L_1 = (__this->f4);
		int32_t L_2 = 0;
		VirtActionInvoker3< int64_t, int32_t, int64_t >::Invoke(&m8096_MI, L_1, p0, (*(int32_t*)(int32_t*)SZArrayLdElema(p6, L_2)), p1);
		goto IL_0050;
	}

IL_0037:
	{
		t1504 * L_3 = (__this->f4);
		t29 * L_4 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4006_MI, p6);
		VirtActionInvoker3< int64_t, t841*, int64_t >::Invoke(&m8097_MI, L_3, p0, ((t841*)Castclass(L_4, InitializedTypeInfo(&t841_TI))), p1);
	}

IL_0050:
	{
		goto IL_0061;
	}

IL_0052:
	{
		t1504 * L_5 = (__this->f4);
		VirtActionInvoker3< int64_t, t296 *, int64_t >::Invoke(&m8099_MI, L_5, p0, p5, p1);
	}

IL_0061:
	{
		return;
	}
}
 t42 * m8076 (t1503 * __this, int64_t p0, t7* p1, MethodInfo* method){
	t42 * V_0 = {0};
	t7* V_1 = {0};
	t929 * V_2 = {0};
	{
		t719 * L_0 = (__this->f5);
		int64_t L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t919_TI), &L_1);
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_0, L_2);
		V_1 = ((t7*)Castclass(L_3, (&t7_TI)));
		t1494 * L_4 = (__this->f2);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		t1494 * L_5 = (__this->f2);
		t42 * L_6 = (t42 *)VirtFuncInvoker2< t42 *, t7*, t7* >::Invoke(&m10212_MI, L_5, V_1, p1);
		V_0 = L_6;
		if (!V_0)
		{
			goto IL_0032;
		}
	}
	{
		return V_0;
	}

IL_0032:
	{
		t929 * L_7 = m7473(NULL, V_1, &m7473_MI);
		V_2 = L_7;
		t42 * L_8 = (t42 *)VirtFuncInvoker2< t42 *, t7*, bool >::Invoke(&m7467_MI, V_2, p1, 1);
		V_0 = L_8;
		if (!V_0)
		{
			goto IL_0047;
		}
	}
	{
		return V_0;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = m1685(NULL, (t7*) &_stringLiteral1644, p1, (t7*) &_stringLiteral503, &m1685_MI);
		t917 * L_10 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_10, L_9, &m3986_MI);
		il2cpp_codegen_raise_exception(L_10);
	}
}
 t42 * m8077 (t1503 * __this, t1304 * p0, uint8_t p1, MethodInfo* method){
	t7* V_0 = {0};
	t42 * V_1 = {0};
	t7* V_2 = {0};
	int64_t V_3 = 0;
	t42 * V_4 = {0};
	uint8_t V_5 = {0};
	{
		V_5 = p1;
		if (V_5 == 0)
		{
			goto IL_002f;
		}
		if (V_5 == 1)
		{
			goto IL_003b;
		}
		if (V_5 == 2)
		{
			goto IL_0046;
		}
		if (V_5 == 3)
		{
			goto IL_0051;
		}
		if (V_5 == 4)
		{
			goto IL_00b4;
		}
		if (V_5 == 5)
		{
			goto IL_00cc;
		}
		if (V_5 == 6)
		{
			goto IL_00d7;
		}
		if (V_5 == 7)
		{
			goto IL_00e2;
		}
	}
	{
		goto IL_0106;
	}

IL_002f:
	{
		uint8_t L_0 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		t42 * L_1 = m8035(NULL, L_0, &m8035_MI);
		return L_1;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		return L_2;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		return L_3;
	}

IL_0051:
	{
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		V_0 = L_4;
		t735 * L_5 = &(__this->f1);
		int32_t L_6 = m8159(L_5, &m8159_MI);
		if ((((uint32_t)L_6) != ((uint32_t)((int32_t)16))))
		{
			goto IL_0097;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_7 = m1713(NULL, V_0, (t7*) &_stringLiteral1645, &m1713_MI);
		if (!L_7)
		{
			goto IL_007f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t1660_0_0_0), &m1554_MI);
		return L_8;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_9 = m1713(NULL, V_0, (t7*) &_stringLiteral1646, &m1713_MI);
		if (!L_9)
		{
			goto IL_0097;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_10 = m1554(NULL, LoadTypeToken(&t2057_0_0_0), &m1554_MI);
		return L_10;
	}

IL_0097:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_11 = m6013(NULL, V_0, &m6013_MI);
		V_1 = L_11;
		if (!V_1)
		{
			goto IL_00a3;
		}
	}
	{
		return V_1;
	}

IL_00a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_12 = m1535(NULL, (t7*) &_stringLiteral1647, V_0, &m1535_MI);
		t917 * L_13 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_13, L_12, &m3986_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_00b4:
	{
		t7* L_14 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		V_2 = L_14;
		uint32_t L_15 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		V_3 = (((uint64_t)L_15));
		t42 * L_16 = m8076(__this, V_3, V_2, &m8076_MI);
		return L_16;
	}

IL_00cc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_17 = m1554(NULL, LoadTypeToken(&t316_0_0_0), &m1554_MI);
		return L_17;
	}

IL_00d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_18 = m1554(NULL, LoadTypeToken(&t446_0_0_0), &m1554_MI);
		return L_18;
	}

IL_00e2:
	{
		uint8_t L_19 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1489_TI));
		t42 * L_20 = m8035(NULL, L_19, &m8035_MI);
		V_4 = L_20;
		t7* L_21 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_22 = m66(NULL, L_21, (t7*) &_stringLiteral1648, &m66_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_23 = m6013(NULL, L_22, &m6013_MI);
		return L_23;
	}

IL_0106:
	{
		t345 * L_24 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_24, (t7*) &_stringLiteral1649, &m3988_MI);
		il2cpp_codegen_raise_exception(L_24);
	}
}
 t29 * m8078 (t29 * __this, t1304 * p0, t42 * p1, MethodInfo* method){
	int32_t V_0 = {0};
	{
		if (p1)
		{
			goto IL_0005;
		}
	}
	{
		return NULL;
	}

IL_0005:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		int32_t L_0 = m6015(NULL, p1, &m6015_MI);
		V_0 = L_0;
		if (((int32_t)(V_0-3)) == 0)
		{
			goto IL_0059;
		}
		if (((int32_t)(V_0-3)) == 1)
		{
			goto IL_0071;
		}
		if (((int32_t)(V_0-3)) == 2)
		{
			goto IL_00d4;
		}
		if (((int32_t)(V_0-3)) == 3)
		{
			goto IL_0065;
		}
		if (((int32_t)(V_0-3)) == 4)
		{
			goto IL_00b0;
		}
		if (((int32_t)(V_0-3)) == 5)
		{
			goto IL_00ec;
		}
		if (((int32_t)(V_0-3)) == 6)
		{
			goto IL_00bc;
		}
		if (((int32_t)(V_0-3)) == 7)
		{
			goto IL_00f8;
		}
		if (((int32_t)(V_0-3)) == 8)
		{
			goto IL_00c8;
		}
		if (((int32_t)(V_0-3)) == 9)
		{
			goto IL_0104;
		}
		if (((int32_t)(V_0-3)) == 10)
		{
			goto IL_00e0;
		}
		if (((int32_t)(V_0-3)) == 11)
		{
			goto IL_00a4;
		}
		if (((int32_t)(V_0-3)) == 12)
		{
			goto IL_008e;
		}
		if (((int32_t)(V_0-3)) == 13)
		{
			goto IL_007d;
		}
		if (((int32_t)(V_0-3)) == 14)
		{
			goto IL_0117;
		}
		if (((int32_t)(V_0-3)) == 15)
		{
			goto IL_0110;
		}
	}
	{
		goto IL_0117;
	}

IL_0059:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6981_MI, p0);
		bool L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t40_TI), &L_2);
		return L_3;
	}

IL_0065:
	{
		uint8_t L_4 = (uint8_t)VirtFuncInvoker0< uint8_t >::Invoke(&m6982_MI, p0);
		uint8_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t348_TI), &L_5);
		return L_6;
	}

IL_0071:
	{
		uint16_t L_7 = (uint16_t)VirtFuncInvoker0< uint16_t >::Invoke(&m6983_MI, p0);
		uint16_t L_8 = L_7;
		t29 * L_9 = Box(InitializedTypeInfo(&t194_TI), &L_8);
		return L_9;
	}

IL_007d:
	{
		int64_t L_10 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m6988_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t465_TI));
		t465  L_11 = m9153(NULL, L_10, &m9153_MI);
		t465  L_12 = L_11;
		t29 * L_13 = Box(InitializedTypeInfo(&t465_TI), &L_12);
		return L_13;
	}

IL_008e:
	{
		t7* L_14 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_15 = m4009(NULL, &m4009_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1126_TI));
		t1126  L_16 = m5735(NULL, L_14, L_15, &m5735_MI);
		t1126  L_17 = L_16;
		t29 * L_18 = Box(InitializedTypeInfo(&t1126_TI), &L_17);
		return L_18;
	}

IL_00a4:
	{
		double L_19 = (double)VirtFuncInvoker0< double >::Invoke(&m6985_MI, p0);
		double L_20 = L_19;
		t29 * L_21 = Box(InitializedTypeInfo(&t601_TI), &L_20);
		return L_21;
	}

IL_00b0:
	{
		int16_t L_22 = (int16_t)VirtFuncInvoker0< int16_t >::Invoke(&m6986_MI, p0);
		int16_t L_23 = L_22;
		t29 * L_24 = Box(InitializedTypeInfo(&t372_TI), &L_23);
		return L_24;
	}

IL_00bc:
	{
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m6987_MI, p0);
		int32_t L_26 = L_25;
		t29 * L_27 = Box(InitializedTypeInfo(&t44_TI), &L_26);
		return L_27;
	}

IL_00c8:
	{
		int64_t L_28 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m6988_MI, p0);
		int64_t L_29 = L_28;
		t29 * L_30 = Box(InitializedTypeInfo(&t919_TI), &L_29);
		return L_30;
	}

IL_00d4:
	{
		int8_t L_31 = (int8_t)VirtFuncInvoker0< int8_t >::Invoke(&m6989_MI, p0);
		int8_t L_32 = L_31;
		t29 * L_33 = Box(InitializedTypeInfo(&t297_TI), &L_32);
		return L_33;
	}

IL_00e0:
	{
		float L_34 = (float)VirtFuncInvoker0< float >::Invoke(&m6991_MI, p0);
		float L_35 = L_34;
		t29 * L_36 = Box(InitializedTypeInfo(&t22_TI), &L_35);
		return L_36;
	}

IL_00ec:
	{
		uint16_t L_37 = (uint16_t)VirtFuncInvoker0< uint16_t >::Invoke(&m6992_MI, p0);
		uint16_t L_38 = L_37;
		t29 * L_39 = Box(InitializedTypeInfo(&t626_TI), &L_38);
		return L_39;
	}

IL_00f8:
	{
		uint32_t L_40 = (uint32_t)VirtFuncInvoker0< uint32_t >::Invoke(&m6993_MI, p0);
		uint32_t L_41 = L_40;
		t29 * L_42 = Box(InitializedTypeInfo(&t344_TI), &L_41);
		return L_42;
	}

IL_0104:
	{
		uint64_t L_43 = (uint64_t)VirtFuncInvoker0< uint64_t >::Invoke(&m6994_MI, p0);
		uint64_t L_44 = L_43;
		t29 * L_45 = Box(InitializedTypeInfo(&t1089_TI), &L_44);
		return L_45;
	}

IL_0110:
	{
		t7* L_46 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6990_MI, p0);
		return L_46;
	}

IL_0117:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_47 = m1554(NULL, LoadTypeToken(&t816_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_47)))
		{
			goto IL_0135;
		}
	}
	{
		int64_t L_48 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m6988_MI, p0);
		t816  L_49 = {0};
		m9520(&L_49, L_48, &m9520_MI);
		t816  L_50 = L_49;
		t29 * L_51 = Box(InitializedTypeInfo(&t816_TI), &L_50);
		return L_51;
	}

IL_0135:
	{
		t7* L_52 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, p1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_53 = m66(NULL, (t7*) &_stringLiteral1640, L_52, &m66_MI);
		t345 * L_54 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_54, L_53, &m3988_MI);
		il2cpp_codegen_raise_exception(L_54);
	}
}
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
extern Il2CppType t1458_0_0_1;
FieldInfo t1503_f0_FieldInfo = 
{
	"_surrogateSelector", &t1458_0_0_1, &t1503_TI, offsetof(t1503, f0), &EmptyCustomAttributesCache};
extern Il2CppType t735_0_0_1;
FieldInfo t1503_f1_FieldInfo = 
{
	"_context", &t735_0_0_1, &t1503_TI, offsetof(t1503, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1494_0_0_1;
FieldInfo t1503_f2_FieldInfo = 
{
	"_binder", &t1494_0_0_1, &t1503_TI, offsetof(t1503, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1497_0_0_1;
FieldInfo t1503_f3_FieldInfo = 
{
	"_filterLevel", &t1497_0_0_1, &t1503_TI, offsetof(t1503, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1504_0_0_1;
FieldInfo t1503_f4_FieldInfo = 
{
	"_manager", &t1504_0_0_1, &t1503_TI, offsetof(t1503, f4), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_1;
FieldInfo t1503_f5_FieldInfo = 
{
	"_registeredAssemblies", &t719_0_0_1, &t1503_TI, offsetof(t1503, f5), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_1;
FieldInfo t1503_f6_FieldInfo = 
{
	"_typeMetadataCache", &t719_0_0_1, &t1503_TI, offsetof(t1503, f6), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t1503_f7_FieldInfo = 
{
	"_lastObject", &t29_0_0_1, &t1503_TI, offsetof(t1503, f7), &EmptyCustomAttributesCache};
extern Il2CppType t919_0_0_1;
FieldInfo t1503_f8_FieldInfo = 
{
	"_lastObjectID", &t919_0_0_1, &t1503_TI, offsetof(t1503, f8), &EmptyCustomAttributesCache};
extern Il2CppType t919_0_0_1;
FieldInfo t1503_f9_FieldInfo = 
{
	"_rootObjectID", &t919_0_0_1, &t1503_TI, offsetof(t1503, f9), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t1503_f10_FieldInfo = 
{
	"arrayBuffer", &t781_0_0_1, &t1503_TI, offsetof(t1503, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1503_f11_FieldInfo = 
{
	"ArrayBufferLength", &t44_0_0_1, &t1503_TI, offsetof(t1503, f11), &EmptyCustomAttributesCache};
static FieldInfo* t1503_FIs[] =
{
	&t1503_f0_FieldInfo,
	&t1503_f1_FieldInfo,
	&t1503_f2_FieldInfo,
	&t1503_f3_FieldInfo,
	&t1503_f4_FieldInfo,
	&t1503_f5_FieldInfo,
	&t1503_f6_FieldInfo,
	&t1503_f7_FieldInfo,
	&t1503_f8_FieldInfo,
	&t1503_f9_FieldInfo,
	&t1503_f10_FieldInfo,
	&t1503_f11_FieldInfo,
	NULL
};
static PropertyInfo t1503____CurrentObject_PropertyInfo = 
{
	&t1503_TI, "CurrentObject", &m8057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1503_PIs[] =
{
	&t1503____CurrentObject_PropertyInfo,
	NULL
};
extern Il2CppType t1483_0_0_0;
static ParameterInfo t1503_m8052_ParameterInfos[] = 
{
	{"formatter", 0, 134222072, &EmptyCustomAttributesCache, &t1483_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8052_MI = 
{
	".ctor", (methodPointerType)&m8052, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1503_m8052_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3636, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t1451_1_0_2;
extern Il2CppType t1451_1_0_0;
static ParameterInfo t1503_m8053_ParameterInfos[] = 
{
	{"reader", 0, 134222073, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"readHeaders", 1, 134222074, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"result", 2, 134222075, &EmptyCustomAttributesCache, &t29_1_0_2},
	{"headers", 3, 134222076, &EmptyCustomAttributesCache, &t1451_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297_t2022_t2058 (MethodInfo* method, void* obj, void** args);
MethodInfo m8053_MI = 
{
	"ReadObjectGraph", (methodPointerType)&m8053, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297_t2022_t2058, t1503_m8053_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 4, false, false, 3637, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t1451_1_0_2;
static ParameterInfo t1503_m8054_ParameterInfos[] = 
{
	{"elem", 0, 134222077, &EmptyCustomAttributesCache, &t1490_0_0_0},
	{"reader", 1, 134222078, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"readHeaders", 2, 134222079, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"result", 3, 134222080, &EmptyCustomAttributesCache, &t29_1_0_2},
	{"headers", 4, 134222081, &EmptyCustomAttributesCache, &t1451_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348_t29_t297_t2022_t2058 (MethodInfo* method, void* obj, void** args);
MethodInfo m8054_MI = 
{
	"ReadObjectGraph", (methodPointerType)&m8054, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t348_t29_t297_t2022_t2058, t1503_m8054_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 5, false, false, 3638, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1304_0_0_0;
static ParameterInfo t1503_m8055_ParameterInfos[] = 
{
	{"element", 0, 134222082, &EmptyCustomAttributesCache, &t1490_0_0_0},
	{"reader", 1, 134222083, &EmptyCustomAttributesCache, &t1304_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8055_MI = 
{
	"ReadNextObject", (methodPointerType)&m8055, &t1503_TI, &t40_0_0_0, RuntimeInvoker_t40_t348_t29, t1503_m8055_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3639, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
static ParameterInfo t1503_m8056_ParameterInfos[] = 
{
	{"reader", 0, 134222084, &EmptyCustomAttributesCache, &t1304_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8056_MI = 
{
	"ReadNextObject", (methodPointerType)&m8056, &t1503_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1503_m8056_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 3640, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8057_MI = 
{
	"get_CurrentObject", (methodPointerType)&m8057, &t1503_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3641, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t919_1_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t733_1_0_2;
extern Il2CppType t733_1_0_0;
static ParameterInfo t1503_m8058_ParameterInfos[] = 
{
	{"element", 0, 134222085, &EmptyCustomAttributesCache, &t1490_0_0_0},
	{"reader", 1, 134222086, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 2, 134222087, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"value", 3, 134222088, &EmptyCustomAttributesCache, &t29_1_0_2},
	{"info", 4, 134222089, &EmptyCustomAttributesCache, &t733_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348_t29_t1712_t2022_t2059 (MethodInfo* method, void* obj, void** args);
MethodInfo m8058_MI = 
{
	"ReadObject", (methodPointerType)&m8058, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t348_t29_t1712_t2022_t2059, t1503_m8058_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 5, false, false, 3642, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
static ParameterInfo t1503_m8059_ParameterInfos[] = 
{
	{"reader", 0, 134222090, &EmptyCustomAttributesCache, &t1304_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8059_MI = 
{
	"ReadAssembly", (methodPointerType)&m8059, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1503_m8059_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 3643, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
extern Il2CppType t733_1_0_2;
static ParameterInfo t1503_m8060_ParameterInfos[] = 
{
	{"reader", 0, 134222091, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"isRuntimeObject", 1, 134222092, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"hasTypeInfo", 2, 134222093, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"objectId", 3, 134222094, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"value", 4, 134222095, &EmptyCustomAttributesCache, &t29_1_0_2},
	{"info", 5, 134222096, &EmptyCustomAttributesCache, &t733_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297_t297_t1712_t2022_t2059 (MethodInfo* method, void* obj, void** args);
MethodInfo m8060_MI = 
{
	"ReadObjectInstance", (methodPointerType)&m8060, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297_t297_t1712_t2022_t2059, t1503_m8060_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 6, false, false, 3644, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
extern Il2CppType t733_1_0_2;
static ParameterInfo t1503_m8061_ParameterInfos[] = 
{
	{"reader", 0, 134222097, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 1, 134222098, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"value", 2, 134222099, &EmptyCustomAttributesCache, &t29_1_0_2},
	{"info", 3, 134222100, &EmptyCustomAttributesCache, &t733_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1712_t2022_t2059 (MethodInfo* method, void* obj, void** args);
MethodInfo m8061_MI = 
{
	"ReadRefTypeObjectInstance", (methodPointerType)&m8061, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1712_t2022_t2059, t1503_m8061_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 4, false, false, 3645, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t1500_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t733_1_0_2;
static ParameterInfo t1503_m8062_ParameterInfos[] = 
{
	{"reader", 0, 134222101, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"metadata", 1, 134222102, &EmptyCustomAttributesCache, &t1500_0_0_0},
	{"objectId", 2, 134222103, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"objectInstance", 3, 134222104, &EmptyCustomAttributesCache, &t29_1_0_2},
	{"info", 4, 134222105, &EmptyCustomAttributesCache, &t733_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t919_t2022_t2059 (MethodInfo* method, void* obj, void** args);
MethodInfo m8062_MI = 
{
	"ReadObjectContent", (methodPointerType)&m8062, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t919_t2022_t2059, t1503_m8062_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 5, false, false, 3646, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
static ParameterInfo t1503_m8063_ParameterInfos[] = 
{
	{"objectId", 0, 134222106, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"objectInstance", 1, 134222107, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"info", 2, 134222108, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"parentObjectId", 3, 134222109, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"parentObjectMemeber", 4, 134222110, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"indices", 5, 134222111, &EmptyCustomAttributesCache, &t841_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919_t29_t29_t919_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8063_MI = 
{
	"RegisterObject", (methodPointerType)&m8063, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t919_t29_t29_t919_t29_t29, t1503_m8063_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 6, false, false, 3647, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
static ParameterInfo t1503_m8064_ParameterInfos[] = 
{
	{"reader", 0, 134222112, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 1, 134222113, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"value", 2, 134222114, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8064_MI = 
{
	"ReadStringIntance", (methodPointerType)&m8064, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1712_t2022, t1503_m8064_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3648, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
static ParameterInfo t1503_m8065_ParameterInfos[] = 
{
	{"reader", 0, 134222115, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 1, 134222116, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"val", 2, 134222117, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8065_MI = 
{
	"ReadGenericArray", (methodPointerType)&m8065, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1712_t2022, t1503_m8065_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3649, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
static ParameterInfo t1503_m8066_ParameterInfos[] = 
{
	{"reader", 0, 134222118, &EmptyCustomAttributesCache, &t1304_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8066_MI = 
{
	"ReadBoxedPrimitiveTypeValue", (methodPointerType)&m8066, &t1503_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1503_m8066_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 3650, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
static ParameterInfo t1503_m8067_ParameterInfos[] = 
{
	{"reader", 0, 134222119, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 1, 134222120, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"val", 2, 134222121, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8067_MI = 
{
	"ReadArrayOfPrimitiveType", (methodPointerType)&m8067, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1712_t2022, t1503_m8067_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3651, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1503_m8068_ParameterInfos[] = 
{
	{"reader", 0, 134222122, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"array", 1, 134222123, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"dataSize", 2, 134222124, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m8068_MI = 
{
	"BlockRead", (methodPointerType)&m8068, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44, t1503_m8068_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3652, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
static ParameterInfo t1503_m8069_ParameterInfos[] = 
{
	{"reader", 0, 134222125, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 1, 134222126, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"array", 2, 134222127, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8069_MI = 
{
	"ReadArrayOfObject", (methodPointerType)&m8069, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1712_t2022, t1503_m8069_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3653, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
static ParameterInfo t1503_m8070_ParameterInfos[] = 
{
	{"reader", 0, 134222128, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"objectId", 1, 134222129, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"array", 2, 134222130, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8070_MI = 
{
	"ReadArrayOfString", (methodPointerType)&m8070, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1712_t2022, t1503_m8070_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3654, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t919_1_0_2;
extern Il2CppType t29_1_0_2;
static ParameterInfo t1503_m8071_ParameterInfos[] = 
{
	{"reader", 0, 134222131, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"elementType", 1, 134222132, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"objectId", 2, 134222133, &EmptyCustomAttributesCache, &t919_1_0_2},
	{"val", 3, 134222134, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m8071_MI = 
{
	"ReadSimpleArray", (methodPointerType)&m8071, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t1712_t2022, t1503_m8071_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 4, false, false, 3655, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1503_m8072_ParameterInfos[] = 
{
	{"reader", 0, 134222135, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"isRuntimeObject", 1, 134222136, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"hasTypeInfo", 2, 134222137, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t1500_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m8072_MI = 
{
	"ReadTypeMetadata", (methodPointerType)&m8072, &t1503_TI, &t1500_0_0_0, RuntimeInvoker_t29_t29_t297_t297, t1503_m8072_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3656, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t841_0_0_0;
static ParameterInfo t1503_m8073_ParameterInfos[] = 
{
	{"reader", 0, 134222138, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"parentObject", 1, 134222139, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"parentObjectId", 2, 134222140, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"info", 3, 134222141, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"valueType", 4, 134222142, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"fieldName", 5, 134222143, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"memberInfo", 6, 134222144, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"indices", 7, 134222145, &EmptyCustomAttributesCache, &t841_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t919_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8073_MI = 
{
	"ReadValue", (methodPointerType)&m8073, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t919_t29_t29_t29_t29_t29, t1503_m8073_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 8, false, false, 3657, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t841_0_0_0;
static ParameterInfo t1503_m8074_ParameterInfos[] = 
{
	{"parentObject", 0, 134222146, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"fieldName", 1, 134222147, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"memberInfo", 2, 134222148, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"info", 3, 134222149, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"value", 4, 134222150, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"valueType", 5, 134222151, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"indices", 6, 134222152, &EmptyCustomAttributesCache, &t841_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8074_MI = 
{
	"SetObjectValue", (methodPointerType)&m8074, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29_t29_t29_t29_t29, t1503_m8074_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 7, false, false, 3658, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t841_0_0_0;
static ParameterInfo t1503_m8075_ParameterInfos[] = 
{
	{"parentObjectId", 0, 134222153, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"childObjectId", 1, 134222154, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"parentObject", 2, 134222155, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"info", 3, 134222156, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"fieldName", 4, 134222157, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"memberInfo", 5, 134222158, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"indices", 6, 134222159, &EmptyCustomAttributesCache, &t841_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919_t919_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8075_MI = 
{
	"RecordFixup", (methodPointerType)&m8075, &t1503_TI, &t21_0_0_0, RuntimeInvoker_t21_t919_t919_t29_t29_t29_t29_t29, t1503_m8075_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 7, false, false, 3659, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1503_m8076_ParameterInfos[] = 
{
	{"assemblyId", 0, 134222160, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"className", 1, 134222161, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8076_MI = 
{
	"GetDeserializationType", (methodPointerType)&m8076, &t1503_TI, &t42_0_0_0, RuntimeInvoker_t29_t919_t29, t1503_m8076_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3660, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t1503_m8077_ParameterInfos[] = 
{
	{"reader", 0, 134222162, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"code", 1, 134222163, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m8077_MI = 
{
	"ReadType", (methodPointerType)&m8077, &t1503_TI, &t42_0_0_0, RuntimeInvoker_t29_t29_t348, t1503_m8077_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 3661, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1304_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1503_m8078_ParameterInfos[] = 
{
	{"reader", 0, 134222164, &EmptyCustomAttributesCache, &t1304_0_0_0},
	{"type", 1, 134222165, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8078_MI = 
{
	"ReadPrimitiveTypeValue", (methodPointerType)&m8078, &t1503_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1503_m8078_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 3662, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1503_MIs[] =
{
	&m8052_MI,
	&m8053_MI,
	&m8054_MI,
	&m8055_MI,
	&m8056_MI,
	&m8057_MI,
	&m8058_MI,
	&m8059_MI,
	&m8060_MI,
	&m8061_MI,
	&m8062_MI,
	&m8063_MI,
	&m8064_MI,
	&m8065_MI,
	&m8066_MI,
	&m8067_MI,
	&m8068_MI,
	&m8069_MI,
	&m8070_MI,
	&m8071_MI,
	&m8072_MI,
	&m8073_MI,
	&m8074_MI,
	&m8075_MI,
	&m8076_MI,
	&m8077_MI,
	&m8078_MI,
	NULL
};
extern TypeInfo t1500_TI;
extern TypeInfo t1502_TI;
static TypeInfo* t1503_TI__nestedTypes[3] =
{
	&t1500_TI,
	&t1502_TI,
	NULL
};
static MethodInfo* t1503_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1503_0_0_0;
extern Il2CppType t1503_1_0_0;
struct t1503;
TypeInfo t1503_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObjectReader", "System.Runtime.Serialization.Formatters.Binary", t1503_MIs, t1503_PIs, t1503_FIs, NULL, &t29_TI, t1503_TI__nestedTypes, NULL, &t1503_TI, NULL, t1503_VT, &EmptyCustomAttributesCache, &t1503_TI, &t1503_0_0_0, &t1503_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1503), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 27, 1, 12, 0, 2, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1495_TI;
#include "t1495MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern Il2CppType t44_0_0_1542;
FieldInfo t1495_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1495_TI, offsetof(t1495, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1495_0_0_32854;
FieldInfo t1495_f2_FieldInfo = 
{
	"Simple", &t1495_0_0_32854, &t1495_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1495_0_0_32854;
FieldInfo t1495_f3_FieldInfo = 
{
	"Full", &t1495_0_0_32854, &t1495_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1495_FIs[] =
{
	&t1495_f1_FieldInfo,
	&t1495_f2_FieldInfo,
	&t1495_f3_FieldInfo,
	NULL
};
static const int32_t t1495_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1495_f2_DefaultValue = 
{
	&t1495_f2_FieldInfo, { (char*)&t1495_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1495_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1495_f3_DefaultValue = 
{
	&t1495_f3_FieldInfo, { (char*)&t1495_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1495_FDVs[] = 
{
	&t1495_f2_DefaultValue,
	&t1495_f3_DefaultValue,
	NULL
};
static MethodInfo* t1495_MIs[] =
{
	NULL
};
static MethodInfo* t1495_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1495_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1495__CustomAttributeCache = {
1,
NULL,
&t1495_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1495_1_0_0;
extern CustomAttributesCache t1495__CustomAttributeCache;
TypeInfo t1495_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FormatterAssemblyStyle", "System.Runtime.Serialization.Formatters", t1495_MIs, NULL, t1495_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1495_VT, &t1495__CustomAttributeCache, &t44_TI, &t1495_0_0_0, &t1495_1_0_0, t1495_IOs, NULL, NULL, t1495_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1495)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1496_TI;
#include "t1496MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern Il2CppType t44_0_0_1542;
FieldInfo t1496_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1496_TI, offsetof(t1496, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1496_0_0_32854;
FieldInfo t1496_f2_FieldInfo = 
{
	"TypesWhenNeeded", &t1496_0_0_32854, &t1496_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1496_0_0_32854;
FieldInfo t1496_f3_FieldInfo = 
{
	"TypesAlways", &t1496_0_0_32854, &t1496_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1496_0_0_32854;
FieldInfo t1496_f4_FieldInfo = 
{
	"XsdString", &t1496_0_0_32854, &t1496_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1496_FIs[] =
{
	&t1496_f1_FieldInfo,
	&t1496_f2_FieldInfo,
	&t1496_f3_FieldInfo,
	&t1496_f4_FieldInfo,
	NULL
};
static const int32_t t1496_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1496_f2_DefaultValue = 
{
	&t1496_f2_FieldInfo, { (char*)&t1496_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1496_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1496_f3_DefaultValue = 
{
	&t1496_f3_FieldInfo, { (char*)&t1496_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1496_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1496_f4_DefaultValue = 
{
	&t1496_f4_FieldInfo, { (char*)&t1496_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1496_FDVs[] = 
{
	&t1496_f2_DefaultValue,
	&t1496_f3_DefaultValue,
	&t1496_f4_DefaultValue,
	NULL
};
static MethodInfo* t1496_MIs[] =
{
	NULL
};
static MethodInfo* t1496_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1496_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1496__CustomAttributeCache = {
1,
NULL,
&t1496_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1496_0_0_0;
extern Il2CppType t1496_1_0_0;
extern CustomAttributesCache t1496__CustomAttributeCache;
TypeInfo t1496_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FormatterTypeStyle", "System.Runtime.Serialization.Formatters", t1496_MIs, NULL, t1496_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1496_VT, &t1496__CustomAttributeCache, &t44_TI, &t1496_0_0_0, &t1496_1_0_0, t1496_IOs, NULL, NULL, t1496_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1496)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1497_TI;
#include "t1497MD.h"



// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern Il2CppType t44_0_0_1542;
FieldInfo t1497_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1497_TI, offsetof(t1497, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1497_0_0_32854;
FieldInfo t1497_f2_FieldInfo = 
{
	"Low", &t1497_0_0_32854, &t1497_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1497_0_0_32854;
FieldInfo t1497_f3_FieldInfo = 
{
	"Full", &t1497_0_0_32854, &t1497_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1497_FIs[] =
{
	&t1497_f1_FieldInfo,
	&t1497_f2_FieldInfo,
	&t1497_f3_FieldInfo,
	NULL
};
static const int32_t t1497_f2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1497_f2_DefaultValue = 
{
	&t1497_f2_FieldInfo, { (char*)&t1497_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1497_f3_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1497_f3_DefaultValue = 
{
	&t1497_f3_FieldInfo, { (char*)&t1497_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1497_FDVs[] = 
{
	&t1497_f2_DefaultValue,
	&t1497_f3_DefaultValue,
	NULL
};
static MethodInfo* t1497_MIs[] =
{
	NULL
};
static MethodInfo* t1497_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1497_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1497__CustomAttributeCache = {
1,
NULL,
&t1497_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1497_0_0_0;
extern Il2CppType t1497_1_0_0;
extern CustomAttributesCache t1497__CustomAttributeCache;
TypeInfo t1497_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeFilterLevel", "System.Runtime.Serialization.Formatters", t1497_MIs, NULL, t1497_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1497_VT, &t1497__CustomAttributeCache, &t44_TI, &t1497_0_0_0, &t1497_1_0_0, t1497_IOs, NULL, NULL, t1497_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1497)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m9091_MI;
extern MethodInfo m8908_MI;
extern MethodInfo m8985_MI;
extern MethodInfo m9016_MI;
extern MethodInfo m9046_MI;


 void m8079 (t1505 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m8080_MI;
 t29 * m8080 (t1505 * __this, t29 * p0, t42 * p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		t29 * L_0 = m9091(NULL, p0, p1, &m9091_MI);
		return L_0;
	}
}
extern MethodInfo m8081_MI;
 bool m8081 (t1505 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1650, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		bool L_1 = m8908(NULL, p0, &m8908_MI);
		return L_1;
	}
}
extern MethodInfo m8082_MI;
 int16_t m8082 (t1505 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1650, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		int16_t L_1 = m8985(NULL, p0, &m8985_MI);
		return L_1;
	}
}
extern MethodInfo m8083_MI;
 int32_t m8083 (t1505 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1650, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		int32_t L_1 = m9000(NULL, p0, &m9000_MI);
		return L_1;
	}
}
extern MethodInfo m8084_MI;
 int64_t m8084 (t1505 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1650, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		int64_t L_1 = m9016(NULL, p0, &m9016_MI);
		return L_1;
	}
}
extern MethodInfo m8085_MI;
 t7* m8085 (t1505 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1650, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		t7* L_1 = m9046(NULL, p0, &m9046_MI);
		return L_1;
	}
}
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8079_MI = 
{
	".ctor", (methodPointerType)&m8079, &t1505_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3665, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1505_m8080_ParameterInfos[] = 
{
	{"value", 0, 134222167, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"type", 1, 134222168, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8080_MI = 
{
	"Convert", (methodPointerType)&m8080, &t1505_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1505_m8080_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 2, false, false, 3666, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1505_m8081_ParameterInfos[] = 
{
	{"value", 0, 134222169, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8081_MI = 
{
	"ToBoolean", (methodPointerType)&m8081, &t1505_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1505_m8081_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 1, false, false, 3667, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1505_m8082_ParameterInfos[] = 
{
	{"value", 0, 134222170, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t372_0_0_0;
extern void* RuntimeInvoker_t372_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8082_MI = 
{
	"ToInt16", (methodPointerType)&m8082, &t1505_TI, &t372_0_0_0, RuntimeInvoker_t372_t29, t1505_m8082_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, false, 3668, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1505_m8083_ParameterInfos[] = 
{
	{"value", 0, 134222171, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8083_MI = 
{
	"ToInt32", (methodPointerType)&m8083, &t1505_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1505_m8083_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 1, false, false, 3669, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1505_m8084_ParameterInfos[] = 
{
	{"value", 0, 134222172, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8084_MI = 
{
	"ToInt64", (methodPointerType)&m8084, &t1505_TI, &t919_0_0_0, RuntimeInvoker_t919_t29, t1505_m8084_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 8, 1, false, false, 3670, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1505_m8085_ParameterInfos[] = 
{
	{"value", 0, 134222173, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8085_MI = 
{
	"ToString", (methodPointerType)&m8085, &t1505_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1505_m8085_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 9, 1, false, false, 3671, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1505_MIs[] =
{
	&m8079_MI,
	&m8080_MI,
	&m8081_MI,
	&m8082_MI,
	&m8083_MI,
	&m8084_MI,
	&m8085_MI,
	NULL
};
static MethodInfo* t1505_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8080_MI,
	&m8081_MI,
	&m8082_MI,
	&m8083_MI,
	&m8084_MI,
	&m8085_MI,
};
extern TypeInfo t1521_TI;
static TypeInfo* t1505_ITIs[] = 
{
	&t1521_TI,
};
static Il2CppInterfaceOffsetPair t1505_IOs[] = 
{
	{ &t1521_TI, 4},
};
void t1505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1505__CustomAttributeCache = {
1,
NULL,
&t1505_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1505_0_0_0;
extern Il2CppType t1505_1_0_0;
struct t1505;
extern CustomAttributesCache t1505__CustomAttributeCache;
TypeInfo t1505_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FormatterConverter", "System.Runtime.Serialization", t1505_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1505_TI, t1505_ITIs, t1505_VT, &t1505__CustomAttributeCache, &t1505_TI, &t1505_0_0_0, &t1505_1_0_0, t1505_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1505), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 0, 0, 0, 0, 10, 1, 1};
#include "t1506.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1506_TI;

extern MethodInfo m7767_MI;


 t29 * m8086 (t29 * __this, t42 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral896, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_1)))
		{
			goto IL_0026;
		}
	}
	{
		t305 * L_2 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_2, (t7*) &_stringLiteral1651, &m1935_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0026:
	{
		t29 * L_3 = m7767(NULL, p0, &m7767_MI);
		return L_3;
	}
}
 t29 * m8087 (t29 * __this, t42 * p0, MethodInfo* method){
	{
		t29 * L_0 = m8086(NULL, p0, &m8086_MI);
		return L_0;
	}
}
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern Il2CppType t42_0_0_0;
static ParameterInfo t1506_m8086_ParameterInfos[] = 
{
	{"type", 0, 134222174, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8086_MI = 
{
	"GetUninitializedObject", (methodPointerType)&m8086, &t1506_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1506_m8086_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3672, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1506_m8087_ParameterInfos[] = 
{
	{"type", 0, 134222175, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8087_MI = 
{
	"GetSafeUninitializedObject", (methodPointerType)&m8087, &t1506_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1506_m8087_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3673, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1506_MIs[] =
{
	&m8086_MI,
	&m8087_MI,
	NULL
};
static MethodInfo* t1506_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1506__CustomAttributeCache = {
1,
NULL,
&t1506_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1506_0_0_0;
extern Il2CppType t1506_1_0_0;
struct t1506;
extern CustomAttributesCache t1506__CustomAttributeCache;
TypeInfo t1506_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FormatterServices", "System.Runtime.Serialization", t1506_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1506_TI, NULL, t1506_VT, &t1506__CustomAttributeCache, &t1506_TI, &t1506_0_0_0, &t1506_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1506), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t918_TI;



// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern Il2CppType t29_0_0_0;
static ParameterInfo t918_m10213_ParameterInfos[] = 
{
	{"sender", 0, 134222176, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10213_MI = 
{
	"OnDeserialization", NULL, &t918_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t918_m10213_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, false, 3674, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t918_MIs[] =
{
	&m10213_MI,
	NULL
};
void t918_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t918__CustomAttributeCache = {
1,
NULL,
&t918_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t918_0_0_0;
extern Il2CppType t918_1_0_0;
struct t918;
extern CustomAttributesCache t918__CustomAttributeCache;
TypeInfo t918_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDeserializationCallback", "System.Runtime.Serialization", t918_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t918_TI, NULL, NULL, &t918__CustomAttributeCache, &t918_TI, &t918_0_0_0, &t918_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Serialization.IFormatter
static MethodInfo* t2049_MIs[] =
{
	NULL
};
void t2049_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2049__CustomAttributeCache = {
1,
NULL,
&t2049_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2049_0_0_0;
extern Il2CppType t2049_1_0_0;
struct t2049;
extern CustomAttributesCache t2049__CustomAttributeCache;
TypeInfo t2049_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IFormatter", "System.Runtime.Serialization", t2049_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2049_TI, NULL, NULL, &t2049__CustomAttributeCache, &t2049_TI, &t2049_0_0_0, &t2049_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern Il2CppType t29_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1521_m10214_ParameterInfos[] = 
{
	{"value", 0, 134222177, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"type", 1, 134222178, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10214_MI = 
{
	"Convert", NULL, &t1521_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1521_m10214_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, false, 3675, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1521_m10215_ParameterInfos[] = 
{
	{"value", 0, 134222179, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10215_MI = 
{
	"ToBoolean", NULL, &t1521_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1521_m10215_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, false, 3676, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1521_m10216_ParameterInfos[] = 
{
	{"value", 0, 134222180, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t372_0_0_0;
extern void* RuntimeInvoker_t372_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10216_MI = 
{
	"ToInt16", NULL, &t1521_TI, &t372_0_0_0, RuntimeInvoker_t372_t29, t1521_m10216_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, false, 3677, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1521_m10217_ParameterInfos[] = 
{
	{"value", 0, 134222181, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10217_MI = 
{
	"ToInt32", NULL, &t1521_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1521_m10217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 3, 1, false, false, 3678, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1521_m10218_ParameterInfos[] = 
{
	{"value", 0, 134222182, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t919_0_0_0;
extern void* RuntimeInvoker_t919_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10218_MI = 
{
	"ToInt64", NULL, &t1521_TI, &t919_0_0_0, RuntimeInvoker_t919_t29, t1521_m10218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, false, 3679, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1521_m10219_ParameterInfos[] = 
{
	{"value", 0, 134222183, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10219_MI = 
{
	"ToString", NULL, &t1521_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1521_m10219_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 1, false, false, 3680, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1521_MIs[] =
{
	&m10214_MI,
	&m10215_MI,
	&m10216_MI,
	&m10217_MI,
	&m10218_MI,
	&m10219_MI,
	NULL
};
void t1521_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1521__CustomAttributeCache = {
2,
NULL,
&t1521_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1521_0_0_0;
extern Il2CppType t1521_1_0_0;
struct t1521;
extern CustomAttributesCache t1521__CustomAttributeCache;
TypeInfo t1521_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IFormatterConverter", "System.Runtime.Serialization", t1521_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1521_TI, NULL, NULL, &t1521__CustomAttributeCache, &t1521_TI, &t1521_0_0_0, &t1521_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 6, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Serialization.IObjectReference
extern Il2CppType t735_0_0_0;
static ParameterInfo t2024_m10211_ParameterInfos[] = 
{
	{"context", 0, 134222184, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m10211_MI = 
{
	"GetRealObject", NULL, &t2024_TI, &t29_0_0_0, RuntimeInvoker_t29_t735, t2024_m10211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, false, 3681, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t2024_MIs[] =
{
	&m10211_MI,
	NULL
};
void t2024_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2024__CustomAttributeCache = {
1,
NULL,
&t2024_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2024_0_0_0;
extern Il2CppType t2024_1_0_0;
struct t2024;
extern CustomAttributesCache t2024__CustomAttributeCache;
TypeInfo t2024_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IObjectReference", "System.Runtime.Serialization", t2024_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2024_TI, NULL, NULL, &t2024__CustomAttributeCache, &t2024_TI, &t2024_0_0_0, &t2024_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Serialization.ISerializationSurrogate
extern Il2CppType t29_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t1458_0_0_0;
static ParameterInfo t1461_m10220_ParameterInfos[] = 
{
	{"obj", 0, 134222185, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"info", 1, 134222186, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 2, 134222187, &EmptyCustomAttributesCache, &t735_0_0_0},
	{"selector", 3, 134222188, &EmptyCustomAttributesCache, &t1458_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t735_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10220_MI = 
{
	"SetObjectData", NULL, &t1461_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t735_t29, t1461_m10220_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 4, false, false, 3682, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1461_MIs[] =
{
	&m10220_MI,
	NULL
};
void t1461_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1461__CustomAttributeCache = {
1,
NULL,
&t1461_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1461_0_0_0;
extern Il2CppType t1461_1_0_0;
struct t1461;
extern CustomAttributesCache t1461__CustomAttributeCache;
TypeInfo t1461_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ISerializationSurrogate", "System.Runtime.Serialization", t1461_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1461_TI, NULL, NULL, &t1461__CustomAttributeCache, &t1461_TI, &t1461_0_0_0, &t1461_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.Serialization.ISurrogateSelector
extern Il2CppType t42_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t1458_1_0_2;
static ParameterInfo t1458_m10205_ParameterInfos[] = 
{
	{"type", 0, 134222189, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"context", 1, 134222190, &EmptyCustomAttributesCache, &t735_0_0_0},
	{"selector", 2, 134222191, &EmptyCustomAttributesCache, &t1458_1_0_2},
};
extern Il2CppType t1461_0_0_0;
extern void* RuntimeInvoker_t29_t29_t735_t2050 (MethodInfo* method, void* obj, void** args);
MethodInfo m10205_MI = 
{
	"GetSurrogate", NULL, &t1458_TI, &t1461_0_0_0, RuntimeInvoker_t29_t29_t735_t2050, t1458_m10205_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 3, false, false, 3683, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1458_MIs[] =
{
	&m10205_MI,
	NULL
};
void t1458_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1458__CustomAttributeCache = {
1,
NULL,
&t1458_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
struct t1458;
extern CustomAttributesCache t1458__CustomAttributeCache;
TypeInfo t1458_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ISurrogateSelector", "System.Runtime.Serialization", t1458_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1458_TI, NULL, NULL, &t1458__CustomAttributeCache, &t1458_TI, &t1458_0_0_0, &t1458_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1507.h"
#include "t1519.h"
#include "t1513.h"
#include "t915.h"
#include "t1508.h"
#include "t1509.h"
#include "t1510.h"
#include "t1512.h"
#include "t1511.h"
extern TypeInfo t1507_TI;
extern TypeInfo t1519_TI;
extern TypeInfo t915_TI;
extern TypeInfo t1508_TI;
extern TypeInfo t1509_TI;
extern TypeInfo t1510_TI;
extern TypeInfo t1512_TI;
extern TypeInfo t1511_TI;
#include "t1507MD.h"
#include "t1519MD.h"
#include "t915MD.h"
#include "t1509MD.h"
#include "t1510MD.h"
#include "t1512MD.h"
#include "t1511MD.h"
extern MethodInfo m4218_MI;
extern MethodInfo m8117_MI;
extern MethodInfo m8119_MI;
extern MethodInfo m8123_MI;
extern MethodInfo m8137_MI;
extern MethodInfo m8132_MI;
extern MethodInfo m1295_MI;
extern MethodInfo m8112_MI;
extern MethodInfo m8118_MI;
extern MethodInfo m3968_MI;
extern MethodInfo m8094_MI;
extern MethodInfo m10213_MI;
extern MethodInfo m8135_MI;
extern MethodInfo m8136_MI;
extern MethodInfo m8122_MI;
extern MethodInfo m8090_MI;
extern MethodInfo m8104_MI;
extern MethodInfo m8095_MI;
extern MethodInfo m8106_MI;
extern MethodInfo m8110_MI;
extern MethodInfo m8108_MI;
extern MethodInfo m3966_MI;
extern MethodInfo m8100_MI;


 void m8088 (t1504 * __this, t29 * p0, t735  p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		__this->f2 = L_0;
		t731 * L_1 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_1, &m3980_MI);
		__this->f3 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_2 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_2, &m4209_MI);
		__this->f4 = L_2;
		m1331(__this, &m1331_MI);
		__this->f6 = p0;
		__this->f7 = p1;
		return;
	}
}
 void m8089 (t1504 * __this, MethodInfo* method){
	t1507 * V_0 = {0};
	bool V_1 = false;
	t1507 * V_2 = {0};
	bool V_3 = false;
	t1507 * V_4 = {0};
	t1519 * V_5 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t G_B7_0 = 0;
	{
		__this->f5 = 1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (__this->f8);
			t719 * L_1 = (__this->f4);
			int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m4218_MI, L_1);
			if ((((int32_t)L_0) >= ((int32_t)L_2)))
			{
				goto IL_0025;
			}
		}

IL_001a:
		{
			t917 * L_3 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
			m3986(L_3, (t7*) &_stringLiteral1652, &m3986_MI);
			il2cpp_codegen_raise_exception(L_3);
		}

IL_0025:
		{
			t1507 * L_4 = (__this->f1);
			V_0 = L_4;
			V_1 = 1;
			t1507 * L_5 = (__this->f0);
			V_2 = L_5;
			goto IL_0136;
		}

IL_003a:
		{
			bool L_6 = m8117(V_2, &m8117_MI);
			if (!L_6)
			{
				goto IL_0045;
			}
		}

IL_0042:
		{
			G_B7_0 = ((int32_t)(V_1));
			goto IL_0046;
		}

IL_0045:
		{
			G_B7_0 = 0;
		}

IL_0046:
		{
			V_3 = ((((int32_t)G_B7_0) == ((int32_t)0))? 1 : 0);
			if (!V_3)
			{
				goto IL_0057;
			}
		}

IL_004d:
		{
			bool L_7 = m8119(V_2, 1, __this, 1, &m8119_MI);
			V_3 = L_7;
		}

IL_0057:
		{
			if (!V_3)
			{
				goto IL_006e;
			}
		}

IL_005a:
		{
			t29 * L_8 = (__this->f6);
			t735  L_9 = (__this->f7);
			bool L_10 = m8123(V_2, __this, L_8, L_9, &m8123_MI);
			V_3 = L_10;
		}

IL_006e:
		{
			if (!V_3)
			{
				goto IL_00bd;
			}
		}

IL_0071:
		{
			t29 * L_11 = (V_2->f1);
			if (!((t29 *)IsInst(L_11, InitializedTypeInfo(&t918_TI))))
			{
				goto IL_008b;
			}
		}

IL_007e:
		{
			t731 * L_12 = (__this->f2);
			VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_12, V_2);
		}

IL_008b:
		{
			t29 * L_13 = (V_2->f1);
			t42 * L_14 = m1430(L_13, &m1430_MI);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1519_TI));
			t1519 * L_15 = m8137(NULL, L_14, &m8137_MI);
			V_5 = L_15;
			bool L_16 = m8132(V_5, &m8132_MI);
			if (!L_16)
			{
				goto IL_00b3;
			}
		}

IL_00a6:
		{
			t731 * L_17 = (__this->f3);
			VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_17, V_2);
		}

IL_00b3:
		{
			t1507 * L_18 = (V_2->f12);
			V_4 = L_18;
			goto IL_012d;
		}

IL_00bd:
		{
			t29 * L_19 = (V_2->f2);
			if (!((t29 *)IsInst(L_19, InitializedTypeInfo(&t2024_TI))))
			{
				goto IL_00fd;
			}
		}

IL_00ca:
		{
			if (V_1)
			{
				goto IL_00fd;
			}
		}

IL_00cd:
		{
			uint8_t L_20 = (V_2->f0);
			if ((((uint32_t)L_20) != ((uint32_t)2)))
			{
				goto IL_00f6;
			}
		}

IL_00d6:
		{
			int64_t L_21 = (V_2->f3);
			int64_t L_22 = L_21;
			t29 * L_23 = Box(InitializedTypeInfo(&t919_TI), &L_22);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			t7* L_24 = m1295(NULL, (t7*) &_stringLiteral1653, L_23, (t7*) &_stringLiteral1654, &m1295_MI);
			t917 * L_25 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
			m3986(L_25, L_24, &m3986_MI);
			il2cpp_codegen_raise_exception(L_25);
		}

IL_00f6:
		{
			V_2->f0 = 2;
		}

IL_00fd:
		{
			t1507 * L_26 = (__this->f1);
			if ((((t1507 *)V_2) == ((t1507 *)L_26)))
			{
				goto IL_012a;
			}
		}

IL_0106:
		{
			t1507 * L_27 = (V_2->f12);
			V_4 = L_27;
			V_2->f12 = (t1507 *)NULL;
			t1507 * L_28 = (__this->f1);
			L_28->f12 = V_2;
			__this->f1 = V_2;
			goto IL_012d;
		}

IL_012a:
		{
			V_4 = V_2;
		}

IL_012d:
		{
			if ((((t1507 *)V_2) != ((t1507 *)V_0)))
			{
				goto IL_0133;
			}
		}

IL_0131:
		{
			V_1 = 0;
		}

IL_0133:
		{
			V_2 = V_4;
		}

IL_0136:
		{
			if (V_2)
			{
				goto IL_003a;
			}
		}

IL_013c:
		{
			// IL_013c: leave.s IL_0146
			leaveInstructions[0] = 0x146; // 1
			THROW_SENTINEL(IL_0146);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_013e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_013e;
	}

IL_013e:
	{ // begin finally (depth: 1)
		__this->f5 = 0;
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x146:
				goto IL_0146;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0146:
	{
		return;
	}
}
 t1507 * m8090 (t1504 * __this, int64_t p0, MethodInfo* method){
	t1507 * V_0 = {0};
	{
		t719 * L_0 = (__this->f4);
		int64_t L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t919_TI), &L_1);
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_0, L_2);
		V_0 = ((t1507 *)Castclass(L_3, InitializedTypeInfo(&t1507_TI)));
		if (V_0)
		{
			goto IL_005c;
		}
	}
	{
		bool L_4 = (__this->f5);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		int64_t L_5 = p0;
		t29 * L_6 = Box(InitializedTypeInfo(&t919_TI), &L_5);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_7 = m1295(NULL, (t7*) &_stringLiteral1655, L_6, (t7*) &_stringLiteral1656, &m1295_MI);
		t917 * L_8 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_8, L_7, &m3986_MI);
		il2cpp_codegen_raise_exception(L_8);
	}

IL_003d:
	{
		t1507 * L_9 = (t1507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1507_TI));
		m8112(L_9, &m8112_MI);
		V_0 = L_9;
		V_0->f3 = p0;
		t719 * L_10 = (__this->f4);
		int64_t L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t919_TI), &L_11);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, L_10, L_12, V_0);
	}

IL_005c:
	{
		bool L_13 = m8118(V_0, &m8118_MI);
		if (L_13)
		{
			goto IL_0087;
		}
	}
	{
		bool L_14 = (__this->f5);
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		int64_t L_15 = p0;
		t29 * L_16 = Box(InitializedTypeInfo(&t919_TI), &L_15);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_17 = m1295(NULL, (t7*) &_stringLiteral1655, L_16, (t7*) &_stringLiteral1656, &m1295_MI);
		t917 * L_18 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_18, L_17, &m3986_MI);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_0087:
	{
		return V_0;
	}
}
 t29 * m8091 (t1504 * __this, int64_t p0, MethodInfo* method){
	t1507 * V_0 = {0};
	{
		if ((((int64_t)p0) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_0, (t7*) &_stringLiteral1657, (t7*) &_stringLiteral1658, &m3968_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		t719 * L_1 = (__this->f4);
		int64_t L_2 = p0;
		t29 * L_3 = Box(InitializedTypeInfo(&t919_TI), &L_2);
		t29 * L_4 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_1, L_3);
		V_0 = ((t1507 *)Castclass(L_4, InitializedTypeInfo(&t1507_TI)));
		if (!V_0)
		{
			goto IL_0037;
		}
	}
	{
		bool L_5 = m8118(V_0, &m8118_MI);
		if (L_5)
		{
			goto IL_0039;
		}
	}

IL_0037:
	{
		return NULL;
	}

IL_0039:
	{
		t29 * L_6 = (V_0->f2);
		return L_6;
	}
}
 void m8092 (t1504 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t1507 * V_1 = {0};
	int32_t V_2 = 0;
	t1507 * V_3 = {0};
	t29 * V_4 = {0};
	{
		t731 * L_0 = (__this->f3);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_0);
		V_0 = ((int32_t)(L_1-1));
		goto IL_0032;
	}

IL_0010:
	{
		t731 * L_2 = (__this->f3);
		t29 * L_3 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, L_2, V_0);
		V_1 = ((t1507 *)Castclass(L_3, InitializedTypeInfo(&t1507_TI)));
		t29 * L_4 = (V_1->f1);
		m8094(__this, L_4, &m8094_MI);
		V_0 = ((int32_t)(V_0-1));
	}

IL_0032:
	{
		if ((((int32_t)V_0) >= ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		t731 * L_5 = (__this->f2);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_5);
		V_2 = ((int32_t)(L_6-1));
		goto IL_0075;
	}

IL_0046:
	{
		t731 * L_7 = (__this->f2);
		t29 * L_8 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, L_7, V_2);
		V_3 = ((t1507 *)Castclass(L_8, InitializedTypeInfo(&t1507_TI)));
		t29 * L_9 = (V_3->f1);
		V_4 = ((t29 *)IsInst(L_9, InitializedTypeInfo(&t918_TI)));
		if (!V_4)
		{
			goto IL_0071;
		}
	}
	{
		InterfaceActionInvoker1< t29 * >::Invoke(&m10213_MI, V_4, __this);
	}

IL_0071:
	{
		V_2 = ((int32_t)(V_2-1));
	}

IL_0075:
	{
		if ((((int32_t)V_2) >= ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
 void m8093 (t1504 * __this, t29 * p0, MethodInfo* method){
	t1519 * V_0 = {0};
	{
		t42 * L_0 = m1430(p0, &m1430_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1519_TI));
		t1519 * L_1 = m8137(NULL, L_0, &m8137_MI);
		V_0 = L_1;
		t735  L_2 = (__this->f7);
		m8135(V_0, p0, L_2, &m8135_MI);
		return;
	}
}
 void m8094 (t1504 * __this, t29 * p0, MethodInfo* method){
	t1519 * V_0 = {0};
	{
		t42 * L_0 = m1430(p0, &m1430_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1519_TI));
		t1519 * L_1 = m8137(NULL, L_0, &m8137_MI);
		V_0 = L_1;
		t735  L_2 = (__this->f7);
		m8136(V_0, p0, L_2, &m8136_MI);
		return;
	}
}
 void m8095 (t1504 * __this, t1508 * p0, MethodInfo* method){
	{
		t1507 * L_0 = (p0->f0);
		m8122(L_0, p0, 1, &m8122_MI);
		t1507 * L_1 = (p0->f1);
		m8122(L_1, p0, 0, &m8122_MI);
		return;
	}
}
 void m8096 (t1504 * __this, int64_t p0, int32_t p1, int64_t p2, MethodInfo* method){
	t1509 * V_0 = {0};
	{
		if ((((int64_t)p0) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_0, (t7*) &_stringLiteral1659, (t7*) &_stringLiteral1660, &m3968_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		if ((((int64_t)p2) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_002a;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_1, (t7*) &_stringLiteral1661, (t7*) &_stringLiteral1662, &m3968_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		t1507 * L_2 = m8090(__this, p0, &m8090_MI);
		t1507 * L_3 = m8090(__this, p2, &m8090_MI);
		t1509 * L_4 = (t1509 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1509_TI));
		m8104(L_4, L_2, p1, L_3, &m8104_MI);
		V_0 = L_4;
		m8095(__this, V_0, &m8095_MI);
		return;
	}
}
 void m8097 (t1504 * __this, int64_t p0, t841* p1, int64_t p2, MethodInfo* method){
	t1510 * V_0 = {0};
	{
		if ((((int64_t)p0) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_0, (t7*) &_stringLiteral1659, (t7*) &_stringLiteral1660, &m3968_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		if ((((int64_t)p2) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_002a;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_1, (t7*) &_stringLiteral1661, (t7*) &_stringLiteral1662, &m3968_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		if (p1)
		{
			goto IL_0038;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral1013, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0038:
	{
		t1507 * L_3 = m8090(__this, p0, &m8090_MI);
		t1507 * L_4 = m8090(__this, p2, &m8090_MI);
		t1510 * L_5 = (t1510 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1510_TI));
		m8106(L_5, L_3, p1, L_4, &m8106_MI);
		V_0 = L_5;
		m8095(__this, V_0, &m8095_MI);
		return;
	}
}
 void m8098 (t1504 * __this, int64_t p0, t7* p1, int64_t p2, MethodInfo* method){
	t1512 * V_0 = {0};
	{
		if ((((int64_t)p0) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_0, (t7*) &_stringLiteral1663, (t7*) &_stringLiteral1664, &m3968_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		if ((((int64_t)p2) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_002a;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_1, (t7*) &_stringLiteral1661, (t7*) &_stringLiteral1662, &m3968_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		if (p1)
		{
			goto IL_0038;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral1665, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0038:
	{
		t1507 * L_3 = m8090(__this, p0, &m8090_MI);
		t1507 * L_4 = m8090(__this, p2, &m8090_MI);
		t1512 * L_5 = (t1512 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1512_TI));
		m8110(L_5, L_3, p1, L_4, &m8110_MI);
		V_0 = L_5;
		m8095(__this, V_0, &m8095_MI);
		return;
	}
}
 void m8099 (t1504 * __this, int64_t p0, t296 * p1, int64_t p2, MethodInfo* method){
	t1511 * V_0 = {0};
	{
		if ((((int64_t)p0) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_0, (t7*) &_stringLiteral1663, (t7*) &_stringLiteral1664, &m3968_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		if ((((int64_t)p2) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_002a;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_1, (t7*) &_stringLiteral1661, (t7*) &_stringLiteral1662, &m3968_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		if (p1)
		{
			goto IL_0038;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral1666, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0038:
	{
		t1507 * L_3 = m8090(__this, p0, &m8090_MI);
		t1507 * L_4 = m8090(__this, p2, &m8090_MI);
		t1511 * L_5 = (t1511 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1511_TI));
		m8108(L_5, L_3, p1, L_4, &m8108_MI);
		V_0 = L_5;
		m8095(__this, V_0, &m8095_MI);
		return;
	}
}
 void m8100 (t1504 * __this, t29 * p0, t1507 * p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1199, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		bool L_1 = m8118(p1, &m8118_MI);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		t29 * L_2 = (p1->f1);
		if ((((t29 *)L_2) == ((t29 *)p0)))
		{
			goto IL_003f;
		}
	}
	{
		int64_t L_3 = (p1->f3);
		int64_t L_4 = L_3;
		t29 * L_5 = Box(InitializedTypeInfo(&t919_TI), &L_4);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_6 = m1295(NULL, (t7*) &_stringLiteral1667, L_5, (t7*) &_stringLiteral1668, &m1295_MI);
		t917 * L_7 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_7, L_6, &m3986_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_003f:
	{
		return;
	}

IL_0040:
	{
		p1->f2 = p0;
		p1->f1 = p0;
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t2024_TI))))
		{
			goto IL_005f;
		}
	}
	{
		p1->f0 = 1;
		goto IL_0066;
	}

IL_005f:
	{
		p1->f0 = 3;
	}

IL_0066:
	{
		t29 * L_8 = (__this->f6);
		if (!L_8)
		{
			goto IL_00a0;
		}
	}
	{
		t29 * L_9 = (__this->f6);
		t42 * L_10 = m1430(p0, &m1430_MI);
		t735  L_11 = (__this->f7);
		t29 ** L_12 = &(p1->f7);
		t29 * L_13 = (t29 *)InterfaceFuncInvoker3< t29 *, t42 *, t735 , t29 ** >::Invoke(&m10205_MI, L_9, L_10, L_11, L_12);
		p1->f6 = L_13;
		t29 * L_14 = (p1->f6);
		if (!L_14)
		{
			goto IL_00a0;
		}
	}
	{
		p1->f0 = 1;
	}

IL_00a0:
	{
		m8119(p1, 1, __this, 0, &m8119_MI);
		m8119(p1, 0, __this, 0, &m8119_MI);
		int32_t L_15 = (__this->f8);
		__this->f8 = ((int32_t)(L_15+1));
		t1507 * L_16 = (__this->f0);
		if (L_16)
		{
			goto IL_00da;
		}
	}
	{
		__this->f0 = p1;
		__this->f1 = p1;
		goto IL_00ed;
	}

IL_00da:
	{
		t1507 * L_17 = (__this->f1);
		L_17->f12 = p1;
		__this->f1 = p1;
	}

IL_00ed:
	{
		return;
	}
}
 void m8101 (t1504 * __this, t29 * p0, int64_t p1, t733 * p2, int64_t p3, t296 * p4, t841* p5, MethodInfo* method){
	t1507 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m3966(L_0, (t7*) &_stringLiteral1199, (t7*) &_stringLiteral1669, &m3966_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0013:
	{
		if ((((int64_t)p1) > ((int64_t)(((int64_t)0)))))
		{
			goto IL_0028;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_1, (t7*) &_stringLiteral1657, (t7*) &_stringLiteral1658, &m3968_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0028:
	{
		t1507 * L_2 = m8090(__this, p1, &m8090_MI);
		V_0 = L_2;
		V_0->f4 = p2;
		V_0->f5 = p3;
		V_0->f8 = p4;
		V_0->f9 = p5;
		m8100(__this, p0, V_0, &m8100_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.ObjectManager
extern Il2CppType t1507_0_0_1;
FieldInfo t1504_f0_FieldInfo = 
{
	"_objectRecordChain", &t1507_0_0_1, &t1504_TI, offsetof(t1504, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1507_0_0_1;
FieldInfo t1504_f1_FieldInfo = 
{
	"_lastObjectRecord", &t1507_0_0_1, &t1504_TI, offsetof(t1504, f1), &EmptyCustomAttributesCache};
extern Il2CppType t731_0_0_1;
FieldInfo t1504_f2_FieldInfo = 
{
	"_deserializedRecords", &t731_0_0_1, &t1504_TI, offsetof(t1504, f2), &EmptyCustomAttributesCache};
extern Il2CppType t731_0_0_1;
FieldInfo t1504_f3_FieldInfo = 
{
	"_onDeserializedCallbackRecords", &t731_0_0_1, &t1504_TI, offsetof(t1504, f3), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_1;
FieldInfo t1504_f4_FieldInfo = 
{
	"_objectRecords", &t719_0_0_1, &t1504_TI, offsetof(t1504, f4), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1504_f5_FieldInfo = 
{
	"_finalFixup", &t40_0_0_1, &t1504_TI, offsetof(t1504, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1458_0_0_1;
FieldInfo t1504_f6_FieldInfo = 
{
	"_selector", &t1458_0_0_1, &t1504_TI, offsetof(t1504, f6), &EmptyCustomAttributesCache};
extern Il2CppType t735_0_0_1;
FieldInfo t1504_f7_FieldInfo = 
{
	"_context", &t735_0_0_1, &t1504_TI, offsetof(t1504, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1504_f8_FieldInfo = 
{
	"_registeredObjectsCount", &t44_0_0_1, &t1504_TI, offsetof(t1504, f8), &EmptyCustomAttributesCache};
static FieldInfo* t1504_FIs[] =
{
	&t1504_f0_FieldInfo,
	&t1504_f1_FieldInfo,
	&t1504_f2_FieldInfo,
	&t1504_f3_FieldInfo,
	&t1504_f4_FieldInfo,
	&t1504_f5_FieldInfo,
	&t1504_f6_FieldInfo,
	&t1504_f7_FieldInfo,
	&t1504_f8_FieldInfo,
	NULL
};
extern Il2CppType t1458_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1504_m8088_ParameterInfos[] = 
{
	{"selector", 0, 134222192, &EmptyCustomAttributesCache, &t1458_0_0_0},
	{"context", 1, 134222193, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m8088_MI = 
{
	".ctor", (methodPointerType)&m8088, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1504_m8088_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3684, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8089_MI = 
{
	"DoFixups", (methodPointerType)&m8089, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 4, 0, false, false, 3685, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
static ParameterInfo t1504_m8090_ParameterInfos[] = 
{
	{"objectID", 0, 134222194, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t1507_0_0_0;
extern void* RuntimeInvoker_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m8090_MI = 
{
	"GetObjectRecord", (methodPointerType)&m8090, &t1504_TI, &t1507_0_0_0, RuntimeInvoker_t29_t919, t1504_m8090_ParameterInfos, &EmptyCustomAttributesCache, 131, 0, 255, 1, false, false, 3686, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
static ParameterInfo t1504_m8091_ParameterInfos[] = 
{
	{"objectID", 0, 134222195, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m8091_MI = 
{
	"GetObject", (methodPointerType)&m8091, &t1504_TI, &t29_0_0_0, RuntimeInvoker_t29_t919, t1504_m8091_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 5, 1, false, false, 3687, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m8092_MI = 
{
	"RaiseDeserializationEvent", (methodPointerType)&m8092, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 6, 0, false, false, 3688, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1504_m8093_ParameterInfos[] = 
{
	{"obj", 0, 134222196, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8093_MI = 
{
	"RaiseOnDeserializingEvent", (methodPointerType)&m8093, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1504_m8093_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 3689, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1504_m8094_ParameterInfos[] = 
{
	{"obj", 0, 134222197, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8094_MI = 
{
	"RaiseOnDeserializedEvent", (methodPointerType)&m8094, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1504_m8094_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 3690, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1508_0_0_0;
extern Il2CppType t1508_0_0_0;
static ParameterInfo t1504_m8095_ParameterInfos[] = 
{
	{"record", 0, 134222198, &EmptyCustomAttributesCache, &t1508_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8095_MI = 
{
	"AddFixup", (methodPointerType)&m8095, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1504_m8095_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 3691, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t1504_m8096_ParameterInfos[] = 
{
	{"arrayToBeFixed", 0, 134222199, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"index", 1, 134222200, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"objectRequired", 2, 134222201, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919_t44_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m8096_MI = 
{
	"RecordArrayElementFixup", (methodPointerType)&m8096, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t919_t44_t919, t1504_m8096_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 3, false, false, 3692, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t1504_m8097_ParameterInfos[] = 
{
	{"arrayToBeFixed", 0, 134222202, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"indices", 1, 134222203, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"objectRequired", 2, 134222204, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m8097_MI = 
{
	"RecordArrayElementFixup", (methodPointerType)&m8097, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t919_t29_t919, t1504_m8097_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 8, 3, false, false, 3693, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t1504_m8098_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222205, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"memberName", 1, 134222206, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"objectRequired", 2, 134222207, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m8098_MI = 
{
	"RecordDelayedFixup", (methodPointerType)&m8098, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t919_t29_t919, t1504_m8098_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 9, 3, false, false, 3694, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t919_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t1504_m8099_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222208, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"member", 1, 134222209, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"objectRequired", 2, 134222210, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t919_t29_t919 (MethodInfo* method, void* obj, void** args);
MethodInfo m8099_MI = 
{
	"RecordFixup", (methodPointerType)&m8099, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t919_t29_t919, t1504_m8099_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 10, 3, false, false, 3695, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1507_0_0_0;
extern Il2CppType t1507_0_0_0;
static ParameterInfo t1504_m8100_ParameterInfos[] = 
{
	{"obj", 0, 134222211, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"record", 1, 134222212, &EmptyCustomAttributesCache, &t1507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8100_MI = 
{
	"RegisterObjectInternal", (methodPointerType)&m8100, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1504_m8100_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3696, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t841_0_0_0;
static ParameterInfo t1504_m8101_ParameterInfos[] = 
{
	{"obj", 0, 134222213, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"objectID", 1, 134222214, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"info", 2, 134222215, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"idOfContainingObj", 3, 134222216, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"member", 4, 134222217, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"arrayIndex", 5, 134222218, &EmptyCustomAttributesCache, &t841_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t919_t29_t919_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8101_MI = 
{
	"RegisterObject", (methodPointerType)&m8101, &t1504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t919_t29_t919_t29_t29, t1504_m8101_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 6, false, false, 3697, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1504_MIs[] =
{
	&m8088_MI,
	&m8089_MI,
	&m8090_MI,
	&m8091_MI,
	&m8092_MI,
	&m8093_MI,
	&m8094_MI,
	&m8095_MI,
	&m8096_MI,
	&m8097_MI,
	&m8098_MI,
	&m8099_MI,
	&m8100_MI,
	&m8101_MI,
	NULL
};
static MethodInfo* t1504_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8089_MI,
	&m8091_MI,
	&m8092_MI,
	&m8096_MI,
	&m8097_MI,
	&m8098_MI,
	&m8099_MI,
};
void t1504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1504__CustomAttributeCache = {
1,
NULL,
&t1504_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1504_0_0_0;
extern Il2CppType t1504_1_0_0;
struct t1504;
extern CustomAttributesCache t1504__CustomAttributeCache;
TypeInfo t1504_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObjectManager", "System.Runtime.Serialization", t1504_MIs, NULL, t1504_FIs, NULL, &t29_TI, NULL, NULL, &t1504_TI, NULL, t1504_VT, &t1504__CustomAttributeCache, &t1504_TI, &t1504_0_0_0, &t1504_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1504), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 14, 0, 9, 0, 0, 11, 0, 0};
#ifndef _MSC_VER
#else
#endif
#include "t1508MD.h"

extern MethodInfo m8116_MI;
extern MethodInfo m10221_MI;


extern MethodInfo m8102_MI;
 void m8102 (t1508 * __this, t1507 * p0, t1507 * p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
extern MethodInfo m8103_MI;
 bool m8103 (t1508 * __this, t1504 * p0, bool p1, MethodInfo* method){
	{
		t1507 * L_0 = (__this->f0);
		bool L_1 = m8118(L_0, &m8118_MI);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		t1507 * L_2 = (__this->f1);
		bool L_3 = m8116(L_2, &m8116_MI);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		VirtActionInvoker1< t1504 * >::Invoke(&m10221_MI, __this, p0);
		return 1;
	}

IL_0023:
	{
		if (!p1)
		{
			goto IL_008c;
		}
	}
	{
		t1507 * L_4 = (__this->f0);
		bool L_5 = m8118(L_4, &m8118_MI);
		if (L_5)
		{
			goto IL_0058;
		}
	}
	{
		t1507 * L_6 = (__this->f0);
		int64_t L_7 = (L_6->f3);
		int64_t L_8 = L_7;
		t29 * L_9 = Box(InitializedTypeInfo(&t919_TI), &L_8);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_10 = m1295(NULL, (t7*) &_stringLiteral1670, L_9, (t7*) &_stringLiteral1671, &m1295_MI);
		t917 * L_11 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_11, L_10, &m3986_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0058:
	{
		t1507 * L_12 = (__this->f1);
		bool L_13 = m8118(L_12, &m8118_MI);
		if (L_13)
		{
			goto IL_008a;
		}
	}
	{
		t1507 * L_14 = (__this->f1);
		int64_t L_15 = (L_14->f3);
		int64_t L_16 = L_15;
		t29 * L_17 = Box(InitializedTypeInfo(&t919_TI), &L_16);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_18 = m1295(NULL, (t7*) &_stringLiteral1670, L_17, (t7*) &_stringLiteral1671, &m1295_MI);
		t917 * L_19 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_19, L_18, &m3986_MI);
		il2cpp_codegen_raise_exception(L_19);
	}

IL_008a:
	{
		return 0;
	}

IL_008c:
	{
		return 0;
	}
}
// Metadata Definition System.Runtime.Serialization.BaseFixupRecord
extern Il2CppType t1507_0_0_5;
FieldInfo t1508_f0_FieldInfo = 
{
	"ObjectToBeFixed", &t1507_0_0_5, &t1508_TI, offsetof(t1508, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1507_0_0_5;
FieldInfo t1508_f1_FieldInfo = 
{
	"ObjectRequired", &t1507_0_0_5, &t1508_TI, offsetof(t1508, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1508_0_0_6;
FieldInfo t1508_f2_FieldInfo = 
{
	"NextSameContainer", &t1508_0_0_6, &t1508_TI, offsetof(t1508, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1508_0_0_6;
FieldInfo t1508_f3_FieldInfo = 
{
	"NextSameRequired", &t1508_0_0_6, &t1508_TI, offsetof(t1508, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1508_FIs[] =
{
	&t1508_f0_FieldInfo,
	&t1508_f1_FieldInfo,
	&t1508_f2_FieldInfo,
	&t1508_f3_FieldInfo,
	NULL
};
extern Il2CppType t1507_0_0_0;
extern Il2CppType t1507_0_0_0;
static ParameterInfo t1508_m8102_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222219, &EmptyCustomAttributesCache, &t1507_0_0_0},
	{"objectRequired", 1, 134222220, &EmptyCustomAttributesCache, &t1507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8102_MI = 
{
	".ctor", (methodPointerType)&m8102, &t1508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1508_m8102_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3698, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1504_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1508_m8103_ParameterInfos[] = 
{
	{"manager", 0, 134222221, &EmptyCustomAttributesCache, &t1504_0_0_0},
	{"strict", 1, 134222222, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m8103_MI = 
{
	"DoFixup", (methodPointerType)&m8103, &t1508_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1508_m8103_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 3699, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1504_0_0_0;
static ParameterInfo t1508_m10221_ParameterInfos[] = 
{
	{"manager", 0, 134222223, &EmptyCustomAttributesCache, &t1504_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10221_MI = 
{
	"FixupImpl", NULL, &t1508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1508_m10221_ParameterInfos, &EmptyCustomAttributesCache, 1476, 0, 4, 1, false, false, 3700, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1508_MIs[] =
{
	&m8102_MI,
	&m8103_MI,
	&m10221_MI,
	NULL
};
static MethodInfo* t1508_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1508_1_0_0;
struct t1508;
TypeInfo t1508_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BaseFixupRecord", "System.Runtime.Serialization", t1508_MIs, NULL, t1508_FIs, NULL, &t29_TI, NULL, NULL, &t1508_TI, NULL, t1508_VT, &EmptyCustomAttributesCache, &t1508_TI, &t1508_0_0_0, &t1508_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1508), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 4, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m3971_MI;


 void m8104 (t1509 * __this, t1507 * p0, int32_t p1, t1507 * p2, MethodInfo* method){
	{
		m8102(__this, p0, p2, &m8102_MI);
		__this->f4 = p1;
		return;
	}
}
extern MethodInfo m8105_MI;
 void m8105 (t1509 * __this, t1504 * p0, MethodInfo* method){
	t20 * V_0 = {0};
	{
		t1507 * L_0 = (__this->f0);
		t29 * L_1 = (L_0->f2);
		V_0 = ((t20 *)Castclass(L_1, InitializedTypeInfo(&t20_TI)));
		t1507 * L_2 = (__this->f1);
		t29 * L_3 = (L_2->f2);
		int32_t L_4 = (__this->f4);
		m3971(V_0, L_3, L_4, &m3971_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.ArrayFixupRecord
extern Il2CppType t44_0_0_1;
FieldInfo t1509_f4_FieldInfo = 
{
	"_index", &t44_0_0_1, &t1509_TI, offsetof(t1509, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1509_FIs[] =
{
	&t1509_f4_FieldInfo,
	NULL
};
extern Il2CppType t1507_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1507_0_0_0;
static ParameterInfo t1509_m8104_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222224, &EmptyCustomAttributesCache, &t1507_0_0_0},
	{"index", 1, 134222225, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"objectRequired", 2, 134222226, &EmptyCustomAttributesCache, &t1507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8104_MI = 
{
	".ctor", (methodPointerType)&m8104, &t1509_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t29, t1509_m8104_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 3701, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1504_0_0_0;
static ParameterInfo t1509_m8105_ParameterInfos[] = 
{
	{"manager", 0, 134222227, &EmptyCustomAttributesCache, &t1504_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8105_MI = 
{
	"FixupImpl", (methodPointerType)&m8105, &t1509_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1509_m8105_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 4, 1, false, false, 3702, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1509_MIs[] =
{
	&m8104_MI,
	&m8105_MI,
	NULL
};
static MethodInfo* t1509_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8105_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1509_0_0_0;
extern Il2CppType t1509_1_0_0;
struct t1509;
TypeInfo t1509_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ArrayFixupRecord", "System.Runtime.Serialization", t1509_MIs, NULL, t1509_FIs, NULL, &t1508_TI, NULL, NULL, &t1509_TI, NULL, t1509_VT, &EmptyCustomAttributesCache, &t1509_TI, &t1509_0_0_0, &t1509_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1509), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 5, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m8114_MI;


 void m8106 (t1510 * __this, t1507 * p0, t841* p1, t1507 * p2, MethodInfo* method){
	{
		m8102(__this, p0, p2, &m8102_MI);
		__this->f4 = p1;
		return;
	}
}
extern MethodInfo m8107_MI;
 void m8107 (t1510 * __this, t1504 * p0, MethodInfo* method){
	{
		t1507 * L_0 = (__this->f0);
		t1507 * L_1 = (__this->f1);
		t29 * L_2 = (L_1->f2);
		t841* L_3 = (__this->f4);
		m8114(L_0, p0, L_2, L_3, &m8114_MI);
		return;
	}
}
// Metadata Definition System.Runtime.Serialization.MultiArrayFixupRecord
extern Il2CppType t841_0_0_1;
FieldInfo t1510_f4_FieldInfo = 
{
	"_indices", &t841_0_0_1, &t1510_TI, offsetof(t1510, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1510_FIs[] =
{
	&t1510_f4_FieldInfo,
	NULL
};
extern Il2CppType t1507_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t1507_0_0_0;
static ParameterInfo t1510_m8106_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222228, &EmptyCustomAttributesCache, &t1507_0_0_0},
	{"indices", 1, 134222229, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"objectRequired", 2, 134222230, &EmptyCustomAttributesCache, &t1507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8106_MI = 
{
	".ctor", (methodPointerType)&m8106, &t1510_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1510_m8106_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 3703, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1504_0_0_0;
static ParameterInfo t1510_m8107_ParameterInfos[] = 
{
	{"manager", 0, 134222231, &EmptyCustomAttributesCache, &t1504_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8107_MI = 
{
	"FixupImpl", (methodPointerType)&m8107, &t1510_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1510_m8107_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 4, 1, false, false, 3704, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1510_MIs[] =
{
	&m8106_MI,
	&m8107_MI,
	NULL
};
static MethodInfo* t1510_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m8107_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1510_0_0_0;
extern Il2CppType t1510_1_0_0;
struct t1510;
TypeInfo t1510_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MultiArrayFixupRecord", "System.Runtime.Serialization", t1510_MIs, NULL, t1510_FIs, NULL, &t1508_TI, NULL, NULL, &t1510_TI, NULL, t1510_VT, &EmptyCustomAttributesCache, &t1510_TI, &t1510_0_0_0, &t1510_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1510), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 5, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
