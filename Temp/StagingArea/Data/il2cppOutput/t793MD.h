﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t793;
struct t781;
struct t7;

 void m4072 (t793 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4073 (t793 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4056 (t793 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4060 (t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4057 (t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4090 (t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4059 (t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4370 (t793 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4371 (t793 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4089 (t793 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4074 (t793 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4076 (t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4372 (t793 * __this, t781* p0, int32_t* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4373 (t793 * __this, t781* p0, int32_t* p1, uint8_t* p2, int32_t* p3, t781** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4061 (t793 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4374 (t793 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4375 (t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
