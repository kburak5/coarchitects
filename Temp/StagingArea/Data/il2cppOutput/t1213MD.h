﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1213;
struct t7;
struct t781;

 void m6368 (t1213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6369 (t1213 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6370 (t1213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6371 (t1213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6372 (t1213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6373 (t1213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6374 (t1213 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
