﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t866;
struct t866_marshaled;
struct t29;
#include "t866.h"

 void m3688 (t866 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t866  m3689 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3690 (t866 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3691 (t866 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3692 (t866 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3693 (t866 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3694 (t866 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3695 (t866 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3696 (t866 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3697 (t866 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3698 (t866 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3699 (t866 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3700 (t866 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t866_marshal(const t866& unmarshaled, t866_marshaled& marshaled);
void t866_marshal_back(const t866_marshaled& marshaled, t866& unmarshaled);
void t866_marshal_cleanup(t866_marshaled& marshaled);
