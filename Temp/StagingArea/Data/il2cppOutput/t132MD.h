﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t132;
struct t7;
struct t29;
#include "t132.h"
#include "t183.h"

 void m1734 (t132 * __this, float p0, float p1, float p2, float p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2331 (t132 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2332 (t132 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2333 (t132 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1592 (t132 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1497 (t29 * __this, t132  p0, t132  p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m2334 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1559 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1596 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1882 (t29 * __this, t132  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m2335 (t29 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
