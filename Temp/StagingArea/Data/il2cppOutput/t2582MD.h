﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2582;
struct t29;
struct t3;
struct t366;
struct t66;
struct t67;
#include "t35.h"
#include "t2562.h"

 void m13928 (t2582 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2562  m13929 (t2582 * __this, t3 * p0, t366 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13930 (t2582 * __this, t3 * p0, t366 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2562  m13931 (t2582 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
