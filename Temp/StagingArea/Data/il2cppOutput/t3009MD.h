﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3009;
struct t29;
struct t20;
#include "t512.h"

 void m16449 (t3009 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16450 (t3009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16451 (t3009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16452 (t3009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16453 (t3009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
