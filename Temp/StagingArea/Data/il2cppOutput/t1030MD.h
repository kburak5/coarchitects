﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1030;
struct t1031;
struct t7;
struct t762;
#include "t1026.h"

 void m4676 (t1030 * __this, t1031 * p0, int32_t p1, t7* p2, t762 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1031 * m4677 (t1030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m4678 (t1030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4679 (t1030 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4680 (t1030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
