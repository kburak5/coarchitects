﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3411;
struct t29;
struct t20;
#include "t1403.h"

 void m18925 (t3411 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18926 (t3411 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18927 (t3411 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18928 (t3411 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18929 (t3411 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
