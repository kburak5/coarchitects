﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2316;
struct t29;
struct t20;
#include "t61.h"

 void m11791 (t2316 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11792 (t2316 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11793 (t2316 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11794 (t2316 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11795 (t2316 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
