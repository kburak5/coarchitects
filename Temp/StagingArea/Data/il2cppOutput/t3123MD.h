﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3123;
struct t29;
struct t557;
struct t316;

 void m17254_gshared (t3123 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17254(__this, p0, p1, method) (void)m17254_gshared((t3123 *)__this, (t29 *)p0, (t557 *)p1, method)
 void m17255_gshared (t3123 * __this, t316* p0, MethodInfo* method);
#define m17255(__this, p0, method) (void)m17255_gshared((t3123 *)__this, (t316*)p0, method)
 bool m17256_gshared (t3123 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17256(__this, p0, p1, method) (bool)m17256_gshared((t3123 *)__this, (t29 *)p0, (t557 *)p1, method)
