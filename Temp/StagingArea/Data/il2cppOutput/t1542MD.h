﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1542;
struct t781;
struct t7;
struct t777;

 void m8346 (t1542 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8347 (t1542 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8348 (t1542 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8349 (t1542 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
