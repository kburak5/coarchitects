﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2197;
struct t29;
struct t20;

 void m10751 (t2197 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m10752 (t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m10753 (t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m10754 (t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m10755 (t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
