﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t51;
struct t29;
struct t52;
struct t2173;
struct t20;
struct t136;
struct t2174;
struct t2175;
struct t2176;
struct t2172;
struct t2177;
struct t2178;
#include "t2179.h"

#include "t294MD.h"
#define m1288(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m10538(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m10540(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m10542(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m10544(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m10546(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m10548(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m10550(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m10552(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m10554(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m10556(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m10558(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m10560(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m10562(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m10564(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m10566(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m10568(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m10570(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m10572(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m10574(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m10576(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m10578(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m10580(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m10582(__this, method) (t2176 *)m10583_gshared((t294 *)__this, method)
#define m10584(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m10586(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m10588(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m10590(__this, p0, method) (t52 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m10592(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m10594(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2179  m10596 (t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m10597(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m10599(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m10601(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m10603(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m10605(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m10607(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m10609(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m1294(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m10612(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m10614(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m10616(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m10618(__this, method) (t2172*)m10619_gshared((t294 *)__this, method)
#define m10620(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m10622(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m10624(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1291(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1292(__this, p0, method) (t52 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m10628(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
