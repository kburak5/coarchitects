﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t819;
struct t29;
struct t789;
struct t731;

 void m3434 (t819 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3435 (t819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t789 * m3436 (t819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3437 (t819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
