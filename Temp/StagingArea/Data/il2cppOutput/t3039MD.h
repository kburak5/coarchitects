﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3039;
struct t29;
#include "t381.h"

 void m16661 (t3039 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16662 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16663 (t3039 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3039 * m16664 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
