﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1054;
struct t777;
struct t781;
struct t7;

 void m4826 (t1054 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4827 (t1054 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4828 (t1054 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4829 (t1054 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
