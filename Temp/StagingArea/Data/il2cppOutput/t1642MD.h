﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1642;
struct t719;
struct t42;
#include "t1642.h"

 void m9232 (t1642 * __this, t1642  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9233 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9234 (t29 * __this, t42 * p0, t1642 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t719 * m9235 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9236 (t29 * __this, t42 * p0, t1642 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
