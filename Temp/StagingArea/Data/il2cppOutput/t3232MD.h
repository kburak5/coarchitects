﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3232;
struct t29;
struct t20;
#include "t859.h"

 void m17956 (t3232 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17957 (t3232 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17958 (t3232 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17959 (t3232 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t859  m17960 (t3232 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
