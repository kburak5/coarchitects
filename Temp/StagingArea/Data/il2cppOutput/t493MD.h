﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t493;
struct t493_marshaled;

void t493_marshal(const t493& unmarshaled, t493_marshaled& marshaled);
void t493_marshal_back(const t493_marshaled& marshaled, t493& unmarshaled);
void t493_marshal_cleanup(t493_marshaled& marshaled);
