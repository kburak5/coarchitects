﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2609;
struct t29;
struct t20;
#include "t187.h"

 void m14054 (t2609 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14055 (t2609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14056 (t2609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14057 (t2609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14058 (t2609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
