﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t863;
struct t764;
struct t828;
struct t829;
struct t7;
struct t833;
#include "t862.h"
#include "t845.h"

 void m3667 (t863 * __this, t764* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3668 (t863 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3669 (t863 * __this, t829 * p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3670 (t863 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3671 (t863 * __this, int32_t p0, int32_t* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3672 (t863 * __this, int32_t p0, int32_t* p1, int32_t* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3673 (t863 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3674 (t863 * __this, uint16_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3675 (t863 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3676 (t863 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3677 (t863 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3678 (t863 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3679 (t863 * __this, int32_t p0, int32_t p1, bool p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3680 (t863 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3681 (t863 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3682 (t863 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3683 (t863 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3684 (t863 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3685 (t863 * __this, int32_t p0, int32_t* p1, int32_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3686 (t863 * __this, t833 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3687 (t863 * __this, t829 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
