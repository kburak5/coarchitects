﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t5;
struct t6;
struct t7;

 void m2 (t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3 (t5 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4 (t5 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5 (t5 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
