﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1223;
struct t1231;
struct t781;
struct t1219;

 void m6520 (t1223 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6521 (t1223 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1231 * m6522 (t1223 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6523 (t1223 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
