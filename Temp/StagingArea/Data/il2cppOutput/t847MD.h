﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t847;
struct t29;

 void m3584 (t847 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3585 (t847 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3586 (t847 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
