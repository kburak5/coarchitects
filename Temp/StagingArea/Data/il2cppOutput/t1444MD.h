﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1444;
struct t7;
struct t316;
struct t1449;
struct t636;
struct t29;
struct t721;
struct t537;
struct t1451;
struct t733;
struct t42;
#include "t735.h"

 void m7860 (t1444 * __this, t1451* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7861 (t1444 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7862 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7863 (t1444 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7864 (t1444 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7865 (t1444 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7866 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1449 * m7867 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7868 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7869 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7870 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7871 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7872 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7873 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7874 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7875 (t1444 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7876 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7877 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7878 (t1444 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7879 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7880 (t1444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
