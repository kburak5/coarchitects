﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t818;
struct t778;
struct t781;
struct t7;
#include "t790.h"

 void m3416 (t818 * __this, t778 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3417 (t818 * __this, t778 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3418 (t818 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3419 (t818 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
