﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t211;
struct t162;
struct t163;
#include "t164.h"

 void m719 (t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m720 (t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m721 (t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m722 (t211 * __this, t162 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m723 (t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m724 (t211 * __this, t164  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m725 (t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m726 (t211 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
