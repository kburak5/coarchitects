﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1629;
struct t733;
struct t29;
struct t7;
struct t1094;
#include "t465.h"
#include "t816.h"
#include "t735.h"
#include "t1629.h"

 void m9184 (t1629 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9185 (t1629 * __this, t465  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9186 (t1629 * __this, int64_t p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9187 (t1629 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9188 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9189 (t1629 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9190 (t1629 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9191 (t1629 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9192 (t1629 * __this, t1629  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9193 (t1629 * __this, t1629  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9194 (t1629 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9195 (t1629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9196 (t1629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9197 (t1629 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9198 (t1629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9199 (t1629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9200 (t1629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
