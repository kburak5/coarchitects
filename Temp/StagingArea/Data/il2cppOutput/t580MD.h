﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t580;
struct t29;
struct t20;
struct t136;

 void m3017 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6770 (t580 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3019 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3020 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3021 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2849 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3022 (t580 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3018 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3023 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3024 (t580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2845 (t580 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
