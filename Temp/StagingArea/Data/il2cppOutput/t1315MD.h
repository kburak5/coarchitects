﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1315;
struct t29;
struct t781;
struct t66;
struct t67;
#include "t35.h"

 void m7022 (t1315 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7023 (t1315 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7024 (t1315 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7025 (t1315 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
