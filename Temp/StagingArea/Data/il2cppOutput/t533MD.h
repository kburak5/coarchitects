﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t533;
struct t42;
struct t537;

 void m2732 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m2733 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m2734 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2735 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
