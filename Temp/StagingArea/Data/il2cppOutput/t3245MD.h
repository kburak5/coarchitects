﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3245;
struct t29;
struct t20;
#include "t1003.h"

 void m18021 (t3245 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18022 (t3245 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18023 (t3245 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18024 (t3245 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18025 (t3245 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
