﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t606;
struct t29;
#include "t737.h"

 void m2886 (t606 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3147 (t606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3148 (t606 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3149 (t606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
