﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1415;
struct t1416;
struct t29;
struct t42;
struct t316;
struct t1417;
struct t7;

 t29 * m7764 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7765 (t29 * __this, t42 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1417 * m7766 (t29 * __this, t42 * p0, t7* p1, t316* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7767 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
