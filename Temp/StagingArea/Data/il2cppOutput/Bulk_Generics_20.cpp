﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4452_TI;

#include "t532.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.WrapperlessIcall>
extern MethodInfo m29870_MI;
static PropertyInfo t4452____Current_PropertyInfo = 
{
	&t4452_TI, "Current", &m29870_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4452_PIs[] =
{
	&t4452____Current_PropertyInfo,
	NULL
};
extern Il2CppType t532_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29870_GM;
MethodInfo m29870_MI = 
{
	"get_Current", NULL, &t4452_TI, &t532_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29870_GM};
static MethodInfo* t4452_MIs[] =
{
	&m29870_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4452_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4452_0_0_0;
extern Il2CppType t4452_1_0_0;
struct t4452;
extern Il2CppGenericClass t4452_GC;
TypeInfo t4452_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4452_MIs, t4452_PIs, NULL, NULL, NULL, NULL, NULL, &t4452_TI, t4452_ITIs, NULL, &EmptyCustomAttributesCache, &t4452_TI, &t4452_0_0_0, &t4452_1_0_0, NULL, &t4452_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3063.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3063_TI;
#include "t3063MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t532_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m16857_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m22741_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m22741(__this, p0, method) (t532 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.WrapperlessIcall>
extern Il2CppType t20_0_0_1;
FieldInfo t3063_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3063_TI, offsetof(t3063, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3063_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3063_TI, offsetof(t3063, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3063_FIs[] =
{
	&t3063_f0_FieldInfo,
	&t3063_f1_FieldInfo,
	NULL
};
extern MethodInfo m16854_MI;
static PropertyInfo t3063____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3063_TI, "System.Collections.IEnumerator.Current", &m16854_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3063____Current_PropertyInfo = 
{
	&t3063_TI, "Current", &m16857_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3063_PIs[] =
{
	&t3063____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3063____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3063_m16853_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16853_GM;
MethodInfo m16853_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3063_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3063_m16853_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16853_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16854_GM;
MethodInfo m16854_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3063_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16854_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16855_GM;
MethodInfo m16855_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3063_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16855_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16856_GM;
MethodInfo m16856_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3063_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16856_GM};
extern Il2CppType t532_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16857_GM;
MethodInfo m16857_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3063_TI, &t532_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16857_GM};
static MethodInfo* t3063_MIs[] =
{
	&m16853_MI,
	&m16854_MI,
	&m16855_MI,
	&m16856_MI,
	&m16857_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m16856_MI;
extern MethodInfo m16855_MI;
static MethodInfo* t3063_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16854_MI,
	&m16856_MI,
	&m16855_MI,
	&m16857_MI,
};
static TypeInfo* t3063_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4452_TI,
};
static Il2CppInterfaceOffsetPair t3063_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4452_TI, 7},
};
extern TypeInfo t532_TI;
static Il2CppRGCTXData t3063_RGCTXData[3] = 
{
	&m16857_MI/* Method Usage */,
	&t532_TI/* Class Usage */,
	&m22741_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3063_0_0_0;
extern Il2CppType t3063_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3063_GC;
extern TypeInfo t20_TI;
TypeInfo t3063_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3063_MIs, t3063_PIs, t3063_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3063_TI, t3063_ITIs, t3063_VT, &EmptyCustomAttributesCache, &t3063_TI, &t3063_0_0_0, &t3063_1_0_0, t3063_IOs, &t3063_GC, NULL, NULL, NULL, t3063_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3063)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5712_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.WrapperlessIcall>
extern MethodInfo m29871_MI;
static PropertyInfo t5712____Count_PropertyInfo = 
{
	&t5712_TI, "Count", &m29871_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29872_MI;
static PropertyInfo t5712____IsReadOnly_PropertyInfo = 
{
	&t5712_TI, "IsReadOnly", &m29872_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5712_PIs[] =
{
	&t5712____Count_PropertyInfo,
	&t5712____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29871_GM;
MethodInfo m29871_MI = 
{
	"get_Count", NULL, &t5712_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29871_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29872_GM;
MethodInfo m29872_MI = 
{
	"get_IsReadOnly", NULL, &t5712_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29872_GM};
extern Il2CppType t532_0_0_0;
extern Il2CppType t532_0_0_0;
static ParameterInfo t5712_m29873_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t532_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29873_GM;
MethodInfo m29873_MI = 
{
	"Add", NULL, &t5712_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5712_m29873_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29873_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29874_GM;
MethodInfo m29874_MI = 
{
	"Clear", NULL, &t5712_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29874_GM};
extern Il2CppType t532_0_0_0;
static ParameterInfo t5712_m29875_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t532_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29875_GM;
MethodInfo m29875_MI = 
{
	"Contains", NULL, &t5712_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5712_m29875_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29875_GM};
extern Il2CppType t3776_0_0_0;
extern Il2CppType t3776_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5712_m29876_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3776_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29876_GM;
MethodInfo m29876_MI = 
{
	"CopyTo", NULL, &t5712_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5712_m29876_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29876_GM};
extern Il2CppType t532_0_0_0;
static ParameterInfo t5712_m29877_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t532_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29877_GM;
MethodInfo m29877_MI = 
{
	"Remove", NULL, &t5712_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5712_m29877_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29877_GM};
static MethodInfo* t5712_MIs[] =
{
	&m29871_MI,
	&m29872_MI,
	&m29873_MI,
	&m29874_MI,
	&m29875_MI,
	&m29876_MI,
	&m29877_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5714_TI;
static TypeInfo* t5712_ITIs[] = 
{
	&t603_TI,
	&t5714_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5712_0_0_0;
extern Il2CppType t5712_1_0_0;
struct t5712;
extern Il2CppGenericClass t5712_GC;
TypeInfo t5712_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5712_MIs, t5712_PIs, NULL, NULL, NULL, NULL, NULL, &t5712_TI, t5712_ITIs, NULL, &EmptyCustomAttributesCache, &t5712_TI, &t5712_0_0_0, &t5712_1_0_0, NULL, &t5712_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.WrapperlessIcall>
extern Il2CppType t4452_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29878_GM;
MethodInfo m29878_MI = 
{
	"GetEnumerator", NULL, &t5714_TI, &t4452_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29878_GM};
static MethodInfo* t5714_MIs[] =
{
	&m29878_MI,
	NULL
};
static TypeInfo* t5714_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5714_0_0_0;
extern Il2CppType t5714_1_0_0;
struct t5714;
extern Il2CppGenericClass t5714_GC;
TypeInfo t5714_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5714_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5714_TI, t5714_ITIs, NULL, &EmptyCustomAttributesCache, &t5714_TI, &t5714_0_0_0, &t5714_1_0_0, NULL, &t5714_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5713_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.WrapperlessIcall>
extern MethodInfo m29879_MI;
extern MethodInfo m29880_MI;
static PropertyInfo t5713____Item_PropertyInfo = 
{
	&t5713_TI, "Item", &m29879_MI, &m29880_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5713_PIs[] =
{
	&t5713____Item_PropertyInfo,
	NULL
};
extern Il2CppType t532_0_0_0;
static ParameterInfo t5713_m29881_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t532_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29881_GM;
MethodInfo m29881_MI = 
{
	"IndexOf", NULL, &t5713_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5713_m29881_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29881_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t532_0_0_0;
static ParameterInfo t5713_m29882_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t532_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29882_GM;
MethodInfo m29882_MI = 
{
	"Insert", NULL, &t5713_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5713_m29882_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29882_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5713_m29883_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29883_GM;
MethodInfo m29883_MI = 
{
	"RemoveAt", NULL, &t5713_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5713_m29883_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29883_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5713_m29879_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t532_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29879_GM;
MethodInfo m29879_MI = 
{
	"get_Item", NULL, &t5713_TI, &t532_0_0_0, RuntimeInvoker_t29_t44, t5713_m29879_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29879_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t532_0_0_0;
static ParameterInfo t5713_m29880_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t532_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29880_GM;
MethodInfo m29880_MI = 
{
	"set_Item", NULL, &t5713_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5713_m29880_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29880_GM};
static MethodInfo* t5713_MIs[] =
{
	&m29881_MI,
	&m29882_MI,
	&m29883_MI,
	&m29879_MI,
	&m29880_MI,
	NULL
};
static TypeInfo* t5713_ITIs[] = 
{
	&t603_TI,
	&t5712_TI,
	&t5714_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5713_0_0_0;
extern Il2CppType t5713_1_0_0;
struct t5713;
extern Il2CppGenericClass t5713_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5713_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5713_MIs, t5713_PIs, NULL, NULL, NULL, NULL, NULL, &t5713_TI, t5713_ITIs, NULL, &t1908__CustomAttributeCache, &t5713_TI, &t5713_0_0_0, &t5713_1_0_0, NULL, &t5713_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4454_TI;

#include "t359.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.DisallowMultipleComponent>
extern MethodInfo m29884_MI;
static PropertyInfo t4454____Current_PropertyInfo = 
{
	&t4454_TI, "Current", &m29884_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4454_PIs[] =
{
	&t4454____Current_PropertyInfo,
	NULL
};
extern Il2CppType t359_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29884_GM;
MethodInfo m29884_MI = 
{
	"get_Current", NULL, &t4454_TI, &t359_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29884_GM};
static MethodInfo* t4454_MIs[] =
{
	&m29884_MI,
	NULL
};
static TypeInfo* t4454_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4454_0_0_0;
extern Il2CppType t4454_1_0_0;
struct t4454;
extern Il2CppGenericClass t4454_GC;
TypeInfo t4454_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4454_MIs, t4454_PIs, NULL, NULL, NULL, NULL, NULL, &t4454_TI, t4454_ITIs, NULL, &EmptyCustomAttributesCache, &t4454_TI, &t4454_0_0_0, &t4454_1_0_0, NULL, &t4454_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3064.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3064_TI;
#include "t3064MD.h"

extern TypeInfo t359_TI;
extern MethodInfo m16862_MI;
extern MethodInfo m22752_MI;
struct t20;
#define m22752(__this, p0, method) (t359 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>
extern Il2CppType t20_0_0_1;
FieldInfo t3064_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3064_TI, offsetof(t3064, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3064_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3064_TI, offsetof(t3064, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3064_FIs[] =
{
	&t3064_f0_FieldInfo,
	&t3064_f1_FieldInfo,
	NULL
};
extern MethodInfo m16859_MI;
static PropertyInfo t3064____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3064_TI, "System.Collections.IEnumerator.Current", &m16859_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3064____Current_PropertyInfo = 
{
	&t3064_TI, "Current", &m16862_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3064_PIs[] =
{
	&t3064____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3064____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3064_m16858_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16858_GM;
MethodInfo m16858_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3064_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3064_m16858_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16858_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16859_GM;
MethodInfo m16859_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3064_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16859_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16860_GM;
MethodInfo m16860_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3064_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16860_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16861_GM;
MethodInfo m16861_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3064_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16861_GM};
extern Il2CppType t359_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16862_GM;
MethodInfo m16862_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3064_TI, &t359_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16862_GM};
static MethodInfo* t3064_MIs[] =
{
	&m16858_MI,
	&m16859_MI,
	&m16860_MI,
	&m16861_MI,
	&m16862_MI,
	NULL
};
extern MethodInfo m16861_MI;
extern MethodInfo m16860_MI;
static MethodInfo* t3064_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16859_MI,
	&m16861_MI,
	&m16860_MI,
	&m16862_MI,
};
static TypeInfo* t3064_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4454_TI,
};
static Il2CppInterfaceOffsetPair t3064_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4454_TI, 7},
};
extern TypeInfo t359_TI;
static Il2CppRGCTXData t3064_RGCTXData[3] = 
{
	&m16862_MI/* Method Usage */,
	&t359_TI/* Class Usage */,
	&m22752_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3064_0_0_0;
extern Il2CppType t3064_1_0_0;
extern Il2CppGenericClass t3064_GC;
TypeInfo t3064_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3064_MIs, t3064_PIs, t3064_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3064_TI, t3064_ITIs, t3064_VT, &EmptyCustomAttributesCache, &t3064_TI, &t3064_0_0_0, &t3064_1_0_0, t3064_IOs, &t3064_GC, NULL, NULL, NULL, t3064_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3064)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5715_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.DisallowMultipleComponent>
extern MethodInfo m29885_MI;
static PropertyInfo t5715____Count_PropertyInfo = 
{
	&t5715_TI, "Count", &m29885_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29886_MI;
static PropertyInfo t5715____IsReadOnly_PropertyInfo = 
{
	&t5715_TI, "IsReadOnly", &m29886_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5715_PIs[] =
{
	&t5715____Count_PropertyInfo,
	&t5715____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29885_GM;
MethodInfo m29885_MI = 
{
	"get_Count", NULL, &t5715_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29885_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29886_GM;
MethodInfo m29886_MI = 
{
	"get_IsReadOnly", NULL, &t5715_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29886_GM};
extern Il2CppType t359_0_0_0;
extern Il2CppType t359_0_0_0;
static ParameterInfo t5715_m29887_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t359_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29887_GM;
MethodInfo m29887_MI = 
{
	"Add", NULL, &t5715_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5715_m29887_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29887_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29888_GM;
MethodInfo m29888_MI = 
{
	"Clear", NULL, &t5715_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29888_GM};
extern Il2CppType t359_0_0_0;
static ParameterInfo t5715_m29889_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t359_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29889_GM;
MethodInfo m29889_MI = 
{
	"Contains", NULL, &t5715_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5715_m29889_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29889_GM};
extern Il2CppType t534_0_0_0;
extern Il2CppType t534_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5715_m29890_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t534_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29890_GM;
MethodInfo m29890_MI = 
{
	"CopyTo", NULL, &t5715_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5715_m29890_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29890_GM};
extern Il2CppType t359_0_0_0;
static ParameterInfo t5715_m29891_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t359_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29891_GM;
MethodInfo m29891_MI = 
{
	"Remove", NULL, &t5715_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5715_m29891_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29891_GM};
static MethodInfo* t5715_MIs[] =
{
	&m29885_MI,
	&m29886_MI,
	&m29887_MI,
	&m29888_MI,
	&m29889_MI,
	&m29890_MI,
	&m29891_MI,
	NULL
};
extern TypeInfo t5717_TI;
static TypeInfo* t5715_ITIs[] = 
{
	&t603_TI,
	&t5717_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5715_0_0_0;
extern Il2CppType t5715_1_0_0;
struct t5715;
extern Il2CppGenericClass t5715_GC;
TypeInfo t5715_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5715_MIs, t5715_PIs, NULL, NULL, NULL, NULL, NULL, &t5715_TI, t5715_ITIs, NULL, &EmptyCustomAttributesCache, &t5715_TI, &t5715_0_0_0, &t5715_1_0_0, NULL, &t5715_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.DisallowMultipleComponent>
extern Il2CppType t4454_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29892_GM;
MethodInfo m29892_MI = 
{
	"GetEnumerator", NULL, &t5717_TI, &t4454_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29892_GM};
static MethodInfo* t5717_MIs[] =
{
	&m29892_MI,
	NULL
};
static TypeInfo* t5717_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5717_0_0_0;
extern Il2CppType t5717_1_0_0;
struct t5717;
extern Il2CppGenericClass t5717_GC;
TypeInfo t5717_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5717_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5717_TI, t5717_ITIs, NULL, &EmptyCustomAttributesCache, &t5717_TI, &t5717_0_0_0, &t5717_1_0_0, NULL, &t5717_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5716_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.DisallowMultipleComponent>
extern MethodInfo m29893_MI;
extern MethodInfo m29894_MI;
static PropertyInfo t5716____Item_PropertyInfo = 
{
	&t5716_TI, "Item", &m29893_MI, &m29894_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5716_PIs[] =
{
	&t5716____Item_PropertyInfo,
	NULL
};
extern Il2CppType t359_0_0_0;
static ParameterInfo t5716_m29895_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t359_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29895_GM;
MethodInfo m29895_MI = 
{
	"IndexOf", NULL, &t5716_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5716_m29895_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29895_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t359_0_0_0;
static ParameterInfo t5716_m29896_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t359_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29896_GM;
MethodInfo m29896_MI = 
{
	"Insert", NULL, &t5716_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5716_m29896_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29896_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5716_m29897_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29897_GM;
MethodInfo m29897_MI = 
{
	"RemoveAt", NULL, &t5716_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5716_m29897_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29897_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5716_m29893_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t359_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29893_GM;
MethodInfo m29893_MI = 
{
	"get_Item", NULL, &t5716_TI, &t359_0_0_0, RuntimeInvoker_t29_t44, t5716_m29893_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29893_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t359_0_0_0;
static ParameterInfo t5716_m29894_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t359_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29894_GM;
MethodInfo m29894_MI = 
{
	"set_Item", NULL, &t5716_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5716_m29894_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29894_GM};
static MethodInfo* t5716_MIs[] =
{
	&m29895_MI,
	&m29896_MI,
	&m29897_MI,
	&m29893_MI,
	&m29894_MI,
	NULL
};
static TypeInfo* t5716_ITIs[] = 
{
	&t603_TI,
	&t5715_TI,
	&t5717_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5716_0_0_0;
extern Il2CppType t5716_1_0_0;
struct t5716;
extern Il2CppGenericClass t5716_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5716_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5716_MIs, t5716_PIs, NULL, NULL, NULL, NULL, NULL, &t5716_TI, t5716_ITIs, NULL, &t1908__CustomAttributeCache, &t5716_TI, &t5716_0_0_0, &t5716_1_0_0, NULL, &t5716_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4456_TI;

#include "t360.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.ExecuteInEditMode>
extern MethodInfo m29898_MI;
static PropertyInfo t4456____Current_PropertyInfo = 
{
	&t4456_TI, "Current", &m29898_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4456_PIs[] =
{
	&t4456____Current_PropertyInfo,
	NULL
};
extern Il2CppType t360_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29898_GM;
MethodInfo m29898_MI = 
{
	"get_Current", NULL, &t4456_TI, &t360_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29898_GM};
static MethodInfo* t4456_MIs[] =
{
	&m29898_MI,
	NULL
};
static TypeInfo* t4456_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4456_0_0_0;
extern Il2CppType t4456_1_0_0;
struct t4456;
extern Il2CppGenericClass t4456_GC;
TypeInfo t4456_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4456_MIs, t4456_PIs, NULL, NULL, NULL, NULL, NULL, &t4456_TI, t4456_ITIs, NULL, &EmptyCustomAttributesCache, &t4456_TI, &t4456_0_0_0, &t4456_1_0_0, NULL, &t4456_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3065.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3065_TI;
#include "t3065MD.h"

extern TypeInfo t360_TI;
extern MethodInfo m16867_MI;
extern MethodInfo m22763_MI;
struct t20;
#define m22763(__this, p0, method) (t360 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3065_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3065_TI, offsetof(t3065, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3065_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3065_TI, offsetof(t3065, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3065_FIs[] =
{
	&t3065_f0_FieldInfo,
	&t3065_f1_FieldInfo,
	NULL
};
extern MethodInfo m16864_MI;
static PropertyInfo t3065____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3065_TI, "System.Collections.IEnumerator.Current", &m16864_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3065____Current_PropertyInfo = 
{
	&t3065_TI, "Current", &m16867_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3065_PIs[] =
{
	&t3065____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3065____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3065_m16863_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16863_GM;
MethodInfo m16863_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3065_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3065_m16863_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16863_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16864_GM;
MethodInfo m16864_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3065_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16864_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16865_GM;
MethodInfo m16865_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3065_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16865_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16866_GM;
MethodInfo m16866_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3065_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16866_GM};
extern Il2CppType t360_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16867_GM;
MethodInfo m16867_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3065_TI, &t360_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16867_GM};
static MethodInfo* t3065_MIs[] =
{
	&m16863_MI,
	&m16864_MI,
	&m16865_MI,
	&m16866_MI,
	&m16867_MI,
	NULL
};
extern MethodInfo m16866_MI;
extern MethodInfo m16865_MI;
static MethodInfo* t3065_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16864_MI,
	&m16866_MI,
	&m16865_MI,
	&m16867_MI,
};
static TypeInfo* t3065_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4456_TI,
};
static Il2CppInterfaceOffsetPair t3065_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4456_TI, 7},
};
extern TypeInfo t360_TI;
static Il2CppRGCTXData t3065_RGCTXData[3] = 
{
	&m16867_MI/* Method Usage */,
	&t360_TI/* Class Usage */,
	&m22763_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3065_0_0_0;
extern Il2CppType t3065_1_0_0;
extern Il2CppGenericClass t3065_GC;
TypeInfo t3065_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3065_MIs, t3065_PIs, t3065_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3065_TI, t3065_ITIs, t3065_VT, &EmptyCustomAttributesCache, &t3065_TI, &t3065_0_0_0, &t3065_1_0_0, t3065_IOs, &t3065_GC, NULL, NULL, NULL, t3065_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3065)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5718_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.ExecuteInEditMode>
extern MethodInfo m29899_MI;
static PropertyInfo t5718____Count_PropertyInfo = 
{
	&t5718_TI, "Count", &m29899_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29900_MI;
static PropertyInfo t5718____IsReadOnly_PropertyInfo = 
{
	&t5718_TI, "IsReadOnly", &m29900_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5718_PIs[] =
{
	&t5718____Count_PropertyInfo,
	&t5718____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29899_GM;
MethodInfo m29899_MI = 
{
	"get_Count", NULL, &t5718_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29899_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29900_GM;
MethodInfo m29900_MI = 
{
	"get_IsReadOnly", NULL, &t5718_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29900_GM};
extern Il2CppType t360_0_0_0;
extern Il2CppType t360_0_0_0;
static ParameterInfo t5718_m29901_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t360_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29901_GM;
MethodInfo m29901_MI = 
{
	"Add", NULL, &t5718_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5718_m29901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29901_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29902_GM;
MethodInfo m29902_MI = 
{
	"Clear", NULL, &t5718_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29902_GM};
extern Il2CppType t360_0_0_0;
static ParameterInfo t5718_m29903_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t360_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29903_GM;
MethodInfo m29903_MI = 
{
	"Contains", NULL, &t5718_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5718_m29903_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29903_GM};
extern Il2CppType t535_0_0_0;
extern Il2CppType t535_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5718_m29904_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t535_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29904_GM;
MethodInfo m29904_MI = 
{
	"CopyTo", NULL, &t5718_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5718_m29904_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29904_GM};
extern Il2CppType t360_0_0_0;
static ParameterInfo t5718_m29905_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t360_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29905_GM;
MethodInfo m29905_MI = 
{
	"Remove", NULL, &t5718_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5718_m29905_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29905_GM};
static MethodInfo* t5718_MIs[] =
{
	&m29899_MI,
	&m29900_MI,
	&m29901_MI,
	&m29902_MI,
	&m29903_MI,
	&m29904_MI,
	&m29905_MI,
	NULL
};
extern TypeInfo t5720_TI;
static TypeInfo* t5718_ITIs[] = 
{
	&t603_TI,
	&t5720_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5718_0_0_0;
extern Il2CppType t5718_1_0_0;
struct t5718;
extern Il2CppGenericClass t5718_GC;
TypeInfo t5718_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5718_MIs, t5718_PIs, NULL, NULL, NULL, NULL, NULL, &t5718_TI, t5718_ITIs, NULL, &EmptyCustomAttributesCache, &t5718_TI, &t5718_0_0_0, &t5718_1_0_0, NULL, &t5718_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.ExecuteInEditMode>
extern Il2CppType t4456_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29906_GM;
MethodInfo m29906_MI = 
{
	"GetEnumerator", NULL, &t5720_TI, &t4456_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29906_GM};
static MethodInfo* t5720_MIs[] =
{
	&m29906_MI,
	NULL
};
static TypeInfo* t5720_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5720_0_0_0;
extern Il2CppType t5720_1_0_0;
struct t5720;
extern Il2CppGenericClass t5720_GC;
TypeInfo t5720_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5720_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5720_TI, t5720_ITIs, NULL, &EmptyCustomAttributesCache, &t5720_TI, &t5720_0_0_0, &t5720_1_0_0, NULL, &t5720_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5719_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.ExecuteInEditMode>
extern MethodInfo m29907_MI;
extern MethodInfo m29908_MI;
static PropertyInfo t5719____Item_PropertyInfo = 
{
	&t5719_TI, "Item", &m29907_MI, &m29908_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5719_PIs[] =
{
	&t5719____Item_PropertyInfo,
	NULL
};
extern Il2CppType t360_0_0_0;
static ParameterInfo t5719_m29909_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t360_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29909_GM;
MethodInfo m29909_MI = 
{
	"IndexOf", NULL, &t5719_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5719_m29909_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29909_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t360_0_0_0;
static ParameterInfo t5719_m29910_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t360_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29910_GM;
MethodInfo m29910_MI = 
{
	"Insert", NULL, &t5719_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5719_m29910_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29910_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5719_m29911_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29911_GM;
MethodInfo m29911_MI = 
{
	"RemoveAt", NULL, &t5719_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5719_m29911_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29911_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5719_m29907_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t360_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29907_GM;
MethodInfo m29907_MI = 
{
	"get_Item", NULL, &t5719_TI, &t360_0_0_0, RuntimeInvoker_t29_t44, t5719_m29907_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29907_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t360_0_0_0;
static ParameterInfo t5719_m29908_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t360_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29908_GM;
MethodInfo m29908_MI = 
{
	"set_Item", NULL, &t5719_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5719_m29908_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29908_GM};
static MethodInfo* t5719_MIs[] =
{
	&m29909_MI,
	&m29910_MI,
	&m29911_MI,
	&m29907_MI,
	&m29908_MI,
	NULL
};
static TypeInfo* t5719_ITIs[] = 
{
	&t603_TI,
	&t5718_TI,
	&t5720_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5719_0_0_0;
extern Il2CppType t5719_1_0_0;
struct t5719;
extern Il2CppGenericClass t5719_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5719_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5719_MIs, t5719_PIs, NULL, NULL, NULL, NULL, NULL, &t5719_TI, t5719_ITIs, NULL, &t1908__CustomAttributeCache, &t5719_TI, &t5719_0_0_0, &t5719_1_0_0, NULL, &t5719_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4458_TI;

#include "t318.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RequireComponent>
extern MethodInfo m29912_MI;
static PropertyInfo t4458____Current_PropertyInfo = 
{
	&t4458_TI, "Current", &m29912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4458_PIs[] =
{
	&t4458____Current_PropertyInfo,
	NULL
};
extern Il2CppType t318_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29912_GM;
MethodInfo m29912_MI = 
{
	"get_Current", NULL, &t4458_TI, &t318_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29912_GM};
static MethodInfo* t4458_MIs[] =
{
	&m29912_MI,
	NULL
};
static TypeInfo* t4458_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4458_0_0_0;
extern Il2CppType t4458_1_0_0;
struct t4458;
extern Il2CppGenericClass t4458_GC;
TypeInfo t4458_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4458_MIs, t4458_PIs, NULL, NULL, NULL, NULL, NULL, &t4458_TI, t4458_ITIs, NULL, &EmptyCustomAttributesCache, &t4458_TI, &t4458_0_0_0, &t4458_1_0_0, NULL, &t4458_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3066.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3066_TI;
#include "t3066MD.h"

extern TypeInfo t318_TI;
extern MethodInfo m16872_MI;
extern MethodInfo m22774_MI;
struct t20;
#define m22774(__this, p0, method) (t318 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>
extern Il2CppType t20_0_0_1;
FieldInfo t3066_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3066_TI, offsetof(t3066, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3066_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3066_TI, offsetof(t3066, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3066_FIs[] =
{
	&t3066_f0_FieldInfo,
	&t3066_f1_FieldInfo,
	NULL
};
extern MethodInfo m16869_MI;
static PropertyInfo t3066____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3066_TI, "System.Collections.IEnumerator.Current", &m16869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3066____Current_PropertyInfo = 
{
	&t3066_TI, "Current", &m16872_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3066_PIs[] =
{
	&t3066____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3066____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3066_m16868_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16868_GM;
MethodInfo m16868_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3066_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3066_m16868_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16868_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16869_GM;
MethodInfo m16869_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3066_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16869_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16870_GM;
MethodInfo m16870_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3066_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16870_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16871_GM;
MethodInfo m16871_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3066_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16871_GM};
extern Il2CppType t318_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16872_GM;
MethodInfo m16872_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3066_TI, &t318_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16872_GM};
static MethodInfo* t3066_MIs[] =
{
	&m16868_MI,
	&m16869_MI,
	&m16870_MI,
	&m16871_MI,
	&m16872_MI,
	NULL
};
extern MethodInfo m16871_MI;
extern MethodInfo m16870_MI;
static MethodInfo* t3066_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16869_MI,
	&m16871_MI,
	&m16870_MI,
	&m16872_MI,
};
static TypeInfo* t3066_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4458_TI,
};
static Il2CppInterfaceOffsetPair t3066_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4458_TI, 7},
};
extern TypeInfo t318_TI;
static Il2CppRGCTXData t3066_RGCTXData[3] = 
{
	&m16872_MI/* Method Usage */,
	&t318_TI/* Class Usage */,
	&m22774_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3066_0_0_0;
extern Il2CppType t3066_1_0_0;
extern Il2CppGenericClass t3066_GC;
TypeInfo t3066_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3066_MIs, t3066_PIs, t3066_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3066_TI, t3066_ITIs, t3066_VT, &EmptyCustomAttributesCache, &t3066_TI, &t3066_0_0_0, &t3066_1_0_0, t3066_IOs, &t3066_GC, NULL, NULL, NULL, t3066_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3066)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5721_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RequireComponent>
extern MethodInfo m29913_MI;
static PropertyInfo t5721____Count_PropertyInfo = 
{
	&t5721_TI, "Count", &m29913_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29914_MI;
static PropertyInfo t5721____IsReadOnly_PropertyInfo = 
{
	&t5721_TI, "IsReadOnly", &m29914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5721_PIs[] =
{
	&t5721____Count_PropertyInfo,
	&t5721____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29913_GM;
MethodInfo m29913_MI = 
{
	"get_Count", NULL, &t5721_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29913_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29914_GM;
MethodInfo m29914_MI = 
{
	"get_IsReadOnly", NULL, &t5721_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29914_GM};
extern Il2CppType t318_0_0_0;
extern Il2CppType t318_0_0_0;
static ParameterInfo t5721_m29915_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t318_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29915_GM;
MethodInfo m29915_MI = 
{
	"Add", NULL, &t5721_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5721_m29915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29915_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29916_GM;
MethodInfo m29916_MI = 
{
	"Clear", NULL, &t5721_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29916_GM};
extern Il2CppType t318_0_0_0;
static ParameterInfo t5721_m29917_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t318_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29917_GM;
MethodInfo m29917_MI = 
{
	"Contains", NULL, &t5721_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5721_m29917_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29917_GM};
extern Il2CppType t536_0_0_0;
extern Il2CppType t536_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5721_m29918_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t536_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29918_GM;
MethodInfo m29918_MI = 
{
	"CopyTo", NULL, &t5721_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5721_m29918_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29918_GM};
extern Il2CppType t318_0_0_0;
static ParameterInfo t5721_m29919_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t318_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29919_GM;
MethodInfo m29919_MI = 
{
	"Remove", NULL, &t5721_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5721_m29919_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29919_GM};
static MethodInfo* t5721_MIs[] =
{
	&m29913_MI,
	&m29914_MI,
	&m29915_MI,
	&m29916_MI,
	&m29917_MI,
	&m29918_MI,
	&m29919_MI,
	NULL
};
extern TypeInfo t5723_TI;
static TypeInfo* t5721_ITIs[] = 
{
	&t603_TI,
	&t5723_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5721_0_0_0;
extern Il2CppType t5721_1_0_0;
struct t5721;
extern Il2CppGenericClass t5721_GC;
TypeInfo t5721_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5721_MIs, t5721_PIs, NULL, NULL, NULL, NULL, NULL, &t5721_TI, t5721_ITIs, NULL, &EmptyCustomAttributesCache, &t5721_TI, &t5721_0_0_0, &t5721_1_0_0, NULL, &t5721_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RequireComponent>
extern Il2CppType t4458_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29920_GM;
MethodInfo m29920_MI = 
{
	"GetEnumerator", NULL, &t5723_TI, &t4458_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29920_GM};
static MethodInfo* t5723_MIs[] =
{
	&m29920_MI,
	NULL
};
static TypeInfo* t5723_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5723_0_0_0;
extern Il2CppType t5723_1_0_0;
struct t5723;
extern Il2CppGenericClass t5723_GC;
TypeInfo t5723_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5723_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5723_TI, t5723_ITIs, NULL, &EmptyCustomAttributesCache, &t5723_TI, &t5723_0_0_0, &t5723_1_0_0, NULL, &t5723_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5722_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RequireComponent>
extern MethodInfo m29921_MI;
extern MethodInfo m29922_MI;
static PropertyInfo t5722____Item_PropertyInfo = 
{
	&t5722_TI, "Item", &m29921_MI, &m29922_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5722_PIs[] =
{
	&t5722____Item_PropertyInfo,
	NULL
};
extern Il2CppType t318_0_0_0;
static ParameterInfo t5722_m29923_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t318_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29923_GM;
MethodInfo m29923_MI = 
{
	"IndexOf", NULL, &t5722_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5722_m29923_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29923_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t318_0_0_0;
static ParameterInfo t5722_m29924_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t318_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29924_GM;
MethodInfo m29924_MI = 
{
	"Insert", NULL, &t5722_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5722_m29924_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29924_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5722_m29925_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29925_GM;
MethodInfo m29925_MI = 
{
	"RemoveAt", NULL, &t5722_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5722_m29925_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29925_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5722_m29921_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t318_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29921_GM;
MethodInfo m29921_MI = 
{
	"get_Item", NULL, &t5722_TI, &t318_0_0_0, RuntimeInvoker_t29_t44, t5722_m29921_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29921_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t318_0_0_0;
static ParameterInfo t5722_m29922_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t318_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29922_GM;
MethodInfo m29922_MI = 
{
	"set_Item", NULL, &t5722_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5722_m29922_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29922_GM};
static MethodInfo* t5722_MIs[] =
{
	&m29923_MI,
	&m29924_MI,
	&m29925_MI,
	&m29921_MI,
	&m29922_MI,
	NULL
};
static TypeInfo* t5722_ITIs[] = 
{
	&t603_TI,
	&t5721_TI,
	&t5723_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5722_0_0_0;
extern Il2CppType t5722_1_0_0;
struct t5722;
extern Il2CppGenericClass t5722_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5722_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5722_MIs, t5722_PIs, NULL, NULL, NULL, NULL, NULL, &t5722_TI, t5722_ITIs, NULL, &t1908__CustomAttributeCache, &t5722_TI, &t5722_0_0_0, &t5722_1_0_0, NULL, &t5722_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t627.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t627_TI;
#include "t627MD.h"

#include "mscorlib_ArrayTypes.h"
#include "t42.h"
#include "t305.h"
#include "t3068.h"
extern TypeInfo t21_TI;
extern TypeInfo t44_TI;
extern TypeInfo t305_TI;
extern TypeInfo t1622_TI;
extern TypeInfo t3068_TI;
extern TypeInfo t42_TI;
#include "t29MD.h"
#include "t305MD.h"
#include "t3068MD.h"
extern MethodInfo m1331_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m8852_MI;
extern MethodInfo m16879_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m22785_MI;
extern MethodInfo m16880_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m22785(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)


 t3068  m16879 (t627 * __this, MethodInfo* method){
	{
		t3068  L_0 = {0};
		m16880(&L_0, __this, &m16880_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Type>
extern Il2CppType t44_0_0_32849;
FieldInfo t627_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t627_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_1;
FieldInfo t627_f1_FieldInfo = 
{
	"_array", &t537_0_0_1, &t627_TI, offsetof(t627, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t627_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t627_TI, offsetof(t627, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t627_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t627_TI, offsetof(t627, f3), &EmptyCustomAttributesCache};
static FieldInfo* t627_FIs[] =
{
	&t627_f0_FieldInfo,
	&t627_f1_FieldInfo,
	&t627_f2_FieldInfo,
	&t627_f3_FieldInfo,
	NULL
};
static const int32_t t627_f0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t627_f0_DefaultValue = 
{
	&t627_f0_FieldInfo, { (char*)&t627_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t627_FDVs[] = 
{
	&t627_f0_DefaultValue,
	NULL
};
extern MethodInfo m16873_MI;
static PropertyInfo t627____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t627_TI, "System.Collections.ICollection.IsSynchronized", &m16873_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16874_MI;
static PropertyInfo t627____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t627_TI, "System.Collections.ICollection.SyncRoot", &m16874_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2911_MI;
static PropertyInfo t627____Count_PropertyInfo = 
{
	&t627_TI, "Count", &m2911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t627_PIs[] =
{
	&t627____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t627____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t627____Count_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2906_GM;
MethodInfo m2906_MI = 
{
	".ctor", (methodPointerType)&m11064_gshared, &t627_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2906_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16873_GM;
MethodInfo m16873_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11065_gshared, &t627_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16873_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16874_GM;
MethodInfo m16874_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11066_gshared, &t627_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16874_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t627_m16875_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16875_GM;
MethodInfo m16875_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11067_gshared, &t627_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t627_m16875_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16875_GM};
extern Il2CppType t3067_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16876_GM;
MethodInfo m16876_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11068_gshared, &t627_TI, &t3067_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16876_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16877_GM;
MethodInfo m16877_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11069_gshared, &t627_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16877_GM};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16878_GM;
MethodInfo m16878_MI = 
{
	"Peek", (methodPointerType)&m11070_gshared, &t627_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16878_GM};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2909_GM;
MethodInfo m2909_MI = 
{
	"Pop", (methodPointerType)&m11071_gshared, &t627_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2909_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t627_m2907_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2907_GM;
MethodInfo m2907_MI = 
{
	"Push", (methodPointerType)&m11072_gshared, &t627_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t627_m2907_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2907_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2911_GM;
MethodInfo m2911_MI = 
{
	"get_Count", (methodPointerType)&m11073_gshared, &t627_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2911_GM};
extern Il2CppType t3068_0_0_0;
extern void* RuntimeInvoker_t3068 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16879_GM;
MethodInfo m16879_MI = 
{
	"GetEnumerator", (methodPointerType)&m16879, &t627_TI, &t3068_0_0_0, RuntimeInvoker_t3068, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16879_GM};
static MethodInfo* t627_MIs[] =
{
	&m2906_MI,
	&m16873_MI,
	&m16874_MI,
	&m16875_MI,
	&m16876_MI,
	&m16877_MI,
	&m16878_MI,
	&m2909_MI,
	&m2907_MI,
	&m2911_MI,
	&m16879_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m16875_MI;
extern MethodInfo m16877_MI;
extern MethodInfo m16876_MI;
static MethodInfo* t627_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2911_MI,
	&m16873_MI,
	&m16874_MI,
	&m16875_MI,
	&m16877_MI,
	&m16876_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t3070_TI;
static TypeInfo* t627_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
	&t3070_TI,
};
static Il2CppInterfaceOffsetPair t627_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
	{ &t3070_TI, 9},
};
extern TypeInfo t3068_TI;
static Il2CppRGCTXData t627_RGCTXData[4] = 
{
	&m16879_MI/* Method Usage */,
	&t3068_TI/* Class Usage */,
	&m22785_MI/* Method Usage */,
	&m16880_MI/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t627_0_0_0;
extern Il2CppType t627_1_0_0;
extern TypeInfo t29_TI;
struct t627;
extern Il2CppGenericClass t627_GC;
extern CustomAttributesCache t717__CustomAttributeCache;
TypeInfo t627_TI = 
{
	&g_System_dll_Image, NULL, "Stack`1", "System.Collections.Generic", t627_MIs, t627_PIs, t627_FIs, NULL, &t29_TI, NULL, NULL, &t627_TI, t627_ITIs, t627_VT, &t717__CustomAttributeCache, &t627_TI, &t627_0_0_0, &t627_1_0_0, t627_IOs, &t627_GC, NULL, t627_FDVs, NULL, t627_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t627), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 11, 3, 4, 0, 0, 10, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m16884_MI;


// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Type>
extern Il2CppType t627_0_0_1;
FieldInfo t3068_f0_FieldInfo = 
{
	"parent", &t627_0_0_1, &t3068_TI, offsetof(t3068, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3068_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3068_TI, offsetof(t3068, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3068_f2_FieldInfo = 
{
	"_version", &t44_0_0_1, &t3068_TI, offsetof(t3068, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3068_FIs[] =
{
	&t3068_f0_FieldInfo,
	&t3068_f1_FieldInfo,
	&t3068_f2_FieldInfo,
	NULL
};
extern MethodInfo m16881_MI;
static PropertyInfo t3068____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3068_TI, "System.Collections.IEnumerator.Current", &m16881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3068____Current_PropertyInfo = 
{
	&t3068_TI, "Current", &m16884_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3068_PIs[] =
{
	&t3068____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3068____Current_PropertyInfo,
	NULL
};
extern Il2CppType t627_0_0_0;
static ParameterInfo t3068_m16880_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16880_GM;
MethodInfo m16880_MI = 
{
	".ctor", (methodPointerType)&m11075_gshared, &t3068_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3068_m16880_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16880_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16881_GM;
MethodInfo m16881_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11076_gshared, &t3068_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16881_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16882_GM;
MethodInfo m16882_MI = 
{
	"Dispose", (methodPointerType)&m11077_gshared, &t3068_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16882_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16883_GM;
MethodInfo m16883_MI = 
{
	"MoveNext", (methodPointerType)&m11078_gshared, &t3068_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16883_GM};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16884_GM;
MethodInfo m16884_MI = 
{
	"get_Current", (methodPointerType)&m11079_gshared, &t3068_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16884_GM};
static MethodInfo* t3068_MIs[] =
{
	&m16880_MI,
	&m16881_MI,
	&m16882_MI,
	&m16883_MI,
	&m16884_MI,
	NULL
};
extern MethodInfo m16883_MI;
extern MethodInfo m16882_MI;
static MethodInfo* t3068_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16881_MI,
	&m16883_MI,
	&m16882_MI,
	&m16884_MI,
};
extern TypeInfo t3067_TI;
static TypeInfo* t3068_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3067_TI,
};
static Il2CppInterfaceOffsetPair t3068_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3067_TI, 7},
};
extern TypeInfo t42_TI;
static Il2CppRGCTXData t3068_RGCTXData[2] = 
{
	&m16884_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3068_0_0_0;
extern Il2CppType t3068_1_0_0;
extern Il2CppGenericClass t3068_GC;
extern TypeInfo t717_TI;
TypeInfo t3068_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t3068_MIs, t3068_PIs, t3068_FIs, NULL, &t110_TI, NULL, &t717_TI, &t3068_TI, t3068_ITIs, t3068_VT, &EmptyCustomAttributesCache, &t3068_TI, &t3068_0_0_0, &t3068_1_0_0, t3068_IOs, &t3068_GC, NULL, NULL, NULL, t3068_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3068)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 3, 3};
#include "t628.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t628_TI;
#include "t628MD.h"

#include "t3074.h"
#include "t3071.h"
#include "t3072.h"
#include "t338.h"
#include "t3079.h"
#include "t3073.h"
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3074_TI;
extern TypeInfo t40_TI;
extern TypeInfo t3069_TI;
extern TypeInfo t3071_TI;
extern TypeInfo t338_TI;
extern TypeInfo t3072_TI;
extern TypeInfo t3079_TI;
#include "t915MD.h"
#include "t602MD.h"
#include "t3071MD.h"
#include "t338MD.h"
#include "t3072MD.h"
#include "t3074MD.h"
#include "t3079MD.h"
extern MethodInfo m16929_MI;
extern MethodInfo m16930_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m16916_MI;
extern MethodInfo m16913_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m2913_MI;
extern MethodInfo m16908_MI;
extern MethodInfo m16914_MI;
extern MethodInfo m16917_MI;
extern MethodInfo m16919_MI;
extern MethodInfo m16902_MI;
extern MethodInfo m16926_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m16927_MI;
extern MethodInfo m26624_MI;
extern MethodInfo m26629_MI;
extern MethodInfo m26631_MI;
extern MethodInfo m26623_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m16918_MI;
extern MethodInfo m16903_MI;
extern MethodInfo m16904_MI;
extern MethodInfo m16937_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m22787_MI;
extern MethodInfo m16911_MI;
extern MethodInfo m16912_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m17012_MI;
extern MethodInfo m16931_MI;
extern MethodInfo m16915_MI;
extern MethodInfo m16921_MI;
extern MethodInfo m17018_MI;
extern MethodInfo m22789_MI;
extern MethodInfo m22797_MI;
extern MethodInfo m5951_MI;
struct t20;
#include "t3077.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m22787(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m22789(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m22797(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t3074  m16913 (t628 * __this, MethodInfo* method){
	{
		t3074  L_0 = {0};
		m16931(&L_0, __this, &m16931_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<System.Type>
extern Il2CppType t44_0_0_32849;
FieldInfo t628_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t628_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_1;
FieldInfo t628_f1_FieldInfo = 
{
	"_items", &t537_0_0_1, &t628_TI, offsetof(t628, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t628_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t628_TI, offsetof(t628, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t628_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t628_TI, offsetof(t628, f3), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_49;
FieldInfo t628_f4_FieldInfo = 
{
	"EmptyArray", &t537_0_0_49, &t628_TI, offsetof(t628_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t628_FIs[] =
{
	&t628_f0_FieldInfo,
	&t628_f1_FieldInfo,
	&t628_f2_FieldInfo,
	&t628_f3_FieldInfo,
	&t628_f4_FieldInfo,
	NULL
};
static const int32_t t628_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t628_f0_DefaultValue = 
{
	&t628_f0_FieldInfo, { (char*)&t628_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t628_FDVs[] = 
{
	&t628_f0_DefaultValue,
	NULL
};
extern MethodInfo m16895_MI;
static PropertyInfo t628____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t628_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16895_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16896_MI;
static PropertyInfo t628____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t628_TI, "System.Collections.ICollection.IsSynchronized", &m16896_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16897_MI;
static PropertyInfo t628____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t628_TI, "System.Collections.ICollection.SyncRoot", &m16897_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16898_MI;
static PropertyInfo t628____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t628_TI, "System.Collections.IList.IsFixedSize", &m16898_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16899_MI;
static PropertyInfo t628____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t628_TI, "System.Collections.IList.IsReadOnly", &m16899_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16900_MI;
extern MethodInfo m16901_MI;
static PropertyInfo t628____System_Collections_IList_Item_PropertyInfo = 
{
	&t628_TI, "System.Collections.IList.Item", &m16900_MI, &m16901_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t628____Capacity_PropertyInfo = 
{
	&t628_TI, "Capacity", &m16926_MI, &m16927_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16928_MI;
static PropertyInfo t628____Count_PropertyInfo = 
{
	&t628_TI, "Count", &m16928_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t628____Item_PropertyInfo = 
{
	&t628_TI, "Item", &m16929_MI, &m16930_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t628_PIs[] =
{
	&t628____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t628____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t628____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t628____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t628____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t628____System_Collections_IList_Item_PropertyInfo,
	&t628____Capacity_PropertyInfo,
	&t628____Count_PropertyInfo,
	&t628____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2912_GM;
MethodInfo m2912_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2912_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16885_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16885_GM;
MethodInfo m16885_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t628_m16885_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16885_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16886_GM;
MethodInfo m16886_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16886_GM};
extern Il2CppType t3067_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16887_GM;
MethodInfo m16887_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t628_TI, &t3067_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16887_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16888_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16888_GM;
MethodInfo m16888_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t628_m16888_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16888_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16889_GM;
MethodInfo m16889_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t628_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16889_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t628_m16890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16890_GM;
MethodInfo m16890_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t628_m16890_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16890_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t628_m16891_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16891_GM;
MethodInfo m16891_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t628_m16891_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16891_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t628_m16892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16892_GM;
MethodInfo m16892_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t628_m16892_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16892_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t628_m16893_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16893_GM;
MethodInfo m16893_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t628_m16893_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16893_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t628_m16894_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16894_GM;
MethodInfo m16894_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16894_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16894_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16895_GM;
MethodInfo m16895_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16895_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16896_GM;
MethodInfo m16896_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16896_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16897_GM;
MethodInfo m16897_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t628_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16897_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16898_GM;
MethodInfo m16898_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16898_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16899_GM;
MethodInfo m16899_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16899_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16900_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16900_GM;
MethodInfo m16900_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t628_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t628_m16900_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16900_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t628_m16901_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16901_GM;
MethodInfo m16901_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t628_m16901_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16901_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t628_m2913_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2913_GM;
MethodInfo m2913_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m2913_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2913_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16902_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16902_GM;
MethodInfo m16902_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t628_m16902_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16902_GM};
extern Il2CppType t3069_0_0_0;
extern Il2CppType t3069_0_0_0;
static ParameterInfo t628_m16903_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3069_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16903_GM;
MethodInfo m16903_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16903_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16903_GM};
extern Il2CppType t3070_0_0_0;
extern Il2CppType t3070_0_0_0;
static ParameterInfo t628_m16904_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3070_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16904_GM;
MethodInfo m16904_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16904_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16904_GM};
extern Il2CppType t3070_0_0_0;
static ParameterInfo t628_m16905_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3070_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16905_GM;
MethodInfo m16905_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16905_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16905_GM};
extern Il2CppType t3071_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16906_GM;
MethodInfo m16906_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t628_TI, &t3071_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16906_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16907_GM;
MethodInfo m16907_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16907_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t628_m16908_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16908_GM;
MethodInfo m16908_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t628_m16908_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16908_GM};
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16909_GM;
MethodInfo m16909_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t628_m16909_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16909_GM};
extern Il2CppType t3072_0_0_0;
extern Il2CppType t3072_0_0_0;
static ParameterInfo t628_m16910_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3072_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16910_GM;
MethodInfo m16910_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t628_TI, &t42_0_0_0, RuntimeInvoker_t29_t29, t628_m16910_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16910_GM};
extern Il2CppType t3072_0_0_0;
static ParameterInfo t628_m16911_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3072_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16911_GM;
MethodInfo m16911_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16911_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16911_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3072_0_0_0;
static ParameterInfo t628_m16912_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t3072_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16912_GM;
MethodInfo m16912_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t628_m16912_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16912_GM};
extern Il2CppType t3074_0_0_0;
extern void* RuntimeInvoker_t3074 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16913_GM;
MethodInfo m16913_MI = 
{
	"GetEnumerator", (methodPointerType)&m16913, &t628_TI, &t3074_0_0_0, RuntimeInvoker_t3074, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16913_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t628_m16914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16914_GM;
MethodInfo m16914_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t628_m16914_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16914_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16915_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16915_GM;
MethodInfo m16915_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t628_m16915_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16915_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16916_GM;
MethodInfo m16916_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t628_m16916_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16916_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t628_m16917_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16917_GM;
MethodInfo m16917_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t628_m16917_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16917_GM};
extern Il2CppType t3070_0_0_0;
static ParameterInfo t628_m16918_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3070_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16918_GM;
MethodInfo m16918_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16918_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16918_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t628_m16919_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16919_GM;
MethodInfo m16919_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t628_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t628_m16919_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16919_GM};
extern Il2CppType t3072_0_0_0;
static ParameterInfo t628_m16920_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3072_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16920_GM;
MethodInfo m16920_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t628_m16920_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16920_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16921_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16921_GM;
MethodInfo m16921_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t628_m16921_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16921_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16922_GM;
MethodInfo m16922_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16922_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16923_GM;
MethodInfo m16923_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16923_GM};
extern Il2CppType t3073_0_0_0;
extern Il2CppType t3073_0_0_0;
static ParameterInfo t628_m16924_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3073_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16924_GM;
MethodInfo m16924_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t628_m16924_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16924_GM};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2914_GM;
MethodInfo m2914_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t628_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2914_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16925_GM;
MethodInfo m16925_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16925_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16926_GM;
MethodInfo m16926_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16926_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16927_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16927_GM;
MethodInfo m16927_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t628_m16927_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16927_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16928_GM;
MethodInfo m16928_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t628_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16928_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t628_m16929_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16929_GM;
MethodInfo m16929_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t628_TI, &t42_0_0_0, RuntimeInvoker_t29_t44, t628_m16929_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16929_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t628_m16930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16930_GM;
MethodInfo m16930_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t628_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t628_m16930_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16930_GM};
static MethodInfo* t628_MIs[] =
{
	&m2912_MI,
	&m16885_MI,
	&m16886_MI,
	&m16887_MI,
	&m16888_MI,
	&m16889_MI,
	&m16890_MI,
	&m16891_MI,
	&m16892_MI,
	&m16893_MI,
	&m16894_MI,
	&m16895_MI,
	&m16896_MI,
	&m16897_MI,
	&m16898_MI,
	&m16899_MI,
	&m16900_MI,
	&m16901_MI,
	&m2913_MI,
	&m16902_MI,
	&m16903_MI,
	&m16904_MI,
	&m16905_MI,
	&m16906_MI,
	&m16907_MI,
	&m16908_MI,
	&m16909_MI,
	&m16910_MI,
	&m16911_MI,
	&m16912_MI,
	&m16913_MI,
	&m16914_MI,
	&m16915_MI,
	&m16916_MI,
	&m16917_MI,
	&m16918_MI,
	&m16919_MI,
	&m16920_MI,
	&m16921_MI,
	&m16922_MI,
	&m16923_MI,
	&m16924_MI,
	&m2914_MI,
	&m16925_MI,
	&m16926_MI,
	&m16927_MI,
	&m16928_MI,
	&m16929_MI,
	&m16930_MI,
	NULL
};
extern MethodInfo m16889_MI;
extern MethodInfo m16888_MI;
extern MethodInfo m16890_MI;
extern MethodInfo m16907_MI;
extern MethodInfo m16891_MI;
extern MethodInfo m16892_MI;
extern MethodInfo m16893_MI;
extern MethodInfo m16894_MI;
extern MethodInfo m16909_MI;
extern MethodInfo m16887_MI;
static MethodInfo* t628_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16889_MI,
	&m16928_MI,
	&m16896_MI,
	&m16897_MI,
	&m16888_MI,
	&m16898_MI,
	&m16899_MI,
	&m16900_MI,
	&m16901_MI,
	&m16890_MI,
	&m16907_MI,
	&m16891_MI,
	&m16892_MI,
	&m16893_MI,
	&m16894_MI,
	&m16921_MI,
	&m16928_MI,
	&m16895_MI,
	&m2913_MI,
	&m16907_MI,
	&m16908_MI,
	&m16909_MI,
	&m16919_MI,
	&m16887_MI,
	&m16914_MI,
	&m16917_MI,
	&m16921_MI,
	&m16929_MI,
	&m16930_MI,
};
extern TypeInfo t868_TI;
extern TypeInfo t3075_TI;
static TypeInfo* t628_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3069_TI,
	&t3070_TI,
	&t3075_TI,
};
static Il2CppInterfaceOffsetPair t628_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3069_TI, 20},
	{ &t3070_TI, 27},
	{ &t3075_TI, 28},
};
extern TypeInfo t628_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3074_TI;
extern TypeInfo t42_TI;
extern TypeInfo t3069_TI;
extern TypeInfo t3071_TI;
static Il2CppRGCTXData t628_RGCTXData[37] = 
{
	&t628_TI/* Static Usage */,
	&t537_TI/* Array Usage */,
	&m16913_MI/* Method Usage */,
	&t3074_TI/* Class Usage */,
	&t42_TI/* Class Usage */,
	&m2913_MI/* Method Usage */,
	&m16908_MI/* Method Usage */,
	&m16914_MI/* Method Usage */,
	&m16916_MI/* Method Usage */,
	&m16917_MI/* Method Usage */,
	&m16919_MI/* Method Usage */,
	&m16929_MI/* Method Usage */,
	&m16930_MI/* Method Usage */,
	&m16902_MI/* Method Usage */,
	&m16926_MI/* Method Usage */,
	&m16927_MI/* Method Usage */,
	&m26624_MI/* Method Usage */,
	&m26629_MI/* Method Usage */,
	&m26631_MI/* Method Usage */,
	&m26623_MI/* Method Usage */,
	&m16918_MI/* Method Usage */,
	&t3069_TI/* Class Usage */,
	&m16903_MI/* Method Usage */,
	&m16904_MI/* Method Usage */,
	&t3071_TI/* Class Usage */,
	&m16937_MI/* Method Usage */,
	&m22787_MI/* Method Usage */,
	&m16911_MI/* Method Usage */,
	&m16912_MI/* Method Usage */,
	&m17012_MI/* Method Usage */,
	&m16931_MI/* Method Usage */,
	&m16915_MI/* Method Usage */,
	&m16921_MI/* Method Usage */,
	&m17018_MI/* Method Usage */,
	&m22789_MI/* Method Usage */,
	&m22797_MI/* Method Usage */,
	&m22785_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t628_0_0_0;
extern Il2CppType t628_1_0_0;
struct t628;
extern Il2CppGenericClass t628_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t628_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t628_MIs, t628_PIs, t628_FIs, NULL, &t29_TI, NULL, NULL, &t628_TI, t628_ITIs, t628_VT, &t1261__CustomAttributeCache, &t628_TI, &t628_0_0_0, &t628_1_0_0, t628_IOs, &t628_GC, NULL, t628_FDVs, NULL, t628_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t628), 0, -1, sizeof(t628_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m16934_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<System.Type>
extern Il2CppType t628_0_0_1;
FieldInfo t3074_f0_FieldInfo = 
{
	"l", &t628_0_0_1, &t3074_TI, offsetof(t3074, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3074_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3074_TI, offsetof(t3074, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3074_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t3074_TI, offsetof(t3074, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t3074_f3_FieldInfo = 
{
	"current", &t42_0_0_1, &t3074_TI, offsetof(t3074, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3074_FIs[] =
{
	&t3074_f0_FieldInfo,
	&t3074_f1_FieldInfo,
	&t3074_f2_FieldInfo,
	&t3074_f3_FieldInfo,
	NULL
};
extern MethodInfo m16932_MI;
static PropertyInfo t3074____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3074_TI, "System.Collections.IEnumerator.Current", &m16932_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16936_MI;
static PropertyInfo t3074____Current_PropertyInfo = 
{
	&t3074_TI, "Current", &m16936_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3074_PIs[] =
{
	&t3074____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3074____Current_PropertyInfo,
	NULL
};
extern Il2CppType t628_0_0_0;
static ParameterInfo t3074_m16931_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16931_GM;
MethodInfo m16931_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t3074_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3074_m16931_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16931_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16932_GM;
MethodInfo m16932_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t3074_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16932_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16933_GM;
MethodInfo m16933_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t3074_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16933_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16934_GM;
MethodInfo m16934_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t3074_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16934_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16935_GM;
MethodInfo m16935_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t3074_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16935_GM};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16936_GM;
MethodInfo m16936_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t3074_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16936_GM};
static MethodInfo* t3074_MIs[] =
{
	&m16931_MI,
	&m16932_MI,
	&m16933_MI,
	&m16934_MI,
	&m16935_MI,
	&m16936_MI,
	NULL
};
extern MethodInfo m16935_MI;
extern MethodInfo m16933_MI;
static MethodInfo* t3074_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16932_MI,
	&m16935_MI,
	&m16933_MI,
	&m16936_MI,
};
static TypeInfo* t3074_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3067_TI,
};
static Il2CppInterfaceOffsetPair t3074_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3067_TI, 7},
};
extern TypeInfo t42_TI;
extern TypeInfo t3074_TI;
static Il2CppRGCTXData t3074_RGCTXData[3] = 
{
	&m16934_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&t3074_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3074_0_0_0;
extern Il2CppType t3074_1_0_0;
extern Il2CppGenericClass t3074_GC;
extern TypeInfo t1261_TI;
TypeInfo t3074_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3074_MIs, t3074_PIs, t3074_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t3074_TI, t3074_ITIs, t3074_VT, &EmptyCustomAttributesCache, &t3074_TI, &t3074_0_0_0, &t3074_1_0_0, t3074_IOs, &t3074_GC, NULL, NULL, NULL, t3074_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3074)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t3076MD.h"
extern MethodInfo m16966_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m26632_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m16998_MI;
extern MethodInfo m26628_MI;
extern MethodInfo m26634_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<System.Type>
extern Il2CppType t3075_0_0_1;
FieldInfo t3071_f0_FieldInfo = 
{
	"list", &t3075_0_0_1, &t3071_TI, offsetof(t3071, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3071_FIs[] =
{
	&t3071_f0_FieldInfo,
	NULL
};
extern MethodInfo m16943_MI;
extern MethodInfo m16944_MI;
static PropertyInfo t3071____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3071_TI, "System.Collections.Generic.IList<T>.Item", &m16943_MI, &m16944_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16945_MI;
static PropertyInfo t3071____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3071_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16945_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16955_MI;
static PropertyInfo t3071____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3071_TI, "System.Collections.ICollection.IsSynchronized", &m16955_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16956_MI;
static PropertyInfo t3071____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3071_TI, "System.Collections.ICollection.SyncRoot", &m16956_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16957_MI;
static PropertyInfo t3071____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3071_TI, "System.Collections.IList.IsFixedSize", &m16957_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16958_MI;
static PropertyInfo t3071____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3071_TI, "System.Collections.IList.IsReadOnly", &m16958_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16959_MI;
extern MethodInfo m16960_MI;
static PropertyInfo t3071____System_Collections_IList_Item_PropertyInfo = 
{
	&t3071_TI, "System.Collections.IList.Item", &m16959_MI, &m16960_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16965_MI;
static PropertyInfo t3071____Count_PropertyInfo = 
{
	&t3071_TI, "Count", &m16965_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3071____Item_PropertyInfo = 
{
	&t3071_TI, "Item", &m16966_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3071_PIs[] =
{
	&t3071____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3071____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3071____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3071____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3071____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3071____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3071____System_Collections_IList_Item_PropertyInfo,
	&t3071____Count_PropertyInfo,
	&t3071____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3075_0_0_0;
extern Il2CppType t3075_0_0_0;
static ParameterInfo t3071_m16937_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16937_GM;
MethodInfo m16937_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3071_m16937_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16937_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3071_m16938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16938_GM;
MethodInfo m16938_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3071_m16938_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16938_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16939_GM;
MethodInfo m16939_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16939_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3071_m16940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16940_GM;
MethodInfo m16940_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3071_m16940_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16940_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3071_m16941_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16941_GM;
MethodInfo m16941_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3071_m16941_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16941_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16942_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16942_GM;
MethodInfo m16942_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3071_m16942_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16942_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16943_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16943_GM;
MethodInfo m16943_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t3071_TI, &t42_0_0_0, RuntimeInvoker_t29_t44, t3071_m16943_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16943_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3071_m16944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16944_GM;
MethodInfo m16944_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3071_m16944_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16944_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16945_GM;
MethodInfo m16945_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16945_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16946_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16946_GM;
MethodInfo m16946_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3071_m16946_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16946_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16947_GM;
MethodInfo m16947_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t3071_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16947_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3071_m16948_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16948_GM;
MethodInfo m16948_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t3071_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3071_m16948_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16948_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16949_GM;
MethodInfo m16949_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16949_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3071_m16950_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16950_GM;
MethodInfo m16950_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3071_m16950_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16950_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3071_m16951_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16951_GM;
MethodInfo m16951_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t3071_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3071_m16951_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16951_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3071_m16952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16952_GM;
MethodInfo m16952_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3071_m16952_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16952_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3071_m16953_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16953_GM;
MethodInfo m16953_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3071_m16953_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16953_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16954_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16954_GM;
MethodInfo m16954_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3071_m16954_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16954_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16955_GM;
MethodInfo m16955_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16955_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16956_GM;
MethodInfo m16956_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t3071_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16956_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16957_GM;
MethodInfo m16957_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16957_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16958_GM;
MethodInfo m16958_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16958_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16959_GM;
MethodInfo m16959_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t3071_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3071_m16959_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16959_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3071_m16960_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16960_GM;
MethodInfo m16960_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3071_m16960_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16960_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3071_m16961_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16961_GM;
MethodInfo m16961_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t3071_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3071_m16961_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16961_GM};
extern Il2CppType t537_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16962_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16962_GM;
MethodInfo m16962_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t3071_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3071_m16962_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16962_GM};
extern Il2CppType t3067_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16963_GM;
MethodInfo m16963_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t3071_TI, &t3067_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16963_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3071_m16964_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16964_GM;
MethodInfo m16964_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t3071_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3071_m16964_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16964_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16965_GM;
MethodInfo m16965_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t3071_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16965_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3071_m16966_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16966_GM;
MethodInfo m16966_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t3071_TI, &t42_0_0_0, RuntimeInvoker_t29_t44, t3071_m16966_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16966_GM};
static MethodInfo* t3071_MIs[] =
{
	&m16937_MI,
	&m16938_MI,
	&m16939_MI,
	&m16940_MI,
	&m16941_MI,
	&m16942_MI,
	&m16943_MI,
	&m16944_MI,
	&m16945_MI,
	&m16946_MI,
	&m16947_MI,
	&m16948_MI,
	&m16949_MI,
	&m16950_MI,
	&m16951_MI,
	&m16952_MI,
	&m16953_MI,
	&m16954_MI,
	&m16955_MI,
	&m16956_MI,
	&m16957_MI,
	&m16958_MI,
	&m16959_MI,
	&m16960_MI,
	&m16961_MI,
	&m16962_MI,
	&m16963_MI,
	&m16964_MI,
	&m16965_MI,
	&m16966_MI,
	NULL
};
extern MethodInfo m16947_MI;
extern MethodInfo m16946_MI;
extern MethodInfo m16948_MI;
extern MethodInfo m16949_MI;
extern MethodInfo m16950_MI;
extern MethodInfo m16951_MI;
extern MethodInfo m16952_MI;
extern MethodInfo m16953_MI;
extern MethodInfo m16954_MI;
extern MethodInfo m16938_MI;
extern MethodInfo m16939_MI;
extern MethodInfo m16961_MI;
extern MethodInfo m16962_MI;
extern MethodInfo m16941_MI;
extern MethodInfo m16964_MI;
extern MethodInfo m16940_MI;
extern MethodInfo m16942_MI;
extern MethodInfo m16963_MI;
static MethodInfo* t3071_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16947_MI,
	&m16965_MI,
	&m16955_MI,
	&m16956_MI,
	&m16946_MI,
	&m16957_MI,
	&m16958_MI,
	&m16959_MI,
	&m16960_MI,
	&m16948_MI,
	&m16949_MI,
	&m16950_MI,
	&m16951_MI,
	&m16952_MI,
	&m16953_MI,
	&m16954_MI,
	&m16965_MI,
	&m16945_MI,
	&m16938_MI,
	&m16939_MI,
	&m16961_MI,
	&m16962_MI,
	&m16941_MI,
	&m16964_MI,
	&m16940_MI,
	&m16942_MI,
	&m16943_MI,
	&m16944_MI,
	&m16963_MI,
	&m16966_MI,
};
static TypeInfo* t3071_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3069_TI,
	&t3075_TI,
	&t3070_TI,
};
static Il2CppInterfaceOffsetPair t3071_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3069_TI, 20},
	{ &t3075_TI, 27},
	{ &t3070_TI, 32},
};
extern TypeInfo t42_TI;
static Il2CppRGCTXData t3071_RGCTXData[9] = 
{
	&m16966_MI/* Method Usage */,
	&m16998_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m26628_MI/* Method Usage */,
	&m26634_MI/* Method Usage */,
	&m26632_MI/* Method Usage */,
	&m26629_MI/* Method Usage */,
	&m26631_MI/* Method Usage */,
	&m26624_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3071_0_0_0;
extern Il2CppType t3071_1_0_0;
struct t3071;
extern Il2CppGenericClass t3071_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3071_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3071_MIs, t3071_PIs, t3071_FIs, NULL, &t29_TI, NULL, NULL, &t3071_TI, t3071_ITIs, t3071_VT, &t1263__CustomAttributeCache, &t3071_TI, &t3071_0_0_0, &t3071_1_0_0, t3071_IOs, &t3071_GC, NULL, NULL, NULL, t3071_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3071), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3076.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3076_TI;

#include "t43.h"
extern MethodInfo m26625_MI;
extern MethodInfo m17001_MI;
extern MethodInfo m17002_MI;
extern MethodInfo m16999_MI;
extern MethodInfo m16997_MI;
extern MethodInfo m2912_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m16990_MI;
extern MethodInfo m17000_MI;
extern MethodInfo m16988_MI;
extern MethodInfo m16993_MI;
extern MethodInfo m16984_MI;
extern MethodInfo m26627_MI;
extern MethodInfo m26635_MI;
extern MethodInfo m26636_MI;
extern MethodInfo m26633_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<System.Type>
extern Il2CppType t3075_0_0_1;
FieldInfo t3076_f0_FieldInfo = 
{
	"list", &t3075_0_0_1, &t3076_TI, offsetof(t3076, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3076_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3076_TI, offsetof(t3076, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3076_FIs[] =
{
	&t3076_f0_FieldInfo,
	&t3076_f1_FieldInfo,
	NULL
};
extern MethodInfo m16968_MI;
static PropertyInfo t3076____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3076_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16968_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16976_MI;
static PropertyInfo t3076____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3076_TI, "System.Collections.ICollection.IsSynchronized", &m16976_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16977_MI;
static PropertyInfo t3076____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3076_TI, "System.Collections.ICollection.SyncRoot", &m16977_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16978_MI;
static PropertyInfo t3076____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3076_TI, "System.Collections.IList.IsFixedSize", &m16978_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16979_MI;
static PropertyInfo t3076____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3076_TI, "System.Collections.IList.IsReadOnly", &m16979_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16980_MI;
extern MethodInfo m16981_MI;
static PropertyInfo t3076____System_Collections_IList_Item_PropertyInfo = 
{
	&t3076_TI, "System.Collections.IList.Item", &m16980_MI, &m16981_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16994_MI;
static PropertyInfo t3076____Count_PropertyInfo = 
{
	&t3076_TI, "Count", &m16994_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16995_MI;
extern MethodInfo m16996_MI;
static PropertyInfo t3076____Item_PropertyInfo = 
{
	&t3076_TI, "Item", &m16995_MI, &m16996_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3076_PIs[] =
{
	&t3076____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3076____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3076____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3076____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3076____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3076____System_Collections_IList_Item_PropertyInfo,
	&t3076____Count_PropertyInfo,
	&t3076____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16967_GM;
MethodInfo m16967_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16967_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16968_GM;
MethodInfo m16968_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16968_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3076_m16969_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16969_GM;
MethodInfo m16969_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3076_m16969_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16969_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16970_GM;
MethodInfo m16970_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t3076_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16970_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16971_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16971_GM;
MethodInfo m16971_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t3076_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3076_m16971_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16971_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16972_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16972_GM;
MethodInfo m16972_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3076_m16972_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16972_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16973_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16973_GM;
MethodInfo m16973_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t3076_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3076_m16973_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16973_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16974_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16974_GM;
MethodInfo m16974_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3076_m16974_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16974_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16975_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16975_GM;
MethodInfo m16975_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3076_m16975_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16975_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16976_GM;
MethodInfo m16976_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16976_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16977_GM;
MethodInfo m16977_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t3076_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16977_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16978_GM;
MethodInfo m16978_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16978_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16979_GM;
MethodInfo m16979_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16979_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3076_m16980_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16980_GM;
MethodInfo m16980_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t3076_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3076_m16980_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16980_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16981_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16981_GM;
MethodInfo m16981_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3076_m16981_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16981_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16982_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16982_GM;
MethodInfo m16982_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3076_m16982_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16982_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16983_GM;
MethodInfo m16983_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16983_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16984_GM;
MethodInfo m16984_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16984_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16985_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16985_GM;
MethodInfo m16985_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3076_m16985_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16985_GM};
extern Il2CppType t537_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3076_m16986_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16986_GM;
MethodInfo m16986_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3076_m16986_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16986_GM};
extern Il2CppType t3067_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16987_GM;
MethodInfo m16987_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t3076_TI, &t3067_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16987_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16988_GM;
MethodInfo m16988_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t3076_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3076_m16988_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16988_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16989_GM;
MethodInfo m16989_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3076_m16989_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16989_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16990_GM;
MethodInfo m16990_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3076_m16990_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16990_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16991_GM;
MethodInfo m16991_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3076_m16991_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16991_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3076_m16992_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16992_GM;
MethodInfo m16992_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3076_m16992_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16992_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3076_m16993_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16993_GM;
MethodInfo m16993_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3076_m16993_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16993_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16994_GM;
MethodInfo m16994_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t3076_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16994_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3076_m16995_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16995_GM;
MethodInfo m16995_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t3076_TI, &t42_0_0_0, RuntimeInvoker_t29_t44, t3076_m16995_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16995_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16996_GM;
MethodInfo m16996_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3076_m16996_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16996_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3076_m16997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16997_GM;
MethodInfo m16997_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3076_m16997_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16997_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16998_GM;
MethodInfo m16998_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3076_m16998_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16998_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3076_m16999_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16999_GM;
MethodInfo m16999_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t3076_TI, &t42_0_0_0, RuntimeInvoker_t29_t29, t3076_m16999_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16999_GM};
extern Il2CppType t3075_0_0_0;
static ParameterInfo t3076_m17000_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17000_GM;
MethodInfo m17000_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t3076_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3076_m17000_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17000_GM};
extern Il2CppType t3075_0_0_0;
static ParameterInfo t3076_m17001_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3075_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17001_GM;
MethodInfo m17001_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3076_m17001_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17001_GM};
extern Il2CppType t3075_0_0_0;
static ParameterInfo t3076_m17002_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3075_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17002_GM;
MethodInfo m17002_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t3076_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3076_m17002_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17002_GM};
static MethodInfo* t3076_MIs[] =
{
	&m16967_MI,
	&m16968_MI,
	&m16969_MI,
	&m16970_MI,
	&m16971_MI,
	&m16972_MI,
	&m16973_MI,
	&m16974_MI,
	&m16975_MI,
	&m16976_MI,
	&m16977_MI,
	&m16978_MI,
	&m16979_MI,
	&m16980_MI,
	&m16981_MI,
	&m16982_MI,
	&m16983_MI,
	&m16984_MI,
	&m16985_MI,
	&m16986_MI,
	&m16987_MI,
	&m16988_MI,
	&m16989_MI,
	&m16990_MI,
	&m16991_MI,
	&m16992_MI,
	&m16993_MI,
	&m16994_MI,
	&m16995_MI,
	&m16996_MI,
	&m16997_MI,
	&m16998_MI,
	&m16999_MI,
	&m17000_MI,
	&m17001_MI,
	&m17002_MI,
	NULL
};
extern MethodInfo m16970_MI;
extern MethodInfo m16969_MI;
extern MethodInfo m16971_MI;
extern MethodInfo m16983_MI;
extern MethodInfo m16972_MI;
extern MethodInfo m16973_MI;
extern MethodInfo m16974_MI;
extern MethodInfo m16975_MI;
extern MethodInfo m16992_MI;
extern MethodInfo m16982_MI;
extern MethodInfo m16985_MI;
extern MethodInfo m16986_MI;
extern MethodInfo m16991_MI;
extern MethodInfo m16989_MI;
extern MethodInfo m16987_MI;
static MethodInfo* t3076_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16970_MI,
	&m16994_MI,
	&m16976_MI,
	&m16977_MI,
	&m16969_MI,
	&m16978_MI,
	&m16979_MI,
	&m16980_MI,
	&m16981_MI,
	&m16971_MI,
	&m16983_MI,
	&m16972_MI,
	&m16973_MI,
	&m16974_MI,
	&m16975_MI,
	&m16992_MI,
	&m16994_MI,
	&m16968_MI,
	&m16982_MI,
	&m16983_MI,
	&m16985_MI,
	&m16986_MI,
	&m16991_MI,
	&m16988_MI,
	&m16989_MI,
	&m16992_MI,
	&m16995_MI,
	&m16996_MI,
	&m16987_MI,
	&m16984_MI,
	&m16990_MI,
	&m16993_MI,
	&m16997_MI,
};
static TypeInfo* t3076_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3069_TI,
	&t3075_TI,
	&t3070_TI,
};
static Il2CppInterfaceOffsetPair t3076_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3069_TI, 20},
	{ &t3075_TI, 27},
	{ &t3070_TI, 32},
};
extern TypeInfo t628_TI;
extern TypeInfo t42_TI;
static Il2CppRGCTXData t3076_RGCTXData[25] = 
{
	&t628_TI/* Class Usage */,
	&m2912_MI/* Method Usage */,
	&m26625_MI/* Method Usage */,
	&m26631_MI/* Method Usage */,
	&m26624_MI/* Method Usage */,
	&m16999_MI/* Method Usage */,
	&m16990_MI/* Method Usage */,
	&m16998_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m26628_MI/* Method Usage */,
	&m26634_MI/* Method Usage */,
	&m17000_MI/* Method Usage */,
	&m16988_MI/* Method Usage */,
	&m16993_MI/* Method Usage */,
	&m17001_MI/* Method Usage */,
	&m17002_MI/* Method Usage */,
	&m26632_MI/* Method Usage */,
	&m16997_MI/* Method Usage */,
	&m16984_MI/* Method Usage */,
	&m26627_MI/* Method Usage */,
	&m26629_MI/* Method Usage */,
	&m26635_MI/* Method Usage */,
	&m26636_MI/* Method Usage */,
	&m26633_MI/* Method Usage */,
	&t42_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3076_0_0_0;
extern Il2CppType t3076_1_0_0;
struct t3076;
extern Il2CppGenericClass t3076_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3076_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3076_MIs, t3076_PIs, t3076_FIs, NULL, &t29_TI, NULL, NULL, &t3076_TI, t3076_ITIs, t3076_VT, &t1262__CustomAttributeCache, &t3076_TI, &t3076_0_0_0, &t3076_1_0_0, t3076_IOs, &t3076_GC, NULL, NULL, NULL, t3076_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3076), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3077_TI;
#include "t3077MD.h"

#include "t1257.h"
#include "t3078.h"
extern TypeInfo t6740_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t3078_TI;
#include "t931MD.h"
#include "t3078MD.h"
extern Il2CppType t6740_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m17008_MI;
extern MethodInfo m29926_MI;
extern MethodInfo m22786_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Type>
extern Il2CppType t3077_0_0_49;
FieldInfo t3077_f0_FieldInfo = 
{
	"_default", &t3077_0_0_49, &t3077_TI, offsetof(t3077_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3077_FIs[] =
{
	&t3077_f0_FieldInfo,
	NULL
};
extern MethodInfo m17007_MI;
static PropertyInfo t3077____Default_PropertyInfo = 
{
	&t3077_TI, "Default", &m17007_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3077_PIs[] =
{
	&t3077____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17003_GM;
MethodInfo m17003_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t3077_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17003_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17004_GM;
MethodInfo m17004_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t3077_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17004_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3077_m17005_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17005_GM;
MethodInfo m17005_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t3077_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3077_m17005_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17005_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3077_m17006_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17006_GM;
MethodInfo m17006_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t3077_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3077_m17006_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17006_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3077_m29926_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29926_GM;
MethodInfo m29926_MI = 
{
	"GetHashCode", NULL, &t3077_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3077_m29926_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29926_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3077_m22786_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22786_GM;
MethodInfo m22786_MI = 
{
	"Equals", NULL, &t3077_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3077_m22786_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22786_GM};
extern Il2CppType t3077_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17007_GM;
MethodInfo m17007_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t3077_TI, &t3077_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17007_GM};
static MethodInfo* t3077_MIs[] =
{
	&m17003_MI,
	&m17004_MI,
	&m17005_MI,
	&m17006_MI,
	&m29926_MI,
	&m22786_MI,
	&m17007_MI,
	NULL
};
extern MethodInfo m17006_MI;
extern MethodInfo m17005_MI;
static MethodInfo* t3077_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m22786_MI,
	&m29926_MI,
	&m17006_MI,
	&m17005_MI,
	NULL,
	NULL,
};
extern TypeInfo t6741_TI;
extern TypeInfo t734_TI;
static TypeInfo* t3077_ITIs[] = 
{
	&t6741_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3077_IOs[] = 
{
	{ &t6741_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3077_TI;
extern TypeInfo t3077_TI;
extern TypeInfo t3078_TI;
extern TypeInfo t42_TI;
static Il2CppRGCTXData t3077_RGCTXData[9] = 
{
	&t6740_0_0_0/* Type Usage */,
	&t42_0_0_0/* Type Usage */,
	&t3077_TI/* Class Usage */,
	&t3077_TI/* Static Usage */,
	&t3078_TI/* Class Usage */,
	&m17008_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m29926_MI/* Method Usage */,
	&m22786_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3077_0_0_0;
extern Il2CppType t3077_1_0_0;
struct t3077;
extern Il2CppGenericClass t3077_GC;
TypeInfo t3077_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3077_MIs, t3077_PIs, t3077_FIs, NULL, &t29_TI, NULL, NULL, &t3077_TI, t3077_ITIs, t3077_VT, &EmptyCustomAttributesCache, &t3077_TI, &t3077_0_0_0, &t3077_1_0_0, t3077_IOs, &t3077_GC, NULL, NULL, NULL, t3077_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3077), 0, -1, sizeof(t3077_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Type>
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t6741_m29927_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29927_GM;
MethodInfo m29927_MI = 
{
	"Equals", NULL, &t6741_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6741_m29927_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29927_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t6741_m29928_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29928_GM;
MethodInfo m29928_MI = 
{
	"GetHashCode", NULL, &t6741_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6741_m29928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29928_GM};
static MethodInfo* t6741_MIs[] =
{
	&m29927_MI,
	&m29928_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6741_0_0_0;
extern Il2CppType t6741_1_0_0;
struct t6741;
extern Il2CppGenericClass t6741_GC;
TypeInfo t6741_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6741_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6741_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6741_TI, &t6741_0_0_0, &t6741_1_0_0, NULL, &t6741_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<System.Type>
extern Il2CppType t42_0_0_0;
static ParameterInfo t6740_m29929_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29929_GM;
MethodInfo m29929_MI = 
{
	"Equals", NULL, &t6740_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6740_m29929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29929_GM};
static MethodInfo* t6740_MIs[] =
{
	&m29929_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6740_1_0_0;
struct t6740;
extern Il2CppGenericClass t6740_GC;
TypeInfo t6740_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6740_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6740_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6740_TI, &t6740_0_0_0, &t6740_1_0_0, NULL, &t6740_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17003_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Type>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17008_GM;
MethodInfo m17008_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t3078_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17008_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3078_m17009_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17009_GM;
MethodInfo m17009_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t3078_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3078_m17009_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17009_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3078_m17010_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17010_GM;
MethodInfo m17010_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t3078_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3078_m17010_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17010_GM};
static MethodInfo* t3078_MIs[] =
{
	&m17008_MI,
	&m17009_MI,
	&m17010_MI,
	NULL
};
extern MethodInfo m17010_MI;
extern MethodInfo m17009_MI;
static MethodInfo* t3078_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17010_MI,
	&m17009_MI,
	&m17006_MI,
	&m17005_MI,
	&m17009_MI,
	&m17010_MI,
};
static Il2CppInterfaceOffsetPair t3078_IOs[] = 
{
	{ &t6741_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3077_TI;
extern TypeInfo t3077_TI;
extern TypeInfo t3078_TI;
extern TypeInfo t42_TI;
extern TypeInfo t42_TI;
static Il2CppRGCTXData t3078_RGCTXData[11] = 
{
	&t6740_0_0_0/* Type Usage */,
	&t42_0_0_0/* Type Usage */,
	&t3077_TI/* Class Usage */,
	&t3077_TI/* Static Usage */,
	&t3078_TI/* Class Usage */,
	&m17008_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m29926_MI/* Method Usage */,
	&m22786_MI/* Method Usage */,
	&m17003_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3078_0_0_0;
extern Il2CppType t3078_1_0_0;
struct t3078;
extern Il2CppGenericClass t3078_GC;
extern TypeInfo t1256_TI;
TypeInfo t3078_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3078_MIs, NULL, NULL, NULL, &t3077_TI, NULL, &t1256_TI, &t3078_TI, NULL, t3078_VT, &EmptyCustomAttributesCache, &t3078_TI, &t3078_0_0_0, &t3078_1_0_0, t3078_IOs, &t3078_GC, NULL, NULL, NULL, t3078_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3078), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Predicate`1<System.Type>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3072_m17011_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17011_GM;
MethodInfo m17011_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t3072_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3072_m17011_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17011_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3072_m17012_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17012_GM;
MethodInfo m17012_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t3072_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3072_m17012_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17012_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3072_m17013_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17013_GM;
MethodInfo m17013_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t3072_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3072_m17013_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17013_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3072_m17014_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17014_GM;
MethodInfo m17014_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t3072_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3072_m17014_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17014_GM};
static MethodInfo* t3072_MIs[] =
{
	&m17011_MI,
	&m17012_MI,
	&m17013_MI,
	&m17014_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m17013_MI;
extern MethodInfo m17014_MI;
static MethodInfo* t3072_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17012_MI,
	&m17013_MI,
	&m17014_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t3072_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3072_1_0_0;
extern TypeInfo t195_TI;
struct t3072;
extern Il2CppGenericClass t3072_GC;
TypeInfo t3072_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t3072_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3072_TI, NULL, t3072_VT, &EmptyCustomAttributesCache, &t3072_TI, &t3072_0_0_0, &t3072_1_0_0, t3072_IOs, &t3072_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3072), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t3080.h"
extern TypeInfo t4461_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t3080_TI;
#include "t3080MD.h"
extern Il2CppType t4461_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m17019_MI;
extern MethodInfo m29930_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<System.Type>
extern Il2CppType t3079_0_0_49;
FieldInfo t3079_f0_FieldInfo = 
{
	"_default", &t3079_0_0_49, &t3079_TI, offsetof(t3079_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3079_FIs[] =
{
	&t3079_f0_FieldInfo,
	NULL
};
static PropertyInfo t3079____Default_PropertyInfo = 
{
	&t3079_TI, "Default", &m17018_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3079_PIs[] =
{
	&t3079____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17015_GM;
MethodInfo m17015_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t3079_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17015_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17016_GM;
MethodInfo m17016_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t3079_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17016_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3079_m17017_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17017_GM;
MethodInfo m17017_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t3079_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3079_m17017_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17017_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3079_m29930_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29930_GM;
MethodInfo m29930_MI = 
{
	"Compare", NULL, &t3079_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3079_m29930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29930_GM};
extern Il2CppType t3079_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17018_GM;
MethodInfo m17018_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t3079_TI, &t3079_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17018_GM};
static MethodInfo* t3079_MIs[] =
{
	&m17015_MI,
	&m17016_MI,
	&m17017_MI,
	&m29930_MI,
	&m17018_MI,
	NULL
};
extern MethodInfo m17017_MI;
static MethodInfo* t3079_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29930_MI,
	&m17017_MI,
	NULL,
};
extern TypeInfo t4460_TI;
extern TypeInfo t726_TI;
static TypeInfo* t3079_ITIs[] = 
{
	&t4460_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3079_IOs[] = 
{
	{ &t4460_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3079_TI;
extern TypeInfo t3079_TI;
extern TypeInfo t3080_TI;
extern TypeInfo t42_TI;
static Il2CppRGCTXData t3079_RGCTXData[8] = 
{
	&t4461_0_0_0/* Type Usage */,
	&t42_0_0_0/* Type Usage */,
	&t3079_TI/* Class Usage */,
	&t3079_TI/* Static Usage */,
	&t3080_TI/* Class Usage */,
	&m17019_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m29930_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3079_0_0_0;
extern Il2CppType t3079_1_0_0;
struct t3079;
extern Il2CppGenericClass t3079_GC;
TypeInfo t3079_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3079_MIs, t3079_PIs, t3079_FIs, NULL, &t29_TI, NULL, NULL, &t3079_TI, t3079_ITIs, t3079_VT, &EmptyCustomAttributesCache, &t3079_TI, &t3079_0_0_0, &t3079_1_0_0, t3079_IOs, &t3079_GC, NULL, NULL, NULL, t3079_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3079), 0, -1, sizeof(t3079_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.Type>
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t4460_m22794_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22794_GM;
MethodInfo m22794_MI = 
{
	"Compare", NULL, &t4460_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4460_m22794_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22794_GM};
static MethodInfo* t4460_MIs[] =
{
	&m22794_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4460_0_0_0;
extern Il2CppType t4460_1_0_0;
struct t4460;
extern Il2CppGenericClass t4460_GC;
TypeInfo t4460_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4460_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4460_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4460_TI, &t4460_0_0_0, &t4460_1_0_0, NULL, &t4460_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<System.Type>
extern Il2CppType t42_0_0_0;
static ParameterInfo t4461_m22795_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22795_GM;
MethodInfo m22795_MI = 
{
	"CompareTo", NULL, &t4461_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4461_m22795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m22795_GM};
static MethodInfo* t4461_MIs[] =
{
	&m22795_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4461_1_0_0;
struct t4461;
extern Il2CppGenericClass t4461_GC;
TypeInfo t4461_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4461_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4461_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4461_TI, &t4461_0_0_0, &t4461_1_0_0, NULL, &t4461_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m17015_MI;
extern MethodInfo m22795_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Type>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17019_GM;
MethodInfo m17019_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t3080_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17019_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3080_m17020_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17020_GM;
MethodInfo m17020_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t3080_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3080_m17020_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17020_GM};
static MethodInfo* t3080_MIs[] =
{
	&m17019_MI,
	&m17020_MI,
	NULL
};
extern MethodInfo m17020_MI;
static MethodInfo* t3080_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17020_MI,
	&m17017_MI,
	&m17020_MI,
};
static Il2CppInterfaceOffsetPair t3080_IOs[] = 
{
	{ &t4460_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3079_TI;
extern TypeInfo t3079_TI;
extern TypeInfo t3080_TI;
extern TypeInfo t42_TI;
extern TypeInfo t42_TI;
extern TypeInfo t4461_TI;
static Il2CppRGCTXData t3080_RGCTXData[12] = 
{
	&t4461_0_0_0/* Type Usage */,
	&t42_0_0_0/* Type Usage */,
	&t3079_TI/* Class Usage */,
	&t3079_TI/* Static Usage */,
	&t3080_TI/* Class Usage */,
	&m17019_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m29930_MI/* Method Usage */,
	&m17015_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&t4461_TI/* Class Usage */,
	&m22795_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3080_0_0_0;
extern Il2CppType t3080_1_0_0;
struct t3080;
extern Il2CppGenericClass t3080_GC;
extern TypeInfo t1246_TI;
TypeInfo t3080_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3080_MIs, NULL, NULL, NULL, &t3079_TI, NULL, &t1246_TI, &t3080_TI, NULL, t3080_VT, &EmptyCustomAttributesCache, &t3080_TI, &t3080_0_0_0, &t3080_1_0_0, t3080_IOs, &t3080_GC, NULL, NULL, NULL, t3080_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3080), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3073_TI;
#include "t3073MD.h"



// Metadata Definition System.Comparison`1<System.Type>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3073_m17021_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17021_GM;
MethodInfo m17021_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t3073_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3073_m17021_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17021_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3073_m17022_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17022_GM;
MethodInfo m17022_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t3073_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3073_m17022_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17022_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3073_m17023_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17023_GM;
MethodInfo m17023_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t3073_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t3073_m17023_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17023_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3073_m17024_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17024_GM;
MethodInfo m17024_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t3073_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3073_m17024_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17024_GM};
static MethodInfo* t3073_MIs[] =
{
	&m17021_MI,
	&m17022_MI,
	&m17023_MI,
	&m17024_MI,
	NULL
};
extern MethodInfo m17022_MI;
extern MethodInfo m17023_MI;
extern MethodInfo m17024_MI;
static MethodInfo* t3073_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17022_MI,
	&m17023_MI,
	&m17024_MI,
};
static Il2CppInterfaceOffsetPair t3073_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3073_1_0_0;
struct t3073;
extern Il2CppGenericClass t3073_GC;
TypeInfo t3073_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3073_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3073_TI, NULL, t3073_VT, &EmptyCustomAttributesCache, &t3073_TI, &t3073_0_0_0, &t3073_1_0_0, t3073_IOs, &t3073_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3073), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4463_TI;

#include "t298.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AddComponentMenu>
extern MethodInfo m29931_MI;
static PropertyInfo t4463____Current_PropertyInfo = 
{
	&t4463_TI, "Current", &m29931_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4463_PIs[] =
{
	&t4463____Current_PropertyInfo,
	NULL
};
extern Il2CppType t298_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29931_GM;
MethodInfo m29931_MI = 
{
	"get_Current", NULL, &t4463_TI, &t298_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29931_GM};
static MethodInfo* t4463_MIs[] =
{
	&m29931_MI,
	NULL
};
static TypeInfo* t4463_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4463_0_0_0;
extern Il2CppType t4463_1_0_0;
struct t4463;
extern Il2CppGenericClass t4463_GC;
TypeInfo t4463_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4463_MIs, t4463_PIs, NULL, NULL, NULL, NULL, NULL, &t4463_TI, t4463_ITIs, NULL, &EmptyCustomAttributesCache, &t4463_TI, &t4463_0_0_0, &t4463_1_0_0, NULL, &t4463_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3081.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3081_TI;
#include "t3081MD.h"

extern TypeInfo t298_TI;
extern MethodInfo m17029_MI;
extern MethodInfo m22800_MI;
struct t20;
#define m22800(__this, p0, method) (t298 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AddComponentMenu>
extern Il2CppType t20_0_0_1;
FieldInfo t3081_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3081_TI, offsetof(t3081, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3081_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3081_TI, offsetof(t3081, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3081_FIs[] =
{
	&t3081_f0_FieldInfo,
	&t3081_f1_FieldInfo,
	NULL
};
extern MethodInfo m17026_MI;
static PropertyInfo t3081____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3081_TI, "System.Collections.IEnumerator.Current", &m17026_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3081____Current_PropertyInfo = 
{
	&t3081_TI, "Current", &m17029_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3081_PIs[] =
{
	&t3081____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3081____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3081_m17025_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17025_GM;
MethodInfo m17025_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3081_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3081_m17025_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17025_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17026_GM;
MethodInfo m17026_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3081_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17026_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17027_GM;
MethodInfo m17027_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3081_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17027_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17028_GM;
MethodInfo m17028_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3081_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17028_GM};
extern Il2CppType t298_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17029_GM;
MethodInfo m17029_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3081_TI, &t298_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17029_GM};
static MethodInfo* t3081_MIs[] =
{
	&m17025_MI,
	&m17026_MI,
	&m17027_MI,
	&m17028_MI,
	&m17029_MI,
	NULL
};
extern MethodInfo m17028_MI;
extern MethodInfo m17027_MI;
static MethodInfo* t3081_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17026_MI,
	&m17028_MI,
	&m17027_MI,
	&m17029_MI,
};
static TypeInfo* t3081_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4463_TI,
};
static Il2CppInterfaceOffsetPair t3081_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4463_TI, 7},
};
extern TypeInfo t298_TI;
static Il2CppRGCTXData t3081_RGCTXData[3] = 
{
	&m17029_MI/* Method Usage */,
	&t298_TI/* Class Usage */,
	&m22800_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3081_0_0_0;
extern Il2CppType t3081_1_0_0;
extern Il2CppGenericClass t3081_GC;
TypeInfo t3081_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3081_MIs, t3081_PIs, t3081_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3081_TI, t3081_ITIs, t3081_VT, &EmptyCustomAttributesCache, &t3081_TI, &t3081_0_0_0, &t3081_1_0_0, t3081_IOs, &t3081_GC, NULL, NULL, NULL, t3081_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3081)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5724_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AddComponentMenu>
extern MethodInfo m29932_MI;
static PropertyInfo t5724____Count_PropertyInfo = 
{
	&t5724_TI, "Count", &m29932_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29933_MI;
static PropertyInfo t5724____IsReadOnly_PropertyInfo = 
{
	&t5724_TI, "IsReadOnly", &m29933_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5724_PIs[] =
{
	&t5724____Count_PropertyInfo,
	&t5724____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29932_GM;
MethodInfo m29932_MI = 
{
	"get_Count", NULL, &t5724_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29932_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29933_GM;
MethodInfo m29933_MI = 
{
	"get_IsReadOnly", NULL, &t5724_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29933_GM};
extern Il2CppType t298_0_0_0;
extern Il2CppType t298_0_0_0;
static ParameterInfo t5724_m29934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29934_GM;
MethodInfo m29934_MI = 
{
	"Add", NULL, &t5724_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5724_m29934_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29934_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29935_GM;
MethodInfo m29935_MI = 
{
	"Clear", NULL, &t5724_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29935_GM};
extern Il2CppType t298_0_0_0;
static ParameterInfo t5724_m29936_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t298_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29936_GM;
MethodInfo m29936_MI = 
{
	"Contains", NULL, &t5724_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5724_m29936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29936_GM};
extern Il2CppType t3777_0_0_0;
extern Il2CppType t3777_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5724_m29937_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3777_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29937_GM;
MethodInfo m29937_MI = 
{
	"CopyTo", NULL, &t5724_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5724_m29937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29937_GM};
extern Il2CppType t298_0_0_0;
static ParameterInfo t5724_m29938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t298_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29938_GM;
MethodInfo m29938_MI = 
{
	"Remove", NULL, &t5724_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5724_m29938_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29938_GM};
static MethodInfo* t5724_MIs[] =
{
	&m29932_MI,
	&m29933_MI,
	&m29934_MI,
	&m29935_MI,
	&m29936_MI,
	&m29937_MI,
	&m29938_MI,
	NULL
};
extern TypeInfo t5726_TI;
static TypeInfo* t5724_ITIs[] = 
{
	&t603_TI,
	&t5726_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5724_0_0_0;
extern Il2CppType t5724_1_0_0;
struct t5724;
extern Il2CppGenericClass t5724_GC;
TypeInfo t5724_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5724_MIs, t5724_PIs, NULL, NULL, NULL, NULL, NULL, &t5724_TI, t5724_ITIs, NULL, &EmptyCustomAttributesCache, &t5724_TI, &t5724_0_0_0, &t5724_1_0_0, NULL, &t5724_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AddComponentMenu>
extern Il2CppType t4463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29939_GM;
MethodInfo m29939_MI = 
{
	"GetEnumerator", NULL, &t5726_TI, &t4463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29939_GM};
static MethodInfo* t5726_MIs[] =
{
	&m29939_MI,
	NULL
};
static TypeInfo* t5726_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5726_0_0_0;
extern Il2CppType t5726_1_0_0;
struct t5726;
extern Il2CppGenericClass t5726_GC;
TypeInfo t5726_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5726_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5726_TI, t5726_ITIs, NULL, &EmptyCustomAttributesCache, &t5726_TI, &t5726_0_0_0, &t5726_1_0_0, NULL, &t5726_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5725_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AddComponentMenu>
extern MethodInfo m29940_MI;
extern MethodInfo m29941_MI;
static PropertyInfo t5725____Item_PropertyInfo = 
{
	&t5725_TI, "Item", &m29940_MI, &m29941_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5725_PIs[] =
{
	&t5725____Item_PropertyInfo,
	NULL
};
extern Il2CppType t298_0_0_0;
static ParameterInfo t5725_m29942_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t298_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29942_GM;
MethodInfo m29942_MI = 
{
	"IndexOf", NULL, &t5725_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5725_m29942_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29942_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t298_0_0_0;
static ParameterInfo t5725_m29943_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29943_GM;
MethodInfo m29943_MI = 
{
	"Insert", NULL, &t5725_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5725_m29943_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29943_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5725_m29944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29944_GM;
MethodInfo m29944_MI = 
{
	"RemoveAt", NULL, &t5725_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5725_m29944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29944_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5725_m29940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t298_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29940_GM;
MethodInfo m29940_MI = 
{
	"get_Item", NULL, &t5725_TI, &t298_0_0_0, RuntimeInvoker_t29_t44, t5725_m29940_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29940_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t298_0_0_0;
static ParameterInfo t5725_m29941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29941_GM;
MethodInfo m29941_MI = 
{
	"set_Item", NULL, &t5725_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5725_m29941_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29941_GM};
static MethodInfo* t5725_MIs[] =
{
	&m29942_MI,
	&m29943_MI,
	&m29944_MI,
	&m29940_MI,
	&m29941_MI,
	NULL
};
static TypeInfo* t5725_ITIs[] = 
{
	&t603_TI,
	&t5724_TI,
	&t5726_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5725_0_0_0;
extern Il2CppType t5725_1_0_0;
struct t5725;
extern Il2CppGenericClass t5725_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5725_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5725_MIs, t5725_PIs, NULL, NULL, NULL, NULL, NULL, &t5725_TI, t5725_ITIs, NULL, &t1908__CustomAttributeCache, &t5725_TI, &t5725_0_0_0, &t5725_1_0_0, NULL, &t5725_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4465_TI;

#include "t632.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterModifier>
extern MethodInfo m29945_MI;
static PropertyInfo t4465____Current_PropertyInfo = 
{
	&t4465_TI, "Current", &m29945_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4465_PIs[] =
{
	&t4465____Current_PropertyInfo,
	NULL
};
extern Il2CppType t632_0_0_0;
extern void* RuntimeInvoker_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29945_GM;
MethodInfo m29945_MI = 
{
	"get_Current", NULL, &t4465_TI, &t632_0_0_0, RuntimeInvoker_t632, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29945_GM};
static MethodInfo* t4465_MIs[] =
{
	&m29945_MI,
	NULL
};
static TypeInfo* t4465_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4465_0_0_0;
extern Il2CppType t4465_1_0_0;
struct t4465;
extern Il2CppGenericClass t4465_GC;
TypeInfo t4465_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4465_MIs, t4465_PIs, NULL, NULL, NULL, NULL, NULL, &t4465_TI, t4465_ITIs, NULL, &EmptyCustomAttributesCache, &t4465_TI, &t4465_0_0_0, &t4465_1_0_0, NULL, &t4465_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3082.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3082_TI;
#include "t3082MD.h"

extern TypeInfo t632_TI;
extern MethodInfo m17034_MI;
extern MethodInfo m22811_MI;
struct t20;
 t632  m22811 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17030_MI;
 void m17030 (t3082 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17031_MI;
 t29 * m17031 (t3082 * __this, MethodInfo* method){
	{
		t632  L_0 = m17034(__this, &m17034_MI);
		t632  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t632_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17032_MI;
 void m17032 (t3082 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17033_MI;
 bool m17033 (t3082 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t632  m17034 (t3082 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t632  L_8 = m22811(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22811_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
extern Il2CppType t20_0_0_1;
FieldInfo t3082_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3082_TI, offsetof(t3082, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3082_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3082_TI, offsetof(t3082, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3082_FIs[] =
{
	&t3082_f0_FieldInfo,
	&t3082_f1_FieldInfo,
	NULL
};
static PropertyInfo t3082____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3082_TI, "System.Collections.IEnumerator.Current", &m17031_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3082____Current_PropertyInfo = 
{
	&t3082_TI, "Current", &m17034_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3082_PIs[] =
{
	&t3082____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3082____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3082_m17030_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17030_GM;
MethodInfo m17030_MI = 
{
	".ctor", (methodPointerType)&m17030, &t3082_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3082_m17030_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17030_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17031_GM;
MethodInfo m17031_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17031, &t3082_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17031_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17032_GM;
MethodInfo m17032_MI = 
{
	"Dispose", (methodPointerType)&m17032, &t3082_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17032_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17033_GM;
MethodInfo m17033_MI = 
{
	"MoveNext", (methodPointerType)&m17033, &t3082_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17033_GM};
extern Il2CppType t632_0_0_0;
extern void* RuntimeInvoker_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17034_GM;
MethodInfo m17034_MI = 
{
	"get_Current", (methodPointerType)&m17034, &t3082_TI, &t632_0_0_0, RuntimeInvoker_t632, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17034_GM};
static MethodInfo* t3082_MIs[] =
{
	&m17030_MI,
	&m17031_MI,
	&m17032_MI,
	&m17033_MI,
	&m17034_MI,
	NULL
};
static MethodInfo* t3082_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17031_MI,
	&m17033_MI,
	&m17032_MI,
	&m17034_MI,
};
static TypeInfo* t3082_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4465_TI,
};
static Il2CppInterfaceOffsetPair t3082_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4465_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3082_0_0_0;
extern Il2CppType t3082_1_0_0;
extern Il2CppGenericClass t3082_GC;
TypeInfo t3082_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3082_MIs, t3082_PIs, t3082_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3082_TI, t3082_ITIs, t3082_VT, &EmptyCustomAttributesCache, &t3082_TI, &t3082_0_0_0, &t3082_1_0_0, t3082_IOs, &t3082_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3082)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5727_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ParameterModifier>
extern MethodInfo m29946_MI;
static PropertyInfo t5727____Count_PropertyInfo = 
{
	&t5727_TI, "Count", &m29946_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29947_MI;
static PropertyInfo t5727____IsReadOnly_PropertyInfo = 
{
	&t5727_TI, "IsReadOnly", &m29947_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5727_PIs[] =
{
	&t5727____Count_PropertyInfo,
	&t5727____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29946_GM;
MethodInfo m29946_MI = 
{
	"get_Count", NULL, &t5727_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29946_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29947_GM;
MethodInfo m29947_MI = 
{
	"get_IsReadOnly", NULL, &t5727_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29947_GM};
extern Il2CppType t632_0_0_0;
extern Il2CppType t632_0_0_0;
static ParameterInfo t5727_m29948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t632_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29948_GM;
MethodInfo m29948_MI = 
{
	"Add", NULL, &t5727_TI, &t21_0_0_0, RuntimeInvoker_t21_t632, t5727_m29948_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29948_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29949_GM;
MethodInfo m29949_MI = 
{
	"Clear", NULL, &t5727_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29949_GM};
extern Il2CppType t632_0_0_0;
static ParameterInfo t5727_m29950_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t632_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29950_GM;
MethodInfo m29950_MI = 
{
	"Contains", NULL, &t5727_TI, &t40_0_0_0, RuntimeInvoker_t40_t632, t5727_m29950_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29950_GM};
extern Il2CppType t634_0_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5727_m29951_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t634_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29951_GM;
MethodInfo m29951_MI = 
{
	"CopyTo", NULL, &t5727_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5727_m29951_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29951_GM};
extern Il2CppType t632_0_0_0;
static ParameterInfo t5727_m29952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t632_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29952_GM;
MethodInfo m29952_MI = 
{
	"Remove", NULL, &t5727_TI, &t40_0_0_0, RuntimeInvoker_t40_t632, t5727_m29952_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29952_GM};
static MethodInfo* t5727_MIs[] =
{
	&m29946_MI,
	&m29947_MI,
	&m29948_MI,
	&m29949_MI,
	&m29950_MI,
	&m29951_MI,
	&m29952_MI,
	NULL
};
extern TypeInfo t5729_TI;
static TypeInfo* t5727_ITIs[] = 
{
	&t603_TI,
	&t5729_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5727_0_0_0;
extern Il2CppType t5727_1_0_0;
struct t5727;
extern Il2CppGenericClass t5727_GC;
TypeInfo t5727_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5727_MIs, t5727_PIs, NULL, NULL, NULL, NULL, NULL, &t5727_TI, t5727_ITIs, NULL, &EmptyCustomAttributesCache, &t5727_TI, &t5727_0_0_0, &t5727_1_0_0, NULL, &t5727_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterModifier>
extern Il2CppType t4465_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29953_GM;
MethodInfo m29953_MI = 
{
	"GetEnumerator", NULL, &t5729_TI, &t4465_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29953_GM};
static MethodInfo* t5729_MIs[] =
{
	&m29953_MI,
	NULL
};
static TypeInfo* t5729_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5729_0_0_0;
extern Il2CppType t5729_1_0_0;
struct t5729;
extern Il2CppGenericClass t5729_GC;
TypeInfo t5729_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5729_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5729_TI, t5729_ITIs, NULL, &EmptyCustomAttributesCache, &t5729_TI, &t5729_0_0_0, &t5729_1_0_0, NULL, &t5729_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5728_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ParameterModifier>
extern MethodInfo m29954_MI;
extern MethodInfo m29955_MI;
static PropertyInfo t5728____Item_PropertyInfo = 
{
	&t5728_TI, "Item", &m29954_MI, &m29955_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5728_PIs[] =
{
	&t5728____Item_PropertyInfo,
	NULL
};
extern Il2CppType t632_0_0_0;
static ParameterInfo t5728_m29956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t632_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29956_GM;
MethodInfo m29956_MI = 
{
	"IndexOf", NULL, &t5728_TI, &t44_0_0_0, RuntimeInvoker_t44_t632, t5728_m29956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29956_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t632_0_0_0;
static ParameterInfo t5728_m29957_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t632_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29957_GM;
MethodInfo m29957_MI = 
{
	"Insert", NULL, &t5728_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t632, t5728_m29957_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29957_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5728_m29958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29958_GM;
MethodInfo m29958_MI = 
{
	"RemoveAt", NULL, &t5728_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5728_m29958_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29958_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5728_m29954_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t632_0_0_0;
extern void* RuntimeInvoker_t632_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29954_GM;
MethodInfo m29954_MI = 
{
	"get_Item", NULL, &t5728_TI, &t632_0_0_0, RuntimeInvoker_t632_t44, t5728_m29954_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29954_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t632_0_0_0;
static ParameterInfo t5728_m29955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t632_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29955_GM;
MethodInfo m29955_MI = 
{
	"set_Item", NULL, &t5728_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t632, t5728_m29955_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29955_GM};
static MethodInfo* t5728_MIs[] =
{
	&m29956_MI,
	&m29957_MI,
	&m29958_MI,
	&m29954_MI,
	&m29955_MI,
	NULL
};
static TypeInfo* t5728_ITIs[] = 
{
	&t603_TI,
	&t5727_TI,
	&t5729_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5728_0_0_0;
extern Il2CppType t5728_1_0_0;
struct t5728;
extern Il2CppGenericClass t5728_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5728_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5728_MIs, t5728_PIs, NULL, NULL, NULL, NULL, NULL, &t5728_TI, t5728_ITIs, NULL, &t1908__CustomAttributeCache, &t5728_TI, &t5728_0_0_0, &t5728_1_0_0, NULL, &t5728_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4467_TI;

#include "t539.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.WritableAttribute>
extern MethodInfo m29959_MI;
static PropertyInfo t4467____Current_PropertyInfo = 
{
	&t4467_TI, "Current", &m29959_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4467_PIs[] =
{
	&t4467____Current_PropertyInfo,
	NULL
};
extern Il2CppType t539_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29959_GM;
MethodInfo m29959_MI = 
{
	"get_Current", NULL, &t4467_TI, &t539_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29959_GM};
static MethodInfo* t4467_MIs[] =
{
	&m29959_MI,
	NULL
};
static TypeInfo* t4467_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4467_0_0_0;
extern Il2CppType t4467_1_0_0;
struct t4467;
extern Il2CppGenericClass t4467_GC;
TypeInfo t4467_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4467_MIs, t4467_PIs, NULL, NULL, NULL, NULL, NULL, &t4467_TI, t4467_ITIs, NULL, &EmptyCustomAttributesCache, &t4467_TI, &t4467_0_0_0, &t4467_1_0_0, NULL, &t4467_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3083.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3083_TI;
#include "t3083MD.h"

extern TypeInfo t539_TI;
extern MethodInfo m17039_MI;
extern MethodInfo m22822_MI;
struct t20;
#define m22822(__this, p0, method) (t539 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.WritableAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3083_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3083_TI, offsetof(t3083, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3083_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3083_TI, offsetof(t3083, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3083_FIs[] =
{
	&t3083_f0_FieldInfo,
	&t3083_f1_FieldInfo,
	NULL
};
extern MethodInfo m17036_MI;
static PropertyInfo t3083____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3083_TI, "System.Collections.IEnumerator.Current", &m17036_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3083____Current_PropertyInfo = 
{
	&t3083_TI, "Current", &m17039_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3083_PIs[] =
{
	&t3083____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3083____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3083_m17035_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17035_GM;
MethodInfo m17035_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3083_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3083_m17035_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17035_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17036_GM;
MethodInfo m17036_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3083_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17036_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17037_GM;
MethodInfo m17037_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3083_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17037_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17038_GM;
MethodInfo m17038_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3083_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17038_GM};
extern Il2CppType t539_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17039_GM;
MethodInfo m17039_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3083_TI, &t539_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17039_GM};
static MethodInfo* t3083_MIs[] =
{
	&m17035_MI,
	&m17036_MI,
	&m17037_MI,
	&m17038_MI,
	&m17039_MI,
	NULL
};
extern MethodInfo m17038_MI;
extern MethodInfo m17037_MI;
static MethodInfo* t3083_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17036_MI,
	&m17038_MI,
	&m17037_MI,
	&m17039_MI,
};
static TypeInfo* t3083_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4467_TI,
};
static Il2CppInterfaceOffsetPair t3083_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4467_TI, 7},
};
extern TypeInfo t539_TI;
static Il2CppRGCTXData t3083_RGCTXData[3] = 
{
	&m17039_MI/* Method Usage */,
	&t539_TI/* Class Usage */,
	&m22822_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3083_0_0_0;
extern Il2CppType t3083_1_0_0;
extern Il2CppGenericClass t3083_GC;
TypeInfo t3083_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3083_MIs, t3083_PIs, t3083_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3083_TI, t3083_ITIs, t3083_VT, &EmptyCustomAttributesCache, &t3083_TI, &t3083_0_0_0, &t3083_1_0_0, t3083_IOs, &t3083_GC, NULL, NULL, NULL, t3083_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3083)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5730_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.WritableAttribute>
extern MethodInfo m29960_MI;
static PropertyInfo t5730____Count_PropertyInfo = 
{
	&t5730_TI, "Count", &m29960_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29961_MI;
static PropertyInfo t5730____IsReadOnly_PropertyInfo = 
{
	&t5730_TI, "IsReadOnly", &m29961_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5730_PIs[] =
{
	&t5730____Count_PropertyInfo,
	&t5730____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29960_GM;
MethodInfo m29960_MI = 
{
	"get_Count", NULL, &t5730_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29960_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29961_GM;
MethodInfo m29961_MI = 
{
	"get_IsReadOnly", NULL, &t5730_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29961_GM};
extern Il2CppType t539_0_0_0;
extern Il2CppType t539_0_0_0;
static ParameterInfo t5730_m29962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t539_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29962_GM;
MethodInfo m29962_MI = 
{
	"Add", NULL, &t5730_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5730_m29962_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29962_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29963_GM;
MethodInfo m29963_MI = 
{
	"Clear", NULL, &t5730_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29963_GM};
extern Il2CppType t539_0_0_0;
static ParameterInfo t5730_m29964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t539_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29964_GM;
MethodInfo m29964_MI = 
{
	"Contains", NULL, &t5730_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5730_m29964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29964_GM};
extern Il2CppType t3778_0_0_0;
extern Il2CppType t3778_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5730_m29965_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3778_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29965_GM;
MethodInfo m29965_MI = 
{
	"CopyTo", NULL, &t5730_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5730_m29965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29965_GM};
extern Il2CppType t539_0_0_0;
static ParameterInfo t5730_m29966_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t539_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29966_GM;
MethodInfo m29966_MI = 
{
	"Remove", NULL, &t5730_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5730_m29966_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29966_GM};
static MethodInfo* t5730_MIs[] =
{
	&m29960_MI,
	&m29961_MI,
	&m29962_MI,
	&m29963_MI,
	&m29964_MI,
	&m29965_MI,
	&m29966_MI,
	NULL
};
extern TypeInfo t5732_TI;
static TypeInfo* t5730_ITIs[] = 
{
	&t603_TI,
	&t5732_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5730_0_0_0;
extern Il2CppType t5730_1_0_0;
struct t5730;
extern Il2CppGenericClass t5730_GC;
TypeInfo t5730_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5730_MIs, t5730_PIs, NULL, NULL, NULL, NULL, NULL, &t5730_TI, t5730_ITIs, NULL, &EmptyCustomAttributesCache, &t5730_TI, &t5730_0_0_0, &t5730_1_0_0, NULL, &t5730_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.WritableAttribute>
extern Il2CppType t4467_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29967_GM;
MethodInfo m29967_MI = 
{
	"GetEnumerator", NULL, &t5732_TI, &t4467_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29967_GM};
static MethodInfo* t5732_MIs[] =
{
	&m29967_MI,
	NULL
};
static TypeInfo* t5732_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5732_0_0_0;
extern Il2CppType t5732_1_0_0;
struct t5732;
extern Il2CppGenericClass t5732_GC;
TypeInfo t5732_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5732_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5732_TI, t5732_ITIs, NULL, &EmptyCustomAttributesCache, &t5732_TI, &t5732_0_0_0, &t5732_1_0_0, NULL, &t5732_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5731_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.WritableAttribute>
extern MethodInfo m29968_MI;
extern MethodInfo m29969_MI;
static PropertyInfo t5731____Item_PropertyInfo = 
{
	&t5731_TI, "Item", &m29968_MI, &m29969_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5731_PIs[] =
{
	&t5731____Item_PropertyInfo,
	NULL
};
extern Il2CppType t539_0_0_0;
static ParameterInfo t5731_m29970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t539_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29970_GM;
MethodInfo m29970_MI = 
{
	"IndexOf", NULL, &t5731_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5731_m29970_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29970_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t539_0_0_0;
static ParameterInfo t5731_m29971_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t539_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29971_GM;
MethodInfo m29971_MI = 
{
	"Insert", NULL, &t5731_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5731_m29971_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29971_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5731_m29972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29972_GM;
MethodInfo m29972_MI = 
{
	"RemoveAt", NULL, &t5731_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5731_m29972_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29972_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5731_m29968_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t539_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29968_GM;
MethodInfo m29968_MI = 
{
	"get_Item", NULL, &t5731_TI, &t539_0_0_0, RuntimeInvoker_t29_t44, t5731_m29968_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29968_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t539_0_0_0;
static ParameterInfo t5731_m29969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t539_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29969_GM;
MethodInfo m29969_MI = 
{
	"set_Item", NULL, &t5731_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5731_m29969_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29969_GM};
static MethodInfo* t5731_MIs[] =
{
	&m29970_MI,
	&m29971_MI,
	&m29972_MI,
	&m29968_MI,
	&m29969_MI,
	NULL
};
static TypeInfo* t5731_ITIs[] = 
{
	&t603_TI,
	&t5730_TI,
	&t5732_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5731_0_0_0;
extern Il2CppType t5731_1_0_0;
struct t5731;
extern Il2CppGenericClass t5731_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5731_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5731_MIs, t5731_PIs, NULL, NULL, NULL, NULL, NULL, &t5731_TI, t5731_ITIs, NULL, &t1908__CustomAttributeCache, &t5731_TI, &t5731_0_0_0, &t5731_1_0_0, NULL, &t5731_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4469_TI;

#include "t540.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AssemblyIsEditorAssembly>
extern MethodInfo m29973_MI;
static PropertyInfo t4469____Current_PropertyInfo = 
{
	&t4469_TI, "Current", &m29973_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4469_PIs[] =
{
	&t4469____Current_PropertyInfo,
	NULL
};
extern Il2CppType t540_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29973_GM;
MethodInfo m29973_MI = 
{
	"get_Current", NULL, &t4469_TI, &t540_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29973_GM};
static MethodInfo* t4469_MIs[] =
{
	&m29973_MI,
	NULL
};
static TypeInfo* t4469_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4469_0_0_0;
extern Il2CppType t4469_1_0_0;
struct t4469;
extern Il2CppGenericClass t4469_GC;
TypeInfo t4469_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4469_MIs, t4469_PIs, NULL, NULL, NULL, NULL, NULL, &t4469_TI, t4469_ITIs, NULL, &EmptyCustomAttributesCache, &t4469_TI, &t4469_0_0_0, &t4469_1_0_0, NULL, &t4469_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3084.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3084_TI;
#include "t3084MD.h"

extern TypeInfo t540_TI;
extern MethodInfo m17044_MI;
extern MethodInfo m22833_MI;
struct t20;
#define m22833(__this, p0, method) (t540 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AssemblyIsEditorAssembly>
extern Il2CppType t20_0_0_1;
FieldInfo t3084_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3084_TI, offsetof(t3084, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3084_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3084_TI, offsetof(t3084, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3084_FIs[] =
{
	&t3084_f0_FieldInfo,
	&t3084_f1_FieldInfo,
	NULL
};
extern MethodInfo m17041_MI;
static PropertyInfo t3084____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3084_TI, "System.Collections.IEnumerator.Current", &m17041_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3084____Current_PropertyInfo = 
{
	&t3084_TI, "Current", &m17044_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3084_PIs[] =
{
	&t3084____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3084____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3084_m17040_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17040_GM;
MethodInfo m17040_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3084_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3084_m17040_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17040_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17041_GM;
MethodInfo m17041_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3084_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17041_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17042_GM;
MethodInfo m17042_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3084_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17042_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17043_GM;
MethodInfo m17043_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3084_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17043_GM};
extern Il2CppType t540_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17044_GM;
MethodInfo m17044_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3084_TI, &t540_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17044_GM};
static MethodInfo* t3084_MIs[] =
{
	&m17040_MI,
	&m17041_MI,
	&m17042_MI,
	&m17043_MI,
	&m17044_MI,
	NULL
};
extern MethodInfo m17043_MI;
extern MethodInfo m17042_MI;
static MethodInfo* t3084_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17041_MI,
	&m17043_MI,
	&m17042_MI,
	&m17044_MI,
};
static TypeInfo* t3084_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4469_TI,
};
static Il2CppInterfaceOffsetPair t3084_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4469_TI, 7},
};
extern TypeInfo t540_TI;
static Il2CppRGCTXData t3084_RGCTXData[3] = 
{
	&m17044_MI/* Method Usage */,
	&t540_TI/* Class Usage */,
	&m22833_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3084_0_0_0;
extern Il2CppType t3084_1_0_0;
extern Il2CppGenericClass t3084_GC;
TypeInfo t3084_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3084_MIs, t3084_PIs, t3084_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3084_TI, t3084_ITIs, t3084_VT, &EmptyCustomAttributesCache, &t3084_TI, &t3084_0_0_0, &t3084_1_0_0, t3084_IOs, &t3084_GC, NULL, NULL, NULL, t3084_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3084)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5733_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AssemblyIsEditorAssembly>
extern MethodInfo m29974_MI;
static PropertyInfo t5733____Count_PropertyInfo = 
{
	&t5733_TI, "Count", &m29974_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29975_MI;
static PropertyInfo t5733____IsReadOnly_PropertyInfo = 
{
	&t5733_TI, "IsReadOnly", &m29975_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5733_PIs[] =
{
	&t5733____Count_PropertyInfo,
	&t5733____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29974_GM;
MethodInfo m29974_MI = 
{
	"get_Count", NULL, &t5733_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29974_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29975_GM;
MethodInfo m29975_MI = 
{
	"get_IsReadOnly", NULL, &t5733_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29975_GM};
extern Il2CppType t540_0_0_0;
extern Il2CppType t540_0_0_0;
static ParameterInfo t5733_m29976_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t540_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29976_GM;
MethodInfo m29976_MI = 
{
	"Add", NULL, &t5733_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5733_m29976_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29976_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29977_GM;
MethodInfo m29977_MI = 
{
	"Clear", NULL, &t5733_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29977_GM};
extern Il2CppType t540_0_0_0;
static ParameterInfo t5733_m29978_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t540_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29978_GM;
MethodInfo m29978_MI = 
{
	"Contains", NULL, &t5733_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5733_m29978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29978_GM};
extern Il2CppType t3779_0_0_0;
extern Il2CppType t3779_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5733_m29979_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3779_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29979_GM;
MethodInfo m29979_MI = 
{
	"CopyTo", NULL, &t5733_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5733_m29979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29979_GM};
extern Il2CppType t540_0_0_0;
static ParameterInfo t5733_m29980_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t540_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29980_GM;
MethodInfo m29980_MI = 
{
	"Remove", NULL, &t5733_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5733_m29980_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29980_GM};
static MethodInfo* t5733_MIs[] =
{
	&m29974_MI,
	&m29975_MI,
	&m29976_MI,
	&m29977_MI,
	&m29978_MI,
	&m29979_MI,
	&m29980_MI,
	NULL
};
extern TypeInfo t5735_TI;
static TypeInfo* t5733_ITIs[] = 
{
	&t603_TI,
	&t5735_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5733_0_0_0;
extern Il2CppType t5733_1_0_0;
struct t5733;
extern Il2CppGenericClass t5733_GC;
TypeInfo t5733_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5733_MIs, t5733_PIs, NULL, NULL, NULL, NULL, NULL, &t5733_TI, t5733_ITIs, NULL, &EmptyCustomAttributesCache, &t5733_TI, &t5733_0_0_0, &t5733_1_0_0, NULL, &t5733_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AssemblyIsEditorAssembly>
extern Il2CppType t4469_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29981_GM;
MethodInfo m29981_MI = 
{
	"GetEnumerator", NULL, &t5735_TI, &t4469_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29981_GM};
static MethodInfo* t5735_MIs[] =
{
	&m29981_MI,
	NULL
};
static TypeInfo* t5735_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5735_0_0_0;
extern Il2CppType t5735_1_0_0;
struct t5735;
extern Il2CppGenericClass t5735_GC;
TypeInfo t5735_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5735_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5735_TI, t5735_ITIs, NULL, &EmptyCustomAttributesCache, &t5735_TI, &t5735_0_0_0, &t5735_1_0_0, NULL, &t5735_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5734_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AssemblyIsEditorAssembly>
extern MethodInfo m29982_MI;
extern MethodInfo m29983_MI;
static PropertyInfo t5734____Item_PropertyInfo = 
{
	&t5734_TI, "Item", &m29982_MI, &m29983_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5734_PIs[] =
{
	&t5734____Item_PropertyInfo,
	NULL
};
extern Il2CppType t540_0_0_0;
static ParameterInfo t5734_m29984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t540_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29984_GM;
MethodInfo m29984_MI = 
{
	"IndexOf", NULL, &t5734_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5734_m29984_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29984_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t540_0_0_0;
static ParameterInfo t5734_m29985_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t540_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29985_GM;
MethodInfo m29985_MI = 
{
	"Insert", NULL, &t5734_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5734_m29985_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29985_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5734_m29986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29986_GM;
MethodInfo m29986_MI = 
{
	"RemoveAt", NULL, &t5734_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5734_m29986_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29986_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5734_m29982_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t540_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29982_GM;
MethodInfo m29982_MI = 
{
	"get_Item", NULL, &t5734_TI, &t540_0_0_0, RuntimeInvoker_t29_t44, t5734_m29982_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29982_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t540_0_0_0;
static ParameterInfo t5734_m29983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t540_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29983_GM;
MethodInfo m29983_MI = 
{
	"set_Item", NULL, &t5734_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5734_m29983_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29983_GM};
static MethodInfo* t5734_MIs[] =
{
	&m29984_MI,
	&m29985_MI,
	&m29986_MI,
	&m29982_MI,
	&m29983_MI,
	NULL
};
static TypeInfo* t5734_ITIs[] = 
{
	&t603_TI,
	&t5733_TI,
	&t5735_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5734_0_0_0;
extern Il2CppType t5734_1_0_0;
struct t5734;
extern Il2CppGenericClass t5734_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5734_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5734_MIs, t5734_PIs, NULL, NULL, NULL, NULL, NULL, &t5734_TI, t5734_ITIs, NULL, &t1908__CustomAttributeCache, &t5734_TI, &t5734_0_0_0, &t5734_1_0_0, NULL, &t5734_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4471_TI;

#include "t498.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.CameraClearFlags>
extern MethodInfo m29987_MI;
static PropertyInfo t4471____Current_PropertyInfo = 
{
	&t4471_TI, "Current", &m29987_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4471_PIs[] =
{
	&t4471____Current_PropertyInfo,
	NULL
};
extern Il2CppType t498_0_0_0;
extern void* RuntimeInvoker_t498 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29987_GM;
MethodInfo m29987_MI = 
{
	"get_Current", NULL, &t4471_TI, &t498_0_0_0, RuntimeInvoker_t498, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29987_GM};
static MethodInfo* t4471_MIs[] =
{
	&m29987_MI,
	NULL
};
static TypeInfo* t4471_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4471_0_0_0;
extern Il2CppType t4471_1_0_0;
struct t4471;
extern Il2CppGenericClass t4471_GC;
TypeInfo t4471_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4471_MIs, t4471_PIs, NULL, NULL, NULL, NULL, NULL, &t4471_TI, t4471_ITIs, NULL, &EmptyCustomAttributesCache, &t4471_TI, &t4471_0_0_0, &t4471_1_0_0, NULL, &t4471_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3085.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3085_TI;
#include "t3085MD.h"

extern TypeInfo t498_TI;
extern MethodInfo m17049_MI;
extern MethodInfo m22844_MI;
struct t20;
 int32_t m22844 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17045_MI;
 void m17045 (t3085 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17046_MI;
 t29 * m17046 (t3085 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17049(__this, &m17049_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t498_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17047_MI;
 void m17047 (t3085 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17048_MI;
 bool m17048 (t3085 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17049 (t3085 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22844(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22844_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.CameraClearFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3085_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3085_TI, offsetof(t3085, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3085_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3085_TI, offsetof(t3085, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3085_FIs[] =
{
	&t3085_f0_FieldInfo,
	&t3085_f1_FieldInfo,
	NULL
};
static PropertyInfo t3085____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3085_TI, "System.Collections.IEnumerator.Current", &m17046_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3085____Current_PropertyInfo = 
{
	&t3085_TI, "Current", &m17049_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3085_PIs[] =
{
	&t3085____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3085____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3085_m17045_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17045_GM;
MethodInfo m17045_MI = 
{
	".ctor", (methodPointerType)&m17045, &t3085_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3085_m17045_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17045_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17046_GM;
MethodInfo m17046_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17046, &t3085_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17046_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17047_GM;
MethodInfo m17047_MI = 
{
	"Dispose", (methodPointerType)&m17047, &t3085_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17047_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17048_GM;
MethodInfo m17048_MI = 
{
	"MoveNext", (methodPointerType)&m17048, &t3085_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17048_GM};
extern Il2CppType t498_0_0_0;
extern void* RuntimeInvoker_t498 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17049_GM;
MethodInfo m17049_MI = 
{
	"get_Current", (methodPointerType)&m17049, &t3085_TI, &t498_0_0_0, RuntimeInvoker_t498, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17049_GM};
static MethodInfo* t3085_MIs[] =
{
	&m17045_MI,
	&m17046_MI,
	&m17047_MI,
	&m17048_MI,
	&m17049_MI,
	NULL
};
static MethodInfo* t3085_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17046_MI,
	&m17048_MI,
	&m17047_MI,
	&m17049_MI,
};
static TypeInfo* t3085_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4471_TI,
};
static Il2CppInterfaceOffsetPair t3085_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4471_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3085_0_0_0;
extern Il2CppType t3085_1_0_0;
extern Il2CppGenericClass t3085_GC;
TypeInfo t3085_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3085_MIs, t3085_PIs, t3085_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3085_TI, t3085_ITIs, t3085_VT, &EmptyCustomAttributesCache, &t3085_TI, &t3085_0_0_0, &t3085_1_0_0, t3085_IOs, &t3085_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3085)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5736_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.CameraClearFlags>
extern MethodInfo m29988_MI;
static PropertyInfo t5736____Count_PropertyInfo = 
{
	&t5736_TI, "Count", &m29988_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29989_MI;
static PropertyInfo t5736____IsReadOnly_PropertyInfo = 
{
	&t5736_TI, "IsReadOnly", &m29989_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5736_PIs[] =
{
	&t5736____Count_PropertyInfo,
	&t5736____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29988_GM;
MethodInfo m29988_MI = 
{
	"get_Count", NULL, &t5736_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29988_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29989_GM;
MethodInfo m29989_MI = 
{
	"get_IsReadOnly", NULL, &t5736_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29989_GM};
extern Il2CppType t498_0_0_0;
extern Il2CppType t498_0_0_0;
static ParameterInfo t5736_m29990_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t498_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29990_GM;
MethodInfo m29990_MI = 
{
	"Add", NULL, &t5736_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5736_m29990_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29990_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29991_GM;
MethodInfo m29991_MI = 
{
	"Clear", NULL, &t5736_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29991_GM};
extern Il2CppType t498_0_0_0;
static ParameterInfo t5736_m29992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t498_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29992_GM;
MethodInfo m29992_MI = 
{
	"Contains", NULL, &t5736_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5736_m29992_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29992_GM};
extern Il2CppType t3780_0_0_0;
extern Il2CppType t3780_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5736_m29993_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3780_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29993_GM;
MethodInfo m29993_MI = 
{
	"CopyTo", NULL, &t5736_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5736_m29993_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29993_GM};
extern Il2CppType t498_0_0_0;
static ParameterInfo t5736_m29994_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t498_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29994_GM;
MethodInfo m29994_MI = 
{
	"Remove", NULL, &t5736_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5736_m29994_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29994_GM};
static MethodInfo* t5736_MIs[] =
{
	&m29988_MI,
	&m29989_MI,
	&m29990_MI,
	&m29991_MI,
	&m29992_MI,
	&m29993_MI,
	&m29994_MI,
	NULL
};
extern TypeInfo t5738_TI;
static TypeInfo* t5736_ITIs[] = 
{
	&t603_TI,
	&t5738_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5736_0_0_0;
extern Il2CppType t5736_1_0_0;
struct t5736;
extern Il2CppGenericClass t5736_GC;
TypeInfo t5736_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5736_MIs, t5736_PIs, NULL, NULL, NULL, NULL, NULL, &t5736_TI, t5736_ITIs, NULL, &EmptyCustomAttributesCache, &t5736_TI, &t5736_0_0_0, &t5736_1_0_0, NULL, &t5736_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.CameraClearFlags>
extern Il2CppType t4471_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29995_GM;
MethodInfo m29995_MI = 
{
	"GetEnumerator", NULL, &t5738_TI, &t4471_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29995_GM};
static MethodInfo* t5738_MIs[] =
{
	&m29995_MI,
	NULL
};
static TypeInfo* t5738_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5738_0_0_0;
extern Il2CppType t5738_1_0_0;
struct t5738;
extern Il2CppGenericClass t5738_GC;
TypeInfo t5738_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5738_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5738_TI, t5738_ITIs, NULL, &EmptyCustomAttributesCache, &t5738_TI, &t5738_0_0_0, &t5738_1_0_0, NULL, &t5738_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5737_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.CameraClearFlags>
extern MethodInfo m29996_MI;
extern MethodInfo m29997_MI;
static PropertyInfo t5737____Item_PropertyInfo = 
{
	&t5737_TI, "Item", &m29996_MI, &m29997_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5737_PIs[] =
{
	&t5737____Item_PropertyInfo,
	NULL
};
extern Il2CppType t498_0_0_0;
static ParameterInfo t5737_m29998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t498_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29998_GM;
MethodInfo m29998_MI = 
{
	"IndexOf", NULL, &t5737_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5737_m29998_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29998_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t498_0_0_0;
static ParameterInfo t5737_m29999_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t498_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29999_GM;
MethodInfo m29999_MI = 
{
	"Insert", NULL, &t5737_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5737_m29999_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29999_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5737_m30000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30000_GM;
MethodInfo m30000_MI = 
{
	"RemoveAt", NULL, &t5737_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5737_m30000_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30000_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5737_m29996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t498_0_0_0;
extern void* RuntimeInvoker_t498_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29996_GM;
MethodInfo m29996_MI = 
{
	"get_Item", NULL, &t5737_TI, &t498_0_0_0, RuntimeInvoker_t498_t44, t5737_m29996_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29996_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t498_0_0_0;
static ParameterInfo t5737_m29997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t498_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29997_GM;
MethodInfo m29997_MI = 
{
	"set_Item", NULL, &t5737_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5737_m29997_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29997_GM};
static MethodInfo* t5737_MIs[] =
{
	&m29998_MI,
	&m29999_MI,
	&m30000_MI,
	&m29996_MI,
	&m29997_MI,
	NULL
};
static TypeInfo* t5737_ITIs[] = 
{
	&t603_TI,
	&t5736_TI,
	&t5738_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5737_0_0_0;
extern Il2CppType t5737_1_0_0;
struct t5737;
extern Il2CppGenericClass t5737_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5737_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5737_MIs, t5737_PIs, NULL, NULL, NULL, NULL, NULL, &t5737_TI, t5737_ITIs, NULL, &t1908__CustomAttributeCache, &t5737_TI, &t5737_0_0_0, &t5737_1_0_0, NULL, &t5737_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4473_TI;

#include "t542.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
extern MethodInfo m30001_MI;
static PropertyInfo t4473____Current_PropertyInfo = 
{
	&t4473_TI, "Current", &m30001_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4473_PIs[] =
{
	&t4473____Current_PropertyInfo,
	NULL
};
extern Il2CppType t542_0_0_0;
extern void* RuntimeInvoker_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30001_GM;
MethodInfo m30001_MI = 
{
	"get_Current", NULL, &t4473_TI, &t542_0_0_0, RuntimeInvoker_t542, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30001_GM};
static MethodInfo* t4473_MIs[] =
{
	&m30001_MI,
	NULL
};
static TypeInfo* t4473_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4473_0_0_0;
extern Il2CppType t4473_1_0_0;
struct t4473;
extern Il2CppGenericClass t4473_GC;
TypeInfo t4473_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4473_MIs, t4473_PIs, NULL, NULL, NULL, NULL, NULL, &t4473_TI, t4473_ITIs, NULL, &EmptyCustomAttributesCache, &t4473_TI, &t4473_0_0_0, &t4473_1_0_0, NULL, &t4473_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3086.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3086_TI;
#include "t3086MD.h"

extern TypeInfo t542_TI;
extern MethodInfo m17054_MI;
extern MethodInfo m22855_MI;
struct t20;
 t542  m22855 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17050_MI;
 void m17050 (t3086 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17051_MI;
 t29 * m17051 (t3086 * __this, MethodInfo* method){
	{
		t542  L_0 = m17054(__this, &m17054_MI);
		t542  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t542_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17052_MI;
 void m17052 (t3086 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17053_MI;
 bool m17053 (t3086 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t542  m17054 (t3086 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t542  L_8 = m22855(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22855_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3086_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3086_TI, offsetof(t3086, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3086_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3086_TI, offsetof(t3086, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3086_FIs[] =
{
	&t3086_f0_FieldInfo,
	&t3086_f1_FieldInfo,
	NULL
};
static PropertyInfo t3086____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3086_TI, "System.Collections.IEnumerator.Current", &m17051_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3086____Current_PropertyInfo = 
{
	&t3086_TI, "Current", &m17054_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3086_PIs[] =
{
	&t3086____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3086____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3086_m17050_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17050_GM;
MethodInfo m17050_MI = 
{
	".ctor", (methodPointerType)&m17050, &t3086_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3086_m17050_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17050_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17051_GM;
MethodInfo m17051_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17051, &t3086_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17051_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17052_GM;
MethodInfo m17052_MI = 
{
	"Dispose", (methodPointerType)&m17052, &t3086_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17052_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17053_GM;
MethodInfo m17053_MI = 
{
	"MoveNext", (methodPointerType)&m17053, &t3086_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17053_GM};
extern Il2CppType t542_0_0_0;
extern void* RuntimeInvoker_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17054_GM;
MethodInfo m17054_MI = 
{
	"get_Current", (methodPointerType)&m17054, &t3086_TI, &t542_0_0_0, RuntimeInvoker_t542, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17054_GM};
static MethodInfo* t3086_MIs[] =
{
	&m17050_MI,
	&m17051_MI,
	&m17052_MI,
	&m17053_MI,
	&m17054_MI,
	NULL
};
static MethodInfo* t3086_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17051_MI,
	&m17053_MI,
	&m17052_MI,
	&m17054_MI,
};
static TypeInfo* t3086_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4473_TI,
};
static Il2CppInterfaceOffsetPair t3086_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4473_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3086_0_0_0;
extern Il2CppType t3086_1_0_0;
extern Il2CppGenericClass t3086_GC;
TypeInfo t3086_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3086_MIs, t3086_PIs, t3086_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3086_TI, t3086_ITIs, t3086_VT, &EmptyCustomAttributesCache, &t3086_TI, &t3086_0_0_0, &t3086_1_0_0, t3086_IOs, &t3086_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3086)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5739_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>
extern MethodInfo m30002_MI;
static PropertyInfo t5739____Count_PropertyInfo = 
{
	&t5739_TI, "Count", &m30002_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30003_MI;
static PropertyInfo t5739____IsReadOnly_PropertyInfo = 
{
	&t5739_TI, "IsReadOnly", &m30003_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5739_PIs[] =
{
	&t5739____Count_PropertyInfo,
	&t5739____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30002_GM;
MethodInfo m30002_MI = 
{
	"get_Count", NULL, &t5739_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30002_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30003_GM;
MethodInfo m30003_MI = 
{
	"get_IsReadOnly", NULL, &t5739_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30003_GM};
extern Il2CppType t542_0_0_0;
extern Il2CppType t542_0_0_0;
static ParameterInfo t5739_m30004_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30004_GM;
MethodInfo m30004_MI = 
{
	"Add", NULL, &t5739_TI, &t21_0_0_0, RuntimeInvoker_t21_t542, t5739_m30004_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30004_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30005_GM;
MethodInfo m30005_MI = 
{
	"Clear", NULL, &t5739_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30005_GM};
extern Il2CppType t542_0_0_0;
static ParameterInfo t5739_m30006_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30006_GM;
MethodInfo m30006_MI = 
{
	"Contains", NULL, &t5739_TI, &t40_0_0_0, RuntimeInvoker_t40_t542, t5739_m30006_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30006_GM};
extern Il2CppType t544_0_0_0;
extern Il2CppType t544_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5739_m30007_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t544_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30007_GM;
MethodInfo m30007_MI = 
{
	"CopyTo", NULL, &t5739_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5739_m30007_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30007_GM};
extern Il2CppType t542_0_0_0;
static ParameterInfo t5739_m30008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30008_GM;
MethodInfo m30008_MI = 
{
	"Remove", NULL, &t5739_TI, &t40_0_0_0, RuntimeInvoker_t40_t542, t5739_m30008_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30008_GM};
static MethodInfo* t5739_MIs[] =
{
	&m30002_MI,
	&m30003_MI,
	&m30004_MI,
	&m30005_MI,
	&m30006_MI,
	&m30007_MI,
	&m30008_MI,
	NULL
};
extern TypeInfo t5741_TI;
static TypeInfo* t5739_ITIs[] = 
{
	&t603_TI,
	&t5741_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5739_0_0_0;
extern Il2CppType t5739_1_0_0;
struct t5739;
extern Il2CppGenericClass t5739_GC;
TypeInfo t5739_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5739_MIs, t5739_PIs, NULL, NULL, NULL, NULL, NULL, &t5739_TI, t5739_ITIs, NULL, &EmptyCustomAttributesCache, &t5739_TI, &t5739_0_0_0, &t5739_1_0_0, NULL, &t5739_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SendMouseEvents/HitInfo>
extern Il2CppType t4473_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30009_GM;
MethodInfo m30009_MI = 
{
	"GetEnumerator", NULL, &t5741_TI, &t4473_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30009_GM};
static MethodInfo* t5741_MIs[] =
{
	&m30009_MI,
	NULL
};
static TypeInfo* t5741_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5741_0_0_0;
extern Il2CppType t5741_1_0_0;
struct t5741;
extern Il2CppGenericClass t5741_GC;
TypeInfo t5741_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5741_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5741_TI, t5741_ITIs, NULL, &EmptyCustomAttributesCache, &t5741_TI, &t5741_0_0_0, &t5741_1_0_0, NULL, &t5741_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5740_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>
extern MethodInfo m30010_MI;
extern MethodInfo m30011_MI;
static PropertyInfo t5740____Item_PropertyInfo = 
{
	&t5740_TI, "Item", &m30010_MI, &m30011_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5740_PIs[] =
{
	&t5740____Item_PropertyInfo,
	NULL
};
extern Il2CppType t542_0_0_0;
static ParameterInfo t5740_m30012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30012_GM;
MethodInfo m30012_MI = 
{
	"IndexOf", NULL, &t5740_TI, &t44_0_0_0, RuntimeInvoker_t44_t542, t5740_m30012_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30012_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t542_0_0_0;
static ParameterInfo t5740_m30013_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30013_GM;
MethodInfo m30013_MI = 
{
	"Insert", NULL, &t5740_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t542, t5740_m30013_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30013_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5740_m30014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30014_GM;
MethodInfo m30014_MI = 
{
	"RemoveAt", NULL, &t5740_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5740_m30014_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30014_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5740_m30010_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t542_0_0_0;
extern void* RuntimeInvoker_t542_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30010_GM;
MethodInfo m30010_MI = 
{
	"get_Item", NULL, &t5740_TI, &t542_0_0_0, RuntimeInvoker_t542_t44, t5740_m30010_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30010_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t542_0_0_0;
static ParameterInfo t5740_m30011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t542_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t542 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30011_GM;
MethodInfo m30011_MI = 
{
	"set_Item", NULL, &t5740_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t542, t5740_m30011_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30011_GM};
static MethodInfo* t5740_MIs[] =
{
	&m30012_MI,
	&m30013_MI,
	&m30014_MI,
	&m30010_MI,
	&m30011_MI,
	NULL
};
static TypeInfo* t5740_ITIs[] = 
{
	&t603_TI,
	&t5739_TI,
	&t5741_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5740_0_0_0;
extern Il2CppType t5740_1_0_0;
struct t5740;
extern Il2CppGenericClass t5740_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5740_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5740_MIs, t5740_PIs, NULL, NULL, NULL, NULL, NULL, &t5740_TI, t5740_ITIs, NULL, &t1908__CustomAttributeCache, &t5740_TI, &t5740_0_0_0, &t5740_1_0_0, NULL, &t5740_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4475_TI;

#include "t545.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.PropertyAttribute>
extern MethodInfo m30015_MI;
static PropertyInfo t4475____Current_PropertyInfo = 
{
	&t4475_TI, "Current", &m30015_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4475_PIs[] =
{
	&t4475____Current_PropertyInfo,
	NULL
};
extern Il2CppType t545_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30015_GM;
MethodInfo m30015_MI = 
{
	"get_Current", NULL, &t4475_TI, &t545_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30015_GM};
static MethodInfo* t4475_MIs[] =
{
	&m30015_MI,
	NULL
};
static TypeInfo* t4475_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4475_0_0_0;
extern Il2CppType t4475_1_0_0;
struct t4475;
extern Il2CppGenericClass t4475_GC;
TypeInfo t4475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4475_MIs, t4475_PIs, NULL, NULL, NULL, NULL, NULL, &t4475_TI, t4475_ITIs, NULL, &EmptyCustomAttributesCache, &t4475_TI, &t4475_0_0_0, &t4475_1_0_0, NULL, &t4475_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3087.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3087_TI;
#include "t3087MD.h"

extern TypeInfo t545_TI;
extern MethodInfo m17059_MI;
extern MethodInfo m22866_MI;
struct t20;
#define m22866(__this, p0, method) (t545 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3087_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3087_TI, offsetof(t3087, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3087_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3087_TI, offsetof(t3087, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3087_FIs[] =
{
	&t3087_f0_FieldInfo,
	&t3087_f1_FieldInfo,
	NULL
};
extern MethodInfo m17056_MI;
static PropertyInfo t3087____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3087_TI, "System.Collections.IEnumerator.Current", &m17056_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3087____Current_PropertyInfo = 
{
	&t3087_TI, "Current", &m17059_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3087_PIs[] =
{
	&t3087____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3087____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3087_m17055_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17055_GM;
MethodInfo m17055_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3087_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3087_m17055_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17055_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17056_GM;
MethodInfo m17056_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3087_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17056_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17057_GM;
MethodInfo m17057_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3087_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17057_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17058_GM;
MethodInfo m17058_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3087_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17058_GM};
extern Il2CppType t545_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17059_GM;
MethodInfo m17059_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3087_TI, &t545_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17059_GM};
static MethodInfo* t3087_MIs[] =
{
	&m17055_MI,
	&m17056_MI,
	&m17057_MI,
	&m17058_MI,
	&m17059_MI,
	NULL
};
extern MethodInfo m17058_MI;
extern MethodInfo m17057_MI;
static MethodInfo* t3087_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17056_MI,
	&m17058_MI,
	&m17057_MI,
	&m17059_MI,
};
static TypeInfo* t3087_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4475_TI,
};
static Il2CppInterfaceOffsetPair t3087_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4475_TI, 7},
};
extern TypeInfo t545_TI;
static Il2CppRGCTXData t3087_RGCTXData[3] = 
{
	&m17059_MI/* Method Usage */,
	&t545_TI/* Class Usage */,
	&m22866_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3087_0_0_0;
extern Il2CppType t3087_1_0_0;
extern Il2CppGenericClass t3087_GC;
TypeInfo t3087_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3087_MIs, t3087_PIs, t3087_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3087_TI, t3087_ITIs, t3087_VT, &EmptyCustomAttributesCache, &t3087_TI, &t3087_0_0_0, &t3087_1_0_0, t3087_IOs, &t3087_GC, NULL, NULL, NULL, t3087_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3087)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5742_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>
extern MethodInfo m30016_MI;
static PropertyInfo t5742____Count_PropertyInfo = 
{
	&t5742_TI, "Count", &m30016_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30017_MI;
static PropertyInfo t5742____IsReadOnly_PropertyInfo = 
{
	&t5742_TI, "IsReadOnly", &m30017_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5742_PIs[] =
{
	&t5742____Count_PropertyInfo,
	&t5742____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30016_GM;
MethodInfo m30016_MI = 
{
	"get_Count", NULL, &t5742_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30016_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30017_GM;
MethodInfo m30017_MI = 
{
	"get_IsReadOnly", NULL, &t5742_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30017_GM};
extern Il2CppType t545_0_0_0;
extern Il2CppType t545_0_0_0;
static ParameterInfo t5742_m30018_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t545_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30018_GM;
MethodInfo m30018_MI = 
{
	"Add", NULL, &t5742_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5742_m30018_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30018_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30019_GM;
MethodInfo m30019_MI = 
{
	"Clear", NULL, &t5742_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30019_GM};
extern Il2CppType t545_0_0_0;
static ParameterInfo t5742_m30020_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t545_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30020_GM;
MethodInfo m30020_MI = 
{
	"Contains", NULL, &t5742_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5742_m30020_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30020_GM};
extern Il2CppType t3781_0_0_0;
extern Il2CppType t3781_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5742_m30021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3781_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30021_GM;
MethodInfo m30021_MI = 
{
	"CopyTo", NULL, &t5742_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5742_m30021_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30021_GM};
extern Il2CppType t545_0_0_0;
static ParameterInfo t5742_m30022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t545_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30022_GM;
MethodInfo m30022_MI = 
{
	"Remove", NULL, &t5742_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5742_m30022_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30022_GM};
static MethodInfo* t5742_MIs[] =
{
	&m30016_MI,
	&m30017_MI,
	&m30018_MI,
	&m30019_MI,
	&m30020_MI,
	&m30021_MI,
	&m30022_MI,
	NULL
};
extern TypeInfo t5744_TI;
static TypeInfo* t5742_ITIs[] = 
{
	&t603_TI,
	&t5744_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5742_0_0_0;
extern Il2CppType t5742_1_0_0;
struct t5742;
extern Il2CppGenericClass t5742_GC;
TypeInfo t5742_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5742_MIs, t5742_PIs, NULL, NULL, NULL, NULL, NULL, &t5742_TI, t5742_ITIs, NULL, &EmptyCustomAttributesCache, &t5742_TI, &t5742_0_0_0, &t5742_1_0_0, NULL, &t5742_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.PropertyAttribute>
extern Il2CppType t4475_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30023_GM;
MethodInfo m30023_MI = 
{
	"GetEnumerator", NULL, &t5744_TI, &t4475_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30023_GM};
static MethodInfo* t5744_MIs[] =
{
	&m30023_MI,
	NULL
};
static TypeInfo* t5744_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5744_0_0_0;
extern Il2CppType t5744_1_0_0;
struct t5744;
extern Il2CppGenericClass t5744_GC;
TypeInfo t5744_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5744_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5744_TI, t5744_ITIs, NULL, &EmptyCustomAttributesCache, &t5744_TI, &t5744_0_0_0, &t5744_1_0_0, NULL, &t5744_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5743_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>
extern MethodInfo m30024_MI;
extern MethodInfo m30025_MI;
static PropertyInfo t5743____Item_PropertyInfo = 
{
	&t5743_TI, "Item", &m30024_MI, &m30025_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5743_PIs[] =
{
	&t5743____Item_PropertyInfo,
	NULL
};
extern Il2CppType t545_0_0_0;
static ParameterInfo t5743_m30026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t545_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30026_GM;
MethodInfo m30026_MI = 
{
	"IndexOf", NULL, &t5743_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5743_m30026_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30026_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t545_0_0_0;
static ParameterInfo t5743_m30027_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t545_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30027_GM;
MethodInfo m30027_MI = 
{
	"Insert", NULL, &t5743_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5743_m30027_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30027_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5743_m30028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30028_GM;
MethodInfo m30028_MI = 
{
	"RemoveAt", NULL, &t5743_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5743_m30028_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30028_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5743_m30024_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t545_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30024_GM;
MethodInfo m30024_MI = 
{
	"get_Item", NULL, &t5743_TI, &t545_0_0_0, RuntimeInvoker_t29_t44, t5743_m30024_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30024_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t545_0_0_0;
static ParameterInfo t5743_m30025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t545_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30025_GM;
MethodInfo m30025_MI = 
{
	"set_Item", NULL, &t5743_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5743_m30025_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30025_GM};
static MethodInfo* t5743_MIs[] =
{
	&m30026_MI,
	&m30027_MI,
	&m30028_MI,
	&m30024_MI,
	&m30025_MI,
	NULL
};
static TypeInfo* t5743_ITIs[] = 
{
	&t603_TI,
	&t5742_TI,
	&t5744_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5743_0_0_0;
extern Il2CppType t5743_1_0_0;
struct t5743;
extern Il2CppGenericClass t5743_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5743_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5743_MIs, t5743_PIs, NULL, NULL, NULL, NULL, NULL, &t5743_TI, t5743_ITIs, NULL, &t1908__CustomAttributeCache, &t5743_TI, &t5743_0_0_0, &t5743_1_0_0, NULL, &t5743_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4477_TI;

#include "t399.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TooltipAttribute>
extern MethodInfo m30029_MI;
static PropertyInfo t4477____Current_PropertyInfo = 
{
	&t4477_TI, "Current", &m30029_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4477_PIs[] =
{
	&t4477____Current_PropertyInfo,
	NULL
};
extern Il2CppType t399_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30029_GM;
MethodInfo m30029_MI = 
{
	"get_Current", NULL, &t4477_TI, &t399_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30029_GM};
static MethodInfo* t4477_MIs[] =
{
	&m30029_MI,
	NULL
};
static TypeInfo* t4477_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4477_0_0_0;
extern Il2CppType t4477_1_0_0;
struct t4477;
extern Il2CppGenericClass t4477_GC;
TypeInfo t4477_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4477_MIs, t4477_PIs, NULL, NULL, NULL, NULL, NULL, &t4477_TI, t4477_ITIs, NULL, &EmptyCustomAttributesCache, &t4477_TI, &t4477_0_0_0, &t4477_1_0_0, NULL, &t4477_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3088.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3088_TI;
#include "t3088MD.h"

extern TypeInfo t399_TI;
extern MethodInfo m17064_MI;
extern MethodInfo m22877_MI;
struct t20;
#define m22877(__this, p0, method) (t399 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3088_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3088_TI, offsetof(t3088, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3088_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3088_TI, offsetof(t3088, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3088_FIs[] =
{
	&t3088_f0_FieldInfo,
	&t3088_f1_FieldInfo,
	NULL
};
extern MethodInfo m17061_MI;
static PropertyInfo t3088____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3088_TI, "System.Collections.IEnumerator.Current", &m17061_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3088____Current_PropertyInfo = 
{
	&t3088_TI, "Current", &m17064_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3088_PIs[] =
{
	&t3088____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3088____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3088_m17060_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17060_GM;
MethodInfo m17060_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3088_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3088_m17060_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17060_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17061_GM;
MethodInfo m17061_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3088_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17061_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17062_GM;
MethodInfo m17062_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3088_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17062_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17063_GM;
MethodInfo m17063_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3088_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17063_GM};
extern Il2CppType t399_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17064_GM;
MethodInfo m17064_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3088_TI, &t399_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17064_GM};
static MethodInfo* t3088_MIs[] =
{
	&m17060_MI,
	&m17061_MI,
	&m17062_MI,
	&m17063_MI,
	&m17064_MI,
	NULL
};
extern MethodInfo m17063_MI;
extern MethodInfo m17062_MI;
static MethodInfo* t3088_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17061_MI,
	&m17063_MI,
	&m17062_MI,
	&m17064_MI,
};
static TypeInfo* t3088_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4477_TI,
};
static Il2CppInterfaceOffsetPair t3088_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4477_TI, 7},
};
extern TypeInfo t399_TI;
static Il2CppRGCTXData t3088_RGCTXData[3] = 
{
	&m17064_MI/* Method Usage */,
	&t399_TI/* Class Usage */,
	&m22877_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3088_0_0_0;
extern Il2CppType t3088_1_0_0;
extern Il2CppGenericClass t3088_GC;
TypeInfo t3088_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3088_MIs, t3088_PIs, t3088_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3088_TI, t3088_ITIs, t3088_VT, &EmptyCustomAttributesCache, &t3088_TI, &t3088_0_0_0, &t3088_1_0_0, t3088_IOs, &t3088_GC, NULL, NULL, NULL, t3088_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3088)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5745_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>
extern MethodInfo m30030_MI;
static PropertyInfo t5745____Count_PropertyInfo = 
{
	&t5745_TI, "Count", &m30030_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30031_MI;
static PropertyInfo t5745____IsReadOnly_PropertyInfo = 
{
	&t5745_TI, "IsReadOnly", &m30031_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5745_PIs[] =
{
	&t5745____Count_PropertyInfo,
	&t5745____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30030_GM;
MethodInfo m30030_MI = 
{
	"get_Count", NULL, &t5745_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30030_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30031_GM;
MethodInfo m30031_MI = 
{
	"get_IsReadOnly", NULL, &t5745_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30031_GM};
extern Il2CppType t399_0_0_0;
extern Il2CppType t399_0_0_0;
static ParameterInfo t5745_m30032_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t399_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30032_GM;
MethodInfo m30032_MI = 
{
	"Add", NULL, &t5745_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5745_m30032_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30032_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30033_GM;
MethodInfo m30033_MI = 
{
	"Clear", NULL, &t5745_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30033_GM};
extern Il2CppType t399_0_0_0;
static ParameterInfo t5745_m30034_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t399_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30034_GM;
MethodInfo m30034_MI = 
{
	"Contains", NULL, &t5745_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5745_m30034_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30034_GM};
extern Il2CppType t3782_0_0_0;
extern Il2CppType t3782_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5745_m30035_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3782_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30035_GM;
MethodInfo m30035_MI = 
{
	"CopyTo", NULL, &t5745_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5745_m30035_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30035_GM};
extern Il2CppType t399_0_0_0;
static ParameterInfo t5745_m30036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t399_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30036_GM;
MethodInfo m30036_MI = 
{
	"Remove", NULL, &t5745_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5745_m30036_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30036_GM};
static MethodInfo* t5745_MIs[] =
{
	&m30030_MI,
	&m30031_MI,
	&m30032_MI,
	&m30033_MI,
	&m30034_MI,
	&m30035_MI,
	&m30036_MI,
	NULL
};
extern TypeInfo t5747_TI;
static TypeInfo* t5745_ITIs[] = 
{
	&t603_TI,
	&t5747_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5745_0_0_0;
extern Il2CppType t5745_1_0_0;
struct t5745;
extern Il2CppGenericClass t5745_GC;
TypeInfo t5745_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5745_MIs, t5745_PIs, NULL, NULL, NULL, NULL, NULL, &t5745_TI, t5745_ITIs, NULL, &EmptyCustomAttributesCache, &t5745_TI, &t5745_0_0_0, &t5745_1_0_0, NULL, &t5745_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TooltipAttribute>
extern Il2CppType t4477_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30037_GM;
MethodInfo m30037_MI = 
{
	"GetEnumerator", NULL, &t5747_TI, &t4477_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30037_GM};
static MethodInfo* t5747_MIs[] =
{
	&m30037_MI,
	NULL
};
static TypeInfo* t5747_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5747_0_0_0;
extern Il2CppType t5747_1_0_0;
struct t5747;
extern Il2CppGenericClass t5747_GC;
TypeInfo t5747_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5747_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5747_TI, t5747_ITIs, NULL, &EmptyCustomAttributesCache, &t5747_TI, &t5747_0_0_0, &t5747_1_0_0, NULL, &t5747_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5746_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>
extern MethodInfo m30038_MI;
extern MethodInfo m30039_MI;
static PropertyInfo t5746____Item_PropertyInfo = 
{
	&t5746_TI, "Item", &m30038_MI, &m30039_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5746_PIs[] =
{
	&t5746____Item_PropertyInfo,
	NULL
};
extern Il2CppType t399_0_0_0;
static ParameterInfo t5746_m30040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t399_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30040_GM;
MethodInfo m30040_MI = 
{
	"IndexOf", NULL, &t5746_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5746_m30040_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30040_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t399_0_0_0;
static ParameterInfo t5746_m30041_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t399_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30041_GM;
MethodInfo m30041_MI = 
{
	"Insert", NULL, &t5746_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5746_m30041_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30041_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5746_m30042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30042_GM;
MethodInfo m30042_MI = 
{
	"RemoveAt", NULL, &t5746_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5746_m30042_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30042_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5746_m30038_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t399_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30038_GM;
MethodInfo m30038_MI = 
{
	"get_Item", NULL, &t5746_TI, &t399_0_0_0, RuntimeInvoker_t29_t44, t5746_m30038_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30038_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t399_0_0_0;
static ParameterInfo t5746_m30039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t399_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30039_GM;
MethodInfo m30039_MI = 
{
	"set_Item", NULL, &t5746_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5746_m30039_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30039_GM};
static MethodInfo* t5746_MIs[] =
{
	&m30040_MI,
	&m30041_MI,
	&m30042_MI,
	&m30038_MI,
	&m30039_MI,
	NULL
};
static TypeInfo* t5746_ITIs[] = 
{
	&t603_TI,
	&t5745_TI,
	&t5747_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5746_0_0_0;
extern Il2CppType t5746_1_0_0;
struct t5746;
extern Il2CppGenericClass t5746_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5746_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5746_MIs, t5746_PIs, NULL, NULL, NULL, NULL, NULL, &t5746_TI, t5746_ITIs, NULL, &t1908__CustomAttributeCache, &t5746_TI, &t5746_0_0_0, &t5746_1_0_0, NULL, &t5746_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4479_TI;

#include "t394.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SpaceAttribute>
extern MethodInfo m30043_MI;
static PropertyInfo t4479____Current_PropertyInfo = 
{
	&t4479_TI, "Current", &m30043_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4479_PIs[] =
{
	&t4479____Current_PropertyInfo,
	NULL
};
extern Il2CppType t394_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30043_GM;
MethodInfo m30043_MI = 
{
	"get_Current", NULL, &t4479_TI, &t394_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30043_GM};
static MethodInfo* t4479_MIs[] =
{
	&m30043_MI,
	NULL
};
static TypeInfo* t4479_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4479_0_0_0;
extern Il2CppType t4479_1_0_0;
struct t4479;
extern Il2CppGenericClass t4479_GC;
TypeInfo t4479_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4479_MIs, t4479_PIs, NULL, NULL, NULL, NULL, NULL, &t4479_TI, t4479_ITIs, NULL, &EmptyCustomAttributesCache, &t4479_TI, &t4479_0_0_0, &t4479_1_0_0, NULL, &t4479_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3089.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3089_TI;
#include "t3089MD.h"

extern TypeInfo t394_TI;
extern MethodInfo m17069_MI;
extern MethodInfo m22888_MI;
struct t20;
#define m22888(__this, p0, method) (t394 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3089_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3089_TI, offsetof(t3089, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3089_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3089_TI, offsetof(t3089, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3089_FIs[] =
{
	&t3089_f0_FieldInfo,
	&t3089_f1_FieldInfo,
	NULL
};
extern MethodInfo m17066_MI;
static PropertyInfo t3089____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3089_TI, "System.Collections.IEnumerator.Current", &m17066_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3089____Current_PropertyInfo = 
{
	&t3089_TI, "Current", &m17069_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3089_PIs[] =
{
	&t3089____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3089____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3089_m17065_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17065_GM;
MethodInfo m17065_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3089_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3089_m17065_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17065_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17066_GM;
MethodInfo m17066_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3089_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17066_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17067_GM;
MethodInfo m17067_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3089_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17067_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17068_GM;
MethodInfo m17068_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3089_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17068_GM};
extern Il2CppType t394_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17069_GM;
MethodInfo m17069_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3089_TI, &t394_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17069_GM};
static MethodInfo* t3089_MIs[] =
{
	&m17065_MI,
	&m17066_MI,
	&m17067_MI,
	&m17068_MI,
	&m17069_MI,
	NULL
};
extern MethodInfo m17068_MI;
extern MethodInfo m17067_MI;
static MethodInfo* t3089_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17066_MI,
	&m17068_MI,
	&m17067_MI,
	&m17069_MI,
};
static TypeInfo* t3089_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4479_TI,
};
static Il2CppInterfaceOffsetPair t3089_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4479_TI, 7},
};
extern TypeInfo t394_TI;
static Il2CppRGCTXData t3089_RGCTXData[3] = 
{
	&m17069_MI/* Method Usage */,
	&t394_TI/* Class Usage */,
	&m22888_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3089_0_0_0;
extern Il2CppType t3089_1_0_0;
extern Il2CppGenericClass t3089_GC;
TypeInfo t3089_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3089_MIs, t3089_PIs, t3089_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3089_TI, t3089_ITIs, t3089_VT, &EmptyCustomAttributesCache, &t3089_TI, &t3089_0_0_0, &t3089_1_0_0, t3089_IOs, &t3089_GC, NULL, NULL, NULL, t3089_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3089)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5748_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>
extern MethodInfo m30044_MI;
static PropertyInfo t5748____Count_PropertyInfo = 
{
	&t5748_TI, "Count", &m30044_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30045_MI;
static PropertyInfo t5748____IsReadOnly_PropertyInfo = 
{
	&t5748_TI, "IsReadOnly", &m30045_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5748_PIs[] =
{
	&t5748____Count_PropertyInfo,
	&t5748____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30044_GM;
MethodInfo m30044_MI = 
{
	"get_Count", NULL, &t5748_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30044_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30045_GM;
MethodInfo m30045_MI = 
{
	"get_IsReadOnly", NULL, &t5748_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30045_GM};
extern Il2CppType t394_0_0_0;
extern Il2CppType t394_0_0_0;
static ParameterInfo t5748_m30046_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30046_GM;
MethodInfo m30046_MI = 
{
	"Add", NULL, &t5748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5748_m30046_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30046_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30047_GM;
MethodInfo m30047_MI = 
{
	"Clear", NULL, &t5748_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30047_GM};
extern Il2CppType t394_0_0_0;
static ParameterInfo t5748_m30048_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t394_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30048_GM;
MethodInfo m30048_MI = 
{
	"Contains", NULL, &t5748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5748_m30048_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30048_GM};
extern Il2CppType t3783_0_0_0;
extern Il2CppType t3783_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5748_m30049_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3783_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30049_GM;
MethodInfo m30049_MI = 
{
	"CopyTo", NULL, &t5748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5748_m30049_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30049_GM};
extern Il2CppType t394_0_0_0;
static ParameterInfo t5748_m30050_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t394_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30050_GM;
MethodInfo m30050_MI = 
{
	"Remove", NULL, &t5748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5748_m30050_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30050_GM};
static MethodInfo* t5748_MIs[] =
{
	&m30044_MI,
	&m30045_MI,
	&m30046_MI,
	&m30047_MI,
	&m30048_MI,
	&m30049_MI,
	&m30050_MI,
	NULL
};
extern TypeInfo t5750_TI;
static TypeInfo* t5748_ITIs[] = 
{
	&t603_TI,
	&t5750_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5748_0_0_0;
extern Il2CppType t5748_1_0_0;
struct t5748;
extern Il2CppGenericClass t5748_GC;
TypeInfo t5748_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5748_MIs, t5748_PIs, NULL, NULL, NULL, NULL, NULL, &t5748_TI, t5748_ITIs, NULL, &EmptyCustomAttributesCache, &t5748_TI, &t5748_0_0_0, &t5748_1_0_0, NULL, &t5748_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SpaceAttribute>
extern Il2CppType t4479_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30051_GM;
MethodInfo m30051_MI = 
{
	"GetEnumerator", NULL, &t5750_TI, &t4479_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30051_GM};
static MethodInfo* t5750_MIs[] =
{
	&m30051_MI,
	NULL
};
static TypeInfo* t5750_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5750_0_0_0;
extern Il2CppType t5750_1_0_0;
struct t5750;
extern Il2CppGenericClass t5750_GC;
TypeInfo t5750_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5750_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5750_TI, t5750_ITIs, NULL, &EmptyCustomAttributesCache, &t5750_TI, &t5750_0_0_0, &t5750_1_0_0, NULL, &t5750_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5749_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>
extern MethodInfo m30052_MI;
extern MethodInfo m30053_MI;
static PropertyInfo t5749____Item_PropertyInfo = 
{
	&t5749_TI, "Item", &m30052_MI, &m30053_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5749_PIs[] =
{
	&t5749____Item_PropertyInfo,
	NULL
};
extern Il2CppType t394_0_0_0;
static ParameterInfo t5749_m30054_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t394_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30054_GM;
MethodInfo m30054_MI = 
{
	"IndexOf", NULL, &t5749_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5749_m30054_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30054_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t394_0_0_0;
static ParameterInfo t5749_m30055_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30055_GM;
MethodInfo m30055_MI = 
{
	"Insert", NULL, &t5749_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5749_m30055_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30055_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5749_m30056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30056_GM;
MethodInfo m30056_MI = 
{
	"RemoveAt", NULL, &t5749_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5749_m30056_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30056_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5749_m30052_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t394_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30052_GM;
MethodInfo m30052_MI = 
{
	"get_Item", NULL, &t5749_TI, &t394_0_0_0, RuntimeInvoker_t29_t44, t5749_m30052_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30052_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t394_0_0_0;
static ParameterInfo t5749_m30053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30053_GM;
MethodInfo m30053_MI = 
{
	"set_Item", NULL, &t5749_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5749_m30053_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30053_GM};
static MethodInfo* t5749_MIs[] =
{
	&m30054_MI,
	&m30055_MI,
	&m30056_MI,
	&m30052_MI,
	&m30053_MI,
	NULL
};
static TypeInfo* t5749_ITIs[] = 
{
	&t603_TI,
	&t5748_TI,
	&t5750_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5749_0_0_0;
extern Il2CppType t5749_1_0_0;
struct t5749;
extern Il2CppGenericClass t5749_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5749_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5749_MIs, t5749_PIs, NULL, NULL, NULL, NULL, NULL, &t5749_TI, t5749_ITIs, NULL, &t1908__CustomAttributeCache, &t5749_TI, &t5749_0_0_0, &t5749_1_0_0, NULL, &t5749_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4481_TI;

#include "t349.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RangeAttribute>
extern MethodInfo m30057_MI;
static PropertyInfo t4481____Current_PropertyInfo = 
{
	&t4481_TI, "Current", &m30057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4481_PIs[] =
{
	&t4481____Current_PropertyInfo,
	NULL
};
extern Il2CppType t349_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30057_GM;
MethodInfo m30057_MI = 
{
	"get_Current", NULL, &t4481_TI, &t349_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30057_GM};
static MethodInfo* t4481_MIs[] =
{
	&m30057_MI,
	NULL
};
static TypeInfo* t4481_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4481_0_0_0;
extern Il2CppType t4481_1_0_0;
struct t4481;
extern Il2CppGenericClass t4481_GC;
TypeInfo t4481_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4481_MIs, t4481_PIs, NULL, NULL, NULL, NULL, NULL, &t4481_TI, t4481_ITIs, NULL, &EmptyCustomAttributesCache, &t4481_TI, &t4481_0_0_0, &t4481_1_0_0, NULL, &t4481_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3090.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3090_TI;
#include "t3090MD.h"

extern TypeInfo t349_TI;
extern MethodInfo m17074_MI;
extern MethodInfo m22899_MI;
struct t20;
#define m22899(__this, p0, method) (t349 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3090_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3090_TI, offsetof(t3090, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3090_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3090_TI, offsetof(t3090, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3090_FIs[] =
{
	&t3090_f0_FieldInfo,
	&t3090_f1_FieldInfo,
	NULL
};
extern MethodInfo m17071_MI;
static PropertyInfo t3090____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3090_TI, "System.Collections.IEnumerator.Current", &m17071_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3090____Current_PropertyInfo = 
{
	&t3090_TI, "Current", &m17074_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3090_PIs[] =
{
	&t3090____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3090____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3090_m17070_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17070_GM;
MethodInfo m17070_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3090_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3090_m17070_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17070_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17071_GM;
MethodInfo m17071_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3090_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17071_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17072_GM;
MethodInfo m17072_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3090_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17072_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17073_GM;
MethodInfo m17073_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3090_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17073_GM};
extern Il2CppType t349_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17074_GM;
MethodInfo m17074_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3090_TI, &t349_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17074_GM};
static MethodInfo* t3090_MIs[] =
{
	&m17070_MI,
	&m17071_MI,
	&m17072_MI,
	&m17073_MI,
	&m17074_MI,
	NULL
};
extern MethodInfo m17073_MI;
extern MethodInfo m17072_MI;
static MethodInfo* t3090_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17071_MI,
	&m17073_MI,
	&m17072_MI,
	&m17074_MI,
};
static TypeInfo* t3090_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4481_TI,
};
static Il2CppInterfaceOffsetPair t3090_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4481_TI, 7},
};
extern TypeInfo t349_TI;
static Il2CppRGCTXData t3090_RGCTXData[3] = 
{
	&m17074_MI/* Method Usage */,
	&t349_TI/* Class Usage */,
	&m22899_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3090_0_0_0;
extern Il2CppType t3090_1_0_0;
extern Il2CppGenericClass t3090_GC;
TypeInfo t3090_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3090_MIs, t3090_PIs, t3090_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3090_TI, t3090_ITIs, t3090_VT, &EmptyCustomAttributesCache, &t3090_TI, &t3090_0_0_0, &t3090_1_0_0, t3090_IOs, &t3090_GC, NULL, NULL, NULL, t3090_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3090)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5751_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>
extern MethodInfo m30058_MI;
static PropertyInfo t5751____Count_PropertyInfo = 
{
	&t5751_TI, "Count", &m30058_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30059_MI;
static PropertyInfo t5751____IsReadOnly_PropertyInfo = 
{
	&t5751_TI, "IsReadOnly", &m30059_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5751_PIs[] =
{
	&t5751____Count_PropertyInfo,
	&t5751____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30058_GM;
MethodInfo m30058_MI = 
{
	"get_Count", NULL, &t5751_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30058_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30059_GM;
MethodInfo m30059_MI = 
{
	"get_IsReadOnly", NULL, &t5751_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30059_GM};
extern Il2CppType t349_0_0_0;
extern Il2CppType t349_0_0_0;
static ParameterInfo t5751_m30060_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t349_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30060_GM;
MethodInfo m30060_MI = 
{
	"Add", NULL, &t5751_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5751_m30060_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30060_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30061_GM;
MethodInfo m30061_MI = 
{
	"Clear", NULL, &t5751_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30061_GM};
extern Il2CppType t349_0_0_0;
static ParameterInfo t5751_m30062_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t349_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30062_GM;
MethodInfo m30062_MI = 
{
	"Contains", NULL, &t5751_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5751_m30062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30062_GM};
extern Il2CppType t3784_0_0_0;
extern Il2CppType t3784_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5751_m30063_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3784_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30063_GM;
MethodInfo m30063_MI = 
{
	"CopyTo", NULL, &t5751_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5751_m30063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30063_GM};
extern Il2CppType t349_0_0_0;
static ParameterInfo t5751_m30064_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t349_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30064_GM;
MethodInfo m30064_MI = 
{
	"Remove", NULL, &t5751_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5751_m30064_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30064_GM};
static MethodInfo* t5751_MIs[] =
{
	&m30058_MI,
	&m30059_MI,
	&m30060_MI,
	&m30061_MI,
	&m30062_MI,
	&m30063_MI,
	&m30064_MI,
	NULL
};
extern TypeInfo t5753_TI;
static TypeInfo* t5751_ITIs[] = 
{
	&t603_TI,
	&t5753_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5751_0_0_0;
extern Il2CppType t5751_1_0_0;
struct t5751;
extern Il2CppGenericClass t5751_GC;
TypeInfo t5751_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5751_MIs, t5751_PIs, NULL, NULL, NULL, NULL, NULL, &t5751_TI, t5751_ITIs, NULL, &EmptyCustomAttributesCache, &t5751_TI, &t5751_0_0_0, &t5751_1_0_0, NULL, &t5751_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RangeAttribute>
extern Il2CppType t4481_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30065_GM;
MethodInfo m30065_MI = 
{
	"GetEnumerator", NULL, &t5753_TI, &t4481_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30065_GM};
static MethodInfo* t5753_MIs[] =
{
	&m30065_MI,
	NULL
};
static TypeInfo* t5753_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5753_0_0_0;
extern Il2CppType t5753_1_0_0;
struct t5753;
extern Il2CppGenericClass t5753_GC;
TypeInfo t5753_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5753_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5753_TI, t5753_ITIs, NULL, &EmptyCustomAttributesCache, &t5753_TI, &t5753_0_0_0, &t5753_1_0_0, NULL, &t5753_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5752_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>
extern MethodInfo m30066_MI;
extern MethodInfo m30067_MI;
static PropertyInfo t5752____Item_PropertyInfo = 
{
	&t5752_TI, "Item", &m30066_MI, &m30067_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5752_PIs[] =
{
	&t5752____Item_PropertyInfo,
	NULL
};
extern Il2CppType t349_0_0_0;
static ParameterInfo t5752_m30068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t349_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30068_GM;
MethodInfo m30068_MI = 
{
	"IndexOf", NULL, &t5752_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5752_m30068_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30068_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t349_0_0_0;
static ParameterInfo t5752_m30069_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t349_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30069_GM;
MethodInfo m30069_MI = 
{
	"Insert", NULL, &t5752_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5752_m30069_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30069_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5752_m30070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30070_GM;
MethodInfo m30070_MI = 
{
	"RemoveAt", NULL, &t5752_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5752_m30070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30070_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5752_m30066_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t349_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30066_GM;
MethodInfo m30066_MI = 
{
	"get_Item", NULL, &t5752_TI, &t349_0_0_0, RuntimeInvoker_t29_t44, t5752_m30066_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30066_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t349_0_0_0;
static ParameterInfo t5752_m30067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t349_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30067_GM;
MethodInfo m30067_MI = 
{
	"set_Item", NULL, &t5752_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5752_m30067_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30067_GM};
static MethodInfo* t5752_MIs[] =
{
	&m30068_MI,
	&m30069_MI,
	&m30070_MI,
	&m30066_MI,
	&m30067_MI,
	NULL
};
static TypeInfo* t5752_ITIs[] = 
{
	&t603_TI,
	&t5751_TI,
	&t5753_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5752_0_0_0;
extern Il2CppType t5752_1_0_0;
struct t5752;
extern Il2CppGenericClass t5752_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5752_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5752_MIs, t5752_PIs, NULL, NULL, NULL, NULL, NULL, &t5752_TI, t5752_ITIs, NULL, &t1908__CustomAttributeCache, &t5752_TI, &t5752_0_0_0, &t5752_1_0_0, NULL, &t5752_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4483_TI;

#include "t405.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextAreaAttribute>
extern MethodInfo m30071_MI;
static PropertyInfo t4483____Current_PropertyInfo = 
{
	&t4483_TI, "Current", &m30071_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4483_PIs[] =
{
	&t4483____Current_PropertyInfo,
	NULL
};
extern Il2CppType t405_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30071_GM;
MethodInfo m30071_MI = 
{
	"get_Current", NULL, &t4483_TI, &t405_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30071_GM};
static MethodInfo* t4483_MIs[] =
{
	&m30071_MI,
	NULL
};
static TypeInfo* t4483_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4483_0_0_0;
extern Il2CppType t4483_1_0_0;
struct t4483;
extern Il2CppGenericClass t4483_GC;
TypeInfo t4483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4483_MIs, t4483_PIs, NULL, NULL, NULL, NULL, NULL, &t4483_TI, t4483_ITIs, NULL, &EmptyCustomAttributesCache, &t4483_TI, &t4483_0_0_0, &t4483_1_0_0, NULL, &t4483_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3091.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3091_TI;
#include "t3091MD.h"

extern TypeInfo t405_TI;
extern MethodInfo m17079_MI;
extern MethodInfo m22910_MI;
struct t20;
#define m22910(__this, p0, method) (t405 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3091_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3091_TI, offsetof(t3091, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3091_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3091_TI, offsetof(t3091, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3091_FIs[] =
{
	&t3091_f0_FieldInfo,
	&t3091_f1_FieldInfo,
	NULL
};
extern MethodInfo m17076_MI;
static PropertyInfo t3091____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3091_TI, "System.Collections.IEnumerator.Current", &m17076_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3091____Current_PropertyInfo = 
{
	&t3091_TI, "Current", &m17079_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3091_PIs[] =
{
	&t3091____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3091____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3091_m17075_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17075_GM;
MethodInfo m17075_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3091_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3091_m17075_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17075_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17076_GM;
MethodInfo m17076_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3091_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17076_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17077_GM;
MethodInfo m17077_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3091_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17077_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17078_GM;
MethodInfo m17078_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3091_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17078_GM};
extern Il2CppType t405_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17079_GM;
MethodInfo m17079_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3091_TI, &t405_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17079_GM};
static MethodInfo* t3091_MIs[] =
{
	&m17075_MI,
	&m17076_MI,
	&m17077_MI,
	&m17078_MI,
	&m17079_MI,
	NULL
};
extern MethodInfo m17078_MI;
extern MethodInfo m17077_MI;
static MethodInfo* t3091_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17076_MI,
	&m17078_MI,
	&m17077_MI,
	&m17079_MI,
};
static TypeInfo* t3091_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4483_TI,
};
static Il2CppInterfaceOffsetPair t3091_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4483_TI, 7},
};
extern TypeInfo t405_TI;
static Il2CppRGCTXData t3091_RGCTXData[3] = 
{
	&m17079_MI/* Method Usage */,
	&t405_TI/* Class Usage */,
	&m22910_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3091_0_0_0;
extern Il2CppType t3091_1_0_0;
extern Il2CppGenericClass t3091_GC;
TypeInfo t3091_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3091_MIs, t3091_PIs, t3091_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3091_TI, t3091_ITIs, t3091_VT, &EmptyCustomAttributesCache, &t3091_TI, &t3091_0_0_0, &t3091_1_0_0, t3091_IOs, &t3091_GC, NULL, NULL, NULL, t3091_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3091)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5754_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>
extern MethodInfo m30072_MI;
static PropertyInfo t5754____Count_PropertyInfo = 
{
	&t5754_TI, "Count", &m30072_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30073_MI;
static PropertyInfo t5754____IsReadOnly_PropertyInfo = 
{
	&t5754_TI, "IsReadOnly", &m30073_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5754_PIs[] =
{
	&t5754____Count_PropertyInfo,
	&t5754____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30072_GM;
MethodInfo m30072_MI = 
{
	"get_Count", NULL, &t5754_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30072_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30073_GM;
MethodInfo m30073_MI = 
{
	"get_IsReadOnly", NULL, &t5754_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30073_GM};
extern Il2CppType t405_0_0_0;
extern Il2CppType t405_0_0_0;
static ParameterInfo t5754_m30074_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30074_GM;
MethodInfo m30074_MI = 
{
	"Add", NULL, &t5754_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5754_m30074_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30074_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30075_GM;
MethodInfo m30075_MI = 
{
	"Clear", NULL, &t5754_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30075_GM};
extern Il2CppType t405_0_0_0;
static ParameterInfo t5754_m30076_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t405_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30076_GM;
MethodInfo m30076_MI = 
{
	"Contains", NULL, &t5754_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5754_m30076_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30076_GM};
extern Il2CppType t3785_0_0_0;
extern Il2CppType t3785_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5754_m30077_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3785_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30077_GM;
MethodInfo m30077_MI = 
{
	"CopyTo", NULL, &t5754_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5754_m30077_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30077_GM};
extern Il2CppType t405_0_0_0;
static ParameterInfo t5754_m30078_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t405_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30078_GM;
MethodInfo m30078_MI = 
{
	"Remove", NULL, &t5754_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5754_m30078_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30078_GM};
static MethodInfo* t5754_MIs[] =
{
	&m30072_MI,
	&m30073_MI,
	&m30074_MI,
	&m30075_MI,
	&m30076_MI,
	&m30077_MI,
	&m30078_MI,
	NULL
};
extern TypeInfo t5756_TI;
static TypeInfo* t5754_ITIs[] = 
{
	&t603_TI,
	&t5756_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5754_0_0_0;
extern Il2CppType t5754_1_0_0;
struct t5754;
extern Il2CppGenericClass t5754_GC;
TypeInfo t5754_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5754_MIs, t5754_PIs, NULL, NULL, NULL, NULL, NULL, &t5754_TI, t5754_ITIs, NULL, &EmptyCustomAttributesCache, &t5754_TI, &t5754_0_0_0, &t5754_1_0_0, NULL, &t5754_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextAreaAttribute>
extern Il2CppType t4483_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30079_GM;
MethodInfo m30079_MI = 
{
	"GetEnumerator", NULL, &t5756_TI, &t4483_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30079_GM};
static MethodInfo* t5756_MIs[] =
{
	&m30079_MI,
	NULL
};
static TypeInfo* t5756_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5756_0_0_0;
extern Il2CppType t5756_1_0_0;
struct t5756;
extern Il2CppGenericClass t5756_GC;
TypeInfo t5756_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5756_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5756_TI, t5756_ITIs, NULL, &EmptyCustomAttributesCache, &t5756_TI, &t5756_0_0_0, &t5756_1_0_0, NULL, &t5756_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5755_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>
extern MethodInfo m30080_MI;
extern MethodInfo m30081_MI;
static PropertyInfo t5755____Item_PropertyInfo = 
{
	&t5755_TI, "Item", &m30080_MI, &m30081_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5755_PIs[] =
{
	&t5755____Item_PropertyInfo,
	NULL
};
extern Il2CppType t405_0_0_0;
static ParameterInfo t5755_m30082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t405_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30082_GM;
MethodInfo m30082_MI = 
{
	"IndexOf", NULL, &t5755_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5755_m30082_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30082_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t405_0_0_0;
static ParameterInfo t5755_m30083_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30083_GM;
MethodInfo m30083_MI = 
{
	"Insert", NULL, &t5755_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5755_m30083_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30083_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5755_m30084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30084_GM;
MethodInfo m30084_MI = 
{
	"RemoveAt", NULL, &t5755_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5755_m30084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30084_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5755_m30080_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t405_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30080_GM;
MethodInfo m30080_MI = 
{
	"get_Item", NULL, &t5755_TI, &t405_0_0_0, RuntimeInvoker_t29_t44, t5755_m30080_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30080_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t405_0_0_0;
static ParameterInfo t5755_m30081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30081_GM;
MethodInfo m30081_MI = 
{
	"set_Item", NULL, &t5755_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5755_m30081_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30081_GM};
static MethodInfo* t5755_MIs[] =
{
	&m30082_MI,
	&m30083_MI,
	&m30084_MI,
	&m30080_MI,
	&m30081_MI,
	NULL
};
static TypeInfo* t5755_ITIs[] = 
{
	&t603_TI,
	&t5754_TI,
	&t5756_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5755_0_0_0;
extern Il2CppType t5755_1_0_0;
struct t5755;
extern Il2CppGenericClass t5755_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5755_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5755_MIs, t5755_PIs, NULL, NULL, NULL, NULL, NULL, &t5755_TI, t5755_ITIs, NULL, &t1908__CustomAttributeCache, &t5755_TI, &t5755_0_0_0, &t5755_1_0_0, NULL, &t5755_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4485_TI;

#include "t397.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SelectionBaseAttribute>
extern MethodInfo m30085_MI;
static PropertyInfo t4485____Current_PropertyInfo = 
{
	&t4485_TI, "Current", &m30085_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4485_PIs[] =
{
	&t4485____Current_PropertyInfo,
	NULL
};
extern Il2CppType t397_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30085_GM;
MethodInfo m30085_MI = 
{
	"get_Current", NULL, &t4485_TI, &t397_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30085_GM};
static MethodInfo* t4485_MIs[] =
{
	&m30085_MI,
	NULL
};
static TypeInfo* t4485_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4485_0_0_0;
extern Il2CppType t4485_1_0_0;
struct t4485;
extern Il2CppGenericClass t4485_GC;
TypeInfo t4485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4485_MIs, t4485_PIs, NULL, NULL, NULL, NULL, NULL, &t4485_TI, t4485_ITIs, NULL, &EmptyCustomAttributesCache, &t4485_TI, &t4485_0_0_0, &t4485_1_0_0, NULL, &t4485_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3092.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3092_TI;
#include "t3092MD.h"

extern TypeInfo t397_TI;
extern MethodInfo m17084_MI;
extern MethodInfo m22921_MI;
struct t20;
#define m22921(__this, p0, method) (t397 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3092_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3092_TI, offsetof(t3092, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3092_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3092_TI, offsetof(t3092, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3092_FIs[] =
{
	&t3092_f0_FieldInfo,
	&t3092_f1_FieldInfo,
	NULL
};
extern MethodInfo m17081_MI;
static PropertyInfo t3092____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3092_TI, "System.Collections.IEnumerator.Current", &m17081_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3092____Current_PropertyInfo = 
{
	&t3092_TI, "Current", &m17084_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3092_PIs[] =
{
	&t3092____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3092____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3092_m17080_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17080_GM;
MethodInfo m17080_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3092_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3092_m17080_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17080_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17081_GM;
MethodInfo m17081_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3092_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17081_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17082_GM;
MethodInfo m17082_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3092_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17082_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17083_GM;
MethodInfo m17083_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3092_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17083_GM};
extern Il2CppType t397_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17084_GM;
MethodInfo m17084_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3092_TI, &t397_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17084_GM};
static MethodInfo* t3092_MIs[] =
{
	&m17080_MI,
	&m17081_MI,
	&m17082_MI,
	&m17083_MI,
	&m17084_MI,
	NULL
};
extern MethodInfo m17083_MI;
extern MethodInfo m17082_MI;
static MethodInfo* t3092_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17081_MI,
	&m17083_MI,
	&m17082_MI,
	&m17084_MI,
};
static TypeInfo* t3092_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4485_TI,
};
static Il2CppInterfaceOffsetPair t3092_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4485_TI, 7},
};
extern TypeInfo t397_TI;
static Il2CppRGCTXData t3092_RGCTXData[3] = 
{
	&m17084_MI/* Method Usage */,
	&t397_TI/* Class Usage */,
	&m22921_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3092_0_0_0;
extern Il2CppType t3092_1_0_0;
extern Il2CppGenericClass t3092_GC;
TypeInfo t3092_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3092_MIs, t3092_PIs, t3092_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3092_TI, t3092_ITIs, t3092_VT, &EmptyCustomAttributesCache, &t3092_TI, &t3092_0_0_0, &t3092_1_0_0, t3092_IOs, &t3092_GC, NULL, NULL, NULL, t3092_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3092)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5757_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>
extern MethodInfo m30086_MI;
static PropertyInfo t5757____Count_PropertyInfo = 
{
	&t5757_TI, "Count", &m30086_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30087_MI;
static PropertyInfo t5757____IsReadOnly_PropertyInfo = 
{
	&t5757_TI, "IsReadOnly", &m30087_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5757_PIs[] =
{
	&t5757____Count_PropertyInfo,
	&t5757____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30086_GM;
MethodInfo m30086_MI = 
{
	"get_Count", NULL, &t5757_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30086_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30087_GM;
MethodInfo m30087_MI = 
{
	"get_IsReadOnly", NULL, &t5757_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30087_GM};
extern Il2CppType t397_0_0_0;
extern Il2CppType t397_0_0_0;
static ParameterInfo t5757_m30088_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30088_GM;
MethodInfo m30088_MI = 
{
	"Add", NULL, &t5757_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5757_m30088_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30088_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30089_GM;
MethodInfo m30089_MI = 
{
	"Clear", NULL, &t5757_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30089_GM};
extern Il2CppType t397_0_0_0;
static ParameterInfo t5757_m30090_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t397_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30090_GM;
MethodInfo m30090_MI = 
{
	"Contains", NULL, &t5757_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5757_m30090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30090_GM};
extern Il2CppType t3786_0_0_0;
extern Il2CppType t3786_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5757_m30091_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3786_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30091_GM;
MethodInfo m30091_MI = 
{
	"CopyTo", NULL, &t5757_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5757_m30091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30091_GM};
extern Il2CppType t397_0_0_0;
static ParameterInfo t5757_m30092_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t397_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30092_GM;
MethodInfo m30092_MI = 
{
	"Remove", NULL, &t5757_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5757_m30092_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30092_GM};
static MethodInfo* t5757_MIs[] =
{
	&m30086_MI,
	&m30087_MI,
	&m30088_MI,
	&m30089_MI,
	&m30090_MI,
	&m30091_MI,
	&m30092_MI,
	NULL
};
extern TypeInfo t5759_TI;
static TypeInfo* t5757_ITIs[] = 
{
	&t603_TI,
	&t5759_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5757_0_0_0;
extern Il2CppType t5757_1_0_0;
struct t5757;
extern Il2CppGenericClass t5757_GC;
TypeInfo t5757_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5757_MIs, t5757_PIs, NULL, NULL, NULL, NULL, NULL, &t5757_TI, t5757_ITIs, NULL, &EmptyCustomAttributesCache, &t5757_TI, &t5757_0_0_0, &t5757_1_0_0, NULL, &t5757_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SelectionBaseAttribute>
extern Il2CppType t4485_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30093_GM;
MethodInfo m30093_MI = 
{
	"GetEnumerator", NULL, &t5759_TI, &t4485_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30093_GM};
static MethodInfo* t5759_MIs[] =
{
	&m30093_MI,
	NULL
};
static TypeInfo* t5759_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5759_0_0_0;
extern Il2CppType t5759_1_0_0;
struct t5759;
extern Il2CppGenericClass t5759_GC;
TypeInfo t5759_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5759_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5759_TI, t5759_ITIs, NULL, &EmptyCustomAttributesCache, &t5759_TI, &t5759_0_0_0, &t5759_1_0_0, NULL, &t5759_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5758_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>
extern MethodInfo m30094_MI;
extern MethodInfo m30095_MI;
static PropertyInfo t5758____Item_PropertyInfo = 
{
	&t5758_TI, "Item", &m30094_MI, &m30095_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5758_PIs[] =
{
	&t5758____Item_PropertyInfo,
	NULL
};
extern Il2CppType t397_0_0_0;
static ParameterInfo t5758_m30096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t397_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30096_GM;
MethodInfo m30096_MI = 
{
	"IndexOf", NULL, &t5758_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5758_m30096_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30096_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t397_0_0_0;
static ParameterInfo t5758_m30097_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30097_GM;
MethodInfo m30097_MI = 
{
	"Insert", NULL, &t5758_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5758_m30097_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30097_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5758_m30098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30098_GM;
MethodInfo m30098_MI = 
{
	"RemoveAt", NULL, &t5758_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5758_m30098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30098_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5758_m30094_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t397_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30094_GM;
MethodInfo m30094_MI = 
{
	"get_Item", NULL, &t5758_TI, &t397_0_0_0, RuntimeInvoker_t29_t44, t5758_m30094_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30094_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t397_0_0_0;
static ParameterInfo t5758_m30095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30095_GM;
MethodInfo m30095_MI = 
{
	"set_Item", NULL, &t5758_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5758_m30095_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30095_GM};
static MethodInfo* t5758_MIs[] =
{
	&m30096_MI,
	&m30097_MI,
	&m30098_MI,
	&m30094_MI,
	&m30095_MI,
	NULL
};
static TypeInfo* t5758_ITIs[] = 
{
	&t603_TI,
	&t5757_TI,
	&t5759_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5758_0_0_0;
extern Il2CppType t5758_1_0_0;
struct t5758;
extern Il2CppGenericClass t5758_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5758_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5758_MIs, t5758_PIs, NULL, NULL, NULL, NULL, NULL, &t5758_TI, t5758_ITIs, NULL, &t1908__CustomAttributeCache, &t5758_TI, &t5758_0_0_0, &t5758_1_0_0, NULL, &t5758_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4487_TI;

#include "t637.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterInfo>
extern MethodInfo m30099_MI;
static PropertyInfo t4487____Current_PropertyInfo = 
{
	&t4487_TI, "Current", &m30099_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4487_PIs[] =
{
	&t4487____Current_PropertyInfo,
	NULL
};
extern Il2CppType t637_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30099_GM;
MethodInfo m30099_MI = 
{
	"get_Current", NULL, &t4487_TI, &t637_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30099_GM};
static MethodInfo* t4487_MIs[] =
{
	&m30099_MI,
	NULL
};
static TypeInfo* t4487_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4487_0_0_0;
extern Il2CppType t4487_1_0_0;
struct t4487;
extern Il2CppGenericClass t4487_GC;
TypeInfo t4487_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4487_MIs, t4487_PIs, NULL, NULL, NULL, NULL, NULL, &t4487_TI, t4487_ITIs, NULL, &EmptyCustomAttributesCache, &t4487_TI, &t4487_0_0_0, &t4487_1_0_0, NULL, &t4487_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3093.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3093_TI;
#include "t3093MD.h"

extern TypeInfo t637_TI;
extern MethodInfo m17089_MI;
extern MethodInfo m22932_MI;
struct t20;
#define m22932(__this, p0, method) (t637 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3093_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3093_TI, offsetof(t3093, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3093_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3093_TI, offsetof(t3093, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3093_FIs[] =
{
	&t3093_f0_FieldInfo,
	&t3093_f1_FieldInfo,
	NULL
};
extern MethodInfo m17086_MI;
static PropertyInfo t3093____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3093_TI, "System.Collections.IEnumerator.Current", &m17086_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3093____Current_PropertyInfo = 
{
	&t3093_TI, "Current", &m17089_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3093_PIs[] =
{
	&t3093____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3093____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3093_m17085_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17085_GM;
MethodInfo m17085_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3093_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3093_m17085_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17085_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17086_GM;
MethodInfo m17086_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3093_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17086_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17087_GM;
MethodInfo m17087_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3093_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17087_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17088_GM;
MethodInfo m17088_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3093_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17088_GM};
extern Il2CppType t637_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17089_GM;
MethodInfo m17089_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3093_TI, &t637_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17089_GM};
static MethodInfo* t3093_MIs[] =
{
	&m17085_MI,
	&m17086_MI,
	&m17087_MI,
	&m17088_MI,
	&m17089_MI,
	NULL
};
extern MethodInfo m17088_MI;
extern MethodInfo m17087_MI;
static MethodInfo* t3093_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17086_MI,
	&m17088_MI,
	&m17087_MI,
	&m17089_MI,
};
static TypeInfo* t3093_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4487_TI,
};
static Il2CppInterfaceOffsetPair t3093_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4487_TI, 7},
};
extern TypeInfo t637_TI;
static Il2CppRGCTXData t3093_RGCTXData[3] = 
{
	&m17089_MI/* Method Usage */,
	&t637_TI/* Class Usage */,
	&m22932_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3093_0_0_0;
extern Il2CppType t3093_1_0_0;
extern Il2CppGenericClass t3093_GC;
TypeInfo t3093_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3093_MIs, t3093_PIs, t3093_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3093_TI, t3093_ITIs, t3093_VT, &EmptyCustomAttributesCache, &t3093_TI, &t3093_0_0_0, &t3093_1_0_0, t3093_IOs, &t3093_GC, NULL, NULL, NULL, t3093_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3093)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5760_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>
extern MethodInfo m30100_MI;
static PropertyInfo t5760____Count_PropertyInfo = 
{
	&t5760_TI, "Count", &m30100_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30101_MI;
static PropertyInfo t5760____IsReadOnly_PropertyInfo = 
{
	&t5760_TI, "IsReadOnly", &m30101_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5760_PIs[] =
{
	&t5760____Count_PropertyInfo,
	&t5760____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30100_GM;
MethodInfo m30100_MI = 
{
	"get_Count", NULL, &t5760_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30100_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30101_GM;
MethodInfo m30101_MI = 
{
	"get_IsReadOnly", NULL, &t5760_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30101_GM};
extern Il2CppType t637_0_0_0;
extern Il2CppType t637_0_0_0;
static ParameterInfo t5760_m30102_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t637_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30102_GM;
MethodInfo m30102_MI = 
{
	"Add", NULL, &t5760_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5760_m30102_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30102_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30103_GM;
MethodInfo m30103_MI = 
{
	"Clear", NULL, &t5760_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30103_GM};
extern Il2CppType t637_0_0_0;
static ParameterInfo t5760_m30104_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t637_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30104_GM;
MethodInfo m30104_MI = 
{
	"Contains", NULL, &t5760_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5760_m30104_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30104_GM};
extern Il2CppType t638_0_0_0;
extern Il2CppType t638_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5760_m30105_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t638_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30105_GM;
MethodInfo m30105_MI = 
{
	"CopyTo", NULL, &t5760_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5760_m30105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30105_GM};
extern Il2CppType t637_0_0_0;
static ParameterInfo t5760_m30106_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t637_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30106_GM;
MethodInfo m30106_MI = 
{
	"Remove", NULL, &t5760_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5760_m30106_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30106_GM};
static MethodInfo* t5760_MIs[] =
{
	&m30100_MI,
	&m30101_MI,
	&m30102_MI,
	&m30103_MI,
	&m30104_MI,
	&m30105_MI,
	&m30106_MI,
	NULL
};
extern TypeInfo t5762_TI;
static TypeInfo* t5760_ITIs[] = 
{
	&t603_TI,
	&t5762_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5760_0_0_0;
extern Il2CppType t5760_1_0_0;
struct t5760;
extern Il2CppGenericClass t5760_GC;
TypeInfo t5760_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5760_MIs, t5760_PIs, NULL, NULL, NULL, NULL, NULL, &t5760_TI, t5760_ITIs, NULL, &EmptyCustomAttributesCache, &t5760_TI, &t5760_0_0_0, &t5760_1_0_0, NULL, &t5760_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterInfo>
extern Il2CppType t4487_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30107_GM;
MethodInfo m30107_MI = 
{
	"GetEnumerator", NULL, &t5762_TI, &t4487_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30107_GM};
static MethodInfo* t5762_MIs[] =
{
	&m30107_MI,
	NULL
};
static TypeInfo* t5762_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5762_0_0_0;
extern Il2CppType t5762_1_0_0;
struct t5762;
extern Il2CppGenericClass t5762_GC;
TypeInfo t5762_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5762_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5762_TI, t5762_ITIs, NULL, &EmptyCustomAttributesCache, &t5762_TI, &t5762_0_0_0, &t5762_1_0_0, NULL, &t5762_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
