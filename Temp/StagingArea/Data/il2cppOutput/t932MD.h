﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t932;
struct t1098;
struct t781;
struct t29;
struct t999;
#include "t934.h"

 void m8332 (t932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5142 (t932 * __this, t1098 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4064 (t932 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8333 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8334 (t932 * __this, int32_t p0, t1098 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8335 (t932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8336 (t932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4043 (t932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8337 (t932 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8338 (t932 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t934  m8339 (t932 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8340 (t932 * __this, t934  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8341 (t932 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8342 (t932 * __this, t29 * p0, t999 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
