﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2575;
struct t29;
struct t155;
struct t66;
struct t67;
#include "t35.h"
#include "t2569.h"

 void m13869 (t2575 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2569  m13870 (t2575 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13871 (t2575 * __this, t155 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2569  m13872 (t2575 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
