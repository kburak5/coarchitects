﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t601;
struct t29;
struct t42;
struct t1094;
struct t7;
struct t295;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 t29 * m5664 (double* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5665 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5666 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5667 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5668 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5669 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5670 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5671 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5672 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5673 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5674 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5675 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5676 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5677 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5678 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5679 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5680 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5681 (double* __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5682 (double* __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5683 (double* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5684 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5685 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5686 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5687 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5688 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5689 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5690 (t29 * __this, t7* p0, int32_t p1, t29 * p2, bool p3, double* p4, t295 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5691 (t29 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5692 (t29 * __this, uint8_t* p0, double* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5693 (double* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5694 (double* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5695 (double* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
