﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t113;
struct t114;
#include "t111.h"

 void m256 (t113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t114 * m257 (t113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m258 (t113 * __this, t114 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m259 (t113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m260 (t113 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
