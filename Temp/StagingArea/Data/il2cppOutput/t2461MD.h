﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2461;
struct t29;
struct t2464;
struct t2416;
struct t733;
struct t2462;
struct t20;
struct t136;
struct t2466;
struct t722;
#include "t735.h"
#include "t2465.h"
#include "t2467.h"
#include "t725.h"

 void m12834_gshared (t2461 * __this, MethodInfo* method);
#define m12834(__this, method) (void)m12834_gshared((t2461 *)__this, method)
 void m12836_gshared (t2461 * __this, t29* p0, MethodInfo* method);
#define m12836(__this, p0, method) (void)m12836_gshared((t2461 *)__this, (t29*)p0, method)
 void m12838_gshared (t2461 * __this, int32_t p0, MethodInfo* method);
#define m12838(__this, p0, method) (void)m12838_gshared((t2461 *)__this, (int32_t)p0, method)
 void m12840_gshared (t2461 * __this, t733 * p0, t735  p1, MethodInfo* method);
#define m12840(__this, p0, p1, method) (void)m12840_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
 t29 * m12842_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12842(__this, p0, method) (t29 *)m12842_gshared((t2461 *)__this, (t29 *)p0, method)
 void m12844_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12844(__this, p0, p1, method) (void)m12844_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
 void m12846_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12846(__this, p0, p1, method) (void)m12846_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
 void m12848_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12848(__this, p0, method) (void)m12848_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m12850_gshared (t2461 * __this, MethodInfo* method);
#define m12850(__this, method) (bool)m12850_gshared((t2461 *)__this, method)
 t29 * m12852_gshared (t2461 * __this, MethodInfo* method);
#define m12852(__this, method) (t29 *)m12852_gshared((t2461 *)__this, method)
 bool m12854_gshared (t2461 * __this, MethodInfo* method);
#define m12854(__this, method) (bool)m12854_gshared((t2461 *)__this, method)
 void m12911 (t2461 * __this, t2465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12912 (t2461 * __this, t2465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12858_gshared (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method);
#define m12858(__this, p0, p1, method) (void)m12858_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
 bool m12913 (t2461 * __this, t2465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12861_gshared (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method);
#define m12861(__this, p0, p1, method) (void)m12861_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
 t29 * m12863_gshared (t2461 * __this, MethodInfo* method);
#define m12863(__this, method) (t29 *)m12863_gshared((t2461 *)__this, method)
 t29* m12865_gshared (t2461 * __this, MethodInfo* method);
#define m12865(__this, method) (t29*)m12865_gshared((t2461 *)__this, method)
 t29 * m12867_gshared (t2461 * __this, MethodInfo* method);
#define m12867(__this, method) (t29 *)m12867_gshared((t2461 *)__this, method)
 int32_t m12869_gshared (t2461 * __this, MethodInfo* method);
#define m12869(__this, method) (int32_t)m12869_gshared((t2461 *)__this, method)
 t29 * m12871_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12871(__this, p0, method) (t29 *)m12871_gshared((t2461 *)__this, (t29 *)p0, method)
 void m12873_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12873(__this, p0, p1, method) (void)m12873_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
 void m12875_gshared (t2461 * __this, int32_t p0, t29* p1, MethodInfo* method);
#define m12875(__this, p0, p1, method) (void)m12875_gshared((t2461 *)__this, (int32_t)p0, (t29*)p1, method)
 void m12877_gshared (t2461 * __this, int32_t p0, MethodInfo* method);
#define m12877(__this, p0, method) (void)m12877_gshared((t2461 *)__this, (int32_t)p0, method)
 void m12879_gshared (t2461 * __this, t20 * p0, int32_t p1, MethodInfo* method);
#define m12879(__this, p0, p1, method) (void)m12879_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
 t2465  m12914 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12882_gshared (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12882(__this, p0, p1, method) (t29 *)m12882_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
 void m12884_gshared (t2461 * __this, t2462* p0, int32_t p1, MethodInfo* method);
#define m12884(__this, p0, p1, method) (void)m12884_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
 void m12886_gshared (t2461 * __this, MethodInfo* method);
#define m12886(__this, method) (void)m12886_gshared((t2461 *)__this, method)
 void m12887_gshared (t2461 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12887(__this, p0, p1, method) (void)m12887_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
 void m12889_gshared (t2461 * __this, MethodInfo* method);
#define m12889(__this, method) (void)m12889_gshared((t2461 *)__this, method)
 bool m12891_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12891(__this, p0, method) (bool)m12891_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m12893_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12893(__this, p0, method) (bool)m12893_gshared((t2461 *)__this, (t29 *)p0, method)
 void m12895_gshared (t2461 * __this, t733 * p0, t735  p1, MethodInfo* method);
#define m12895(__this, p0, p1, method) (void)m12895_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
 void m12897_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12897(__this, p0, method) (void)m12897_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m12899_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12899(__this, p0, method) (bool)m12899_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m12900_gshared (t2461 * __this, t29 * p0, t29 ** p1, MethodInfo* method);
#define m12900(__this, p0, p1, method) (bool)m12900_gshared((t2461 *)__this, (t29 *)p0, (t29 **)p1, method)
 t2464 * m12902_gshared (t2461 * __this, MethodInfo* method);
#define m12902(__this, method) (t2464 *)m12902_gshared((t2461 *)__this, method)
 t29 * m12904_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12904(__this, p0, method) (t29 *)m12904_gshared((t2461 *)__this, (t29 *)p0, method)
 t29 * m12906_gshared (t2461 * __this, t29 * p0, MethodInfo* method);
#define m12906(__this, p0, method) (t29 *)m12906_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m12915 (t2461 * __this, t2465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2467  m12916 (t2461 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12910_gshared (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12910(__this, p0, p1, method) (t725 )m12910_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
