﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1265;
struct t29;
struct t731;
struct t674;
struct t20;
struct t136;
struct t726;
struct t316;
struct t42;

 void m6575 (t1265 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6576 (t1265 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6577 (t1265 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6578 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6579 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6580 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6581 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6582 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6583 (t1265 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6584 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6585 (t1265 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6586 (t1265 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6587 (t1265 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6588 (t1265 * __this, t29 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6589 (t1265 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6590 (t1265 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6591 (t1265 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6592 (t1265 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6593 (t1265 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6594 (t1265 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6595 (t1265 * __this, int32_t p0, t20 * p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6596 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6597 (t1265 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6598 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6599 (t1265 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m6600 (t1265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t20 * m6601 (t1265 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
