﻿#pragma once
#include <stdint.h>
struct t204;
struct t204_marshaled;
struct t485;
#include "t29.h"
#include "t35.h"
struct t204  : public t29
{
	t35 f0;
};
struct t204_SFs{
	t204 * f1;
	t204 * f2;
	t485 * f3;
};
// Native definition for marshalling of: UnityEngine.Event
struct t204_marshaled
{
	t35 f0;
};
