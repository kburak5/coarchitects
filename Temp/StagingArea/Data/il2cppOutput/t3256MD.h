﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3256;
struct t29;
struct t20;
#include "t1043.h"

 void m18076 (t3256 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18077 (t3256 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18078 (t3256 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18079 (t3256 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18080 (t3256 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
