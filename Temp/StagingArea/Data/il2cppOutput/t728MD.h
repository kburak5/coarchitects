﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t728;
struct t29;
struct t729;

 void m3113 (t728 * __this, t729 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3114 (t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3115 (t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3116 (t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
