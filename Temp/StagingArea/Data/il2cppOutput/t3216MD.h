﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3216;
struct t29;
struct t20;
#include "t821.h"

 void m17880 (t3216 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17881 (t3216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17882 (t3216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17883 (t3216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17884 (t3216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
