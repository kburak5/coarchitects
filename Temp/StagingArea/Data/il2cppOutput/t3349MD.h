﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3349;
struct t29;
struct t20;
#include "t1313.h"

 void m18617 (t3349 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18618 (t3349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18619 (t3349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18620 (t3349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18621 (t3349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
