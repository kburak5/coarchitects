﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t867;
struct t29;
struct t868;

 void m3701 (t867 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3702 (t867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3703 (t867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3704 (t867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
