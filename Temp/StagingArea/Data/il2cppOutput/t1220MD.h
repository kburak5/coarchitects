﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1220;
struct t7;
struct t781;

 void m6416 (t1220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6417 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6418 (t1220 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6419 (t1220 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6420 (t1220 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6421 (t1220 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6422 (t1220 * __this, t781* p0, int32_t p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6423 (t1220 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6424 (t1220 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6425 (t1220 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6426 (t1220 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
