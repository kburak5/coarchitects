﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3120;
#include "t552.h"

 void m17241 (t3120 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17242 (t3120 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17243 (t3120 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
