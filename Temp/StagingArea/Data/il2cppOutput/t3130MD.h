﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3130;
struct t29;
struct t557;
struct t3131;
struct t316;

 void m17282 (t3130 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17283 (t3130 * __this, t3131 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17284 (t3130 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17285 (t3130 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
