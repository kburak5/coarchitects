﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2592;
struct t29;
struct t20;
#include "t17.h"

 void m13971 (t2592 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13972 (t2592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13973 (t2592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13974 (t2592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m13975 (t2592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
