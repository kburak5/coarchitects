﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1348;
struct t42;
struct t929;
struct t7;
struct t1142;
struct t660;
struct t631;
struct t537;
struct t634;
struct t1147;
struct t1143;
struct t1144;
struct t1145;
struct t557;
struct t1146;
struct t29;
struct t316;
struct t633;
struct t446;
struct t295;
#include "t43.h"
#include "t1148.h"
#include "t630.h"
#include "t1150.h"

 bool m7334 (t1348 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7335 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m7336 (t1348 * __this, int32_t p0, t631 * p1, int32_t p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1147* m7337 (t1348 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1143 * m7338 (t1348 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1144 * m7339 (t1348 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7340 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m7341 (t1348 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7342 (t1348 * __this, t7* p0, int32_t p1, t631 * p2, int32_t p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m7343 (t1348 * __this, t7* p0, int32_t p1, t631 * p2, t42 * p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7344 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7345 (t1348 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7346 (t1348 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7347 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7348 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7349 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7350 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7351 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7352 (t1348 * __this, t7* p0, int32_t p1, t631 * p2, t29 * p3, t316* p4, t634* p5, t633 * p6, t446* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7353 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7354 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m7355 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7356 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7357 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7358 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7359 (t1348 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7360 (t1348 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7361 (t1348 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7362 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7363 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7364 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7365 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7366 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t43  m7367 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7368 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7369 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7370 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7371 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7372 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7373 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7374 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7375 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7376 (t1348 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7377 (t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7378 (t1348 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
