﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1228;
struct t1219;
struct t29;
struct t292;
struct t7;

 void m6475 (t1228 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6476 (t1228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6477 (t1228 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6478 (t1228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6479 (t1228 * __this, t292 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6480 (t1228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
