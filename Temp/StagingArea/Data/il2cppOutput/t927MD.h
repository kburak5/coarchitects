﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t927;
struct t733;
struct t29;
#include "t35.h"
#include "t735.h"

 void m6051 (t927 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m6052 (t927 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6053 (t927 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6054 (t927 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6055 (t927 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
