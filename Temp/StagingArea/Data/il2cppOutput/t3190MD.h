﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3190;
struct t29;
struct t770;
struct t3196;
struct t20;
struct t136;
struct t771;
#include "t3197.h"

 void m17760 (t3190 * __this, t770 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17761 (t3190 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17762 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17763 (t3190 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17764 (t3190 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m17765 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17766 (t3190 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17767 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17768 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17769 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17770 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17771 (t3190 * __this, t771* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3197  m17772 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17773 (t3190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
