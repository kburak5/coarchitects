﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3420;
struct t29;
struct t20;
#include "t1156.h"

 void m18970 (t3420 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18971 (t3420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18972 (t3420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18973 (t3420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18974 (t3420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
