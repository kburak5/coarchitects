﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t745;
struct t7;
struct t781;
struct t733;
struct t29;
#include "t735.h"
#include "t795.h"

 void m8162 (t745 * __this, t781* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5247 (t745 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4088 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8163 (t745 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4114 (t745 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4113 (t745 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8164 (t745 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4115 (t745 * __this, t745 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4116 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4086 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4117 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4118 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4112 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4119 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4120 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4121 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4122 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8165 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4104 (t745 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4107 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4106 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4111 (t745 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4101 (t745 * __this, t781* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4103 (t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
