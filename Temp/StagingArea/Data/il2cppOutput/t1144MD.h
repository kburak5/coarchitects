﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1144;
struct t42;
struct t1346;
struct t29;
struct t631;
struct t633;
struct t316;
#include "t1347.h"
#include "t927.h"
#include "t1149.h"
#include "t630.h"
#include "t35.h"

 void m7524 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7525 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7526 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7527 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7528 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7529 (t1144 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1144 * m7530 (t29 * __this, t35 p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1144 * m7531 (t29 * __this, t927  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7532 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1346 * m7533 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1346 * m7534 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7535 (t1144 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
