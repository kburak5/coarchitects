﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t937;
struct t7;
struct t295;
struct t733;
#include "t735.h"

 void m8173 (t937 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4058 (t937 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4062 (t937 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8174 (t937 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8175 (t937 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
