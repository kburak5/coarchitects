﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1271;
struct t633;
struct t29;
struct t733;
#include "t735.h"

 void m6679 (t1271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6680 (t1271 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6681 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6682 (t1271 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6683 (t1271 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
