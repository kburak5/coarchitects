﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t495;
struct t29;
struct t7;
struct t66;
struct t67;
#include "t35.h"
#include "t447.h"

 void m2473 (t495 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2474 (t495 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2475 (t495 * __this, t7* p0, t7* p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2476 (t495 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
