﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2119;
struct t29;
struct t557;
struct t2120;
struct t316;

 void m10321_gshared (t2119 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m10321(__this, p0, p1, method) (void)m10321_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
 void m10322_gshared (t2119 * __this, t2120 * p0, MethodInfo* method);
#define m10322(__this, p0, method) (void)m10322_gshared((t2119 *)__this, (t2120 *)p0, method)
 void m10323_gshared (t2119 * __this, t316* p0, MethodInfo* method);
#define m10323(__this, p0, method) (void)m10323_gshared((t2119 *)__this, (t316*)p0, method)
 bool m10324_gshared (t2119 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m10324(__this, p0, p1, method) (bool)m10324_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
