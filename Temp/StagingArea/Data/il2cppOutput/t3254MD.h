﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3254;
struct t29;
struct t20;
#include "t1044.h"

 void m18066 (t3254 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18067 (t3254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18068 (t3254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18069 (t3254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18070 (t3254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
