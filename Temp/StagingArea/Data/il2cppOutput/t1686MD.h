﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1686;
struct t1686_marshaled;

void t1686_marshal(const t1686& unmarshaled, t1686_marshaled& marshaled);
void t1686_marshal_back(const t1686_marshaled& marshaled, t1686& unmarshaled);
void t1686_marshal_cleanup(t1686_marshaled& marshaled);
