﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t362;
struct t7;
struct t29;
#include "t362.h"
#include "t23.h"

 float m2337 (t29 * __this, t362  p0, t362  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t362  m1884 (t29 * __this, t362  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t362  m2338 (t29 * __this, t362 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2339 (t362 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2340 (t362 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2341 (t362 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1621 (t29 * __this, t362  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1790 (t29 * __this, t362  p0, t362  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
