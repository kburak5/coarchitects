﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t992;
struct t781;
struct t783;
struct t955;

 void m4418 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4419 (t29 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4420 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4421 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4422 (t29 * __this, t783 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4423 (t29 * __this, t783 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4424 (t29 * __this, t783 * p0, t955 * p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4425 (t29 * __this, t783 * p0, t955 * p1, t781* p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4426 (t29 * __this, t783 * p0, t955 * p1, t781* p2, t781* p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4427 (t29 * __this, t955 * p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
