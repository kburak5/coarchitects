﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2610;
struct t29;
struct t20;
#include "t188.h"

 void m14059 (t2610 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14060 (t2610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14061 (t2610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14062 (t2610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14063 (t2610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
