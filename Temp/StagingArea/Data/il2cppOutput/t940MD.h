﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t940;
struct t7;
struct t793;
struct t292;

 void m4491 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4492 (t29 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4065 (t29 * __this, t793 * p0, bool p1, t7* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4493 (t29 * __this, t292 * p0, t793 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
