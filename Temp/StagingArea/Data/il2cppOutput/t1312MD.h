﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1312;
struct t7;
struct t733;
#include "t735.h"

 void m7016 (t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7017 (t1312 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7018 (t1312 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7019 (t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7020 (t1312 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7021 (t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
