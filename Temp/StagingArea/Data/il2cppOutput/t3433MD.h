﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3433;
struct t29;
struct t20;
#include "t1490.h"

 void m19035 (t3433 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19036 (t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19037 (t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19038 (t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m19039 (t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
