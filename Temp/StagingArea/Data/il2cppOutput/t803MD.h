﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t803;
struct t796;
#include "t814.h"
#include "t815.h"
#include "t817.h"
#include "t465.h"

 void m3404 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t796 * m3405 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3406 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3407 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3408 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m3409 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3410 (t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
