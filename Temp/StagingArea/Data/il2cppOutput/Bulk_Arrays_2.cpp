﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "UnityEngine_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t482_TI;


#include "t20.h"

// Metadata Definition UnityEngine.GUIStyle[]
static MethodInfo* t482_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m4200_MI;
extern MethodInfo m5904_MI;
extern MethodInfo m5920_MI;
extern MethodInfo m5921_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5922_MI;
extern MethodInfo m5923_MI;
extern MethodInfo m5895_MI;
extern MethodInfo m5896_MI;
extern MethodInfo m5897_MI;
extern MethodInfo m5898_MI;
extern MethodInfo m5899_MI;
extern MethodInfo m5900_MI;
extern MethodInfo m5901_MI;
extern MethodInfo m5902_MI;
extern MethodInfo m5903_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m5905_MI;
extern MethodInfo m5906_MI;
extern MethodInfo m22176_MI;
extern MethodInfo m5907_MI;
extern MethodInfo m22177_MI;
extern MethodInfo m22178_MI;
extern MethodInfo m22179_MI;
extern MethodInfo m22180_MI;
extern MethodInfo m22181_MI;
extern MethodInfo m5908_MI;
extern MethodInfo m22175_MI;
extern MethodInfo m22183_MI;
extern MethodInfo m22184_MI;
extern MethodInfo m19494_MI;
extern MethodInfo m19496_MI;
extern MethodInfo m19498_MI;
extern MethodInfo m19499_MI;
extern MethodInfo m19500_MI;
extern MethodInfo m19501_MI;
extern MethodInfo m19490_MI;
extern MethodInfo m19503_MI;
extern MethodInfo m19504_MI;
static MethodInfo* t482_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22176_MI,
	&m5907_MI,
	&m22177_MI,
	&m22178_MI,
	&m22179_MI,
	&m22180_MI,
	&m22181_MI,
	&m5908_MI,
	&m22175_MI,
	&m22183_MI,
	&m22184_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5583_TI;
extern TypeInfo t5584_TI;
extern TypeInfo t5585_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2186_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t482_ITIs[] = 
{
	&t5583_TI,
	&t5584_TI,
	&t5585_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static Il2CppInterfaceOffsetPair t482_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5583_TI, 21},
	{ &t5584_TI, 28},
	{ &t5585_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t482_0_0_0;
extern Il2CppType t482_1_0_0;
extern TypeInfo t20_TI;
struct t466;
extern TypeInfo t466_TI;
extern CustomAttributesCache t466__CustomAttributeCache_m2274;
extern CustomAttributesCache t466__CustomAttributeCache_m2275;
extern CustomAttributesCache t466__CustomAttributeCache_m2276;
extern CustomAttributesCache t466__CustomAttributeCache_m2277;
extern CustomAttributesCache t466__CustomAttributeCache_m2279;
extern CustomAttributesCache t466__CustomAttributeCache_m2282;
extern CustomAttributesCache t466__CustomAttributeCache_m2283;
extern CustomAttributesCache t466__CustomAttributeCache_m2284;
extern CustomAttributesCache t466__CustomAttributeCache_m2285;
extern CustomAttributesCache t466__CustomAttributeCache_m2286;
extern CustomAttributesCache t466__CustomAttributeCache_m2287;
extern CustomAttributesCache t466__CustomAttributeCache_m2288;
extern CustomAttributesCache t466__CustomAttributeCache_m2289;
extern CustomAttributesCache t466__CustomAttributeCache_m2291;
extern CustomAttributesCache t466__CustomAttributeCache_m2295;
extern CustomAttributesCache t466__CustomAttributeCache_m2297;
extern CustomAttributesCache t466__CustomAttributeCache_m2299;
TypeInfo t482_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GUIStyle[]", "UnityEngine", t482_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t466_TI, t482_ITIs, t482_VT, &EmptyCustomAttributesCache, &t482_TI, &t482_0_0_0, &t482_1_0_0, t482_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t466 *), -1, sizeof(t482_SFs), 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, true, true, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#include "mscorlib_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2892_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
static MethodInfo* t2892_MIs[] =
{
	NULL
};
extern MethodInfo m22187_MI;
extern MethodInfo m22188_MI;
extern MethodInfo m22189_MI;
extern MethodInfo m22190_MI;
extern MethodInfo m22191_MI;
extern MethodInfo m22192_MI;
extern MethodInfo m22186_MI;
extern MethodInfo m22194_MI;
extern MethodInfo m22195_MI;
extern MethodInfo m19755_MI;
extern MethodInfo m19756_MI;
extern MethodInfo m19757_MI;
extern MethodInfo m19758_MI;
extern MethodInfo m19759_MI;
extern MethodInfo m19760_MI;
extern MethodInfo m19754_MI;
extern MethodInfo m19762_MI;
extern MethodInfo m19763_MI;
static MethodInfo* t2892_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22187_MI,
	&m5907_MI,
	&m22188_MI,
	&m22189_MI,
	&m22190_MI,
	&m22191_MI,
	&m22192_MI,
	&m5908_MI,
	&m22186_MI,
	&m22194_MI,
	&m22195_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5586_TI;
extern TypeInfo t5587_TI;
extern TypeInfo t5588_TI;
extern TypeInfo t5116_TI;
extern TypeInfo t5117_TI;
extern TypeInfo t5118_TI;
static TypeInfo* t2892_ITIs[] = 
{
	&t5586_TI,
	&t5587_TI,
	&t5588_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2892_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5586_TI, 21},
	{ &t5587_TI, 28},
	{ &t5588_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2892_0_0_0;
extern Il2CppType t2892_1_0_0;
#include "t2893.h"
extern TypeInfo t2893_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2892_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2892_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2893_TI, t2892_ITIs, t2892_VT, &EmptyCustomAttributesCache, &t2892_TI, &t2892_0_0_0, &t2892_1_0_0, t2892_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2893 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3746_TI;



// Metadata Definition UnityEngine.FontStyle[]
static MethodInfo* t3746_MIs[] =
{
	NULL
};
extern MethodInfo m22205_MI;
extern MethodInfo m22206_MI;
extern MethodInfo m22207_MI;
extern MethodInfo m22208_MI;
extern MethodInfo m22209_MI;
extern MethodInfo m22210_MI;
extern MethodInfo m22204_MI;
extern MethodInfo m22212_MI;
extern MethodInfo m22213_MI;
extern MethodInfo m19711_MI;
extern MethodInfo m19712_MI;
extern MethodInfo m19713_MI;
extern MethodInfo m19714_MI;
extern MethodInfo m19715_MI;
extern MethodInfo m19716_MI;
extern MethodInfo m19710_MI;
extern MethodInfo m19718_MI;
extern MethodInfo m19719_MI;
extern MethodInfo m19722_MI;
extern MethodInfo m19723_MI;
extern MethodInfo m19724_MI;
extern MethodInfo m19725_MI;
extern MethodInfo m19726_MI;
extern MethodInfo m19727_MI;
extern MethodInfo m19721_MI;
extern MethodInfo m19729_MI;
extern MethodInfo m19730_MI;
extern MethodInfo m19733_MI;
extern MethodInfo m19734_MI;
extern MethodInfo m19735_MI;
extern MethodInfo m19736_MI;
extern MethodInfo m19737_MI;
extern MethodInfo m19738_MI;
extern MethodInfo m19732_MI;
extern MethodInfo m19740_MI;
extern MethodInfo m19741_MI;
extern MethodInfo m19744_MI;
extern MethodInfo m19745_MI;
extern MethodInfo m19746_MI;
extern MethodInfo m19747_MI;
extern MethodInfo m19748_MI;
extern MethodInfo m19749_MI;
extern MethodInfo m19743_MI;
extern MethodInfo m19751_MI;
extern MethodInfo m19752_MI;
static MethodInfo* t3746_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22205_MI,
	&m5907_MI,
	&m22206_MI,
	&m22207_MI,
	&m22208_MI,
	&m22209_MI,
	&m22210_MI,
	&m5908_MI,
	&m22204_MI,
	&m22212_MI,
	&m22213_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5589_TI;
extern TypeInfo t5590_TI;
extern TypeInfo t5591_TI;
extern TypeInfo t5104_TI;
extern TypeInfo t5105_TI;
extern TypeInfo t5106_TI;
extern TypeInfo t5107_TI;
extern TypeInfo t5108_TI;
extern TypeInfo t5109_TI;
extern TypeInfo t5110_TI;
extern TypeInfo t5111_TI;
extern TypeInfo t5112_TI;
extern TypeInfo t5113_TI;
extern TypeInfo t5114_TI;
extern TypeInfo t5115_TI;
static TypeInfo* t3746_ITIs[] = 
{
	&t5589_TI,
	&t5590_TI,
	&t5591_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3746_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5589_TI, 21},
	{ &t5590_TI, 28},
	{ &t5591_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3746_0_0_0;
extern Il2CppType t3746_1_0_0;
#include "t149.h"
extern TypeInfo t149_TI;
extern TypeInfo t44_TI;
TypeInfo t3746_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "FontStyle[]", "UnityEngine", t3746_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t149_TI, t3746_ITIs, t3746_VT, &EmptyCustomAttributesCache, &t44_TI, &t3746_0_0_0, &t3746_1_0_0, t3746_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3747_TI;



// Metadata Definition UnityEngine.TouchScreenKeyboardType[]
static MethodInfo* t3747_MIs[] =
{
	NULL
};
extern MethodInfo m22216_MI;
extern MethodInfo m22217_MI;
extern MethodInfo m22218_MI;
extern MethodInfo m22219_MI;
extern MethodInfo m22220_MI;
extern MethodInfo m22221_MI;
extern MethodInfo m22215_MI;
extern MethodInfo m22223_MI;
extern MethodInfo m22224_MI;
static MethodInfo* t3747_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22216_MI,
	&m5907_MI,
	&m22217_MI,
	&m22218_MI,
	&m22219_MI,
	&m22220_MI,
	&m22221_MI,
	&m5908_MI,
	&m22215_MI,
	&m22223_MI,
	&m22224_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5592_TI;
extern TypeInfo t5593_TI;
extern TypeInfo t5594_TI;
static TypeInfo* t3747_ITIs[] = 
{
	&t5592_TI,
	&t5593_TI,
	&t5594_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3747_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5592_TI, 21},
	{ &t5593_TI, 28},
	{ &t5594_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3747_0_0_0;
extern Il2CppType t3747_1_0_0;
#include "t205.h"
extern TypeInfo t205_TI;
TypeInfo t3747_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TouchScreenKeyboardType[]", "UnityEngine", t3747_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t205_TI, t3747_ITIs, t3747_VT, &EmptyCustomAttributesCache, &t44_TI, &t3747_0_0_0, &t3747_1_0_0, t3747_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2910_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
static MethodInfo* t2910_MIs[] =
{
	NULL
};
extern MethodInfo m22227_MI;
extern MethodInfo m22228_MI;
extern MethodInfo m22229_MI;
extern MethodInfo m22230_MI;
extern MethodInfo m22231_MI;
extern MethodInfo m22232_MI;
extern MethodInfo m22226_MI;
extern MethodInfo m22234_MI;
extern MethodInfo m22235_MI;
static MethodInfo* t2910_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22227_MI,
	&m5907_MI,
	&m22228_MI,
	&m22229_MI,
	&m22230_MI,
	&m22231_MI,
	&m22232_MI,
	&m5908_MI,
	&m22226_MI,
	&m22234_MI,
	&m22235_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5595_TI;
extern TypeInfo t5596_TI;
extern TypeInfo t5597_TI;
static TypeInfo* t2910_ITIs[] = 
{
	&t5595_TI,
	&t5596_TI,
	&t5597_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2910_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5595_TI, 21},
	{ &t5596_TI, 28},
	{ &t5597_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2910_0_0_0;
extern Il2CppType t2910_1_0_0;
#include "t2911.h"
extern TypeInfo t2911_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2910_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2910_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2911_TI, t2910_ITIs, t2910_VT, &EmptyCustomAttributesCache, &t2910_TI, &t2910_0_0_0, &t2910_1_0_0, t2910_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2911 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3748_TI;



// Metadata Definition UnityEngine.KeyCode[]
static MethodInfo* t3748_MIs[] =
{
	NULL
};
extern MethodInfo m22245_MI;
extern MethodInfo m22246_MI;
extern MethodInfo m22247_MI;
extern MethodInfo m22248_MI;
extern MethodInfo m22249_MI;
extern MethodInfo m22250_MI;
extern MethodInfo m22244_MI;
extern MethodInfo m22252_MI;
extern MethodInfo m22253_MI;
static MethodInfo* t3748_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22245_MI,
	&m5907_MI,
	&m22246_MI,
	&m22247_MI,
	&m22248_MI,
	&m22249_MI,
	&m22250_MI,
	&m5908_MI,
	&m22244_MI,
	&m22252_MI,
	&m22253_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5598_TI;
extern TypeInfo t5599_TI;
extern TypeInfo t5600_TI;
static TypeInfo* t3748_ITIs[] = 
{
	&t5598_TI,
	&t5599_TI,
	&t5600_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3748_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5598_TI, 21},
	{ &t5599_TI, 28},
	{ &t5600_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3748_0_0_0;
extern Il2CppType t3748_1_0_0;
#include "t383.h"
extern TypeInfo t383_TI;
TypeInfo t3748_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "KeyCode[]", "UnityEngine", t3748_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t383_TI, t3748_ITIs, t3748_VT, &EmptyCustomAttributesCache, &t44_TI, &t3748_0_0_0, &t3748_1_0_0, t3748_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3749_TI;



// Metadata Definition UnityEngine.EventType[]
static MethodInfo* t3749_MIs[] =
{
	NULL
};
extern MethodInfo m22256_MI;
extern MethodInfo m22257_MI;
extern MethodInfo m22258_MI;
extern MethodInfo m22259_MI;
extern MethodInfo m22260_MI;
extern MethodInfo m22261_MI;
extern MethodInfo m22255_MI;
extern MethodInfo m22263_MI;
extern MethodInfo m22264_MI;
static MethodInfo* t3749_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22256_MI,
	&m5907_MI,
	&m22257_MI,
	&m22258_MI,
	&m22259_MI,
	&m22260_MI,
	&m22261_MI,
	&m5908_MI,
	&m22255_MI,
	&m22263_MI,
	&m22264_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5601_TI;
extern TypeInfo t5602_TI;
extern TypeInfo t5603_TI;
static TypeInfo* t3749_ITIs[] = 
{
	&t5601_TI,
	&t5602_TI,
	&t5603_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3749_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5601_TI, 21},
	{ &t5602_TI, 28},
	{ &t5603_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3749_0_0_0;
extern Il2CppType t3749_1_0_0;
#include "t384.h"
extern TypeInfo t384_TI;
TypeInfo t3749_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "EventType[]", "UnityEngine", t3749_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t384_TI, t3749_ITIs, t3749_VT, &EmptyCustomAttributesCache, &t44_TI, &t3749_0_0_0, &t3749_1_0_0, t3749_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3750_TI;



// Metadata Definition UnityEngine.EventModifiers[]
static MethodInfo* t3750_MIs[] =
{
	NULL
};
extern MethodInfo m22267_MI;
extern MethodInfo m22268_MI;
extern MethodInfo m22269_MI;
extern MethodInfo m22270_MI;
extern MethodInfo m22271_MI;
extern MethodInfo m22272_MI;
extern MethodInfo m22266_MI;
extern MethodInfo m22274_MI;
extern MethodInfo m22275_MI;
static MethodInfo* t3750_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22267_MI,
	&m5907_MI,
	&m22268_MI,
	&m22269_MI,
	&m22270_MI,
	&m22271_MI,
	&m22272_MI,
	&m5908_MI,
	&m22266_MI,
	&m22274_MI,
	&m22275_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5604_TI;
extern TypeInfo t5605_TI;
extern TypeInfo t5606_TI;
static TypeInfo* t3750_ITIs[] = 
{
	&t5604_TI,
	&t5605_TI,
	&t5606_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3750_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5604_TI, 21},
	{ &t5605_TI, 28},
	{ &t5606_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3750_0_0_0;
extern Il2CppType t3750_1_0_0;
#include "t382.h"
extern TypeInfo t382_TI;
extern CustomAttributesCache t382__CustomAttributeCache;
TypeInfo t3750_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "EventModifiers[]", "UnityEngine", t3750_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t382_TI, t3750_ITIs, t3750_VT, &EmptyCustomAttributesCache, &t44_TI, &t3750_0_0_0, &t3750_1_0_0, t3750_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3751_TI;



// Metadata Definition UnityEngine.DrivenTransformProperties[]
static MethodInfo* t3751_MIs[] =
{
	NULL
};
extern MethodInfo m22278_MI;
extern MethodInfo m22279_MI;
extern MethodInfo m22280_MI;
extern MethodInfo m22281_MI;
extern MethodInfo m22282_MI;
extern MethodInfo m22283_MI;
extern MethodInfo m22277_MI;
extern MethodInfo m22285_MI;
extern MethodInfo m22286_MI;
static MethodInfo* t3751_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22278_MI,
	&m5907_MI,
	&m22279_MI,
	&m22280_MI,
	&m22281_MI,
	&m22282_MI,
	&m22283_MI,
	&m5908_MI,
	&m22277_MI,
	&m22285_MI,
	&m22286_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5607_TI;
extern TypeInfo t5608_TI;
extern TypeInfo t5609_TI;
static TypeInfo* t3751_ITIs[] = 
{
	&t5607_TI,
	&t5608_TI,
	&t5609_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3751_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5607_TI, 21},
	{ &t5608_TI, 28},
	{ &t5609_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3751_0_0_0;
extern Il2CppType t3751_1_0_0;
#include "t393.h"
extern TypeInfo t393_TI;
extern CustomAttributesCache t393__CustomAttributeCache;
TypeInfo t3751_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DrivenTransformProperties[]", "UnityEngine", t3751_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t393_TI, t3751_ITIs, t3751_VT, &EmptyCustomAttributesCache, &t44_TI, &t3751_0_0_0, &t3751_1_0_0, t3751_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3752_TI;



// Metadata Definition UnityEngine.RectTransform/Edge[]
static MethodInfo* t3752_MIs[] =
{
	NULL
};
extern MethodInfo m22290_MI;
extern MethodInfo m22291_MI;
extern MethodInfo m22292_MI;
extern MethodInfo m22293_MI;
extern MethodInfo m22294_MI;
extern MethodInfo m22295_MI;
extern MethodInfo m22289_MI;
extern MethodInfo m22297_MI;
extern MethodInfo m22298_MI;
static MethodInfo* t3752_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22290_MI,
	&m5907_MI,
	&m22291_MI,
	&m22292_MI,
	&m22293_MI,
	&m22294_MI,
	&m22295_MI,
	&m5908_MI,
	&m22289_MI,
	&m22297_MI,
	&m22298_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5610_TI;
extern TypeInfo t5611_TI;
extern TypeInfo t5612_TI;
static TypeInfo* t3752_ITIs[] = 
{
	&t5610_TI,
	&t5611_TI,
	&t5612_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3752_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5610_TI, 21},
	{ &t5611_TI, 28},
	{ &t5612_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3752_0_0_0;
extern Il2CppType t3752_1_0_0;
#include "t413.h"
extern TypeInfo t413_TI;
TypeInfo t3752_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Edge[]", "", t3752_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t413_TI, t3752_ITIs, t3752_VT, &EmptyCustomAttributesCache, &t44_TI, &t3752_0_0_0, &t3752_1_0_0, t3752_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3753_TI;



// Metadata Definition UnityEngine.RectTransform/Axis[]
static MethodInfo* t3753_MIs[] =
{
	NULL
};
extern MethodInfo m22301_MI;
extern MethodInfo m22302_MI;
extern MethodInfo m22303_MI;
extern MethodInfo m22304_MI;
extern MethodInfo m22305_MI;
extern MethodInfo m22306_MI;
extern MethodInfo m22300_MI;
extern MethodInfo m22308_MI;
extern MethodInfo m22309_MI;
static MethodInfo* t3753_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22301_MI,
	&m5907_MI,
	&m22302_MI,
	&m22303_MI,
	&m22304_MI,
	&m22305_MI,
	&m22306_MI,
	&m5908_MI,
	&m22300_MI,
	&m22308_MI,
	&m22309_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5613_TI;
extern TypeInfo t5614_TI;
extern TypeInfo t5615_TI;
static TypeInfo* t3753_ITIs[] = 
{
	&t5613_TI,
	&t5614_TI,
	&t5615_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3753_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5613_TI, 21},
	{ &t5614_TI, 28},
	{ &t5615_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3753_0_0_0;
extern Il2CppType t3753_1_0_0;
#include "t408.h"
extern TypeInfo t408_TI;
TypeInfo t3753_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Axis[]", "", t3753_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t408_TI, t3753_ITIs, t3753_VT, &EmptyCustomAttributesCache, &t44_TI, &t3753_0_0_0, &t3753_1_0_0, t3753_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3754_TI;



// Metadata Definition UnityEngine.SerializePrivateVariables[]
static MethodInfo* t3754_MIs[] =
{
	NULL
};
extern MethodInfo m22312_MI;
extern MethodInfo m22313_MI;
extern MethodInfo m22314_MI;
extern MethodInfo m22315_MI;
extern MethodInfo m22316_MI;
extern MethodInfo m22317_MI;
extern MethodInfo m22311_MI;
extern MethodInfo m22319_MI;
extern MethodInfo m22320_MI;
extern MethodInfo m22323_MI;
extern MethodInfo m22324_MI;
extern MethodInfo m22325_MI;
extern MethodInfo m22326_MI;
extern MethodInfo m22327_MI;
extern MethodInfo m22328_MI;
extern MethodInfo m22322_MI;
extern MethodInfo m22330_MI;
extern MethodInfo m22331_MI;
extern MethodInfo m22334_MI;
extern MethodInfo m22335_MI;
extern MethodInfo m22336_MI;
extern MethodInfo m22337_MI;
extern MethodInfo m22338_MI;
extern MethodInfo m22339_MI;
extern MethodInfo m22333_MI;
extern MethodInfo m22341_MI;
extern MethodInfo m22342_MI;
static MethodInfo* t3754_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22312_MI,
	&m5907_MI,
	&m22313_MI,
	&m22314_MI,
	&m22315_MI,
	&m22316_MI,
	&m22317_MI,
	&m5908_MI,
	&m22311_MI,
	&m22319_MI,
	&m22320_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5616_TI;
extern TypeInfo t5617_TI;
extern TypeInfo t5618_TI;
extern TypeInfo t5619_TI;
extern TypeInfo t5620_TI;
extern TypeInfo t5621_TI;
extern TypeInfo t5622_TI;
extern TypeInfo t5623_TI;
extern TypeInfo t5624_TI;
static TypeInfo* t3754_ITIs[] = 
{
	&t5616_TI,
	&t5617_TI,
	&t5618_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3754_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5616_TI, 21},
	{ &t5617_TI, 28},
	{ &t5618_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3754_0_0_0;
extern Il2CppType t3754_1_0_0;
struct t489;
extern TypeInfo t489_TI;
extern CustomAttributesCache t489__CustomAttributeCache;
TypeInfo t3754_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SerializePrivateVariables[]", "UnityEngine", t3754_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t489_TI, t3754_ITIs, t3754_VT, &EmptyCustomAttributesCache, &t3754_TI, &t3754_0_0_0, &t3754_1_0_0, t3754_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t489 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3542_TI;



// Metadata Definition System.Attribute[]
static MethodInfo* t3542_MIs[] =
{
	NULL
};
static MethodInfo* t3542_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3542_ITIs[] = 
{
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3542_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5619_TI, 21},
	{ &t5620_TI, 28},
	{ &t5621_TI, 33},
	{ &t5622_TI, 34},
	{ &t5623_TI, 41},
	{ &t5624_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3542_0_0_0;
extern Il2CppType t3542_1_0_0;
struct t490;
extern TypeInfo t490_TI;
extern CustomAttributesCache t490__CustomAttributeCache;
TypeInfo t3542_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Attribute[]", "System", t3542_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t490_TI, t3542_ITIs, t3542_VT, &EmptyCustomAttributesCache, &t3542_TI, &t3542_0_0_0, &t3542_1_0_0, t3542_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t490 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3543_TI;



// Metadata Definition System.Runtime.InteropServices._Attribute[]
static MethodInfo* t3543_MIs[] =
{
	NULL
};
static MethodInfo* t3543_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
};
static TypeInfo* t3543_ITIs[] = 
{
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
};
static Il2CppInterfaceOffsetPair t3543_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5622_TI, 21},
	{ &t5623_TI, 28},
	{ &t5624_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3543_0_0_0;
extern Il2CppType t3543_1_0_0;
struct t604;
extern TypeInfo t604_TI;
extern CustomAttributesCache t604__CustomAttributeCache;
TypeInfo t3543_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Attribute[]", "System.Runtime.InteropServices", t3543_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t604_TI, t3543_ITIs, t3543_VT, &EmptyCustomAttributesCache, &t3543_TI, &t3543_0_0_0, &t3543_1_0_0, t3543_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3755_TI;



// Metadata Definition UnityEngine.SerializeField[]
static MethodInfo* t3755_MIs[] =
{
	NULL
};
extern MethodInfo m22345_MI;
extern MethodInfo m22346_MI;
extern MethodInfo m22347_MI;
extern MethodInfo m22348_MI;
extern MethodInfo m22349_MI;
extern MethodInfo m22350_MI;
extern MethodInfo m22344_MI;
extern MethodInfo m22352_MI;
extern MethodInfo m22353_MI;
static MethodInfo* t3755_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22345_MI,
	&m5907_MI,
	&m22346_MI,
	&m22347_MI,
	&m22348_MI,
	&m22349_MI,
	&m22350_MI,
	&m5908_MI,
	&m22344_MI,
	&m22352_MI,
	&m22353_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5625_TI;
extern TypeInfo t5626_TI;
extern TypeInfo t5627_TI;
static TypeInfo* t3755_ITIs[] = 
{
	&t5625_TI,
	&t5626_TI,
	&t5627_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3755_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5625_TI, 21},
	{ &t5626_TI, 28},
	{ &t5627_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3755_0_0_0;
extern Il2CppType t3755_1_0_0;
struct t300;
extern TypeInfo t300_TI;
TypeInfo t3755_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SerializeField[]", "UnityEngine", t3755_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t300_TI, t3755_ITIs, t3755_VT, &EmptyCustomAttributesCache, &t3755_TI, &t3755_0_0_0, &t3755_1_0_0, t3755_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t300 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3756_TI;



// Metadata Definition UnityEngine.Shader[]
static MethodInfo* t3756_MIs[] =
{
	NULL
};
extern MethodInfo m22356_MI;
extern MethodInfo m22357_MI;
extern MethodInfo m22358_MI;
extern MethodInfo m22359_MI;
extern MethodInfo m22360_MI;
extern MethodInfo m22361_MI;
extern MethodInfo m22355_MI;
extern MethodInfo m22363_MI;
extern MethodInfo m22364_MI;
extern MethodInfo m19546_MI;
extern MethodInfo m19547_MI;
extern MethodInfo m19548_MI;
extern MethodInfo m19549_MI;
extern MethodInfo m19550_MI;
extern MethodInfo m19551_MI;
extern MethodInfo m19545_MI;
extern MethodInfo m19553_MI;
extern MethodInfo m19554_MI;
static MethodInfo* t3756_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22356_MI,
	&m5907_MI,
	&m22357_MI,
	&m22358_MI,
	&m22359_MI,
	&m22360_MI,
	&m22361_MI,
	&m5908_MI,
	&m22355_MI,
	&m22363_MI,
	&m22364_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5628_TI;
extern TypeInfo t5629_TI;
extern TypeInfo t5630_TI;
extern TypeInfo t5065_TI;
extern TypeInfo t5066_TI;
extern TypeInfo t5067_TI;
static TypeInfo* t3756_ITIs[] = 
{
	&t5628_TI,
	&t5629_TI,
	&t5630_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3756_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5628_TI, 21},
	{ &t5629_TI, 28},
	{ &t5630_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3756_0_0_0;
extern Il2CppType t3756_1_0_0;
struct t491;
extern TypeInfo t491_TI;
extern CustomAttributesCache t491__CustomAttributeCache_m2443;
TypeInfo t3756_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Shader[]", "UnityEngine", t3756_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t491_TI, t3756_ITIs, t3756_VT, &EmptyCustomAttributesCache, &t3756_TI, &t3756_0_0_0, &t3756_1_0_0, t3756_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t491 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3757_TI;



// Metadata Definition UnityEngine.Material[]
static MethodInfo* t3757_MIs[] =
{
	NULL
};
extern MethodInfo m22368_MI;
extern MethodInfo m22369_MI;
extern MethodInfo m22370_MI;
extern MethodInfo m22371_MI;
extern MethodInfo m22372_MI;
extern MethodInfo m22373_MI;
extern MethodInfo m22367_MI;
extern MethodInfo m22375_MI;
extern MethodInfo m22376_MI;
static MethodInfo* t3757_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22368_MI,
	&m5907_MI,
	&m22369_MI,
	&m22370_MI,
	&m22371_MI,
	&m22372_MI,
	&m22373_MI,
	&m5908_MI,
	&m22367_MI,
	&m22375_MI,
	&m22376_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5631_TI;
extern TypeInfo t5632_TI;
extern TypeInfo t5633_TI;
static TypeInfo* t3757_ITIs[] = 
{
	&t5631_TI,
	&t5632_TI,
	&t5633_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3757_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5631_TI, 21},
	{ &t5632_TI, 28},
	{ &t5633_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3757_0_0_0;
extern Il2CppType t3757_1_0_0;
struct t156;
extern TypeInfo t156_TI;
extern CustomAttributesCache t156__CustomAttributeCache_m2445;
extern CustomAttributesCache t156__CustomAttributeCache_m2447;
extern CustomAttributesCache t156__CustomAttributeCache_m2448;
extern CustomAttributesCache t156__CustomAttributeCache_m2449;
TypeInfo t3757_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Material[]", "UnityEngine", t3757_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t156_TI, t3757_ITIs, t3757_VT, &EmptyCustomAttributesCache, &t3757_TI, &t3757_0_0_0, &t3757_1_0_0, t3757_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t156 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3758_TI;



// Metadata Definition UnityEngine.Sprite[]
static MethodInfo* t3758_MIs[] =
{
	NULL
};
extern MethodInfo m22380_MI;
extern MethodInfo m22381_MI;
extern MethodInfo m22382_MI;
extern MethodInfo m22383_MI;
extern MethodInfo m22384_MI;
extern MethodInfo m22385_MI;
extern MethodInfo m22379_MI;
extern MethodInfo m22387_MI;
extern MethodInfo m22388_MI;
static MethodInfo* t3758_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22380_MI,
	&m5907_MI,
	&m22381_MI,
	&m22382_MI,
	&m22383_MI,
	&m22384_MI,
	&m22385_MI,
	&m5908_MI,
	&m22379_MI,
	&m22387_MI,
	&m22388_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5634_TI;
extern TypeInfo t5635_TI;
extern TypeInfo t5636_TI;
static TypeInfo* t3758_ITIs[] = 
{
	&t5634_TI,
	&t5635_TI,
	&t5636_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3758_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5634_TI, 21},
	{ &t5635_TI, 28},
	{ &t5636_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3758_0_0_0;
extern Il2CppType t3758_1_0_0;
struct t180;
extern TypeInfo t180_TI;
extern CustomAttributesCache t180__CustomAttributeCache_m1652;
extern CustomAttributesCache t180__CustomAttributeCache_m1649;
extern CustomAttributesCache t180__CustomAttributeCache_m1646;
extern CustomAttributesCache t180__CustomAttributeCache_m1678;
extern CustomAttributesCache t180__CustomAttributeCache_m1647;
TypeInfo t3758_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Sprite[]", "UnityEngine", t3758_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t180_TI, t3758_ITIs, t3758_VT, &EmptyCustomAttributesCache, &t3758_TI, &t3758_0_0_0, &t3758_1_0_0, t3758_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t180 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3759_TI;



// Metadata Definition UnityEngine.SpriteRenderer[]
static MethodInfo* t3759_MIs[] =
{
	NULL
};
extern MethodInfo m22392_MI;
extern MethodInfo m22393_MI;
extern MethodInfo m22394_MI;
extern MethodInfo m22395_MI;
extern MethodInfo m22396_MI;
extern MethodInfo m22397_MI;
extern MethodInfo m22391_MI;
extern MethodInfo m22399_MI;
extern MethodInfo m22400_MI;
extern MethodInfo m22003_MI;
extern MethodInfo m22004_MI;
extern MethodInfo m22005_MI;
extern MethodInfo m22006_MI;
extern MethodInfo m22007_MI;
extern MethodInfo m22008_MI;
extern MethodInfo m22002_MI;
extern MethodInfo m22010_MI;
extern MethodInfo m22011_MI;
extern MethodInfo m19535_MI;
extern MethodInfo m19536_MI;
extern MethodInfo m19537_MI;
extern MethodInfo m19538_MI;
extern MethodInfo m19539_MI;
extern MethodInfo m19540_MI;
extern MethodInfo m19534_MI;
extern MethodInfo m19542_MI;
extern MethodInfo m19543_MI;
static MethodInfo* t3759_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22392_MI,
	&m5907_MI,
	&m22393_MI,
	&m22394_MI,
	&m22395_MI,
	&m22396_MI,
	&m22397_MI,
	&m5908_MI,
	&m22391_MI,
	&m22399_MI,
	&m22400_MI,
	&m5905_MI,
	&m5906_MI,
	&m22003_MI,
	&m5907_MI,
	&m22004_MI,
	&m22005_MI,
	&m22006_MI,
	&m22007_MI,
	&m22008_MI,
	&m5908_MI,
	&m22002_MI,
	&m22010_MI,
	&m22011_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5637_TI;
extern TypeInfo t5638_TI;
extern TypeInfo t5639_TI;
extern TypeInfo t5547_TI;
extern TypeInfo t5548_TI;
extern TypeInfo t5549_TI;
extern TypeInfo t2240_TI;
extern TypeInfo t2245_TI;
extern TypeInfo t2241_TI;
static TypeInfo* t3759_ITIs[] = 
{
	&t5637_TI,
	&t5638_TI,
	&t5639_TI,
	&t5547_TI,
	&t5548_TI,
	&t5549_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3759_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5637_TI, 21},
	{ &t5638_TI, 28},
	{ &t5639_TI, 33},
	{ &t5547_TI, 34},
	{ &t5548_TI, 41},
	{ &t5549_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3759_0_0_0;
extern Il2CppType t3759_1_0_0;
struct t331;
extern TypeInfo t331_TI;
TypeInfo t3759_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SpriteRenderer[]", "UnityEngine", t3759_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t331_TI, t3759_ITIs, t3759_VT, &EmptyCustomAttributesCache, &t3759_TI, &t3759_0_0_0, &t3759_1_0_0, t3759_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t331 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t497_TI;



// Metadata Definition UnityEngine.Camera[]
static MethodInfo* t497_MIs[] =
{
	NULL
};
extern MethodInfo m22405_MI;
extern MethodInfo m22406_MI;
extern MethodInfo m22407_MI;
extern MethodInfo m22408_MI;
extern MethodInfo m22409_MI;
extern MethodInfo m22410_MI;
extern MethodInfo m22404_MI;
extern MethodInfo m22412_MI;
extern MethodInfo m22413_MI;
extern MethodInfo m19524_MI;
extern MethodInfo m19525_MI;
extern MethodInfo m19526_MI;
extern MethodInfo m19527_MI;
extern MethodInfo m19528_MI;
extern MethodInfo m19529_MI;
extern MethodInfo m19523_MI;
extern MethodInfo m19531_MI;
extern MethodInfo m19532_MI;
static MethodInfo* t497_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22405_MI,
	&m5907_MI,
	&m22406_MI,
	&m22407_MI,
	&m22408_MI,
	&m22409_MI,
	&m22410_MI,
	&m5908_MI,
	&m22404_MI,
	&m22412_MI,
	&m22413_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5640_TI;
extern TypeInfo t5641_TI;
extern TypeInfo t5642_TI;
extern TypeInfo t5062_TI;
extern TypeInfo t5063_TI;
extern TypeInfo t5064_TI;
static TypeInfo* t497_ITIs[] = 
{
	&t5640_TI,
	&t5641_TI,
	&t5642_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t497_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5640_TI, 21},
	{ &t5641_TI, 28},
	{ &t5642_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t497_0_0_0;
extern Il2CppType t497_1_0_0;
struct t24;
extern TypeInfo t24_TI;
extern CustomAttributesCache t24__CustomAttributeCache_m1466;
extern CustomAttributesCache t24__CustomAttributeCache_m1465;
extern CustomAttributesCache t24__CustomAttributeCache_m1302;
extern CustomAttributesCache t24__CustomAttributeCache_m1480;
extern CustomAttributesCache t24__CustomAttributeCache_m2484;
extern CustomAttributesCache t24__CustomAttributeCache_m2485;
extern CustomAttributesCache t24__CustomAttributeCache_m2487;
extern CustomAttributesCache t24__CustomAttributeCache_m2488;
extern CustomAttributesCache t24__CustomAttributeCache_m2489;
extern CustomAttributesCache t24__CustomAttributeCache_m2490;
extern CustomAttributesCache t24__CustomAttributeCache_m2491;
extern CustomAttributesCache t24__CustomAttributeCache_m38;
extern CustomAttributesCache t24__CustomAttributeCache_m2492;
extern CustomAttributesCache t24__CustomAttributeCache_m2493;
extern CustomAttributesCache t24__CustomAttributeCache_m2498;
extern CustomAttributesCache t24__CustomAttributeCache_m2500;
TypeInfo t497_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Camera[]", "UnityEngine", t497_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t24_TI, t497_ITIs, t497_VT, &EmptyCustomAttributesCache, &t497_TI, &t497_0_0_0, &t497_1_0_0, t497_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t24 *), -1, sizeof(t497_SFs), 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t501_TI;



// Metadata Definition UnityEngine.Display[]
static MethodInfo* t501_MIs[] =
{
	NULL
};
extern MethodInfo m22417_MI;
extern MethodInfo m22418_MI;
extern MethodInfo m22419_MI;
extern MethodInfo m22420_MI;
extern MethodInfo m22421_MI;
extern MethodInfo m22422_MI;
extern MethodInfo m22416_MI;
extern MethodInfo m22424_MI;
extern MethodInfo m22425_MI;
static MethodInfo* t501_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22417_MI,
	&m5907_MI,
	&m22418_MI,
	&m22419_MI,
	&m22420_MI,
	&m22421_MI,
	&m22422_MI,
	&m5908_MI,
	&m22416_MI,
	&m22424_MI,
	&m22425_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5643_TI;
extern TypeInfo t5644_TI;
extern TypeInfo t5645_TI;
static TypeInfo* t501_ITIs[] = 
{
	&t5643_TI,
	&t5644_TI,
	&t5645_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t501_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5643_TI, 21},
	{ &t5644_TI, 28},
	{ &t5645_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t501_0_0_0;
extern Il2CppType t501_1_0_0;
struct t500;
extern TypeInfo t500_TI;
extern CustomAttributesCache t500__CustomAttributeCache_m2530;
extern CustomAttributesCache t500__CustomAttributeCache_m2531;
extern CustomAttributesCache t500__CustomAttributeCache_m2532;
extern CustomAttributesCache t500__CustomAttributeCache_m2533;
extern CustomAttributesCache t500__CustomAttributeCache_m2534;
extern CustomAttributesCache t500__CustomAttributeCache_m2535;
extern CustomAttributesCache t500__CustomAttributeCache_m2536;
extern CustomAttributesCache t500__CustomAttributeCache_m2537;
TypeInfo t501_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Display[]", "UnityEngine", t501_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t500_TI, t501_ITIs, t501_VT, &EmptyCustomAttributesCache, &t501_TI, &t501_0_0_0, &t501_1_0_0, t501_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t500 *), -1, sizeof(t501_SFs), 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t502_TI;



// Metadata Definition System.IntPtr[]
static MethodInfo* t502_MIs[] =
{
	NULL
};
extern MethodInfo m22428_MI;
extern MethodInfo m22429_MI;
extern MethodInfo m22430_MI;
extern MethodInfo m22431_MI;
extern MethodInfo m22432_MI;
extern MethodInfo m22433_MI;
extern MethodInfo m22427_MI;
extern MethodInfo m22435_MI;
extern MethodInfo m22436_MI;
extern MethodInfo m22439_MI;
extern MethodInfo m22440_MI;
extern MethodInfo m22441_MI;
extern MethodInfo m22442_MI;
extern MethodInfo m22443_MI;
extern MethodInfo m22444_MI;
extern MethodInfo m22438_MI;
extern MethodInfo m22446_MI;
extern MethodInfo m22447_MI;
static MethodInfo* t502_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22428_MI,
	&m5907_MI,
	&m22429_MI,
	&m22430_MI,
	&m22431_MI,
	&m22432_MI,
	&m22433_MI,
	&m5908_MI,
	&m22427_MI,
	&m22435_MI,
	&m22436_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5646_TI;
extern TypeInfo t5647_TI;
extern TypeInfo t5648_TI;
extern TypeInfo t5649_TI;
extern TypeInfo t5650_TI;
extern TypeInfo t5651_TI;
static TypeInfo* t502_ITIs[] = 
{
	&t5646_TI,
	&t5647_TI,
	&t5648_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t502_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5646_TI, 21},
	{ &t5647_TI, 28},
	{ &t5648_TI, 33},
	{ &t5649_TI, 34},
	{ &t5650_TI, 41},
	{ &t5651_TI, 46},
	{ &t5116_TI, 47},
	{ &t5117_TI, 54},
	{ &t5118_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t502_0_0_0;
extern Il2CppType t502_1_0_0;
#include "t35.h"
extern TypeInfo t35_TI;
extern CustomAttributesCache t35__CustomAttributeCache;
extern CustomAttributesCache t35__CustomAttributeCache_m2887;
extern CustomAttributesCache t35__CustomAttributeCache_m5807;
extern CustomAttributesCache t35__CustomAttributeCache_m5808;
extern CustomAttributesCache t35__CustomAttributeCache_m5811;
extern CustomAttributesCache t35__CustomAttributeCache_m5814;
extern CustomAttributesCache t35__CustomAttributeCache_m2949;
extern CustomAttributesCache t35__CustomAttributeCache_m2888;
extern CustomAttributesCache t35__CustomAttributeCache_m5817;
extern CustomAttributesCache t35__CustomAttributeCache_m5818;
extern CustomAttributesCache t35__CustomAttributeCache_m5819;
extern CustomAttributesCache t35__CustomAttributeCache_m5820;
TypeInfo t502_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IntPtr[]", "System", t502_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t35_TI, t502_ITIs, t502_VT, &EmptyCustomAttributesCache, &t502_TI, &t502_0_0_0, &t502_1_0_0, t502_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t35), -1, sizeof(t502_SFs), 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3544_TI;



// Metadata Definition System.Runtime.Serialization.ISerializable[]
static MethodInfo* t3544_MIs[] =
{
	NULL
};
static MethodInfo* t3544_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
};
static TypeInfo* t3544_ITIs[] = 
{
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
};
static Il2CppInterfaceOffsetPair t3544_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5649_TI, 21},
	{ &t5650_TI, 28},
	{ &t5651_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3544_0_0_0;
extern Il2CppType t3544_1_0_0;
struct t374;
extern TypeInfo t374_TI;
extern CustomAttributesCache t374__CustomAttributeCache;
TypeInfo t3544_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ISerializable[]", "System.Runtime.Serialization", t3544_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t374_TI, t3544_ITIs, t3544_VT, &EmptyCustomAttributesCache, &t3544_TI, &t3544_0_0_0, &t3544_1_0_0, t3544_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3760_TI;



// Metadata Definition UnityEngine.TouchPhase[]
static MethodInfo* t3760_MIs[] =
{
	NULL
};
extern MethodInfo m22451_MI;
extern MethodInfo m22452_MI;
extern MethodInfo m22453_MI;
extern MethodInfo m22454_MI;
extern MethodInfo m22455_MI;
extern MethodInfo m22456_MI;
extern MethodInfo m22450_MI;
extern MethodInfo m22458_MI;
extern MethodInfo m22459_MI;
static MethodInfo* t3760_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22451_MI,
	&m5907_MI,
	&m22452_MI,
	&m22453_MI,
	&m22454_MI,
	&m22455_MI,
	&m22456_MI,
	&m5908_MI,
	&m22450_MI,
	&m22458_MI,
	&m22459_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5652_TI;
extern TypeInfo t5653_TI;
extern TypeInfo t5654_TI;
static TypeInfo* t3760_ITIs[] = 
{
	&t5652_TI,
	&t5653_TI,
	&t5654_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3760_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5652_TI, 21},
	{ &t5653_TI, 28},
	{ &t5654_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3760_0_0_0;
extern Il2CppType t3760_1_0_0;
#include "t319.h"
extern TypeInfo t319_TI;
TypeInfo t3760_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TouchPhase[]", "UnityEngine", t3760_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t319_TI, t3760_ITIs, t3760_VT, &EmptyCustomAttributesCache, &t44_TI, &t3760_0_0_0, &t3760_1_0_0, t3760_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3761_TI;



// Metadata Definition UnityEngine.IMECompositionMode[]
static MethodInfo* t3761_MIs[] =
{
	NULL
};
extern MethodInfo m22462_MI;
extern MethodInfo m22463_MI;
extern MethodInfo m22464_MI;
extern MethodInfo m22465_MI;
extern MethodInfo m22466_MI;
extern MethodInfo m22467_MI;
extern MethodInfo m22461_MI;
extern MethodInfo m22469_MI;
extern MethodInfo m22470_MI;
static MethodInfo* t3761_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22462_MI,
	&m5907_MI,
	&m22463_MI,
	&m22464_MI,
	&m22465_MI,
	&m22466_MI,
	&m22467_MI,
	&m5908_MI,
	&m22461_MI,
	&m22469_MI,
	&m22470_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5655_TI;
extern TypeInfo t5656_TI;
extern TypeInfo t5657_TI;
static TypeInfo* t3761_ITIs[] = 
{
	&t5655_TI,
	&t5656_TI,
	&t5657_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3761_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5655_TI, 21},
	{ &t5656_TI, 28},
	{ &t5657_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3761_0_0_0;
extern Il2CppType t3761_1_0_0;
#include "t386.h"
extern TypeInfo t386_TI;
TypeInfo t3761_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "IMECompositionMode[]", "UnityEngine", t3761_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t386_TI, t3761_ITIs, t3761_VT, &EmptyCustomAttributesCache, &t44_TI, &t3761_0_0_0, &t3761_1_0_0, t3761_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3762_TI;



// Metadata Definition UnityEngine.HideFlags[]
static MethodInfo* t3762_MIs[] =
{
	NULL
};
extern MethodInfo m22473_MI;
extern MethodInfo m22474_MI;
extern MethodInfo m22475_MI;
extern MethodInfo m22476_MI;
extern MethodInfo m22477_MI;
extern MethodInfo m22478_MI;
extern MethodInfo m22472_MI;
extern MethodInfo m22480_MI;
extern MethodInfo m22481_MI;
static MethodInfo* t3762_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22473_MI,
	&m5907_MI,
	&m22474_MI,
	&m22475_MI,
	&m22476_MI,
	&m22477_MI,
	&m22478_MI,
	&m5908_MI,
	&m22472_MI,
	&m22480_MI,
	&m22481_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5658_TI;
extern TypeInfo t5659_TI;
extern TypeInfo t5660_TI;
static TypeInfo* t3762_ITIs[] = 
{
	&t5658_TI,
	&t5659_TI,
	&t5660_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3762_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5658_TI, 21},
	{ &t5659_TI, 28},
	{ &t5660_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3762_0_0_0;
extern Il2CppType t3762_1_0_0;
#include "t385.h"
extern TypeInfo t385_TI;
extern CustomAttributesCache t385__CustomAttributeCache;
TypeInfo t3762_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HideFlags[]", "UnityEngine", t3762_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t385_TI, t3762_ITIs, t3762_VT, &EmptyCustomAttributesCache, &t44_TI, &t3762_0_0_0, &t3762_1_0_0, t3762_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3763_TI;



// Metadata Definition UnityEngine.GameObject[]
static MethodInfo* t3763_MIs[] =
{
	NULL
};
extern MethodInfo m22486_MI;
extern MethodInfo m22487_MI;
extern MethodInfo m22488_MI;
extern MethodInfo m22489_MI;
extern MethodInfo m22490_MI;
extern MethodInfo m22491_MI;
extern MethodInfo m22485_MI;
extern MethodInfo m22493_MI;
extern MethodInfo m22494_MI;
static MethodInfo* t3763_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22486_MI,
	&m5907_MI,
	&m22487_MI,
	&m22488_MI,
	&m22489_MI,
	&m22490_MI,
	&m22491_MI,
	&m5908_MI,
	&m22485_MI,
	&m22493_MI,
	&m22494_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5661_TI;
extern TypeInfo t5662_TI;
extern TypeInfo t5663_TI;
static TypeInfo* t3763_ITIs[] = 
{
	&t5661_TI,
	&t5662_TI,
	&t5663_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3763_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5661_TI, 21},
	{ &t5662_TI, 28},
	{ &t5663_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3763_0_0_0;
extern Il2CppType t3763_1_0_0;
struct t16;
extern TypeInfo t16_TI;
extern CustomAttributesCache t16__CustomAttributeCache_m2553;
extern CustomAttributesCache t16__CustomAttributeCache_m2896;
extern CustomAttributesCache t16__CustomAttributeCache_m2554;
extern CustomAttributesCache t16__CustomAttributeCache_m1363;
extern CustomAttributesCache t16__CustomAttributeCache_m1781;
extern CustomAttributesCache t16__CustomAttributeCache_m1782;
extern CustomAttributesCache t16__CustomAttributeCache_m1393;
extern CustomAttributesCache t16__CustomAttributeCache_m2555;
extern CustomAttributesCache t16__CustomAttributeCache_m2556;
extern CustomAttributesCache t16__CustomAttributeCache_m2557;
extern CustomAttributesCache t16__CustomAttributeCache_m2558;
TypeInfo t3763_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GameObject[]", "UnityEngine", t3763_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t16_TI, t3763_ITIs, t3763_VT, &EmptyCustomAttributesCache, &t3763_TI, &t3763_0_0_0, &t3763_1_0_0, t3763_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t16 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3764_TI;



// Metadata Definition UnityEngine.Collider[]
static MethodInfo* t3764_MIs[] =
{
	NULL
};
extern MethodInfo m22499_MI;
extern MethodInfo m22500_MI;
extern MethodInfo m22501_MI;
extern MethodInfo m22502_MI;
extern MethodInfo m22503_MI;
extern MethodInfo m22504_MI;
extern MethodInfo m22498_MI;
extern MethodInfo m22506_MI;
extern MethodInfo m22507_MI;
static MethodInfo* t3764_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22499_MI,
	&m5907_MI,
	&m22500_MI,
	&m22501_MI,
	&m22502_MI,
	&m22503_MI,
	&m22504_MI,
	&m5908_MI,
	&m22498_MI,
	&m22506_MI,
	&m22507_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5664_TI;
extern TypeInfo t5665_TI;
extern TypeInfo t5666_TI;
static TypeInfo* t3764_ITIs[] = 
{
	&t5664_TI,
	&t5665_TI,
	&t5666_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3764_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5664_TI, 21},
	{ &t5665_TI, 28},
	{ &t5666_TI, 33},
	{ &t2240_TI, 34},
	{ &t2245_TI, 41},
	{ &t2241_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3764_0_0_0;
extern Il2CppType t3764_1_0_0;
struct t336;
extern TypeInfo t336_TI;
TypeInfo t3764_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Collider[]", "UnityEngine", t3764_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t336_TI, t3764_ITIs, t3764_VT, &EmptyCustomAttributesCache, &t3764_TI, &t3764_0_0_0, &t3764_1_0_0, t3764_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t336 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2980_TI;



// Metadata Definition UnityEngine.Rigidbody2D[]
static MethodInfo* t2980_MIs[] =
{
	NULL
};
extern MethodInfo m22511_MI;
extern MethodInfo m22512_MI;
extern MethodInfo m22513_MI;
extern MethodInfo m22514_MI;
extern MethodInfo m22515_MI;
extern MethodInfo m22516_MI;
extern MethodInfo m22510_MI;
extern MethodInfo m22518_MI;
extern MethodInfo m22519_MI;
static MethodInfo* t2980_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22511_MI,
	&m5907_MI,
	&m22512_MI,
	&m22513_MI,
	&m22514_MI,
	&m22515_MI,
	&m22516_MI,
	&m5908_MI,
	&m22510_MI,
	&m22518_MI,
	&m22519_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2982_TI;
extern TypeInfo t2989_TI;
extern TypeInfo t2983_TI;
static TypeInfo* t2980_ITIs[] = 
{
	&t2982_TI,
	&t2989_TI,
	&t2983_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2980_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2982_TI, 21},
	{ &t2989_TI, 28},
	{ &t2983_TI, 33},
	{ &t2240_TI, 34},
	{ &t2245_TI, 41},
	{ &t2241_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2980_0_0_0;
extern Il2CppType t2980_1_0_0;
struct t507;
extern TypeInfo t507_TI;
TypeInfo t2980_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Rigidbody2D[]", "UnityEngine", t2980_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t507_TI, t2980_ITIs, t2980_VT, &EmptyCustomAttributesCache, &t2980_TI, &t2980_0_0_0, &t2980_1_0_0, t2980_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t507 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3765_TI;



// Metadata Definition UnityEngine.Collider2D[]
static MethodInfo* t3765_MIs[] =
{
	NULL
};
extern MethodInfo m22538_MI;
extern MethodInfo m22539_MI;
extern MethodInfo m22540_MI;
extern MethodInfo m22541_MI;
extern MethodInfo m22542_MI;
extern MethodInfo m22543_MI;
extern MethodInfo m22537_MI;
extern MethodInfo m22545_MI;
extern MethodInfo m22546_MI;
static MethodInfo* t3765_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22538_MI,
	&m5907_MI,
	&m22539_MI,
	&m22540_MI,
	&m22541_MI,
	&m22542_MI,
	&m22543_MI,
	&m5908_MI,
	&m22537_MI,
	&m22545_MI,
	&m22546_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5667_TI;
extern TypeInfo t5668_TI;
extern TypeInfo t5669_TI;
static TypeInfo* t3765_ITIs[] = 
{
	&t5667_TI,
	&t5668_TI,
	&t5669_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3765_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5667_TI, 21},
	{ &t5668_TI, 28},
	{ &t5669_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3765_0_0_0;
extern Il2CppType t3765_1_0_0;
struct t332;
extern TypeInfo t332_TI;
extern CustomAttributesCache t332__CustomAttributeCache_m2607;
TypeInfo t3765_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Collider2D[]", "UnityEngine", t3765_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t332_TI, t3765_ITIs, t3765_VT, &EmptyCustomAttributesCache, &t3765_TI, &t3765_0_0_0, &t3765_1_0_0, t3765_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t332 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3766_TI;



// Metadata Definition UnityEngine.AudioClip[]
static MethodInfo* t3766_MIs[] =
{
	NULL
};
extern MethodInfo m22550_MI;
extern MethodInfo m22551_MI;
extern MethodInfo m22552_MI;
extern MethodInfo m22553_MI;
extern MethodInfo m22554_MI;
extern MethodInfo m22555_MI;
extern MethodInfo m22549_MI;
extern MethodInfo m22557_MI;
extern MethodInfo m22558_MI;
static MethodInfo* t3766_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22550_MI,
	&m5907_MI,
	&m22551_MI,
	&m22552_MI,
	&m22553_MI,
	&m22554_MI,
	&m22555_MI,
	&m5908_MI,
	&m22549_MI,
	&m22557_MI,
	&m22558_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5670_TI;
extern TypeInfo t5671_TI;
extern TypeInfo t5672_TI;
static TypeInfo* t3766_ITIs[] = 
{
	&t5670_TI,
	&t5671_TI,
	&t5672_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3766_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5670_TI, 21},
	{ &t5671_TI, 28},
	{ &t5672_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3766_0_0_0;
extern Il2CppType t3766_1_0_0;
struct t511;
extern TypeInfo t511_TI;
TypeInfo t3766_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AudioClip[]", "UnityEngine", t3766_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t511_TI, t3766_ITIs, t3766_VT, &EmptyCustomAttributesCache, &t3766_TI, &t3766_0_0_0, &t3766_1_0_0, t3766_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t511 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t509_TI;



// Metadata Definition System.Single[]
static MethodInfo* t509_MIs[] =
{
	NULL
};
extern MethodInfo m22562_MI;
extern MethodInfo m22563_MI;
extern MethodInfo m22564_MI;
extern MethodInfo m22565_MI;
extern MethodInfo m22566_MI;
extern MethodInfo m22567_MI;
extern MethodInfo m22561_MI;
extern MethodInfo m22569_MI;
extern MethodInfo m22570_MI;
extern MethodInfo m22573_MI;
extern MethodInfo m22574_MI;
extern MethodInfo m22575_MI;
extern MethodInfo m22576_MI;
extern MethodInfo m22577_MI;
extern MethodInfo m22578_MI;
extern MethodInfo m22572_MI;
extern MethodInfo m22580_MI;
extern MethodInfo m22581_MI;
extern MethodInfo m22584_MI;
extern MethodInfo m22585_MI;
extern MethodInfo m22586_MI;
extern MethodInfo m22587_MI;
extern MethodInfo m22588_MI;
extern MethodInfo m22589_MI;
extern MethodInfo m22583_MI;
extern MethodInfo m22591_MI;
extern MethodInfo m22592_MI;
static MethodInfo* t509_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22562_MI,
	&m5907_MI,
	&m22563_MI,
	&m22564_MI,
	&m22565_MI,
	&m22566_MI,
	&m22567_MI,
	&m5908_MI,
	&m22561_MI,
	&m22569_MI,
	&m22570_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m22573_MI,
	&m5907_MI,
	&m22574_MI,
	&m22575_MI,
	&m22576_MI,
	&m22577_MI,
	&m22578_MI,
	&m5908_MI,
	&m22572_MI,
	&m22580_MI,
	&m22581_MI,
	&m5905_MI,
	&m5906_MI,
	&m22584_MI,
	&m5907_MI,
	&m22585_MI,
	&m22586_MI,
	&m22587_MI,
	&m22588_MI,
	&m22589_MI,
	&m5908_MI,
	&m22583_MI,
	&m22591_MI,
	&m22592_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5673_TI;
extern TypeInfo t5674_TI;
extern TypeInfo t5675_TI;
extern TypeInfo t5676_TI;
extern TypeInfo t5677_TI;
extern TypeInfo t5678_TI;
extern TypeInfo t5679_TI;
extern TypeInfo t5680_TI;
extern TypeInfo t5681_TI;
static TypeInfo* t509_ITIs[] = 
{
	&t5673_TI,
	&t5674_TI,
	&t5675_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5676_TI,
	&t5677_TI,
	&t5678_TI,
	&t5679_TI,
	&t5680_TI,
	&t5681_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t509_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5673_TI, 21},
	{ &t5674_TI, 28},
	{ &t5675_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5676_TI, 73},
	{ &t5677_TI, 80},
	{ &t5678_TI, 85},
	{ &t5679_TI, 86},
	{ &t5680_TI, 93},
	{ &t5681_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t509_0_0_0;
extern Il2CppType t509_1_0_0;
#include "t22.h"
extern TypeInfo t22_TI;
extern CustomAttributesCache t22__CustomAttributeCache;
extern CustomAttributesCache t22__CustomAttributeCache_m5657;
TypeInfo t509_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Single[]", "System", t509_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t22_TI, t509_ITIs, t509_VT, &EmptyCustomAttributesCache, &t509_TI, &t509_0_0_0, &t509_1_0_0, t509_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (float), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3545_TI;



// Metadata Definition System.IComparable`1<System.Single>[]
static MethodInfo* t3545_MIs[] =
{
	NULL
};
static MethodInfo* t3545_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22573_MI,
	&m5907_MI,
	&m22574_MI,
	&m22575_MI,
	&m22576_MI,
	&m22577_MI,
	&m22578_MI,
	&m5908_MI,
	&m22572_MI,
	&m22580_MI,
	&m22581_MI,
};
static TypeInfo* t3545_ITIs[] = 
{
	&t5676_TI,
	&t5677_TI,
	&t5678_TI,
};
static Il2CppInterfaceOffsetPair t3545_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5676_TI, 21},
	{ &t5677_TI, 28},
	{ &t5678_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3545_0_0_0;
extern Il2CppType t3545_1_0_0;
struct t1751;
extern TypeInfo t1751_TI;
TypeInfo t3545_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3545_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1751_TI, t3545_ITIs, t3545_VT, &EmptyCustomAttributesCache, &t3545_TI, &t3545_0_0_0, &t3545_1_0_0, t3545_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3546_TI;



// Metadata Definition System.IEquatable`1<System.Single>[]
static MethodInfo* t3546_MIs[] =
{
	NULL
};
static MethodInfo* t3546_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22584_MI,
	&m5907_MI,
	&m22585_MI,
	&m22586_MI,
	&m22587_MI,
	&m22588_MI,
	&m22589_MI,
	&m5908_MI,
	&m22583_MI,
	&m22591_MI,
	&m22592_MI,
};
static TypeInfo* t3546_ITIs[] = 
{
	&t5679_TI,
	&t5680_TI,
	&t5681_TI,
};
static Il2CppInterfaceOffsetPair t3546_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5679_TI, 21},
	{ &t5680_TI, 28},
	{ &t5681_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3546_0_0_0;
extern Il2CppType t3546_1_0_0;
struct t1752;
extern TypeInfo t1752_TI;
TypeInfo t3546_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3546_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1752_TI, t3546_ITIs, t3546_VT, &EmptyCustomAttributesCache, &t3546_TI, &t3546_0_0_0, &t3546_1_0_0, t3546_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3767_TI;



// Metadata Definition UnityEngine.AnimationEventSource[]
static MethodInfo* t3767_MIs[] =
{
	NULL
};
extern MethodInfo m22595_MI;
extern MethodInfo m22596_MI;
extern MethodInfo m22597_MI;
extern MethodInfo m22598_MI;
extern MethodInfo m22599_MI;
extern MethodInfo m22600_MI;
extern MethodInfo m22594_MI;
extern MethodInfo m22602_MI;
extern MethodInfo m22603_MI;
static MethodInfo* t3767_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22595_MI,
	&m5907_MI,
	&m22596_MI,
	&m22597_MI,
	&m22598_MI,
	&m22599_MI,
	&m22600_MI,
	&m5908_MI,
	&m22594_MI,
	&m22602_MI,
	&m22603_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5682_TI;
extern TypeInfo t5683_TI;
extern TypeInfo t5684_TI;
static TypeInfo* t3767_ITIs[] = 
{
	&t5682_TI,
	&t5683_TI,
	&t5684_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3767_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5682_TI, 21},
	{ &t5683_TI, 28},
	{ &t5684_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3767_0_0_0;
extern Il2CppType t3767_1_0_0;
#include "t512.h"
extern TypeInfo t512_TI;
TypeInfo t3767_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AnimationEventSource[]", "UnityEngine", t3767_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t512_TI, t3767_ITIs, t3767_VT, &EmptyCustomAttributesCache, &t44_TI, &t3767_0_0_0, &t3767_1_0_0, t3767_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t519_TI;



// Metadata Definition UnityEngine.Keyframe[]
static MethodInfo* t519_MIs[] =
{
	NULL
};
extern MethodInfo m22606_MI;
extern MethodInfo m22607_MI;
extern MethodInfo m22608_MI;
extern MethodInfo m22609_MI;
extern MethodInfo m22610_MI;
extern MethodInfo m22611_MI;
extern MethodInfo m22605_MI;
extern MethodInfo m22613_MI;
extern MethodInfo m22614_MI;
static MethodInfo* t519_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22606_MI,
	&m5907_MI,
	&m22607_MI,
	&m22608_MI,
	&m22609_MI,
	&m22610_MI,
	&m22611_MI,
	&m5908_MI,
	&m22605_MI,
	&m22613_MI,
	&m22614_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5685_TI;
extern TypeInfo t5686_TI;
extern TypeInfo t5687_TI;
static TypeInfo* t519_ITIs[] = 
{
	&t5685_TI,
	&t5686_TI,
	&t5687_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t519_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5685_TI, 21},
	{ &t5686_TI, 28},
	{ &t5687_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t519_0_0_0;
extern Il2CppType t519_1_0_0;
#include "t517.h"
extern TypeInfo t517_TI;
TypeInfo t519_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Keyframe[]", "UnityEngine", t519_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t517_TI, t519_ITIs, t519_VT, &EmptyCustomAttributesCache, &t519_TI, &t519_0_0_0, &t519_1_0_0, t519_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t517 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3768_TI;



// Metadata Definition UnityEngine.Animator[]
static MethodInfo* t3768_MIs[] =
{
	NULL
};
extern MethodInfo m22617_MI;
extern MethodInfo m22618_MI;
extern MethodInfo m22619_MI;
extern MethodInfo m22620_MI;
extern MethodInfo m22621_MI;
extern MethodInfo m22622_MI;
extern MethodInfo m22616_MI;
extern MethodInfo m22624_MI;
extern MethodInfo m22625_MI;
static MethodInfo* t3768_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22617_MI,
	&m5907_MI,
	&m22618_MI,
	&m22619_MI,
	&m22620_MI,
	&m22621_MI,
	&m22622_MI,
	&m5908_MI,
	&m22616_MI,
	&m22624_MI,
	&m22625_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5688_TI;
extern TypeInfo t5689_TI;
extern TypeInfo t5690_TI;
static TypeInfo* t3768_ITIs[] = 
{
	&t5688_TI,
	&t5689_TI,
	&t5690_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3768_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5688_TI, 21},
	{ &t5689_TI, 28},
	{ &t5690_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3768_0_0_0;
extern Il2CppType t3768_1_0_0;
struct t229;
extern TypeInfo t229_TI;
extern CustomAttributesCache t229__CustomAttributeCache_m1894;
extern CustomAttributesCache t229__CustomAttributeCache_m2663;
extern CustomAttributesCache t229__CustomAttributeCache_m2664;
extern CustomAttributesCache t229__CustomAttributeCache_m2665;
TypeInfo t3768_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Animator[]", "UnityEngine", t3768_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t229_TI, t3768_ITIs, t3768_VT, &EmptyCustomAttributesCache, &t3768_TI, &t3768_0_0_0, &t3768_1_0_0, t3768_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t229 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3769_TI;



// Metadata Definition UnityEngine.RuntimeAnimatorController[]
static MethodInfo* t3769_MIs[] =
{
	NULL
};
extern MethodInfo m22629_MI;
extern MethodInfo m22630_MI;
extern MethodInfo m22631_MI;
extern MethodInfo m22632_MI;
extern MethodInfo m22633_MI;
extern MethodInfo m22634_MI;
extern MethodInfo m22628_MI;
extern MethodInfo m22636_MI;
extern MethodInfo m22637_MI;
static MethodInfo* t3769_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22629_MI,
	&m5907_MI,
	&m22630_MI,
	&m22631_MI,
	&m22632_MI,
	&m22633_MI,
	&m22634_MI,
	&m5908_MI,
	&m22628_MI,
	&m22636_MI,
	&m22637_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5691_TI;
extern TypeInfo t5692_TI;
extern TypeInfo t5693_TI;
static TypeInfo* t3769_ITIs[] = 
{
	&t5691_TI,
	&t5692_TI,
	&t5693_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3769_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5691_TI, 21},
	{ &t5692_TI, 28},
	{ &t5693_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3769_0_0_0;
extern Il2CppType t3769_1_0_0;
struct t398;
extern TypeInfo t398_TI;
TypeInfo t3769_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RuntimeAnimatorController[]", "UnityEngine", t3769_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t398_TI, t3769_ITIs, t3769_VT, &EmptyCustomAttributesCache, &t3769_TI, &t3769_0_0_0, &t3769_1_0_0, t3769_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t398 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3770_TI;



// Metadata Definition UnityEngine.Terrain[]
static MethodInfo* t3770_MIs[] =
{
	NULL
};
extern MethodInfo m22641_MI;
extern MethodInfo m22642_MI;
extern MethodInfo m22643_MI;
extern MethodInfo m22644_MI;
extern MethodInfo m22645_MI;
extern MethodInfo m22646_MI;
extern MethodInfo m22640_MI;
extern MethodInfo m22648_MI;
extern MethodInfo m22649_MI;
static MethodInfo* t3770_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22641_MI,
	&m5907_MI,
	&m22642_MI,
	&m22643_MI,
	&m22644_MI,
	&m22645_MI,
	&m22646_MI,
	&m5908_MI,
	&m22640_MI,
	&m22648_MI,
	&m22649_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5694_TI;
extern TypeInfo t5695_TI;
extern TypeInfo t5696_TI;
static TypeInfo* t3770_ITIs[] = 
{
	&t5694_TI,
	&t5695_TI,
	&t5696_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3770_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5694_TI, 21},
	{ &t5695_TI, 28},
	{ &t5696_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3770_0_0_0;
extern Il2CppType t3770_1_0_0;
struct t525;
extern TypeInfo t525_TI;
TypeInfo t3770_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Terrain[]", "UnityEngine", t3770_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t525_TI, t3770_ITIs, t3770_VT, &EmptyCustomAttributesCache, &t3770_TI, &t3770_0_0_0, &t3770_1_0_0, t3770_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t525 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3771_TI;



// Metadata Definition UnityEngine.TextAnchor[]
static MethodInfo* t3771_MIs[] =
{
	NULL
};
extern MethodInfo m22653_MI;
extern MethodInfo m22654_MI;
extern MethodInfo m22655_MI;
extern MethodInfo m22656_MI;
extern MethodInfo m22657_MI;
extern MethodInfo m22658_MI;
extern MethodInfo m22652_MI;
extern MethodInfo m22660_MI;
extern MethodInfo m22661_MI;
static MethodInfo* t3771_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22653_MI,
	&m5907_MI,
	&m22654_MI,
	&m22655_MI,
	&m22656_MI,
	&m22657_MI,
	&m22658_MI,
	&m5908_MI,
	&m22652_MI,
	&m22660_MI,
	&m22661_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5697_TI;
extern TypeInfo t5698_TI;
extern TypeInfo t5699_TI;
static TypeInfo* t3771_ITIs[] = 
{
	&t5697_TI,
	&t5698_TI,
	&t5699_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3771_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5697_TI, 21},
	{ &t5698_TI, 28},
	{ &t5699_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3771_0_0_0;
extern Il2CppType t3771_1_0_0;
#include "t150.h"
extern TypeInfo t150_TI;
TypeInfo t3771_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextAnchor[]", "UnityEngine", t3771_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t150_TI, t3771_ITIs, t3771_VT, &EmptyCustomAttributesCache, &t44_TI, &t3771_0_0_0, &t3771_1_0_0, t3771_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3772_TI;



// Metadata Definition UnityEngine.HorizontalWrapMode[]
static MethodInfo* t3772_MIs[] =
{
	NULL
};
extern MethodInfo m22664_MI;
extern MethodInfo m22665_MI;
extern MethodInfo m22666_MI;
extern MethodInfo m22667_MI;
extern MethodInfo m22668_MI;
extern MethodInfo m22669_MI;
extern MethodInfo m22663_MI;
extern MethodInfo m22671_MI;
extern MethodInfo m22672_MI;
static MethodInfo* t3772_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22664_MI,
	&m5907_MI,
	&m22665_MI,
	&m22666_MI,
	&m22667_MI,
	&m22668_MI,
	&m22669_MI,
	&m5908_MI,
	&m22663_MI,
	&m22671_MI,
	&m22672_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5700_TI;
extern TypeInfo t5701_TI;
extern TypeInfo t5702_TI;
static TypeInfo* t3772_ITIs[] = 
{
	&t5700_TI,
	&t5701_TI,
	&t5702_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3772_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5700_TI, 21},
	{ &t5701_TI, 28},
	{ &t5702_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3772_0_0_0;
extern Il2CppType t3772_1_0_0;
#include "t151.h"
extern TypeInfo t151_TI;
TypeInfo t3772_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HorizontalWrapMode[]", "UnityEngine", t3772_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t151_TI, t3772_ITIs, t3772_VT, &EmptyCustomAttributesCache, &t44_TI, &t3772_0_0_0, &t3772_1_0_0, t3772_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3773_TI;



// Metadata Definition UnityEngine.VerticalWrapMode[]
static MethodInfo* t3773_MIs[] =
{
	NULL
};
extern MethodInfo m22675_MI;
extern MethodInfo m22676_MI;
extern MethodInfo m22677_MI;
extern MethodInfo m22678_MI;
extern MethodInfo m22679_MI;
extern MethodInfo m22680_MI;
extern MethodInfo m22674_MI;
extern MethodInfo m22682_MI;
extern MethodInfo m22683_MI;
static MethodInfo* t3773_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22675_MI,
	&m5907_MI,
	&m22676_MI,
	&m22677_MI,
	&m22678_MI,
	&m22679_MI,
	&m22680_MI,
	&m5908_MI,
	&m22674_MI,
	&m22682_MI,
	&m22683_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5703_TI;
extern TypeInfo t5704_TI;
extern TypeInfo t5705_TI;
static TypeInfo* t3773_ITIs[] = 
{
	&t5703_TI,
	&t5704_TI,
	&t5705_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3773_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5703_TI, 21},
	{ &t5704_TI, 28},
	{ &t5705_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3773_0_0_0;
extern Il2CppType t3773_1_0_0;
#include "t152.h"
extern TypeInfo t152_TI;
TypeInfo t3773_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "VerticalWrapMode[]", "UnityEngine", t3773_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t152_TI, t3773_ITIs, t3773_VT, &EmptyCustomAttributesCache, &t44_TI, &t3773_0_0_0, &t3773_1_0_0, t3773_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3774_TI;



// Metadata Definition UnityEngine.RenderMode[]
static MethodInfo* t3774_MIs[] =
{
	NULL
};
extern MethodInfo m22717_MI;
extern MethodInfo m22718_MI;
extern MethodInfo m22719_MI;
extern MethodInfo m22720_MI;
extern MethodInfo m22721_MI;
extern MethodInfo m22722_MI;
extern MethodInfo m22716_MI;
extern MethodInfo m22724_MI;
extern MethodInfo m22725_MI;
static MethodInfo* t3774_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22717_MI,
	&m5907_MI,
	&m22718_MI,
	&m22719_MI,
	&m22720_MI,
	&m22721_MI,
	&m22722_MI,
	&m5908_MI,
	&m22716_MI,
	&m22724_MI,
	&m22725_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5706_TI;
extern TypeInfo t5707_TI;
extern TypeInfo t5708_TI;
static TypeInfo* t3774_ITIs[] = 
{
	&t5706_TI,
	&t5707_TI,
	&t5708_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3774_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5706_TI, 21},
	{ &t5707_TI, 28},
	{ &t5708_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3774_0_0_0;
extern Il2CppType t3774_1_0_0;
#include "t361.h"
extern TypeInfo t361_TI;
TypeInfo t3774_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RenderMode[]", "UnityEngine", t3774_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t361_TI, t3774_ITIs, t3774_VT, &EmptyCustomAttributesCache, &t44_TI, &t3774_0_0_0, &t3774_1_0_0, t3774_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3775_TI;



// Metadata Definition UnityEngine.CanvasRenderer[]
static MethodInfo* t3775_MIs[] =
{
	NULL
};
extern MethodInfo m22730_MI;
extern MethodInfo m22731_MI;
extern MethodInfo m22732_MI;
extern MethodInfo m22733_MI;
extern MethodInfo m22734_MI;
extern MethodInfo m22735_MI;
extern MethodInfo m22729_MI;
extern MethodInfo m22737_MI;
extern MethodInfo m22738_MI;
static MethodInfo* t3775_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22730_MI,
	&m5907_MI,
	&m22731_MI,
	&m22732_MI,
	&m22733_MI,
	&m22734_MI,
	&m22735_MI,
	&m5908_MI,
	&m22729_MI,
	&m22737_MI,
	&m22738_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5709_TI;
extern TypeInfo t5710_TI;
extern TypeInfo t5711_TI;
static TypeInfo* t3775_ITIs[] = 
{
	&t5709_TI,
	&t5710_TI,
	&t5711_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3775_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5709_TI, 21},
	{ &t5710_TI, 28},
	{ &t5711_TI, 33},
	{ &t2240_TI, 34},
	{ &t2245_TI, 41},
	{ &t2241_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3775_0_0_0;
extern Il2CppType t3775_1_0_0;
struct t159;
extern TypeInfo t159_TI;
extern CustomAttributesCache t159__CustomAttributeCache_m2721;
extern CustomAttributesCache t159__CustomAttributeCache_m1591;
extern CustomAttributesCache t159__CustomAttributeCache_m2005;
extern CustomAttributesCache t159__CustomAttributeCache_m1578;
extern CustomAttributesCache t159__CustomAttributeCache_m2722;
extern CustomAttributesCache t159__CustomAttributeCache_m2723;
extern CustomAttributesCache t159__CustomAttributeCache_m1569;
extern CustomAttributesCache t159__CustomAttributeCache_m1552;
TypeInfo t3775_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CanvasRenderer[]", "UnityEngine", t3775_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t159_TI, t3775_ITIs, t3775_VT, &EmptyCustomAttributesCache, &t3775_TI, &t3775_0_0_0, &t3775_1_0_0, t3775_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t159 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3776_TI;



// Metadata Definition UnityEngine.WrapperlessIcall[]
static MethodInfo* t3776_MIs[] =
{
	NULL
};
extern MethodInfo m22742_MI;
extern MethodInfo m22743_MI;
extern MethodInfo m22744_MI;
extern MethodInfo m22745_MI;
extern MethodInfo m22746_MI;
extern MethodInfo m22747_MI;
extern MethodInfo m22741_MI;
extern MethodInfo m22749_MI;
extern MethodInfo m22750_MI;
static MethodInfo* t3776_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22742_MI,
	&m5907_MI,
	&m22743_MI,
	&m22744_MI,
	&m22745_MI,
	&m22746_MI,
	&m22747_MI,
	&m5908_MI,
	&m22741_MI,
	&m22749_MI,
	&m22750_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5712_TI;
extern TypeInfo t5713_TI;
extern TypeInfo t5714_TI;
static TypeInfo* t3776_ITIs[] = 
{
	&t5712_TI,
	&t5713_TI,
	&t5714_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3776_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5712_TI, 21},
	{ &t5713_TI, 28},
	{ &t5714_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3776_0_0_0;
extern Il2CppType t3776_1_0_0;
struct t532;
extern TypeInfo t532_TI;
TypeInfo t3776_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "WrapperlessIcall[]", "UnityEngine", t3776_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t532_TI, t3776_ITIs, t3776_VT, &EmptyCustomAttributesCache, &t3776_TI, &t3776_0_0_0, &t3776_1_0_0, t3776_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t532 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t534_TI;



// Metadata Definition UnityEngine.DisallowMultipleComponent[]
static MethodInfo* t534_MIs[] =
{
	NULL
};
extern MethodInfo m22753_MI;
extern MethodInfo m22754_MI;
extern MethodInfo m22755_MI;
extern MethodInfo m22756_MI;
extern MethodInfo m22757_MI;
extern MethodInfo m22758_MI;
extern MethodInfo m22752_MI;
extern MethodInfo m22760_MI;
extern MethodInfo m22761_MI;
static MethodInfo* t534_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22753_MI,
	&m5907_MI,
	&m22754_MI,
	&m22755_MI,
	&m22756_MI,
	&m22757_MI,
	&m22758_MI,
	&m5908_MI,
	&m22752_MI,
	&m22760_MI,
	&m22761_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5715_TI;
extern TypeInfo t5716_TI;
extern TypeInfo t5717_TI;
static TypeInfo* t534_ITIs[] = 
{
	&t5715_TI,
	&t5716_TI,
	&t5717_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t534_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5715_TI, 21},
	{ &t5716_TI, 28},
	{ &t5717_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t534_0_0_0;
extern Il2CppType t534_1_0_0;
struct t359;
extern TypeInfo t359_TI;
extern CustomAttributesCache t359__CustomAttributeCache;
TypeInfo t534_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DisallowMultipleComponent[]", "UnityEngine", t534_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t359_TI, t534_ITIs, t534_VT, &EmptyCustomAttributesCache, &t534_TI, &t534_0_0_0, &t534_1_0_0, t534_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t359 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t535_TI;



// Metadata Definition UnityEngine.ExecuteInEditMode[]
static MethodInfo* t535_MIs[] =
{
	NULL
};
extern MethodInfo m22764_MI;
extern MethodInfo m22765_MI;
extern MethodInfo m22766_MI;
extern MethodInfo m22767_MI;
extern MethodInfo m22768_MI;
extern MethodInfo m22769_MI;
extern MethodInfo m22763_MI;
extern MethodInfo m22771_MI;
extern MethodInfo m22772_MI;
static MethodInfo* t535_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22764_MI,
	&m5907_MI,
	&m22765_MI,
	&m22766_MI,
	&m22767_MI,
	&m22768_MI,
	&m22769_MI,
	&m5908_MI,
	&m22763_MI,
	&m22771_MI,
	&m22772_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5718_TI;
extern TypeInfo t5719_TI;
extern TypeInfo t5720_TI;
static TypeInfo* t535_ITIs[] = 
{
	&t5718_TI,
	&t5719_TI,
	&t5720_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t535_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5718_TI, 21},
	{ &t5719_TI, 28},
	{ &t5720_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t535_0_0_0;
extern Il2CppType t535_1_0_0;
struct t360;
extern TypeInfo t360_TI;
TypeInfo t535_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ExecuteInEditMode[]", "UnityEngine", t535_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t360_TI, t535_ITIs, t535_VT, &EmptyCustomAttributesCache, &t535_TI, &t535_0_0_0, &t535_1_0_0, t535_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t360 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t536_TI;



// Metadata Definition UnityEngine.RequireComponent[]
static MethodInfo* t536_MIs[] =
{
	NULL
};
extern MethodInfo m22775_MI;
extern MethodInfo m22776_MI;
extern MethodInfo m22777_MI;
extern MethodInfo m22778_MI;
extern MethodInfo m22779_MI;
extern MethodInfo m22780_MI;
extern MethodInfo m22774_MI;
extern MethodInfo m22782_MI;
extern MethodInfo m22783_MI;
static MethodInfo* t536_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22775_MI,
	&m5907_MI,
	&m22776_MI,
	&m22777_MI,
	&m22778_MI,
	&m22779_MI,
	&m22780_MI,
	&m5908_MI,
	&m22774_MI,
	&m22782_MI,
	&m22783_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5721_TI;
extern TypeInfo t5722_TI;
extern TypeInfo t5723_TI;
static TypeInfo* t536_ITIs[] = 
{
	&t5721_TI,
	&t5722_TI,
	&t5723_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t536_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5721_TI, 21},
	{ &t5722_TI, 28},
	{ &t5723_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t536_0_0_0;
extern Il2CppType t536_1_0_0;
struct t318;
extern TypeInfo t318_TI;
extern CustomAttributesCache t318__CustomAttributeCache;
TypeInfo t536_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RequireComponent[]", "UnityEngine", t536_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t318_TI, t536_ITIs, t536_VT, &EmptyCustomAttributesCache, &t536_TI, &t536_0_0_0, &t536_1_0_0, t536_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t318 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3777_TI;



// Metadata Definition UnityEngine.AddComponentMenu[]
static MethodInfo* t3777_MIs[] =
{
	NULL
};
extern MethodInfo m22801_MI;
extern MethodInfo m22802_MI;
extern MethodInfo m22803_MI;
extern MethodInfo m22804_MI;
extern MethodInfo m22805_MI;
extern MethodInfo m22806_MI;
extern MethodInfo m22800_MI;
extern MethodInfo m22808_MI;
extern MethodInfo m22809_MI;
static MethodInfo* t3777_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22801_MI,
	&m5907_MI,
	&m22802_MI,
	&m22803_MI,
	&m22804_MI,
	&m22805_MI,
	&m22806_MI,
	&m5908_MI,
	&m22800_MI,
	&m22808_MI,
	&m22809_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5724_TI;
extern TypeInfo t5725_TI;
extern TypeInfo t5726_TI;
static TypeInfo* t3777_ITIs[] = 
{
	&t5724_TI,
	&t5725_TI,
	&t5726_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3777_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5724_TI, 21},
	{ &t5725_TI, 28},
	{ &t5726_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3777_0_0_0;
extern Il2CppType t3777_1_0_0;
struct t298;
extern TypeInfo t298_TI;
TypeInfo t3777_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AddComponentMenu[]", "UnityEngine", t3777_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t298_TI, t3777_ITIs, t3777_VT, &EmptyCustomAttributesCache, &t3777_TI, &t3777_0_0_0, &t3777_1_0_0, t3777_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t298 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t634_TI;



// Metadata Definition System.Reflection.ParameterModifier[]
static MethodInfo* t634_MIs[] =
{
	NULL
};
extern MethodInfo m22812_MI;
extern MethodInfo m22813_MI;
extern MethodInfo m22814_MI;
extern MethodInfo m22815_MI;
extern MethodInfo m22816_MI;
extern MethodInfo m22817_MI;
extern MethodInfo m22811_MI;
extern MethodInfo m22819_MI;
extern MethodInfo m22820_MI;
static MethodInfo* t634_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22812_MI,
	&m5907_MI,
	&m22813_MI,
	&m22814_MI,
	&m22815_MI,
	&m22816_MI,
	&m22817_MI,
	&m5908_MI,
	&m22811_MI,
	&m22819_MI,
	&m22820_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5727_TI;
extern TypeInfo t5728_TI;
extern TypeInfo t5729_TI;
static TypeInfo* t634_ITIs[] = 
{
	&t5727_TI,
	&t5728_TI,
	&t5729_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t634_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5727_TI, 21},
	{ &t5728_TI, 28},
	{ &t5729_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t634_0_0_0;
extern Il2CppType t634_1_0_0;
#include "t632.h"
extern TypeInfo t632_TI;
extern CustomAttributesCache t632__CustomAttributeCache;
TypeInfo t634_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterModifier[]", "System.Reflection", t634_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t632_TI, t634_ITIs, t634_VT, &EmptyCustomAttributesCache, &t634_TI, &t634_0_0_0, &t634_1_0_0, t634_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t632 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3778_TI;



// Metadata Definition UnityEngine.WritableAttribute[]
static MethodInfo* t3778_MIs[] =
{
	NULL
};
extern MethodInfo m22823_MI;
extern MethodInfo m22824_MI;
extern MethodInfo m22825_MI;
extern MethodInfo m22826_MI;
extern MethodInfo m22827_MI;
extern MethodInfo m22828_MI;
extern MethodInfo m22822_MI;
extern MethodInfo m22830_MI;
extern MethodInfo m22831_MI;
static MethodInfo* t3778_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22823_MI,
	&m5907_MI,
	&m22824_MI,
	&m22825_MI,
	&m22826_MI,
	&m22827_MI,
	&m22828_MI,
	&m5908_MI,
	&m22822_MI,
	&m22830_MI,
	&m22831_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5730_TI;
extern TypeInfo t5731_TI;
extern TypeInfo t5732_TI;
static TypeInfo* t3778_ITIs[] = 
{
	&t5730_TI,
	&t5731_TI,
	&t5732_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3778_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5730_TI, 21},
	{ &t5731_TI, 28},
	{ &t5732_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3778_0_0_0;
extern Il2CppType t3778_1_0_0;
struct t539;
extern TypeInfo t539_TI;
extern CustomAttributesCache t539__CustomAttributeCache;
TypeInfo t3778_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "WritableAttribute[]", "UnityEngine", t3778_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t539_TI, t3778_ITIs, t3778_VT, &EmptyCustomAttributesCache, &t3778_TI, &t3778_0_0_0, &t3778_1_0_0, t3778_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t539 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3779_TI;



// Metadata Definition UnityEngine.AssemblyIsEditorAssembly[]
static MethodInfo* t3779_MIs[] =
{
	NULL
};
extern MethodInfo m22834_MI;
extern MethodInfo m22835_MI;
extern MethodInfo m22836_MI;
extern MethodInfo m22837_MI;
extern MethodInfo m22838_MI;
extern MethodInfo m22839_MI;
extern MethodInfo m22833_MI;
extern MethodInfo m22841_MI;
extern MethodInfo m22842_MI;
static MethodInfo* t3779_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22834_MI,
	&m5907_MI,
	&m22835_MI,
	&m22836_MI,
	&m22837_MI,
	&m22838_MI,
	&m22839_MI,
	&m5908_MI,
	&m22833_MI,
	&m22841_MI,
	&m22842_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5733_TI;
extern TypeInfo t5734_TI;
extern TypeInfo t5735_TI;
static TypeInfo* t3779_ITIs[] = 
{
	&t5733_TI,
	&t5734_TI,
	&t5735_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3779_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5733_TI, 21},
	{ &t5734_TI, 28},
	{ &t5735_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3779_0_0_0;
extern Il2CppType t3779_1_0_0;
struct t540;
extern TypeInfo t540_TI;
extern CustomAttributesCache t540__CustomAttributeCache;
TypeInfo t3779_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AssemblyIsEditorAssembly[]", "UnityEngine", t3779_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t540_TI, t3779_ITIs, t3779_VT, &EmptyCustomAttributesCache, &t3779_TI, &t3779_0_0_0, &t3779_1_0_0, t3779_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t540 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3780_TI;



// Metadata Definition UnityEngine.CameraClearFlags[]
static MethodInfo* t3780_MIs[] =
{
	NULL
};
extern MethodInfo m22845_MI;
extern MethodInfo m22846_MI;
extern MethodInfo m22847_MI;
extern MethodInfo m22848_MI;
extern MethodInfo m22849_MI;
extern MethodInfo m22850_MI;
extern MethodInfo m22844_MI;
extern MethodInfo m22852_MI;
extern MethodInfo m22853_MI;
static MethodInfo* t3780_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22845_MI,
	&m5907_MI,
	&m22846_MI,
	&m22847_MI,
	&m22848_MI,
	&m22849_MI,
	&m22850_MI,
	&m5908_MI,
	&m22844_MI,
	&m22852_MI,
	&m22853_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5736_TI;
extern TypeInfo t5737_TI;
extern TypeInfo t5738_TI;
static TypeInfo* t3780_ITIs[] = 
{
	&t5736_TI,
	&t5737_TI,
	&t5738_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3780_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5736_TI, 21},
	{ &t5737_TI, 28},
	{ &t5738_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3780_0_0_0;
extern Il2CppType t3780_1_0_0;
#include "t498.h"
extern TypeInfo t498_TI;
TypeInfo t3780_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CameraClearFlags[]", "UnityEngine", t3780_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t498_TI, t3780_ITIs, t3780_VT, &EmptyCustomAttributesCache, &t44_TI, &t3780_0_0_0, &t3780_1_0_0, t3780_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t544_TI;



// Metadata Definition UnityEngine.SendMouseEvents/HitInfo[]
static MethodInfo* t544_MIs[] =
{
	NULL
};
extern MethodInfo m22856_MI;
extern MethodInfo m22857_MI;
extern MethodInfo m22858_MI;
extern MethodInfo m22859_MI;
extern MethodInfo m22860_MI;
extern MethodInfo m22861_MI;
extern MethodInfo m22855_MI;
extern MethodInfo m22863_MI;
extern MethodInfo m22864_MI;
static MethodInfo* t544_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22856_MI,
	&m5907_MI,
	&m22857_MI,
	&m22858_MI,
	&m22859_MI,
	&m22860_MI,
	&m22861_MI,
	&m5908_MI,
	&m22855_MI,
	&m22863_MI,
	&m22864_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5739_TI;
extern TypeInfo t5740_TI;
extern TypeInfo t5741_TI;
static TypeInfo* t544_ITIs[] = 
{
	&t5739_TI,
	&t5740_TI,
	&t5741_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t544_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5739_TI, 21},
	{ &t5740_TI, 28},
	{ &t5741_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t544_0_0_0;
extern Il2CppType t544_1_0_0;
#include "t542.h"
extern TypeInfo t542_TI;
TypeInfo t544_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "HitInfo[]", "", t544_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t542_TI, t544_ITIs, t544_VT, &EmptyCustomAttributesCache, &t544_TI, &t544_0_0_0, &t544_1_0_0, t544_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t542 ), -1, 0, 0, -1, 1048843, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3781_TI;



// Metadata Definition UnityEngine.PropertyAttribute[]
static MethodInfo* t3781_MIs[] =
{
	NULL
};
extern MethodInfo m22867_MI;
extern MethodInfo m22868_MI;
extern MethodInfo m22869_MI;
extern MethodInfo m22870_MI;
extern MethodInfo m22871_MI;
extern MethodInfo m22872_MI;
extern MethodInfo m22866_MI;
extern MethodInfo m22874_MI;
extern MethodInfo m22875_MI;
static MethodInfo* t3781_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22867_MI,
	&m5907_MI,
	&m22868_MI,
	&m22869_MI,
	&m22870_MI,
	&m22871_MI,
	&m22872_MI,
	&m5908_MI,
	&m22866_MI,
	&m22874_MI,
	&m22875_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5742_TI;
extern TypeInfo t5743_TI;
extern TypeInfo t5744_TI;
static TypeInfo* t3781_ITIs[] = 
{
	&t5742_TI,
	&t5743_TI,
	&t5744_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3781_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5742_TI, 21},
	{ &t5743_TI, 28},
	{ &t5744_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3781_0_0_0;
extern Il2CppType t3781_1_0_0;
struct t545;
extern TypeInfo t545_TI;
extern CustomAttributesCache t545__CustomAttributeCache;
TypeInfo t3781_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PropertyAttribute[]", "UnityEngine", t3781_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t545_TI, t3781_ITIs, t3781_VT, &EmptyCustomAttributesCache, &t3781_TI, &t3781_0_0_0, &t3781_1_0_0, t3781_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t545 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3782_TI;



// Metadata Definition UnityEngine.TooltipAttribute[]
static MethodInfo* t3782_MIs[] =
{
	NULL
};
extern MethodInfo m22878_MI;
extern MethodInfo m22879_MI;
extern MethodInfo m22880_MI;
extern MethodInfo m22881_MI;
extern MethodInfo m22882_MI;
extern MethodInfo m22883_MI;
extern MethodInfo m22877_MI;
extern MethodInfo m22885_MI;
extern MethodInfo m22886_MI;
static MethodInfo* t3782_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22878_MI,
	&m5907_MI,
	&m22879_MI,
	&m22880_MI,
	&m22881_MI,
	&m22882_MI,
	&m22883_MI,
	&m5908_MI,
	&m22877_MI,
	&m22885_MI,
	&m22886_MI,
	&m5905_MI,
	&m5906_MI,
	&m22867_MI,
	&m5907_MI,
	&m22868_MI,
	&m22869_MI,
	&m22870_MI,
	&m22871_MI,
	&m22872_MI,
	&m5908_MI,
	&m22866_MI,
	&m22874_MI,
	&m22875_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5745_TI;
extern TypeInfo t5746_TI;
extern TypeInfo t5747_TI;
static TypeInfo* t3782_ITIs[] = 
{
	&t5745_TI,
	&t5746_TI,
	&t5747_TI,
	&t5742_TI,
	&t5743_TI,
	&t5744_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3782_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5745_TI, 21},
	{ &t5746_TI, 28},
	{ &t5747_TI, 33},
	{ &t5742_TI, 34},
	{ &t5743_TI, 41},
	{ &t5744_TI, 46},
	{ &t5619_TI, 47},
	{ &t5620_TI, 54},
	{ &t5621_TI, 59},
	{ &t5622_TI, 60},
	{ &t5623_TI, 67},
	{ &t5624_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3782_0_0_0;
extern Il2CppType t3782_1_0_0;
struct t399;
extern TypeInfo t399_TI;
extern CustomAttributesCache t399__CustomAttributeCache;
TypeInfo t3782_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TooltipAttribute[]", "UnityEngine", t3782_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t399_TI, t3782_ITIs, t3782_VT, &EmptyCustomAttributesCache, &t3782_TI, &t3782_0_0_0, &t3782_1_0_0, t3782_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t399 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3783_TI;



// Metadata Definition UnityEngine.SpaceAttribute[]
static MethodInfo* t3783_MIs[] =
{
	NULL
};
extern MethodInfo m22889_MI;
extern MethodInfo m22890_MI;
extern MethodInfo m22891_MI;
extern MethodInfo m22892_MI;
extern MethodInfo m22893_MI;
extern MethodInfo m22894_MI;
extern MethodInfo m22888_MI;
extern MethodInfo m22896_MI;
extern MethodInfo m22897_MI;
static MethodInfo* t3783_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22889_MI,
	&m5907_MI,
	&m22890_MI,
	&m22891_MI,
	&m22892_MI,
	&m22893_MI,
	&m22894_MI,
	&m5908_MI,
	&m22888_MI,
	&m22896_MI,
	&m22897_MI,
	&m5905_MI,
	&m5906_MI,
	&m22867_MI,
	&m5907_MI,
	&m22868_MI,
	&m22869_MI,
	&m22870_MI,
	&m22871_MI,
	&m22872_MI,
	&m5908_MI,
	&m22866_MI,
	&m22874_MI,
	&m22875_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5748_TI;
extern TypeInfo t5749_TI;
extern TypeInfo t5750_TI;
static TypeInfo* t3783_ITIs[] = 
{
	&t5748_TI,
	&t5749_TI,
	&t5750_TI,
	&t5742_TI,
	&t5743_TI,
	&t5744_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3783_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5748_TI, 21},
	{ &t5749_TI, 28},
	{ &t5750_TI, 33},
	{ &t5742_TI, 34},
	{ &t5743_TI, 41},
	{ &t5744_TI, 46},
	{ &t5619_TI, 47},
	{ &t5620_TI, 54},
	{ &t5621_TI, 59},
	{ &t5622_TI, 60},
	{ &t5623_TI, 67},
	{ &t5624_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3783_0_0_0;
extern Il2CppType t3783_1_0_0;
struct t394;
extern TypeInfo t394_TI;
extern CustomAttributesCache t394__CustomAttributeCache;
TypeInfo t3783_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SpaceAttribute[]", "UnityEngine", t3783_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t394_TI, t3783_ITIs, t3783_VT, &EmptyCustomAttributesCache, &t3783_TI, &t3783_0_0_0, &t3783_1_0_0, t3783_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t394 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3784_TI;



// Metadata Definition UnityEngine.RangeAttribute[]
static MethodInfo* t3784_MIs[] =
{
	NULL
};
extern MethodInfo m22900_MI;
extern MethodInfo m22901_MI;
extern MethodInfo m22902_MI;
extern MethodInfo m22903_MI;
extern MethodInfo m22904_MI;
extern MethodInfo m22905_MI;
extern MethodInfo m22899_MI;
extern MethodInfo m22907_MI;
extern MethodInfo m22908_MI;
static MethodInfo* t3784_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22900_MI,
	&m5907_MI,
	&m22901_MI,
	&m22902_MI,
	&m22903_MI,
	&m22904_MI,
	&m22905_MI,
	&m5908_MI,
	&m22899_MI,
	&m22907_MI,
	&m22908_MI,
	&m5905_MI,
	&m5906_MI,
	&m22867_MI,
	&m5907_MI,
	&m22868_MI,
	&m22869_MI,
	&m22870_MI,
	&m22871_MI,
	&m22872_MI,
	&m5908_MI,
	&m22866_MI,
	&m22874_MI,
	&m22875_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5751_TI;
extern TypeInfo t5752_TI;
extern TypeInfo t5753_TI;
static TypeInfo* t3784_ITIs[] = 
{
	&t5751_TI,
	&t5752_TI,
	&t5753_TI,
	&t5742_TI,
	&t5743_TI,
	&t5744_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3784_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5751_TI, 21},
	{ &t5752_TI, 28},
	{ &t5753_TI, 33},
	{ &t5742_TI, 34},
	{ &t5743_TI, 41},
	{ &t5744_TI, 46},
	{ &t5619_TI, 47},
	{ &t5620_TI, 54},
	{ &t5621_TI, 59},
	{ &t5622_TI, 60},
	{ &t5623_TI, 67},
	{ &t5624_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3784_0_0_0;
extern Il2CppType t3784_1_0_0;
struct t349;
extern TypeInfo t349_TI;
extern CustomAttributesCache t349__CustomAttributeCache;
TypeInfo t3784_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RangeAttribute[]", "UnityEngine", t3784_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t349_TI, t3784_ITIs, t3784_VT, &EmptyCustomAttributesCache, &t3784_TI, &t3784_0_0_0, &t3784_1_0_0, t3784_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t349 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3785_TI;



// Metadata Definition UnityEngine.TextAreaAttribute[]
static MethodInfo* t3785_MIs[] =
{
	NULL
};
extern MethodInfo m22911_MI;
extern MethodInfo m22912_MI;
extern MethodInfo m22913_MI;
extern MethodInfo m22914_MI;
extern MethodInfo m22915_MI;
extern MethodInfo m22916_MI;
extern MethodInfo m22910_MI;
extern MethodInfo m22918_MI;
extern MethodInfo m22919_MI;
static MethodInfo* t3785_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22911_MI,
	&m5907_MI,
	&m22912_MI,
	&m22913_MI,
	&m22914_MI,
	&m22915_MI,
	&m22916_MI,
	&m5908_MI,
	&m22910_MI,
	&m22918_MI,
	&m22919_MI,
	&m5905_MI,
	&m5906_MI,
	&m22867_MI,
	&m5907_MI,
	&m22868_MI,
	&m22869_MI,
	&m22870_MI,
	&m22871_MI,
	&m22872_MI,
	&m5908_MI,
	&m22866_MI,
	&m22874_MI,
	&m22875_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5754_TI;
extern TypeInfo t5755_TI;
extern TypeInfo t5756_TI;
static TypeInfo* t3785_ITIs[] = 
{
	&t5754_TI,
	&t5755_TI,
	&t5756_TI,
	&t5742_TI,
	&t5743_TI,
	&t5744_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3785_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5754_TI, 21},
	{ &t5755_TI, 28},
	{ &t5756_TI, 33},
	{ &t5742_TI, 34},
	{ &t5743_TI, 41},
	{ &t5744_TI, 46},
	{ &t5619_TI, 47},
	{ &t5620_TI, 54},
	{ &t5621_TI, 59},
	{ &t5622_TI, 60},
	{ &t5623_TI, 67},
	{ &t5624_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3785_0_0_0;
extern Il2CppType t3785_1_0_0;
struct t405;
extern TypeInfo t405_TI;
extern CustomAttributesCache t405__CustomAttributeCache;
TypeInfo t3785_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextAreaAttribute[]", "UnityEngine", t3785_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t405_TI, t3785_ITIs, t3785_VT, &EmptyCustomAttributesCache, &t3785_TI, &t3785_0_0_0, &t3785_1_0_0, t3785_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t405 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3786_TI;



// Metadata Definition UnityEngine.SelectionBaseAttribute[]
static MethodInfo* t3786_MIs[] =
{
	NULL
};
extern MethodInfo m22922_MI;
extern MethodInfo m22923_MI;
extern MethodInfo m22924_MI;
extern MethodInfo m22925_MI;
extern MethodInfo m22926_MI;
extern MethodInfo m22927_MI;
extern MethodInfo m22921_MI;
extern MethodInfo m22929_MI;
extern MethodInfo m22930_MI;
static MethodInfo* t3786_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22922_MI,
	&m5907_MI,
	&m22923_MI,
	&m22924_MI,
	&m22925_MI,
	&m22926_MI,
	&m22927_MI,
	&m5908_MI,
	&m22921_MI,
	&m22929_MI,
	&m22930_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5757_TI;
extern TypeInfo t5758_TI;
extern TypeInfo t5759_TI;
static TypeInfo* t3786_ITIs[] = 
{
	&t5757_TI,
	&t5758_TI,
	&t5759_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3786_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5757_TI, 21},
	{ &t5758_TI, 28},
	{ &t5759_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3786_0_0_0;
extern Il2CppType t3786_1_0_0;
struct t397;
extern TypeInfo t397_TI;
extern CustomAttributesCache t397__CustomAttributeCache;
TypeInfo t3786_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SelectionBaseAttribute[]", "UnityEngine", t3786_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t397_TI, t3786_ITIs, t3786_VT, &EmptyCustomAttributesCache, &t3786_TI, &t3786_0_0_0, &t3786_1_0_0, t3786_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t397 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t638_TI;



// Metadata Definition System.Reflection.ParameterInfo[]
static MethodInfo* t638_MIs[] =
{
	NULL
};
extern MethodInfo m22933_MI;
extern MethodInfo m22934_MI;
extern MethodInfo m22935_MI;
extern MethodInfo m22936_MI;
extern MethodInfo m22937_MI;
extern MethodInfo m22938_MI;
extern MethodInfo m22932_MI;
extern MethodInfo m22940_MI;
extern MethodInfo m22941_MI;
extern MethodInfo m19836_MI;
extern MethodInfo m19837_MI;
extern MethodInfo m19838_MI;
extern MethodInfo m19839_MI;
extern MethodInfo m19840_MI;
extern MethodInfo m19841_MI;
extern MethodInfo m19835_MI;
extern MethodInfo m19843_MI;
extern MethodInfo m19844_MI;
extern MethodInfo m22944_MI;
extern MethodInfo m22945_MI;
extern MethodInfo m22946_MI;
extern MethodInfo m22947_MI;
extern MethodInfo m22948_MI;
extern MethodInfo m22949_MI;
extern MethodInfo m22943_MI;
extern MethodInfo m22951_MI;
extern MethodInfo m22952_MI;
static MethodInfo* t638_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22933_MI,
	&m5907_MI,
	&m22934_MI,
	&m22935_MI,
	&m22936_MI,
	&m22937_MI,
	&m22938_MI,
	&m5908_MI,
	&m22932_MI,
	&m22940_MI,
	&m22941_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m22944_MI,
	&m5907_MI,
	&m22945_MI,
	&m22946_MI,
	&m22947_MI,
	&m22948_MI,
	&m22949_MI,
	&m5908_MI,
	&m22943_MI,
	&m22951_MI,
	&m22952_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5760_TI;
extern TypeInfo t5761_TI;
extern TypeInfo t5762_TI;
extern TypeInfo t5134_TI;
extern TypeInfo t5135_TI;
extern TypeInfo t5136_TI;
extern TypeInfo t5763_TI;
extern TypeInfo t5764_TI;
extern TypeInfo t5765_TI;
static TypeInfo* t638_ITIs[] = 
{
	&t5760_TI,
	&t5761_TI,
	&t5762_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5763_TI,
	&t5764_TI,
	&t5765_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t638_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5760_TI, 21},
	{ &t5761_TI, 28},
	{ &t5762_TI, 33},
	{ &t5134_TI, 34},
	{ &t5135_TI, 41},
	{ &t5136_TI, 46},
	{ &t5763_TI, 47},
	{ &t5764_TI, 54},
	{ &t5765_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t638_0_0_0;
extern Il2CppType t638_1_0_0;
struct t637;
extern TypeInfo t637_TI;
extern CustomAttributesCache t637__CustomAttributeCache;
TypeInfo t638_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterInfo[]", "System.Reflection", t638_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t637_TI, t638_ITIs, t638_VT, &EmptyCustomAttributesCache, &t638_TI, &t638_0_0_0, &t638_1_0_0, t638_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t637 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3547_TI;



// Metadata Definition System.Runtime.InteropServices._ParameterInfo[]
static MethodInfo* t3547_MIs[] =
{
	NULL
};
static MethodInfo* t3547_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22944_MI,
	&m5907_MI,
	&m22945_MI,
	&m22946_MI,
	&m22947_MI,
	&m22948_MI,
	&m22949_MI,
	&m5908_MI,
	&m22943_MI,
	&m22951_MI,
	&m22952_MI,
};
static TypeInfo* t3547_ITIs[] = 
{
	&t5763_TI,
	&t5764_TI,
	&t5765_TI,
};
static Il2CppInterfaceOffsetPair t3547_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5763_TI, 21},
	{ &t5764_TI, 28},
	{ &t5765_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3547_0_0_0;
extern Il2CppType t3547_1_0_0;
struct t2037;
extern TypeInfo t2037_TI;
extern CustomAttributesCache t2037__CustomAttributeCache;
TypeInfo t3547_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ParameterInfo[]", "System.Runtime.InteropServices", t3547_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2037_TI, t3547_ITIs, t3547_VT, &EmptyCustomAttributesCache, &t3547_TI, &t3547_0_0_0, &t3547_1_0_0, t3547_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3787_TI;



// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute[]
static MethodInfo* t3787_MIs[] =
{
	NULL
};
extern MethodInfo m22955_MI;
extern MethodInfo m22956_MI;
extern MethodInfo m22957_MI;
extern MethodInfo m22958_MI;
extern MethodInfo m22959_MI;
extern MethodInfo m22960_MI;
extern MethodInfo m22954_MI;
extern MethodInfo m22962_MI;
extern MethodInfo m22963_MI;
static MethodInfo* t3787_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22955_MI,
	&m5907_MI,
	&m22956_MI,
	&m22957_MI,
	&m22958_MI,
	&m22959_MI,
	&m22960_MI,
	&m5908_MI,
	&m22954_MI,
	&m22962_MI,
	&m22963_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5766_TI;
extern TypeInfo t5767_TI;
extern TypeInfo t5768_TI;
static TypeInfo* t3787_ITIs[] = 
{
	&t5766_TI,
	&t5767_TI,
	&t5768_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3787_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5766_TI, 21},
	{ &t5767_TI, 28},
	{ &t5768_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3787_0_0_0;
extern Il2CppType t3787_1_0_0;
struct t549;
extern TypeInfo t549_TI;
extern CustomAttributesCache t549__CustomAttributeCache;
TypeInfo t3787_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SharedBetweenAnimatorsAttribute[]", "UnityEngine", t3787_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t549_TI, t3787_ITIs, t3787_VT, &EmptyCustomAttributesCache, &t3787_TI, &t3787_0_0_0, &t3787_1_0_0, t3787_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t549 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3788_TI;



// Metadata Definition UnityEngine.StateMachineBehaviour[]
static MethodInfo* t3788_MIs[] =
{
	NULL
};
extern MethodInfo m22966_MI;
extern MethodInfo m22967_MI;
extern MethodInfo m22968_MI;
extern MethodInfo m22969_MI;
extern MethodInfo m22970_MI;
extern MethodInfo m22971_MI;
extern MethodInfo m22965_MI;
extern MethodInfo m22973_MI;
extern MethodInfo m22974_MI;
extern MethodInfo m21991_MI;
extern MethodInfo m21992_MI;
extern MethodInfo m21993_MI;
extern MethodInfo m21994_MI;
extern MethodInfo m21995_MI;
extern MethodInfo m21996_MI;
extern MethodInfo m21990_MI;
extern MethodInfo m21998_MI;
extern MethodInfo m21999_MI;
static MethodInfo* t3788_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22966_MI,
	&m5907_MI,
	&m22967_MI,
	&m22968_MI,
	&m22969_MI,
	&m22970_MI,
	&m22971_MI,
	&m5908_MI,
	&m22965_MI,
	&m22973_MI,
	&m22974_MI,
	&m5905_MI,
	&m5906_MI,
	&m21991_MI,
	&m5907_MI,
	&m21992_MI,
	&m21993_MI,
	&m21994_MI,
	&m21995_MI,
	&m21996_MI,
	&m5908_MI,
	&m21990_MI,
	&m21998_MI,
	&m21999_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5769_TI;
extern TypeInfo t5770_TI;
extern TypeInfo t5771_TI;
extern TypeInfo t5544_TI;
extern TypeInfo t5545_TI;
extern TypeInfo t5546_TI;
static TypeInfo* t3788_ITIs[] = 
{
	&t5769_TI,
	&t5770_TI,
	&t5771_TI,
	&t5544_TI,
	&t5545_TI,
	&t5546_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3788_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5769_TI, 21},
	{ &t5770_TI, 28},
	{ &t5771_TI, 33},
	{ &t5544_TI, 34},
	{ &t5545_TI, 41},
	{ &t5546_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3788_0_0_0;
extern Il2CppType t3788_1_0_0;
struct t550;
extern TypeInfo t550_TI;
TypeInfo t3788_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "StateMachineBehaviour[]", "UnityEngine", t3788_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t550_TI, t3788_ITIs, t3788_VT, &EmptyCustomAttributesCache, &t3788_TI, &t3788_0_0_0, &t3788_1_0_0, t3788_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t550 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3105_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
static MethodInfo* t3105_MIs[] =
{
	NULL
};
extern MethodInfo m22978_MI;
extern MethodInfo m22979_MI;
extern MethodInfo m22980_MI;
extern MethodInfo m22981_MI;
extern MethodInfo m22982_MI;
extern MethodInfo m22983_MI;
extern MethodInfo m22977_MI;
extern MethodInfo m22985_MI;
extern MethodInfo m22986_MI;
static MethodInfo* t3105_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22978_MI,
	&m5907_MI,
	&m22979_MI,
	&m22980_MI,
	&m22981_MI,
	&m22982_MI,
	&m22983_MI,
	&m5908_MI,
	&m22977_MI,
	&m22985_MI,
	&m22986_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5772_TI;
extern TypeInfo t5773_TI;
extern TypeInfo t5774_TI;
static TypeInfo* t3105_ITIs[] = 
{
	&t5772_TI,
	&t5773_TI,
	&t5774_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3105_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5772_TI, 21},
	{ &t5773_TI, 28},
	{ &t5774_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3105_0_0_0;
extern Il2CppType t3105_1_0_0;
#include "t3106.h"
extern TypeInfo t3106_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t3105_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t3105_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t3106_TI, t3105_ITIs, t3105_VT, &EmptyCustomAttributesCache, &t3105_TI, &t3105_0_0_0, &t3105_1_0_0, t3105_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t3106 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3100_TI;



// Metadata Definition UnityEngine.Event[]
static MethodInfo* t3100_MIs[] =
{
	NULL
};
extern MethodInfo m22989_MI;
extern MethodInfo m22990_MI;
extern MethodInfo m22991_MI;
extern MethodInfo m22992_MI;
extern MethodInfo m22993_MI;
extern MethodInfo m22994_MI;
extern MethodInfo m22988_MI;
extern MethodInfo m22996_MI;
extern MethodInfo m22997_MI;
static MethodInfo* t3100_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22989_MI,
	&m5907_MI,
	&m22990_MI,
	&m22991_MI,
	&m22992_MI,
	&m22993_MI,
	&m22994_MI,
	&m5908_MI,
	&m22988_MI,
	&m22996_MI,
	&m22997_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5775_TI;
extern TypeInfo t5776_TI;
extern TypeInfo t5777_TI;
static TypeInfo* t3100_ITIs[] = 
{
	&t5775_TI,
	&t5776_TI,
	&t5777_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3100_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5775_TI, 21},
	{ &t5776_TI, 28},
	{ &t5777_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3100_0_0_0;
extern Il2CppType t3100_1_0_0;
struct t204;
extern TypeInfo t204_TI;
extern CustomAttributesCache t204__CustomAttributeCache_U3CU3Ef__switch$map0;
extern CustomAttributesCache t204__CustomAttributeCache_m2302;
extern CustomAttributesCache t204__CustomAttributeCache_m2304;
extern CustomAttributesCache t204__CustomAttributeCache_m1759;
extern CustomAttributesCache t204__CustomAttributeCache_m2305;
extern CustomAttributesCache t204__CustomAttributeCache_m2307;
extern CustomAttributesCache t204__CustomAttributeCache_m1755;
extern CustomAttributesCache t204__CustomAttributeCache_m1757;
extern CustomAttributesCache t204__CustomAttributeCache_m2308;
extern CustomAttributesCache t204__CustomAttributeCache_m1756;
extern CustomAttributesCache t204__CustomAttributeCache_m2310;
extern CustomAttributesCache t204__CustomAttributeCache_m1760;
TypeInfo t3100_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Event[]", "UnityEngine", t3100_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t204_TI, t3100_ITIs, t3100_VT, &EmptyCustomAttributesCache, &t3100_TI, &t3100_0_0_0, &t3100_1_0_0, t3100_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t204 *), -1, sizeof(t3100_SFs), 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, true, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3101_TI;



// Metadata Definition UnityEngine.TextEditor/TextEditOp[]
static MethodInfo* t3101_MIs[] =
{
	NULL
};
extern MethodInfo m23000_MI;
extern MethodInfo m23001_MI;
extern MethodInfo m23002_MI;
extern MethodInfo m23003_MI;
extern MethodInfo m23004_MI;
extern MethodInfo m23005_MI;
extern MethodInfo m22999_MI;
extern MethodInfo m23007_MI;
extern MethodInfo m23008_MI;
static MethodInfo* t3101_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23000_MI,
	&m5907_MI,
	&m23001_MI,
	&m23002_MI,
	&m23003_MI,
	&m23004_MI,
	&m23005_MI,
	&m5908_MI,
	&m22999_MI,
	&m23007_MI,
	&m23008_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5778_TI;
extern TypeInfo t5779_TI;
extern TypeInfo t5780_TI;
static TypeInfo* t3101_ITIs[] = 
{
	&t5778_TI,
	&t5779_TI,
	&t5780_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3101_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5778_TI, 21},
	{ &t5779_TI, 28},
	{ &t5780_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3101_0_0_0;
extern Il2CppType t3101_1_0_0;
#include "t552.h"
extern TypeInfo t552_TI;
TypeInfo t3101_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TextEditOp[]", "", t3101_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t552_TI, t3101_ITIs, t3101_VT, &EmptyCustomAttributesCache, &t44_TI, &t3101_0_0_0, &t3101_1_0_0, t3101_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3789_TI;



// Metadata Definition UnityEngine.TextEditor/DblClickSnapping[]
static MethodInfo* t3789_MIs[] =
{
	NULL
};
extern MethodInfo m23018_MI;
extern MethodInfo m23019_MI;
extern MethodInfo m23020_MI;
extern MethodInfo m23021_MI;
extern MethodInfo m23022_MI;
extern MethodInfo m23023_MI;
extern MethodInfo m23017_MI;
extern MethodInfo m23025_MI;
extern MethodInfo m23026_MI;
static MethodInfo* t3789_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23018_MI,
	&m5907_MI,
	&m23019_MI,
	&m23020_MI,
	&m23021_MI,
	&m23022_MI,
	&m23023_MI,
	&m5908_MI,
	&m23017_MI,
	&m23025_MI,
	&m23026_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5781_TI;
extern TypeInfo t5782_TI;
extern TypeInfo t5783_TI;
static TypeInfo* t3789_ITIs[] = 
{
	&t5781_TI,
	&t5782_TI,
	&t5783_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3789_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5781_TI, 21},
	{ &t5782_TI, 28},
	{ &t5783_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3789_0_0_0;
extern Il2CppType t3789_1_0_0;
#include "t551.h"
extern TypeInfo t551_TI;
extern TypeInfo t348_TI;
TypeInfo t3789_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DblClickSnapping[]", "", t3789_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t551_TI, t3789_ITIs, t3789_VT, &EmptyCustomAttributesCache, &t348_TI, &t3789_0_0_0, &t3789_1_0_0, t3789_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3790_TI;



// Metadata Definition UnityEngine.Events.PersistentListenerMode[]
static MethodInfo* t3790_MIs[] =
{
	NULL
};
extern MethodInfo m23029_MI;
extern MethodInfo m23030_MI;
extern MethodInfo m23031_MI;
extern MethodInfo m23032_MI;
extern MethodInfo m23033_MI;
extern MethodInfo m23034_MI;
extern MethodInfo m23028_MI;
extern MethodInfo m23036_MI;
extern MethodInfo m23037_MI;
static MethodInfo* t3790_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23029_MI,
	&m5907_MI,
	&m23030_MI,
	&m23031_MI,
	&m23032_MI,
	&m23033_MI,
	&m23034_MI,
	&m5908_MI,
	&m23028_MI,
	&m23036_MI,
	&m23037_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5784_TI;
extern TypeInfo t5785_TI;
extern TypeInfo t5786_TI;
static TypeInfo* t3790_ITIs[] = 
{
	&t5784_TI,
	&t5785_TI,
	&t5786_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3790_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5784_TI, 21},
	{ &t5785_TI, 28},
	{ &t5786_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3790_0_0_0;
extern Il2CppType t3790_1_0_0;
#include "t554.h"
extern TypeInfo t554_TI;
TypeInfo t3790_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PersistentListenerMode[]", "UnityEngine.Events", t3790_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t554_TI, t3790_ITIs, t3790_VT, &EmptyCustomAttributesCache, &t44_TI, &t3790_0_0_0, &t3790_1_0_0, t3790_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3791_TI;



// Metadata Definition UnityEngine.Events.UnityEventCallState[]
static MethodInfo* t3791_MIs[] =
{
	NULL
};
extern MethodInfo m23040_MI;
extern MethodInfo m23041_MI;
extern MethodInfo m23042_MI;
extern MethodInfo m23043_MI;
extern MethodInfo m23044_MI;
extern MethodInfo m23045_MI;
extern MethodInfo m23039_MI;
extern MethodInfo m23047_MI;
extern MethodInfo m23048_MI;
static MethodInfo* t3791_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23040_MI,
	&m5907_MI,
	&m23041_MI,
	&m23042_MI,
	&m23043_MI,
	&m23044_MI,
	&m23045_MI,
	&m5908_MI,
	&m23039_MI,
	&m23047_MI,
	&m23048_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5787_TI;
extern TypeInfo t5788_TI;
extern TypeInfo t5789_TI;
static TypeInfo* t3791_ITIs[] = 
{
	&t5787_TI,
	&t5788_TI,
	&t5789_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3791_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5787_TI, 21},
	{ &t5788_TI, 28},
	{ &t5789_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3791_0_0_0;
extern Il2CppType t3791_1_0_0;
#include "t564.h"
extern TypeInfo t564_TI;
TypeInfo t3791_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEventCallState[]", "UnityEngine.Events", t3791_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t564_TI, t3791_ITIs, t3791_VT, &EmptyCustomAttributesCache, &t44_TI, &t3791_0_0_0, &t3791_1_0_0, t3791_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3132_TI;



// Metadata Definition UnityEngine.Events.PersistentCall[]
static MethodInfo* t3132_MIs[] =
{
	NULL
};
extern MethodInfo m23052_MI;
extern MethodInfo m23053_MI;
extern MethodInfo m23054_MI;
extern MethodInfo m23055_MI;
extern MethodInfo m23056_MI;
extern MethodInfo m23057_MI;
extern MethodInfo m23051_MI;
extern MethodInfo m23059_MI;
extern MethodInfo m23060_MI;
static MethodInfo* t3132_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23052_MI,
	&m5907_MI,
	&m23053_MI,
	&m23054_MI,
	&m23055_MI,
	&m23056_MI,
	&m23057_MI,
	&m5908_MI,
	&m23051_MI,
	&m23059_MI,
	&m23060_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t3134_TI;
extern TypeInfo t3140_TI;
extern TypeInfo t3135_TI;
static TypeInfo* t3132_ITIs[] = 
{
	&t3134_TI,
	&t3140_TI,
	&t3135_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3132_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3134_TI, 21},
	{ &t3140_TI, 28},
	{ &t3135_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3132_0_0_0;
extern Il2CppType t3132_1_0_0;
struct t565;
extern TypeInfo t565_TI;
extern CustomAttributesCache t565__CustomAttributeCache_m_Target;
extern CustomAttributesCache t565__CustomAttributeCache_m_MethodName;
extern CustomAttributesCache t565__CustomAttributeCache_m_Mode;
extern CustomAttributesCache t565__CustomAttributeCache_m_Arguments;
extern CustomAttributesCache t565__CustomAttributeCache_m_CallState;
TypeInfo t3132_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "PersistentCall[]", "UnityEngine.Events", t3132_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t565_TI, t3132_ITIs, t3132_VT, &EmptyCustomAttributesCache, &t3132_TI, &t3132_0_0_0, &t3132_1_0_0, t3132_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t565 *), -1, 0, 0, -1, 1056768, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3146_TI;



// Metadata Definition UnityEngine.Events.BaseInvokableCall[]
static MethodInfo* t3146_MIs[] =
{
	NULL
};
extern MethodInfo m23078_MI;
extern MethodInfo m23079_MI;
extern MethodInfo m23080_MI;
extern MethodInfo m23081_MI;
extern MethodInfo m23082_MI;
extern MethodInfo m23083_MI;
extern MethodInfo m23077_MI;
extern MethodInfo m23085_MI;
extern MethodInfo m23086_MI;
static MethodInfo* t3146_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23078_MI,
	&m5907_MI,
	&m23079_MI,
	&m23080_MI,
	&m23081_MI,
	&m23082_MI,
	&m23083_MI,
	&m5908_MI,
	&m23077_MI,
	&m23085_MI,
	&m23086_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t3148_TI;
extern TypeInfo t3154_TI;
extern TypeInfo t3149_TI;
static TypeInfo* t3146_ITIs[] = 
{
	&t3148_TI,
	&t3154_TI,
	&t3149_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3146_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3148_TI, 21},
	{ &t3154_TI, 28},
	{ &t3149_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3146_0_0_0;
extern Il2CppType t3146_1_0_0;
struct t556;
extern TypeInfo t556_TI;
TypeInfo t3146_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "BaseInvokableCall[]", "UnityEngine.Events", t3146_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t556_TI, t3146_ITIs, t3146_VT, &EmptyCustomAttributesCache, &t3146_TI, &t3146_0_0_0, &t3146_1_0_0, t3146_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t556 *), -1, 0, 0, -1, 1048704, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t771_TI;



// Metadata Definition System.Boolean[]
static MethodInfo* t771_MIs[] =
{
	NULL
};
extern MethodInfo m23104_MI;
extern MethodInfo m23105_MI;
extern MethodInfo m23106_MI;
extern MethodInfo m23107_MI;
extern MethodInfo m23108_MI;
extern MethodInfo m23109_MI;
extern MethodInfo m23103_MI;
extern MethodInfo m23111_MI;
extern MethodInfo m23112_MI;
extern MethodInfo m23115_MI;
extern MethodInfo m23116_MI;
extern MethodInfo m23117_MI;
extern MethodInfo m23118_MI;
extern MethodInfo m23119_MI;
extern MethodInfo m23120_MI;
extern MethodInfo m23114_MI;
extern MethodInfo m23122_MI;
extern MethodInfo m23123_MI;
extern MethodInfo m23126_MI;
extern MethodInfo m23127_MI;
extern MethodInfo m23128_MI;
extern MethodInfo m23129_MI;
extern MethodInfo m23130_MI;
extern MethodInfo m23131_MI;
extern MethodInfo m23125_MI;
extern MethodInfo m23133_MI;
extern MethodInfo m23134_MI;
static MethodInfo* t771_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23104_MI,
	&m5907_MI,
	&m23105_MI,
	&m23106_MI,
	&m23107_MI,
	&m23108_MI,
	&m23109_MI,
	&m5908_MI,
	&m23103_MI,
	&m23111_MI,
	&m23112_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m23115_MI,
	&m5907_MI,
	&m23116_MI,
	&m23117_MI,
	&m23118_MI,
	&m23119_MI,
	&m23120_MI,
	&m5908_MI,
	&m23114_MI,
	&m23122_MI,
	&m23123_MI,
	&m5905_MI,
	&m5906_MI,
	&m23126_MI,
	&m5907_MI,
	&m23127_MI,
	&m23128_MI,
	&m23129_MI,
	&m23130_MI,
	&m23131_MI,
	&m5908_MI,
	&m23125_MI,
	&m23133_MI,
	&m23134_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5790_TI;
extern TypeInfo t5791_TI;
extern TypeInfo t5792_TI;
extern TypeInfo t5793_TI;
extern TypeInfo t5794_TI;
extern TypeInfo t5795_TI;
extern TypeInfo t5796_TI;
extern TypeInfo t5797_TI;
extern TypeInfo t5798_TI;
static TypeInfo* t771_ITIs[] = 
{
	&t5790_TI,
	&t5791_TI,
	&t5792_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5793_TI,
	&t5794_TI,
	&t5795_TI,
	&t5796_TI,
	&t5797_TI,
	&t5798_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t771_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5790_TI, 21},
	{ &t5791_TI, 28},
	{ &t5792_TI, 33},
	{ &t5110_TI, 34},
	{ &t5111_TI, 41},
	{ &t5112_TI, 46},
	{ &t5113_TI, 47},
	{ &t5114_TI, 54},
	{ &t5115_TI, 59},
	{ &t5793_TI, 60},
	{ &t5794_TI, 67},
	{ &t5795_TI, 72},
	{ &t5796_TI, 73},
	{ &t5797_TI, 80},
	{ &t5798_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t771_0_0_0;
extern Il2CppType t771_1_0_0;
#include "t40.h"
extern TypeInfo t40_TI;
extern CustomAttributesCache t40__CustomAttributeCache;
TypeInfo t771_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Boolean[]", "System", t771_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t40_TI, t771_ITIs, t771_VT, &EmptyCustomAttributesCache, &t771_TI, &t771_0_0_0, &t771_1_0_0, t771_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (bool), -1, sizeof(t771_SFs), 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3548_TI;



// Metadata Definition System.IComparable`1<System.Boolean>[]
static MethodInfo* t3548_MIs[] =
{
	NULL
};
static MethodInfo* t3548_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23115_MI,
	&m5907_MI,
	&m23116_MI,
	&m23117_MI,
	&m23118_MI,
	&m23119_MI,
	&m23120_MI,
	&m5908_MI,
	&m23114_MI,
	&m23122_MI,
	&m23123_MI,
};
static TypeInfo* t3548_ITIs[] = 
{
	&t5793_TI,
	&t5794_TI,
	&t5795_TI,
};
static Il2CppInterfaceOffsetPair t3548_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5793_TI, 21},
	{ &t5794_TI, 28},
	{ &t5795_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3548_0_0_0;
extern Il2CppType t3548_1_0_0;
struct t1759;
extern TypeInfo t1759_TI;
TypeInfo t3548_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3548_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1759_TI, t3548_ITIs, t3548_VT, &EmptyCustomAttributesCache, &t3548_TI, &t3548_0_0_0, &t3548_1_0_0, t3548_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3549_TI;



// Metadata Definition System.IEquatable`1<System.Boolean>[]
static MethodInfo* t3549_MIs[] =
{
	NULL
};
static MethodInfo* t3549_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23126_MI,
	&m5907_MI,
	&m23127_MI,
	&m23128_MI,
	&m23129_MI,
	&m23130_MI,
	&m23131_MI,
	&m5908_MI,
	&m23125_MI,
	&m23133_MI,
	&m23134_MI,
};
static TypeInfo* t3549_ITIs[] = 
{
	&t5796_TI,
	&t5797_TI,
	&t5798_TI,
};
static Il2CppInterfaceOffsetPair t3549_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5796_TI, 21},
	{ &t5797_TI, 28},
	{ &t5798_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3549_0_0_0;
extern Il2CppType t3549_1_0_0;
struct t1760;
extern TypeInfo t1760_TI;
TypeInfo t3549_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3549_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1760_TI, t3549_ITIs, t3549_VT, &EmptyCustomAttributesCache, &t3549_TI, &t3549_0_0_0, &t3549_1_0_0, t3549_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3792_TI;



// Metadata Definition UnityEngine.UserAuthorizationDialog[]
static MethodInfo* t3792_MIs[] =
{
	NULL
};
extern MethodInfo m23137_MI;
extern MethodInfo m23138_MI;
extern MethodInfo m23139_MI;
extern MethodInfo m23140_MI;
extern MethodInfo m23141_MI;
extern MethodInfo m23142_MI;
extern MethodInfo m23136_MI;
extern MethodInfo m23144_MI;
extern MethodInfo m23145_MI;
extern MethodInfo m19513_MI;
extern MethodInfo m19514_MI;
extern MethodInfo m19515_MI;
extern MethodInfo m19516_MI;
extern MethodInfo m19517_MI;
extern MethodInfo m19518_MI;
extern MethodInfo m19512_MI;
extern MethodInfo m19520_MI;
extern MethodInfo m19521_MI;
static MethodInfo* t3792_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23137_MI,
	&m5907_MI,
	&m23138_MI,
	&m23139_MI,
	&m23140_MI,
	&m23141_MI,
	&m23142_MI,
	&m5908_MI,
	&m23136_MI,
	&m23144_MI,
	&m23145_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5799_TI;
extern TypeInfo t5800_TI;
extern TypeInfo t5801_TI;
extern TypeInfo t5059_TI;
extern TypeInfo t5060_TI;
extern TypeInfo t5061_TI;
static TypeInfo* t3792_ITIs[] = 
{
	&t5799_TI,
	&t5800_TI,
	&t5801_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3792_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5799_TI, 21},
	{ &t5800_TI, 28},
	{ &t5801_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3792_0_0_0;
extern Il2CppType t3792_1_0_0;
struct t575;
extern TypeInfo t575_TI;
extern CustomAttributesCache t575__CustomAttributeCache;
TypeInfo t3792_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UserAuthorizationDialog[]", "UnityEngine", t3792_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t575_TI, t3792_ITIs, t3792_VT, &EmptyCustomAttributesCache, &t3792_TI, &t3792_0_0_0, &t3792_1_0_0, t3792_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t575 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3793_TI;



// Metadata Definition UnityEngine.Internal.DefaultValueAttribute[]
static MethodInfo* t3793_MIs[] =
{
	NULL
};
extern MethodInfo m23149_MI;
extern MethodInfo m23150_MI;
extern MethodInfo m23151_MI;
extern MethodInfo m23152_MI;
extern MethodInfo m23153_MI;
extern MethodInfo m23154_MI;
extern MethodInfo m23148_MI;
extern MethodInfo m23156_MI;
extern MethodInfo m23157_MI;
static MethodInfo* t3793_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23149_MI,
	&m5907_MI,
	&m23150_MI,
	&m23151_MI,
	&m23152_MI,
	&m23153_MI,
	&m23154_MI,
	&m5908_MI,
	&m23148_MI,
	&m23156_MI,
	&m23157_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5802_TI;
extern TypeInfo t5803_TI;
extern TypeInfo t5804_TI;
static TypeInfo* t3793_ITIs[] = 
{
	&t5802_TI,
	&t5803_TI,
	&t5804_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3793_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5802_TI, 21},
	{ &t5803_TI, 28},
	{ &t5804_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3793_0_0_0;
extern Il2CppType t3793_1_0_0;
struct t576;
extern TypeInfo t576_TI;
extern CustomAttributesCache t576__CustomAttributeCache;
TypeInfo t3793_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "DefaultValueAttribute[]", "UnityEngine.Internal", t3793_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t576_TI, t3793_ITIs, t3793_VT, &EmptyCustomAttributesCache, &t3793_TI, &t3793_0_0_0, &t3793_1_0_0, t3793_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t576 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3794_TI;



// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute[]
static MethodInfo* t3794_MIs[] =
{
	NULL
};
extern MethodInfo m23160_MI;
extern MethodInfo m23161_MI;
extern MethodInfo m23162_MI;
extern MethodInfo m23163_MI;
extern MethodInfo m23164_MI;
extern MethodInfo m23165_MI;
extern MethodInfo m23159_MI;
extern MethodInfo m23167_MI;
extern MethodInfo m23168_MI;
static MethodInfo* t3794_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23160_MI,
	&m5907_MI,
	&m23161_MI,
	&m23162_MI,
	&m23163_MI,
	&m23164_MI,
	&m23165_MI,
	&m5908_MI,
	&m23159_MI,
	&m23167_MI,
	&m23168_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5805_TI;
extern TypeInfo t5806_TI;
extern TypeInfo t5807_TI;
static TypeInfo* t3794_ITIs[] = 
{
	&t5805_TI,
	&t5806_TI,
	&t5807_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3794_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5805_TI, 21},
	{ &t5806_TI, 28},
	{ &t5807_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3794_0_0_0;
extern Il2CppType t3794_1_0_0;
struct t577;
extern TypeInfo t577_TI;
extern CustomAttributesCache t577__CustomAttributeCache;
TypeInfo t3794_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ExcludeFromDocsAttribute[]", "UnityEngine.Internal", t3794_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t577_TI, t3794_ITIs, t3794_VT, &EmptyCustomAttributesCache, &t3794_TI, &t3794_0_0_0, &t3794_1_0_0, t3794_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t577 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3795_TI;



// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute[]
static MethodInfo* t3795_MIs[] =
{
	NULL
};
extern MethodInfo m23171_MI;
extern MethodInfo m23172_MI;
extern MethodInfo m23173_MI;
extern MethodInfo m23174_MI;
extern MethodInfo m23175_MI;
extern MethodInfo m23176_MI;
extern MethodInfo m23170_MI;
extern MethodInfo m23178_MI;
extern MethodInfo m23179_MI;
static MethodInfo* t3795_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23171_MI,
	&m5907_MI,
	&m23172_MI,
	&m23173_MI,
	&m23174_MI,
	&m23175_MI,
	&m23176_MI,
	&m5908_MI,
	&m23170_MI,
	&m23178_MI,
	&m23179_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5808_TI;
extern TypeInfo t5809_TI;
extern TypeInfo t5810_TI;
static TypeInfo* t3795_ITIs[] = 
{
	&t5808_TI,
	&t5809_TI,
	&t5810_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3795_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5808_TI, 21},
	{ &t5809_TI, 28},
	{ &t5810_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3795_0_0_0;
extern Il2CppType t3795_1_0_0;
struct t299;
extern TypeInfo t299_TI;
extern CustomAttributesCache t299__CustomAttributeCache;
TypeInfo t3795_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "FormerlySerializedAsAttribute[]", "UnityEngine.Serialization", t3795_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t299_TI, t3795_ITIs, t3795_VT, &EmptyCustomAttributesCache, &t3795_TI, &t3795_0_0_0, &t3795_1_0_0, t3795_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t299 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3796_TI;



// Metadata Definition UnityEngineInternal.TypeInferenceRules[]
static MethodInfo* t3796_MIs[] =
{
	NULL
};
extern MethodInfo m23182_MI;
extern MethodInfo m23183_MI;
extern MethodInfo m23184_MI;
extern MethodInfo m23185_MI;
extern MethodInfo m23186_MI;
extern MethodInfo m23187_MI;
extern MethodInfo m23181_MI;
extern MethodInfo m23189_MI;
extern MethodInfo m23190_MI;
static MethodInfo* t3796_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23182_MI,
	&m5907_MI,
	&m23183_MI,
	&m23184_MI,
	&m23185_MI,
	&m23186_MI,
	&m23187_MI,
	&m5908_MI,
	&m23181_MI,
	&m23189_MI,
	&m23190_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5811_TI;
extern TypeInfo t5812_TI;
extern TypeInfo t5813_TI;
static TypeInfo* t3796_ITIs[] = 
{
	&t5811_TI,
	&t5812_TI,
	&t5813_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3796_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5811_TI, 21},
	{ &t5812_TI, 28},
	{ &t5813_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3796_0_0_0;
extern Il2CppType t3796_1_0_0;
#include "t578.h"
extern TypeInfo t578_TI;
TypeInfo t3796_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TypeInferenceRules[]", "UnityEngineInternal", t3796_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t578_TI, t3796_ITIs, t3796_VT, &EmptyCustomAttributesCache, &t44_TI, &t3796_0_0_0, &t3796_1_0_0, t3796_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3797_TI;



// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute[]
static MethodInfo* t3797_MIs[] =
{
	NULL
};
extern MethodInfo m23193_MI;
extern MethodInfo m23194_MI;
extern MethodInfo m23195_MI;
extern MethodInfo m23196_MI;
extern MethodInfo m23197_MI;
extern MethodInfo m23198_MI;
extern MethodInfo m23192_MI;
extern MethodInfo m23200_MI;
extern MethodInfo m23201_MI;
static MethodInfo* t3797_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23193_MI,
	&m5907_MI,
	&m23194_MI,
	&m23195_MI,
	&m23196_MI,
	&m23197_MI,
	&m23198_MI,
	&m5908_MI,
	&m23192_MI,
	&m23200_MI,
	&m23201_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5814_TI;
extern TypeInfo t5815_TI;
extern TypeInfo t5816_TI;
static TypeInfo* t3797_ITIs[] = 
{
	&t5814_TI,
	&t5815_TI,
	&t5816_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3797_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5814_TI, 21},
	{ &t5815_TI, 28},
	{ &t5816_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3797_0_0_0;
extern Il2CppType t3797_1_0_0;
struct t579;
extern TypeInfo t579_TI;
extern CustomAttributesCache t579__CustomAttributeCache;
TypeInfo t3797_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TypeInferenceRuleAttribute[]", "UnityEngineInternal", t3797_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t579_TI, t3797_ITIs, t3797_VT, &EmptyCustomAttributesCache, &t3797_TI, &t3797_0_0_0, &t3797_1_0_0, t3797_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t579 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#include "System.Core_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3895_TI;



// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute[]
static MethodInfo* t3895_MIs[] =
{
	NULL
};
extern MethodInfo m23204_MI;
extern MethodInfo m23205_MI;
extern MethodInfo m23206_MI;
extern MethodInfo m23207_MI;
extern MethodInfo m23208_MI;
extern MethodInfo m23209_MI;
extern MethodInfo m23203_MI;
extern MethodInfo m23211_MI;
extern MethodInfo m23212_MI;
static MethodInfo* t3895_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23204_MI,
	&m5907_MI,
	&m23205_MI,
	&m23206_MI,
	&m23207_MI,
	&m23208_MI,
	&m23209_MI,
	&m5908_MI,
	&m23203_MI,
	&m23211_MI,
	&m23212_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5817_TI;
extern TypeInfo t5818_TI;
extern TypeInfo t5819_TI;
static TypeInfo* t3895_ITIs[] = 
{
	&t5817_TI,
	&t5818_TI,
	&t5819_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3895_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5817_TI, 21},
	{ &t5818_TI, 28},
	{ &t5819_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t3895_0_0_0;
extern Il2CppType t3895_1_0_0;
struct t686;
extern TypeInfo t686_TI;
extern CustomAttributesCache t686__CustomAttributeCache;
TypeInfo t3895_TI = 
{
	&g_System_Core_dll_Image, NULL, "ExtensionAttribute[]", "System.Runtime.CompilerServices", t3895_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t686_TI, t3895_ITIs, t3895_VT, &EmptyCustomAttributesCache, &t3895_TI, &t3895_0_0_0, &t3895_1_0_0, t3895_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t686 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#include "System_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3896_TI;



// Metadata Definition System.MonoTODOAttribute[]
static MethodInfo* t3896_MIs[] =
{
	NULL
};
extern MethodInfo m23215_MI;
extern MethodInfo m23216_MI;
extern MethodInfo m23217_MI;
extern MethodInfo m23218_MI;
extern MethodInfo m23219_MI;
extern MethodInfo m23220_MI;
extern MethodInfo m23214_MI;
extern MethodInfo m23222_MI;
extern MethodInfo m23223_MI;
static MethodInfo* t3896_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23215_MI,
	&m5907_MI,
	&m23216_MI,
	&m23217_MI,
	&m23218_MI,
	&m23219_MI,
	&m23220_MI,
	&m5908_MI,
	&m23214_MI,
	&m23222_MI,
	&m23223_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5820_TI;
extern TypeInfo t5821_TI;
extern TypeInfo t5822_TI;
static TypeInfo* t3896_ITIs[] = 
{
	&t5820_TI,
	&t5821_TI,
	&t5822_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3896_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5820_TI, 21},
	{ &t5821_TI, 28},
	{ &t5822_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3896_0_0_0;
extern Il2CppType t3896_1_0_0;
struct t715;
extern TypeInfo t715_TI;
extern CustomAttributesCache t715__CustomAttributeCache;
TypeInfo t3896_TI = 
{
	&g_System_dll_Image, NULL, "MonoTODOAttribute[]", "System", t3896_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t715_TI, t3896_ITIs, t3896_VT, &EmptyCustomAttributesCache, &t3896_TI, &t3896_0_0_0, &t3896_1_0_0, t3896_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t715 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3897_TI;



// Metadata Definition System.ComponentModel.EditorBrowsableAttribute[]
static MethodInfo* t3897_MIs[] =
{
	NULL
};
extern MethodInfo m23226_MI;
extern MethodInfo m23227_MI;
extern MethodInfo m23228_MI;
extern MethodInfo m23229_MI;
extern MethodInfo m23230_MI;
extern MethodInfo m23231_MI;
extern MethodInfo m23225_MI;
extern MethodInfo m23233_MI;
extern MethodInfo m23234_MI;
static MethodInfo* t3897_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23226_MI,
	&m5907_MI,
	&m23227_MI,
	&m23228_MI,
	&m23229_MI,
	&m23230_MI,
	&m23231_MI,
	&m5908_MI,
	&m23225_MI,
	&m23233_MI,
	&m23234_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5823_TI;
extern TypeInfo t5824_TI;
extern TypeInfo t5825_TI;
static TypeInfo* t3897_ITIs[] = 
{
	&t5823_TI,
	&t5824_TI,
	&t5825_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3897_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5823_TI, 21},
	{ &t5824_TI, 28},
	{ &t5825_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3897_0_0_0;
extern Il2CppType t3897_1_0_0;
struct t606;
extern TypeInfo t606_TI;
extern CustomAttributesCache t606__CustomAttributeCache;
TypeInfo t3897_TI = 
{
	&g_System_dll_Image, NULL, "EditorBrowsableAttribute[]", "System.ComponentModel", t3897_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t606_TI, t3897_ITIs, t3897_VT, &EmptyCustomAttributesCache, &t3897_TI, &t3897_0_0_0, &t3897_1_0_0, t3897_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t606 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3898_TI;



// Metadata Definition System.ComponentModel.EditorBrowsableState[]
static MethodInfo* t3898_MIs[] =
{
	NULL
};
extern MethodInfo m23237_MI;
extern MethodInfo m23238_MI;
extern MethodInfo m23239_MI;
extern MethodInfo m23240_MI;
extern MethodInfo m23241_MI;
extern MethodInfo m23242_MI;
extern MethodInfo m23236_MI;
extern MethodInfo m23244_MI;
extern MethodInfo m23245_MI;
static MethodInfo* t3898_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23237_MI,
	&m5907_MI,
	&m23238_MI,
	&m23239_MI,
	&m23240_MI,
	&m23241_MI,
	&m23242_MI,
	&m5908_MI,
	&m23236_MI,
	&m23244_MI,
	&m23245_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5826_TI;
extern TypeInfo t5827_TI;
extern TypeInfo t5828_TI;
static TypeInfo* t3898_ITIs[] = 
{
	&t5826_TI,
	&t5827_TI,
	&t5828_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3898_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5826_TI, 21},
	{ &t5827_TI, 28},
	{ &t5828_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3898_0_0_0;
extern Il2CppType t3898_1_0_0;
#include "t737.h"
extern TypeInfo t737_TI;
TypeInfo t3898_TI = 
{
	&g_System_dll_Image, NULL, "EditorBrowsableState[]", "System.ComponentModel", t3898_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t737_TI, t3898_ITIs, t3898_VT, &EmptyCustomAttributesCache, &t44_TI, &t3898_0_0_0, &t3898_1_0_0, t3898_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3899_TI;



// Metadata Definition System.ComponentModel.TypeConverterAttribute[]
static MethodInfo* t3899_MIs[] =
{
	NULL
};
extern MethodInfo m23248_MI;
extern MethodInfo m23249_MI;
extern MethodInfo m23250_MI;
extern MethodInfo m23251_MI;
extern MethodInfo m23252_MI;
extern MethodInfo m23253_MI;
extern MethodInfo m23247_MI;
extern MethodInfo m23255_MI;
extern MethodInfo m23256_MI;
static MethodInfo* t3899_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23248_MI,
	&m5907_MI,
	&m23249_MI,
	&m23250_MI,
	&m23251_MI,
	&m23252_MI,
	&m23253_MI,
	&m5908_MI,
	&m23247_MI,
	&m23255_MI,
	&m23256_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5829_TI;
extern TypeInfo t5830_TI;
extern TypeInfo t5831_TI;
static TypeInfo* t3899_ITIs[] = 
{
	&t5829_TI,
	&t5830_TI,
	&t5831_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3899_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5829_TI, 21},
	{ &t5830_TI, 28},
	{ &t5831_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3899_0_0_0;
extern Il2CppType t3899_1_0_0;
struct t739;
extern TypeInfo t739_TI;
extern CustomAttributesCache t739__CustomAttributeCache;
TypeInfo t3899_TI = 
{
	&g_System_dll_Image, NULL, "TypeConverterAttribute[]", "System.ComponentModel", t3899_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t739_TI, t3899_ITIs, t3899_VT, &EmptyCustomAttributesCache, &t3899_TI, &t3899_0_0_0, &t3899_1_0_0, t3899_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t739 *), -1, sizeof(t3899_SFs), 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3900_TI;



// Metadata Definition System.Net.Security.AuthenticationLevel[]
static MethodInfo* t3900_MIs[] =
{
	NULL
};
extern MethodInfo m23259_MI;
extern MethodInfo m23260_MI;
extern MethodInfo m23261_MI;
extern MethodInfo m23262_MI;
extern MethodInfo m23263_MI;
extern MethodInfo m23264_MI;
extern MethodInfo m23258_MI;
extern MethodInfo m23266_MI;
extern MethodInfo m23267_MI;
static MethodInfo* t3900_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23259_MI,
	&m5907_MI,
	&m23260_MI,
	&m23261_MI,
	&m23262_MI,
	&m23263_MI,
	&m23264_MI,
	&m5908_MI,
	&m23258_MI,
	&m23266_MI,
	&m23267_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5832_TI;
extern TypeInfo t5833_TI;
extern TypeInfo t5834_TI;
static TypeInfo* t3900_ITIs[] = 
{
	&t5832_TI,
	&t5833_TI,
	&t5834_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3900_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5832_TI, 21},
	{ &t5833_TI, 28},
	{ &t5834_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3900_0_0_0;
extern Il2CppType t3900_1_0_0;
#include "t740.h"
extern TypeInfo t740_TI;
TypeInfo t3900_TI = 
{
	&g_System_dll_Image, NULL, "AuthenticationLevel[]", "System.Net.Security", t3900_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t740_TI, t3900_ITIs, t3900_VT, &EmptyCustomAttributesCache, &t44_TI, &t3900_0_0_0, &t3900_1_0_0, t3900_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3901_TI;



// Metadata Definition System.Net.Security.SslPolicyErrors[]
static MethodInfo* t3901_MIs[] =
{
	NULL
};
extern MethodInfo m23270_MI;
extern MethodInfo m23271_MI;
extern MethodInfo m23272_MI;
extern MethodInfo m23273_MI;
extern MethodInfo m23274_MI;
extern MethodInfo m23275_MI;
extern MethodInfo m23269_MI;
extern MethodInfo m23277_MI;
extern MethodInfo m23278_MI;
static MethodInfo* t3901_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23270_MI,
	&m5907_MI,
	&m23271_MI,
	&m23272_MI,
	&m23273_MI,
	&m23274_MI,
	&m23275_MI,
	&m5908_MI,
	&m23269_MI,
	&m23277_MI,
	&m23278_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5835_TI;
extern TypeInfo t5836_TI;
extern TypeInfo t5837_TI;
static TypeInfo* t3901_ITIs[] = 
{
	&t5835_TI,
	&t5836_TI,
	&t5837_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3901_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5835_TI, 21},
	{ &t5836_TI, 28},
	{ &t5837_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3901_0_0_0;
extern Il2CppType t3901_1_0_0;
#include "t741.h"
extern TypeInfo t741_TI;
extern CustomAttributesCache t741__CustomAttributeCache;
TypeInfo t3901_TI = 
{
	&g_System_dll_Image, NULL, "SslPolicyErrors[]", "System.Net.Security", t3901_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t741_TI, t3901_ITIs, t3901_VT, &EmptyCustomAttributesCache, &t44_TI, &t3901_0_0_0, &t3901_1_0_0, t3901_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3902_TI;



// Metadata Definition System.Net.Sockets.AddressFamily[]
static MethodInfo* t3902_MIs[] =
{
	NULL
};
extern MethodInfo m23281_MI;
extern MethodInfo m23282_MI;
extern MethodInfo m23283_MI;
extern MethodInfo m23284_MI;
extern MethodInfo m23285_MI;
extern MethodInfo m23286_MI;
extern MethodInfo m23280_MI;
extern MethodInfo m23288_MI;
extern MethodInfo m23289_MI;
static MethodInfo* t3902_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23281_MI,
	&m5907_MI,
	&m23282_MI,
	&m23283_MI,
	&m23284_MI,
	&m23285_MI,
	&m23286_MI,
	&m5908_MI,
	&m23280_MI,
	&m23288_MI,
	&m23289_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5838_TI;
extern TypeInfo t5839_TI;
extern TypeInfo t5840_TI;
static TypeInfo* t3902_ITIs[] = 
{
	&t5838_TI,
	&t5839_TI,
	&t5840_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3902_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5838_TI, 21},
	{ &t5839_TI, 28},
	{ &t5840_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3902_0_0_0;
extern Il2CppType t3902_1_0_0;
#include "t742.h"
extern TypeInfo t742_TI;
TypeInfo t3902_TI = 
{
	&g_System_dll_Image, NULL, "AddressFamily[]", "System.Net.Sockets", t3902_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t742_TI, t3902_ITIs, t3902_VT, &EmptyCustomAttributesCache, &t44_TI, &t3902_0_0_0, &t3902_1_0_0, t3902_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3550_TI;



// Metadata Definition System.IO.FileAccess[]
static MethodInfo* t3550_MIs[] =
{
	NULL
};
extern MethodInfo m23292_MI;
extern MethodInfo m23293_MI;
extern MethodInfo m23294_MI;
extern MethodInfo m23295_MI;
extern MethodInfo m23296_MI;
extern MethodInfo m23297_MI;
extern MethodInfo m23291_MI;
extern MethodInfo m23299_MI;
extern MethodInfo m23300_MI;
static MethodInfo* t3550_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23292_MI,
	&m5907_MI,
	&m23293_MI,
	&m23294_MI,
	&m23295_MI,
	&m23296_MI,
	&m23297_MI,
	&m5908_MI,
	&m23291_MI,
	&m23299_MI,
	&m23300_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5841_TI;
extern TypeInfo t5842_TI;
extern TypeInfo t5843_TI;
static TypeInfo* t3550_ITIs[] = 
{
	&t5841_TI,
	&t5842_TI,
	&t5843_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3550_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5841_TI, 21},
	{ &t5842_TI, 28},
	{ &t5843_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3550_0_0_0;
extern Il2CppType t3550_1_0_0;
#include "t751.h"
extern TypeInfo t751_TI;
extern CustomAttributesCache t751__CustomAttributeCache;
TypeInfo t3550_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FileAccess[]", "System.IO", t3550_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t751_TI, t3550_ITIs, t3550_VT, &EmptyCustomAttributesCache, &t44_TI, &t3550_0_0_0, &t3550_1_0_0, t3550_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t764_TI;



// Metadata Definition System.UInt16[]
static MethodInfo* t764_MIs[] =
{
	NULL
};
extern MethodInfo m23303_MI;
extern MethodInfo m23304_MI;
extern MethodInfo m23305_MI;
extern MethodInfo m23306_MI;
extern MethodInfo m23307_MI;
extern MethodInfo m23308_MI;
extern MethodInfo m23302_MI;
extern MethodInfo m23310_MI;
extern MethodInfo m23311_MI;
extern MethodInfo m23314_MI;
extern MethodInfo m23315_MI;
extern MethodInfo m23316_MI;
extern MethodInfo m23317_MI;
extern MethodInfo m23318_MI;
extern MethodInfo m23319_MI;
extern MethodInfo m23313_MI;
extern MethodInfo m23321_MI;
extern MethodInfo m23322_MI;
extern MethodInfo m23325_MI;
extern MethodInfo m23326_MI;
extern MethodInfo m23327_MI;
extern MethodInfo m23328_MI;
extern MethodInfo m23329_MI;
extern MethodInfo m23330_MI;
extern MethodInfo m23324_MI;
extern MethodInfo m23332_MI;
extern MethodInfo m23333_MI;
static MethodInfo* t764_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23303_MI,
	&m5907_MI,
	&m23304_MI,
	&m23305_MI,
	&m23306_MI,
	&m23307_MI,
	&m23308_MI,
	&m5908_MI,
	&m23302_MI,
	&m23310_MI,
	&m23311_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m23314_MI,
	&m5907_MI,
	&m23315_MI,
	&m23316_MI,
	&m23317_MI,
	&m23318_MI,
	&m23319_MI,
	&m5908_MI,
	&m23313_MI,
	&m23321_MI,
	&m23322_MI,
	&m5905_MI,
	&m5906_MI,
	&m23325_MI,
	&m5907_MI,
	&m23326_MI,
	&m23327_MI,
	&m23328_MI,
	&m23329_MI,
	&m23330_MI,
	&m5908_MI,
	&m23324_MI,
	&m23332_MI,
	&m23333_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5844_TI;
extern TypeInfo t5845_TI;
extern TypeInfo t5846_TI;
extern TypeInfo t5847_TI;
extern TypeInfo t5848_TI;
extern TypeInfo t5849_TI;
extern TypeInfo t5850_TI;
extern TypeInfo t5851_TI;
extern TypeInfo t5852_TI;
static TypeInfo* t764_ITIs[] = 
{
	&t5844_TI,
	&t5845_TI,
	&t5846_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5847_TI,
	&t5848_TI,
	&t5849_TI,
	&t5850_TI,
	&t5851_TI,
	&t5852_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t764_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5844_TI, 21},
	{ &t5845_TI, 28},
	{ &t5846_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5847_TI, 73},
	{ &t5848_TI, 80},
	{ &t5849_TI, 85},
	{ &t5850_TI, 86},
	{ &t5851_TI, 93},
	{ &t5852_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t764_0_0_0;
extern Il2CppType t764_1_0_0;
#include "t626.h"
extern TypeInfo t626_TI;
extern CustomAttributesCache t626__CustomAttributeCache;
extern CustomAttributesCache t626__CustomAttributeCache_m5517;
extern CustomAttributesCache t626__CustomAttributeCache_m5518;
extern CustomAttributesCache t626__CustomAttributeCache_m5519;
extern CustomAttributesCache t626__CustomAttributeCache_m5520;
TypeInfo t764_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UInt16[]", "System", t764_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t626_TI, t764_ITIs, t764_VT, &EmptyCustomAttributesCache, &t764_TI, &t764_0_0_0, &t764_1_0_0, t764_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint16_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3551_TI;



// Metadata Definition System.IComparable`1<System.UInt16>[]
static MethodInfo* t3551_MIs[] =
{
	NULL
};
static MethodInfo* t3551_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23314_MI,
	&m5907_MI,
	&m23315_MI,
	&m23316_MI,
	&m23317_MI,
	&m23318_MI,
	&m23319_MI,
	&m5908_MI,
	&m23313_MI,
	&m23321_MI,
	&m23322_MI,
};
static TypeInfo* t3551_ITIs[] = 
{
	&t5847_TI,
	&t5848_TI,
	&t5849_TI,
};
static Il2CppInterfaceOffsetPair t3551_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5847_TI, 21},
	{ &t5848_TI, 28},
	{ &t5849_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3551_0_0_0;
extern Il2CppType t3551_1_0_0;
struct t1730;
extern TypeInfo t1730_TI;
TypeInfo t3551_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3551_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1730_TI, t3551_ITIs, t3551_VT, &EmptyCustomAttributesCache, &t3551_TI, &t3551_0_0_0, &t3551_1_0_0, t3551_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3552_TI;



// Metadata Definition System.IEquatable`1<System.UInt16>[]
static MethodInfo* t3552_MIs[] =
{
	NULL
};
static MethodInfo* t3552_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23325_MI,
	&m5907_MI,
	&m23326_MI,
	&m23327_MI,
	&m23328_MI,
	&m23329_MI,
	&m23330_MI,
	&m5908_MI,
	&m23324_MI,
	&m23332_MI,
	&m23333_MI,
};
static TypeInfo* t3552_ITIs[] = 
{
	&t5850_TI,
	&t5851_TI,
	&t5852_TI,
};
static Il2CppInterfaceOffsetPair t3552_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5850_TI, 21},
	{ &t5851_TI, 28},
	{ &t5852_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3552_0_0_0;
extern Il2CppType t3552_1_0_0;
struct t1731;
extern TypeInfo t1731_TI;
TypeInfo t3552_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3552_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1731_TI, t3552_ITIs, t3552_VT, &EmptyCustomAttributesCache, &t3552_TI, &t3552_0_0_0, &t3552_1_0_0, t3552_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3903_TI;



// Metadata Definition System.Net.SecurityProtocolType[]
static MethodInfo* t3903_MIs[] =
{
	NULL
};
extern MethodInfo m23336_MI;
extern MethodInfo m23337_MI;
extern MethodInfo m23338_MI;
extern MethodInfo m23339_MI;
extern MethodInfo m23340_MI;
extern MethodInfo m23341_MI;
extern MethodInfo m23335_MI;
extern MethodInfo m23343_MI;
extern MethodInfo m23344_MI;
static MethodInfo* t3903_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23336_MI,
	&m5907_MI,
	&m23337_MI,
	&m23338_MI,
	&m23339_MI,
	&m23340_MI,
	&m23341_MI,
	&m5908_MI,
	&m23335_MI,
	&m23343_MI,
	&m23344_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5853_TI;
extern TypeInfo t5854_TI;
extern TypeInfo t5855_TI;
static TypeInfo* t3903_ITIs[] = 
{
	&t5853_TI,
	&t5854_TI,
	&t5855_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3903_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5853_TI, 21},
	{ &t5854_TI, 28},
	{ &t5855_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3903_0_0_0;
extern Il2CppType t3903_1_0_0;
#include "t766.h"
extern TypeInfo t766_TI;
extern CustomAttributesCache t766__CustomAttributeCache;
TypeInfo t3903_TI = 
{
	&g_System_dll_Image, NULL, "SecurityProtocolType[]", "System.Net", t3903_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t766_TI, t3903_ITIs, t3903_VT, &EmptyCustomAttributesCache, &t44_TI, &t3903_0_0_0, &t3903_1_0_0, t3903_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3191_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
static MethodInfo* t3191_MIs[] =
{
	NULL
};
extern MethodInfo m23347_MI;
extern MethodInfo m23348_MI;
extern MethodInfo m23349_MI;
extern MethodInfo m23350_MI;
extern MethodInfo m23351_MI;
extern MethodInfo m23352_MI;
extern MethodInfo m23346_MI;
extern MethodInfo m23354_MI;
extern MethodInfo m23355_MI;
static MethodInfo* t3191_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23347_MI,
	&m5907_MI,
	&m23348_MI,
	&m23349_MI,
	&m23350_MI,
	&m23351_MI,
	&m23352_MI,
	&m5908_MI,
	&m23346_MI,
	&m23354_MI,
	&m23355_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5856_TI;
extern TypeInfo t5857_TI;
extern TypeInfo t5858_TI;
static TypeInfo* t3191_ITIs[] = 
{
	&t5856_TI,
	&t5857_TI,
	&t5858_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3191_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5856_TI, 21},
	{ &t5857_TI, 28},
	{ &t5858_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3191_0_0_0;
extern Il2CppType t3191_1_0_0;
#include "t3192.h"
extern TypeInfo t3192_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t3191_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t3191_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t3192_TI, t3191_ITIs, t3191_VT, &EmptyCustomAttributesCache, &t3191_TI, &t3191_0_0_0, &t3191_1_0_0, t3191_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t3192 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3904_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.OpenFlags[]
static MethodInfo* t3904_MIs[] =
{
	NULL
};
extern MethodInfo m23365_MI;
extern MethodInfo m23366_MI;
extern MethodInfo m23367_MI;
extern MethodInfo m23368_MI;
extern MethodInfo m23369_MI;
extern MethodInfo m23370_MI;
extern MethodInfo m23364_MI;
extern MethodInfo m23372_MI;
extern MethodInfo m23373_MI;
static MethodInfo* t3904_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23365_MI,
	&m5907_MI,
	&m23366_MI,
	&m23367_MI,
	&m23368_MI,
	&m23369_MI,
	&m23370_MI,
	&m5908_MI,
	&m23364_MI,
	&m23372_MI,
	&m23373_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5859_TI;
extern TypeInfo t5860_TI;
extern TypeInfo t5861_TI;
static TypeInfo* t3904_ITIs[] = 
{
	&t5859_TI,
	&t5860_TI,
	&t5861_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3904_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5859_TI, 21},
	{ &t5860_TI, 28},
	{ &t5861_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3904_0_0_0;
extern Il2CppType t3904_1_0_0;
#include "t775.h"
extern TypeInfo t775_TI;
extern CustomAttributesCache t775__CustomAttributeCache;
TypeInfo t3904_TI = 
{
	&g_System_dll_Image, NULL, "OpenFlags[]", "System.Security.Cryptography.X509Certificates", t3904_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t775_TI, t3904_ITIs, t3904_VT, &EmptyCustomAttributesCache, &t44_TI, &t3904_0_0_0, &t3904_1_0_0, t3904_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t781_TI;



// Metadata Definition System.Byte[]
static MethodInfo* t781_MIs[] =
{
	NULL
};
extern MethodInfo m23376_MI;
extern MethodInfo m23377_MI;
extern MethodInfo m23378_MI;
extern MethodInfo m23379_MI;
extern MethodInfo m23380_MI;
extern MethodInfo m23381_MI;
extern MethodInfo m23375_MI;
extern MethodInfo m23383_MI;
extern MethodInfo m23384_MI;
extern MethodInfo m23387_MI;
extern MethodInfo m23388_MI;
extern MethodInfo m23389_MI;
extern MethodInfo m23390_MI;
extern MethodInfo m23391_MI;
extern MethodInfo m23392_MI;
extern MethodInfo m23386_MI;
extern MethodInfo m23394_MI;
extern MethodInfo m23395_MI;
extern MethodInfo m23398_MI;
extern MethodInfo m23399_MI;
extern MethodInfo m23400_MI;
extern MethodInfo m23401_MI;
extern MethodInfo m23402_MI;
extern MethodInfo m23403_MI;
extern MethodInfo m23397_MI;
extern MethodInfo m23405_MI;
extern MethodInfo m23406_MI;
static MethodInfo* t781_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23376_MI,
	&m5907_MI,
	&m23377_MI,
	&m23378_MI,
	&m23379_MI,
	&m23380_MI,
	&m23381_MI,
	&m5908_MI,
	&m23375_MI,
	&m23383_MI,
	&m23384_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m23387_MI,
	&m5907_MI,
	&m23388_MI,
	&m23389_MI,
	&m23390_MI,
	&m23391_MI,
	&m23392_MI,
	&m5908_MI,
	&m23386_MI,
	&m23394_MI,
	&m23395_MI,
	&m5905_MI,
	&m5906_MI,
	&m23398_MI,
	&m5907_MI,
	&m23399_MI,
	&m23400_MI,
	&m23401_MI,
	&m23402_MI,
	&m23403_MI,
	&m5908_MI,
	&m23397_MI,
	&m23405_MI,
	&m23406_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5862_TI;
extern TypeInfo t5863_TI;
extern TypeInfo t5864_TI;
extern TypeInfo t5865_TI;
extern TypeInfo t5866_TI;
extern TypeInfo t5867_TI;
extern TypeInfo t5868_TI;
extern TypeInfo t5869_TI;
extern TypeInfo t5870_TI;
static TypeInfo* t781_ITIs[] = 
{
	&t5862_TI,
	&t5863_TI,
	&t5864_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5865_TI,
	&t5866_TI,
	&t5867_TI,
	&t5868_TI,
	&t5869_TI,
	&t5870_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t781_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5862_TI, 21},
	{ &t5863_TI, 28},
	{ &t5864_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5865_TI, 73},
	{ &t5866_TI, 80},
	{ &t5867_TI, 85},
	{ &t5868_TI, 86},
	{ &t5869_TI, 93},
	{ &t5870_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t781_0_0_0;
extern Il2CppType t781_1_0_0;
#include "t348.h"
extern CustomAttributesCache t348__CustomAttributeCache;
TypeInfo t781_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Byte[]", "System", t781_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t348_TI, t781_ITIs, t781_VT, &EmptyCustomAttributesCache, &t781_TI, &t781_0_0_0, &t781_1_0_0, t781_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3553_TI;



// Metadata Definition System.IComparable`1<System.Byte>[]
static MethodInfo* t3553_MIs[] =
{
	NULL
};
static MethodInfo* t3553_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23387_MI,
	&m5907_MI,
	&m23388_MI,
	&m23389_MI,
	&m23390_MI,
	&m23391_MI,
	&m23392_MI,
	&m5908_MI,
	&m23386_MI,
	&m23394_MI,
	&m23395_MI,
};
static TypeInfo* t3553_ITIs[] = 
{
	&t5865_TI,
	&t5866_TI,
	&t5867_TI,
};
static Il2CppInterfaceOffsetPair t3553_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5865_TI, 21},
	{ &t5866_TI, 28},
	{ &t5867_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3553_0_0_0;
extern Il2CppType t3553_1_0_0;
struct t1721;
extern TypeInfo t1721_TI;
TypeInfo t3553_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3553_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1721_TI, t3553_ITIs, t3553_VT, &EmptyCustomAttributesCache, &t3553_TI, &t3553_0_0_0, &t3553_1_0_0, t3553_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3554_TI;



// Metadata Definition System.IEquatable`1<System.Byte>[]
static MethodInfo* t3554_MIs[] =
{
	NULL
};
static MethodInfo* t3554_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23398_MI,
	&m5907_MI,
	&m23399_MI,
	&m23400_MI,
	&m23401_MI,
	&m23402_MI,
	&m23403_MI,
	&m5908_MI,
	&m23397_MI,
	&m23405_MI,
	&m23406_MI,
};
static TypeInfo* t3554_ITIs[] = 
{
	&t5868_TI,
	&t5869_TI,
	&t5870_TI,
};
static Il2CppInterfaceOffsetPair t3554_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5868_TI, 21},
	{ &t5869_TI, 28},
	{ &t5870_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3554_0_0_0;
extern Il2CppType t3554_1_0_0;
struct t1722;
extern TypeInfo t1722_TI;
TypeInfo t3554_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3554_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1722_TI, t3554_ITIs, t3554_VT, &EmptyCustomAttributesCache, &t3554_TI, &t3554_0_0_0, &t3554_1_0_0, t3554_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3905_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.StoreLocation[]
static MethodInfo* t3905_MIs[] =
{
	NULL
};
extern MethodInfo m23409_MI;
extern MethodInfo m23410_MI;
extern MethodInfo m23411_MI;
extern MethodInfo m23412_MI;
extern MethodInfo m23413_MI;
extern MethodInfo m23414_MI;
extern MethodInfo m23408_MI;
extern MethodInfo m23416_MI;
extern MethodInfo m23417_MI;
static MethodInfo* t3905_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23409_MI,
	&m5907_MI,
	&m23410_MI,
	&m23411_MI,
	&m23412_MI,
	&m23413_MI,
	&m23414_MI,
	&m5908_MI,
	&m23408_MI,
	&m23416_MI,
	&m23417_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5871_TI;
extern TypeInfo t5872_TI;
extern TypeInfo t5873_TI;
static TypeInfo* t3905_ITIs[] = 
{
	&t5871_TI,
	&t5872_TI,
	&t5873_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3905_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5871_TI, 21},
	{ &t5872_TI, 28},
	{ &t5873_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3905_0_0_0;
extern Il2CppType t3905_1_0_0;
#include "t784.h"
extern TypeInfo t784_TI;
TypeInfo t3905_TI = 
{
	&g_System_dll_Image, NULL, "StoreLocation[]", "System.Security.Cryptography.X509Certificates", t3905_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t784_TI, t3905_ITIs, t3905_VT, &EmptyCustomAttributesCache, &t44_TI, &t3905_0_0_0, &t3905_1_0_0, t3905_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
