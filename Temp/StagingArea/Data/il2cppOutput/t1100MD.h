﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1100;
struct t996;

 void m5144 (t1100 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8253 (t1100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8254 (t1100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8255 (t1100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8256 (t1100 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8257 (t29 * __this, t996* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
