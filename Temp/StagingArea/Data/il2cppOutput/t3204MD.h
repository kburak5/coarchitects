﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3204;
struct t29;
struct t20;
#include "t775.h"

 void m17820 (t3204 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17821 (t3204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17822 (t3204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17823 (t3204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17824 (t3204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
