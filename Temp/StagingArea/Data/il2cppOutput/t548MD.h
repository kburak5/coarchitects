﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t548;
struct t295;
struct t1290;
struct t635;
struct t7;

 void m6783 (t548 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2920 (t548 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6784 (t548 * __this, t295 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6785 (t548 * __this, t295 * p0, int32_t p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6786 (t548 * __this, t295 * p0, int32_t p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6787 (t548 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1290* m6788 (t29 * __this, t295 * p0, int32_t p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2944 (t548 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t635 * m2935 (t548 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6789 (t548 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
