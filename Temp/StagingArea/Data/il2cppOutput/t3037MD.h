﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3037;
struct t29;
#include "t381.h"

 void m16649 (t3037 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16650 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16651 (t3037 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16652 (t3037 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3037 * m16653 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
