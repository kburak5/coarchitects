﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t295;
struct t7;
struct t733;
struct t292;
struct t636;
struct t42;
#include "t735.h"

 void m5100 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2945 (t295 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5271 (t295 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2947 (t295 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m2858 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2946 (t295 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6049 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1684 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2859 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2860 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2857 (t295 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2856 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6050 (t295 * __this, t292 * p0, t636 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m2861 (t295 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
