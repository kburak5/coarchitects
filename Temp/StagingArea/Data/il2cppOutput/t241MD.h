﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t241;
struct t2697;
struct t557;
struct t7;
struct t29;
struct t556;

 void m1928 (t241 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14664 (t241 * __this, t2697 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14665 (t241 * __this, t2697 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m1929 (t241 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m1930 (t241 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m14666 (t29 * __this, t2697 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1931 (t241 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
