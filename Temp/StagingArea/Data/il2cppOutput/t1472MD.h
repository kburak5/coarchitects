﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1472;
struct t7;
struct t1474;
struct t42;

 t7* m7962 (t1472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1474* m7963 (t1472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7964 (t1472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7965 (t1472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
