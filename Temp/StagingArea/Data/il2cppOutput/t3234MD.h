﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3234;
struct t29;
struct t20;
#include "t895.h"

 void m17966 (t3234 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17967 (t3234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17968 (t3234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17969 (t3234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t895  m17970 (t3234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
