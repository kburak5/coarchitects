﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t234;
struct t2;
struct t232;
struct t6;
struct t24;
struct t64;
struct t139;
struct t25;
#include "t231.h"
#include "t233.h"
#include "t140.h"

 void m893 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m894 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m895 (t234 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m896 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m897 (t234 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m898 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m899 (t234 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m900 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m901 (t234 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m902 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m903 (t234 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m904 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m905 (t234 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m906 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m907 (t234 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m908 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m909 (t234 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t232 * m910 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m911 (t234 * __this, t232 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m912 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m913 (t234 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m914 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m915 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m916 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m917 (t234 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m918 (t234 * __this, float p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m919 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m920 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m921 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m922 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m923 (t234 * __this, t6 * p0, t24 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m924 (t234 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m925 (t234 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m926 (t234 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m927 (t234 * __this, t64 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m928 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m929 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m930 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m931 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m932 (t234 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m933 (t234 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m934 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m935 (t234 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
