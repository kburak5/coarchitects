﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1696;
struct t1696_marshaled;

void t1696_marshal(const t1696& unmarshaled, t1696_marshaled& marshaled);
void t1696_marshal_back(const t1696_marshaled& marshaled, t1696& unmarshaled);
void t1696_marshal_cleanup(t1696_marshaled& marshaled);
