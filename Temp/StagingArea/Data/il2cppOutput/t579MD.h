﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t579;
struct t7;
#include "t578.h"

 void m2831 (t579 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2832 (t579 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2833 (t579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
