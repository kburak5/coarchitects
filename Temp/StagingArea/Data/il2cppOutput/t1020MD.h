﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1020;
struct t781;
struct t1040;
struct t1041;
struct t1025;
struct t1036;
struct t1033;
struct t1042;
#include "t1026.h"
#include "t1043.h"
#include "t1037.h"
#include "t1044.h"

 void m4704 (t1020 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4705 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4706 (t1020 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4707 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4708 (t1020 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4709 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4710 (t1020 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4711 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m4712 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4713 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4714 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4715 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4716 (t1020 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1040 * m4717 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1041 * m4718 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4719 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4720 (t1020 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4721 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4722 (t1020 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4723 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4724 (t1020 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4725 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4726 (t1020 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1025 * m4727 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4728 (t1020 * __this, t1025 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1036 * m4729 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m4730 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4731 (t1020 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m4732 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4733 (t1020 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4734 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4735 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4736 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4737 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4738 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4739 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4740 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4741 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4742 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4743 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4744 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4745 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4746 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4747 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4748 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4749 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4750 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4751 (t1020 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1033 * m4752 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4753 (t1020 * __this, t1033 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4754 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4755 (t1020 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4756 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4757 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4758 (t1020 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4759 (t1020 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1042 * m4760 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1042 * m4761 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1042 * m4762 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1042 * m4763 (t1020 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4764 (t1020 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4765 (t1020 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
