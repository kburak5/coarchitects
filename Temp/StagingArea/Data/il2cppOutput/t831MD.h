﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t831;
struct t29;
struct t830;
struct t20;
struct t136;

 void m3510 (t831 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3511 (t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3512 (t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3513 (t831 * __this, t830 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3514 (t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3515 (t831 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3516 (t831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
