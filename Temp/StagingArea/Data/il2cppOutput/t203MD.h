﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t203;
struct t203_marshaled;

 void m2072 (t203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2073 (t203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2074 (t203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t203_marshal(const t203& unmarshaled, t203_marshaled& marshaled);
void t203_marshal_back(const t203_marshaled& marshaled, t203& unmarshaled);
void t203_marshal_cleanup(t203_marshaled& marshaled);
