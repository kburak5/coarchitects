﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3333;
struct t29;
struct t20;
#include "t1285.h"

 void m18537 (t3333 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18538 (t3333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18539 (t3333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18540 (t3333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18541 (t3333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
