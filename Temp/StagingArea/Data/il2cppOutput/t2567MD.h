﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2567;
struct t29;
struct t364;
struct t2424;
struct t20;
struct t136;
struct t841;
#include "t2573.h"

 void m13830 (t2567 * __this, t364 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13831 (t2567 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13832 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13833 (t2567 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13834 (t2567 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m13835 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13836 (t2567 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13837 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13838 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13839 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13840 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13841 (t2567 * __this, t841* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2573  m13842 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13843 (t2567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
