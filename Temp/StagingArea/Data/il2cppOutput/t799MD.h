﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t799;
struct t29;
struct t745;
struct t762;

 void m3340 (t799 * __this, t762 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3341 (t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3342 (t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m3343 (t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3344 (t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
