﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t635;
struct t636;
struct t7;

 void m6775 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6776 (t635 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6777 (t29 * __this, int32_t p0, bool p1, t636 ** p2, int32_t* p3, int32_t* p4, t7** p5, int32_t* p6, int32_t* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2942 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2941 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6778 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6779 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m2936 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6780 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6781 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6782 (t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
