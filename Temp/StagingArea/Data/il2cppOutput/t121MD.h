﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t121;
struct t7;
struct t6;
struct t114;
#include "t120.h"
#include "t17.h"

 void m284 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m285 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m286 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m287 (t121 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m288 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m289 (t121 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m290 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m291 (t121 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m292 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m293 (t121 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m294 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m295 (t121 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m296 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m297 (t121 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m298 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m299 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m300 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m301 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m302 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m303 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m304 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m305 (t121 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m306 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m307 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m308 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m309 (t29 * __this, bool p0, bool p1, t6 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m310 (t121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m311 (t121 * __this, t114 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
