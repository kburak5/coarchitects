﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3255;
struct t29;
struct t20;
#include "t1021.h"

 void m18071 (t3255 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18072 (t3255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18073 (t3255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18074 (t3255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18075 (t3255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
