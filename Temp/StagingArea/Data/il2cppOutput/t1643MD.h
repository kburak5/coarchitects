﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1643;
struct t760;
struct t733;
struct t7;
#include "t1644.h"
#include "t735.h"

 void m9485 (t1643 * __this, int32_t p0, t760 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9486 (t1643 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9487 (t1643 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9488 (t1643 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
