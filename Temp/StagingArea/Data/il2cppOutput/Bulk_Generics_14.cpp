﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2774.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2774_TI;
#include "t2774MD.h"

#include "t21.h"
#include "t41.h"
#include "t557.h"
#include "t259.h"
#include "mscorlib_ArrayTypes.h"
#include "t29.h"
#include "t2775.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t259_TI;
extern TypeInfo t2775_TI;
extern TypeInfo t21_TI;
#include "t2775MD.h"
extern MethodInfo m15166_MI;
extern MethodInfo m15168_MI;

#include "t20.h"

// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t2774_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2774_TI, offsetof(t2774, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2774_FIs[] =
{
	&t2774_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t259_0_0_0;
extern Il2CppType t259_0_0_0;
static ParameterInfo t2774_m15164_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15164_GM;
MethodInfo m15164_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2774_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2774_m15164_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15164_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2774_m15165_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15165_GM;
MethodInfo m15165_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2774_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2774_m15165_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15165_GM};
static MethodInfo* t2774_MIs[] =
{
	&m15164_MI,
	&m15165_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m15165_MI;
extern MethodInfo m15169_MI;
static MethodInfo* t2774_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15165_MI,
	&m15169_MI,
};
extern Il2CppType t2776_0_0_0;
extern TypeInfo t2776_TI;
extern MethodInfo m21795_MI;
extern TypeInfo t259_TI;
extern MethodInfo m15171_MI;
extern TypeInfo t259_TI;
static Il2CppRGCTXData t2774_RGCTXData[8] = 
{
	&t2776_0_0_0/* Type Usage */,
	&t2776_TI/* Class Usage */,
	&m21795_MI/* Method Usage */,
	&t259_TI/* Class Usage */,
	&m15171_MI/* Method Usage */,
	&m15166_MI/* Method Usage */,
	&t259_TI/* Class Usage */,
	&m15168_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2774_0_0_0;
extern Il2CppType t2774_1_0_0;
struct t2774;
extern Il2CppGenericClass t2774_GC;
TypeInfo t2774_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2774_MIs, NULL, t2774_FIs, NULL, &t2775_TI, NULL, NULL, &t2774_TI, NULL, t2774_VT, &EmptyCustomAttributesCache, &t2774_TI, &t2774_0_0_0, &t2774_1_0_0, NULL, &t2774_GC, NULL, NULL, NULL, t2774_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2774), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2776.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t7.h"
#include "t305.h"
#include "t40.h"
extern TypeInfo t2776_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2776MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m21795(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType t2776_0_0_1;
FieldInfo t2775_f0_FieldInfo = 
{
	"Delegate", &t2776_0_0_1, &t2775_TI, offsetof(t2775, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2775_FIs[] =
{
	&t2775_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2775_m15166_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15166_GM;
MethodInfo m15166_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2775_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2775_m15166_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15166_GM};
extern Il2CppType t2776_0_0_0;
static ParameterInfo t2775_m15167_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2776_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15167_GM;
MethodInfo m15167_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2775_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2775_m15167_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15167_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2775_m15168_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15168_GM;
MethodInfo m15168_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2775_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2775_m15168_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15168_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2775_m15169_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15169_GM;
MethodInfo m15169_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2775_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2775_m15169_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15169_GM};
static MethodInfo* t2775_MIs[] =
{
	&m15166_MI,
	&m15167_MI,
	&m15168_MI,
	&m15169_MI,
	NULL
};
static MethodInfo* t2775_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15168_MI,
	&m15169_MI,
};
extern TypeInfo t2776_TI;
extern TypeInfo t259_TI;
static Il2CppRGCTXData t2775_RGCTXData[5] = 
{
	&t2776_0_0_0/* Type Usage */,
	&t2776_TI/* Class Usage */,
	&m21795_MI/* Method Usage */,
	&t259_TI/* Class Usage */,
	&m15171_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2775_0_0_0;
extern Il2CppType t2775_1_0_0;
extern TypeInfo t556_TI;
struct t2775;
extern Il2CppGenericClass t2775_GC;
TypeInfo t2775_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2775_MIs, NULL, t2775_FIs, NULL, &t556_TI, NULL, NULL, &t2775_TI, NULL, t2775_VT, &EmptyCustomAttributesCache, &t2775_TI, &t2775_0_0_0, &t2775_1_0_0, NULL, &t2775_GC, NULL, NULL, NULL, t2775_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2775), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2776_m15170_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15170_GM;
MethodInfo m15170_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2776_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2776_m15170_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15170_GM};
extern Il2CppType t259_0_0_0;
static ParameterInfo t2776_m15171_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15171_GM;
MethodInfo m15171_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2776_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2776_m15171_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15171_GM};
extern Il2CppType t259_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2776_m15172_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15172_GM;
MethodInfo m15172_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2776_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2776_m15172_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15172_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2776_m15173_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15173_GM;
MethodInfo m15173_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2776_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2776_m15173_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15173_GM};
static MethodInfo* t2776_MIs[] =
{
	&m15170_MI,
	&m15171_MI,
	&m15172_MI,
	&m15173_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m15172_MI;
extern MethodInfo m15173_MI;
static MethodInfo* t2776_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15171_MI,
	&m15172_MI,
	&m15173_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2776_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2776_1_0_0;
extern TypeInfo t195_TI;
struct t2776;
extern Il2CppGenericClass t2776_GC;
TypeInfo t2776_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2776_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2776_TI, NULL, t2776_VT, &EmptyCustomAttributesCache, &t2776_TI, &t2776_0_0_0, &t2776_1_0_0, t2776_IOs, &t2776_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2776), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t416_TI;

#include "t265.h"


// Metadata Definition System.IEquatable`1<UnityEngine.UI.LayoutRebuilder>
extern Il2CppType t265_0_0_0;
extern Il2CppType t265_0_0_0;
static ParameterInfo t416_m28783_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t265_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t265 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28783_GM;
MethodInfo m28783_MI = 
{
	"Equals", NULL, &t416_TI, &t40_0_0_0, RuntimeInvoker_t40_t265, t416_m28783_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28783_GM};
static MethodInfo* t416_MIs[] =
{
	&m28783_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t416_0_0_0;
extern Il2CppType t416_1_0_0;
struct t416;
extern Il2CppGenericClass t416_GC;
TypeInfo t416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t416_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t416_TI, NULL, NULL, &EmptyCustomAttributesCache, &t416_TI, &t416_0_0_0, &t416_1_0_0, NULL, &t416_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t266.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t266_TI;
#include "t266MD.h"

#include "t28.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Component>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t266_m1999_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1999_GM;
MethodInfo m1999_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t266_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t266_m1999_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1999_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t266_m2002_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2002_GM;
MethodInfo m2002_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t266_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t266_m2002_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2002_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t266_m15174_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15174_GM;
MethodInfo m15174_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t266_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t266_m15174_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15174_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t266_m15175_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15175_GM;
MethodInfo m15175_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t266_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t266_m15175_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15175_GM};
static MethodInfo* t266_MIs[] =
{
	&m1999_MI,
	&m2002_MI,
	&m15174_MI,
	&m15175_MI,
	NULL
};
extern MethodInfo m2002_MI;
extern MethodInfo m15174_MI;
extern MethodInfo m15175_MI;
static MethodInfo* t266_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2002_MI,
	&m15174_MI,
	&m15175_MI,
};
static Il2CppInterfaceOffsetPair t266_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t266_0_0_0;
extern Il2CppType t266_1_0_0;
struct t266;
extern Il2CppGenericClass t266_GC;
TypeInfo t266_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t266_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t266_TI, NULL, t266_VT, &EmptyCustomAttributesCache, &t266_TI, &t266_0_0_0, &t266_1_0_0, t266_IOs, &t266_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t266), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t270.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t270_TI;
#include "t270MD.h"

#include "t22.h"


extern MethodInfo m2003_MI;
 void m2003 (t270 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m2004_MI;
 float m2004 (t270 * __this, t29 * p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m2004((t270 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef float (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m15176_MI;
 t29 * m15176 (t270 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m15177_MI;
 float m15177 (t270 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t270_m2003_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2003_GM;
MethodInfo m2003_MI = 
{
	".ctor", (methodPointerType)&m2003, &t270_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t270_m2003_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2003_GM};
extern Il2CppType t271_0_0_0;
extern Il2CppType t271_0_0_0;
static ParameterInfo t270_m2004_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2004_GM;
MethodInfo m2004_MI = 
{
	"Invoke", (methodPointerType)&m2004, &t270_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t270_m2004_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2004_GM};
extern Il2CppType t271_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t270_m15176_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t271_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15176_GM;
MethodInfo m15176_MI = 
{
	"BeginInvoke", (methodPointerType)&m15176, &t270_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t270_m15176_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15176_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t270_m15177_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15177_GM;
MethodInfo m15177_MI = 
{
	"EndInvoke", (methodPointerType)&m15177, &t270_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t270_m15177_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15177_GM};
static MethodInfo* t270_MIs[] =
{
	&m2003_MI,
	&m2004_MI,
	&m15176_MI,
	&m15177_MI,
	NULL
};
static MethodInfo* t270_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2004_MI,
	&m15176_MI,
	&m15177_MI,
};
static Il2CppInterfaceOffsetPair t270_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t270_0_0_0;
extern Il2CppType t270_1_0_0;
struct t270;
extern Il2CppGenericClass t270_GC;
TypeInfo t270_TI = 
{
	&g_System_Core_dll_Image, NULL, "Func`2", "System", t270_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t270_TI, NULL, t270_VT, &EmptyCustomAttributesCache, &t270_TI, &t270_0_0_0, &t270_1_0_0, t270_IOs, &t270_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t270), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4303_TI;

#include "t272.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>
extern MethodInfo m28784_MI;
static PropertyInfo t4303____Current_PropertyInfo = 
{
	&t4303_TI, "Current", &m28784_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4303_PIs[] =
{
	&t4303____Current_PropertyInfo,
	NULL
};
extern Il2CppType t272_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28784_GM;
MethodInfo m28784_MI = 
{
	"get_Current", NULL, &t4303_TI, &t272_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28784_GM};
static MethodInfo* t4303_MIs[] =
{
	&m28784_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4303_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4303_0_0_0;
extern Il2CppType t4303_1_0_0;
struct t4303;
extern Il2CppGenericClass t4303_GC;
TypeInfo t4303_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4303_MIs, t4303_PIs, NULL, NULL, NULL, NULL, NULL, &t4303_TI, t4303_ITIs, NULL, &EmptyCustomAttributesCache, &t4303_TI, &t4303_0_0_0, &t4303_1_0_0, NULL, &t4303_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2777.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2777_TI;
#include "t2777MD.h"

#include "t44.h"
#include "t914.h"
extern TypeInfo t272_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m15182_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m21797_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m21797(__this, p0, method) (t272 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2777_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2777_TI, offsetof(t2777, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2777_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2777_TI, offsetof(t2777, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2777_FIs[] =
{
	&t2777_f0_FieldInfo,
	&t2777_f1_FieldInfo,
	NULL
};
extern MethodInfo m15179_MI;
static PropertyInfo t2777____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2777_TI, "System.Collections.IEnumerator.Current", &m15179_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2777____Current_PropertyInfo = 
{
	&t2777_TI, "Current", &m15182_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2777_PIs[] =
{
	&t2777____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2777____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2777_m15178_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15178_GM;
MethodInfo m15178_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2777_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2777_m15178_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15178_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15179_GM;
MethodInfo m15179_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2777_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15179_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15180_GM;
MethodInfo m15180_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2777_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15180_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15181_GM;
MethodInfo m15181_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2777_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15181_GM};
extern Il2CppType t272_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15182_GM;
MethodInfo m15182_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2777_TI, &t272_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15182_GM};
static MethodInfo* t2777_MIs[] =
{
	&m15178_MI,
	&m15179_MI,
	&m15180_MI,
	&m15181_MI,
	&m15182_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m15181_MI;
extern MethodInfo m15180_MI;
static MethodInfo* t2777_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15179_MI,
	&m15181_MI,
	&m15180_MI,
	&m15182_MI,
};
static TypeInfo* t2777_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4303_TI,
};
static Il2CppInterfaceOffsetPair t2777_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4303_TI, 7},
};
extern TypeInfo t272_TI;
static Il2CppRGCTXData t2777_RGCTXData[3] = 
{
	&m15182_MI/* Method Usage */,
	&t272_TI/* Class Usage */,
	&m21797_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2777_0_0_0;
extern Il2CppType t2777_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2777_GC;
extern TypeInfo t20_TI;
TypeInfo t2777_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2777_MIs, t2777_PIs, t2777_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2777_TI, t2777_ITIs, t2777_VT, &EmptyCustomAttributesCache, &t2777_TI, &t2777_0_0_0, &t2777_1_0_0, t2777_IOs, &t2777_GC, NULL, NULL, NULL, t2777_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2777)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5496_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>
extern MethodInfo m28785_MI;
static PropertyInfo t5496____Count_PropertyInfo = 
{
	&t5496_TI, "Count", &m28785_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28786_MI;
static PropertyInfo t5496____IsReadOnly_PropertyInfo = 
{
	&t5496_TI, "IsReadOnly", &m28786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5496_PIs[] =
{
	&t5496____Count_PropertyInfo,
	&t5496____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28785_GM;
MethodInfo m28785_MI = 
{
	"get_Count", NULL, &t5496_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28785_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28786_GM;
MethodInfo m28786_MI = 
{
	"get_IsReadOnly", NULL, &t5496_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28786_GM};
extern Il2CppType t272_0_0_0;
extern Il2CppType t272_0_0_0;
static ParameterInfo t5496_m28787_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28787_GM;
MethodInfo m28787_MI = 
{
	"Add", NULL, &t5496_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5496_m28787_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28787_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28788_GM;
MethodInfo m28788_MI = 
{
	"Clear", NULL, &t5496_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28788_GM};
extern Il2CppType t272_0_0_0;
static ParameterInfo t5496_m28789_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28789_GM;
MethodInfo m28789_MI = 
{
	"Contains", NULL, &t5496_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5496_m28789_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28789_GM};
extern Il2CppType t3885_0_0_0;
extern Il2CppType t3885_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5496_m28790_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3885_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28790_GM;
MethodInfo m28790_MI = 
{
	"CopyTo", NULL, &t5496_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5496_m28790_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28790_GM};
extern Il2CppType t272_0_0_0;
static ParameterInfo t5496_m28791_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28791_GM;
MethodInfo m28791_MI = 
{
	"Remove", NULL, &t5496_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5496_m28791_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28791_GM};
static MethodInfo* t5496_MIs[] =
{
	&m28785_MI,
	&m28786_MI,
	&m28787_MI,
	&m28788_MI,
	&m28789_MI,
	&m28790_MI,
	&m28791_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5498_TI;
static TypeInfo* t5496_ITIs[] = 
{
	&t603_TI,
	&t5498_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5496_0_0_0;
extern Il2CppType t5496_1_0_0;
struct t5496;
extern Il2CppGenericClass t5496_GC;
TypeInfo t5496_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5496_MIs, t5496_PIs, NULL, NULL, NULL, NULL, NULL, &t5496_TI, t5496_ITIs, NULL, &EmptyCustomAttributesCache, &t5496_TI, &t5496_0_0_0, &t5496_1_0_0, NULL, &t5496_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType t4303_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28792_GM;
MethodInfo m28792_MI = 
{
	"GetEnumerator", NULL, &t5498_TI, &t4303_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28792_GM};
static MethodInfo* t5498_MIs[] =
{
	&m28792_MI,
	NULL
};
static TypeInfo* t5498_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5498_0_0_0;
extern Il2CppType t5498_1_0_0;
struct t5498;
extern Il2CppGenericClass t5498_GC;
TypeInfo t5498_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5498_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5498_TI, t5498_ITIs, NULL, &EmptyCustomAttributesCache, &t5498_TI, &t5498_0_0_0, &t5498_1_0_0, NULL, &t5498_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5497_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>
extern MethodInfo m28793_MI;
extern MethodInfo m28794_MI;
static PropertyInfo t5497____Item_PropertyInfo = 
{
	&t5497_TI, "Item", &m28793_MI, &m28794_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5497_PIs[] =
{
	&t5497____Item_PropertyInfo,
	NULL
};
extern Il2CppType t272_0_0_0;
static ParameterInfo t5497_m28795_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28795_GM;
MethodInfo m28795_MI = 
{
	"IndexOf", NULL, &t5497_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5497_m28795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28795_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t272_0_0_0;
static ParameterInfo t5497_m28796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28796_GM;
MethodInfo m28796_MI = 
{
	"Insert", NULL, &t5497_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5497_m28796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28796_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5497_m28797_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28797_GM;
MethodInfo m28797_MI = 
{
	"RemoveAt", NULL, &t5497_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5497_m28797_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28797_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5497_m28793_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t272_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28793_GM;
MethodInfo m28793_MI = 
{
	"get_Item", NULL, &t5497_TI, &t272_0_0_0, RuntimeInvoker_t29_t44, t5497_m28793_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28793_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t272_0_0_0;
static ParameterInfo t5497_m28794_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28794_GM;
MethodInfo m28794_MI = 
{
	"set_Item", NULL, &t5497_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5497_m28794_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28794_GM};
static MethodInfo* t5497_MIs[] =
{
	&m28795_MI,
	&m28796_MI,
	&m28797_MI,
	&m28793_MI,
	&m28794_MI,
	NULL
};
static TypeInfo* t5497_ITIs[] = 
{
	&t603_TI,
	&t5496_TI,
	&t5498_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5497_0_0_0;
extern Il2CppType t5497_1_0_0;
struct t5497;
extern Il2CppGenericClass t5497_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5497_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5497_MIs, t5497_PIs, NULL, NULL, NULL, NULL, NULL, &t5497_TI, t5497_ITIs, NULL, &t1908__CustomAttributeCache, &t5497_TI, &t5497_0_0_0, &t5497_1_0_0, NULL, &t5497_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2778.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2778_TI;
#include "t2778MD.h"

#include "t2779.h"
extern TypeInfo t2779_TI;
#include "t2779MD.h"
extern MethodInfo m15185_MI;
extern MethodInfo m15187_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t2778_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2778_TI, offsetof(t2778, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2778_FIs[] =
{
	&t2778_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t272_0_0_0;
static ParameterInfo t2778_m15183_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15183_GM;
MethodInfo m15183_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2778_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2778_m15183_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15183_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2778_m15184_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15184_GM;
MethodInfo m15184_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2778_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2778_m15184_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15184_GM};
static MethodInfo* t2778_MIs[] =
{
	&m15183_MI,
	&m15184_MI,
	NULL
};
extern MethodInfo m15184_MI;
extern MethodInfo m15188_MI;
static MethodInfo* t2778_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15184_MI,
	&m15188_MI,
};
extern Il2CppType t2780_0_0_0;
extern TypeInfo t2780_TI;
extern MethodInfo m21807_MI;
extern TypeInfo t272_TI;
extern MethodInfo m15190_MI;
extern TypeInfo t272_TI;
static Il2CppRGCTXData t2778_RGCTXData[8] = 
{
	&t2780_0_0_0/* Type Usage */,
	&t2780_TI/* Class Usage */,
	&m21807_MI/* Method Usage */,
	&t272_TI/* Class Usage */,
	&m15190_MI/* Method Usage */,
	&m15185_MI/* Method Usage */,
	&t272_TI/* Class Usage */,
	&m15187_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2778_0_0_0;
extern Il2CppType t2778_1_0_0;
struct t2778;
extern Il2CppGenericClass t2778_GC;
TypeInfo t2778_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2778_MIs, NULL, t2778_FIs, NULL, &t2779_TI, NULL, NULL, &t2778_TI, NULL, t2778_VT, &EmptyCustomAttributesCache, &t2778_TI, &t2778_0_0_0, &t2778_1_0_0, NULL, &t2778_GC, NULL, NULL, NULL, t2778_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2778), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2780.h"
extern TypeInfo t2780_TI;
#include "t2780MD.h"
struct t556;
#define m21807(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType t2780_0_0_1;
FieldInfo t2779_f0_FieldInfo = 
{
	"Delegate", &t2780_0_0_1, &t2779_TI, offsetof(t2779, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2779_FIs[] =
{
	&t2779_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2779_m15185_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15185_GM;
MethodInfo m15185_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2779_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2779_m15185_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15185_GM};
extern Il2CppType t2780_0_0_0;
static ParameterInfo t2779_m15186_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2780_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15186_GM;
MethodInfo m15186_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2779_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2779_m15186_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15186_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2779_m15187_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15187_GM;
MethodInfo m15187_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2779_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2779_m15187_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15187_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2779_m15188_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15188_GM;
MethodInfo m15188_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2779_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2779_m15188_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15188_GM};
static MethodInfo* t2779_MIs[] =
{
	&m15185_MI,
	&m15186_MI,
	&m15187_MI,
	&m15188_MI,
	NULL
};
static MethodInfo* t2779_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15187_MI,
	&m15188_MI,
};
extern TypeInfo t2780_TI;
extern TypeInfo t272_TI;
static Il2CppRGCTXData t2779_RGCTXData[5] = 
{
	&t2780_0_0_0/* Type Usage */,
	&t2780_TI/* Class Usage */,
	&m21807_MI/* Method Usage */,
	&t272_TI/* Class Usage */,
	&m15190_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2779_0_0_0;
extern Il2CppType t2779_1_0_0;
struct t2779;
extern Il2CppGenericClass t2779_GC;
TypeInfo t2779_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2779_MIs, NULL, t2779_FIs, NULL, &t556_TI, NULL, NULL, &t2779_TI, NULL, t2779_VT, &EmptyCustomAttributesCache, &t2779_TI, &t2779_0_0_0, &t2779_1_0_0, NULL, &t2779_GC, NULL, NULL, NULL, t2779_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2779), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2780_m15189_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15189_GM;
MethodInfo m15189_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2780_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2780_m15189_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15189_GM};
extern Il2CppType t272_0_0_0;
static ParameterInfo t2780_m15190_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15190_GM;
MethodInfo m15190_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2780_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2780_m15190_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15190_GM};
extern Il2CppType t272_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2780_m15191_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t272_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15191_GM;
MethodInfo m15191_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2780_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2780_m15191_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15191_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2780_m15192_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15192_GM;
MethodInfo m15192_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2780_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2780_m15192_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15192_GM};
static MethodInfo* t2780_MIs[] =
{
	&m15189_MI,
	&m15190_MI,
	&m15191_MI,
	&m15192_MI,
	NULL
};
extern MethodInfo m15191_MI;
extern MethodInfo m15192_MI;
static MethodInfo* t2780_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15190_MI,
	&m15191_MI,
	&m15192_MI,
};
static Il2CppInterfaceOffsetPair t2780_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2780_1_0_0;
struct t2780;
extern Il2CppGenericClass t2780_GC;
TypeInfo t2780_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2780_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2780_TI, NULL, t2780_VT, &EmptyCustomAttributesCache, &t2780_TI, &t2780_0_0_0, &t2780_1_0_0, t2780_IOs, &t2780_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2780), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4305_TI;

#include "t273.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Mask>
extern MethodInfo m28798_MI;
static PropertyInfo t4305____Current_PropertyInfo = 
{
	&t4305_TI, "Current", &m28798_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4305_PIs[] =
{
	&t4305____Current_PropertyInfo,
	NULL
};
extern Il2CppType t273_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28798_GM;
MethodInfo m28798_MI = 
{
	"get_Current", NULL, &t4305_TI, &t273_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28798_GM};
static MethodInfo* t4305_MIs[] =
{
	&m28798_MI,
	NULL
};
static TypeInfo* t4305_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4305_0_0_0;
extern Il2CppType t4305_1_0_0;
struct t4305;
extern Il2CppGenericClass t4305_GC;
TypeInfo t4305_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4305_MIs, t4305_PIs, NULL, NULL, NULL, NULL, NULL, &t4305_TI, t4305_ITIs, NULL, &EmptyCustomAttributesCache, &t4305_TI, &t4305_0_0_0, &t4305_1_0_0, NULL, &t4305_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2781.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2781_TI;
#include "t2781MD.h"

extern TypeInfo t273_TI;
extern MethodInfo m15197_MI;
extern MethodInfo m21809_MI;
struct t20;
#define m21809(__this, p0, method) (t273 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>
extern Il2CppType t20_0_0_1;
FieldInfo t2781_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2781_TI, offsetof(t2781, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2781_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2781_TI, offsetof(t2781, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2781_FIs[] =
{
	&t2781_f0_FieldInfo,
	&t2781_f1_FieldInfo,
	NULL
};
extern MethodInfo m15194_MI;
static PropertyInfo t2781____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2781_TI, "System.Collections.IEnumerator.Current", &m15194_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2781____Current_PropertyInfo = 
{
	&t2781_TI, "Current", &m15197_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2781_PIs[] =
{
	&t2781____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2781____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2781_m15193_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15193_GM;
MethodInfo m15193_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2781_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2781_m15193_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15193_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15194_GM;
MethodInfo m15194_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2781_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15194_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15195_GM;
MethodInfo m15195_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2781_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15195_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15196_GM;
MethodInfo m15196_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2781_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15196_GM};
extern Il2CppType t273_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15197_GM;
MethodInfo m15197_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2781_TI, &t273_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15197_GM};
static MethodInfo* t2781_MIs[] =
{
	&m15193_MI,
	&m15194_MI,
	&m15195_MI,
	&m15196_MI,
	&m15197_MI,
	NULL
};
extern MethodInfo m15196_MI;
extern MethodInfo m15195_MI;
static MethodInfo* t2781_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15194_MI,
	&m15196_MI,
	&m15195_MI,
	&m15197_MI,
};
static TypeInfo* t2781_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4305_TI,
};
static Il2CppInterfaceOffsetPair t2781_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4305_TI, 7},
};
extern TypeInfo t273_TI;
static Il2CppRGCTXData t2781_RGCTXData[3] = 
{
	&m15197_MI/* Method Usage */,
	&t273_TI/* Class Usage */,
	&m21809_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2781_0_0_0;
extern Il2CppType t2781_1_0_0;
extern Il2CppGenericClass t2781_GC;
TypeInfo t2781_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2781_MIs, t2781_PIs, t2781_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2781_TI, t2781_ITIs, t2781_VT, &EmptyCustomAttributesCache, &t2781_TI, &t2781_0_0_0, &t2781_1_0_0, t2781_IOs, &t2781_GC, NULL, NULL, NULL, t2781_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2781)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5499_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>
extern MethodInfo m28799_MI;
static PropertyInfo t5499____Count_PropertyInfo = 
{
	&t5499_TI, "Count", &m28799_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28800_MI;
static PropertyInfo t5499____IsReadOnly_PropertyInfo = 
{
	&t5499_TI, "IsReadOnly", &m28800_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5499_PIs[] =
{
	&t5499____Count_PropertyInfo,
	&t5499____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28799_GM;
MethodInfo m28799_MI = 
{
	"get_Count", NULL, &t5499_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28799_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28800_GM;
MethodInfo m28800_MI = 
{
	"get_IsReadOnly", NULL, &t5499_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28800_GM};
extern Il2CppType t273_0_0_0;
extern Il2CppType t273_0_0_0;
static ParameterInfo t5499_m28801_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28801_GM;
MethodInfo m28801_MI = 
{
	"Add", NULL, &t5499_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5499_m28801_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28801_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28802_GM;
MethodInfo m28802_MI = 
{
	"Clear", NULL, &t5499_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28802_GM};
extern Il2CppType t273_0_0_0;
static ParameterInfo t5499_m28803_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28803_GM;
MethodInfo m28803_MI = 
{
	"Contains", NULL, &t5499_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5499_m28803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28803_GM};
extern Il2CppType t3886_0_0_0;
extern Il2CppType t3886_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5499_m28804_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3886_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28804_GM;
MethodInfo m28804_MI = 
{
	"CopyTo", NULL, &t5499_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5499_m28804_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28804_GM};
extern Il2CppType t273_0_0_0;
static ParameterInfo t5499_m28805_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28805_GM;
MethodInfo m28805_MI = 
{
	"Remove", NULL, &t5499_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5499_m28805_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28805_GM};
static MethodInfo* t5499_MIs[] =
{
	&m28799_MI,
	&m28800_MI,
	&m28801_MI,
	&m28802_MI,
	&m28803_MI,
	&m28804_MI,
	&m28805_MI,
	NULL
};
extern TypeInfo t5501_TI;
static TypeInfo* t5499_ITIs[] = 
{
	&t603_TI,
	&t5501_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5499_0_0_0;
extern Il2CppType t5499_1_0_0;
struct t5499;
extern Il2CppGenericClass t5499_GC;
TypeInfo t5499_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5499_MIs, t5499_PIs, NULL, NULL, NULL, NULL, NULL, &t5499_TI, t5499_ITIs, NULL, &EmptyCustomAttributesCache, &t5499_TI, &t5499_0_0_0, &t5499_1_0_0, NULL, &t5499_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Mask>
extern Il2CppType t4305_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28806_GM;
MethodInfo m28806_MI = 
{
	"GetEnumerator", NULL, &t5501_TI, &t4305_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28806_GM};
static MethodInfo* t5501_MIs[] =
{
	&m28806_MI,
	NULL
};
static TypeInfo* t5501_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5501_0_0_0;
extern Il2CppType t5501_1_0_0;
struct t5501;
extern Il2CppGenericClass t5501_GC;
TypeInfo t5501_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5501_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5501_TI, t5501_ITIs, NULL, &EmptyCustomAttributesCache, &t5501_TI, &t5501_0_0_0, &t5501_1_0_0, NULL, &t5501_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5500_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Mask>
extern MethodInfo m28807_MI;
extern MethodInfo m28808_MI;
static PropertyInfo t5500____Item_PropertyInfo = 
{
	&t5500_TI, "Item", &m28807_MI, &m28808_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5500_PIs[] =
{
	&t5500____Item_PropertyInfo,
	NULL
};
extern Il2CppType t273_0_0_0;
static ParameterInfo t5500_m28809_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28809_GM;
MethodInfo m28809_MI = 
{
	"IndexOf", NULL, &t5500_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5500_m28809_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28809_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t273_0_0_0;
static ParameterInfo t5500_m28810_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28810_GM;
MethodInfo m28810_MI = 
{
	"Insert", NULL, &t5500_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5500_m28810_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28810_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5500_m28811_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28811_GM;
MethodInfo m28811_MI = 
{
	"RemoveAt", NULL, &t5500_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5500_m28811_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28811_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5500_m28807_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t273_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28807_GM;
MethodInfo m28807_MI = 
{
	"get_Item", NULL, &t5500_TI, &t273_0_0_0, RuntimeInvoker_t29_t44, t5500_m28807_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28807_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t273_0_0_0;
static ParameterInfo t5500_m28808_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28808_GM;
MethodInfo m28808_MI = 
{
	"set_Item", NULL, &t5500_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5500_m28808_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28808_GM};
static MethodInfo* t5500_MIs[] =
{
	&m28809_MI,
	&m28810_MI,
	&m28811_MI,
	&m28807_MI,
	&m28808_MI,
	NULL
};
static TypeInfo* t5500_ITIs[] = 
{
	&t603_TI,
	&t5499_TI,
	&t5501_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5500_0_0_0;
extern Il2CppType t5500_1_0_0;
struct t5500;
extern Il2CppGenericClass t5500_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5500_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5500_MIs, t5500_PIs, NULL, NULL, NULL, NULL, NULL, &t5500_TI, t5500_ITIs, NULL, &t1908__CustomAttributeCache, &t5500_TI, &t5500_0_0_0, &t5500_1_0_0, NULL, &t5500_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5502_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern MethodInfo m28812_MI;
static PropertyInfo t5502____Count_PropertyInfo = 
{
	&t5502_TI, "Count", &m28812_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28813_MI;
static PropertyInfo t5502____IsReadOnly_PropertyInfo = 
{
	&t5502_TI, "IsReadOnly", &m28813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5502_PIs[] =
{
	&t5502____Count_PropertyInfo,
	&t5502____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28812_GM;
MethodInfo m28812_MI = 
{
	"get_Count", NULL, &t5502_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28812_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28813_GM;
MethodInfo m28813_MI = 
{
	"get_IsReadOnly", NULL, &t5502_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28813_GM};
extern Il2CppType t355_0_0_0;
extern Il2CppType t355_0_0_0;
static ParameterInfo t5502_m28814_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t355_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28814_GM;
MethodInfo m28814_MI = 
{
	"Add", NULL, &t5502_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5502_m28814_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28814_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28815_GM;
MethodInfo m28815_MI = 
{
	"Clear", NULL, &t5502_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28815_GM};
extern Il2CppType t355_0_0_0;
static ParameterInfo t5502_m28816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t355_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28816_GM;
MethodInfo m28816_MI = 
{
	"Contains", NULL, &t5502_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5502_m28816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28816_GM};
extern Il2CppType t3887_0_0_0;
extern Il2CppType t3887_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5502_m28817_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3887_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28817_GM;
MethodInfo m28817_MI = 
{
	"CopyTo", NULL, &t5502_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5502_m28817_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28817_GM};
extern Il2CppType t355_0_0_0;
static ParameterInfo t5502_m28818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t355_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28818_GM;
MethodInfo m28818_MI = 
{
	"Remove", NULL, &t5502_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5502_m28818_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28818_GM};
static MethodInfo* t5502_MIs[] =
{
	&m28812_MI,
	&m28813_MI,
	&m28814_MI,
	&m28815_MI,
	&m28816_MI,
	&m28817_MI,
	&m28818_MI,
	NULL
};
extern TypeInfo t5504_TI;
static TypeInfo* t5502_ITIs[] = 
{
	&t603_TI,
	&t5504_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5502_0_0_0;
extern Il2CppType t5502_1_0_0;
struct t5502;
extern Il2CppGenericClass t5502_GC;
TypeInfo t5502_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5502_MIs, t5502_PIs, NULL, NULL, NULL, NULL, NULL, &t5502_TI, t5502_ITIs, NULL, &EmptyCustomAttributesCache, &t5502_TI, &t5502_0_0_0, &t5502_1_0_0, NULL, &t5502_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern Il2CppType t4307_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28819_GM;
MethodInfo m28819_MI = 
{
	"GetEnumerator", NULL, &t5504_TI, &t4307_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28819_GM};
static MethodInfo* t5504_MIs[] =
{
	&m28819_MI,
	NULL
};
static TypeInfo* t5504_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5504_0_0_0;
extern Il2CppType t5504_1_0_0;
struct t5504;
extern Il2CppGenericClass t5504_GC;
TypeInfo t5504_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5504_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5504_TI, t5504_ITIs, NULL, &EmptyCustomAttributesCache, &t5504_TI, &t5504_0_0_0, &t5504_1_0_0, NULL, &t5504_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4307_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern MethodInfo m28820_MI;
static PropertyInfo t4307____Current_PropertyInfo = 
{
	&t4307_TI, "Current", &m28820_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4307_PIs[] =
{
	&t4307____Current_PropertyInfo,
	NULL
};
extern Il2CppType t355_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28820_GM;
MethodInfo m28820_MI = 
{
	"get_Current", NULL, &t4307_TI, &t355_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28820_GM};
static MethodInfo* t4307_MIs[] =
{
	&m28820_MI,
	NULL
};
static TypeInfo* t4307_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4307_0_0_0;
extern Il2CppType t4307_1_0_0;
struct t4307;
extern Il2CppGenericClass t4307_GC;
TypeInfo t4307_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4307_MIs, t4307_PIs, NULL, NULL, NULL, NULL, NULL, &t4307_TI, t4307_ITIs, NULL, &EmptyCustomAttributesCache, &t4307_TI, &t4307_0_0_0, &t4307_1_0_0, NULL, &t4307_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2782.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2782_TI;
#include "t2782MD.h"

extern TypeInfo t355_TI;
extern MethodInfo m15202_MI;
extern MethodInfo m21820_MI;
struct t20;
#define m21820(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern Il2CppType t20_0_0_1;
FieldInfo t2782_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2782_TI, offsetof(t2782, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2782_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2782_TI, offsetof(t2782, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2782_FIs[] =
{
	&t2782_f0_FieldInfo,
	&t2782_f1_FieldInfo,
	NULL
};
extern MethodInfo m15199_MI;
static PropertyInfo t2782____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2782_TI, "System.Collections.IEnumerator.Current", &m15199_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2782____Current_PropertyInfo = 
{
	&t2782_TI, "Current", &m15202_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2782_PIs[] =
{
	&t2782____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2782____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2782_m15198_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15198_GM;
MethodInfo m15198_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2782_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2782_m15198_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15198_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15199_GM;
MethodInfo m15199_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2782_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15199_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15200_GM;
MethodInfo m15200_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2782_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15200_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15201_GM;
MethodInfo m15201_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2782_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15201_GM};
extern Il2CppType t355_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15202_GM;
MethodInfo m15202_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2782_TI, &t355_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15202_GM};
static MethodInfo* t2782_MIs[] =
{
	&m15198_MI,
	&m15199_MI,
	&m15200_MI,
	&m15201_MI,
	&m15202_MI,
	NULL
};
extern MethodInfo m15201_MI;
extern MethodInfo m15200_MI;
static MethodInfo* t2782_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15199_MI,
	&m15201_MI,
	&m15200_MI,
	&m15202_MI,
};
static TypeInfo* t2782_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4307_TI,
};
static Il2CppInterfaceOffsetPair t2782_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4307_TI, 7},
};
extern TypeInfo t355_TI;
static Il2CppRGCTXData t2782_RGCTXData[3] = 
{
	&m15202_MI/* Method Usage */,
	&t355_TI/* Class Usage */,
	&m21820_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2782_0_0_0;
extern Il2CppType t2782_1_0_0;
extern Il2CppGenericClass t2782_GC;
TypeInfo t2782_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2782_MIs, t2782_PIs, t2782_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2782_TI, t2782_ITIs, t2782_VT, &EmptyCustomAttributesCache, &t2782_TI, &t2782_0_0_0, &t2782_1_0_0, t2782_IOs, &t2782_GC, NULL, NULL, NULL, t2782_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2782)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5503_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern MethodInfo m28821_MI;
extern MethodInfo m28822_MI;
static PropertyInfo t5503____Item_PropertyInfo = 
{
	&t5503_TI, "Item", &m28821_MI, &m28822_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5503_PIs[] =
{
	&t5503____Item_PropertyInfo,
	NULL
};
extern Il2CppType t355_0_0_0;
static ParameterInfo t5503_m28823_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t355_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28823_GM;
MethodInfo m28823_MI = 
{
	"IndexOf", NULL, &t5503_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5503_m28823_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28823_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t355_0_0_0;
static ParameterInfo t5503_m28824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t355_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28824_GM;
MethodInfo m28824_MI = 
{
	"Insert", NULL, &t5503_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5503_m28824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28824_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5503_m28825_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28825_GM;
MethodInfo m28825_MI = 
{
	"RemoveAt", NULL, &t5503_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5503_m28825_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28825_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5503_m28821_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t355_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28821_GM;
MethodInfo m28821_MI = 
{
	"get_Item", NULL, &t5503_TI, &t355_0_0_0, RuntimeInvoker_t29_t44, t5503_m28821_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28821_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t355_0_0_0;
static ParameterInfo t5503_m28822_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t355_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28822_GM;
MethodInfo m28822_MI = 
{
	"set_Item", NULL, &t5503_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5503_m28822_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28822_GM};
static MethodInfo* t5503_MIs[] =
{
	&m28823_MI,
	&m28824_MI,
	&m28825_MI,
	&m28821_MI,
	&m28822_MI,
	NULL
};
static TypeInfo* t5503_ITIs[] = 
{
	&t603_TI,
	&t5502_TI,
	&t5504_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5503_0_0_0;
extern Il2CppType t5503_1_0_0;
struct t5503;
extern Il2CppGenericClass t5503_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5503_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5503_MIs, t5503_PIs, NULL, NULL, NULL, NULL, NULL, &t5503_TI, t5503_ITIs, NULL, &t1908__CustomAttributeCache, &t5503_TI, &t5503_0_0_0, &t5503_1_0_0, NULL, &t5503_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5505_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>
extern MethodInfo m28826_MI;
static PropertyInfo t5505____Count_PropertyInfo = 
{
	&t5505_TI, "Count", &m28826_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28827_MI;
static PropertyInfo t5505____IsReadOnly_PropertyInfo = 
{
	&t5505_TI, "IsReadOnly", &m28827_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5505_PIs[] =
{
	&t5505____Count_PropertyInfo,
	&t5505____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28826_GM;
MethodInfo m28826_MI = 
{
	"get_Count", NULL, &t5505_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28826_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28827_GM;
MethodInfo m28827_MI = 
{
	"get_IsReadOnly", NULL, &t5505_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28827_GM};
extern Il2CppType t370_0_0_0;
extern Il2CppType t370_0_0_0;
static ParameterInfo t5505_m28828_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t370_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28828_GM;
MethodInfo m28828_MI = 
{
	"Add", NULL, &t5505_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5505_m28828_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28828_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28829_GM;
MethodInfo m28829_MI = 
{
	"Clear", NULL, &t5505_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28829_GM};
extern Il2CppType t370_0_0_0;
static ParameterInfo t5505_m28830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t370_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28830_GM;
MethodInfo m28830_MI = 
{
	"Contains", NULL, &t5505_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5505_m28830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28830_GM};
extern Il2CppType t3888_0_0_0;
extern Il2CppType t3888_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5505_m28831_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3888_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28831_GM;
MethodInfo m28831_MI = 
{
	"CopyTo", NULL, &t5505_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5505_m28831_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28831_GM};
extern Il2CppType t370_0_0_0;
static ParameterInfo t5505_m28832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t370_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28832_GM;
MethodInfo m28832_MI = 
{
	"Remove", NULL, &t5505_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5505_m28832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28832_GM};
static MethodInfo* t5505_MIs[] =
{
	&m28826_MI,
	&m28827_MI,
	&m28828_MI,
	&m28829_MI,
	&m28830_MI,
	&m28831_MI,
	&m28832_MI,
	NULL
};
extern TypeInfo t5507_TI;
static TypeInfo* t5505_ITIs[] = 
{
	&t603_TI,
	&t5507_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5505_0_0_0;
extern Il2CppType t5505_1_0_0;
struct t5505;
extern Il2CppGenericClass t5505_GC;
TypeInfo t5505_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5505_MIs, t5505_PIs, NULL, NULL, NULL, NULL, NULL, &t5505_TI, t5505_ITIs, NULL, &EmptyCustomAttributesCache, &t5505_TI, &t5505_0_0_0, &t5505_1_0_0, NULL, &t5505_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMask>
extern Il2CppType t4309_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28833_GM;
MethodInfo m28833_MI = 
{
	"GetEnumerator", NULL, &t5507_TI, &t4309_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28833_GM};
static MethodInfo* t5507_MIs[] =
{
	&m28833_MI,
	NULL
};
static TypeInfo* t5507_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5507_0_0_0;
extern Il2CppType t5507_1_0_0;
struct t5507;
extern Il2CppGenericClass t5507_GC;
TypeInfo t5507_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5507_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5507_TI, t5507_ITIs, NULL, &EmptyCustomAttributesCache, &t5507_TI, &t5507_0_0_0, &t5507_1_0_0, NULL, &t5507_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4309_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMask>
extern MethodInfo m28834_MI;
static PropertyInfo t4309____Current_PropertyInfo = 
{
	&t4309_TI, "Current", &m28834_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4309_PIs[] =
{
	&t4309____Current_PropertyInfo,
	NULL
};
extern Il2CppType t370_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28834_GM;
MethodInfo m28834_MI = 
{
	"get_Current", NULL, &t4309_TI, &t370_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28834_GM};
static MethodInfo* t4309_MIs[] =
{
	&m28834_MI,
	NULL
};
static TypeInfo* t4309_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4309_0_0_0;
extern Il2CppType t4309_1_0_0;
struct t4309;
extern Il2CppGenericClass t4309_GC;
TypeInfo t4309_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4309_MIs, t4309_PIs, NULL, NULL, NULL, NULL, NULL, &t4309_TI, t4309_ITIs, NULL, &EmptyCustomAttributesCache, &t4309_TI, &t4309_0_0_0, &t4309_1_0_0, NULL, &t4309_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2783.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2783_TI;
#include "t2783MD.h"

extern TypeInfo t370_TI;
extern MethodInfo m15207_MI;
extern MethodInfo m21831_MI;
struct t20;
#define m21831(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>
extern Il2CppType t20_0_0_1;
FieldInfo t2783_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2783_TI, offsetof(t2783, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2783_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2783_TI, offsetof(t2783, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2783_FIs[] =
{
	&t2783_f0_FieldInfo,
	&t2783_f1_FieldInfo,
	NULL
};
extern MethodInfo m15204_MI;
static PropertyInfo t2783____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2783_TI, "System.Collections.IEnumerator.Current", &m15204_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2783____Current_PropertyInfo = 
{
	&t2783_TI, "Current", &m15207_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2783_PIs[] =
{
	&t2783____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2783____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2783_m15203_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15203_GM;
MethodInfo m15203_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2783_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2783_m15203_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15203_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15204_GM;
MethodInfo m15204_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2783_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15204_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15205_GM;
MethodInfo m15205_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2783_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15205_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15206_GM;
MethodInfo m15206_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2783_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15206_GM};
extern Il2CppType t370_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15207_GM;
MethodInfo m15207_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2783_TI, &t370_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15207_GM};
static MethodInfo* t2783_MIs[] =
{
	&m15203_MI,
	&m15204_MI,
	&m15205_MI,
	&m15206_MI,
	&m15207_MI,
	NULL
};
extern MethodInfo m15206_MI;
extern MethodInfo m15205_MI;
static MethodInfo* t2783_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15204_MI,
	&m15206_MI,
	&m15205_MI,
	&m15207_MI,
};
static TypeInfo* t2783_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4309_TI,
};
static Il2CppInterfaceOffsetPair t2783_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4309_TI, 7},
};
extern TypeInfo t370_TI;
static Il2CppRGCTXData t2783_RGCTXData[3] = 
{
	&m15207_MI/* Method Usage */,
	&t370_TI/* Class Usage */,
	&m21831_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2783_0_0_0;
extern Il2CppType t2783_1_0_0;
extern Il2CppGenericClass t2783_GC;
TypeInfo t2783_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2783_MIs, t2783_PIs, t2783_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2783_TI, t2783_ITIs, t2783_VT, &EmptyCustomAttributesCache, &t2783_TI, &t2783_0_0_0, &t2783_1_0_0, t2783_IOs, &t2783_GC, NULL, NULL, NULL, t2783_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2783)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5506_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IMask>
extern MethodInfo m28835_MI;
extern MethodInfo m28836_MI;
static PropertyInfo t5506____Item_PropertyInfo = 
{
	&t5506_TI, "Item", &m28835_MI, &m28836_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5506_PIs[] =
{
	&t5506____Item_PropertyInfo,
	NULL
};
extern Il2CppType t370_0_0_0;
static ParameterInfo t5506_m28837_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t370_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28837_GM;
MethodInfo m28837_MI = 
{
	"IndexOf", NULL, &t5506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5506_m28837_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28837_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t370_0_0_0;
static ParameterInfo t5506_m28838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t370_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28838_GM;
MethodInfo m28838_MI = 
{
	"Insert", NULL, &t5506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5506_m28838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28838_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5506_m28839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28839_GM;
MethodInfo m28839_MI = 
{
	"RemoveAt", NULL, &t5506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5506_m28839_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28839_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5506_m28835_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t370_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28835_GM;
MethodInfo m28835_MI = 
{
	"get_Item", NULL, &t5506_TI, &t370_0_0_0, RuntimeInvoker_t29_t44, t5506_m28835_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28835_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t370_0_0_0;
static ParameterInfo t5506_m28836_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t370_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28836_GM;
MethodInfo m28836_MI = 
{
	"set_Item", NULL, &t5506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5506_m28836_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28836_GM};
static MethodInfo* t5506_MIs[] =
{
	&m28837_MI,
	&m28838_MI,
	&m28839_MI,
	&m28835_MI,
	&m28836_MI,
	NULL
};
static TypeInfo* t5506_ITIs[] = 
{
	&t603_TI,
	&t5505_TI,
	&t5507_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5506_0_0_0;
extern Il2CppType t5506_1_0_0;
struct t5506;
extern Il2CppGenericClass t5506_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5506_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5506_MIs, t5506_PIs, NULL, NULL, NULL, NULL, NULL, &t5506_TI, t5506_ITIs, NULL, &t1908__CustomAttributeCache, &t5506_TI, &t5506_0_0_0, &t5506_1_0_0, NULL, &t5506_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5508_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>
extern MethodInfo m28840_MI;
static PropertyInfo t5508____Count_PropertyInfo = 
{
	&t5508_TI, "Count", &m28840_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28841_MI;
static PropertyInfo t5508____IsReadOnly_PropertyInfo = 
{
	&t5508_TI, "IsReadOnly", &m28841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5508_PIs[] =
{
	&t5508____Count_PropertyInfo,
	&t5508____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28840_GM;
MethodInfo m28840_MI = 
{
	"get_Count", NULL, &t5508_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28840_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28841_GM;
MethodInfo m28841_MI = 
{
	"get_IsReadOnly", NULL, &t5508_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28841_GM};
extern Il2CppType t354_0_0_0;
extern Il2CppType t354_0_0_0;
static ParameterInfo t5508_m28842_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t354_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28842_GM;
MethodInfo m28842_MI = 
{
	"Add", NULL, &t5508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5508_m28842_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28842_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28843_GM;
MethodInfo m28843_MI = 
{
	"Clear", NULL, &t5508_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28843_GM};
extern Il2CppType t354_0_0_0;
static ParameterInfo t5508_m28844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t354_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28844_GM;
MethodInfo m28844_MI = 
{
	"Contains", NULL, &t5508_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5508_m28844_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28844_GM};
extern Il2CppType t3889_0_0_0;
extern Il2CppType t3889_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5508_m28845_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3889_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28845_GM;
MethodInfo m28845_MI = 
{
	"CopyTo", NULL, &t5508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5508_m28845_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28845_GM};
extern Il2CppType t354_0_0_0;
static ParameterInfo t5508_m28846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t354_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28846_GM;
MethodInfo m28846_MI = 
{
	"Remove", NULL, &t5508_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5508_m28846_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28846_GM};
static MethodInfo* t5508_MIs[] =
{
	&m28840_MI,
	&m28841_MI,
	&m28842_MI,
	&m28843_MI,
	&m28844_MI,
	&m28845_MI,
	&m28846_MI,
	NULL
};
extern TypeInfo t5510_TI;
static TypeInfo* t5508_ITIs[] = 
{
	&t603_TI,
	&t5510_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5508_0_0_0;
extern Il2CppType t5508_1_0_0;
struct t5508;
extern Il2CppGenericClass t5508_GC;
TypeInfo t5508_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5508_MIs, t5508_PIs, NULL, NULL, NULL, NULL, NULL, &t5508_TI, t5508_ITIs, NULL, &EmptyCustomAttributesCache, &t5508_TI, &t5508_0_0_0, &t5508_1_0_0, NULL, &t5508_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMaterialModifier>
extern Il2CppType t4311_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28847_GM;
MethodInfo m28847_MI = 
{
	"GetEnumerator", NULL, &t5510_TI, &t4311_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28847_GM};
static MethodInfo* t5510_MIs[] =
{
	&m28847_MI,
	NULL
};
static TypeInfo* t5510_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5510_0_0_0;
extern Il2CppType t5510_1_0_0;
struct t5510;
extern Il2CppGenericClass t5510_GC;
TypeInfo t5510_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5510_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5510_TI, t5510_ITIs, NULL, &EmptyCustomAttributesCache, &t5510_TI, &t5510_0_0_0, &t5510_1_0_0, NULL, &t5510_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4311_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMaterialModifier>
extern MethodInfo m28848_MI;
static PropertyInfo t4311____Current_PropertyInfo = 
{
	&t4311_TI, "Current", &m28848_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4311_PIs[] =
{
	&t4311____Current_PropertyInfo,
	NULL
};
extern Il2CppType t354_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28848_GM;
MethodInfo m28848_MI = 
{
	"get_Current", NULL, &t4311_TI, &t354_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28848_GM};
static MethodInfo* t4311_MIs[] =
{
	&m28848_MI,
	NULL
};
static TypeInfo* t4311_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4311_0_0_0;
extern Il2CppType t4311_1_0_0;
struct t4311;
extern Il2CppGenericClass t4311_GC;
TypeInfo t4311_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4311_MIs, t4311_PIs, NULL, NULL, NULL, NULL, NULL, &t4311_TI, t4311_ITIs, NULL, &EmptyCustomAttributesCache, &t4311_TI, &t4311_0_0_0, &t4311_1_0_0, NULL, &t4311_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2784.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2784_TI;
#include "t2784MD.h"

extern TypeInfo t354_TI;
extern MethodInfo m15212_MI;
extern MethodInfo m21842_MI;
struct t20;
#define m21842(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>
extern Il2CppType t20_0_0_1;
FieldInfo t2784_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2784_TI, offsetof(t2784, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2784_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2784_TI, offsetof(t2784, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2784_FIs[] =
{
	&t2784_f0_FieldInfo,
	&t2784_f1_FieldInfo,
	NULL
};
extern MethodInfo m15209_MI;
static PropertyInfo t2784____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2784_TI, "System.Collections.IEnumerator.Current", &m15209_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2784____Current_PropertyInfo = 
{
	&t2784_TI, "Current", &m15212_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2784_PIs[] =
{
	&t2784____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2784____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2784_m15208_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15208_GM;
MethodInfo m15208_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2784_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2784_m15208_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15208_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15209_GM;
MethodInfo m15209_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2784_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15209_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15210_GM;
MethodInfo m15210_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2784_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15210_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15211_GM;
MethodInfo m15211_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2784_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15211_GM};
extern Il2CppType t354_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15212_GM;
MethodInfo m15212_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2784_TI, &t354_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15212_GM};
static MethodInfo* t2784_MIs[] =
{
	&m15208_MI,
	&m15209_MI,
	&m15210_MI,
	&m15211_MI,
	&m15212_MI,
	NULL
};
extern MethodInfo m15211_MI;
extern MethodInfo m15210_MI;
static MethodInfo* t2784_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15209_MI,
	&m15211_MI,
	&m15210_MI,
	&m15212_MI,
};
static TypeInfo* t2784_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4311_TI,
};
static Il2CppInterfaceOffsetPair t2784_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4311_TI, 7},
};
extern TypeInfo t354_TI;
static Il2CppRGCTXData t2784_RGCTXData[3] = 
{
	&m15212_MI/* Method Usage */,
	&t354_TI/* Class Usage */,
	&m21842_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2784_0_0_0;
extern Il2CppType t2784_1_0_0;
extern Il2CppGenericClass t2784_GC;
TypeInfo t2784_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2784_MIs, t2784_PIs, t2784_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2784_TI, t2784_ITIs, t2784_VT, &EmptyCustomAttributesCache, &t2784_TI, &t2784_0_0_0, &t2784_1_0_0, t2784_IOs, &t2784_GC, NULL, NULL, NULL, t2784_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2784)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5509_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>
extern MethodInfo m28849_MI;
extern MethodInfo m28850_MI;
static PropertyInfo t5509____Item_PropertyInfo = 
{
	&t5509_TI, "Item", &m28849_MI, &m28850_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5509_PIs[] =
{
	&t5509____Item_PropertyInfo,
	NULL
};
extern Il2CppType t354_0_0_0;
static ParameterInfo t5509_m28851_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t354_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28851_GM;
MethodInfo m28851_MI = 
{
	"IndexOf", NULL, &t5509_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5509_m28851_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28851_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t354_0_0_0;
static ParameterInfo t5509_m28852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t354_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28852_GM;
MethodInfo m28852_MI = 
{
	"Insert", NULL, &t5509_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5509_m28852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28852_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5509_m28853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28853_GM;
MethodInfo m28853_MI = 
{
	"RemoveAt", NULL, &t5509_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5509_m28853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28853_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5509_m28849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t354_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28849_GM;
MethodInfo m28849_MI = 
{
	"get_Item", NULL, &t5509_TI, &t354_0_0_0, RuntimeInvoker_t29_t44, t5509_m28849_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28849_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t354_0_0_0;
static ParameterInfo t5509_m28850_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t354_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28850_GM;
MethodInfo m28850_MI = 
{
	"set_Item", NULL, &t5509_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5509_m28850_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28850_GM};
static MethodInfo* t5509_MIs[] =
{
	&m28851_MI,
	&m28852_MI,
	&m28853_MI,
	&m28849_MI,
	&m28850_MI,
	NULL
};
static TypeInfo* t5509_ITIs[] = 
{
	&t603_TI,
	&t5508_TI,
	&t5510_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5509_0_0_0;
extern Il2CppType t5509_1_0_0;
struct t5509;
extern Il2CppGenericClass t5509_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5509_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5509_MIs, t5509_PIs, NULL, NULL, NULL, NULL, NULL, &t5509_TI, t5509_ITIs, NULL, &t1908__CustomAttributeCache, &t5509_TI, &t5509_0_0_0, &t5509_1_0_0, NULL, &t5509_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2785.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2785_TI;
#include "t2785MD.h"

#include "t2786.h"
extern TypeInfo t2786_TI;
#include "t2786MD.h"
extern MethodInfo m15215_MI;
extern MethodInfo m15217_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>
extern Il2CppType t316_0_0_33;
FieldInfo t2785_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2785_TI, offsetof(t2785, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2785_FIs[] =
{
	&t2785_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t273_0_0_0;
static ParameterInfo t2785_m15213_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15213_GM;
MethodInfo m15213_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2785_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2785_m15213_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15213_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2785_m15214_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15214_GM;
MethodInfo m15214_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2785_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2785_m15214_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15214_GM};
static MethodInfo* t2785_MIs[] =
{
	&m15213_MI,
	&m15214_MI,
	NULL
};
extern MethodInfo m15214_MI;
extern MethodInfo m15218_MI;
static MethodInfo* t2785_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15214_MI,
	&m15218_MI,
};
extern Il2CppType t2787_0_0_0;
extern TypeInfo t2787_TI;
extern MethodInfo m21852_MI;
extern TypeInfo t273_TI;
extern MethodInfo m15220_MI;
extern TypeInfo t273_TI;
static Il2CppRGCTXData t2785_RGCTXData[8] = 
{
	&t2787_0_0_0/* Type Usage */,
	&t2787_TI/* Class Usage */,
	&m21852_MI/* Method Usage */,
	&t273_TI/* Class Usage */,
	&m15220_MI/* Method Usage */,
	&m15215_MI/* Method Usage */,
	&t273_TI/* Class Usage */,
	&m15217_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2785_0_0_0;
extern Il2CppType t2785_1_0_0;
struct t2785;
extern Il2CppGenericClass t2785_GC;
TypeInfo t2785_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2785_MIs, NULL, t2785_FIs, NULL, &t2786_TI, NULL, NULL, &t2785_TI, NULL, t2785_VT, &EmptyCustomAttributesCache, &t2785_TI, &t2785_0_0_0, &t2785_1_0_0, NULL, &t2785_GC, NULL, NULL, NULL, t2785_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2785), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2787.h"
extern TypeInfo t2787_TI;
#include "t2787MD.h"
struct t556;
#define m21852(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>
extern Il2CppType t2787_0_0_1;
FieldInfo t2786_f0_FieldInfo = 
{
	"Delegate", &t2787_0_0_1, &t2786_TI, offsetof(t2786, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2786_FIs[] =
{
	&t2786_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2786_m15215_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15215_GM;
MethodInfo m15215_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2786_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2786_m15215_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15215_GM};
extern Il2CppType t2787_0_0_0;
static ParameterInfo t2786_m15216_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2787_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15216_GM;
MethodInfo m15216_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2786_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2786_m15216_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15216_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2786_m15217_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15217_GM;
MethodInfo m15217_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2786_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2786_m15217_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15217_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2786_m15218_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15218_GM;
MethodInfo m15218_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2786_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2786_m15218_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15218_GM};
static MethodInfo* t2786_MIs[] =
{
	&m15215_MI,
	&m15216_MI,
	&m15217_MI,
	&m15218_MI,
	NULL
};
static MethodInfo* t2786_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15217_MI,
	&m15218_MI,
};
extern TypeInfo t2787_TI;
extern TypeInfo t273_TI;
static Il2CppRGCTXData t2786_RGCTXData[5] = 
{
	&t2787_0_0_0/* Type Usage */,
	&t2787_TI/* Class Usage */,
	&m21852_MI/* Method Usage */,
	&t273_TI/* Class Usage */,
	&m15220_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2786_0_0_0;
extern Il2CppType t2786_1_0_0;
struct t2786;
extern Il2CppGenericClass t2786_GC;
TypeInfo t2786_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2786_MIs, NULL, t2786_FIs, NULL, &t556_TI, NULL, NULL, &t2786_TI, NULL, t2786_VT, &EmptyCustomAttributesCache, &t2786_TI, &t2786_0_0_0, &t2786_1_0_0, NULL, &t2786_GC, NULL, NULL, NULL, t2786_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2786), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2787_m15219_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15219_GM;
MethodInfo m15219_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2787_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2787_m15219_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15219_GM};
extern Il2CppType t273_0_0_0;
static ParameterInfo t2787_m15220_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15220_GM;
MethodInfo m15220_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2787_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2787_m15220_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15220_GM};
extern Il2CppType t273_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2787_m15221_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t273_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15221_GM;
MethodInfo m15221_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2787_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2787_m15221_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15221_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2787_m15222_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15222_GM;
MethodInfo m15222_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2787_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2787_m15222_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15222_GM};
static MethodInfo* t2787_MIs[] =
{
	&m15219_MI,
	&m15220_MI,
	&m15221_MI,
	&m15222_MI,
	NULL
};
extern MethodInfo m15221_MI;
extern MethodInfo m15222_MI;
static MethodInfo* t2787_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15220_MI,
	&m15221_MI,
	&m15222_MI,
};
static Il2CppInterfaceOffsetPair t2787_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2787_1_0_0;
struct t2787;
extern Il2CppGenericClass t2787_GC;
TypeInfo t2787_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2787_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2787_TI, NULL, t2787_VT, &EmptyCustomAttributesCache, &t2787_TI, &t2787_0_0_0, &t2787_1_0_0, t2787_IOs, &t2787_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2787), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t276.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t276_TI;
#include "t276MD.h"

#include "t2788.h"
#include "t277.h"
#include "t278.h"
extern TypeInfo t2788_TI;
extern TypeInfo t44_TI;
extern TypeInfo t278_TI;
extern TypeInfo t277_TI;
#include "t2788MD.h"
#include "t29MD.h"
#include "t931MD.h"
#include "t277MD.h"
#include "t293MD.h"
extern MethodInfo m15223_MI;
extern MethodInfo m15226_MI;
extern MethodInfo m15236_MI;
extern MethodInfo m15227_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m21870_MI;
extern MethodInfo m15224_MI;
extern MethodInfo m15234_MI;
extern MethodInfo m15248_MI;
extern MethodInfo m15233_MI;
extern MethodInfo m2868_MI;
extern MethodInfo m1296_MI;
extern MethodInfo m15235_MI;
struct t931;
#include "t931.h"
struct t931;
 t29 * m20008_gshared (t29 * __this, MethodInfo* method);
#define m20008(__this, method) (t29 *)m20008_gshared((t29 *)__this, method)
#define m21870(__this, method) (t278 *)m20008_gshared((t29 *)__this, method)


// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType t2788_0_0_33;
FieldInfo t276_f0_FieldInfo = 
{
	"m_Stack", &t2788_0_0_33, &t276_TI, offsetof(t276, f0), &EmptyCustomAttributesCache};
extern Il2CppType t277_0_0_33;
FieldInfo t276_f1_FieldInfo = 
{
	"m_ActionOnGet", &t277_0_0_33, &t276_TI, offsetof(t276, f1), &EmptyCustomAttributesCache};
extern Il2CppType t277_0_0_33;
FieldInfo t276_f2_FieldInfo = 
{
	"m_ActionOnRelease", &t277_0_0_33, &t276_TI, offsetof(t276, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo t276_f3_FieldInfo = 
{
	"<countAll>k__BackingField", &t44_0_0_1, &t276_TI, offsetof(t276, f3), &t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField};
static FieldInfo* t276_FIs[] =
{
	&t276_f0_FieldInfo,
	&t276_f1_FieldInfo,
	&t276_f2_FieldInfo,
	&t276_f3_FieldInfo,
	NULL
};
static PropertyInfo t276____countAll_PropertyInfo = 
{
	&t276_TI, "countAll", &m15223_MI, &m15224_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15225_MI;
static PropertyInfo t276____countActive_PropertyInfo = 
{
	&t276_TI, "countActive", &m15225_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t276____countInactive_PropertyInfo = 
{
	&t276_TI, "countInactive", &m15226_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t276_PIs[] =
{
	&t276____countAll_PropertyInfo,
	&t276____countActive_PropertyInfo,
	&t276____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType t277_0_0_0;
extern Il2CppType t277_0_0_0;
extern Il2CppType t277_0_0_0;
static ParameterInfo t276_m2027_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &t277_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &t277_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2027_GM;
MethodInfo m2027_MI = 
{
	".ctor", (methodPointerType)&m11051_gshared, &t276_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t276_m2027_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2027_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern Il2CppGenericMethod m15223_GM;
MethodInfo m15223_MI = 
{
	"get_countAll", (methodPointerType)&m11053_gshared, &t276_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t282__CustomAttributeCache_m2037, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15223_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t276_m15224_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
extern Il2CppGenericMethod m15224_GM;
MethodInfo m15224_MI = 
{
	"set_countAll", (methodPointerType)&m11055_gshared, &t276_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t276_m15224_ParameterInfos, &t282__CustomAttributeCache_m2038, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15224_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15225_GM;
MethodInfo m15225_MI = 
{
	"get_countActive", (methodPointerType)&m11057_gshared, &t276_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15225_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15226_GM;
MethodInfo m15226_MI = 
{
	"get_countInactive", (methodPointerType)&m11059_gshared, &t276_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15226_GM};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2028_GM;
MethodInfo m2028_MI = 
{
	"Get", (methodPointerType)&m11061_gshared, &t276_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2028_GM};
extern Il2CppType t278_0_0_0;
extern Il2CppType t278_0_0_0;
static ParameterInfo t276_m2029_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2029_GM;
MethodInfo m2029_MI = 
{
	"Release", (methodPointerType)&m11063_gshared, &t276_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t276_m2029_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2029_GM};
static MethodInfo* t276_MIs[] =
{
	&m2027_MI,
	&m15223_MI,
	&m15224_MI,
	&m15225_MI,
	&m15226_MI,
	&m2028_MI,
	&m2029_MI,
	NULL
};
static MethodInfo* t276_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t2788_TI;
extern TypeInfo t278_TI;
static Il2CppRGCTXData t276_RGCTXData[12] = 
{
	&t2788_TI/* Class Usage */,
	&m15227_MI/* Method Usage */,
	&m15223_MI/* Method Usage */,
	&m15226_MI/* Method Usage */,
	&m15236_MI/* Method Usage */,
	&t278_TI/* Class Usage */,
	&m21870_MI/* Method Usage */,
	&m15224_MI/* Method Usage */,
	&m15234_MI/* Method Usage */,
	&m15248_MI/* Method Usage */,
	&m15233_MI/* Method Usage */,
	&m15235_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t276_0_0_0;
extern Il2CppType t276_1_0_0;
struct t276;
extern Il2CppGenericClass t276_GC;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
TypeInfo t276_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ObjectPool`1", "UnityEngine.UI", t276_MIs, t276_PIs, t276_FIs, NULL, &t29_TI, NULL, NULL, &t276_TI, NULL, t276_VT, &EmptyCustomAttributesCache, &t276_TI, &t276_0_0_0, &t276_1_0_0, NULL, &t276_GC, NULL, NULL, NULL, t276_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t276), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 3, 4, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2791.h"
extern TypeInfo t1622_TI;
extern TypeInfo t2791_TI;
#include "t2791MD.h"
extern MethodInfo m4199_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m8852_MI;
extern MethodInfo m15237_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m21869_MI;
extern MethodInfo m15243_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m21869(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)


 t2791  m15237 (t2788 * __this, MethodInfo* method){
	{
		t2791  L_0 = {0};
		m15243(&L_0, __this, &m15243_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType t44_0_0_32849;
FieldInfo t2788_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2788_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2789_0_0_1;
FieldInfo t2788_f1_FieldInfo = 
{
	"_array", &t2789_0_0_1, &t2788_TI, offsetof(t2788, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2788_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2788_TI, offsetof(t2788, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2788_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2788_TI, offsetof(t2788, f3), &EmptyCustomAttributesCache};
static FieldInfo* t2788_FIs[] =
{
	&t2788_f0_FieldInfo,
	&t2788_f1_FieldInfo,
	&t2788_f2_FieldInfo,
	&t2788_f3_FieldInfo,
	NULL
};
static const int32_t t2788_f0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t2788_f0_DefaultValue = 
{
	&t2788_f0_FieldInfo, { (char*)&t2788_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2788_FDVs[] = 
{
	&t2788_f0_DefaultValue,
	NULL
};
extern MethodInfo m15228_MI;
static PropertyInfo t2788____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2788_TI, "System.Collections.ICollection.IsSynchronized", &m15228_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15229_MI;
static PropertyInfo t2788____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2788_TI, "System.Collections.ICollection.SyncRoot", &m15229_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2788____Count_PropertyInfo = 
{
	&t2788_TI, "Count", &m15236_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2788_PIs[] =
{
	&t2788____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2788____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2788____Count_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15227_GM;
MethodInfo m15227_MI = 
{
	".ctor", (methodPointerType)&m11064_gshared, &t2788_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15227_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15228_GM;
MethodInfo m15228_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11065_gshared, &t2788_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15228_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15229_GM;
MethodInfo m15229_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11066_gshared, &t2788_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15229_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2788_m15230_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15230_GM;
MethodInfo m15230_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11067_gshared, &t2788_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2788_m15230_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15230_GM};
extern Il2CppType t2790_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15231_GM;
MethodInfo m15231_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11068_gshared, &t2788_TI, &t2790_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15231_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15232_GM;
MethodInfo m15232_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11069_gshared, &t2788_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15232_GM};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15233_GM;
MethodInfo m15233_MI = 
{
	"Peek", (methodPointerType)&m11070_gshared, &t2788_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15233_GM};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15234_GM;
MethodInfo m15234_MI = 
{
	"Pop", (methodPointerType)&m11071_gshared, &t2788_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15234_GM};
extern Il2CppType t278_0_0_0;
static ParameterInfo t2788_m15235_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15235_GM;
MethodInfo m15235_MI = 
{
	"Push", (methodPointerType)&m11072_gshared, &t2788_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2788_m15235_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15235_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15236_GM;
MethodInfo m15236_MI = 
{
	"get_Count", (methodPointerType)&m11073_gshared, &t2788_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15236_GM};
extern Il2CppType t2791_0_0_0;
extern void* RuntimeInvoker_t2791 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15237_GM;
MethodInfo m15237_MI = 
{
	"GetEnumerator", (methodPointerType)&m15237, &t2788_TI, &t2791_0_0_0, RuntimeInvoker_t2791, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15237_GM};
static MethodInfo* t2788_MIs[] =
{
	&m15227_MI,
	&m15228_MI,
	&m15229_MI,
	&m15230_MI,
	&m15231_MI,
	&m15232_MI,
	&m15233_MI,
	&m15234_MI,
	&m15235_MI,
	&m15236_MI,
	&m15237_MI,
	NULL
};
extern MethodInfo m15230_MI;
extern MethodInfo m15232_MI;
extern MethodInfo m15231_MI;
static MethodInfo* t2788_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15236_MI,
	&m15228_MI,
	&m15229_MI,
	&m15230_MI,
	&m15232_MI,
	&m15231_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t5513_TI;
static TypeInfo* t2788_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
	&t5513_TI,
};
static Il2CppInterfaceOffsetPair t2788_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
	{ &t5513_TI, 9},
};
extern TypeInfo t2791_TI;
static Il2CppRGCTXData t2788_RGCTXData[4] = 
{
	&m15237_MI/* Method Usage */,
	&t2791_TI/* Class Usage */,
	&m21869_MI/* Method Usage */,
	&m15243_MI/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2788_0_0_0;
extern Il2CppType t2788_1_0_0;
struct t2788;
extern Il2CppGenericClass t2788_GC;
extern CustomAttributesCache t717__CustomAttributeCache;
TypeInfo t2788_TI = 
{
	&g_System_dll_Image, NULL, "Stack`1", "System.Collections.Generic", t2788_MIs, t2788_PIs, t2788_FIs, NULL, &t29_TI, NULL, NULL, &t2788_TI, t2788_ITIs, t2788_VT, &t717__CustomAttributeCache, &t2788_TI, &t2788_0_0_0, &t2788_1_0_0, t2788_IOs, &t2788_GC, NULL, t2788_FDVs, NULL, t2788_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2788), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 11, 3, 4, 0, 0, 10, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType t2790_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28854_GM;
MethodInfo m28854_MI = 
{
	"GetEnumerator", NULL, &t5513_TI, &t2790_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28854_GM};
static MethodInfo* t5513_MIs[] =
{
	&m28854_MI,
	NULL
};
static TypeInfo* t5513_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5513_0_0_0;
extern Il2CppType t5513_1_0_0;
struct t5513;
extern Il2CppGenericClass t5513_GC;
TypeInfo t5513_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5513_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5513_TI, t5513_ITIs, NULL, &EmptyCustomAttributesCache, &t5513_TI, &t5513_0_0_0, &t5513_1_0_0, NULL, &t5513_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2790_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern MethodInfo m28855_MI;
static PropertyInfo t2790____Current_PropertyInfo = 
{
	&t2790_TI, "Current", &m28855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2790_PIs[] =
{
	&t2790____Current_PropertyInfo,
	NULL
};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28855_GM;
MethodInfo m28855_MI = 
{
	"get_Current", NULL, &t2790_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28855_GM};
static MethodInfo* t2790_MIs[] =
{
	&m28855_MI,
	NULL
};
static TypeInfo* t2790_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2790_0_0_0;
extern Il2CppType t2790_1_0_0;
struct t2790;
extern Il2CppGenericClass t2790_GC;
TypeInfo t2790_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2790_MIs, t2790_PIs, NULL, NULL, NULL, NULL, NULL, &t2790_TI, t2790_ITIs, NULL, &EmptyCustomAttributesCache, &t2790_TI, &t2790_0_0_0, &t2790_1_0_0, NULL, &t2790_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2792.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2792_TI;
#include "t2792MD.h"

extern MethodInfo m15242_MI;
extern MethodInfo m21858_MI;
struct t20;
#define m21858(__this, p0, method) (t278 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType t20_0_0_1;
FieldInfo t2792_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2792_TI, offsetof(t2792, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2792_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2792_TI, offsetof(t2792, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2792_FIs[] =
{
	&t2792_f0_FieldInfo,
	&t2792_f1_FieldInfo,
	NULL
};
extern MethodInfo m15239_MI;
static PropertyInfo t2792____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2792_TI, "System.Collections.IEnumerator.Current", &m15239_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2792____Current_PropertyInfo = 
{
	&t2792_TI, "Current", &m15242_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2792_PIs[] =
{
	&t2792____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2792____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2792_m15238_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15238_GM;
MethodInfo m15238_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2792_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2792_m15238_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15238_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15239_GM;
MethodInfo m15239_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2792_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15239_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15240_GM;
MethodInfo m15240_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2792_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15240_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15241_GM;
MethodInfo m15241_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2792_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15241_GM};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15242_GM;
MethodInfo m15242_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2792_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15242_GM};
static MethodInfo* t2792_MIs[] =
{
	&m15238_MI,
	&m15239_MI,
	&m15240_MI,
	&m15241_MI,
	&m15242_MI,
	NULL
};
extern MethodInfo m15241_MI;
extern MethodInfo m15240_MI;
static MethodInfo* t2792_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15239_MI,
	&m15241_MI,
	&m15240_MI,
	&m15242_MI,
};
static TypeInfo* t2792_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2790_TI,
};
static Il2CppInterfaceOffsetPair t2792_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2790_TI, 7},
};
extern TypeInfo t278_TI;
static Il2CppRGCTXData t2792_RGCTXData[3] = 
{
	&m15242_MI/* Method Usage */,
	&t278_TI/* Class Usage */,
	&m21858_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2792_0_0_0;
extern Il2CppType t2792_1_0_0;
extern Il2CppGenericClass t2792_GC;
TypeInfo t2792_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2792_MIs, t2792_PIs, t2792_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2792_TI, t2792_ITIs, t2792_VT, &EmptyCustomAttributesCache, &t2792_TI, &t2792_0_0_0, &t2792_1_0_0, t2792_IOs, &t2792_GC, NULL, NULL, NULL, t2792_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2792)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5511_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern MethodInfo m28856_MI;
static PropertyInfo t5511____Count_PropertyInfo = 
{
	&t5511_TI, "Count", &m28856_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28857_MI;
static PropertyInfo t5511____IsReadOnly_PropertyInfo = 
{
	&t5511_TI, "IsReadOnly", &m28857_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5511_PIs[] =
{
	&t5511____Count_PropertyInfo,
	&t5511____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28856_GM;
MethodInfo m28856_MI = 
{
	"get_Count", NULL, &t5511_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28856_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28857_GM;
MethodInfo m28857_MI = 
{
	"get_IsReadOnly", NULL, &t5511_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28857_GM};
extern Il2CppType t278_0_0_0;
static ParameterInfo t5511_m28858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28858_GM;
MethodInfo m28858_MI = 
{
	"Add", NULL, &t5511_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5511_m28858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28858_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28859_GM;
MethodInfo m28859_MI = 
{
	"Clear", NULL, &t5511_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28859_GM};
extern Il2CppType t278_0_0_0;
static ParameterInfo t5511_m28860_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28860_GM;
MethodInfo m28860_MI = 
{
	"Contains", NULL, &t5511_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5511_m28860_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28860_GM};
extern Il2CppType t2789_0_0_0;
extern Il2CppType t2789_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5511_m28861_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2789_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28861_GM;
MethodInfo m28861_MI = 
{
	"CopyTo", NULL, &t5511_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5511_m28861_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28861_GM};
extern Il2CppType t278_0_0_0;
static ParameterInfo t5511_m28862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28862_GM;
MethodInfo m28862_MI = 
{
	"Remove", NULL, &t5511_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5511_m28862_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28862_GM};
static MethodInfo* t5511_MIs[] =
{
	&m28856_MI,
	&m28857_MI,
	&m28858_MI,
	&m28859_MI,
	&m28860_MI,
	&m28861_MI,
	&m28862_MI,
	NULL
};
static TypeInfo* t5511_ITIs[] = 
{
	&t603_TI,
	&t5513_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5511_0_0_0;
extern Il2CppType t5511_1_0_0;
struct t5511;
extern Il2CppGenericClass t5511_GC;
TypeInfo t5511_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5511_MIs, t5511_PIs, NULL, NULL, NULL, NULL, NULL, &t5511_TI, t5511_ITIs, NULL, &EmptyCustomAttributesCache, &t5511_TI, &t5511_0_0_0, &t5511_1_0_0, NULL, &t5511_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5512_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern MethodInfo m28863_MI;
extern MethodInfo m28864_MI;
static PropertyInfo t5512____Item_PropertyInfo = 
{
	&t5512_TI, "Item", &m28863_MI, &m28864_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5512_PIs[] =
{
	&t5512____Item_PropertyInfo,
	NULL
};
extern Il2CppType t278_0_0_0;
static ParameterInfo t5512_m28865_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28865_GM;
MethodInfo m28865_MI = 
{
	"IndexOf", NULL, &t5512_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5512_m28865_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28865_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t278_0_0_0;
static ParameterInfo t5512_m28866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28866_GM;
MethodInfo m28866_MI = 
{
	"Insert", NULL, &t5512_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5512_m28866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28866_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5512_m28867_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28867_GM;
MethodInfo m28867_MI = 
{
	"RemoveAt", NULL, &t5512_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5512_m28867_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28867_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5512_m28863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28863_GM;
MethodInfo m28863_MI = 
{
	"get_Item", NULL, &t5512_TI, &t278_0_0_0, RuntimeInvoker_t29_t44, t5512_m28863_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28863_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t278_0_0_0;
static ParameterInfo t5512_m28864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28864_GM;
MethodInfo m28864_MI = 
{
	"set_Item", NULL, &t5512_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5512_m28864_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28864_GM};
static MethodInfo* t5512_MIs[] =
{
	&m28865_MI,
	&m28866_MI,
	&m28867_MI,
	&m28863_MI,
	&m28864_MI,
	NULL
};
static TypeInfo* t5512_ITIs[] = 
{
	&t603_TI,
	&t5511_TI,
	&t5513_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5512_0_0_0;
extern Il2CppType t5512_1_0_0;
struct t5512;
extern Il2CppGenericClass t5512_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5512_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5512_MIs, t5512_PIs, NULL, NULL, NULL, NULL, NULL, &t5512_TI, t5512_ITIs, NULL, &t1908__CustomAttributeCache, &t5512_TI, &t5512_0_0_0, &t5512_1_0_0, NULL, &t5512_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15247_MI;


// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType t2788_0_0_1;
FieldInfo t2791_f0_FieldInfo = 
{
	"parent", &t2788_0_0_1, &t2791_TI, offsetof(t2791, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2791_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2791_TI, offsetof(t2791, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2791_f2_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2791_TI, offsetof(t2791, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2791_FIs[] =
{
	&t2791_f0_FieldInfo,
	&t2791_f1_FieldInfo,
	&t2791_f2_FieldInfo,
	NULL
};
extern MethodInfo m15244_MI;
static PropertyInfo t2791____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2791_TI, "System.Collections.IEnumerator.Current", &m15244_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2791____Current_PropertyInfo = 
{
	&t2791_TI, "Current", &m15247_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2791_PIs[] =
{
	&t2791____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2791____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2788_0_0_0;
static ParameterInfo t2791_m15243_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t2788_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15243_GM;
MethodInfo m15243_MI = 
{
	".ctor", (methodPointerType)&m11075_gshared, &t2791_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2791_m15243_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15243_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15244_GM;
MethodInfo m15244_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11076_gshared, &t2791_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15244_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15245_GM;
MethodInfo m15245_MI = 
{
	"Dispose", (methodPointerType)&m11077_gshared, &t2791_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15245_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15246_GM;
MethodInfo m15246_MI = 
{
	"MoveNext", (methodPointerType)&m11078_gshared, &t2791_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15246_GM};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15247_GM;
MethodInfo m15247_MI = 
{
	"get_Current", (methodPointerType)&m11079_gshared, &t2791_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15247_GM};
static MethodInfo* t2791_MIs[] =
{
	&m15243_MI,
	&m15244_MI,
	&m15245_MI,
	&m15246_MI,
	&m15247_MI,
	NULL
};
extern MethodInfo m15246_MI;
extern MethodInfo m15245_MI;
static MethodInfo* t2791_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15244_MI,
	&m15246_MI,
	&m15245_MI,
	&m15247_MI,
};
static TypeInfo* t2791_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2790_TI,
};
static Il2CppInterfaceOffsetPair t2791_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2790_TI, 7},
};
extern TypeInfo t278_TI;
static Il2CppRGCTXData t2791_RGCTXData[2] = 
{
	&m15247_MI/* Method Usage */,
	&t278_TI/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2791_0_0_0;
extern Il2CppType t2791_1_0_0;
extern Il2CppGenericClass t2791_GC;
extern TypeInfo t717_TI;
TypeInfo t2791_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t2791_MIs, t2791_PIs, t2791_FIs, NULL, &t110_TI, NULL, &t717_TI, &t2791_TI, t2791_ITIs, t2791_VT, &EmptyCustomAttributesCache, &t2791_TI, &t2791_0_0_0, &t2791_1_0_0, t2791_IOs, &t2791_GC, NULL, NULL, NULL, t2791_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2791)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t277_m2026_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2026_GM;
MethodInfo m2026_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t277_m2026_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2026_GM};
extern Il2CppType t278_0_0_0;
static ParameterInfo t277_m15248_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15248_GM;
MethodInfo m15248_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t277_m15248_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15248_GM};
extern Il2CppType t278_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t277_m15249_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15249_GM;
MethodInfo m15249_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t277_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t277_m15249_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15249_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t277_m15250_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15250_GM;
MethodInfo m15250_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t277_m15250_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15250_GM};
static MethodInfo* t277_MIs[] =
{
	&m2026_MI,
	&m15248_MI,
	&m15249_MI,
	&m15250_MI,
	NULL
};
extern MethodInfo m15249_MI;
extern MethodInfo m15250_MI;
static MethodInfo* t277_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15248_MI,
	&m15249_MI,
	&m15250_MI,
};
static Il2CppInterfaceOffsetPair t277_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t277_1_0_0;
struct t277;
extern Il2CppGenericClass t277_GC;
TypeInfo t277_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t277_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t277_TI, NULL, t277_VT, &EmptyCustomAttributesCache, &t277_TI, &t277_0_0_0, &t277_1_0_0, t277_IOs, &t277_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t277), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t280.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t280_TI;
#include "t280MD.h"

#include "t2793.h"
#include "t281.h"
#include "t268.h"
extern TypeInfo t2793_TI;
extern TypeInfo t268_TI;
extern TypeInfo t281_TI;
#include "t2793MD.h"
#include "t281MD.h"
extern MethodInfo m15251_MI;
extern MethodInfo m15254_MI;
extern MethodInfo m15264_MI;
extern MethodInfo m15255_MI;
extern MethodInfo m21884_MI;
extern MethodInfo m15252_MI;
extern MethodInfo m15262_MI;
extern MethodInfo m15276_MI;
extern MethodInfo m15261_MI;
extern MethodInfo m15263_MI;
struct t931;
#define m21884(__this, method) (t268 *)m20008_gshared((t29 *)__this, method)


// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType t2793_0_0_33;
FieldInfo t280_f0_FieldInfo = 
{
	"m_Stack", &t2793_0_0_33, &t280_TI, offsetof(t280, f0), &EmptyCustomAttributesCache};
extern Il2CppType t281_0_0_33;
FieldInfo t280_f1_FieldInfo = 
{
	"m_ActionOnGet", &t281_0_0_33, &t280_TI, offsetof(t280, f1), &EmptyCustomAttributesCache};
extern Il2CppType t281_0_0_33;
FieldInfo t280_f2_FieldInfo = 
{
	"m_ActionOnRelease", &t281_0_0_33, &t280_TI, offsetof(t280, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo t280_f3_FieldInfo = 
{
	"<countAll>k__BackingField", &t44_0_0_1, &t280_TI, offsetof(t280, f3), &t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField};
static FieldInfo* t280_FIs[] =
{
	&t280_f0_FieldInfo,
	&t280_f1_FieldInfo,
	&t280_f2_FieldInfo,
	&t280_f3_FieldInfo,
	NULL
};
static PropertyInfo t280____countAll_PropertyInfo = 
{
	&t280_TI, "countAll", &m15251_MI, &m15252_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15253_MI;
static PropertyInfo t280____countActive_PropertyInfo = 
{
	&t280_TI, "countActive", &m15253_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t280____countInactive_PropertyInfo = 
{
	&t280_TI, "countInactive", &m15254_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t280_PIs[] =
{
	&t280____countAll_PropertyInfo,
	&t280____countActive_PropertyInfo,
	&t280____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType t281_0_0_0;
extern Il2CppType t281_0_0_0;
extern Il2CppType t281_0_0_0;
static ParameterInfo t280_m2032_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &t281_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &t281_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2032_GM;
MethodInfo m2032_MI = 
{
	".ctor", (methodPointerType)&m11051_gshared, &t280_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t280_m2032_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2032_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern Il2CppGenericMethod m15251_GM;
MethodInfo m15251_MI = 
{
	"get_countAll", (methodPointerType)&m11053_gshared, &t280_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t282__CustomAttributeCache_m2037, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15251_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t280_m15252_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
extern Il2CppGenericMethod m15252_GM;
MethodInfo m15252_MI = 
{
	"set_countAll", (methodPointerType)&m11055_gshared, &t280_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t280_m15252_ParameterInfos, &t282__CustomAttributeCache_m2038, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15252_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15253_GM;
MethodInfo m15253_MI = 
{
	"get_countActive", (methodPointerType)&m11057_gshared, &t280_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15253_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15254_GM;
MethodInfo m15254_MI = 
{
	"get_countInactive", (methodPointerType)&m11059_gshared, &t280_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15254_GM};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2033_GM;
MethodInfo m2033_MI = 
{
	"Get", (methodPointerType)&m11061_gshared, &t280_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2033_GM};
extern Il2CppType t268_0_0_0;
extern Il2CppType t268_0_0_0;
static ParameterInfo t280_m2034_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2034_GM;
MethodInfo m2034_MI = 
{
	"Release", (methodPointerType)&m11063_gshared, &t280_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t280_m2034_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2034_GM};
static MethodInfo* t280_MIs[] =
{
	&m2032_MI,
	&m15251_MI,
	&m15252_MI,
	&m15253_MI,
	&m15254_MI,
	&m2033_MI,
	&m2034_MI,
	NULL
};
static MethodInfo* t280_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t2793_TI;
extern TypeInfo t268_TI;
static Il2CppRGCTXData t280_RGCTXData[12] = 
{
	&t2793_TI/* Class Usage */,
	&m15255_MI/* Method Usage */,
	&m15251_MI/* Method Usage */,
	&m15254_MI/* Method Usage */,
	&m15264_MI/* Method Usage */,
	&t268_TI/* Class Usage */,
	&m21884_MI/* Method Usage */,
	&m15252_MI/* Method Usage */,
	&m15262_MI/* Method Usage */,
	&m15276_MI/* Method Usage */,
	&m15261_MI/* Method Usage */,
	&m15263_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t280_0_0_0;
extern Il2CppType t280_1_0_0;
struct t280;
extern Il2CppGenericClass t280_GC;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
TypeInfo t280_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ObjectPool`1", "UnityEngine.UI", t280_MIs, t280_PIs, t280_FIs, NULL, &t29_TI, NULL, NULL, &t280_TI, NULL, t280_VT, &EmptyCustomAttributesCache, &t280_TI, &t280_0_0_0, &t280_1_0_0, NULL, &t280_GC, NULL, NULL, NULL, t280_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t280), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 3, 4, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2796.h"
extern TypeInfo t2796_TI;
#include "t2796MD.h"
extern MethodInfo m15265_MI;
extern MethodInfo m21883_MI;
extern MethodInfo m15271_MI;
struct t20;
#define m21883(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)


 t2796  m15265 (t2793 * __this, MethodInfo* method){
	{
		t2796  L_0 = {0};
		m15271(&L_0, __this, &m15271_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType t44_0_0_32849;
FieldInfo t2793_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2793_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2794_0_0_1;
FieldInfo t2793_f1_FieldInfo = 
{
	"_array", &t2794_0_0_1, &t2793_TI, offsetof(t2793, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2793_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2793_TI, offsetof(t2793, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2793_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2793_TI, offsetof(t2793, f3), &EmptyCustomAttributesCache};
static FieldInfo* t2793_FIs[] =
{
	&t2793_f0_FieldInfo,
	&t2793_f1_FieldInfo,
	&t2793_f2_FieldInfo,
	&t2793_f3_FieldInfo,
	NULL
};
static const int32_t t2793_f0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t2793_f0_DefaultValue = 
{
	&t2793_f0_FieldInfo, { (char*)&t2793_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2793_FDVs[] = 
{
	&t2793_f0_DefaultValue,
	NULL
};
extern MethodInfo m15256_MI;
static PropertyInfo t2793____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2793_TI, "System.Collections.ICollection.IsSynchronized", &m15256_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15257_MI;
static PropertyInfo t2793____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2793_TI, "System.Collections.ICollection.SyncRoot", &m15257_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2793____Count_PropertyInfo = 
{
	&t2793_TI, "Count", &m15264_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2793_PIs[] =
{
	&t2793____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2793____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2793____Count_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15255_GM;
MethodInfo m15255_MI = 
{
	".ctor", (methodPointerType)&m11064_gshared, &t2793_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15255_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15256_GM;
MethodInfo m15256_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11065_gshared, &t2793_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15256_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15257_GM;
MethodInfo m15257_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11066_gshared, &t2793_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15257_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2793_m15258_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15258_GM;
MethodInfo m15258_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11067_gshared, &t2793_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2793_m15258_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15258_GM};
extern Il2CppType t2795_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15259_GM;
MethodInfo m15259_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11068_gshared, &t2793_TI, &t2795_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15259_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15260_GM;
MethodInfo m15260_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11069_gshared, &t2793_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15260_GM};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15261_GM;
MethodInfo m15261_MI = 
{
	"Peek", (methodPointerType)&m11070_gshared, &t2793_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15261_GM};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15262_GM;
MethodInfo m15262_MI = 
{
	"Pop", (methodPointerType)&m11071_gshared, &t2793_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15262_GM};
extern Il2CppType t268_0_0_0;
static ParameterInfo t2793_m15263_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15263_GM;
MethodInfo m15263_MI = 
{
	"Push", (methodPointerType)&m11072_gshared, &t2793_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2793_m15263_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15263_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15264_GM;
MethodInfo m15264_MI = 
{
	"get_Count", (methodPointerType)&m11073_gshared, &t2793_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15264_GM};
extern Il2CppType t2796_0_0_0;
extern void* RuntimeInvoker_t2796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15265_GM;
MethodInfo m15265_MI = 
{
	"GetEnumerator", (methodPointerType)&m15265, &t2793_TI, &t2796_0_0_0, RuntimeInvoker_t2796, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15265_GM};
static MethodInfo* t2793_MIs[] =
{
	&m15255_MI,
	&m15256_MI,
	&m15257_MI,
	&m15258_MI,
	&m15259_MI,
	&m15260_MI,
	&m15261_MI,
	&m15262_MI,
	&m15263_MI,
	&m15264_MI,
	&m15265_MI,
	NULL
};
extern MethodInfo m15258_MI;
extern MethodInfo m15260_MI;
extern MethodInfo m15259_MI;
static MethodInfo* t2793_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15264_MI,
	&m15256_MI,
	&m15257_MI,
	&m15258_MI,
	&m15260_MI,
	&m15259_MI,
};
extern TypeInfo t5516_TI;
static TypeInfo* t2793_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
	&t5516_TI,
};
static Il2CppInterfaceOffsetPair t2793_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
	{ &t5516_TI, 9},
};
extern TypeInfo t2796_TI;
static Il2CppRGCTXData t2793_RGCTXData[4] = 
{
	&m15265_MI/* Method Usage */,
	&t2796_TI/* Class Usage */,
	&m21883_MI/* Method Usage */,
	&m15271_MI/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2793_0_0_0;
extern Il2CppType t2793_1_0_0;
struct t2793;
extern Il2CppGenericClass t2793_GC;
extern CustomAttributesCache t717__CustomAttributeCache;
TypeInfo t2793_TI = 
{
	&g_System_dll_Image, NULL, "Stack`1", "System.Collections.Generic", t2793_MIs, t2793_PIs, t2793_FIs, NULL, &t29_TI, NULL, NULL, &t2793_TI, t2793_ITIs, t2793_VT, &t717__CustomAttributeCache, &t2793_TI, &t2793_0_0_0, &t2793_1_0_0, t2793_IOs, &t2793_GC, NULL, t2793_FDVs, NULL, t2793_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2793), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 11, 3, 4, 0, 0, 10, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType t2795_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28868_GM;
MethodInfo m28868_MI = 
{
	"GetEnumerator", NULL, &t5516_TI, &t2795_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28868_GM};
static MethodInfo* t5516_MIs[] =
{
	&m28868_MI,
	NULL
};
static TypeInfo* t5516_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5516_0_0_0;
extern Il2CppType t5516_1_0_0;
struct t5516;
extern Il2CppGenericClass t5516_GC;
TypeInfo t5516_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5516_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5516_TI, t5516_ITIs, NULL, &EmptyCustomAttributesCache, &t5516_TI, &t5516_0_0_0, &t5516_1_0_0, NULL, &t5516_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2795_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern MethodInfo m28869_MI;
static PropertyInfo t2795____Current_PropertyInfo = 
{
	&t2795_TI, "Current", &m28869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2795_PIs[] =
{
	&t2795____Current_PropertyInfo,
	NULL
};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28869_GM;
MethodInfo m28869_MI = 
{
	"get_Current", NULL, &t2795_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28869_GM};
static MethodInfo* t2795_MIs[] =
{
	&m28869_MI,
	NULL
};
static TypeInfo* t2795_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2795_0_0_0;
extern Il2CppType t2795_1_0_0;
struct t2795;
extern Il2CppGenericClass t2795_GC;
TypeInfo t2795_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2795_MIs, t2795_PIs, NULL, NULL, NULL, NULL, NULL, &t2795_TI, t2795_ITIs, NULL, &EmptyCustomAttributesCache, &t2795_TI, &t2795_0_0_0, &t2795_1_0_0, NULL, &t2795_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2797.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2797_TI;
#include "t2797MD.h"

extern MethodInfo m15270_MI;
extern MethodInfo m21872_MI;
struct t20;
#define m21872(__this, p0, method) (t268 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType t20_0_0_1;
FieldInfo t2797_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2797_TI, offsetof(t2797, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2797_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2797_TI, offsetof(t2797, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2797_FIs[] =
{
	&t2797_f0_FieldInfo,
	&t2797_f1_FieldInfo,
	NULL
};
extern MethodInfo m15267_MI;
static PropertyInfo t2797____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2797_TI, "System.Collections.IEnumerator.Current", &m15267_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2797____Current_PropertyInfo = 
{
	&t2797_TI, "Current", &m15270_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2797_PIs[] =
{
	&t2797____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2797____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2797_m15266_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15266_GM;
MethodInfo m15266_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2797_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2797_m15266_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15266_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15267_GM;
MethodInfo m15267_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2797_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15267_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15268_GM;
MethodInfo m15268_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2797_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15268_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15269_GM;
MethodInfo m15269_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2797_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15269_GM};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15270_GM;
MethodInfo m15270_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2797_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15270_GM};
static MethodInfo* t2797_MIs[] =
{
	&m15266_MI,
	&m15267_MI,
	&m15268_MI,
	&m15269_MI,
	&m15270_MI,
	NULL
};
extern MethodInfo m15269_MI;
extern MethodInfo m15268_MI;
static MethodInfo* t2797_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15267_MI,
	&m15269_MI,
	&m15268_MI,
	&m15270_MI,
};
static TypeInfo* t2797_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2795_TI,
};
static Il2CppInterfaceOffsetPair t2797_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2795_TI, 7},
};
extern TypeInfo t268_TI;
static Il2CppRGCTXData t2797_RGCTXData[3] = 
{
	&m15270_MI/* Method Usage */,
	&t268_TI/* Class Usage */,
	&m21872_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2797_0_0_0;
extern Il2CppType t2797_1_0_0;
extern Il2CppGenericClass t2797_GC;
TypeInfo t2797_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2797_MIs, t2797_PIs, t2797_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2797_TI, t2797_ITIs, t2797_VT, &EmptyCustomAttributesCache, &t2797_TI, &t2797_0_0_0, &t2797_1_0_0, t2797_IOs, &t2797_GC, NULL, NULL, NULL, t2797_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2797)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5514_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern MethodInfo m28870_MI;
static PropertyInfo t5514____Count_PropertyInfo = 
{
	&t5514_TI, "Count", &m28870_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28871_MI;
static PropertyInfo t5514____IsReadOnly_PropertyInfo = 
{
	&t5514_TI, "IsReadOnly", &m28871_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5514_PIs[] =
{
	&t5514____Count_PropertyInfo,
	&t5514____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28870_GM;
MethodInfo m28870_MI = 
{
	"get_Count", NULL, &t5514_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28870_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28871_GM;
MethodInfo m28871_MI = 
{
	"get_IsReadOnly", NULL, &t5514_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28871_GM};
extern Il2CppType t268_0_0_0;
static ParameterInfo t5514_m28872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28872_GM;
MethodInfo m28872_MI = 
{
	"Add", NULL, &t5514_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5514_m28872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28872_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28873_GM;
MethodInfo m28873_MI = 
{
	"Clear", NULL, &t5514_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28873_GM};
extern Il2CppType t268_0_0_0;
static ParameterInfo t5514_m28874_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28874_GM;
MethodInfo m28874_MI = 
{
	"Contains", NULL, &t5514_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5514_m28874_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28874_GM};
extern Il2CppType t2794_0_0_0;
extern Il2CppType t2794_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5514_m28875_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2794_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28875_GM;
MethodInfo m28875_MI = 
{
	"CopyTo", NULL, &t5514_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5514_m28875_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28875_GM};
extern Il2CppType t268_0_0_0;
static ParameterInfo t5514_m28876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28876_GM;
MethodInfo m28876_MI = 
{
	"Remove", NULL, &t5514_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5514_m28876_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28876_GM};
static MethodInfo* t5514_MIs[] =
{
	&m28870_MI,
	&m28871_MI,
	&m28872_MI,
	&m28873_MI,
	&m28874_MI,
	&m28875_MI,
	&m28876_MI,
	NULL
};
static TypeInfo* t5514_ITIs[] = 
{
	&t603_TI,
	&t5516_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5514_0_0_0;
extern Il2CppType t5514_1_0_0;
struct t5514;
extern Il2CppGenericClass t5514_GC;
TypeInfo t5514_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5514_MIs, t5514_PIs, NULL, NULL, NULL, NULL, NULL, &t5514_TI, t5514_ITIs, NULL, &EmptyCustomAttributesCache, &t5514_TI, &t5514_0_0_0, &t5514_1_0_0, NULL, &t5514_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5515_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern MethodInfo m28877_MI;
extern MethodInfo m28878_MI;
static PropertyInfo t5515____Item_PropertyInfo = 
{
	&t5515_TI, "Item", &m28877_MI, &m28878_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5515_PIs[] =
{
	&t5515____Item_PropertyInfo,
	NULL
};
extern Il2CppType t268_0_0_0;
static ParameterInfo t5515_m28879_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28879_GM;
MethodInfo m28879_MI = 
{
	"IndexOf", NULL, &t5515_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5515_m28879_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28879_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t268_0_0_0;
static ParameterInfo t5515_m28880_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28880_GM;
MethodInfo m28880_MI = 
{
	"Insert", NULL, &t5515_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5515_m28880_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28880_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5515_m28881_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28881_GM;
MethodInfo m28881_MI = 
{
	"RemoveAt", NULL, &t5515_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5515_m28881_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28881_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5515_m28877_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28877_GM;
MethodInfo m28877_MI = 
{
	"get_Item", NULL, &t5515_TI, &t268_0_0_0, RuntimeInvoker_t29_t44, t5515_m28877_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28877_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t268_0_0_0;
static ParameterInfo t5515_m28878_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28878_GM;
MethodInfo m28878_MI = 
{
	"set_Item", NULL, &t5515_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5515_m28878_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28878_GM};
static MethodInfo* t5515_MIs[] =
{
	&m28879_MI,
	&m28880_MI,
	&m28881_MI,
	&m28877_MI,
	&m28878_MI,
	NULL
};
static TypeInfo* t5515_ITIs[] = 
{
	&t603_TI,
	&t5514_TI,
	&t5516_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5515_0_0_0;
extern Il2CppType t5515_1_0_0;
struct t5515;
extern Il2CppGenericClass t5515_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5515_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5515_MIs, t5515_PIs, NULL, NULL, NULL, NULL, NULL, &t5515_TI, t5515_ITIs, NULL, &t1908__CustomAttributeCache, &t5515_TI, &t5515_0_0_0, &t5515_1_0_0, NULL, &t5515_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15275_MI;


// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType t2793_0_0_1;
FieldInfo t2796_f0_FieldInfo = 
{
	"parent", &t2793_0_0_1, &t2796_TI, offsetof(t2796, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2796_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2796_TI, offsetof(t2796, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2796_f2_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2796_TI, offsetof(t2796, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2796_FIs[] =
{
	&t2796_f0_FieldInfo,
	&t2796_f1_FieldInfo,
	&t2796_f2_FieldInfo,
	NULL
};
extern MethodInfo m15272_MI;
static PropertyInfo t2796____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2796_TI, "System.Collections.IEnumerator.Current", &m15272_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2796____Current_PropertyInfo = 
{
	&t2796_TI, "Current", &m15275_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2796_PIs[] =
{
	&t2796____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2796____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2793_0_0_0;
static ParameterInfo t2796_m15271_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t2793_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15271_GM;
MethodInfo m15271_MI = 
{
	".ctor", (methodPointerType)&m11075_gshared, &t2796_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2796_m15271_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15271_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15272_GM;
MethodInfo m15272_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11076_gshared, &t2796_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15272_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15273_GM;
MethodInfo m15273_MI = 
{
	"Dispose", (methodPointerType)&m11077_gshared, &t2796_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15273_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15274_GM;
MethodInfo m15274_MI = 
{
	"MoveNext", (methodPointerType)&m11078_gshared, &t2796_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15274_GM};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15275_GM;
MethodInfo m15275_MI = 
{
	"get_Current", (methodPointerType)&m11079_gshared, &t2796_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15275_GM};
static MethodInfo* t2796_MIs[] =
{
	&m15271_MI,
	&m15272_MI,
	&m15273_MI,
	&m15274_MI,
	&m15275_MI,
	NULL
};
extern MethodInfo m15274_MI;
extern MethodInfo m15273_MI;
static MethodInfo* t2796_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15272_MI,
	&m15274_MI,
	&m15273_MI,
	&m15275_MI,
};
static TypeInfo* t2796_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2795_TI,
};
static Il2CppInterfaceOffsetPair t2796_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2795_TI, 7},
};
extern TypeInfo t268_TI;
static Il2CppRGCTXData t2796_RGCTXData[2] = 
{
	&m15275_MI/* Method Usage */,
	&t268_TI/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2796_0_0_0;
extern Il2CppType t2796_1_0_0;
extern Il2CppGenericClass t2796_GC;
TypeInfo t2796_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t2796_MIs, t2796_PIs, t2796_FIs, NULL, &t110_TI, NULL, &t717_TI, &t2796_TI, t2796_ITIs, t2796_VT, &EmptyCustomAttributesCache, &t2796_TI, &t2796_0_0_0, &t2796_1_0_0, t2796_IOs, &t2796_GC, NULL, NULL, NULL, t2796_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2796)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t281_m2031_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2031_GM;
MethodInfo m2031_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t281_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t281_m2031_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2031_GM};
extern Il2CppType t268_0_0_0;
static ParameterInfo t281_m15276_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15276_GM;
MethodInfo m15276_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t281_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t281_m15276_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15276_GM};
extern Il2CppType t268_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t281_m15277_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15277_GM;
MethodInfo m15277_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t281_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t281_m15277_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15277_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t281_m15278_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15278_GM;
MethodInfo m15278_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t281_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t281_m15278_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15278_GM};
static MethodInfo* t281_MIs[] =
{
	&m2031_MI,
	&m15276_MI,
	&m15277_MI,
	&m15278_MI,
	NULL
};
extern MethodInfo m15277_MI;
extern MethodInfo m15278_MI;
static MethodInfo* t281_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15276_MI,
	&m15277_MI,
	&m15278_MI,
};
static Il2CppInterfaceOffsetPair t281_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t281_1_0_0;
struct t281;
extern Il2CppGenericClass t281_GC;
TypeInfo t281_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t281_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t281_TI, NULL, t281_VT, &EmptyCustomAttributesCache, &t281_TI, &t281_0_0_0, &t281_1_0_0, t281_IOs, &t281_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t281), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4317_TI;

#include "t283.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.BaseVertexEffect>
extern MethodInfo m28882_MI;
static PropertyInfo t4317____Current_PropertyInfo = 
{
	&t4317_TI, "Current", &m28882_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4317_PIs[] =
{
	&t4317____Current_PropertyInfo,
	NULL
};
extern Il2CppType t283_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28882_GM;
MethodInfo m28882_MI = 
{
	"get_Current", NULL, &t4317_TI, &t283_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28882_GM};
static MethodInfo* t4317_MIs[] =
{
	&m28882_MI,
	NULL
};
static TypeInfo* t4317_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4317_0_0_0;
extern Il2CppType t4317_1_0_0;
struct t4317;
extern Il2CppGenericClass t4317_GC;
TypeInfo t4317_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4317_MIs, t4317_PIs, NULL, NULL, NULL, NULL, NULL, &t4317_TI, t4317_ITIs, NULL, &EmptyCustomAttributesCache, &t4317_TI, &t4317_0_0_0, &t4317_1_0_0, NULL, &t4317_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2798.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2798_TI;
#include "t2798MD.h"

extern TypeInfo t283_TI;
extern MethodInfo m15283_MI;
extern MethodInfo m21886_MI;
struct t20;
#define m21886(__this, p0, method) (t283 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType t20_0_0_1;
FieldInfo t2798_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2798_TI, offsetof(t2798, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2798_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2798_TI, offsetof(t2798, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2798_FIs[] =
{
	&t2798_f0_FieldInfo,
	&t2798_f1_FieldInfo,
	NULL
};
extern MethodInfo m15280_MI;
static PropertyInfo t2798____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2798_TI, "System.Collections.IEnumerator.Current", &m15280_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2798____Current_PropertyInfo = 
{
	&t2798_TI, "Current", &m15283_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2798_PIs[] =
{
	&t2798____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2798____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2798_m15279_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15279_GM;
MethodInfo m15279_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2798_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2798_m15279_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15279_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15280_GM;
MethodInfo m15280_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2798_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15280_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15281_GM;
MethodInfo m15281_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2798_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15281_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15282_GM;
MethodInfo m15282_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2798_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15282_GM};
extern Il2CppType t283_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15283_GM;
MethodInfo m15283_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2798_TI, &t283_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15283_GM};
static MethodInfo* t2798_MIs[] =
{
	&m15279_MI,
	&m15280_MI,
	&m15281_MI,
	&m15282_MI,
	&m15283_MI,
	NULL
};
extern MethodInfo m15282_MI;
extern MethodInfo m15281_MI;
static MethodInfo* t2798_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15280_MI,
	&m15282_MI,
	&m15281_MI,
	&m15283_MI,
};
static TypeInfo* t2798_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4317_TI,
};
static Il2CppInterfaceOffsetPair t2798_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4317_TI, 7},
};
extern TypeInfo t283_TI;
static Il2CppRGCTXData t2798_RGCTXData[3] = 
{
	&m15283_MI/* Method Usage */,
	&t283_TI/* Class Usage */,
	&m21886_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2798_0_0_0;
extern Il2CppType t2798_1_0_0;
extern Il2CppGenericClass t2798_GC;
TypeInfo t2798_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2798_MIs, t2798_PIs, t2798_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2798_TI, t2798_ITIs, t2798_VT, &EmptyCustomAttributesCache, &t2798_TI, &t2798_0_0_0, &t2798_1_0_0, t2798_IOs, &t2798_GC, NULL, NULL, NULL, t2798_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2798)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5517_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>
extern MethodInfo m28883_MI;
static PropertyInfo t5517____Count_PropertyInfo = 
{
	&t5517_TI, "Count", &m28883_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28884_MI;
static PropertyInfo t5517____IsReadOnly_PropertyInfo = 
{
	&t5517_TI, "IsReadOnly", &m28884_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5517_PIs[] =
{
	&t5517____Count_PropertyInfo,
	&t5517____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28883_GM;
MethodInfo m28883_MI = 
{
	"get_Count", NULL, &t5517_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28883_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28884_GM;
MethodInfo m28884_MI = 
{
	"get_IsReadOnly", NULL, &t5517_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28884_GM};
extern Il2CppType t283_0_0_0;
extern Il2CppType t283_0_0_0;
static ParameterInfo t5517_m28885_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28885_GM;
MethodInfo m28885_MI = 
{
	"Add", NULL, &t5517_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5517_m28885_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28885_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28886_GM;
MethodInfo m28886_MI = 
{
	"Clear", NULL, &t5517_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28886_GM};
extern Il2CppType t283_0_0_0;
static ParameterInfo t5517_m28887_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28887_GM;
MethodInfo m28887_MI = 
{
	"Contains", NULL, &t5517_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5517_m28887_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28887_GM};
extern Il2CppType t3890_0_0_0;
extern Il2CppType t3890_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5517_m28888_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3890_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28888_GM;
MethodInfo m28888_MI = 
{
	"CopyTo", NULL, &t5517_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5517_m28888_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28888_GM};
extern Il2CppType t283_0_0_0;
static ParameterInfo t5517_m28889_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28889_GM;
MethodInfo m28889_MI = 
{
	"Remove", NULL, &t5517_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5517_m28889_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28889_GM};
static MethodInfo* t5517_MIs[] =
{
	&m28883_MI,
	&m28884_MI,
	&m28885_MI,
	&m28886_MI,
	&m28887_MI,
	&m28888_MI,
	&m28889_MI,
	NULL
};
extern TypeInfo t5519_TI;
static TypeInfo* t5517_ITIs[] = 
{
	&t603_TI,
	&t5519_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5517_0_0_0;
extern Il2CppType t5517_1_0_0;
struct t5517;
extern Il2CppGenericClass t5517_GC;
TypeInfo t5517_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5517_MIs, t5517_PIs, NULL, NULL, NULL, NULL, NULL, &t5517_TI, t5517_ITIs, NULL, &EmptyCustomAttributesCache, &t5517_TI, &t5517_0_0_0, &t5517_1_0_0, NULL, &t5517_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType t4317_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28890_GM;
MethodInfo m28890_MI = 
{
	"GetEnumerator", NULL, &t5519_TI, &t4317_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28890_GM};
static MethodInfo* t5519_MIs[] =
{
	&m28890_MI,
	NULL
};
static TypeInfo* t5519_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5519_0_0_0;
extern Il2CppType t5519_1_0_0;
struct t5519;
extern Il2CppGenericClass t5519_GC;
TypeInfo t5519_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5519_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5519_TI, t5519_ITIs, NULL, &EmptyCustomAttributesCache, &t5519_TI, &t5519_0_0_0, &t5519_1_0_0, NULL, &t5519_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5518_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>
extern MethodInfo m28891_MI;
extern MethodInfo m28892_MI;
static PropertyInfo t5518____Item_PropertyInfo = 
{
	&t5518_TI, "Item", &m28891_MI, &m28892_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5518_PIs[] =
{
	&t5518____Item_PropertyInfo,
	NULL
};
extern Il2CppType t283_0_0_0;
static ParameterInfo t5518_m28893_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28893_GM;
MethodInfo m28893_MI = 
{
	"IndexOf", NULL, &t5518_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5518_m28893_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28893_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t283_0_0_0;
static ParameterInfo t5518_m28894_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28894_GM;
MethodInfo m28894_MI = 
{
	"Insert", NULL, &t5518_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5518_m28894_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28894_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5518_m28895_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28895_GM;
MethodInfo m28895_MI = 
{
	"RemoveAt", NULL, &t5518_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5518_m28895_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28895_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5518_m28891_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t283_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28891_GM;
MethodInfo m28891_MI = 
{
	"get_Item", NULL, &t5518_TI, &t283_0_0_0, RuntimeInvoker_t29_t44, t5518_m28891_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28891_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t283_0_0_0;
static ParameterInfo t5518_m28892_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28892_GM;
MethodInfo m28892_MI = 
{
	"set_Item", NULL, &t5518_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5518_m28892_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28892_GM};
static MethodInfo* t5518_MIs[] =
{
	&m28893_MI,
	&m28894_MI,
	&m28895_MI,
	&m28891_MI,
	&m28892_MI,
	NULL
};
static TypeInfo* t5518_ITIs[] = 
{
	&t603_TI,
	&t5517_TI,
	&t5519_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5518_0_0_0;
extern Il2CppType t5518_1_0_0;
struct t5518;
extern Il2CppGenericClass t5518_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5518_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5518_MIs, t5518_PIs, NULL, NULL, NULL, NULL, NULL, &t5518_TI, t5518_ITIs, NULL, &t1908__CustomAttributeCache, &t5518_TI, &t5518_0_0_0, &t5518_1_0_0, NULL, &t5518_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5520_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>
extern MethodInfo m28896_MI;
static PropertyInfo t5520____Count_PropertyInfo = 
{
	&t5520_TI, "Count", &m28896_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28897_MI;
static PropertyInfo t5520____IsReadOnly_PropertyInfo = 
{
	&t5520_TI, "IsReadOnly", &m28897_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5520_PIs[] =
{
	&t5520____Count_PropertyInfo,
	&t5520____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28896_GM;
MethodInfo m28896_MI = 
{
	"get_Count", NULL, &t5520_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28896_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28897_GM;
MethodInfo m28897_MI = 
{
	"get_IsReadOnly", NULL, &t5520_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28897_GM};
extern Il2CppType t356_0_0_0;
extern Il2CppType t356_0_0_0;
static ParameterInfo t5520_m28898_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t356_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28898_GM;
MethodInfo m28898_MI = 
{
	"Add", NULL, &t5520_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5520_m28898_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28898_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28899_GM;
MethodInfo m28899_MI = 
{
	"Clear", NULL, &t5520_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28899_GM};
extern Il2CppType t356_0_0_0;
static ParameterInfo t5520_m28900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t356_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28900_GM;
MethodInfo m28900_MI = 
{
	"Contains", NULL, &t5520_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5520_m28900_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28900_GM};
extern Il2CppType t3891_0_0_0;
extern Il2CppType t3891_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5520_m28901_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3891_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28901_GM;
MethodInfo m28901_MI = 
{
	"CopyTo", NULL, &t5520_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5520_m28901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28901_GM};
extern Il2CppType t356_0_0_0;
static ParameterInfo t5520_m28902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t356_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28902_GM;
MethodInfo m28902_MI = 
{
	"Remove", NULL, &t5520_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5520_m28902_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28902_GM};
static MethodInfo* t5520_MIs[] =
{
	&m28896_MI,
	&m28897_MI,
	&m28898_MI,
	&m28899_MI,
	&m28900_MI,
	&m28901_MI,
	&m28902_MI,
	NULL
};
extern TypeInfo t5522_TI;
static TypeInfo* t5520_ITIs[] = 
{
	&t603_TI,
	&t5522_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5520_0_0_0;
extern Il2CppType t5520_1_0_0;
struct t5520;
extern Il2CppGenericClass t5520_GC;
TypeInfo t5520_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5520_MIs, t5520_PIs, NULL, NULL, NULL, NULL, NULL, &t5520_TI, t5520_ITIs, NULL, &EmptyCustomAttributesCache, &t5520_TI, &t5520_0_0_0, &t5520_1_0_0, NULL, &t5520_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IVertexModifier>
extern Il2CppType t4319_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28903_GM;
MethodInfo m28903_MI = 
{
	"GetEnumerator", NULL, &t5522_TI, &t4319_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28903_GM};
static MethodInfo* t5522_MIs[] =
{
	&m28903_MI,
	NULL
};
static TypeInfo* t5522_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5522_0_0_0;
extern Il2CppType t5522_1_0_0;
struct t5522;
extern Il2CppGenericClass t5522_GC;
TypeInfo t5522_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5522_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5522_TI, t5522_ITIs, NULL, &EmptyCustomAttributesCache, &t5522_TI, &t5522_0_0_0, &t5522_1_0_0, NULL, &t5522_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4319_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IVertexModifier>
extern MethodInfo m28904_MI;
static PropertyInfo t4319____Current_PropertyInfo = 
{
	&t4319_TI, "Current", &m28904_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4319_PIs[] =
{
	&t4319____Current_PropertyInfo,
	NULL
};
extern Il2CppType t356_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28904_GM;
MethodInfo m28904_MI = 
{
	"get_Current", NULL, &t4319_TI, &t356_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28904_GM};
static MethodInfo* t4319_MIs[] =
{
	&m28904_MI,
	NULL
};
static TypeInfo* t4319_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4319_0_0_0;
extern Il2CppType t4319_1_0_0;
struct t4319;
extern Il2CppGenericClass t4319_GC;
TypeInfo t4319_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4319_MIs, t4319_PIs, NULL, NULL, NULL, NULL, NULL, &t4319_TI, t4319_ITIs, NULL, &EmptyCustomAttributesCache, &t4319_TI, &t4319_0_0_0, &t4319_1_0_0, NULL, &t4319_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2799.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2799_TI;
#include "t2799MD.h"

extern TypeInfo t356_TI;
extern MethodInfo m15288_MI;
extern MethodInfo m21897_MI;
struct t20;
#define m21897(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>
extern Il2CppType t20_0_0_1;
FieldInfo t2799_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2799_TI, offsetof(t2799, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2799_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2799_TI, offsetof(t2799, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2799_FIs[] =
{
	&t2799_f0_FieldInfo,
	&t2799_f1_FieldInfo,
	NULL
};
extern MethodInfo m15285_MI;
static PropertyInfo t2799____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2799_TI, "System.Collections.IEnumerator.Current", &m15285_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2799____Current_PropertyInfo = 
{
	&t2799_TI, "Current", &m15288_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2799_PIs[] =
{
	&t2799____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2799____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2799_m15284_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15284_GM;
MethodInfo m15284_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2799_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2799_m15284_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15284_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15285_GM;
MethodInfo m15285_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2799_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15285_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15286_GM;
MethodInfo m15286_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2799_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15286_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15287_GM;
MethodInfo m15287_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2799_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15287_GM};
extern Il2CppType t356_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15288_GM;
MethodInfo m15288_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2799_TI, &t356_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15288_GM};
static MethodInfo* t2799_MIs[] =
{
	&m15284_MI,
	&m15285_MI,
	&m15286_MI,
	&m15287_MI,
	&m15288_MI,
	NULL
};
extern MethodInfo m15287_MI;
extern MethodInfo m15286_MI;
static MethodInfo* t2799_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15285_MI,
	&m15287_MI,
	&m15286_MI,
	&m15288_MI,
};
static TypeInfo* t2799_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4319_TI,
};
static Il2CppInterfaceOffsetPair t2799_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4319_TI, 7},
};
extern TypeInfo t356_TI;
static Il2CppRGCTXData t2799_RGCTXData[3] = 
{
	&m15288_MI/* Method Usage */,
	&t356_TI/* Class Usage */,
	&m21897_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2799_0_0_0;
extern Il2CppType t2799_1_0_0;
extern Il2CppGenericClass t2799_GC;
TypeInfo t2799_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2799_MIs, t2799_PIs, t2799_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2799_TI, t2799_ITIs, t2799_VT, &EmptyCustomAttributesCache, &t2799_TI, &t2799_0_0_0, &t2799_1_0_0, t2799_IOs, &t2799_GC, NULL, NULL, NULL, t2799_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2799)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5521_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>
extern MethodInfo m28905_MI;
extern MethodInfo m28906_MI;
static PropertyInfo t5521____Item_PropertyInfo = 
{
	&t5521_TI, "Item", &m28905_MI, &m28906_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5521_PIs[] =
{
	&t5521____Item_PropertyInfo,
	NULL
};
extern Il2CppType t356_0_0_0;
static ParameterInfo t5521_m28907_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t356_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28907_GM;
MethodInfo m28907_MI = 
{
	"IndexOf", NULL, &t5521_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5521_m28907_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28907_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t356_0_0_0;
static ParameterInfo t5521_m28908_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t356_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28908_GM;
MethodInfo m28908_MI = 
{
	"Insert", NULL, &t5521_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5521_m28908_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28908_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5521_m28909_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28909_GM;
MethodInfo m28909_MI = 
{
	"RemoveAt", NULL, &t5521_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5521_m28909_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28909_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5521_m28905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t356_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28905_GM;
MethodInfo m28905_MI = 
{
	"get_Item", NULL, &t5521_TI, &t356_0_0_0, RuntimeInvoker_t29_t44, t5521_m28905_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28905_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t356_0_0_0;
static ParameterInfo t5521_m28906_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t356_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28906_GM;
MethodInfo m28906_MI = 
{
	"set_Item", NULL, &t5521_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5521_m28906_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28906_GM};
static MethodInfo* t5521_MIs[] =
{
	&m28907_MI,
	&m28908_MI,
	&m28909_MI,
	&m28905_MI,
	&m28906_MI,
	NULL
};
static TypeInfo* t5521_ITIs[] = 
{
	&t603_TI,
	&t5520_TI,
	&t5522_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5521_0_0_0;
extern Il2CppType t5521_1_0_0;
struct t5521;
extern Il2CppGenericClass t5521_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5521_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5521_MIs, t5521_PIs, NULL, NULL, NULL, NULL, NULL, &t5521_TI, t5521_ITIs, NULL, &t1908__CustomAttributeCache, &t5521_TI, &t5521_0_0_0, &t5521_1_0_0, NULL, &t5521_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2800.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2800_TI;
#include "t2800MD.h"

#include "t2801.h"
extern TypeInfo t2801_TI;
#include "t2801MD.h"
extern MethodInfo m15291_MI;
extern MethodInfo m15293_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType t316_0_0_33;
FieldInfo t2800_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2800_TI, offsetof(t2800, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2800_FIs[] =
{
	&t2800_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t283_0_0_0;
static ParameterInfo t2800_m15289_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15289_GM;
MethodInfo m15289_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2800_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2800_m15289_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15289_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2800_m15290_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15290_GM;
MethodInfo m15290_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2800_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2800_m15290_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15290_GM};
static MethodInfo* t2800_MIs[] =
{
	&m15289_MI,
	&m15290_MI,
	NULL
};
extern MethodInfo m15290_MI;
extern MethodInfo m15294_MI;
static MethodInfo* t2800_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15290_MI,
	&m15294_MI,
};
extern Il2CppType t2802_0_0_0;
extern TypeInfo t2802_TI;
extern MethodInfo m21907_MI;
extern TypeInfo t283_TI;
extern MethodInfo m15296_MI;
extern TypeInfo t283_TI;
static Il2CppRGCTXData t2800_RGCTXData[8] = 
{
	&t2802_0_0_0/* Type Usage */,
	&t2802_TI/* Class Usage */,
	&m21907_MI/* Method Usage */,
	&t283_TI/* Class Usage */,
	&m15296_MI/* Method Usage */,
	&m15291_MI/* Method Usage */,
	&t283_TI/* Class Usage */,
	&m15293_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2800_0_0_0;
extern Il2CppType t2800_1_0_0;
struct t2800;
extern Il2CppGenericClass t2800_GC;
TypeInfo t2800_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2800_MIs, NULL, t2800_FIs, NULL, &t2801_TI, NULL, NULL, &t2800_TI, NULL, t2800_VT, &EmptyCustomAttributesCache, &t2800_TI, &t2800_0_0_0, &t2800_1_0_0, NULL, &t2800_GC, NULL, NULL, NULL, t2800_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2800), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2802.h"
extern TypeInfo t2802_TI;
#include "t2802MD.h"
struct t556;
#define m21907(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType t2802_0_0_1;
FieldInfo t2801_f0_FieldInfo = 
{
	"Delegate", &t2802_0_0_1, &t2801_TI, offsetof(t2801, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2801_FIs[] =
{
	&t2801_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2801_m15291_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15291_GM;
MethodInfo m15291_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2801_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2801_m15291_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15291_GM};
extern Il2CppType t2802_0_0_0;
static ParameterInfo t2801_m15292_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2802_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15292_GM;
MethodInfo m15292_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2801_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2801_m15292_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15292_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2801_m15293_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15293_GM;
MethodInfo m15293_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2801_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2801_m15293_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15293_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2801_m15294_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15294_GM;
MethodInfo m15294_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2801_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2801_m15294_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15294_GM};
static MethodInfo* t2801_MIs[] =
{
	&m15291_MI,
	&m15292_MI,
	&m15293_MI,
	&m15294_MI,
	NULL
};
static MethodInfo* t2801_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15293_MI,
	&m15294_MI,
};
extern TypeInfo t2802_TI;
extern TypeInfo t283_TI;
static Il2CppRGCTXData t2801_RGCTXData[5] = 
{
	&t2802_0_0_0/* Type Usage */,
	&t2802_TI/* Class Usage */,
	&m21907_MI/* Method Usage */,
	&t283_TI/* Class Usage */,
	&m15296_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2801_0_0_0;
extern Il2CppType t2801_1_0_0;
struct t2801;
extern Il2CppGenericClass t2801_GC;
TypeInfo t2801_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2801_MIs, NULL, t2801_FIs, NULL, &t556_TI, NULL, NULL, &t2801_TI, NULL, t2801_VT, &EmptyCustomAttributesCache, &t2801_TI, &t2801_0_0_0, &t2801_1_0_0, NULL, &t2801_GC, NULL, NULL, NULL, t2801_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2801), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2802_m15295_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15295_GM;
MethodInfo m15295_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2802_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2802_m15295_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15295_GM};
extern Il2CppType t283_0_0_0;
static ParameterInfo t2802_m15296_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15296_GM;
MethodInfo m15296_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2802_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2802_m15296_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15296_GM};
extern Il2CppType t283_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2802_m15297_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t283_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15297_GM;
MethodInfo m15297_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2802_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2802_m15297_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15297_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2802_m15298_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15298_GM;
MethodInfo m15298_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2802_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2802_m15298_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15298_GM};
static MethodInfo* t2802_MIs[] =
{
	&m15295_MI,
	&m15296_MI,
	&m15297_MI,
	&m15298_MI,
	NULL
};
extern MethodInfo m15297_MI;
extern MethodInfo m15298_MI;
static MethodInfo* t2802_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15296_MI,
	&m15297_MI,
	&m15298_MI,
};
static Il2CppInterfaceOffsetPair t2802_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2802_1_0_0;
struct t2802;
extern Il2CppGenericClass t2802_GC;
TypeInfo t2802_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2802_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2802_TI, NULL, t2802_VT, &EmptyCustomAttributesCache, &t2802_TI, &t2802_0_0_0, &t2802_1_0_0, t2802_IOs, &t2802_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2802), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4321_TI;

#include "t284.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Outline>
extern MethodInfo m28910_MI;
static PropertyInfo t4321____Current_PropertyInfo = 
{
	&t4321_TI, "Current", &m28910_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4321_PIs[] =
{
	&t4321____Current_PropertyInfo,
	NULL
};
extern Il2CppType t284_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28910_GM;
MethodInfo m28910_MI = 
{
	"get_Current", NULL, &t4321_TI, &t284_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28910_GM};
static MethodInfo* t4321_MIs[] =
{
	&m28910_MI,
	NULL
};
static TypeInfo* t4321_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4321_0_0_0;
extern Il2CppType t4321_1_0_0;
struct t4321;
extern Il2CppGenericClass t4321_GC;
TypeInfo t4321_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4321_MIs, t4321_PIs, NULL, NULL, NULL, NULL, NULL, &t4321_TI, t4321_ITIs, NULL, &EmptyCustomAttributesCache, &t4321_TI, &t4321_0_0_0, &t4321_1_0_0, NULL, &t4321_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2803.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2803_TI;
#include "t2803MD.h"

extern TypeInfo t284_TI;
extern MethodInfo m15303_MI;
extern MethodInfo m21909_MI;
struct t20;
#define m21909(__this, p0, method) (t284 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>
extern Il2CppType t20_0_0_1;
FieldInfo t2803_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2803_TI, offsetof(t2803, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2803_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2803_TI, offsetof(t2803, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2803_FIs[] =
{
	&t2803_f0_FieldInfo,
	&t2803_f1_FieldInfo,
	NULL
};
extern MethodInfo m15300_MI;
static PropertyInfo t2803____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2803_TI, "System.Collections.IEnumerator.Current", &m15300_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2803____Current_PropertyInfo = 
{
	&t2803_TI, "Current", &m15303_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2803_PIs[] =
{
	&t2803____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2803____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2803_m15299_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15299_GM;
MethodInfo m15299_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2803_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2803_m15299_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15299_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15300_GM;
MethodInfo m15300_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2803_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15300_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15301_GM;
MethodInfo m15301_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2803_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15301_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15302_GM;
MethodInfo m15302_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2803_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15302_GM};
extern Il2CppType t284_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15303_GM;
MethodInfo m15303_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2803_TI, &t284_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15303_GM};
static MethodInfo* t2803_MIs[] =
{
	&m15299_MI,
	&m15300_MI,
	&m15301_MI,
	&m15302_MI,
	&m15303_MI,
	NULL
};
extern MethodInfo m15302_MI;
extern MethodInfo m15301_MI;
static MethodInfo* t2803_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15300_MI,
	&m15302_MI,
	&m15301_MI,
	&m15303_MI,
};
static TypeInfo* t2803_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4321_TI,
};
static Il2CppInterfaceOffsetPair t2803_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4321_TI, 7},
};
extern TypeInfo t284_TI;
static Il2CppRGCTXData t2803_RGCTXData[3] = 
{
	&m15303_MI/* Method Usage */,
	&t284_TI/* Class Usage */,
	&m21909_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2803_0_0_0;
extern Il2CppType t2803_1_0_0;
extern Il2CppGenericClass t2803_GC;
TypeInfo t2803_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2803_MIs, t2803_PIs, t2803_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2803_TI, t2803_ITIs, t2803_VT, &EmptyCustomAttributesCache, &t2803_TI, &t2803_0_0_0, &t2803_1_0_0, t2803_IOs, &t2803_GC, NULL, NULL, NULL, t2803_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2803)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5523_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>
extern MethodInfo m28911_MI;
static PropertyInfo t5523____Count_PropertyInfo = 
{
	&t5523_TI, "Count", &m28911_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28912_MI;
static PropertyInfo t5523____IsReadOnly_PropertyInfo = 
{
	&t5523_TI, "IsReadOnly", &m28912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5523_PIs[] =
{
	&t5523____Count_PropertyInfo,
	&t5523____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28911_GM;
MethodInfo m28911_MI = 
{
	"get_Count", NULL, &t5523_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28911_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28912_GM;
MethodInfo m28912_MI = 
{
	"get_IsReadOnly", NULL, &t5523_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28912_GM};
extern Il2CppType t284_0_0_0;
extern Il2CppType t284_0_0_0;
static ParameterInfo t5523_m28913_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28913_GM;
MethodInfo m28913_MI = 
{
	"Add", NULL, &t5523_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5523_m28913_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28913_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28914_GM;
MethodInfo m28914_MI = 
{
	"Clear", NULL, &t5523_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28914_GM};
extern Il2CppType t284_0_0_0;
static ParameterInfo t5523_m28915_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28915_GM;
MethodInfo m28915_MI = 
{
	"Contains", NULL, &t5523_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5523_m28915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28915_GM};
extern Il2CppType t3892_0_0_0;
extern Il2CppType t3892_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5523_m28916_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3892_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28916_GM;
MethodInfo m28916_MI = 
{
	"CopyTo", NULL, &t5523_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5523_m28916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28916_GM};
extern Il2CppType t284_0_0_0;
static ParameterInfo t5523_m28917_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28917_GM;
MethodInfo m28917_MI = 
{
	"Remove", NULL, &t5523_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5523_m28917_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28917_GM};
static MethodInfo* t5523_MIs[] =
{
	&m28911_MI,
	&m28912_MI,
	&m28913_MI,
	&m28914_MI,
	&m28915_MI,
	&m28916_MI,
	&m28917_MI,
	NULL
};
extern TypeInfo t5525_TI;
static TypeInfo* t5523_ITIs[] = 
{
	&t603_TI,
	&t5525_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5523_0_0_0;
extern Il2CppType t5523_1_0_0;
struct t5523;
extern Il2CppGenericClass t5523_GC;
TypeInfo t5523_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5523_MIs, t5523_PIs, NULL, NULL, NULL, NULL, NULL, &t5523_TI, t5523_ITIs, NULL, &EmptyCustomAttributesCache, &t5523_TI, &t5523_0_0_0, &t5523_1_0_0, NULL, &t5523_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Outline>
extern Il2CppType t4321_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28918_GM;
MethodInfo m28918_MI = 
{
	"GetEnumerator", NULL, &t5525_TI, &t4321_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28918_GM};
static MethodInfo* t5525_MIs[] =
{
	&m28918_MI,
	NULL
};
static TypeInfo* t5525_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5525_0_0_0;
extern Il2CppType t5525_1_0_0;
struct t5525;
extern Il2CppGenericClass t5525_GC;
TypeInfo t5525_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5525_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5525_TI, t5525_ITIs, NULL, &EmptyCustomAttributesCache, &t5525_TI, &t5525_0_0_0, &t5525_1_0_0, NULL, &t5525_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5524_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Outline>
extern MethodInfo m28919_MI;
extern MethodInfo m28920_MI;
static PropertyInfo t5524____Item_PropertyInfo = 
{
	&t5524_TI, "Item", &m28919_MI, &m28920_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5524_PIs[] =
{
	&t5524____Item_PropertyInfo,
	NULL
};
extern Il2CppType t284_0_0_0;
static ParameterInfo t5524_m28921_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28921_GM;
MethodInfo m28921_MI = 
{
	"IndexOf", NULL, &t5524_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5524_m28921_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28921_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t284_0_0_0;
static ParameterInfo t5524_m28922_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28922_GM;
MethodInfo m28922_MI = 
{
	"Insert", NULL, &t5524_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5524_m28922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28922_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5524_m28923_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28923_GM;
MethodInfo m28923_MI = 
{
	"RemoveAt", NULL, &t5524_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5524_m28923_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28923_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5524_m28919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t284_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28919_GM;
MethodInfo m28919_MI = 
{
	"get_Item", NULL, &t5524_TI, &t284_0_0_0, RuntimeInvoker_t29_t44, t5524_m28919_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28919_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t284_0_0_0;
static ParameterInfo t5524_m28920_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28920_GM;
MethodInfo m28920_MI = 
{
	"set_Item", NULL, &t5524_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5524_m28920_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28920_GM};
static MethodInfo* t5524_MIs[] =
{
	&m28921_MI,
	&m28922_MI,
	&m28923_MI,
	&m28919_MI,
	&m28920_MI,
	NULL
};
static TypeInfo* t5524_ITIs[] = 
{
	&t603_TI,
	&t5523_TI,
	&t5525_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5524_0_0_0;
extern Il2CppType t5524_1_0_0;
struct t5524;
extern Il2CppGenericClass t5524_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5524_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5524_MIs, t5524_PIs, NULL, NULL, NULL, NULL, NULL, &t5524_TI, t5524_ITIs, NULL, &t1908__CustomAttributeCache, &t5524_TI, &t5524_0_0_0, &t5524_1_0_0, NULL, &t5524_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5526_TI;

#include "t285.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>
extern MethodInfo m28924_MI;
static PropertyInfo t5526____Count_PropertyInfo = 
{
	&t5526_TI, "Count", &m28924_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28925_MI;
static PropertyInfo t5526____IsReadOnly_PropertyInfo = 
{
	&t5526_TI, "IsReadOnly", &m28925_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5526_PIs[] =
{
	&t5526____Count_PropertyInfo,
	&t5526____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28924_GM;
MethodInfo m28924_MI = 
{
	"get_Count", NULL, &t5526_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28924_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28925_GM;
MethodInfo m28925_MI = 
{
	"get_IsReadOnly", NULL, &t5526_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28925_GM};
extern Il2CppType t285_0_0_0;
extern Il2CppType t285_0_0_0;
static ParameterInfo t5526_m28926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28926_GM;
MethodInfo m28926_MI = 
{
	"Add", NULL, &t5526_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5526_m28926_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28926_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28927_GM;
MethodInfo m28927_MI = 
{
	"Clear", NULL, &t5526_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28927_GM};
extern Il2CppType t285_0_0_0;
static ParameterInfo t5526_m28928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28928_GM;
MethodInfo m28928_MI = 
{
	"Contains", NULL, &t5526_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5526_m28928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28928_GM};
extern Il2CppType t3893_0_0_0;
extern Il2CppType t3893_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5526_m28929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3893_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28929_GM;
MethodInfo m28929_MI = 
{
	"CopyTo", NULL, &t5526_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5526_m28929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28929_GM};
extern Il2CppType t285_0_0_0;
static ParameterInfo t5526_m28930_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28930_GM;
MethodInfo m28930_MI = 
{
	"Remove", NULL, &t5526_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5526_m28930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28930_GM};
static MethodInfo* t5526_MIs[] =
{
	&m28924_MI,
	&m28925_MI,
	&m28926_MI,
	&m28927_MI,
	&m28928_MI,
	&m28929_MI,
	&m28930_MI,
	NULL
};
extern TypeInfo t5528_TI;
static TypeInfo* t5526_ITIs[] = 
{
	&t603_TI,
	&t5528_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5526_0_0_0;
extern Il2CppType t5526_1_0_0;
struct t5526;
extern Il2CppGenericClass t5526_GC;
TypeInfo t5526_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5526_MIs, t5526_PIs, NULL, NULL, NULL, NULL, NULL, &t5526_TI, t5526_ITIs, NULL, &EmptyCustomAttributesCache, &t5526_TI, &t5526_0_0_0, &t5526_1_0_0, NULL, &t5526_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Shadow>
extern Il2CppType t4323_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28931_GM;
MethodInfo m28931_MI = 
{
	"GetEnumerator", NULL, &t5528_TI, &t4323_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28931_GM};
static MethodInfo* t5528_MIs[] =
{
	&m28931_MI,
	NULL
};
static TypeInfo* t5528_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5528_0_0_0;
extern Il2CppType t5528_1_0_0;
struct t5528;
extern Il2CppGenericClass t5528_GC;
TypeInfo t5528_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5528_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5528_TI, t5528_ITIs, NULL, &EmptyCustomAttributesCache, &t5528_TI, &t5528_0_0_0, &t5528_1_0_0, NULL, &t5528_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4323_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Shadow>
extern MethodInfo m28932_MI;
static PropertyInfo t4323____Current_PropertyInfo = 
{
	&t4323_TI, "Current", &m28932_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4323_PIs[] =
{
	&t4323____Current_PropertyInfo,
	NULL
};
extern Il2CppType t285_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28932_GM;
MethodInfo m28932_MI = 
{
	"get_Current", NULL, &t4323_TI, &t285_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28932_GM};
static MethodInfo* t4323_MIs[] =
{
	&m28932_MI,
	NULL
};
static TypeInfo* t4323_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4323_0_0_0;
extern Il2CppType t4323_1_0_0;
struct t4323;
extern Il2CppGenericClass t4323_GC;
TypeInfo t4323_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4323_MIs, t4323_PIs, NULL, NULL, NULL, NULL, NULL, &t4323_TI, t4323_ITIs, NULL, &EmptyCustomAttributesCache, &t4323_TI, &t4323_0_0_0, &t4323_1_0_0, NULL, &t4323_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2804.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2804_TI;
#include "t2804MD.h"

extern TypeInfo t285_TI;
extern MethodInfo m15308_MI;
extern MethodInfo m21920_MI;
struct t20;
#define m21920(__this, p0, method) (t285 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>
extern Il2CppType t20_0_0_1;
FieldInfo t2804_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2804_TI, offsetof(t2804, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2804_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2804_TI, offsetof(t2804, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2804_FIs[] =
{
	&t2804_f0_FieldInfo,
	&t2804_f1_FieldInfo,
	NULL
};
extern MethodInfo m15305_MI;
static PropertyInfo t2804____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2804_TI, "System.Collections.IEnumerator.Current", &m15305_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2804____Current_PropertyInfo = 
{
	&t2804_TI, "Current", &m15308_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2804_PIs[] =
{
	&t2804____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2804____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2804_m15304_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15304_GM;
MethodInfo m15304_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2804_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2804_m15304_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15304_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15305_GM;
MethodInfo m15305_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2804_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15305_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15306_GM;
MethodInfo m15306_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2804_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15306_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15307_GM;
MethodInfo m15307_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2804_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15307_GM};
extern Il2CppType t285_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15308_GM;
MethodInfo m15308_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2804_TI, &t285_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15308_GM};
static MethodInfo* t2804_MIs[] =
{
	&m15304_MI,
	&m15305_MI,
	&m15306_MI,
	&m15307_MI,
	&m15308_MI,
	NULL
};
extern MethodInfo m15307_MI;
extern MethodInfo m15306_MI;
static MethodInfo* t2804_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15305_MI,
	&m15307_MI,
	&m15306_MI,
	&m15308_MI,
};
static TypeInfo* t2804_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4323_TI,
};
static Il2CppInterfaceOffsetPair t2804_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4323_TI, 7},
};
extern TypeInfo t285_TI;
static Il2CppRGCTXData t2804_RGCTXData[3] = 
{
	&m15308_MI/* Method Usage */,
	&t285_TI/* Class Usage */,
	&m21920_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2804_0_0_0;
extern Il2CppType t2804_1_0_0;
extern Il2CppGenericClass t2804_GC;
TypeInfo t2804_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2804_MIs, t2804_PIs, t2804_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2804_TI, t2804_ITIs, t2804_VT, &EmptyCustomAttributesCache, &t2804_TI, &t2804_0_0_0, &t2804_1_0_0, t2804_IOs, &t2804_GC, NULL, NULL, NULL, t2804_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2804)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5527_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>
extern MethodInfo m28933_MI;
extern MethodInfo m28934_MI;
static PropertyInfo t5527____Item_PropertyInfo = 
{
	&t5527_TI, "Item", &m28933_MI, &m28934_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5527_PIs[] =
{
	&t5527____Item_PropertyInfo,
	NULL
};
extern Il2CppType t285_0_0_0;
static ParameterInfo t5527_m28935_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28935_GM;
MethodInfo m28935_MI = 
{
	"IndexOf", NULL, &t5527_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5527_m28935_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28935_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t285_0_0_0;
static ParameterInfo t5527_m28936_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28936_GM;
MethodInfo m28936_MI = 
{
	"Insert", NULL, &t5527_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5527_m28936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28936_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5527_m28937_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28937_GM;
MethodInfo m28937_MI = 
{
	"RemoveAt", NULL, &t5527_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5527_m28937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28937_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5527_m28933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t285_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28933_GM;
MethodInfo m28933_MI = 
{
	"get_Item", NULL, &t5527_TI, &t285_0_0_0, RuntimeInvoker_t29_t44, t5527_m28933_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28933_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t285_0_0_0;
static ParameterInfo t5527_m28934_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28934_GM;
MethodInfo m28934_MI = 
{
	"set_Item", NULL, &t5527_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5527_m28934_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28934_GM};
static MethodInfo* t5527_MIs[] =
{
	&m28935_MI,
	&m28936_MI,
	&m28937_MI,
	&m28933_MI,
	&m28934_MI,
	NULL
};
static TypeInfo* t5527_ITIs[] = 
{
	&t603_TI,
	&t5526_TI,
	&t5528_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5527_0_0_0;
extern Il2CppType t5527_1_0_0;
struct t5527;
extern Il2CppGenericClass t5527_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5527_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5527_MIs, t5527_PIs, NULL, NULL, NULL, NULL, NULL, &t5527_TI, t5527_ITIs, NULL, &t1908__CustomAttributeCache, &t5527_TI, &t5527_0_0_0, &t5527_1_0_0, NULL, &t5527_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2805.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2805_TI;
#include "t2805MD.h"

#include "t2806.h"
extern TypeInfo t2806_TI;
#include "t2806MD.h"
extern MethodInfo m15311_MI;
extern MethodInfo m15313_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>
extern Il2CppType t316_0_0_33;
FieldInfo t2805_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2805_TI, offsetof(t2805, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2805_FIs[] =
{
	&t2805_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t284_0_0_0;
static ParameterInfo t2805_m15309_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15309_GM;
MethodInfo m15309_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2805_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2805_m15309_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15309_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2805_m15310_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15310_GM;
MethodInfo m15310_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2805_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2805_m15310_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15310_GM};
static MethodInfo* t2805_MIs[] =
{
	&m15309_MI,
	&m15310_MI,
	NULL
};
extern MethodInfo m15310_MI;
extern MethodInfo m15314_MI;
static MethodInfo* t2805_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15310_MI,
	&m15314_MI,
};
extern Il2CppType t2807_0_0_0;
extern TypeInfo t2807_TI;
extern MethodInfo m21930_MI;
extern TypeInfo t284_TI;
extern MethodInfo m15316_MI;
extern TypeInfo t284_TI;
static Il2CppRGCTXData t2805_RGCTXData[8] = 
{
	&t2807_0_0_0/* Type Usage */,
	&t2807_TI/* Class Usage */,
	&m21930_MI/* Method Usage */,
	&t284_TI/* Class Usage */,
	&m15316_MI/* Method Usage */,
	&m15311_MI/* Method Usage */,
	&t284_TI/* Class Usage */,
	&m15313_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2805_0_0_0;
extern Il2CppType t2805_1_0_0;
struct t2805;
extern Il2CppGenericClass t2805_GC;
TypeInfo t2805_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2805_MIs, NULL, t2805_FIs, NULL, &t2806_TI, NULL, NULL, &t2805_TI, NULL, t2805_VT, &EmptyCustomAttributesCache, &t2805_TI, &t2805_0_0_0, &t2805_1_0_0, NULL, &t2805_GC, NULL, NULL, NULL, t2805_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2805), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2807.h"
extern TypeInfo t2807_TI;
#include "t2807MD.h"
struct t556;
#define m21930(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>
extern Il2CppType t2807_0_0_1;
FieldInfo t2806_f0_FieldInfo = 
{
	"Delegate", &t2807_0_0_1, &t2806_TI, offsetof(t2806, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2806_FIs[] =
{
	&t2806_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2806_m15311_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15311_GM;
MethodInfo m15311_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2806_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2806_m15311_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15311_GM};
extern Il2CppType t2807_0_0_0;
static ParameterInfo t2806_m15312_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2807_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15312_GM;
MethodInfo m15312_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2806_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2806_m15312_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15312_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2806_m15313_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15313_GM;
MethodInfo m15313_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2806_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2806_m15313_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15313_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2806_m15314_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15314_GM;
MethodInfo m15314_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2806_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2806_m15314_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15314_GM};
static MethodInfo* t2806_MIs[] =
{
	&m15311_MI,
	&m15312_MI,
	&m15313_MI,
	&m15314_MI,
	NULL
};
static MethodInfo* t2806_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15313_MI,
	&m15314_MI,
};
extern TypeInfo t2807_TI;
extern TypeInfo t284_TI;
static Il2CppRGCTXData t2806_RGCTXData[5] = 
{
	&t2807_0_0_0/* Type Usage */,
	&t2807_TI/* Class Usage */,
	&m21930_MI/* Method Usage */,
	&t284_TI/* Class Usage */,
	&m15316_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2806_0_0_0;
extern Il2CppType t2806_1_0_0;
struct t2806;
extern Il2CppGenericClass t2806_GC;
TypeInfo t2806_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2806_MIs, NULL, t2806_FIs, NULL, &t556_TI, NULL, NULL, &t2806_TI, NULL, t2806_VT, &EmptyCustomAttributesCache, &t2806_TI, &t2806_0_0_0, &t2806_1_0_0, NULL, &t2806_GC, NULL, NULL, NULL, t2806_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2806), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2807_m15315_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15315_GM;
MethodInfo m15315_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2807_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2807_m15315_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15315_GM};
extern Il2CppType t284_0_0_0;
static ParameterInfo t2807_m15316_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15316_GM;
MethodInfo m15316_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2807_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2807_m15316_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15316_GM};
extern Il2CppType t284_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2807_m15317_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t284_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15317_GM;
MethodInfo m15317_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2807_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2807_m15317_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15317_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2807_m15318_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15318_GM;
MethodInfo m15318_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2807_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2807_m15318_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15318_GM};
static MethodInfo* t2807_MIs[] =
{
	&m15315_MI,
	&m15316_MI,
	&m15317_MI,
	&m15318_MI,
	NULL
};
extern MethodInfo m15317_MI;
extern MethodInfo m15318_MI;
static MethodInfo* t2807_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15316_MI,
	&m15317_MI,
	&m15318_MI,
};
static Il2CppInterfaceOffsetPair t2807_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2807_1_0_0;
struct t2807;
extern Il2CppGenericClass t2807_GC;
TypeInfo t2807_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2807_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2807_TI, NULL, t2807_VT, &EmptyCustomAttributesCache, &t2807_TI, &t2807_0_0_0, &t2807_1_0_0, t2807_IOs, &t2807_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2807), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4325_TI;

#include "t286.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.PositionAsUV1>
extern MethodInfo m28938_MI;
static PropertyInfo t4325____Current_PropertyInfo = 
{
	&t4325_TI, "Current", &m28938_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4325_PIs[] =
{
	&t4325____Current_PropertyInfo,
	NULL
};
extern Il2CppType t286_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28938_GM;
MethodInfo m28938_MI = 
{
	"get_Current", NULL, &t4325_TI, &t286_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28938_GM};
static MethodInfo* t4325_MIs[] =
{
	&m28938_MI,
	NULL
};
static TypeInfo* t4325_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4325_0_0_0;
extern Il2CppType t4325_1_0_0;
struct t4325;
extern Il2CppGenericClass t4325_GC;
TypeInfo t4325_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4325_MIs, t4325_PIs, NULL, NULL, NULL, NULL, NULL, &t4325_TI, t4325_ITIs, NULL, &EmptyCustomAttributesCache, &t4325_TI, &t4325_0_0_0, &t4325_1_0_0, NULL, &t4325_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2808.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2808_TI;
#include "t2808MD.h"

extern TypeInfo t286_TI;
extern MethodInfo m15323_MI;
extern MethodInfo m21932_MI;
struct t20;
#define m21932(__this, p0, method) (t286 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType t20_0_0_1;
FieldInfo t2808_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2808_TI, offsetof(t2808, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2808_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2808_TI, offsetof(t2808, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2808_FIs[] =
{
	&t2808_f0_FieldInfo,
	&t2808_f1_FieldInfo,
	NULL
};
extern MethodInfo m15320_MI;
static PropertyInfo t2808____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2808_TI, "System.Collections.IEnumerator.Current", &m15320_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2808____Current_PropertyInfo = 
{
	&t2808_TI, "Current", &m15323_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2808_PIs[] =
{
	&t2808____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2808____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2808_m15319_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15319_GM;
MethodInfo m15319_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2808_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2808_m15319_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15319_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15320_GM;
MethodInfo m15320_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2808_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15320_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15321_GM;
MethodInfo m15321_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2808_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15321_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15322_GM;
MethodInfo m15322_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2808_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15322_GM};
extern Il2CppType t286_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15323_GM;
MethodInfo m15323_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2808_TI, &t286_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15323_GM};
static MethodInfo* t2808_MIs[] =
{
	&m15319_MI,
	&m15320_MI,
	&m15321_MI,
	&m15322_MI,
	&m15323_MI,
	NULL
};
extern MethodInfo m15322_MI;
extern MethodInfo m15321_MI;
static MethodInfo* t2808_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15320_MI,
	&m15322_MI,
	&m15321_MI,
	&m15323_MI,
};
static TypeInfo* t2808_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4325_TI,
};
static Il2CppInterfaceOffsetPair t2808_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4325_TI, 7},
};
extern TypeInfo t286_TI;
static Il2CppRGCTXData t2808_RGCTXData[3] = 
{
	&m15323_MI/* Method Usage */,
	&t286_TI/* Class Usage */,
	&m21932_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2808_0_0_0;
extern Il2CppType t2808_1_0_0;
extern Il2CppGenericClass t2808_GC;
TypeInfo t2808_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2808_MIs, t2808_PIs, t2808_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2808_TI, t2808_ITIs, t2808_VT, &EmptyCustomAttributesCache, &t2808_TI, &t2808_0_0_0, &t2808_1_0_0, t2808_IOs, &t2808_GC, NULL, NULL, NULL, t2808_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2808)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5529_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>
extern MethodInfo m28939_MI;
static PropertyInfo t5529____Count_PropertyInfo = 
{
	&t5529_TI, "Count", &m28939_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28940_MI;
static PropertyInfo t5529____IsReadOnly_PropertyInfo = 
{
	&t5529_TI, "IsReadOnly", &m28940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5529_PIs[] =
{
	&t5529____Count_PropertyInfo,
	&t5529____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28939_GM;
MethodInfo m28939_MI = 
{
	"get_Count", NULL, &t5529_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28939_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28940_GM;
MethodInfo m28940_MI = 
{
	"get_IsReadOnly", NULL, &t5529_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28940_GM};
extern Il2CppType t286_0_0_0;
extern Il2CppType t286_0_0_0;
static ParameterInfo t5529_m28941_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28941_GM;
MethodInfo m28941_MI = 
{
	"Add", NULL, &t5529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5529_m28941_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28941_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28942_GM;
MethodInfo m28942_MI = 
{
	"Clear", NULL, &t5529_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28942_GM};
extern Il2CppType t286_0_0_0;
static ParameterInfo t5529_m28943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28943_GM;
MethodInfo m28943_MI = 
{
	"Contains", NULL, &t5529_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5529_m28943_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28943_GM};
extern Il2CppType t3894_0_0_0;
extern Il2CppType t3894_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5529_m28944_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3894_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28944_GM;
MethodInfo m28944_MI = 
{
	"CopyTo", NULL, &t5529_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5529_m28944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28944_GM};
extern Il2CppType t286_0_0_0;
static ParameterInfo t5529_m28945_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28945_GM;
MethodInfo m28945_MI = 
{
	"Remove", NULL, &t5529_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5529_m28945_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28945_GM};
static MethodInfo* t5529_MIs[] =
{
	&m28939_MI,
	&m28940_MI,
	&m28941_MI,
	&m28942_MI,
	&m28943_MI,
	&m28944_MI,
	&m28945_MI,
	NULL
};
extern TypeInfo t5531_TI;
static TypeInfo* t5529_ITIs[] = 
{
	&t603_TI,
	&t5531_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5529_0_0_0;
extern Il2CppType t5529_1_0_0;
struct t5529;
extern Il2CppGenericClass t5529_GC;
TypeInfo t5529_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5529_MIs, t5529_PIs, NULL, NULL, NULL, NULL, NULL, &t5529_TI, t5529_ITIs, NULL, &EmptyCustomAttributesCache, &t5529_TI, &t5529_0_0_0, &t5529_1_0_0, NULL, &t5529_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType t4325_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28946_GM;
MethodInfo m28946_MI = 
{
	"GetEnumerator", NULL, &t5531_TI, &t4325_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28946_GM};
static MethodInfo* t5531_MIs[] =
{
	&m28946_MI,
	NULL
};
static TypeInfo* t5531_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5531_0_0_0;
extern Il2CppType t5531_1_0_0;
struct t5531;
extern Il2CppGenericClass t5531_GC;
TypeInfo t5531_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5531_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5531_TI, t5531_ITIs, NULL, &EmptyCustomAttributesCache, &t5531_TI, &t5531_0_0_0, &t5531_1_0_0, NULL, &t5531_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5530_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>
extern MethodInfo m28947_MI;
extern MethodInfo m28948_MI;
static PropertyInfo t5530____Item_PropertyInfo = 
{
	&t5530_TI, "Item", &m28947_MI, &m28948_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5530_PIs[] =
{
	&t5530____Item_PropertyInfo,
	NULL
};
extern Il2CppType t286_0_0_0;
static ParameterInfo t5530_m28949_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28949_GM;
MethodInfo m28949_MI = 
{
	"IndexOf", NULL, &t5530_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5530_m28949_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28949_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t286_0_0_0;
static ParameterInfo t5530_m28950_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28950_GM;
MethodInfo m28950_MI = 
{
	"Insert", NULL, &t5530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5530_m28950_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28950_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5530_m28951_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28951_GM;
MethodInfo m28951_MI = 
{
	"RemoveAt", NULL, &t5530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5530_m28951_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28951_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5530_m28947_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t286_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28947_GM;
MethodInfo m28947_MI = 
{
	"get_Item", NULL, &t5530_TI, &t286_0_0_0, RuntimeInvoker_t29_t44, t5530_m28947_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28947_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t286_0_0_0;
static ParameterInfo t5530_m28948_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28948_GM;
MethodInfo m28948_MI = 
{
	"set_Item", NULL, &t5530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5530_m28948_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28948_GM};
static MethodInfo* t5530_MIs[] =
{
	&m28949_MI,
	&m28950_MI,
	&m28951_MI,
	&m28947_MI,
	&m28948_MI,
	NULL
};
static TypeInfo* t5530_ITIs[] = 
{
	&t603_TI,
	&t5529_TI,
	&t5531_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5530_0_0_0;
extern Il2CppType t5530_1_0_0;
struct t5530;
extern Il2CppGenericClass t5530_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5530_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5530_MIs, t5530_PIs, NULL, NULL, NULL, NULL, NULL, &t5530_TI, t5530_ITIs, NULL, &t1908__CustomAttributeCache, &t5530_TI, &t5530_0_0_0, &t5530_1_0_0, NULL, &t5530_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2809.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2809_TI;
#include "t2809MD.h"

#include "t2810.h"
extern TypeInfo t2810_TI;
#include "t2810MD.h"
extern MethodInfo m15326_MI;
extern MethodInfo m15328_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType t316_0_0_33;
FieldInfo t2809_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2809_TI, offsetof(t2809, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2809_FIs[] =
{
	&t2809_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t286_0_0_0;
static ParameterInfo t2809_m15324_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15324_GM;
MethodInfo m15324_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2809_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2809_m15324_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15324_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2809_m15325_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15325_GM;
MethodInfo m15325_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2809_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2809_m15325_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15325_GM};
static MethodInfo* t2809_MIs[] =
{
	&m15324_MI,
	&m15325_MI,
	NULL
};
extern MethodInfo m15325_MI;
extern MethodInfo m15329_MI;
static MethodInfo* t2809_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15325_MI,
	&m15329_MI,
};
extern Il2CppType t2811_0_0_0;
extern TypeInfo t2811_TI;
extern MethodInfo m21942_MI;
extern TypeInfo t286_TI;
extern MethodInfo m15331_MI;
extern TypeInfo t286_TI;
static Il2CppRGCTXData t2809_RGCTXData[8] = 
{
	&t2811_0_0_0/* Type Usage */,
	&t2811_TI/* Class Usage */,
	&m21942_MI/* Method Usage */,
	&t286_TI/* Class Usage */,
	&m15331_MI/* Method Usage */,
	&m15326_MI/* Method Usage */,
	&t286_TI/* Class Usage */,
	&m15328_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2809_0_0_0;
extern Il2CppType t2809_1_0_0;
struct t2809;
extern Il2CppGenericClass t2809_GC;
TypeInfo t2809_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2809_MIs, NULL, t2809_FIs, NULL, &t2810_TI, NULL, NULL, &t2809_TI, NULL, t2809_VT, &EmptyCustomAttributesCache, &t2809_TI, &t2809_0_0_0, &t2809_1_0_0, NULL, &t2809_GC, NULL, NULL, NULL, t2809_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2809), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2811.h"
extern TypeInfo t2811_TI;
#include "t2811MD.h"
struct t556;
#define m21942(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType t2811_0_0_1;
FieldInfo t2810_f0_FieldInfo = 
{
	"Delegate", &t2811_0_0_1, &t2810_TI, offsetof(t2810, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2810_FIs[] =
{
	&t2810_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2810_m15326_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15326_GM;
MethodInfo m15326_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2810_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2810_m15326_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15326_GM};
extern Il2CppType t2811_0_0_0;
static ParameterInfo t2810_m15327_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2811_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15327_GM;
MethodInfo m15327_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2810_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2810_m15327_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15327_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2810_m15328_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15328_GM;
MethodInfo m15328_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2810_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2810_m15328_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15328_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2810_m15329_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15329_GM;
MethodInfo m15329_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2810_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2810_m15329_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15329_GM};
static MethodInfo* t2810_MIs[] =
{
	&m15326_MI,
	&m15327_MI,
	&m15328_MI,
	&m15329_MI,
	NULL
};
static MethodInfo* t2810_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15328_MI,
	&m15329_MI,
};
extern TypeInfo t2811_TI;
extern TypeInfo t286_TI;
static Il2CppRGCTXData t2810_RGCTXData[5] = 
{
	&t2811_0_0_0/* Type Usage */,
	&t2811_TI/* Class Usage */,
	&m21942_MI/* Method Usage */,
	&t286_TI/* Class Usage */,
	&m15331_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2810_0_0_0;
extern Il2CppType t2810_1_0_0;
struct t2810;
extern Il2CppGenericClass t2810_GC;
TypeInfo t2810_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2810_MIs, NULL, t2810_FIs, NULL, &t556_TI, NULL, NULL, &t2810_TI, NULL, t2810_VT, &EmptyCustomAttributesCache, &t2810_TI, &t2810_0_0_0, &t2810_1_0_0, NULL, &t2810_GC, NULL, NULL, NULL, t2810_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2810), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2811_m15330_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15330_GM;
MethodInfo m15330_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2811_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2811_m15330_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15330_GM};
extern Il2CppType t286_0_0_0;
static ParameterInfo t2811_m15331_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15331_GM;
MethodInfo m15331_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2811_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2811_m15331_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15331_GM};
extern Il2CppType t286_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2811_m15332_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t286_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15332_GM;
MethodInfo m15332_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2811_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2811_m15332_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15332_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2811_m15333_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15333_GM;
MethodInfo m15333_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2811_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2811_m15333_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15333_GM};
static MethodInfo* t2811_MIs[] =
{
	&m15330_MI,
	&m15331_MI,
	&m15332_MI,
	&m15333_MI,
	NULL
};
extern MethodInfo m15332_MI;
extern MethodInfo m15333_MI;
static MethodInfo* t2811_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15331_MI,
	&m15332_MI,
	&m15333_MI,
};
static Il2CppInterfaceOffsetPair t2811_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2811_1_0_0;
struct t2811;
extern Il2CppGenericClass t2811_GC;
TypeInfo t2811_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2811_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2811_TI, NULL, t2811_VT, &EmptyCustomAttributesCache, &t2811_TI, &t2811_0_0_0, &t2811_1_0_0, t2811_IOs, &t2811_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2811), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2812.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2812_TI;
#include "t2812MD.h"

#include "t2813.h"
extern TypeInfo t2813_TI;
#include "t2813MD.h"
extern MethodInfo m15336_MI;
extern MethodInfo m15338_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>
extern Il2CppType t316_0_0_33;
FieldInfo t2812_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2812_TI, offsetof(t2812, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2812_FIs[] =
{
	&t2812_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t285_0_0_0;
static ParameterInfo t2812_m15334_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15334_GM;
MethodInfo m15334_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2812_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2812_m15334_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15334_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2812_m15335_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15335_GM;
MethodInfo m15335_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2812_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2812_m15335_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15335_GM};
static MethodInfo* t2812_MIs[] =
{
	&m15334_MI,
	&m15335_MI,
	NULL
};
extern MethodInfo m15335_MI;
extern MethodInfo m15339_MI;
static MethodInfo* t2812_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15335_MI,
	&m15339_MI,
};
extern Il2CppType t2814_0_0_0;
extern TypeInfo t2814_TI;
extern MethodInfo m21943_MI;
extern TypeInfo t285_TI;
extern MethodInfo m15341_MI;
extern TypeInfo t285_TI;
static Il2CppRGCTXData t2812_RGCTXData[8] = 
{
	&t2814_0_0_0/* Type Usage */,
	&t2814_TI/* Class Usage */,
	&m21943_MI/* Method Usage */,
	&t285_TI/* Class Usage */,
	&m15341_MI/* Method Usage */,
	&m15336_MI/* Method Usage */,
	&t285_TI/* Class Usage */,
	&m15338_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2812_0_0_0;
extern Il2CppType t2812_1_0_0;
struct t2812;
extern Il2CppGenericClass t2812_GC;
TypeInfo t2812_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2812_MIs, NULL, t2812_FIs, NULL, &t2813_TI, NULL, NULL, &t2812_TI, NULL, t2812_VT, &EmptyCustomAttributesCache, &t2812_TI, &t2812_0_0_0, &t2812_1_0_0, NULL, &t2812_GC, NULL, NULL, NULL, t2812_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2812), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2814.h"
extern TypeInfo t2814_TI;
#include "t2814MD.h"
struct t556;
#define m21943(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>
extern Il2CppType t2814_0_0_1;
FieldInfo t2813_f0_FieldInfo = 
{
	"Delegate", &t2814_0_0_1, &t2813_TI, offsetof(t2813, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2813_FIs[] =
{
	&t2813_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2813_m15336_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15336_GM;
MethodInfo m15336_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2813_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2813_m15336_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15336_GM};
extern Il2CppType t2814_0_0_0;
static ParameterInfo t2813_m15337_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2814_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15337_GM;
MethodInfo m15337_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2813_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2813_m15337_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15337_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2813_m15338_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15338_GM;
MethodInfo m15338_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2813_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2813_m15338_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15338_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2813_m15339_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15339_GM;
MethodInfo m15339_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2813_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2813_m15339_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15339_GM};
static MethodInfo* t2813_MIs[] =
{
	&m15336_MI,
	&m15337_MI,
	&m15338_MI,
	&m15339_MI,
	NULL
};
static MethodInfo* t2813_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15338_MI,
	&m15339_MI,
};
extern TypeInfo t2814_TI;
extern TypeInfo t285_TI;
static Il2CppRGCTXData t2813_RGCTXData[5] = 
{
	&t2814_0_0_0/* Type Usage */,
	&t2814_TI/* Class Usage */,
	&m21943_MI/* Method Usage */,
	&t285_TI/* Class Usage */,
	&m15341_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2813_0_0_0;
extern Il2CppType t2813_1_0_0;
struct t2813;
extern Il2CppGenericClass t2813_GC;
TypeInfo t2813_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2813_MIs, NULL, t2813_FIs, NULL, &t556_TI, NULL, NULL, &t2813_TI, NULL, t2813_VT, &EmptyCustomAttributesCache, &t2813_TI, &t2813_0_0_0, &t2813_1_0_0, NULL, &t2813_GC, NULL, NULL, NULL, t2813_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2813), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2814_m15340_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15340_GM;
MethodInfo m15340_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2814_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2814_m15340_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15340_GM};
extern Il2CppType t285_0_0_0;
static ParameterInfo t2814_m15341_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15341_GM;
MethodInfo m15341_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2814_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2814_m15341_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15341_GM};
extern Il2CppType t285_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2814_m15342_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t285_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15342_GM;
MethodInfo m15342_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2814_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2814_m15342_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15342_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2814_m15343_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15343_GM;
MethodInfo m15343_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2814_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2814_m15343_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15343_GM};
static MethodInfo* t2814_MIs[] =
{
	&m15340_MI,
	&m15341_MI,
	&m15342_MI,
	&m15343_MI,
	NULL
};
extern MethodInfo m15342_MI;
extern MethodInfo m15343_MI;
static MethodInfo* t2814_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15341_MI,
	&m15342_MI,
	&m15343_MI,
};
static Il2CppInterfaceOffsetPair t2814_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2814_1_0_0;
struct t2814;
extern Il2CppGenericClass t2814_GC;
TypeInfo t2814_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2814_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2814_TI, NULL, t2814_VT, &EmptyCustomAttributesCache, &t2814_TI, &t2814_0_0_0, &t2814_1_0_0, t2814_IOs, &t2814_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2814), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4327_TI;

#include "t442.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AssetBundle>
extern MethodInfo m28952_MI;
static PropertyInfo t4327____Current_PropertyInfo = 
{
	&t4327_TI, "Current", &m28952_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4327_PIs[] =
{
	&t4327____Current_PropertyInfo,
	NULL
};
extern Il2CppType t442_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28952_GM;
MethodInfo m28952_MI = 
{
	"get_Current", NULL, &t4327_TI, &t442_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28952_GM};
static MethodInfo* t4327_MIs[] =
{
	&m28952_MI,
	NULL
};
static TypeInfo* t4327_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4327_0_0_0;
extern Il2CppType t4327_1_0_0;
struct t4327;
extern Il2CppGenericClass t4327_GC;
TypeInfo t4327_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4327_MIs, t4327_PIs, NULL, NULL, NULL, NULL, NULL, &t4327_TI, t4327_ITIs, NULL, &EmptyCustomAttributesCache, &t4327_TI, &t4327_0_0_0, &t4327_1_0_0, NULL, &t4327_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2815.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2815_TI;
#include "t2815MD.h"

extern TypeInfo t442_TI;
extern MethodInfo m15348_MI;
extern MethodInfo m21945_MI;
struct t20;
#define m21945(__this, p0, method) (t442 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AssetBundle>
extern Il2CppType t20_0_0_1;
FieldInfo t2815_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2815_TI, offsetof(t2815, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2815_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2815_TI, offsetof(t2815, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2815_FIs[] =
{
	&t2815_f0_FieldInfo,
	&t2815_f1_FieldInfo,
	NULL
};
extern MethodInfo m15345_MI;
static PropertyInfo t2815____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2815_TI, "System.Collections.IEnumerator.Current", &m15345_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2815____Current_PropertyInfo = 
{
	&t2815_TI, "Current", &m15348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2815_PIs[] =
{
	&t2815____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2815____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2815_m15344_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15344_GM;
MethodInfo m15344_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2815_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2815_m15344_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15344_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15345_GM;
MethodInfo m15345_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2815_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15345_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15346_GM;
MethodInfo m15346_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2815_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15346_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15347_GM;
MethodInfo m15347_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2815_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15347_GM};
extern Il2CppType t442_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15348_GM;
MethodInfo m15348_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2815_TI, &t442_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15348_GM};
static MethodInfo* t2815_MIs[] =
{
	&m15344_MI,
	&m15345_MI,
	&m15346_MI,
	&m15347_MI,
	&m15348_MI,
	NULL
};
extern MethodInfo m15347_MI;
extern MethodInfo m15346_MI;
static MethodInfo* t2815_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15345_MI,
	&m15347_MI,
	&m15346_MI,
	&m15348_MI,
};
static TypeInfo* t2815_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4327_TI,
};
static Il2CppInterfaceOffsetPair t2815_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4327_TI, 7},
};
extern TypeInfo t442_TI;
static Il2CppRGCTXData t2815_RGCTXData[3] = 
{
	&m15348_MI/* Method Usage */,
	&t442_TI/* Class Usage */,
	&m21945_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2815_0_0_0;
extern Il2CppType t2815_1_0_0;
extern Il2CppGenericClass t2815_GC;
TypeInfo t2815_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2815_MIs, t2815_PIs, t2815_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2815_TI, t2815_ITIs, t2815_VT, &EmptyCustomAttributesCache, &t2815_TI, &t2815_0_0_0, &t2815_1_0_0, t2815_IOs, &t2815_GC, NULL, NULL, NULL, t2815_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2815)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5532_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AssetBundle>
extern MethodInfo m28953_MI;
static PropertyInfo t5532____Count_PropertyInfo = 
{
	&t5532_TI, "Count", &m28953_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28954_MI;
static PropertyInfo t5532____IsReadOnly_PropertyInfo = 
{
	&t5532_TI, "IsReadOnly", &m28954_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5532_PIs[] =
{
	&t5532____Count_PropertyInfo,
	&t5532____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28953_GM;
MethodInfo m28953_MI = 
{
	"get_Count", NULL, &t5532_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28953_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28954_GM;
MethodInfo m28954_MI = 
{
	"get_IsReadOnly", NULL, &t5532_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28954_GM};
extern Il2CppType t442_0_0_0;
extern Il2CppType t442_0_0_0;
static ParameterInfo t5532_m28955_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28955_GM;
MethodInfo m28955_MI = 
{
	"Add", NULL, &t5532_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5532_m28955_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28955_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28956_GM;
MethodInfo m28956_MI = 
{
	"Clear", NULL, &t5532_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28956_GM};
extern Il2CppType t442_0_0_0;
static ParameterInfo t5532_m28957_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28957_GM;
MethodInfo m28957_MI = 
{
	"Contains", NULL, &t5532_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5532_m28957_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28957_GM};
extern Il2CppType t3732_0_0_0;
extern Il2CppType t3732_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5532_m28958_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3732_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28958_GM;
MethodInfo m28958_MI = 
{
	"CopyTo", NULL, &t5532_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5532_m28958_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28958_GM};
extern Il2CppType t442_0_0_0;
static ParameterInfo t5532_m28959_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28959_GM;
MethodInfo m28959_MI = 
{
	"Remove", NULL, &t5532_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5532_m28959_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28959_GM};
static MethodInfo* t5532_MIs[] =
{
	&m28953_MI,
	&m28954_MI,
	&m28955_MI,
	&m28956_MI,
	&m28957_MI,
	&m28958_MI,
	&m28959_MI,
	NULL
};
extern TypeInfo t5534_TI;
static TypeInfo* t5532_ITIs[] = 
{
	&t603_TI,
	&t5534_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5532_0_0_0;
extern Il2CppType t5532_1_0_0;
struct t5532;
extern Il2CppGenericClass t5532_GC;
TypeInfo t5532_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5532_MIs, t5532_PIs, NULL, NULL, NULL, NULL, NULL, &t5532_TI, t5532_ITIs, NULL, &EmptyCustomAttributesCache, &t5532_TI, &t5532_0_0_0, &t5532_1_0_0, NULL, &t5532_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AssetBundle>
extern Il2CppType t4327_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28960_GM;
MethodInfo m28960_MI = 
{
	"GetEnumerator", NULL, &t5534_TI, &t4327_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28960_GM};
static MethodInfo* t5534_MIs[] =
{
	&m28960_MI,
	NULL
};
static TypeInfo* t5534_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5534_0_0_0;
extern Il2CppType t5534_1_0_0;
struct t5534;
extern Il2CppGenericClass t5534_GC;
TypeInfo t5534_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5534_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5534_TI, t5534_ITIs, NULL, &EmptyCustomAttributesCache, &t5534_TI, &t5534_0_0_0, &t5534_1_0_0, NULL, &t5534_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5533_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AssetBundle>
extern MethodInfo m28961_MI;
extern MethodInfo m28962_MI;
static PropertyInfo t5533____Item_PropertyInfo = 
{
	&t5533_TI, "Item", &m28961_MI, &m28962_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5533_PIs[] =
{
	&t5533____Item_PropertyInfo,
	NULL
};
extern Il2CppType t442_0_0_0;
static ParameterInfo t5533_m28963_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28963_GM;
MethodInfo m28963_MI = 
{
	"IndexOf", NULL, &t5533_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5533_m28963_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28963_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t442_0_0_0;
static ParameterInfo t5533_m28964_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28964_GM;
MethodInfo m28964_MI = 
{
	"Insert", NULL, &t5533_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5533_m28964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28964_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5533_m28965_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28965_GM;
MethodInfo m28965_MI = 
{
	"RemoveAt", NULL, &t5533_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5533_m28965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28965_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5533_m28961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t442_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28961_GM;
MethodInfo m28961_MI = 
{
	"get_Item", NULL, &t5533_TI, &t442_0_0_0, RuntimeInvoker_t29_t44, t5533_m28961_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28961_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t442_0_0_0;
static ParameterInfo t5533_m28962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28962_GM;
MethodInfo m28962_MI = 
{
	"set_Item", NULL, &t5533_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5533_m28962_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28962_GM};
static MethodInfo* t5533_MIs[] =
{
	&m28963_MI,
	&m28964_MI,
	&m28965_MI,
	&m28961_MI,
	&m28962_MI,
	NULL
};
static TypeInfo* t5533_ITIs[] = 
{
	&t603_TI,
	&t5532_TI,
	&t5534_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5533_0_0_0;
extern Il2CppType t5533_1_0_0;
struct t5533;
extern Il2CppGenericClass t5533_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5533_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5533_MIs, t5533_PIs, NULL, NULL, NULL, NULL, NULL, &t5533_TI, t5533_ITIs, NULL, &t1908__CustomAttributeCache, &t5533_TI, &t5533_0_0_0, &t5533_1_0_0, NULL, &t5533_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2816.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2816_TI;
#include "t2816MD.h"

#include "t2817.h"
extern TypeInfo t2817_TI;
#include "t2817MD.h"
extern MethodInfo m15351_MI;
extern MethodInfo m15353_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AssetBundle>
extern Il2CppType t316_0_0_33;
FieldInfo t2816_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2816_TI, offsetof(t2816, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2816_FIs[] =
{
	&t2816_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t442_0_0_0;
static ParameterInfo t2816_m15349_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15349_GM;
MethodInfo m15349_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2816_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2816_m15349_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15349_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2816_m15350_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15350_GM;
MethodInfo m15350_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2816_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2816_m15350_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15350_GM};
static MethodInfo* t2816_MIs[] =
{
	&m15349_MI,
	&m15350_MI,
	NULL
};
extern MethodInfo m15350_MI;
extern MethodInfo m15354_MI;
static MethodInfo* t2816_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15350_MI,
	&m15354_MI,
};
extern Il2CppType t2818_0_0_0;
extern TypeInfo t2818_TI;
extern MethodInfo m21955_MI;
extern TypeInfo t442_TI;
extern MethodInfo m15356_MI;
extern TypeInfo t442_TI;
static Il2CppRGCTXData t2816_RGCTXData[8] = 
{
	&t2818_0_0_0/* Type Usage */,
	&t2818_TI/* Class Usage */,
	&m21955_MI/* Method Usage */,
	&t442_TI/* Class Usage */,
	&m15356_MI/* Method Usage */,
	&m15351_MI/* Method Usage */,
	&t442_TI/* Class Usage */,
	&m15353_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2816_0_0_0;
extern Il2CppType t2816_1_0_0;
struct t2816;
extern Il2CppGenericClass t2816_GC;
TypeInfo t2816_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2816_MIs, NULL, t2816_FIs, NULL, &t2817_TI, NULL, NULL, &t2816_TI, NULL, t2816_VT, &EmptyCustomAttributesCache, &t2816_TI, &t2816_0_0_0, &t2816_1_0_0, NULL, &t2816_GC, NULL, NULL, NULL, t2816_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2816), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2818.h"
extern TypeInfo t2818_TI;
#include "t2818MD.h"
struct t556;
#define m21955(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.AssetBundle>
extern Il2CppType t2818_0_0_1;
FieldInfo t2817_f0_FieldInfo = 
{
	"Delegate", &t2818_0_0_1, &t2817_TI, offsetof(t2817, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2817_FIs[] =
{
	&t2817_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2817_m15351_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15351_GM;
MethodInfo m15351_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2817_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2817_m15351_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15351_GM};
extern Il2CppType t2818_0_0_0;
static ParameterInfo t2817_m15352_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2818_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15352_GM;
MethodInfo m15352_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2817_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2817_m15352_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15352_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2817_m15353_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15353_GM;
MethodInfo m15353_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2817_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2817_m15353_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15353_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2817_m15354_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15354_GM;
MethodInfo m15354_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2817_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2817_m15354_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15354_GM};
static MethodInfo* t2817_MIs[] =
{
	&m15351_MI,
	&m15352_MI,
	&m15353_MI,
	&m15354_MI,
	NULL
};
static MethodInfo* t2817_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15353_MI,
	&m15354_MI,
};
extern TypeInfo t2818_TI;
extern TypeInfo t442_TI;
static Il2CppRGCTXData t2817_RGCTXData[5] = 
{
	&t2818_0_0_0/* Type Usage */,
	&t2818_TI/* Class Usage */,
	&m21955_MI/* Method Usage */,
	&t442_TI/* Class Usage */,
	&m15356_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2817_0_0_0;
extern Il2CppType t2817_1_0_0;
struct t2817;
extern Il2CppGenericClass t2817_GC;
TypeInfo t2817_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2817_MIs, NULL, t2817_FIs, NULL, &t556_TI, NULL, NULL, &t2817_TI, NULL, t2817_VT, &EmptyCustomAttributesCache, &t2817_TI, &t2817_0_0_0, &t2817_1_0_0, NULL, &t2817_GC, NULL, NULL, NULL, t2817_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2817), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.AssetBundle>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2818_m15355_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15355_GM;
MethodInfo m15355_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2818_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2818_m15355_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15355_GM};
extern Il2CppType t442_0_0_0;
static ParameterInfo t2818_m15356_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15356_GM;
MethodInfo m15356_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2818_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2818_m15356_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15356_GM};
extern Il2CppType t442_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2818_m15357_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t442_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15357_GM;
MethodInfo m15357_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2818_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2818_m15357_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15357_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2818_m15358_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15358_GM;
MethodInfo m15358_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2818_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2818_m15358_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15358_GM};
static MethodInfo* t2818_MIs[] =
{
	&m15355_MI,
	&m15356_MI,
	&m15357_MI,
	&m15358_MI,
	NULL
};
extern MethodInfo m15357_MI;
extern MethodInfo m15358_MI;
static MethodInfo* t2818_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15356_MI,
	&m15357_MI,
	&m15358_MI,
};
static Il2CppInterfaceOffsetPair t2818_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2818_1_0_0;
struct t2818;
extern Il2CppGenericClass t2818_GC;
TypeInfo t2818_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2818_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2818_TI, NULL, t2818_VT, &EmptyCustomAttributesCache, &t2818_TI, &t2818_0_0_0, &t2818_1_0_0, t2818_IOs, &t2818_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2818), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
