﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t973;
struct t972;
struct t974;
struct t975;
#include "t970.h"

 t972 * m4300 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4301 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4302 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4303 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4304 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m4305 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m4306 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t974* m4307 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t974* m4308 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4309 (t29 * __this, t972 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4310 (t29 * __this, t972 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4311 (t29 * __this, t975* p0, uint32_t p1, uint32_t p2, t975* p3, uint32_t p4, uint32_t p5, t975* p6, uint32_t p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4312 (t29 * __this, t975* p0, int32_t p1, int32_t p2, t975* p3, int32_t p4, int32_t p5, t975* p6, int32_t p7, int32_t p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m4313 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4314 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
