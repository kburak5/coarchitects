﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t760;
struct t7;
struct t29;

 void m9590 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9591 (t760 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3999 (t760 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9592 (t760 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9593 (t760 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9594 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9595 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9596 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9597 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9598 (t760 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9599 (t760 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9600 (t760 * __this, t760 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9601 (t760 * __this, t760 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9602 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9603 (t760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t760 * m9604 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9605 (t29 * __this, t760 * p0, t760 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9606 (t29 * __this, t760 * p0, t760 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
