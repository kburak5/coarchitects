﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1275;
struct t29;
struct t719;
#include "t725.h"
#include "t1274.h"

 void m6688 (t1275 * __this, t719 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6689 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6690 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6691 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6692 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m6693 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6694 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6695 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6696 (t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
