﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t810;
struct t781;
struct t949;
struct t793;
#include "t465.h"

 void m4531 (t810 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4532 (t810 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4168 (t810 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t949 * m4174 (t810 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
