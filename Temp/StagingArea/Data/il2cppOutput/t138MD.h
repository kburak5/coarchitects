﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t138;
struct t29;

 void m367 (t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m368 (t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m369 (t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m370 (t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m371 (t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m372 (t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
