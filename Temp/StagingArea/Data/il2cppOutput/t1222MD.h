﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1222;
struct t1225;
struct t136;
struct t1227;

 void m6469 (t1222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6470 (t1222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1225 * m6471 (t1222 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6472 (t1222 * __this, t1225 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1227 * m6473 (t1222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6474 (t1222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
