﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo t685_TI;
#include "t685.h"
#include "t685MD.h"
extern MethodInfo m3041_MI;
extern TypeInfo t46_TI;
#include "t46.h"
#include "t46MD.h"
extern MethodInfo m71_MI;
extern MethodInfo m72_MI;
extern TypeInfo t686_TI;
#include "t686.h"
#include "t686MD.h"
extern MethodInfo m3042_MI;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), &m3041_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), &m3041_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), &m3041_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), &m3041_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), &m3041_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), &m3041_MI);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), &m3041_MI);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), &m3041_MI);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		t46 * tmp;
		tmp = (t46 *)il2cpp_codegen_object_new (&t46_TI);
		m71(tmp, &m71_MI);
		m72(tmp, true, &m72_MI);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		t686 * tmp;
		tmp = (t686 *)il2cpp_codegen_object_new (&t686_TI);
		m3042(tmp, &m3042_MI);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), &m3041_MI);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), &m3041_MI);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), &m3041_MI);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		t685 * tmp;
		tmp = (t685 *)il2cpp_codegen_object_new (&t685_TI);
		m3041(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), &m3041_MI);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_UnityEngine_Assembly__CustomAttributeCache = {
14,
NULL,
&g_UnityEngine_Assembly_CustomAttributesCacheGenerator
};
