﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3490;
struct t29;
#include "t1629.h"

 void m19393 (t3490 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19394 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19395 (t3490 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19396 (t3490 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3490 * m19397 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
