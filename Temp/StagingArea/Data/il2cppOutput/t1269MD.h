﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1269;
struct t29;
struct t858;

 void m6657 (t1269 * __this, t858 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6658 (t1269 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6659 (t1269 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6660 (t1269 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
