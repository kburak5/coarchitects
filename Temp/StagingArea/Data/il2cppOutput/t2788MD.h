﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2788;
struct t29;
struct t20;
struct t2790;
struct t136;
struct t278;
#include "t2791.h"

#include "t2229MD.h"
#define m15227(__this, method) (void)m11064_gshared((t2229 *)__this, method)
#define m15228(__this, method) (bool)m11065_gshared((t2229 *)__this, method)
#define m15229(__this, method) (t29 *)m11066_gshared((t2229 *)__this, method)
#define m15230(__this, p0, p1, method) (void)m11067_gshared((t2229 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m15231(__this, method) (t29*)m11068_gshared((t2229 *)__this, method)
#define m15232(__this, method) (t29 *)m11069_gshared((t2229 *)__this, method)
#define m15233(__this, method) (t278 *)m11070_gshared((t2229 *)__this, method)
#define m15234(__this, method) (t278 *)m11071_gshared((t2229 *)__this, method)
#define m15235(__this, p0, method) (void)m11072_gshared((t2229 *)__this, (t29 *)p0, method)
#define m15236(__this, method) (int32_t)m11073_gshared((t2229 *)__this, method)
 t2791  m15237 (t2788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
