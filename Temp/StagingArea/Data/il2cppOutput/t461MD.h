﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t461;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m2122 (t461 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2123 (t461 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2124 (t461 * __this, int32_t p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2125 (t461 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
