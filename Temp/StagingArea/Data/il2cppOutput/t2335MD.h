﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2335;
struct t29;
struct t20;
#include "t112.h"

 void m12008 (t2335 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12009 (t2335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12010 (t2335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12011 (t2335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12012 (t2335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
