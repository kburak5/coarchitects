﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t636;
struct t638;
struct t29;
struct t316;
struct t631;
struct t633;
struct t537;
#include "t1343.h"
#include "t1342.h"
#include "t1150.h"
#include "t35.h"
#include "t630.h"

 void m7541 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7542 (t29 * __this, t1343  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7543 (t29 * __this, t35 p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7544 (t29 * __this, t1343  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7545 (t29 * __this, t35 p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7546 (t636 * __this, t29 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7547 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7548 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2952 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7549 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7550 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7551 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7552 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7553 (t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
