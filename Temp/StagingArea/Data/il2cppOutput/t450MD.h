﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t450;
struct t450_marshaled;
struct t7;
struct t42;

 void m2075 (t450 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2076 (t29 * __this, t450 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t450 * m2077 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t450 * m2078 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t450 * m2079 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t450_marshal(const t450& unmarshaled, t450_marshaled& marshaled);
void t450_marshal_back(const t450_marshaled& marshaled, t450& unmarshaled);
void t450_marshal_cleanup(t450_marshaled& marshaled);
