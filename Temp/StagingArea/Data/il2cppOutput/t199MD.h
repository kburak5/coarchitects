﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t199;
struct t7;
#include "t205.h"

 t199 * m1813 (t29 * __this, t7* p0, int32_t p1, bool p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t199 * m1814 (t29 * __this, t7* p0, int32_t p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t199 * m2301 (t29 * __this, t7* p0, int32_t p1, bool p2, bool p3, bool p4, bool p5, t7* p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1712 (t199 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1714 (t199 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1812 (t29 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1711 (t199 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1811 (t199 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1743 (t199 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1739 (t199 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1738 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
