﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2759;
struct t29;
struct t20;
#include "t256.h"

 void m15104 (t2759 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15105 (t2759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15106 (t2759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15107 (t2759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15108 (t2759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
