﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t481;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m2187 (t481 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2188 (t481 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2189 (t481 * __this, t67 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2190 (t481 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
