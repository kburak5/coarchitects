﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t115;
struct t113;
struct t6;
#include "t111.h"
#include "t112.h"

 void m261 (t115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m262 (t115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m263 (t115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t113 * m264 (t115 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m265 (t115 * __this, int32_t p0, int32_t p1, t6 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
