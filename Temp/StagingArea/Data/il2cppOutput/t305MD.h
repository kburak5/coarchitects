﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t305;
struct t7;
struct t295;
struct t733;
#include "t735.h"

 void m8852 (t305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1935 (t305 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4156 (t305 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3973 (t305 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8853 (t305 * __this, t7* p0, t7* p1, t295 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8854 (t305 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8855 (t305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8856 (t305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8857 (t305 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
