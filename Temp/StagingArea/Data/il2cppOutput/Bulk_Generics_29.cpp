﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6166_TI;

#include "t1189.h"
#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<Mono.Globalization.Unicode.SimpleCollator/ExtenderType>
extern MethodInfo m32080_MI;
extern MethodInfo m32081_MI;
static PropertyInfo t6166____Item_PropertyInfo = 
{
	&t6166_TI, "Item", &m32080_MI, &m32081_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6166_PIs[] =
{
	&t6166____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1189_0_0_0;
extern Il2CppType t1189_0_0_0;
static ParameterInfo t6166_m32082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1189_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32082_GM;
MethodInfo m32082_MI = 
{
	"IndexOf", NULL, &t6166_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6166_m32082_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32082_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1189_0_0_0;
static ParameterInfo t6166_m32083_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1189_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32083_GM;
MethodInfo m32083_MI = 
{
	"Insert", NULL, &t6166_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6166_m32083_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32083_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6166_m32084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32084_GM;
MethodInfo m32084_MI = 
{
	"RemoveAt", NULL, &t6166_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6166_m32084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32084_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6166_m32080_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1189_0_0_0;
extern void* RuntimeInvoker_t1189_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32080_GM;
MethodInfo m32080_MI = 
{
	"get_Item", NULL, &t6166_TI, &t1189_0_0_0, RuntimeInvoker_t1189_t44, t6166_m32080_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32080_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1189_0_0_0;
static ParameterInfo t6166_m32081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1189_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32081_GM;
MethodInfo m32081_MI = 
{
	"set_Item", NULL, &t6166_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6166_m32081_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32081_GM};
static MethodInfo* t6166_MIs[] =
{
	&m32082_MI,
	&m32083_MI,
	&m32084_MI,
	&m32080_MI,
	&m32081_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6165_TI;
extern TypeInfo t6167_TI;
static TypeInfo* t6166_ITIs[] = 
{
	&t603_TI,
	&t6165_TI,
	&t6167_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6166_0_0_0;
extern Il2CppType t6166_1_0_0;
struct t6166;
extern Il2CppGenericClass t6166_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6166_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6166_MIs, t6166_PIs, NULL, NULL, NULL, NULL, NULL, &t6166_TI, t6166_ITIs, NULL, &t1908__CustomAttributeCache, &t6166_TI, &t6166_0_0_0, &t6166_1_0_0, NULL, &t6166_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4751_TI;

#include "t1197.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo m32085_MI;
static PropertyInfo t4751____Current_PropertyInfo = 
{
	&t4751_TI, "Current", &m32085_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4751_PIs[] =
{
	&t4751____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1197_0_0_0;
extern void* RuntimeInvoker_t1197 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32085_GM;
MethodInfo m32085_MI = 
{
	"get_Current", NULL, &t4751_TI, &t1197_0_0_0, RuntimeInvoker_t1197, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32085_GM};
static MethodInfo* t4751_MIs[] =
{
	&m32085_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4751_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4751_0_0_0;
extern Il2CppType t4751_1_0_0;
struct t4751;
extern Il2CppGenericClass t4751_GC;
TypeInfo t4751_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4751_MIs, t4751_PIs, NULL, NULL, NULL, NULL, NULL, &t4751_TI, t4751_ITIs, NULL, &EmptyCustomAttributesCache, &t4751_TI, &t4751_0_0_0, &t4751_1_0_0, NULL, &t4751_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3323.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3323_TI;
#include "t3323MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t1197_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18501_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m24542_MI;
struct t20;
#include "t915.h"
 int32_t m24542 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18497_MI;
 void m18497 (t3323 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18498_MI;
 t29 * m18498 (t3323 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18501(__this, &m18501_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1197_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18499_MI;
 void m18499 (t3323 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18500_MI;
 bool m18500 (t3323 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18501 (t3323 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24542(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24542_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType t20_0_0_1;
FieldInfo t3323_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3323_TI, offsetof(t3323, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3323_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3323_TI, offsetof(t3323, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3323_FIs[] =
{
	&t3323_f0_FieldInfo,
	&t3323_f1_FieldInfo,
	NULL
};
static PropertyInfo t3323____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3323_TI, "System.Collections.IEnumerator.Current", &m18498_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3323____Current_PropertyInfo = 
{
	&t3323_TI, "Current", &m18501_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3323_PIs[] =
{
	&t3323____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3323____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3323_m18497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18497_GM;
MethodInfo m18497_MI = 
{
	".ctor", (methodPointerType)&m18497, &t3323_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3323_m18497_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18497_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18498_GM;
MethodInfo m18498_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18498, &t3323_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18498_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18499_GM;
MethodInfo m18499_MI = 
{
	"Dispose", (methodPointerType)&m18499, &t3323_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18499_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18500_GM;
MethodInfo m18500_MI = 
{
	"MoveNext", (methodPointerType)&m18500, &t3323_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18500_GM};
extern Il2CppType t1197_0_0_0;
extern void* RuntimeInvoker_t1197 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18501_GM;
MethodInfo m18501_MI = 
{
	"get_Current", (methodPointerType)&m18501, &t3323_TI, &t1197_0_0_0, RuntimeInvoker_t1197, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18501_GM};
static MethodInfo* t3323_MIs[] =
{
	&m18497_MI,
	&m18498_MI,
	&m18499_MI,
	&m18500_MI,
	&m18501_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3323_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18498_MI,
	&m18500_MI,
	&m18499_MI,
	&m18501_MI,
};
static TypeInfo* t3323_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4751_TI,
};
static Il2CppInterfaceOffsetPair t3323_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4751_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3323_0_0_0;
extern Il2CppType t3323_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3323_GC;
extern TypeInfo t20_TI;
TypeInfo t3323_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3323_MIs, t3323_PIs, t3323_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3323_TI, t3323_ITIs, t3323_VT, &EmptyCustomAttributesCache, &t3323_TI, &t3323_0_0_0, &t3323_1_0_0, t3323_IOs, &t3323_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3323)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6168_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo m32086_MI;
static PropertyInfo t6168____Count_PropertyInfo = 
{
	&t6168_TI, "Count", &m32086_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32087_MI;
static PropertyInfo t6168____IsReadOnly_PropertyInfo = 
{
	&t6168_TI, "IsReadOnly", &m32087_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6168_PIs[] =
{
	&t6168____Count_PropertyInfo,
	&t6168____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32086_GM;
MethodInfo m32086_MI = 
{
	"get_Count", NULL, &t6168_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32086_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32087_GM;
MethodInfo m32087_MI = 
{
	"get_IsReadOnly", NULL, &t6168_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32087_GM};
extern Il2CppType t1197_0_0_0;
extern Il2CppType t1197_0_0_0;
static ParameterInfo t6168_m32088_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32088_GM;
MethodInfo m32088_MI = 
{
	"Add", NULL, &t6168_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6168_m32088_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32088_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32089_GM;
MethodInfo m32089_MI = 
{
	"Clear", NULL, &t6168_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32089_GM};
extern Il2CppType t1197_0_0_0;
static ParameterInfo t6168_m32090_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32090_GM;
MethodInfo m32090_MI = 
{
	"Contains", NULL, &t6168_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6168_m32090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32090_GM};
extern Il2CppType t3595_0_0_0;
extern Il2CppType t3595_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6168_m32091_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3595_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32091_GM;
MethodInfo m32091_MI = 
{
	"CopyTo", NULL, &t6168_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6168_m32091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32091_GM};
extern Il2CppType t1197_0_0_0;
static ParameterInfo t6168_m32092_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32092_GM;
MethodInfo m32092_MI = 
{
	"Remove", NULL, &t6168_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6168_m32092_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32092_GM};
static MethodInfo* t6168_MIs[] =
{
	&m32086_MI,
	&m32087_MI,
	&m32088_MI,
	&m32089_MI,
	&m32090_MI,
	&m32091_MI,
	&m32092_MI,
	NULL
};
extern TypeInfo t6170_TI;
static TypeInfo* t6168_ITIs[] = 
{
	&t603_TI,
	&t6170_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6168_0_0_0;
extern Il2CppType t6168_1_0_0;
struct t6168;
extern Il2CppGenericClass t6168_GC;
TypeInfo t6168_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6168_MIs, t6168_PIs, NULL, NULL, NULL, NULL, NULL, &t6168_TI, t6168_ITIs, NULL, &EmptyCustomAttributesCache, &t6168_TI, &t6168_0_0_0, &t6168_1_0_0, NULL, &t6168_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType t4751_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32093_GM;
MethodInfo m32093_MI = 
{
	"GetEnumerator", NULL, &t6170_TI, &t4751_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32093_GM};
static MethodInfo* t6170_MIs[] =
{
	&m32093_MI,
	NULL
};
static TypeInfo* t6170_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6170_0_0_0;
extern Il2CppType t6170_1_0_0;
struct t6170;
extern Il2CppGenericClass t6170_GC;
TypeInfo t6170_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6170_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6170_TI, t6170_ITIs, NULL, &EmptyCustomAttributesCache, &t6170_TI, &t6170_0_0_0, &t6170_1_0_0, NULL, &t6170_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6169_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo m32094_MI;
extern MethodInfo m32095_MI;
static PropertyInfo t6169____Item_PropertyInfo = 
{
	&t6169_TI, "Item", &m32094_MI, &m32095_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6169_PIs[] =
{
	&t6169____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1197_0_0_0;
static ParameterInfo t6169_m32096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32096_GM;
MethodInfo m32096_MI = 
{
	"IndexOf", NULL, &t6169_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6169_m32096_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32096_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1197_0_0_0;
static ParameterInfo t6169_m32097_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32097_GM;
MethodInfo m32097_MI = 
{
	"Insert", NULL, &t6169_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6169_m32097_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32097_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6169_m32098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32098_GM;
MethodInfo m32098_MI = 
{
	"RemoveAt", NULL, &t6169_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6169_m32098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32098_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6169_m32094_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1197_0_0_0;
extern void* RuntimeInvoker_t1197_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32094_GM;
MethodInfo m32094_MI = 
{
	"get_Item", NULL, &t6169_TI, &t1197_0_0_0, RuntimeInvoker_t1197_t44, t6169_m32094_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32094_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1197_0_0_0;
static ParameterInfo t6169_m32095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32095_GM;
MethodInfo m32095_MI = 
{
	"set_Item", NULL, &t6169_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6169_m32095_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32095_GM};
static MethodInfo* t6169_MIs[] =
{
	&m32096_MI,
	&m32097_MI,
	&m32098_MI,
	&m32094_MI,
	&m32095_MI,
	NULL
};
static TypeInfo* t6169_ITIs[] = 
{
	&t603_TI,
	&t6168_TI,
	&t6170_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6169_0_0_0;
extern Il2CppType t6169_1_0_0;
struct t6169;
extern Il2CppGenericClass t6169_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6169_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6169_MIs, t6169_PIs, NULL, NULL, NULL, NULL, NULL, &t6169_TI, t6169_ITIs, NULL, &t1908__CustomAttributeCache, &t6169_TI, &t6169_0_0_0, &t6169_1_0_0, NULL, &t6169_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4753_TI;

#include "t1196.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger>
extern MethodInfo m32099_MI;
static PropertyInfo t4753____Current_PropertyInfo = 
{
	&t4753_TI, "Current", &m32099_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4753_PIs[] =
{
	&t4753____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1196_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32099_GM;
MethodInfo m32099_MI = 
{
	"get_Current", NULL, &t4753_TI, &t1196_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32099_GM};
static MethodInfo* t4753_MIs[] =
{
	&m32099_MI,
	NULL
};
static TypeInfo* t4753_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4753_0_0_0;
extern Il2CppType t4753_1_0_0;
struct t4753;
extern Il2CppGenericClass t4753_GC;
TypeInfo t4753_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4753_MIs, t4753_PIs, NULL, NULL, NULL, NULL, NULL, &t4753_TI, t4753_ITIs, NULL, &EmptyCustomAttributesCache, &t4753_TI, &t4753_0_0_0, &t4753_1_0_0, NULL, &t4753_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3324.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3324_TI;
#include "t3324MD.h"

extern TypeInfo t1196_TI;
extern MethodInfo m18506_MI;
extern MethodInfo m24553_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m24553(__this, p0, method) (t1196 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.BigInteger>
extern Il2CppType t20_0_0_1;
FieldInfo t3324_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3324_TI, offsetof(t3324, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3324_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3324_TI, offsetof(t3324, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3324_FIs[] =
{
	&t3324_f0_FieldInfo,
	&t3324_f1_FieldInfo,
	NULL
};
extern MethodInfo m18503_MI;
static PropertyInfo t3324____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3324_TI, "System.Collections.IEnumerator.Current", &m18503_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3324____Current_PropertyInfo = 
{
	&t3324_TI, "Current", &m18506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3324_PIs[] =
{
	&t3324____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3324____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3324_m18502_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18502_GM;
MethodInfo m18502_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3324_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3324_m18502_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18502_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18503_GM;
MethodInfo m18503_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3324_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18503_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18504_GM;
MethodInfo m18504_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3324_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18504_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18505_GM;
MethodInfo m18505_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3324_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18505_GM};
extern Il2CppType t1196_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18506_GM;
MethodInfo m18506_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3324_TI, &t1196_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18506_GM};
static MethodInfo* t3324_MIs[] =
{
	&m18502_MI,
	&m18503_MI,
	&m18504_MI,
	&m18505_MI,
	&m18506_MI,
	NULL
};
extern MethodInfo m18505_MI;
extern MethodInfo m18504_MI;
static MethodInfo* t3324_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18503_MI,
	&m18505_MI,
	&m18504_MI,
	&m18506_MI,
};
static TypeInfo* t3324_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4753_TI,
};
static Il2CppInterfaceOffsetPair t3324_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4753_TI, 7},
};
extern TypeInfo t1196_TI;
static Il2CppRGCTXData t3324_RGCTXData[3] = 
{
	&m18506_MI/* Method Usage */,
	&t1196_TI/* Class Usage */,
	&m24553_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3324_0_0_0;
extern Il2CppType t3324_1_0_0;
extern Il2CppGenericClass t3324_GC;
TypeInfo t3324_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3324_MIs, t3324_PIs, t3324_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3324_TI, t3324_ITIs, t3324_VT, &EmptyCustomAttributesCache, &t3324_TI, &t3324_0_0_0, &t3324_1_0_0, t3324_IOs, &t3324_GC, NULL, NULL, NULL, t3324_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3324)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6171_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>
extern MethodInfo m32100_MI;
static PropertyInfo t6171____Count_PropertyInfo = 
{
	&t6171_TI, "Count", &m32100_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32101_MI;
static PropertyInfo t6171____IsReadOnly_PropertyInfo = 
{
	&t6171_TI, "IsReadOnly", &m32101_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6171_PIs[] =
{
	&t6171____Count_PropertyInfo,
	&t6171____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32100_GM;
MethodInfo m32100_MI = 
{
	"get_Count", NULL, &t6171_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32100_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32101_GM;
MethodInfo m32101_MI = 
{
	"get_IsReadOnly", NULL, &t6171_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32101_GM};
extern Il2CppType t1196_0_0_0;
extern Il2CppType t1196_0_0_0;
static ParameterInfo t6171_m32102_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1196_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32102_GM;
MethodInfo m32102_MI = 
{
	"Add", NULL, &t6171_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6171_m32102_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32102_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32103_GM;
MethodInfo m32103_MI = 
{
	"Clear", NULL, &t6171_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32103_GM};
extern Il2CppType t1196_0_0_0;
static ParameterInfo t6171_m32104_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1196_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32104_GM;
MethodInfo m32104_MI = 
{
	"Contains", NULL, &t6171_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6171_m32104_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32104_GM};
extern Il2CppType t1203_0_0_0;
extern Il2CppType t1203_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6171_m32105_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1203_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32105_GM;
MethodInfo m32105_MI = 
{
	"CopyTo", NULL, &t6171_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6171_m32105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32105_GM};
extern Il2CppType t1196_0_0_0;
static ParameterInfo t6171_m32106_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1196_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32106_GM;
MethodInfo m32106_MI = 
{
	"Remove", NULL, &t6171_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6171_m32106_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32106_GM};
static MethodInfo* t6171_MIs[] =
{
	&m32100_MI,
	&m32101_MI,
	&m32102_MI,
	&m32103_MI,
	&m32104_MI,
	&m32105_MI,
	&m32106_MI,
	NULL
};
extern TypeInfo t6173_TI;
static TypeInfo* t6171_ITIs[] = 
{
	&t603_TI,
	&t6173_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6171_0_0_0;
extern Il2CppType t6171_1_0_0;
struct t6171;
extern Il2CppGenericClass t6171_GC;
TypeInfo t6171_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6171_MIs, t6171_PIs, NULL, NULL, NULL, NULL, NULL, &t6171_TI, t6171_ITIs, NULL, &EmptyCustomAttributesCache, &t6171_TI, &t6171_0_0_0, &t6171_1_0_0, NULL, &t6171_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger>
extern Il2CppType t4753_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32107_GM;
MethodInfo m32107_MI = 
{
	"GetEnumerator", NULL, &t6173_TI, &t4753_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32107_GM};
static MethodInfo* t6173_MIs[] =
{
	&m32107_MI,
	NULL
};
static TypeInfo* t6173_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6173_0_0_0;
extern Il2CppType t6173_1_0_0;
struct t6173;
extern Il2CppGenericClass t6173_GC;
TypeInfo t6173_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6173_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6173_TI, t6173_ITIs, NULL, &EmptyCustomAttributesCache, &t6173_TI, &t6173_0_0_0, &t6173_1_0_0, NULL, &t6173_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6172_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger>
extern MethodInfo m32108_MI;
extern MethodInfo m32109_MI;
static PropertyInfo t6172____Item_PropertyInfo = 
{
	&t6172_TI, "Item", &m32108_MI, &m32109_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6172_PIs[] =
{
	&t6172____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1196_0_0_0;
static ParameterInfo t6172_m32110_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1196_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32110_GM;
MethodInfo m32110_MI = 
{
	"IndexOf", NULL, &t6172_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6172_m32110_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32110_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1196_0_0_0;
static ParameterInfo t6172_m32111_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1196_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32111_GM;
MethodInfo m32111_MI = 
{
	"Insert", NULL, &t6172_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6172_m32111_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32111_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6172_m32112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32112_GM;
MethodInfo m32112_MI = 
{
	"RemoveAt", NULL, &t6172_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6172_m32112_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32112_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6172_m32108_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1196_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32108_GM;
MethodInfo m32108_MI = 
{
	"get_Item", NULL, &t6172_TI, &t1196_0_0_0, RuntimeInvoker_t29_t44, t6172_m32108_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32108_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1196_0_0_0;
static ParameterInfo t6172_m32109_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1196_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32109_GM;
MethodInfo m32109_MI = 
{
	"set_Item", NULL, &t6172_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6172_m32109_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32109_GM};
static MethodInfo* t6172_MIs[] =
{
	&m32110_MI,
	&m32111_MI,
	&m32112_MI,
	&m32108_MI,
	&m32109_MI,
	NULL
};
static TypeInfo* t6172_ITIs[] = 
{
	&t603_TI,
	&t6171_TI,
	&t6173_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6172_0_0_0;
extern Il2CppType t6172_1_0_0;
struct t6172;
extern Il2CppGenericClass t6172_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6172_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6172_MIs, t6172_PIs, NULL, NULL, NULL, NULL, NULL, &t6172_TI, t6172_ITIs, NULL, &t1908__CustomAttributeCache, &t6172_TI, &t6172_0_0_0, &t6172_1_0_0, NULL, &t6172_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4755_TI;

#include "t1200.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger/Sign>
extern MethodInfo m32113_MI;
static PropertyInfo t4755____Current_PropertyInfo = 
{
	&t4755_TI, "Current", &m32113_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4755_PIs[] =
{
	&t4755____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1200_0_0_0;
extern void* RuntimeInvoker_t1200 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32113_GM;
MethodInfo m32113_MI = 
{
	"get_Current", NULL, &t4755_TI, &t1200_0_0_0, RuntimeInvoker_t1200, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32113_GM};
static MethodInfo* t4755_MIs[] =
{
	&m32113_MI,
	NULL
};
static TypeInfo* t4755_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4755_0_0_0;
extern Il2CppType t4755_1_0_0;
struct t4755;
extern Il2CppGenericClass t4755_GC;
TypeInfo t4755_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4755_MIs, t4755_PIs, NULL, NULL, NULL, NULL, NULL, &t4755_TI, t4755_ITIs, NULL, &EmptyCustomAttributesCache, &t4755_TI, &t4755_0_0_0, &t4755_1_0_0, NULL, &t4755_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3325.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3325_TI;
#include "t3325MD.h"

extern TypeInfo t1200_TI;
extern MethodInfo m18511_MI;
extern MethodInfo m24564_MI;
struct t20;
 int32_t m24564 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18507_MI;
 void m18507 (t3325 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18508_MI;
 t29 * m18508 (t3325 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18511(__this, &m18511_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1200_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18509_MI;
 void m18509 (t3325 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18510_MI;
 bool m18510 (t3325 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18511 (t3325 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24564(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24564_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>
extern Il2CppType t20_0_0_1;
FieldInfo t3325_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3325_TI, offsetof(t3325, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3325_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3325_TI, offsetof(t3325, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3325_FIs[] =
{
	&t3325_f0_FieldInfo,
	&t3325_f1_FieldInfo,
	NULL
};
static PropertyInfo t3325____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3325_TI, "System.Collections.IEnumerator.Current", &m18508_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3325____Current_PropertyInfo = 
{
	&t3325_TI, "Current", &m18511_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3325_PIs[] =
{
	&t3325____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3325____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3325_m18507_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18507_GM;
MethodInfo m18507_MI = 
{
	".ctor", (methodPointerType)&m18507, &t3325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3325_m18507_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18507_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18508_GM;
MethodInfo m18508_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18508, &t3325_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18508_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18509_GM;
MethodInfo m18509_MI = 
{
	"Dispose", (methodPointerType)&m18509, &t3325_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18509_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18510_GM;
MethodInfo m18510_MI = 
{
	"MoveNext", (methodPointerType)&m18510, &t3325_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18510_GM};
extern Il2CppType t1200_0_0_0;
extern void* RuntimeInvoker_t1200 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18511_GM;
MethodInfo m18511_MI = 
{
	"get_Current", (methodPointerType)&m18511, &t3325_TI, &t1200_0_0_0, RuntimeInvoker_t1200, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18511_GM};
static MethodInfo* t3325_MIs[] =
{
	&m18507_MI,
	&m18508_MI,
	&m18509_MI,
	&m18510_MI,
	&m18511_MI,
	NULL
};
static MethodInfo* t3325_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18508_MI,
	&m18510_MI,
	&m18509_MI,
	&m18511_MI,
};
static TypeInfo* t3325_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4755_TI,
};
static Il2CppInterfaceOffsetPair t3325_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4755_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3325_0_0_0;
extern Il2CppType t3325_1_0_0;
extern Il2CppGenericClass t3325_GC;
TypeInfo t3325_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3325_MIs, t3325_PIs, t3325_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3325_TI, t3325_ITIs, t3325_VT, &EmptyCustomAttributesCache, &t3325_TI, &t3325_0_0_0, &t3325_1_0_0, t3325_IOs, &t3325_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3325)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6174_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>
extern MethodInfo m32114_MI;
static PropertyInfo t6174____Count_PropertyInfo = 
{
	&t6174_TI, "Count", &m32114_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32115_MI;
static PropertyInfo t6174____IsReadOnly_PropertyInfo = 
{
	&t6174_TI, "IsReadOnly", &m32115_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6174_PIs[] =
{
	&t6174____Count_PropertyInfo,
	&t6174____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32114_GM;
MethodInfo m32114_MI = 
{
	"get_Count", NULL, &t6174_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32114_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32115_GM;
MethodInfo m32115_MI = 
{
	"get_IsReadOnly", NULL, &t6174_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32115_GM};
extern Il2CppType t1200_0_0_0;
extern Il2CppType t1200_0_0_0;
static ParameterInfo t6174_m32116_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1200_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32116_GM;
MethodInfo m32116_MI = 
{
	"Add", NULL, &t6174_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6174_m32116_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32116_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32117_GM;
MethodInfo m32117_MI = 
{
	"Clear", NULL, &t6174_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32117_GM};
extern Il2CppType t1200_0_0_0;
static ParameterInfo t6174_m32118_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1200_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32118_GM;
MethodInfo m32118_MI = 
{
	"Contains", NULL, &t6174_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6174_m32118_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32118_GM};
extern Il2CppType t3596_0_0_0;
extern Il2CppType t3596_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6174_m32119_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3596_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32119_GM;
MethodInfo m32119_MI = 
{
	"CopyTo", NULL, &t6174_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6174_m32119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32119_GM};
extern Il2CppType t1200_0_0_0;
static ParameterInfo t6174_m32120_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1200_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32120_GM;
MethodInfo m32120_MI = 
{
	"Remove", NULL, &t6174_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6174_m32120_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32120_GM};
static MethodInfo* t6174_MIs[] =
{
	&m32114_MI,
	&m32115_MI,
	&m32116_MI,
	&m32117_MI,
	&m32118_MI,
	&m32119_MI,
	&m32120_MI,
	NULL
};
extern TypeInfo t6176_TI;
static TypeInfo* t6174_ITIs[] = 
{
	&t603_TI,
	&t6176_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6174_0_0_0;
extern Il2CppType t6174_1_0_0;
struct t6174;
extern Il2CppGenericClass t6174_GC;
TypeInfo t6174_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6174_MIs, t6174_PIs, NULL, NULL, NULL, NULL, NULL, &t6174_TI, t6174_ITIs, NULL, &EmptyCustomAttributesCache, &t6174_TI, &t6174_0_0_0, &t6174_1_0_0, NULL, &t6174_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>
extern Il2CppType t4755_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32121_GM;
MethodInfo m32121_MI = 
{
	"GetEnumerator", NULL, &t6176_TI, &t4755_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32121_GM};
static MethodInfo* t6176_MIs[] =
{
	&m32121_MI,
	NULL
};
static TypeInfo* t6176_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6176_0_0_0;
extern Il2CppType t6176_1_0_0;
struct t6176;
extern Il2CppGenericClass t6176_GC;
TypeInfo t6176_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6176_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6176_TI, t6176_ITIs, NULL, &EmptyCustomAttributesCache, &t6176_TI, &t6176_0_0_0, &t6176_1_0_0, NULL, &t6176_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6175_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>
extern MethodInfo m32122_MI;
extern MethodInfo m32123_MI;
static PropertyInfo t6175____Item_PropertyInfo = 
{
	&t6175_TI, "Item", &m32122_MI, &m32123_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6175_PIs[] =
{
	&t6175____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1200_0_0_0;
static ParameterInfo t6175_m32124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1200_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32124_GM;
MethodInfo m32124_MI = 
{
	"IndexOf", NULL, &t6175_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6175_m32124_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32124_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1200_0_0_0;
static ParameterInfo t6175_m32125_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1200_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32125_GM;
MethodInfo m32125_MI = 
{
	"Insert", NULL, &t6175_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6175_m32125_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32125_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6175_m32126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32126_GM;
MethodInfo m32126_MI = 
{
	"RemoveAt", NULL, &t6175_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6175_m32126_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32126_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6175_m32122_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1200_0_0_0;
extern void* RuntimeInvoker_t1200_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32122_GM;
MethodInfo m32122_MI = 
{
	"get_Item", NULL, &t6175_TI, &t1200_0_0_0, RuntimeInvoker_t1200_t44, t6175_m32122_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32122_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1200_0_0_0;
static ParameterInfo t6175_m32123_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1200_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32123_GM;
MethodInfo m32123_MI = 
{
	"set_Item", NULL, &t6175_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6175_m32123_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32123_GM};
static MethodInfo* t6175_MIs[] =
{
	&m32124_MI,
	&m32125_MI,
	&m32126_MI,
	&m32122_MI,
	&m32123_MI,
	NULL
};
static TypeInfo* t6175_ITIs[] = 
{
	&t603_TI,
	&t6174_TI,
	&t6176_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6175_0_0_0;
extern Il2CppType t6175_1_0_0;
struct t6175;
extern Il2CppGenericClass t6175_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6175_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6175_MIs, t6175_PIs, NULL, NULL, NULL, NULL, NULL, &t6175_TI, t6175_ITIs, NULL, &t1908__CustomAttributeCache, &t6175_TI, &t6175_0_0_0, &t6175_1_0_0, NULL, &t6175_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3326.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3326_TI;
#include "t3326MD.h"



// Metadata Definition System.Collections.Generic.CollectionDebuggerView`1<System.Object>
static MethodInfo* t3326_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t3326_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3326_0_0_0;
extern Il2CppType t3326_1_0_0;
extern TypeInfo t29_TI;
struct t3326;
extern Il2CppGenericClass t3326_GC;
TypeInfo t3326_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CollectionDebuggerView`1", "System.Collections.Generic", t3326_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t3326_TI, NULL, t3326_VT, &EmptyCustomAttributesCache, &t3326_TI, &t3326_0_0_0, &t3326_1_0_0, NULL, &t3326_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3326), 0, -1, 0, 0, -1, 1048832, 0, false, false, false, false, true, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t3327.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3327_TI;
#include "t3327MD.h"



// Metadata Definition System.Collections.Generic.CollectionDebuggerView`2<System.Object,System.Object>
static MethodInfo* t3327_MIs[] =
{
	NULL
};
static MethodInfo* t3327_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3327_0_0_0;
extern Il2CppType t3327_1_0_0;
struct t3327;
extern Il2CppGenericClass t3327_GC;
TypeInfo t3327_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CollectionDebuggerView`2", "System.Collections.Generic", t3327_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t3327_TI, NULL, t3327_VT, &EmptyCustomAttributesCache, &t3327_TI, &t3327_0_0_0, &t3327_1_0_0, NULL, &t3327_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3327), 0, -1, 0, 0, -1, 1048832, 0, false, false, false, false, true, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4757_TI;

#include "t1272.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Hashtable/Slot>
extern MethodInfo m32127_MI;
static PropertyInfo t4757____Current_PropertyInfo = 
{
	&t4757_TI, "Current", &m32127_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4757_PIs[] =
{
	&t4757____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1272_0_0_0;
extern void* RuntimeInvoker_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32127_GM;
MethodInfo m32127_MI = 
{
	"get_Current", NULL, &t4757_TI, &t1272_0_0_0, RuntimeInvoker_t1272, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32127_GM};
static MethodInfo* t4757_MIs[] =
{
	&m32127_MI,
	NULL
};
static TypeInfo* t4757_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4757_0_0_0;
extern Il2CppType t4757_1_0_0;
struct t4757;
extern Il2CppGenericClass t4757_GC;
TypeInfo t4757_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4757_MIs, t4757_PIs, NULL, NULL, NULL, NULL, NULL, &t4757_TI, t4757_ITIs, NULL, &EmptyCustomAttributesCache, &t4757_TI, &t4757_0_0_0, &t4757_1_0_0, NULL, &t4757_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3328.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3328_TI;
#include "t3328MD.h"

extern TypeInfo t1272_TI;
extern MethodInfo m18516_MI;
extern MethodInfo m24575_MI;
struct t20;
 t1272  m24575 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18512_MI;
 void m18512 (t3328 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18513_MI;
 t29 * m18513 (t3328 * __this, MethodInfo* method){
	{
		t1272  L_0 = m18516(__this, &m18516_MI);
		t1272  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1272_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18514_MI;
 void m18514 (t3328 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18515_MI;
 bool m18515 (t3328 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t1272  m18516 (t3328 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t1272  L_8 = m24575(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24575_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>
extern Il2CppType t20_0_0_1;
FieldInfo t3328_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3328_TI, offsetof(t3328, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3328_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3328_TI, offsetof(t3328, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3328_FIs[] =
{
	&t3328_f0_FieldInfo,
	&t3328_f1_FieldInfo,
	NULL
};
static PropertyInfo t3328____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3328_TI, "System.Collections.IEnumerator.Current", &m18513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3328____Current_PropertyInfo = 
{
	&t3328_TI, "Current", &m18516_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3328_PIs[] =
{
	&t3328____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3328____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3328_m18512_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18512_GM;
MethodInfo m18512_MI = 
{
	".ctor", (methodPointerType)&m18512, &t3328_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3328_m18512_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18512_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18513_GM;
MethodInfo m18513_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18513, &t3328_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18513_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18514_GM;
MethodInfo m18514_MI = 
{
	"Dispose", (methodPointerType)&m18514, &t3328_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18514_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18515_GM;
MethodInfo m18515_MI = 
{
	"MoveNext", (methodPointerType)&m18515, &t3328_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18515_GM};
extern Il2CppType t1272_0_0_0;
extern void* RuntimeInvoker_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18516_GM;
MethodInfo m18516_MI = 
{
	"get_Current", (methodPointerType)&m18516, &t3328_TI, &t1272_0_0_0, RuntimeInvoker_t1272, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18516_GM};
static MethodInfo* t3328_MIs[] =
{
	&m18512_MI,
	&m18513_MI,
	&m18514_MI,
	&m18515_MI,
	&m18516_MI,
	NULL
};
static MethodInfo* t3328_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18513_MI,
	&m18515_MI,
	&m18514_MI,
	&m18516_MI,
};
static TypeInfo* t3328_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4757_TI,
};
static Il2CppInterfaceOffsetPair t3328_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4757_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3328_0_0_0;
extern Il2CppType t3328_1_0_0;
extern Il2CppGenericClass t3328_GC;
TypeInfo t3328_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3328_MIs, t3328_PIs, t3328_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3328_TI, t3328_ITIs, t3328_VT, &EmptyCustomAttributesCache, &t3328_TI, &t3328_0_0_0, &t3328_1_0_0, t3328_IOs, &t3328_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3328)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6177_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Hashtable/Slot>
extern MethodInfo m32128_MI;
static PropertyInfo t6177____Count_PropertyInfo = 
{
	&t6177_TI, "Count", &m32128_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32129_MI;
static PropertyInfo t6177____IsReadOnly_PropertyInfo = 
{
	&t6177_TI, "IsReadOnly", &m32129_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6177_PIs[] =
{
	&t6177____Count_PropertyInfo,
	&t6177____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32128_GM;
MethodInfo m32128_MI = 
{
	"get_Count", NULL, &t6177_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32128_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32129_GM;
MethodInfo m32129_MI = 
{
	"get_IsReadOnly", NULL, &t6177_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32129_GM};
extern Il2CppType t1272_0_0_0;
extern Il2CppType t1272_0_0_0;
static ParameterInfo t6177_m32130_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32130_GM;
MethodInfo m32130_MI = 
{
	"Add", NULL, &t6177_TI, &t21_0_0_0, RuntimeInvoker_t21_t1272, t6177_m32130_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32130_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32131_GM;
MethodInfo m32131_MI = 
{
	"Clear", NULL, &t6177_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32131_GM};
extern Il2CppType t1272_0_0_0;
static ParameterInfo t6177_m32132_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1272_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32132_GM;
MethodInfo m32132_MI = 
{
	"Contains", NULL, &t6177_TI, &t40_0_0_0, RuntimeInvoker_t40_t1272, t6177_m32132_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32132_GM};
extern Il2CppType t1278_0_0_0;
extern Il2CppType t1278_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6177_m32133_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1278_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32133_GM;
MethodInfo m32133_MI = 
{
	"CopyTo", NULL, &t6177_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6177_m32133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32133_GM};
extern Il2CppType t1272_0_0_0;
static ParameterInfo t6177_m32134_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1272_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32134_GM;
MethodInfo m32134_MI = 
{
	"Remove", NULL, &t6177_TI, &t40_0_0_0, RuntimeInvoker_t40_t1272, t6177_m32134_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32134_GM};
static MethodInfo* t6177_MIs[] =
{
	&m32128_MI,
	&m32129_MI,
	&m32130_MI,
	&m32131_MI,
	&m32132_MI,
	&m32133_MI,
	&m32134_MI,
	NULL
};
extern TypeInfo t6179_TI;
static TypeInfo* t6177_ITIs[] = 
{
	&t603_TI,
	&t6179_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6177_0_0_0;
extern Il2CppType t6177_1_0_0;
struct t6177;
extern Il2CppGenericClass t6177_GC;
TypeInfo t6177_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6177_MIs, t6177_PIs, NULL, NULL, NULL, NULL, NULL, &t6177_TI, t6177_ITIs, NULL, &EmptyCustomAttributesCache, &t6177_TI, &t6177_0_0_0, &t6177_1_0_0, NULL, &t6177_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Hashtable/Slot>
extern Il2CppType t4757_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32135_GM;
MethodInfo m32135_MI = 
{
	"GetEnumerator", NULL, &t6179_TI, &t4757_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32135_GM};
static MethodInfo* t6179_MIs[] =
{
	&m32135_MI,
	NULL
};
static TypeInfo* t6179_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6179_0_0_0;
extern Il2CppType t6179_1_0_0;
struct t6179;
extern Il2CppGenericClass t6179_GC;
TypeInfo t6179_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6179_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6179_TI, t6179_ITIs, NULL, &EmptyCustomAttributesCache, &t6179_TI, &t6179_0_0_0, &t6179_1_0_0, NULL, &t6179_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6178_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Hashtable/Slot>
extern MethodInfo m32136_MI;
extern MethodInfo m32137_MI;
static PropertyInfo t6178____Item_PropertyInfo = 
{
	&t6178_TI, "Item", &m32136_MI, &m32137_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6178_PIs[] =
{
	&t6178____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1272_0_0_0;
static ParameterInfo t6178_m32138_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1272_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32138_GM;
MethodInfo m32138_MI = 
{
	"IndexOf", NULL, &t6178_TI, &t44_0_0_0, RuntimeInvoker_t44_t1272, t6178_m32138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32138_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1272_0_0_0;
static ParameterInfo t6178_m32139_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32139_GM;
MethodInfo m32139_MI = 
{
	"Insert", NULL, &t6178_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1272, t6178_m32139_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32139_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6178_m32140_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32140_GM;
MethodInfo m32140_MI = 
{
	"RemoveAt", NULL, &t6178_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6178_m32140_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32140_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6178_m32136_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1272_0_0_0;
extern void* RuntimeInvoker_t1272_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32136_GM;
MethodInfo m32136_MI = 
{
	"get_Item", NULL, &t6178_TI, &t1272_0_0_0, RuntimeInvoker_t1272_t44, t6178_m32136_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32136_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1272_0_0_0;
static ParameterInfo t6178_m32137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1272_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32137_GM;
MethodInfo m32137_MI = 
{
	"set_Item", NULL, &t6178_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1272, t6178_m32137_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32137_GM};
static MethodInfo* t6178_MIs[] =
{
	&m32138_MI,
	&m32139_MI,
	&m32140_MI,
	&m32136_MI,
	&m32137_MI,
	NULL
};
static TypeInfo* t6178_ITIs[] = 
{
	&t603_TI,
	&t6177_TI,
	&t6179_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6178_0_0_0;
extern Il2CppType t6178_1_0_0;
struct t6178;
extern Il2CppGenericClass t6178_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6178_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6178_MIs, t6178_PIs, NULL, NULL, NULL, NULL, NULL, &t6178_TI, t6178_ITIs, NULL, &t1908__CustomAttributeCache, &t6178_TI, &t6178_0_0_0, &t6178_1_0_0, NULL, &t6178_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4759_TI;

#include "t1274.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Hashtable/EnumeratorMode>
extern MethodInfo m32141_MI;
static PropertyInfo t4759____Current_PropertyInfo = 
{
	&t4759_TI, "Current", &m32141_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4759_PIs[] =
{
	&t4759____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1274_0_0_0;
extern void* RuntimeInvoker_t1274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32141_GM;
MethodInfo m32141_MI = 
{
	"get_Current", NULL, &t4759_TI, &t1274_0_0_0, RuntimeInvoker_t1274, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32141_GM};
static MethodInfo* t4759_MIs[] =
{
	&m32141_MI,
	NULL
};
static TypeInfo* t4759_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4759_0_0_0;
extern Il2CppType t4759_1_0_0;
struct t4759;
extern Il2CppGenericClass t4759_GC;
TypeInfo t4759_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4759_MIs, t4759_PIs, NULL, NULL, NULL, NULL, NULL, &t4759_TI, t4759_ITIs, NULL, &EmptyCustomAttributesCache, &t4759_TI, &t4759_0_0_0, &t4759_1_0_0, NULL, &t4759_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3329.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3329_TI;
#include "t3329MD.h"

extern TypeInfo t1274_TI;
extern MethodInfo m18521_MI;
extern MethodInfo m24586_MI;
struct t20;
 int32_t m24586 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18517_MI;
 void m18517 (t3329 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18518_MI;
 t29 * m18518 (t3329 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18521(__this, &m18521_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1274_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18519_MI;
 void m18519 (t3329 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18520_MI;
 bool m18520 (t3329 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18521 (t3329 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24586(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24586_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Hashtable/EnumeratorMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3329_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3329_TI, offsetof(t3329, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3329_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3329_TI, offsetof(t3329, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3329_FIs[] =
{
	&t3329_f0_FieldInfo,
	&t3329_f1_FieldInfo,
	NULL
};
static PropertyInfo t3329____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3329_TI, "System.Collections.IEnumerator.Current", &m18518_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3329____Current_PropertyInfo = 
{
	&t3329_TI, "Current", &m18521_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3329_PIs[] =
{
	&t3329____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3329____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3329_m18517_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18517_GM;
MethodInfo m18517_MI = 
{
	".ctor", (methodPointerType)&m18517, &t3329_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3329_m18517_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18517_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18518_GM;
MethodInfo m18518_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18518, &t3329_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18518_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18519_GM;
MethodInfo m18519_MI = 
{
	"Dispose", (methodPointerType)&m18519, &t3329_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18519_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18520_GM;
MethodInfo m18520_MI = 
{
	"MoveNext", (methodPointerType)&m18520, &t3329_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18520_GM};
extern Il2CppType t1274_0_0_0;
extern void* RuntimeInvoker_t1274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18521_GM;
MethodInfo m18521_MI = 
{
	"get_Current", (methodPointerType)&m18521, &t3329_TI, &t1274_0_0_0, RuntimeInvoker_t1274, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18521_GM};
static MethodInfo* t3329_MIs[] =
{
	&m18517_MI,
	&m18518_MI,
	&m18519_MI,
	&m18520_MI,
	&m18521_MI,
	NULL
};
static MethodInfo* t3329_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18518_MI,
	&m18520_MI,
	&m18519_MI,
	&m18521_MI,
};
static TypeInfo* t3329_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4759_TI,
};
static Il2CppInterfaceOffsetPair t3329_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4759_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3329_0_0_0;
extern Il2CppType t3329_1_0_0;
extern Il2CppGenericClass t3329_GC;
TypeInfo t3329_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3329_MIs, t3329_PIs, t3329_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3329_TI, t3329_ITIs, t3329_VT, &EmptyCustomAttributesCache, &t3329_TI, &t3329_0_0_0, &t3329_1_0_0, t3329_IOs, &t3329_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3329)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6180_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Hashtable/EnumeratorMode>
extern MethodInfo m32142_MI;
static PropertyInfo t6180____Count_PropertyInfo = 
{
	&t6180_TI, "Count", &m32142_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32143_MI;
static PropertyInfo t6180____IsReadOnly_PropertyInfo = 
{
	&t6180_TI, "IsReadOnly", &m32143_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6180_PIs[] =
{
	&t6180____Count_PropertyInfo,
	&t6180____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32142_GM;
MethodInfo m32142_MI = 
{
	"get_Count", NULL, &t6180_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32142_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32143_GM;
MethodInfo m32143_MI = 
{
	"get_IsReadOnly", NULL, &t6180_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32143_GM};
extern Il2CppType t1274_0_0_0;
extern Il2CppType t1274_0_0_0;
static ParameterInfo t6180_m32144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1274_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32144_GM;
MethodInfo m32144_MI = 
{
	"Add", NULL, &t6180_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6180_m32144_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32144_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32145_GM;
MethodInfo m32145_MI = 
{
	"Clear", NULL, &t6180_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32145_GM};
extern Il2CppType t1274_0_0_0;
static ParameterInfo t6180_m32146_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1274_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32146_GM;
MethodInfo m32146_MI = 
{
	"Contains", NULL, &t6180_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6180_m32146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32146_GM};
extern Il2CppType t3597_0_0_0;
extern Il2CppType t3597_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6180_m32147_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3597_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32147_GM;
MethodInfo m32147_MI = 
{
	"CopyTo", NULL, &t6180_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6180_m32147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32147_GM};
extern Il2CppType t1274_0_0_0;
static ParameterInfo t6180_m32148_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1274_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32148_GM;
MethodInfo m32148_MI = 
{
	"Remove", NULL, &t6180_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6180_m32148_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32148_GM};
static MethodInfo* t6180_MIs[] =
{
	&m32142_MI,
	&m32143_MI,
	&m32144_MI,
	&m32145_MI,
	&m32146_MI,
	&m32147_MI,
	&m32148_MI,
	NULL
};
extern TypeInfo t6182_TI;
static TypeInfo* t6180_ITIs[] = 
{
	&t603_TI,
	&t6182_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6180_0_0_0;
extern Il2CppType t6180_1_0_0;
struct t6180;
extern Il2CppGenericClass t6180_GC;
TypeInfo t6180_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6180_MIs, t6180_PIs, NULL, NULL, NULL, NULL, NULL, &t6180_TI, t6180_ITIs, NULL, &EmptyCustomAttributesCache, &t6180_TI, &t6180_0_0_0, &t6180_1_0_0, NULL, &t6180_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Hashtable/EnumeratorMode>
extern Il2CppType t4759_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32149_GM;
MethodInfo m32149_MI = 
{
	"GetEnumerator", NULL, &t6182_TI, &t4759_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32149_GM};
static MethodInfo* t6182_MIs[] =
{
	&m32149_MI,
	NULL
};
static TypeInfo* t6182_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6182_0_0_0;
extern Il2CppType t6182_1_0_0;
struct t6182;
extern Il2CppGenericClass t6182_GC;
TypeInfo t6182_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6182_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6182_TI, t6182_ITIs, NULL, &EmptyCustomAttributesCache, &t6182_TI, &t6182_0_0_0, &t6182_1_0_0, NULL, &t6182_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6181_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Hashtable/EnumeratorMode>
extern MethodInfo m32150_MI;
extern MethodInfo m32151_MI;
static PropertyInfo t6181____Item_PropertyInfo = 
{
	&t6181_TI, "Item", &m32150_MI, &m32151_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6181_PIs[] =
{
	&t6181____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1274_0_0_0;
static ParameterInfo t6181_m32152_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1274_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32152_GM;
MethodInfo m32152_MI = 
{
	"IndexOf", NULL, &t6181_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6181_m32152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32152_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1274_0_0_0;
static ParameterInfo t6181_m32153_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1274_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32153_GM;
MethodInfo m32153_MI = 
{
	"Insert", NULL, &t6181_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6181_m32153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32153_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6181_m32154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32154_GM;
MethodInfo m32154_MI = 
{
	"RemoveAt", NULL, &t6181_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6181_m32154_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32154_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6181_m32150_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1274_0_0_0;
extern void* RuntimeInvoker_t1274_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32150_GM;
MethodInfo m32150_MI = 
{
	"get_Item", NULL, &t6181_TI, &t1274_0_0_0, RuntimeInvoker_t1274_t44, t6181_m32150_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32150_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1274_0_0_0;
static ParameterInfo t6181_m32151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1274_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32151_GM;
MethodInfo m32151_MI = 
{
	"set_Item", NULL, &t6181_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6181_m32151_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32151_GM};
static MethodInfo* t6181_MIs[] =
{
	&m32152_MI,
	&m32153_MI,
	&m32154_MI,
	&m32150_MI,
	&m32151_MI,
	NULL
};
static TypeInfo* t6181_ITIs[] = 
{
	&t603_TI,
	&t6180_TI,
	&t6182_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6181_0_0_0;
extern Il2CppType t6181_1_0_0;
struct t6181;
extern Il2CppGenericClass t6181_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6181_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6181_MIs, t6181_PIs, NULL, NULL, NULL, NULL, NULL, &t6181_TI, t6181_ITIs, NULL, &t1908__CustomAttributeCache, &t6181_TI, &t6181_0_0_0, &t6181_1_0_0, NULL, &t6181_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4761_TI;

#include "t1279.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.SortedList/Slot>
extern MethodInfo m32155_MI;
static PropertyInfo t4761____Current_PropertyInfo = 
{
	&t4761_TI, "Current", &m32155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4761_PIs[] =
{
	&t4761____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1279_0_0_0;
extern void* RuntimeInvoker_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32155_GM;
MethodInfo m32155_MI = 
{
	"get_Current", NULL, &t4761_TI, &t1279_0_0_0, RuntimeInvoker_t1279, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32155_GM};
static MethodInfo* t4761_MIs[] =
{
	&m32155_MI,
	NULL
};
static TypeInfo* t4761_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4761_0_0_0;
extern Il2CppType t4761_1_0_0;
struct t4761;
extern Il2CppGenericClass t4761_GC;
TypeInfo t4761_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4761_MIs, t4761_PIs, NULL, NULL, NULL, NULL, NULL, &t4761_TI, t4761_ITIs, NULL, &EmptyCustomAttributesCache, &t4761_TI, &t4761_0_0_0, &t4761_1_0_0, NULL, &t4761_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3330.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3330_TI;
#include "t3330MD.h"

extern TypeInfo t1279_TI;
extern MethodInfo m18526_MI;
extern MethodInfo m24597_MI;
struct t20;
 t1279  m24597 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18522_MI;
 void m18522 (t3330 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18523_MI;
 t29 * m18523 (t3330 * __this, MethodInfo* method){
	{
		t1279  L_0 = m18526(__this, &m18526_MI);
		t1279  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1279_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18524_MI;
 void m18524 (t3330 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18525_MI;
 bool m18525 (t3330 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t1279  m18526 (t3330 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t1279  L_8 = m24597(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24597_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
extern Il2CppType t20_0_0_1;
FieldInfo t3330_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3330_TI, offsetof(t3330, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3330_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3330_TI, offsetof(t3330, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3330_FIs[] =
{
	&t3330_f0_FieldInfo,
	&t3330_f1_FieldInfo,
	NULL
};
static PropertyInfo t3330____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3330_TI, "System.Collections.IEnumerator.Current", &m18523_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3330____Current_PropertyInfo = 
{
	&t3330_TI, "Current", &m18526_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3330_PIs[] =
{
	&t3330____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3330____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3330_m18522_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18522_GM;
MethodInfo m18522_MI = 
{
	".ctor", (methodPointerType)&m18522, &t3330_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3330_m18522_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18522_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18523_GM;
MethodInfo m18523_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18523, &t3330_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18523_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18524_GM;
MethodInfo m18524_MI = 
{
	"Dispose", (methodPointerType)&m18524, &t3330_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18524_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18525_GM;
MethodInfo m18525_MI = 
{
	"MoveNext", (methodPointerType)&m18525, &t3330_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18525_GM};
extern Il2CppType t1279_0_0_0;
extern void* RuntimeInvoker_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18526_GM;
MethodInfo m18526_MI = 
{
	"get_Current", (methodPointerType)&m18526, &t3330_TI, &t1279_0_0_0, RuntimeInvoker_t1279, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18526_GM};
static MethodInfo* t3330_MIs[] =
{
	&m18522_MI,
	&m18523_MI,
	&m18524_MI,
	&m18525_MI,
	&m18526_MI,
	NULL
};
static MethodInfo* t3330_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18523_MI,
	&m18525_MI,
	&m18524_MI,
	&m18526_MI,
};
static TypeInfo* t3330_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4761_TI,
};
static Il2CppInterfaceOffsetPair t3330_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4761_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3330_0_0_0;
extern Il2CppType t3330_1_0_0;
extern Il2CppGenericClass t3330_GC;
TypeInfo t3330_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3330_MIs, t3330_PIs, t3330_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3330_TI, t3330_ITIs, t3330_VT, &EmptyCustomAttributesCache, &t3330_TI, &t3330_0_0_0, &t3330_1_0_0, t3330_IOs, &t3330_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3330)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6183_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.SortedList/Slot>
extern MethodInfo m32156_MI;
static PropertyInfo t6183____Count_PropertyInfo = 
{
	&t6183_TI, "Count", &m32156_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32157_MI;
static PropertyInfo t6183____IsReadOnly_PropertyInfo = 
{
	&t6183_TI, "IsReadOnly", &m32157_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6183_PIs[] =
{
	&t6183____Count_PropertyInfo,
	&t6183____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32156_GM;
MethodInfo m32156_MI = 
{
	"get_Count", NULL, &t6183_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32156_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32157_GM;
MethodInfo m32157_MI = 
{
	"get_IsReadOnly", NULL, &t6183_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32157_GM};
extern Il2CppType t1279_0_0_0;
extern Il2CppType t1279_0_0_0;
static ParameterInfo t6183_m32158_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1279_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32158_GM;
MethodInfo m32158_MI = 
{
	"Add", NULL, &t6183_TI, &t21_0_0_0, RuntimeInvoker_t21_t1279, t6183_m32158_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32158_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32159_GM;
MethodInfo m32159_MI = 
{
	"Clear", NULL, &t6183_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32159_GM};
extern Il2CppType t1279_0_0_0;
static ParameterInfo t6183_m32160_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1279_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32160_GM;
MethodInfo m32160_MI = 
{
	"Contains", NULL, &t6183_TI, &t40_0_0_0, RuntimeInvoker_t40_t1279, t6183_m32160_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32160_GM};
extern Il2CppType t1282_0_0_0;
extern Il2CppType t1282_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6183_m32161_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1282_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32161_GM;
MethodInfo m32161_MI = 
{
	"CopyTo", NULL, &t6183_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6183_m32161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32161_GM};
extern Il2CppType t1279_0_0_0;
static ParameterInfo t6183_m32162_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1279_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32162_GM;
MethodInfo m32162_MI = 
{
	"Remove", NULL, &t6183_TI, &t40_0_0_0, RuntimeInvoker_t40_t1279, t6183_m32162_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32162_GM};
static MethodInfo* t6183_MIs[] =
{
	&m32156_MI,
	&m32157_MI,
	&m32158_MI,
	&m32159_MI,
	&m32160_MI,
	&m32161_MI,
	&m32162_MI,
	NULL
};
extern TypeInfo t6185_TI;
static TypeInfo* t6183_ITIs[] = 
{
	&t603_TI,
	&t6185_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6183_0_0_0;
extern Il2CppType t6183_1_0_0;
struct t6183;
extern Il2CppGenericClass t6183_GC;
TypeInfo t6183_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6183_MIs, t6183_PIs, NULL, NULL, NULL, NULL, NULL, &t6183_TI, t6183_ITIs, NULL, &EmptyCustomAttributesCache, &t6183_TI, &t6183_0_0_0, &t6183_1_0_0, NULL, &t6183_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.SortedList/Slot>
extern Il2CppType t4761_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32163_GM;
MethodInfo m32163_MI = 
{
	"GetEnumerator", NULL, &t6185_TI, &t4761_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32163_GM};
static MethodInfo* t6185_MIs[] =
{
	&m32163_MI,
	NULL
};
static TypeInfo* t6185_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6185_0_0_0;
extern Il2CppType t6185_1_0_0;
struct t6185;
extern Il2CppGenericClass t6185_GC;
TypeInfo t6185_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6185_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6185_TI, t6185_ITIs, NULL, &EmptyCustomAttributesCache, &t6185_TI, &t6185_0_0_0, &t6185_1_0_0, NULL, &t6185_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6184_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.SortedList/Slot>
extern MethodInfo m32164_MI;
extern MethodInfo m32165_MI;
static PropertyInfo t6184____Item_PropertyInfo = 
{
	&t6184_TI, "Item", &m32164_MI, &m32165_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6184_PIs[] =
{
	&t6184____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1279_0_0_0;
static ParameterInfo t6184_m32166_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1279_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32166_GM;
MethodInfo m32166_MI = 
{
	"IndexOf", NULL, &t6184_TI, &t44_0_0_0, RuntimeInvoker_t44_t1279, t6184_m32166_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32166_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1279_0_0_0;
static ParameterInfo t6184_m32167_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1279_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32167_GM;
MethodInfo m32167_MI = 
{
	"Insert", NULL, &t6184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1279, t6184_m32167_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32167_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6184_m32168_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32168_GM;
MethodInfo m32168_MI = 
{
	"RemoveAt", NULL, &t6184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6184_m32168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32168_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6184_m32164_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1279_0_0_0;
extern void* RuntimeInvoker_t1279_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32164_GM;
MethodInfo m32164_MI = 
{
	"get_Item", NULL, &t6184_TI, &t1279_0_0_0, RuntimeInvoker_t1279_t44, t6184_m32164_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32164_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1279_0_0_0;
static ParameterInfo t6184_m32165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1279_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1279 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32165_GM;
MethodInfo m32165_MI = 
{
	"set_Item", NULL, &t6184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1279, t6184_m32165_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32165_GM};
static MethodInfo* t6184_MIs[] =
{
	&m32166_MI,
	&m32167_MI,
	&m32168_MI,
	&m32164_MI,
	&m32165_MI,
	NULL
};
static TypeInfo* t6184_ITIs[] = 
{
	&t603_TI,
	&t6183_TI,
	&t6185_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6184_0_0_0;
extern Il2CppType t6184_1_0_0;
struct t6184;
extern Il2CppGenericClass t6184_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6184_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6184_MIs, t6184_PIs, NULL, NULL, NULL, NULL, NULL, &t6184_TI, t6184_ITIs, NULL, &t1908__CustomAttributeCache, &t6184_TI, &t6184_0_0_0, &t6184_1_0_0, NULL, &t6184_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4763_TI;

#include "t1280.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.SortedList/EnumeratorMode>
extern MethodInfo m32169_MI;
static PropertyInfo t4763____Current_PropertyInfo = 
{
	&t4763_TI, "Current", &m32169_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4763_PIs[] =
{
	&t4763____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1280_0_0_0;
extern void* RuntimeInvoker_t1280 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32169_GM;
MethodInfo m32169_MI = 
{
	"get_Current", NULL, &t4763_TI, &t1280_0_0_0, RuntimeInvoker_t1280, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32169_GM};
static MethodInfo* t4763_MIs[] =
{
	&m32169_MI,
	NULL
};
static TypeInfo* t4763_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4763_0_0_0;
extern Il2CppType t4763_1_0_0;
struct t4763;
extern Il2CppGenericClass t4763_GC;
TypeInfo t4763_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4763_MIs, t4763_PIs, NULL, NULL, NULL, NULL, NULL, &t4763_TI, t4763_ITIs, NULL, &EmptyCustomAttributesCache, &t4763_TI, &t4763_0_0_0, &t4763_1_0_0, NULL, &t4763_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3331.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3331_TI;
#include "t3331MD.h"

extern TypeInfo t1280_TI;
extern MethodInfo m18531_MI;
extern MethodInfo m24608_MI;
struct t20;
 int32_t m24608 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18527_MI;
 void m18527 (t3331 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18528_MI;
 t29 * m18528 (t3331 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18531(__this, &m18531_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1280_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18529_MI;
 void m18529 (t3331 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18530_MI;
 bool m18530 (t3331 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18531 (t3331 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24608(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24608_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3331_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3331_TI, offsetof(t3331, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3331_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3331_TI, offsetof(t3331, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3331_FIs[] =
{
	&t3331_f0_FieldInfo,
	&t3331_f1_FieldInfo,
	NULL
};
static PropertyInfo t3331____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3331_TI, "System.Collections.IEnumerator.Current", &m18528_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3331____Current_PropertyInfo = 
{
	&t3331_TI, "Current", &m18531_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3331_PIs[] =
{
	&t3331____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3331____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3331_m18527_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18527_GM;
MethodInfo m18527_MI = 
{
	".ctor", (methodPointerType)&m18527, &t3331_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3331_m18527_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18527_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18528_GM;
MethodInfo m18528_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18528, &t3331_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18528_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18529_GM;
MethodInfo m18529_MI = 
{
	"Dispose", (methodPointerType)&m18529, &t3331_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18529_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18530_GM;
MethodInfo m18530_MI = 
{
	"MoveNext", (methodPointerType)&m18530, &t3331_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18530_GM};
extern Il2CppType t1280_0_0_0;
extern void* RuntimeInvoker_t1280 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18531_GM;
MethodInfo m18531_MI = 
{
	"get_Current", (methodPointerType)&m18531, &t3331_TI, &t1280_0_0_0, RuntimeInvoker_t1280, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18531_GM};
static MethodInfo* t3331_MIs[] =
{
	&m18527_MI,
	&m18528_MI,
	&m18529_MI,
	&m18530_MI,
	&m18531_MI,
	NULL
};
static MethodInfo* t3331_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18528_MI,
	&m18530_MI,
	&m18529_MI,
	&m18531_MI,
};
static TypeInfo* t3331_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4763_TI,
};
static Il2CppInterfaceOffsetPair t3331_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4763_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3331_0_0_0;
extern Il2CppType t3331_1_0_0;
extern Il2CppGenericClass t3331_GC;
TypeInfo t3331_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3331_MIs, t3331_PIs, t3331_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3331_TI, t3331_ITIs, t3331_VT, &EmptyCustomAttributesCache, &t3331_TI, &t3331_0_0_0, &t3331_1_0_0, t3331_IOs, &t3331_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3331)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6186_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.SortedList/EnumeratorMode>
extern MethodInfo m32170_MI;
static PropertyInfo t6186____Count_PropertyInfo = 
{
	&t6186_TI, "Count", &m32170_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32171_MI;
static PropertyInfo t6186____IsReadOnly_PropertyInfo = 
{
	&t6186_TI, "IsReadOnly", &m32171_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6186_PIs[] =
{
	&t6186____Count_PropertyInfo,
	&t6186____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32170_GM;
MethodInfo m32170_MI = 
{
	"get_Count", NULL, &t6186_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32170_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32171_GM;
MethodInfo m32171_MI = 
{
	"get_IsReadOnly", NULL, &t6186_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32171_GM};
extern Il2CppType t1280_0_0_0;
extern Il2CppType t1280_0_0_0;
static ParameterInfo t6186_m32172_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1280_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32172_GM;
MethodInfo m32172_MI = 
{
	"Add", NULL, &t6186_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6186_m32172_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32172_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32173_GM;
MethodInfo m32173_MI = 
{
	"Clear", NULL, &t6186_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32173_GM};
extern Il2CppType t1280_0_0_0;
static ParameterInfo t6186_m32174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1280_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32174_GM;
MethodInfo m32174_MI = 
{
	"Contains", NULL, &t6186_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6186_m32174_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32174_GM};
extern Il2CppType t3598_0_0_0;
extern Il2CppType t3598_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6186_m32175_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3598_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32175_GM;
MethodInfo m32175_MI = 
{
	"CopyTo", NULL, &t6186_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6186_m32175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32175_GM};
extern Il2CppType t1280_0_0_0;
static ParameterInfo t6186_m32176_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1280_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32176_GM;
MethodInfo m32176_MI = 
{
	"Remove", NULL, &t6186_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6186_m32176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32176_GM};
static MethodInfo* t6186_MIs[] =
{
	&m32170_MI,
	&m32171_MI,
	&m32172_MI,
	&m32173_MI,
	&m32174_MI,
	&m32175_MI,
	&m32176_MI,
	NULL
};
extern TypeInfo t6188_TI;
static TypeInfo* t6186_ITIs[] = 
{
	&t603_TI,
	&t6188_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6186_0_0_0;
extern Il2CppType t6186_1_0_0;
struct t6186;
extern Il2CppGenericClass t6186_GC;
TypeInfo t6186_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6186_MIs, t6186_PIs, NULL, NULL, NULL, NULL, NULL, &t6186_TI, t6186_ITIs, NULL, &EmptyCustomAttributesCache, &t6186_TI, &t6186_0_0_0, &t6186_1_0_0, NULL, &t6186_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.SortedList/EnumeratorMode>
extern Il2CppType t4763_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32177_GM;
MethodInfo m32177_MI = 
{
	"GetEnumerator", NULL, &t6188_TI, &t4763_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32177_GM};
static MethodInfo* t6188_MIs[] =
{
	&m32177_MI,
	NULL
};
static TypeInfo* t6188_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6188_0_0_0;
extern Il2CppType t6188_1_0_0;
struct t6188;
extern Il2CppGenericClass t6188_GC;
TypeInfo t6188_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6188_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6188_TI, t6188_ITIs, NULL, &EmptyCustomAttributesCache, &t6188_TI, &t6188_0_0_0, &t6188_1_0_0, NULL, &t6188_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6187_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.SortedList/EnumeratorMode>
extern MethodInfo m32178_MI;
extern MethodInfo m32179_MI;
static PropertyInfo t6187____Item_PropertyInfo = 
{
	&t6187_TI, "Item", &m32178_MI, &m32179_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6187_PIs[] =
{
	&t6187____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1280_0_0_0;
static ParameterInfo t6187_m32180_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1280_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32180_GM;
MethodInfo m32180_MI = 
{
	"IndexOf", NULL, &t6187_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6187_m32180_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32180_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1280_0_0_0;
static ParameterInfo t6187_m32181_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1280_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32181_GM;
MethodInfo m32181_MI = 
{
	"Insert", NULL, &t6187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6187_m32181_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32181_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6187_m32182_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32182_GM;
MethodInfo m32182_MI = 
{
	"RemoveAt", NULL, &t6187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6187_m32182_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32182_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6187_m32178_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1280_0_0_0;
extern void* RuntimeInvoker_t1280_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32178_GM;
MethodInfo m32178_MI = 
{
	"get_Item", NULL, &t6187_TI, &t1280_0_0_0, RuntimeInvoker_t1280_t44, t6187_m32178_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32178_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1280_0_0_0;
static ParameterInfo t6187_m32179_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1280_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32179_GM;
MethodInfo m32179_MI = 
{
	"set_Item", NULL, &t6187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6187_m32179_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32179_GM};
static MethodInfo* t6187_MIs[] =
{
	&m32180_MI,
	&m32181_MI,
	&m32182_MI,
	&m32178_MI,
	&m32179_MI,
	NULL
};
static TypeInfo* t6187_ITIs[] = 
{
	&t603_TI,
	&t6186_TI,
	&t6188_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6187_0_0_0;
extern Il2CppType t6187_1_0_0;
struct t6187;
extern Il2CppGenericClass t6187_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6187_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6187_MIs, t6187_PIs, NULL, NULL, NULL, NULL, NULL, &t6187_TI, t6187_ITIs, NULL, &t1908__CustomAttributeCache, &t6187_TI, &t6187_0_0_0, &t6187_1_0_0, NULL, &t6187_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4765_TI;

#include "t1284.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Configuration.Assemblies.AssemblyHashAlgorithm>
extern MethodInfo m32183_MI;
static PropertyInfo t4765____Current_PropertyInfo = 
{
	&t4765_TI, "Current", &m32183_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4765_PIs[] =
{
	&t4765____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1284_0_0_0;
extern void* RuntimeInvoker_t1284 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32183_GM;
MethodInfo m32183_MI = 
{
	"get_Current", NULL, &t4765_TI, &t1284_0_0_0, RuntimeInvoker_t1284, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32183_GM};
static MethodInfo* t4765_MIs[] =
{
	&m32183_MI,
	NULL
};
static TypeInfo* t4765_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4765_0_0_0;
extern Il2CppType t4765_1_0_0;
struct t4765;
extern Il2CppGenericClass t4765_GC;
TypeInfo t4765_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4765_MIs, t4765_PIs, NULL, NULL, NULL, NULL, NULL, &t4765_TI, t4765_ITIs, NULL, &EmptyCustomAttributesCache, &t4765_TI, &t4765_0_0_0, &t4765_1_0_0, NULL, &t4765_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3332.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3332_TI;
#include "t3332MD.h"

extern TypeInfo t1284_TI;
extern MethodInfo m18536_MI;
extern MethodInfo m24619_MI;
struct t20;
 int32_t m24619 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18532_MI;
 void m18532 (t3332 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18533_MI;
 t29 * m18533 (t3332 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18536(__this, &m18536_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1284_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18534_MI;
 void m18534 (t3332 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18535_MI;
 bool m18535 (t3332 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18536 (t3332 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24619(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24619_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyHashAlgorithm>
extern Il2CppType t20_0_0_1;
FieldInfo t3332_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3332_TI, offsetof(t3332, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3332_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3332_TI, offsetof(t3332, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3332_FIs[] =
{
	&t3332_f0_FieldInfo,
	&t3332_f1_FieldInfo,
	NULL
};
static PropertyInfo t3332____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3332_TI, "System.Collections.IEnumerator.Current", &m18533_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3332____Current_PropertyInfo = 
{
	&t3332_TI, "Current", &m18536_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3332_PIs[] =
{
	&t3332____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3332____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3332_m18532_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18532_GM;
MethodInfo m18532_MI = 
{
	".ctor", (methodPointerType)&m18532, &t3332_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3332_m18532_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18532_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18533_GM;
MethodInfo m18533_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18533, &t3332_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18533_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18534_GM;
MethodInfo m18534_MI = 
{
	"Dispose", (methodPointerType)&m18534, &t3332_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18534_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18535_GM;
MethodInfo m18535_MI = 
{
	"MoveNext", (methodPointerType)&m18535, &t3332_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18535_GM};
extern Il2CppType t1284_0_0_0;
extern void* RuntimeInvoker_t1284 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18536_GM;
MethodInfo m18536_MI = 
{
	"get_Current", (methodPointerType)&m18536, &t3332_TI, &t1284_0_0_0, RuntimeInvoker_t1284, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18536_GM};
static MethodInfo* t3332_MIs[] =
{
	&m18532_MI,
	&m18533_MI,
	&m18534_MI,
	&m18535_MI,
	&m18536_MI,
	NULL
};
static MethodInfo* t3332_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18533_MI,
	&m18535_MI,
	&m18534_MI,
	&m18536_MI,
};
static TypeInfo* t3332_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4765_TI,
};
static Il2CppInterfaceOffsetPair t3332_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4765_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3332_0_0_0;
extern Il2CppType t3332_1_0_0;
extern Il2CppGenericClass t3332_GC;
TypeInfo t3332_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3332_MIs, t3332_PIs, t3332_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3332_TI, t3332_ITIs, t3332_VT, &EmptyCustomAttributesCache, &t3332_TI, &t3332_0_0_0, &t3332_1_0_0, t3332_IOs, &t3332_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3332)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6189_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Configuration.Assemblies.AssemblyHashAlgorithm>
extern MethodInfo m32184_MI;
static PropertyInfo t6189____Count_PropertyInfo = 
{
	&t6189_TI, "Count", &m32184_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32185_MI;
static PropertyInfo t6189____IsReadOnly_PropertyInfo = 
{
	&t6189_TI, "IsReadOnly", &m32185_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6189_PIs[] =
{
	&t6189____Count_PropertyInfo,
	&t6189____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32184_GM;
MethodInfo m32184_MI = 
{
	"get_Count", NULL, &t6189_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32184_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32185_GM;
MethodInfo m32185_MI = 
{
	"get_IsReadOnly", NULL, &t6189_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32185_GM};
extern Il2CppType t1284_0_0_0;
extern Il2CppType t1284_0_0_0;
static ParameterInfo t6189_m32186_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32186_GM;
MethodInfo m32186_MI = 
{
	"Add", NULL, &t6189_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6189_m32186_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32186_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32187_GM;
MethodInfo m32187_MI = 
{
	"Clear", NULL, &t6189_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32187_GM};
extern Il2CppType t1284_0_0_0;
static ParameterInfo t6189_m32188_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1284_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32188_GM;
MethodInfo m32188_MI = 
{
	"Contains", NULL, &t6189_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6189_m32188_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32188_GM};
extern Il2CppType t3599_0_0_0;
extern Il2CppType t3599_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6189_m32189_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3599_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32189_GM;
MethodInfo m32189_MI = 
{
	"CopyTo", NULL, &t6189_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6189_m32189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32189_GM};
extern Il2CppType t1284_0_0_0;
static ParameterInfo t6189_m32190_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1284_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32190_GM;
MethodInfo m32190_MI = 
{
	"Remove", NULL, &t6189_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6189_m32190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32190_GM};
static MethodInfo* t6189_MIs[] =
{
	&m32184_MI,
	&m32185_MI,
	&m32186_MI,
	&m32187_MI,
	&m32188_MI,
	&m32189_MI,
	&m32190_MI,
	NULL
};
extern TypeInfo t6191_TI;
static TypeInfo* t6189_ITIs[] = 
{
	&t603_TI,
	&t6191_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6189_0_0_0;
extern Il2CppType t6189_1_0_0;
struct t6189;
extern Il2CppGenericClass t6189_GC;
TypeInfo t6189_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6189_MIs, t6189_PIs, NULL, NULL, NULL, NULL, NULL, &t6189_TI, t6189_ITIs, NULL, &EmptyCustomAttributesCache, &t6189_TI, &t6189_0_0_0, &t6189_1_0_0, NULL, &t6189_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Configuration.Assemblies.AssemblyHashAlgorithm>
extern Il2CppType t4765_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32191_GM;
MethodInfo m32191_MI = 
{
	"GetEnumerator", NULL, &t6191_TI, &t4765_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32191_GM};
static MethodInfo* t6191_MIs[] =
{
	&m32191_MI,
	NULL
};
static TypeInfo* t6191_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6191_0_0_0;
extern Il2CppType t6191_1_0_0;
struct t6191;
extern Il2CppGenericClass t6191_GC;
TypeInfo t6191_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6191_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6191_TI, t6191_ITIs, NULL, &EmptyCustomAttributesCache, &t6191_TI, &t6191_0_0_0, &t6191_1_0_0, NULL, &t6191_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6190_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Configuration.Assemblies.AssemblyHashAlgorithm>
extern MethodInfo m32192_MI;
extern MethodInfo m32193_MI;
static PropertyInfo t6190____Item_PropertyInfo = 
{
	&t6190_TI, "Item", &m32192_MI, &m32193_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6190_PIs[] =
{
	&t6190____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1284_0_0_0;
static ParameterInfo t6190_m32194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1284_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32194_GM;
MethodInfo m32194_MI = 
{
	"IndexOf", NULL, &t6190_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6190_m32194_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32194_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1284_0_0_0;
static ParameterInfo t6190_m32195_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32195_GM;
MethodInfo m32195_MI = 
{
	"Insert", NULL, &t6190_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6190_m32195_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32195_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6190_m32196_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32196_GM;
MethodInfo m32196_MI = 
{
	"RemoveAt", NULL, &t6190_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6190_m32196_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32196_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6190_m32192_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1284_0_0_0;
extern void* RuntimeInvoker_t1284_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32192_GM;
MethodInfo m32192_MI = 
{
	"get_Item", NULL, &t6190_TI, &t1284_0_0_0, RuntimeInvoker_t1284_t44, t6190_m32192_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32192_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1284_0_0_0;
static ParameterInfo t6190_m32193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1284_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32193_GM;
MethodInfo m32193_MI = 
{
	"set_Item", NULL, &t6190_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6190_m32193_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32193_GM};
static MethodInfo* t6190_MIs[] =
{
	&m32194_MI,
	&m32195_MI,
	&m32196_MI,
	&m32192_MI,
	&m32193_MI,
	NULL
};
static TypeInfo* t6190_ITIs[] = 
{
	&t603_TI,
	&t6189_TI,
	&t6191_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6190_0_0_0;
extern Il2CppType t6190_1_0_0;
struct t6190;
extern Il2CppGenericClass t6190_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6190_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6190_MIs, t6190_PIs, NULL, NULL, NULL, NULL, NULL, &t6190_TI, t6190_ITIs, NULL, &t1908__CustomAttributeCache, &t6190_TI, &t6190_0_0_0, &t6190_1_0_0, NULL, &t6190_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4767_TI;

#include "t1285.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>
extern MethodInfo m32197_MI;
static PropertyInfo t4767____Current_PropertyInfo = 
{
	&t4767_TI, "Current", &m32197_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4767_PIs[] =
{
	&t4767____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1285_0_0_0;
extern void* RuntimeInvoker_t1285 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32197_GM;
MethodInfo m32197_MI = 
{
	"get_Current", NULL, &t4767_TI, &t1285_0_0_0, RuntimeInvoker_t1285, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32197_GM};
static MethodInfo* t4767_MIs[] =
{
	&m32197_MI,
	NULL
};
static TypeInfo* t4767_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4767_0_0_0;
extern Il2CppType t4767_1_0_0;
struct t4767;
extern Il2CppGenericClass t4767_GC;
TypeInfo t4767_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4767_MIs, t4767_PIs, NULL, NULL, NULL, NULL, NULL, &t4767_TI, t4767_ITIs, NULL, &EmptyCustomAttributesCache, &t4767_TI, &t4767_0_0_0, &t4767_1_0_0, NULL, &t4767_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3333.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3333_TI;
#include "t3333MD.h"

extern TypeInfo t1285_TI;
extern MethodInfo m18541_MI;
extern MethodInfo m24630_MI;
struct t20;
 int32_t m24630 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18537_MI;
 void m18537 (t3333 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18538_MI;
 t29 * m18538 (t3333 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18541(__this, &m18541_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1285_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18539_MI;
 void m18539 (t3333 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18540_MI;
 bool m18540 (t3333 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18541 (t3333 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24630(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24630_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>
extern Il2CppType t20_0_0_1;
FieldInfo t3333_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3333_TI, offsetof(t3333, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3333_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3333_TI, offsetof(t3333, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3333_FIs[] =
{
	&t3333_f0_FieldInfo,
	&t3333_f1_FieldInfo,
	NULL
};
static PropertyInfo t3333____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3333_TI, "System.Collections.IEnumerator.Current", &m18538_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3333____Current_PropertyInfo = 
{
	&t3333_TI, "Current", &m18541_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3333_PIs[] =
{
	&t3333____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3333____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3333_m18537_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18537_GM;
MethodInfo m18537_MI = 
{
	".ctor", (methodPointerType)&m18537, &t3333_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3333_m18537_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18537_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18538_GM;
MethodInfo m18538_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18538, &t3333_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18538_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18539_GM;
MethodInfo m18539_MI = 
{
	"Dispose", (methodPointerType)&m18539, &t3333_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18539_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18540_GM;
MethodInfo m18540_MI = 
{
	"MoveNext", (methodPointerType)&m18540, &t3333_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18540_GM};
extern Il2CppType t1285_0_0_0;
extern void* RuntimeInvoker_t1285 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18541_GM;
MethodInfo m18541_MI = 
{
	"get_Current", (methodPointerType)&m18541, &t3333_TI, &t1285_0_0_0, RuntimeInvoker_t1285, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18541_GM};
static MethodInfo* t3333_MIs[] =
{
	&m18537_MI,
	&m18538_MI,
	&m18539_MI,
	&m18540_MI,
	&m18541_MI,
	NULL
};
static MethodInfo* t3333_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18538_MI,
	&m18540_MI,
	&m18539_MI,
	&m18541_MI,
};
static TypeInfo* t3333_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4767_TI,
};
static Il2CppInterfaceOffsetPair t3333_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4767_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3333_0_0_0;
extern Il2CppType t3333_1_0_0;
extern Il2CppGenericClass t3333_GC;
TypeInfo t3333_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3333_MIs, t3333_PIs, t3333_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3333_TI, t3333_ITIs, t3333_VT, &EmptyCustomAttributesCache, &t3333_TI, &t3333_0_0_0, &t3333_1_0_0, t3333_IOs, &t3333_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3333)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6192_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>
extern MethodInfo m32198_MI;
static PropertyInfo t6192____Count_PropertyInfo = 
{
	&t6192_TI, "Count", &m32198_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32199_MI;
static PropertyInfo t6192____IsReadOnly_PropertyInfo = 
{
	&t6192_TI, "IsReadOnly", &m32199_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6192_PIs[] =
{
	&t6192____Count_PropertyInfo,
	&t6192____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32198_GM;
MethodInfo m32198_MI = 
{
	"get_Count", NULL, &t6192_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32198_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32199_GM;
MethodInfo m32199_MI = 
{
	"get_IsReadOnly", NULL, &t6192_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32199_GM};
extern Il2CppType t1285_0_0_0;
extern Il2CppType t1285_0_0_0;
static ParameterInfo t6192_m32200_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32200_GM;
MethodInfo m32200_MI = 
{
	"Add", NULL, &t6192_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6192_m32200_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32200_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32201_GM;
MethodInfo m32201_MI = 
{
	"Clear", NULL, &t6192_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32201_GM};
extern Il2CppType t1285_0_0_0;
static ParameterInfo t6192_m32202_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1285_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32202_GM;
MethodInfo m32202_MI = 
{
	"Contains", NULL, &t6192_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6192_m32202_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32202_GM};
extern Il2CppType t3600_0_0_0;
extern Il2CppType t3600_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6192_m32203_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3600_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32203_GM;
MethodInfo m32203_MI = 
{
	"CopyTo", NULL, &t6192_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6192_m32203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32203_GM};
extern Il2CppType t1285_0_0_0;
static ParameterInfo t6192_m32204_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1285_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32204_GM;
MethodInfo m32204_MI = 
{
	"Remove", NULL, &t6192_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6192_m32204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32204_GM};
static MethodInfo* t6192_MIs[] =
{
	&m32198_MI,
	&m32199_MI,
	&m32200_MI,
	&m32201_MI,
	&m32202_MI,
	&m32203_MI,
	&m32204_MI,
	NULL
};
extern TypeInfo t6194_TI;
static TypeInfo* t6192_ITIs[] = 
{
	&t603_TI,
	&t6194_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6192_0_0_0;
extern Il2CppType t6192_1_0_0;
struct t6192;
extern Il2CppGenericClass t6192_GC;
TypeInfo t6192_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6192_MIs, t6192_PIs, NULL, NULL, NULL, NULL, NULL, &t6192_TI, t6192_ITIs, NULL, &EmptyCustomAttributesCache, &t6192_TI, &t6192_0_0_0, &t6192_1_0_0, NULL, &t6192_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>
extern Il2CppType t4767_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32205_GM;
MethodInfo m32205_MI = 
{
	"GetEnumerator", NULL, &t6194_TI, &t4767_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32205_GM};
static MethodInfo* t6194_MIs[] =
{
	&m32205_MI,
	NULL
};
static TypeInfo* t6194_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6194_0_0_0;
extern Il2CppType t6194_1_0_0;
struct t6194;
extern Il2CppGenericClass t6194_GC;
TypeInfo t6194_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6194_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6194_TI, t6194_ITIs, NULL, &EmptyCustomAttributesCache, &t6194_TI, &t6194_0_0_0, &t6194_1_0_0, NULL, &t6194_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6193_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>
extern MethodInfo m32206_MI;
extern MethodInfo m32207_MI;
static PropertyInfo t6193____Item_PropertyInfo = 
{
	&t6193_TI, "Item", &m32206_MI, &m32207_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6193_PIs[] =
{
	&t6193____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1285_0_0_0;
static ParameterInfo t6193_m32208_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1285_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32208_GM;
MethodInfo m32208_MI = 
{
	"IndexOf", NULL, &t6193_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6193_m32208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32208_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1285_0_0_0;
static ParameterInfo t6193_m32209_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32209_GM;
MethodInfo m32209_MI = 
{
	"Insert", NULL, &t6193_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6193_m32209_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32209_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6193_m32210_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32210_GM;
MethodInfo m32210_MI = 
{
	"RemoveAt", NULL, &t6193_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6193_m32210_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32210_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6193_m32206_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1285_0_0_0;
extern void* RuntimeInvoker_t1285_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32206_GM;
MethodInfo m32206_MI = 
{
	"get_Item", NULL, &t6193_TI, &t1285_0_0_0, RuntimeInvoker_t1285_t44, t6193_m32206_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32206_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1285_0_0_0;
static ParameterInfo t6193_m32207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1285_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32207_GM;
MethodInfo m32207_MI = 
{
	"set_Item", NULL, &t6193_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6193_m32207_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32207_GM};
static MethodInfo* t6193_MIs[] =
{
	&m32208_MI,
	&m32209_MI,
	&m32210_MI,
	&m32206_MI,
	&m32207_MI,
	NULL
};
static TypeInfo* t6193_ITIs[] = 
{
	&t603_TI,
	&t6192_TI,
	&t6194_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6193_0_0_0;
extern Il2CppType t6193_1_0_0;
struct t6193;
extern Il2CppGenericClass t6193_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6193_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6193_MIs, t6193_PIs, NULL, NULL, NULL, NULL, NULL, &t6193_TI, t6193_ITIs, NULL, &t1908__CustomAttributeCache, &t6193_TI, &t6193_0_0_0, &t6193_1_0_0, NULL, &t6193_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4769_TI;

#include "t708.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggableAttribute>
extern MethodInfo m32211_MI;
static PropertyInfo t4769____Current_PropertyInfo = 
{
	&t4769_TI, "Current", &m32211_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4769_PIs[] =
{
	&t4769____Current_PropertyInfo,
	NULL
};
extern Il2CppType t708_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32211_GM;
MethodInfo m32211_MI = 
{
	"get_Current", NULL, &t4769_TI, &t708_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32211_GM};
static MethodInfo* t4769_MIs[] =
{
	&m32211_MI,
	NULL
};
static TypeInfo* t4769_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4769_0_0_0;
extern Il2CppType t4769_1_0_0;
struct t4769;
extern Il2CppGenericClass t4769_GC;
TypeInfo t4769_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4769_MIs, t4769_PIs, NULL, NULL, NULL, NULL, NULL, &t4769_TI, t4769_ITIs, NULL, &EmptyCustomAttributesCache, &t4769_TI, &t4769_0_0_0, &t4769_1_0_0, NULL, &t4769_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3334.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3334_TI;
#include "t3334MD.h"

extern TypeInfo t708_TI;
extern MethodInfo m18546_MI;
extern MethodInfo m24641_MI;
struct t20;
#define m24641(__this, p0, method) (t708 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3334_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3334_TI, offsetof(t3334, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3334_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3334_TI, offsetof(t3334, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3334_FIs[] =
{
	&t3334_f0_FieldInfo,
	&t3334_f1_FieldInfo,
	NULL
};
extern MethodInfo m18543_MI;
static PropertyInfo t3334____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3334_TI, "System.Collections.IEnumerator.Current", &m18543_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3334____Current_PropertyInfo = 
{
	&t3334_TI, "Current", &m18546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3334_PIs[] =
{
	&t3334____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3334____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3334_m18542_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18542_GM;
MethodInfo m18542_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3334_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3334_m18542_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18542_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18543_GM;
MethodInfo m18543_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3334_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18543_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18544_GM;
MethodInfo m18544_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3334_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18544_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18545_GM;
MethodInfo m18545_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3334_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18545_GM};
extern Il2CppType t708_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18546_GM;
MethodInfo m18546_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3334_TI, &t708_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18546_GM};
static MethodInfo* t3334_MIs[] =
{
	&m18542_MI,
	&m18543_MI,
	&m18544_MI,
	&m18545_MI,
	&m18546_MI,
	NULL
};
extern MethodInfo m18545_MI;
extern MethodInfo m18544_MI;
static MethodInfo* t3334_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18543_MI,
	&m18545_MI,
	&m18544_MI,
	&m18546_MI,
};
static TypeInfo* t3334_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4769_TI,
};
static Il2CppInterfaceOffsetPair t3334_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4769_TI, 7},
};
extern TypeInfo t708_TI;
static Il2CppRGCTXData t3334_RGCTXData[3] = 
{
	&m18546_MI/* Method Usage */,
	&t708_TI/* Class Usage */,
	&m24641_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3334_0_0_0;
extern Il2CppType t3334_1_0_0;
extern Il2CppGenericClass t3334_GC;
TypeInfo t3334_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3334_MIs, t3334_PIs, t3334_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3334_TI, t3334_ITIs, t3334_VT, &EmptyCustomAttributesCache, &t3334_TI, &t3334_0_0_0, &t3334_1_0_0, t3334_IOs, &t3334_GC, NULL, NULL, NULL, t3334_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3334)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6195_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggableAttribute>
extern MethodInfo m32212_MI;
static PropertyInfo t6195____Count_PropertyInfo = 
{
	&t6195_TI, "Count", &m32212_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32213_MI;
static PropertyInfo t6195____IsReadOnly_PropertyInfo = 
{
	&t6195_TI, "IsReadOnly", &m32213_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6195_PIs[] =
{
	&t6195____Count_PropertyInfo,
	&t6195____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32212_GM;
MethodInfo m32212_MI = 
{
	"get_Count", NULL, &t6195_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32212_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32213_GM;
MethodInfo m32213_MI = 
{
	"get_IsReadOnly", NULL, &t6195_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32213_GM};
extern Il2CppType t708_0_0_0;
extern Il2CppType t708_0_0_0;
static ParameterInfo t6195_m32214_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t708_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32214_GM;
MethodInfo m32214_MI = 
{
	"Add", NULL, &t6195_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6195_m32214_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32214_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32215_GM;
MethodInfo m32215_MI = 
{
	"Clear", NULL, &t6195_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32215_GM};
extern Il2CppType t708_0_0_0;
static ParameterInfo t6195_m32216_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t708_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32216_GM;
MethodInfo m32216_MI = 
{
	"Contains", NULL, &t6195_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6195_m32216_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32216_GM};
extern Il2CppType t3601_0_0_0;
extern Il2CppType t3601_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6195_m32217_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3601_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32217_GM;
MethodInfo m32217_MI = 
{
	"CopyTo", NULL, &t6195_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6195_m32217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32217_GM};
extern Il2CppType t708_0_0_0;
static ParameterInfo t6195_m32218_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t708_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32218_GM;
MethodInfo m32218_MI = 
{
	"Remove", NULL, &t6195_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6195_m32218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32218_GM};
static MethodInfo* t6195_MIs[] =
{
	&m32212_MI,
	&m32213_MI,
	&m32214_MI,
	&m32215_MI,
	&m32216_MI,
	&m32217_MI,
	&m32218_MI,
	NULL
};
extern TypeInfo t6197_TI;
static TypeInfo* t6195_ITIs[] = 
{
	&t603_TI,
	&t6197_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6195_0_0_0;
extern Il2CppType t6195_1_0_0;
struct t6195;
extern Il2CppGenericClass t6195_GC;
TypeInfo t6195_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6195_MIs, t6195_PIs, NULL, NULL, NULL, NULL, NULL, &t6195_TI, t6195_ITIs, NULL, &EmptyCustomAttributesCache, &t6195_TI, &t6195_0_0_0, &t6195_1_0_0, NULL, &t6195_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggableAttribute>
extern Il2CppType t4769_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32219_GM;
MethodInfo m32219_MI = 
{
	"GetEnumerator", NULL, &t6197_TI, &t4769_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32219_GM};
static MethodInfo* t6197_MIs[] =
{
	&m32219_MI,
	NULL
};
static TypeInfo* t6197_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6197_0_0_0;
extern Il2CppType t6197_1_0_0;
struct t6197;
extern Il2CppGenericClass t6197_GC;
TypeInfo t6197_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6197_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6197_TI, t6197_ITIs, NULL, &EmptyCustomAttributesCache, &t6197_TI, &t6197_0_0_0, &t6197_1_0_0, NULL, &t6197_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6196_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggableAttribute>
extern MethodInfo m32220_MI;
extern MethodInfo m32221_MI;
static PropertyInfo t6196____Item_PropertyInfo = 
{
	&t6196_TI, "Item", &m32220_MI, &m32221_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6196_PIs[] =
{
	&t6196____Item_PropertyInfo,
	NULL
};
extern Il2CppType t708_0_0_0;
static ParameterInfo t6196_m32222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t708_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32222_GM;
MethodInfo m32222_MI = 
{
	"IndexOf", NULL, &t6196_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6196_m32222_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32222_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t708_0_0_0;
static ParameterInfo t6196_m32223_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t708_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32223_GM;
MethodInfo m32223_MI = 
{
	"Insert", NULL, &t6196_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6196_m32223_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32223_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6196_m32224_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32224_GM;
MethodInfo m32224_MI = 
{
	"RemoveAt", NULL, &t6196_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6196_m32224_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32224_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6196_m32220_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t708_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32220_GM;
MethodInfo m32220_MI = 
{
	"get_Item", NULL, &t6196_TI, &t708_0_0_0, RuntimeInvoker_t29_t44, t6196_m32220_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32220_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t708_0_0_0;
static ParameterInfo t6196_m32221_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t708_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32221_GM;
MethodInfo m32221_MI = 
{
	"set_Item", NULL, &t6196_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6196_m32221_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32221_GM};
static MethodInfo* t6196_MIs[] =
{
	&m32222_MI,
	&m32223_MI,
	&m32224_MI,
	&m32220_MI,
	&m32221_MI,
	NULL
};
static TypeInfo* t6196_ITIs[] = 
{
	&t603_TI,
	&t6195_TI,
	&t6197_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6196_0_0_0;
extern Il2CppType t6196_1_0_0;
struct t6196;
extern Il2CppGenericClass t6196_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6196_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6196_MIs, t6196_PIs, NULL, NULL, NULL, NULL, NULL, &t6196_TI, t6196_ITIs, NULL, &t1908__CustomAttributeCache, &t6196_TI, &t6196_0_0_0, &t6196_1_0_0, NULL, &t6196_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4771_TI;

#include "t1286.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggableAttribute/DebuggingModes>
extern MethodInfo m32225_MI;
static PropertyInfo t4771____Current_PropertyInfo = 
{
	&t4771_TI, "Current", &m32225_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4771_PIs[] =
{
	&t4771____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1286_0_0_0;
extern void* RuntimeInvoker_t1286 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32225_GM;
MethodInfo m32225_MI = 
{
	"get_Current", NULL, &t4771_TI, &t1286_0_0_0, RuntimeInvoker_t1286, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32225_GM};
static MethodInfo* t4771_MIs[] =
{
	&m32225_MI,
	NULL
};
static TypeInfo* t4771_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4771_0_0_0;
extern Il2CppType t4771_1_0_0;
struct t4771;
extern Il2CppGenericClass t4771_GC;
TypeInfo t4771_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4771_MIs, t4771_PIs, NULL, NULL, NULL, NULL, NULL, &t4771_TI, t4771_ITIs, NULL, &EmptyCustomAttributesCache, &t4771_TI, &t4771_0_0_0, &t4771_1_0_0, NULL, &t4771_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3335.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3335_TI;
#include "t3335MD.h"

extern TypeInfo t1286_TI;
extern MethodInfo m18551_MI;
extern MethodInfo m24652_MI;
struct t20;
 int32_t m24652 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18547_MI;
 void m18547 (t3335 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18548_MI;
 t29 * m18548 (t3335 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18551(__this, &m18551_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1286_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18549_MI;
 void m18549 (t3335 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18550_MI;
 bool m18550 (t3335 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18551 (t3335 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24652(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24652_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute/DebuggingModes>
extern Il2CppType t20_0_0_1;
FieldInfo t3335_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3335_TI, offsetof(t3335, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3335_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3335_TI, offsetof(t3335, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3335_FIs[] =
{
	&t3335_f0_FieldInfo,
	&t3335_f1_FieldInfo,
	NULL
};
static PropertyInfo t3335____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3335_TI, "System.Collections.IEnumerator.Current", &m18548_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3335____Current_PropertyInfo = 
{
	&t3335_TI, "Current", &m18551_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3335_PIs[] =
{
	&t3335____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3335____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3335_m18547_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18547_GM;
MethodInfo m18547_MI = 
{
	".ctor", (methodPointerType)&m18547, &t3335_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3335_m18547_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18547_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18548_GM;
MethodInfo m18548_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18548, &t3335_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18548_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18549_GM;
MethodInfo m18549_MI = 
{
	"Dispose", (methodPointerType)&m18549, &t3335_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18549_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18550_GM;
MethodInfo m18550_MI = 
{
	"MoveNext", (methodPointerType)&m18550, &t3335_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18550_GM};
extern Il2CppType t1286_0_0_0;
extern void* RuntimeInvoker_t1286 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18551_GM;
MethodInfo m18551_MI = 
{
	"get_Current", (methodPointerType)&m18551, &t3335_TI, &t1286_0_0_0, RuntimeInvoker_t1286, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18551_GM};
static MethodInfo* t3335_MIs[] =
{
	&m18547_MI,
	&m18548_MI,
	&m18549_MI,
	&m18550_MI,
	&m18551_MI,
	NULL
};
static MethodInfo* t3335_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18548_MI,
	&m18550_MI,
	&m18549_MI,
	&m18551_MI,
};
static TypeInfo* t3335_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4771_TI,
};
static Il2CppInterfaceOffsetPair t3335_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4771_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3335_0_0_0;
extern Il2CppType t3335_1_0_0;
extern Il2CppGenericClass t3335_GC;
TypeInfo t3335_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3335_MIs, t3335_PIs, t3335_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3335_TI, t3335_ITIs, t3335_VT, &EmptyCustomAttributesCache, &t3335_TI, &t3335_0_0_0, &t3335_1_0_0, t3335_IOs, &t3335_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3335)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6198_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggableAttribute/DebuggingModes>
extern MethodInfo m32226_MI;
static PropertyInfo t6198____Count_PropertyInfo = 
{
	&t6198_TI, "Count", &m32226_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32227_MI;
static PropertyInfo t6198____IsReadOnly_PropertyInfo = 
{
	&t6198_TI, "IsReadOnly", &m32227_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6198_PIs[] =
{
	&t6198____Count_PropertyInfo,
	&t6198____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32226_GM;
MethodInfo m32226_MI = 
{
	"get_Count", NULL, &t6198_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32226_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32227_GM;
MethodInfo m32227_MI = 
{
	"get_IsReadOnly", NULL, &t6198_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32227_GM};
extern Il2CppType t1286_0_0_0;
extern Il2CppType t1286_0_0_0;
static ParameterInfo t6198_m32228_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32228_GM;
MethodInfo m32228_MI = 
{
	"Add", NULL, &t6198_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6198_m32228_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32228_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32229_GM;
MethodInfo m32229_MI = 
{
	"Clear", NULL, &t6198_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32229_GM};
extern Il2CppType t1286_0_0_0;
static ParameterInfo t6198_m32230_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1286_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32230_GM;
MethodInfo m32230_MI = 
{
	"Contains", NULL, &t6198_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6198_m32230_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32230_GM};
extern Il2CppType t3602_0_0_0;
extern Il2CppType t3602_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6198_m32231_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3602_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32231_GM;
MethodInfo m32231_MI = 
{
	"CopyTo", NULL, &t6198_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6198_m32231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32231_GM};
extern Il2CppType t1286_0_0_0;
static ParameterInfo t6198_m32232_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1286_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32232_GM;
MethodInfo m32232_MI = 
{
	"Remove", NULL, &t6198_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6198_m32232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32232_GM};
static MethodInfo* t6198_MIs[] =
{
	&m32226_MI,
	&m32227_MI,
	&m32228_MI,
	&m32229_MI,
	&m32230_MI,
	&m32231_MI,
	&m32232_MI,
	NULL
};
extern TypeInfo t6200_TI;
static TypeInfo* t6198_ITIs[] = 
{
	&t603_TI,
	&t6200_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6198_0_0_0;
extern Il2CppType t6198_1_0_0;
struct t6198;
extern Il2CppGenericClass t6198_GC;
TypeInfo t6198_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6198_MIs, t6198_PIs, NULL, NULL, NULL, NULL, NULL, &t6198_TI, t6198_ITIs, NULL, &EmptyCustomAttributesCache, &t6198_TI, &t6198_0_0_0, &t6198_1_0_0, NULL, &t6198_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggableAttribute/DebuggingModes>
extern Il2CppType t4771_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32233_GM;
MethodInfo m32233_MI = 
{
	"GetEnumerator", NULL, &t6200_TI, &t4771_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32233_GM};
static MethodInfo* t6200_MIs[] =
{
	&m32233_MI,
	NULL
};
static TypeInfo* t6200_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6200_0_0_0;
extern Il2CppType t6200_1_0_0;
struct t6200;
extern Il2CppGenericClass t6200_GC;
TypeInfo t6200_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6200_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6200_TI, t6200_ITIs, NULL, &EmptyCustomAttributesCache, &t6200_TI, &t6200_0_0_0, &t6200_1_0_0, NULL, &t6200_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6199_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggableAttribute/DebuggingModes>
extern MethodInfo m32234_MI;
extern MethodInfo m32235_MI;
static PropertyInfo t6199____Item_PropertyInfo = 
{
	&t6199_TI, "Item", &m32234_MI, &m32235_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6199_PIs[] =
{
	&t6199____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1286_0_0_0;
static ParameterInfo t6199_m32236_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1286_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32236_GM;
MethodInfo m32236_MI = 
{
	"IndexOf", NULL, &t6199_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6199_m32236_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32236_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1286_0_0_0;
static ParameterInfo t6199_m32237_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32237_GM;
MethodInfo m32237_MI = 
{
	"Insert", NULL, &t6199_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6199_m32237_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32237_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6199_m32238_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32238_GM;
MethodInfo m32238_MI = 
{
	"RemoveAt", NULL, &t6199_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6199_m32238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32238_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6199_m32234_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1286_0_0_0;
extern void* RuntimeInvoker_t1286_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32234_GM;
MethodInfo m32234_MI = 
{
	"get_Item", NULL, &t6199_TI, &t1286_0_0_0, RuntimeInvoker_t1286_t44, t6199_m32234_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32234_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1286_0_0_0;
static ParameterInfo t6199_m32235_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1286_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32235_GM;
MethodInfo m32235_MI = 
{
	"set_Item", NULL, &t6199_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6199_m32235_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32235_GM};
static MethodInfo* t6199_MIs[] =
{
	&m32236_MI,
	&m32237_MI,
	&m32238_MI,
	&m32234_MI,
	&m32235_MI,
	NULL
};
static TypeInfo* t6199_ITIs[] = 
{
	&t603_TI,
	&t6198_TI,
	&t6200_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6199_0_0_0;
extern Il2CppType t6199_1_0_0;
struct t6199;
extern Il2CppGenericClass t6199_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6199_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6199_MIs, t6199_PIs, NULL, NULL, NULL, NULL, NULL, &t6199_TI, t6199_ITIs, NULL, &t1908__CustomAttributeCache, &t6199_TI, &t6199_0_0_0, &t6199_1_0_0, NULL, &t6199_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4773_TI;

#include "t1287.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerDisplayAttribute>
extern MethodInfo m32239_MI;
static PropertyInfo t4773____Current_PropertyInfo = 
{
	&t4773_TI, "Current", &m32239_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4773_PIs[] =
{
	&t4773____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1287_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32239_GM;
MethodInfo m32239_MI = 
{
	"get_Current", NULL, &t4773_TI, &t1287_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32239_GM};
static MethodInfo* t4773_MIs[] =
{
	&m32239_MI,
	NULL
};
static TypeInfo* t4773_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4773_0_0_0;
extern Il2CppType t4773_1_0_0;
struct t4773;
extern Il2CppGenericClass t4773_GC;
TypeInfo t4773_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4773_MIs, t4773_PIs, NULL, NULL, NULL, NULL, NULL, &t4773_TI, t4773_ITIs, NULL, &EmptyCustomAttributesCache, &t4773_TI, &t4773_0_0_0, &t4773_1_0_0, NULL, &t4773_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3336.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3336_TI;
#include "t3336MD.h"

extern TypeInfo t1287_TI;
extern MethodInfo m18556_MI;
extern MethodInfo m24663_MI;
struct t20;
#define m24663(__this, p0, method) (t1287 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerDisplayAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3336_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3336_TI, offsetof(t3336, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3336_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3336_TI, offsetof(t3336, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3336_FIs[] =
{
	&t3336_f0_FieldInfo,
	&t3336_f1_FieldInfo,
	NULL
};
extern MethodInfo m18553_MI;
static PropertyInfo t3336____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3336_TI, "System.Collections.IEnumerator.Current", &m18553_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3336____Current_PropertyInfo = 
{
	&t3336_TI, "Current", &m18556_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3336_PIs[] =
{
	&t3336____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3336____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3336_m18552_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18552_GM;
MethodInfo m18552_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3336_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3336_m18552_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18552_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18553_GM;
MethodInfo m18553_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3336_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18553_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18554_GM;
MethodInfo m18554_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3336_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18554_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18555_GM;
MethodInfo m18555_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3336_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18555_GM};
extern Il2CppType t1287_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18556_GM;
MethodInfo m18556_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3336_TI, &t1287_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18556_GM};
static MethodInfo* t3336_MIs[] =
{
	&m18552_MI,
	&m18553_MI,
	&m18554_MI,
	&m18555_MI,
	&m18556_MI,
	NULL
};
extern MethodInfo m18555_MI;
extern MethodInfo m18554_MI;
static MethodInfo* t3336_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18553_MI,
	&m18555_MI,
	&m18554_MI,
	&m18556_MI,
};
static TypeInfo* t3336_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4773_TI,
};
static Il2CppInterfaceOffsetPair t3336_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4773_TI, 7},
};
extern TypeInfo t1287_TI;
static Il2CppRGCTXData t3336_RGCTXData[3] = 
{
	&m18556_MI/* Method Usage */,
	&t1287_TI/* Class Usage */,
	&m24663_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3336_0_0_0;
extern Il2CppType t3336_1_0_0;
extern Il2CppGenericClass t3336_GC;
TypeInfo t3336_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3336_MIs, t3336_PIs, t3336_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3336_TI, t3336_ITIs, t3336_VT, &EmptyCustomAttributesCache, &t3336_TI, &t3336_0_0_0, &t3336_1_0_0, t3336_IOs, &t3336_GC, NULL, NULL, NULL, t3336_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3336)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6201_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerDisplayAttribute>
extern MethodInfo m32240_MI;
static PropertyInfo t6201____Count_PropertyInfo = 
{
	&t6201_TI, "Count", &m32240_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32241_MI;
static PropertyInfo t6201____IsReadOnly_PropertyInfo = 
{
	&t6201_TI, "IsReadOnly", &m32241_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6201_PIs[] =
{
	&t6201____Count_PropertyInfo,
	&t6201____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32240_GM;
MethodInfo m32240_MI = 
{
	"get_Count", NULL, &t6201_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32240_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32241_GM;
MethodInfo m32241_MI = 
{
	"get_IsReadOnly", NULL, &t6201_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32241_GM};
extern Il2CppType t1287_0_0_0;
extern Il2CppType t1287_0_0_0;
static ParameterInfo t6201_m32242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1287_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32242_GM;
MethodInfo m32242_MI = 
{
	"Add", NULL, &t6201_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6201_m32242_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32242_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32243_GM;
MethodInfo m32243_MI = 
{
	"Clear", NULL, &t6201_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32243_GM};
extern Il2CppType t1287_0_0_0;
static ParameterInfo t6201_m32244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1287_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32244_GM;
MethodInfo m32244_MI = 
{
	"Contains", NULL, &t6201_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6201_m32244_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32244_GM};
extern Il2CppType t3603_0_0_0;
extern Il2CppType t3603_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6201_m32245_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3603_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32245_GM;
MethodInfo m32245_MI = 
{
	"CopyTo", NULL, &t6201_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6201_m32245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32245_GM};
extern Il2CppType t1287_0_0_0;
static ParameterInfo t6201_m32246_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1287_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32246_GM;
MethodInfo m32246_MI = 
{
	"Remove", NULL, &t6201_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6201_m32246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32246_GM};
static MethodInfo* t6201_MIs[] =
{
	&m32240_MI,
	&m32241_MI,
	&m32242_MI,
	&m32243_MI,
	&m32244_MI,
	&m32245_MI,
	&m32246_MI,
	NULL
};
extern TypeInfo t6203_TI;
static TypeInfo* t6201_ITIs[] = 
{
	&t603_TI,
	&t6203_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6201_0_0_0;
extern Il2CppType t6201_1_0_0;
struct t6201;
extern Il2CppGenericClass t6201_GC;
TypeInfo t6201_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6201_MIs, t6201_PIs, NULL, NULL, NULL, NULL, NULL, &t6201_TI, t6201_ITIs, NULL, &EmptyCustomAttributesCache, &t6201_TI, &t6201_0_0_0, &t6201_1_0_0, NULL, &t6201_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerDisplayAttribute>
extern Il2CppType t4773_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32247_GM;
MethodInfo m32247_MI = 
{
	"GetEnumerator", NULL, &t6203_TI, &t4773_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32247_GM};
static MethodInfo* t6203_MIs[] =
{
	&m32247_MI,
	NULL
};
static TypeInfo* t6203_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6203_0_0_0;
extern Il2CppType t6203_1_0_0;
struct t6203;
extern Il2CppGenericClass t6203_GC;
TypeInfo t6203_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6203_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6203_TI, t6203_ITIs, NULL, &EmptyCustomAttributesCache, &t6203_TI, &t6203_0_0_0, &t6203_1_0_0, NULL, &t6203_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6202_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggerDisplayAttribute>
extern MethodInfo m32248_MI;
extern MethodInfo m32249_MI;
static PropertyInfo t6202____Item_PropertyInfo = 
{
	&t6202_TI, "Item", &m32248_MI, &m32249_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6202_PIs[] =
{
	&t6202____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1287_0_0_0;
static ParameterInfo t6202_m32250_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1287_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32250_GM;
MethodInfo m32250_MI = 
{
	"IndexOf", NULL, &t6202_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6202_m32250_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32250_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1287_0_0_0;
static ParameterInfo t6202_m32251_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1287_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32251_GM;
MethodInfo m32251_MI = 
{
	"Insert", NULL, &t6202_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6202_m32251_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32251_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6202_m32252_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32252_GM;
MethodInfo m32252_MI = 
{
	"RemoveAt", NULL, &t6202_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6202_m32252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32252_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6202_m32248_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1287_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32248_GM;
MethodInfo m32248_MI = 
{
	"get_Item", NULL, &t6202_TI, &t1287_0_0_0, RuntimeInvoker_t29_t44, t6202_m32248_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32248_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1287_0_0_0;
static ParameterInfo t6202_m32249_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1287_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32249_GM;
MethodInfo m32249_MI = 
{
	"set_Item", NULL, &t6202_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6202_m32249_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32249_GM};
static MethodInfo* t6202_MIs[] =
{
	&m32250_MI,
	&m32251_MI,
	&m32252_MI,
	&m32248_MI,
	&m32249_MI,
	NULL
};
static TypeInfo* t6202_ITIs[] = 
{
	&t603_TI,
	&t6201_TI,
	&t6203_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6202_0_0_0;
extern Il2CppType t6202_1_0_0;
struct t6202;
extern Il2CppGenericClass t6202_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6202_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6202_MIs, t6202_PIs, NULL, NULL, NULL, NULL, NULL, &t6202_TI, t6202_ITIs, NULL, &t1908__CustomAttributeCache, &t6202_TI, &t6202_0_0_0, &t6202_1_0_0, NULL, &t6202_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4775_TI;

#include "t1288.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>
extern MethodInfo m32253_MI;
static PropertyInfo t4775____Current_PropertyInfo = 
{
	&t4775_TI, "Current", &m32253_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4775_PIs[] =
{
	&t4775____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1288_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32253_GM;
MethodInfo m32253_MI = 
{
	"get_Current", NULL, &t4775_TI, &t1288_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32253_GM};
static MethodInfo* t4775_MIs[] =
{
	&m32253_MI,
	NULL
};
static TypeInfo* t4775_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4775_0_0_0;
extern Il2CppType t4775_1_0_0;
struct t4775;
extern Il2CppGenericClass t4775_GC;
TypeInfo t4775_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4775_MIs, t4775_PIs, NULL, NULL, NULL, NULL, NULL, &t4775_TI, t4775_ITIs, NULL, &EmptyCustomAttributesCache, &t4775_TI, &t4775_0_0_0, &t4775_1_0_0, NULL, &t4775_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3337.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3337_TI;
#include "t3337MD.h"

extern TypeInfo t1288_TI;
extern MethodInfo m18561_MI;
extern MethodInfo m24674_MI;
struct t20;
#define m24674(__this, p0, method) (t1288 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3337_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3337_TI, offsetof(t3337, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3337_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3337_TI, offsetof(t3337, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3337_FIs[] =
{
	&t3337_f0_FieldInfo,
	&t3337_f1_FieldInfo,
	NULL
};
extern MethodInfo m18558_MI;
static PropertyInfo t3337____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3337_TI, "System.Collections.IEnumerator.Current", &m18558_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3337____Current_PropertyInfo = 
{
	&t3337_TI, "Current", &m18561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3337_PIs[] =
{
	&t3337____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3337____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3337_m18557_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18557_GM;
MethodInfo m18557_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3337_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3337_m18557_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18557_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18558_GM;
MethodInfo m18558_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3337_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18558_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18559_GM;
MethodInfo m18559_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3337_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18559_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18560_GM;
MethodInfo m18560_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3337_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18560_GM};
extern Il2CppType t1288_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18561_GM;
MethodInfo m18561_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3337_TI, &t1288_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18561_GM};
static MethodInfo* t3337_MIs[] =
{
	&m18557_MI,
	&m18558_MI,
	&m18559_MI,
	&m18560_MI,
	&m18561_MI,
	NULL
};
extern MethodInfo m18560_MI;
extern MethodInfo m18559_MI;
static MethodInfo* t3337_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18558_MI,
	&m18560_MI,
	&m18559_MI,
	&m18561_MI,
};
static TypeInfo* t3337_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4775_TI,
};
static Il2CppInterfaceOffsetPair t3337_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4775_TI, 7},
};
extern TypeInfo t1288_TI;
static Il2CppRGCTXData t3337_RGCTXData[3] = 
{
	&m18561_MI/* Method Usage */,
	&t1288_TI/* Class Usage */,
	&m24674_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3337_0_0_0;
extern Il2CppType t3337_1_0_0;
extern Il2CppGenericClass t3337_GC;
TypeInfo t3337_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3337_MIs, t3337_PIs, t3337_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3337_TI, t3337_ITIs, t3337_VT, &EmptyCustomAttributesCache, &t3337_TI, &t3337_0_0_0, &t3337_1_0_0, t3337_IOs, &t3337_GC, NULL, NULL, NULL, t3337_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3337)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6204_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerStepThroughAttribute>
extern MethodInfo m32254_MI;
static PropertyInfo t6204____Count_PropertyInfo = 
{
	&t6204_TI, "Count", &m32254_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32255_MI;
static PropertyInfo t6204____IsReadOnly_PropertyInfo = 
{
	&t6204_TI, "IsReadOnly", &m32255_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6204_PIs[] =
{
	&t6204____Count_PropertyInfo,
	&t6204____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32254_GM;
MethodInfo m32254_MI = 
{
	"get_Count", NULL, &t6204_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32254_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32255_GM;
MethodInfo m32255_MI = 
{
	"get_IsReadOnly", NULL, &t6204_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32255_GM};
extern Il2CppType t1288_0_0_0;
extern Il2CppType t1288_0_0_0;
static ParameterInfo t6204_m32256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1288_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32256_GM;
MethodInfo m32256_MI = 
{
	"Add", NULL, &t6204_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6204_m32256_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32256_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32257_GM;
MethodInfo m32257_MI = 
{
	"Clear", NULL, &t6204_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32257_GM};
extern Il2CppType t1288_0_0_0;
static ParameterInfo t6204_m32258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1288_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32258_GM;
MethodInfo m32258_MI = 
{
	"Contains", NULL, &t6204_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6204_m32258_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32258_GM};
extern Il2CppType t3604_0_0_0;
extern Il2CppType t3604_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6204_m32259_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3604_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32259_GM;
MethodInfo m32259_MI = 
{
	"CopyTo", NULL, &t6204_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6204_m32259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32259_GM};
extern Il2CppType t1288_0_0_0;
static ParameterInfo t6204_m32260_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1288_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32260_GM;
MethodInfo m32260_MI = 
{
	"Remove", NULL, &t6204_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6204_m32260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32260_GM};
static MethodInfo* t6204_MIs[] =
{
	&m32254_MI,
	&m32255_MI,
	&m32256_MI,
	&m32257_MI,
	&m32258_MI,
	&m32259_MI,
	&m32260_MI,
	NULL
};
extern TypeInfo t6206_TI;
static TypeInfo* t6204_ITIs[] = 
{
	&t603_TI,
	&t6206_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6204_0_0_0;
extern Il2CppType t6204_1_0_0;
struct t6204;
extern Il2CppGenericClass t6204_GC;
TypeInfo t6204_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6204_MIs, t6204_PIs, NULL, NULL, NULL, NULL, NULL, &t6204_TI, t6204_ITIs, NULL, &EmptyCustomAttributesCache, &t6204_TI, &t6204_0_0_0, &t6204_1_0_0, NULL, &t6204_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerStepThroughAttribute>
extern Il2CppType t4775_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32261_GM;
MethodInfo m32261_MI = 
{
	"GetEnumerator", NULL, &t6206_TI, &t4775_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32261_GM};
static MethodInfo* t6206_MIs[] =
{
	&m32261_MI,
	NULL
};
static TypeInfo* t6206_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6206_0_0_0;
extern Il2CppType t6206_1_0_0;
struct t6206;
extern Il2CppGenericClass t6206_GC;
TypeInfo t6206_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6206_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6206_TI, t6206_ITIs, NULL, &EmptyCustomAttributesCache, &t6206_TI, &t6206_0_0_0, &t6206_1_0_0, NULL, &t6206_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6205_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggerStepThroughAttribute>
extern MethodInfo m32262_MI;
extern MethodInfo m32263_MI;
static PropertyInfo t6205____Item_PropertyInfo = 
{
	&t6205_TI, "Item", &m32262_MI, &m32263_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6205_PIs[] =
{
	&t6205____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1288_0_0_0;
static ParameterInfo t6205_m32264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1288_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32264_GM;
MethodInfo m32264_MI = 
{
	"IndexOf", NULL, &t6205_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6205_m32264_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32264_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1288_0_0_0;
static ParameterInfo t6205_m32265_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1288_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32265_GM;
MethodInfo m32265_MI = 
{
	"Insert", NULL, &t6205_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6205_m32265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32265_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6205_m32266_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32266_GM;
MethodInfo m32266_MI = 
{
	"RemoveAt", NULL, &t6205_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6205_m32266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32266_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6205_m32262_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1288_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32262_GM;
MethodInfo m32262_MI = 
{
	"get_Item", NULL, &t6205_TI, &t1288_0_0_0, RuntimeInvoker_t29_t44, t6205_m32262_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32262_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1288_0_0_0;
static ParameterInfo t6205_m32263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1288_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32263_GM;
MethodInfo m32263_MI = 
{
	"set_Item", NULL, &t6205_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6205_m32263_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32263_GM};
static MethodInfo* t6205_MIs[] =
{
	&m32264_MI,
	&m32265_MI,
	&m32266_MI,
	&m32262_MI,
	&m32263_MI,
	NULL
};
static TypeInfo* t6205_ITIs[] = 
{
	&t603_TI,
	&t6204_TI,
	&t6206_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6205_0_0_0;
extern Il2CppType t6205_1_0_0;
struct t6205;
extern Il2CppGenericClass t6205_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6205_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6205_MIs, t6205_PIs, NULL, NULL, NULL, NULL, NULL, &t6205_TI, t6205_ITIs, NULL, &t1908__CustomAttributeCache, &t6205_TI, &t6205_0_0_0, &t6205_1_0_0, NULL, &t6205_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4777_TI;

#include "t1289.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern MethodInfo m32267_MI;
static PropertyInfo t4777____Current_PropertyInfo = 
{
	&t4777_TI, "Current", &m32267_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4777_PIs[] =
{
	&t4777____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1289_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32267_GM;
MethodInfo m32267_MI = 
{
	"get_Current", NULL, &t4777_TI, &t1289_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32267_GM};
static MethodInfo* t4777_MIs[] =
{
	&m32267_MI,
	NULL
};
static TypeInfo* t4777_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4777_0_0_0;
extern Il2CppType t4777_1_0_0;
struct t4777;
extern Il2CppGenericClass t4777_GC;
TypeInfo t4777_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4777_MIs, t4777_PIs, NULL, NULL, NULL, NULL, NULL, &t4777_TI, t4777_ITIs, NULL, &EmptyCustomAttributesCache, &t4777_TI, &t4777_0_0_0, &t4777_1_0_0, NULL, &t4777_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3338.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3338_TI;
#include "t3338MD.h"

extern TypeInfo t1289_TI;
extern MethodInfo m18566_MI;
extern MethodInfo m24685_MI;
struct t20;
#define m24685(__this, p0, method) (t1289 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3338_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3338_TI, offsetof(t3338, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3338_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3338_TI, offsetof(t3338, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3338_FIs[] =
{
	&t3338_f0_FieldInfo,
	&t3338_f1_FieldInfo,
	NULL
};
extern MethodInfo m18563_MI;
static PropertyInfo t3338____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3338_TI, "System.Collections.IEnumerator.Current", &m18563_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3338____Current_PropertyInfo = 
{
	&t3338_TI, "Current", &m18566_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3338_PIs[] =
{
	&t3338____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3338____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3338_m18562_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18562_GM;
MethodInfo m18562_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3338_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3338_m18562_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18562_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18563_GM;
MethodInfo m18563_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3338_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18563_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18564_GM;
MethodInfo m18564_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3338_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18564_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18565_GM;
MethodInfo m18565_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3338_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18565_GM};
extern Il2CppType t1289_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18566_GM;
MethodInfo m18566_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3338_TI, &t1289_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18566_GM};
static MethodInfo* t3338_MIs[] =
{
	&m18562_MI,
	&m18563_MI,
	&m18564_MI,
	&m18565_MI,
	&m18566_MI,
	NULL
};
extern MethodInfo m18565_MI;
extern MethodInfo m18564_MI;
static MethodInfo* t3338_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18563_MI,
	&m18565_MI,
	&m18564_MI,
	&m18566_MI,
};
static TypeInfo* t3338_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4777_TI,
};
static Il2CppInterfaceOffsetPair t3338_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4777_TI, 7},
};
extern TypeInfo t1289_TI;
static Il2CppRGCTXData t3338_RGCTXData[3] = 
{
	&m18566_MI/* Method Usage */,
	&t1289_TI/* Class Usage */,
	&m24685_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3338_0_0_0;
extern Il2CppType t3338_1_0_0;
extern Il2CppGenericClass t3338_GC;
TypeInfo t3338_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3338_MIs, t3338_PIs, t3338_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3338_TI, t3338_ITIs, t3338_VT, &EmptyCustomAttributesCache, &t3338_TI, &t3338_0_0_0, &t3338_1_0_0, t3338_IOs, &t3338_GC, NULL, NULL, NULL, t3338_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3338)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6207_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern MethodInfo m32268_MI;
static PropertyInfo t6207____Count_PropertyInfo = 
{
	&t6207_TI, "Count", &m32268_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32269_MI;
static PropertyInfo t6207____IsReadOnly_PropertyInfo = 
{
	&t6207_TI, "IsReadOnly", &m32269_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6207_PIs[] =
{
	&t6207____Count_PropertyInfo,
	&t6207____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32268_GM;
MethodInfo m32268_MI = 
{
	"get_Count", NULL, &t6207_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32268_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32269_GM;
MethodInfo m32269_MI = 
{
	"get_IsReadOnly", NULL, &t6207_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32269_GM};
extern Il2CppType t1289_0_0_0;
extern Il2CppType t1289_0_0_0;
static ParameterInfo t6207_m32270_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1289_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32270_GM;
MethodInfo m32270_MI = 
{
	"Add", NULL, &t6207_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6207_m32270_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32270_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32271_GM;
MethodInfo m32271_MI = 
{
	"Clear", NULL, &t6207_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32271_GM};
extern Il2CppType t1289_0_0_0;
static ParameterInfo t6207_m32272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1289_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32272_GM;
MethodInfo m32272_MI = 
{
	"Contains", NULL, &t6207_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6207_m32272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32272_GM};
extern Il2CppType t3605_0_0_0;
extern Il2CppType t3605_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6207_m32273_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3605_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32273_GM;
MethodInfo m32273_MI = 
{
	"CopyTo", NULL, &t6207_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6207_m32273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32273_GM};
extern Il2CppType t1289_0_0_0;
static ParameterInfo t6207_m32274_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1289_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32274_GM;
MethodInfo m32274_MI = 
{
	"Remove", NULL, &t6207_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6207_m32274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32274_GM};
static MethodInfo* t6207_MIs[] =
{
	&m32268_MI,
	&m32269_MI,
	&m32270_MI,
	&m32271_MI,
	&m32272_MI,
	&m32273_MI,
	&m32274_MI,
	NULL
};
extern TypeInfo t6209_TI;
static TypeInfo* t6207_ITIs[] = 
{
	&t603_TI,
	&t6209_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6207_0_0_0;
extern Il2CppType t6207_1_0_0;
struct t6207;
extern Il2CppGenericClass t6207_GC;
TypeInfo t6207_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6207_MIs, t6207_PIs, NULL, NULL, NULL, NULL, NULL, &t6207_TI, t6207_ITIs, NULL, &EmptyCustomAttributesCache, &t6207_TI, &t6207_0_0_0, &t6207_1_0_0, NULL, &t6207_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern Il2CppType t4777_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32275_GM;
MethodInfo m32275_MI = 
{
	"GetEnumerator", NULL, &t6209_TI, &t4777_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32275_GM};
static MethodInfo* t6209_MIs[] =
{
	&m32275_MI,
	NULL
};
static TypeInfo* t6209_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6209_0_0_0;
extern Il2CppType t6209_1_0_0;
struct t6209;
extern Il2CppGenericClass t6209_GC;
TypeInfo t6209_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6209_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6209_TI, t6209_ITIs, NULL, &EmptyCustomAttributesCache, &t6209_TI, &t6209_0_0_0, &t6209_1_0_0, NULL, &t6209_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6208_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern MethodInfo m32276_MI;
extern MethodInfo m32277_MI;
static PropertyInfo t6208____Item_PropertyInfo = 
{
	&t6208_TI, "Item", &m32276_MI, &m32277_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6208_PIs[] =
{
	&t6208____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1289_0_0_0;
static ParameterInfo t6208_m32278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1289_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32278_GM;
MethodInfo m32278_MI = 
{
	"IndexOf", NULL, &t6208_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6208_m32278_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32278_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1289_0_0_0;
static ParameterInfo t6208_m32279_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1289_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32279_GM;
MethodInfo m32279_MI = 
{
	"Insert", NULL, &t6208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6208_m32279_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32279_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6208_m32280_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32280_GM;
MethodInfo m32280_MI = 
{
	"RemoveAt", NULL, &t6208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6208_m32280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32280_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6208_m32276_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1289_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32276_GM;
MethodInfo m32276_MI = 
{
	"get_Item", NULL, &t6208_TI, &t1289_0_0_0, RuntimeInvoker_t29_t44, t6208_m32276_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32276_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1289_0_0_0;
static ParameterInfo t6208_m32277_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1289_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32277_GM;
MethodInfo m32277_MI = 
{
	"set_Item", NULL, &t6208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6208_m32277_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32277_GM};
static MethodInfo* t6208_MIs[] =
{
	&m32278_MI,
	&m32279_MI,
	&m32280_MI,
	&m32276_MI,
	&m32277_MI,
	NULL
};
static TypeInfo* t6208_ITIs[] = 
{
	&t603_TI,
	&t6207_TI,
	&t6209_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6208_0_0_0;
extern Il2CppType t6208_1_0_0;
struct t6208;
extern Il2CppGenericClass t6208_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6208_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6208_MIs, t6208_PIs, NULL, NULL, NULL, NULL, NULL, &t6208_TI, t6208_ITIs, NULL, &t1908__CustomAttributeCache, &t6208_TI, &t6208_0_0_0, &t6208_1_0_0, NULL, &t6208_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4779_TI;

#include "t635.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.StackFrame>
extern MethodInfo m32281_MI;
static PropertyInfo t4779____Current_PropertyInfo = 
{
	&t4779_TI, "Current", &m32281_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4779_PIs[] =
{
	&t4779____Current_PropertyInfo,
	NULL
};
extern Il2CppType t635_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32281_GM;
MethodInfo m32281_MI = 
{
	"get_Current", NULL, &t4779_TI, &t635_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32281_GM};
static MethodInfo* t4779_MIs[] =
{
	&m32281_MI,
	NULL
};
static TypeInfo* t4779_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4779_0_0_0;
extern Il2CppType t4779_1_0_0;
struct t4779;
extern Il2CppGenericClass t4779_GC;
TypeInfo t4779_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4779_MIs, t4779_PIs, NULL, NULL, NULL, NULL, NULL, &t4779_TI, t4779_ITIs, NULL, &EmptyCustomAttributesCache, &t4779_TI, &t4779_0_0_0, &t4779_1_0_0, NULL, &t4779_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3339.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3339_TI;
#include "t3339MD.h"

extern TypeInfo t635_TI;
extern MethodInfo m18571_MI;
extern MethodInfo m24696_MI;
struct t20;
#define m24696(__this, p0, method) (t635 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>
extern Il2CppType t20_0_0_1;
FieldInfo t3339_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3339_TI, offsetof(t3339, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3339_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3339_TI, offsetof(t3339, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3339_FIs[] =
{
	&t3339_f0_FieldInfo,
	&t3339_f1_FieldInfo,
	NULL
};
extern MethodInfo m18568_MI;
static PropertyInfo t3339____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3339_TI, "System.Collections.IEnumerator.Current", &m18568_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3339____Current_PropertyInfo = 
{
	&t3339_TI, "Current", &m18571_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3339_PIs[] =
{
	&t3339____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3339____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3339_m18567_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18567_GM;
MethodInfo m18567_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3339_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3339_m18567_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18567_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18568_GM;
MethodInfo m18568_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3339_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18568_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18569_GM;
MethodInfo m18569_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3339_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18569_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18570_GM;
MethodInfo m18570_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3339_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18570_GM};
extern Il2CppType t635_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18571_GM;
MethodInfo m18571_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3339_TI, &t635_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18571_GM};
static MethodInfo* t3339_MIs[] =
{
	&m18567_MI,
	&m18568_MI,
	&m18569_MI,
	&m18570_MI,
	&m18571_MI,
	NULL
};
extern MethodInfo m18570_MI;
extern MethodInfo m18569_MI;
static MethodInfo* t3339_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18568_MI,
	&m18570_MI,
	&m18569_MI,
	&m18571_MI,
};
static TypeInfo* t3339_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4779_TI,
};
static Il2CppInterfaceOffsetPair t3339_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4779_TI, 7},
};
extern TypeInfo t635_TI;
static Il2CppRGCTXData t3339_RGCTXData[3] = 
{
	&m18571_MI/* Method Usage */,
	&t635_TI/* Class Usage */,
	&m24696_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3339_0_0_0;
extern Il2CppType t3339_1_0_0;
extern Il2CppGenericClass t3339_GC;
TypeInfo t3339_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3339_MIs, t3339_PIs, t3339_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3339_TI, t3339_ITIs, t3339_VT, &EmptyCustomAttributesCache, &t3339_TI, &t3339_0_0_0, &t3339_1_0_0, t3339_IOs, &t3339_GC, NULL, NULL, NULL, t3339_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3339)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6210_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>
extern MethodInfo m32282_MI;
static PropertyInfo t6210____Count_PropertyInfo = 
{
	&t6210_TI, "Count", &m32282_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32283_MI;
static PropertyInfo t6210____IsReadOnly_PropertyInfo = 
{
	&t6210_TI, "IsReadOnly", &m32283_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6210_PIs[] =
{
	&t6210____Count_PropertyInfo,
	&t6210____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32282_GM;
MethodInfo m32282_MI = 
{
	"get_Count", NULL, &t6210_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32282_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32283_GM;
MethodInfo m32283_MI = 
{
	"get_IsReadOnly", NULL, &t6210_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32283_GM};
extern Il2CppType t635_0_0_0;
extern Il2CppType t635_0_0_0;
static ParameterInfo t6210_m32284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t635_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32284_GM;
MethodInfo m32284_MI = 
{
	"Add", NULL, &t6210_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6210_m32284_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32284_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32285_GM;
MethodInfo m32285_MI = 
{
	"Clear", NULL, &t6210_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32285_GM};
extern Il2CppType t635_0_0_0;
static ParameterInfo t6210_m32286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t635_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32286_GM;
MethodInfo m32286_MI = 
{
	"Contains", NULL, &t6210_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6210_m32286_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32286_GM};
extern Il2CppType t1290_0_0_0;
extern Il2CppType t1290_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6210_m32287_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1290_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32287_GM;
MethodInfo m32287_MI = 
{
	"CopyTo", NULL, &t6210_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6210_m32287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32287_GM};
extern Il2CppType t635_0_0_0;
static ParameterInfo t6210_m32288_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t635_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32288_GM;
MethodInfo m32288_MI = 
{
	"Remove", NULL, &t6210_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6210_m32288_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32288_GM};
static MethodInfo* t6210_MIs[] =
{
	&m32282_MI,
	&m32283_MI,
	&m32284_MI,
	&m32285_MI,
	&m32286_MI,
	&m32287_MI,
	&m32288_MI,
	NULL
};
extern TypeInfo t6212_TI;
static TypeInfo* t6210_ITIs[] = 
{
	&t603_TI,
	&t6212_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6210_0_0_0;
extern Il2CppType t6210_1_0_0;
struct t6210;
extern Il2CppGenericClass t6210_GC;
TypeInfo t6210_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6210_MIs, t6210_PIs, NULL, NULL, NULL, NULL, NULL, &t6210_TI, t6210_ITIs, NULL, &EmptyCustomAttributesCache, &t6210_TI, &t6210_0_0_0, &t6210_1_0_0, NULL, &t6210_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>
extern Il2CppType t4779_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32289_GM;
MethodInfo m32289_MI = 
{
	"GetEnumerator", NULL, &t6212_TI, &t4779_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32289_GM};
static MethodInfo* t6212_MIs[] =
{
	&m32289_MI,
	NULL
};
static TypeInfo* t6212_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6212_0_0_0;
extern Il2CppType t6212_1_0_0;
struct t6212;
extern Il2CppGenericClass t6212_GC;
TypeInfo t6212_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6212_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6212_TI, t6212_ITIs, NULL, &EmptyCustomAttributesCache, &t6212_TI, &t6212_0_0_0, &t6212_1_0_0, NULL, &t6212_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6211_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>
extern MethodInfo m32290_MI;
extern MethodInfo m32291_MI;
static PropertyInfo t6211____Item_PropertyInfo = 
{
	&t6211_TI, "Item", &m32290_MI, &m32291_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6211_PIs[] =
{
	&t6211____Item_PropertyInfo,
	NULL
};
extern Il2CppType t635_0_0_0;
static ParameterInfo t6211_m32292_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t635_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32292_GM;
MethodInfo m32292_MI = 
{
	"IndexOf", NULL, &t6211_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6211_m32292_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32292_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t635_0_0_0;
static ParameterInfo t6211_m32293_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t635_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32293_GM;
MethodInfo m32293_MI = 
{
	"Insert", NULL, &t6211_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6211_m32293_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32293_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6211_m32294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32294_GM;
MethodInfo m32294_MI = 
{
	"RemoveAt", NULL, &t6211_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6211_m32294_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32294_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6211_m32290_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t635_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32290_GM;
MethodInfo m32290_MI = 
{
	"get_Item", NULL, &t6211_TI, &t635_0_0_0, RuntimeInvoker_t29_t44, t6211_m32290_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32290_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t635_0_0_0;
static ParameterInfo t6211_m32291_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t635_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32291_GM;
MethodInfo m32291_MI = 
{
	"set_Item", NULL, &t6211_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6211_m32291_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32291_GM};
static MethodInfo* t6211_MIs[] =
{
	&m32292_MI,
	&m32293_MI,
	&m32294_MI,
	&m32290_MI,
	&m32291_MI,
	NULL
};
static TypeInfo* t6211_ITIs[] = 
{
	&t603_TI,
	&t6210_TI,
	&t6212_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6211_0_0_0;
extern Il2CppType t6211_1_0_0;
struct t6211;
extern Il2CppGenericClass t6211_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6211_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6211_MIs, t6211_PIs, NULL, NULL, NULL, NULL, NULL, &t6211_TI, t6211_ITIs, NULL, &t1908__CustomAttributeCache, &t6211_TI, &t6211_0_0_0, &t6211_1_0_0, NULL, &t6211_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4781_TI;

#include "t1120.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>
extern MethodInfo m32295_MI;
static PropertyInfo t4781____Current_PropertyInfo = 
{
	&t4781_TI, "Current", &m32295_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4781_PIs[] =
{
	&t4781____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1120_0_0_0;
extern void* RuntimeInvoker_t1120 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32295_GM;
MethodInfo m32295_MI = 
{
	"get_Current", NULL, &t4781_TI, &t1120_0_0_0, RuntimeInvoker_t1120, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32295_GM};
static MethodInfo* t4781_MIs[] =
{
	&m32295_MI,
	NULL
};
static TypeInfo* t4781_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4781_0_0_0;
extern Il2CppType t4781_1_0_0;
struct t4781;
extern Il2CppGenericClass t4781_GC;
TypeInfo t4781_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4781_MIs, t4781_PIs, NULL, NULL, NULL, NULL, NULL, &t4781_TI, t4781_ITIs, NULL, &EmptyCustomAttributesCache, &t4781_TI, &t4781_0_0_0, &t4781_1_0_0, NULL, &t4781_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3340.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3340_TI;
#include "t3340MD.h"

extern TypeInfo t1120_TI;
extern MethodInfo m18576_MI;
extern MethodInfo m24707_MI;
struct t20;
 int32_t m24707 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18572_MI;
 void m18572 (t3340 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18573_MI;
 t29 * m18573 (t3340 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18576(__this, &m18576_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1120_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18574_MI;
 void m18574 (t3340 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18575_MI;
 bool m18575 (t3340 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18576 (t3340 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24707(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24707_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
extern Il2CppType t20_0_0_1;
FieldInfo t3340_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3340_TI, offsetof(t3340, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3340_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3340_TI, offsetof(t3340, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3340_FIs[] =
{
	&t3340_f0_FieldInfo,
	&t3340_f1_FieldInfo,
	NULL
};
static PropertyInfo t3340____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3340_TI, "System.Collections.IEnumerator.Current", &m18573_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3340____Current_PropertyInfo = 
{
	&t3340_TI, "Current", &m18576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3340_PIs[] =
{
	&t3340____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3340____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3340_m18572_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18572_GM;
MethodInfo m18572_MI = 
{
	".ctor", (methodPointerType)&m18572, &t3340_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3340_m18572_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18572_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18573_GM;
MethodInfo m18573_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18573, &t3340_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18573_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18574_GM;
MethodInfo m18574_MI = 
{
	"Dispose", (methodPointerType)&m18574, &t3340_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18574_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18575_GM;
MethodInfo m18575_MI = 
{
	"MoveNext", (methodPointerType)&m18575, &t3340_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18575_GM};
extern Il2CppType t1120_0_0_0;
extern void* RuntimeInvoker_t1120 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18576_GM;
MethodInfo m18576_MI = 
{
	"get_Current", (methodPointerType)&m18576, &t3340_TI, &t1120_0_0_0, RuntimeInvoker_t1120, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18576_GM};
static MethodInfo* t3340_MIs[] =
{
	&m18572_MI,
	&m18573_MI,
	&m18574_MI,
	&m18575_MI,
	&m18576_MI,
	NULL
};
static MethodInfo* t3340_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18573_MI,
	&m18575_MI,
	&m18574_MI,
	&m18576_MI,
};
static TypeInfo* t3340_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4781_TI,
};
static Il2CppInterfaceOffsetPair t3340_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4781_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3340_0_0_0;
extern Il2CppType t3340_1_0_0;
extern Il2CppGenericClass t3340_GC;
TypeInfo t3340_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3340_MIs, t3340_PIs, t3340_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3340_TI, t3340_ITIs, t3340_VT, &EmptyCustomAttributesCache, &t3340_TI, &t3340_0_0_0, &t3340_1_0_0, t3340_IOs, &t3340_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3340)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6213_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>
extern MethodInfo m32296_MI;
static PropertyInfo t6213____Count_PropertyInfo = 
{
	&t6213_TI, "Count", &m32296_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32297_MI;
static PropertyInfo t6213____IsReadOnly_PropertyInfo = 
{
	&t6213_TI, "IsReadOnly", &m32297_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6213_PIs[] =
{
	&t6213____Count_PropertyInfo,
	&t6213____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32296_GM;
MethodInfo m32296_MI = 
{
	"get_Count", NULL, &t6213_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32296_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32297_GM;
MethodInfo m32297_MI = 
{
	"get_IsReadOnly", NULL, &t6213_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32297_GM};
extern Il2CppType t1120_0_0_0;
extern Il2CppType t1120_0_0_0;
static ParameterInfo t6213_m32298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32298_GM;
MethodInfo m32298_MI = 
{
	"Add", NULL, &t6213_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6213_m32298_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32298_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32299_GM;
MethodInfo m32299_MI = 
{
	"Clear", NULL, &t6213_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32299_GM};
extern Il2CppType t1120_0_0_0;
static ParameterInfo t6213_m32300_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1120_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32300_GM;
MethodInfo m32300_MI = 
{
	"Contains", NULL, &t6213_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6213_m32300_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32300_GM};
extern Il2CppType t3606_0_0_0;
extern Il2CppType t3606_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6213_m32301_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3606_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32301_GM;
MethodInfo m32301_MI = 
{
	"CopyTo", NULL, &t6213_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6213_m32301_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32301_GM};
extern Il2CppType t1120_0_0_0;
static ParameterInfo t6213_m32302_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1120_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32302_GM;
MethodInfo m32302_MI = 
{
	"Remove", NULL, &t6213_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6213_m32302_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32302_GM};
static MethodInfo* t6213_MIs[] =
{
	&m32296_MI,
	&m32297_MI,
	&m32298_MI,
	&m32299_MI,
	&m32300_MI,
	&m32301_MI,
	&m32302_MI,
	NULL
};
extern TypeInfo t6215_TI;
static TypeInfo* t6213_ITIs[] = 
{
	&t603_TI,
	&t6215_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6213_0_0_0;
extern Il2CppType t6213_1_0_0;
struct t6213;
extern Il2CppGenericClass t6213_GC;
TypeInfo t6213_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6213_MIs, t6213_PIs, NULL, NULL, NULL, NULL, NULL, &t6213_TI, t6213_ITIs, NULL, &EmptyCustomAttributesCache, &t6213_TI, &t6213_0_0_0, &t6213_1_0_0, NULL, &t6213_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>
extern Il2CppType t4781_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32303_GM;
MethodInfo m32303_MI = 
{
	"GetEnumerator", NULL, &t6215_TI, &t4781_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32303_GM};
static MethodInfo* t6215_MIs[] =
{
	&m32303_MI,
	NULL
};
static TypeInfo* t6215_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6215_0_0_0;
extern Il2CppType t6215_1_0_0;
struct t6215;
extern Il2CppGenericClass t6215_GC;
TypeInfo t6215_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6215_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6215_TI, t6215_ITIs, NULL, &EmptyCustomAttributesCache, &t6215_TI, &t6215_0_0_0, &t6215_1_0_0, NULL, &t6215_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6214_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.CompareOptions>
extern MethodInfo m32304_MI;
extern MethodInfo m32305_MI;
static PropertyInfo t6214____Item_PropertyInfo = 
{
	&t6214_TI, "Item", &m32304_MI, &m32305_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6214_PIs[] =
{
	&t6214____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1120_0_0_0;
static ParameterInfo t6214_m32306_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1120_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32306_GM;
MethodInfo m32306_MI = 
{
	"IndexOf", NULL, &t6214_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6214_m32306_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32306_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1120_0_0_0;
static ParameterInfo t6214_m32307_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32307_GM;
MethodInfo m32307_MI = 
{
	"Insert", NULL, &t6214_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6214_m32307_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32307_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6214_m32308_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32308_GM;
MethodInfo m32308_MI = 
{
	"RemoveAt", NULL, &t6214_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6214_m32308_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32308_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6214_m32304_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1120_0_0_0;
extern void* RuntimeInvoker_t1120_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32304_GM;
MethodInfo m32304_MI = 
{
	"get_Item", NULL, &t6214_TI, &t1120_0_0_0, RuntimeInvoker_t1120_t44, t6214_m32304_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32304_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1120_0_0_0;
static ParameterInfo t6214_m32305_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32305_GM;
MethodInfo m32305_MI = 
{
	"set_Item", NULL, &t6214_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6214_m32305_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32305_GM};
static MethodInfo* t6214_MIs[] =
{
	&m32306_MI,
	&m32307_MI,
	&m32308_MI,
	&m32304_MI,
	&m32305_MI,
	NULL
};
static TypeInfo* t6214_ITIs[] = 
{
	&t603_TI,
	&t6213_TI,
	&t6215_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6214_0_0_0;
extern Il2CppType t6214_1_0_0;
struct t6214;
extern Il2CppGenericClass t6214_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6214_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6214_MIs, t6214_PIs, NULL, NULL, NULL, NULL, NULL, &t6214_TI, t6214_ITIs, NULL, &t1908__CustomAttributeCache, &t6214_TI, &t6214_0_0_0, &t6214_1_0_0, NULL, &t6214_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4783_TI;

#include "t1291.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>
extern MethodInfo m32309_MI;
static PropertyInfo t4783____Current_PropertyInfo = 
{
	&t4783_TI, "Current", &m32309_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4783_PIs[] =
{
	&t4783____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1291_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32309_GM;
MethodInfo m32309_MI = 
{
	"get_Current", NULL, &t4783_TI, &t1291_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32309_GM};
static MethodInfo* t4783_MIs[] =
{
	&m32309_MI,
	NULL
};
static TypeInfo* t4783_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4783_0_0_0;
extern Il2CppType t4783_1_0_0;
struct t4783;
extern Il2CppGenericClass t4783_GC;
TypeInfo t4783_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4783_MIs, t4783_PIs, NULL, NULL, NULL, NULL, NULL, &t4783_TI, t4783_ITIs, NULL, &EmptyCustomAttributesCache, &t4783_TI, &t4783_0_0_0, &t4783_1_0_0, NULL, &t4783_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3341.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3341_TI;
#include "t3341MD.h"

extern TypeInfo t1291_TI;
extern MethodInfo m18581_MI;
extern MethodInfo m24718_MI;
struct t20;
#define m24718(__this, p0, method) (t1291 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.Calendar>
extern Il2CppType t20_0_0_1;
FieldInfo t3341_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3341_TI, offsetof(t3341, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3341_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3341_TI, offsetof(t3341, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3341_FIs[] =
{
	&t3341_f0_FieldInfo,
	&t3341_f1_FieldInfo,
	NULL
};
extern MethodInfo m18578_MI;
static PropertyInfo t3341____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3341_TI, "System.Collections.IEnumerator.Current", &m18578_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3341____Current_PropertyInfo = 
{
	&t3341_TI, "Current", &m18581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3341_PIs[] =
{
	&t3341____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3341____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3341_m18577_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18577_GM;
MethodInfo m18577_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3341_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3341_m18577_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18577_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18578_GM;
MethodInfo m18578_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3341_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18578_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18579_GM;
MethodInfo m18579_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3341_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18579_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18580_GM;
MethodInfo m18580_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3341_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18580_GM};
extern Il2CppType t1291_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18581_GM;
MethodInfo m18581_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3341_TI, &t1291_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18581_GM};
static MethodInfo* t3341_MIs[] =
{
	&m18577_MI,
	&m18578_MI,
	&m18579_MI,
	&m18580_MI,
	&m18581_MI,
	NULL
};
extern MethodInfo m18580_MI;
extern MethodInfo m18579_MI;
static MethodInfo* t3341_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18578_MI,
	&m18580_MI,
	&m18579_MI,
	&m18581_MI,
};
static TypeInfo* t3341_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4783_TI,
};
static Il2CppInterfaceOffsetPair t3341_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4783_TI, 7},
};
extern TypeInfo t1291_TI;
static Il2CppRGCTXData t3341_RGCTXData[3] = 
{
	&m18581_MI/* Method Usage */,
	&t1291_TI/* Class Usage */,
	&m24718_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3341_0_0_0;
extern Il2CppType t3341_1_0_0;
extern Il2CppGenericClass t3341_GC;
TypeInfo t3341_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3341_MIs, t3341_PIs, t3341_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3341_TI, t3341_ITIs, t3341_VT, &EmptyCustomAttributesCache, &t3341_TI, &t3341_0_0_0, &t3341_1_0_0, t3341_IOs, &t3341_GC, NULL, NULL, NULL, t3341_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3341)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6216_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.Calendar>
extern MethodInfo m32310_MI;
static PropertyInfo t6216____Count_PropertyInfo = 
{
	&t6216_TI, "Count", &m32310_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32311_MI;
static PropertyInfo t6216____IsReadOnly_PropertyInfo = 
{
	&t6216_TI, "IsReadOnly", &m32311_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6216_PIs[] =
{
	&t6216____Count_PropertyInfo,
	&t6216____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32310_GM;
MethodInfo m32310_MI = 
{
	"get_Count", NULL, &t6216_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32310_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32311_GM;
MethodInfo m32311_MI = 
{
	"get_IsReadOnly", NULL, &t6216_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32311_GM};
extern Il2CppType t1291_0_0_0;
extern Il2CppType t1291_0_0_0;
static ParameterInfo t6216_m32312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1291_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32312_GM;
MethodInfo m32312_MI = 
{
	"Add", NULL, &t6216_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6216_m32312_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32312_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32313_GM;
MethodInfo m32313_MI = 
{
	"Clear", NULL, &t6216_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32313_GM};
extern Il2CppType t1291_0_0_0;
static ParameterInfo t6216_m32314_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1291_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32314_GM;
MethodInfo m32314_MI = 
{
	"Contains", NULL, &t6216_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6216_m32314_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32314_GM};
extern Il2CppType t1297_0_0_0;
extern Il2CppType t1297_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6216_m32315_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1297_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32315_GM;
MethodInfo m32315_MI = 
{
	"CopyTo", NULL, &t6216_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6216_m32315_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32315_GM};
extern Il2CppType t1291_0_0_0;
static ParameterInfo t6216_m32316_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1291_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32316_GM;
MethodInfo m32316_MI = 
{
	"Remove", NULL, &t6216_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6216_m32316_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32316_GM};
static MethodInfo* t6216_MIs[] =
{
	&m32310_MI,
	&m32311_MI,
	&m32312_MI,
	&m32313_MI,
	&m32314_MI,
	&m32315_MI,
	&m32316_MI,
	NULL
};
extern TypeInfo t6218_TI;
static TypeInfo* t6216_ITIs[] = 
{
	&t603_TI,
	&t6218_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6216_0_0_0;
extern Il2CppType t6216_1_0_0;
struct t6216;
extern Il2CppGenericClass t6216_GC;
TypeInfo t6216_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6216_MIs, t6216_PIs, NULL, NULL, NULL, NULL, NULL, &t6216_TI, t6216_ITIs, NULL, &EmptyCustomAttributesCache, &t6216_TI, &t6216_0_0_0, &t6216_1_0_0, NULL, &t6216_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>
extern Il2CppType t4783_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32317_GM;
MethodInfo m32317_MI = 
{
	"GetEnumerator", NULL, &t6218_TI, &t4783_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32317_GM};
static MethodInfo* t6218_MIs[] =
{
	&m32317_MI,
	NULL
};
static TypeInfo* t6218_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6218_0_0_0;
extern Il2CppType t6218_1_0_0;
struct t6218;
extern Il2CppGenericClass t6218_GC;
TypeInfo t6218_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6218_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6218_TI, t6218_ITIs, NULL, &EmptyCustomAttributesCache, &t6218_TI, &t6218_0_0_0, &t6218_1_0_0, NULL, &t6218_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6217_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.Calendar>
extern MethodInfo m32318_MI;
extern MethodInfo m32319_MI;
static PropertyInfo t6217____Item_PropertyInfo = 
{
	&t6217_TI, "Item", &m32318_MI, &m32319_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6217_PIs[] =
{
	&t6217____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1291_0_0_0;
static ParameterInfo t6217_m32320_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1291_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32320_GM;
MethodInfo m32320_MI = 
{
	"IndexOf", NULL, &t6217_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6217_m32320_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32320_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1291_0_0_0;
static ParameterInfo t6217_m32321_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1291_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32321_GM;
MethodInfo m32321_MI = 
{
	"Insert", NULL, &t6217_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6217_m32321_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32321_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6217_m32322_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32322_GM;
MethodInfo m32322_MI = 
{
	"RemoveAt", NULL, &t6217_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6217_m32322_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32322_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6217_m32318_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1291_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32318_GM;
MethodInfo m32318_MI = 
{
	"get_Item", NULL, &t6217_TI, &t1291_0_0_0, RuntimeInvoker_t29_t44, t6217_m32318_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32318_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1291_0_0_0;
static ParameterInfo t6217_m32319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1291_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32319_GM;
MethodInfo m32319_MI = 
{
	"set_Item", NULL, &t6217_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6217_m32319_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32319_GM};
static MethodInfo* t6217_MIs[] =
{
	&m32320_MI,
	&m32321_MI,
	&m32322_MI,
	&m32318_MI,
	&m32319_MI,
	NULL
};
static TypeInfo* t6217_ITIs[] = 
{
	&t603_TI,
	&t6216_TI,
	&t6218_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6217_0_0_0;
extern Il2CppType t6217_1_0_0;
struct t6217;
extern Il2CppGenericClass t6217_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6217_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6217_MIs, t6217_PIs, NULL, NULL, NULL, NULL, NULL, &t6217_TI, t6217_ITIs, NULL, &t1908__CustomAttributeCache, &t6217_TI, &t6217_0_0_0, &t6217_1_0_0, NULL, &t6217_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4785_TI;

#include "t1298.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo m32323_MI;
static PropertyInfo t4785____Current_PropertyInfo = 
{
	&t4785_TI, "Current", &m32323_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4785_PIs[] =
{
	&t4785____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1298_0_0_0;
extern void* RuntimeInvoker_t1298 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32323_GM;
MethodInfo m32323_MI = 
{
	"get_Current", NULL, &t4785_TI, &t1298_0_0_0, RuntimeInvoker_t1298, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32323_GM};
static MethodInfo* t4785_MIs[] =
{
	&m32323_MI,
	NULL
};
static TypeInfo* t4785_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4785_0_0_0;
extern Il2CppType t4785_1_0_0;
struct t4785;
extern Il2CppGenericClass t4785_GC;
TypeInfo t4785_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4785_MIs, t4785_PIs, NULL, NULL, NULL, NULL, NULL, &t4785_TI, t4785_ITIs, NULL, &EmptyCustomAttributesCache, &t4785_TI, &t4785_0_0_0, &t4785_1_0_0, NULL, &t4785_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3342.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3342_TI;
#include "t3342MD.h"

extern TypeInfo t1298_TI;
extern MethodInfo m18586_MI;
extern MethodInfo m24729_MI;
struct t20;
 int32_t m24729 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18582_MI;
 void m18582 (t3342 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18583_MI;
 t29 * m18583 (t3342 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18586(__this, &m18586_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1298_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18584_MI;
 void m18584 (t3342 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18585_MI;
 bool m18585 (t3342 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18586 (t3342 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24729(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24729_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3342_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3342_TI, offsetof(t3342, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3342_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3342_TI, offsetof(t3342, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3342_FIs[] =
{
	&t3342_f0_FieldInfo,
	&t3342_f1_FieldInfo,
	NULL
};
static PropertyInfo t3342____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3342_TI, "System.Collections.IEnumerator.Current", &m18583_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3342____Current_PropertyInfo = 
{
	&t3342_TI, "Current", &m18586_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3342_PIs[] =
{
	&t3342____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3342____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3342_m18582_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18582_GM;
MethodInfo m18582_MI = 
{
	".ctor", (methodPointerType)&m18582, &t3342_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3342_m18582_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18582_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18583_GM;
MethodInfo m18583_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18583, &t3342_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18583_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18584_GM;
MethodInfo m18584_MI = 
{
	"Dispose", (methodPointerType)&m18584, &t3342_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18584_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18585_GM;
MethodInfo m18585_MI = 
{
	"MoveNext", (methodPointerType)&m18585, &t3342_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18585_GM};
extern Il2CppType t1298_0_0_0;
extern void* RuntimeInvoker_t1298 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18586_GM;
MethodInfo m18586_MI = 
{
	"get_Current", (methodPointerType)&m18586, &t3342_TI, &t1298_0_0_0, RuntimeInvoker_t1298, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18586_GM};
static MethodInfo* t3342_MIs[] =
{
	&m18582_MI,
	&m18583_MI,
	&m18584_MI,
	&m18585_MI,
	&m18586_MI,
	NULL
};
static MethodInfo* t3342_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18583_MI,
	&m18585_MI,
	&m18584_MI,
	&m18586_MI,
};
static TypeInfo* t3342_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4785_TI,
};
static Il2CppInterfaceOffsetPair t3342_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4785_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3342_0_0_0;
extern Il2CppType t3342_1_0_0;
extern Il2CppGenericClass t3342_GC;
TypeInfo t3342_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3342_MIs, t3342_PIs, t3342_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3342_TI, t3342_ITIs, t3342_VT, &EmptyCustomAttributesCache, &t3342_TI, &t3342_0_0_0, &t3342_1_0_0, t3342_IOs, &t3342_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3342)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6219_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo m32324_MI;
static PropertyInfo t6219____Count_PropertyInfo = 
{
	&t6219_TI, "Count", &m32324_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32325_MI;
static PropertyInfo t6219____IsReadOnly_PropertyInfo = 
{
	&t6219_TI, "IsReadOnly", &m32325_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6219_PIs[] =
{
	&t6219____Count_PropertyInfo,
	&t6219____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32324_GM;
MethodInfo m32324_MI = 
{
	"get_Count", NULL, &t6219_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32324_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32325_GM;
MethodInfo m32325_MI = 
{
	"get_IsReadOnly", NULL, &t6219_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32325_GM};
extern Il2CppType t1298_0_0_0;
extern Il2CppType t1298_0_0_0;
static ParameterInfo t6219_m32326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32326_GM;
MethodInfo m32326_MI = 
{
	"Add", NULL, &t6219_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6219_m32326_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32326_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32327_GM;
MethodInfo m32327_MI = 
{
	"Clear", NULL, &t6219_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32327_GM};
extern Il2CppType t1298_0_0_0;
static ParameterInfo t6219_m32328_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1298_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32328_GM;
MethodInfo m32328_MI = 
{
	"Contains", NULL, &t6219_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6219_m32328_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32328_GM};
extern Il2CppType t3607_0_0_0;
extern Il2CppType t3607_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6219_m32329_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3607_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32329_GM;
MethodInfo m32329_MI = 
{
	"CopyTo", NULL, &t6219_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6219_m32329_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32329_GM};
extern Il2CppType t1298_0_0_0;
static ParameterInfo t6219_m32330_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1298_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32330_GM;
MethodInfo m32330_MI = 
{
	"Remove", NULL, &t6219_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6219_m32330_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32330_GM};
static MethodInfo* t6219_MIs[] =
{
	&m32324_MI,
	&m32325_MI,
	&m32326_MI,
	&m32327_MI,
	&m32328_MI,
	&m32329_MI,
	&m32330_MI,
	NULL
};
extern TypeInfo t6221_TI;
static TypeInfo* t6219_ITIs[] = 
{
	&t603_TI,
	&t6221_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6219_0_0_0;
extern Il2CppType t6219_1_0_0;
struct t6219;
extern Il2CppGenericClass t6219_GC;
TypeInfo t6219_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6219_MIs, t6219_PIs, NULL, NULL, NULL, NULL, NULL, &t6219_TI, t6219_ITIs, NULL, &EmptyCustomAttributesCache, &t6219_TI, &t6219_0_0_0, &t6219_1_0_0, NULL, &t6219_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>
extern Il2CppType t4785_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32331_GM;
MethodInfo m32331_MI = 
{
	"GetEnumerator", NULL, &t6221_TI, &t4785_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32331_GM};
static MethodInfo* t6221_MIs[] =
{
	&m32331_MI,
	NULL
};
static TypeInfo* t6221_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6221_0_0_0;
extern Il2CppType t6221_1_0_0;
struct t6221;
extern Il2CppGenericClass t6221_GC;
TypeInfo t6221_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6221_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6221_TI, t6221_ITIs, NULL, &EmptyCustomAttributesCache, &t6221_TI, &t6221_0_0_0, &t6221_1_0_0, NULL, &t6221_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6220_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo m32332_MI;
extern MethodInfo m32333_MI;
static PropertyInfo t6220____Item_PropertyInfo = 
{
	&t6220_TI, "Item", &m32332_MI, &m32333_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6220_PIs[] =
{
	&t6220____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1298_0_0_0;
static ParameterInfo t6220_m32334_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1298_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32334_GM;
MethodInfo m32334_MI = 
{
	"IndexOf", NULL, &t6220_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6220_m32334_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32334_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1298_0_0_0;
static ParameterInfo t6220_m32335_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32335_GM;
MethodInfo m32335_MI = 
{
	"Insert", NULL, &t6220_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6220_m32335_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32335_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6220_m32336_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32336_GM;
MethodInfo m32336_MI = 
{
	"RemoveAt", NULL, &t6220_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6220_m32336_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32336_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6220_m32332_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1298_0_0_0;
extern void* RuntimeInvoker_t1298_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32332_GM;
MethodInfo m32332_MI = 
{
	"get_Item", NULL, &t6220_TI, &t1298_0_0_0, RuntimeInvoker_t1298_t44, t6220_m32332_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32332_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1298_0_0_0;
static ParameterInfo t6220_m32333_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32333_GM;
MethodInfo m32333_MI = 
{
	"set_Item", NULL, &t6220_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6220_m32333_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32333_GM};
static MethodInfo* t6220_MIs[] =
{
	&m32334_MI,
	&m32335_MI,
	&m32336_MI,
	&m32332_MI,
	&m32333_MI,
	NULL
};
static TypeInfo* t6220_ITIs[] = 
{
	&t603_TI,
	&t6219_TI,
	&t6221_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6220_0_0_0;
extern Il2CppType t6220_1_0_0;
struct t6220;
extern Il2CppGenericClass t6220_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6220_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6220_MIs, t6220_PIs, NULL, NULL, NULL, NULL, NULL, &t6220_TI, t6220_ITIs, NULL, &t1908__CustomAttributeCache, &t6220_TI, &t6220_0_0_0, &t6220_1_0_0, NULL, &t6220_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4787_TI;

#include "t1093.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>
extern MethodInfo m32337_MI;
static PropertyInfo t4787____Current_PropertyInfo = 
{
	&t4787_TI, "Current", &m32337_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4787_PIs[] =
{
	&t4787____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1093_0_0_0;
extern void* RuntimeInvoker_t1093 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32337_GM;
MethodInfo m32337_MI = 
{
	"get_Current", NULL, &t4787_TI, &t1093_0_0_0, RuntimeInvoker_t1093, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32337_GM};
static MethodInfo* t4787_MIs[] =
{
	&m32337_MI,
	NULL
};
static TypeInfo* t4787_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4787_0_0_0;
extern Il2CppType t4787_1_0_0;
struct t4787;
extern Il2CppGenericClass t4787_GC;
TypeInfo t4787_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4787_MIs, t4787_PIs, NULL, NULL, NULL, NULL, NULL, &t4787_TI, t4787_ITIs, NULL, &EmptyCustomAttributesCache, &t4787_TI, &t4787_0_0_0, &t4787_1_0_0, NULL, &t4787_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3343.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3343_TI;
#include "t3343MD.h"

extern TypeInfo t1093_TI;
extern MethodInfo m18591_MI;
extern MethodInfo m24740_MI;
struct t20;
 int32_t m24740 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18587_MI;
 void m18587 (t3343 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18588_MI;
 t29 * m18588 (t3343 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18591(__this, &m18591_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1093_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18589_MI;
 void m18589 (t3343 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18590_MI;
 bool m18590 (t3343 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18591 (t3343 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24740(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24740_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
extern Il2CppType t20_0_0_1;
FieldInfo t3343_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3343_TI, offsetof(t3343, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3343_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3343_TI, offsetof(t3343, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3343_FIs[] =
{
	&t3343_f0_FieldInfo,
	&t3343_f1_FieldInfo,
	NULL
};
static PropertyInfo t3343____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3343_TI, "System.Collections.IEnumerator.Current", &m18588_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3343____Current_PropertyInfo = 
{
	&t3343_TI, "Current", &m18591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3343_PIs[] =
{
	&t3343____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3343____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3343_m18587_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18587_GM;
MethodInfo m18587_MI = 
{
	".ctor", (methodPointerType)&m18587, &t3343_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3343_m18587_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18587_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18588_GM;
MethodInfo m18588_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18588, &t3343_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18588_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18589_GM;
MethodInfo m18589_MI = 
{
	"Dispose", (methodPointerType)&m18589, &t3343_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18589_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18590_GM;
MethodInfo m18590_MI = 
{
	"MoveNext", (methodPointerType)&m18590, &t3343_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18590_GM};
extern Il2CppType t1093_0_0_0;
extern void* RuntimeInvoker_t1093 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18591_GM;
MethodInfo m18591_MI = 
{
	"get_Current", (methodPointerType)&m18591, &t3343_TI, &t1093_0_0_0, RuntimeInvoker_t1093, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18591_GM};
static MethodInfo* t3343_MIs[] =
{
	&m18587_MI,
	&m18588_MI,
	&m18589_MI,
	&m18590_MI,
	&m18591_MI,
	NULL
};
static MethodInfo* t3343_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18588_MI,
	&m18590_MI,
	&m18589_MI,
	&m18591_MI,
};
static TypeInfo* t3343_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4787_TI,
};
static Il2CppInterfaceOffsetPair t3343_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4787_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3343_0_0_0;
extern Il2CppType t3343_1_0_0;
extern Il2CppGenericClass t3343_GC;
TypeInfo t3343_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3343_MIs, t3343_PIs, t3343_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3343_TI, t3343_ITIs, t3343_VT, &EmptyCustomAttributesCache, &t3343_TI, &t3343_0_0_0, &t3343_1_0_0, t3343_IOs, &t3343_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3343)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6222_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>
extern MethodInfo m32338_MI;
static PropertyInfo t6222____Count_PropertyInfo = 
{
	&t6222_TI, "Count", &m32338_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32339_MI;
static PropertyInfo t6222____IsReadOnly_PropertyInfo = 
{
	&t6222_TI, "IsReadOnly", &m32339_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6222_PIs[] =
{
	&t6222____Count_PropertyInfo,
	&t6222____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32338_GM;
MethodInfo m32338_MI = 
{
	"get_Count", NULL, &t6222_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32338_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32339_GM;
MethodInfo m32339_MI = 
{
	"get_IsReadOnly", NULL, &t6222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32339_GM};
extern Il2CppType t1093_0_0_0;
extern Il2CppType t1093_0_0_0;
static ParameterInfo t6222_m32340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1093_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32340_GM;
MethodInfo m32340_MI = 
{
	"Add", NULL, &t6222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6222_m32340_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32340_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32341_GM;
MethodInfo m32341_MI = 
{
	"Clear", NULL, &t6222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32341_GM};
extern Il2CppType t1093_0_0_0;
static ParameterInfo t6222_m32342_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1093_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32342_GM;
MethodInfo m32342_MI = 
{
	"Contains", NULL, &t6222_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6222_m32342_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32342_GM};
extern Il2CppType t3608_0_0_0;
extern Il2CppType t3608_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6222_m32343_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3608_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32343_GM;
MethodInfo m32343_MI = 
{
	"CopyTo", NULL, &t6222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6222_m32343_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32343_GM};
extern Il2CppType t1093_0_0_0;
static ParameterInfo t6222_m32344_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1093_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32344_GM;
MethodInfo m32344_MI = 
{
	"Remove", NULL, &t6222_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6222_m32344_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32344_GM};
static MethodInfo* t6222_MIs[] =
{
	&m32338_MI,
	&m32339_MI,
	&m32340_MI,
	&m32341_MI,
	&m32342_MI,
	&m32343_MI,
	&m32344_MI,
	NULL
};
extern TypeInfo t6224_TI;
static TypeInfo* t6222_ITIs[] = 
{
	&t603_TI,
	&t6224_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6222_0_0_0;
extern Il2CppType t6222_1_0_0;
struct t6222;
extern Il2CppGenericClass t6222_GC;
TypeInfo t6222_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6222_MIs, t6222_PIs, NULL, NULL, NULL, NULL, NULL, &t6222_TI, t6222_ITIs, NULL, &EmptyCustomAttributesCache, &t6222_TI, &t6222_0_0_0, &t6222_1_0_0, NULL, &t6222_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>
extern Il2CppType t4787_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32345_GM;
MethodInfo m32345_MI = 
{
	"GetEnumerator", NULL, &t6224_TI, &t4787_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32345_GM};
static MethodInfo* t6224_MIs[] =
{
	&m32345_MI,
	NULL
};
static TypeInfo* t6224_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6224_0_0_0;
extern Il2CppType t6224_1_0_0;
struct t6224;
extern Il2CppGenericClass t6224_GC;
TypeInfo t6224_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6224_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6224_TI, t6224_ITIs, NULL, &EmptyCustomAttributesCache, &t6224_TI, &t6224_0_0_0, &t6224_1_0_0, NULL, &t6224_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6223_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>
extern MethodInfo m32346_MI;
extern MethodInfo m32347_MI;
static PropertyInfo t6223____Item_PropertyInfo = 
{
	&t6223_TI, "Item", &m32346_MI, &m32347_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6223_PIs[] =
{
	&t6223____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1093_0_0_0;
static ParameterInfo t6223_m32348_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1093_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32348_GM;
MethodInfo m32348_MI = 
{
	"IndexOf", NULL, &t6223_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6223_m32348_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32348_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1093_0_0_0;
static ParameterInfo t6223_m32349_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1093_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32349_GM;
MethodInfo m32349_MI = 
{
	"Insert", NULL, &t6223_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6223_m32349_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32349_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6223_m32350_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32350_GM;
MethodInfo m32350_MI = 
{
	"RemoveAt", NULL, &t6223_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6223_m32350_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32350_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6223_m32346_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1093_0_0_0;
extern void* RuntimeInvoker_t1093_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32346_GM;
MethodInfo m32346_MI = 
{
	"get_Item", NULL, &t6223_TI, &t1093_0_0_0, RuntimeInvoker_t1093_t44, t6223_m32346_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32346_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1093_0_0_0;
static ParameterInfo t6223_m32347_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1093_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32347_GM;
MethodInfo m32347_MI = 
{
	"set_Item", NULL, &t6223_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6223_m32347_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32347_GM};
static MethodInfo* t6223_MIs[] =
{
	&m32348_MI,
	&m32349_MI,
	&m32350_MI,
	&m32346_MI,
	&m32347_MI,
	NULL
};
static TypeInfo* t6223_ITIs[] = 
{
	&t603_TI,
	&t6222_TI,
	&t6224_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6223_0_0_0;
extern Il2CppType t6223_1_0_0;
struct t6223;
extern Il2CppGenericClass t6223_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6223_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6223_MIs, t6223_PIs, NULL, NULL, NULL, NULL, NULL, &t6223_TI, t6223_ITIs, NULL, &t1908__CustomAttributeCache, &t6223_TI, &t6223_0_0_0, &t6223_1_0_0, NULL, &t6223_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4789_TI;

#include "t1301.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo m32351_MI;
static PropertyInfo t4789____Current_PropertyInfo = 
{
	&t4789_TI, "Current", &m32351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4789_PIs[] =
{
	&t4789____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1301_0_0_0;
extern void* RuntimeInvoker_t1301 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32351_GM;
MethodInfo m32351_MI = 
{
	"get_Current", NULL, &t4789_TI, &t1301_0_0_0, RuntimeInvoker_t1301, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32351_GM};
static MethodInfo* t4789_MIs[] =
{
	&m32351_MI,
	NULL
};
static TypeInfo* t4789_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4789_0_0_0;
extern Il2CppType t4789_1_0_0;
struct t4789;
extern Il2CppGenericClass t4789_GC;
TypeInfo t4789_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4789_MIs, t4789_PIs, NULL, NULL, NULL, NULL, NULL, &t4789_TI, t4789_ITIs, NULL, &EmptyCustomAttributesCache, &t4789_TI, &t4789_0_0_0, &t4789_1_0_0, NULL, &t4789_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3344.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3344_TI;
#include "t3344MD.h"

extern TypeInfo t1301_TI;
extern MethodInfo m18596_MI;
extern MethodInfo m24751_MI;
struct t20;
 int32_t m24751 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18592_MI;
 void m18592 (t3344 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18593_MI;
 t29 * m18593 (t3344 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18596(__this, &m18596_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1301_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18594_MI;
 void m18594 (t3344 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18595_MI;
 bool m18595 (t3344 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18596 (t3344 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24751(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24751_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
extern Il2CppType t20_0_0_1;
FieldInfo t3344_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3344_TI, offsetof(t3344, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3344_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3344_TI, offsetof(t3344, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3344_FIs[] =
{
	&t3344_f0_FieldInfo,
	&t3344_f1_FieldInfo,
	NULL
};
static PropertyInfo t3344____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3344_TI, "System.Collections.IEnumerator.Current", &m18593_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3344____Current_PropertyInfo = 
{
	&t3344_TI, "Current", &m18596_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3344_PIs[] =
{
	&t3344____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3344____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3344_m18592_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18592_GM;
MethodInfo m18592_MI = 
{
	".ctor", (methodPointerType)&m18592, &t3344_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3344_m18592_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18592_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18593_GM;
MethodInfo m18593_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18593, &t3344_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18593_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18594_GM;
MethodInfo m18594_MI = 
{
	"Dispose", (methodPointerType)&m18594, &t3344_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18594_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18595_GM;
MethodInfo m18595_MI = 
{
	"MoveNext", (methodPointerType)&m18595, &t3344_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18595_GM};
extern Il2CppType t1301_0_0_0;
extern void* RuntimeInvoker_t1301 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18596_GM;
MethodInfo m18596_MI = 
{
	"get_Current", (methodPointerType)&m18596, &t3344_TI, &t1301_0_0_0, RuntimeInvoker_t1301, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18596_GM};
static MethodInfo* t3344_MIs[] =
{
	&m18592_MI,
	&m18593_MI,
	&m18594_MI,
	&m18595_MI,
	&m18596_MI,
	NULL
};
static MethodInfo* t3344_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18593_MI,
	&m18595_MI,
	&m18594_MI,
	&m18596_MI,
};
static TypeInfo* t3344_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4789_TI,
};
static Il2CppInterfaceOffsetPair t3344_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4789_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3344_0_0_0;
extern Il2CppType t3344_1_0_0;
extern Il2CppGenericClass t3344_GC;
TypeInfo t3344_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3344_MIs, t3344_PIs, t3344_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3344_TI, t3344_ITIs, t3344_VT, &EmptyCustomAttributesCache, &t3344_TI, &t3344_0_0_0, &t3344_1_0_0, t3344_IOs, &t3344_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3344)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
