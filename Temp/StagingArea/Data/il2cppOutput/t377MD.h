﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t377;
struct t7;

 void m1728 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2770 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1732 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2771 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2772 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2773 (t377 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2774 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1733 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2775 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1729 (t377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
