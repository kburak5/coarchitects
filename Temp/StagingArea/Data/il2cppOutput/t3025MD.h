﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3025;
struct t29;
struct t20;
#include "t152.h"

 void m16514 (t3025 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16515 (t3025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16516 (t3025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16517 (t3025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16518 (t3025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
