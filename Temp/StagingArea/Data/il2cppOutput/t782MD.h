﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t782;
struct t7;
struct t781;
#include "t936.h"

 void m8203 (t782 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m4051 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m8204 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8205 (t782 * __this, t936  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8206 (t782 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8207 (t782 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
