﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1698;
struct t1698_marshaled;

void t1698_marshal(const t1698& unmarshaled, t1698_marshaled& marshaled);
void t1698_marshal_back(const t1698_marshaled& marshaled, t1698& unmarshaled);
void t1698_marshal_cleanup(t1698_marshaled& marshaled);
