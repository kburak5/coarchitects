﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1466;
struct t1477;
struct t1479;
struct t1478;
struct t7;
struct t42;
struct t733;
struct t29;
#include "t735.h"

 void m7979 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7980 (t1466 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7981 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7982 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7983 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7984 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7985 (t1466 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7986 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7987 (t1466 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7988 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7989 (t1466 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7990 (t1466 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7991 (t1466 * __this, t735  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7992 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7993 (t1466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
