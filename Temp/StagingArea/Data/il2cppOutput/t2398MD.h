﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2398;
struct t29;
struct t20;
#include "t330.h"

 void m12394 (t2398 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12395 (t2398 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12396 (t2398 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12397 (t2398 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t330  m12398 (t2398 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
