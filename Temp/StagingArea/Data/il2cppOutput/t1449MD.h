﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1449;
struct t733;
struct t7;
struct t29;
#include "t735.h"

 void m7855 (t1449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7856 (t1449 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7857 (t1449 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7858 (t1449 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
