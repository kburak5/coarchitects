﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t484;
struct t466;
struct t157;
#include "t132.h"
#include "t35.h"

 void m2249 (t484 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2250 (t484 * __this, t466 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2251 (t484 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2252 (t484 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2253 (t484 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t157 * m2254 (t484 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2255 (t484 * __this, t132 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2256 (t484 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
