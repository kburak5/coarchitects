﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3330;
struct t29;
struct t20;
#include "t1279.h"

 void m18522 (t3330 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18523 (t3330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18524 (t3330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18525 (t3330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1279  m18526 (t3330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
