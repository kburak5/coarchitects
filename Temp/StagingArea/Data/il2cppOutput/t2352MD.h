﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2352;
struct t29;
struct t20;
#include "t322.h"

 void m12097 (t2352 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12098 (t2352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12099 (t2352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12100 (t2352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t322  m12101 (t2352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
