﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1050;
#include "t35.h"

 void m8813 (t1050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8814 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8815 (t1050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m8816 (t1050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8817 (t1050 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8818 (t1050 * __this, t35 p0, int32_t p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8819 (t1050 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5238 (t1050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5267 (t1050 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8820 (t1050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8821 (t1050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
