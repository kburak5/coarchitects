﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1369;
struct t733;
struct t7;
struct t42;
struct t537;
struct t29;
#include "t735.h"
#include "t1149.h"

 void m7536 (t1369 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7537 (t29 * __this, t733 * p0, t7* p1, t42 * p2, t7* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7538 (t29 * __this, t733 * p0, t7* p1, t42 * p2, t7* p3, int32_t p4, t537* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7539 (t1369 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7540 (t1369 * __this, t735  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
