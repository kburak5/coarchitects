﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1580;
struct t7;
struct t781;

 void m8565 (t1580 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8566 (t1580 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8567 (t1580 * __this, t7* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
