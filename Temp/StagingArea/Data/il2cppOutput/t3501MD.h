﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3501;
struct t29;
struct t20;
#include "t947.h"

 void m19447 (t3501 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19448 (t3501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19449 (t3501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19450 (t3501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19451 (t3501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
