﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2227;
struct t29;
struct t20;
struct t2232;
struct t136;
struct t105;
#include "t2233.h"

#include "t2229MD.h"
#define m11080(__this, method) (void)m11064_gshared((t2229 *)__this, method)
#define m11081(__this, method) (bool)m11065_gshared((t2229 *)__this, method)
#define m11082(__this, method) (t29 *)m11066_gshared((t2229 *)__this, method)
#define m11083(__this, p0, p1, method) (void)m11067_gshared((t2229 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m11084(__this, method) (t29*)m11068_gshared((t2229 *)__this, method)
#define m11085(__this, method) (t29 *)m11069_gshared((t2229 *)__this, method)
#define m11086(__this, method) (t105 *)m11070_gshared((t2229 *)__this, method)
#define m11087(__this, method) (t105 *)m11071_gshared((t2229 *)__this, method)
#define m11088(__this, p0, method) (void)m11072_gshared((t2229 *)__this, (t29 *)p0, method)
#define m11089(__this, method) (int32_t)m11073_gshared((t2229 *)__this, method)
 t2233  m11090 (t2227 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
