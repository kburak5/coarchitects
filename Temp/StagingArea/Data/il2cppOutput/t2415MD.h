﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2415;
struct t29;
struct t2418;
struct t2416;
struct t733;
struct t2419;
struct t20;
struct t136;
struct t2421;
struct t722;
#include "t735.h"
#include "t2420.h"
#include "t2422.h"
#include "t725.h"

 void m12482 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12483 (t2415 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12484 (t2415 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12485 (t2415 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12486 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12487 (t2415 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12488 (t2415 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12489 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12490 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12491 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12492 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12493 (t2415 * __this, t2420  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12494 (t2415 * __this, t2420  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12495 (t2415 * __this, t2419* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12496 (t2415 * __this, t2420  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12497 (t2415 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12498 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m12499 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12500 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12501 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12502 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12503 (t2415 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12504 (t2415 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12505 (t2415 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12506 (t2415 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2420  m12507 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12508 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12509 (t2415 * __this, t2419* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12510 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12511 (t2415 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12512 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12513 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12514 (t2415 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12515 (t2415 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12516 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12517 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12518 (t2415 * __this, t29 * p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2418 * m12519 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12520 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12521 (t2415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12522 (t2415 * __this, t2420  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2422  m12523 (t2415 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12524 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
