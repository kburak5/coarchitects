﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6059_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.SByte>>
extern Il2CppType t4680_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31568_GM;
MethodInfo m31568_MI = 
{
	"GetEnumerator", NULL, &t6059_TI, &t4680_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31568_GM};
static MethodInfo* t6059_MIs[] =
{
	&m31568_MI,
	NULL
};
extern TypeInfo t603_TI;
static TypeInfo* t6059_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6059_0_0_0;
extern Il2CppType t6059_1_0_0;
struct t6059;
extern Il2CppGenericClass t6059_GC;
TypeInfo t6059_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6059_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6059_TI, t6059_ITIs, NULL, &EmptyCustomAttributesCache, &t6059_TI, &t6059_0_0_0, &t6059_1_0_0, NULL, &t6059_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4680_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.SByte>>
extern MethodInfo m31569_MI;
static PropertyInfo t4680____Current_PropertyInfo = 
{
	&t4680_TI, "Current", &m31569_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4680_PIs[] =
{
	&t4680____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1725_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31569_GM;
MethodInfo m31569_MI = 
{
	"get_Current", NULL, &t4680_TI, &t1725_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31569_GM};
static MethodInfo* t4680_MIs[] =
{
	&m31569_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4680_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4680_0_0_0;
extern Il2CppType t4680_1_0_0;
struct t4680;
extern Il2CppGenericClass t4680_GC;
TypeInfo t4680_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4680_MIs, t4680_PIs, NULL, NULL, NULL, NULL, NULL, &t4680_TI, t4680_ITIs, NULL, &EmptyCustomAttributesCache, &t4680_TI, &t4680_0_0_0, &t4680_1_0_0, NULL, &t4680_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1725_TI;

#include "t40.h"
#include "t297.h"


// Metadata Definition System.IEquatable`1<System.SByte>
extern Il2CppType t297_0_0_0;
extern Il2CppType t297_0_0_0;
static ParameterInfo t1725_m31570_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31570_GM;
MethodInfo m31570_MI = 
{
	"Equals", NULL, &t1725_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t1725_m31570_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31570_GM};
static MethodInfo* t1725_MIs[] =
{
	&m31570_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1725_0_0_0;
extern Il2CppType t1725_1_0_0;
struct t1725;
extern Il2CppGenericClass t1725_GC;
TypeInfo t1725_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1725_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1725_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1725_TI, &t1725_0_0_0, &t1725_1_0_0, NULL, &t1725_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3272.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3272_TI;
#include "t3272MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18160_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m24092_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m24092(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.SByte>>
extern Il2CppType t20_0_0_1;
FieldInfo t3272_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3272_TI, offsetof(t3272, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3272_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3272_TI, offsetof(t3272, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3272_FIs[] =
{
	&t3272_f0_FieldInfo,
	&t3272_f1_FieldInfo,
	NULL
};
extern MethodInfo m18157_MI;
static PropertyInfo t3272____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3272_TI, "System.Collections.IEnumerator.Current", &m18157_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3272____Current_PropertyInfo = 
{
	&t3272_TI, "Current", &m18160_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3272_PIs[] =
{
	&t3272____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3272____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3272_m18156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18156_GM;
MethodInfo m18156_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3272_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3272_m18156_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18156_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18157_GM;
MethodInfo m18157_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3272_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18157_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18158_GM;
MethodInfo m18158_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18158_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18159_GM;
MethodInfo m18159_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3272_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18159_GM};
extern Il2CppType t1725_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18160_GM;
MethodInfo m18160_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3272_TI, &t1725_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18160_GM};
static MethodInfo* t3272_MIs[] =
{
	&m18156_MI,
	&m18157_MI,
	&m18158_MI,
	&m18159_MI,
	&m18160_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m18159_MI;
extern MethodInfo m18158_MI;
static MethodInfo* t3272_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18157_MI,
	&m18159_MI,
	&m18158_MI,
	&m18160_MI,
};
static TypeInfo* t3272_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4680_TI,
};
static Il2CppInterfaceOffsetPair t3272_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4680_TI, 7},
};
extern TypeInfo t1725_TI;
static Il2CppRGCTXData t3272_RGCTXData[3] = 
{
	&m18160_MI/* Method Usage */,
	&t1725_TI/* Class Usage */,
	&m24092_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3272_0_0_0;
extern Il2CppType t3272_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3272_GC;
extern TypeInfo t20_TI;
TypeInfo t3272_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3272_MIs, t3272_PIs, t3272_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3272_TI, t3272_ITIs, t3272_VT, &EmptyCustomAttributesCache, &t3272_TI, &t3272_0_0_0, &t3272_1_0_0, t3272_IOs, &t3272_GC, NULL, NULL, NULL, t3272_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3272)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6058_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.SByte>>
extern MethodInfo m31571_MI;
extern MethodInfo m31572_MI;
static PropertyInfo t6058____Item_PropertyInfo = 
{
	&t6058_TI, "Item", &m31571_MI, &m31572_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6058_PIs[] =
{
	&t6058____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1725_0_0_0;
static ParameterInfo t6058_m31573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1725_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31573_GM;
MethodInfo m31573_MI = 
{
	"IndexOf", NULL, &t6058_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6058_m31573_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31573_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1725_0_0_0;
static ParameterInfo t6058_m31574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1725_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31574_GM;
MethodInfo m31574_MI = 
{
	"Insert", NULL, &t6058_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6058_m31574_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31574_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6058_m31575_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31575_GM;
MethodInfo m31575_MI = 
{
	"RemoveAt", NULL, &t6058_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6058_m31575_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31575_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6058_m31571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1725_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31571_GM;
MethodInfo m31571_MI = 
{
	"get_Item", NULL, &t6058_TI, &t1725_0_0_0, RuntimeInvoker_t29_t44, t6058_m31571_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31571_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1725_0_0_0;
static ParameterInfo t6058_m31572_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1725_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31572_GM;
MethodInfo m31572_MI = 
{
	"set_Item", NULL, &t6058_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6058_m31572_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31572_GM};
static MethodInfo* t6058_MIs[] =
{
	&m31573_MI,
	&m31574_MI,
	&m31575_MI,
	&m31571_MI,
	&m31572_MI,
	NULL
};
extern TypeInfo t6057_TI;
static TypeInfo* t6058_ITIs[] = 
{
	&t603_TI,
	&t6057_TI,
	&t6059_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6058_0_0_0;
extern Il2CppType t6058_1_0_0;
struct t6058;
extern Il2CppGenericClass t6058_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6058_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6058_MIs, t6058_PIs, NULL, NULL, NULL, NULL, NULL, &t6058_TI, t6058_ITIs, NULL, &t1908__CustomAttributeCache, &t6058_TI, &t6058_0_0_0, &t6058_1_0_0, NULL, &t6058_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4681_TI;

#include "t372.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Int16>
extern MethodInfo m31576_MI;
static PropertyInfo t4681____Current_PropertyInfo = 
{
	&t4681_TI, "Current", &m31576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4681_PIs[] =
{
	&t4681____Current_PropertyInfo,
	NULL
};
extern Il2CppType t372_0_0_0;
extern void* RuntimeInvoker_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31576_GM;
MethodInfo m31576_MI = 
{
	"get_Current", NULL, &t4681_TI, &t372_0_0_0, RuntimeInvoker_t372, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31576_GM};
static MethodInfo* t4681_MIs[] =
{
	&m31576_MI,
	NULL
};
static TypeInfo* t4681_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4681_0_0_0;
extern Il2CppType t4681_1_0_0;
struct t4681;
extern Il2CppGenericClass t4681_GC;
TypeInfo t4681_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4681_MIs, t4681_PIs, NULL, NULL, NULL, NULL, NULL, &t4681_TI, t4681_ITIs, NULL, &EmptyCustomAttributesCache, &t4681_TI, &t4681_0_0_0, &t4681_1_0_0, NULL, &t4681_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3273.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3273_TI;
#include "t3273MD.h"

extern TypeInfo t372_TI;
extern MethodInfo m18165_MI;
extern MethodInfo m24103_MI;
struct t20;
 int16_t m24103 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18161_MI;
 void m18161 (t3273 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18162_MI;
 t29 * m18162 (t3273 * __this, MethodInfo* method){
	{
		int16_t L_0 = m18165(__this, &m18165_MI);
		int16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t372_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18163_MI;
 void m18163 (t3273 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18164_MI;
 bool m18164 (t3273 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int16_t m18165 (t3273 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int16_t L_8 = m24103(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24103_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Int16>
extern Il2CppType t20_0_0_1;
FieldInfo t3273_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3273_TI, offsetof(t3273, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3273_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3273_TI, offsetof(t3273, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3273_FIs[] =
{
	&t3273_f0_FieldInfo,
	&t3273_f1_FieldInfo,
	NULL
};
static PropertyInfo t3273____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3273_TI, "System.Collections.IEnumerator.Current", &m18162_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3273____Current_PropertyInfo = 
{
	&t3273_TI, "Current", &m18165_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3273_PIs[] =
{
	&t3273____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3273____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3273_m18161_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18161_GM;
MethodInfo m18161_MI = 
{
	".ctor", (methodPointerType)&m18161, &t3273_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3273_m18161_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18161_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18162_GM;
MethodInfo m18162_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18162, &t3273_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18162_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18163_GM;
MethodInfo m18163_MI = 
{
	"Dispose", (methodPointerType)&m18163, &t3273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18163_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18164_GM;
MethodInfo m18164_MI = 
{
	"MoveNext", (methodPointerType)&m18164, &t3273_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18164_GM};
extern Il2CppType t372_0_0_0;
extern void* RuntimeInvoker_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18165_GM;
MethodInfo m18165_MI = 
{
	"get_Current", (methodPointerType)&m18165, &t3273_TI, &t372_0_0_0, RuntimeInvoker_t372, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18165_GM};
static MethodInfo* t3273_MIs[] =
{
	&m18161_MI,
	&m18162_MI,
	&m18163_MI,
	&m18164_MI,
	&m18165_MI,
	NULL
};
static MethodInfo* t3273_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18162_MI,
	&m18164_MI,
	&m18163_MI,
	&m18165_MI,
};
static TypeInfo* t3273_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4681_TI,
};
static Il2CppInterfaceOffsetPair t3273_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4681_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3273_0_0_0;
extern Il2CppType t3273_1_0_0;
extern Il2CppGenericClass t3273_GC;
TypeInfo t3273_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3273_MIs, t3273_PIs, t3273_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3273_TI, t3273_ITIs, t3273_VT, &EmptyCustomAttributesCache, &t3273_TI, &t3273_0_0_0, &t3273_1_0_0, t3273_IOs, &t3273_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3273)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6060_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Int16>
extern MethodInfo m31577_MI;
static PropertyInfo t6060____Count_PropertyInfo = 
{
	&t6060_TI, "Count", &m31577_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31578_MI;
static PropertyInfo t6060____IsReadOnly_PropertyInfo = 
{
	&t6060_TI, "IsReadOnly", &m31578_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6060_PIs[] =
{
	&t6060____Count_PropertyInfo,
	&t6060____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31577_GM;
MethodInfo m31577_MI = 
{
	"get_Count", NULL, &t6060_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31577_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31578_GM;
MethodInfo m31578_MI = 
{
	"get_IsReadOnly", NULL, &t6060_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31578_GM};
extern Il2CppType t372_0_0_0;
extern Il2CppType t372_0_0_0;
static ParameterInfo t6060_m31579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31579_GM;
MethodInfo m31579_MI = 
{
	"Add", NULL, &t6060_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t6060_m31579_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31579_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31580_GM;
MethodInfo m31580_MI = 
{
	"Clear", NULL, &t6060_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31580_GM};
extern Il2CppType t372_0_0_0;
static ParameterInfo t6060_m31581_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31581_GM;
MethodInfo m31581_MI = 
{
	"Contains", NULL, &t6060_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t6060_m31581_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31581_GM};
extern Il2CppType t1763_0_0_0;
extern Il2CppType t1763_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6060_m31582_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1763_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31582_GM;
MethodInfo m31582_MI = 
{
	"CopyTo", NULL, &t6060_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6060_m31582_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31582_GM};
extern Il2CppType t372_0_0_0;
static ParameterInfo t6060_m31583_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31583_GM;
MethodInfo m31583_MI = 
{
	"Remove", NULL, &t6060_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t6060_m31583_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31583_GM};
static MethodInfo* t6060_MIs[] =
{
	&m31577_MI,
	&m31578_MI,
	&m31579_MI,
	&m31580_MI,
	&m31581_MI,
	&m31582_MI,
	&m31583_MI,
	NULL
};
extern TypeInfo t6062_TI;
static TypeInfo* t6060_ITIs[] = 
{
	&t603_TI,
	&t6062_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6060_0_0_0;
extern Il2CppType t6060_1_0_0;
struct t6060;
extern Il2CppGenericClass t6060_GC;
TypeInfo t6060_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6060_MIs, t6060_PIs, NULL, NULL, NULL, NULL, NULL, &t6060_TI, t6060_ITIs, NULL, &EmptyCustomAttributesCache, &t6060_TI, &t6060_0_0_0, &t6060_1_0_0, NULL, &t6060_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Int16>
extern Il2CppType t4681_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31584_GM;
MethodInfo m31584_MI = 
{
	"GetEnumerator", NULL, &t6062_TI, &t4681_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31584_GM};
static MethodInfo* t6062_MIs[] =
{
	&m31584_MI,
	NULL
};
static TypeInfo* t6062_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6062_0_0_0;
extern Il2CppType t6062_1_0_0;
struct t6062;
extern Il2CppGenericClass t6062_GC;
TypeInfo t6062_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6062_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6062_TI, t6062_ITIs, NULL, &EmptyCustomAttributesCache, &t6062_TI, &t6062_0_0_0, &t6062_1_0_0, NULL, &t6062_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6061_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Int16>
extern MethodInfo m31585_MI;
extern MethodInfo m31586_MI;
static PropertyInfo t6061____Item_PropertyInfo = 
{
	&t6061_TI, "Item", &m31585_MI, &m31586_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6061_PIs[] =
{
	&t6061____Item_PropertyInfo,
	NULL
};
extern Il2CppType t372_0_0_0;
static ParameterInfo t6061_m31587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31587_GM;
MethodInfo m31587_MI = 
{
	"IndexOf", NULL, &t6061_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t6061_m31587_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31587_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t372_0_0_0;
static ParameterInfo t6061_m31588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31588_GM;
MethodInfo m31588_MI = 
{
	"Insert", NULL, &t6061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t372, t6061_m31588_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31588_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6061_m31589_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31589_GM;
MethodInfo m31589_MI = 
{
	"RemoveAt", NULL, &t6061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6061_m31589_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31589_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6061_m31585_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t372_0_0_0;
extern void* RuntimeInvoker_t372_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31585_GM;
MethodInfo m31585_MI = 
{
	"get_Item", NULL, &t6061_TI, &t372_0_0_0, RuntimeInvoker_t372_t44, t6061_m31585_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31585_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t372_0_0_0;
static ParameterInfo t6061_m31586_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31586_GM;
MethodInfo m31586_MI = 
{
	"set_Item", NULL, &t6061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t372, t6061_m31586_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31586_GM};
static MethodInfo* t6061_MIs[] =
{
	&m31587_MI,
	&m31588_MI,
	&m31589_MI,
	&m31585_MI,
	&m31586_MI,
	NULL
};
static TypeInfo* t6061_ITIs[] = 
{
	&t603_TI,
	&t6060_TI,
	&t6062_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6061_0_0_0;
extern Il2CppType t6061_1_0_0;
struct t6061;
extern Il2CppGenericClass t6061_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6061_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6061_MIs, t6061_PIs, NULL, NULL, NULL, NULL, NULL, &t6061_TI, t6061_ITIs, NULL, &t1908__CustomAttributeCache, &t6061_TI, &t6061_0_0_0, &t6061_1_0_0, NULL, &t6061_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6063_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int16>>
extern MethodInfo m31590_MI;
static PropertyInfo t6063____Count_PropertyInfo = 
{
	&t6063_TI, "Count", &m31590_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31591_MI;
static PropertyInfo t6063____IsReadOnly_PropertyInfo = 
{
	&t6063_TI, "IsReadOnly", &m31591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6063_PIs[] =
{
	&t6063____Count_PropertyInfo,
	&t6063____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31590_GM;
MethodInfo m31590_MI = 
{
	"get_Count", NULL, &t6063_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31590_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31591_GM;
MethodInfo m31591_MI = 
{
	"get_IsReadOnly", NULL, &t6063_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31591_GM};
extern Il2CppType t1727_0_0_0;
extern Il2CppType t1727_0_0_0;
static ParameterInfo t6063_m31592_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1727_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31592_GM;
MethodInfo m31592_MI = 
{
	"Add", NULL, &t6063_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6063_m31592_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31592_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31593_GM;
MethodInfo m31593_MI = 
{
	"Clear", NULL, &t6063_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31593_GM};
extern Il2CppType t1727_0_0_0;
static ParameterInfo t6063_m31594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1727_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31594_GM;
MethodInfo m31594_MI = 
{
	"Contains", NULL, &t6063_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6063_m31594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31594_GM};
extern Il2CppType t3568_0_0_0;
extern Il2CppType t3568_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6063_m31595_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3568_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31595_GM;
MethodInfo m31595_MI = 
{
	"CopyTo", NULL, &t6063_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6063_m31595_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31595_GM};
extern Il2CppType t1727_0_0_0;
static ParameterInfo t6063_m31596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1727_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31596_GM;
MethodInfo m31596_MI = 
{
	"Remove", NULL, &t6063_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6063_m31596_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31596_GM};
static MethodInfo* t6063_MIs[] =
{
	&m31590_MI,
	&m31591_MI,
	&m31592_MI,
	&m31593_MI,
	&m31594_MI,
	&m31595_MI,
	&m31596_MI,
	NULL
};
extern TypeInfo t6065_TI;
static TypeInfo* t6063_ITIs[] = 
{
	&t603_TI,
	&t6065_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6063_0_0_0;
extern Il2CppType t6063_1_0_0;
struct t6063;
extern Il2CppGenericClass t6063_GC;
TypeInfo t6063_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6063_MIs, t6063_PIs, NULL, NULL, NULL, NULL, NULL, &t6063_TI, t6063_ITIs, NULL, &EmptyCustomAttributesCache, &t6063_TI, &t6063_0_0_0, &t6063_1_0_0, NULL, &t6063_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Int16>>
extern Il2CppType t4683_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31597_GM;
MethodInfo m31597_MI = 
{
	"GetEnumerator", NULL, &t6065_TI, &t4683_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31597_GM};
static MethodInfo* t6065_MIs[] =
{
	&m31597_MI,
	NULL
};
static TypeInfo* t6065_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6065_0_0_0;
extern Il2CppType t6065_1_0_0;
struct t6065;
extern Il2CppGenericClass t6065_GC;
TypeInfo t6065_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6065_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6065_TI, t6065_ITIs, NULL, &EmptyCustomAttributesCache, &t6065_TI, &t6065_0_0_0, &t6065_1_0_0, NULL, &t6065_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4683_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Int16>>
extern MethodInfo m31598_MI;
static PropertyInfo t4683____Current_PropertyInfo = 
{
	&t4683_TI, "Current", &m31598_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4683_PIs[] =
{
	&t4683____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1727_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31598_GM;
MethodInfo m31598_MI = 
{
	"get_Current", NULL, &t4683_TI, &t1727_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31598_GM};
static MethodInfo* t4683_MIs[] =
{
	&m31598_MI,
	NULL
};
static TypeInfo* t4683_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4683_0_0_0;
extern Il2CppType t4683_1_0_0;
struct t4683;
extern Il2CppGenericClass t4683_GC;
TypeInfo t4683_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4683_MIs, t4683_PIs, NULL, NULL, NULL, NULL, NULL, &t4683_TI, t4683_ITIs, NULL, &EmptyCustomAttributesCache, &t4683_TI, &t4683_0_0_0, &t4683_1_0_0, NULL, &t4683_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1727_TI;



// Metadata Definition System.IComparable`1<System.Int16>
extern Il2CppType t372_0_0_0;
static ParameterInfo t1727_m31599_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31599_GM;
MethodInfo m31599_MI = 
{
	"CompareTo", NULL, &t1727_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t1727_m31599_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31599_GM};
static MethodInfo* t1727_MIs[] =
{
	&m31599_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1727_1_0_0;
struct t1727;
extern Il2CppGenericClass t1727_GC;
TypeInfo t1727_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1727_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1727_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1727_TI, &t1727_0_0_0, &t1727_1_0_0, NULL, &t1727_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3274.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3274_TI;
#include "t3274MD.h"

extern MethodInfo m18170_MI;
extern MethodInfo m24114_MI;
struct t20;
#define m24114(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Int16>>
extern Il2CppType t20_0_0_1;
FieldInfo t3274_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3274_TI, offsetof(t3274, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3274_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3274_TI, offsetof(t3274, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3274_FIs[] =
{
	&t3274_f0_FieldInfo,
	&t3274_f1_FieldInfo,
	NULL
};
extern MethodInfo m18167_MI;
static PropertyInfo t3274____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3274_TI, "System.Collections.IEnumerator.Current", &m18167_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3274____Current_PropertyInfo = 
{
	&t3274_TI, "Current", &m18170_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3274_PIs[] =
{
	&t3274____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3274____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3274_m18166_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18166_GM;
MethodInfo m18166_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3274_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3274_m18166_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18166_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18167_GM;
MethodInfo m18167_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3274_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18167_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18168_GM;
MethodInfo m18168_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3274_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18168_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18169_GM;
MethodInfo m18169_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3274_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18169_GM};
extern Il2CppType t1727_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18170_GM;
MethodInfo m18170_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3274_TI, &t1727_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18170_GM};
static MethodInfo* t3274_MIs[] =
{
	&m18166_MI,
	&m18167_MI,
	&m18168_MI,
	&m18169_MI,
	&m18170_MI,
	NULL
};
extern MethodInfo m18169_MI;
extern MethodInfo m18168_MI;
static MethodInfo* t3274_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18167_MI,
	&m18169_MI,
	&m18168_MI,
	&m18170_MI,
};
static TypeInfo* t3274_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4683_TI,
};
static Il2CppInterfaceOffsetPair t3274_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4683_TI, 7},
};
extern TypeInfo t1727_TI;
static Il2CppRGCTXData t3274_RGCTXData[3] = 
{
	&m18170_MI/* Method Usage */,
	&t1727_TI/* Class Usage */,
	&m24114_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3274_0_0_0;
extern Il2CppType t3274_1_0_0;
extern Il2CppGenericClass t3274_GC;
TypeInfo t3274_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3274_MIs, t3274_PIs, t3274_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3274_TI, t3274_ITIs, t3274_VT, &EmptyCustomAttributesCache, &t3274_TI, &t3274_0_0_0, &t3274_1_0_0, t3274_IOs, &t3274_GC, NULL, NULL, NULL, t3274_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3274)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6064_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Int16>>
extern MethodInfo m31600_MI;
extern MethodInfo m31601_MI;
static PropertyInfo t6064____Item_PropertyInfo = 
{
	&t6064_TI, "Item", &m31600_MI, &m31601_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6064_PIs[] =
{
	&t6064____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1727_0_0_0;
static ParameterInfo t6064_m31602_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1727_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31602_GM;
MethodInfo m31602_MI = 
{
	"IndexOf", NULL, &t6064_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6064_m31602_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31602_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1727_0_0_0;
static ParameterInfo t6064_m31603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1727_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31603_GM;
MethodInfo m31603_MI = 
{
	"Insert", NULL, &t6064_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6064_m31603_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31603_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6064_m31604_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31604_GM;
MethodInfo m31604_MI = 
{
	"RemoveAt", NULL, &t6064_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6064_m31604_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31604_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6064_m31600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1727_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31600_GM;
MethodInfo m31600_MI = 
{
	"get_Item", NULL, &t6064_TI, &t1727_0_0_0, RuntimeInvoker_t29_t44, t6064_m31600_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31600_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1727_0_0_0;
static ParameterInfo t6064_m31601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1727_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31601_GM;
MethodInfo m31601_MI = 
{
	"set_Item", NULL, &t6064_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6064_m31601_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31601_GM};
static MethodInfo* t6064_MIs[] =
{
	&m31602_MI,
	&m31603_MI,
	&m31604_MI,
	&m31600_MI,
	&m31601_MI,
	NULL
};
static TypeInfo* t6064_ITIs[] = 
{
	&t603_TI,
	&t6063_TI,
	&t6065_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6064_0_0_0;
extern Il2CppType t6064_1_0_0;
struct t6064;
extern Il2CppGenericClass t6064_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6064_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6064_MIs, t6064_PIs, NULL, NULL, NULL, NULL, NULL, &t6064_TI, t6064_ITIs, NULL, &t1908__CustomAttributeCache, &t6064_TI, &t6064_0_0_0, &t6064_1_0_0, NULL, &t6064_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6066_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int16>>
extern MethodInfo m31605_MI;
static PropertyInfo t6066____Count_PropertyInfo = 
{
	&t6066_TI, "Count", &m31605_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31606_MI;
static PropertyInfo t6066____IsReadOnly_PropertyInfo = 
{
	&t6066_TI, "IsReadOnly", &m31606_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6066_PIs[] =
{
	&t6066____Count_PropertyInfo,
	&t6066____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31605_GM;
MethodInfo m31605_MI = 
{
	"get_Count", NULL, &t6066_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31605_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31606_GM;
MethodInfo m31606_MI = 
{
	"get_IsReadOnly", NULL, &t6066_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31606_GM};
extern Il2CppType t1728_0_0_0;
extern Il2CppType t1728_0_0_0;
static ParameterInfo t6066_m31607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1728_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31607_GM;
MethodInfo m31607_MI = 
{
	"Add", NULL, &t6066_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6066_m31607_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31607_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31608_GM;
MethodInfo m31608_MI = 
{
	"Clear", NULL, &t6066_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31608_GM};
extern Il2CppType t1728_0_0_0;
static ParameterInfo t6066_m31609_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1728_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31609_GM;
MethodInfo m31609_MI = 
{
	"Contains", NULL, &t6066_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6066_m31609_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31609_GM};
extern Il2CppType t3569_0_0_0;
extern Il2CppType t3569_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6066_m31610_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3569_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31610_GM;
MethodInfo m31610_MI = 
{
	"CopyTo", NULL, &t6066_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6066_m31610_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31610_GM};
extern Il2CppType t1728_0_0_0;
static ParameterInfo t6066_m31611_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1728_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31611_GM;
MethodInfo m31611_MI = 
{
	"Remove", NULL, &t6066_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6066_m31611_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31611_GM};
static MethodInfo* t6066_MIs[] =
{
	&m31605_MI,
	&m31606_MI,
	&m31607_MI,
	&m31608_MI,
	&m31609_MI,
	&m31610_MI,
	&m31611_MI,
	NULL
};
extern TypeInfo t6068_TI;
static TypeInfo* t6066_ITIs[] = 
{
	&t603_TI,
	&t6068_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6066_0_0_0;
extern Il2CppType t6066_1_0_0;
struct t6066;
extern Il2CppGenericClass t6066_GC;
TypeInfo t6066_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6066_MIs, t6066_PIs, NULL, NULL, NULL, NULL, NULL, &t6066_TI, t6066_ITIs, NULL, &EmptyCustomAttributesCache, &t6066_TI, &t6066_0_0_0, &t6066_1_0_0, NULL, &t6066_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Int16>>
extern Il2CppType t4685_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31612_GM;
MethodInfo m31612_MI = 
{
	"GetEnumerator", NULL, &t6068_TI, &t4685_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31612_GM};
static MethodInfo* t6068_MIs[] =
{
	&m31612_MI,
	NULL
};
static TypeInfo* t6068_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6068_0_0_0;
extern Il2CppType t6068_1_0_0;
struct t6068;
extern Il2CppGenericClass t6068_GC;
TypeInfo t6068_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6068_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6068_TI, t6068_ITIs, NULL, &EmptyCustomAttributesCache, &t6068_TI, &t6068_0_0_0, &t6068_1_0_0, NULL, &t6068_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4685_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Int16>>
extern MethodInfo m31613_MI;
static PropertyInfo t4685____Current_PropertyInfo = 
{
	&t4685_TI, "Current", &m31613_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4685_PIs[] =
{
	&t4685____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1728_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31613_GM;
MethodInfo m31613_MI = 
{
	"get_Current", NULL, &t4685_TI, &t1728_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31613_GM};
static MethodInfo* t4685_MIs[] =
{
	&m31613_MI,
	NULL
};
static TypeInfo* t4685_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4685_0_0_0;
extern Il2CppType t4685_1_0_0;
struct t4685;
extern Il2CppGenericClass t4685_GC;
TypeInfo t4685_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4685_MIs, t4685_PIs, NULL, NULL, NULL, NULL, NULL, &t4685_TI, t4685_ITIs, NULL, &EmptyCustomAttributesCache, &t4685_TI, &t4685_0_0_0, &t4685_1_0_0, NULL, &t4685_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1728_TI;



// Metadata Definition System.IEquatable`1<System.Int16>
extern Il2CppType t372_0_0_0;
static ParameterInfo t1728_m31614_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31614_GM;
MethodInfo m31614_MI = 
{
	"Equals", NULL, &t1728_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t1728_m31614_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31614_GM};
static MethodInfo* t1728_MIs[] =
{
	&m31614_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1728_1_0_0;
struct t1728;
extern Il2CppGenericClass t1728_GC;
TypeInfo t1728_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1728_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1728_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1728_TI, &t1728_0_0_0, &t1728_1_0_0, NULL, &t1728_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3275.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3275_TI;
#include "t3275MD.h"

extern MethodInfo m18175_MI;
extern MethodInfo m24125_MI;
struct t20;
#define m24125(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int16>>
extern Il2CppType t20_0_0_1;
FieldInfo t3275_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3275_TI, offsetof(t3275, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3275_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3275_TI, offsetof(t3275, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3275_FIs[] =
{
	&t3275_f0_FieldInfo,
	&t3275_f1_FieldInfo,
	NULL
};
extern MethodInfo m18172_MI;
static PropertyInfo t3275____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3275_TI, "System.Collections.IEnumerator.Current", &m18172_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3275____Current_PropertyInfo = 
{
	&t3275_TI, "Current", &m18175_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3275_PIs[] =
{
	&t3275____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3275____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3275_m18171_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18171_GM;
MethodInfo m18171_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3275_m18171_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18171_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18172_GM;
MethodInfo m18172_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3275_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18172_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18173_GM;
MethodInfo m18173_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3275_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18173_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18174_GM;
MethodInfo m18174_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3275_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18174_GM};
extern Il2CppType t1728_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18175_GM;
MethodInfo m18175_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3275_TI, &t1728_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18175_GM};
static MethodInfo* t3275_MIs[] =
{
	&m18171_MI,
	&m18172_MI,
	&m18173_MI,
	&m18174_MI,
	&m18175_MI,
	NULL
};
extern MethodInfo m18174_MI;
extern MethodInfo m18173_MI;
static MethodInfo* t3275_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18172_MI,
	&m18174_MI,
	&m18173_MI,
	&m18175_MI,
};
static TypeInfo* t3275_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4685_TI,
};
static Il2CppInterfaceOffsetPair t3275_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4685_TI, 7},
};
extern TypeInfo t1728_TI;
static Il2CppRGCTXData t3275_RGCTXData[3] = 
{
	&m18175_MI/* Method Usage */,
	&t1728_TI/* Class Usage */,
	&m24125_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3275_0_0_0;
extern Il2CppType t3275_1_0_0;
extern Il2CppGenericClass t3275_GC;
TypeInfo t3275_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3275_MIs, t3275_PIs, t3275_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3275_TI, t3275_ITIs, t3275_VT, &EmptyCustomAttributesCache, &t3275_TI, &t3275_0_0_0, &t3275_1_0_0, t3275_IOs, &t3275_GC, NULL, NULL, NULL, t3275_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3275)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6067_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Int16>>
extern MethodInfo m31615_MI;
extern MethodInfo m31616_MI;
static PropertyInfo t6067____Item_PropertyInfo = 
{
	&t6067_TI, "Item", &m31615_MI, &m31616_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6067_PIs[] =
{
	&t6067____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1728_0_0_0;
static ParameterInfo t6067_m31617_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1728_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31617_GM;
MethodInfo m31617_MI = 
{
	"IndexOf", NULL, &t6067_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6067_m31617_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31617_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1728_0_0_0;
static ParameterInfo t6067_m31618_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1728_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31618_GM;
MethodInfo m31618_MI = 
{
	"Insert", NULL, &t6067_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6067_m31618_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31618_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6067_m31619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31619_GM;
MethodInfo m31619_MI = 
{
	"RemoveAt", NULL, &t6067_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6067_m31619_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31619_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6067_m31615_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1728_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31615_GM;
MethodInfo m31615_MI = 
{
	"get_Item", NULL, &t6067_TI, &t1728_0_0_0, RuntimeInvoker_t29_t44, t6067_m31615_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31615_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1728_0_0_0;
static ParameterInfo t6067_m31616_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1728_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31616_GM;
MethodInfo m31616_MI = 
{
	"set_Item", NULL, &t6067_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6067_m31616_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31616_GM};
static MethodInfo* t6067_MIs[] =
{
	&m31617_MI,
	&m31618_MI,
	&m31619_MI,
	&m31615_MI,
	&m31616_MI,
	NULL
};
static TypeInfo* t6067_ITIs[] = 
{
	&t603_TI,
	&t6066_TI,
	&t6068_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6067_0_0_0;
extern Il2CppType t6067_1_0_0;
struct t6067;
extern Il2CppGenericClass t6067_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6067_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6067_MIs, t6067_PIs, NULL, NULL, NULL, NULL, NULL, &t6067_TI, t6067_ITIs, NULL, &t1908__CustomAttributeCache, &t6067_TI, &t6067_0_0_0, &t6067_1_0_0, NULL, &t6067_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t1741.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1741_TI;
#include "t1741MD.h"

#include "t305.h"
#include "t3282.h"
#include "t3279.h"
#include "t3280.h"
#include "t338.h"
#include "t3285.h"
#include "t3281.h"
extern TypeInfo t7_TI;
extern TypeInfo t44_TI;
extern TypeInfo t21_TI;
extern TypeInfo t305_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t446_TI;
extern TypeInfo t3282_TI;
extern TypeInfo t40_TI;
extern TypeInfo t3277_TI;
extern TypeInfo t3278_TI;
extern TypeInfo t3276_TI;
extern TypeInfo t3279_TI;
extern TypeInfo t338_TI;
extern TypeInfo t3280_TI;
extern TypeInfo t3285_TI;
#include "t305MD.h"
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t3279MD.h"
#include "t338MD.h"
#include "t3280MD.h"
#include "t3282MD.h"
#include "t3285MD.h"
extern MethodInfo m18219_MI;
extern MethodInfo m18220_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m24136_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m18207_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m18204_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m9678_MI;
extern MethodInfo m18199_MI;
extern MethodInfo m18205_MI;
extern MethodInfo m18208_MI;
extern MethodInfo m18210_MI;
extern MethodInfo m18193_MI;
extern MethodInfo m18217_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m18218_MI;
extern MethodInfo m27307_MI;
extern MethodInfo m27312_MI;
extern MethodInfo m27314_MI;
extern MethodInfo m27306_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m18209_MI;
extern MethodInfo m18194_MI;
extern MethodInfo m18195_MI;
extern MethodInfo m18227_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m24138_MI;
extern MethodInfo m18202_MI;
extern MethodInfo m18203_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m18294_MI;
extern MethodInfo m18221_MI;
extern MethodInfo m18206_MI;
extern MethodInfo m18212_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m18300_MI;
extern MethodInfo m24140_MI;
extern MethodInfo m24148_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m24136(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2901.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m24138(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m24140(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m24148(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t3282  m18204 (t1741 * __this, MethodInfo* method){
	{
		t3282  L_0 = {0};
		m18221(&L_0, __this, &m18221_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<System.String>
extern Il2CppType t44_0_0_32849;
FieldInfo t1741_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t1741_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t1741_f1_FieldInfo = 
{
	"_items", &t446_0_0_1, &t1741_TI, offsetof(t1741, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1741_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t1741_TI, offsetof(t1741, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1741_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t1741_TI, offsetof(t1741, f3), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_49;
FieldInfo t1741_f4_FieldInfo = 
{
	"EmptyArray", &t446_0_0_49, &t1741_TI, offsetof(t1741_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1741_FIs[] =
{
	&t1741_f0_FieldInfo,
	&t1741_f1_FieldInfo,
	&t1741_f2_FieldInfo,
	&t1741_f3_FieldInfo,
	&t1741_f4_FieldInfo,
	NULL
};
static const int32_t t1741_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1741_f0_DefaultValue = 
{
	&t1741_f0_FieldInfo, { (char*)&t1741_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1741_FDVs[] = 
{
	&t1741_f0_DefaultValue,
	NULL
};
extern MethodInfo m18186_MI;
static PropertyInfo t1741____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t1741_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m18186_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18187_MI;
static PropertyInfo t1741____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t1741_TI, "System.Collections.ICollection.IsSynchronized", &m18187_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18188_MI;
static PropertyInfo t1741____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t1741_TI, "System.Collections.ICollection.SyncRoot", &m18188_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18189_MI;
static PropertyInfo t1741____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t1741_TI, "System.Collections.IList.IsFixedSize", &m18189_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18190_MI;
static PropertyInfo t1741____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t1741_TI, "System.Collections.IList.IsReadOnly", &m18190_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18191_MI;
extern MethodInfo m18192_MI;
static PropertyInfo t1741____System_Collections_IList_Item_PropertyInfo = 
{
	&t1741_TI, "System.Collections.IList.Item", &m18191_MI, &m18192_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1741____Capacity_PropertyInfo = 
{
	&t1741_TI, "Capacity", &m18217_MI, &m18218_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m9677_MI;
static PropertyInfo t1741____Count_PropertyInfo = 
{
	&t1741_TI, "Count", &m9677_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1741____Item_PropertyInfo = 
{
	&t1741_TI, "Item", &m18219_MI, &m18220_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1741_PIs[] =
{
	&t1741____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t1741____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t1741____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t1741____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t1741____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t1741____System_Collections_IList_Item_PropertyInfo,
	&t1741____Capacity_PropertyInfo,
	&t1741____Count_PropertyInfo,
	&t1741____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m9676_GM;
MethodInfo m9676_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m9676_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18176_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18176_GM;
MethodInfo m18176_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1741_m18176_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18176_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18177_GM;
MethodInfo m18177_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18177_GM};
extern Il2CppType t3276_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18178_GM;
MethodInfo m18178_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t1741_TI, &t3276_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18178_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18179_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18179_GM;
MethodInfo m18179_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t1741_m18179_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18179_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18180_GM;
MethodInfo m18180_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t1741_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18180_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1741_m18181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18181_GM;
MethodInfo m18181_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1741_m18181_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18181_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1741_m18182_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18182_GM;
MethodInfo m18182_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1741_m18182_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18182_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1741_m18183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18183_GM;
MethodInfo m18183_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1741_m18183_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18183_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1741_m18184_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18184_GM;
MethodInfo m18184_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1741_m18184_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18184_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1741_m18185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18185_GM;
MethodInfo m18185_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18185_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18185_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18186_GM;
MethodInfo m18186_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18186_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18187_GM;
MethodInfo m18187_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18187_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18188_GM;
MethodInfo m18188_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t1741_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18188_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18189_GM;
MethodInfo m18189_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18189_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18190_GM;
MethodInfo m18190_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18190_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18191_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18191_GM;
MethodInfo m18191_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t1741_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t1741_m18191_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18191_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1741_m18192_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18192_GM;
MethodInfo m18192_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1741_m18192_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18192_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1741_m9678_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m9678_GM;
MethodInfo m9678_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m9678_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m9678_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18193_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18193_GM;
MethodInfo m18193_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1741_m18193_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18193_GM};
extern Il2CppType t3277_0_0_0;
extern Il2CppType t3277_0_0_0;
static ParameterInfo t1741_m18194_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3277_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18194_GM;
MethodInfo m18194_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18194_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18194_GM};
extern Il2CppType t3278_0_0_0;
extern Il2CppType t3278_0_0_0;
static ParameterInfo t1741_m18195_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18195_GM;
MethodInfo m18195_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18195_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18195_GM};
extern Il2CppType t3278_0_0_0;
static ParameterInfo t1741_m18196_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18196_GM;
MethodInfo m18196_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18196_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18196_GM};
extern Il2CppType t3279_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18197_GM;
MethodInfo m18197_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t1741_TI, &t3279_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18197_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18198_GM;
MethodInfo m18198_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18198_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1741_m18199_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18199_GM;
MethodInfo m18199_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1741_m18199_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18199_GM};
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18200_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18200_GM;
MethodInfo m18200_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t1741_m18200_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18200_GM};
extern Il2CppType t3280_0_0_0;
extern Il2CppType t3280_0_0_0;
static ParameterInfo t1741_m18201_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3280_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18201_GM;
MethodInfo m18201_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t1741_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1741_m18201_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18201_GM};
extern Il2CppType t3280_0_0_0;
static ParameterInfo t1741_m18202_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3280_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18202_GM;
MethodInfo m18202_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18202_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18202_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3280_0_0_0;
static ParameterInfo t1741_m18203_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t3280_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18203_GM;
MethodInfo m18203_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t1741_m18203_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m18203_GM};
extern Il2CppType t3282_0_0_0;
extern void* RuntimeInvoker_t3282 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18204_GM;
MethodInfo m18204_MI = 
{
	"GetEnumerator", (methodPointerType)&m18204, &t1741_TI, &t3282_0_0_0, RuntimeInvoker_t3282, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18204_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1741_m18205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18205_GM;
MethodInfo m18205_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1741_m18205_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18205_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18206_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18206_GM;
MethodInfo m18206_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t1741_m18206_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18206_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18207_GM;
MethodInfo m18207_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1741_m18207_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18207_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1741_m18208_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18208_GM;
MethodInfo m18208_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1741_m18208_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18208_GM};
extern Il2CppType t3278_0_0_0;
static ParameterInfo t1741_m18209_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18209_GM;
MethodInfo m18209_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18209_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18209_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1741_m18210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18210_GM;
MethodInfo m18210_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t1741_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1741_m18210_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18210_GM};
extern Il2CppType t3280_0_0_0;
static ParameterInfo t1741_m18211_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3280_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18211_GM;
MethodInfo m18211_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1741_m18211_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18211_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18212_GM;
MethodInfo m18212_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1741_m18212_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18212_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18213_GM;
MethodInfo m18213_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18213_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18214_GM;
MethodInfo m18214_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18214_GM};
extern Il2CppType t3281_0_0_0;
extern Il2CppType t3281_0_0_0;
static ParameterInfo t1741_m18215_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3281_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18215_GM;
MethodInfo m18215_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1741_m18215_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18215_GM};
extern Il2CppType t446_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m9679_GM;
MethodInfo m9679_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t1741_TI, &t446_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m9679_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18216_GM;
MethodInfo m18216_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18216_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18217_GM;
MethodInfo m18217_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18217_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18218_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18218_GM;
MethodInfo m18218_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1741_m18218_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18218_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m9677_GM;
MethodInfo m9677_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t1741_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m9677_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1741_m18219_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18219_GM;
MethodInfo m18219_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t1741_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t1741_m18219_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18219_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1741_m18220_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18220_GM;
MethodInfo m18220_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t1741_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1741_m18220_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18220_GM};
static MethodInfo* t1741_MIs[] =
{
	&m9676_MI,
	&m18176_MI,
	&m18177_MI,
	&m18178_MI,
	&m18179_MI,
	&m18180_MI,
	&m18181_MI,
	&m18182_MI,
	&m18183_MI,
	&m18184_MI,
	&m18185_MI,
	&m18186_MI,
	&m18187_MI,
	&m18188_MI,
	&m18189_MI,
	&m18190_MI,
	&m18191_MI,
	&m18192_MI,
	&m9678_MI,
	&m18193_MI,
	&m18194_MI,
	&m18195_MI,
	&m18196_MI,
	&m18197_MI,
	&m18198_MI,
	&m18199_MI,
	&m18200_MI,
	&m18201_MI,
	&m18202_MI,
	&m18203_MI,
	&m18204_MI,
	&m18205_MI,
	&m18206_MI,
	&m18207_MI,
	&m18208_MI,
	&m18209_MI,
	&m18210_MI,
	&m18211_MI,
	&m18212_MI,
	&m18213_MI,
	&m18214_MI,
	&m18215_MI,
	&m9679_MI,
	&m18216_MI,
	&m18217_MI,
	&m18218_MI,
	&m9677_MI,
	&m18219_MI,
	&m18220_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m18180_MI;
extern MethodInfo m18179_MI;
extern MethodInfo m18181_MI;
extern MethodInfo m18198_MI;
extern MethodInfo m18182_MI;
extern MethodInfo m18183_MI;
extern MethodInfo m18184_MI;
extern MethodInfo m18185_MI;
extern MethodInfo m18200_MI;
extern MethodInfo m18178_MI;
static MethodInfo* t1741_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m18180_MI,
	&m9677_MI,
	&m18187_MI,
	&m18188_MI,
	&m18179_MI,
	&m18189_MI,
	&m18190_MI,
	&m18191_MI,
	&m18192_MI,
	&m18181_MI,
	&m18198_MI,
	&m18182_MI,
	&m18183_MI,
	&m18184_MI,
	&m18185_MI,
	&m18212_MI,
	&m9677_MI,
	&m18186_MI,
	&m9678_MI,
	&m18198_MI,
	&m18199_MI,
	&m18200_MI,
	&m18210_MI,
	&m18178_MI,
	&m18205_MI,
	&m18208_MI,
	&m18212_MI,
	&m18219_MI,
	&m18220_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t3283_TI;
static TypeInfo* t1741_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3277_TI,
	&t3278_TI,
	&t3283_TI,
};
static Il2CppInterfaceOffsetPair t1741_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3277_TI, 20},
	{ &t3278_TI, 27},
	{ &t3283_TI, 28},
};
extern TypeInfo t1741_TI;
extern TypeInfo t446_TI;
extern TypeInfo t3282_TI;
extern TypeInfo t7_TI;
extern TypeInfo t3277_TI;
extern TypeInfo t3279_TI;
static Il2CppRGCTXData t1741_RGCTXData[37] = 
{
	&t1741_TI/* Static Usage */,
	&t446_TI/* Array Usage */,
	&m18204_MI/* Method Usage */,
	&t3282_TI/* Class Usage */,
	&t7_TI/* Class Usage */,
	&m9678_MI/* Method Usage */,
	&m18199_MI/* Method Usage */,
	&m18205_MI/* Method Usage */,
	&m18207_MI/* Method Usage */,
	&m18208_MI/* Method Usage */,
	&m18210_MI/* Method Usage */,
	&m18219_MI/* Method Usage */,
	&m18220_MI/* Method Usage */,
	&m18193_MI/* Method Usage */,
	&m18217_MI/* Method Usage */,
	&m18218_MI/* Method Usage */,
	&m27307_MI/* Method Usage */,
	&m27312_MI/* Method Usage */,
	&m27314_MI/* Method Usage */,
	&m27306_MI/* Method Usage */,
	&m18209_MI/* Method Usage */,
	&t3277_TI/* Class Usage */,
	&m18194_MI/* Method Usage */,
	&m18195_MI/* Method Usage */,
	&t3279_TI/* Class Usage */,
	&m18227_MI/* Method Usage */,
	&m24138_MI/* Method Usage */,
	&m18202_MI/* Method Usage */,
	&m18203_MI/* Method Usage */,
	&m18294_MI/* Method Usage */,
	&m18221_MI/* Method Usage */,
	&m18206_MI/* Method Usage */,
	&m18212_MI/* Method Usage */,
	&m18300_MI/* Method Usage */,
	&m24140_MI/* Method Usage */,
	&m24148_MI/* Method Usage */,
	&m24136_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1741_0_0_0;
extern Il2CppType t1741_1_0_0;
extern TypeInfo t29_TI;
struct t1741;
extern Il2CppGenericClass t1741_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t1741_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t1741_MIs, t1741_PIs, t1741_FIs, NULL, &t29_TI, NULL, NULL, &t1741_TI, t1741_ITIs, t1741_VT, &t1261__CustomAttributeCache, &t1741_TI, &t1741_0_0_0, &t1741_1_0_0, t1741_IOs, &t1741_GC, NULL, t1741_FDVs, NULL, t1741_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1741), 0, -1, sizeof(t1741_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t42.h"
#include "t1101.h"
extern TypeInfo t42_TI;
extern TypeInfo t1101_TI;
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m18224_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<System.String>
extern Il2CppType t1741_0_0_1;
FieldInfo t3282_f0_FieldInfo = 
{
	"l", &t1741_0_0_1, &t3282_TI, offsetof(t3282, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3282_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3282_TI, offsetof(t3282, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3282_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t3282_TI, offsetof(t3282, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t3282_f3_FieldInfo = 
{
	"current", &t7_0_0_1, &t3282_TI, offsetof(t3282, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3282_FIs[] =
{
	&t3282_f0_FieldInfo,
	&t3282_f1_FieldInfo,
	&t3282_f2_FieldInfo,
	&t3282_f3_FieldInfo,
	NULL
};
extern MethodInfo m18222_MI;
static PropertyInfo t3282____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3282_TI, "System.Collections.IEnumerator.Current", &m18222_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18226_MI;
static PropertyInfo t3282____Current_PropertyInfo = 
{
	&t3282_TI, "Current", &m18226_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3282_PIs[] =
{
	&t3282____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3282____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1741_0_0_0;
static ParameterInfo t3282_m18221_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t1741_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18221_GM;
MethodInfo m18221_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t3282_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3282_m18221_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18221_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18222_GM;
MethodInfo m18222_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t3282_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18222_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18223_GM;
MethodInfo m18223_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t3282_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18223_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18224_GM;
MethodInfo m18224_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t3282_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18224_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18225_GM;
MethodInfo m18225_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t3282_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18225_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18226_GM;
MethodInfo m18226_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t3282_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18226_GM};
static MethodInfo* t3282_MIs[] =
{
	&m18221_MI,
	&m18222_MI,
	&m18223_MI,
	&m18224_MI,
	&m18225_MI,
	&m18226_MI,
	NULL
};
extern MethodInfo m18225_MI;
extern MethodInfo m18223_MI;
static MethodInfo* t3282_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18222_MI,
	&m18225_MI,
	&m18223_MI,
	&m18226_MI,
};
static TypeInfo* t3282_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3276_TI,
};
static Il2CppInterfaceOffsetPair t3282_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3276_TI, 7},
};
extern TypeInfo t7_TI;
extern TypeInfo t3282_TI;
static Il2CppRGCTXData t3282_RGCTXData[3] = 
{
	&m18224_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&t3282_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3282_0_0_0;
extern Il2CppType t3282_1_0_0;
extern Il2CppGenericClass t3282_GC;
extern TypeInfo t1261_TI;
TypeInfo t3282_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3282_MIs, t3282_PIs, t3282_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t3282_TI, t3282_ITIs, t3282_VT, &EmptyCustomAttributesCache, &t3282_TI, &t3282_0_0_0, &t3282_1_0_0, t3282_IOs, &t3282_GC, NULL, NULL, NULL, t3282_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3282)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t3284MD.h"
extern MethodInfo m18256_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m27315_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m18288_MI;
extern MethodInfo m27311_MI;
extern MethodInfo m27317_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
extern Il2CppType t3283_0_0_1;
FieldInfo t3279_f0_FieldInfo = 
{
	"list", &t3283_0_0_1, &t3279_TI, offsetof(t3279, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3279_FIs[] =
{
	&t3279_f0_FieldInfo,
	NULL
};
extern MethodInfo m18233_MI;
extern MethodInfo m18234_MI;
static PropertyInfo t3279____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3279_TI, "System.Collections.Generic.IList<T>.Item", &m18233_MI, &m18234_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18235_MI;
static PropertyInfo t3279____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3279_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m18235_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18245_MI;
static PropertyInfo t3279____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3279_TI, "System.Collections.ICollection.IsSynchronized", &m18245_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18246_MI;
static PropertyInfo t3279____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3279_TI, "System.Collections.ICollection.SyncRoot", &m18246_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18247_MI;
static PropertyInfo t3279____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3279_TI, "System.Collections.IList.IsFixedSize", &m18247_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18248_MI;
static PropertyInfo t3279____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3279_TI, "System.Collections.IList.IsReadOnly", &m18248_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18249_MI;
extern MethodInfo m18250_MI;
static PropertyInfo t3279____System_Collections_IList_Item_PropertyInfo = 
{
	&t3279_TI, "System.Collections.IList.Item", &m18249_MI, &m18250_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18255_MI;
static PropertyInfo t3279____Count_PropertyInfo = 
{
	&t3279_TI, "Count", &m18255_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3279____Item_PropertyInfo = 
{
	&t3279_TI, "Item", &m18256_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3279_PIs[] =
{
	&t3279____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3279____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3279____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3279____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3279____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3279____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3279____System_Collections_IList_Item_PropertyInfo,
	&t3279____Count_PropertyInfo,
	&t3279____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3283_0_0_0;
extern Il2CppType t3283_0_0_0;
static ParameterInfo t3279_m18227_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18227_GM;
MethodInfo m18227_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3279_m18227_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18227_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3279_m18228_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18228_GM;
MethodInfo m18228_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3279_m18228_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18228_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18229_GM;
MethodInfo m18229_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18229_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3279_m18230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18230_GM;
MethodInfo m18230_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3279_m18230_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18230_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3279_m18231_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18231_GM;
MethodInfo m18231_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3279_m18231_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18231_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18232_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18232_GM;
MethodInfo m18232_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3279_m18232_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18232_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18233_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18233_GM;
MethodInfo m18233_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t3279_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t3279_m18233_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18233_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3279_m18234_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18234_GM;
MethodInfo m18234_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3279_m18234_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18234_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18235_GM;
MethodInfo m18235_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18235_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18236_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18236_GM;
MethodInfo m18236_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3279_m18236_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18236_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18237_GM;
MethodInfo m18237_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t3279_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18237_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3279_m18238_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18238_GM;
MethodInfo m18238_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t3279_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3279_m18238_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18238_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18239_GM;
MethodInfo m18239_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18239_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3279_m18240_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18240_GM;
MethodInfo m18240_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3279_m18240_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18240_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3279_m18241_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18241_GM;
MethodInfo m18241_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t3279_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3279_m18241_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18241_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3279_m18242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18242_GM;
MethodInfo m18242_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3279_m18242_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18242_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3279_m18243_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18243_GM;
MethodInfo m18243_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3279_m18243_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18243_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18244_GM;
MethodInfo m18244_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3279_m18244_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18244_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18245_GM;
MethodInfo m18245_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18245_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18246_GM;
MethodInfo m18246_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t3279_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18246_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18247_GM;
MethodInfo m18247_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18247_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18248_GM;
MethodInfo m18248_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18248_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18249_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18249_GM;
MethodInfo m18249_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t3279_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3279_m18249_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18249_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3279_m18250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18250_GM;
MethodInfo m18250_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3279_m18250_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18250_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3279_m18251_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18251_GM;
MethodInfo m18251_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t3279_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3279_m18251_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18251_GM};
extern Il2CppType t446_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18252_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18252_GM;
MethodInfo m18252_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t3279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3279_m18252_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18252_GM};
extern Il2CppType t3276_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18253_GM;
MethodInfo m18253_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t3279_TI, &t3276_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18253_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3279_m18254_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18254_GM;
MethodInfo m18254_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t3279_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3279_m18254_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18254_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18255_GM;
MethodInfo m18255_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t3279_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18255_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3279_m18256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18256_GM;
MethodInfo m18256_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t3279_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t3279_m18256_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18256_GM};
static MethodInfo* t3279_MIs[] =
{
	&m18227_MI,
	&m18228_MI,
	&m18229_MI,
	&m18230_MI,
	&m18231_MI,
	&m18232_MI,
	&m18233_MI,
	&m18234_MI,
	&m18235_MI,
	&m18236_MI,
	&m18237_MI,
	&m18238_MI,
	&m18239_MI,
	&m18240_MI,
	&m18241_MI,
	&m18242_MI,
	&m18243_MI,
	&m18244_MI,
	&m18245_MI,
	&m18246_MI,
	&m18247_MI,
	&m18248_MI,
	&m18249_MI,
	&m18250_MI,
	&m18251_MI,
	&m18252_MI,
	&m18253_MI,
	&m18254_MI,
	&m18255_MI,
	&m18256_MI,
	NULL
};
extern MethodInfo m18237_MI;
extern MethodInfo m18236_MI;
extern MethodInfo m18238_MI;
extern MethodInfo m18239_MI;
extern MethodInfo m18240_MI;
extern MethodInfo m18241_MI;
extern MethodInfo m18242_MI;
extern MethodInfo m18243_MI;
extern MethodInfo m18244_MI;
extern MethodInfo m18228_MI;
extern MethodInfo m18229_MI;
extern MethodInfo m18251_MI;
extern MethodInfo m18252_MI;
extern MethodInfo m18231_MI;
extern MethodInfo m18254_MI;
extern MethodInfo m18230_MI;
extern MethodInfo m18232_MI;
extern MethodInfo m18253_MI;
static MethodInfo* t3279_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m18237_MI,
	&m18255_MI,
	&m18245_MI,
	&m18246_MI,
	&m18236_MI,
	&m18247_MI,
	&m18248_MI,
	&m18249_MI,
	&m18250_MI,
	&m18238_MI,
	&m18239_MI,
	&m18240_MI,
	&m18241_MI,
	&m18242_MI,
	&m18243_MI,
	&m18244_MI,
	&m18255_MI,
	&m18235_MI,
	&m18228_MI,
	&m18229_MI,
	&m18251_MI,
	&m18252_MI,
	&m18231_MI,
	&m18254_MI,
	&m18230_MI,
	&m18232_MI,
	&m18233_MI,
	&m18234_MI,
	&m18253_MI,
	&m18256_MI,
};
static TypeInfo* t3279_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3277_TI,
	&t3283_TI,
	&t3278_TI,
};
static Il2CppInterfaceOffsetPair t3279_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3277_TI, 20},
	{ &t3283_TI, 27},
	{ &t3278_TI, 32},
};
extern TypeInfo t7_TI;
static Il2CppRGCTXData t3279_RGCTXData[9] = 
{
	&m18256_MI/* Method Usage */,
	&m18288_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m27311_MI/* Method Usage */,
	&m27317_MI/* Method Usage */,
	&m27315_MI/* Method Usage */,
	&m27312_MI/* Method Usage */,
	&m27314_MI/* Method Usage */,
	&m27307_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3279_0_0_0;
extern Il2CppType t3279_1_0_0;
struct t3279;
extern Il2CppGenericClass t3279_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3279_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3279_MIs, t3279_PIs, t3279_FIs, NULL, &t29_TI, NULL, NULL, &t3279_TI, t3279_ITIs, t3279_VT, &t1263__CustomAttributeCache, &t3279_TI, &t3279_0_0_0, &t3279_1_0_0, t3279_IOs, &t3279_GC, NULL, NULL, NULL, t3279_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3279), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3284.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3284_TI;

#include "t43.h"
extern MethodInfo m27308_MI;
extern MethodInfo m18291_MI;
extern MethodInfo m18292_MI;
extern MethodInfo m18289_MI;
extern MethodInfo m18287_MI;
extern MethodInfo m9676_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m18280_MI;
extern MethodInfo m18290_MI;
extern MethodInfo m18278_MI;
extern MethodInfo m18283_MI;
extern MethodInfo m18274_MI;
extern MethodInfo m27310_MI;
extern MethodInfo m27318_MI;
extern MethodInfo m27319_MI;
extern MethodInfo m27316_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<System.String>
extern Il2CppType t3283_0_0_1;
FieldInfo t3284_f0_FieldInfo = 
{
	"list", &t3283_0_0_1, &t3284_TI, offsetof(t3284, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3284_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3284_TI, offsetof(t3284, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3284_FIs[] =
{
	&t3284_f0_FieldInfo,
	&t3284_f1_FieldInfo,
	NULL
};
extern MethodInfo m18258_MI;
static PropertyInfo t3284____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3284_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m18258_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18266_MI;
static PropertyInfo t3284____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3284_TI, "System.Collections.ICollection.IsSynchronized", &m18266_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18267_MI;
static PropertyInfo t3284____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3284_TI, "System.Collections.ICollection.SyncRoot", &m18267_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18268_MI;
static PropertyInfo t3284____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3284_TI, "System.Collections.IList.IsFixedSize", &m18268_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18269_MI;
static PropertyInfo t3284____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3284_TI, "System.Collections.IList.IsReadOnly", &m18269_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18270_MI;
extern MethodInfo m18271_MI;
static PropertyInfo t3284____System_Collections_IList_Item_PropertyInfo = 
{
	&t3284_TI, "System.Collections.IList.Item", &m18270_MI, &m18271_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18284_MI;
static PropertyInfo t3284____Count_PropertyInfo = 
{
	&t3284_TI, "Count", &m18284_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m18285_MI;
extern MethodInfo m18286_MI;
static PropertyInfo t3284____Item_PropertyInfo = 
{
	&t3284_TI, "Item", &m18285_MI, &m18286_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3284_PIs[] =
{
	&t3284____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3284____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3284____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3284____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3284____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3284____System_Collections_IList_Item_PropertyInfo,
	&t3284____Count_PropertyInfo,
	&t3284____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18257_GM;
MethodInfo m18257_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18257_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18258_GM;
MethodInfo m18258_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18258_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3284_m18259_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18259_GM;
MethodInfo m18259_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3284_m18259_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18259_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18260_GM;
MethodInfo m18260_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t3284_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18260_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18261_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18261_GM;
MethodInfo m18261_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t3284_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3284_m18261_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18261_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18262_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18262_GM;
MethodInfo m18262_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3284_m18262_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18262_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18263_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18263_GM;
MethodInfo m18263_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t3284_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3284_m18263_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18263_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18264_GM;
MethodInfo m18264_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3284_m18264_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18264_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18265_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18265_GM;
MethodInfo m18265_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3284_m18265_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18265_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18266_GM;
MethodInfo m18266_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18266_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18267_GM;
MethodInfo m18267_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t3284_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18267_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18268_GM;
MethodInfo m18268_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18268_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18269_GM;
MethodInfo m18269_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18269_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3284_m18270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18270_GM;
MethodInfo m18270_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t3284_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3284_m18270_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18270_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18271_GM;
MethodInfo m18271_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3284_m18271_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18271_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18272_GM;
MethodInfo m18272_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3284_m18272_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18272_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18273_GM;
MethodInfo m18273_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18273_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18274_GM;
MethodInfo m18274_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18274_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18275_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18275_GM;
MethodInfo m18275_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3284_m18275_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18275_GM};
extern Il2CppType t446_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3284_m18276_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18276_GM;
MethodInfo m18276_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3284_m18276_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18276_GM};
extern Il2CppType t3276_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18277_GM;
MethodInfo m18277_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t3284_TI, &t3276_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18277_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18278_GM;
MethodInfo m18278_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t3284_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3284_m18278_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18278_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18279_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18279_GM;
MethodInfo m18279_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3284_m18279_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18279_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18280_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18280_GM;
MethodInfo m18280_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3284_m18280_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18280_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18281_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18281_GM;
MethodInfo m18281_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3284_m18281_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18281_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3284_m18282_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18282_GM;
MethodInfo m18282_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3284_m18282_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18282_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3284_m18283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18283_GM;
MethodInfo m18283_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3284_m18283_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18283_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18284_GM;
MethodInfo m18284_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t3284_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18284_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3284_m18285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18285_GM;
MethodInfo m18285_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t3284_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t3284_m18285_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18285_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18286_GM;
MethodInfo m18286_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3284_m18286_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18286_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3284_m18287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18287_GM;
MethodInfo m18287_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3284_m18287_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18287_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18288_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18288_GM;
MethodInfo m18288_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3284_m18288_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18288_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3284_m18289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18289_GM;
MethodInfo m18289_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t3284_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t3284_m18289_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18289_GM};
extern Il2CppType t3283_0_0_0;
static ParameterInfo t3284_m18290_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3283_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18290_GM;
MethodInfo m18290_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t3284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3284_m18290_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18290_GM};
extern Il2CppType t3283_0_0_0;
static ParameterInfo t3284_m18291_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3283_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18291_GM;
MethodInfo m18291_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3284_m18291_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18291_GM};
extern Il2CppType t3283_0_0_0;
static ParameterInfo t3284_m18292_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3283_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18292_GM;
MethodInfo m18292_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t3284_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3284_m18292_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18292_GM};
static MethodInfo* t3284_MIs[] =
{
	&m18257_MI,
	&m18258_MI,
	&m18259_MI,
	&m18260_MI,
	&m18261_MI,
	&m18262_MI,
	&m18263_MI,
	&m18264_MI,
	&m18265_MI,
	&m18266_MI,
	&m18267_MI,
	&m18268_MI,
	&m18269_MI,
	&m18270_MI,
	&m18271_MI,
	&m18272_MI,
	&m18273_MI,
	&m18274_MI,
	&m18275_MI,
	&m18276_MI,
	&m18277_MI,
	&m18278_MI,
	&m18279_MI,
	&m18280_MI,
	&m18281_MI,
	&m18282_MI,
	&m18283_MI,
	&m18284_MI,
	&m18285_MI,
	&m18286_MI,
	&m18287_MI,
	&m18288_MI,
	&m18289_MI,
	&m18290_MI,
	&m18291_MI,
	&m18292_MI,
	NULL
};
extern MethodInfo m18260_MI;
extern MethodInfo m18259_MI;
extern MethodInfo m18261_MI;
extern MethodInfo m18273_MI;
extern MethodInfo m18262_MI;
extern MethodInfo m18263_MI;
extern MethodInfo m18264_MI;
extern MethodInfo m18265_MI;
extern MethodInfo m18282_MI;
extern MethodInfo m18272_MI;
extern MethodInfo m18275_MI;
extern MethodInfo m18276_MI;
extern MethodInfo m18281_MI;
extern MethodInfo m18279_MI;
extern MethodInfo m18277_MI;
static MethodInfo* t3284_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m18260_MI,
	&m18284_MI,
	&m18266_MI,
	&m18267_MI,
	&m18259_MI,
	&m18268_MI,
	&m18269_MI,
	&m18270_MI,
	&m18271_MI,
	&m18261_MI,
	&m18273_MI,
	&m18262_MI,
	&m18263_MI,
	&m18264_MI,
	&m18265_MI,
	&m18282_MI,
	&m18284_MI,
	&m18258_MI,
	&m18272_MI,
	&m18273_MI,
	&m18275_MI,
	&m18276_MI,
	&m18281_MI,
	&m18278_MI,
	&m18279_MI,
	&m18282_MI,
	&m18285_MI,
	&m18286_MI,
	&m18277_MI,
	&m18274_MI,
	&m18280_MI,
	&m18283_MI,
	&m18287_MI,
};
static TypeInfo* t3284_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3277_TI,
	&t3283_TI,
	&t3278_TI,
};
static Il2CppInterfaceOffsetPair t3284_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3277_TI, 20},
	{ &t3283_TI, 27},
	{ &t3278_TI, 32},
};
extern TypeInfo t1741_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t3284_RGCTXData[25] = 
{
	&t1741_TI/* Class Usage */,
	&m9676_MI/* Method Usage */,
	&m27308_MI/* Method Usage */,
	&m27314_MI/* Method Usage */,
	&m27307_MI/* Method Usage */,
	&m18289_MI/* Method Usage */,
	&m18280_MI/* Method Usage */,
	&m18288_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m27311_MI/* Method Usage */,
	&m27317_MI/* Method Usage */,
	&m18290_MI/* Method Usage */,
	&m18278_MI/* Method Usage */,
	&m18283_MI/* Method Usage */,
	&m18291_MI/* Method Usage */,
	&m18292_MI/* Method Usage */,
	&m27315_MI/* Method Usage */,
	&m18287_MI/* Method Usage */,
	&m18274_MI/* Method Usage */,
	&m27310_MI/* Method Usage */,
	&m27312_MI/* Method Usage */,
	&m27318_MI/* Method Usage */,
	&m27319_MI/* Method Usage */,
	&m27316_MI/* Method Usage */,
	&t7_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3284_0_0_0;
extern Il2CppType t3284_1_0_0;
struct t3284;
extern Il2CppGenericClass t3284_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3284_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3284_MIs, t3284_PIs, t3284_FIs, NULL, &t29_TI, NULL, NULL, &t3284_TI, t3284_ITIs, t3284_VT, &t1262__CustomAttributeCache, &t3284_TI, &t3284_0_0_0, &t3284_1_0_0, t3284_IOs, &t3284_GC, NULL, NULL, NULL, t3284_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3284), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Predicate`1<System.String>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3280_m18293_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18293_GM;
MethodInfo m18293_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t3280_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3280_m18293_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18293_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3280_m18294_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18294_GM;
MethodInfo m18294_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t3280_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3280_m18294_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18294_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3280_m18295_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18295_GM;
MethodInfo m18295_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t3280_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3280_m18295_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m18295_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3280_m18296_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18296_GM;
MethodInfo m18296_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t3280_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3280_m18296_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18296_GM};
static MethodInfo* t3280_MIs[] =
{
	&m18293_MI,
	&m18294_MI,
	&m18295_MI,
	&m18296_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m18295_MI;
extern MethodInfo m18296_MI;
static MethodInfo* t3280_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m18294_MI,
	&m18295_MI,
	&m18296_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t3280_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3280_1_0_0;
extern TypeInfo t195_TI;
struct t3280;
extern Il2CppGenericClass t3280_GC;
TypeInfo t3280_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t3280_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3280_TI, NULL, t3280_VT, &EmptyCustomAttributesCache, &t3280_TI, &t3280_0_0_0, &t3280_1_0_0, t3280_IOs, &t3280_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3280), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t3286.h"
extern TypeInfo t1745_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3286_TI;
#include "t931MD.h"
#include "t3286MD.h"
extern Il2CppType t1745_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m18301_MI;
extern MethodInfo m31620_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<System.String>
extern Il2CppType t3285_0_0_49;
FieldInfo t3285_f0_FieldInfo = 
{
	"_default", &t3285_0_0_49, &t3285_TI, offsetof(t3285_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3285_FIs[] =
{
	&t3285_f0_FieldInfo,
	NULL
};
static PropertyInfo t3285____Default_PropertyInfo = 
{
	&t3285_TI, "Default", &m18300_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3285_PIs[] =
{
	&t3285____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18297_GM;
MethodInfo m18297_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t3285_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18297_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18298_GM;
MethodInfo m18298_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t3285_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18298_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3285_m18299_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18299_GM;
MethodInfo m18299_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t3285_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3285_m18299_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18299_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3285_m31620_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31620_GM;
MethodInfo m31620_MI = 
{
	"Compare", NULL, &t3285_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3285_m31620_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31620_GM};
extern Il2CppType t3285_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18300_GM;
MethodInfo m18300_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t3285_TI, &t3285_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18300_GM};
static MethodInfo* t3285_MIs[] =
{
	&m18297_MI,
	&m18298_MI,
	&m18299_MI,
	&m31620_MI,
	&m18300_MI,
	NULL
};
extern MethodInfo m18299_MI;
static MethodInfo* t3285_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m31620_MI,
	&m18299_MI,
	NULL,
};
extern TypeInfo t2095_TI;
extern TypeInfo t726_TI;
static TypeInfo* t3285_ITIs[] = 
{
	&t2095_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3285_IOs[] = 
{
	{ &t2095_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3285_TI;
extern TypeInfo t3285_TI;
extern TypeInfo t3286_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t3285_RGCTXData[8] = 
{
	&t1745_0_0_0/* Type Usage */,
	&t7_0_0_0/* Type Usage */,
	&t3285_TI/* Class Usage */,
	&t3285_TI/* Static Usage */,
	&t3286_TI/* Class Usage */,
	&m18301_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m31620_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3285_0_0_0;
extern Il2CppType t3285_1_0_0;
struct t3285;
extern Il2CppGenericClass t3285_GC;
TypeInfo t3285_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3285_MIs, t3285_PIs, t3285_FIs, NULL, &t29_TI, NULL, NULL, &t3285_TI, t3285_ITIs, t3285_VT, &EmptyCustomAttributesCache, &t3285_TI, &t3285_0_0_0, &t3285_1_0_0, t3285_IOs, &t3285_GC, NULL, NULL, NULL, t3285_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3285), 0, -1, sizeof(t3285_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.String>
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t2095_m24145_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m24145_GM;
MethodInfo m24145_MI = 
{
	"Compare", NULL, &t2095_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2095_m24145_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m24145_GM};
static MethodInfo* t2095_MIs[] =
{
	&m24145_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2095_0_0_0;
extern Il2CppType t2095_1_0_0;
struct t2095;
extern Il2CppGenericClass t2095_GC;
TypeInfo t2095_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t2095_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2095_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2095_TI, &t2095_0_0_0, &t2095_1_0_0, NULL, &t2095_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m18297_MI;
extern MethodInfo m24146_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.String>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18301_GM;
MethodInfo m18301_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t3286_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18301_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3286_m18302_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18302_GM;
MethodInfo m18302_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t3286_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3286_m18302_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18302_GM};
static MethodInfo* t3286_MIs[] =
{
	&m18301_MI,
	&m18302_MI,
	NULL
};
extern MethodInfo m18302_MI;
static MethodInfo* t3286_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m18302_MI,
	&m18299_MI,
	&m18302_MI,
};
static Il2CppInterfaceOffsetPair t3286_IOs[] = 
{
	{ &t2095_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3285_TI;
extern TypeInfo t3285_TI;
extern TypeInfo t3286_TI;
extern TypeInfo t7_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1745_TI;
static Il2CppRGCTXData t3286_RGCTXData[12] = 
{
	&t1745_0_0_0/* Type Usage */,
	&t7_0_0_0/* Type Usage */,
	&t3285_TI/* Class Usage */,
	&t3285_TI/* Static Usage */,
	&t3286_TI/* Class Usage */,
	&m18301_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m31620_MI/* Method Usage */,
	&m18297_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&t1745_TI/* Class Usage */,
	&m24146_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3286_0_0_0;
extern Il2CppType t3286_1_0_0;
struct t3286;
extern Il2CppGenericClass t3286_GC;
extern TypeInfo t1246_TI;
TypeInfo t3286_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3286_MIs, NULL, NULL, NULL, &t3285_TI, NULL, &t1246_TI, &t3286_TI, NULL, t3286_VT, &EmptyCustomAttributesCache, &t3286_TI, &t3286_0_0_0, &t3286_1_0_0, t3286_IOs, &t3286_GC, NULL, NULL, NULL, t3286_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3286), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3281_TI;
#include "t3281MD.h"



// Metadata Definition System.Comparison`1<System.String>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3281_m18303_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18303_GM;
MethodInfo m18303_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t3281_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3281_m18303_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18303_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3281_m18304_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18304_GM;
MethodInfo m18304_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t3281_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3281_m18304_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18304_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3281_m18305_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18305_GM;
MethodInfo m18305_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t3281_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t3281_m18305_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m18305_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3281_m18306_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18306_GM;
MethodInfo m18306_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t3281_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3281_m18306_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18306_GM};
static MethodInfo* t3281_MIs[] =
{
	&m18303_MI,
	&m18304_MI,
	&m18305_MI,
	&m18306_MI,
	NULL
};
extern MethodInfo m18304_MI;
extern MethodInfo m18305_MI;
extern MethodInfo m18306_MI;
static MethodInfo* t3281_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m18304_MI,
	&m18305_MI,
	&m18306_MI,
};
static Il2CppInterfaceOffsetPair t3281_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3281_1_0_0;
struct t3281;
extern Il2CppGenericClass t3281_GC;
TypeInfo t3281_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3281_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3281_TI, NULL, t3281_VT, &EmptyCustomAttributesCache, &t3281_TI, &t3281_0_0_0, &t3281_1_0_0, t3281_IOs, &t3281_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3281), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1757_TI;

#include "t1126.h"


// Metadata Definition System.IComparable`1<System.Decimal>
extern Il2CppType t1126_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t1757_m31621_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31621_GM;
MethodInfo m31621_MI = 
{
	"CompareTo", NULL, &t1757_TI, &t44_0_0_0, RuntimeInvoker_t44_t1126, t1757_m31621_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31621_GM};
static MethodInfo* t1757_MIs[] =
{
	&m31621_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1757_0_0_0;
extern Il2CppType t1757_1_0_0;
struct t1757;
extern Il2CppGenericClass t1757_GC;
TypeInfo t1757_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1757_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1757_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1757_TI, &t1757_0_0_0, &t1757_1_0_0, NULL, &t1757_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1758_TI;



// Metadata Definition System.IEquatable`1<System.Decimal>
extern Il2CppType t1126_0_0_0;
static ParameterInfo t1758_m31622_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31622_GM;
MethodInfo m31622_MI = 
{
	"Equals", NULL, &t1758_TI, &t40_0_0_0, RuntimeInvoker_t40_t1126, t1758_m31622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31622_GM};
static MethodInfo* t1758_MIs[] =
{
	&m31622_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1758_0_0_0;
extern Il2CppType t1758_1_0_0;
struct t1758;
extern Il2CppGenericClass t1758_GC;
TypeInfo t1758_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1758_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1758_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1758_TI, &t1758_0_0_0, &t1758_1_0_0, NULL, &t1758_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4687_TI;

#include "t1131.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UIntPtr>
extern MethodInfo m31623_MI;
static PropertyInfo t4687____Current_PropertyInfo = 
{
	&t4687_TI, "Current", &m31623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4687_PIs[] =
{
	&t4687____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1131_0_0_0;
extern void* RuntimeInvoker_t1131 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31623_GM;
MethodInfo m31623_MI = 
{
	"get_Current", NULL, &t4687_TI, &t1131_0_0_0, RuntimeInvoker_t1131, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31623_GM};
static MethodInfo* t4687_MIs[] =
{
	&m31623_MI,
	NULL
};
static TypeInfo* t4687_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4687_0_0_0;
extern Il2CppType t4687_1_0_0;
struct t4687;
extern Il2CppGenericClass t4687_GC;
TypeInfo t4687_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4687_MIs, t4687_PIs, NULL, NULL, NULL, NULL, NULL, &t4687_TI, t4687_ITIs, NULL, &EmptyCustomAttributesCache, &t4687_TI, &t4687_0_0_0, &t4687_1_0_0, NULL, &t4687_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3287.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3287_TI;
#include "t3287MD.h"

extern TypeInfo t1131_TI;
extern MethodInfo m18311_MI;
extern MethodInfo m24151_MI;
struct t20;
 t1131  m24151 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18307_MI;
 void m18307 (t3287 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18308_MI;
 t29 * m18308 (t3287 * __this, MethodInfo* method){
	{
		t1131  L_0 = m18311(__this, &m18311_MI);
		t1131  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1131_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18309_MI;
 void m18309 (t3287 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18310_MI;
 bool m18310 (t3287 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t1131  m18311 (t3287 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t1131  L_8 = m24151(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24151_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UIntPtr>
extern Il2CppType t20_0_0_1;
FieldInfo t3287_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3287_TI, offsetof(t3287, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3287_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3287_TI, offsetof(t3287, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3287_FIs[] =
{
	&t3287_f0_FieldInfo,
	&t3287_f1_FieldInfo,
	NULL
};
static PropertyInfo t3287____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3287_TI, "System.Collections.IEnumerator.Current", &m18308_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3287____Current_PropertyInfo = 
{
	&t3287_TI, "Current", &m18311_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3287_PIs[] =
{
	&t3287____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3287____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3287_m18307_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18307_GM;
MethodInfo m18307_MI = 
{
	".ctor", (methodPointerType)&m18307, &t3287_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3287_m18307_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18307_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18308_GM;
MethodInfo m18308_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18308, &t3287_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18308_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18309_GM;
MethodInfo m18309_MI = 
{
	"Dispose", (methodPointerType)&m18309, &t3287_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18309_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18310_GM;
MethodInfo m18310_MI = 
{
	"MoveNext", (methodPointerType)&m18310, &t3287_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18310_GM};
extern Il2CppType t1131_0_0_0;
extern void* RuntimeInvoker_t1131 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18311_GM;
MethodInfo m18311_MI = 
{
	"get_Current", (methodPointerType)&m18311, &t3287_TI, &t1131_0_0_0, RuntimeInvoker_t1131, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18311_GM};
static MethodInfo* t3287_MIs[] =
{
	&m18307_MI,
	&m18308_MI,
	&m18309_MI,
	&m18310_MI,
	&m18311_MI,
	NULL
};
static MethodInfo* t3287_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18308_MI,
	&m18310_MI,
	&m18309_MI,
	&m18311_MI,
};
static TypeInfo* t3287_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4687_TI,
};
static Il2CppInterfaceOffsetPair t3287_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4687_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3287_0_0_0;
extern Il2CppType t3287_1_0_0;
extern Il2CppGenericClass t3287_GC;
TypeInfo t3287_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3287_MIs, t3287_PIs, t3287_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3287_TI, t3287_ITIs, t3287_VT, &EmptyCustomAttributesCache, &t3287_TI, &t3287_0_0_0, &t3287_1_0_0, t3287_IOs, &t3287_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3287)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6069_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UIntPtr>
extern MethodInfo m31624_MI;
static PropertyInfo t6069____Count_PropertyInfo = 
{
	&t6069_TI, "Count", &m31624_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31625_MI;
static PropertyInfo t6069____IsReadOnly_PropertyInfo = 
{
	&t6069_TI, "IsReadOnly", &m31625_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6069_PIs[] =
{
	&t6069____Count_PropertyInfo,
	&t6069____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31624_GM;
MethodInfo m31624_MI = 
{
	"get_Count", NULL, &t6069_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31624_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31625_GM;
MethodInfo m31625_MI = 
{
	"get_IsReadOnly", NULL, &t6069_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31625_GM};
extern Il2CppType t1131_0_0_0;
extern Il2CppType t1131_0_0_0;
static ParameterInfo t6069_m31626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1131_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31626_GM;
MethodInfo m31626_MI = 
{
	"Add", NULL, &t6069_TI, &t21_0_0_0, RuntimeInvoker_t21_t35, t6069_m31626_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31626_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31627_GM;
MethodInfo m31627_MI = 
{
	"Clear", NULL, &t6069_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31627_GM};
extern Il2CppType t1131_0_0_0;
static ParameterInfo t6069_m31628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1131_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31628_GM;
MethodInfo m31628_MI = 
{
	"Contains", NULL, &t6069_TI, &t40_0_0_0, RuntimeInvoker_t40_t35, t6069_m31628_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31628_GM};
extern Il2CppType t3571_0_0_0;
extern Il2CppType t3571_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6069_m31629_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3571_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31629_GM;
MethodInfo m31629_MI = 
{
	"CopyTo", NULL, &t6069_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6069_m31629_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31629_GM};
extern Il2CppType t1131_0_0_0;
static ParameterInfo t6069_m31630_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1131_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31630_GM;
MethodInfo m31630_MI = 
{
	"Remove", NULL, &t6069_TI, &t40_0_0_0, RuntimeInvoker_t40_t35, t6069_m31630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31630_GM};
static MethodInfo* t6069_MIs[] =
{
	&m31624_MI,
	&m31625_MI,
	&m31626_MI,
	&m31627_MI,
	&m31628_MI,
	&m31629_MI,
	&m31630_MI,
	NULL
};
extern TypeInfo t6071_TI;
static TypeInfo* t6069_ITIs[] = 
{
	&t603_TI,
	&t6071_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6069_0_0_0;
extern Il2CppType t6069_1_0_0;
struct t6069;
extern Il2CppGenericClass t6069_GC;
TypeInfo t6069_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6069_MIs, t6069_PIs, NULL, NULL, NULL, NULL, NULL, &t6069_TI, t6069_ITIs, NULL, &EmptyCustomAttributesCache, &t6069_TI, &t6069_0_0_0, &t6069_1_0_0, NULL, &t6069_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UIntPtr>
extern Il2CppType t4687_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31631_GM;
MethodInfo m31631_MI = 
{
	"GetEnumerator", NULL, &t6071_TI, &t4687_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31631_GM};
static MethodInfo* t6071_MIs[] =
{
	&m31631_MI,
	NULL
};
static TypeInfo* t6071_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6071_0_0_0;
extern Il2CppType t6071_1_0_0;
struct t6071;
extern Il2CppGenericClass t6071_GC;
TypeInfo t6071_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6071_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6071_TI, t6071_ITIs, NULL, &EmptyCustomAttributesCache, &t6071_TI, &t6071_0_0_0, &t6071_1_0_0, NULL, &t6071_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6070_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UIntPtr>
extern MethodInfo m31632_MI;
extern MethodInfo m31633_MI;
static PropertyInfo t6070____Item_PropertyInfo = 
{
	&t6070_TI, "Item", &m31632_MI, &m31633_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6070_PIs[] =
{
	&t6070____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1131_0_0_0;
static ParameterInfo t6070_m31634_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1131_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31634_GM;
MethodInfo m31634_MI = 
{
	"IndexOf", NULL, &t6070_TI, &t44_0_0_0, RuntimeInvoker_t44_t35, t6070_m31634_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31634_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1131_0_0_0;
static ParameterInfo t6070_m31635_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1131_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31635_GM;
MethodInfo m31635_MI = 
{
	"Insert", NULL, &t6070_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t35, t6070_m31635_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31635_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6070_m31636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31636_GM;
MethodInfo m31636_MI = 
{
	"RemoveAt", NULL, &t6070_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6070_m31636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31636_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6070_m31632_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1131_0_0_0;
extern void* RuntimeInvoker_t1131_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31632_GM;
MethodInfo m31632_MI = 
{
	"get_Item", NULL, &t6070_TI, &t1131_0_0_0, RuntimeInvoker_t1131_t44, t6070_m31632_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31632_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1131_0_0_0;
static ParameterInfo t6070_m31633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1131_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31633_GM;
MethodInfo m31633_MI = 
{
	"set_Item", NULL, &t6070_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t35, t6070_m31633_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31633_GM};
static MethodInfo* t6070_MIs[] =
{
	&m31634_MI,
	&m31635_MI,
	&m31636_MI,
	&m31632_MI,
	&m31633_MI,
	NULL
};
static TypeInfo* t6070_ITIs[] = 
{
	&t603_TI,
	&t6069_TI,
	&t6071_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6070_0_0_0;
extern Il2CppType t6070_1_0_0;
struct t6070;
extern Il2CppGenericClass t6070_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6070_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6070_MIs, t6070_PIs, NULL, NULL, NULL, NULL, NULL, &t6070_TI, t6070_ITIs, NULL, &t1908__CustomAttributeCache, &t6070_TI, &t6070_0_0_0, &t6070_1_0_0, NULL, &t6070_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4689_TI;

#include "t353.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Delegate>
extern MethodInfo m31637_MI;
static PropertyInfo t4689____Current_PropertyInfo = 
{
	&t4689_TI, "Current", &m31637_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4689_PIs[] =
{
	&t4689____Current_PropertyInfo,
	NULL
};
extern Il2CppType t353_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31637_GM;
MethodInfo m31637_MI = 
{
	"get_Current", NULL, &t4689_TI, &t353_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31637_GM};
static MethodInfo* t4689_MIs[] =
{
	&m31637_MI,
	NULL
};
static TypeInfo* t4689_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4689_0_0_0;
extern Il2CppType t4689_1_0_0;
struct t4689;
extern Il2CppGenericClass t4689_GC;
TypeInfo t4689_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4689_MIs, t4689_PIs, NULL, NULL, NULL, NULL, NULL, &t4689_TI, t4689_ITIs, NULL, &EmptyCustomAttributesCache, &t4689_TI, &t4689_0_0_0, &t4689_1_0_0, NULL, &t4689_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3288.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3288_TI;
#include "t3288MD.h"

extern TypeInfo t353_TI;
extern MethodInfo m18316_MI;
extern MethodInfo m24162_MI;
struct t20;
#define m24162(__this, p0, method) (t353 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Delegate>
extern Il2CppType t20_0_0_1;
FieldInfo t3288_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3288_TI, offsetof(t3288, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3288_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3288_TI, offsetof(t3288, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3288_FIs[] =
{
	&t3288_f0_FieldInfo,
	&t3288_f1_FieldInfo,
	NULL
};
extern MethodInfo m18313_MI;
static PropertyInfo t3288____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3288_TI, "System.Collections.IEnumerator.Current", &m18313_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3288____Current_PropertyInfo = 
{
	&t3288_TI, "Current", &m18316_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3288_PIs[] =
{
	&t3288____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3288____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3288_m18312_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18312_GM;
MethodInfo m18312_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3288_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3288_m18312_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18312_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18313_GM;
MethodInfo m18313_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3288_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18313_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18314_GM;
MethodInfo m18314_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3288_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18314_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18315_GM;
MethodInfo m18315_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3288_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18315_GM};
extern Il2CppType t353_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18316_GM;
MethodInfo m18316_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3288_TI, &t353_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18316_GM};
static MethodInfo* t3288_MIs[] =
{
	&m18312_MI,
	&m18313_MI,
	&m18314_MI,
	&m18315_MI,
	&m18316_MI,
	NULL
};
extern MethodInfo m18315_MI;
extern MethodInfo m18314_MI;
static MethodInfo* t3288_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18313_MI,
	&m18315_MI,
	&m18314_MI,
	&m18316_MI,
};
static TypeInfo* t3288_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4689_TI,
};
static Il2CppInterfaceOffsetPair t3288_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4689_TI, 7},
};
extern TypeInfo t353_TI;
static Il2CppRGCTXData t3288_RGCTXData[3] = 
{
	&m18316_MI/* Method Usage */,
	&t353_TI/* Class Usage */,
	&m24162_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3288_0_0_0;
extern Il2CppType t3288_1_0_0;
extern Il2CppGenericClass t3288_GC;
TypeInfo t3288_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3288_MIs, t3288_PIs, t3288_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3288_TI, t3288_ITIs, t3288_VT, &EmptyCustomAttributesCache, &t3288_TI, &t3288_0_0_0, &t3288_1_0_0, t3288_IOs, &t3288_GC, NULL, NULL, NULL, t3288_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3288)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6072_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Delegate>
extern MethodInfo m31638_MI;
static PropertyInfo t6072____Count_PropertyInfo = 
{
	&t6072_TI, "Count", &m31638_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31639_MI;
static PropertyInfo t6072____IsReadOnly_PropertyInfo = 
{
	&t6072_TI, "IsReadOnly", &m31639_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6072_PIs[] =
{
	&t6072____Count_PropertyInfo,
	&t6072____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31638_GM;
MethodInfo m31638_MI = 
{
	"get_Count", NULL, &t6072_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31638_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31639_GM;
MethodInfo m31639_MI = 
{
	"get_IsReadOnly", NULL, &t6072_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31639_GM};
extern Il2CppType t353_0_0_0;
extern Il2CppType t353_0_0_0;
static ParameterInfo t6072_m31640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31640_GM;
MethodInfo m31640_MI = 
{
	"Add", NULL, &t6072_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6072_m31640_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31640_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31641_GM;
MethodInfo m31641_MI = 
{
	"Clear", NULL, &t6072_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31641_GM};
extern Il2CppType t353_0_0_0;
static ParameterInfo t6072_m31642_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31642_GM;
MethodInfo m31642_MI = 
{
	"Contains", NULL, &t6072_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6072_m31642_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31642_GM};
extern Il2CppType t1132_0_0_0;
extern Il2CppType t1132_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6072_m31643_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1132_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31643_GM;
MethodInfo m31643_MI = 
{
	"CopyTo", NULL, &t6072_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6072_m31643_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31643_GM};
extern Il2CppType t353_0_0_0;
static ParameterInfo t6072_m31644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31644_GM;
MethodInfo m31644_MI = 
{
	"Remove", NULL, &t6072_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6072_m31644_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31644_GM};
static MethodInfo* t6072_MIs[] =
{
	&m31638_MI,
	&m31639_MI,
	&m31640_MI,
	&m31641_MI,
	&m31642_MI,
	&m31643_MI,
	&m31644_MI,
	NULL
};
extern TypeInfo t6074_TI;
static TypeInfo* t6072_ITIs[] = 
{
	&t603_TI,
	&t6074_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6072_0_0_0;
extern Il2CppType t6072_1_0_0;
struct t6072;
extern Il2CppGenericClass t6072_GC;
TypeInfo t6072_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6072_MIs, t6072_PIs, NULL, NULL, NULL, NULL, NULL, &t6072_TI, t6072_ITIs, NULL, &EmptyCustomAttributesCache, &t6072_TI, &t6072_0_0_0, &t6072_1_0_0, NULL, &t6072_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Delegate>
extern Il2CppType t4689_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31645_GM;
MethodInfo m31645_MI = 
{
	"GetEnumerator", NULL, &t6074_TI, &t4689_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31645_GM};
static MethodInfo* t6074_MIs[] =
{
	&m31645_MI,
	NULL
};
static TypeInfo* t6074_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6074_0_0_0;
extern Il2CppType t6074_1_0_0;
struct t6074;
extern Il2CppGenericClass t6074_GC;
TypeInfo t6074_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6074_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6074_TI, t6074_ITIs, NULL, &EmptyCustomAttributesCache, &t6074_TI, &t6074_0_0_0, &t6074_1_0_0, NULL, &t6074_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6073_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Delegate>
extern MethodInfo m31646_MI;
extern MethodInfo m31647_MI;
static PropertyInfo t6073____Item_PropertyInfo = 
{
	&t6073_TI, "Item", &m31646_MI, &m31647_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6073_PIs[] =
{
	&t6073____Item_PropertyInfo,
	NULL
};
extern Il2CppType t353_0_0_0;
static ParameterInfo t6073_m31648_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31648_GM;
MethodInfo m31648_MI = 
{
	"IndexOf", NULL, &t6073_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6073_m31648_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31648_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t353_0_0_0;
static ParameterInfo t6073_m31649_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31649_GM;
MethodInfo m31649_MI = 
{
	"Insert", NULL, &t6073_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6073_m31649_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31649_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6073_m31650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31650_GM;
MethodInfo m31650_MI = 
{
	"RemoveAt", NULL, &t6073_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6073_m31650_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31650_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6073_m31646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t353_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31646_GM;
MethodInfo m31646_MI = 
{
	"get_Item", NULL, &t6073_TI, &t353_0_0_0, RuntimeInvoker_t29_t44, t6073_m31646_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31646_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t353_0_0_0;
static ParameterInfo t6073_m31647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31647_GM;
MethodInfo m31647_MI = 
{
	"set_Item", NULL, &t6073_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6073_m31647_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31647_GM};
static MethodInfo* t6073_MIs[] =
{
	&m31648_MI,
	&m31649_MI,
	&m31650_MI,
	&m31646_MI,
	&m31647_MI,
	NULL
};
static TypeInfo* t6073_ITIs[] = 
{
	&t603_TI,
	&t6072_TI,
	&t6074_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6073_0_0_0;
extern Il2CppType t6073_1_0_0;
struct t6073;
extern Il2CppGenericClass t6073_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6073_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6073_MIs, t6073_PIs, NULL, NULL, NULL, NULL, NULL, &t6073_TI, t6073_ITIs, NULL, &t1908__CustomAttributeCache, &t6073_TI, &t6073_0_0_0, &t6073_1_0_0, NULL, &t6073_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4691_TI;

#include "t291.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.FlagsAttribute>
extern MethodInfo m31651_MI;
static PropertyInfo t4691____Current_PropertyInfo = 
{
	&t4691_TI, "Current", &m31651_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4691_PIs[] =
{
	&t4691____Current_PropertyInfo,
	NULL
};
extern Il2CppType t291_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31651_GM;
MethodInfo m31651_MI = 
{
	"get_Current", NULL, &t4691_TI, &t291_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31651_GM};
static MethodInfo* t4691_MIs[] =
{
	&m31651_MI,
	NULL
};
static TypeInfo* t4691_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4691_0_0_0;
extern Il2CppType t4691_1_0_0;
struct t4691;
extern Il2CppGenericClass t4691_GC;
TypeInfo t4691_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4691_MIs, t4691_PIs, NULL, NULL, NULL, NULL, NULL, &t4691_TI, t4691_ITIs, NULL, &EmptyCustomAttributesCache, &t4691_TI, &t4691_0_0_0, &t4691_1_0_0, NULL, &t4691_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3289.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3289_TI;
#include "t3289MD.h"

extern TypeInfo t291_TI;
extern MethodInfo m18321_MI;
extern MethodInfo m24173_MI;
struct t20;
#define m24173(__this, p0, method) (t291 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.FlagsAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3289_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3289_TI, offsetof(t3289, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3289_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3289_TI, offsetof(t3289, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3289_FIs[] =
{
	&t3289_f0_FieldInfo,
	&t3289_f1_FieldInfo,
	NULL
};
extern MethodInfo m18318_MI;
static PropertyInfo t3289____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3289_TI, "System.Collections.IEnumerator.Current", &m18318_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3289____Current_PropertyInfo = 
{
	&t3289_TI, "Current", &m18321_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3289_PIs[] =
{
	&t3289____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3289____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3289_m18317_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18317_GM;
MethodInfo m18317_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3289_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3289_m18317_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18317_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18318_GM;
MethodInfo m18318_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3289_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18318_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18319_GM;
MethodInfo m18319_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3289_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18319_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18320_GM;
MethodInfo m18320_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3289_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18320_GM};
extern Il2CppType t291_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18321_GM;
MethodInfo m18321_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3289_TI, &t291_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18321_GM};
static MethodInfo* t3289_MIs[] =
{
	&m18317_MI,
	&m18318_MI,
	&m18319_MI,
	&m18320_MI,
	&m18321_MI,
	NULL
};
extern MethodInfo m18320_MI;
extern MethodInfo m18319_MI;
static MethodInfo* t3289_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18318_MI,
	&m18320_MI,
	&m18319_MI,
	&m18321_MI,
};
static TypeInfo* t3289_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4691_TI,
};
static Il2CppInterfaceOffsetPair t3289_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4691_TI, 7},
};
extern TypeInfo t291_TI;
static Il2CppRGCTXData t3289_RGCTXData[3] = 
{
	&m18321_MI/* Method Usage */,
	&t291_TI/* Class Usage */,
	&m24173_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3289_0_0_0;
extern Il2CppType t3289_1_0_0;
extern Il2CppGenericClass t3289_GC;
TypeInfo t3289_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3289_MIs, t3289_PIs, t3289_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3289_TI, t3289_ITIs, t3289_VT, &EmptyCustomAttributesCache, &t3289_TI, &t3289_0_0_0, &t3289_1_0_0, t3289_IOs, &t3289_GC, NULL, NULL, NULL, t3289_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3289)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6075_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.FlagsAttribute>
extern MethodInfo m31652_MI;
static PropertyInfo t6075____Count_PropertyInfo = 
{
	&t6075_TI, "Count", &m31652_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31653_MI;
static PropertyInfo t6075____IsReadOnly_PropertyInfo = 
{
	&t6075_TI, "IsReadOnly", &m31653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6075_PIs[] =
{
	&t6075____Count_PropertyInfo,
	&t6075____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31652_GM;
MethodInfo m31652_MI = 
{
	"get_Count", NULL, &t6075_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31652_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31653_GM;
MethodInfo m31653_MI = 
{
	"get_IsReadOnly", NULL, &t6075_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31653_GM};
extern Il2CppType t291_0_0_0;
extern Il2CppType t291_0_0_0;
static ParameterInfo t6075_m31654_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t291_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31654_GM;
MethodInfo m31654_MI = 
{
	"Add", NULL, &t6075_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6075_m31654_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31654_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31655_GM;
MethodInfo m31655_MI = 
{
	"Clear", NULL, &t6075_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31655_GM};
extern Il2CppType t291_0_0_0;
static ParameterInfo t6075_m31656_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t291_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31656_GM;
MethodInfo m31656_MI = 
{
	"Contains", NULL, &t6075_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6075_m31656_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31656_GM};
extern Il2CppType t3572_0_0_0;
extern Il2CppType t3572_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6075_m31657_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3572_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31657_GM;
MethodInfo m31657_MI = 
{
	"CopyTo", NULL, &t6075_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6075_m31657_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31657_GM};
extern Il2CppType t291_0_0_0;
static ParameterInfo t6075_m31658_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t291_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31658_GM;
MethodInfo m31658_MI = 
{
	"Remove", NULL, &t6075_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6075_m31658_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31658_GM};
static MethodInfo* t6075_MIs[] =
{
	&m31652_MI,
	&m31653_MI,
	&m31654_MI,
	&m31655_MI,
	&m31656_MI,
	&m31657_MI,
	&m31658_MI,
	NULL
};
extern TypeInfo t6077_TI;
static TypeInfo* t6075_ITIs[] = 
{
	&t603_TI,
	&t6077_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6075_0_0_0;
extern Il2CppType t6075_1_0_0;
struct t6075;
extern Il2CppGenericClass t6075_GC;
TypeInfo t6075_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6075_MIs, t6075_PIs, NULL, NULL, NULL, NULL, NULL, &t6075_TI, t6075_ITIs, NULL, &EmptyCustomAttributesCache, &t6075_TI, &t6075_0_0_0, &t6075_1_0_0, NULL, &t6075_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.FlagsAttribute>
extern Il2CppType t4691_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31659_GM;
MethodInfo m31659_MI = 
{
	"GetEnumerator", NULL, &t6077_TI, &t4691_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31659_GM};
static MethodInfo* t6077_MIs[] =
{
	&m31659_MI,
	NULL
};
static TypeInfo* t6077_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6077_0_0_0;
extern Il2CppType t6077_1_0_0;
struct t6077;
extern Il2CppGenericClass t6077_GC;
TypeInfo t6077_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6077_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6077_TI, t6077_ITIs, NULL, &EmptyCustomAttributesCache, &t6077_TI, &t6077_0_0_0, &t6077_1_0_0, NULL, &t6077_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6076_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.FlagsAttribute>
extern MethodInfo m31660_MI;
extern MethodInfo m31661_MI;
static PropertyInfo t6076____Item_PropertyInfo = 
{
	&t6076_TI, "Item", &m31660_MI, &m31661_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6076_PIs[] =
{
	&t6076____Item_PropertyInfo,
	NULL
};
extern Il2CppType t291_0_0_0;
static ParameterInfo t6076_m31662_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t291_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31662_GM;
MethodInfo m31662_MI = 
{
	"IndexOf", NULL, &t6076_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6076_m31662_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31662_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t291_0_0_0;
static ParameterInfo t6076_m31663_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t291_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31663_GM;
MethodInfo m31663_MI = 
{
	"Insert", NULL, &t6076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6076_m31663_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31663_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6076_m31664_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31664_GM;
MethodInfo m31664_MI = 
{
	"RemoveAt", NULL, &t6076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6076_m31664_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31664_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6076_m31660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t291_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31660_GM;
MethodInfo m31660_MI = 
{
	"get_Item", NULL, &t6076_TI, &t291_0_0_0, RuntimeInvoker_t29_t44, t6076_m31660_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31660_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t291_0_0_0;
static ParameterInfo t6076_m31661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t291_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31661_GM;
MethodInfo m31661_MI = 
{
	"set_Item", NULL, &t6076_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6076_m31661_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31661_GM};
static MethodInfo* t6076_MIs[] =
{
	&m31662_MI,
	&m31663_MI,
	&m31664_MI,
	&m31660_MI,
	&m31661_MI,
	NULL
};
static TypeInfo* t6076_ITIs[] = 
{
	&t603_TI,
	&t6075_TI,
	&t6077_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6076_0_0_0;
extern Il2CppType t6076_1_0_0;
struct t6076;
extern Il2CppGenericClass t6076_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6076_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6076_MIs, t6076_PIs, NULL, NULL, NULL, NULL, NULL, &t6076_TI, t6076_ITIs, NULL, &t1908__CustomAttributeCache, &t6076_TI, &t6076_0_0_0, &t6076_1_0_0, NULL, &t6076_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3290.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3290_TI;
#include "t3290MD.h"



extern MethodInfo m18322_MI;
 void m18322_gshared (t3290 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m18323_MI;
 t29 * m18323_gshared (t3290 * __this, t29 * p0, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m18323((t3290 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m18324_MI;
 t29 * m18324_gshared (t3290 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m18325_MI;
 t29 * m18325_gshared (t3290 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Converter`2<System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3290_m18322_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18322_GM;
MethodInfo m18322_MI = 
{
	".ctor", (methodPointerType)&m18322_gshared, &t3290_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3290_m18322_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18322_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3290_m18323_ParameterInfos[] = 
{
	{"input", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18323_GM;
MethodInfo m18323_MI = 
{
	"Invoke", (methodPointerType)&m18323_gshared, &t3290_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3290_m18323_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18323_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3290_m18324_ParameterInfos[] = 
{
	{"input", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18324_GM;
MethodInfo m18324_MI = 
{
	"BeginInvoke", (methodPointerType)&m18324_gshared, &t3290_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3290_m18324_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m18324_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3290_m18325_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18325_GM;
MethodInfo m18325_MI = 
{
	"EndInvoke", (methodPointerType)&m18325_gshared, &t3290_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3290_m18325_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18325_GM};
static MethodInfo* t3290_MIs[] =
{
	&m18322_MI,
	&m18323_MI,
	&m18324_MI,
	&m18325_MI,
	NULL
};
static MethodInfo* t3290_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m18323_MI,
	&m18324_MI,
	&m18325_MI,
};
static Il2CppInterfaceOffsetPair t3290_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3290_0_0_0;
extern Il2CppType t3290_1_0_0;
struct t3290;
extern Il2CppGenericClass t3290_GC;
TypeInfo t3290_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Converter`2", "System", t3290_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3290_TI, NULL, t3290_VT, &EmptyCustomAttributesCache, &t3290_TI, &t3290_0_0_0, &t3290_1_0_0, t3290_IOs, &t3290_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3290), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3291.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3291_TI;
#include "t3291MD.h"

#include "t3292.h"
extern TypeInfo t346_TI;
extern TypeInfo t3292_TI;
#include "t3292MD.h"
extern MethodInfo m18341_MI;
extern MethodInfo m18336_MI;
extern MethodInfo m10145_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m18342_MI;
extern MethodInfo m3988_MI;
struct t20;
 int32_t m10145_gshared (t29 * __this, t316* p0, t29 * p1, MethodInfo* method);
#define m10145(__this, p0, p1, method) (int32_t)m10145_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, method)


extern MethodInfo m18326_MI;
 void m18326_gshared (t3291 * __this, t316* p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m18327_MI;
 t29 * m18327_gshared (t3291 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (t29*)VirtFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), __this);
		return L_0;
	}
}
extern MethodInfo m18328_MI;
 t29 * m18328_gshared (t3291 * __this, int32_t p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f0);
		if ((((uint32_t)p0) < ((uint32_t)(((int32_t)(((t20 *)L_0)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		t316* L_2 = (__this->f0);
		int32_t L_3 = p0;
		return (*(t29 **)(t29 **)SZArrayLdElema(L_2, L_3));
	}
}
extern MethodInfo m18329_MI;
 void m18329_gshared (t3291 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t295 * L_0 = (( t295 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m18330_MI;
 int32_t m18330_gshared (t3291 * __this, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f0);
		return (((int32_t)(((t20 *)L_0)->max_length)));
	}
}
extern MethodInfo m18331_MI;
 bool m18331_gshared (t3291 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
extern MethodInfo m18332_MI;
 void m18332_gshared (t3291 * __this, t29 * p0, MethodInfo* method)
{
	{
		t295 * L_0 = (( t295 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m18333_MI;
 void m18333_gshared (t3291 * __this, MethodInfo* method)
{
	{
		t295 * L_0 = (( t295 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m18334_MI;
 bool m18334_gshared (t3291 * __this, t29 * p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f0);
		int32_t L_1 = (( int32_t (*) (t29 * __this, t316* p0, t29 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, L_0, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return ((((int32_t)((((int32_t)L_1) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m18335_MI;
 void m18335_gshared (t3291 * __this, t316* p0, int32_t p1, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f0);
		VirtActionInvoker2< t20 *, int32_t >::Invoke(&m4199_MI, L_0, (t20 *)(t20 *)p0, p1);
		return;
	}
}
 t29* m18336_gshared (t3291 * __this, MethodInfo* method)
{
	t3292 * V_0 = {0};
	{
		t3292 * L_0 = (t3292 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		(( void (*) (t3292 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = L_0;
		V_0->f3 = __this;
		return V_0;
	}
}
extern MethodInfo m18337_MI;
 int32_t m18337_gshared (t3291 * __this, t29 * p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f0);
		int32_t L_1 = (( int32_t (*) (t29 * __this, t316* p0, t29 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, L_0, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
extern MethodInfo m18338_MI;
 void m18338_gshared (t3291 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t295 * L_0 = (( t295 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m18339_MI;
 bool m18339_gshared (t3291 * __this, t29 * p0, MethodInfo* method)
{
	{
		t295 * L_0 = (( t295 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m18340_MI;
 void m18340_gshared (t3291 * __this, int32_t p0, MethodInfo* method)
{
	{
		t295 * L_0 = (( t295 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		il2cpp_codegen_raise_exception(L_0);
	}
}
 t295 * m18341_gshared (t29 * __this, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1040, &m3988_MI);
		return L_0;
	}
}
// Metadata Definition System.Array/ArrayReadOnlyList`1<System.Object>
extern Il2CppType t316_0_0_1;
FieldInfo t3291_f0_FieldInfo = 
{
	"array", &t316_0_0_1, &t3291_TI, offsetof(t3291, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3291_FIs[] =
{
	&t3291_f0_FieldInfo,
	NULL
};
static PropertyInfo t3291____Item_PropertyInfo = 
{
	&t3291_TI, "Item", &m18328_MI, &m18329_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3291____Count_PropertyInfo = 
{
	&t3291_TI, "Count", &m18330_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3291____IsReadOnly_PropertyInfo = 
{
	&t3291_TI, "IsReadOnly", &m18331_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3291_PIs[] =
{
	&t3291____Item_PropertyInfo,
	&t3291____Count_PropertyInfo,
	&t3291____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t3291_m18326_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18326_GM;
MethodInfo m18326_MI = 
{
	".ctor", (methodPointerType)&m18326_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3291_m18326_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18326_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18327_GM;
MethodInfo m18327_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m18327_gshared, &t3291_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18327_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3291_m18328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18328_GM;
MethodInfo m18328_MI = 
{
	"get_Item", (methodPointerType)&m18328_gshared, &t3291_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3291_m18328_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18328_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3291_m18329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18329_GM;
MethodInfo m18329_MI = 
{
	"set_Item", (methodPointerType)&m18329_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3291_m18329_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18329_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18330_GM;
MethodInfo m18330_MI = 
{
	"get_Count", (methodPointerType)&m18330_gshared, &t3291_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18330_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18331_GM;
MethodInfo m18331_MI = 
{
	"get_IsReadOnly", (methodPointerType)&m18331_gshared, &t3291_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18331_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3291_m18332_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18332_GM;
MethodInfo m18332_MI = 
{
	"Add", (methodPointerType)&m18332_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3291_m18332_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18332_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18333_GM;
MethodInfo m18333_MI = 
{
	"Clear", (methodPointerType)&m18333_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18333_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3291_m18334_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18334_GM;
MethodInfo m18334_MI = 
{
	"Contains", (methodPointerType)&m18334_gshared, &t3291_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3291_m18334_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18334_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3291_m18335_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18335_GM;
MethodInfo m18335_MI = 
{
	"CopyTo", (methodPointerType)&m18335_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3291_m18335_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18335_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1137__CustomAttributeCache_m9704;
extern Il2CppGenericMethod m18336_GM;
MethodInfo m18336_MI = 
{
	"GetEnumerator", (methodPointerType)&m18336_gshared, &t3291_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &t1137__CustomAttributeCache_m9704, 486, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18336_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3291_m18337_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18337_GM;
MethodInfo m18337_MI = 
{
	"IndexOf", (methodPointerType)&m18337_gshared, &t3291_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3291_m18337_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18337_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3291_m18338_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18338_GM;
MethodInfo m18338_MI = 
{
	"Insert", (methodPointerType)&m18338_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3291_m18338_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18338_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3291_m18339_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18339_GM;
MethodInfo m18339_MI = 
{
	"Remove", (methodPointerType)&m18339_gshared, &t3291_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3291_m18339_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18339_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3291_m18340_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18340_GM;
MethodInfo m18340_MI = 
{
	"RemoveAt", (methodPointerType)&m18340_gshared, &t3291_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3291_m18340_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18340_GM};
extern Il2CppType t295_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18341_GM;
MethodInfo m18341_MI = 
{
	"ReadOnlyError", (methodPointerType)&m18341_gshared, &t3291_TI, &t295_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 145, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18341_GM};
static MethodInfo* t3291_MIs[] =
{
	&m18326_MI,
	&m18327_MI,
	&m18328_MI,
	&m18329_MI,
	&m18330_MI,
	&m18331_MI,
	&m18332_MI,
	&m18333_MI,
	&m18334_MI,
	&m18335_MI,
	&m18336_MI,
	&m18337_MI,
	&m18338_MI,
	&m18339_MI,
	&m18340_MI,
	&m18341_MI,
	NULL
};
static MethodInfo* t3291_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m18327_MI,
	&m18337_MI,
	&m18338_MI,
	&m18340_MI,
	&m18328_MI,
	&m18329_MI,
	&m18330_MI,
	&m18331_MI,
	&m18332_MI,
	&m18333_MI,
	&m18334_MI,
	&m18335_MI,
	&m18339_MI,
	&m18336_MI,
};
extern TypeInfo t2186_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t3291_ITIs[] = 
{
	&t603_TI,
	&t2186_TI,
	&t2182_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3291_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t2186_TI, 5},
	{ &t2182_TI, 10},
	{ &t2183_TI, 17},
};
extern TypeInfo t3292_TI;
static Il2CppRGCTXData t3291_RGCTXData[5] = 
{
	&m18336_MI/* Method Usage */,
	&m18341_MI/* Method Usage */,
	&m10145_MI/* Method Usage */,
	&t3292_TI/* Class Usage */,
	&m18342_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3291_0_0_0;
extern Il2CppType t3291_1_0_0;
struct t3291;
extern Il2CppGenericClass t3291_GC;
extern CustomAttributesCache t1137__CustomAttributeCache;
extern CustomAttributesCache t1137__CustomAttributeCache_m9704;
TypeInfo t3291_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ArrayReadOnlyList`1", "", t3291_MIs, t3291_PIs, t3291_FIs, NULL, &t29_TI, NULL, &t20_TI, &t3291_TI, t3291_ITIs, t3291_VT, &t1137__CustomAttributeCache, &t3291_TI, &t3291_0_0_0, &t3291_1_0_0, t3291_IOs, &t3291_GC, NULL, NULL, NULL, t3291_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3291), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, true, false, false, false, false, false, false, false, 16, 3, 1, 0, 0, 18, 4, 4};
#ifndef _MSC_VER
#else
#endif

#include "t344.h"


 void m18342_gshared (t3292 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m18343_MI;
 t29 * m18343_gshared (t3292 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m18344_MI;
 t29 * m18344_gshared (t3292 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f2);
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m18345_MI;
 bool m18345_gshared (t3292 * __this, MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->f1);
		V_0 = L_0;
		__this->f1 = (-1);
		if (V_0 == 0)
		{
			goto IL_001e;
		}
		if (V_0 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0076;
	}

IL_001e:
	{
		__this->f0 = 0;
		goto IL_005a;
	}

IL_0027:
	{
		t3291 * L_1 = (__this->f3);
		t316* L_2 = (L_1->f0);
		int32_t L_3 = (__this->f0);
		int32_t L_4 = L_3;
		__this->f2 = (*(t29 **)(t29 **)SZArrayLdElema(L_2, L_4));
		__this->f1 = 1;
		goto IL_0078;
	}

IL_004c:
	{
		int32_t L_5 = (__this->f0);
		__this->f0 = ((int32_t)(L_5+1));
	}

IL_005a:
	{
		int32_t L_6 = (__this->f0);
		t3291 * L_7 = (__this->f3);
		t316* L_8 = (L_7->f0);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f1 = (-1);
	}

IL_0076:
	{
		return 0;
	}

IL_0078:
	{
		return 1;
	}
	// Dead block : IL_007a: ldloc.1
}
extern MethodInfo m18346_MI;
 void m18346_gshared (t3292 * __this, MethodInfo* method)
{
	{
		__this->f1 = (-1);
		return;
	}
}
// Metadata Definition System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
extern Il2CppType t44_0_0_3;
FieldInfo t3292_f0_FieldInfo = 
{
	"<i>__0", &t44_0_0_3, &t3292_TI, offsetof(t3292, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t3292_f1_FieldInfo = 
{
	"$PC", &t44_0_0_3, &t3292_TI, offsetof(t3292, f1), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_3;
FieldInfo t3292_f2_FieldInfo = 
{
	"$current", &t29_0_0_3, &t3292_TI, offsetof(t3292, f2), &EmptyCustomAttributesCache};
extern Il2CppType t3291_0_0_3;
FieldInfo t3292_f3_FieldInfo = 
{
	"<>f__this", &t3291_0_0_3, &t3292_TI, offsetof(t3292, f3), &EmptyCustomAttributesCache};
static FieldInfo* t3292_FIs[] =
{
	&t3292_f0_FieldInfo,
	&t3292_f1_FieldInfo,
	&t3292_f2_FieldInfo,
	&t3292_f3_FieldInfo,
	NULL
};
static PropertyInfo t3292____System_Collections_Generic_IEnumeratorU3CTU3E_Current_PropertyInfo = 
{
	&t3292_TI, "System.Collections.Generic.IEnumerator<T>.Current", &m18343_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3292____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3292_TI, "System.Collections.IEnumerator.Current", &m18344_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3292_PIs[] =
{
	&t3292____System_Collections_Generic_IEnumeratorU3CTU3E_Current_PropertyInfo,
	&t3292____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18342_GM;
MethodInfo m18342_MI = 
{
	".ctor", (methodPointerType)&m18342_gshared, &t3292_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18342_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1136__CustomAttributeCache_m9690;
extern Il2CppGenericMethod m18343_GM;
MethodInfo m18343_MI = 
{
	"System.Collections.Generic.IEnumerator<T>.get_Current", (methodPointerType)&m18343_gshared, &t3292_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t1136__CustomAttributeCache_m9690, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18343_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1136__CustomAttributeCache_m9691;
extern Il2CppGenericMethod m18344_GM;
MethodInfo m18344_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18344_gshared, &t3292_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t1136__CustomAttributeCache_m9691, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18344_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18345_GM;
MethodInfo m18345_MI = 
{
	"MoveNext", (methodPointerType)&m18345_gshared, &t3292_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18345_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1136__CustomAttributeCache_m9693;
extern Il2CppGenericMethod m18346_GM;
MethodInfo m18346_MI = 
{
	"Dispose", (methodPointerType)&m18346_gshared, &t3292_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t1136__CustomAttributeCache_m9693, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18346_GM};
static MethodInfo* t3292_MIs[] =
{
	&m18342_MI,
	&m18343_MI,
	&m18344_MI,
	&m18345_MI,
	&m18346_MI,
	NULL
};
static MethodInfo* t3292_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m18344_MI,
	&m18345_MI,
	&m18346_MI,
	&m18343_MI,
};
static TypeInfo* t3292_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t3292_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t346_TI, 7},
};
extern TypeInfo t29_TI;
static Il2CppRGCTXData t3292_RGCTXData[1] = 
{
	&t29_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3292_0_0_0;
extern Il2CppType t3292_1_0_0;
struct t3292;
extern Il2CppGenericClass t3292_GC;
extern TypeInfo t1137_TI;
extern CustomAttributesCache t1136__CustomAttributeCache;
extern CustomAttributesCache t1136__CustomAttributeCache_m9690;
extern CustomAttributesCache t1136__CustomAttributeCache_m9691;
extern CustomAttributesCache t1136__CustomAttributeCache_m9693;
TypeInfo t3292_TI = 
{
	&g_mscorlib_dll_Image, NULL, "<GetEnumerator>c__Iterator0", "", t3292_MIs, t3292_PIs, t3292_FIs, NULL, &t29_TI, NULL, &t1137_TI, &t3292_TI, t3292_ITIs, t3292_VT, &t1136__CustomAttributeCache, &t3292_TI, &t3292_0_0_0, &t3292_1_0_0, t3292_IOs, &t3292_GC, NULL, NULL, NULL, t3292_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3292), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4693_TI;

#include "t557.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
extern MethodInfo m31665_MI;
static PropertyInfo t4693____Current_PropertyInfo = 
{
	&t4693_TI, "Current", &m31665_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4693_PIs[] =
{
	&t4693____Current_PropertyInfo,
	NULL
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31665_GM;
MethodInfo m31665_MI = 
{
	"get_Current", NULL, &t4693_TI, &t557_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31665_GM};
static MethodInfo* t4693_MIs[] =
{
	&m31665_MI,
	NULL
};
static TypeInfo* t4693_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4693_0_0_0;
extern Il2CppType t4693_1_0_0;
struct t4693;
extern Il2CppGenericClass t4693_GC;
TypeInfo t4693_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4693_MIs, t4693_PIs, NULL, NULL, NULL, NULL, NULL, &t4693_TI, t4693_ITIs, NULL, &EmptyCustomAttributesCache, &t4693_TI, &t4693_0_0_0, &t4693_1_0_0, NULL, &t4693_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3293.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3293_TI;
#include "t3293MD.h"

extern TypeInfo t557_TI;
extern MethodInfo m18351_MI;
extern MethodInfo m24212_MI;
struct t20;
#define m24212(__this, p0, method) (t557 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3293_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3293_TI, offsetof(t3293, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3293_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3293_TI, offsetof(t3293, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3293_FIs[] =
{
	&t3293_f0_FieldInfo,
	&t3293_f1_FieldInfo,
	NULL
};
extern MethodInfo m18348_MI;
static PropertyInfo t3293____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3293_TI, "System.Collections.IEnumerator.Current", &m18348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3293____Current_PropertyInfo = 
{
	&t3293_TI, "Current", &m18351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3293_PIs[] =
{
	&t3293____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3293____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3293_m18347_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18347_GM;
MethodInfo m18347_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3293_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3293_m18347_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18347_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18348_GM;
MethodInfo m18348_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3293_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18348_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18349_GM;
MethodInfo m18349_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3293_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18349_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18350_GM;
MethodInfo m18350_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3293_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18350_GM};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18351_GM;
MethodInfo m18351_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3293_TI, &t557_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18351_GM};
static MethodInfo* t3293_MIs[] =
{
	&m18347_MI,
	&m18348_MI,
	&m18349_MI,
	&m18350_MI,
	&m18351_MI,
	NULL
};
extern MethodInfo m18350_MI;
extern MethodInfo m18349_MI;
static MethodInfo* t3293_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18348_MI,
	&m18350_MI,
	&m18349_MI,
	&m18351_MI,
};
static TypeInfo* t3293_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4693_TI,
};
static Il2CppInterfaceOffsetPair t3293_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4693_TI, 7},
};
extern TypeInfo t557_TI;
static Il2CppRGCTXData t3293_RGCTXData[3] = 
{
	&m18351_MI/* Method Usage */,
	&t557_TI/* Class Usage */,
	&m24212_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3293_0_0_0;
extern Il2CppType t3293_1_0_0;
extern Il2CppGenericClass t3293_GC;
TypeInfo t3293_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3293_MIs, t3293_PIs, t3293_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3293_TI, t3293_ITIs, t3293_VT, &EmptyCustomAttributesCache, &t3293_TI, &t3293_0_0_0, &t3293_1_0_0, t3293_IOs, &t3293_GC, NULL, NULL, NULL, t3293_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3293)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6078_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>
extern MethodInfo m31666_MI;
static PropertyInfo t6078____Count_PropertyInfo = 
{
	&t6078_TI, "Count", &m31666_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31667_MI;
static PropertyInfo t6078____IsReadOnly_PropertyInfo = 
{
	&t6078_TI, "IsReadOnly", &m31667_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6078_PIs[] =
{
	&t6078____Count_PropertyInfo,
	&t6078____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31666_GM;
MethodInfo m31666_MI = 
{
	"get_Count", NULL, &t6078_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31666_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31667_GM;
MethodInfo m31667_MI = 
{
	"get_IsReadOnly", NULL, &t6078_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31667_GM};
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t6078_m31668_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31668_GM;
MethodInfo m31668_MI = 
{
	"Add", NULL, &t6078_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6078_m31668_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31668_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31669_GM;
MethodInfo m31669_MI = 
{
	"Clear", NULL, &t6078_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31669_GM};
extern Il2CppType t557_0_0_0;
static ParameterInfo t6078_m31670_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31670_GM;
MethodInfo m31670_MI = 
{
	"Contains", NULL, &t6078_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6078_m31670_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31670_GM};
extern Il2CppType t1145_0_0_0;
extern Il2CppType t1145_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6078_m31671_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1145_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31671_GM;
MethodInfo m31671_MI = 
{
	"CopyTo", NULL, &t6078_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6078_m31671_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31671_GM};
extern Il2CppType t557_0_0_0;
static ParameterInfo t6078_m31672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31672_GM;
MethodInfo m31672_MI = 
{
	"Remove", NULL, &t6078_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6078_m31672_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31672_GM};
static MethodInfo* t6078_MIs[] =
{
	&m31666_MI,
	&m31667_MI,
	&m31668_MI,
	&m31669_MI,
	&m31670_MI,
	&m31671_MI,
	&m31672_MI,
	NULL
};
extern TypeInfo t6080_TI;
static TypeInfo* t6078_ITIs[] = 
{
	&t603_TI,
	&t6080_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6078_0_0_0;
extern Il2CppType t6078_1_0_0;
struct t6078;
extern Il2CppGenericClass t6078_GC;
TypeInfo t6078_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6078_MIs, t6078_PIs, NULL, NULL, NULL, NULL, NULL, &t6078_TI, t6078_ITIs, NULL, &EmptyCustomAttributesCache, &t6078_TI, &t6078_0_0_0, &t6078_1_0_0, NULL, &t6078_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
extern Il2CppType t4693_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31673_GM;
MethodInfo m31673_MI = 
{
	"GetEnumerator", NULL, &t6080_TI, &t4693_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31673_GM};
static MethodInfo* t6080_MIs[] =
{
	&m31673_MI,
	NULL
};
static TypeInfo* t6080_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6080_0_0_0;
extern Il2CppType t6080_1_0_0;
struct t6080;
extern Il2CppGenericClass t6080_GC;
TypeInfo t6080_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6080_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6080_TI, t6080_ITIs, NULL, &EmptyCustomAttributesCache, &t6080_TI, &t6080_0_0_0, &t6080_1_0_0, NULL, &t6080_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6079_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodInfo>
extern MethodInfo m31674_MI;
extern MethodInfo m31675_MI;
static PropertyInfo t6079____Item_PropertyInfo = 
{
	&t6079_TI, "Item", &m31674_MI, &m31675_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6079_PIs[] =
{
	&t6079____Item_PropertyInfo,
	NULL
};
extern Il2CppType t557_0_0_0;
static ParameterInfo t6079_m31676_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31676_GM;
MethodInfo m31676_MI = 
{
	"IndexOf", NULL, &t6079_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6079_m31676_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31676_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t6079_m31677_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31677_GM;
MethodInfo m31677_MI = 
{
	"Insert", NULL, &t6079_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6079_m31677_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31677_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6079_m31678_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31678_GM;
MethodInfo m31678_MI = 
{
	"RemoveAt", NULL, &t6079_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6079_m31678_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31678_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6079_m31674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31674_GM;
MethodInfo m31674_MI = 
{
	"get_Item", NULL, &t6079_TI, &t557_0_0_0, RuntimeInvoker_t29_t44, t6079_m31674_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31674_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t6079_m31675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31675_GM;
MethodInfo m31675_MI = 
{
	"set_Item", NULL, &t6079_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6079_m31675_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31675_GM};
static MethodInfo* t6079_MIs[] =
{
	&m31676_MI,
	&m31677_MI,
	&m31678_MI,
	&m31674_MI,
	&m31675_MI,
	NULL
};
static TypeInfo* t6079_ITIs[] = 
{
	&t603_TI,
	&t6078_TI,
	&t6080_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6079_0_0_0;
extern Il2CppType t6079_1_0_0;
struct t6079;
extern Il2CppGenericClass t6079_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6079_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6079_MIs, t6079_PIs, NULL, NULL, NULL, NULL, NULL, &t6079_TI, t6079_ITIs, NULL, &t1908__CustomAttributeCache, &t6079_TI, &t6079_0_0_0, &t6079_1_0_0, NULL, &t6079_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6081_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>
extern MethodInfo m31679_MI;
static PropertyInfo t6081____Count_PropertyInfo = 
{
	&t6081_TI, "Count", &m31679_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31680_MI;
static PropertyInfo t6081____IsReadOnly_PropertyInfo = 
{
	&t6081_TI, "IsReadOnly", &m31680_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6081_PIs[] =
{
	&t6081____Count_PropertyInfo,
	&t6081____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31679_GM;
MethodInfo m31679_MI = 
{
	"get_Count", NULL, &t6081_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31679_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31680_GM;
MethodInfo m31680_MI = 
{
	"get_IsReadOnly", NULL, &t6081_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31680_GM};
extern Il2CppType t2015_0_0_0;
extern Il2CppType t2015_0_0_0;
static ParameterInfo t6081_m31681_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2015_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31681_GM;
MethodInfo m31681_MI = 
{
	"Add", NULL, &t6081_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6081_m31681_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31681_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31682_GM;
MethodInfo m31682_MI = 
{
	"Clear", NULL, &t6081_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31682_GM};
extern Il2CppType t2015_0_0_0;
static ParameterInfo t6081_m31683_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2015_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31683_GM;
MethodInfo m31683_MI = 
{
	"Contains", NULL, &t6081_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6081_m31683_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31683_GM};
extern Il2CppType t3573_0_0_0;
extern Il2CppType t3573_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6081_m31684_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3573_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31684_GM;
MethodInfo m31684_MI = 
{
	"CopyTo", NULL, &t6081_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6081_m31684_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31684_GM};
extern Il2CppType t2015_0_0_0;
static ParameterInfo t6081_m31685_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2015_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31685_GM;
MethodInfo m31685_MI = 
{
	"Remove", NULL, &t6081_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6081_m31685_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31685_GM};
static MethodInfo* t6081_MIs[] =
{
	&m31679_MI,
	&m31680_MI,
	&m31681_MI,
	&m31682_MI,
	&m31683_MI,
	&m31684_MI,
	&m31685_MI,
	NULL
};
extern TypeInfo t6083_TI;
static TypeInfo* t6081_ITIs[] = 
{
	&t603_TI,
	&t6083_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6081_0_0_0;
extern Il2CppType t6081_1_0_0;
struct t6081;
extern Il2CppGenericClass t6081_GC;
TypeInfo t6081_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6081_MIs, t6081_PIs, NULL, NULL, NULL, NULL, NULL, &t6081_TI, t6081_ITIs, NULL, &EmptyCustomAttributesCache, &t6081_TI, &t6081_0_0_0, &t6081_1_0_0, NULL, &t6081_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodInfo>
extern Il2CppType t4695_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31686_GM;
MethodInfo m31686_MI = 
{
	"GetEnumerator", NULL, &t6083_TI, &t4695_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31686_GM};
static MethodInfo* t6083_MIs[] =
{
	&m31686_MI,
	NULL
};
static TypeInfo* t6083_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6083_0_0_0;
extern Il2CppType t6083_1_0_0;
struct t6083;
extern Il2CppGenericClass t6083_GC;
TypeInfo t6083_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6083_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6083_TI, t6083_ITIs, NULL, &EmptyCustomAttributesCache, &t6083_TI, &t6083_0_0_0, &t6083_1_0_0, NULL, &t6083_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4695_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodInfo>
extern MethodInfo m31687_MI;
static PropertyInfo t4695____Current_PropertyInfo = 
{
	&t4695_TI, "Current", &m31687_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4695_PIs[] =
{
	&t4695____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2015_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31687_GM;
MethodInfo m31687_MI = 
{
	"get_Current", NULL, &t4695_TI, &t2015_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31687_GM};
static MethodInfo* t4695_MIs[] =
{
	&m31687_MI,
	NULL
};
static TypeInfo* t4695_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4695_0_0_0;
extern Il2CppType t4695_1_0_0;
struct t4695;
extern Il2CppGenericClass t4695_GC;
TypeInfo t4695_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4695_MIs, t4695_PIs, NULL, NULL, NULL, NULL, NULL, &t4695_TI, t4695_ITIs, NULL, &EmptyCustomAttributesCache, &t4695_TI, &t4695_0_0_0, &t4695_1_0_0, NULL, &t4695_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3294.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3294_TI;
#include "t3294MD.h"

extern TypeInfo t2015_TI;
extern MethodInfo m18356_MI;
extern MethodInfo m24223_MI;
struct t20;
#define m24223(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3294_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3294_TI, offsetof(t3294, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3294_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3294_TI, offsetof(t3294, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3294_FIs[] =
{
	&t3294_f0_FieldInfo,
	&t3294_f1_FieldInfo,
	NULL
};
extern MethodInfo m18353_MI;
static PropertyInfo t3294____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3294_TI, "System.Collections.IEnumerator.Current", &m18353_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3294____Current_PropertyInfo = 
{
	&t3294_TI, "Current", &m18356_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3294_PIs[] =
{
	&t3294____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3294____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3294_m18352_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18352_GM;
MethodInfo m18352_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3294_m18352_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18352_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18353_GM;
MethodInfo m18353_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3294_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18353_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18354_GM;
MethodInfo m18354_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18354_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18355_GM;
MethodInfo m18355_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18355_GM};
extern Il2CppType t2015_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18356_GM;
MethodInfo m18356_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3294_TI, &t2015_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18356_GM};
static MethodInfo* t3294_MIs[] =
{
	&m18352_MI,
	&m18353_MI,
	&m18354_MI,
	&m18355_MI,
	&m18356_MI,
	NULL
};
extern MethodInfo m18355_MI;
extern MethodInfo m18354_MI;
static MethodInfo* t3294_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18353_MI,
	&m18355_MI,
	&m18354_MI,
	&m18356_MI,
};
static TypeInfo* t3294_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4695_TI,
};
static Il2CppInterfaceOffsetPair t3294_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4695_TI, 7},
};
extern TypeInfo t2015_TI;
static Il2CppRGCTXData t3294_RGCTXData[3] = 
{
	&m18356_MI/* Method Usage */,
	&t2015_TI/* Class Usage */,
	&m24223_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3294_0_0_0;
extern Il2CppType t3294_1_0_0;
extern Il2CppGenericClass t3294_GC;
TypeInfo t3294_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3294_MIs, t3294_PIs, t3294_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3294_TI, t3294_ITIs, t3294_VT, &EmptyCustomAttributesCache, &t3294_TI, &t3294_0_0_0, &t3294_1_0_0, t3294_IOs, &t3294_GC, NULL, NULL, NULL, t3294_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3294)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6082_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>
extern MethodInfo m31688_MI;
extern MethodInfo m31689_MI;
static PropertyInfo t6082____Item_PropertyInfo = 
{
	&t6082_TI, "Item", &m31688_MI, &m31689_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6082_PIs[] =
{
	&t6082____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2015_0_0_0;
static ParameterInfo t6082_m31690_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2015_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31690_GM;
MethodInfo m31690_MI = 
{
	"IndexOf", NULL, &t6082_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6082_m31690_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31690_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2015_0_0_0;
static ParameterInfo t6082_m31691_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2015_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31691_GM;
MethodInfo m31691_MI = 
{
	"Insert", NULL, &t6082_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6082_m31691_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31691_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6082_m31692_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31692_GM;
MethodInfo m31692_MI = 
{
	"RemoveAt", NULL, &t6082_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6082_m31692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31692_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6082_m31688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2015_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31688_GM;
MethodInfo m31688_MI = 
{
	"get_Item", NULL, &t6082_TI, &t2015_0_0_0, RuntimeInvoker_t29_t44, t6082_m31688_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31688_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2015_0_0_0;
static ParameterInfo t6082_m31689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2015_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31689_GM;
MethodInfo m31689_MI = 
{
	"set_Item", NULL, &t6082_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6082_m31689_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31689_GM};
static MethodInfo* t6082_MIs[] =
{
	&m31690_MI,
	&m31691_MI,
	&m31692_MI,
	&m31688_MI,
	&m31689_MI,
	NULL
};
static TypeInfo* t6082_ITIs[] = 
{
	&t603_TI,
	&t6081_TI,
	&t6083_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6082_0_0_0;
extern Il2CppType t6082_1_0_0;
struct t6082;
extern Il2CppGenericClass t6082_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6082_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6082_MIs, t6082_PIs, NULL, NULL, NULL, NULL, NULL, &t6082_TI, t6082_ITIs, NULL, &t1908__CustomAttributeCache, &t6082_TI, &t6082_0_0_0, &t6082_1_0_0, NULL, &t6082_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6084_TI;

#include "t636.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>
extern MethodInfo m31693_MI;
static PropertyInfo t6084____Count_PropertyInfo = 
{
	&t6084_TI, "Count", &m31693_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31694_MI;
static PropertyInfo t6084____IsReadOnly_PropertyInfo = 
{
	&t6084_TI, "IsReadOnly", &m31694_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6084_PIs[] =
{
	&t6084____Count_PropertyInfo,
	&t6084____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31693_GM;
MethodInfo m31693_MI = 
{
	"get_Count", NULL, &t6084_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31693_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31694_GM;
MethodInfo m31694_MI = 
{
	"get_IsReadOnly", NULL, &t6084_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31694_GM};
extern Il2CppType t636_0_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t6084_m31695_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31695_GM;
MethodInfo m31695_MI = 
{
	"Add", NULL, &t6084_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6084_m31695_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31695_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31696_GM;
MethodInfo m31696_MI = 
{
	"Clear", NULL, &t6084_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31696_GM};
extern Il2CppType t636_0_0_0;
static ParameterInfo t6084_m31697_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31697_GM;
MethodInfo m31697_MI = 
{
	"Contains", NULL, &t6084_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6084_m31697_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31697_GM};
extern Il2CppType t1365_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6084_m31698_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1365_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31698_GM;
MethodInfo m31698_MI = 
{
	"CopyTo", NULL, &t6084_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6084_m31698_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31698_GM};
extern Il2CppType t636_0_0_0;
static ParameterInfo t6084_m31699_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31699_GM;
MethodInfo m31699_MI = 
{
	"Remove", NULL, &t6084_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6084_m31699_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31699_GM};
static MethodInfo* t6084_MIs[] =
{
	&m31693_MI,
	&m31694_MI,
	&m31695_MI,
	&m31696_MI,
	&m31697_MI,
	&m31698_MI,
	&m31699_MI,
	NULL
};
extern TypeInfo t6086_TI;
static TypeInfo* t6084_ITIs[] = 
{
	&t603_TI,
	&t6086_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6084_0_0_0;
extern Il2CppType t6084_1_0_0;
struct t6084;
extern Il2CppGenericClass t6084_GC;
TypeInfo t6084_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6084_MIs, t6084_PIs, NULL, NULL, NULL, NULL, NULL, &t6084_TI, t6084_ITIs, NULL, &EmptyCustomAttributesCache, &t6084_TI, &t6084_0_0_0, &t6084_1_0_0, NULL, &t6084_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodBase>
extern Il2CppType t4696_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31700_GM;
MethodInfo m31700_MI = 
{
	"GetEnumerator", NULL, &t6086_TI, &t4696_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31700_GM};
static MethodInfo* t6086_MIs[] =
{
	&m31700_MI,
	NULL
};
static TypeInfo* t6086_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6086_0_0_0;
extern Il2CppType t6086_1_0_0;
struct t6086;
extern Il2CppGenericClass t6086_GC;
TypeInfo t6086_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6086_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6086_TI, t6086_ITIs, NULL, &EmptyCustomAttributesCache, &t6086_TI, &t6086_0_0_0, &t6086_1_0_0, NULL, &t6086_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4696_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodBase>
extern MethodInfo m31701_MI;
static PropertyInfo t4696____Current_PropertyInfo = 
{
	&t4696_TI, "Current", &m31701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4696_PIs[] =
{
	&t4696____Current_PropertyInfo,
	NULL
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31701_GM;
MethodInfo m31701_MI = 
{
	"get_Current", NULL, &t4696_TI, &t636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31701_GM};
static MethodInfo* t4696_MIs[] =
{
	&m31701_MI,
	NULL
};
static TypeInfo* t4696_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4696_0_0_0;
extern Il2CppType t4696_1_0_0;
struct t4696;
extern Il2CppGenericClass t4696_GC;
TypeInfo t4696_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4696_MIs, t4696_PIs, NULL, NULL, NULL, NULL, NULL, &t4696_TI, t4696_ITIs, NULL, &EmptyCustomAttributesCache, &t4696_TI, &t4696_0_0_0, &t4696_1_0_0, NULL, &t4696_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3295.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3295_TI;
#include "t3295MD.h"

extern TypeInfo t636_TI;
extern MethodInfo m18361_MI;
extern MethodInfo m24234_MI;
struct t20;
#define m24234(__this, p0, method) (t636 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodBase>
extern Il2CppType t20_0_0_1;
FieldInfo t3295_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3295_TI, offsetof(t3295, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3295_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3295_TI, offsetof(t3295, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3295_FIs[] =
{
	&t3295_f0_FieldInfo,
	&t3295_f1_FieldInfo,
	NULL
};
extern MethodInfo m18358_MI;
static PropertyInfo t3295____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3295_TI, "System.Collections.IEnumerator.Current", &m18358_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3295____Current_PropertyInfo = 
{
	&t3295_TI, "Current", &m18361_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3295_PIs[] =
{
	&t3295____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3295____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3295_m18357_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18357_GM;
MethodInfo m18357_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3295_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3295_m18357_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18357_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18358_GM;
MethodInfo m18358_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3295_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18358_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18359_GM;
MethodInfo m18359_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3295_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18359_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18360_GM;
MethodInfo m18360_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3295_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18360_GM};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18361_GM;
MethodInfo m18361_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3295_TI, &t636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18361_GM};
static MethodInfo* t3295_MIs[] =
{
	&m18357_MI,
	&m18358_MI,
	&m18359_MI,
	&m18360_MI,
	&m18361_MI,
	NULL
};
extern MethodInfo m18360_MI;
extern MethodInfo m18359_MI;
static MethodInfo* t3295_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18358_MI,
	&m18360_MI,
	&m18359_MI,
	&m18361_MI,
};
static TypeInfo* t3295_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4696_TI,
};
static Il2CppInterfaceOffsetPair t3295_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4696_TI, 7},
};
extern TypeInfo t636_TI;
static Il2CppRGCTXData t3295_RGCTXData[3] = 
{
	&m18361_MI/* Method Usage */,
	&t636_TI/* Class Usage */,
	&m24234_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3295_0_0_0;
extern Il2CppType t3295_1_0_0;
extern Il2CppGenericClass t3295_GC;
TypeInfo t3295_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3295_MIs, t3295_PIs, t3295_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3295_TI, t3295_ITIs, t3295_VT, &EmptyCustomAttributesCache, &t3295_TI, &t3295_0_0_0, &t3295_1_0_0, t3295_IOs, &t3295_GC, NULL, NULL, NULL, t3295_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3295)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6085_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodBase>
extern MethodInfo m31702_MI;
extern MethodInfo m31703_MI;
static PropertyInfo t6085____Item_PropertyInfo = 
{
	&t6085_TI, "Item", &m31702_MI, &m31703_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6085_PIs[] =
{
	&t6085____Item_PropertyInfo,
	NULL
};
extern Il2CppType t636_0_0_0;
static ParameterInfo t6085_m31704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31704_GM;
MethodInfo m31704_MI = 
{
	"IndexOf", NULL, &t6085_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6085_m31704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31704_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t6085_m31705_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31705_GM;
MethodInfo m31705_MI = 
{
	"Insert", NULL, &t6085_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6085_m31705_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31705_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6085_m31706_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31706_GM;
MethodInfo m31706_MI = 
{
	"RemoveAt", NULL, &t6085_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6085_m31706_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31706_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6085_m31702_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31702_GM;
MethodInfo m31702_MI = 
{
	"get_Item", NULL, &t6085_TI, &t636_0_0_0, RuntimeInvoker_t29_t44, t6085_m31702_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31702_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t6085_m31703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31703_GM;
MethodInfo m31703_MI = 
{
	"set_Item", NULL, &t6085_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6085_m31703_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31703_GM};
static MethodInfo* t6085_MIs[] =
{
	&m31704_MI,
	&m31705_MI,
	&m31706_MI,
	&m31702_MI,
	&m31703_MI,
	NULL
};
static TypeInfo* t6085_ITIs[] = 
{
	&t603_TI,
	&t6084_TI,
	&t6086_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6085_0_0_0;
extern Il2CppType t6085_1_0_0;
struct t6085;
extern Il2CppGenericClass t6085_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6085_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6085_MIs, t6085_PIs, NULL, NULL, NULL, NULL, NULL, &t6085_TI, t6085_ITIs, NULL, &t1908__CustomAttributeCache, &t6085_TI, &t6085_0_0_0, &t6085_1_0_0, NULL, &t6085_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6087_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>
extern MethodInfo m31707_MI;
static PropertyInfo t6087____Count_PropertyInfo = 
{
	&t6087_TI, "Count", &m31707_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31708_MI;
static PropertyInfo t6087____IsReadOnly_PropertyInfo = 
{
	&t6087_TI, "IsReadOnly", &m31708_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6087_PIs[] =
{
	&t6087____Count_PropertyInfo,
	&t6087____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31707_GM;
MethodInfo m31707_MI = 
{
	"get_Count", NULL, &t6087_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31707_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31708_GM;
MethodInfo m31708_MI = 
{
	"get_IsReadOnly", NULL, &t6087_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31708_GM};
extern Il2CppType t2010_0_0_0;
extern Il2CppType t2010_0_0_0;
static ParameterInfo t6087_m31709_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2010_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31709_GM;
MethodInfo m31709_MI = 
{
	"Add", NULL, &t6087_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6087_m31709_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31709_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31710_GM;
MethodInfo m31710_MI = 
{
	"Clear", NULL, &t6087_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31710_GM};
extern Il2CppType t2010_0_0_0;
static ParameterInfo t6087_m31711_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2010_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31711_GM;
MethodInfo m31711_MI = 
{
	"Contains", NULL, &t6087_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6087_m31711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31711_GM};
extern Il2CppType t3574_0_0_0;
extern Il2CppType t3574_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6087_m31712_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3574_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31712_GM;
MethodInfo m31712_MI = 
{
	"CopyTo", NULL, &t6087_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6087_m31712_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31712_GM};
extern Il2CppType t2010_0_0_0;
static ParameterInfo t6087_m31713_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2010_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31713_GM;
MethodInfo m31713_MI = 
{
	"Remove", NULL, &t6087_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6087_m31713_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31713_GM};
static MethodInfo* t6087_MIs[] =
{
	&m31707_MI,
	&m31708_MI,
	&m31709_MI,
	&m31710_MI,
	&m31711_MI,
	&m31712_MI,
	&m31713_MI,
	NULL
};
extern TypeInfo t6089_TI;
static TypeInfo* t6087_ITIs[] = 
{
	&t603_TI,
	&t6089_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6087_0_0_0;
extern Il2CppType t6087_1_0_0;
struct t6087;
extern Il2CppGenericClass t6087_GC;
TypeInfo t6087_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6087_MIs, t6087_PIs, NULL, NULL, NULL, NULL, NULL, &t6087_TI, t6087_ITIs, NULL, &EmptyCustomAttributesCache, &t6087_TI, &t6087_0_0_0, &t6087_1_0_0, NULL, &t6087_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBase>
extern Il2CppType t4698_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31714_GM;
MethodInfo m31714_MI = 
{
	"GetEnumerator", NULL, &t6089_TI, &t4698_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31714_GM};
static MethodInfo* t6089_MIs[] =
{
	&m31714_MI,
	NULL
};
static TypeInfo* t6089_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6089_0_0_0;
extern Il2CppType t6089_1_0_0;
struct t6089;
extern Il2CppGenericClass t6089_GC;
TypeInfo t6089_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6089_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6089_TI, t6089_ITIs, NULL, &EmptyCustomAttributesCache, &t6089_TI, &t6089_0_0_0, &t6089_1_0_0, NULL, &t6089_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4698_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBase>
extern MethodInfo m31715_MI;
static PropertyInfo t4698____Current_PropertyInfo = 
{
	&t4698_TI, "Current", &m31715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4698_PIs[] =
{
	&t4698____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2010_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31715_GM;
MethodInfo m31715_MI = 
{
	"get_Current", NULL, &t4698_TI, &t2010_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31715_GM};
static MethodInfo* t4698_MIs[] =
{
	&m31715_MI,
	NULL
};
static TypeInfo* t4698_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4698_0_0_0;
extern Il2CppType t4698_1_0_0;
struct t4698;
extern Il2CppGenericClass t4698_GC;
TypeInfo t4698_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4698_MIs, t4698_PIs, NULL, NULL, NULL, NULL, NULL, &t4698_TI, t4698_ITIs, NULL, &EmptyCustomAttributesCache, &t4698_TI, &t4698_0_0_0, &t4698_1_0_0, NULL, &t4698_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3296.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3296_TI;
#include "t3296MD.h"

extern TypeInfo t2010_TI;
extern MethodInfo m18366_MI;
extern MethodInfo m24245_MI;
struct t20;
#define m24245(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>
extern Il2CppType t20_0_0_1;
FieldInfo t3296_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3296_TI, offsetof(t3296, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3296_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3296_TI, offsetof(t3296, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3296_FIs[] =
{
	&t3296_f0_FieldInfo,
	&t3296_f1_FieldInfo,
	NULL
};
extern MethodInfo m18363_MI;
static PropertyInfo t3296____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3296_TI, "System.Collections.IEnumerator.Current", &m18363_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3296____Current_PropertyInfo = 
{
	&t3296_TI, "Current", &m18366_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3296_PIs[] =
{
	&t3296____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3296____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3296_m18362_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18362_GM;
MethodInfo m18362_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3296_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3296_m18362_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18362_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18363_GM;
MethodInfo m18363_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3296_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18363_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18364_GM;
MethodInfo m18364_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3296_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18364_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18365_GM;
MethodInfo m18365_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3296_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18365_GM};
extern Il2CppType t2010_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18366_GM;
MethodInfo m18366_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3296_TI, &t2010_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18366_GM};
static MethodInfo* t3296_MIs[] =
{
	&m18362_MI,
	&m18363_MI,
	&m18364_MI,
	&m18365_MI,
	&m18366_MI,
	NULL
};
extern MethodInfo m18365_MI;
extern MethodInfo m18364_MI;
static MethodInfo* t3296_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18363_MI,
	&m18365_MI,
	&m18364_MI,
	&m18366_MI,
};
static TypeInfo* t3296_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4698_TI,
};
static Il2CppInterfaceOffsetPair t3296_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4698_TI, 7},
};
extern TypeInfo t2010_TI;
static Il2CppRGCTXData t3296_RGCTXData[3] = 
{
	&m18366_MI/* Method Usage */,
	&t2010_TI/* Class Usage */,
	&m24245_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3296_0_0_0;
extern Il2CppType t3296_1_0_0;
extern Il2CppGenericClass t3296_GC;
TypeInfo t3296_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3296_MIs, t3296_PIs, t3296_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3296_TI, t3296_ITIs, t3296_VT, &EmptyCustomAttributesCache, &t3296_TI, &t3296_0_0_0, &t3296_1_0_0, t3296_IOs, &t3296_GC, NULL, NULL, NULL, t3296_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3296)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6088_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>
extern MethodInfo m31716_MI;
extern MethodInfo m31717_MI;
static PropertyInfo t6088____Item_PropertyInfo = 
{
	&t6088_TI, "Item", &m31716_MI, &m31717_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6088_PIs[] =
{
	&t6088____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2010_0_0_0;
static ParameterInfo t6088_m31718_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2010_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31718_GM;
MethodInfo m31718_MI = 
{
	"IndexOf", NULL, &t6088_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6088_m31718_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31718_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2010_0_0_0;
static ParameterInfo t6088_m31719_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2010_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31719_GM;
MethodInfo m31719_MI = 
{
	"Insert", NULL, &t6088_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6088_m31719_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31719_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6088_m31720_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31720_GM;
MethodInfo m31720_MI = 
{
	"RemoveAt", NULL, &t6088_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6088_m31720_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31720_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6088_m31716_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2010_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31716_GM;
MethodInfo m31716_MI = 
{
	"get_Item", NULL, &t6088_TI, &t2010_0_0_0, RuntimeInvoker_t29_t44, t6088_m31716_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31716_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2010_0_0_0;
static ParameterInfo t6088_m31717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2010_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31717_GM;
MethodInfo m31717_MI = 
{
	"set_Item", NULL, &t6088_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6088_m31717_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31717_GM};
static MethodInfo* t6088_MIs[] =
{
	&m31718_MI,
	&m31719_MI,
	&m31720_MI,
	&m31716_MI,
	&m31717_MI,
	NULL
};
static TypeInfo* t6088_ITIs[] = 
{
	&t603_TI,
	&t6087_TI,
	&t6089_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6088_0_0_0;
extern Il2CppType t6088_1_0_0;
struct t6088;
extern Il2CppGenericClass t6088_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6088_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6088_MIs, t6088_PIs, NULL, NULL, NULL, NULL, NULL, &t6088_TI, t6088_ITIs, NULL, &t1908__CustomAttributeCache, &t6088_TI, &t6088_0_0_0, &t6088_1_0_0, NULL, &t6088_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4700_TI;

#include "t660.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ConstructorInfo>
extern MethodInfo m31721_MI;
static PropertyInfo t4700____Current_PropertyInfo = 
{
	&t4700_TI, "Current", &m31721_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4700_PIs[] =
{
	&t4700____Current_PropertyInfo,
	NULL
};
extern Il2CppType t660_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31721_GM;
MethodInfo m31721_MI = 
{
	"get_Current", NULL, &t4700_TI, &t660_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31721_GM};
static MethodInfo* t4700_MIs[] =
{
	&m31721_MI,
	NULL
};
static TypeInfo* t4700_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4700_0_0_0;
extern Il2CppType t4700_1_0_0;
struct t4700;
extern Il2CppGenericClass t4700_GC;
TypeInfo t4700_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4700_MIs, t4700_PIs, NULL, NULL, NULL, NULL, NULL, &t4700_TI, t4700_ITIs, NULL, &EmptyCustomAttributesCache, &t4700_TI, &t4700_0_0_0, &t4700_1_0_0, NULL, &t4700_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3297.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3297_TI;
#include "t3297MD.h"

extern TypeInfo t660_TI;
extern MethodInfo m18371_MI;
extern MethodInfo m24256_MI;
struct t20;
#define m24256(__this, p0, method) (t660 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ConstructorInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3297_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3297_TI, offsetof(t3297, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3297_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3297_TI, offsetof(t3297, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3297_FIs[] =
{
	&t3297_f0_FieldInfo,
	&t3297_f1_FieldInfo,
	NULL
};
extern MethodInfo m18368_MI;
static PropertyInfo t3297____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3297_TI, "System.Collections.IEnumerator.Current", &m18368_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3297____Current_PropertyInfo = 
{
	&t3297_TI, "Current", &m18371_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3297_PIs[] =
{
	&t3297____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3297____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3297_m18367_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18367_GM;
MethodInfo m18367_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3297_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3297_m18367_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18367_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18368_GM;
MethodInfo m18368_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3297_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18368_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18369_GM;
MethodInfo m18369_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3297_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18369_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18370_GM;
MethodInfo m18370_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3297_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18370_GM};
extern Il2CppType t660_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18371_GM;
MethodInfo m18371_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3297_TI, &t660_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18371_GM};
static MethodInfo* t3297_MIs[] =
{
	&m18367_MI,
	&m18368_MI,
	&m18369_MI,
	&m18370_MI,
	&m18371_MI,
	NULL
};
extern MethodInfo m18370_MI;
extern MethodInfo m18369_MI;
static MethodInfo* t3297_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18368_MI,
	&m18370_MI,
	&m18369_MI,
	&m18371_MI,
};
static TypeInfo* t3297_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4700_TI,
};
static Il2CppInterfaceOffsetPair t3297_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4700_TI, 7},
};
extern TypeInfo t660_TI;
static Il2CppRGCTXData t3297_RGCTXData[3] = 
{
	&m18371_MI/* Method Usage */,
	&t660_TI/* Class Usage */,
	&m24256_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3297_0_0_0;
extern Il2CppType t3297_1_0_0;
extern Il2CppGenericClass t3297_GC;
TypeInfo t3297_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3297_MIs, t3297_PIs, t3297_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3297_TI, t3297_ITIs, t3297_VT, &EmptyCustomAttributesCache, &t3297_TI, &t3297_0_0_0, &t3297_1_0_0, t3297_IOs, &t3297_GC, NULL, NULL, NULL, t3297_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3297)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6090_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ConstructorInfo>
extern MethodInfo m31722_MI;
static PropertyInfo t6090____Count_PropertyInfo = 
{
	&t6090_TI, "Count", &m31722_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31723_MI;
static PropertyInfo t6090____IsReadOnly_PropertyInfo = 
{
	&t6090_TI, "IsReadOnly", &m31723_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6090_PIs[] =
{
	&t6090____Count_PropertyInfo,
	&t6090____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31722_GM;
MethodInfo m31722_MI = 
{
	"get_Count", NULL, &t6090_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31722_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31723_GM;
MethodInfo m31723_MI = 
{
	"get_IsReadOnly", NULL, &t6090_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31723_GM};
extern Il2CppType t660_0_0_0;
extern Il2CppType t660_0_0_0;
static ParameterInfo t6090_m31724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t660_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31724_GM;
MethodInfo m31724_MI = 
{
	"Add", NULL, &t6090_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6090_m31724_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31724_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31725_GM;
MethodInfo m31725_MI = 
{
	"Clear", NULL, &t6090_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31725_GM};
extern Il2CppType t660_0_0_0;
static ParameterInfo t6090_m31726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t660_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31726_GM;
MethodInfo m31726_MI = 
{
	"Contains", NULL, &t6090_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6090_m31726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31726_GM};
extern Il2CppType t1147_0_0_0;
extern Il2CppType t1147_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6090_m31727_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1147_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31727_GM;
MethodInfo m31727_MI = 
{
	"CopyTo", NULL, &t6090_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6090_m31727_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31727_GM};
extern Il2CppType t660_0_0_0;
static ParameterInfo t6090_m31728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t660_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31728_GM;
MethodInfo m31728_MI = 
{
	"Remove", NULL, &t6090_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6090_m31728_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31728_GM};
static MethodInfo* t6090_MIs[] =
{
	&m31722_MI,
	&m31723_MI,
	&m31724_MI,
	&m31725_MI,
	&m31726_MI,
	&m31727_MI,
	&m31728_MI,
	NULL
};
extern TypeInfo t6092_TI;
static TypeInfo* t6090_ITIs[] = 
{
	&t603_TI,
	&t6092_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6090_0_0_0;
extern Il2CppType t6090_1_0_0;
struct t6090;
extern Il2CppGenericClass t6090_GC;
TypeInfo t6090_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6090_MIs, t6090_PIs, NULL, NULL, NULL, NULL, NULL, &t6090_TI, t6090_ITIs, NULL, &EmptyCustomAttributesCache, &t6090_TI, &t6090_0_0_0, &t6090_1_0_0, NULL, &t6090_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>
extern Il2CppType t4700_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31729_GM;
MethodInfo m31729_MI = 
{
	"GetEnumerator", NULL, &t6092_TI, &t4700_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31729_GM};
static MethodInfo* t6092_MIs[] =
{
	&m31729_MI,
	NULL
};
static TypeInfo* t6092_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6092_0_0_0;
extern Il2CppType t6092_1_0_0;
struct t6092;
extern Il2CppGenericClass t6092_GC;
TypeInfo t6092_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6092_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6092_TI, t6092_ITIs, NULL, &EmptyCustomAttributesCache, &t6092_TI, &t6092_0_0_0, &t6092_1_0_0, NULL, &t6092_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6091_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ConstructorInfo>
extern MethodInfo m31730_MI;
extern MethodInfo m31731_MI;
static PropertyInfo t6091____Item_PropertyInfo = 
{
	&t6091_TI, "Item", &m31730_MI, &m31731_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6091_PIs[] =
{
	&t6091____Item_PropertyInfo,
	NULL
};
extern Il2CppType t660_0_0_0;
static ParameterInfo t6091_m31732_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t660_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31732_GM;
MethodInfo m31732_MI = 
{
	"IndexOf", NULL, &t6091_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6091_m31732_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31732_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t660_0_0_0;
static ParameterInfo t6091_m31733_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t660_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31733_GM;
MethodInfo m31733_MI = 
{
	"Insert", NULL, &t6091_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6091_m31733_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31733_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6091_m31734_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31734_GM;
MethodInfo m31734_MI = 
{
	"RemoveAt", NULL, &t6091_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6091_m31734_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31734_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6091_m31730_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t660_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31730_GM;
MethodInfo m31730_MI = 
{
	"get_Item", NULL, &t6091_TI, &t660_0_0_0, RuntimeInvoker_t29_t44, t6091_m31730_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31730_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t660_0_0_0;
static ParameterInfo t6091_m31731_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t660_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31731_GM;
MethodInfo m31731_MI = 
{
	"set_Item", NULL, &t6091_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6091_m31731_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31731_GM};
static MethodInfo* t6091_MIs[] =
{
	&m31732_MI,
	&m31733_MI,
	&m31734_MI,
	&m31730_MI,
	&m31731_MI,
	NULL
};
static TypeInfo* t6091_ITIs[] = 
{
	&t603_TI,
	&t6090_TI,
	&t6092_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6091_0_0_0;
extern Il2CppType t6091_1_0_0;
struct t6091;
extern Il2CppGenericClass t6091_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6091_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6091_MIs, t6091_PIs, NULL, NULL, NULL, NULL, NULL, &t6091_TI, t6091_ITIs, NULL, &t1908__CustomAttributeCache, &t6091_TI, &t6091_0_0_0, &t6091_1_0_0, NULL, &t6091_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6093_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorInfo>
extern MethodInfo m31735_MI;
static PropertyInfo t6093____Count_PropertyInfo = 
{
	&t6093_TI, "Count", &m31735_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31736_MI;
static PropertyInfo t6093____IsReadOnly_PropertyInfo = 
{
	&t6093_TI, "IsReadOnly", &m31736_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6093_PIs[] =
{
	&t6093____Count_PropertyInfo,
	&t6093____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31735_GM;
MethodInfo m31735_MI = 
{
	"get_Count", NULL, &t6093_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31735_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31736_GM;
MethodInfo m31736_MI = 
{
	"get_IsReadOnly", NULL, &t6093_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31736_GM};
extern Il2CppType t2009_0_0_0;
extern Il2CppType t2009_0_0_0;
static ParameterInfo t6093_m31737_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2009_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31737_GM;
MethodInfo m31737_MI = 
{
	"Add", NULL, &t6093_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6093_m31737_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31737_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31738_GM;
MethodInfo m31738_MI = 
{
	"Clear", NULL, &t6093_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31738_GM};
extern Il2CppType t2009_0_0_0;
static ParameterInfo t6093_m31739_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2009_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31739_GM;
MethodInfo m31739_MI = 
{
	"Contains", NULL, &t6093_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6093_m31739_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31739_GM};
extern Il2CppType t3575_0_0_0;
extern Il2CppType t3575_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6093_m31740_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3575_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31740_GM;
MethodInfo m31740_MI = 
{
	"CopyTo", NULL, &t6093_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6093_m31740_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31740_GM};
extern Il2CppType t2009_0_0_0;
static ParameterInfo t6093_m31741_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2009_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31741_GM;
MethodInfo m31741_MI = 
{
	"Remove", NULL, &t6093_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6093_m31741_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31741_GM};
static MethodInfo* t6093_MIs[] =
{
	&m31735_MI,
	&m31736_MI,
	&m31737_MI,
	&m31738_MI,
	&m31739_MI,
	&m31740_MI,
	&m31741_MI,
	NULL
};
extern TypeInfo t6095_TI;
static TypeInfo* t6093_ITIs[] = 
{
	&t603_TI,
	&t6095_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6093_0_0_0;
extern Il2CppType t6093_1_0_0;
struct t6093;
extern Il2CppGenericClass t6093_GC;
TypeInfo t6093_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6093_MIs, t6093_PIs, NULL, NULL, NULL, NULL, NULL, &t6093_TI, t6093_ITIs, NULL, &EmptyCustomAttributesCache, &t6093_TI, &t6093_0_0_0, &t6093_1_0_0, NULL, &t6093_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ConstructorInfo>
extern Il2CppType t4702_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31742_GM;
MethodInfo m31742_MI = 
{
	"GetEnumerator", NULL, &t6095_TI, &t4702_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31742_GM};
static MethodInfo* t6095_MIs[] =
{
	&m31742_MI,
	NULL
};
static TypeInfo* t6095_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6095_0_0_0;
extern Il2CppType t6095_1_0_0;
struct t6095;
extern Il2CppGenericClass t6095_GC;
TypeInfo t6095_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6095_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6095_TI, t6095_ITIs, NULL, &EmptyCustomAttributesCache, &t6095_TI, &t6095_0_0_0, &t6095_1_0_0, NULL, &t6095_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4702_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ConstructorInfo>
extern MethodInfo m31743_MI;
static PropertyInfo t4702____Current_PropertyInfo = 
{
	&t4702_TI, "Current", &m31743_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4702_PIs[] =
{
	&t4702____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2009_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31743_GM;
MethodInfo m31743_MI = 
{
	"get_Current", NULL, &t4702_TI, &t2009_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31743_GM};
static MethodInfo* t4702_MIs[] =
{
	&m31743_MI,
	NULL
};
static TypeInfo* t4702_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4702_0_0_0;
extern Il2CppType t4702_1_0_0;
struct t4702;
extern Il2CppGenericClass t4702_GC;
TypeInfo t4702_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4702_MIs, t4702_PIs, NULL, NULL, NULL, NULL, NULL, &t4702_TI, t4702_ITIs, NULL, &EmptyCustomAttributesCache, &t4702_TI, &t4702_0_0_0, &t4702_1_0_0, NULL, &t4702_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3298.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3298_TI;
#include "t3298MD.h"

extern TypeInfo t2009_TI;
extern MethodInfo m18376_MI;
extern MethodInfo m24267_MI;
struct t20;
#define m24267(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3298_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3298_TI, offsetof(t3298, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3298_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3298_TI, offsetof(t3298, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3298_FIs[] =
{
	&t3298_f0_FieldInfo,
	&t3298_f1_FieldInfo,
	NULL
};
extern MethodInfo m18373_MI;
static PropertyInfo t3298____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3298_TI, "System.Collections.IEnumerator.Current", &m18373_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3298____Current_PropertyInfo = 
{
	&t3298_TI, "Current", &m18376_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3298_PIs[] =
{
	&t3298____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3298____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3298_m18372_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18372_GM;
MethodInfo m18372_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3298_m18372_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18372_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18373_GM;
MethodInfo m18373_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3298_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18373_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18374_GM;
MethodInfo m18374_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3298_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18374_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18375_GM;
MethodInfo m18375_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3298_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18375_GM};
extern Il2CppType t2009_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18376_GM;
MethodInfo m18376_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3298_TI, &t2009_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18376_GM};
static MethodInfo* t3298_MIs[] =
{
	&m18372_MI,
	&m18373_MI,
	&m18374_MI,
	&m18375_MI,
	&m18376_MI,
	NULL
};
extern MethodInfo m18375_MI;
extern MethodInfo m18374_MI;
static MethodInfo* t3298_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18373_MI,
	&m18375_MI,
	&m18374_MI,
	&m18376_MI,
};
static TypeInfo* t3298_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4702_TI,
};
static Il2CppInterfaceOffsetPair t3298_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4702_TI, 7},
};
extern TypeInfo t2009_TI;
static Il2CppRGCTXData t3298_RGCTXData[3] = 
{
	&m18376_MI/* Method Usage */,
	&t2009_TI/* Class Usage */,
	&m24267_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3298_0_0_0;
extern Il2CppType t3298_1_0_0;
extern Il2CppGenericClass t3298_GC;
TypeInfo t3298_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3298_MIs, t3298_PIs, t3298_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3298_TI, t3298_ITIs, t3298_VT, &EmptyCustomAttributesCache, &t3298_TI, &t3298_0_0_0, &t3298_1_0_0, t3298_IOs, &t3298_GC, NULL, NULL, NULL, t3298_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3298)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6094_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorInfo>
extern MethodInfo m31744_MI;
extern MethodInfo m31745_MI;
static PropertyInfo t6094____Item_PropertyInfo = 
{
	&t6094_TI, "Item", &m31744_MI, &m31745_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6094_PIs[] =
{
	&t6094____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2009_0_0_0;
static ParameterInfo t6094_m31746_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2009_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31746_GM;
MethodInfo m31746_MI = 
{
	"IndexOf", NULL, &t6094_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6094_m31746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31746_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2009_0_0_0;
static ParameterInfo t6094_m31747_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2009_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31747_GM;
MethodInfo m31747_MI = 
{
	"Insert", NULL, &t6094_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6094_m31747_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31747_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6094_m31748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31748_GM;
MethodInfo m31748_MI = 
{
	"RemoveAt", NULL, &t6094_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6094_m31748_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31748_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6094_m31744_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2009_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31744_GM;
MethodInfo m31744_MI = 
{
	"get_Item", NULL, &t6094_TI, &t2009_0_0_0, RuntimeInvoker_t29_t44, t6094_m31744_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31744_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2009_0_0_0;
static ParameterInfo t6094_m31745_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2009_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31745_GM;
MethodInfo m31745_MI = 
{
	"set_Item", NULL, &t6094_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6094_m31745_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31745_GM};
static MethodInfo* t6094_MIs[] =
{
	&m31746_MI,
	&m31747_MI,
	&m31748_MI,
	&m31744_MI,
	&m31745_MI,
	NULL
};
static TypeInfo* t6094_ITIs[] = 
{
	&t603_TI,
	&t6093_TI,
	&t6095_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6094_0_0_0;
extern Il2CppType t6094_1_0_0;
struct t6094;
extern Il2CppGenericClass t6094_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6094_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6094_MIs, t6094_PIs, NULL, NULL, NULL, NULL, NULL, &t6094_TI, t6094_ITIs, NULL, &t1908__CustomAttributeCache, &t6094_TI, &t6094_0_0_0, &t6094_1_0_0, NULL, &t6094_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4704_TI;

#include "t1660.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoType>
extern MethodInfo m31749_MI;
static PropertyInfo t4704____Current_PropertyInfo = 
{
	&t4704_TI, "Current", &m31749_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4704_PIs[] =
{
	&t4704____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1660_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31749_GM;
MethodInfo m31749_MI = 
{
	"get_Current", NULL, &t4704_TI, &t1660_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31749_GM};
static MethodInfo* t4704_MIs[] =
{
	&m31749_MI,
	NULL
};
static TypeInfo* t4704_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4704_0_0_0;
extern Il2CppType t4704_1_0_0;
struct t4704;
extern Il2CppGenericClass t4704_GC;
TypeInfo t4704_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4704_MIs, t4704_PIs, NULL, NULL, NULL, NULL, NULL, &t4704_TI, t4704_ITIs, NULL, &EmptyCustomAttributesCache, &t4704_TI, &t4704_0_0_0, &t4704_1_0_0, NULL, &t4704_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3299.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3299_TI;
#include "t3299MD.h"

extern TypeInfo t1660_TI;
extern MethodInfo m18381_MI;
extern MethodInfo m24278_MI;
struct t20;
#define m24278(__this, p0, method) (t1660 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.MonoType>
extern Il2CppType t20_0_0_1;
FieldInfo t3299_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3299_TI, offsetof(t3299, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3299_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3299_TI, offsetof(t3299, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3299_FIs[] =
{
	&t3299_f0_FieldInfo,
	&t3299_f1_FieldInfo,
	NULL
};
extern MethodInfo m18378_MI;
static PropertyInfo t3299____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3299_TI, "System.Collections.IEnumerator.Current", &m18378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3299____Current_PropertyInfo = 
{
	&t3299_TI, "Current", &m18381_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3299_PIs[] =
{
	&t3299____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3299____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3299_m18377_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18377_GM;
MethodInfo m18377_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3299_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3299_m18377_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18377_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18378_GM;
MethodInfo m18378_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3299_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18378_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18379_GM;
MethodInfo m18379_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3299_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18379_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18380_GM;
MethodInfo m18380_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3299_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18380_GM};
extern Il2CppType t1660_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18381_GM;
MethodInfo m18381_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3299_TI, &t1660_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18381_GM};
static MethodInfo* t3299_MIs[] =
{
	&m18377_MI,
	&m18378_MI,
	&m18379_MI,
	&m18380_MI,
	&m18381_MI,
	NULL
};
extern MethodInfo m18380_MI;
extern MethodInfo m18379_MI;
static MethodInfo* t3299_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18378_MI,
	&m18380_MI,
	&m18379_MI,
	&m18381_MI,
};
static TypeInfo* t3299_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4704_TI,
};
static Il2CppInterfaceOffsetPair t3299_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4704_TI, 7},
};
extern TypeInfo t1660_TI;
static Il2CppRGCTXData t3299_RGCTXData[3] = 
{
	&m18381_MI/* Method Usage */,
	&t1660_TI/* Class Usage */,
	&m24278_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3299_0_0_0;
extern Il2CppType t3299_1_0_0;
extern Il2CppGenericClass t3299_GC;
TypeInfo t3299_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3299_MIs, t3299_PIs, t3299_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3299_TI, t3299_ITIs, t3299_VT, &EmptyCustomAttributesCache, &t3299_TI, &t3299_0_0_0, &t3299_1_0_0, t3299_IOs, &t3299_GC, NULL, NULL, NULL, t3299_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3299)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6096_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoType>
extern MethodInfo m31750_MI;
static PropertyInfo t6096____Count_PropertyInfo = 
{
	&t6096_TI, "Count", &m31750_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31751_MI;
static PropertyInfo t6096____IsReadOnly_PropertyInfo = 
{
	&t6096_TI, "IsReadOnly", &m31751_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6096_PIs[] =
{
	&t6096____Count_PropertyInfo,
	&t6096____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31750_GM;
MethodInfo m31750_MI = 
{
	"get_Count", NULL, &t6096_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31750_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31751_GM;
MethodInfo m31751_MI = 
{
	"get_IsReadOnly", NULL, &t6096_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31751_GM};
extern Il2CppType t1660_0_0_0;
extern Il2CppType t1660_0_0_0;
static ParameterInfo t6096_m31752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1660_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31752_GM;
MethodInfo m31752_MI = 
{
	"Add", NULL, &t6096_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6096_m31752_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31752_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31753_GM;
MethodInfo m31753_MI = 
{
	"Clear", NULL, &t6096_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31753_GM};
extern Il2CppType t1660_0_0_0;
static ParameterInfo t6096_m31754_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1660_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31754_GM;
MethodInfo m31754_MI = 
{
	"Contains", NULL, &t6096_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6096_m31754_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31754_GM};
extern Il2CppType t2057_0_0_0;
extern Il2CppType t2057_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6096_m31755_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2057_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31755_GM;
MethodInfo m31755_MI = 
{
	"CopyTo", NULL, &t6096_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6096_m31755_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31755_GM};
extern Il2CppType t1660_0_0_0;
static ParameterInfo t6096_m31756_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1660_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31756_GM;
MethodInfo m31756_MI = 
{
	"Remove", NULL, &t6096_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6096_m31756_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31756_GM};
static MethodInfo* t6096_MIs[] =
{
	&m31750_MI,
	&m31751_MI,
	&m31752_MI,
	&m31753_MI,
	&m31754_MI,
	&m31755_MI,
	&m31756_MI,
	NULL
};
extern TypeInfo t6098_TI;
static TypeInfo* t6096_ITIs[] = 
{
	&t603_TI,
	&t6098_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6096_0_0_0;
extern Il2CppType t6096_1_0_0;
struct t6096;
extern Il2CppGenericClass t6096_GC;
TypeInfo t6096_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6096_MIs, t6096_PIs, NULL, NULL, NULL, NULL, NULL, &t6096_TI, t6096_ITIs, NULL, &EmptyCustomAttributesCache, &t6096_TI, &t6096_0_0_0, &t6096_1_0_0, NULL, &t6096_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoType>
extern Il2CppType t4704_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31757_GM;
MethodInfo m31757_MI = 
{
	"GetEnumerator", NULL, &t6098_TI, &t4704_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31757_GM};
static MethodInfo* t6098_MIs[] =
{
	&m31757_MI,
	NULL
};
static TypeInfo* t6098_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6098_0_0_0;
extern Il2CppType t6098_1_0_0;
struct t6098;
extern Il2CppGenericClass t6098_GC;
TypeInfo t6098_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6098_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6098_TI, t6098_ITIs, NULL, &EmptyCustomAttributesCache, &t6098_TI, &t6098_0_0_0, &t6098_1_0_0, NULL, &t6098_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6097_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.MonoType>
extern MethodInfo m31758_MI;
extern MethodInfo m31759_MI;
static PropertyInfo t6097____Item_PropertyInfo = 
{
	&t6097_TI, "Item", &m31758_MI, &m31759_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6097_PIs[] =
{
	&t6097____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1660_0_0_0;
static ParameterInfo t6097_m31760_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1660_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31760_GM;
MethodInfo m31760_MI = 
{
	"IndexOf", NULL, &t6097_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6097_m31760_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31760_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1660_0_0_0;
static ParameterInfo t6097_m31761_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1660_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31761_GM;
MethodInfo m31761_MI = 
{
	"Insert", NULL, &t6097_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6097_m31761_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31761_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6097_m31762_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31762_GM;
MethodInfo m31762_MI = 
{
	"RemoveAt", NULL, &t6097_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6097_m31762_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31762_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6097_m31758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1660_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31758_GM;
MethodInfo m31758_MI = 
{
	"get_Item", NULL, &t6097_TI, &t1660_0_0_0, RuntimeInvoker_t29_t44, t6097_m31758_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31758_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1660_0_0_0;
static ParameterInfo t6097_m31759_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1660_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31759_GM;
MethodInfo m31759_MI = 
{
	"set_Item", NULL, &t6097_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6097_m31759_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31759_GM};
static MethodInfo* t6097_MIs[] =
{
	&m31760_MI,
	&m31761_MI,
	&m31762_MI,
	&m31758_MI,
	&m31759_MI,
	NULL
};
static TypeInfo* t6097_ITIs[] = 
{
	&t603_TI,
	&t6096_TI,
	&t6098_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6097_0_0_0;
extern Il2CppType t6097_1_0_0;
struct t6097;
extern Il2CppGenericClass t6097_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6097_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6097_MIs, t6097_PIs, NULL, NULL, NULL, NULL, NULL, &t6097_TI, t6097_ITIs, NULL, &t1908__CustomAttributeCache, &t6097_TI, &t6097_0_0_0, &t6097_1_0_0, NULL, &t6097_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4706_TI;

#include "t391.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ParamArrayAttribute>
extern MethodInfo m31763_MI;
static PropertyInfo t4706____Current_PropertyInfo = 
{
	&t4706_TI, "Current", &m31763_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4706_PIs[] =
{
	&t4706____Current_PropertyInfo,
	NULL
};
extern Il2CppType t391_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31763_GM;
MethodInfo m31763_MI = 
{
	"get_Current", NULL, &t4706_TI, &t391_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31763_GM};
static MethodInfo* t4706_MIs[] =
{
	&m31763_MI,
	NULL
};
static TypeInfo* t4706_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4706_0_0_0;
extern Il2CppType t4706_1_0_0;
struct t4706;
extern Il2CppGenericClass t4706_GC;
TypeInfo t4706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4706_MIs, t4706_PIs, NULL, NULL, NULL, NULL, NULL, &t4706_TI, t4706_ITIs, NULL, &EmptyCustomAttributesCache, &t4706_TI, &t4706_0_0_0, &t4706_1_0_0, NULL, &t4706_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3300.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3300_TI;
#include "t3300MD.h"

extern TypeInfo t391_TI;
extern MethodInfo m18386_MI;
extern MethodInfo m24289_MI;
struct t20;
#define m24289(__this, p0, method) (t391 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ParamArrayAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3300_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3300_TI, offsetof(t3300, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3300_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3300_TI, offsetof(t3300, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3300_FIs[] =
{
	&t3300_f0_FieldInfo,
	&t3300_f1_FieldInfo,
	NULL
};
extern MethodInfo m18383_MI;
static PropertyInfo t3300____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3300_TI, "System.Collections.IEnumerator.Current", &m18383_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3300____Current_PropertyInfo = 
{
	&t3300_TI, "Current", &m18386_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3300_PIs[] =
{
	&t3300____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3300____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3300_m18382_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18382_GM;
MethodInfo m18382_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3300_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3300_m18382_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18382_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18383_GM;
MethodInfo m18383_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3300_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18383_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18384_GM;
MethodInfo m18384_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3300_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18384_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18385_GM;
MethodInfo m18385_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3300_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18385_GM};
extern Il2CppType t391_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18386_GM;
MethodInfo m18386_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3300_TI, &t391_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18386_GM};
static MethodInfo* t3300_MIs[] =
{
	&m18382_MI,
	&m18383_MI,
	&m18384_MI,
	&m18385_MI,
	&m18386_MI,
	NULL
};
extern MethodInfo m18385_MI;
extern MethodInfo m18384_MI;
static MethodInfo* t3300_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18383_MI,
	&m18385_MI,
	&m18384_MI,
	&m18386_MI,
};
static TypeInfo* t3300_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4706_TI,
};
static Il2CppInterfaceOffsetPair t3300_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4706_TI, 7},
};
extern TypeInfo t391_TI;
static Il2CppRGCTXData t3300_RGCTXData[3] = 
{
	&m18386_MI/* Method Usage */,
	&t391_TI/* Class Usage */,
	&m24289_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3300_0_0_0;
extern Il2CppType t3300_1_0_0;
extern Il2CppGenericClass t3300_GC;
TypeInfo t3300_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3300_MIs, t3300_PIs, t3300_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3300_TI, t3300_ITIs, t3300_VT, &EmptyCustomAttributesCache, &t3300_TI, &t3300_0_0_0, &t3300_1_0_0, t3300_IOs, &t3300_GC, NULL, NULL, NULL, t3300_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3300)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6099_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ParamArrayAttribute>
extern MethodInfo m31764_MI;
static PropertyInfo t6099____Count_PropertyInfo = 
{
	&t6099_TI, "Count", &m31764_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31765_MI;
static PropertyInfo t6099____IsReadOnly_PropertyInfo = 
{
	&t6099_TI, "IsReadOnly", &m31765_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6099_PIs[] =
{
	&t6099____Count_PropertyInfo,
	&t6099____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31764_GM;
MethodInfo m31764_MI = 
{
	"get_Count", NULL, &t6099_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31764_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31765_GM;
MethodInfo m31765_MI = 
{
	"get_IsReadOnly", NULL, &t6099_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31765_GM};
extern Il2CppType t391_0_0_0;
extern Il2CppType t391_0_0_0;
static ParameterInfo t6099_m31766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t391_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31766_GM;
MethodInfo m31766_MI = 
{
	"Add", NULL, &t6099_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6099_m31766_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31766_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31767_GM;
MethodInfo m31767_MI = 
{
	"Clear", NULL, &t6099_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31767_GM};
extern Il2CppType t391_0_0_0;
static ParameterInfo t6099_m31768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t391_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31768_GM;
MethodInfo m31768_MI = 
{
	"Contains", NULL, &t6099_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6099_m31768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31768_GM};
extern Il2CppType t3576_0_0_0;
extern Il2CppType t3576_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6099_m31769_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3576_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31769_GM;
MethodInfo m31769_MI = 
{
	"CopyTo", NULL, &t6099_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6099_m31769_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31769_GM};
extern Il2CppType t391_0_0_0;
static ParameterInfo t6099_m31770_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t391_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31770_GM;
MethodInfo m31770_MI = 
{
	"Remove", NULL, &t6099_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6099_m31770_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31770_GM};
static MethodInfo* t6099_MIs[] =
{
	&m31764_MI,
	&m31765_MI,
	&m31766_MI,
	&m31767_MI,
	&m31768_MI,
	&m31769_MI,
	&m31770_MI,
	NULL
};
extern TypeInfo t6101_TI;
static TypeInfo* t6099_ITIs[] = 
{
	&t603_TI,
	&t6101_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6099_0_0_0;
extern Il2CppType t6099_1_0_0;
struct t6099;
extern Il2CppGenericClass t6099_GC;
TypeInfo t6099_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6099_MIs, t6099_PIs, NULL, NULL, NULL, NULL, NULL, &t6099_TI, t6099_ITIs, NULL, &EmptyCustomAttributesCache, &t6099_TI, &t6099_0_0_0, &t6099_1_0_0, NULL, &t6099_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ParamArrayAttribute>
extern Il2CppType t4706_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31771_GM;
MethodInfo m31771_MI = 
{
	"GetEnumerator", NULL, &t6101_TI, &t4706_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31771_GM};
static MethodInfo* t6101_MIs[] =
{
	&m31771_MI,
	NULL
};
static TypeInfo* t6101_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6101_0_0_0;
extern Il2CppType t6101_1_0_0;
struct t6101;
extern Il2CppGenericClass t6101_GC;
TypeInfo t6101_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6101_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6101_TI, t6101_ITIs, NULL, &EmptyCustomAttributesCache, &t6101_TI, &t6101_0_0_0, &t6101_1_0_0, NULL, &t6101_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6100_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ParamArrayAttribute>
extern MethodInfo m31772_MI;
extern MethodInfo m31773_MI;
static PropertyInfo t6100____Item_PropertyInfo = 
{
	&t6100_TI, "Item", &m31772_MI, &m31773_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6100_PIs[] =
{
	&t6100____Item_PropertyInfo,
	NULL
};
extern Il2CppType t391_0_0_0;
static ParameterInfo t6100_m31774_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t391_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31774_GM;
MethodInfo m31774_MI = 
{
	"IndexOf", NULL, &t6100_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6100_m31774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31774_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t391_0_0_0;
static ParameterInfo t6100_m31775_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t391_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31775_GM;
MethodInfo m31775_MI = 
{
	"Insert", NULL, &t6100_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6100_m31775_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31775_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6100_m31776_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31776_GM;
MethodInfo m31776_MI = 
{
	"RemoveAt", NULL, &t6100_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6100_m31776_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31776_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6100_m31772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t391_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31772_GM;
MethodInfo m31772_MI = 
{
	"get_Item", NULL, &t6100_TI, &t391_0_0_0, RuntimeInvoker_t29_t44, t6100_m31772_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31772_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t391_0_0_0;
static ParameterInfo t6100_m31773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t391_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31773_GM;
MethodInfo m31773_MI = 
{
	"set_Item", NULL, &t6100_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6100_m31773_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31773_GM};
static MethodInfo* t6100_MIs[] =
{
	&m31774_MI,
	&m31775_MI,
	&m31776_MI,
	&m31772_MI,
	&m31773_MI,
	NULL
};
static TypeInfo* t6100_ITIs[] = 
{
	&t603_TI,
	&t6099_TI,
	&t6101_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6100_0_0_0;
extern Il2CppType t6100_1_0_0;
struct t6100;
extern Il2CppGenericClass t6100_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6100_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6100_MIs, t6100_PIs, NULL, NULL, NULL, NULL, NULL, &t6100_TI, t6100_ITIs, NULL, &t1908__CustomAttributeCache, &t6100_TI, &t6100_0_0_0, &t6100_1_0_0, NULL, &t6100_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4708_TI;

#include "t1151.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.OutAttribute>
extern MethodInfo m31777_MI;
static PropertyInfo t4708____Current_PropertyInfo = 
{
	&t4708_TI, "Current", &m31777_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4708_PIs[] =
{
	&t4708____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1151_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31777_GM;
MethodInfo m31777_MI = 
{
	"get_Current", NULL, &t4708_TI, &t1151_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31777_GM};
static MethodInfo* t4708_MIs[] =
{
	&m31777_MI,
	NULL
};
static TypeInfo* t4708_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4708_0_0_0;
extern Il2CppType t4708_1_0_0;
struct t4708;
extern Il2CppGenericClass t4708_GC;
TypeInfo t4708_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4708_MIs, t4708_PIs, NULL, NULL, NULL, NULL, NULL, &t4708_TI, t4708_ITIs, NULL, &EmptyCustomAttributesCache, &t4708_TI, &t4708_0_0_0, &t4708_1_0_0, NULL, &t4708_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3301.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3301_TI;
#include "t3301MD.h"

extern TypeInfo t1151_TI;
extern MethodInfo m18391_MI;
extern MethodInfo m24300_MI;
struct t20;
#define m24300(__this, p0, method) (t1151 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.OutAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3301_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3301_TI, offsetof(t3301, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3301_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3301_TI, offsetof(t3301, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3301_FIs[] =
{
	&t3301_f0_FieldInfo,
	&t3301_f1_FieldInfo,
	NULL
};
extern MethodInfo m18388_MI;
static PropertyInfo t3301____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3301_TI, "System.Collections.IEnumerator.Current", &m18388_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3301____Current_PropertyInfo = 
{
	&t3301_TI, "Current", &m18391_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3301_PIs[] =
{
	&t3301____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3301____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3301_m18387_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18387_GM;
MethodInfo m18387_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3301_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3301_m18387_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18387_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18388_GM;
MethodInfo m18388_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3301_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18388_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18389_GM;
MethodInfo m18389_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3301_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18389_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18390_GM;
MethodInfo m18390_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3301_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18390_GM};
extern Il2CppType t1151_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18391_GM;
MethodInfo m18391_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3301_TI, &t1151_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18391_GM};
static MethodInfo* t3301_MIs[] =
{
	&m18387_MI,
	&m18388_MI,
	&m18389_MI,
	&m18390_MI,
	&m18391_MI,
	NULL
};
extern MethodInfo m18390_MI;
extern MethodInfo m18389_MI;
static MethodInfo* t3301_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18388_MI,
	&m18390_MI,
	&m18389_MI,
	&m18391_MI,
};
static TypeInfo* t3301_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4708_TI,
};
static Il2CppInterfaceOffsetPair t3301_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4708_TI, 7},
};
extern TypeInfo t1151_TI;
static Il2CppRGCTXData t3301_RGCTXData[3] = 
{
	&m18391_MI/* Method Usage */,
	&t1151_TI/* Class Usage */,
	&m24300_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3301_0_0_0;
extern Il2CppType t3301_1_0_0;
extern Il2CppGenericClass t3301_GC;
TypeInfo t3301_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3301_MIs, t3301_PIs, t3301_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3301_TI, t3301_ITIs, t3301_VT, &EmptyCustomAttributesCache, &t3301_TI, &t3301_0_0_0, &t3301_1_0_0, t3301_IOs, &t3301_GC, NULL, NULL, NULL, t3301_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3301)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6102_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.OutAttribute>
extern MethodInfo m31778_MI;
static PropertyInfo t6102____Count_PropertyInfo = 
{
	&t6102_TI, "Count", &m31778_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31779_MI;
static PropertyInfo t6102____IsReadOnly_PropertyInfo = 
{
	&t6102_TI, "IsReadOnly", &m31779_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6102_PIs[] =
{
	&t6102____Count_PropertyInfo,
	&t6102____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31778_GM;
MethodInfo m31778_MI = 
{
	"get_Count", NULL, &t6102_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31778_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31779_GM;
MethodInfo m31779_MI = 
{
	"get_IsReadOnly", NULL, &t6102_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31779_GM};
extern Il2CppType t1151_0_0_0;
extern Il2CppType t1151_0_0_0;
static ParameterInfo t6102_m31780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31780_GM;
MethodInfo m31780_MI = 
{
	"Add", NULL, &t6102_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6102_m31780_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31780_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31781_GM;
MethodInfo m31781_MI = 
{
	"Clear", NULL, &t6102_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31781_GM};
extern Il2CppType t1151_0_0_0;
static ParameterInfo t6102_m31782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1151_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31782_GM;
MethodInfo m31782_MI = 
{
	"Contains", NULL, &t6102_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6102_m31782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31782_GM};
extern Il2CppType t3577_0_0_0;
extern Il2CppType t3577_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6102_m31783_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3577_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31783_GM;
MethodInfo m31783_MI = 
{
	"CopyTo", NULL, &t6102_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6102_m31783_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31783_GM};
extern Il2CppType t1151_0_0_0;
static ParameterInfo t6102_m31784_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1151_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31784_GM;
MethodInfo m31784_MI = 
{
	"Remove", NULL, &t6102_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6102_m31784_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31784_GM};
static MethodInfo* t6102_MIs[] =
{
	&m31778_MI,
	&m31779_MI,
	&m31780_MI,
	&m31781_MI,
	&m31782_MI,
	&m31783_MI,
	&m31784_MI,
	NULL
};
extern TypeInfo t6104_TI;
static TypeInfo* t6102_ITIs[] = 
{
	&t603_TI,
	&t6104_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6102_0_0_0;
extern Il2CppType t6102_1_0_0;
struct t6102;
extern Il2CppGenericClass t6102_GC;
TypeInfo t6102_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6102_MIs, t6102_PIs, NULL, NULL, NULL, NULL, NULL, &t6102_TI, t6102_ITIs, NULL, &EmptyCustomAttributesCache, &t6102_TI, &t6102_0_0_0, &t6102_1_0_0, NULL, &t6102_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.OutAttribute>
extern Il2CppType t4708_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31785_GM;
MethodInfo m31785_MI = 
{
	"GetEnumerator", NULL, &t6104_TI, &t4708_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31785_GM};
static MethodInfo* t6104_MIs[] =
{
	&m31785_MI,
	NULL
};
static TypeInfo* t6104_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6104_0_0_0;
extern Il2CppType t6104_1_0_0;
struct t6104;
extern Il2CppGenericClass t6104_GC;
TypeInfo t6104_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6104_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6104_TI, t6104_ITIs, NULL, &EmptyCustomAttributesCache, &t6104_TI, &t6104_0_0_0, &t6104_1_0_0, NULL, &t6104_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6103_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.OutAttribute>
extern MethodInfo m31786_MI;
extern MethodInfo m31787_MI;
static PropertyInfo t6103____Item_PropertyInfo = 
{
	&t6103_TI, "Item", &m31786_MI, &m31787_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6103_PIs[] =
{
	&t6103____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1151_0_0_0;
static ParameterInfo t6103_m31788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1151_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31788_GM;
MethodInfo m31788_MI = 
{
	"IndexOf", NULL, &t6103_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6103_m31788_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31788_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1151_0_0_0;
static ParameterInfo t6103_m31789_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31789_GM;
MethodInfo m31789_MI = 
{
	"Insert", NULL, &t6103_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6103_m31789_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31789_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6103_m31790_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31790_GM;
MethodInfo m31790_MI = 
{
	"RemoveAt", NULL, &t6103_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6103_m31790_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31790_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6103_m31786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1151_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31786_GM;
MethodInfo m31786_MI = 
{
	"get_Item", NULL, &t6103_TI, &t1151_0_0_0, RuntimeInvoker_t29_t44, t6103_m31786_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31786_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1151_0_0_0;
static ParameterInfo t6103_m31787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31787_GM;
MethodInfo m31787_MI = 
{
	"set_Item", NULL, &t6103_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6103_m31787_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31787_GM};
static MethodInfo* t6103_MIs[] =
{
	&m31788_MI,
	&m31789_MI,
	&m31790_MI,
	&m31786_MI,
	&m31787_MI,
	NULL
};
static TypeInfo* t6103_ITIs[] = 
{
	&t603_TI,
	&t6102_TI,
	&t6104_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6103_0_0_0;
extern Il2CppType t6103_1_0_0;
struct t6103;
extern Il2CppGenericClass t6103_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6103_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6103_MIs, t6103_PIs, NULL, NULL, NULL, NULL, NULL, &t6103_TI, t6103_ITIs, NULL, &t1908__CustomAttributeCache, &t6103_TI, &t6103_0_0_0, &t6103_1_0_0, NULL, &t6103_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4710_TI;

#include "t327.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ObsoleteAttribute>
extern MethodInfo m31791_MI;
static PropertyInfo t4710____Current_PropertyInfo = 
{
	&t4710_TI, "Current", &m31791_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4710_PIs[] =
{
	&t4710____Current_PropertyInfo,
	NULL
};
extern Il2CppType t327_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31791_GM;
MethodInfo m31791_MI = 
{
	"get_Current", NULL, &t4710_TI, &t327_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31791_GM};
static MethodInfo* t4710_MIs[] =
{
	&m31791_MI,
	NULL
};
static TypeInfo* t4710_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4710_0_0_0;
extern Il2CppType t4710_1_0_0;
struct t4710;
extern Il2CppGenericClass t4710_GC;
TypeInfo t4710_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4710_MIs, t4710_PIs, NULL, NULL, NULL, NULL, NULL, &t4710_TI, t4710_ITIs, NULL, &EmptyCustomAttributesCache, &t4710_TI, &t4710_0_0_0, &t4710_1_0_0, NULL, &t4710_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3302.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3302_TI;
#include "t3302MD.h"

extern TypeInfo t327_TI;
extern MethodInfo m18396_MI;
extern MethodInfo m24311_MI;
struct t20;
#define m24311(__this, p0, method) (t327 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ObsoleteAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3302_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3302_TI, offsetof(t3302, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3302_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3302_TI, offsetof(t3302, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3302_FIs[] =
{
	&t3302_f0_FieldInfo,
	&t3302_f1_FieldInfo,
	NULL
};
extern MethodInfo m18393_MI;
static PropertyInfo t3302____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3302_TI, "System.Collections.IEnumerator.Current", &m18393_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3302____Current_PropertyInfo = 
{
	&t3302_TI, "Current", &m18396_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3302_PIs[] =
{
	&t3302____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3302____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3302_m18392_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18392_GM;
MethodInfo m18392_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3302_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3302_m18392_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18392_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18393_GM;
MethodInfo m18393_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3302_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18393_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18394_GM;
MethodInfo m18394_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3302_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18394_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18395_GM;
MethodInfo m18395_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3302_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18395_GM};
extern Il2CppType t327_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18396_GM;
MethodInfo m18396_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3302_TI, &t327_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18396_GM};
static MethodInfo* t3302_MIs[] =
{
	&m18392_MI,
	&m18393_MI,
	&m18394_MI,
	&m18395_MI,
	&m18396_MI,
	NULL
};
extern MethodInfo m18395_MI;
extern MethodInfo m18394_MI;
static MethodInfo* t3302_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18393_MI,
	&m18395_MI,
	&m18394_MI,
	&m18396_MI,
};
static TypeInfo* t3302_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4710_TI,
};
static Il2CppInterfaceOffsetPair t3302_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4710_TI, 7},
};
extern TypeInfo t327_TI;
static Il2CppRGCTXData t3302_RGCTXData[3] = 
{
	&m18396_MI/* Method Usage */,
	&t327_TI/* Class Usage */,
	&m24311_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3302_0_0_0;
extern Il2CppType t3302_1_0_0;
extern Il2CppGenericClass t3302_GC;
TypeInfo t3302_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3302_MIs, t3302_PIs, t3302_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3302_TI, t3302_ITIs, t3302_VT, &EmptyCustomAttributesCache, &t3302_TI, &t3302_0_0_0, &t3302_1_0_0, t3302_IOs, &t3302_GC, NULL, NULL, NULL, t3302_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3302)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6105_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ObsoleteAttribute>
extern MethodInfo m31792_MI;
static PropertyInfo t6105____Count_PropertyInfo = 
{
	&t6105_TI, "Count", &m31792_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31793_MI;
static PropertyInfo t6105____IsReadOnly_PropertyInfo = 
{
	&t6105_TI, "IsReadOnly", &m31793_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6105_PIs[] =
{
	&t6105____Count_PropertyInfo,
	&t6105____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31792_GM;
MethodInfo m31792_MI = 
{
	"get_Count", NULL, &t6105_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31792_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31793_GM;
MethodInfo m31793_MI = 
{
	"get_IsReadOnly", NULL, &t6105_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31793_GM};
extern Il2CppType t327_0_0_0;
extern Il2CppType t327_0_0_0;
static ParameterInfo t6105_m31794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t327_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31794_GM;
MethodInfo m31794_MI = 
{
	"Add", NULL, &t6105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6105_m31794_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31794_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31795_GM;
MethodInfo m31795_MI = 
{
	"Clear", NULL, &t6105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31795_GM};
extern Il2CppType t327_0_0_0;
static ParameterInfo t6105_m31796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t327_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31796_GM;
MethodInfo m31796_MI = 
{
	"Contains", NULL, &t6105_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6105_m31796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31796_GM};
extern Il2CppType t3578_0_0_0;
extern Il2CppType t3578_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6105_m31797_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3578_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31797_GM;
MethodInfo m31797_MI = 
{
	"CopyTo", NULL, &t6105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6105_m31797_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31797_GM};
extern Il2CppType t327_0_0_0;
static ParameterInfo t6105_m31798_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t327_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31798_GM;
MethodInfo m31798_MI = 
{
	"Remove", NULL, &t6105_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6105_m31798_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31798_GM};
static MethodInfo* t6105_MIs[] =
{
	&m31792_MI,
	&m31793_MI,
	&m31794_MI,
	&m31795_MI,
	&m31796_MI,
	&m31797_MI,
	&m31798_MI,
	NULL
};
extern TypeInfo t6107_TI;
static TypeInfo* t6105_ITIs[] = 
{
	&t603_TI,
	&t6107_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6105_0_0_0;
extern Il2CppType t6105_1_0_0;
struct t6105;
extern Il2CppGenericClass t6105_GC;
TypeInfo t6105_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6105_MIs, t6105_PIs, NULL, NULL, NULL, NULL, NULL, &t6105_TI, t6105_ITIs, NULL, &EmptyCustomAttributesCache, &t6105_TI, &t6105_0_0_0, &t6105_1_0_0, NULL, &t6105_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ObsoleteAttribute>
extern Il2CppType t4710_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31799_GM;
MethodInfo m31799_MI = 
{
	"GetEnumerator", NULL, &t6107_TI, &t4710_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31799_GM};
static MethodInfo* t6107_MIs[] =
{
	&m31799_MI,
	NULL
};
static TypeInfo* t6107_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6107_0_0_0;
extern Il2CppType t6107_1_0_0;
struct t6107;
extern Il2CppGenericClass t6107_GC;
TypeInfo t6107_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6107_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6107_TI, t6107_ITIs, NULL, &EmptyCustomAttributesCache, &t6107_TI, &t6107_0_0_0, &t6107_1_0_0, NULL, &t6107_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
