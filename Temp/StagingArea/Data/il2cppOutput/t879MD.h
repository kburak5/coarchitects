﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t879;
struct t7;
struct t875;
#include "t845.h"
#include "t866.h"

 void m3854 (t879 * __this, t875 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3855 (t879 * __this, t875 * p0, int32_t p1, int32_t p2, t7* p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3856 (t879 * __this, t875 * p0, int32_t p1, int32_t p2, uint16_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3857 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3858 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3859 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3860 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3861 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3862 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3863 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3864 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3865 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3866 (t879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t866  m3867 (t879 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
