﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1296;
struct t446;
struct t7;
struct t1291;
struct t1094;
struct t29;
struct t42;
#include "t1292.h"

 void m6870 (t1296 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6871 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6872 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1296 * m6873 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6874 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1296 * m6875 (t29 * __this, t1296 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6876 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6877 (t1296 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6878 (t1296 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6879 (t1296 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6880 (t1296 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6881 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6882 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6883 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6884 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6885 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6886 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6887 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6888 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6889 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6890 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6891 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6892 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6893 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6894 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6895 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1296 * m6896 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1296 * m6897 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1291 * m6898 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6899 (t1296 * __this, t1291 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6900 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6901 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6902 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6903 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6904 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6905 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6906 (t1296 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6907 (t1296 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6908 (t1296 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6909 (t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m6910 (t1296 * __this, t446* p0, t446* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
