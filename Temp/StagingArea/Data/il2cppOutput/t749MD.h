﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t749;
struct t730;
struct t733;
struct t7;
struct t29;
struct t136;
#include "t735.h"

 void m3242 (t749 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3243 (t749 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3244 (t749 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3245 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3246 (t749 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3247 (t749 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3248 (t749 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3249 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3250 (t749 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3251 (t749 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3252 (t749 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3253 (t749 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t730 * m3254 (t749 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3255 (t749 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3256 (t749 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3257 (t749 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3258 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3259 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
