﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1332;
struct t958;
struct t200;
struct t7;

 void m7240 (t1332 * __this, t958 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7241 (t1332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7242 (t1332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7243 (t1332 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7244 (t1332 * __this, t200* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7245 (t1332 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7246 (t1332 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7247 (t1332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7248 (t1332 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
