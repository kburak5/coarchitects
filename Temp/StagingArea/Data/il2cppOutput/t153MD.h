﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t153;
struct t15;
struct t148;

 void m436 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m437 (t29 * __this, t15 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m438 (t29 * __this, t148 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m439 (t29 * __this, t15 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
