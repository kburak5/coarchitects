﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2698;
struct t29;
struct t557;
struct t2697;
struct t316;

 void m14671 (t2698 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14672 (t2698 * __this, t2697 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14673 (t2698 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14674 (t2698 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
