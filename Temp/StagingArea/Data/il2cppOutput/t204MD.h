﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t204;
struct t204_marshaled;
struct t7;
struct t29;
#include "t384.h"
#include "t17.h"
#include "t382.h"
#include "t383.h"
#include "t35.h"

 void m1736 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2302 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2303 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2304 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1759 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2305 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2306 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2307 (t204 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1755 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m1757 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2308 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1756 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t204 * m2309 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2310 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2311 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1760 (t29 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2312 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2313 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2314 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2315 (t204 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2316 (t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t204_marshal(const t204& unmarshaled, t204_marshaled& marshaled);
void t204_marshal_back(const t204_marshaled& marshaled, t204& unmarshaled);
void t204_marshal_cleanup(t204_marshaled& marshaled);
