﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2686;
struct t29;
struct t235;

#include "t2001MD.h"
#define m14617(__this, method) (void)m10703_gshared((t2001 *)__this, method)
#define m14618(__this, method) (void)m10704_gshared((t29 *)__this, method)
#define m14619(__this, p0, method) (int32_t)m10705_gshared((t2001 *)__this, (t29 *)p0, method)
#define m14620(__this, p0, p1, method) (bool)m10706_gshared((t2001 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m14621(__this, method) (t2686 *)m10707_gshared((t29 *)__this, method)
