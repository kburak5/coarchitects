﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2402;
struct t29;
struct t20;
#include "t127.h"

 void m12412 (t2402 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12413 (t2402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12414 (t2402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12415 (t2402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t127  m12416 (t2402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
