﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t744;
struct t748;
#include "t465.h"

 void m3221 (t744 * __this, t748 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t748 * m3222 (t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3223 (t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m3224 (t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3225 (t744 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3226 (t744 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3227 (t744 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3228 (t744 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3229 (t744 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3230 (t744 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3231 (t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
