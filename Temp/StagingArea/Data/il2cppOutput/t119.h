﻿#pragma once
#include <stdint.h>
#include "t110.h"
#include "t17.h"
#include "t319.h"
struct t119 
{
	int32_t f0;
	t17  f1;
	t17  f2;
	t17  f3;
	float f4;
	int32_t f5;
	int32_t f6;
};
// Native definition for marshalling of: UnityEngine.Touch
struct t119_marshaled
{
	int32_t f0;
	t17  f1;
	t17  f2;
	t17  f3;
	float f4;
	int32_t f5;
	int32_t f6;
};
