﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t122;
struct t6;
struct t7;

 void m312 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m313 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m314 (t122 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m315 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m316 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m317 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m318 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m319 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m320 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m321 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m322 (t122 * __this, t6 * p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m323 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m324 (t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
