﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t827;
struct t828;
struct t829;
struct t7;

 void m3501 (t827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3502 (t827 * __this, t829 * p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
