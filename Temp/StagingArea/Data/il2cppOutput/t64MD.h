﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t64;
struct t50;
#include "t17.h"
#include "t106.h"

 void m181 (t64 * __this, t50 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m182 (t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m183 (t64 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m184 (t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m185 (t64 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
