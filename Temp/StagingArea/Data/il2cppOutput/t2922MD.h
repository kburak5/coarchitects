﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2922;
struct t29;
struct t20;
#include "t393.h"

 void m16021 (t2922 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16022 (t2922 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16023 (t2922 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16024 (t2922 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16025 (t2922 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
