﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1425;
struct t1433;
struct t7;

 void m7792 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7793 (t1425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1425 * m7794 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7795 (t1425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7796 (t1425 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7797 (t1425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
