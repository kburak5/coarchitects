﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t350;
struct t29;
struct t15;
struct t2474;
struct t20;
struct t136;
struct t2475;
struct t2476;
struct t2477;
struct t2473;
struct t2478;
struct t2479;
#include "t2480.h"

#include "t294MD.h"
#define m1543(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m12983(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m12984(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m12985(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m12986(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m12987(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m12988(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m12989(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m12990(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m12991(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m12992(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m12993(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m12994(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m12995(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m12996(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m12997(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m12998(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m12999(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1549(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m13000(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m13001(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m13002(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m13003(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m13004(__this, method) (t2477 *)m10583_gshared((t294 *)__this, method)
#define m13005(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m13006(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m13007(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m13008(__this, p0, method) (t15 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m13009(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m13010(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2480  m13011 (t350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13012(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m13013(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m13014(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m13015(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13016(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m1550(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m13017(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m13018(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m13019(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m13020(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m13021(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m13022(__this, method) (t2473*)m10619_gshared((t294 *)__this, method)
#define m13023(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m13024(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m13025(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1548(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1547(__this, p0, method) (t15 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m13026(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
