﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1089;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 bool m5389 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5390 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5391 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5392 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5393 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5394 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5395 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5396 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5397 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5398 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5399 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5400 (uint64_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5401 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5402 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5403 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5404 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5405 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5406 (uint64_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5407 (uint64_t* __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5408 (uint64_t* __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5409 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5410 (t29 * __this, t7* p0, int32_t p1, t29 * p2, bool p3, uint64_t* p4, t295 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5411 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5412 (t29 * __this, t7* p0, uint64_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5413 (uint64_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5108 (uint64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5414 (uint64_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5415 (uint64_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
