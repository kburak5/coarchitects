﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t268;
struct t29;
struct t28;
struct t2239;
struct t20;
struct t136;
struct t2240;
struct t2241;
struct t2242;
struct t2238;
struct t267;
struct t2243;
#include "t2244.h"

#include "t294MD.h"
#define m11119(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m11120(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m11121(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m11122(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m11123(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m11124(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m11125(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m11126(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m11127(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m11128(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11129(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m11130(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m11131(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m11132(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m11133(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m11134(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m11135(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m11136(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11137(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m11138(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m11139(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m11140(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m11141(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m11142(__this, method) (t2242 *)m10583_gshared((t294 *)__this, method)
#define m2035(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m11143(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m11144(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m11145(__this, p0, method) (t28 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11146(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m11147(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2244  m11148 (t268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m11149(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m11150(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m11151(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m11152(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11153(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m11154(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m2001(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11155(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m11156(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m11157(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m11158(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m11159(__this, method) (t2238*)m10619_gshared((t294 *)__this, method)
#define m11160(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m11161(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m11162(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1558(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1556(__this, p0, method) (t28 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m11163(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
